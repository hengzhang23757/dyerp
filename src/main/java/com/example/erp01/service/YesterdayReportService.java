package com.example.erp01.service;

import com.example.erp01.model.*;

import java.util.List;

public interface YesterdayReportService {

    List<YesterdayTailor> getYesterdayTailorReport();

    List<YesterdayReWork> getYesterdayReWorkReport();

    List<YesterdayWorkShop> getYesterdayHangProduction();

    List<YesterdayWorkShop> getYesterdayMiniProduction();

    Integer getHangWorkCountByProcedureNumber(String orderName, Integer procedureNumber);

    Integer getMiniWorkCountByProcedureNumber(String orderName, Integer procedureNumber);

    List<YesterdayFinish> getYesterdayFinishReport();

    List<YesterdayFinish> getYesterdayHangFinishReport();

    List<DailyFabric> getYesterdayFabricReport();
}
