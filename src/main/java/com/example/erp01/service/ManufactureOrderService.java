package com.example.erp01.service;

import com.example.erp01.model.ManufactureOrder;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ManufactureOrderService {

    Integer addManufactureOrder(ManufactureOrder manufactureOrder);

    ManufactureOrder getManufactureOrderByOrderName(String orderName);

    Integer deleteManufactureByOrder(String orderName);

    Integer updateManufactureOrder(ManufactureOrder manufactureOrder);

    List<String> getOrderNameByCustomerSeason(String season, String customerName);

    List<ManufactureOrder> getManufactureOrderByOrderNameList(List<String> orderNameList);

    List<String> getHistoryDistinctSeason();

    List<String> getHistoryCustomer();

    List<ManufactureOrder> getManufactureOrderListByModelSeason(String modelType, List<String> seasonList, List<String> customerNameList);

    Integer changeModifyStateByOrderName(String orderName, String modifyState);

    List<ManufactureOrder> getManufactureOrderByModifyState(String modifyState);

    void changeOrderName(String orderName, String toOrderName);

}
