package com.example.erp01.service;

import com.example.erp01.model.*;

import java.util.List;
import java.util.Map;

public interface EmpSkillsService {

    Integer addEmpSkillsBatch(List<EmpSkills> empSkillsList);

    List<EmpSkills> getEmpSkillsByInfo(String groupName, Integer procedureCode, String employeeNumber);

    List<EmpSkills> getDistinctHistoryPieceWorkDetail();

    List<Float> getPieceWorkDetailByInfo(Integer procedureCode, String employeeNumber);

    List<EmpSkills> getYesterdayEmpSkills();

    List<EmpSkills> getEmpSkillsByGroupProcedureCode(String groupName, List<Integer> procedureCodeList);

    List<EmpSkills> getAllEmpSkills();

    Integer updateEmpSkillsBatch(List<EmpSkills> empSkillsList);

    Integer dailyCleanEmpSkills();

    List<Integer> getProcedureCodeYesterday();

    MinMaxNid getYesterdayMinMaxNid();

    List<ProcedureTemplate> getProcedureTemplateByProcedureCode(List<Integer> procedureCodeList);

    Integer updateSamByProcedureCode(Float SAM, Integer procedureCode, Long beginNid, Long endNid);

    List<Metre> getProcedureCombineOfYesterday();

    Integer updateMetreBatch(List<Metre> metreList);

    List<EmpSkills> getSkillsByMap(Map<String, Object> params);
}
