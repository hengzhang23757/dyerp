package com.example.erp01.service;

import com.example.erp01.model.CutStorageQuery;
import com.example.erp01.model.Storage;
import com.example.erp01.model.StorageFloor;
import com.example.erp01.model.StorageState;
import org.apache.ibatis.annotations.Param;

import java.util.List;

//@Service
public interface StorageService {

    int inStore(List<Storage> storageList);

    int outStore(List<String> subQcodeList);

    int changeStore(String changestoreJson);

    List<StorageState> getStorageState();

    List<StorageFloor> getStorageFloor();

    List<String> storageReport(String storehouseLocation);

    int outStoreOne(String subTailorQcode);

    List<CutStorageQuery> getCutStorageByOrder(String orderName, String colorName, String sizeName);

    List<String> getStorageColorHint(String orderName);

    List<String> getStorageSizeHint(String orderName, String colorName);

    int deleteStorageOnFinish(String orderName, Integer bedNumber, String colorName, String sizeName, String storehouseLocation);

    List<Storage> getStorageByStoreHouseLocation(String storehouseLocation);

    List<Storage> getAllStorage();

    List<Storage> getStorageByOrderColorSizeLocation(String orderName, String colorName, String sizeName, String storehouseLocation);

    Integer deleteStorageById(Integer storageID);

    Integer deleteStorageByInfo(String orderName, Integer bedNumber, Integer packageNumber);

    List<String> getMatch(String orderName, Integer bedNumber, Integer packageNumber);

    List<Storage> getStorageByInfo(String orderName, String colorName, String sizeName);
}
