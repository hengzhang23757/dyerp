package com.example.erp01.service;

import com.example.erp01.model.*;
import org.apache.ibatis.annotations.Param;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Date;
import java.util.List;

public interface TailorService {

    int saveTailorData(List<Tailor> tailorList);

    Integer getMaxBedNumber(String orderName);

    Integer getMaxPackageNumberByOrder(String orderName, Integer tailorType);

    int deleteTailor(Integer tailorID);

    int deleteTailorByOrderBed(String orderName,Integer bedNumber);

    int deleteTailorByOrderBedPack(String orderName,Integer bedNumber,List<Integer> packageNumberList);

    List<Integer> getPackageNumbersByOrderBedColor(String orderName, Integer bedNumber, String colorName);

    Integer updateTailorData(List<Tailor> tailorList);

    Tailor getTailorByTailorQcodeID(Integer tailorQcodeID, Integer tailorType);

    List<Tailor> getTailorsByTailorQcodeIDs(List<Integer> tailorQcodeIDList, Integer tailorType);

    List<Tailor> getBedInfoInDelete(String orderName);

    List<Tailor> getPackageInfoInDelete(String orderName, Integer bedNumber);

    List<Integer> getBedNumbersByOrderNamePartNameTailorType(String orderName, String partName, Integer tailorType);

    List<TailorReport> tailorReport(String orderName, Integer bedNumber, Integer tailorType);

    List<CutQueryLeak> getTailorInfoByOrderPartType(String orderName, String partName, Integer tailorType, String from, String to);

    List<TailorMonthReport> tailorMonthReport(Date from, Date to, String orderName, String partName, Integer tailorType, String groupName);

    List<TailorMonthReport> otherTailorMonthReport(Date from, Date to, String orderName, String partName, Integer tailorType, String groupName);

    List<Tailor> getTailorInfoGroupByColorSizePartType(String orderName, String partName, Integer tailorType);

    List<Tailor> getTailorInfoGroupByColorPartType(String orderName, String partName, Integer tailorType);

    List<Completion> getTailorCompletion(String orderName, String partName, Integer tailorType);

    List<CutLocation> getPackageInfo( String orderName, Integer bedNumber, String colorName, String sizeName, Integer packageNumber);

    List<Tailor> getTailorByInfo(String orderName, String colorName, String sizeName, String partName, Integer bedNumber, Integer packageNumber, Integer tailorType, Integer isDelete);

    List<WeekAmount> getTailorWeekAmount(String partName, Integer tailorType);

    Integer getTailorCountByInfo(String orderName, String colorName, String sizeName, String partName, Integer bedNumber, Integer packageNumber, Integer tailorType);

    Integer getWellCountByInfo(String orderName, String colorName, String sizeName, String partName, Integer bedNumber, Integer packageNumber, Integer tailorType);

    Integer preDeleteInDeleteBed(String orderName, Integer bedNumber);

    Integer preDeleteInDeletePackage(String orderName,Integer bedNumber,List<Integer> packageNumberList);

    Integer getWellCountByColorSizeList(String orderName, List<String> colorList, List<String> sizeList);

    List<Tailor> getTailorByOrderPartNameColorSize(String orderName, Integer tailorType, String partName , String colorName , String sizeName, Integer isDelete);

    List<TailorMatch> getRecentWeekCutInfo();

    List<TailorMatch> getMainTailorMatchDataByInfo(List<String> orderNameList);

    List<TailorMatch> getOtherTailorMatchDataByInfo(List<String> orderNameList);

    List<CutQueryLeak> getTailorInfoGroupByColorSize(String orderName, String colorName, String sizeName, String partName, Integer tailorType);

    Date getLastCutDateByOrderBed(String orderName, Integer bedNumber);

    Integer getMaxBedNumberByOrderTailorType(String orderName, Integer tailorType);

    List<Tailor> monthTailorReportGroupByBedNumber(String from, String to, String orderName, String groupName);

    List<Tailor> otherMonthTailorReportGroupByBedNumber(String from, String to, String orderName, String groupName);

    Integer updateTailorLayerCountByInfo(String orderName, Integer bedNumber, Integer packageNumber, Integer layerCount);

    List<Tailor> getTailorSummaryByOrderByOrderList(List<String> orderNameList);
}
