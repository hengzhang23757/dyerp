package com.example.erp01.service;

import com.example.erp01.model.ColorManage;

import java.util.List;

public interface ColorManageService {

    Integer addColorManage(ColorManage colorManage);

    Integer deleteColorManage(Integer id);

    List<ColorManage> getAllColorManage();

    Integer updateColorManage(ColorManage colorManage);

}
