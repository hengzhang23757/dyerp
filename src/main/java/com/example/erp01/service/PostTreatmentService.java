package com.example.erp01.service;

import com.example.erp01.model.PieceWork;
import com.example.erp01.model.PostTreatment;
import com.example.erp01.model.Tailor;

import java.util.List;

public interface PostTreatmentService {

    Integer addPostTreatmentBatch(List<PostTreatment> postTreatmentList);

    List<PostTreatment> getPostTreatmentByOrderName(String orderName);

    List<Tailor> getWellCountByOrderNameList(List<String> orderNameList, String from, String to);

    List<PieceWork> getPieceWorkSummaryByOrderNameList(List<String> orderNameList, List<Integer> procedureNumberList, String from, String to);

    List<PostTreatment> getPostTreatmentByOrderNameList(List<String> orderNameList, String from, String to);

    List<PostTreatment> getPostTreatmentByOrderType(String orderName, Integer washCount, Integer shipCount, Integer surplusCount, Integer defectiveCount, Integer chickenCount, Integer sampleCount);

    Integer deletePostTreatmentById(Integer id);

    Integer updatePostTreatment(PostTreatment postTreatment);

}
