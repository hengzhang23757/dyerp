package com.example.erp01.service;

import com.example.erp01.model.*;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface EmbStorageService {

    int embInStore(List<EmbStorage> embStorageList);

    int embOutStore(List<String> embOutStoreList);

    int embChangeStore(String embChangeStoreJson);

    List<EmbStorageState> getEmbStorageState();

    List<StorageFloor> getEmbStorageFloor();

    List<EmbStorageQuery> embStorageQuery(String orderName, String colorName, String sizeName);

    List<String> getEmbOrderHint(String subOrderName);

    List<String> getEmbColorHint(String orderName);

    List<String> getEmbSizeHint(String orderName, String colorName);

    int embOutStoreOne(String subTailorQcode);

    String getLocationByQcode(String tailorQcode);

    List<EmbStorage> getEmbStorageByOrderColorSize(String embStoreLocation, String orderName, String colorName, String sizeName);

    Integer embOutStoreBatch(String embStoreLocation, String orderName, String colorName, String sizeName);

    String getLocationByOrderColorSizePackage(String orderName, Integer bedNumber, String colorName, String sizeName, Integer packageNumber);

    List<EmbStorage> getEmbStorageByEmbStoreLocation(String embStoreLocation);

    List<EmbStorage> getAllEmbStorage();

    List<EmbStorage> getEmbStorageByOrderColorSizeLocation(String orderName, String colorName, String sizeName, String embStoreLocation);

    Integer embOutStoreSingle(List<EmbOutInfo> embOutInfoList);

    Integer deleteEmbStorageByInfo(String orderName, String colorName, String sizeName, String embStoreLocation);

    Integer deleteEmbStorageById(Integer embStorageID);

    Integer deleteEmbStorageByOrderBedPackage(String orderName, Integer bedNumber, Integer packageNumber);

    List<EmbStorage> getEmbStorageGroupByColor(String orderName);

    List<EmbStorage> getEmbStorageSummaryByLocation(String embStoreLocation);

    List<EmbStorage> getEmbStorageDetailByOrderColorSize(String orderName, String colorName, String sizeName);

    List<EmbStorage> getEmbStorageByOrderColorSizeList(String orderName, List<String> colorList, List<String> sizeList);

    List<EmbStorage> getEmbStorageByInfo(String orderName, String colorName, String sizeName);
}
