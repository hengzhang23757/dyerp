package com.example.erp01.service;

import com.example.erp01.model.BasicFabric;

import java.util.List;

public interface BasicFabricService {

    int addBasicFabricBatch(List<BasicFabric> basicFabricList);

    List<BasicFabric> getAllBasicFabric();

    List<BasicFabric> getThreeMonthsBasicFabric();

    int deleteBasicFabric(Integer basicFabricID);

    int updateBasicFabric(BasicFabric basicFabric);

    List<String> getBasicFabricHint(String subFabricName);

    List<String> getFabricSupplierHint(String basicFabricName);

    String getBasicFabricNumberByNameSupplier(String basicFabricName, String basicFabricSupplier);

    List<String> getBasicFabricColorByNameSupplier(String basicFabricName, String basicFabricSupplier);

    String getBasicFabricColorNumberByNameSupplierColor(String basicFabricName, String basicFabricSupplier, String basicFabricColor);

    List<String> getBasicFabricWidthByNameSupplierColor(String basicFabricName, String basicFabricSupplier, String basicFabricColor);

    List<String> getBasicFabricWeightByNameSupplierColorWidth(String basicFabricName, String basicFabricSupplier, String basicFabricColor, String basicFabricWidth);

}
