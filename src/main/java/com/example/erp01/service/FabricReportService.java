package com.example.erp01.service;

import com.example.erp01.model.*;

import java.util.Date;
import java.util.List;

public interface FabricReportService {

    List<FabricPlan> getFabricPlanByOrder(String orderName);

    List<FabricDetail> getFabricDetailByOrder(String orderName, String colorName);

    List<FabricDetail> getAllTypeFabricDetailByOrder(String orderName, String colorName);

    List<FabricStorage> getFabricStorageByOrder(String orderName, String colorName);

    List<FabricOutRecord> getFabricOutRecordByOrder(String orderName, String colorName);

    List<LooseFabric> getLooseFabricByInfo(Date from, Date to, String orderName);

}
