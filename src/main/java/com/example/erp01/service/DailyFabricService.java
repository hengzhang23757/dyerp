package com.example.erp01.service;

import com.example.erp01.model.DailyFabric;

import java.util.List;

public interface DailyFabricService {

    Integer addDailyFabricBatch(List<DailyFabric> dailyFabricList);

    List<DailyFabric> getDailyFabricOneMonth();

}
