package com.example.erp01.service;

import com.example.erp01.model.OrderProcedureDetail;

import java.util.List;

public interface OrderProcedureDetailService {

    int addOrderProcedureDetail(List<OrderProcedureDetail> orderProcedureDetailList);

    int deleteOrderProcedureByOrderName(String orderName);

    List<OrderProcedureDetail> getOrderProcedureDetailByOrderName(String orderName);

    int updateOrderProcedureDetailBatch(List<OrderProcedureDetail> orderProcedureDetailList);

    int deleteOrderProcedureDetailBatch(List<Integer> idList);

}
