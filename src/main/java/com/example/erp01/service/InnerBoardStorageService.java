package com.example.erp01.service;

import com.example.erp01.model.InnerBoardStorage;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface InnerBoardStorageService {

    int innerBoardInStore(InnerBoardStorage innerBoardStorage);

    int innerBoardOutStore(InnerBoardStorage innerBoardStorage);

    int deleteInnerBoardStorage(Integer innerBoardStorageID);

    List<InnerBoardStorage> getAllInnerBoardStorage();

    int updateInnerBoardStorage(InnerBoardStorage innerBoardStorage);

    List<String> getLocationHint(String versionNumber, String fabricNumber, String colorNumber, String jarNumber);

    InnerBoardStorage getInnerBoardStorage(String location,String versionNumber,String fabricNumber,String colorNumber,String jarNumber);

}
