package com.example.erp01.service;

import com.example.erp01.model.AccessoryInStore;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AccessoryInStoreService {

    int addAccessoryInStoreBatch(List<AccessoryInStore> accessoryInStoreList);

    int deleteAccessoryInStoreByID(Integer id);

    List<AccessoryInStore> getAccessoryInStoreByOrder(String orderName);

    AccessoryInStore getAccessoryInStoreById(Integer id);

    List<AccessoryInStore> getAccessoryInStoreByInfo(String orderName, String accessoryName, String specification, String accessoryColor, String colorName, String sizeName, Integer accessoryID);

    List<AccessoryInStore> getTodayAccessoryInStore();

    List<AccessoryInStore> getAccessoryInStoreByColorSizeList(String orderName, List<String> colorList, List<String> sizeList);

    List<AccessoryInStore> getAccessoryInStoreByTime(String from, String to);

    List<AccessoryInStore> getAccessoryInStoreByIdList(List<Integer> accessoryIdList);

    Integer updateAccessoryInStoreInChange(AccessoryInStore accessoryInStore);

    List<AccessoryInStore> getBjInStoreByOrder(String orderName);

    List<Integer> getAccessoryIdListByAddFee(List<Integer> idList);
}
