package com.example.erp01.service;

import com.example.erp01.model.ClothesVersionProcess;

import java.util.List;

public interface ClothesVersionProcessService {

    int addClothesVersionProcess(ClothesVersionProcess clothesVersionProcess);

    List<ClothesVersionProcess> getUniqueClothesVersionProcess();

    List<ClothesVersionProcess> getClothesVersionProcessByOrder(String orderName);

    int addClothesVersionProcessBatch(List<ClothesVersionProcess> clothesVersionProcessList);

    int deleteClothesVersionProcess(Integer clothesVersionProcessID);

    int deleteClothesVersionProcessByOrder(String orderName);

}
