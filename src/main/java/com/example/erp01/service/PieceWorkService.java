package com.example.erp01.service;

import com.example.erp01.model.*;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface PieceWorkService {

    int addPieceWorkBatch(List<PieceWork> pieceWorkList);

    int addPieceWork(PieceWork pieceWork);

    int deletePieceWork(Integer id);

    PieceWork getPieceWorkByID(Integer id);

    List<PieceWork> getAllPieceWork();

    List<PieceWork> getPieceWorkByEmpNum(String employeeNumber);

    List<PieceWork> getPieceWorkByTime(Date from, Date to);

    List<PieceWork> getPieceWorkByEmpNumTime(String employeeNumber, Date from, Date to);

    List<PieceWork> getDetailPieceWork(Date from, Date to, String groupName, String employeeNumber);

    List<PieceWorkResult> getPieceWorkToday();

    List<PieceWorkResult> getPieceWorkThisMonth();

    List<PieceWork> getPieceWorkEmpToday(String employeeNumber);

    List<PieceWork> getPieceWorkEmpThisMonth(String employeeNumber);

    List<Object> queryPieceWorkToday(String groupName, String employeeNumber);

    List<Object> queryPieceWorkThisMonth(String groupName, String employeeNumber);

    List<SalaryCount> queryPieceWork(Date from, Date to, String groupName, String employeeNumber);

    List<PieceWork> getTodaySummary(String employeeNumber);

    List<PieceWork> getThisMonthSummary(String employeeNumber);

    List<PieceWork> getPieceWorkByOrderBedPackID(String orderName, Integer bedNumber, Integer packageNumber, Integer procedureNumber, Integer tailorQcodeID);

    List<PieceWork> getPieceWorkByOrderBedPack(String orderName, Integer bedNumber, Integer packageNumber, Integer procedureNumber);

    int addPieceWorkBatchNew(List<PieceWork> pieceWorkList);

    int addPieceWorkTotal(PieceWork pieceWork);

    List<MiniDetailQuery> getDetailProductionByEmp(Date from, Date to, String employeeNumber, String orderName, String colorName, String sizeName);

    List<GeneralSalary> getMiniSalaryByTimeEmp(Date from, Date to, String employeeNumber);

    List<GeneralSalary> getGroupMiniSalaryByTime(Date from, Date to, String groupName);

    List<PieceWorkEmp> getProcedureInfoByOrderBedPackage(String orderName, Integer bedNumber, Integer packageNumber);

    List<ManualInput> getAllManualInput();

    List<Unload> getAllUnloadRecord();

    Integer getPieceCountByOrderColorSize(String orderName, Integer procedureNumber, String colorName, String sizeName);

    List<SalaryCount> getMiniProcedureProgress(Date from, Date to, Integer procedureFrom, Integer procedureTo,String orderName,String groupName);

    List<CutLocation> getPieceWorkInfo(String orderName, Integer bedNumber, String colorName, String sizeName);

    List<WeekAmount> getPieceWeekAmount();

    List<WeekAmount> getFinishWeekAmount();

    List<CompletionCount> getPieceWorkCompletion(Integer procedureNumber);

    List<SalaryCount> getOrderMiniSalarySummary(String orderName,Date from,Date to);

    List<ProductionProgressDetail> getUnloadByOrderTime(String orderName, Date from, Date to);

    int updateManualInput(PieceWork pieceWork);

    List<ManualInput> getTodayManualInput();

    List<ManualInput> getOneMonthManualInput();

    List<ManualInput> getThreeMonthsManualInput();

    List<PieceWorkResult> searchPieceWorkManagement(String orderName, String colorName, String sizeName, Integer bedNumber, Integer packageNumber, Integer procedureNumber, Integer page, Integer limit);

    Integer deletePieceWorkBatch(List<Integer> pieceWorkIDList);

    List<GeneralSalary> getMiniProcedureProgressNew(Date from, Date to, Integer procedureFrom, Integer procedureTo, String orderName, String groupName, String employeeNumber);

    List<CutLocation> getMatchInfo(String orderName, Integer bedNumber, String colorName, String sizeName);

    List<GeneralSalary> getMiniSalaryDetail(Date from, Date to, String employeeNumber, String orderName);

    List<GeneralSalary> getMiniSalaryByOrderColorSizeEmp(Date from, Date to, String employeeNumber, String orderName, String colorName, String sizeName);

    List<GeneralSalary> getMiniProcedureProgressMultiGroup(Date from, Date to, Integer procedureFrom, Integer procedureTo, String orderName, List<String> groupList, String employeeNumber);

    List<GeneralSalary> getDetailProductionByInfo(String from, String to, String employeeNumber, String orderName, String colorName, String sizeName);

    List<GeneralSalary> getDetailProductionOfEachLine(Date pieceDate, String employeeNumber, String orderName, Integer procedureNumber, String colorName, String sizeName);

    List<GeneralSalary> getDetailProductionSummaryByInfo(String from, String to, String employeeNumber, String orderName, String colorName, String sizeName);

    List<GeneralSalary> getDetailProductionDetailByInfo(String from, String to, String employeeNumber, String orderName, String colorName, String sizeName);

    Integer updateProcedureNameByOrderProcedure(String orderName, Integer procedureNumber, String procedureName);

    List<GeneralSalary> getPieceInfoGroupByOrderColor(String orderName, Integer procedureNumber);

    List<PieceWork> getPieceWorkByOrderNameProcedureNumber(String orderName, Integer procedureNumber, String colorName, String sizeName);

    Integer updateGroupNameByEmployee(Date from, Date to, String employeeNumber, String groupName);

    List<GeneralSalary> getGroupPieceWorkSummary(String from, String to, String orderName, String groupName);

    List<PieceWork> getPieceWorkByOrderNameBedNumberPackage(String orderName, Integer bedNumber, Integer packageNumber);

    Integer getPieceCountOfToday(String employeeNumber);

    List<GeneralSalary> getPackageDetailByInfo(Date from, Date to, String employeeNumber);

    List<CutQueryLeak> getProductionByProcedureColorSize(String orderName, Integer procedureNumber, String colorName, String sizeName);

    Integer addPieceWorkTotalBatch(List<PieceWork> pieceWorkList);

    Integer updatePieceWorkLayerCountFinance(Integer pieceWorkID, Integer layerCount);

    List<ManualInput> getByInfo(Map<String, Object> map);
}
