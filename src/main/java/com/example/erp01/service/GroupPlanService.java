package com.example.erp01.service;

import com.example.erp01.model.GroupPlan;

import java.util.List;

public interface GroupPlanService {

    int addGroupPlan(GroupPlan groupPlan);

    int deleteGroupPlan(Integer groupPlanID);

    List<GroupPlan> getAllGroupPlan();

    GroupPlan getGroupPlanByOrderGroup(String orderName, String groupName);

    int updateGroupPlan(GroupPlan groupPlan);

}
