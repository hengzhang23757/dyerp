package com.example.erp01.service;

import com.example.erp01.model.GeneralSalary;
import com.example.erp01.model.HourEmp;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

public interface HourEmpService {

    int addHourEmp(HourEmp hourEmp);

    List<HourEmp> getAllHourEmp();

    int deleteHourEmp(Integer hourEmpID);

    List<GeneralSalary> getHourEmpByTimeEmp(Date from, Date to, String employeeNumber);

    List<GeneralSalary> getGroupHourEmpByTime(Date from, Date to, String groupName);

    List<HourEmp> getHourProcedureProgress(Date from, Date to, Integer procedureFrom, Integer procedureTo, String orderName, String groupName);

    List<HourEmp> getOrderHourEmpDetail(String orderName, Date from, Date to);

    List<HourEmp> getOrderHourEmpSummary(String orderName,Date from,Date to);

    int updateHourEmp(HourEmp hourEmp);

    List<HourEmp> getTodayHourEmp();

    List<HourEmp> getOneMonthHourEmp();

    List<HourEmp> getThreeMonthsHourEmp();

    List<GeneralSalary> getHourProcedureProgressNew(Date from, Date to, Integer procedureFrom, Integer procedureTo, String orderName, String groupName, String employeeNumber);

    List<HourEmp> getHourProcedureProgressMultiGroup(Date from, Date to, Integer procedureFrom, Integer procedureTo, String orderName, List<String> groupList, String employeeNumber);

}
