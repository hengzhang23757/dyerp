package com.example.erp01.service;

import com.example.erp01.model.DeliveryReport;
import com.example.erp01.model.OutBoundReport;

import java.util.List;

public interface OutBoundService {

    List<Integer> getBedNumbersOfOutBound(String customerName, String orderName, String outOrderName, String printingPart, String colorName);

    List<OutBoundReport> getOutBoundDetailByCustomerOrder(String customerName, String orderName, String outOrderName, String printingPart, String colorName, List<Integer> bedNumberList);

    List<DeliveryReport> getDeliveryReport(String customerName, String orderName, String outOrderName, String printingPart, String colorName, List<Integer> bedNumberList);

}
