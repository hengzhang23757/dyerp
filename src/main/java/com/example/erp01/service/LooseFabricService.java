package com.example.erp01.service;

import com.example.erp01.model.LooseFabric;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface LooseFabricService {

    Integer saveLooseFabricData(List<LooseFabric> looseFabricList);

    Integer getFinishLooseFabricCount(String styleNumber,String colorName, String fabricName, String fabricColor, String jarNumber);

    LooseFabric getLooseFabricByID(Integer qCodeID);

    List<LooseFabric> getLooseFabricByOrderFabric(String orderName, String colorName, String fabricName, String fabricColor);

    Integer updateLooseFabric(List<LooseFabric> looseFabricList);

    List<LooseFabric> getMaxLooseFabricInfoByJar(String orderName, String fabricName, String fabricColor);

    List<LooseFabric> getFabricJarByInfo(String orderName, String colorName, String fabricName, String fabricColor);

    Integer updateJarByInfo(String orderName, String fabricName, String fabricColor, String jarNumber, String colorName, String updateJar);

    List<LooseFabric> getMaxLooseFabricInfoByInfo(String orderName, String colorName, String fabricName, String fabricColor);

    Integer getLooseCountByOrder(String orderName, List<String> colorNameList, Integer cutState);

    Integer changeStateInTailorScan(Integer qCodeID);

    Float getPreCutFabricWeightByOrderColor(String orderName, String colorName, String jarNumber);

    Integer deleteLooseFabricByOrderNameJar(String orderName, String jarNumber);
}
