package com.example.erp01.service;

import com.example.erp01.model.*;

import java.util.List;

public interface RpcService {

    List<XhlTailorReport> getInStoreSummary(String customerName, String orderName);

    List<XhlTailor> getInStoreDetail(String customerName, String orderName, String colorName, String sizeName, String printingPart);

    List<OutBoundReport> getOutBoundSummary(String customerName, String orderName, String from, String to);

    List<XhlPieceWork> getOutBoundDetail(String customerName, String orderName, String colorName, String sizeName, String printingPart);

    List<PieceWorkReport> getPieceWorkSummary(String customerName, String orderName);

    List<XhlPieceWork> getPieceWorkDetail(String customerName, String orderName, String colorName, String sizeName, String printingPart);

    List<OutBoundReport> getInspectionSummary(String customerName, String orderName);

    List<XhlPieceWork> getInspectionDetail(String customerName, String orderName, String colorName, String sizeName, String printingPart);

}
