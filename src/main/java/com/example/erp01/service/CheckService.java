package com.example.erp01.service;

import com.example.erp01.model.AttendLog;
import com.example.erp01.model.DailyCheck;
import com.example.erp01.model.SummaryCheck;

import java.util.Date;
import java.util.List;

public interface CheckService {

    Integer addAttendLogBatch(List<AttendLog> attendLogList);

    List<AttendLog> getAttendLogByEmp(Date fromDate, Date toDate, String ccCount);

    Integer addDailyCheckBatch(List<DailyCheck> dailyCheckList);

    List<DailyCheck> getDailyCheckByMonthGroup(String monthString, String groupName);

    List<AttendLog> getMonthAttendLogByEmp(String monthString, String ccCount);

    Integer addSummaryCheckBatch(List<SummaryCheck> summaryCheckList);

    List<SummaryCheck> getSummaryCheckByMonthGroup(String monthString, String groupName);

}
