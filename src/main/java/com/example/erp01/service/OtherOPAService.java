package com.example.erp01.service;

import com.example.erp01.model.OtherOPA;

import java.util.List;

public interface OtherOPAService {

    int addOtherOPA(OtherOPA otherOPA);

    int addOtherOPABatch(List<OtherOPA> otherOPAList);

    int deleteOtherOPA(Integer otherOpaID);

    List<OtherOPA> getAllOtherOPA();

    List<OtherOPA> getTodayOtherOPA();

    List<OtherOPA> getOneMonthOtherOPA();

    List<OtherOPA> getThreeMonthOtherOPA();

    List<OtherOPA> getOtherOPAByOrder(String orderName);

    List<OtherOPA> getOtherBedOPAByOrder(String orderName);
}
