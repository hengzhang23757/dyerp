package com.example.erp01.service;

public interface MaxTailorQcodeIDService {

   public int getMaxTailorQcodeId();

   public int updateMaxTailorQcodeId(int qCodeId);

}
