package com.example.erp01.service;

import com.example.erp01.model.OrderProcedure;
import com.example.erp01.model.ProcedureInfo;
import com.example.erp01.model.ProcedureTemplate;

import java.util.List;
import java.util.Map;

public interface ProcedureTemplateService {

    Integer addProcedureTemplate(ProcedureTemplate procedureTemplate);

    Integer addProcedureTemplateBatch(List<ProcedureTemplate> procedureTemplateList);

    Integer deleteProcedureTemplate(Integer procedureID);

    Integer deleteProcedureTemplateBatch(List<Integer> procedureIDList);

    List<ProcedureTemplate> getAllProcedureTemplate(Map<String, Object> map);

    Integer getMaxProcedureCode();

    ProcedureTemplate getByID(Integer procedureID);

    String getProcedureNumberByProcedureCode(Integer procedureCode);

    String getProcedureNameByProcedureCode(Integer procedureCode);

    Float getPiecePriceByProcedureCode(Integer procedureCode);

    Integer updateProcedureTemplate(ProcedureTemplate procedureTemplate);

    ProcedureInfo getProcedureInfoByID(Integer procedureID);

    List<ProcedureTemplate> getProcedureTemplateByLevel(String procedureLevel);

    ProcedureTemplate getProcedureTemplateByProcedureCode(Integer procedureCode);

    List<ProcedureTemplate> getProcedureTemplateHintByInfo(Integer procedureNumber, String subProcedureName, String subProcedureDescription, String subRemark,Integer page,Integer limit);

    List<ProcedureTemplate> getOrderProcedureByMap(Map<String, Object> params);
}
