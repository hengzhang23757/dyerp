package com.example.erp01.service;

import com.example.erp01.model.MaxBedNumber;

public interface MaxBedNumberService {

   Integer getMaxBedNumberByOrder(String orderName);

   Integer addMaxBedNumber(MaxBedNumber maxBedNumber);

}
