package com.example.erp01.service;

import com.example.erp01.model.PrintPart;
import com.example.erp01.model.Tailor;

import java.util.List;

public interface SyncTailorService {

    List<Tailor> getTailorByInfo(String orderName, Integer bedNumber, Integer packageNumber);

    Integer addTailorBatch(List<Tailor> tailorList);

    Integer addPrintPartBatch(List<PrintPart> printPartList);

}
