package com.example.erp01.service;

import com.example.erp01.model.OrderMeasureItem;

import java.util.List;

public interface OrderMeasureItemService {

    Integer addOrderMeasureItemBatch(List<OrderMeasureItem> orderMeasureItemList);

    List<OrderMeasureItem> getOrderMeasureItemByOrder(String orderName);

    Integer deleteOrderMeasureItemByOrder(String orderName);

    List<String> getUniquePartCodeListByOrder(String orderName);

}
