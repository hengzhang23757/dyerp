package com.example.erp01.service;

import com.example.erp01.model.*;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface FinishTailorService {

    List<FinishTailor> generateTailorData(String jsonStr);

    int saveTailorData(List<FinishTailor> tailorList);

    Integer getMaxPackageNumberByOrder(String orderName);

    List<String> getTailorQcodeByTailorQcodeID(List<Integer> tailorQcodeIDList);

    String getOneTailorQcodeByTailorQcodeID(Integer tailorQcodeID);

    int deleteTailorByOrderBedPack(String orderName, List<Integer> packageNumberList);

    List<FinishTailor> getFinishTailorByOrderColorSizePart(String orderName, String colorName, String sizeName, String partName);

    List<CutQueryLeak> getFinishTailorInfo(String orderName, String partName);

    Integer getFinishTailorCountByOrderColorSize(String orderName, String colorName, String sizeName, String partName);

    Integer updateTailorData(List<FinishTailor> finishTailorList);

    List<Integer> getTailorQcodeIDByOrderColorSize(@Param("orderName") String orderName, @Param("colorName") String colorName, @Param("sizeName") String sizeName);

    FinishTailor getFinishTailorByTailorQcodeID(@Param("tailorQcodeID") Integer tailorQcodeID);

    List<FinishTailor> getFinishTailorByOrderPartNameColorSize(String orderName, String partName, String colorName, String sizeName);

    Integer updateFinishTailorBatchInCheck(List<FinishTailor> finishTailorList);

    List<FinishTailor> getShelfStorageByShelfName(String shelfName);

    Integer updateFinishTailorBatchOutCheck(List<FinishTailor> finishTailorList);

    Integer updateFinishTailorBatchReturnCheck(List<FinishTailor> finishTailorList);

    List<FinishTailor> getFinishTailorByInfo(Map<String, Object> map);

    Integer updateFinishTailorLayerCountByInfo(String orderName, Integer bedNumber, Integer packageNumber, Integer layerCount);

}
