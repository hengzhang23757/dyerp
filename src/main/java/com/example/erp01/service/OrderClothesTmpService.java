package com.example.erp01.service;

import com.example.erp01.model.OrderClothes;
import com.example.erp01.model.OrderClothesTmp;
import com.example.erp01.model.Tailor;

import java.util.List;

public interface OrderClothesTmpService {

    Integer addOrderClothesTmpBatch(List<OrderClothesTmp> orderClothesTmpList);

    List<String> getYesterdayTailorOrderName();

    List<Tailor> getTailorTotalInfoAmongYesterday(List<String> orderNameList);

    List<OrderClothes> getOrderClothesByOrderNameList(List<String> orderNameList);
}
