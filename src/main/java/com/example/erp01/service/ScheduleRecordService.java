package com.example.erp01.service;

import com.example.erp01.model.Employee;
import com.example.erp01.model.ScheduleRecord;

import java.util.List;

public interface ScheduleRecordService {

    Integer addScheduleRecordBatch(List<ScheduleRecord> scheduleRecordList);

    List<ScheduleRecord> getScheduleRecordByInfo(String orderName, String groupName);

    Integer deleteScheduleRecordByInfo(String orderName, String groupName);

    List<ScheduleRecord> getUniqueScheduleRecordOneYear();

    List<Employee> getEmployeeByOrderGroup(String orderName, String groupName);

}
