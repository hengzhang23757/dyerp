package com.example.erp01.service;

import com.example.erp01.model.CutBedColor;
import com.example.erp01.model.DepartmentPlan;
import com.example.erp01.model.LooseFabric;

import java.util.Date;
import java.util.List;

public interface DepartmentPlanService {

    Integer addDepartmentPlan(DepartmentPlan departmentPlan);

    Integer deleteDepartmentPlan(Integer departmentPlanID);

    Integer updateDepartmentPlan(DepartmentPlan departmentPlan);

    List<DepartmentPlan> getDepartmentPlanOneMonth();

    List<DepartmentPlan> getTailorPlanDetail(String orderName, Date beginDate);

    List<DepartmentPlan> getLoosePlanDetail(String orderName, Date beginDate);

    List<String> getTailorOrderNameOfToday();

    List<String> getLooseOrderNameOfToday();

    List<DepartmentPlan> getDepartmentPlanBetweenToday(String planType, String orderName);

    Integer getWellCountByOrderDate(String orderName, Date beginDate);

    Integer getLooseCountByOrderDate(String orderName, Date beginDate);

    List<DepartmentPlan> getDepartmentPlanByFinishState(String finishState, String orderName, String colorName);

    Integer getMaxPlanOrder();

    Integer updateDepartmentPlanBatch(List<DepartmentPlan> departmentPlanList);

    Integer setDepartmentPlanFinish(Integer departmentPlanID);

}
