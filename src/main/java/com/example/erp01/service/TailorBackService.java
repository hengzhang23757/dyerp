package com.example.erp01.service;

import com.example.erp01.model.TailorBack;

import java.util.List;

public interface TailorBackService {

    int saveTailorBackData(List<TailorBack> tailorBackList);

}
