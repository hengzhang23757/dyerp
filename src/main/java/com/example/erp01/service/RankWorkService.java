package com.example.erp01.service;

import com.example.erp01.model.GeneralSalary;

import java.util.List;

public interface RankWorkService {

    List<GeneralSalary> getGroupRank(String groupName);

}
