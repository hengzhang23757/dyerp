package com.example.erp01.service;

import com.example.erp01.model.*;

import java.util.Date;
import java.util.List;

public interface EmbOutStoreService {

    int addEmbOutStore(List<EmbOutStore> embOutStoreList);

    int addEmbOutStoreOne(EmbOutStore embOutStore);

    int deleteEmbOutStore(Integer embOutStoreID);

    Integer getEmbOutStoreCount(String groupName, String orderName, String colorName, String sizeName, Date embPlanDate);

    List<EmbOutDetail> getEmbOutDetail(Date from, Date to);

    List<TailorReport> getEmbOutPackageDetail(String orderName, String from, String to, String groupName);

    List<CutQueryLeak> getEmbOutInfoByOrder(String orderName);

    List<CutLocation> getEmbOutInfo(String orderName, Integer bedNumber, String colorName, String sizeName, Integer packageNumber);

    List<EmbOutDaily> getEmbOutDaily(String orderName);

    List<WeekAmount> getEmbOutWeekAmount();

    List<CompletionCount> getEmbOutCompletion();

    Integer getEmbOutSumCount(String orderName);

    List<EmbStorage> getEmbOutStoreGroupByColor(String orderName);

    EmbOutStore getEmbOutStoreByInfo(String orderName, Integer bedNumber, Integer packageNumber);

    Integer deleteEmbOutStoreByInfo(String orderName, Integer bedNumber, Integer packageNumber);

    List<CutQueryLeak> getEmbOutStoreGroupByColorSize(String orderName, String colorName, String sizeName);

    List<EmbOutDetail> getAmongEmbOut(Date fromDate, Date toDate, String orderName);

    List<TailorReport> getEmbOutPackageCountByInfo(String orderName, String from,  String to, String groupName);
}
