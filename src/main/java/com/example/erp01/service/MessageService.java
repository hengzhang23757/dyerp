package com.example.erp01.service;

import com.example.erp01.model.Message;

import java.util.List;
import java.util.Map;

public interface MessageService {

    Integer addMessageBatch(List<Message> messageList);

    List<Message> getMessageByInfo(String receiveUser, String messageType);

    Integer updateMessageReadType(List<Integer> idList);

    Integer getReadStateCountByMessageType(String receiveUser, String messageType);

    List<Message> getMessageCountGroupByType(String receiveUser);

    Message getMessageById(Integer id);

    Integer updateMessageReadTypeByReceiveUser(String receiveUser, String messageType);

    List<Message> getSendedMessage(String sendUser);

    Integer countBySender(String sendUser);

    List<Message> getSendedMessageByPage(String sendUser, Integer page, Integer limit);

    Integer autoSendMessageToDepartment(String department, Message message);

    List<Message> getUnFinishMessageByType(Map<String, Object> map);

    List<Message> getMessageContentByInfo(Map<String, Object> map);

    Integer updateMessageBatch(List<Message> messageList);

    Integer getUnFinishCountTotalCount();
}
