package com.example.erp01.service;

import com.example.erp01.model.*;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

public interface OtherTailorService {

    List<OtherTailor> generateOtherTailorData(String jsonStr);

    int saveOtherTailorData(List<OtherTailor> otherTailorList);

    List<OtherTailor> getAllOtherTailorData();

    List<OtherTailor> getAllOtherTailorDataByOrder(String orderName);

    Integer getMaxBedNumberByOrder(String orderName);

    List<OtherTailor> getOtherTailorByOrderNameBedNum(String orderName, int bedNumber);

    int getOtherTailorCountByOrderNameBedNum(String orderName, int bedNumber);

    List<Integer> getOtherBedNumbersByOrderName(String orderName);

    List<String> getOtherSizeNamesByOrderName(String orderName);

    List<String> getOtherPartNamesByOrderName(String orderName);

    Integer getOtherTailorCountByOrderNameBedNumPart(String orderName, int bedNumber, String partName);

    List<OtherTailor> getOtherTailorByOrderNameBedNumPart(String orderName, int bedNumber, String partName);

    List<TailorReport> otherTailorReport(String orderName, String partName, Integer bedNumber);

    Integer getOtherMaxPackageNumberByOrder(String orderName);

    List<OtherTailor> getDetailOtherTailorByOrderNameBedNumber(String orderName, int bedNumber);

    int deleteOtherTailor(Integer otherTailorID);

    int deleteOtherTailorByOrderBed(String orderName, Integer bedNumber);

    int deleteOtherTailorByOrderBedPack(String orderName, Integer bedNumber, List<Integer> packageNumberList);

    List<OtherTailor> getAllOtherTailorByOrderBed(String orderName, Integer bedNumber, Integer packageNumber);

    List<String> getOtherTailorQcodeByTailorQcodeID(List<Integer> tailorQcodeIDList);

    String getOneOtherTailorQcodeByTailorQcodeID(Integer tailorQcodeID);

    List<CutQueryLeak> getOtherTailorInfoByNamePart(String orderName, String partName);

    List<Integer> getOtherBedNumbersByOrderPart(String orderName, String partName);

    Integer deleteByOrderPartBed(String orderName, String partName, Integer bedNumber);

    Integer getOtherMaxPackageNumberByOrderpart(String orderName, String partName);

    List<OtherTailor> getDetailOtherTailorByOrderPartBed(String orderName, String partName, Integer bedNumber);

    List<Integer> getOtherPackageNumbersByOrderBedColor(String orderName, Integer bedNumber, String colorName);

    Integer getOtherLayerCountByOrderBedPackage(String orderName, Integer bedNumber, Integer packageNumber);

    List<OtherTailorMonthReport> getOtherTailorMonthReport(Date from, Date to, String orderName, String groupName);

    Integer getCutCountByOrderPart(String orderName, String partName);

    Integer updateOtherTailorData(List<OtherTailor> otherTailorList);

    List<OtherTailor> getOtherTailorDataByOrderBed(String orderName, Integer bedNumber);

    OtherTailor getOtherTailorByTailorQcodeID(Integer tailorQcodeID);

    List<OtherTailor> getOtherTailorByOrderPartColorSize(String orderName, String partName, String colorName, String sizeName);

    OtherTailor getOneOtherTailorByTailorQcodeID(Integer tailorQcodeID);
}
