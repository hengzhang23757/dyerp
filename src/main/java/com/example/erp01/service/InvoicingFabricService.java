package com.example.erp01.service;

import com.example.erp01.model.FabricDetail;
import com.example.erp01.model.FabricOutRecord;
import com.example.erp01.model.FabricStorage;

import java.util.List;
import java.util.Map;

public interface InvoicingFabricService {

    List<Integer> getFabricOperateIdList(Map<String, Object> map);

    List<FabricDetail> getFabricDetailByIdList(List<Integer> fabricIDList);

    List<FabricStorage> getFabricStorageByIdList(List<Integer> fabricIDList);

    List<FabricOutRecord> getFabricOutRecordByIdList(List<Integer> fabricIDList);

    List<FabricDetail> getFabricDetailByMap(Map<String, Object> map);

    List<FabricStorage> getFabricStorageByMap(Map<String, Object> map);

    List<FabricOutRecord> getFabricOutRecordByMap(Map<String, Object> map);

}
