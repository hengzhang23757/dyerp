package com.example.erp01.service;

import com.example.erp01.model.FabricHandOver;

import java.util.Date;
import java.util.List;

public interface FabricHandOverService {

    Integer addFabricHandOverBatch(List<FabricHandOver> fabricHandOverList);

    List<FabricHandOver> getFabricHandOverByShelf(String shelfName);

    List<FabricHandOver> getFabricHandOverByInfo(Date from, Date to, String orderName, String colorName, String fabricColor);

    Integer deleteFabricHandOverByShelf(String shelfName);

    Integer updateFabricHandOverBatch(List<FabricHandOver> fabricHandOverList);

    List<FabricHandOver> getFabricHandOverByQcode(Integer qCodeID);

    List<String> getFabricHandOverColorHint(String orderName);

    List<String> getFabricHandOverFabricColorHint(String orderName);

}
