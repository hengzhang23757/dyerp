package com.example.erp01.service;

import com.example.erp01.model.AccessoryInStore;
import com.example.erp01.model.FabricDetail;
import com.example.erp01.model.FabricMonthCheck;
import com.example.erp01.model.FabricOutRecord;

import java.util.List;
import java.util.Map;

public interface FabricAccessoryMonthCheckService {

    List<FabricMonthCheck> getFabricDetailCheckByInfo(Map<String, Object> map);

    List<FabricMonthCheck> getFabricOutRecordCheckByInfo(Map<String, Object> map);

    List<String> getAllFabricSupplier();

    List<String> getAllAccessorySupplier();

    Integer updateCheckDetailInCheck(List<FabricDetail> fabricDetailList);

    Integer updateCheckOutRecordInCheck(List<FabricOutRecord> fabricOutRecordList);

    List<AccessoryInStore> getAccessoryInStoreByInfo(Map<String, Object> map);

    Integer updateAccessoryInStoreInCheck(List<AccessoryInStore> accessoryInStoreList);

    Integer quitFabricDetailBatch(List<Integer> fabricDetailIDList);

    Integer quitAccessoryInStoreBatch(List<Integer> idList);

}
