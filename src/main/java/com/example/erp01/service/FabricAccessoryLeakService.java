package com.example.erp01.service;

import com.example.erp01.model.*;

import java.util.List;

public interface FabricAccessoryLeakService {

    List<FabricAccessoryLeak> getOrderInfoByOrderList(List<String> orderNameList);

    List<FabricAccessoryLeak> getCutInfoByOrderList(List<String> orderNameList);

    List<FabricAccessoryLeak> getFabricAccessoryLeakByOrderList(List<String> orderNameList, String type);

    List<FabricAccessoryLeak> getFabricAccessoryReturnByOrderList(List<String> orderNameList, String type);

    List<ManufactureFabric> getManufactureFabricByOrderList(List<String> orderNameList);

    List<FabricDetail> getFabricDetailByOrderList(List<String> orderNameList);

    List<ManufactureAccessory> getManufactureAccessoryByOrderList(List<String> orderNameList);

    List<AccessoryInStore> getAccessoryInStoreByOrderList(List<String> orderNameList);

}
