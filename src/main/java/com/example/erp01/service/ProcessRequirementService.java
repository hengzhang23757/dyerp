package com.example.erp01.service;

import com.example.erp01.model.ProcessRequirement;

import java.util.List;

public interface ProcessRequirementService {

    Integer addProcessRequirementBatch(List<ProcessRequirement> processRequirementList);

    List<ProcessRequirement> getProcessRequirementByOrder(String orderName);

    Integer deleteProcessRequirementByOrder(String orderName);

    Integer deleteProcessRequirementByID(Integer processRequirementID);

    Integer updateProcessRequirement(ProcessRequirement processRequirement);

    Integer addProcessRequirement(ProcessRequirement processRequirement);

}
