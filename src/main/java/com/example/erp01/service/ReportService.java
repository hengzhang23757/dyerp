package com.example.erp01.service;

import com.example.erp01.model.*;

import java.util.Date;
import java.util.List;

public interface ReportService {

    List<ProductionProgress> getProductionProgressByOrderTimeGroup(String orderName, Date from, Date to, String groupName);

    List<MiniProgress> getProductionProgressByOrder(String orderName);

    List<MiniProgress> getProductionProgressByOrderGroup(String orderName, String groupName);

    List<CutDaily> getCutDailyDetailByOrderTime(String orderName, Date from, Date to);

    List<ProductionProgress> getMiniProductionProgressByOrderGroup(String orderName, Date from, Date to, String groupName);

    List<ProductionProgressDetail> getProductionProgressDetailByOrderTimeGroup(String orderName, Date from, Date to, String groupName, Integer procedureNumber);

    List<ProductionProgressDetail> getMiniProductionProgressDetailByOrderTimeGroup(String orderName, Date from, Date to, String groupName, Integer procedureNumber);

    List<PersonProductionDetail> getPersonProductionOfProcedure(String orderName, Date from, Date to, String groupName, Integer procedureNumber);

    List<PersonProductionDetail> getMiniPersonProductionOfProcedure(String orderName, Date from, Date to, String groupName, Integer procedureNumber);

    List<PersonProductionDetail> getEachPersonProduction(String orderName, Date from, Date to, String groupName);

    List<PersonProductionDetail> getMiniEachPersonProductionOfProcedure(String orderName, Date from, Date to, String groupName);

    List<CutQueryLeak> getCutInStoreProgress(String orderName, Date from, Date to);

    List<CutQueryLeak> getEmbInStoreProgress(String orderName, Date from, Date to);

    List<CutQueryLeak> getEmbOutStoreProgress(String orderName, Date from, Date to);

    Integer getCutInStoreCount(String orderName, Date from, Date to);

    Integer getEmbInStoreCount(String orderName, Date from, Date to);

    Integer getEmbOutStoreCount(String orderName, Date from, Date to);

    Date getCreateTimeByOrderBed(String orderName, Integer bedNumber);

    Date getOtherCreateTimeByOrderBed(String orderName, String partName, Integer bedNumber);

    List<TailorMonthReport> getTailorMonthReportOfEachDay(Date from, Date to, String orderName, String partName, Integer tailorType, String groupName);

    List<TailorMonthReport> getOtherTailorMonthReportOfEachDay(Date from, Date to, String orderName, String partName, Integer tailorType, String groupName);

    List<CutBedColor> getCutBedColorByOrder(String orderName, String partName, Integer tailorType, String from, String to);

    Integer fillProcedurePrice(List<OrderProcedure> orderProcedureList, String monthString);

    List<String> getPieceWorkOrderNameListByMonthString(String monthString);

    List<GeneralSalary> getPieceWorkSummarySalaryByInfo(Date from, Date to, String groupName);

    List<GeneralSalary> getPieceWorkGroupSalaryByInfo(Date from, Date to, String groupName);
}
