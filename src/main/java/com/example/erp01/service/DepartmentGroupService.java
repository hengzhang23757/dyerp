package com.example.erp01.service;

import com.example.erp01.model.DepartmentGroup;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface DepartmentGroupService {

    Integer addDepartmentGroup(DepartmentGroup departmentGroup);

    Integer deleteDepartmentGroup(Integer id);

    List<DepartmentGroup> getAllDepartmentGroup();

    DepartmentGroup getDepartmentGroupByDepartment(String departmentName);

    List<String> getAllCheckGroupName();

    List<String> getAllDepartmentName();

    List<String> getAllGroupName();

    List<String> listGroupNameByDepartment(String departmentName);
}
