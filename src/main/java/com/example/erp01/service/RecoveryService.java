package com.example.erp01.service;

import com.example.erp01.model.Tailor;

import java.util.List;

public interface RecoveryService {

    List<Integer> getDeletedBedNumber(String orderName);

    List<Tailor> getDeletedTailorByInfo(String orderName, Integer bedNumber);

    List<Tailor> getNormalTailorByInfo(String orderName, Integer bedNumber);

    Integer recoverTailorByInfo(String orderName, List<Integer> tailorIDList);

    List<Tailor> getSelectedTailorByID(String orderName, List<Integer> tailorIDList);

}
