package com.example.erp01.service;

import com.example.erp01.model.RankNotice;

import java.util.List;

public interface RankNoticeService {

    Integer addRankNotice(RankNotice rankNotice);

    Integer deleteRankNotice(Integer id);

    List<RankNotice> getRankNoticeByRole(String role);

}
