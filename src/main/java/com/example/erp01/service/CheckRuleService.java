package com.example.erp01.service;

import com.example.erp01.model.CheckRule;

import java.util.List;

public interface CheckRuleService {

    List<CheckRule> getAllCheckRule();

    Integer updateCheckRule(CheckRule checkRule);

}
