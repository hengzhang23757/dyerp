package com.example.erp01.service;

import com.example.erp01.model.OPA;

import java.util.List;

public interface OPAService {

    int addOPA(OPA opa);

    int deleteOPA(Integer opaID);

    List<OPA> getAllOPA();

    List<OPA> getTodayOPA();

    List<OPA> getOneMonthOPA();

    List<OPA> getThreeMonthOPA();

    List<OPA> getOPAByOrder(String orderName);

    List<OPA> getBedOPAByOrder(String orderName);

}
