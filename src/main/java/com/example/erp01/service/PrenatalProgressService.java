package com.example.erp01.service;

import com.example.erp01.model.OrderClothes;
import com.example.erp01.model.PrenatalProgress;

import java.util.List;

public interface PrenatalProgressService {

    List<OrderClothes> getWorkOrderByTime(String from, String to);

    List<PrenatalProgress> getPreCutCountByOrder(String orderName);
}
