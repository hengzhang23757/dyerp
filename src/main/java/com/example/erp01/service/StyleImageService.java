package com.example.erp01.service;

import com.example.erp01.model.StyleImage;
import org.apache.ibatis.annotations.Param;

public interface StyleImageService {

    Integer addStyleImage(StyleImage styleImage);

    StyleImage getStyleImageByOrder(String orderName);

    void changeOrderName(String orderName, String toOrderName);

}
