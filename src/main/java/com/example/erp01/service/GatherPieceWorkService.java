package com.example.erp01.service;

import com.example.erp01.model.GeneralSalary;

import java.util.List;

public interface GatherPieceWorkService {

    List<GeneralSalary> getGatherPieceWorkAmongSummary(String orderName, String groupName, String employeeNumber, String from, String to, Integer procedureNumber);

    List<GeneralSalary> getGatherPieceWorkEachDaySummary(String orderName, String groupName, String employeeNumber, String from, String to, Integer procedureNumber);

    List<GeneralSalary> getGatherSalaryEachDaySummary(String orderName, String groupName, String employeeNumber, String from, String to, Integer procedureNumber);

}
