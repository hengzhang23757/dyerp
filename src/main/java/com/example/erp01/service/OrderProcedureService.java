package com.example.erp01.service;

import com.example.erp01.model.OrderProcedure;
import com.example.erp01.model.ProcedureDetail;
import com.example.erp01.model.ProcedureInfo;
import com.example.erp01.model.ProcedureTemplate;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface OrderProcedureService {

    int addOrderProcedure(OrderProcedure orderProcedure);

    int addOrderProcedureBatch(List<OrderProcedure> orderProcedureList);

    int deleteOrderProcedure(Integer orderProcedureID);

    int deleteOrderProcedureByName(String orderName);

    List<OrderProcedure> getUniqueOrderProcedure(Map<String, Object> map);

    List<ProcedureInfo> getProcedureInfoByOrderName(String orderName);

    List<Integer> getProcedureNumbersByOrderName(String orderName);

    ProcedureInfo getProcedureCodeNameByNumber(@Param("orderName")String orderName,@Param("procedureNumber") Integer procedureNumber);

    List<ProcedureDetail> getProcedureDetailByOrder(String orderName);

    List<OrderProcedure> getOrderProcedureByOrder(String orderName);

    ProcedureInfo getProcedureInfoByOrderProcedureNumber(@Param("orderName")String orderName,@Param("procedureNumber") Integer procedureNumber);

    Integer changeProcedureState(String orderName, Integer procedureState);

    Float getSectionSamByOrder(String orderName, String procedureSection);

    Integer updateOrderProcedureByProcedureCode(ProcedureTemplate procedureTemplate);

    List<ProcedureInfo> getSpecialProcedureInfoByOrder(String orderName);

    Integer changeProcedureStateBatch(String orderName, List<Integer> procedureNumberList, Integer procedureState);

    List<OrderProcedure> getOrderProcedureToReview(String orderName, List<Integer> procedureNumberList);

    List<OrderProcedure> getOrderProcedureByState(String orderName, List<Integer> procedureStateList);

    List<Integer> getDistinctProcedureStateByOrder(String orderName);

    List<OrderProcedure> getOrderProcedureByOrderProcedureSection(String orderName, String procedureSection);

    List<OrderProcedure> getOrderProcedureByOrderNames(List<String> orderNameList, Integer procedureState);

    Integer changeScanPartOfProcedure(String orderName, Integer procedureNumber,String scanPart);

    Double getSectionPriceByOrder(String orderName, String procedureSection);

    Integer getPendingOrderProcedureCount(Integer procedureState);

    OrderProcedure getOrderProcedureByOrderNameProcedureNumber(String orderName, Integer procedureNumber);

    List<OrderProcedure> getProduresByOrderName(String orderName);

    List<OrderProcedure> getOrderProcedureByMap(Map<String, Object> params);
}
