package com.example.erp01.service;

import com.example.erp01.model.CutLocation;
import com.example.erp01.model.CutQueryLeak;
import com.example.erp01.model.EmbStorage;

import java.util.List;

public interface EmbInStoreService {

    int addEmbInStore(List<EmbStorage> embStorageList);

    int deleteEmbInStore(List<String> subQcodeList);

    List<CutLocation> getEmbInfo(String orderName, Integer bedNumber,String colorName, String sizeName, Integer packageNumber);

    Integer getTotalEmbInStoreCountByOrder(String orderName);

    List<EmbStorage> getEmbInStoreGroupByColor(String orderName);

    List<CutQueryLeak> getEmbInStoreInfoGroupByColorSize(String orderName, String colorName, String sizeName);
}
