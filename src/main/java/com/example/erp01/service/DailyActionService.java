package com.example.erp01.service;

import com.example.erp01.model.DailyAction;

import java.util.Date;
import java.util.List;

public interface DailyActionService {

    Integer addDailyAction(DailyAction dailyAction);

    DailyAction getDailyActionByInfo(String orderName, String groupName, Date workDate);

    Integer updateDailyAction(DailyAction dailyAction);

    Integer getSumLooseFabricCountByTask(Integer taskID);

}
