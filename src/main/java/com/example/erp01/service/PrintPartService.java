package com.example.erp01.service;

import com.example.erp01.model.PrintPart;

import java.util.List;

public interface PrintPartService {

    int addPrintPart(PrintPart printPart);

    int addPrintPartBatch(List<PrintPart> printPartList);

    int deletePrintPart(Integer printPartID);

    List<String> getPrintPartByOrder(String orderName);

    List<String> getMainPrintPartByOrder(String orderName);

    List<String> getOtherPrintPartByOrder(String orderName);

    List<PrintPart> getAllPrintPart();

    List<PrintPart> getAllPrintPartByOrder(String orderName);

}
