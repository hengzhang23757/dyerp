package com.example.erp01.service;

import com.example.erp01.model.GroupProgress;

import java.util.Date;
import java.util.List;

public interface GroupProgressService {

    List<GroupProgress> getGroupEmbOutByInfo(Date from, Date to, String orderName, List<String> groupList);

    List<GroupProgress> getAllGroupEmbOutByInfo(Date from, Date to, String orderName);

    List<GroupProgress> getGroupHangCountByInfo(Date from, Date to, String orderName, List<String> groupList);

    List<GroupProgress> getAllGroupHangCountByInfo(Date from, Date to, String orderName);

    List<GroupProgress> getGroupInspectionCountByInfo(Date from, Date to, String orderName, List<String> groupList);

    List<GroupProgress> getAllGroupInspectionCountByInfo(Date from, Date to, String orderName);

    List<GroupProgress> getEachDayGroupEmbOutByInfo(Date from, Date to, String orderName, List<String> groupList);

    List<GroupProgress> getEachDayGroupHangCountByInfo(Date from, Date to, String orderName, List<String> groupList);

    List<GroupProgress> getEachDayGroupInspectionCountByInfo(Date from, Date to, String orderName, List<String> groupList);

    List<GroupProgress> getGroupCheckCountByInfo(Date from, Date to, String orderName, List<String> groupList);

    List<GroupProgress> getAllGroupCheckCountByInfo(Date from, Date to, String orderName);

    List<GroupProgress> getEachDayGroupCheckCountByInfo(Date from, Date to, String orderName, List<String> groupList);




    List<GroupProgress> getGroupProgressByTimeProcedure(String from, String to, String orderName, List<String> groupList, List<Integer> procedureNumberList);

    List<GroupProgress> getGroupProgressByTimeProcedureGroupByDay(String from, String to, String orderName, List<String> groupList, List<Integer> procedureNumberList);
}
