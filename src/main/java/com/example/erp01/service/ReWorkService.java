package com.example.erp01.service;

import com.example.erp01.model.ReWork;

import java.util.Date;
import java.util.List;

public interface ReWorkService {

    List<ReWork> getReWorkByOrder(String orderName);

    Integer getWorkCountByProcedure(String orderName, Integer procedureNumber, Date reWorkDate);

    List<ReWork> getReWorkSummaryByOrder(String orderName);
}
