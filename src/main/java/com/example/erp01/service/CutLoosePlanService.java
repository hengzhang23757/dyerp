package com.example.erp01.service;

import com.example.erp01.model.CutLoosePlan;

import java.util.List;

public interface CutLoosePlanService {

    Integer addOrUpdateCutLoosePlan(CutLoosePlan cutLoosePlan);

    Integer updateCutLoosePlanBatch(List<CutLoosePlan> cutLoosePlanList);

    Integer deleteCutLoosePlan(Integer id);

    List<CutLoosePlan> getCutLoosePlanByFinishState(String finishState);

    Integer updateFinishState(Integer id);
}
