package com.example.erp01.service;

import com.example.erp01.model.ClothesPrice;

import java.util.List;

public interface ClothesPriceService {

    Integer addClothesPriceBatch(List<ClothesPrice> clothesPriceList);

    Integer deleteClothesPriceBatch(List<Integer> idList);

    Integer updateClothesPrice(ClothesPrice clothesPrice);

    List<ClothesPrice> getClothesPriceByInfo(String orderName, String colorName);

    Integer updateClothesPriceBatch(List<ClothesPrice> clothesPriceList);

}
