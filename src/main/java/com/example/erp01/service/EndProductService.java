package com.example.erp01.service;

import com.example.erp01.model.EndProduct;
import com.example.erp01.model.EndProductPay;

import java.util.List;

public interface EndProductService {

    Integer addEndProductBatch(List<EndProduct> endProductList);

    Integer updateEndProduct(EndProduct endProduct);

    Integer updateEndProductBatch(List<EndProduct> endProductList);

    EndProduct getInStoreEndProductByOrderColorSize(String orderName, String colorName, String sizeName, String endType);

    Integer getOutStoreProductCountByOrderColorSize(String orderName, String colorName, String sizeName, String endType);

    List<EndProduct> getEndProductSummary();

    Integer deleteEndProductBatch(List<Integer> idList);

    Integer deleteEndProduct(Integer id);

    List<EndProduct> getEndProductInStoreInfo(String orderName, String endType);

    List<EndProduct> getEndProductStorageInfo(String orderName, String endType);

    List<EndProduct> getEndProductOutStoreInfo(String orderName, String endType);

    List<EndProduct> getEndProductStorageSummary(String endType, String orderName);

    List<EndProduct> getEndProductByIdList(List<Integer> idList);

    List<String> getEndProductStorageColorList(String orderName, String endType);

    List<String> getEndProductStorageSizeList(String orderName, String endType);

    Integer getOutStoreCountByOrderName(String orderName, String endType);

    EndProduct getStorageByOrderColorSize(String orderName, String colorName, String sizeName, String endType);

    List<EndProduct> getDefectiveSummary();

    List<EndProduct> getDefectiveEndProductSummary(String orderName);

    List<EndProduct> getDefectiveEndProductDetailInByOrder(String orderName);

    List<EndProduct> getDefectiveEndProductDetailStorageByOrder(String orderName);

    List<EndProduct> getDefectiveEndProductDetailOutByOrder(String orderName);

    EndProduct getEndProductById(Integer id);

    Integer endProductPreCheck(List<Integer> idList);

    List<EndProduct> getPreAccountEndProduct();

    Integer endProductTakeIntoAccount(List<Integer> idList, String accountDate, String payNumber);

    List<EndProduct> getAllEndProduct();

    List<EndProduct> getEndProductOutStoreByInfo(String monthStr, String customerName, String orderName, String endType);

    List<EndProduct> getEndProductAccountByOrderNameList(List<String> orderNameList);

    List<EndProduct> getEndProductSummaryByCustomer(String from, String to);

    Integer quitAccountBatch(List<Integer> idList);

    List<EndProductPay> endProductToEndProductPay(String payNumber);

    Integer quitAccountByPayNumber(String payNumber);

    Integer updatePriceByOrderName(String orderName, String colorName, Float price);

    List<String> getPayNumberByOrderName(String orderName);

    Float getSumMoneyByPayNumber(String payNumber);

}
