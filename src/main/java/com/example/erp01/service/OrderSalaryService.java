package com.example.erp01.service;

import com.example.erp01.model.GeneralSalary;
import com.example.erp01.model.OrderSalary;

import java.util.List;

public interface OrderSalaryService {

    List<OrderSalary> getOrderSalarySummaryByOrderProcedure(String orderName, Integer procedureNumber);

    List<OrderSalary> getOrderSalarySummaryByOrderProcedureMonth(String orderName, Integer procedureNumber);

    List<String> getPieceWorkOrderNameListByMonth(String from, String to);

    List<String> getCutOrderNameListByMonth(String from, String to);

    List<OrderSalary> getOrderCountByOrderNameList(List<String> orderNameList);

    List<OrderSalary> getMonthCutCountByMonthOrderNameList(String from, String to, List<String> orderNameList);

    List<OrderSalary> getMonthPieceSalary(String from, String to, List<String> orderNameList);

    List<OrderSalary> getMonthHourSalary(String from, String to);

    List<OrderSalary> getOrderCutInfoByMonth(String orderName);

    List<GeneralSalary> getOrderProcedureCountSummary(String orderName, Integer procedureNumber);


}
