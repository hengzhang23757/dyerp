package com.example.erp01.service;

import com.example.erp01.model.AddFeeVo;
import com.example.erp01.model.FabricAccessoryReview;
import com.example.erp01.model.ManufactureAccessory;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ManufactureAccessoryService {

    Integer addManufactureAccessoryBatch(List<ManufactureAccessory> manufactureAccessoryList);

    List<ManufactureAccessory> getManufactureAccessoryByOrder(String orderName, Integer batchOrder);

    Integer deleteManufactureAccessoryByName(String orderName);

    Integer deleteManufactureAccessoryByID(Integer accessoryID);

    Integer deleteManufactureAccessoryBatch(List<Integer> accessoryIDList);

    Integer updateManufactureAccessory(ManufactureAccessory manufactureAccessory);

    List<ManufactureAccessory> getManufactureAccessoryHintByName(String subAccessoryName,Integer page,Integer limit);

    List<ManufactureAccessory> getManufactureAccessoryHintByNumber(String subAccessoryName, Integer page, Integer limit);

    Integer updateManufactureAccessoryOnPlaceOrder(List<ManufactureAccessory> manufactureAccessoryList);

    List<ManufactureAccessory> getManufactureAccessoryOneYear();

    List<String> getDistinctAccessorySupplier();

    ManufactureAccessory getManufactureAccessoryByID(Integer accessoryID);

    List<String> getOrderNamesByAccessoryInfo(String accessoryName, String specification, String accessoryColor);

    ManufactureAccessory getManufactureAccessoryByAccessoryInfo(String orderName, String accessoryName, String specification, String accessoryColor);

    List<ManufactureAccessory> getManufactureAccessoryInPlaceOrderByInfo(List<String> orderNameList, String supplier, String materialName, String checkState);

    List<ManufactureAccessory> getManufactureAccessoryInPlaceOrder(String orderName, String supplier, String checkState);

    Integer changeAccessoryOrderStateInPlaceOrder(List<Integer> accessoryIDList);

    Integer updateManufactureAccessoryBatch(List<ManufactureAccessory> manufactureAccessoryList);

    Integer changeAccessoryCheckStateInPlaceOrder(List<Integer> accessoryIDList, String preCheck, String checkState, String preCheckEmp, String checkEmp);

    List<ManufactureAccessory> getManufactureAccessoryByOrderCheckState(String orderName, String checkState);

    List<ManufactureAccessory> getManufactureAccessoryByOrderCheckStateOrderList(List<String> orderNameList, String checkState, String preCheck);

    List<ManufactureAccessory> getBjByOrderCheckStateOrderList(List<String> orderNameList, String checkState);

    List<String> getManufactureAccessoryNameHint(String subName);

    List<ManufactureAccessory> getAccessoryCheckStateRecentYearByOrder(List<String> orderNameList);

    List<FabricAccessoryReview> getManufactureAccessoryReviewState(String checkState);

    List<ManufactureAccessory> getManufactureAccessoryByCheckNumberState(String checkNumber, String checkState);

    Integer destroyAccessoryCheck(String checkNumber);

    Integer getAccessoryCheckNumberCountByState(String checkState);

    Integer updateAccessoryAfterCommit(List<ManufactureAccessory> manufactureAccessoryList);

    List<ManufactureAccessory> getManufactureAccessoryByColorSizeList(String orderName, List<String> colorList, List<String> sizeList);

    Integer updateAccessoryOrderStateByCheckNumber(String checkNumber);

    List<String> getDistinctAccessoryNameByOrder(String orderName);

    List<ManufactureAccessory> getManufactureAccessoryByIdList(List<Integer> accessoryIDList);

    List<String> getAccessoryCheckNumberHint(String keyWord);

    List<ManufactureAccessory> getAccessoryNameByOrderName(String orderName, Integer batchOrder, String accessoryType);

    List<ManufactureAccessory> getAccessoryNameChildByOrderName(String orderName, Integer batchOrder, String accessoryType);

    Integer getMaxAccessoryKey(String orderName, Integer batchOrder);

    List<ManufactureAccessory> getAccessoryKeyByOrder(String orderName, Integer batchOrder, String accessoryType);

    void updateAccessoryCustomerUsage(List<ManufactureAccessory> manufactureAccessoryList);




    List<FabricAccessoryReview> getManufactureAccessoryReviewStateCombine(Map<String, Object> map);

    List<FabricAccessoryReview> getBjReviewStateCombine(Map<String, Object> map);

    void deleteManufactureAccessoryByAccessoryType(String orderName, String accessoryType, Integer batchOrder, String checkState);

    List<ManufactureAccessory> getBjByOrder(String orderName, String accessoryType, Integer batchOrder);

    void changeFeeStateBatch(List<Integer> accessoryIDList, Integer feeState);

    List<AddFeeVo> getFeeVoByInfo(Map<String, Object> map);

    void changeOrderName(String orderName, String toOrderName);
}
