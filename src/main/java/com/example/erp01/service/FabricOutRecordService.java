package com.example.erp01.service;

import com.example.erp01.model.FabricOutRecord;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface FabricOutRecordService {

    Integer addFabricOutRecordBatch(List<FabricOutRecord> fabricOutRecordList);

    Integer addFabricOutRecord(FabricOutRecord fabricOutRecord);

    Integer deleteFabricOutRecord(Integer fabricOutRecordID);

    List<FabricOutRecord> getAllFabricOutRecord();

    List<FabricOutRecord> getFabricOutRecordByOrder(String orderName);

    List<FabricOutRecord> getTodayFabricOutRecord();

    List<FabricOutRecord> getOneMonthFabricOutRecord();

    List<FabricOutRecord> getThreeMonthsFabricOutRecord();

    List<FabricOutRecord> getFabricOutRecordByInfo(String orderName, String fabricName, String colorName, String fabricColor, Integer fabricID);

    List<FabricOutRecord> getFabricOutRecordByOrderType(String orderName, String operateType, String colorName);

    List<FabricOutRecord> getFabricOutRecordByTime(String from, String to);

    FabricOutRecord getFabricOutRecordById(Integer fabricOutRecordID);

    Float getReturnFabricWeightByFabricID(Integer fabricID);

    List<FabricOutRecord> getFabricOutRecordByMap(Map<String, Object> map);

    Integer deleteFabricOutRecordByOrderJar(String orderName, String jarName);

}
