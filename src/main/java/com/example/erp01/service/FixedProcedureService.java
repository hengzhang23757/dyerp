package com.example.erp01.service;

import com.example.erp01.model.FixedProcedure;

import java.util.List;

public interface FixedProcedureService {

    Integer addFixedProcedure(FixedProcedure fixedProcedure);

    Integer deleteFixedProcedure(Integer id);

    List<FixedProcedure> getAllFixedProcedure();

}
