package com.example.erp01.service;

import com.example.erp01.model.PieceWork;
import com.example.erp01.model.PieceWorkBack;
import com.example.erp01.model.PieceWorkDelete;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface KeyDataDeleteService {

    PieceWorkDelete getPieceWorkByDeleteId(Integer pieceWorkID);

    List<PieceWorkDelete> getPieceWorkListByDeleteId(List<Integer> pieceWorkIDList);

    Integer addPieceWorkDeleteBatch(List<PieceWorkDelete> pieceWorkDeleteList);

    String getEarlyCreateTimeOfDeleteData(List<Integer> pieceWorkIDList);

    List<PieceWork> getPieceWordByStep(Date pieceWorkDate);

    Integer addPieceWordBackBatch(List<PieceWorkBack> pieceWorkBackList);

    Integer deletePieceWorkBacked(Date pieceWorkDate);

    List<PieceWorkDelete> getPieceWorkDeleteByMap(Map<String, Object> map);

    Integer deleteKeyData(List<Integer> idList);

}
