package com.example.erp01.service;

public interface MaxFinishTailorQcodeIDService {

   public int getMaxTailorQcodeId();

   public int updateMaxTailorQcodeId(int qCodeId);

}
