package com.example.erp01.service;

import com.example.erp01.model.EmbPlan;

import java.util.Date;
import java.util.List;

public interface EmbPlanService {

    Integer addEmbPlanBatch(List<EmbPlan> embPlanList);

    Integer addEmbPlan(EmbPlan embPlan);

    Integer deleteEmbPlan(Integer embPlanID);

    List<EmbPlan> getTodayEmbPlan();

    List<EmbPlan> getOneMonthEmbPlan();

    List<EmbPlan> getThreeMonthEmbPlan();

    Integer updateEmbPlan(EmbPlan embPlan);

    EmbPlan getEmbPlanByOrderGroup(String orderName, String groupName, Date outTime);

    EmbPlan getEmbPlanByOrderGroupDate(String orderName, String groupName, Date outTime);

    Integer updateEmbPlanOnOut(Integer embPlanID, Integer actCount, Float achievePercent);

    Integer getEmbStorageCountByInfo(String orderName, String colorName, String sizeName);

    EmbPlan getEmbPlanByOrderColorSizeGroupDate(String orderName, String colorName, String sizeName, String groupName, Date outTime);

    List<EmbPlan> getUnFinishEmbPlan();

    Integer updateEmbPlanCountByID(Integer embPlanID, Integer planCount);

    List<EmbPlan> getEmbPlansByOrderGroupDate(String orderName, String groupName, Date outTime);
}
