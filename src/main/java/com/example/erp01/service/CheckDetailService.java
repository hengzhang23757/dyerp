package com.example.erp01.service;

import com.example.erp01.model.CheckDetail;

import java.util.Date;
import java.util.List;

public interface CheckDetailService {

    List<CheckDetail> getCheckDetailByInfo(Date fromDate, Date toDate, String employeeNumber);

    List<CheckDetail> getYesterdayCheckDetail();

    Integer updateCheckDetail(CheckDetail checkDetail);
}
