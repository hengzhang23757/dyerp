package com.example.erp01.service;

import com.example.erp01.model.CutInStore;
import com.example.erp01.model.CutLocation;
import com.example.erp01.model.Storage;

import java.util.List;

public interface CutInStoreService {

    int cutInStore(List<Storage> cutInStoreList);

    int deleteCutInStore(List<String> tailorQcodeList);

//    List<String> getCutLocationByColorSize(String orderName, String colorName, String sizeName);

    List<String> getCutLocationByPackage(String orderName, Integer bedNumber, Integer packageNumber);

    List<CutLocation> getCutInfo(String orderName,Integer bedNumber, String colorName, String sizeName, Integer packageNumber);

    Integer deleteCutInStoreByInfo(String orderName, Integer bedNumber, Integer packageNumber);

}
