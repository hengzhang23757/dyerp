package com.example.erp01.service;

import com.example.erp01.model.ClothesDevFabricNumber;

import java.util.List;

public interface ClothesDevFabricNumberService {

    Integer addClothesDevFabricNumber(ClothesDevFabricNumber clothesDevFabricNumber);

    Integer deleteClothesDevFabricNumber(Integer id);

    List<ClothesDevFabricNumber> getAllClothesDevFabricNumber();

    Integer updateClothesDevFabricNumber(ClothesDevFabricNumber clothesDevFabricNumber);


}
