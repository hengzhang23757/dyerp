package com.example.erp01.service;

import com.example.erp01.model.*;

import java.util.List;

public interface FabricAdjustService {

    List<ManufactureFabric> getAllManufactureFabric();

    List<FabricDetail> getAllFabricDetail();

    List<FabricStorage> getAllFabricStorage();

    List<FabricOutRecord> getAllFabricOutRecord();

    List<ManufactureAccessory> getAllManufactureAccessory();

    List<AccessoryInStore> getAllAccessoryInStore();

    List<AccessoryStorage> getAllAccessoryStorage();

    List<AccessoryOutRecord> getAllAccessoryOutRecord();

    Integer updateFabricDetailIdBatch(List<FabricDetail> fabricDetailList);

    Integer updateFabricStorageIdBatch(List<FabricStorage> fabricStorageList);

    Integer updateFabricOutRecordIdBatch(List<FabricOutRecord> fabricOutRecordList);

    Integer updateAccessoryInStoreIdBatch(List<AccessoryInStore> accessoryInStoreList);

    Integer updateAccessoryStorageIdBatch(List<AccessoryStorage> accessoryStorageList);

    Integer updateAccessoryOutRecordIdBatch(List<AccessoryOutRecord> accessoryOutRecordList);

    Integer updateAccessoryAccountInStoreBatch(List<AccessoryInStore> accessoryInStoreList);

    Integer updateAccessoryInStorePriceBatch(List<AccessoryInStore> accessoryInStoreList);
}
