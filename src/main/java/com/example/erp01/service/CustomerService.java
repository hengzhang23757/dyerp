package com.example.erp01.service;


import com.example.erp01.model.Customer;
import org.springframework.stereotype.Service;

import java.util.List;

//@Service
public interface CustomerService {

    Integer addCustomer(Customer customer);

    Integer updateCustomer(Customer customer);

    List<Customer> getAllCustomer();

    Integer deleteCustomer(Integer id);

    String getMaxCustomerCodeByPreFix(String preFix);

    List<Customer> getCustomerHintPage(String keyWord, int page, int limit);

    Integer getCustomerCount(String keyWord);

}
