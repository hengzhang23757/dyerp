package com.example.erp01.service;

import com.example.erp01.model.Employee;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface EmployeeService {

    int addEmployee(Employee employee);

    Employee employeeLogin(String employeeNumber, String passWord);

    int deleteEmployee(Integer employeeID);

    Employee getEmpByID(Integer employeeID);

    int updateEmployee(Employee employee);

    List<String> getEmpHint(String subEmpNumber);

    List<Employee> getAllEmployee();

    String getEmpNameByEmpNum(String employeeNumber);

    String getGroupNameByEmpNum(String employeeNumber);

    List<Employee> getEmployeeByGroup(String groupName);

    int updatePassWord(String employeeNumber, String passWord);

    List<Employee> getEmpByEmpNumber(String employeeNumber);

    List<String> getEmpNameHint(String subEmpName);

    List<String> getEmpNumbersByName(String employeeName);

    List<Employee> getEmpByIdentifyCard(String identifyCard);

    String getPositionByEmployeeNumber(String employeeNumber);

    List<Employee> getEmployeeByGroups(List<String> groupList);

    List<Employee> getAllWorkEmployee();

    List<Employee> getEmployeeByPositions(List<String> positionList);

    Integer getEmployeeCountByGroup(@Param("groupName")String groupName);

    List<String> getAllGroupName();

    List<Employee> getAllInStateEmployee();

    List<Employee> getAllOffStateEmployee();

    List<Employee> getCheckEmployee();

    List<Employee> getEmployeeInfoHit(String keyWord, Integer page, Integer limit);

    Integer getEmployeeCountOfHint(String keyWord);


    List<Employee> getProduceEmployeeByGroup(String groupName);

    List<Employee> getEmpByEmpNumberList(List<String> empList);
}
