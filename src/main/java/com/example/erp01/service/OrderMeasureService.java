package com.example.erp01.service;

import com.example.erp01.model.OrderMeasure;

import java.util.List;

public interface OrderMeasureService {

    Integer addOrderMeasureBatch(List<OrderMeasure> orderMeasureList);

    List<OrderMeasure> getOrderMeasureByOrder(String orderName);

    Integer deleteOrderMeasureByOrder(String orderName);

    Integer updateOrderMeasure(OrderMeasure orderMeasure);

    Integer deleteOrderMeasureByID(Integer orderMeasureID);

    Integer addOrderMeasure(OrderMeasure orderMeasure);

    OrderMeasure getOneOrderMeasureByOrder(String orderName);

}
