package com.example.erp01.service;

import java.util.List;

public interface GroupService {

    List<String> getAllGroupNames();

    List<String> getBigGroupNames();

    List<String> getSewGroupNames();

}
