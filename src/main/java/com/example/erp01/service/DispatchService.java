package com.example.erp01.service;

import com.example.erp01.model.Dispatch;
import com.example.erp01.model.ProcedureInfo;

import java.util.List;

public interface DispatchService {

    int addDispatchBatch(List<Dispatch> dispatchList);

    int addDispatch(Dispatch dispatch);

    int deleteDispatch(Integer dispatchID);

    List<Dispatch> getAllDispatch();

    List<Dispatch> getDispatchByOrder(String orderName);

    List<Dispatch> getDispatchByGroup(String groupName);

    List<Dispatch> getDispatchByEmp(String employeeNumber);

    List<Dispatch> getDispatchByOrderGroup(String orderName,String groupName);

    List<ProcedureInfo> getProcedureInfoByOrderEmp(String orderName, String employeeNumber);

    List<String> getDistinctEmployeeNameByOrder(String orderName, String groupName);

    List<Dispatch> getDispatchByOrderGroupEmp(String orderName, String groupName, String employeeName);

    List<Dispatch> getTodayDispatch();

    List<Dispatch> getOneMonthDispatch();

    List<Dispatch> getThreeMonthsDispatch();

    Integer deleteDispatchByOrderEmployeeList(String orderName, List<String> employeeNumberList);

}
