//package com.example.erp01.service;
//
//import com.example.erp01.model.FabricPlan;
//
//import java.util.Date;
//import java.util.List;
//
//public interface FabricPlanService {
//
//    int addFabricPlanBatch(List<FabricPlan> fabricPlanList);
//
//    int deleteFabricPlan(Integer fabricPlanID);
//
//    List<FabricPlan> getAllFabricPlan();
//
//    int updateFabricPlan(FabricPlan fabricPlan);
//
//    List<FabricPlan> getUniqueFabricPlan();
//
//    List<FabricPlan> getFabricPlanDetailByOrder(String orderName);
//
//    List<FabricPlan> getFabricPlanDetailByOrderSeasonDate(String orderName, Integer orderCount, String season, Date orderDate);
//
//    int deleteFabricPlanByOrder(String orderName);
//
//    int deleteFabricPlanByOrderSeasonDate(String orderName, Integer orderCount, String season, Date orderDate);
//
//    List<String> getFabricNamesByOrder(String orderName);
//
//    List<String> getFabricColorsByOrderFabric(String orderName, String fabricName);
//
//    String getColorNameByOrderFabric(String orderName, String fabricName, String fabricColor);
//
//    Integer getFabricOrderCountByOrderFabricColor(String orderName, String fabricName, String fabricColor);
//
//    String getSupplierByOrderFabricColor(String orderName, String fabricName, String fabricColor);
//
//    List<String> getColorNamesByOrderFabric(String orderName, String fabricName, String fabricColor);
//
//}
