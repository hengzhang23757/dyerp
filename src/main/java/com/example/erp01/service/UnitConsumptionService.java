package com.example.erp01.service;

import com.example.erp01.model.UnitConsumption;

import java.util.List;

public interface UnitConsumptionService {

    Float getPlanConsumptionByJar(String orderName, String colorName, String jarName);

    List<UnitConsumption> getConsumptionInfoByOrder(String orderName, String colorName);

    Float getReturnFabricCountByOrderColorJar(String orderName, String colorName, String jarName);


    List<UnitConsumption> getOtherConsumptionSummaryOfEachBed(String orderName, String colorName, String partName);

    List<UnitConsumption> getOtherBasicInfoOfEachBed(String orderName, String colorName, String partName);

    Float getOtherPlanConsumptionByJar(String orderName, String colorName, String jarName);

    Integer getOtherSumLayerCountOfByOrderColorName(String orderName, String colorName, String partName);

    List<UnitConsumption> getOtherConsumptionSummaryOfOrder(String orderName, String colorName, String partName);

    Float getOtherActConsumptionByColor(String orderName, String colorName, String partName);

    Float getOtherReturnFabricCountByOrderColorJar(String orderName, String colorName, String jarName);


    List<UnitConsumption> getConsumptionSummaryOfOrder(String orderName, String colorName, String partName, Integer tailorType);

    Float getActConsumptionByColor(String orderName, String colorName, String partName, Integer tailorType);

    Integer getSumLayerCountOfByOrderColorName(String orderName, String colorName, String partName, Integer tailorType);

    List<UnitConsumption> getBasicInfoOfEachBed(String orderName, String colorName, String partName, Integer tailorType);

    List<UnitConsumption> getConsumptionSummaryOfEachBed(String orderName, String colorName, String partName, Integer tailorType);

    String getMaxJarNumberByOrderColorPartType(String orderName, String colorName, String partName, Integer tailorType);
}
