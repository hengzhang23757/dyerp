package com.example.erp01.service;

import com.example.erp01.model.ManufactureOrder;
import com.example.erp01.model.OrderClothes;

import java.util.List;

public interface SyncOrderService {

    ManufactureOrder getManufactureOrderByOrderName(String orderName);

    Integer addManufactureOrderOne(ManufactureOrder manufactureOrder);

    List<OrderClothes> getOrderClothesByOrderName(String orderName);

    Integer addOrderClothesBatch(List<OrderClothes> orderClothesList);

}
