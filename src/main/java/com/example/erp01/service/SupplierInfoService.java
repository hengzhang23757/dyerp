package com.example.erp01.service;

import com.example.erp01.model.SupplierInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SupplierInfoService {

    Integer addSupplierInfo(SupplierInfo supplierInfo);

    List<SupplierInfo> getSupplierInfoHint(String subSupplierName,Integer page,Integer limit);

    List<SupplierInfo> getAllSupplierInfo();

    Integer updateSupplierInfo(SupplierInfo supplierInfo);

    Integer deleteSupplierInfo(Integer id);

    List<String> getAllSupplierNameByInfo(String supplierType);
}
