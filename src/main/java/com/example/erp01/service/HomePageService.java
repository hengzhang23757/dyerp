package com.example.erp01.service;

import com.example.erp01.model.HomePage;

import java.util.List;

public interface HomePageService {

    List<HomePage> getUnFinishOrder();

    Integer addHomePageDataBatch(List<HomePage> homePageList);

    Integer updateHomePageData(List<HomePage> homePageList);

    List<HomePage> getHomePageRecentThreeMonth();

    Integer deleteHomePageDataByOrderName(String orderName);

}
