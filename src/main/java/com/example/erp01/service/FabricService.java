package com.example.erp01.service;

import com.example.erp01.model.Fabric;
import com.example.erp01.model.FabricDifference;
import com.example.erp01.model.FabricLeak;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface FabricService {

    int addFabricBatch(List<Fabric> fabricList);

    int deleteFabric(Integer fabricID);

    List<Fabric> getAllFabric();

    int updateFabric(Fabric fabric);

    List<String> getStyleNumberHint(String subStyleNumber);

    List<String> getFabricNumberHint(String styleNumber);

    String getVersionNumberHint(String styleNumber);

    List<String> getFabricColorNumberHint(String styleNumber,String fabricNumber);

    List<String> getJarNumberHint(String styleNumber, String fabricNumber, String fabricColor);

    Float getDeliveryQuantityHint(String styleNumber,String fabricNumber,String fabricColor, String jarNumber);

    Integer getBatchNumberHint(String styleNumber,String fabricNumber,String fabricColor,String jarNumber);

    List<String> getSeasonHint(String subSeason);

    List<String> getVersionNumberBySeason(String season);

    List<FabricLeak> getFabricLeakQuery(String season, String versionNumber);

    String getColorNameByNumber(String styleNumber,String fabricColorNumber);

    List<String> getFabricColorHint(String styleNumber, String fabricNumber);

    String getColorNumberByName(String styleNumber, String fabricColor);

    List<FabricDifference> getFabricDifference(String styleNumber);

    List<String> getVersionNumberBySubVersion(String subVersion);

    List<String> getStyleNumbersByVersion(String versionNumber);

    List<Fabric> getOneMonthFabric();

    List<Fabric> getThreeMonthsFabric();
}
