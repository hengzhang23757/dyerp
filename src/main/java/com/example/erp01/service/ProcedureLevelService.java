package com.example.erp01.service;

import com.example.erp01.model.ProcedureLevel;

import java.util.List;

public interface ProcedureLevelService {

    List<ProcedureLevel> getAllProcedureLevel();

    int addProcedureLevel(ProcedureLevel procedureLevel);

    int deleteProcedureLevel(Integer levelID);

    int updateProcedureLevel(ProcedureLevel procedureLevel);

    Float getLevelValueByName(String level);

}
