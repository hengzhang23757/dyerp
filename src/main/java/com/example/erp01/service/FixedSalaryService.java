package com.example.erp01.service;

import com.example.erp01.model.FixedSalary;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface FixedSalaryService {

    Integer addFixedSalary(FixedSalary fixedSalary);

    Integer deleteFixedSalary(Integer fixedID);

    List<FixedSalary> getAllFixedSalary();

    List<String> getAllFixedSalaryValue();

}
