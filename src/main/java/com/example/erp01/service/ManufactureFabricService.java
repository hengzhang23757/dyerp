package com.example.erp01.service;

import com.example.erp01.model.AddFeeVo;
import com.example.erp01.model.FabricAccessoryReview;
import com.example.erp01.model.ManufactureFabric;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ManufactureFabricService {

    Integer addManufactureFabricBatch(List<ManufactureFabric> manufactureFabricList);

    List<ManufactureFabric> getManufactureFabricByOrder(String orderName, String colorName);

    Integer deleteManufactureFabricByName(String orderName);

    Integer deleteManufactureFabricByID(Integer fabricID);

    Integer updateManufactureFabric(ManufactureFabric manufactureFabric);

    List<ManufactureFabric> getManufactureFabricHintByFabricName(String subFabricName,Integer page,Integer limit);

    Integer updateManufactureFabricOnPlaceOrder(List<ManufactureFabric> manufactureFabricList);

    List<String> getDistinctSupplier();

    List<ManufactureFabric> getManufactureFabricOneYear(String orderName);

    ManufactureFabric getManufactureFabricByID(Integer fabricID);

    List<ManufactureFabric> getManufactureFabricByInfo(String orderName, String fabricName, String colorName, String fabricColor);

    List<ManufactureFabric> getManufactureFabricInPlaceOrder(String orderName, String supplier, String checkState);

    Integer changeFabricOrderStateInPlaceOrder(List<Integer> fabricIDList);

    List<ManufactureFabric> getManufactureFabricSummaryByOrder(String orderName);

    Integer updateManufactureFabricBatch(List<ManufactureFabric> manufactureFabricList);

    Integer changeFabricCheckStateInPlaceOrder(List<Integer> fabricIDList, String preCheck, String checkState, String preCheckEmp, String checkEmp);

    List<ManufactureFabric> getManufactureFabricByOrderCheckState(String orderName, String checkState);

    List<ManufactureFabric> getManufactureFabricByOrderListCheckState(List<String> orderNameList, String checkState);

    List<String> getFabricNameHint(String subName);

    List<ManufactureFabric> getManufactureFabricInPlaceOrderByInfo(List<String> orderNameList, String supplier, String materialName, String checkState);

    List<FabricAccessoryReview> getManufactureFabricReviewState(String checkState);

    List<ManufactureFabric> getFabricCheckStateRecentYearByOrder(List<String> orderNameList);

    List<ManufactureFabric> getManufactureFabricByCheckNumberState(String checkNumber, String checkState);

    Integer destroyFabricCheck(String checkNumber);

    Integer getFabricCheckNumberCountByState(String checkState);

    ManufactureFabric getOneManufactureFabricByInfo(String orderName, String colorName, String fabricName);

    Integer updateManufactureFabricAfterCommit(List<ManufactureFabric> manufactureFabricList);

    Integer updateFabricOrderStateByCheckNumber(String checkNumber);

    List<String> getDistinctFabricNameByOrder(String orderName);

    Integer deleteManufactureFabricBatch(List<Integer> fabricIDList);

    List<ManufactureFabric> getManufactureFabricByFabricIdList(List<Integer> fabricIDList);

    Integer reAddManufactureFabric(ManufactureFabric manufactureFabric);

    List<String> getFabricCheckNumberHint(String keyWord);

    List<FabricAccessoryReview> getManufactureFabricReviewStateCombine(Map<String, Object> map);

    void changeFeeStateBatch(List<Integer> fabricIDList, Integer feeState);

    List<AddFeeVo> getFeeVoByInfo(Map<String, Object> map);

    void changeOrderName(String orderName, String toOrderName);


}
