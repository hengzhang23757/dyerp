package com.example.erp01.service;

import com.example.erp01.model.Metre;

import java.util.Date;
import java.util.List;

public interface MetreService {

    List<Metre> getMetreByOrderTimeEmp(Integer procedureNumber, String orderName, String employeeName, Date fromTime, Date toTime);

    List<Metre> getMetreByInfo(String orderName, List<Integer> procedureNumberList , List<String> groupNameList, List<String> employeeNameList, Date fromTime, Date toTime);
}
