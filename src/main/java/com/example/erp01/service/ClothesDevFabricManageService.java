package com.example.erp01.service;

import com.example.erp01.model.ClothesDevFabricManage;

import java.util.List;
import java.util.Map;

public interface ClothesDevFabricManageService {

    Integer addClothesDevFabricManage(ClothesDevFabricManage clothesDevFabricManage);

    List<ClothesDevFabricManage> getClothesDevFabricManageByMap(Map<String, Object> map);

    Integer updateClothesDevFabricManage(ClothesDevFabricManage clothesDevFabricManage);

    Integer deleteClothesDevFabricManage(Integer id);

    List<String> getClothsDevFabricNameHint(String keyWord);

    List<String> getClothsDevFabricNumberHint(String keyWord);

    List<ClothesDevFabricManage> getClothesDevFabricManageHint(String subFabricName, Integer page, Integer limit);

    Integer deleteClothesDevFabricRecordByFabricId(Integer fabricID);

    ClothesDevFabricManage getClothesDevFabricManageById(Integer id);

    Integer updateClothesDevFabricManageBatch(List<ClothesDevFabricManage> clothesDevFabricManageList);

    Integer deleteClothesDevFabricManageBatch(List<Integer> idList);

    String getMaxClothesDevFabricOrderByPreFix(String preFix);

    List<ClothesDevFabricManage> getClothesDevFabricManageRecordByInfo(Map<String, Object> map);

    Integer quitClothesDevFabricManageCheck(List<Integer> idList);

}
