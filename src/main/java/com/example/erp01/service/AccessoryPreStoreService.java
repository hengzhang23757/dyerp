package com.example.erp01.service;

import com.example.erp01.model.AccessoryPreStore;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface AccessoryPreStoreService {

    Integer addAccessoryPreStoreBatch(List<AccessoryPreStore> accessoryPreStoreList);

    Integer deleteAccessoryPreStoreById(Integer id);

    Integer deleteAccessoryPreStoreBatch(List<Integer> idList);

    Integer updateAccessoryPreStore(AccessoryPreStore accessoryPreStore);

    List<AccessoryPreStore> getAllAccessoryPreStore();

    List<AccessoryPreStore> getAccessoryPreStoreHint(String subAccessoryName, Integer page, Integer limit);

    AccessoryPreStore getAccessoryPreStoreById(Integer id);

    List<AccessoryPreStore> getAccessoryPreStoreByPreId(Integer preStoreId);

    Integer updateAccessoryPreStoreBatch(List<AccessoryPreStore> accessoryPreStoreList);

    Integer addAccessoryPreStore(AccessoryPreStore accessoryPreStore);

    List<AccessoryPreStore> getAccessoryPreStoreByAccessoryNumber(String accessoryNumber, String operateType);

    String getMaxAccessoryOrderByPreFix(String preFix);

    List<AccessoryPreStore> getAccessoryStorageByAccessoryNumberList(List<String> accessoryNumberList);

    List<AccessoryPreStore> getAccessoryPreStoreByInfo(Map<String, Object> map);

    Integer quitCheckAccessoryPreStoreBatch(List<Integer> idList);

    List<AccessoryPreStore> getHistoryIndexAccessoryPreStoreByInfo(Map<String, Object> map);

    Integer deleteAccessoryPreStoreByPreId(Integer preStoreId);

    List<AccessoryPreStore> getAccessoryPreStoreForCombine();

    Integer updateAccessoryPreStoreBatchInCombineOrder(List<AccessoryPreStore> accessoryPreStoreList);

    List<AccessoryPreStore> getAccessoryPreStoreForCheck(Map<String, Object> param);

    List<AccessoryPreStore> getAccessoryPreStoreByCheckNumber(String checkNumber);

    Integer updateAccessoryPreStoreOrderState(String checkNumber, String orderState);

    Integer updateAccessoryPreStoreCheckStateByCheckNumber(String checkNumber, String checkState);

    Integer getAccessoryPreStoreCheckCount();

    AccessoryPreStore getMaxStorageAccessoryPreStoreByAccessoryNumber(String accessoryNumber, String specification, String accessoryColor);

    Integer updateAccessoryAfterCommit(Integer id, Float accessoryCount, Float price);

    Integer updateAccessoryPreStoreInMonthCheck(List<AccessoryPreStore> accessoryPreStoreList);

}
