package com.example.erp01.service;

import com.example.erp01.model.OPA;
import com.example.erp01.model.OPALeak;
import com.example.erp01.model.OpaBackInput;
import com.example.erp01.model.OtherOPA;

import java.util.Date;
import java.util.List;

public interface OPALeakService {

    List<String> getOPAPartNamesByOrder(String orderName);

    List<String> getOtherOPAPartNamesByOrder(String orderName);

    List<OPALeak> getTailorOPA(String orderName, String partName, Integer bedNumber);

    List<OPALeak> getOtherTailorOPA(String orderName, String partName, Integer bedNumber);

    List<OPALeak> getOtherOPA(String orderName, String partName);

    List<OPALeak> getOPABack(String orderName, String partName, Date fromDate, Date toDate);

    List<OPALeak> getTailorOPAByBed(String orderName, String partName, Integer bedNumber);

    List<OPALeak> getOtherTailorOPAByBed(String orderName, String partName, Integer bedNumber);

    List<OPALeak> getMonthOPAInfo(Date fromDate, Date toDate);

    List<OPALeak> getOtherMonthOPAInfo(Date fromDate, Date toDate);

    List<OPALeak> getMonthOPABackInfo(Date fromDate, Date toDate);

    List<OPALeak> getTailorCountByOrderPart(String orderName, String partName);

    List<OPALeak> getOtherTailorCountByOrderPart(String orderName, String partName);

    List<Integer> getOPABedNumbersByOrderPart(String orderName, String partName);

    List<OPALeak> getTailorInfoByOrderPart(String orderName, String partName);

    List<OPALeak> getOtherTailorInfoByOrderPart(String orderName, String partName);

    List<OPA> getOPAByOrderTime(String orderName, String partName, Date fromDate, Date toDate);

    List<OtherOPA> getOtherOPAByOrderTime(String orderName, String partName, Date fromDate, Date toDate);

    List<OpaBackInput> getOPABackInputByOrderTime(String orderName, String partName, Date fromDate, Date toDate);

}
