package com.example.erp01.service;

import com.example.erp01.model.*;

import java.util.List;
import java.util.Map;

public interface ClothesDevService {


    // 基本
    Integer addClothesDevInfo(ClothesDevInfo clothesDevInfo);

    Integer updateClothesDevInfo(ClothesDevInfo clothesDevInfo);

    ClothesDevInfo getClothesDevInfoById(Integer id);

    List<ClothesDevInfo> getClothesDevInfoByMap(Map<String, Object> map);

    String getMaxClothesVersionOrder(String clothesPreFix);

    String getClothesDevOrderNameByPreFix(String preFix);

    ClothesDevInfo getOneClothesDevInfoByMap(Map<String, Object> map);

    Integer updateClothesDevInfoByDevNumber(ClothesDevInfo clothesDevInfo);

    Integer deleteClothesDevInfoByDevNumber(String clothesDevNumber);

    // 颜色
    Integer addClothesDevColorBatch(List<ClothesDevColor> clothesDevColorList);

    List<ClothesDevColor> getClothesDevColorByMap(Map<String, Object> map);

    Integer deleteClothesDevColorByDevId(Integer devId);

    Integer deleteClothesDevColorByDevNumber(String clothesDevNumber);

    // 尺码
    Integer addClothesDevSizeBatch(List<ClothesDevSize> clothesDevSizeList);

    List<ClothesDevSize> getClothesDevSizeByMap(Map<String, Object> map);

    Integer deleteClothesDevSizeByDevId(Integer devId);

    Integer deleteClothesDevSizeByDevNumber(String clothesDevNumber);

    // 面料
    Integer addClothesDevFabricBatch(List<ClothesDevFabric> clothesDevFabrics);

    Integer deleteClothesDevFabricBatch(List<Integer> idList);

    List<ClothesDevFabric> getClothesDevFabricByMap(Map<String, Object> map);

    Integer updateClothesDevFabricBatch(List<ClothesDevFabric> clothesDevFabricList);

    Integer deleteClothesDevFabricByDevNumber(String clothesDevNumber);

    List<ClothesDevFabric> getUniqueClothesDevFabricByDevNumber(String clothesDevNumber);

    // 辅料
    Integer addClothesDevAccessoryBatch(List<ClothesDevAccessory> clothesDevAccessoryList);

    Integer deleteClothesDevAccessoryBatch(List<Integer> idList);

    List<ClothesDevAccessory> getClothesDevAccessoryByMap(Map<String, Object> map);

    Integer updateClothesDevAccessoryBatch(List<ClothesDevAccessory> clothesDevAccessoryList);

    List<ClothesDevAccessory> getClothesDevAccessoryHint(String subAccessoryName, Integer page, Integer limit);

    Integer deleteClothesDevAccessoryByDevNumber(String clothesDevNumber);

    //图片
    Integer addClothesDevImage(ClothesDevImage clothesDevImage);

    Integer deleteClothesDevImageByDevId(Integer devId);

    Integer updateClothesDevImage(ClothesDevImage clothesDevImage);

    ClothesDevImage getClothesDevImageByMap(Map<String, Object> map);

    Integer updateClothesDevImageByDevNumber(ClothesDevImage clothesDevImage);

    Integer deleteClothesDevImageByDevNumber(String clothesDevNumber);

    // 工艺
    Integer addClothesDevProcess(ClothesDevProcess clothesDevProcess);

    Integer updateClothesDevProcess(ClothesDevProcess clothesDevProcess);

    Integer deleteClothesDevProcessByDevId(Integer devId);

    ClothesDevProcess getClothesDevProcessByMap(Map<String, Object> map);

    Integer updateClothesDevProcessByDevNumber(ClothesDevProcess clothesDevProcess);

    Integer deleteClothesDevProcessByDevNumber(String clothesDevNumber);

    // 尺寸
    Integer addClothesDevMeasure(ClothesDevMeasure clothesDevMeasure);

    Integer updateClothesDevMeasure(ClothesDevMeasure clothesDevMeasure);

    Integer deleteClothesDevMeasureByDevId(Integer devId);

    ClothesDevMeasure getClothesDevMeasureByMap(Map<String, Object> map);

    Integer updateClothesDevMeasureByDevNumber(ClothesDevMeasure clothesDevMeasure);

    Integer deleteClothesDevMeasureByDevNumber(String clothesDevNumber);

}
