package com.example.erp01.service;

import com.example.erp01.model.ClothesDevAccessoryNumber;

import java.util.List;

public interface ClothesDevAccessoryNumberService {

    Integer addClothesDevAccessoryNumber(ClothesDevAccessoryNumber clothesDevAccessoryNumber);

    Integer deleteClothesDevAccessoryNumber(Integer id);

    List<ClothesDevAccessoryNumber> getAllClothesDevAccessoryNumber();

    Integer updateClothesDevAccessoryNumber(ClothesDevAccessoryNumber clothesDevAccessoryNumber);


}
