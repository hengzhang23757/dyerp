package com.example.erp01.service;

import com.example.erp01.model.OtherStorage;

import java.util.List;

public interface OtherStorageService {

    int otherInStore(List<OtherStorage> otherStorageList);

    int otherOutStore(List<OtherStorage> otherStorageList);

    List<OtherStorage> searchOtherInStorage(String orderName, String partName, String colorName, String sizeName);

    List<OtherStorage> searchOtherOutStorage(String orderName, String partName, String colorName, String sizeName);
}
