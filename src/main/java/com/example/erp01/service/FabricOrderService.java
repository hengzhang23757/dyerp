package com.example.erp01.service;

import com.example.erp01.model.FabricOrder;

import java.util.List;

public interface FabricOrderService {

    List<FabricOrder> getAllFabricOrder();

    List<FabricOrder> getTodayFabricOrder();

    List<FabricOrder> getOneMonthFabricOrder();

    List<FabricOrder> getThreeMonthsFabricOrder();

    Integer addFabricOrderBatch(List<FabricOrder> fabricOrderList);

    Integer updateFabricOrder(FabricOrder fabricOrder);

    Integer commitFabricOrder(List<Integer> fabricOrderIDList);

    Integer deleteFabricOrderBatch(List<Integer> fabricOrderIDList);

    Integer deleteFabricOrder(Integer fabricOrderID);

}
