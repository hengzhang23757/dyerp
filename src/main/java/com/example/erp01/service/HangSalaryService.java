package com.example.erp01.service;

import com.example.erp01.model.Completion;
import com.example.erp01.model.CompletionCount;
import com.example.erp01.model.HangSalary;
import com.example.erp01.model.WeekAmount;

import java.util.Date;
import java.util.List;

public interface HangSalaryService {

    List<HangSalary> getHangSalaryByTimeEmp(Date from, Date to, String employeeNumber);

    List<HangSalary> getGroupHangSalaryByTime(Date from, Date to, String groupName);

    List<HangSalary> getHangProcedureProgress(Date from, Date to, Integer procedureFrom, Integer procedureTo, String orderName, String groupName);

    List<WeekAmount> getHangWeekAmount();

    List<WeekAmount> getHangFinishWeekAmount();

    List<CompletionCount> getHangCompletion(Integer procedureNumber);

    List<String> getEmployeeNamesByOrderProcedure(String orderName, Integer procedureNumber);

    List<HangSalary> getOrderHangSalarySummary(String orderName,Date from,Date to);

    List<HangSalary> getHangProcedureProgressNew(Date from, Date to, Integer procedureFrom, Integer procedureTo, String orderName, String groupName, String employeeNumber);

    List<HangSalary> getHangSalaryDetail(Date from, Date to, String employeeNumber, String orderName);

    List<HangSalary> getHangSalaryByOrderColorSizeEmp(Date from, Date to, String employeeNumber, String orderName, String colorName, String sizeName);

    Integer getHangPieceCountByOrderColorSize(String orderName, Integer procedureNumber, String colorName, String sizeName);

    List<HangSalary> getHangProcedureProgressMultiGroup(Date from, Date to, Integer procedureFrom, Integer procedureTo, String orderName, List<String> groupList, String employeeNumber);
}
