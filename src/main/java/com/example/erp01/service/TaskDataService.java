package com.example.erp01.service;

import com.example.erp01.model.GeneralSalary;
import com.example.erp01.model.PieceWork;
import com.example.erp01.model.TaskData;
import com.example.erp01.model.TaskLinks;

import java.util.Date;
import java.util.List;

public interface TaskDataService {

    Integer addTaskData(TaskData taskData);

    Integer addTaskDataBatch(List<TaskData> taskDataList);

    List<TaskData> getTaskDataByInfo(Date from, Date to, String groupName);

    Integer updateTaskData(TaskData taskData);

    Integer deleteTaskDataById(Integer id);

    List<TaskData> getAllTaskData();

    List<TaskData> getTaskDataByTypeGroup(Date from, Date to, List<String> taskTypeList, List<String> groupNameList);

    List<TaskData> getUpdateTaskDataByInfo(Integer procedureNumber, Long maxNid);

    Long getMaxNidFromTaskData();

    TaskData getActTaskDataByInfo(Date updateTime, String groupName, String orderName);

    TaskData getPlanTaskDataByInfo(Date startDate, String groupName, String orderName);

    TaskData getParentTaskDataByGroupName(String groupName, String taskType);

    List<TaskData> getLooseFabricCountEachOrder();

    List<TaskData> getTaskDataByTaskTypeGroupName(String taskType, String groupName);

    List<TaskLinks> getAllTaskLinks();

    TaskData getTaskDataById(Integer id);

    List<TaskData> getUnFinishTaskDataByTaskTypeGroupName(String taskType, String groupName);

    TaskData getCurrentTaskDataByGroupType(String taskType, String groupName);

    Integer clearTaskDataByGroupType(String taskType, String groupName);

    Integer addTaskLinksBatch(List<TaskLinks> taskLinkList);

    List<PieceWork> getSomeDayFinishCount(String groupName, Date someDay);

    List<TaskData> getAllTaskDataByType(String taskType);

    TaskData getTaskDataByBetweenDateType(String taskType, String groupName, String orderName, Date someDay);

    TaskData getPlanTaskDataByOrder(String groupName, String orderName, Date someDay);

    List<GeneralSalary> getGroupDailyPieceWorkByInfo(String groupName, String orderName, Date someDay);

    List<Integer> getActIdByDeliveryState(String deliveryState);

    Integer getFinishCountByTaskBegin(String groupName, String orderName, Date startDate);
}
