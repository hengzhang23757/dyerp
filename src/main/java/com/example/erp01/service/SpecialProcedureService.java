package com.example.erp01.service;

import com.example.erp01.model.SpecialProcedure;

import java.util.List;

public interface SpecialProcedureService {

    int addSpecialProcedureBatch(List<SpecialProcedure> specialProcedureList);

    int deleteSpecialProcedure(Integer specialID);

    List<SpecialProcedure> getAllSpecialProcedure();

    List<SpecialProcedure> getSpecialProcedureByOrderProcedure(String orderName, Integer procedureNumber);

}
