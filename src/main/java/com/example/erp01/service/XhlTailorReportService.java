package com.example.erp01.service;

import com.example.erp01.model.XhlTailor;
import com.example.erp01.model.XhlTailorReport;

import java.util.Date;
import java.util.List;

public interface XhlTailorReportService {

    List<XhlTailorReport> getXhlTailorReportByInfo(Date from, Date to, String customerName, String orderName, Integer bedNumber, String colorName, String userName);

    List<XhlTailor> getXhlTailorDetailByInfo(Date from, Date to, String customerName, String orderName, Integer bedNumber, String colorName, String userName);

}
