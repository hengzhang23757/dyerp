package com.example.erp01.service;

import com.example.erp01.model.OrderProcedure;

import java.util.List;

public interface ProcedureReviewService {

    List<OrderProcedure> getOrderProcedureByInfo(String from, String to, String orderName);

    Integer updateOrderProcedureByIdBatch(List<Integer> orderProcedureIDList, Integer procedureState);

    List<String> getMonthPieceWorkOrderName(String from, String to, String orderName);
}
