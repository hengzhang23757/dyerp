package com.example.erp01.service;

import com.example.erp01.model.EndProductPay;

import java.util.List;

public interface EndProductPayService {

    Integer addEndProductPay(EndProductPay endProductPay);

    Integer addEndProductPayBatch(List<EndProductPay> endProductPayList);

    Integer deleteEndProductPay(Integer id);

    Integer updateEndProductPay(EndProductPay endProductPay);

    List<EndProductPay> getEndProductPaySummary();

    Integer getEndProductPayCountByOrder(String orderName);

    List<EndProductPay> getEndProductPayByOrderName(String orderName);

    Integer deleteEndProductPayBatch(List<Integer> idList);

    List<EndProductPay> getEndProductPayByInfo(String customerName, String payMonth, String orderName, String payType);

    Integer deleteEndProductPayByPayNumber(String payNumber);

    EndProductPay getEndProductPayByPayNumber(String payNumber);

    List<EndProductPay> monthSummaryByCustomer(String payMonth);

}
