package com.example.erp01.service;

import com.example.erp01.model.Room;
import com.example.erp01.model.RoomState;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RoomService {

    int addRoomBatch(List<Room> roomList);

    int deleteRoom(Integer roomID);

    int updateRoom(Room room);

    List<Room> getAllRoom();

    List<String> getAllFloor();

    List<RoomState> getAllRoomInfoByFloor(String floor);

    Integer getCapacityOfRoom(String roomNumber);

    Room getRoomByID(Integer roomID);

}
