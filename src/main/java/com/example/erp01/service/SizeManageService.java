package com.example.erp01.service;

import com.example.erp01.model.SizeManage;

import java.util.List;

public interface SizeManageService {

    List<SizeManage> getAllSizeManage();

    Integer addSizeManageOne(SizeManage sizeManage);

    Integer deleteSizeManageById(Integer sizeManageID);

    List<String> getAllSizeName();
}
