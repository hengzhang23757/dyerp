package com.example.erp01.service;

import com.example.erp01.model.StyleManage;

import java.util.List;

public interface StyleManageService {

    Integer addStyleManage(StyleManage styleManage);

    Integer deleteStyleManage(Integer id);

    List<StyleManage> getAllStyleManage();

    Integer updateStyleManage(StyleManage styleManage);

    Integer updateProcessInStyleManage(StyleManage styleManage);

    Integer updateSizeInStyleManage(StyleManage styleManage);

    StyleManage getStyleManageById(Integer id);

    List<StyleManage> getStyleManageHintPage(String keyWord, int page, int limit);

    Integer getStyleManageCount(String keyWord);
}
