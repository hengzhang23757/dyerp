package com.example.erp01.service;

import com.example.erp01.model.OrderDetection;

import java.util.List;
import java.util.Map;

public interface OrderDetectionService {

    Integer addOrderDetection(OrderDetection orderDetection);

    Integer updateOrderDetection(OrderDetection orderDetection);

    Integer deleteOrderDetection(Integer id);

    List<OrderDetection> getOrderDetectionByInfo(Map<String, Object> map);

    OrderDetection getOrderDetectionById(Integer id);

}
