package com.example.erp01.service.impl;

import com.example.erp01.mapper.StyleImageMapper;
import com.example.erp01.model.StyleImage;
import com.example.erp01.service.StyleImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StyleImageServiceImpl implements StyleImageService {

    @Autowired
    private StyleImageMapper styleImageMapper;

    @Override
    public Integer addStyleImage(StyleImage styleImage) {
        try{
            styleImageMapper.addStyleImage(styleImage);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public StyleImage getStyleImageByOrder(String orderName) {
        StyleImage styleImage;
        try{
            styleImage = styleImageMapper.getStyleImageByOrder(orderName);
            return styleImage;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void changeOrderName(String orderName, String toOrderName) {
        styleImageMapper.changeOrderName(orderName, toOrderName);
    }
}
