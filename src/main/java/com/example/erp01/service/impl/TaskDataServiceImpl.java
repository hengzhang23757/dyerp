package com.example.erp01.service.impl;

import com.example.erp01.mapper.TaskDataMapper;
import com.example.erp01.model.GeneralSalary;
import com.example.erp01.model.PieceWork;
import com.example.erp01.model.TaskData;
import com.example.erp01.model.TaskLinks;
import com.example.erp01.service.TaskDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class TaskDataServiceImpl implements TaskDataService {

    @Autowired
    private TaskDataMapper taskDataMapper;

    @Override
    public Integer addTaskData(TaskData taskData) {
        try{
            taskDataMapper.addTaskData(taskData);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer addTaskDataBatch(List<TaskData> taskDataList) {
        try{
            if (taskDataList != null && !taskDataList.isEmpty()){
                taskDataMapper.addTaskDataBatch(taskDataList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<TaskData> getTaskDataByInfo(Date from, Date to, String groupName) {
        List<TaskData> taskDataList = new ArrayList<>();
        try{
            taskDataList = taskDataMapper.getTaskDataByInfo(from, to, groupName);
            return taskDataList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return taskDataList;
    }

    @Override
    public Integer updateTaskData(TaskData taskData) {
        try{
            taskDataMapper.updateTaskData(taskData);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer deleteTaskDataById(Integer id) {
        try{
            taskDataMapper.deleteTaskDataById(id);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<TaskData> getAllTaskData() {
        List<TaskData> taskDataList = new ArrayList<>();
        try{
            taskDataList = taskDataMapper.getAllTaskData();
        }catch (Exception e){
            e.printStackTrace();
        }
        return taskDataList;
    }

    @Override
    public List<TaskData> getTaskDataByTypeGroup(Date from, Date to, List<String> taskTypeList, List<String> groupNameList) {
        List<TaskData> taskDataList = new ArrayList<>();
        try{
            taskDataList = taskDataMapper.getTaskDataByTypeGroup(from, to, taskTypeList, groupNameList);
        }catch (Exception e){
            e.printStackTrace();
        }
        return taskDataList;
    }

    @Override
    public List<TaskData> getUpdateTaskDataByInfo(Integer procedureNumber, Long maxNid) {
        List<TaskData> taskDataList = new ArrayList<>();
        try{
            taskDataList = taskDataMapper.getUpdateTaskDataByInfo(procedureNumber, maxNid);
            return taskDataList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return taskDataList;
    }

    @Override
    public Long getMaxNidFromTaskData() {
        Long maxNid = 0L;
        try{
            if (taskDataMapper.getMaxNidFromTaskData() != null){
                maxNid = taskDataMapper.getMaxNidFromTaskData();
            }
            return maxNid;
        }catch (Exception e){
            e.printStackTrace();
        }
        return maxNid;
    }

    @Override
    public TaskData getActTaskDataByInfo(Date updateTime, String groupName, String orderName) {
        TaskData taskData;
        try{
            taskData = taskDataMapper.getActTaskDataByInfo(updateTime, groupName, orderName);
            return taskData;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public TaskData getPlanTaskDataByInfo(Date startDate, String groupName, String orderName) {
        TaskData taskData;
        try{
            taskData = taskDataMapper.getPlanTaskDataByInfo(startDate, groupName, orderName);
            return taskData;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public TaskData getParentTaskDataByGroupName(String groupName, String taskType) {
        TaskData taskData;
        try{
            taskData = taskDataMapper.getParentTaskDataByGroupName(groupName, taskType);
            return taskData;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<TaskData> getLooseFabricCountEachOrder() {
        List<TaskData> taskDataList = new ArrayList<>();
        try{
            taskDataList = taskDataMapper.getLooseFabricCountEachOrder();
            return taskDataList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return taskDataList;
    }

    @Override
    public List<TaskData> getTaskDataByTaskTypeGroupName(String taskType, String groupName) {
        List<TaskData> taskDataList = new ArrayList<>();
        try{
            taskDataList = taskDataMapper.getTaskDataByTaskTypeGroupName(taskType, groupName);
            return taskDataList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return taskDataList;
    }

    @Override
    public List<TaskLinks> getAllTaskLinks() {
        List<TaskLinks> taskLinksList = new ArrayList<>();
        try{
            taskLinksList = taskDataMapper.getAllTaskLinks();
            return taskLinksList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return taskLinksList;
    }

    @Override
    public TaskData getTaskDataById(Integer id) {
        TaskData taskData = null;
        try{
            taskData = taskDataMapper.getTaskDataById(id);
            return taskData;
        }catch (Exception e){
            e.printStackTrace();
        }
        return taskData;
    }

    @Override
    public List<TaskData> getUnFinishTaskDataByTaskTypeGroupName(String taskType, String groupName) {
        List<TaskData> taskDataList = new ArrayList<>();
        try{
            taskDataList = taskDataMapper.getUnFinishTaskDataByTaskTypeGroupName(taskType, groupName);
            return taskDataList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return taskDataList;
    }

    @Override
    public TaskData getCurrentTaskDataByGroupType(String taskType, String groupName) {
        TaskData taskData = null;
        try{
            taskData = taskDataMapper.getCurrentTaskDataByGroupType(taskType, groupName);
            return taskData;
        }catch (Exception e){
            e.printStackTrace();
        }
        return taskData;
    }

    @Override
    public Integer clearTaskDataByGroupType(String taskType, String groupName) {
        try{
            taskDataMapper.clearTaskDataByGroupType(taskType, groupName);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer addTaskLinksBatch(List<TaskLinks> taskLinkList) {
        try{
            if (taskLinkList != null && !taskLinkList.isEmpty()){
                taskDataMapper.addTaskLinksBatch(taskLinkList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<PieceWork> getSomeDayFinishCount(String groupName, Date someDay) {
        List<PieceWork> pieceWorkList = new ArrayList<>();
        try{
            pieceWorkList = taskDataMapper.getSomeDayFinishCount(groupName, someDay);
            return pieceWorkList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return pieceWorkList;
    }

    @Override
    public List<TaskData> getAllTaskDataByType(String taskType) {
        List<TaskData> taskDataList = new ArrayList<>();
        try{
            taskDataList = taskDataMapper.getAllTaskDataByType(taskType);
            return taskDataList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return taskDataList;
    }

    @Override
    public TaskData getTaskDataByBetweenDateType(String taskType, String groupName, String orderName, Date someDay) {
        TaskData taskData = null;
        try{
            taskData = taskDataMapper.getTaskDataByBetweenDateType(taskType, groupName, orderName, someDay);
            return taskData;
        }catch (Exception e){
            e.printStackTrace();
        }
        return taskData;
    }

    @Override
    public TaskData getPlanTaskDataByOrder(String groupName, String orderName, Date someDay) {
        TaskData taskData = null;
        try{
            taskData = taskDataMapper.getPlanTaskDataByOrder(groupName, orderName, someDay);
            return taskData;
        }catch (Exception e){
            e.printStackTrace();
        }
        return taskData;
    }

    @Override
    public List<GeneralSalary> getGroupDailyPieceWorkByInfo(String groupName, String orderName, Date someDay) {
        List<GeneralSalary> generalSalaryList = new ArrayList<>();
        try{
            generalSalaryList = taskDataMapper.getGroupDailyPieceWorkByInfo(groupName, orderName, someDay);
            return generalSalaryList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return generalSalaryList;
    }

    @Override
    public List<Integer> getActIdByDeliveryState(String deliveryState) {
        List<Integer> idList = new ArrayList<>();
        try{
            idList = taskDataMapper.getActIdByDeliveryState(deliveryState);
            return idList;
        } catch (Exception e){
            e.printStackTrace();
        }
        return idList;
    }

    @Override
    public Integer getFinishCountByTaskBegin(String groupName, String orderName, Date startDate) {
        Integer finishCount = 0;
        try{
            if (taskDataMapper.getFinishCountByTaskBegin(groupName, orderName, startDate) != null){
                finishCount = taskDataMapper.getFinishCountByTaskBegin(groupName, orderName, startDate);
            }
            return finishCount;
        }catch (Exception e){
            e.printStackTrace();
        }
        return finishCount;
    }

}
