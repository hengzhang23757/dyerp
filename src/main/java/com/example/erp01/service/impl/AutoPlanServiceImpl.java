package com.example.erp01.service.impl;

import com.example.erp01.mapper.EmployeeMapper;
import com.example.erp01.model.EmpSkills;
import com.example.erp01.model.Employee;
import com.example.erp01.model.ProcedureTemplate;
import com.example.erp01.model.ScheduleRecord;
import com.example.erp01.service.AutoPlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class AutoPlanServiceImpl implements AutoPlanService {

    @Autowired
    private EmployeeMapper employeeMapper;

    @Override
    public double[][] generateSkillMatrix(List<String> empList, List<EmpSkills> empSkills, List<ProcedureTemplate> procedureList) {
        int m = empList.size();
        int n = procedureList.size();
        double[][] skills = new double[m][n];
        Map<Integer, Integer> standardSkills = procedureList.stream().collect(Collectors.toMap(ProcedureTemplate::getProcedureCode, ProcedureTemplate::getHourCap));

        for (int i = 0; i < m; i++) {
            int finalI = i;
            for (int j = 0; j < n; j++) {
                int finalJ = j;
                Integer hourCap = empSkills.stream().filter(o -> empList.get(finalI).equals(o.getEmployeeNumber()) && procedureList.get(finalJ).getProcedureCode().equals(o.getProcedureCode()))
                        .findFirst().map(EmpSkills::getHourCap).orElse(procedureList.get(finalJ).getHourCap());
                skills[i][j] = hourCap;
            }
        }
        return skills;
    }

    @Override
    public List<ScheduleRecord> generateEmpPlan(double[] xes, List<String> empList, List<ProcedureTemplate> procedureList, List<EmpSkills> skills, List<Employee> employeeList) {
        if(xes.length != empList.size()*procedureList.size()) return null;
        List<ScheduleRecord> recordList = new ArrayList<ScheduleRecord>();
        int m = empList.size();
        int n = procedureList.size();
        for(int i = 0; i < m;i++) {
            for(int j = 0; j < n;j++) {
                if(xes[i*n+j] != 0){
                    int finalI = i;
                    int finalJ = j;
                    EmpSkills empSkill = skills.stream().filter(o -> o.getEmployeeNumber().equals(empList.get(finalI)) && o.getProcedureCode().equals(procedureList.get(finalJ).getProcedureCode())).findFirst().orElse(null);
                    Employee tmpEmp = employeeList.stream().filter(o->o.getEmployeeNumber().equals(empList.get(finalI))).findFirst().orElse(new Employee());
                    ScheduleRecord record = new ScheduleRecord();
                    record.setEmployeeNumber(tmpEmp.getEmployeeNumber());
                    record.setEmployeeName(tmpEmp.getEmployeeName());
                    record.setHourCount((float) xes[i*n+j]);
                    record.setProcedureCode(procedureList.get(j).getProcedureCode());
                    record.setProcedureName(procedureList.get(j).getProcedureName());
                    record.setProcedureNumber(procedureList.get(j).getProcedureNumber());
                    if(empSkill!=null){
                        record.setTimeCount(empSkill.getTimeCount());
                        record.setSam(empSkill.getSam());
                    }else{
                        record.setTimeCount((float) procedureList.get(j).getSAM());
                        record.setSam((float) procedureList.get(j).getSAM());
                    }
                    recordList.add(record);
                }
            }
        }
        return recordList;
    }

    @Override
    public Map<String, Object> calculateBalanceAndEchart(double[] xes, double[][] matrixA, double dworkHour) {
        Map<String, Object> result = new HashMap<>();
        double[] dailyOutput = new double[matrixA[0].length];
        for(int i=0; i<matrixA[0].length; i++){
            for(int j=0; j<matrixA.length; j++){
                dailyOutput[i] = dailyOutput[i] + matrixA[j][i] * xes[j*matrixA[0].length+i];
            }
        }
        double sumBalance = 0;
        double averageBalance = 0;
        double minDaily = dailyOutput[0];
        for(int i=0; i<dailyOutput.length; i++){
            dailyOutput[i] = Math.floor(dailyOutput[i]);
            minDaily = minDaily<dailyOutput[i]?minDaily:dailyOutput[i];
        }
        for(int i=0; i<dailyOutput.length; i++){
            sumBalance += minDaily/dailyOutput[i];
        }
        averageBalance = sumBalance/dailyOutput.length;
        result.put("averageBalance",averageBalance);
        result.put("dailyOutput",dailyOutput);
        result.put("minDaily",minDaily);
        return result;
    }
}
