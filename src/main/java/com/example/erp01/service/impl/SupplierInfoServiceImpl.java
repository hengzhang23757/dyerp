package com.example.erp01.service.impl;

import com.example.erp01.mapper.SupplierInfoMapper;
import com.example.erp01.model.SupplierInfo;
import com.example.erp01.service.SupplierInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
@Service
public class SupplierInfoServiceImpl implements SupplierInfoService {
    @Autowired
    private SupplierInfoMapper supplierInfoMapper;

    @Override
    public Integer addSupplierInfo(SupplierInfo supplierInfo) {
        try{
            supplierInfoMapper.addSupplierInfo(supplierInfo);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<SupplierInfo> getSupplierInfoHint(String subSupplierName,Integer page,Integer limit) {
        List<SupplierInfo> supplierInfoList = new ArrayList<>();
        try{
            supplierInfoList = supplierInfoMapper.getSupplierInfoHint(subSupplierName,page,limit);
            return supplierInfoList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return supplierInfoList;
    }

    @Override
    public List<SupplierInfo> getAllSupplierInfo() {
        return supplierInfoMapper.getAllSupplierInfo();
    }

    @Override
    public Integer updateSupplierInfo(SupplierInfo supplierInfo) {
        try{
            supplierInfoMapper.updateSupplierInfo(supplierInfo);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer deleteSupplierInfo(Integer id) {
        try{
            supplierInfoMapper.deleteSupplierInfo(id);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<String> getAllSupplierNameByInfo(String supplierType) {
        return supplierInfoMapper.getAllSupplierNameByInfo(supplierType);
    }
}
