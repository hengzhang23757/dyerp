package com.example.erp01.service.impl;

import com.example.erp01.mapper.ScheduleRecordMapper;
import com.example.erp01.model.Employee;
import com.example.erp01.model.ScheduleRecord;
import com.example.erp01.service.ScheduleRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
@Service
public class ScheduleRecordServiceImpl implements ScheduleRecordService {

    @Autowired
    private ScheduleRecordMapper scheduleRecordMapper;

    @Override
    public Integer addScheduleRecordBatch(List<ScheduleRecord> scheduleRecordList) {
        try{
            if (scheduleRecordList != null && !scheduleRecordList.isEmpty()){
                scheduleRecordMapper.addScheduleRecordBatch(scheduleRecordList);
            }
            return 0;
        } catch (Exception e){
            e.printStackTrace();
        }
        return 1;

    }

    @Override
    public List<ScheduleRecord> getScheduleRecordByInfo(String orderName, String groupName) {
        List<ScheduleRecord> scheduleRecordList = new ArrayList<>();
        try{
            scheduleRecordList = scheduleRecordMapper.getScheduleRecordByInfo(orderName, groupName);
            return scheduleRecordList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return scheduleRecordList;
    }

    @Override
    public Integer deleteScheduleRecordByInfo(String orderName, String groupName) {
        try{
            scheduleRecordMapper.deleteScheduleRecordByInfo(orderName, groupName);
            return 0;
        } catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<ScheduleRecord> getUniqueScheduleRecordOneYear() {
        List<ScheduleRecord> scheduleRecordList = new ArrayList<>();
        try{
            scheduleRecordList = scheduleRecordMapper.getUniqueScheduleRecordOneYear();
            return scheduleRecordList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return scheduleRecordList;
    }

    @Override
    public List<Employee> getEmployeeByOrderGroup(String orderName, String groupName) {
        List<Employee> employeeList = new ArrayList<>();
        try{
            employeeList = scheduleRecordMapper.getEmployeeByOrderGroup(orderName, groupName);
            return employeeList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return employeeList;
    }
}
