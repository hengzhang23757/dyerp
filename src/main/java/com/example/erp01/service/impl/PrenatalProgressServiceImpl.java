package com.example.erp01.service.impl;

import com.example.erp01.mapper.PrenatalProgressMapper;
import com.example.erp01.model.OrderClothes;
import com.example.erp01.model.PrenatalProgress;
import com.example.erp01.service.PrenatalProgressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class PrenatalProgressServiceImpl implements PrenatalProgressService {

    @Autowired
    private PrenatalProgressMapper prenatalProgressMapper;

    @Override
    public List<OrderClothes> getWorkOrderByTime(String from, String to) {
        return prenatalProgressMapper.getWorkOrderByTime(from, to);
    }

    @Override
    public List<PrenatalProgress> getPreCutCountByOrder(String orderName) {
        return prenatalProgressMapper.getPreCutCountByOrder(orderName);
    }
}
