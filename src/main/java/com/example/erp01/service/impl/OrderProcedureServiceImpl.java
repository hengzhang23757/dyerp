package com.example.erp01.service.impl;

import com.example.erp01.mapper.OrderProcedureMapper;
import com.example.erp01.model.OrderProcedure;
import com.example.erp01.model.ProcedureDetail;
import com.example.erp01.model.ProcedureInfo;
import com.example.erp01.model.ProcedureTemplate;
import com.example.erp01.service.OrderProcedureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class OrderProcedureServiceImpl implements OrderProcedureService {

    @Autowired
    private OrderProcedureMapper orderProcedureMapper;


    @Override
    public int addOrderProcedure(OrderProcedure orderProcedure) {
        try{
            orderProcedureMapper.addOrderProcedure(orderProcedure);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public int addOrderProcedureBatch(List<OrderProcedure> orderProcedureList) {
        try{
            if (orderProcedureList != null && !orderProcedureList.isEmpty()){
                orderProcedureMapper.addOrderProcedureBatch(orderProcedureList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public int deleteOrderProcedure(Integer orderProcedureID) {
        try{
            orderProcedureMapper.deleteOrderProcedure(orderProcedureID);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public int deleteOrderProcedureByName(String orderName) {
        try{
            orderProcedureMapper.deleteOrderProcedureByName(orderName);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<OrderProcedure> getUniqueOrderProcedure(Map<String, Object> map) {
        List<OrderProcedure>  orderProcedureList = new ArrayList<>();
        try{
            orderProcedureList = orderProcedureMapper.getUniqueOrderProcedure(map);
            return orderProcedureList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return orderProcedureList;
    }

    @Override
    public List<ProcedureInfo> getProcedureInfoByOrderName(String orderName) {
        List<ProcedureInfo> procedureInfoList = new ArrayList<>();
        try{
            procedureInfoList = orderProcedureMapper.getProcedureInfoByOrderName(orderName);
            return procedureInfoList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return procedureInfoList;
    }

    @Override
    public List<Integer> getProcedureNumbersByOrderName(String orderName) {
        List<Integer> procedureNumberList = new ArrayList<>();
        try{
            procedureNumberList = orderProcedureMapper.getProcedureNumbersByOrderName(orderName);
            return procedureNumberList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return procedureNumberList;
    }

    @Override
    public ProcedureInfo getProcedureCodeNameByNumber(String orderName, Integer procedureNumber) {
        ProcedureInfo procedureInfo = null;
        try{
            procedureInfo = orderProcedureMapper.getProcedureCodeNameByNumber(orderName, procedureNumber);
            return procedureInfo;
        }catch (Exception e){
            e.printStackTrace();
        }
        return procedureInfo;
    }

    @Override
    public List<ProcedureDetail> getProcedureDetailByOrder(String orderName) {
        List<ProcedureDetail> procedureDetailList = new ArrayList<>();
        try{
            procedureDetailList = orderProcedureMapper.getProcedureDetailByOrder(orderName);
            return procedureDetailList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return procedureDetailList;

    }

    @Override
    public List<OrderProcedure> getOrderProcedureByOrder(String orderName) {
        List<OrderProcedure> orderProcedureList = new ArrayList<>();
        try{
            orderProcedureList = orderProcedureMapper.getOrderProcedureByOrder(orderName);
            return orderProcedureList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return orderProcedureList;
    }

    @Override
    public ProcedureInfo getProcedureInfoByOrderProcedureNumber(String orderName, Integer procedureNumber) {
        ProcedureInfo procedureInfo = null;
        try{
            procedureInfo = orderProcedureMapper.getProcedureInfoByOrderProcedureNumber(orderName, procedureNumber);
            return procedureInfo;
        }catch (Exception e){
            e.printStackTrace();
        }
        return procedureInfo;

    }

    @Override
    public Integer changeProcedureState(String orderName, Integer procedureState) {
        try{
            orderProcedureMapper.changeProcedureState(orderName, procedureState);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Float getSectionSamByOrder(String orderName, String procedureSection) {
        Float samValue = 0f;
        try{
            samValue = orderProcedureMapper.getSectionSamByOrder(orderName, procedureSection);
            return samValue;
        }catch (Exception e){
            e.printStackTrace();
        }
        return samValue;
    }

    @Override
    public Integer updateOrderProcedureByProcedureCode(ProcedureTemplate procedureTemplate) {
        try{
            orderProcedureMapper.updateOrderProcedureByProcedureCode(procedureTemplate);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<ProcedureInfo> getSpecialProcedureInfoByOrder(String orderName) {
        List<ProcedureInfo> procedureInfoList = new ArrayList<>();
        try{
            procedureInfoList = orderProcedureMapper.getSpecialProcedureInfoByOrder(orderName);
            return procedureInfoList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return procedureInfoList;
    }

    @Override
    public Integer changeProcedureStateBatch(String orderName, List<Integer> procedureNumberList, Integer procedureState) {
        try{
            Map<String, Object> map = new HashMap<>();
            map.put("orderName",orderName);
            map.put("list",procedureNumberList);
            map.put("procedureState",procedureState);
            orderProcedureMapper.changeProcedureStateBatch(map);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<OrderProcedure> getOrderProcedureToReview(String orderName, List<Integer> procedureNumberList) {
        List<OrderProcedure> orderProcedureList = new ArrayList<>();
        try{
            Map<String, Object> map = new HashMap<>();
            map.put("orderName",orderName);
            map.put("list",procedureNumberList);
            orderProcedureList = orderProcedureMapper.getOrderProcedureToReview(map);
            return orderProcedureList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return orderProcedureList;
    }

    @Override
    public List<OrderProcedure> getOrderProcedureByState(String orderName, List<Integer> procedureStateList) {
        List<OrderProcedure> orderProcedureList = new ArrayList<>();
        try{
            Map<String, Object> map = new HashMap<>();
            map.put("orderName",orderName);
            map.put("list", procedureStateList);
            orderProcedureList = orderProcedureMapper.getOrderProcedureByState(map);
            return orderProcedureList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return orderProcedureList;
    }

    @Override
    public List<Integer> getDistinctProcedureStateByOrder(String orderName) {
        List<Integer> procedureStateList = new ArrayList<>();
        try{
            procedureStateList = orderProcedureMapper.getDistinctProcedureStateByOrder(orderName);
            return procedureStateList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return procedureStateList;
    }

    @Override
    public List<OrderProcedure> getOrderProcedureByOrderProcedureSection(String orderName, String procedureSection) {
        List<OrderProcedure> orderProcedureList = new ArrayList<>();
        try{
            orderProcedureList = orderProcedureMapper.getOrderProcedureByOrderProcedureSection(orderName, procedureSection);
            return orderProcedureList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return orderProcedureList;
    }

    @Override
    public List<OrderProcedure> getOrderProcedureByOrderNames(List<String> orderNameList, Integer procedureState) {
        List<OrderProcedure> orderProcedureList = new ArrayList<>();
        try{
            if (orderNameList != null && !orderNameList.isEmpty()){
                orderProcedureList = orderProcedureMapper.getOrderProcedureByOrderNames(orderNameList, procedureState);
            }
            return orderProcedureList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return orderProcedureList;
    }

    @Override
    public Integer changeScanPartOfProcedure(String orderName, Integer procedureNumber, String scanPart) {
        try{
            orderProcedureMapper.changeScanPartOfProcedure(orderName, procedureNumber, scanPart);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Double getSectionPriceByOrder(String orderName, String procedureSection) {
        Double sectionPrice = 0d;
        try{
            if (orderProcedureMapper.getSectionPriceByOrder(orderName, procedureSection) != null){
                sectionPrice = orderProcedureMapper.getSectionPriceByOrder(orderName, procedureSection);
            }
            return sectionPrice;
        }catch (Exception e){
            e.printStackTrace();
        }
        return sectionPrice;
    }

    @Override
    public Integer getPendingOrderProcedureCount(Integer procedureState) {
        Integer pendingCount = 0;
        try{
            if (orderProcedureMapper.getPendingOrderProcedureCount(procedureState) != null){
                pendingCount = orderProcedureMapper.getPendingOrderProcedureCount(procedureState);
            }
            return pendingCount;
        } catch (Exception e){
            e.printStackTrace();
        }
        return pendingCount;
    }

    @Override
    public OrderProcedure getOrderProcedureByOrderNameProcedureNumber(String orderName, Integer procedureNumber) {
        return orderProcedureMapper.getOrderProcedureByOrderNameProcedureNumber(orderName, procedureNumber);
    }

    @Override
    public List<OrderProcedure> getProduresByOrderName(String orderName) {
        return orderProcedureMapper.getSewOrderProcedureByOrder(orderName);
    }

    @Override
    public List<OrderProcedure> getOrderProcedureByMap(Map<String, Object> params) {
        return orderProcedureMapper.getOrderProcedureByMap(params);
    }


}
