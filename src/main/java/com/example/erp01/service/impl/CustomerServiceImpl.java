package com.example.erp01.service.impl;

import com.example.erp01.mapper.CustomerMapper;
import com.example.erp01.model.Customer;
import com.example.erp01.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class CustomerServiceImpl implements CustomerService {


    @Autowired
    private CustomerMapper customerMapper;


    @Override
    public Integer addCustomer(Customer customer) {
        try{
            customerMapper.addCustomer(customer);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer updateCustomer(Customer customer) {
        try{
            customerMapper.updateCustomer(customer);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<Customer> getAllCustomer() {
        return customerMapper.getAllCustomer();
    }

    @Override
    public Integer deleteCustomer(Integer id) {
        try{
            customerMapper.deleteCustomer(id);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public String getMaxCustomerCodeByPreFix(String preFix) {
        String maxOrder = "0000";
        try{
            if (customerMapper.getMaxCustomerCodeByPreFix(preFix) != null){
                maxOrder = customerMapper.getMaxCustomerCodeByPreFix(preFix);
            }
            return maxOrder;
        }catch (Exception e){
            e.printStackTrace();
        }
        return maxOrder;
    }

    @Override
    public List<Customer> getCustomerHintPage(String keyWord, int page, int limit) {
        return customerMapper.getCustomerHintPage(keyWord, page, limit);
    }

    @Override
    public Integer getCustomerCount(String keyWord) {
        int count = 0;
        try{
            if (customerMapper.getCustomerCount(keyWord) != null){
                count = customerMapper.getCustomerCount(keyWord);
            }
            return count;
        }catch (Exception e){
            e.printStackTrace();
        }
        return count;
    }
}
