package com.example.erp01.service.impl;

import com.example.erp01.mapper.ProcedureLevelMapper;
import com.example.erp01.model.ProcedureLevel;
import com.example.erp01.service.ProcedureLevelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class ProcedureLevelServiceImpl implements ProcedureLevelService {
    @Autowired
    private ProcedureLevelMapper procedureLevelMapper;

    @Override
    public List<ProcedureLevel> getAllProcedureLevel() {
        List<ProcedureLevel> procedureLevelList = new ArrayList<>();
        try{
            procedureLevelList = procedureLevelMapper.getAllProcedureLevel();
            return procedureLevelList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return procedureLevelList;
    }

    @Override
    public int addProcedureLevel(ProcedureLevel procedureLevel) {
        try{
            procedureLevelMapper.addProcedureLevel(procedureLevel);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public int deleteProcedureLevel(Integer levelID) {
        try{
            procedureLevelMapper.deleteProcedureLevel(levelID);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public int updateProcedureLevel(ProcedureLevel procedureLevel) {
        try{
            procedureLevelMapper.updateProcedureLevel(procedureLevel);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public Float getLevelValueByName(String level) {
        Float levelValue = 0f;
        try{
            levelValue = procedureLevelMapper.getLevelValueByName(level);
            if (levelValue == null){
                levelValue = 0f;
            }
            return levelValue;
        }catch (Exception e){
            e.printStackTrace();
        }
        return levelValue;
    }
}
