package com.example.erp01.service.impl;

import com.example.erp01.mapper.InvoicingFabricMapper;
import com.example.erp01.model.FabricDetail;
import com.example.erp01.model.FabricOutRecord;
import com.example.erp01.model.FabricStorage;
import com.example.erp01.service.InvoicingFabricService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
@Service
public class InvoicingServiceImpl implements InvoicingFabricService {

    @Autowired
    private InvoicingFabricMapper invoicingFabricMapper;

    @Override
    public List<Integer> getFabricOperateIdList(Map<String, Object> map) {
        List<Integer> fabricIDList = invoicingFabricMapper.getFabricDetailIdList(map);
        List<Integer> fabricIDList1 = invoicingFabricMapper.getFabricOutRecordIdList(map);
        fabricIDList.addAll(fabricIDList1);
        return fabricIDList;
    }

    @Override
    public List<FabricDetail> getFabricDetailByIdList(List<Integer> fabricIDList) {
        List<FabricDetail> fabricDetailList = new ArrayList<>();
        try{
            if (fabricIDList != null && !fabricIDList.isEmpty()){
                fabricDetailList = invoicingFabricMapper.getFabricDetailByIdList(fabricIDList);
            }
            return fabricDetailList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return fabricDetailList;
    }

    @Override
    public List<FabricStorage> getFabricStorageByIdList(List<Integer> fabricIDList) {
        List<FabricStorage> fabricStorageList = new ArrayList<>();
        try{
            if (fabricIDList != null && !fabricIDList.isEmpty()){
                fabricStorageList = invoicingFabricMapper.getFabricStorageByIdList(fabricIDList);
            }
            return fabricStorageList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return fabricStorageList;
    }

    @Override
    public List<FabricOutRecord> getFabricOutRecordByIdList(List<Integer> fabricIDList) {
        List<FabricOutRecord> fabricOutRecordList = new ArrayList<>();
        try{
            if (fabricIDList != null && !fabricIDList.isEmpty()){
                fabricOutRecordList = invoicingFabricMapper.getFabricOutRecordByIdList(fabricIDList);
            }
            return fabricOutRecordList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return fabricOutRecordList;
    }

    @Override
    public List<FabricDetail> getFabricDetailByMap(Map<String, Object> map) {
        return invoicingFabricMapper.getFabricDetailByMap(map);
    }

    @Override
    public List<FabricStorage> getFabricStorageByMap(Map<String, Object> map) {
        return invoicingFabricMapper.getFabricStorageByMap(map);
    }

    @Override
    public List<FabricOutRecord> getFabricOutRecordByMap(Map<String, Object> map) {
        return invoicingFabricMapper.getFabricOutRecordByMap(map);
    }
}
