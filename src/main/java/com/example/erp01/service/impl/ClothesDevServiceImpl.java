package com.example.erp01.service.impl;

import com.example.erp01.mapper.*;
import com.example.erp01.model.*;
import com.example.erp01.service.ClothesDevService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class ClothesDevServiceImpl implements ClothesDevService {

    @Autowired
    private ClothesDevInfoMapper clothesDevInfoMapper;
    @Autowired
    private ClothesDevColorMapper clothesDevColorMapper;
    @Autowired
    private ClothesDevSizeMapper clothesDevSizeMapper;
    @Autowired
    private ClothesDevFabricMapper clothesDevFabricMapper;
    @Autowired
    private ClothesDevAccessoryMapper clothesDevAccessoryMapper;
    @Autowired
    private ClothesDevImageMapper clothesDevImageMapper;
    @Autowired
    private ClothesDevMeasureMapper clothesDevMeasureMapper;
    @Autowired
    private ClothesDevProcessMapper clothesDevProcessMapper;

    @Override
    public Integer addClothesDevInfo(ClothesDevInfo clothesDevInfo) {
        try{
            clothesDevInfoMapper.addClothesDevInfo(clothesDevInfo);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer updateClothesDevInfo(ClothesDevInfo clothesDevInfo) {
        try{
            clothesDevInfoMapper.updateClothesDevInfo(clothesDevInfo);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public ClothesDevInfo getClothesDevInfoById(Integer id) {
        return clothesDevInfoMapper.getClothesDevInfoById(id);
    }

    @Override
    public List<ClothesDevInfo> getClothesDevInfoByMap(Map<String, Object> map) {
        return clothesDevInfoMapper.getClothesDevInfoByMap(map);
    }

    @Override
    public String getMaxClothesVersionOrder(String clothesPreFix) {
        String maxOrder = "000";
        try{
            if (clothesDevInfoMapper.getMaxClothesVersionOrder(clothesPreFix) != null){
                maxOrder = clothesDevInfoMapper.getMaxClothesVersionOrder(clothesPreFix);
            }
            return maxOrder;
        }catch (Exception e){
            e.printStackTrace();
        }
        return maxOrder;
    }

    @Override
    public String getClothesDevOrderNameByPreFix(String preFix) {
        String maxOrder = "0000";
        try{
            if (clothesDevInfoMapper.getClothesDevOrderNameByPreFix(preFix) != null){
                maxOrder = clothesDevInfoMapper.getClothesDevOrderNameByPreFix(preFix);
            }
            return maxOrder;
        }catch (Exception e){
            e.printStackTrace();
        }
        return maxOrder;
    }

    @Override
    public ClothesDevInfo getOneClothesDevInfoByMap(Map<String, Object> map) {
        return clothesDevInfoMapper.getOneClothesDevInfoByMap(map);
    }

    @Override
    public Integer updateClothesDevInfoByDevNumber(ClothesDevInfo clothesDevInfo) {
        try{
            clothesDevInfoMapper.updateClothesDevInfoByDevNumber(clothesDevInfo);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer deleteClothesDevInfoByDevNumber(String clothesDevNumber) {
        try{
            clothesDevInfoMapper.deleteClothesDevInfoByDevNumber(clothesDevNumber);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer addClothesDevColorBatch(List<ClothesDevColor> clothesDevColorList) {
        try{
            if (clothesDevColorList != null && !clothesDevColorList.isEmpty()){
                clothesDevColorMapper.addClothesDevColorBatch(clothesDevColorList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<ClothesDevColor> getClothesDevColorByMap(Map<String, Object> map) {
        return clothesDevColorMapper.getClothesDevColorByMap(map);
    }

    @Override
    public Integer deleteClothesDevColorByDevId(Integer devId) {
        try{
            clothesDevColorMapper.deleteClothesDevColorByDevId(devId);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer deleteClothesDevColorByDevNumber(String clothesDevNumber) {
        try{
            clothesDevColorMapper.deleteClothesDevColorByDevNumber(clothesDevNumber);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer addClothesDevSizeBatch(List<ClothesDevSize> clothesDevSizeList) {
        try{
            if (clothesDevSizeList != null && !clothesDevSizeList.isEmpty()){
                clothesDevSizeMapper.addClothesDevSizeBatch(clothesDevSizeList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<ClothesDevSize> getClothesDevSizeByMap(Map<String, Object> map) {
        return clothesDevSizeMapper.getClothesDevSizeByMap(map);
    }

    @Override
    public Integer deleteClothesDevSizeByDevId(Integer devId) {
        return clothesDevSizeMapper.deleteClothesDevSizeByDevId(devId);
    }

    @Override
    public Integer deleteClothesDevSizeByDevNumber(String clothesDevNumber) {
        try{
            clothesDevSizeMapper.deleteClothesDevSizeByDevNumber(clothesDevNumber);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer addClothesDevFabricBatch(List<ClothesDevFabric> clothesDevFabricList) {
        try{
            clothesDevFabricMapper.addClothesDevFabricBatch(clothesDevFabricList);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer deleteClothesDevFabricBatch(List<Integer> idList) {
        try{
            if (idList != null && !idList.isEmpty()){
                clothesDevFabricMapper.deleteClothesDevFabricBatch(idList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<ClothesDevFabric> getClothesDevFabricByMap(Map<String, Object> map) {
        return clothesDevFabricMapper.getClothesDevFabricByMap(map);
    }

    @Override
    public Integer updateClothesDevFabricBatch(List<ClothesDevFabric> clothesDevFabricList) {
        try{
            if (clothesDevFabricList != null && !clothesDevFabricList.isEmpty()){
                clothesDevFabricMapper.updateClothesDevFabricBatch(clothesDevFabricList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer deleteClothesDevFabricByDevNumber(String clothesDevNumber) {
        try{
            clothesDevFabricMapper.deleteClothesDevFabricByDevNumber(clothesDevNumber);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<ClothesDevFabric> getUniqueClothesDevFabricByDevNumber(String clothesDevNumber) {
        return clothesDevFabricMapper.getUniqueClothesDevFabricByDevNumber(clothesDevNumber);
    }

    @Override
    public Integer addClothesDevAccessoryBatch(List<ClothesDevAccessory> clothesDevAccessoryList) {
        try{
            if (clothesDevAccessoryList != null && !clothesDevAccessoryList.isEmpty()){
                clothesDevAccessoryMapper.addClothesDevAccessoryBatch(clothesDevAccessoryList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer deleteClothesDevAccessoryBatch(List<Integer> idList) {
        try{
            if (idList != null && !idList.isEmpty()){
                clothesDevAccessoryMapper.deleteClothesDevAccessoryBatch(idList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<ClothesDevAccessory> getClothesDevAccessoryByMap(Map<String, Object> map) {
        return clothesDevAccessoryMapper.getClothesDevAccessoryByMap(map);
    }

    @Override
    public Integer updateClothesDevAccessoryBatch(List<ClothesDevAccessory> clothesDevAccessoryList) {
        try{
            if (clothesDevAccessoryList != null && !clothesDevAccessoryList.isEmpty()){
                clothesDevAccessoryMapper.updateClothesDevAccessoryBatch(clothesDevAccessoryList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<ClothesDevAccessory> getClothesDevAccessoryHint(String subAccessoryName, Integer page, Integer limit) {
        return clothesDevAccessoryMapper.getClothesDevAccessoryHint(subAccessoryName, page, limit);
    }

    @Override
    public Integer deleteClothesDevAccessoryByDevNumber(String clothesDevNumber) {
        try{
            clothesDevAccessoryMapper.deleteClothesDevAccessoryByDevNumber(clothesDevNumber);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer addClothesDevImage(ClothesDevImage clothesDevImage) {
        try{
            clothesDevImageMapper.addClothesDevImage(clothesDevImage);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer deleteClothesDevImageByDevId(Integer devId) {
        try{
            clothesDevImageMapper.deleteClothesDevImageByDevId(devId);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer updateClothesDevImage(ClothesDevImage clothesDevImage) {
        try{
            clothesDevImageMapper.updateClothesDevImage(clothesDevImage);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public ClothesDevImage getClothesDevImageByMap(Map<String, Object> map) {
        return clothesDevImageMapper.getClothesDevImageByMap(map);
    }

    @Override
    public Integer updateClothesDevImageByDevNumber(ClothesDevImage clothesDevImage) {
        try{
            clothesDevImageMapper.updateClothesDevImageByDevNumber(clothesDevImage);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer deleteClothesDevImageByDevNumber(String clothesDevNumber) {
        try{
            clothesDevImageMapper.deleteClothesDevImageByDevNumber(clothesDevNumber);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer addClothesDevProcess(ClothesDevProcess clothesDevProcess) {
        try{
            clothesDevProcessMapper.addClothesDevProcess(clothesDevProcess);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer updateClothesDevProcess(ClothesDevProcess clothesDevProcess) {
        try{
            clothesDevProcessMapper.updateClothesDevProcess(clothesDevProcess);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer deleteClothesDevProcessByDevId(Integer devId) {
        try{
            clothesDevProcessMapper.deleteClothesDevProcessByDevId(devId);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public ClothesDevProcess getClothesDevProcessByMap(Map<String, Object> map) {
        return clothesDevProcessMapper.getClothesDevProcessByMap(map);
    }

    @Override
    public Integer updateClothesDevProcessByDevNumber(ClothesDevProcess clothesDevProcess) {
        try{
            clothesDevProcessMapper.updateClothesDevProcessByDevNumber(clothesDevProcess);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer deleteClothesDevProcessByDevNumber(String clothesDevNumber) {
        try{
            clothesDevProcessMapper.deleteClothesDevProcessByDevNumber(clothesDevNumber);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer addClothesDevMeasure(ClothesDevMeasure clothesDevMeasure) {
        try{
            clothesDevMeasureMapper.addClothesDevMeasure(clothesDevMeasure);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer updateClothesDevMeasure(ClothesDevMeasure clothesDevMeasure) {
        try{
            clothesDevMeasureMapper.updateClothesDevMeasure(clothesDevMeasure);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer deleteClothesDevMeasureByDevId(Integer devId) {
        try{
            clothesDevMeasureMapper.deleteClothesDevMeasureByDevId(devId);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public ClothesDevMeasure getClothesDevMeasureByMap(Map<String, Object> map) {
        return clothesDevMeasureMapper.getClothesDevMeasureByMap(map);
    }

    @Override
    public Integer updateClothesDevMeasureByDevNumber(ClothesDevMeasure clothesDevMeasure) {
        try{
            clothesDevMeasureMapper.updateClothesDevMeasureByDevNumber(clothesDevMeasure);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer deleteClothesDevMeasureByDevNumber(String clothesDevNumber) {
        try{
            clothesDevMeasureMapper.deleteClothesDevMeasureByDevNumber(clothesDevNumber);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }
}
