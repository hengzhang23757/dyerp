package com.example.erp01.service.impl;

import com.example.erp01.mapper.RecoveryMapper;
import com.example.erp01.model.Tailor;
import com.example.erp01.service.RecoveryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RecoveryServiceImpl implements RecoveryService {

    @Autowired
    private RecoveryMapper recoveryMapper;

    @Override
    public List<Integer> getDeletedBedNumber(String orderName) {
        List<Integer> bedNumberList = new ArrayList<>();
        try{
            bedNumberList = recoveryMapper.getDeletedBedNumber(orderName);
            return bedNumberList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return bedNumberList;
    }

    @Override
    public List<Tailor> getDeletedTailorByInfo(String orderName, Integer bedNumber) {
        List<Tailor> tailorList = new ArrayList<>();
        try{
            tailorList = recoveryMapper.getDeletedTailorByInfo(orderName, bedNumber);
            return tailorList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return tailorList;
    }

    @Override
    public List<Tailor> getNormalTailorByInfo(String orderName, Integer bedNumber) {
        List<Tailor> tailorList = new ArrayList<>();
        try{
            tailorList = recoveryMapper.getNormalTailorByInfo(orderName, bedNumber);
            return tailorList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return tailorList;
    }

    @Override
    public Integer recoverTailorByInfo(String orderName, List<Integer> tailorIDList) {
        try{
            recoveryMapper.recoverTailorByInfo(orderName, tailorIDList);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<Tailor> getSelectedTailorByID(String orderName, List<Integer> tailorIDList) {
        List<Tailor> tailorList = new ArrayList<>();
        try{
            tailorList = recoveryMapper.getSelectedTailorByID(orderName, tailorIDList);
            return tailorList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return tailorList;
    }
}
