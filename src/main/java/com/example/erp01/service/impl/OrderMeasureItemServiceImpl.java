package com.example.erp01.service.impl;

import com.example.erp01.mapper.OrderMeasureItemMapper;
import com.example.erp01.model.OrderMeasureItem;
import com.example.erp01.service.OrderMeasureItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderMeasureItemServiceImpl implements OrderMeasureItemService {

    @Autowired
    private OrderMeasureItemMapper orderMeasureItemMapper;

    @Override
    public Integer addOrderMeasureItemBatch(List<OrderMeasureItem> orderMeasureItemList) {
        try{
            if (orderMeasureItemList != null && !orderMeasureItemList.isEmpty()){
                orderMeasureItemMapper.addOrderMeasureItemBatch(orderMeasureItemList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<OrderMeasureItem> getOrderMeasureItemByOrder(String orderName) {
        return orderMeasureItemMapper.getOrderMeasureItemByOrder(orderName);
    }

    @Override
    public Integer deleteOrderMeasureItemByOrder(String orderName) {
        try{
            orderMeasureItemMapper.deleteOrderMeasureItemByOrder(orderName);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<String> getUniquePartCodeListByOrder(String orderName) {
        return orderMeasureItemMapper.getUniquePartCodeListByOrder(orderName);
    }
}
