package com.example.erp01.service.impl;

import com.example.erp01.mapper.MaxTailorQcodeIDMapper;
import com.example.erp01.service.MaxTailorQcodeIDService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by hujian on 2019/6/16
 */
@Service
public class MaxTailorQcodeIDServiceImpl implements MaxTailorQcodeIDService {

    @Resource
    private MaxTailorQcodeIDMapper maxTailorQcodeIDMapper;

    public int getMaxTailorQcodeId() {
        return maxTailorQcodeIDMapper.getMaxTailorQcodeId();
    }

    public int updateMaxTailorQcodeId(int qCodeId) {
        return maxTailorQcodeIDMapper.updateMaxTailorQcodeId(qCodeId);
    }

}
