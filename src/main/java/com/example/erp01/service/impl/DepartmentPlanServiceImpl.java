package com.example.erp01.service.impl;

import com.example.erp01.mapper.DepartmentPlanMapper;
import com.example.erp01.model.CutBedColor;
import com.example.erp01.model.DepartmentPlan;
import com.example.erp01.model.LooseFabric;
import com.example.erp01.service.DepartmentPlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class DepartmentPlanServiceImpl implements DepartmentPlanService {

    @Autowired
    private DepartmentPlanMapper departmentPlanMapper;

    @Override
    public Integer addDepartmentPlan(DepartmentPlan departmentPlan) {
        try{
            departmentPlanMapper.addDepartmentPlan(departmentPlan);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer deleteDepartmentPlan(Integer departmentPlanID) {
        try{
            departmentPlanMapper.deleteDepartmentPlan(departmentPlanID);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer updateDepartmentPlan(DepartmentPlan departmentPlan) {
        try{
            departmentPlanMapper.updateDepartmentPlan(departmentPlan);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<DepartmentPlan> getDepartmentPlanOneMonth() {
        List<DepartmentPlan> departmentPlanList = new ArrayList<>();
        try{
            departmentPlanList = departmentPlanMapper.getDepartmentPlanOneMonth();
            return departmentPlanList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return departmentPlanList;
    }

    @Override
    public List<DepartmentPlan> getTailorPlanDetail(String orderName, Date beginDate) {
        List<DepartmentPlan> departmentPlanList = new ArrayList<>();
        try{
            departmentPlanList = departmentPlanMapper.getTailorPlanDetail(orderName, beginDate);
            return departmentPlanList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return departmentPlanList;
    }

    @Override
    public List<DepartmentPlan> getLoosePlanDetail(String orderName, Date beginDate) {
        List<DepartmentPlan> departmentPlanList = new ArrayList<>();
        try{
            departmentPlanList = departmentPlanMapper.getLoosePlanDetail(orderName, beginDate);
            return departmentPlanList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return departmentPlanList;
    }

    @Override
    public List<String> getTailorOrderNameOfToday() {
        List<String> orderNameList = new ArrayList<>();
        try{
            orderNameList = departmentPlanMapper.getTailorOrderNameOfToday();
            return orderNameList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return orderNameList;
    }

    @Override
    public List<String> getLooseOrderNameOfToday() {
        List<String> orderNameList = new ArrayList<>();
        try{
            orderNameList = departmentPlanMapper.getLooseOrderNameOfToday();
            return orderNameList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return orderNameList;
    }

    @Override
    public List<DepartmentPlan> getDepartmentPlanBetweenToday(String planType, String orderName) {
        List<DepartmentPlan> departmentPlanList = new ArrayList<>();
        try{
            departmentPlanList = departmentPlanMapper.getDepartmentPlanBetweenToday(planType, orderName);
            return departmentPlanList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return departmentPlanList;
    }

    @Override
    public Integer getWellCountByOrderDate(String orderName, Date beginDate) {
        Integer wellCount = 0;
        try{
            if (departmentPlanMapper.getWellCountByOrderDate(orderName, beginDate) != null){
                wellCount = departmentPlanMapper.getWellCountByOrderDate(orderName, beginDate);
            }
            return wellCount;
        }catch (Exception e){
            e.printStackTrace();
        }
        return wellCount;
    }

    @Override
    public Integer getLooseCountByOrderDate(String orderName, Date beginDate) {
        Integer looseCount = 0;
        try{
            if (departmentPlanMapper.getLooseCountByOrderDate(orderName, beginDate) != null){
                looseCount = departmentPlanMapper.getLooseCountByOrderDate(orderName, beginDate);
            }
            return looseCount;
        }catch (Exception e){
            e.printStackTrace();
        }
        return looseCount;
    }

    @Override
    public List<DepartmentPlan> getDepartmentPlanByFinishState(String finishState, String orderName, String colorName) {
        List<DepartmentPlan> departmentPlanList = new ArrayList<>();
        try{
            departmentPlanList = departmentPlanMapper.getDepartmentPlanByFinishState(finishState, orderName, colorName);
            return departmentPlanList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return departmentPlanList;
    }

    @Override
    public Integer getMaxPlanOrder() {
        Integer planOrder = 1;
        try{
            if (departmentPlanMapper.getMaxPlanOrder() != null){
                planOrder = departmentPlanMapper.getMaxPlanOrder();
            }
            return planOrder;
        }catch (Exception e){
            e.printStackTrace();
        }
        return planOrder;
    }

    @Override
    public Integer updateDepartmentPlanBatch(List<DepartmentPlan> departmentPlanList) {
        try{
            if (departmentPlanList != null && !departmentPlanList.isEmpty()){
                departmentPlanMapper.updateDepartmentPlanBatch(departmentPlanList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer setDepartmentPlanFinish(Integer departmentPlanID) {
        try{
            departmentPlanMapper.setDepartmentPlanFinish(departmentPlanID);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }
}
