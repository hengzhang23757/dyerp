package com.example.erp01.service.impl;

import com.example.erp01.mapper.HomePageMapper;
import com.example.erp01.model.HomePage;
import com.example.erp01.service.HomePageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class HomePageServiceImpl implements HomePageService {

    @Autowired
    private HomePageMapper homePageMapper;

    @Override
    public List<HomePage> getUnFinishOrder() {
        List<HomePage> homePageList = new ArrayList<>();
        try{
            homePageList = homePageMapper.getUnFinishOrder();
            return homePageList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return homePageList;
    }

    @Override
    public Integer addHomePageDataBatch(List<HomePage> homePageList) {
        try{
            if (homePageList != null && !homePageList.isEmpty()){
                homePageMapper.addHomePageDataBatch(homePageList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer updateHomePageData(List<HomePage> homePageList) {
        try{
            if (homePageList != null && !homePageList.isEmpty()){
                homePageMapper.updateHomePageData(homePageList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<HomePage> getHomePageRecentThreeMonth() {
        List<HomePage> homePageList = new ArrayList<>();
        try {
            homePageList = homePageMapper.getHomePageRecentThreeMonth();
            return homePageList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return homePageList;
    }

    @Override
    public Integer deleteHomePageDataByOrderName(String orderName) {
        try{
            homePageMapper.deleteHomePageDataByOrderName(orderName);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }
}
