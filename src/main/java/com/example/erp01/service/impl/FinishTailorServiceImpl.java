package com.example.erp01.service.impl;

import com.example.erp01.mapper.FinishTailorMapper;
import com.example.erp01.model.CutQueryLeak;
import com.example.erp01.model.FinishTailor;
import com.example.erp01.service.FinishTailorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class FinishTailorServiceImpl implements FinishTailorService {

    @Autowired
    private FinishTailorMapper finishTailorMapper;
    @Override
    public List<FinishTailor> generateTailorData(String jsonStr) {
        List<FinishTailor> tailorList = null;
        try{
            tailorList = finishTailorMapper.generateTailorData(jsonStr);
            return tailorList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return tailorList;
    }

    @Override
    public int saveTailorData(List<FinishTailor> tailorList) {
        try{
            finishTailorMapper.saveTailorData(tailorList);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer getMaxPackageNumberByOrder(String orderName) {
        Integer maxPackageNumber = null;
        try{
            maxPackageNumber = finishTailorMapper.getMaxPackageNumberByOrder(orderName);
            return maxPackageNumber;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<String> getTailorQcodeByTailorQcodeID(List<Integer> tailorQcodeIDList) {
        List<String> tailorQcodeList = new ArrayList<>();
        try{
            tailorQcodeList = finishTailorMapper.getTailorQcodeByTailorQcodeID(tailorQcodeIDList);
            return tailorQcodeList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String getOneTailorQcodeByTailorQcodeID(Integer tailorQcodeID) {
        String tailorQcode = null;
        try{
            tailorQcode = finishTailorMapper.getOneTailorQcodeByTailorQcodeID(tailorQcodeID);
            return tailorQcode;
        }catch (Exception e){
            e.printStackTrace();
        }
        return tailorQcode;
    }

    @Override
    public int deleteTailorByOrderBedPack(String orderName, List<Integer> packageNumberList) {
        try{
            Map<String, Object> map = new HashMap<>();
            map.put("orderName",orderName);
            map.put("list",packageNumberList);
            finishTailorMapper.deleteTailorByOrderBedPack(map);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<FinishTailor> getFinishTailorByOrderColorSizePart(String orderName, String colorName, String sizeName, String partName) {
        List<FinishTailor> finishTailorList = new ArrayList<>();
        try{
            finishTailorList = finishTailorMapper.getFinishTailorByOrderColorSizePart(orderName, colorName, sizeName, partName);
            return finishTailorList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return finishTailorList;
    }

    @Override
    public List<CutQueryLeak> getFinishTailorInfo(String orderName, String partName) {
        List<CutQueryLeak> cutQueryLeakList = new ArrayList<>();
        try{
            cutQueryLeakList = finishTailorMapper.getFinishTailorInfo(orderName, partName);
            return cutQueryLeakList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return cutQueryLeakList;
    }

    @Override
    public Integer getFinishTailorCountByOrderColorSize(String orderName, String colorName, String sizeName, String partName) {
        Integer finishTailorCount = 0;
        try{
            if (finishTailorMapper.getFinishTailorCountByOrderColorSize(orderName, colorName, sizeName, partName) != null){
                finishTailorCount = finishTailorMapper.getFinishTailorCountByOrderColorSize(orderName, colorName, sizeName, partName);
            }
            return finishTailorCount;
        }catch (Exception e){
            e.printStackTrace();
        }
        return finishTailorCount;
    }

    @Override
    public Integer updateTailorData(List<FinishTailor> finishTailorList) {
        try{
            if (finishTailorList != null && !finishTailorList.isEmpty()){
                finishTailorMapper.updateTailorData(finishTailorList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<Integer> getTailorQcodeIDByOrderColorSize(String orderName, String colorName, String sizeName) {
        List<Integer> tailorQcodeIDList = new ArrayList<>();
        try{
            tailorQcodeIDList = finishTailorMapper.getTailorQcodeIDByOrderColorSize(orderName, colorName, sizeName);
            return tailorQcodeIDList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return tailorQcodeIDList;
    }

    @Override
    public FinishTailor getFinishTailorByTailorQcodeID(Integer tailorQcodeID) {
        FinishTailor finishTailor = null;
        try{
            if (finishTailorMapper.getFinishTailorByTailorQcodeID(tailorQcodeID) != null){
                finishTailor = finishTailorMapper.getFinishTailorByTailorQcodeID(tailorQcodeID);
            }
            return finishTailor;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<FinishTailor> getFinishTailorByOrderPartNameColorSize(String orderName, String partName, String colorName, String sizeName) {
        List<FinishTailor> finishTailorList = new ArrayList<>();
        try{
            finishTailorList = finishTailorMapper.getFinishTailorByOrderPartNameColorSize(orderName, partName, colorName, sizeName);
            return finishTailorList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return finishTailorList;
    }

    @Override
    public Integer updateFinishTailorBatchInCheck(List<FinishTailor> finishTailorList) {
        try{
            if (finishTailorList != null && !finishTailorList.isEmpty()){
                finishTailorMapper.updateFinishTailorBatchInCheck(finishTailorList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<FinishTailor> getShelfStorageByShelfName(String shelfName) {
        return finishTailorMapper.getShelfStorageByShelfName(shelfName);
    }

    @Override
    public Integer updateFinishTailorBatchOutCheck(List<FinishTailor> finishTailorList) {
        try{
            if (finishTailorList != null && !finishTailorList.isEmpty()){
                finishTailorMapper.updateFinishTailorBatchOutCheck(finishTailorList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer updateFinishTailorBatchReturnCheck(List<FinishTailor> finishTailorList) {
        try{
            if (finishTailorList != null && !finishTailorList.isEmpty()){
                finishTailorMapper.updateFinishTailorBatchReturnCheck(finishTailorList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<FinishTailor> getFinishTailorByInfo(Map<String, Object> map) {
        return finishTailorMapper.getFinishTailorByInfo(map);
    }

    @Override
    public Integer updateFinishTailorLayerCountByInfo(String orderName, Integer bedNumber, Integer packageNumber, Integer layerCount) {
        try{
            finishTailorMapper.updateFinishTailorLayerCountByInfo(orderName, bedNumber, packageNumber, layerCount);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }
}
