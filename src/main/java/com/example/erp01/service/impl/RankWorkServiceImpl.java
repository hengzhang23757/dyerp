package com.example.erp01.service.impl;

import com.example.erp01.mapper.RankWorkMapper;
import com.example.erp01.model.GeneralSalary;
import com.example.erp01.service.RankWorkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
@Service
public class RankWorkServiceImpl implements RankWorkService {
    @Autowired
    private RankWorkMapper rankWorkMapper;

    @Override
    public List<GeneralSalary> getGroupRank(String groupName) {
        List<GeneralSalary> generalSalaryList = new ArrayList<>();
        try{
            generalSalaryList = rankWorkMapper.getGroupRank(groupName);
            return generalSalaryList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return generalSalaryList;
    }
}
