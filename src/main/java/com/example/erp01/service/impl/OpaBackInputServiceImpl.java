package com.example.erp01.service.impl;

import com.example.erp01.mapper.OpaBackInputMapper;
import com.example.erp01.model.OpaBackInput;
import com.example.erp01.service.OpaBackInputService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class OpaBackInputServiceImpl implements OpaBackInputService {

    @Autowired
    private OpaBackInputMapper opaBackInputMapper;


    @Override
    public int addOpaBackInput(OpaBackInput opaBackInput) {
        try{
            opaBackInputMapper.addOpaBackInput(opaBackInput);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<OpaBackInput> getAllOpaBackInput() {
        List<OpaBackInput> opaBackInputList = new ArrayList<>();
        try{
            opaBackInputList = opaBackInputMapper.getAllOpaBackInput();
            return opaBackInputList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return opaBackInputList;
    }

    @Override
    public int deleteOpaBackInput(Integer opaBackInputID) {
        try{
            opaBackInputMapper.deleteOpaBackInput(opaBackInputID);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public int addOpaBackInputBatch(List<OpaBackInput> opaBackInputList) {
        try{
            if (opaBackInputList != null && !opaBackInputList.isEmpty()){
                opaBackInputMapper.addOpaBackInputBatch(opaBackInputList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public int updateOpaBackInput(OpaBackInput opaBackInput) {
        try{
            opaBackInputMapper.updateOpaBackInput(opaBackInput);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<OpaBackInput> getTodayOpaBackInput() {
        List<OpaBackInput> opaBackInputList = new ArrayList<>();
        try{
            opaBackInputList = opaBackInputMapper.getTodayOpaBackInput();
            return opaBackInputList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return opaBackInputList;
    }

    @Override
    public List<OpaBackInput> getOneMonthOpaBackInput() {
        List<OpaBackInput> opaBackInputList = new ArrayList<>();
        try{
            opaBackInputList = opaBackInputMapper.getOneMonthOpaBackInput();
            return opaBackInputList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return opaBackInputList;
    }

    @Override
    public List<OpaBackInput> getThreeMonthOpaBackInput() {
        List<OpaBackInput> opaBackInputList = new ArrayList<>();
        try{
            opaBackInputList = opaBackInputMapper.getThreeMonthOpaBackInput();
            return opaBackInputList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return opaBackInputList;
    }

    @Override
    public List<OpaBackInput> getOpaBackInputByOrder(String orderName) {
        List<OpaBackInput> opaBackInputList = new ArrayList<>();
        try{
            opaBackInputList = opaBackInputMapper.getOpaBackInputByOrder(orderName);
            return opaBackInputList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return opaBackInputList;
    }

    @Override
    public List<OpaBackInput> getBedOpaBackInputByOrder(String orderName) {
        List<OpaBackInput> opaBackInputList = new ArrayList<>();
        try{
            opaBackInputList = opaBackInputMapper.getBedOpaBackInputByOrder(orderName);
            return opaBackInputList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return opaBackInputList;
    }
}
