package com.example.erp01.service.impl;

import com.example.erp01.mapper.CutInStoreMapper;
import com.example.erp01.model.CutInStore;
import com.example.erp01.model.CutLocation;
import com.example.erp01.model.Storage;
import com.example.erp01.service.CutInStoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CutInStoreServiceImpl implements CutInStoreService {

    @Autowired
    private CutInStoreMapper cutInStoreMapper;

    @Override
    public int cutInStore(List<Storage> cutInStoreList) {
        try{
            if(cutInStoreList!=null && !cutInStoreList.isEmpty()){
                cutInStoreMapper.cutInStore(cutInStoreList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public int deleteCutInStore(List<String> tailorQcodeList) {
        try{
            cutInStoreMapper.deleteCutInStore(tailorQcodeList);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

//    @Override
//    public List<String> getCutLocationByColorSize(String orderName, String colorName, String sizeName) {
//        List<String> locationList = new ArrayList<>();
//        try{
//            locationList = cutInStoreMapper.getCutLocationByColorSize(orderName, colorName, sizeName);
//            return locationList;
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//        return locationList;
//    }

    @Override
    public List<String> getCutLocationByPackage(String orderName, Integer bedNumber, Integer packageNumber) {
        List<String> locationList = new ArrayList<>();
        try{
            locationList = cutInStoreMapper.getCutLocationByPackage(orderName, bedNumber, packageNumber);
            return locationList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return locationList;
    }

    @Override
    public List<CutLocation> getCutInfo(String orderName, Integer bedNumber, String colorName, String sizeName, Integer packageNumber) {
        List<CutLocation> cutLocationList = new ArrayList<>();
        try{
            cutLocationList = cutInStoreMapper.getCutInfo(orderName, bedNumber, colorName, sizeName, packageNumber);
            return cutLocationList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return cutLocationList;

    }

    @Override
    public Integer deleteCutInStoreByInfo(String orderName, Integer bedNumber, Integer packageNumber) {
        try{
            cutInStoreMapper.deleteCutInStoreByInfo(orderName, bedNumber, packageNumber);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }
}
