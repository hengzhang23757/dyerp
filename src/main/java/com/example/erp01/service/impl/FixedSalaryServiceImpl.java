package com.example.erp01.service.impl;

import com.example.erp01.mapper.FixedSalaryMapper;
import com.example.erp01.model.FixedSalary;
import com.example.erp01.service.FixedSalaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class FixedSalaryServiceImpl implements FixedSalaryService {

    @Autowired
    private FixedSalaryMapper fixedSalaryMapper;

    @Override
    public Integer addFixedSalary(FixedSalary fixedSalary) {
        try{
            fixedSalaryMapper.addFixedSalary(fixedSalary);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer deleteFixedSalary(Integer fixedID) {
        try{
            fixedSalaryMapper.deleteFixedSalary(fixedID);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<FixedSalary> getAllFixedSalary() {
        List<FixedSalary> fixedSalaryList = new ArrayList<>();
        try{
            fixedSalaryList = fixedSalaryMapper.getAllFixedSalary();
            return fixedSalaryList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return fixedSalaryList;
    }

    @Override
    public List<String> getAllFixedSalaryValue() {
        List<String> fixedSalaryValueList = new ArrayList<>();
        try{
            fixedSalaryValueList = fixedSalaryMapper.getAllFixedSalaryValue();
            return fixedSalaryValueList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return fixedSalaryValueList;
    }
}
