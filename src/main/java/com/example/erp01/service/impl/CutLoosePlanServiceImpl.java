package com.example.erp01.service.impl;

import com.example.erp01.mapper.CutLoosePlanMapper;
import com.example.erp01.model.CutLoosePlan;
import com.example.erp01.service.CutLoosePlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
@Service
public class CutLoosePlanServiceImpl implements CutLoosePlanService {

    @Autowired
    private CutLoosePlanMapper cutLoosePlanMapper;

    @Override
    public Integer addOrUpdateCutLoosePlan(CutLoosePlan cutLoosePlan) {
        try{
            if (cutLoosePlan.getId() != null){
                cutLoosePlanMapper.updateCutLoosePlan(cutLoosePlan);
            } else {
                cutLoosePlanMapper.addCutLoosePlan(cutLoosePlan);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer updateCutLoosePlanBatch(List<CutLoosePlan> cutLoosePlanList) {
        try{
            if (cutLoosePlanList != null && !cutLoosePlanList.isEmpty()){
                cutLoosePlanMapper.updateCutLoosePlanBatch(cutLoosePlanList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer deleteCutLoosePlan(Integer id) {
        try{
            cutLoosePlanMapper.deleteCutLoosePlan(id);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<CutLoosePlan> getCutLoosePlanByFinishState(String finishState) {
        List<CutLoosePlan> cutLoosePlanList = new ArrayList<>();
        try{
            cutLoosePlanList = cutLoosePlanMapper.getCutLoosePlanByFinishState(finishState);
            return cutLoosePlanList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return cutLoosePlanList;
    }


    @Override
    public Integer updateFinishState(Integer id) {
        try{
            cutLoosePlanMapper.updateFinishState(id);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }
}
