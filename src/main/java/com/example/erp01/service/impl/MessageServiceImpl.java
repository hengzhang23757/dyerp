package com.example.erp01.service.impl;

import com.example.erp01.mapper.MessageMapper;
import com.example.erp01.mapper.UserMapper;
import com.example.erp01.model.Message;
import com.example.erp01.model.User;
import com.example.erp01.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.jws.soap.SOAPBinding;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class MessageServiceImpl implements MessageService {

    @Autowired
    private MessageMapper messageMapper;

    @Autowired
    private UserMapper userMapper;

    @Override
    public Integer addMessageBatch(List<Message> messageList) {
        try{
            if (messageList != null && !messageList.isEmpty()){
                messageMapper.addMessageBatch(messageList);
            }
            return 0;
        } catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<Message> getMessageByInfo(String receiveUser, String messageType) {
        List<Message> messageList = new ArrayList<>();
        try{
            messageList = messageMapper.getMessageByInfo(receiveUser, messageType);
            return messageList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return messageList;
    }

    @Override
    public Integer updateMessageReadType(List<Integer> idList) {
        try{
            if (idList != null && !idList.isEmpty()){
                messageMapper.updateMessageReadType(idList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer getReadStateCountByMessageType(String receiveUser, String messageType) {
        Integer count = 0;
        try{
            if (messageMapper.getReadStateCountByMessageType(receiveUser, messageType) != null){
                count = messageMapper.getReadStateCountByMessageType(receiveUser, messageType);
            }
            return count;
        } catch (Exception e){
            e.printStackTrace();
        }
        return count;
    }

    @Override
    public List<Message> getMessageCountGroupByType(String receiveUser) {
        List<Message> messageList = new ArrayList<>();
        try{
            messageList = messageMapper.getMessageCountGroupByType(receiveUser);
            return messageList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return messageList;
    }

    @Override
    public Message getMessageById(Integer id) {
        Message message = null;
        try{
            message = messageMapper.getMessageById(id);
            return message;
        }catch (Exception e){
            e.printStackTrace();
        }
        return message;
    }

    @Override
    public Integer updateMessageReadTypeByReceiveUser(String receiveUser, String messageType) {
        try{
            messageMapper.updateMessageReadTypeByReceiveUser(receiveUser, messageType);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<Message> getSendedMessage(String sendUser) {
        List<Message> messageList = messageMapper.getSendedMessage(sendUser);
        return messageList;
    }

    @Override
    public Integer countBySender(String sendUser) {
        return messageMapper.countBySender(sendUser);
    }

    @Override
    public List<Message> getSendedMessageByPage(String sendUser, Integer page, Integer limit) {
        List<Message> messageList = messageMapper.getSendedMessageByPage(sendUser,(page-1)*limit,limit);
        return messageList;
    }

    @Override
    public Integer autoSendMessageToDepartment(String department, Message message) {
//        List<User> userList = userMapper.getUserByDepartment(department);
        try{
            List<Message> messageList = new ArrayList<>();
            Message tmpMessage = new Message();
            tmpMessage.setMessageType(message.getMessageType());
            tmpMessage.setCreateUser("admin");
            tmpMessage.setReadType("未读");
            tmpMessage.setReceiveUser(department);
            tmpMessage.setOrderName(message.getOrderName());
            tmpMessage.setClothesVersionNumber(message.getClothesVersionNumber());
            tmpMessage.setTitle(message.getTitle());
            tmpMessage.setMessageBodyOne(message.getMessageBodyOne());
            messageList.add(tmpMessage);
            messageMapper.addMessageBatch(messageList);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<Message> getUnFinishMessageByType(Map<String, Object> map) {
        return messageMapper.getUnFinishMessageByType(map);
    }

    @Override
    public List<Message> getMessageContentByInfo(Map<String, Object> map) {
        return messageMapper.getMessageContentByInfo(map);
    }

    @Override
    public Integer updateMessageBatch(List<Message> messageList) {
        try{
            if (messageList != null && !messageList.isEmpty()){
                messageMapper.updateMessageBatch(messageList);
            }
            return 0;
        } catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer getUnFinishCountTotalCount() {
        Integer messageCount = 0;
        try{
            if (messageMapper.getUnFinishCountTotalCount() != null){
                messageCount = messageMapper.getUnFinishCountTotalCount();
            }
            return messageCount;
        } catch (Exception e){
            e.printStackTrace();
        }
        return messageCount;
    }
}
