package com.example.erp01.service.impl;

import com.example.erp01.mapper.AccessoryOutRecordMapper;
import com.example.erp01.model.AccessoryOutRecord;
import com.example.erp01.service.AccessoryOutRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
@Service
public class AccessoryOutRecordServiceImpl implements AccessoryOutRecordService {

    @Autowired
    private AccessoryOutRecordMapper accessoryOutRecordMapper;

    @Override
    public Integer addAccessoryOutRecord(AccessoryOutRecord accessoryOutRecord) {
        try{
            if (accessoryOutRecord != null){
                accessoryOutRecordMapper.addAccessoryOutRecord(accessoryOutRecord);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer addAccessoryOutRecordBatch(List<AccessoryOutRecord> accessoryOutRecordList) {
        try{
            if (accessoryOutRecordList != null && accessoryOutRecordList.size() > 0){
                accessoryOutRecordMapper.addAccessoryOutRecordBatch(accessoryOutRecordList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<AccessoryOutRecord> getAccessoryOutRecordByOrder(String orderName) {
        List<AccessoryOutRecord> accessoryOutRecordList = new ArrayList<>();
        try{
            accessoryOutRecordList = accessoryOutRecordMapper.getAccessoryOutRecordByOrder(orderName);
            return accessoryOutRecordList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return accessoryOutRecordList;
    }

    @Override
    public List<AccessoryOutRecord> getAccessoryOutRecordByInfo(String orderName, String accessoryName, String specification, String accessoryColor, String colorName, String sizeName, Integer accessoryID) {
        List<AccessoryOutRecord> accessoryOutRecordList = new ArrayList<>();
        try{
            accessoryOutRecordList = accessoryOutRecordMapper.getAccessoryOutRecordByInfo(orderName, accessoryName, specification, accessoryColor, colorName, sizeName, accessoryID);
            return accessoryOutRecordList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return accessoryOutRecordList;
    }

    @Override
    public List<AccessoryOutRecord> getTodayAccessoryOutRecord() {
        List<AccessoryOutRecord> accessoryOutRecordList = new ArrayList<>();
        try{
            accessoryOutRecordList = accessoryOutRecordMapper.getTodayAccessoryOutRecord();
            return accessoryOutRecordList;
        } catch (Exception e){
            e.printStackTrace();
        }
        return accessoryOutRecordList;
    }

    @Override
    public List<AccessoryOutRecord> getAccessoryOutRecordByTime(String from, String to) {
        List<AccessoryOutRecord> accessoryOutRecordList = new ArrayList<>();
        try{
            accessoryOutRecordList = accessoryOutRecordMapper.getAccessoryOutRecordByTime(from, to);
            return accessoryOutRecordList;
        } catch (Exception e){
            e.printStackTrace();
        }
        return accessoryOutRecordList;
    }

    @Override
    public List<AccessoryOutRecord> getAccessoryOutRecordByOrderNameList(List<String> orderNameList) {
        List<AccessoryOutRecord> accessoryOutRecordList = new ArrayList<>();
        if (orderNameList != null && !orderNameList.isEmpty()){
            accessoryOutRecordList = accessoryOutRecordMapper.getAccessoryOutRecordByOrderNameList(orderNameList);
        }
        return accessoryOutRecordList;
    }

    @Override
    public List<AccessoryOutRecord> getBjOutRecordByInfo(String orderName) {
        return accessoryOutRecordMapper.getBjOutRecordByInfo(orderName);
    }
}
