package com.example.erp01.service.impl;

import com.example.erp01.mapper.InvoicingAccessoryMapper;
import com.example.erp01.model.AccessoryInStore;
import com.example.erp01.model.AccessoryOutRecord;
import com.example.erp01.model.AccessoryStorage;
import com.example.erp01.service.InvoicingAccessoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class InvoicingAccessoryServiceImpl implements InvoicingAccessoryService {

    @Autowired
    InvoicingAccessoryMapper invoicingAccessoryMapper;

    @Override
    public List<Integer> getAccessoryOperateIdList(Map<String, Object> map) {
        List<Integer> accessoryIdList = invoicingAccessoryMapper.getAccessoryInStoreIdList(map);
        List<Integer> accessoryIdList1 = invoicingAccessoryMapper.getAccessoryOutRecordIdList(map);
        accessoryIdList.addAll(accessoryIdList1);
        return accessoryIdList;
    }

    @Override
    public List<AccessoryInStore> getAccessoryInStoreByIdList(List<Integer> accessoryIDList) {
        List<AccessoryInStore> accessoryInStoreList = new ArrayList<>();
        try{
            if (accessoryIDList != null && !accessoryIDList.isEmpty()){
                accessoryInStoreList = invoicingAccessoryMapper.getAccessoryInStoreByIdList(accessoryIDList);
            }
            return accessoryInStoreList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return accessoryInStoreList;
    }

    @Override
    public List<AccessoryStorage> getAccessoryStorageByIdList(List<Integer> accessoryIDList) {
        List<AccessoryStorage> accessoryStorageList = new ArrayList<>();
        try{
            if (accessoryIDList != null && !accessoryIDList.isEmpty()){
                accessoryStorageList = invoicingAccessoryMapper.getAccessoryStorageByIdList(accessoryIDList);
            }
            return accessoryStorageList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return accessoryStorageList;
    }

    @Override
    public List<AccessoryOutRecord> getAccessoryOutRecordByIdList(List<Integer> accessoryIDList) {
        List<AccessoryOutRecord> accessoryOutRecordList = new ArrayList<>();
        try{
            if (accessoryIDList != null && !accessoryIDList.isEmpty()){
                accessoryOutRecordList = invoicingAccessoryMapper.getAccessoryOutRecordByIdList(accessoryIDList);
            }
            return accessoryOutRecordList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return accessoryOutRecordList;
    }

    @Override
    public List<AccessoryInStore> getAccessoryInStoreByMap(Map<String, Object> map) {
        return invoicingAccessoryMapper.getAccessoryInStoreByMap(map);
    }

    @Override
    public List<AccessoryStorage> getAccessoryStorageByMap(Map<String, Object> map) {
        return invoicingAccessoryMapper.getAccessoryStorageByMap(map);
    }

    @Override
    public List<AccessoryOutRecord> getAccessoryOutRecordByMap(Map<String, Object> map) {
        return invoicingAccessoryMapper.getAccessoryOutRecordByMap(map);
    }
}
