package com.example.erp01.service.impl;

import com.example.erp01.mapper.MaxBedNumberMapper;
import com.example.erp01.mapper.MaxTailorQcodeIDMapper;
import com.example.erp01.model.MaxBedNumber;
import com.example.erp01.service.MaxBedNumberService;
import com.example.erp01.service.MaxTailorQcodeIDService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


@Service
public class MaxBedNumberServiceImpl implements MaxBedNumberService {

    @Autowired
    private MaxBedNumberMapper maxBedNumberMapper;

    @Override
    public Integer getMaxBedNumberByOrder(String orderName) {
        Integer maxBedNumber = 0;
        try{
            if (maxBedNumberMapper.getMaxBedNumberByOrder(orderName) != null){
                maxBedNumber = maxBedNumberMapper.getMaxBedNumberByOrder(orderName);
            }
            return maxBedNumber;
        }catch (Exception e){
            e.printStackTrace();
        }
        return maxBedNumber;
    }

    @Override
    public Integer addMaxBedNumber(MaxBedNumber maxBedNumber) {
        try{
            maxBedNumberMapper.addMaxBedNumber(maxBedNumber);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }
}
