package com.example.erp01.service.impl;

import com.example.erp01.mapper.HourEmpMapper;
import com.example.erp01.model.GeneralSalary;
import com.example.erp01.model.HourEmp;
import com.example.erp01.service.HourEmpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class HourEmpServiceImpl implements HourEmpService {
    @Autowired
    private HourEmpMapper hourEmpMapper;

    @Override
    public int addHourEmp(HourEmp hourEmp) {
        try{
            hourEmpMapper.addHourEmp(hourEmp);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<HourEmp> getAllHourEmp() {
        List<HourEmp> hourEmpList = new ArrayList<>();
        try{
            hourEmpList = hourEmpMapper.getAllHourEmp();
            return hourEmpList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return hourEmpList;
    }

    @Override
    public int deleteHourEmp(Integer hourEmpID) {
        try{
            hourEmpMapper.deleteHourEmp(hourEmpID);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<GeneralSalary> getHourEmpByTimeEmp(Date from, Date to, String employeeNumber) {
        List<GeneralSalary> generalSalaryList = new ArrayList<>();
        try{
            generalSalaryList = hourEmpMapper.getHourEmpByTimeEmp(from,to,employeeNumber);
            return generalSalaryList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return generalSalaryList;
    }

    @Override
    public List<GeneralSalary> getGroupHourEmpByTime(Date from, Date to, String groupName) {
        List<GeneralSalary> generalSalaryList = new ArrayList<>();
        try{
            generalSalaryList = hourEmpMapper.getGroupHourEmpByTime(from,to,groupName);
            return generalSalaryList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return generalSalaryList;
    }

    @Override
    public List<HourEmp> getHourProcedureProgress(Date from, Date to, Integer procedureFrom, Integer procedureTo, String orderName, String groupName) {
        List<HourEmp> hourEmpList = new ArrayList<>();
        try{
            hourEmpList = hourEmpMapper.getHourProcedureProgress(from, to, procedureFrom, procedureTo, orderName, groupName);
            return hourEmpList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return hourEmpList;
    }

    @Override
    public List<HourEmp> getOrderHourEmpDetail(String orderName, Date from, Date to) {
        List<HourEmp> hourEmpList = new ArrayList<>();
        try{
            hourEmpList = hourEmpMapper.getOrderHourEmpDetail(orderName, from, to);
            return hourEmpList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return hourEmpList;
    }

    @Override
    public List<HourEmp> getOrderHourEmpSummary(String orderName,Date from,Date to) {
        List<HourEmp> hourEmpList = new ArrayList<>();
        try{
            hourEmpList = hourEmpMapper.getOrderHourEmpSummary(orderName,from,to);
            return hourEmpList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return hourEmpList;
    }

    @Override
    public int updateHourEmp(HourEmp hourEmp) {
        try{
            hourEmpMapper.updateHourEmp(hourEmp);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<HourEmp> getTodayHourEmp() {
        List<HourEmp> hourEmpList = new ArrayList<>();
        try{
            hourEmpList = hourEmpMapper.getTodayHourEmp();
            return hourEmpList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return hourEmpList;
    }

    @Override
    public List<HourEmp> getOneMonthHourEmp() {
        List<HourEmp> hourEmpList = new ArrayList<>();
        try{
            hourEmpList = hourEmpMapper.getOneMonthHourEmp();
            return hourEmpList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return hourEmpList;
    }

    @Override
    public List<HourEmp> getThreeMonthsHourEmp() {
        List<HourEmp> hourEmpList = new ArrayList<>();
        try{
            hourEmpList = hourEmpMapper.getThreeMonthsHourEmp();
            return hourEmpList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return hourEmpList;
    }

    @Override
    public List<GeneralSalary> getHourProcedureProgressNew(Date from, Date to, Integer procedureFrom, Integer procedureTo, String orderName, String groupName, String employeeNumber) {
        List<GeneralSalary> generalSalaryList = new ArrayList<>();
        try{
            generalSalaryList = hourEmpMapper.getHourProcedureProgressNew(from, to, procedureFrom, procedureTo, orderName, groupName, employeeNumber);
            return generalSalaryList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return generalSalaryList;
    }

    @Override
    public List<HourEmp> getHourProcedureProgressMultiGroup(Date from, Date to, Integer procedureFrom, Integer procedureTo, String orderName, List<String> groupList, String employeeNumber) {
        List<HourEmp> hourEmpList = new ArrayList<>();
        try{
            hourEmpList = hourEmpMapper.getHourProcedureProgressMultiGroup(from, to, procedureFrom, procedureTo, orderName, groupList, employeeNumber);
            return hourEmpList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return hourEmpList;
    }
}
