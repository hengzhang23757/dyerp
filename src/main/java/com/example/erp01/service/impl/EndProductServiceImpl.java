package com.example.erp01.service.impl;

import com.example.erp01.mapper.EndProductMapper;
import com.example.erp01.model.EndProduct;
import com.example.erp01.model.EndProductPay;
import com.example.erp01.service.EndProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class EndProductServiceImpl implements EndProductService {

    @Autowired
    private EndProductMapper endProductMapper;

    @Override
    public Integer addEndProductBatch(List<EndProduct> endProductList) {
        try{
            if (endProductList != null && !endProductList.isEmpty()){
                endProductMapper.addEndProductBatch(endProductList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer updateEndProduct(EndProduct endProduct) {
        try{
            endProductMapper.updateEndProduct(endProduct);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer updateEndProductBatch(List<EndProduct> endProductList) {
        try{
            if (endProductList != null && !endProductList.isEmpty()){
                endProductMapper.updateEndProductBatch(endProductList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public EndProduct getInStoreEndProductByOrderColorSize(String orderName, String colorName, String sizeName, String endType) {
        return endProductMapper.getInStoreEndProductByOrderColorSize(orderName, colorName, sizeName, endType);
    }

    @Override
    public Integer getOutStoreProductCountByOrderColorSize(String orderName, String colorName, String sizeName, String endType) {
        int outStoreCount = 0;
        if (endProductMapper.getOutStoreProductCountByOrderColorSize(orderName, colorName, sizeName, endType) != null){
            outStoreCount = endProductMapper.getOutStoreProductCountByOrderColorSize(orderName, colorName, sizeName, endType);
        }
        return outStoreCount;
    }

    @Override
    public List<EndProduct> getEndProductSummary() {
        List<EndProduct> endProductList = new ArrayList<>();
        try{
            endProductList = endProductMapper.getEndProductSummary();
            return endProductList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return endProductList;
    }

    @Override
    public Integer deleteEndProductBatch(List<Integer> idList) {
        try{
            if (idList != null && !idList.isEmpty()){
                endProductMapper.deleteEndProductBatch(idList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer deleteEndProduct(Integer id) {
        try{
            endProductMapper.deleteEndProduct(id);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<EndProduct> getEndProductInStoreInfo(String orderName, String endType) {
        List<EndProduct> endProductList = new ArrayList<>();
        try{
            endProductList = endProductMapper.getEndProductInStoreInfo(orderName, endType);
            return endProductList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return endProductList;
    }

    @Override
    public List<EndProduct> getEndProductStorageInfo(String orderName, String endType) {
        List<EndProduct> endProductList = new ArrayList<>();
        try{
            endProductList = endProductMapper.getEndProductStorageInfo(orderName, endType);
            return endProductList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return endProductList;
    }

    @Override
    public List<EndProduct> getEndProductOutStoreInfo(String orderName, String endType) {
        List<EndProduct> endProductList = new ArrayList<>();
        try{
            endProductList = endProductMapper.getEndProductOutStoreInfo(orderName, endType);
            return endProductList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return endProductList;
    }

    @Override
    public List<EndProduct> getEndProductStorageSummary(String endType, String orderName) {
        List<EndProduct> endProductList = new ArrayList<>();
        try{
            endProductList = endProductMapper.getEndProductStorageSummary(endType, orderName);
            return endProductList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return endProductList;
    }

    @Override
    public List<EndProduct> getEndProductByIdList(List<Integer> idList) {
        return endProductMapper.getEndProductByIdList(idList);
    }

    @Override
    public List<String> getEndProductStorageColorList(String orderName, String endType) {
        List<String> colorNameList = new ArrayList<>();
        try{
            colorNameList = endProductMapper.getEndProductStorageColorList(orderName, endType);
            return colorNameList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return colorNameList;
    }

    @Override
    public List<String> getEndProductStorageSizeList(String orderName, String endType) {
        List<String> sizeNameList = new ArrayList<>();
        try{
            sizeNameList = endProductMapper.getEndProductStorageSizeList(orderName, endType);
            return sizeNameList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return sizeNameList;
    }

    @Override
    public Integer getOutStoreCountByOrderName(String orderName, String endType) {
        Integer outStoreCount = 0;
        try{
            if (endProductMapper.getOutStoreCountByOrderName(orderName, endType) != null){
                outStoreCount = endProductMapper.getOutStoreCountByOrderName(orderName, endType);
            }
            return outStoreCount;
        } catch (Exception e){
            e.printStackTrace();
        }
        return outStoreCount;
    }

    @Override
    public EndProduct getStorageByOrderColorSize(String orderName, String colorName, String sizeName, String endType) {
        return endProductMapper.getStorageByOrderColorSize(orderName, colorName, sizeName, endType);
    }

    @Override
    public List<EndProduct> getDefectiveSummary() {
        List<EndProduct> endProductList = new ArrayList<>();
        try{
            endProductList = endProductMapper.getDefectiveSummary();
            return endProductList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return endProductList;
    }

    @Override
    public List<EndProduct> getDefectiveEndProductSummary(String orderName) {
        return endProductMapper.getDefectiveEndProductSummary(orderName);
    }

    @Override
    public List<EndProduct> getDefectiveEndProductDetailInByOrder(String orderName) {
        return endProductMapper.getDefectiveEndProductDetailInByOrder(orderName);
    }

    @Override
    public List<EndProduct> getDefectiveEndProductDetailStorageByOrder(String orderName) {
        return endProductMapper.getDefectiveEndProductDetailStorageByOrder(orderName);
    }

    @Override
    public List<EndProduct> getDefectiveEndProductDetailOutByOrder(String orderName) {
        return endProductMapper.getDefectiveEndProductDetailOutByOrder(orderName);
    }

    @Override
    public EndProduct getEndProductById(Integer id) {
        return endProductMapper.getEndProductById(id);
    }

    @Override
    public Integer endProductPreCheck(List<Integer> idList) {
        int accountCount = 0;
        try{
            if (endProductMapper.endProductPreCheck(idList) != null){
                accountCount = endProductMapper.endProductPreCheck(idList);
            }
            return accountCount;
        }catch (Exception e){
            e.printStackTrace();
        }
        return accountCount;
    }

    @Override
    public List<EndProduct> getPreAccountEndProduct() {
        return endProductMapper.getPreAccountEndProduct();
    }

    @Override
    public Integer endProductTakeIntoAccount(List<Integer> idList, String accountDate, String payNumber) {
        try{
            endProductMapper.endProductTakeIntoAccount(idList, accountDate, payNumber);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<EndProduct> getAllEndProduct() {
        return endProductMapper.getAllEndProduct();
    }

    @Override
    public List<EndProduct> getEndProductOutStoreByInfo(String monthStr, String customerName, String orderName, String endType) {
        return endProductMapper.getEndProductOutStoreByInfo(monthStr, customerName, orderName, endType);
    }

    @Override
    public List<EndProduct> getEndProductAccountByOrderNameList(List<String> orderNameList) {
        List<EndProduct> endProductList = new ArrayList<>();
        try{
            if (orderNameList != null && !orderNameList.isEmpty()){
                endProductList = endProductMapper.getEndProductAccountByOrderNameList(orderNameList);
            }
            return endProductList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return endProductList;
    }

    @Override
    public List<EndProduct> getEndProductSummaryByCustomer(String from, String to) {
        return endProductMapper.getEndProductSummaryByCustomer(from, to);
    }

    @Override
    public Integer quitAccountBatch(List<Integer> idList) {
        try{
            if (idList != null && !idList.isEmpty()){
                endProductMapper.quitAccountBatch(idList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<EndProductPay> endProductToEndProductPay(String payNumber) {
        return endProductMapper.endProductToEndProductPay(payNumber);
    }

    @Override
    public Integer quitAccountByPayNumber(String payNumber) {
        try{
            endProductMapper.quitAccountByPayNumber(payNumber);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer updatePriceByOrderName(String orderName, String colorName, Float price) {
        try{
            endProductMapper.updatePriceByOrderName(orderName, colorName, price);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<String> getPayNumberByOrderName(String orderName) {
        return endProductMapper.getPayNumberByOrderName(orderName);
    }

    @Override
    public Float getSumMoneyByPayNumber(String payNumber) {
        float sumMoney = 0;
        try{
            if (endProductMapper.getSumMoneyByPayNumber(payNumber) != null){
                sumMoney = endProductMapper.getSumMoneyByPayNumber(payNumber);
            }
            return sumMoney;
        }catch (Exception e){
            e.printStackTrace();
        }
        return sumMoney;
    }
}
