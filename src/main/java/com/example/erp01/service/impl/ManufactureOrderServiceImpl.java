package com.example.erp01.service.impl;

import com.example.erp01.mapper.ManufactureOrderMapper;
import com.example.erp01.model.ManufactureOrder;
import com.example.erp01.service.ManufactureOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ManufactureOrderServiceImpl implements ManufactureOrderService {

    @Autowired
    private ManufactureOrderMapper manufactureOrderMapper;

    @Override
    public Integer addManufactureOrder(ManufactureOrder manufactureOrder) {
        try{
            if (manufactureOrder != null){
                manufactureOrderMapper.addManufactureOrder(manufactureOrder);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public ManufactureOrder getManufactureOrderByOrderName(String orderName) {
        ManufactureOrder manufactureOrder;
        try{
            manufactureOrder = manufactureOrderMapper.getManufactureOrderByOrderName(orderName);
            return manufactureOrder;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Integer deleteManufactureByOrder(String orderName) {
        try{
            manufactureOrderMapper.deleteManufactureByOrder(orderName);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer updateManufactureOrder(ManufactureOrder manufactureOrder) {
        try{
            manufactureOrderMapper.updateManufactureOrder(manufactureOrder);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<String> getOrderNameByCustomerSeason(String season, String customerName) {
        List<String> orderNameList = new ArrayList<>();
        try{
            orderNameList = manufactureOrderMapper.getOrderNameByCustomerSeason(season, customerName);
            return orderNameList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return orderNameList;
    }

    @Override
    public List<ManufactureOrder> getManufactureOrderByOrderNameList(List<String> orderNameList) {
        List<ManufactureOrder> manufactureOrderList = new ArrayList<>();
        try{
            if (orderNameList != null && !orderNameList.isEmpty()){
                manufactureOrderList = manufactureOrderMapper.getManufactureOrderByOrderNameList(orderNameList);
            }
            return manufactureOrderList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return manufactureOrderList;
    }

    @Override
    public List<String> getHistoryDistinctSeason() {
        List<String> seasonList = new ArrayList<>();
        try{
            seasonList = manufactureOrderMapper.getHistoryDistinctSeason();
            return seasonList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return seasonList;
    }

    @Override
    public List<String> getHistoryCustomer() {
        List<String> customerNameList = new ArrayList<>();
        try{
            customerNameList = manufactureOrderMapper.getHistoryCustomer();
            return customerNameList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return customerNameList;
    }

    @Override
    public List<ManufactureOrder> getManufactureOrderListByModelSeason(String modelType, List<String> seasonList, List<String> customerNameList) {
        List<ManufactureOrder> manufactureOrderList = new ArrayList<>();
        try{
            if ((seasonList != null && !seasonList.isEmpty()) || (customerNameList != null && !customerNameList.isEmpty())){
                manufactureOrderList = manufactureOrderMapper.getManufactureOrderListByModelSeason(modelType, seasonList, customerNameList);
            }
            return manufactureOrderList;
        } catch (Exception e){
            e.printStackTrace();
        }
        return manufactureOrderList;
    }

    @Override
    public Integer changeModifyStateByOrderName(String orderName, String modifyState) {
        try{
            manufactureOrderMapper.changeModifyStateByOrderName(orderName, modifyState);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<ManufactureOrder> getManufactureOrderByModifyState(String modifyState) {
        return manufactureOrderMapper.getManufactureOrderByModifyState(modifyState);
    }

    @Override
    public void changeOrderName(String orderName, String toOrderName) {
        manufactureOrderMapper.changeOrderName(orderName, toOrderName);
    }


}
