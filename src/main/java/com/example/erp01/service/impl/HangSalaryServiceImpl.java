package com.example.erp01.service.impl;

import com.example.erp01.mapper.HangSalaryMapper;
import com.example.erp01.model.Completion;
import com.example.erp01.model.CompletionCount;
import com.example.erp01.model.HangSalary;
import com.example.erp01.model.WeekAmount;
import com.example.erp01.service.HangSalaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
@Service
public class HangSalaryServiceImpl implements HangSalaryService {
    @Autowired
    private HangSalaryMapper hangSalaryMapper;

    @Override
    public List<HangSalary> getHangSalaryByTimeEmp(Date from, Date to, String employeeNumber) {
        List<HangSalary> hangSalaryList = new ArrayList<>();
        try{
            hangSalaryList = hangSalaryMapper.getHangSalaryByTimeEmp(from, to, employeeNumber);
            return hangSalaryList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return hangSalaryList;
    }

    @Override
    public List<HangSalary> getGroupHangSalaryByTime(Date from, Date to, String groupName) {
        List<HangSalary> hangSalaryList = new ArrayList<>();
        try{
            hangSalaryList = hangSalaryMapper.getGroupHangSalaryByTime(from, to, groupName);
            return hangSalaryList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return hangSalaryList;
    }

    @Override
    public List<HangSalary> getHangProcedureProgress(Date from, Date to, Integer procedureFrom, Integer procedureTo, String orderName, String groupName) {
        List<HangSalary> hangSalaryList = new ArrayList<>();
        try{
            hangSalaryList = hangSalaryMapper.getHangProcedureProgress(from, to, procedureFrom, procedureTo, orderName, groupName);
            return hangSalaryList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return hangSalaryList;
    }

    @Override
    public List<WeekAmount> getHangWeekAmount() {
        List<WeekAmount> weekAmountList = new ArrayList<>();
        try{
            weekAmountList = hangSalaryMapper.getHangWeekAmount();
            return weekAmountList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return weekAmountList;
    }

    @Override
    public List<WeekAmount> getHangFinishWeekAmount() {
        List<WeekAmount> weekAmountList = new ArrayList<>();
        try{
            weekAmountList = hangSalaryMapper.getHangFinishWeekAmount();
            return weekAmountList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return weekAmountList;
    }

    @Override
    public List<CompletionCount> getHangCompletion(Integer procedureNumber) {
        List<CompletionCount> completionCountList = new ArrayList<>();
        try{
            completionCountList = hangSalaryMapper.getHangCompletion(procedureNumber);
            return completionCountList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return completionCountList;
    }

    @Override
    public List<String> getEmployeeNamesByOrderProcedure(String orderName, Integer procedureNumber) {
        List<String> employeeNameList = new ArrayList<>();
        try{
            employeeNameList = hangSalaryMapper.getEmployeeNamesByOrderProcedure(orderName, procedureNumber);
            return employeeNameList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return employeeNameList;
    }

    @Override
    public List<HangSalary> getOrderHangSalarySummary(String orderName,Date from,Date to) {
        List<HangSalary> hangSalaryList = new ArrayList<>();
        try{
            hangSalaryList = hangSalaryMapper.getOrderHangSalarySummary(orderName,from,to);
            return hangSalaryList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return hangSalaryList;
    }

    @Override
    public List<HangSalary> getHangProcedureProgressNew(Date from, Date to, Integer procedureFrom, Integer procedureTo, String orderName, String groupName, String employeeNumber) {
        List<HangSalary> hangSalaryList = new ArrayList<>();
        try{
            hangSalaryList = hangSalaryMapper.getHangProcedureProgressNew(from, to, procedureFrom, procedureTo, orderName, groupName, employeeNumber);
            return hangSalaryList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return hangSalaryList;
    }

    @Override
    public List<HangSalary> getHangSalaryDetail(Date from, Date to, String employeeNumber, String orderName) {
        List<HangSalary> hangSalaryList = new ArrayList<>();
        try{
            hangSalaryList = hangSalaryMapper.getHangSalaryDetail(from, to, employeeNumber, orderName);
            return hangSalaryList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return hangSalaryList;
    }

    @Override
    public List<HangSalary> getHangSalaryByOrderColorSizeEmp(Date from, Date to, String employeeNumber, String orderName, String colorName, String sizeName) {
        List<HangSalary> hangSalaryList = new ArrayList<>();
        try{
            hangSalaryList = hangSalaryMapper.getHangSalaryByOrderColorSizeEmp(from, to, employeeNumber, orderName, colorName, sizeName);
            return hangSalaryList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return hangSalaryList;
    }

    @Override
    public Integer getHangPieceCountByOrderColorSize(String orderName, Integer procedureNumber, String colorName, String sizeName) {
        Integer hangPieceCount = 0;
        try{
            if (hangSalaryMapper.getHangPieceCountByOrderColorSize(orderName, procedureNumber, colorName, sizeName) != null){
                hangPieceCount = hangSalaryMapper.getHangPieceCountByOrderColorSize(orderName, procedureNumber, colorName, sizeName);
            }
            return hangPieceCount;
        }catch (Exception e){
            e.printStackTrace();
        }
        return hangPieceCount;
    }

    @Override
    public List<HangSalary> getHangProcedureProgressMultiGroup(Date from, Date to, Integer procedureFrom, Integer procedureTo, String orderName, List<String> groupList, String employeeNumber) {
        List<HangSalary> hangSalaryList = new ArrayList<>();
        try{
            hangSalaryList = hangSalaryMapper.getHangProcedureProgressMultiGroup(from, to, procedureFrom, procedureTo, orderName, groupList, employeeNumber);
            return hangSalaryList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return hangSalaryList;
    }
}
