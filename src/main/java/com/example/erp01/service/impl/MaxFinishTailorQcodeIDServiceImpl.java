package com.example.erp01.service.impl;

import com.example.erp01.mapper.MaxFinishTailorQcodeIDMapper;
import com.example.erp01.mapper.MaxTailorQcodeIDMapper;
import com.example.erp01.service.MaxFinishTailorQcodeIDService;
import com.example.erp01.service.MaxTailorQcodeIDService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by hujian on 2019/10/12
 */
@Service
public class MaxFinishTailorQcodeIDServiceImpl implements MaxFinishTailorQcodeIDService {

    @Resource
    private MaxFinishTailorQcodeIDMapper maxFinishTailorQcodeIDMapper;

    public int getMaxTailorQcodeId() {
        return maxFinishTailorQcodeIDMapper.getMaxTailorQcodeId();
    }

    public int updateMaxTailorQcodeId(int qCodeId) {
        return maxFinishTailorQcodeIDMapper.updateMaxTailorQcodeId(qCodeId);
    }

}
