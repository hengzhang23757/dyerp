package com.example.erp01.service.impl;

import com.example.erp01.mapper.FabricOrderMapper;
import com.example.erp01.model.FabricOrder;
import com.example.erp01.service.FabricOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class FabricOrderServiceImpl implements FabricOrderService {

    @Autowired
    private FabricOrderMapper fabricOrderMapper;

    @Override
    public List<FabricOrder> getAllFabricOrder() {
        List<FabricOrder> fabricOrderList = new ArrayList<>();
        try{
            fabricOrderList = fabricOrderMapper.getAllFabricOrder();
            return fabricOrderList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return fabricOrderList;

    }

    @Override
    public List<FabricOrder> getTodayFabricOrder() {
        List<FabricOrder> fabricOrderList = new ArrayList<>();
        try{
            fabricOrderList = fabricOrderMapper.getTodayFabricOrder();
            return fabricOrderList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return fabricOrderList;
    }

    @Override
    public List<FabricOrder> getOneMonthFabricOrder() {
        List<FabricOrder> fabricOrderList = new ArrayList<>();
        try{
            fabricOrderList = fabricOrderMapper.getOneMonthFabricOrder();
            return fabricOrderList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return fabricOrderList;
    }

    @Override
    public List<FabricOrder> getThreeMonthsFabricOrder() {
        List<FabricOrder> fabricOrderList = new ArrayList<>();
        try{
            fabricOrderList = fabricOrderMapper.getThreeMonthsFabricOrder();
            return fabricOrderList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return fabricOrderList;
    }

    @Override
    public Integer addFabricOrderBatch(List<FabricOrder> fabricOrderList) {
        try{
            fabricOrderMapper.addFabricOrderBatch(fabricOrderList);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer updateFabricOrder(FabricOrder fabricOrder) {
        try{
            fabricOrderMapper.updateFabricOrder(fabricOrder);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer commitFabricOrder(List<Integer> fabricOrderIDList) {
        try{
            if (fabricOrderIDList!=null && !fabricOrderIDList.isEmpty()){
                fabricOrderMapper.commitFabricOrder(fabricOrderIDList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer deleteFabricOrderBatch(List<Integer> fabricOrderIDList) {
        try{
            fabricOrderMapper.deleteFabricOrderBatch(fabricOrderIDList);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer deleteFabricOrder(Integer fabricOrderID) {
        try{
            fabricOrderMapper.deleteFabricOrder(fabricOrderID);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }
}
