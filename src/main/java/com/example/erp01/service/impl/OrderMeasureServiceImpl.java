package com.example.erp01.service.impl;

import com.example.erp01.mapper.OrderMeasureMapper;
import com.example.erp01.model.OrderMeasure;
import com.example.erp01.service.OrderMeasureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class OrderMeasureServiceImpl implements OrderMeasureService {

    @Autowired
    private OrderMeasureMapper orderMeasureMapper;

    @Override
    public Integer addOrderMeasureBatch(List<OrderMeasure> orderMeasureList) {
        try{
            if (orderMeasureList != null && orderMeasureList.size() > 0){
                orderMeasureMapper.addOrderMeasureBatch(orderMeasureList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;

    }

    @Override
    public List<OrderMeasure> getOrderMeasureByOrder(String orderName) {
        List<OrderMeasure> orderMeasureList = new ArrayList<>();
        try{
            orderMeasureList = orderMeasureMapper.getOrderMeasureByOrder(orderName);
            return orderMeasureList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return orderMeasureList;
    }

    @Override
    public Integer deleteOrderMeasureByOrder(String orderName) {
        try{
            orderMeasureMapper.deleteOrderMeasureByOrder(orderName);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer updateOrderMeasure(OrderMeasure orderMeasure) {
        try{
            orderMeasureMapper.updateOrderMeasure(orderMeasure);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer deleteOrderMeasureByID(Integer orderMeasureID) {
        try{
            orderMeasureMapper.deleteOrderMeasureByID(orderMeasureID);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer addOrderMeasure(OrderMeasure orderMeasure) {
        try{
            orderMeasureMapper.addOrderMeasure(orderMeasure);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public OrderMeasure getOneOrderMeasureByOrder(String orderName) {
        return orderMeasureMapper.getOneOrderMeasureByOrder(orderName);
    }
}
