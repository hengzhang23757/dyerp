package com.example.erp01.service.impl;

import com.example.erp01.mapper.AccessoryStorageMapper;
import com.example.erp01.model.AccessoryStorage;
import com.example.erp01.service.AccessoryStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
@Service
public class AccessoryStorageServiceImpl implements AccessoryStorageService {

    @Autowired
    private AccessoryStorageMapper accessoryStorageMapper;

    @Override
    public int addAccessoryStorageBatch(List<AccessoryStorage> accessoryStorageList) {
        try{
            if (accessoryStorageList != null && !accessoryStorageList.isEmpty()){
                accessoryStorageMapper.addAccessoryStorageBatch(accessoryStorageList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public int deleteAccessoryByID(Integer id) {
        try{
            accessoryStorageMapper.deleteAccessoryByID(id);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<AccessoryStorage> getAccessoryStorageByOrder(String orderName) {
        List<AccessoryStorage> accessoryStorageList = new ArrayList<>();
        try{
            accessoryStorageList = accessoryStorageMapper.getAccessoryStorageByOrder(orderName);
            return accessoryStorageList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return accessoryStorageList;
    }

    @Override
    public List<AccessoryStorage> getAllAccessoryStorage() {
        List<AccessoryStorage> accessoryStorageList = new ArrayList<>();
        try{
            accessoryStorageList = accessoryStorageMapper.getAllAccessoryStorage();
            return accessoryStorageList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return accessoryStorageList;
    }

    @Override
    public AccessoryStorage getAccessoryById(Integer id) {
        AccessoryStorage accessoryStorage = null;
        try{
            if (accessoryStorageMapper.getAccessoryById(id) != null){
                accessoryStorage = accessoryStorageMapper.getAccessoryById(id);
            }
            return accessoryStorage;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<AccessoryStorage> getAccessoryStorageByInfo(String orderName, String accessoryName, String specification, String accessoryColor, String colorName, String sizeName, Integer accessoryID) {
        List<AccessoryStorage> accessoryStorageList = new ArrayList<>();
        try{
            accessoryStorageList = accessoryStorageMapper.getAccessoryStorageByInfo(orderName, accessoryName, specification, accessoryColor, colorName, sizeName, accessoryID);
            return accessoryStorageList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return accessoryStorageList;
    }

    @Override
    public int updateAccessoryStorage(AccessoryStorage accessoryStorage) {
        try{
            accessoryStorageMapper.updateAccessoryStorage(accessoryStorage);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public int updateAccessoryStorageBatch(List<AccessoryStorage> accessoryStorageList) {
        try{
            if (accessoryStorageList != null && !accessoryStorageList.isEmpty()){
                accessoryStorageMapper.updateAccessoryStorageBatch(accessoryStorageList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public int deleteAccessoryStorageBatch(List<Integer> idList) {
        try{
            if (idList != null && !idList.isEmpty()){
                accessoryStorageMapper.deleteAccessoryStorageBatch(idList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;

    }

    @Override
    public List<AccessoryStorage> getAccessoryStorageByColorSizeList(String orderName, List<String> colorList, List<String> sizeList) {
        List<AccessoryStorage> accessoryStorageList = new ArrayList<>();
        try{
            accessoryStorageList = accessoryStorageMapper.getAccessoryStorageByColorSizeList(orderName, colorList, sizeList);
            return accessoryStorageList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return accessoryStorageList;
    }

    @Override
    public List<AccessoryStorage> getAccessoryStorageByAccessoryIdList(List<Integer> accessoryIdList) {
        List<AccessoryStorage> accessoryStorageList = new ArrayList<>();
        try{
            if (accessoryIdList != null && !accessoryIdList.isEmpty()){
                accessoryStorageList = accessoryStorageMapper.getAccessoryStorageByAccessoryIdList(accessoryIdList);
            }
            return accessoryStorageList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return accessoryStorageList;
    }

    @Override
    public List<AccessoryStorage> getBjStorageByInfo(String orderName) {
        return accessoryStorageMapper.getBjStorageByInfo(orderName);
    }
}
