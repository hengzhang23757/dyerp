package com.example.erp01.service.impl;

import com.example.erp01.mapper.DepartmentGroupMapper;
import com.example.erp01.model.DepartmentGroup;
import com.example.erp01.service.DepartmentGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DepartmentGroupServiceImpl implements DepartmentGroupService {

    @Autowired
    private DepartmentGroupMapper departmentGroupMapper;

    @Override
    public Integer addDepartmentGroup(DepartmentGroup departmentGroup) {
        try{
            departmentGroupMapper.addDepartmentGroup(departmentGroup);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer deleteDepartmentGroup(Integer id) {
        try{
            departmentGroupMapper.deleteDepartmentGroup(id);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<DepartmentGroup> getAllDepartmentGroup() {
        List<DepartmentGroup> departmentGroupList = new ArrayList<>();
        try{
            departmentGroupList = departmentGroupMapper.getAllDepartmentGroup();
            return departmentGroupList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return departmentGroupList;
    }

    @Override
    public DepartmentGroup getDepartmentGroupByDepartment(String departmentName) {
        DepartmentGroup departmentGroup = null;
        try{
            departmentGroup = departmentGroupMapper.getDepartmentGroupByDepartment(departmentName);
            return departmentGroup;
        }catch (Exception e){
            e.printStackTrace();
        }
        return departmentGroup;
    }

    @Override
    public List<String> getAllCheckGroupName() {
        List<String> groupList = new ArrayList<>();
        try{
            groupList = departmentGroupMapper.getAllCheckGroupName();
            return groupList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return groupList;
    }

    @Override
    public List<String> getAllDepartmentName() {
        return departmentGroupMapper.getAllDepartmentName();
    }

    @Override
    public List<String> getAllGroupName() {
        return departmentGroupMapper.getAllGroupName();
    }

    @Override
    public List<String> listGroupNameByDepartment(String departmentName) {
        return departmentGroupMapper.listGroupNameByDepartment(departmentName);
    }
}
