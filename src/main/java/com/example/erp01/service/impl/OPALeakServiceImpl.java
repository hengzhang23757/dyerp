package com.example.erp01.service.impl;

import com.example.erp01.mapper.OPALeakMapper;
import com.example.erp01.model.*;
import com.example.erp01.service.OPALeakService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class OPALeakServiceImpl implements OPALeakService {
    @Autowired
    private OPALeakMapper opaLeakMapper;

    @Override
    public List<String> getOPAPartNamesByOrder(String orderName) {
        List<String> partNameList = new ArrayList<>();
        try{
            partNameList = opaLeakMapper.getOPAPartNamesByOrder(orderName);
            return partNameList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return partNameList;
    }

    @Override
    public List<String> getOtherOPAPartNamesByOrder(String orderName) {
        List<String> partNameList = new ArrayList<>();
        try{
            partNameList = opaLeakMapper.getOtherOPAPartNamesByOrder(orderName);
            return partNameList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return partNameList;
    }

    @Override
    public List<OPALeak> getTailorOPA(String orderName, String partName, Integer bedNumber) {
        List<OPALeak> opaLeakList = new ArrayList<>();
        try{
            opaLeakList = opaLeakMapper.getTailorOPA(orderName, partName, bedNumber);
            return opaLeakList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return opaLeakList;
    }

    @Override
    public List<OPALeak> getOtherTailorOPA(String orderName, String partName, Integer bedNumber) {
        List<OPALeak> opaLeakList = new ArrayList<>();
        try{
            opaLeakList = opaLeakMapper.getOtherTailorOPA(orderName, partName, bedNumber);
            return opaLeakList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return opaLeakList;
    }

    @Override
    public List<OPALeak> getOtherOPA(String orderName, String partName) {
        List<OPALeak> opaLeakList = new ArrayList<>();
        try{
            opaLeakList = opaLeakMapper.getOtherOPA(orderName, partName);
            return opaLeakList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return opaLeakList;
    }

    @Override
    public List<OPALeak> getOPABack(String orderName, String partName, Date fromDate, Date toDate) {
        List<OPALeak> opaLeakList = new ArrayList<>();
        try{
            opaLeakList = opaLeakMapper.getOPABack(orderName, partName, fromDate, toDate);
            return opaLeakList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return opaLeakList;
    }

    @Override
    public List<OPALeak> getTailorOPAByBed(String orderName, String partName, Integer bedNumber) {
        List<OPALeak> opaLeakList = new ArrayList<>();
        try{
            opaLeakList = opaLeakMapper.getTailorOPAByBed(orderName, partName, bedNumber);
            return opaLeakList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return opaLeakList;
    }

    @Override
    public List<OPALeak> getOtherTailorOPAByBed(String orderName, String partName, Integer bedNumber) {
        List<OPALeak> opaLeakList = new ArrayList<>();
        try{
            opaLeakList = opaLeakMapper.getOtherTailorOPAByBed(orderName, partName, bedNumber);
            return opaLeakList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return opaLeakList;
    }

    @Override
    public List<OPALeak> getMonthOPAInfo(Date fromDate, Date toDate) {
        List<OPALeak> opaLeakList = new ArrayList<>();
        try{
            opaLeakList = opaLeakMapper.getMonthOPAInfo(fromDate, toDate);
            return opaLeakList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return opaLeakList;
    }

    @Override
    public List<OPALeak> getOtherMonthOPAInfo(Date fromDate, Date toDate) {
        List<OPALeak> opaLeakList = new ArrayList<>();
        try{
            opaLeakList = opaLeakMapper.getOtherMonthOPAInfo(fromDate, toDate);
            return opaLeakList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return opaLeakList;
    }

    @Override
    public List<OPALeak> getMonthOPABackInfo(Date fromDate, Date toDate) {
        List<OPALeak> opaLeakList = new ArrayList<>();
        try{
            opaLeakList = opaLeakMapper.getMonthOPABackInfo(fromDate, toDate);
            return opaLeakList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return opaLeakList;
    }

    @Override
    public List<OPALeak> getTailorCountByOrderPart(String orderName, String partName) {
        List<OPALeak> opaLeakList = new ArrayList<>();
        try{
            opaLeakList = opaLeakMapper.getTailorCountByOrderPart(orderName, partName);
            return opaLeakList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return opaLeakList;
    }

    @Override
    public List<OPALeak> getOtherTailorCountByOrderPart(String orderName, String partName) {
        List<OPALeak> opaLeakList = new ArrayList<>();
        try{
            opaLeakList = opaLeakMapper.getOtherTailorCountByOrderPart(orderName, partName);
            return opaLeakList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return opaLeakList;
    }

    @Override
    public List<Integer> getOPABedNumbersByOrderPart(String orderName, String partName) {
        List<Integer> bedNumberList = new ArrayList<>();
        try{
            bedNumberList = opaLeakMapper.getOPABedNumbersByOrderPart(orderName, partName);
            return bedNumberList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return bedNumberList;
    }

    @Override
    public List<OPALeak> getTailorInfoByOrderPart(String orderName, String partName) {
        List<OPALeak> opaLeakList = new ArrayList<>();
        try{
            opaLeakList = opaLeakMapper.getTailorInfoByOrderPart(orderName, partName);
            return opaLeakList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return opaLeakList;
    }

    @Override
    public List<OPALeak> getOtherTailorInfoByOrderPart(String orderName, String partName) {
        List<OPALeak> opaLeakList = new ArrayList<>();
        try{
            opaLeakList = opaLeakMapper.getOtherTailorInfoByOrderPart(orderName, partName);
            return opaLeakList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return opaLeakList;
    }

    @Override
    public List<OPA> getOPAByOrderTime(String orderName, String partName, Date fromDate, Date toDate) {
        List<OPA> opaList = new ArrayList<>();
        try{
            opaList = opaLeakMapper.getOPAByOrderTime(orderName, partName, fromDate, toDate);
            return opaList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return opaList;
    }

    @Override
    public List<OtherOPA> getOtherOPAByOrderTime(String orderName, String partName, Date fromDate, Date toDate) {
        List<OtherOPA> otherOPAList = new ArrayList<>();
        try{
            otherOPAList = opaLeakMapper.getOtherOPAByOrderTime(orderName, partName, fromDate, toDate);
            return otherOPAList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return otherOPAList;
    }

    @Override
    public List<OpaBackInput> getOPABackInputByOrderTime(String orderName, String partName, Date fromDate, Date toDate) {
        List<OpaBackInput> opaBackInputList = new ArrayList<>();
        try{
            opaBackInputList = opaLeakMapper.getOPABackInputByOrderTime(orderName, partName, fromDate, toDate);
            return opaBackInputList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return opaBackInputList;
    }


}
