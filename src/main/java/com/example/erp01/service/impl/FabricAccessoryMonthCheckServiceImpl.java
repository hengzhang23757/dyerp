package com.example.erp01.service.impl;

import com.example.erp01.mapper.FabricAccessoryMonthCheckMapper;
import com.example.erp01.model.AccessoryInStore;
import com.example.erp01.model.FabricDetail;
import com.example.erp01.model.FabricMonthCheck;
import com.example.erp01.model.FabricOutRecord;
import com.example.erp01.service.FabricAccessoryMonthCheckService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
@Service
public class FabricAccessoryMonthCheckServiceImpl implements FabricAccessoryMonthCheckService {

    @Autowired
    private FabricAccessoryMonthCheckMapper fabricAccessoryMonthCheckMapper;

    @Override
    public List<FabricMonthCheck> getFabricDetailCheckByInfo(Map<String, Object> map) {
        return fabricAccessoryMonthCheckMapper.getFabricDetailCheckByInfo(map);
    }

    @Override
    public List<FabricMonthCheck> getFabricOutRecordCheckByInfo(Map<String, Object> map) {
        return fabricAccessoryMonthCheckMapper.getFabricOutRecordCheckByInfo(map);
    }

    @Override
    public List<String> getAllFabricSupplier() {
        return fabricAccessoryMonthCheckMapper.getAllFabricSupplier();
    }

    @Override
    public List<String> getAllAccessorySupplier() {
        return fabricAccessoryMonthCheckMapper.getAllAccessorySupplier();
    }

    @Override
    public Integer updateCheckDetailInCheck(List<FabricDetail> fabricDetailList) {
        try{
            if (fabricDetailList != null && !fabricDetailList.isEmpty()){
                fabricAccessoryMonthCheckMapper.updateCheckDetailInCheck(fabricDetailList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer updateCheckOutRecordInCheck(List<FabricOutRecord> fabricOutRecordList) {
        try{
            if (fabricOutRecordList != null && !fabricOutRecordList.isEmpty()){
                fabricAccessoryMonthCheckMapper.updateCheckOutRecordInCheck(fabricOutRecordList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<AccessoryInStore> getAccessoryInStoreByInfo(Map<String, Object> map) {
        return fabricAccessoryMonthCheckMapper.getAccessoryInStoreByInfo(map);
    }

    @Override
    public Integer updateAccessoryInStoreInCheck(List<AccessoryInStore> accessoryInStoreList) {
        try{
            if (accessoryInStoreList != null && !accessoryInStoreList.isEmpty()){
                fabricAccessoryMonthCheckMapper.updateAccessoryInStoreInCheck(accessoryInStoreList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer quitFabricDetailBatch(List<Integer> fabricDetailIDList) {
        try{
            if (fabricDetailIDList != null && !fabricDetailIDList.isEmpty()){
                fabricAccessoryMonthCheckMapper.quitFabricDetailBatch(fabricDetailIDList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer quitAccessoryInStoreBatch(List<Integer> idList) {
        try{
            if (idList != null && !idList.isEmpty()){
                fabricAccessoryMonthCheckMapper.quitAccessoryInStoreBatch(idList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }
}
