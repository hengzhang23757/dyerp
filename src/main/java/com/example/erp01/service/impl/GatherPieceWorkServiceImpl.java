package com.example.erp01.service.impl;

import com.example.erp01.mapper.GatherPieceWorkMapper;
import com.example.erp01.model.GeneralSalary;
import com.example.erp01.service.GatherPieceWorkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class GatherPieceWorkServiceImpl implements GatherPieceWorkService {

    @Autowired
    private GatherPieceWorkMapper gatherPieceWorkMapper;

    @Override
    public List<GeneralSalary> getGatherPieceWorkAmongSummary(String orderName, String groupName, String employeeNumber, String from, String to, Integer procedureNumber) {
        List<GeneralSalary> generalSalaryList = new ArrayList<>();
        try{
            generalSalaryList = gatherPieceWorkMapper.getGatherPieceWorkAmongSummary(orderName, groupName, employeeNumber, from, to, procedureNumber);
            return generalSalaryList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return generalSalaryList;
    }

    @Override
    public List<GeneralSalary> getGatherPieceWorkEachDaySummary(String orderName, String groupName, String employeeNumber, String from, String to, Integer procedureNumber) {
        List<GeneralSalary> generalSalaryList = new ArrayList<>();
        try{
            generalSalaryList = gatherPieceWorkMapper.getGatherPieceWorkEachDaySummary(orderName, groupName, employeeNumber, from, to, procedureNumber);
            return generalSalaryList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return generalSalaryList;
    }

    @Override
    public List<GeneralSalary> getGatherSalaryEachDaySummary(String orderName, String groupName, String employeeNumber, String from, String to, Integer procedureNumber) {
        List<GeneralSalary> generalSalaryList = new ArrayList<>();
        try{
            generalSalaryList = gatherPieceWorkMapper.getGatherSalaryEachDaySummary(orderName, groupName, employeeNumber, from, to, procedureNumber);
            return generalSalaryList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return generalSalaryList;
    }
}
