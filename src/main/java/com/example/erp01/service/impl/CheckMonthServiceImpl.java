package com.example.erp01.service.impl;

import com.example.erp01.mapper.CheckMonthMapper;
import com.example.erp01.model.CheckMonth;
import com.example.erp01.service.CheckMonthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class CheckMonthServiceImpl implements CheckMonthService {

    @Autowired
    private CheckMonthMapper checkMonthMapper;

    @Override
    public Integer addCheckMonth(CheckMonth checkMonth) {
        try{
            checkMonthMapper.addCheckMonth(checkMonth);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer deleteCheckMonth(Integer id) {
        try{
            checkMonthMapper.deleteCheckMonth(id);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer updateCheckMonth(CheckMonth checkMonth) {
        try{
            checkMonthMapper.updateCheckMonth(checkMonth);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<CheckMonth> getAllCheckMonth() {
        return checkMonthMapper.getAllCheckMonth();
    }

    @Override
    public CheckMonth getCheckMonthByInfo(Map<String, Object> map) {
        return checkMonthMapper.getCheckMonthByInfo(map);
    }
}
