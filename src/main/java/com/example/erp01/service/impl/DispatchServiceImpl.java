package com.example.erp01.service.impl;

import com.example.erp01.mapper.DispatchMapper;
import com.example.erp01.model.Dispatch;
import com.example.erp01.model.ProcedureInfo;
import com.example.erp01.service.DispatchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DispatchServiceImpl implements DispatchService {

    @Autowired
    private DispatchMapper dispatchMapper;

    @Override
    public int addDispatchBatch(List<Dispatch> dispatchList) {
        try{
            if(dispatchList!=null && !dispatchList.isEmpty()){
                dispatchMapper.addDispatchBatch(dispatchList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public int addDispatch(Dispatch dispatch) {
        try{
            dispatchMapper.addDispatch(dispatch);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public int deleteDispatch(Integer dispatchID) {
        try{
            dispatchMapper.deleteDispatch(dispatchID);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<Dispatch> getAllDispatch() {
        List<Dispatch> dispatchList = new ArrayList<>();
        try{
            dispatchList = dispatchMapper.getAllDispatch();
            return dispatchList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return dispatchList;
    }

    @Override
    public List<Dispatch> getDispatchByOrder(String orderName) {
        List<Dispatch> dispatchList = new ArrayList<>();
        try{
            dispatchList = dispatchMapper.getDispatchByOrder(orderName);
            return dispatchList;
        }catch (Exception E){
            E.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Dispatch> getDispatchByGroup(String groupName) {
        List<Dispatch> dispatchList = new ArrayList<>();
        try{
            dispatchList = dispatchMapper.getDispatchByGroup(groupName);
            return dispatchList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Dispatch> getDispatchByEmp(String employeeNumber) {
        List<Dispatch> dispatchList = new ArrayList<>();
        try{
            dispatchList = dispatchMapper.getDispatchByEmp(employeeNumber);
            return dispatchList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Dispatch> getDispatchByOrderGroup(String orderName, String groupName) {
        List<Dispatch> dispatchList = new ArrayList<>();
        try{
            dispatchList = dispatchMapper.getDispatchByOrderGroup(orderName, groupName);
            return dispatchList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<ProcedureInfo> getProcedureInfoByOrderEmp(String orderName, String employeeNumber) {
        List<ProcedureInfo> procedureInfoList = new ArrayList<>();
        try{
            procedureInfoList =dispatchMapper.getProcedureInfoByOrderEmp(orderName, employeeNumber);
            return procedureInfoList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return procedureInfoList;
    }

    @Override
    public List<String> getDistinctEmployeeNameByOrder(String orderName, String groupName) {
        List<String> employeeNameList = new ArrayList<>();
        try{
            employeeNameList = dispatchMapper.getDistinctEmployeeNameByOrder(orderName, groupName);
            return employeeNameList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return employeeNameList;
    }

    @Override
    public List<Dispatch> getDispatchByOrderGroupEmp(String orderName, String groupName, String employeeName) {
        List<Dispatch> dispatchList = new ArrayList<>();
        try{
            dispatchList = dispatchMapper.getDispatchByOrderGroupEmp(orderName, groupName, employeeName);
            return dispatchList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Dispatch> getTodayDispatch() {
        List<Dispatch> dispatchList = new ArrayList<>();
        try{
            dispatchList = dispatchMapper.getTodayDispatch();
            return dispatchList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return dispatchList;
    }

    @Override
    public List<Dispatch> getOneMonthDispatch() {
        List<Dispatch> dispatchList = new ArrayList<>();
        try{
            dispatchList = dispatchMapper.getOneMonthDispatch();
            return dispatchList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return dispatchList;
    }

    @Override
    public List<Dispatch> getThreeMonthsDispatch() {
        List<Dispatch> dispatchList = new ArrayList<>();
        try{
            dispatchList = dispatchMapper.getThreeMonthsDispatch();
            return dispatchList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return dispatchList;
    }

    @Override
    public Integer deleteDispatchByOrderEmployeeList(String orderName, List<String> employeeNumberList) {
        try{
            if (employeeNumberList != null && !employeeNumberList.isEmpty()){
                dispatchMapper.deleteDispatchByOrderEmployeeList(orderName, employeeNumberList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

}
