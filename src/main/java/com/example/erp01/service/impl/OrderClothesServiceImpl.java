package com.example.erp01.service.impl;

import com.example.erp01.mapper.OrderClothesMapper;
import com.example.erp01.model.Completion;
import com.example.erp01.model.CutQueryLeak;
import com.example.erp01.model.OrderClothes;
import com.example.erp01.service.OrderClothesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class OrderClothesServiceImpl implements OrderClothesService {

    @Autowired
    private OrderClothesMapper orderClothesMapper;

    @Override
    public int addOrderClothes(List<OrderClothes> orderClothesList) {
        try{
            if(orderClothesList!=null && !orderClothesList.isEmpty()){
                orderClothesMapper.addOrderClothes(orderClothesList);
            }
            return 0;
        }catch (DataIntegrityViolationException e){
            return 100;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<OrderClothes> getAllOrderClothes() {
        List<OrderClothes> orderClothesList = null;
        try{
            orderClothesList = orderClothesMapper.getAllOrderClothes();
            return orderClothesList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return orderClothesList;
    }

    @Override
    public List<OrderClothes> getOrderSummary() {
        List<OrderClothes> orderClothesList = new ArrayList<>();
        try {
            orderClothesList = orderClothesMapper.getOrderSummary();
            return orderClothesList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return orderClothesList;
    }

    @Override
    public List<OrderClothes> getOrderByName(String orderName) {
        List<OrderClothes> orderClothesList = null;
        try{
            orderClothesList = orderClothesMapper.getOrderByName(orderName);
            return orderClothesList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return orderClothesList;
    }

    @Override
    public int deleteOrderByName(String orderName) {
        try{
            orderClothesMapper.deleteOrderByName(orderName);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<String> getOrderHint(String subOrderName) {
        List<String> orderList = new ArrayList<>();
        try{
            orderList = orderClothesMapper.getOrderHint(subOrderName);
            return orderList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return orderList;
    }

    @Override
    public List<String> getColorHint(String orderName) {
        List<String> colorList = new ArrayList<>();
        try{
            colorList = orderClothesMapper.getColorHint(orderName);
            return colorList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return colorList;
    }

    @Override
    public String getCustomerNameByOrderName(String orderName) {
        try{
            return orderClothesMapper.getCustomerNameByOrderName(orderName);
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<CutQueryLeak> getOrderInfoByName(String orderName) {
        List<CutQueryLeak> orderInfoList = new ArrayList<>();
        try{
            orderInfoList = orderClothesMapper.getOrderInfoByName(orderName);
            return orderInfoList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return orderInfoList;
    }

    @Override
    public String getVersionNumberByOrderName(String orderName) {
        try{
            return orderClothesMapper.getVersionNumberByOrderName(orderName);
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Integer getOrderTotalCount(String orderName) {
        Integer totalOrderCount = 0;
        try{
            totalOrderCount = orderClothesMapper.getOrderTotalCount(orderName);
            return totalOrderCount;
        }catch (Exception e){
            e.printStackTrace();
        }
        return totalOrderCount;
    }

    @Override
    public List<String> getOrderSizeNamesByOrder(String orderName) {
        List<String> sizeNameList = new ArrayList<>();
        try{
            sizeNameList = orderClothesMapper.getOrderSizeNamesByOrder(orderName);
            return sizeNameList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return sizeNameList;
    }

    @Override
    public String getDescriptionByOrder(String orderName) {
        try{
            return orderClothesMapper.getDescriptionByOrder(orderName);
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<String> getVersionHint(String subVersion) {
        List<String> versionList = new ArrayList<>();
        try{
            versionList = orderClothesMapper.getVersionHint(subVersion);
            return versionList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return versionList;
    }

    @Override
    public List<String> getOrderByVersion(String clothesVersionNumber) {
        List<String> orderList = new ArrayList<>();
        try{
            orderList = orderClothesMapper.getOrderByVersion(clothesVersionNumber);
            return orderList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return orderList;
    }

    @Override
    public List<Completion> getOrderCompletion() {
        List<Completion> completionList = new ArrayList<>();
        try{
            completionList = orderClothesMapper.getOrderCompletion();
            return completionList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return completionList;
    }

    @Override
    public String getSeasonByOrder(String orderName) {
        try{
            return orderClothesMapper.getSeasonByOrder(orderName);
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<OrderClothes> getOneMonthOrderClothes() {
        List<OrderClothes> orderClothesList = null;
        try{
            orderClothesList = orderClothesMapper.getOneMonthOrderClothes();
            return orderClothesList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return orderClothesList;
    }

    @Override
    public List<OrderClothes> getThreeMonthsOrderClothes() {
        List<OrderClothes> orderClothesList = null;
        try{
            orderClothesList = orderClothesMapper.getThreeMonthsOrderClothes();
            return orderClothesList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return orderClothesList;
    }

    @Override
    public List<OrderClothes> getOneMonthOrderSummary() {
        List<OrderClothes> orderClothesList = new ArrayList<>();
        try {
            orderClothesList = orderClothesMapper.getOneMonthOrderSummary();
            return orderClothesList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return orderClothesList;
    }

    @Override
    public List<OrderClothes> getThreeMonthsOrderSummary() {
        List<OrderClothes> orderClothesList = new ArrayList<>();
        try {
            orderClothesList = orderClothesMapper.getThreeMonthsOrderSummary();
            return orderClothesList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return orderClothesList;
    }

    @Override
    public Integer getColorOrderCount(String orderName, String colorName) {
        Integer colorOrderCount = 0;
        try{
            if (orderClothesMapper.getColorOrderCount(orderName, colorName) != null){
                colorOrderCount = orderClothesMapper.getColorOrderCount(orderName, colorName);
            }
            return colorOrderCount;
        }catch (Exception e){
            e.printStackTrace();
        }
        return colorOrderCount;
    }

    @Override
    public List<String> getAllCustomerName() {
        List<String> customerNameList = new ArrayList<>();
        try{
            customerNameList = orderClothesMapper.getAllCustomerName();
            return customerNameList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return customerNameList;
    }

    @Override
    public List<OrderClothes> getOrderClothesByOrder(List<String> orderNameList, String customerName) {
        List<OrderClothes> orderClothesList = new ArrayList<>();
        try{
            orderClothesList = orderClothesMapper.getOrderClothesByOrder(orderNameList, customerName);
            return orderClothesList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return orderClothesList;
    }

    @Override
    public List<String> getOrderColorNamesByOrder(String orderName) {
        List<String> colorNameList = new ArrayList<>();
        try{
            colorNameList = orderClothesMapper.getOrderColorNamesByOrder(orderName);
            return colorNameList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return colorNameList;
    }

    @Override
    public Integer updateOrderClothes(List<OrderClothes> orderClothesList) {
        try{
            if (orderClothesList != null && !orderClothesList.isEmpty()){
                orderClothesMapper.updateOrderClothes(orderClothesList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<OrderClothes> getOneYearOrderSummary(String orderName) {
        List<OrderClothes> orderClothesList = new ArrayList<>();
        try {
            orderClothesList = orderClothesMapper.getOneYearOrderSummary(orderName);
            return orderClothesList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return orderClothesList;
    }

    @Override
    public List<OrderClothes> getOrderSummaryByColor(String orderName) {
        List<OrderClothes> orderClothesList = new ArrayList<>();
        try{
            orderClothesList = orderClothesMapper.getOrderSummaryByColor(orderName);
            return orderClothesList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return orderClothesList;
    }

    @Override
    public List<OrderClothes> getOrderListSummaryByColor(List<String> orderNameList) {
        List<OrderClothes> orderClothesList = new ArrayList<>();
        try{
            orderClothesList = orderClothesMapper.getOrderListSummaryByColor(orderNameList);
            return orderClothesList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return orderClothesList;
    }

    @Override
    public List<OrderClothes> getOrderClothesByOrderName(String orderName) {
        List<OrderClothes> orderClothesList = new ArrayList<>();
        try{
            orderClothesList = orderClothesMapper.getOrderClothesByOrderName(orderName);
            return orderClothesList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return orderClothesList;
    }

    @Override
    public Integer updateOrderClothesByID(OrderClothes orderClothes) {
        try{
            if (orderClothes != null){
                orderClothesMapper.updateOrderClothesByID(orderClothes);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer deleteOrderClothesByID(Integer orderClothesID) {
        try{
            orderClothesMapper.deleteOrderClothesByID(orderClothesID);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer getOrderCountByColorSizeList(String orderName, List<String> colorList, List<String> sizeList) {
        Integer orderCount = 0;
        try{
            if (orderClothesMapper.getOrderCountByColorSizeList(orderName, colorList, sizeList) != null){
                orderCount = orderClothesMapper.getOrderCountByColorSizeList(orderName, colorList, sizeList);
            }
            return orderCount;
        }catch (Exception e){
            e.printStackTrace();
        }
        return orderCount;
    }

    @Override
    public List<OrderClothes> getLastSeasonSummary() {
        List<OrderClothes> orderClothesList = new ArrayList<>();
        try {
            orderClothesList = orderClothesMapper.getLastSeasonSummary();
            return orderClothesList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return orderClothesList;
    }

    @Override
    public List<OrderClothes> getOrderAndVersionByOrder(String subOrderName) {
        List<OrderClothes> orderClothesList = new ArrayList<>();
        try{
            orderClothesList = orderClothesMapper.getOrderAndVersionByOrder(subOrderName);
            return orderClothesList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return orderClothesList;
    }

    @Override
    public List<OrderClothes> getOrderAndVersionByVersion(String subVersion) {
        List<OrderClothes> orderClothesList = new ArrayList<>();
        try{
            orderClothesList = orderClothesMapper.getOrderAndVersionByVersion(subVersion);
            return orderClothesList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return orderClothesList;
    }

    @Override
    public List<OrderClothes> getOrderAndVersionByVersionPage(String subVersion,int page, int limit) {
        List<OrderClothes> orderClothesList = new ArrayList<>();
        try{
            orderClothesList = orderClothesMapper.getOrderAndVersionByVersionPage(subVersion,page,limit);
            return orderClothesList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return orderClothesList;
    }

    @Override
    public Integer getCountOrderAndVersionByVersion(String subVersion) {
        return orderClothesMapper.getCountOrderAndVersionByVersion(subVersion);
    }

    @Override
    public Integer updateOrderClothesByOrder(String orderName, String styleDescription, String season, String customerName, String userName) {
        try{
            orderClothesMapper.updateOrderClothesByOrder(orderName, styleDescription, season, customerName, userName);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<OrderClothes> getOrderClothesSummaryByMap(Map<String, Object> map) {
        List<OrderClothes> orderClothesList = new ArrayList<>();
        try{
            orderClothesList = orderClothesMapper.getOrderClothesSummaryByMap(map);
            return orderClothesList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return orderClothesList;
    }

    @Override
    public List<OrderClothes> getOrderCountByOrderNameList(List<String> orderNameList) {
        List<OrderClothes> orderClothesList = new ArrayList<>();
        try{
            if (orderNameList != null && !orderNameList.isEmpty()){
                orderClothesList = orderClothesMapper.getOrderCountByOrderNameList(orderNameList);
            }
            return orderClothesList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return orderClothesList;
    }

    @Override
    public Integer deleteOrderClothesBatch(List<Integer> orderClothesIDList) {
        try{
            if (orderClothesIDList != null && !orderClothesIDList.isEmpty()){
                orderClothesMapper.deleteOrderClothesBatch(orderClothesIDList);
            }
            return 0;
        } catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer updateOrderStateByOrder(String orderName, Integer orderState) {
        try{
            orderClothesMapper.updateOrderStateByOrder(orderName, orderState);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<OrderClothes> getOrderClothesCountGroupByColor(String orderName) {
        return orderClothesMapper.getOrderClothesCountGroupByColor(orderName);
    }

    @Override
    public List<OrderClothes> getOrderClothesCountGroupBySize(String orderName) {
        return orderClothesMapper.getOrderClothesCountGroupBySize(orderName);
    }

    @Override
    public void changeOrderName(String orderName, String toOrderName) {
        orderClothesMapper.changeOrderName(orderName, toOrderName);
    }

    @Override
    public List<OrderClothes> getOrderAndVersionHint(String keyword) {
        return orderClothesMapper.getOrderAndVersionHint(keyword);
    }

}
