package com.example.erp01.service.impl;

import com.example.erp01.mapper.MetreMapper;
import com.example.erp01.model.Metre;
import com.example.erp01.service.MetreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class MetreServiceImpl implements MetreService {
    @Autowired
    private MetreMapper metreMapper;

    @Override
    public List<Metre> getMetreByOrderTimeEmp(Integer procedureNumber, String orderName, String employeeName, Date fromTime, Date toTime) {
        List<Metre> metreList = new ArrayList<>();
        try{
            metreList = metreMapper.getMetreByOrderTimeEmp(procedureNumber, orderName, employeeName, fromTime, toTime);
            return metreList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return metreList;
    }

    @Override
    public List<Metre> getMetreByInfo(String orderName, List<Integer> procedureNumberList, List<String> groupNameList, List<String> employeeNameList, Date fromTime, Date toTime) {
        List<Metre> metreList = new ArrayList<>();
        try{
            metreList = metreMapper.getMetreByInfo(orderName, procedureNumberList, groupNameList, employeeNameList, fromTime, toTime);
            return metreList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return metreList;
    }
}
