package com.example.erp01.service.impl;

import com.example.erp01.mapper.FabricReportMapper;
import com.example.erp01.model.*;
import com.example.erp01.service.FabricReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class FabricReportServiceImpl implements FabricReportService {

    @Autowired
    private FabricReportMapper fabricReportMapper;

    @Override
    public List<FabricPlan> getFabricPlanByOrder(String orderName) {
        List<FabricPlan> fabricPlanList = new ArrayList<>();
        try{
            fabricPlanList = fabricReportMapper.getFabricPlanByOrder(orderName);
            return fabricPlanList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return fabricPlanList;
    }

    @Override
    public List<FabricDetail> getFabricDetailByOrder(String orderName, String colorName) {
        List<FabricDetail> fabricDetailList = new ArrayList<>();
        try{
            fabricDetailList = fabricReportMapper.getFabricDetailByOrder(orderName, colorName);
            return fabricDetailList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return fabricDetailList;
    }

    @Override
    public List<FabricDetail> getAllTypeFabricDetailByOrder(String orderName, String colorName) {
        List<FabricDetail> fabricDetailList = new ArrayList<>();
        try{
            fabricDetailList = fabricReportMapper.getAllTypeFabricDetailByOrder(orderName, colorName);
            return fabricDetailList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return fabricDetailList;
    }

    @Override
    public List<FabricStorage> getFabricStorageByOrder(String orderName, String colorName) {
        List<FabricStorage> fabricStorageList = new ArrayList<>();
        try{
            fabricStorageList = fabricReportMapper.getFabricStorageByOrder(orderName, colorName);
            return fabricStorageList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return fabricStorageList;
    }

    @Override
    public List<FabricOutRecord> getFabricOutRecordByOrder(String orderName, String colorName) {
        List<FabricOutRecord> fabricOutRecordList = new ArrayList<>();
        try{
            fabricOutRecordList = fabricReportMapper.getFabricOutRecordByOrder(orderName, colorName);
            return fabricOutRecordList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return fabricOutRecordList;
    }

    @Override
    public List<LooseFabric> getLooseFabricByInfo(Date from, Date to, String orderName) {
        List<LooseFabric> looseFabricList = new ArrayList<>();
        try{
            looseFabricList = fabricReportMapper.getLooseFabricByInfo(from, to, orderName);
            return looseFabricList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return looseFabricList;
    }
}
