package com.example.erp01.service.impl;

import com.example.erp01.mapper.PostTreatmentMapper;
import com.example.erp01.model.PieceWork;
import com.example.erp01.model.PostTreatment;
import com.example.erp01.model.Tailor;
import com.example.erp01.service.PostTreatmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PostTreatmentServiceImpl implements PostTreatmentService {
    @Autowired
    private PostTreatmentMapper postTreatmentMapper;

    @Override
    public Integer addPostTreatmentBatch(List<PostTreatment> postTreatmentList) {
        try{
            if (postTreatmentList != null && !postTreatmentList.isEmpty()){
                postTreatmentMapper.addPostTreatmentBatch(postTreatmentList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;

    }

    @Override
    public List<PostTreatment> getPostTreatmentByOrderName(String orderName) {
        List<PostTreatment> postTreatmentList = new ArrayList<>();
        try{
            postTreatmentList = postTreatmentMapper.getPostTreatmentByOrder(orderName);
            return postTreatmentList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return postTreatmentList;
    }

    @Override
    public List<Tailor> getWellCountByOrderNameList(List<String> orderNameList, String from, String to) {
        List<Tailor> tailorList = new ArrayList<>();
        try{
            if (orderNameList != null && !orderNameList.isEmpty()){
                tailorList = postTreatmentMapper.getWellCountByOrderNameList(orderNameList, from, to);
            }
            return tailorList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return tailorList;
    }

    @Override
    public List<PieceWork> getPieceWorkSummaryByOrderNameList(List<String> orderNameList, List<Integer> procedureNumberList, String from, String to) {
        List<PieceWork> pieceWorkList = new ArrayList<>();
        try{
            if (orderNameList != null && !orderNameList.isEmpty() && procedureNumberList != null && !procedureNumberList.isEmpty()){
                pieceWorkList = postTreatmentMapper.getPieceWorkSummaryByOrderNameList(orderNameList, procedureNumberList, from, to);
            }
            return pieceWorkList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return pieceWorkList;
    }

    @Override
    public List<PostTreatment> getPostTreatmentByOrderNameList(List<String> orderNameList, String from, String to) {
        List<PostTreatment> postTreatmentList = new ArrayList<>();
        try{
            if (orderNameList != null && !orderNameList.isEmpty()){
                postTreatmentList = postTreatmentMapper.getPostTreatmentByOrderNameList(orderNameList, from, to);
            }
            return postTreatmentList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return postTreatmentList;
    }

    @Override
    public List<PostTreatment> getPostTreatmentByOrderType(String orderName, Integer washCount, Integer shipCount, Integer surplusCount, Integer defectiveCount, Integer chickenCount, Integer sampleCount) {
        List<PostTreatment> postTreatmentList = new ArrayList<>();
        try{
            postTreatmentList = postTreatmentMapper.getPostTreatmentByOrderType(orderName, washCount, shipCount, surplusCount, defectiveCount, chickenCount, sampleCount);
            return postTreatmentList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return postTreatmentList;
    }

    @Override
    public Integer deletePostTreatmentById(Integer id) {
        return postTreatmentMapper.deletePostTreatmentById(id);
    }

    @Override
    public Integer updatePostTreatment(PostTreatment postTreatment) {
        try{
            postTreatmentMapper.updatePostTreatment(postTreatment);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }
}
