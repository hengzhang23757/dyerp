package com.example.erp01.service.impl;

import com.example.erp01.mapper.StorageMapper;
import com.example.erp01.model.CutStorageQuery;
import com.example.erp01.model.Storage;
import com.example.erp01.model.StorageFloor;
import com.example.erp01.model.StorageState;
import com.example.erp01.service.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class StorageServiceImpl implements StorageService {
    @Autowired
    private StorageMapper storageMapper;
    @Override
    public int inStore(List<Storage> storageList) {
        try{
            if(storageList!=null && !storageList.isEmpty()){
                storageMapper.inStore(storageList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public int outStore(List<String> subQcodeList) {
        try{
            storageMapper.outStore(subQcodeList);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public int changeStore(String changestoreJson) {
        try{
            storageMapper.changeStore(changestoreJson);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<StorageState> getStorageState() {
        List<StorageState> storageStateList = new ArrayList<>();
        try{
            storageStateList = storageMapper.getStorageState();

            return storageStateList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return storageStateList;
    }

    @Override
    public List<StorageFloor> getStorageFloor() {
        List<StorageFloor> storageFloorList = new ArrayList<>();
        try{
            storageFloorList = storageMapper.getStorageFloor();

            return storageFloorList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return storageFloorList;
    }

    @Override
    public List<String> storageReport(String storehouseLocation) {
        List<String> storageList = new ArrayList<>();
        try{
            storageList = storageMapper.storageReport(storehouseLocation);
            return storageList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return storageList;
    }

    @Override
    public int outStoreOne(String subTailorQcode) {
        try{
            storageMapper.outStoreOne(subTailorQcode);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<CutStorageQuery> getCutStorageByOrder(String orderName, String colorName, String sizeName) {
        List<CutStorageQuery> cutStorageQueryList = new ArrayList<>();
        try{
            cutStorageQueryList = storageMapper.getCutStorageByOrder(orderName, colorName, sizeName);
            return cutStorageQueryList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return cutStorageQueryList;
    }

    @Override
    public List<String> getStorageColorHint(String orderName) {
        List<String> colorList = new ArrayList<>();
        try{
            colorList = storageMapper.getStorageColorHint(orderName);
            return colorList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return colorList;
    }

    @Override
    public List<String> getStorageSizeHint(String orderName, String colorName) {
        List<String> sizeList = new ArrayList<>();
        try{
            sizeList = storageMapper.getStorageSizeHint(orderName, colorName);
            return sizeList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return sizeList;
    }

    @Override
    public int deleteStorageOnFinish(String orderName, Integer bedNumber, String colorName, String sizeName, String storehouseLocation) {
        try{
            storageMapper.deleteStorageOnFinish(orderName, bedNumber, colorName, sizeName, storehouseLocation);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<Storage> getStorageByStoreHouseLocation(String storehouseLocation) {
        List<Storage> storageList = new ArrayList<>();
        try{
            storageList = storageMapper.getStorageByStoreHouseLocation(storehouseLocation);
            return storageList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return storageList;
    }

    @Override
    public List<Storage> getAllStorage() {
        List<Storage> storageList = new ArrayList<>();
        try{
            storageList = storageMapper.getAllStorage();
            return storageList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return storageList;
    }

    @Override
    public List<Storage> getStorageByOrderColorSizeLocation(String orderName, String colorName, String sizeName, String storehouseLocation) {
        List<Storage> storageList = new ArrayList<>();
        try{
            storageList = storageMapper.getStorageByOrderColorSizeLocation(orderName, colorName, sizeName, storehouseLocation);
            return storageList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return storageList;
    }

    @Override
    public Integer deleteStorageById(Integer storageID) {
        try{
            storageMapper.deleteStorageById(storageID);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer deleteStorageByInfo(String orderName, Integer bedNumber, Integer packageNumber) {
        try{
            storageMapper.deleteStorageByInfo(orderName, bedNumber, packageNumber);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<String> getMatch(String orderName, Integer bedNumber, Integer packageNumber) {
        List<String> locationList = new ArrayList<>();
        try{
            locationList = storageMapper.getMatch(orderName, bedNumber, packageNumber);
            return locationList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return locationList;
    }

    @Override
    public List<Storage> getStorageByInfo(String orderName, String colorName, String sizeName) {
        List<Storage> storageList = new ArrayList<>();
        try{
            storageList = storageMapper.getStorageByInfo(orderName, colorName, sizeName);
            return storageList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return storageList;
    }

//    @Override
//    public List<String> getMatch(List<String> tailorQcodeList) {
//
//        List<String> locationList = new ArrayList<>();
//        for(String tailorQcode : tailorQcodeList) {
//            try{
//                String location = storageMapper.getMatch(tailorQcode);
//                locationList.add(location);
//            }catch (Exception e){
//                e.printStackTrace();
//            }
//        }
//        return locationList;
//    }
}
