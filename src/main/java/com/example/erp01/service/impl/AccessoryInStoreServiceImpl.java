package com.example.erp01.service.impl;

import com.example.erp01.mapper.AccessoryInStoreMapper;
import com.example.erp01.model.AccessoryInStore;
import com.example.erp01.service.AccessoryInStoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AccessoryInStoreServiceImpl implements AccessoryInStoreService {

    @Autowired
    private AccessoryInStoreMapper accessoryInStoreMapper;


    @Override
    public int addAccessoryInStoreBatch(List<AccessoryInStore> accessoryInStoreList) {
        try{
            if (accessoryInStoreList != null && !accessoryInStoreList.isEmpty()){
                accessoryInStoreMapper.addAccessoryInStoreBatch(accessoryInStoreList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public int deleteAccessoryInStoreByID(Integer id) {
        try{
            accessoryInStoreMapper.deleteAccessoryInStoreByID(id);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<AccessoryInStore> getAccessoryInStoreByOrder(String orderName) {
        List<AccessoryInStore> accessoryInStoreList = new ArrayList<>();
        try{
            accessoryInStoreList = accessoryInStoreMapper.getAccessoryInStoreByOrder(orderName);
            return accessoryInStoreList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return accessoryInStoreList;
    }

    @Override
    public AccessoryInStore getAccessoryInStoreById(Integer id) {
        AccessoryInStore accessoryInStore = null;
        try{
            accessoryInStore = accessoryInStoreMapper.getAccessoryInStoreById(id);
            return accessoryInStore;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<AccessoryInStore> getAccessoryInStoreByInfo(String orderName, String accessoryName, String specification, String accessoryColor, String colorName, String sizeName, Integer accessoryID) {
        List<AccessoryInStore> accessoryInStoreList = new ArrayList<>();
        try{
            accessoryInStoreList = accessoryInStoreMapper.getAccessoryInStoreByInfo(orderName, accessoryName, specification, accessoryColor, colorName, sizeName, accessoryID);
            return accessoryInStoreList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return accessoryInStoreList;
    }

    @Override
    public List<AccessoryInStore> getTodayAccessoryInStore() {
        List<AccessoryInStore> accessoryInStoreList = new ArrayList<>();
        try{
            accessoryInStoreList = accessoryInStoreMapper.getTodayAccessoryInStore();
            return accessoryInStoreList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return accessoryInStoreList;
    }

    @Override
    public List<AccessoryInStore> getAccessoryInStoreByColorSizeList(String orderName, List<String> colorList, List<String> sizeList) {
        List<AccessoryInStore> accessoryInStoreList = new ArrayList<>();
        try{
            accessoryInStoreList = accessoryInStoreMapper.getAccessoryInStoreByColorSizeList(orderName, colorList, sizeList);
            return accessoryInStoreList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return accessoryInStoreList;
    }

    @Override
    public List<AccessoryInStore> getAccessoryInStoreByTime(String from, String to) {
        List<AccessoryInStore> accessoryInStoreList = new ArrayList<>();
        try{
            accessoryInStoreList = accessoryInStoreMapper.getAccessoryInStoreByTime(from, to);
            return accessoryInStoreList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return accessoryInStoreList;
    }

    @Override
    public List<AccessoryInStore> getAccessoryInStoreByIdList(List<Integer> accessoryIdList) {
        List<AccessoryInStore> accessoryInStoreList = new ArrayList<>();
        try{
            if (accessoryIdList != null && !accessoryIdList.isEmpty()){
                accessoryInStoreList = accessoryInStoreMapper.getAccessoryInStoreByIdList(accessoryIdList);
            }
            return accessoryInStoreList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return accessoryInStoreList;
    }

    @Override
    public Integer updateAccessoryInStoreInChange(AccessoryInStore accessoryInStore) {
        try{
            accessoryInStoreMapper.updateAccessoryInStoreInChange(accessoryInStore);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<AccessoryInStore> getBjInStoreByOrder(String orderName) {
        return accessoryInStoreMapper.getBjInStoreByOrder(orderName);
    }

    @Override
    public List<Integer> getAccessoryIdListByAddFee(List<Integer> idList) {
        List<Integer> accessoryIdList = new ArrayList<>();
        if (idList != null && !idList.isEmpty()){
            accessoryIdList = accessoryInStoreMapper.getAccessoryIdListByAddFee(idList);
        }
        return accessoryIdList;
    }
}
