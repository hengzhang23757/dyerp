package com.example.erp01.service.impl;

import com.example.erp01.mapper.OrderClothesTmpMapper;
import com.example.erp01.model.OrderClothes;
import com.example.erp01.model.OrderClothesTmp;
import com.example.erp01.model.Tailor;
import com.example.erp01.service.OrderClothesTmpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class OrderClothesTmpServiceImpl implements OrderClothesTmpService {

    @Autowired
    private OrderClothesTmpMapper orderClothesTmpMapper;

    @Override
    public Integer addOrderClothesTmpBatch(List<OrderClothesTmp> orderClothesTmpList) {
        try{
            if (orderClothesTmpList != null && !orderClothesTmpList.isEmpty()){
                orderClothesTmpMapper.addOrderClothesTmpBatch(orderClothesTmpList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<String> getYesterdayTailorOrderName() {
        List<String> orderNameList = new ArrayList<>();
        try{
            orderNameList = orderClothesTmpMapper.getYesterdayTailorOrderName();
            return orderNameList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return orderNameList;
    }

    @Override
    public List<Tailor> getTailorTotalInfoAmongYesterday(List<String> orderNameList) {
        List<Tailor> tailorList = new ArrayList<>();
        try{
            if (orderNameList != null && !orderNameList.isEmpty()){
                tailorList = orderClothesTmpMapper.getTailorTotalInfoAmongYesterday(orderNameList);
            }
            return tailorList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return tailorList;
    }

    @Override
    public List<OrderClothes> getOrderClothesByOrderNameList(List<String> orderNameList) {
        List<OrderClothes> orderClothesList = new ArrayList<>();
        try{
            if (orderNameList != null && !orderNameList.isEmpty()){
                orderClothesList = orderClothesTmpMapper.getOrderClothesByOrderNameList(orderNameList);
            }
            return orderClothesList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return orderClothesList;
    }
}
