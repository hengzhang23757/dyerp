package com.example.erp01.service.impl;

import com.example.erp01.mapper.CostAccountMapper;
import com.example.erp01.model.GeneralSalary;
import com.example.erp01.service.CostAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CostAccountServiceImpl implements CostAccountService {

    @Autowired
    private CostAccountMapper costAccountMapper;

    @Override
    public Float getFabricCostByOrder(String orderName) {
        Float fabricCost = costAccountMapper.getFabricCostByOrder(orderName);
        if (fabricCost != null){
            return fabricCost;
        }
        return 0f;
    }

    @Override
    public Float getFabricDeductionCostByOrder(String orderName) {
        Float fabricCost = costAccountMapper.getFabricDeductionCostByOrder(orderName);
        if (fabricCost != null){
            return fabricCost;
        }
        return 0f;
    }

    @Override
    public Float getAccessoryCostByOrder(String orderName) {
        Float accessoryCost = costAccountMapper.getAccessoryCostByOrder(orderName);
        if (accessoryCost != null){
            return accessoryCost;
        }
        return 0f;
    }

    @Override
    public List<GeneralSalary> getPieceWorkSummaryByOrder(String orderName) {
        return costAccountMapper.getPieceWorkSummaryByOrder(orderName);
    }

    @Override
    public List<GeneralSalary> getHourEmpSummaryByOrder(String orderName) {
        return costAccountMapper.getHourEmpSummaryByOrder(orderName);
    }

    @Override
    public Float getFabricOrderCost(String orderName) {
        Float cost = costAccountMapper.getFabricOrderCost(orderName);
        if (cost != null){
            return cost;
        }
        return 0f;
    }

    @Override
    public Float getAccessoryOrderCost(String orderName) {
        Float cost = costAccountMapper.getAccessoryOrderCost(orderName);
        if (cost != null){
            return cost;
        }
        return 0f;
    }

    @Override
    public Float getFabricAddFee(String orderName) {
        Float cost = costAccountMapper.getFabricAddFee(orderName);
        if (cost != null){
            return cost;
        }
        return 0f;
    }

    @Override
    public Float getAccessoryAddFee(String orderName) {
        Float cost = costAccountMapper.getAccessoryAddFee(orderName);
        if (cost != null){
            return cost;
        }
        return 0f;
    }

    @Override
    public Float getFabricCustomerCost(String orderName) {
        Float cost = costAccountMapper.getFabricCustomerCost(orderName);
        if (cost != null){
            return cost;
        }
        return 0f;
    }

    @Override
    public Float getAccessoryCustomerCost(String orderName) {
        Float cost = costAccountMapper.getAccessoryCustomerCost(orderName);
        if (cost != null){
            return cost;
        }
        return 0f;
    }

}
