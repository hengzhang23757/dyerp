package com.example.erp01.service.impl;

import com.example.erp01.mapper.OrderProcedureDetailMapper;
import com.example.erp01.model.OrderProcedureDetail;
import com.example.erp01.service.OrderProcedureDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class OrderProcedureDetailServiceImpl implements OrderProcedureDetailService {


    @Autowired
    private OrderProcedureDetailMapper orderProcedureDetailMapper;


    @Override
    public int addOrderProcedureDetail(List<OrderProcedureDetail> orderProcedureDetailList) {
        try{
            if(orderProcedureDetailList!=null && !orderProcedureDetailList.isEmpty()){
                orderProcedureDetailMapper.addOrderProcedureDetail(orderProcedureDetailList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public int deleteOrderProcedureByOrderName(String orderName) {
        try{
            orderProcedureDetailMapper.deleteOrderProcedureByOrderName(orderName);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<OrderProcedureDetail> getOrderProcedureDetailByOrderName(String orderName) {
        List<OrderProcedureDetail> orderProcedureDetailList = new ArrayList<>();
        try{
            orderProcedureDetailList = orderProcedureDetailMapper.getOrderProcedureDetailByOrderName(orderName);
            return orderProcedureDetailList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return orderProcedureDetailList;
    }

    @Override
    public int updateOrderProcedureDetailBatch(List<OrderProcedureDetail> orderProcedureDetailList) {
        try{
            if(orderProcedureDetailList!=null && !orderProcedureDetailList.isEmpty()){
                orderProcedureDetailMapper.updateOrderProcedureDetailBatch(orderProcedureDetailList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public int deleteOrderProcedureDetailBatch(List<Integer> idList) {
        try{
            if(idList!=null && !idList.isEmpty()){
                orderProcedureDetailMapper.deleteOrderProcedureDetailBatch(idList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }
}
