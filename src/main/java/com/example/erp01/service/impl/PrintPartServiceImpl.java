package com.example.erp01.service.impl;

import com.example.erp01.mapper.PrintPartMapper;
import com.example.erp01.model.PrintPart;
import com.example.erp01.service.PrintPartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PrintPartServiceImpl implements PrintPartService {

    @Autowired
    private PrintPartMapper printPartMapper;

    @Override
    public int addPrintPart(PrintPart printPart) {
        try{
            printPartMapper.addPrintPart(printPart);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public int addPrintPartBatch(List<PrintPart> printPartList) {
        try{
            if (printPartList != null && !printPartList.isEmpty()){
                printPartMapper.addPrintPartBatch(printPartList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public int deletePrintPart(Integer printPartID) {
        try{
            printPartMapper.deletePrintPart(printPartID);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<String> getPrintPartByOrder(String orderName) {
        List<String> printPartList = new ArrayList<>();
        try{
            printPartList = printPartMapper.getPrintPartByOrder(orderName);
            return printPartList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return printPartList;
    }

    @Override
    public List<String> getMainPrintPartByOrder(String orderName) {
        List<String> printPartList = new ArrayList<>();
        try{
            printPartList = printPartMapper.getMainPrintPartByOrder(orderName);
            return printPartList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return printPartList;
    }

    @Override
    public List<String> getOtherPrintPartByOrder(String orderName) {
        List<String> printPartList = new ArrayList<>();
        try{
            printPartList = printPartMapper.getOtherPrintPartByOrder(orderName);
            return printPartList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return printPartList;
    }

    @Override
    public List<PrintPart> getAllPrintPart() {
        List<PrintPart> printPartList = new ArrayList<>();
        try{
            printPartList = printPartMapper.getAllPrintPart();
            return printPartList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return printPartList;
    }

    @Override
    public List<PrintPart> getAllPrintPartByOrder(String orderName) {
        return printPartMapper.getAllPrintPartByOrder(orderName);
    }
}
