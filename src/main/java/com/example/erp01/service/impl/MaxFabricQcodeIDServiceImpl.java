package com.example.erp01.service.impl;


import com.example.erp01.mapper.MaxFabricQcodeIDMapper;
import com.example.erp01.service.MaxFabricQcodeIDService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * Created by hujian on 2019/6/16
 */
@Service
public class MaxFabricQcodeIDServiceImpl implements MaxFabricQcodeIDService {

    @Autowired
    private MaxFabricQcodeIDMapper maxFabricQcodeIDMapper;


    @Override
    public Integer getMaxFabricQcodeId() {
        Integer maxFabricQcodeID = 0;
        try{
            if (maxFabricQcodeIDMapper.getMaxFabricQcodeId() != null){
                maxFabricQcodeID = maxFabricQcodeIDMapper.getMaxFabricQcodeId();
            }
            return maxFabricQcodeID;
        }catch (Exception e){
            e.printStackTrace();
        }
        return maxFabricQcodeID;
    }

    @Override
    public Integer updateMaxFabricQcodeId(int qCodeId) {
        try{
            maxFabricQcodeIDMapper.updateMaxFabricQcodeId(qCodeId);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }
}
