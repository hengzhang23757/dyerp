package com.example.erp01.service.impl;

import com.example.erp01.mapper.AccessoryPreStoreMapper;
import com.example.erp01.model.AccessoryPreStore;
import com.example.erp01.service.AccessoryPreStoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class AccessoryPreStoreServiceImpl implements AccessoryPreStoreService {

    @Autowired
    private AccessoryPreStoreMapper accessoryPreStoreMapper;

    @Override
    public Integer addAccessoryPreStoreBatch(List<AccessoryPreStore> accessoryPreStoreList) {
        try{
            if (accessoryPreStoreList != null && !accessoryPreStoreList.isEmpty()){
                accessoryPreStoreMapper.addAccessoryPreStoreBatch(accessoryPreStoreList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer deleteAccessoryPreStoreById(Integer id) {
        try{
            accessoryPreStoreMapper.deleteAccessoryPreStoreById(id);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer deleteAccessoryPreStoreBatch(List<Integer> idList) {
        try{
            if (idList != null && !idList.isEmpty()){
                accessoryPreStoreMapper.deleteAccessoryPreStoreBatch(idList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer updateAccessoryPreStore(AccessoryPreStore accessoryPreStore) {
        try{
            accessoryPreStoreMapper.updateAccessoryPreStore(accessoryPreStore);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<AccessoryPreStore> getAllAccessoryPreStore() {
        return accessoryPreStoreMapper.getAllAccessoryPreStore();
    }

    @Override
    public List<AccessoryPreStore> getAccessoryPreStoreHint(String subAccessoryName, Integer page, Integer limit) {
        List<AccessoryPreStore> accessoryPreStoreList = new ArrayList<>();
        try{
            accessoryPreStoreList = accessoryPreStoreMapper.getAccessoryPreStoreHint(subAccessoryName, page, limit);
        }catch (Exception e){
            e.printStackTrace();
        }
        return accessoryPreStoreList;
    }

    @Override
    public AccessoryPreStore getAccessoryPreStoreById(Integer id) {
        AccessoryPreStore accessoryPreStore;
        try{
            accessoryPreStore = accessoryPreStoreMapper.getAccessoryPreStoreById(id);
            return accessoryPreStore;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<AccessoryPreStore> getAccessoryPreStoreByPreId(Integer preStoreId) {
        List<AccessoryPreStore> accessoryPreStoreList = new ArrayList<>();
        try{
            accessoryPreStoreList = accessoryPreStoreMapper.getAccessoryPreStoreByPreId(preStoreId);
            return accessoryPreStoreList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return accessoryPreStoreList;
    }

    @Override
    public Integer updateAccessoryPreStoreBatch(List<AccessoryPreStore> accessoryPreStoreList) {
        try{
            if (accessoryPreStoreList != null && !accessoryPreStoreList.isEmpty()){
                accessoryPreStoreMapper.updateAccessoryPreStoreBatch(accessoryPreStoreList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer addAccessoryPreStore(AccessoryPreStore accessoryPreStore) {
        try{
            accessoryPreStoreMapper.addAccessoryPreStore(accessoryPreStore);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<AccessoryPreStore> getAccessoryPreStoreByAccessoryNumber(String accessoryNumber, String operateType) {
        return accessoryPreStoreMapper.getAccessoryPreStoreByAccessoryNumber(accessoryNumber, operateType);
    }

    @Override
    public String getMaxAccessoryOrderByPreFix(String preFix) {
        String maxOrder = "0000";
        try{
            if (accessoryPreStoreMapper.getMaxAccessoryOrderByPreFix(preFix) != null){
                maxOrder = accessoryPreStoreMapper.getMaxAccessoryOrderByPreFix(preFix);
            }
            return maxOrder;
        }catch (Exception e){
            e.printStackTrace();
        }
        return maxOrder;
    }

    @Override
    public List<AccessoryPreStore> getAccessoryStorageByAccessoryNumberList(List<String> accessoryNumberList) {
        List<AccessoryPreStore> accessoryPreStoreList = new ArrayList<>();
        try{
            if (accessoryNumberList != null && !accessoryNumberList.isEmpty()){
                accessoryPreStoreList = accessoryPreStoreMapper.getAccessoryStorageByAccessoryNumberList(accessoryNumberList);
            }
            return accessoryPreStoreList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return accessoryPreStoreList;
    }

    @Override
    public List<AccessoryPreStore> getAccessoryPreStoreByInfo(Map<String, Object> map) {
        return accessoryPreStoreMapper.getAccessoryPreStoreByInfo(map);
    }

    @Override
    public Integer quitCheckAccessoryPreStoreBatch(List<Integer> idList) {
        try{
            if (idList != null && !idList.isEmpty()){
                accessoryPreStoreMapper.quitCheckAccessoryPreStoreBatch(idList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<AccessoryPreStore> getHistoryIndexAccessoryPreStoreByInfo(Map<String, Object> map) {
        return accessoryPreStoreMapper.getHistoryIndexAccessoryPreStoreByInfo(map);
    }

    @Override
    public Integer deleteAccessoryPreStoreByPreId(Integer preStoreId) {
        try{
            accessoryPreStoreMapper.deleteAccessoryPreStoreByPreId(preStoreId);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<AccessoryPreStore> getAccessoryPreStoreForCombine() {
        return accessoryPreStoreMapper.getAccessoryPreStoreForCombine();
    }

    @Override
    public Integer updateAccessoryPreStoreBatchInCombineOrder(List<AccessoryPreStore> accessoryPreStoreList) {
        try{
            if (accessoryPreStoreList != null && !accessoryPreStoreList.isEmpty()){
                accessoryPreStoreMapper.updateAccessoryPreStoreBatchInCombineOrder(accessoryPreStoreList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<AccessoryPreStore> getAccessoryPreStoreForCheck(Map<String, Object> param) {
        return accessoryPreStoreMapper.getAccessoryPreStoreForCheck(param);
    }

    @Override
    public List<AccessoryPreStore> getAccessoryPreStoreByCheckNumber(String checkNumber) {
        return accessoryPreStoreMapper.getAccessoryPreStoreByCheckNumber(checkNumber);
    }

    @Override
    public Integer updateAccessoryPreStoreOrderState(String checkNumber, String orderState) {
        try{
            accessoryPreStoreMapper.updateAccessoryPreStoreOrderState(checkNumber, orderState);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer updateAccessoryPreStoreCheckStateByCheckNumber(String checkNumber, String checkState) {
        try{
            accessoryPreStoreMapper.updateAccessoryPreStoreCheckStateByCheckNumber(checkNumber, checkState);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer getAccessoryPreStoreCheckCount() {
        int checkCount = 0;
        try{
            if (accessoryPreStoreMapper.getAccessoryPreStoreCheckCount() != null){
                checkCount = accessoryPreStoreMapper.getAccessoryPreStoreCheckCount();
            }
            return checkCount;
        }catch (Exception e){
            e.printStackTrace();
        }
        return checkCount;
    }

    @Override
    public AccessoryPreStore getMaxStorageAccessoryPreStoreByAccessoryNumber(String accessoryNumber, String specification, String accessoryColor) {
        return accessoryPreStoreMapper.getMaxStorageAccessoryPreStoreByAccessoryNumber(accessoryNumber, specification, accessoryColor);
    }

    @Override
    public Integer updateAccessoryAfterCommit(Integer id, Float accessoryCount, Float price) {
        try{
            accessoryPreStoreMapper.updateAccessoryAfterCommit(id, accessoryCount, price);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer updateAccessoryPreStoreInMonthCheck(List<AccessoryPreStore> accessoryPreStoreList) {
        try{
            if (accessoryPreStoreList != null && !accessoryPreStoreList.isEmpty()){
                accessoryPreStoreMapper.updateAccessoryPreStoreInMonthCheck(accessoryPreStoreList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }


}
