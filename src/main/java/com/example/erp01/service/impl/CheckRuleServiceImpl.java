package com.example.erp01.service.impl;

import com.example.erp01.mapper.CheckRuleMapper;
import com.example.erp01.model.CheckRule;
import com.example.erp01.service.CheckRuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
@Service
public class CheckRuleServiceImpl implements CheckRuleService {

    @Autowired
    private CheckRuleMapper checkRuleMapper;

    @Override
    public List<CheckRule> getAllCheckRule() {
        List<CheckRule> checkRuleList = new ArrayList<>();
        try{
            checkRuleList = checkRuleMapper.getAllCheckRule();
            return checkRuleList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return checkRuleList;

    }

    @Override
    public Integer updateCheckRule(CheckRule checkRule) {
        try{
            if (checkRule != null){
                checkRuleMapper.updateCheckRule(checkRule);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }
}
