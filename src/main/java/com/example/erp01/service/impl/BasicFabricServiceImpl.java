package com.example.erp01.service.impl;

import com.example.erp01.mapper.BasicFabricMapper;
import com.example.erp01.model.BasicFabric;
import com.example.erp01.service.BasicFabricService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class BasicFabricServiceImpl implements BasicFabricService {

    @Autowired
    private BasicFabricMapper basicFabricMapper;

    @Override
    public int addBasicFabricBatch(List<BasicFabric> basicFabricList) {
        try{
            if (basicFabricList != null && !basicFabricList.isEmpty()){
                basicFabricMapper.addBasicFabricBatch(basicFabricList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;

    }

    @Override
    public List<BasicFabric> getAllBasicFabric() {
        List<BasicFabric> basicFabricList = new ArrayList<>();
        try{
            basicFabricList = basicFabricMapper.getAllBasicFabric();
            return basicFabricList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return basicFabricList;
    }

    @Override
    public List<BasicFabric> getThreeMonthsBasicFabric() {
        List<BasicFabric> basicFabricList = new ArrayList<>();
        try{
            basicFabricList = basicFabricMapper.getThreeMonthsBasicFabric();
            return basicFabricList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return basicFabricList;
    }

    @Override
    public int deleteBasicFabric(Integer basicFabricID) {
        try{
            basicFabricMapper.deleteBasicFabric(basicFabricID);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public int updateBasicFabric(BasicFabric basicFabric) {
        try{
            basicFabricMapper.updateBasicFabric(basicFabric);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<String> getBasicFabricHint(String subFabricName) {
        List<String> basicFabricNameList = new ArrayList<>();
        try{
            basicFabricNameList = basicFabricMapper.getBasicFabricHint(subFabricName);
            return basicFabricNameList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return basicFabricNameList;
    }

    @Override
    public List<String> getFabricSupplierHint(String basicFabricName) {
        List<String> supplierList = new ArrayList<>();
        try{
            supplierList = basicFabricMapper.getFabricSupplierHint(basicFabricName);
            return supplierList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return supplierList;
    }

    @Override
    public String getBasicFabricNumberByNameSupplier(String basicFabricName, String basicFabricSupplier) {
        String basicFabricNumber;
        try{
            basicFabricNumber = basicFabricMapper.getBasicFabricNumberByNameSupplier(basicFabricName, basicFabricSupplier);
            return basicFabricNumber;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<String> getBasicFabricColorByNameSupplier(String basicFabricName, String basicFabricSupplier) {
        List<String> basicFabricColorList = new ArrayList<>();
        try{
            basicFabricColorList = basicFabricMapper.getBasicFabricColorByNameSupplier(basicFabricName, basicFabricSupplier);
            return basicFabricColorList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return basicFabricColorList;
    }

    @Override
    public String getBasicFabricColorNumberByNameSupplierColor(String basicFabricName, String basicFabricSupplier, String basicFabricColor) {
        String basicFabricColorNumber;
        try{
            basicFabricColorNumber = basicFabricMapper.getBasicFabricColorNumberByNameSupplierColor(basicFabricName, basicFabricSupplier, basicFabricColor);
            return basicFabricColorNumber;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<String> getBasicFabricWidthByNameSupplierColor(String basicFabricName, String basicFabricSupplier, String basicFabricColor) {
        List<String> basicFabricWidthList = new ArrayList<>();
        try{
            basicFabricWidthList = basicFabricMapper.getBasicFabricWidthByNameSupplierColor(basicFabricName, basicFabricSupplier, basicFabricColor);
            return basicFabricWidthList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return basicFabricWidthList;
    }

    @Override
    public List<String> getBasicFabricWeightByNameSupplierColorWidth(String basicFabricName, String basicFabricSupplier, String basicFabricColor, String basicFabricWidth) {
        List<String> basicFabricWeightList = new ArrayList<>();
        try{
            basicFabricWeightList = basicFabricMapper.getBasicFabricWeightByNameSupplierColorWidth(basicFabricName, basicFabricSupplier, basicFabricColor, basicFabricWidth);
            return basicFabricWeightList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return basicFabricWeightList;
    }
}
