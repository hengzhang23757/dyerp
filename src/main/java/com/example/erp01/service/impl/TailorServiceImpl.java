package com.example.erp01.service.impl;

import com.example.erp01.mapper.TailorMapper;
import com.example.erp01.model.*;
import com.example.erp01.service.TailorService;
import com.sun.corba.se.impl.ior.OldJIDLObjectKeyTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.*;

@com.alibaba.dubbo.config.annotation.Service(version = "1.0.0",timeout=3000)
@Service
public class TailorServiceImpl implements TailorService {

    @Autowired
    private TailorMapper tailorMapper;

    @Override
    public int saveTailorData(List<Tailor> tailorList) {
        try{
            if (tailorList != null && !tailorList.isEmpty()){
                tailorMapper.saveTailorData(tailorList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer getMaxBedNumber(String orderName) {
        try{
            Integer bedNumber = tailorMapper.getMaxBedNumber(orderName);
            return bedNumber;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

//    @Override
//    public List<Tailor> getTailorByOrderNameBedNumPart(String orderName, int bedNumber, String partName) {
//        List<Tailor> tailorList = new ArrayList<>();
//
//        try{
//            tailorList = tailorMapper.getTailorByOrderNameBedNumPart(orderName, bedNumber, partName);
//            return tailorList;
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//        return tailorList;
//    }

    @Override
    public Integer getMaxPackageNumberByOrder(String orderName, Integer tailorType) {
        Integer maxPackageNumber = null;
        try{
            maxPackageNumber = tailorMapper.getMaxPackageNumberByOrder(orderName, tailorType);
            return maxPackageNumber;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

//    @Override
//    public String getOneTailorQcodeByTailorQcodeID(Integer tailorQcodeID) {
//        try{
//            String tailorQcode = tailorMapper.getOneTailorQcodeByTailorQcodeID(tailorQcodeID);
//            return tailorQcode;
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//        return null;
//    }

//    @Override
//    public List<Tailor> getDetailTailorByOrderNameBedNumber(String orderName, int bedNumber) {
//        List<Tailor> tailorList = new ArrayList<>();
//        try{
//            tailorList = tailorMapper.getDetailTailorByOrderNameBedNumber(orderName, bedNumber);
//            return tailorList;
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//        return null;
//    }

    @Override
    public int deleteTailor(Integer tailorID) {
        try{
            tailorMapper.deleteTailor(tailorID);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public int deleteTailorByOrderBed(String orderName, Integer bedNumber) {
        try{
            tailorMapper.deleteTailorByOrderBed(orderName, bedNumber);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public int deleteTailorByOrderBedPack(String orderName, Integer bedNumber, List<Integer> packageNumberList) {
        try{
            Map<String, Object> map = new HashMap<>();
            map.put("orderName",orderName);
            map.put("bedNumber",bedNumber);
            map.put("list",packageNumberList);
            tailorMapper.deleteTailorByOrderBedPack(map);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

//    @Override
//    public List<Tailor> getAllTailorByOrderBed(String orderName, Integer bedNumber, Integer packageNumber) {
//        List<Tailor> tailorList = new ArrayList<>();
//        try{
//            tailorList = tailorMapper.getAllTailorByOrderBed(orderName, bedNumber, packageNumber);
//            return tailorList;
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//        return tailorList;
//    }

//    @Override
//    public List<TailorSummary> getTailorSummaryByOrderBed(String orderName, Integer bedNumber) {
//        List<TailorSummary> tailorSummaryList = new ArrayList<>();
//        try{
//            tailorSummaryList = tailorMapper.getTailorSummaryByOrderBed(orderName, bedNumber);
//            return tailorSummaryList;
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//        return tailorSummaryList;
//    }

    @Override
    public List<Integer> getPackageNumbersByOrderBedColor(String orderName, Integer bedNumber, String colorName) {
        List<Integer> packageNumberList = new ArrayList<>();
        try{
            packageNumberList = tailorMapper.getPackageNumbersByOrderBedColor(orderName, bedNumber, colorName);
            return packageNumberList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return packageNumberList;
    }

//    @Override
//    public Integer getLayerCountByOrderBedPackage(String orderName, Integer bedNumber, Integer packageNumber) {
//        Integer layerCount = 0;
//        try{
//            layerCount = tailorMapper.getLayerCountByOrderBedPackage(orderName, bedNumber, packageNumber);
//            return layerCount;
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//        return layerCount;
//    }


//    @Override
//    public List<Tailor> getAllTailorByOrderColorSizePart(String orderName, String colorName, String sizeName, String partName) {
//        List<Tailor> tailorList = new ArrayList<>();
//        try{
//            tailorList = tailorMapper.getAllTailorByOrderColorSizePart(orderName, colorName, sizeName, partName);
//            return tailorList;
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//        return tailorList;
//    }

    @Override
    public Integer updateTailorData(List<Tailor> tailorList) {
        try{
            if (tailorList != null && !tailorList.isEmpty()){
                tailorMapper.updateTailorData(tailorList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<CutLocation> getPackageInfo(String orderName, Integer bedNumber, String colorName, String sizeName, Integer packageNumber) {
        List<CutLocation> cutLocationList = new ArrayList<>();
        try{
            cutLocationList = tailorMapper.getPackageInfo(orderName, bedNumber,colorName, sizeName, packageNumber);
            return cutLocationList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return cutLocationList;
    }


//    @Override
//    public List<Tailor> getTailorDataByOrderBed(String orderName, Integer bedNumber) {
//        List<Tailor> tailorList = new ArrayList<>();
//        try{
//            tailorList = tailorMapper.getTailorDataByOrderBed(orderName, bedNumber);
//            return tailorList;
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//        return tailorList;
//    }

    @Override
    public Tailor getTailorByTailorQcodeID(Integer tailorQcodeID, Integer tailorType) {
        Tailor tailor = null;
        try{
            tailor = tailorMapper.getTailorByTailorQcodeID(tailorQcodeID, tailorType);
            return tailor;
        }catch (Exception e){
            e.printStackTrace();
        }
        return tailor;
    }

    @Override
    public List<Tailor> getTailorsByTailorQcodeIDs(List<Integer> tailorQcodeIDList, Integer tailorType) {
        List<Tailor> tailorList = new ArrayList<>();
        try{
            if (tailorQcodeIDList != null && !tailorQcodeIDList.isEmpty()){
                tailorList = tailorMapper.getTailorsByTailorQcodeIDs(tailorQcodeIDList, tailorType);
            }
            return tailorList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return tailorList;
    }

    @Override
    public List<Tailor> getBedInfoInDelete(String orderName) {
        List<Tailor> tailorList = new ArrayList<>();
        try{
            tailorList = tailorMapper.getBedInfoInDelete(orderName);
            return tailorList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return tailorList;
    }

    @Override
    public List<Tailor> getPackageInfoInDelete(String orderName, Integer bedNumber) {
        List<Tailor> tailorList = new ArrayList<>();
        try{
            tailorList = tailorMapper.getPackageInfoInDelete(orderName, bedNumber);
            return tailorList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return tailorList;
    }

    @Override
    public List<Integer> getBedNumbersByOrderNamePartNameTailorType(String orderName, String partName, Integer tailorType) {
        List<Integer> bedNumberList = new ArrayList<>();
        try{
            bedNumberList = tailorMapper.getBedNumbersByOrderNamePartNameTailorType(orderName, partName, tailorType);
            return bedNumberList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return bedNumberList;
    }

    @Override
    public List<TailorReport> tailorReport(String orderName, Integer bedNumber, Integer tailorType) {
        List<TailorReport> tailorReportList = new ArrayList<>();
        try{
            tailorReportList = tailorMapper.tailorReport(orderName, bedNumber, tailorType);
            return tailorReportList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return tailorReportList;
    }

    @Override
    public List<CutQueryLeak> getTailorInfoByOrderPartType(String orderName, String partName, Integer tailorType, String from, String to) {
        List<CutQueryLeak> cutQueryLeakList = new ArrayList<>();
        try{
            cutQueryLeakList = tailorMapper.getTailorInfoByOrderPartType(orderName, partName, tailorType, from, to);
            return cutQueryLeakList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return cutQueryLeakList;
    }

    @Override
    public List<TailorMonthReport> tailorMonthReport(Date from, Date to, String orderName, String partName, Integer tailorType, String groupName) {
        List<TailorMonthReport> tailorMonthReportList = new ArrayList<>();
        try{
            tailorMonthReportList = tailorMapper.tailorMonthReport(from, to, orderName, partName, tailorType, groupName);
            return tailorMonthReportList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return tailorMonthReportList;
    }

    @Override
    public List<TailorMonthReport> otherTailorMonthReport(Date from, Date to, String orderName, String partName, Integer tailorType, String groupName) {
        List<TailorMonthReport> tailorMonthReportList = new ArrayList<>();
        try{
            tailorMonthReportList = tailorMapper.otherTailorMonthReport(from, to, orderName, partName, tailorType, groupName);
            return tailorMonthReportList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return tailorMonthReportList;
    }

    @Override
    public List<Tailor> getTailorInfoGroupByColorSizePartType(String orderName, String partName, Integer tailorType) {
        List<Tailor> tailorList = new ArrayList<>();
        try{
            tailorList = tailorMapper.getTailorInfoGroupByColorSizePartType(orderName, partName, tailorType);
            return tailorList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return tailorList;
    }

    @Override
    public List<Tailor> getTailorInfoGroupByColorPartType(String orderName, String partName, Integer tailorType) {
        List<Tailor> tailorList = new ArrayList<>();
        try{
            tailorList = tailorMapper.getTailorInfoGroupByColorPartType(orderName, partName, tailorType);
            return tailorList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return tailorList;
    }

    @Override
    public List<Completion> getTailorCompletion(String orderName, String partName, Integer tailorType) {
        List<Completion> completionList = new ArrayList<>();
        try{
            completionList = tailorMapper.getTailorCompletion(orderName, partName, tailorType);
            return completionList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return completionList;
    }

    @Override
    public List<Tailor> getTailorByInfo(String orderName, String colorName, String sizeName, String partName, Integer bedNumber, Integer packageNumber, Integer tailorType, Integer isDelete) {
        List<Tailor> tailorList = new ArrayList<>();
        try{
            tailorList = tailorMapper.getTailorByInfo(orderName, colorName, sizeName, partName, bedNumber, packageNumber, tailorType, isDelete);
            return tailorList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return tailorList;
    }

    @Override
    public List<WeekAmount> getTailorWeekAmount(String partName, Integer tailorType) {
        List<WeekAmount> weekAmountList = new ArrayList<>();
        try{
            weekAmountList = tailorMapper.getTailorWeekAmount(partName, tailorType);
            return weekAmountList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return weekAmountList;
    }

    @Override
    public Integer getTailorCountByInfo(String orderName, String colorName, String sizeName, String partName, Integer bedNumber, Integer packageNumber, Integer tailorType) {
        Integer tailorCount = 0;
        try{
            if (tailorMapper.getTailorCountByInfo(orderName, colorName, sizeName, partName, bedNumber, packageNumber, tailorType) != null){
                tailorCount = tailorMapper.getTailorCountByInfo(orderName, colorName, sizeName, partName, bedNumber, packageNumber, tailorType);
            }
            return tailorCount;
        } catch (Exception e){
            e.printStackTrace();
        }
        return tailorCount;
    }

    @Override
    public Integer getWellCountByInfo(String orderName, String colorName, String sizeName, String partName, Integer bedNumber, Integer packageNumber, Integer tailorType) {
        Integer wellCount = 0;
        try{
            if (tailorMapper.getWellCountByInfo(orderName, colorName, sizeName, partName, bedNumber, packageNumber, tailorType) != null){
                wellCount = tailorMapper.getWellCountByInfo(orderName, colorName, sizeName, partName, bedNumber, packageNumber, tailorType);
            }
            return wellCount;
        } catch (Exception e){
            e.printStackTrace();
        }
        return wellCount;
    }

    @Override
    public Integer preDeleteInDeleteBed(String orderName, Integer bedNumber) {
        try{
            tailorMapper.preDeleteInDeleteBed(orderName, bedNumber);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer preDeleteInDeletePackage(String orderName, Integer bedNumber, List<Integer> packageNumberList) {
        try{
            Map<String, Object> map = new HashMap<>();
            map.put("orderName",orderName);
            map.put("bedNumber",bedNumber);
            map.put("list",packageNumberList);
            tailorMapper.preDeleteInDeletePackage(map);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer getWellCountByColorSizeList(String orderName, List<String> colorList, List<String> sizeList) {
        Integer wellCount = 0;
        try{
            if (tailorMapper.getWellCountByColorSizeList(orderName, colorList, sizeList) != null){
                wellCount = tailorMapper.getWellCountByColorSizeList(orderName, colorList, sizeList);
            }
            return wellCount;
        } catch (Exception e){
            e.printStackTrace();
        }
        return wellCount;
    }

    @Override
    public List<Tailor> getTailorByOrderPartNameColorSize(String orderName, Integer tailorType, String partName, String colorName, String sizeName, Integer isDelete) {
        List<Tailor> tailorList = new ArrayList<>();
        try{
            tailorList = tailorMapper.getTailorByOrderPartNameColorSize(orderName, tailorType, partName, colorName, sizeName, isDelete);
            return tailorList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return tailorList;
    }

    @Override
    public List<TailorMatch> getRecentWeekCutInfo() {
        List<TailorMatch> tailorMatchList = new ArrayList<>();
        try{
            tailorMatchList = tailorMapper.getRecentWeekCutInfo();
            return tailorMatchList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return tailorMatchList;
    }

    @Override
    public List<TailorMatch> getMainTailorMatchDataByInfo(List<String> orderNameList) {
        List<TailorMatch> tailorMatchList = new ArrayList<>();
        try{
            if (orderNameList != null && !orderNameList.isEmpty()){
                tailorMatchList = tailorMapper.getMainTailorMatchDataByInfo(orderNameList);
            }
            return tailorMatchList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return tailorMatchList;
    }

    @Override
    public List<TailorMatch> getOtherTailorMatchDataByInfo(List<String> orderNameList) {
        List<TailorMatch> tailorMatchList = new ArrayList<>();
        try{
            if (orderNameList != null && !orderNameList.isEmpty()){
                tailorMatchList = tailorMapper.getOtherTailorMatchDataByInfo(orderNameList);
            }
            return tailorMatchList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return tailorMatchList;
    }

    @Override
    public List<CutQueryLeak> getTailorInfoGroupByColorSize(String orderName, String colorName, String sizeName, String partName, Integer tailorType) {
        List<CutQueryLeak> cutQueryLeakList = new ArrayList<>();
        try{
            cutQueryLeakList = tailorMapper.getTailorInfoGroupByColorSize(orderName, colorName, sizeName, partName, tailorType);
            return cutQueryLeakList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return cutQueryLeakList;
    }

    @Override
    public Date getLastCutDateByOrderBed(String orderName, Integer bedNumber) {
        Date cutDate = null;
        try{
            cutDate = tailorMapper.getLastCutDateByOrderBed(orderName, bedNumber);
            return cutDate;
        }catch (Exception e){
            e.printStackTrace();
        }
        return cutDate;
    }

    @Override
    public Integer getMaxBedNumberByOrderTailorType(String orderName, Integer tailorType) {
        Integer bedNumber;
        if (tailorType.equals(0)){
            bedNumber = 0;
        } else {
            bedNumber = 10000;
        }
        try{
            if (tailorMapper.getMaxBedNumberByOrderTailorType(orderName, tailorType) != null){
                if (tailorType.equals(1) && tailorMapper.getMaxBedNumberByOrderTailorType(orderName, tailorType) < 10000){
                    bedNumber = 10000;
                } else {
                    bedNumber = tailorMapper.getMaxBedNumberByOrderTailorType(orderName, tailorType);
                }
            }
            return bedNumber;
        }catch (Exception e){
            e.printStackTrace();
        }
        return bedNumber;
    }

    @Override
    public List<Tailor> monthTailorReportGroupByBedNumber(String from, String to, String orderName, String groupName) {
        List<Tailor> tailorList = new ArrayList<>();
        try{
            tailorList = tailorMapper.monthTailorReportGroupByBedNumber(from, to, orderName, groupName);
            return tailorList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return tailorList;
    }

    @Override
    public List<Tailor> otherMonthTailorReportGroupByBedNumber(String from, String to, String orderName, String groupName) {
        List<Tailor> tailorList = new ArrayList<>();
        try{
            tailorList = tailorMapper.otherMonthTailorReportGroupByBedNumber(from, to, orderName, groupName);
            return tailorList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return tailorList;
    }

    @Override
    public Integer updateTailorLayerCountByInfo(String orderName, Integer bedNumber, Integer packageNumber, Integer layerCount) {
        try{
            tailorMapper.updateTailorLayerCountByInfo(orderName, bedNumber, packageNumber, layerCount);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<Tailor> getTailorSummaryByOrderByOrderList(List<String> orderNameList) {
        List<Tailor> tailorList = new ArrayList<>();
        if (orderNameList != null && !orderNameList.isEmpty()){
            tailorList = tailorMapper.getTailorSummaryByOrderByOrderList(orderNameList);
        }
        return tailorList;
    }

}
