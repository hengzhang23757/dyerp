package com.example.erp01.service.impl;

import com.example.erp01.mapper.GroupProgressMapper;
import com.example.erp01.model.GroupProgress;
import com.example.erp01.service.GroupProgressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class GroupProgressServiceImpl implements GroupProgressService {

    @Autowired
    private GroupProgressMapper groupProgressMapper;


    @Override
    public List<GroupProgress> getGroupEmbOutByInfo(Date from, Date to, String orderName, List<String> groupList) {
        List<GroupProgress> groupProgressList = new ArrayList<>();
        try{
            groupProgressList = groupProgressMapper.getGroupEmbOutByInfo(from, to, orderName, groupList);
            return groupProgressList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return groupProgressList;
    }

    @Override
    public List<GroupProgress> getAllGroupEmbOutByInfo(Date from, Date to, String orderName) {
        List<GroupProgress> groupProgressList = new ArrayList<>();
        try{
            groupProgressList = groupProgressMapper.getAllGroupEmbOutByInfo(from, to, orderName);
            return groupProgressList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return groupProgressList;
    }

    @Override
    public List<GroupProgress> getGroupHangCountByInfo(Date from, Date to, String orderName, List<String> groupList) {
        List<GroupProgress> groupProgressList = new ArrayList<>();
        try{
            groupProgressList = groupProgressMapper.getGroupHangCountByInfo(from, to, orderName, groupList);
            return groupProgressList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return groupProgressList;
    }

    @Override
    public List<GroupProgress> getAllGroupHangCountByInfo(Date from, Date to, String orderName) {
        List<GroupProgress> groupProgressList = new ArrayList<>();
        try{
            groupProgressList = groupProgressMapper.getAllGroupHangCountByInfo(from, to, orderName);
            return groupProgressList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return groupProgressList;
    }

    @Override
    public List<GroupProgress> getGroupInspectionCountByInfo(Date from, Date to, String orderName, List<String> groupList) {
        List<GroupProgress> groupProgressList = new ArrayList<>();
        try{
            groupProgressList = groupProgressMapper.getGroupInspectionCountByInfo(from, to, orderName, groupList);
            return groupProgressList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return groupProgressList;
    }

    @Override
    public List<GroupProgress> getAllGroupInspectionCountByInfo(Date from, Date to, String orderName) {
        List<GroupProgress> groupProgressList = new ArrayList<>();
        try{
            groupProgressList = groupProgressMapper.getAllGroupInspectionCountByInfo(from, to, orderName);
            return groupProgressList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return groupProgressList;
    }

    @Override
    public List<GroupProgress> getEachDayGroupEmbOutByInfo(Date from, Date to, String orderName, List<String> groupList) {
        List<GroupProgress> groupProgressList = new ArrayList<>();
        try{
            groupProgressList = groupProgressMapper.getEachDayGroupEmbOutByInfo(from, to, orderName, groupList);
            return groupProgressList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return groupProgressList;
    }

    @Override
    public List<GroupProgress> getEachDayGroupHangCountByInfo(Date from, Date to, String orderName, List<String> groupList) {
        List<GroupProgress> groupProgressList = new ArrayList<>();
        try{
            groupProgressList = groupProgressMapper.getEachDayGroupHangCountByInfo(from, to, orderName, groupList);
            return groupProgressList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return groupProgressList;
    }

    @Override
    public List<GroupProgress> getEachDayGroupInspectionCountByInfo(Date from, Date to, String orderName, List<String> groupList) {
        List<GroupProgress> groupProgressList = new ArrayList<>();
        try{
            groupProgressList = groupProgressMapper.getEachDayGroupInspectionCountByInfo(from, to, orderName, groupList);
            return groupProgressList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return groupProgressList;
    }

    @Override
    public List<GroupProgress> getGroupCheckCountByInfo(Date from, Date to, String orderName, List<String> groupList) {
        List<GroupProgress> groupProgressList = new ArrayList<>();
        try{
            groupProgressList = groupProgressMapper.getGroupCheckCountByInfo(from, to, orderName, groupList);
            return groupProgressList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return groupProgressList;
    }

    @Override
    public List<GroupProgress> getAllGroupCheckCountByInfo(Date from, Date to, String orderName) {
        List<GroupProgress> groupProgressList = new ArrayList<>();
        try{
            groupProgressList = groupProgressMapper.getAllGroupCheckCountByInfo(from, to, orderName);
            return groupProgressList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return groupProgressList;
    }

    @Override
    public List<GroupProgress> getEachDayGroupCheckCountByInfo(Date from, Date to, String orderName, List<String> groupList) {
        List<GroupProgress> groupProgressList = new ArrayList<>();
        try{
            groupProgressList = groupProgressMapper.getEachDayGroupCheckCountByInfo(from, to, orderName, groupList);
            return groupProgressList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return groupProgressList;
    }

    @Override
    public List<GroupProgress> getGroupProgressByTimeProcedure(String from, String to, String orderName, List<String> groupList, List<Integer> procedureNumberList) {
        return groupProgressMapper.getGroupProgressByTimeProcedure(from, to, orderName, groupList, procedureNumberList);
    }

    @Override
    public List<GroupProgress> getGroupProgressByTimeProcedureGroupByDay(String from, String to, String orderName, List<String> groupList, List<Integer> procedureNumberList) {
        return groupProgressMapper.getGroupProgressByTimeProcedureGroupByDay(from, to, orderName, groupList, procedureNumberList);
    }
}
