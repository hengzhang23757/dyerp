package com.example.erp01.service.impl;

import com.example.erp01.mapper.EndProductPayMapper;
import com.example.erp01.model.EndProductPay;
import com.example.erp01.service.EndProductPayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class EndProductPayServiceImpl implements EndProductPayService {

    @Autowired
    private EndProductPayMapper endProductPayMapper;

    @Override
    public Integer addEndProductPay(EndProductPay endProductPay) {
        try{
            endProductPayMapper.addEndProductPay(endProductPay);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer addEndProductPayBatch(List<EndProductPay> endProductPayList) {
        try{
            if (endProductPayList != null && !endProductPayList.isEmpty()){
                endProductPayMapper.addEndProductPayBatch(endProductPayList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer deleteEndProductPay(Integer id) {
        try{
            endProductPayMapper.deleteEndProductPay(id);
            return 0;
        }catch (Exception E){
            E.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer updateEndProductPay(EndProductPay endProductPay) {
        try{
            endProductPayMapper.updateEndProductPay(endProductPay);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<EndProductPay> getEndProductPaySummary() {
        List<EndProductPay> endProductPayList = new ArrayList<>();
        try{
            endProductPayList = endProductPayMapper.getEndProductPaySummary();
            return endProductPayList;
        } catch (Exception e){
            e.printStackTrace();
        }
        return endProductPayList;
    }

    @Override
    public Integer getEndProductPayCountByOrder(String orderName) {
        Integer payCount = 0;
        try{
            if (endProductPayMapper.getEndProductPayCountByOrder(orderName) != null){
                payCount = endProductPayMapper.getEndProductPayCountByOrder(orderName);
            }
            return payCount;
        }catch (Exception e){
            e.printStackTrace();
        }
        return payCount;
    }

    @Override
    public List<EndProductPay> getEndProductPayByOrderName(String orderName) {
        List<EndProductPay> endProductPayList = new ArrayList<>();
        try{
            endProductPayList = endProductPayMapper.getEndProductPayByOrderName(orderName);
            return endProductPayList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return endProductPayList;
    }

    @Override
    public Integer deleteEndProductPayBatch(List<Integer> idList) {
        try{
            if (idList != null && !idList.isEmpty()){
                endProductPayMapper.deleteEndProductPayBatch(idList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<EndProductPay> getEndProductPayByInfo(String customerName, String payMonth, String orderName, String payType) {
        return endProductPayMapper.getEndProductPayByInfo(customerName, payMonth, orderName, payType);
    }

    @Override
    public Integer deleteEndProductPayByPayNumber(String payNumber) {
        try{
            endProductPayMapper.deleteEndProductPayByPayNumber(payNumber);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public EndProductPay getEndProductPayByPayNumber(String payNumber) {
        return endProductPayMapper.getEndProductPayByPayNumber(payNumber);
    }

    @Override
    public List<EndProductPay> monthSummaryByCustomer(String payMonth) {
        return endProductPayMapper.monthSummaryByCustomer(payMonth);
    }

}
