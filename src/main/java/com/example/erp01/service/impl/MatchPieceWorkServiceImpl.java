package com.example.erp01.service.impl;

import com.example.erp01.mapper.MatchPieceWorkMapper;
import com.example.erp01.model.MatchPieceWork;
import com.example.erp01.service.MatchPieceWorkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MatchPieceWorkServiceImpl implements MatchPieceWorkService {

    @Autowired
    private MatchPieceWorkMapper matchPieceWorkMapper;

    @Override
    public Integer addMatchPieceWorkBatch(List<MatchPieceWork> matchPieceWorkList) {
        try{
            if (matchPieceWorkList != null && matchPieceWorkList.size() > 0){
                matchPieceWorkMapper.addMatchPieceWorkBatch(matchPieceWorkList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<MatchPieceWork> getMatchPieceWorkSummary() {
        List<MatchPieceWork> matchPieceWorkList = new ArrayList<>();
        try{
            matchPieceWorkList = matchPieceWorkMapper.getMatchPieceWorkSummary();
            return matchPieceWorkList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return matchPieceWorkList;
    }

    @Override
    public Integer deleteMatchPieceWorkByInfo(String orderName, String colorName, String sizeName) {
        try{
            matchPieceWorkMapper.deleteMatchPieceWorkByInfo(orderName, colorName, sizeName);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer deleteMatchPieceWorkByIdList(List<Integer> pieceWorkIdList) {
        try{
            if (pieceWorkIdList != null && pieceWorkIdList.size() > 0){
                matchPieceWorkMapper.deleteMatchPieceWorkByIdList(pieceWorkIdList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<MatchPieceWork> getMatchPieceWorkByInfo(String orderName, String colorName, String sizeName) {
        List<MatchPieceWork> matchPieceWorkList = new ArrayList<>();
        try{
            matchPieceWorkList = matchPieceWorkMapper.getMatchPieceWorkByInfo(orderName, colorName, sizeName);
            return matchPieceWorkList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return matchPieceWorkList;
    }

    @Override
    public Integer deleteMatchPieceWorkById(Integer pieceWorkID) {
        try{
            matchPieceWorkMapper.deleteMatchPieceWorkById(pieceWorkID);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer addMatchPieceWork(MatchPieceWork matchPieceWork) {
        try{
            matchPieceWorkMapper.addMatchPieceWork(matchPieceWork);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }
}
