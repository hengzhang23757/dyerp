package com.example.erp01.service.impl;

import com.example.erp01.mapper.EmbPlanMapper;
import com.example.erp01.model.EmbPlan;
import com.example.erp01.service.EmbPlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class EmbPlanServiceImpl implements EmbPlanService {

    @Autowired
    private EmbPlanMapper embPlanMapper;

    @Override
    public Integer addEmbPlanBatch(List<EmbPlan> embPlanList) {
        try{
            if (embPlanList.size() != 0 && embPlanList != null){
                embPlanMapper.addEmbPlanBatch(embPlanList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer addEmbPlan(EmbPlan embPlan) {
        try{
            if (embPlan != null){
                embPlanMapper.addEmbPlan(embPlan);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer deleteEmbPlan(Integer embPlanID) {
        try{
            embPlanMapper.deleteEmbPlan(embPlanID);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<EmbPlan> getTodayEmbPlan() {
        List<EmbPlan> embPlanList = new ArrayList<>();
        try{
            embPlanList = embPlanMapper.getTodayEmbPlan();
            return embPlanList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return embPlanList;
    }

    @Override
    public List<EmbPlan> getOneMonthEmbPlan() {
        List<EmbPlan> embPlanList = new ArrayList<>();
        try{
            embPlanList = embPlanMapper.getOneMonthEmbPlan();
            return embPlanList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return embPlanList;
    }

    @Override
    public List<EmbPlan> getThreeMonthEmbPlan() {
        List<EmbPlan> embPlanList = new ArrayList<>();
        try{
            embPlanList = embPlanMapper.getThreeMonthEmbPlan();
            return embPlanList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return embPlanList;
    }

    @Override
    public Integer updateEmbPlan(EmbPlan embPlan) {
        try{
            embPlanMapper.updateEmbPlan(embPlan);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public EmbPlan getEmbPlanByOrderGroup(String orderName, String groupName, Date outTime) {
        EmbPlan embPlan;
        try{
            embPlan = embPlanMapper.getEmbPlanByOrderGroup(orderName, groupName, outTime);
            return embPlan;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public EmbPlan getEmbPlanByOrderGroupDate(String orderName, String groupName, Date outTime) {
        EmbPlan embPlan;
        try{
            embPlan = embPlanMapper.getEmbPlanByOrderGroupDate(orderName, groupName, outTime);
            return embPlan;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Integer updateEmbPlanOnOut(Integer embPlanID, Integer actCount, Float achievePercent) {
        try{
            embPlanMapper.updateEmbPlanOnOut(embPlanID, actCount, achievePercent);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer getEmbStorageCountByInfo(String orderName, String colorName, String sizeName) {
        Integer storageCount = 0;
        try{
            if (embPlanMapper.getEmbStorageCountByInfo(orderName, colorName, sizeName) != null){
                storageCount = embPlanMapper.getEmbStorageCountByInfo(orderName, colorName, sizeName);
            }
            return storageCount;
        }catch (Exception e){
            e.printStackTrace();
        }
        return storageCount;
    }

    @Override
    public EmbPlan getEmbPlanByOrderColorSizeGroupDate(String orderName, String colorName, String sizeName, String groupName, Date outTime) {
        EmbPlan embPlan;
        try{
            embPlan = embPlanMapper.getEmbPlanByOrderColorSizeGroupDate(orderName, colorName, sizeName, groupName, outTime);
            return embPlan;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<EmbPlan> getUnFinishEmbPlan() {
        List<EmbPlan> embPlanList = new ArrayList<>();
        try{
            embPlanList = embPlanMapper.getUnFinishEmbPlan();
            return embPlanList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return embPlanList;
    }

    @Override
    public Integer updateEmbPlanCountByID(Integer embPlanID, Integer planCount) {
        try{
            embPlanMapper.updateEmbPlanCountByID(embPlanID, planCount);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<EmbPlan> getEmbPlansByOrderGroupDate(String orderName, String groupName, Date outTime) {
        List<EmbPlan> embPlanList = new ArrayList<>();
        try{
            embPlanList = embPlanMapper.getEmbPlansByOrderGroupDate(orderName, groupName, outTime);
            return embPlanList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return embPlanList;
    }
}
