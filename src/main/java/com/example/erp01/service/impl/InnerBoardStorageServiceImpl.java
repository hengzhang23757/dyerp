package com.example.erp01.service.impl;

import com.example.erp01.mapper.InnerBoardStorageMapper;
import com.example.erp01.model.InnerBoardStorage;
import com.example.erp01.service.InnerBoardStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
@Service
public class InnerBoardStorageServiceImpl implements InnerBoardStorageService {

    @Autowired
    private InnerBoardStorageMapper innerBoardStorageMapper;


    @Override
    public int innerBoardInStore(InnerBoardStorage innerBoardStorage) {
        try{
            innerBoardStorageMapper.innerBoardInStore(innerBoardStorage);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public int innerBoardOutStore(InnerBoardStorage innerBoardStorage) {
        try{
            innerBoardStorageMapper.innerBoardOutStore(innerBoardStorage);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public int deleteInnerBoardStorage(Integer innerBoardStorageID) {
        try{
            innerBoardStorageMapper.deleteInnerBoardStorage(innerBoardStorageID);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<InnerBoardStorage> getAllInnerBoardStorage() {
        List<InnerBoardStorage> innerBoardStorageList = new ArrayList<>();
        try{
            innerBoardStorageList = innerBoardStorageMapper.getAllInnerBoardStorage();
            return innerBoardStorageList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return innerBoardStorageList;
    }

    @Override
    public int updateInnerBoardStorage(InnerBoardStorage innerBoardStorage) {
        try{
            innerBoardStorageMapper.updateInnerBoardStorage(innerBoardStorage);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<String> getLocationHint(String versionNumber, String fabricNumber, String colorNumber, String jarNumber) {
        List<String> locationList = new ArrayList<>();
        try{
            locationList = innerBoardStorageMapper.getLocationHint(versionNumber, fabricNumber, colorNumber, jarNumber);
            return locationList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return locationList;
    }

    @Override
    public InnerBoardStorage getInnerBoardStorage(String location, String versionNumber, String fabricNumber, String colorNumber, String jarNumber) {
        InnerBoardStorage innerBoardStorage = null;
        try{
            innerBoardStorage = innerBoardStorageMapper.getInnerBoardStorage(location, versionNumber, fabricNumber, colorNumber, jarNumber);
            return innerBoardStorage;
        }catch (Exception e){
            e.printStackTrace();
        }
        return innerBoardStorage;
    }
}
