package com.example.erp01.service.impl;

import com.example.erp01.mapper.FabricMapper;
import com.example.erp01.model.Fabric;
import com.example.erp01.model.FabricDifference;
import com.example.erp01.model.FabricLeak;
import com.example.erp01.model.Inspection;
import com.example.erp01.service.FabricService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class FabricServiceImpl implements FabricService {

    @Autowired
    private FabricMapper fabricMapper;

    @Override
    public int addFabricBatch(List<Fabric> fabricList) {
        try{
            if(fabricList!=null && !fabricList.isEmpty()){
                fabricMapper.addFabricBatch(fabricList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public int deleteFabric(Integer fabricID) {
        try{
            fabricMapper.deleteFabric(fabricID);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<Fabric> getAllFabric() {
        List<Fabric> fabricList = new ArrayList<>();
        try{
            fabricList = fabricMapper.getAllFabric();
            return fabricList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return fabricList;
    }

    @Override
    public int updateFabric(Fabric fabric) {
        try{
            fabricMapper.updateFabric(fabric);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<String> getStyleNumberHint(String subStyleNumber) {
        List<String> styleNumberList = new ArrayList<>();
        try{
            styleNumberList = fabricMapper.getStyleNumberHint(subStyleNumber);
            return styleNumberList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return styleNumberList;
    }

    @Override
    public List<String> getFabricNumberHint(String styleNumber) {
        List<String> fabricNumberList = new ArrayList<>();
        try{
            fabricNumberList = fabricMapper.getFabricNumberHint(styleNumber);
            return fabricNumberList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return fabricNumberList;
    }

    @Override
    public String getVersionNumberHint(String styleNumber) {
        String versionNumber = null;
        try{
            versionNumber = fabricMapper.getVersionNumberHint(styleNumber);
            return versionNumber;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<String> getFabricColorNumberHint(String styleNumber, String fabricNumber) {
        List<String> fabricColorNumberList = new ArrayList<>();
        try{
            fabricColorNumberList = fabricMapper.getFabricColorNumberHint(styleNumber, fabricNumber);
            return fabricColorNumberList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return fabricColorNumberList;
    }

    @Override
    public List<String> getJarNumberHint(String styleNumber, String fabricNumber, String fabricColor) {
        List<String> jarNumberList = new ArrayList<>();
        try{
            jarNumberList = fabricMapper.getJarNumberHint(styleNumber, fabricNumber, fabricColor);
            return jarNumberList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return jarNumberList;
    }

    @Override
    public Float getDeliveryQuantityHint(String styleNumber, String fabricNumber, String fabricColor, String jarNumber) {
        Float fabricCount = null;
        try{
            fabricCount = fabricMapper.getDeliveryQuantityHint(styleNumber, fabricNumber, fabricColor, jarNumber);
            return fabricCount;
        }catch (Exception e){
            e.printStackTrace();
        }
        return fabricCount;
    }

    @Override
    public Integer getBatchNumberHint(String styleNumber, String fabricNumber, String fabricColor, String jarNumber) {
        Integer batchNumber = null;
        try{
            batchNumber = fabricMapper.getBatchNumberHint(styleNumber, fabricNumber, fabricColor, jarNumber);
            return batchNumber;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<String> getSeasonHint(String subSeason) {
        List<String> seasonList = new ArrayList<>();
        try{
            seasonList = fabricMapper.getSeasonHint(subSeason);
            return seasonList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return seasonList;
    }

    @Override
    public List<String> getVersionNumberBySeason(String season) {
        List<String> versionNumberList = new ArrayList<>();
        try{
            versionNumberList = fabricMapper.getVersionNumberBySeason(season);
            return versionNumberList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return versionNumberList;
    }

    @Override
    public List<FabricLeak> getFabricLeakQuery(String season, String versionNumber) {
        List<FabricLeak> fabricLeakList = new ArrayList<>();
        try{
            fabricLeakList = fabricMapper.getFabricLeakQuery(season, versionNumber);
            return fabricLeakList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return fabricLeakList;
    }

    @Override
    public String getColorNameByNumber(String styleNumber, String fabricColorNumber) {
        String fabricColor = null;
        try{
            fabricColor = fabricMapper.getColorNameByNumber(styleNumber, fabricColorNumber);
            return fabricColor;
        }catch (Exception e){
            e.printStackTrace();
        }
        return fabricColor;
    }

    @Override
    public String getColorNumberByName(String styleNumber, String fabricColor) {
        String fabricColorNumber = null;
        try{
            fabricColorNumber = fabricMapper.getColorNumberByName(styleNumber, fabricColor);
            return fabricColorNumber;
        }catch (Exception e){
            e.printStackTrace();
        }
        return fabricColorNumber;
    }

    @Override
    public List<FabricDifference> getFabricDifference(String styleNumber) {
        List<FabricDifference> fabricDifferenceList = new ArrayList<>();
        try{
            fabricDifferenceList = fabricMapper.getFabricDifference(styleNumber);
            return fabricDifferenceList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return fabricDifferenceList;
    }

    @Override
    public List<String> getVersionNumberBySubVersion(String subVersion) {
        List<String> versionNumberList = new ArrayList<>();
        try{
            versionNumberList = fabricMapper.getVersionNumberBySubVersion(subVersion);
            return versionNumberList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return versionNumberList;
    }

    @Override
    public List<String> getStyleNumbersByVersion(String versionNumber) {
        List<String> styleNumberList = new ArrayList<>();
        try{
            styleNumberList = fabricMapper.getStyleNumbersByVersion(versionNumber);
            return styleNumberList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return styleNumberList;
    }

    @Override
    public List<Fabric> getOneMonthFabric() {
        List<Fabric> fabricList = new ArrayList<>();
        try{
            fabricList = fabricMapper.getOneMonthFabric();
            return fabricList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return fabricList;
    }

    @Override
    public List<Fabric> getThreeMonthsFabric() {
        List<Fabric> fabricList = new ArrayList<>();
        try{
            fabricList = fabricMapper.getThreeMonthsFabric();
            return fabricList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return fabricList;
    }

    @Override
    public List<String> getFabricColorHint(String styleNumber, String fabricNumber) {
        List<String> fabricColorList = new ArrayList<>();
        try{
            fabricColorList = fabricMapper.getFabricColorHint(styleNumber, fabricNumber);
            return fabricColorList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return fabricColorList;
    }


}
