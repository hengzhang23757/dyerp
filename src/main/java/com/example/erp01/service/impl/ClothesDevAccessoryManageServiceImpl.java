package com.example.erp01.service.impl;

import com.example.erp01.mapper.ClothesDevAccessoryManageMapper;
import com.example.erp01.model.ClothesDevAccessoryManage;
import com.example.erp01.service.ClothesDevAccessoryManageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class ClothesDevAccessoryManageServiceImpl implements ClothesDevAccessoryManageService {

    @Autowired
    private ClothesDevAccessoryManageMapper clothesDevAccessoryManageMapper;

    @Override
    public Integer addClothesDevAccessoryManage(ClothesDevAccessoryManage clothesDevAccessoryManage) {
        try{
            clothesDevAccessoryManageMapper.addClothesDevAccessoryManage(clothesDevAccessoryManage);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<ClothesDevAccessoryManage> getClothesDevAccessoryManageByMap(Map<String, Object> map) {
        return clothesDevAccessoryManageMapper.getClothesDevAccessoryManageByMap(map);
    }

    @Override
    public Integer updateClothesDevAccessoryManage(ClothesDevAccessoryManage clothesDevAccessoryManage) {
        try{
            clothesDevAccessoryManageMapper.updateClothesDevAccessoryManage(clothesDevAccessoryManage);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer deleteClothesDevAccessory(Integer id) {
        try{
            clothesDevAccessoryManageMapper.deleteClothesDevAccessory(id);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<String> getClothsDevAccessoryNameHint(String keyWord) {
        return clothesDevAccessoryManageMapper.getClothsDevAccessoryNameHint(keyWord);
    }

    @Override
    public List<String> getClothsDevAccessoryNumberHint(String keyWord) {
        return clothesDevAccessoryManageMapper.getClothsDevAccessoryNumberHint(keyWord);
    }

    @Override
    public List<ClothesDevAccessoryManage> getClothesDevAccessoryManageHint(String subAccessoryName, Integer page, Integer limit) {
        return clothesDevAccessoryManageMapper.getClothesDevAccessoryManageHint(subAccessoryName, page, limit);
    }

    @Override
    public List<ClothesDevAccessoryManage> getClothesDevAccessoryManageHintByNumber(String subAccessoryNumber, Integer page, Integer limit) {
        return clothesDevAccessoryManageMapper.getClothesDevAccessoryManageHintByNumber(subAccessoryNumber, page, limit);
    }

    @Override
    public Integer deleteClothesDevAccessoryRecordByAccessoryId(Integer accessoryID) {
        try{
            clothesDevAccessoryManageMapper.deleteClothesDevAccessoryRecordByAccessoryId(accessoryID);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public ClothesDevAccessoryManage getClothesDevAccessoryManageById(Integer id) {
        return clothesDevAccessoryManageMapper.getClothesDevAccessoryManageById(id);
    }

    @Override
    public Integer updateClothesDevAccessoryManageBatch(List<ClothesDevAccessoryManage> clothesDevAccessoryManageList) {
        try {
            if (clothesDevAccessoryManageList != null && !clothesDevAccessoryManageList.isEmpty()){
                clothesDevAccessoryManageMapper.updateClothesDevAccessoryManageBatch(clothesDevAccessoryManageList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer deleteClothesDevAccessoryManageBatch(List<Integer> idList) {
        try{
            if (idList != null && !idList.isEmpty()){
                clothesDevAccessoryManageMapper.deleteClothesDevAccessoryManageBatch(idList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public String getMaxClothesDevAccessoryOrderByPreFix(String preFix) {
        String maxOrder = "00000";
        try{
            if (clothesDevAccessoryManageMapper.getMaxClothesDevAccessoryOrderByPreFix(preFix) != null){
                maxOrder = clothesDevAccessoryManageMapper.getMaxClothesDevAccessoryOrderByPreFix(preFix);
            }
            return maxOrder;
        }catch (Exception e){
            e.printStackTrace();
        }
        return maxOrder;
    }

    @Override
    public List<ClothesDevAccessoryManage> getClothesDevAccessoryManageRecordByInfo(Map<String, Object> map) {
        return clothesDevAccessoryManageMapper.getClothesDevAccessoryManageRecordByInfo(map);
    }

    @Override
    public Integer quitClothesDevAccessoryManageCheck(List<Integer> idList) {
        try{
            if (idList != null && !idList.isEmpty()){
                clothesDevAccessoryManageMapper.quitClothesDevAccessoryManageCheck(idList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<ClothesDevAccessoryManage> getClothesDevAccessoryManageByAccessoryNumberList(List<String> accessoryNumberList) {
        List<ClothesDevAccessoryManage> clothesDevAccessoryManageList = new ArrayList<>();
        try{
            if (accessoryNumberList != null && !accessoryNumberList.isEmpty()){
                clothesDevAccessoryManageList = clothesDevAccessoryManageMapper.getClothesDevAccessoryManageByAccessoryNumberList(accessoryNumberList);
            }
            return clothesDevAccessoryManageList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return clothesDevAccessoryManageList;
    }

    @Override
    public Integer updateClothesDevAccessoryManageOnlyCount(List<ClothesDevAccessoryManage> clothesDevAccessoryManageList) {
        try{
            if (clothesDevAccessoryManageList != null && !clothesDevAccessoryManageList.isEmpty()){
                clothesDevAccessoryManageMapper.updateClothesDevAccessoryManageOnlyCount(clothesDevAccessoryManageList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer addClothesDevAccessoryManageBatch(List<ClothesDevAccessoryManage> clothesDevAccessoryManageList) {
        try{
            if (clothesDevAccessoryManageList != null && !clothesDevAccessoryManageList.isEmpty()){
                clothesDevAccessoryManageMapper.addClothesDevAccessoryManageBatch(clothesDevAccessoryManageList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }
}
