package com.example.erp01.service.impl;

import com.example.erp01.mapper.ReWorkMapper;
import com.example.erp01.model.ReWork;
import com.example.erp01.service.ReWorkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class ReWorkServiceImpl implements ReWorkService {

    @Autowired
    private ReWorkMapper reWorkMapper;

    @Override
    public List<ReWork> getReWorkByOrder(String orderName) {
        List<ReWork> reWorkList = new ArrayList<>();
        try{
            reWorkList = reWorkMapper.getReWorkByOrder(orderName);
            return reWorkList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return reWorkList;

    }

    @Override
    public Integer getWorkCountByProcedure(String orderName, Integer procedureNumber, Date reWorkDate) {
        Integer reWorkCount = 0;
        try{
            reWorkCount = reWorkMapper.getWorkCountByProcedure(orderName, procedureNumber, reWorkDate);
            return reWorkCount;
        }catch (Exception e){
            e.printStackTrace();
        }
        return reWorkCount;
    }

    @Override
    public List<ReWork> getReWorkSummaryByOrder(String orderName) {
        List<ReWork> reWorkList = new ArrayList<>();
        try{
            reWorkList = reWorkMapper.getReWorkSummaryByOrder(orderName);
            return reWorkList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return reWorkList;
    }
}
