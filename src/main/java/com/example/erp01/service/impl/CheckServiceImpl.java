package com.example.erp01.service.impl;

import com.example.erp01.mapper.CheckMapper;
import com.example.erp01.model.AttendLog;
import com.example.erp01.model.DailyCheck;
import com.example.erp01.model.SummaryCheck;
import com.example.erp01.service.CheckService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
@Service
public class CheckServiceImpl implements CheckService {

    @Autowired
    private CheckMapper checkMapper;

    @Override
    public Integer addAttendLogBatch(List<AttendLog> attendLogList) {
        try{
            if (attendLogList != null && !attendLogList.isEmpty()){
                checkMapper.addAttendLogBatch(attendLogList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<AttendLog> getAttendLogByEmp(Date fromDate, Date toDate, String ccCount) {
        List<AttendLog> attendLogList = new ArrayList<>();
        try{
            attendLogList = checkMapper.getAttendLogByEmp(fromDate, toDate, ccCount);

            return attendLogList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return attendLogList;
    }

    @Override
    public Integer addDailyCheckBatch(List<DailyCheck> dailyCheckList) {
        try{
            if (dailyCheckList != null && !dailyCheckList.isEmpty()){
                checkMapper.addDailyCheckBatch(dailyCheckList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<DailyCheck> getDailyCheckByMonthGroup(String monthString, String groupName) {
        List<DailyCheck> dailyCheckList = new ArrayList<>();
        try{
            dailyCheckList = checkMapper.getDailyCheckByMonthGroup(monthString, groupName);
            return dailyCheckList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return dailyCheckList;
    }

    @Override
    public List<AttendLog> getMonthAttendLogByEmp(String monthString, String ccCount) {
        List<AttendLog> attendLogList = new ArrayList<>();
        try{
            attendLogList = checkMapper.getMonthAttendLogByEmp(monthString, ccCount);
            return attendLogList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return attendLogList;
    }

    @Override
    public Integer addSummaryCheckBatch(List<SummaryCheck> summaryCheckList) {
        try{
            if (summaryCheckList != null && !summaryCheckList.isEmpty()){
                checkMapper.addSummaryCheckBatch(summaryCheckList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<SummaryCheck> getSummaryCheckByMonthGroup(String monthString, String groupName) {
        List<SummaryCheck> summaryCheckList = new ArrayList<>();
        try{
            summaryCheckList = checkMapper.getSummaryCheckByMonthGroup(monthString, groupName);
            return summaryCheckList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return summaryCheckList;
    }
}
