package com.example.erp01.service.impl;

import com.example.erp01.mapper.UnitConsumptionMapper;
import com.example.erp01.model.UnitConsumption;
import com.example.erp01.service.UnitConsumptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UnitConsumptionServiceImpl implements UnitConsumptionService {

    @Autowired
    private UnitConsumptionMapper unitConsumptionMapper;

    @Override
    public Float getPlanConsumptionByJar(String orderName, String colorName, String jarName) {
        Float planConsumption = 0f;
        try{
             if (unitConsumptionMapper.getPlanConsumptionByJar(orderName, colorName, jarName) != null){
                 return unitConsumptionMapper.getPlanConsumptionByJar(orderName, colorName, jarName);
             }
            return planConsumption;
        }catch (Exception e){
            e.printStackTrace();
        }
        return planConsumption;
    }

    @Override
    public List<UnitConsumption> getConsumptionInfoByOrder(String orderName, String colorName) {
        List<UnitConsumption> unitConsumptionList = new ArrayList<>();
        try{
            unitConsumptionList = unitConsumptionMapper.getConsumptionInfoByOrder(orderName, colorName);
            return unitConsumptionList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return unitConsumptionList;
    }


    @Override
    public Float getReturnFabricCountByOrderColorJar(String orderName, String colorName, String jarName) {
        Float consumption = 0f;
        try{
            if (unitConsumptionMapper.getReturnFabricCountByOrderColorJar(orderName, colorName, jarName) != null){
                consumption = unitConsumptionMapper.getReturnFabricCountByOrderColorJar(orderName, colorName, jarName);
            }
            return consumption;
        }catch (Exception e){
            e.printStackTrace();
        }
        return consumption;
    }

    @Override
    public List<UnitConsumption> getOtherConsumptionSummaryOfEachBed(String orderName, String colorName, String partName) {
        List<UnitConsumption> unitConsumptionList = new ArrayList<>();
        try{
            unitConsumptionList = unitConsumptionMapper.getOtherConsumptionSummaryOfEachBed(orderName, colorName, partName);
            return unitConsumptionList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return unitConsumptionList;
    }

    @Override
    public List<UnitConsumption> getOtherBasicInfoOfEachBed(String orderName, String colorName, String partName) {
        List<UnitConsumption> unitConsumptionList = new ArrayList<>();
        try{
            unitConsumptionList = unitConsumptionMapper.getOtherBasicInfoOfEachBed(orderName, colorName, partName);
            return unitConsumptionList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return unitConsumptionList;
    }

    @Override
    public Float getOtherPlanConsumptionByJar(String orderName, String colorName, String jarName) {
        Float planConsumption = 0f;
        try{
            if (unitConsumptionMapper.getOtherPlanConsumptionByJar(orderName, colorName, jarName) != null){
                return unitConsumptionMapper.getOtherPlanConsumptionByJar(orderName, colorName, jarName);
            }
            return planConsumption;
        }catch (Exception e){
            e.printStackTrace();
        }
        return planConsumption;
    }

    @Override
    public Integer getOtherSumLayerCountOfByOrderColorName(String orderName, String colorName, String partName) {
        Integer layerCount = 0;
        try{
            if (unitConsumptionMapper.getOtherSumLayerCountOfByOrderColorName(orderName, colorName, partName) != null){
                layerCount = unitConsumptionMapper.getOtherSumLayerCountOfByOrderColorName(orderName, colorName, partName);
            }
            return layerCount;
        }catch (Exception e){
            e.printStackTrace();
        }
        return layerCount;
    }

    @Override
    public List<UnitConsumption> getOtherConsumptionSummaryOfOrder(String orderName, String colorName, String partName) {
        List<UnitConsumption> unitConsumptionList = new ArrayList<>();
        try{
            unitConsumptionList = unitConsumptionMapper.getOtherConsumptionSummaryOfOrder(orderName, colorName, partName);
            return unitConsumptionList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return unitConsumptionList;
    }

    @Override
    public Float getOtherActConsumptionByColor(String orderName, String colorName, String partName) {
        Float consumption = 0f;
        try{
            if (unitConsumptionMapper.getOtherActConsumptionByColor(orderName, colorName, partName) != null){
                consumption = unitConsumptionMapper.getOtherActConsumptionByColor(orderName, colorName, partName);
            }
            return consumption;
        }catch (Exception e){
            e.printStackTrace();
        }
        return consumption;
    }

    @Override
    public Float getOtherReturnFabricCountByOrderColorJar(String orderName, String colorName, String jarName) {
        Float consumption = 0f;
        try{
            if (unitConsumptionMapper.getOtherReturnFabricCountByOrderColorJar(orderName, colorName, jarName) != null){
                consumption = unitConsumptionMapper.getOtherReturnFabricCountByOrderColorJar(orderName, colorName, jarName);
            }
            return consumption;
        }catch (Exception e){
            e.printStackTrace();
        }
        return consumption;
    }

    @Override
    public List<UnitConsumption> getConsumptionSummaryOfOrder(String orderName, String colorName, String partName, Integer tailorType) {
        List<UnitConsumption> unitConsumptionList = new ArrayList<>();
        try{
            unitConsumptionList = unitConsumptionMapper.getConsumptionSummaryOfOrder(orderName, colorName, partName, tailorType);
            return unitConsumptionList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return unitConsumptionList;
    }

    @Override
    public Float getActConsumptionByColor(String orderName, String colorName, String partName, Integer tailorType) {
        Float consumption = 0f;
        try{
            if (unitConsumptionMapper.getActConsumptionByColor(orderName, colorName, partName, tailorType) != null){
                consumption = unitConsumptionMapper.getActConsumptionByColor(orderName, colorName, partName, tailorType);
            }
            return consumption;
        }catch (Exception e){
            e.printStackTrace();
        }
        return consumption;
    }

    @Override
    public Integer getSumLayerCountOfByOrderColorName(String orderName, String colorName, String partName, Integer tailorType) {
        Integer layerCount = 0;
        try{
            if (unitConsumptionMapper.getSumLayerCountOfByOrderColorName(orderName, colorName, partName, tailorType) != null){
                layerCount = unitConsumptionMapper.getSumLayerCountOfByOrderColorName(orderName, colorName, partName, tailorType);
            }
            return layerCount;
        }catch (Exception e){
            e.printStackTrace();
        }
        return layerCount;
    }

    @Override
    public List<UnitConsumption> getBasicInfoOfEachBed(String orderName, String colorName, String partName, Integer tailorType) {
        List<UnitConsumption> unitConsumptionList = new ArrayList<>();
        try{
            unitConsumptionList = unitConsumptionMapper.getBasicInfoOfEachBed(orderName, colorName, partName, tailorType);
            return unitConsumptionList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return unitConsumptionList;
    }

    @Override
    public List<UnitConsumption> getConsumptionSummaryOfEachBed(String orderName, String colorName, String partName, Integer tailorType) {
        List<UnitConsumption> unitConsumptionList = new ArrayList<>();
        try{
            unitConsumptionList = unitConsumptionMapper.getConsumptionSummaryOfEachBed(orderName, colorName, partName, tailorType);
            return unitConsumptionList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return unitConsumptionList;
    }

    @Override
    public String getMaxJarNumberByOrderColorPartType(String orderName, String colorName, String partName, Integer tailorType) {
        String jarNumber = "";
        try{
            if (unitConsumptionMapper.getMaxJarNumberByOrderColorPartType(orderName, colorName, partName, tailorType) != null){
                jarNumber = unitConsumptionMapper.getMaxJarNumberByOrderColorPartType(orderName, colorName, partName, tailorType);
            }
            return jarNumber;
        } catch (Exception e){
            e.printStackTrace();
        }
        return jarNumber;
    }


}
