package com.example.erp01.service.impl;

import com.example.erp01.mapper.ClothesDevAccessoryNumberMapper;
import com.example.erp01.model.ClothesDevAccessoryNumber;
import com.example.erp01.service.ClothesDevAccessoryNumberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClothesDevAccessoryNumberServiceImpl implements ClothesDevAccessoryNumberService {

    @Autowired
    private ClothesDevAccessoryNumberMapper clothesDevAccessoryNumberMapper;


    @Override
    public Integer addClothesDevAccessoryNumber(ClothesDevAccessoryNumber clothesDevAccessoryNumber) {
        try{
            clothesDevAccessoryNumberMapper.addClothesDevAccessoryNumber(clothesDevAccessoryNumber);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer deleteClothesDevAccessoryNumber(Integer id) {
        try{
            clothesDevAccessoryNumberMapper.deleteClothesDevAccessoryNumber(id);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<ClothesDevAccessoryNumber> getAllClothesDevAccessoryNumber() {
        return clothesDevAccessoryNumberMapper.getAllClothesDevAccessoryNumber();
    }

    @Override
    public Integer updateClothesDevAccessoryNumber(ClothesDevAccessoryNumber clothesDevAccessoryNumber) {
        try{
            clothesDevAccessoryNumberMapper.updateClothesDevAccessoryNumber(clothesDevAccessoryNumber);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }
}
