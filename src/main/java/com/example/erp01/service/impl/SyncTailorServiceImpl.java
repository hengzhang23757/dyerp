package com.example.erp01.service.impl;

import com.example.erp01.model.PrintPart;
import com.example.erp01.model.Tailor;
import com.example.erp01.service.SyncTailorService;
import com.example.erp01.ycMapper.SyncTailorMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class SyncTailorServiceImpl implements SyncTailorService {

    @Autowired
    private SyncTailorMapper syncTailorMapper;

    @Override
    public List<Tailor> getTailorByInfo(String orderName, Integer bedNumber, Integer packageNumber) {
        return syncTailorMapper.getTailorByInfo(orderName, bedNumber, packageNumber);
    }

    @Override
    public Integer addTailorBatch(List<Tailor> tailorList) {
        try{
            if (tailorList != null && !tailorList.isEmpty()){
                syncTailorMapper.addTailorBatch(tailorList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer addPrintPartBatch(List<PrintPart> printPartList) {
        try{
            if (printPartList != null && !printPartList.isEmpty()){
                syncTailorMapper.addPrintPartBatch(printPartList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }
}
