package com.example.erp01.service.impl;

import com.example.erp01.mapper.ReportMapper;
import com.example.erp01.model.*;
import com.example.erp01.service.ReportService;
import com.example.erp01.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class ReportServiceImpl implements ReportService {

    @Autowired
    private ReportMapper reportMapper;

    @Override
    public List<ProductionProgress> getProductionProgressByOrderTimeGroup(String orderName, Date from, Date to, String groupName) {
        List<ProductionProgress> productionProgressList = new ArrayList<>();
        try{
            productionProgressList = reportMapper.getProductionProgressByOrderTimeGroup(orderName, from, to, groupName);
            return productionProgressList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return productionProgressList;
    }

    @Override
    public List<MiniProgress> getProductionProgressByOrder(String orderName) {
        List<MiniProgress> productionProgressList = new ArrayList<>();
        try{
            productionProgressList = reportMapper.getProductionProgressByOrder(orderName);
            return productionProgressList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return productionProgressList;
    }

    @Override
    public List<MiniProgress> getProductionProgressByOrderGroup(String orderName, String groupName) {
        List<MiniProgress> productionProgressList = new ArrayList<>();
        try{
            productionProgressList = reportMapper.getProductionProgressByOrderGroup(orderName, groupName);
            return productionProgressList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return productionProgressList;
    }

    @Override
    public List<CutDaily> getCutDailyDetailByOrderTime(String orderName, Date from, Date to) {
        List<CutDaily> cutDailyList = new ArrayList<>();
        try{
            cutDailyList = reportMapper.getCutDailyDetailByOrderTime(orderName, from, to);
            return cutDailyList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return cutDailyList;
    }

    @Override
    public List<ProductionProgress> getMiniProductionProgressByOrderGroup(String orderName, Date from, Date to, String groupName) {
        List<ProductionProgress> productionProgressList = new ArrayList<>();
        try{
            productionProgressList = reportMapper.getMiniProductionProgressByOrderGroup(orderName, from, to, groupName);
            return productionProgressList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return productionProgressList;
    }

    @Override
    public List<ProductionProgressDetail> getProductionProgressDetailByOrderTimeGroup(String orderName, Date from, Date to, String groupName, Integer procedureNumber) {
        List<ProductionProgressDetail> productionProgressDetailList = new ArrayList<>();
        try{
            productionProgressDetailList = reportMapper.getProductionProgressDetailByOrderTimeGroup(orderName, from, to, groupName, procedureNumber);
            return productionProgressDetailList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return productionProgressDetailList;
    }

    @Override
    public List<ProductionProgressDetail> getMiniProductionProgressDetailByOrderTimeGroup(String orderName, Date from, Date to, String groupName, Integer procedureNumber) {
        List<ProductionProgressDetail> productionProgressDetailList = new ArrayList<>();
        try{
            productionProgressDetailList = reportMapper.getMiniProductionProgressDetailByOrderTimeGroup(orderName, from, to, groupName, procedureNumber);
            return productionProgressDetailList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return productionProgressDetailList;
    }

    @Override
    public List<PersonProductionDetail> getPersonProductionOfProcedure(String orderName, Date from, Date to, String groupName, Integer procedureNumber) {
        List<PersonProductionDetail> personProductionDetailList = new ArrayList<>();
        try{
            personProductionDetailList = reportMapper.getPersonProductionOfProcedure(orderName, from, to, groupName, procedureNumber);
            return personProductionDetailList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return personProductionDetailList;
    }

    @Override
    public List<PersonProductionDetail> getMiniPersonProductionOfProcedure(String orderName, Date from, Date to, String groupName, Integer procedureNumber) {
        List<PersonProductionDetail> personProductionDetailList = new ArrayList<>();
        try{
            personProductionDetailList = reportMapper.getMiniPersonProductionOfProcedure(orderName, from, to, groupName, procedureNumber);
            return personProductionDetailList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return personProductionDetailList;
    }

    @Override
    public List<PersonProductionDetail> getEachPersonProduction(String orderName, Date from, Date to, String groupName) {
        List<PersonProductionDetail> personProductionDetailList = new ArrayList<>();
        try{
            personProductionDetailList = reportMapper.getEachPersonProduction(orderName, from, to, groupName);
            return personProductionDetailList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return personProductionDetailList;
    }

    @Override
    public List<PersonProductionDetail> getMiniEachPersonProductionOfProcedure(String orderName, Date from, Date to, String groupName) {
        List<PersonProductionDetail> personProductionDetailList = new ArrayList<>();
        try{
            personProductionDetailList = reportMapper.getMiniEachPersonProductionOfProcedure(orderName, from, to, groupName);
            return personProductionDetailList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return personProductionDetailList;
    }

    @Override
    public List<CutQueryLeak> getCutInStoreProgress(String orderName, Date from, Date to) {
        List<CutQueryLeak> cutInStoreList = new ArrayList<>();
        try{
            cutInStoreList = reportMapper.getCutInStoreProgress(orderName, from, to);
            return cutInStoreList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return cutInStoreList;
    }

    @Override
    public List<CutQueryLeak> getEmbInStoreProgress(String orderName, Date from, Date to) {
        List<CutQueryLeak> embInStoreList = new ArrayList<>();
        try{
            embInStoreList = reportMapper.getEmbInStoreProgress(orderName, from, to);
            return embInStoreList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return embInStoreList;
    }

    @Override
    public List<CutQueryLeak> getEmbOutStoreProgress(String orderName, Date from, Date to) {
        List<CutQueryLeak> embOutStoreList = new ArrayList<>();
        try{
            embOutStoreList = reportMapper.getEmbOutStoreProgress(orderName, from, to);
            return embOutStoreList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return embOutStoreList;
    }

    @Override
    public Integer getCutInStoreCount(String orderName, Date from, Date to) {
        Integer res = 0;
        try{
            res = reportMapper.getCutInStoreCount(orderName, from, to);
            if (res != null){
                return res;
            }else {
                return 0;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return res;
    }

    @Override
    public Integer getEmbInStoreCount(String orderName, Date from, Date to) {
        Integer res = 0;
        try{
            res = reportMapper.getEmbInStoreCount(orderName, from, to);
            if (res != null){
                return res;
            }else {
                return 0;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return res;
    }

    @Override
    public Integer getEmbOutStoreCount(String orderName, Date from, Date to) {
        Integer res = 0;
        try{
            res = reportMapper.getEmbOutStoreCount(orderName, from, to);
            if (res != null){
                return res;
            }else {
                return 0;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return res;
    }

    @Override
    public Date getCreateTimeByOrderBed(String orderName, Integer bedNumber) {
        Date createTime = new Date();
        try{
            createTime = reportMapper.getCreateTimeByOrderBed(orderName, bedNumber);
            return createTime;
        }catch (Exception e){
            e.printStackTrace();
        }
        return createTime;
    }

    @Override
    public Date getOtherCreateTimeByOrderBed(String orderName, String partName, Integer bedNumber) {
        Date createTime = new Date();
        try{
            createTime = reportMapper.getOtherCreateTimeByOrderBed(orderName, partName, bedNumber);
            return createTime;
        }catch (Exception e){
            e.printStackTrace();
        }
        return createTime;
    }

    @Override
    public List<TailorMonthReport> getTailorMonthReportOfEachDay(Date from, Date to, String orderName, String partName, Integer tailorType, String groupName) {
        List<TailorMonthReport> tailorMonthReportList = new ArrayList<>();
        try{
            tailorMonthReportList = reportMapper.getTailorMonthReportOfEachDay(from, to, orderName, partName, tailorType, groupName);
            return tailorMonthReportList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return tailorMonthReportList;
    }

    @Override
    public List<TailorMonthReport> getOtherTailorMonthReportOfEachDay(Date from, Date to, String orderName, String partName, Integer tailorType, String groupName) {
        List<TailorMonthReport> tailorMonthReportList = new ArrayList<>();
        try{
            tailorMonthReportList = reportMapper.getOtherTailorMonthReportOfEachDay(from, to, orderName, partName, tailorType, groupName);
            return tailorMonthReportList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return tailorMonthReportList;
    }

    @Override
    public List<CutBedColor> getCutBedColorByOrder(String orderName, String partName, Integer tailorType, String from, String to) {
        List<CutBedColor> cutBedColorList = new ArrayList<>();
        try{
            cutBedColorList = reportMapper.getCutBedColorByOrder(orderName, partName, tailorType, from, to);
            return cutBedColorList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return cutBedColorList;
    }

    @Override
    public Integer fillProcedurePrice(List<OrderProcedure> orderProcedureList, String monthString) {
        try{
            if (orderProcedureList != null && !orderProcedureList.isEmpty()){
                reportMapper.fillProcedurePrice(orderProcedureList, monthString);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<String> getPieceWorkOrderNameListByMonthString(String monthString) {
        List<String> orderNameList = new ArrayList<>();
        String firstDay = DateUtils.getFirstDayOfMonth(monthString);
        String lastDay = DateUtils.getLastDayOfMonth(monthString);
        try{
            orderNameList = reportMapper.getPieceWorkOrderNameListByMonthString(firstDay,lastDay);
            return orderNameList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return orderNameList;
    }

    @Override
    public List<GeneralSalary> getPieceWorkSummarySalaryByInfo(Date from, Date to, String groupName) {
        List<GeneralSalary> generalSalaryList = new ArrayList<>();
        try{
            generalSalaryList = reportMapper.getPieceWorkSummarySalaryByInfo(from, to, groupName);
            return generalSalaryList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return generalSalaryList;
    }

    @Override
    public List<GeneralSalary> getPieceWorkGroupSalaryByInfo(Date from, Date to, String groupName) {
        List<GeneralSalary> generalSalaryList = new ArrayList<>();
        try{
            generalSalaryList = reportMapper.getPieceWorkGroupSalaryByInfo(from, to, groupName);
            return generalSalaryList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return generalSalaryList;
    }
}
