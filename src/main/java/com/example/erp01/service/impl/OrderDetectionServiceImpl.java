package com.example.erp01.service.impl;

import com.example.erp01.mapper.OrderDetectionMapper;
import com.example.erp01.model.OrderDetection;
import com.example.erp01.service.OrderDetectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class OrderDetectionServiceImpl implements OrderDetectionService {

    @Autowired
    private OrderDetectionMapper orderDetectionMapper;

    @Override
    public Integer addOrderDetection(OrderDetection orderDetection) {
        try{
            orderDetectionMapper.addOrderDetection(orderDetection);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer updateOrderDetection(OrderDetection orderDetection) {
        try{
            orderDetectionMapper.updateOrderDetection(orderDetection);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer deleteOrderDetection(Integer id) {
        try{
            orderDetectionMapper.deleteOrderDetection(id);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<OrderDetection> getOrderDetectionByInfo(Map<String, Object> map) {
        return orderDetectionMapper.getOrderDetectionByInfo(map);
    }

    @Override
    public OrderDetection getOrderDetectionById(Integer id) {
        return orderDetectionMapper.getOrderDetectionById(id);
    }
}
