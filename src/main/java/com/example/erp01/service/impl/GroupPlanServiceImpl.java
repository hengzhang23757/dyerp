package com.example.erp01.service.impl;

import com.example.erp01.mapper.GroupPlanMapper;
import com.example.erp01.model.GroupPlan;
import com.example.erp01.service.GroupPlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class GroupPlanServiceImpl implements GroupPlanService {

    @Autowired
    private GroupPlanMapper groupPlanMapper;

    @Override
    public int addGroupPlan(GroupPlan groupPlan) {
        try{
            groupPlanMapper.addGroupPlan(groupPlan);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public int deleteGroupPlan(Integer groupPlanID) {
        try{
            groupPlanMapper.deleteGroupPlan(groupPlanID);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<GroupPlan> getAllGroupPlan() {
        List<GroupPlan> groupPlanList = new ArrayList<>();
        try{
            groupPlanList = groupPlanMapper.getAllGroupPlan();
            return groupPlanList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return groupPlanList;
    }

    @Override
    public GroupPlan getGroupPlanByOrderGroup(String orderName, String groupName) {
        GroupPlan groupPlan = null;
        try{
            groupPlan = groupPlanMapper.getGroupPlanByOrderGroup(orderName, groupName);
            return groupPlan;
        }catch (Exception e){
            e.printStackTrace();
        }
        return groupPlan;
    }

    @Override
    public int updateGroupPlan(GroupPlan groupPlan) {
        try{
            groupPlanMapper.updateGroupPlan(groupPlan);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }
}
