package com.example.erp01.service.impl;

import com.example.erp01.mapper.OrderSalaryMapper;
import com.example.erp01.model.GeneralSalary;
import com.example.erp01.model.OrderSalary;
import com.example.erp01.service.OrderSalaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class OrderSalaryServiceImpl implements OrderSalaryService {

    @Autowired
    private OrderSalaryMapper orderSalaryMapper;

    @Override
    public List<OrderSalary> getOrderSalarySummaryByOrderProcedure(String orderName, Integer procedureNumber) {
        List<OrderSalary> orderSalaryList = new ArrayList<>();
        try{
            orderSalaryList = orderSalaryMapper.getOrderSalarySummaryByOrderProcedure(orderName, procedureNumber);
            return orderSalaryList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return orderSalaryList;
    }

    @Override
    public List<OrderSalary> getOrderSalarySummaryByOrderProcedureMonth(String orderName, Integer procedureNumber) {
        List<OrderSalary> orderSalaryList = new ArrayList<>();
        try{
            orderSalaryList = orderSalaryMapper.getOrderSalarySummaryByOrderProcedureMonth(orderName, procedureNumber);
            return orderSalaryList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return orderSalaryList;
    }

    @Override
    public List<String> getPieceWorkOrderNameListByMonth(String from, String to) {
        return orderSalaryMapper.getPieceWorkOrderNameListByMonth(from, to);
    }

    @Override
    public List<String> getCutOrderNameListByMonth(String from, String to) {
        return orderSalaryMapper.getCutOrderNameListByMonth(from, to);
    }

    @Override
    public List<OrderSalary> getOrderCountByOrderNameList(List<String> orderNameList) {
        List<OrderSalary> orderSalaryList = new ArrayList<>();
        if (orderNameList != null && !orderNameList.isEmpty()){
            return orderSalaryMapper.getOrderCountByOrderNameList(orderNameList);
        } else {
            return orderSalaryList;
        }
    }

    @Override
    public List<OrderSalary> getMonthCutCountByMonthOrderNameList(String from, String to, List<String> orderNameList) {
        List<OrderSalary> orderSalaryList = new ArrayList<>();
        if (orderNameList != null && !orderNameList.isEmpty()){
            return orderSalaryMapper.getMonthCutCountByMonthOrderNameList(from, to, orderNameList);
        } else {
            return orderSalaryList;
        }
    }

    @Override
    public List<OrderSalary> getMonthPieceSalary(String from, String to, List<String> orderNameList) {
        List<OrderSalary> orderSalaryList = new ArrayList<>();
        if (orderNameList != null && !orderNameList.isEmpty()){
            return orderSalaryMapper.getMonthPieceSalary(from, to, orderNameList);
        } else {
            return orderSalaryList;
        }
    }

    @Override
    public List<OrderSalary> getMonthHourSalary(String from, String to) {
        List<OrderSalary> orderSalaryList = new ArrayList<>();
        try{
            orderSalaryList = orderSalaryMapper.getMonthHourSalary(from, to);
            return orderSalaryList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return orderSalaryList;
    }

    @Override
    public List<OrderSalary> getOrderCutInfoByMonth(String orderName) {
        List<OrderSalary> orderSalaryList = new ArrayList<>();
        try{
            orderSalaryList = orderSalaryMapper.getOrderCutInfoByMonth(orderName);
            return orderSalaryList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return orderSalaryList;
    }

    @Override
    public List<GeneralSalary> getOrderProcedureCountSummary(String orderName, Integer procedureNumber) {
        List<GeneralSalary> generalSalaryList = new ArrayList<>();
        try{
            generalSalaryList = orderSalaryMapper.getOrderProcedureCountSummary(orderName, procedureNumber);
            return generalSalaryList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return generalSalaryList;
    }
}
