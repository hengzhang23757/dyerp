package com.example.erp01.service.impl;

import com.example.erp01.mapper.MonthSalaryMapper;
import com.example.erp01.model.MonthSalary;
import com.example.erp01.service.MonthSalaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
@Service
public class MonthSalaryServiceImpl implements MonthSalaryService {

    @Autowired
    private MonthSalaryMapper monthSalaryMapper;

    @Override
    public Integer addMonthSalaryBatch(List<MonthSalary> monthSalaryList) {
        try{
            if (monthSalaryList != null && !monthSalaryList.isEmpty()){
                monthSalaryMapper.addMonthSalaryBatch(monthSalaryList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<MonthSalary> getMonthSalaryByMonthString(String monthString) {
        List<MonthSalary> monthSalaryList = new ArrayList<>();
        try{
            monthSalaryList = monthSalaryMapper.getMonthSalaryByMonthString(monthString);
            return monthSalaryList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return monthSalaryList;
    }

    @Override
    public Integer deleteMonthSalaryByMonthString(String monthString) {
        try{
            monthSalaryMapper.deleteMonthSalaryByMonthString(monthString);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }
}
