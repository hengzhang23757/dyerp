package com.example.erp01.service.impl;

import com.example.erp01.mapper.EmbInStoreMapper;
import com.example.erp01.model.CutLocation;
import com.example.erp01.model.CutQueryLeak;
import com.example.erp01.model.EmbStorage;
import com.example.erp01.service.EmbInStoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class EmbInStoreServiceImpl implements EmbInStoreService {

    @Autowired
    private EmbInStoreMapper embInStoreMapper;

    @Override
    public int addEmbInStore(List<EmbStorage> embStorageList) {
        try{
            if(embStorageList!=null && !embStorageList.isEmpty()){
                embInStoreMapper.addEmbInStore(embStorageList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public int deleteEmbInStore(List<String> subQcodeList) {
        try{
            embInStoreMapper.deleteEmbInStore(subQcodeList);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<CutLocation> getEmbInfo(String orderName, Integer bedNumber,String colorName, String sizeName, Integer packageNumber) {
        List<CutLocation> cutLocationList = new ArrayList<>();
        try{
            cutLocationList = embInStoreMapper.getEmbInfo(orderName, bedNumber,colorName, sizeName, packageNumber);
            return cutLocationList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return cutLocationList;

    }

    @Override
    public Integer getTotalEmbInStoreCountByOrder(String orderName) {
        Integer embInStoreCount = 0;
        try{
            if (embInStoreMapper.getTotalEmbInStoreCountByOrder(orderName) != null){
                embInStoreCount = embInStoreMapper.getTotalEmbInStoreCountByOrder(orderName);
            }
            return embInStoreCount;
        }catch (Exception e){
            e.printStackTrace();
        }
        return embInStoreCount;
    }

    @Override
    public List<EmbStorage> getEmbInStoreGroupByColor(String orderName) {
        List<EmbStorage> embStorageList = new ArrayList<>();
        try{
            embStorageList = embInStoreMapper.getEmbInStoreGroupByColor(orderName);
            return embStorageList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return embStorageList;
    }

    @Override
    public List<CutQueryLeak> getEmbInStoreInfoGroupByColorSize(String orderName, String colorName, String sizeName) {
        List<CutQueryLeak> cutQueryLeakList = new ArrayList<>();
        try{
            cutQueryLeakList = embInStoreMapper.getEmbInStoreInfoGroupByColorSize(orderName, colorName, sizeName);
            return cutQueryLeakList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return cutQueryLeakList;
    }
}
