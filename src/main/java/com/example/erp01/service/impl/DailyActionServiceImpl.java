package com.example.erp01.service.impl;

import com.example.erp01.mapper.DailyActionMapper;
import com.example.erp01.model.DailyAction;
import com.example.erp01.service.DailyActionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
@Service
public class DailyActionServiceImpl implements DailyActionService {

    @Autowired
    private DailyActionMapper dailyActionMapper;

    @Override
    public Integer addDailyAction(DailyAction dailyAction) {
        try{
            dailyActionMapper.addDailyAction(dailyAction);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public DailyAction getDailyActionByInfo(String orderName, String groupName, Date workDate) {
        DailyAction dailyAction;
        try{
            dailyAction = dailyActionMapper.getDailyActionByInfo(orderName, groupName, workDate);
            return dailyAction;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Integer updateDailyAction(DailyAction dailyAction) {
        try{
            dailyActionMapper.updateDailyAction(dailyAction);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer getSumLooseFabricCountByTask(Integer taskID) {
        Integer fabricCount = 0;
        try{
            if (dailyActionMapper.getSumLooseFabricCountByTask(taskID) != null){
                fabricCount = dailyActionMapper.getSumLooseFabricCountByTask(taskID);
            }
            return fabricCount;
        }catch (Exception e){
            e.printStackTrace();
        }
        return fabricCount;
    }
}
