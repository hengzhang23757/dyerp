package com.example.erp01.service.impl;

import com.example.erp01.mapper.FixedProcedureMapper;
import com.example.erp01.model.FixedProcedure;
import com.example.erp01.service.FixedProcedureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class FixedProcedureServiceImpl implements FixedProcedureService {

    @Autowired
    private FixedProcedureMapper fixedProcedureMapper;

    @Override
    public Integer addFixedProcedure(FixedProcedure fixedProcedure) {
        try{
            fixedProcedureMapper.addFixedProcedure(fixedProcedure);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer deleteFixedProcedure(Integer id) {
        try{
            fixedProcedureMapper.deleteFixedProcedure(id);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<FixedProcedure> getAllFixedProcedure() {
        List<FixedProcedure> fixedProcedureList = new ArrayList<>();
        try{
            fixedProcedureList = fixedProcedureMapper.getAllFixedProcedure();
            return fixedProcedureList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return fixedProcedureList;
    }
}
