package com.example.erp01.service.impl;

import com.example.erp01.mapper.ProcedureTemplateMapper;
import com.example.erp01.model.OrderProcedure;
import com.example.erp01.model.ProcedureInfo;
import com.example.erp01.model.ProcedureTemplate;
import com.example.erp01.service.ProcedureTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class ProcedureTemplateImpl implements ProcedureTemplateService {
    @Autowired
    private ProcedureTemplateMapper procedureTemplateMapper;
    @Override
    public Integer addProcedureTemplate(ProcedureTemplate procedureTemplate) {
        try{
            procedureTemplateMapper.addProcedureTemplate(procedureTemplate);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer addProcedureTemplateBatch(List<ProcedureTemplate> procedureTemplateList) {
        try{
            procedureTemplateMapper.addProcedureTemplateBatch(procedureTemplateList);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer deleteProcedureTemplate(Integer procedureID) {
        try{
            procedureTemplateMapper.deleteProcedureTemplate(procedureID);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer deleteProcedureTemplateBatch(List<Integer> procedureIDList) {
        try{
            if (procedureIDList != null && !procedureIDList.isEmpty()){
                procedureTemplateMapper.deleteProcedureTemplateBatch(procedureIDList);
            }
            return 0;
        } catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<ProcedureTemplate> getAllProcedureTemplate(Map<String, Object> map) {
        List<ProcedureTemplate> procedureTemplateList = new ArrayList<>();
        try{
            procedureTemplateList = procedureTemplateMapper.getAllProcedureTemplate(map);
            return procedureTemplateList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return procedureTemplateList;
    }

    @Override
    public Integer getMaxProcedureCode() {
        Integer tmp = 0;
        try{
            tmp = procedureTemplateMapper.getMaxProcedureCode();
            return tmp;
        }catch (Exception e){
            e.printStackTrace();
        }
        return tmp;
    }

    @Override
    public ProcedureTemplate getByID(Integer procedureID) {
        ProcedureTemplate procedureTemplate = null;
        try{
            procedureTemplate = procedureTemplateMapper.getByID(procedureID);
            return procedureTemplate;
        }catch (Exception e){
            e.printStackTrace();
        }
        return procedureTemplate;
    }

    @Override
    public String getProcedureNumberByProcedureCode(Integer procedureCode) {
        String procedureNumber = null;
        try{
            procedureNumber = procedureTemplateMapper.getProcedureNumberByProcedureCode(procedureCode);
            return procedureNumber;
        }catch (Exception e){
            e.printStackTrace();
        }
        return procedureNumber;
    }

    @Override
    public String getProcedureNameByProcedureCode(Integer procedureCode) {
        String procedureName = null;
        try{
            procedureName = procedureTemplateMapper.getProcedureNameByProcedureCode(procedureCode);
            return procedureName;
        }catch (Exception e){
            e.printStackTrace();
        }
        return procedureName;
    }

    @Override
    public Float getPiecePriceByProcedureCode(Integer procedureCode) {
        Float price = null;
        try{
            price = procedureTemplateMapper.getPiecePriceByProcedureCode(procedureCode);
            return price;
        }catch (Exception e){
            e.printStackTrace();
        }
        return price;
    }

    @Override
    public Integer updateProcedureTemplate(ProcedureTemplate procedureTemplate) {
        try{
            procedureTemplateMapper.updateProcedureTemplate(procedureTemplate);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public ProcedureInfo getProcedureInfoByID(Integer procedureID) {
        ProcedureInfo procedureInfo = null;
        try{
            procedureInfo = procedureTemplateMapper.getProcedureInfoByID(procedureID);
            return procedureInfo;
        }catch (Exception e){
            e.printStackTrace();
        }
        return procedureInfo;
    }

    @Override
    public List<ProcedureTemplate> getProcedureTemplateByLevel(String procedureLevel) {
        List<ProcedureTemplate> procedureTemplateList = new ArrayList<>();
        try{
            procedureTemplateList = procedureTemplateMapper.getProcedureTemplateByLevel(procedureLevel);
            return procedureTemplateList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return procedureTemplateList;
    }

    @Override
    public ProcedureTemplate getProcedureTemplateByProcedureCode(Integer procedureCode) {
        ProcedureTemplate procedureTemplate;
        try{
            procedureTemplate = procedureTemplateMapper.getProcedureTemplateByProcedureCode(procedureCode);
            return procedureTemplate;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<ProcedureTemplate> getProcedureTemplateHintByInfo(Integer procedureNumber, String subProcedureName, String subProcedureDescription, String subRemark,Integer page,Integer limit) {
        List<ProcedureTemplate> procedureTemplateList = new ArrayList<>();
        try{
            procedureTemplateList = procedureTemplateMapper.getProcedureTemplateHintByInfo(procedureNumber, subProcedureName, subProcedureDescription, subRemark,page,limit);
            return procedureTemplateList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return procedureTemplateList;
    }

    @Override
    public List<ProcedureTemplate> getOrderProcedureByMap(Map<String, Object> params) {
        return procedureTemplateMapper.getOrderProcedureByMap(params);
    }

}
