package com.example.erp01.service.impl;

import com.example.erp01.mapper.FabricHandOverMapper;
import com.example.erp01.model.FabricHandOver;
import com.example.erp01.service.FabricHandOverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class FabricHandOverServiceImpl implements FabricHandOverService {

    @Autowired
    private FabricHandOverMapper fabricHandOverMapper;

    @Override
    public Integer addFabricHandOverBatch(List<FabricHandOver> fabricHandOverList) {
        try{
            if (fabricHandOverList != null && fabricHandOverList.size() > 0){
                fabricHandOverMapper.addFabricHandOverBatch(fabricHandOverList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<FabricHandOver> getFabricHandOverByShelf(String shelfName) {
        List<FabricHandOver> fabricHandOverList = new ArrayList<>();
        try{
            fabricHandOverList = fabricHandOverMapper.getFabricHandOverByShelf(shelfName);
            return fabricHandOverList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return fabricHandOverList;
    }

    @Override
    public List<FabricHandOver> getFabricHandOverByInfo(Date from, Date to, String orderName, String colorName, String fabricColor) {
        List<FabricHandOver> fabricHandOverList = new ArrayList<>();
        try{
            fabricHandOverList = fabricHandOverMapper.getFabricHandOverByInfo(from, to, orderName, colorName, fabricColor);
            return fabricHandOverList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return fabricHandOverList;
    }

    @Override
    public Integer deleteFabricHandOverByShelf(String shelfName) {
        try{
            fabricHandOverMapper.deleteFabricHandOverByShelf(shelfName);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer updateFabricHandOverBatch(List<FabricHandOver> fabricHandOverList) {
        try{
            if (fabricHandOverList != null && fabricHandOverList.size() > 0){
                fabricHandOverMapper.updateFabricHandOverBatch(fabricHandOverList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<FabricHandOver> getFabricHandOverByQcode(Integer qCodeID) {
        List<FabricHandOver> fabricHandOverList = new ArrayList<>();
        try{
            fabricHandOverList = fabricHandOverMapper.getFabricHandOverByQcode(qCodeID);
            return fabricHandOverList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return fabricHandOverList;
    }

    @Override
    public List<String> getFabricHandOverColorHint(String orderName) {
        List<String> colorNameList = new ArrayList<>();
        try{
            colorNameList = fabricHandOverMapper.getFabricHandOverColorHint(orderName);
            return colorNameList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return colorNameList;
    }

    @Override
    public List<String> getFabricHandOverFabricColorHint(String orderName) {
        List<String> fabricColorList = new ArrayList<>();
        try{
            fabricColorList = fabricHandOverMapper.getFabricHandOverFabricColorHint(orderName);
            return fabricColorList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return fabricColorList;
    }
}
