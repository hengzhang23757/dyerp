package com.example.erp01.service.impl;

import com.example.erp01.mapper.YesterdayReportMapper;
import com.example.erp01.model.*;
import com.example.erp01.service.YesterdayReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class YesterdayReportServiceImpl implements YesterdayReportService {

    @Autowired
    private YesterdayReportMapper yesterdayReportMapper;

    @Override
    public List<YesterdayTailor> getYesterdayTailorReport() {
        List<YesterdayTailor> yesterdayTailorList = new ArrayList<>();
        try{
            yesterdayTailorList = yesterdayReportMapper.getYesterdayTailorReport();
            return yesterdayTailorList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return yesterdayTailorList;
    }

    @Override
    public List<YesterdayReWork> getYesterdayReWorkReport() {
        List<YesterdayReWork> yesterdayReWorkList = new ArrayList<>();
        try{
            yesterdayReWorkList = yesterdayReportMapper.getYesterdayReWorkReport();
            return yesterdayReWorkList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return yesterdayReWorkList;
    }

    @Override
    public List<YesterdayWorkShop> getYesterdayHangProduction() {
        List<YesterdayWorkShop> yesterdayWorkShopList = new ArrayList<>();
        try{
            yesterdayWorkShopList = yesterdayReportMapper.getYesterdayHangProduction();
            return yesterdayWorkShopList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return yesterdayWorkShopList;
    }

    @Override
    public List<YesterdayWorkShop> getYesterdayMiniProduction() {
        List<YesterdayWorkShop> yesterdayWorkShopList = new ArrayList<>();
        try{
            yesterdayWorkShopList = yesterdayReportMapper.getYesterdayMiniProduction();
            return yesterdayWorkShopList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return yesterdayWorkShopList;
    }

    @Override
    public Integer getHangWorkCountByProcedureNumber(String orderName, Integer procedureNumber) {
        Integer hangWorkCount = 0;
        try{
            if (yesterdayReportMapper.getHangWorkCountByProcedureNumber(orderName, procedureNumber) != null){
                hangWorkCount = yesterdayReportMapper.getHangWorkCountByProcedureNumber(orderName, procedureNumber);
            }
            return hangWorkCount;
        }catch (Exception e){
            e.printStackTrace();
        }
        return hangWorkCount;
    }

    @Override
    public Integer getMiniWorkCountByProcedureNumber(String orderName, Integer procedureNumber) {
        Integer miniWorkCount = 0;
        try{
            if (yesterdayReportMapper.getMiniWorkCountByProcedureNumber(orderName, procedureNumber) != null){
                miniWorkCount = yesterdayReportMapper.getMiniWorkCountByProcedureNumber(orderName, procedureNumber);
            }
            return miniWorkCount;
        }catch (Exception e){
            e.printStackTrace();
        }
        return miniWorkCount;
    }

    @Override
    public List<YesterdayFinish> getYesterdayFinishReport() {
        List<YesterdayFinish> yesterdayFinishList = new ArrayList<>();
        try{
            yesterdayFinishList = yesterdayReportMapper.getYesterdayFinishReport();
            return yesterdayFinishList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return yesterdayFinishList;
    }

    @Override
    public List<YesterdayFinish> getYesterdayHangFinishReport() {
        List<YesterdayFinish> yesterdayFinishList = new ArrayList<>();
        try{
            yesterdayFinishList = yesterdayReportMapper.getYesterdayHangFinishReport();
            return yesterdayFinishList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return yesterdayFinishList;
    }

    @Override
    public List<DailyFabric> getYesterdayFabricReport() {
        List<DailyFabric> dailyFabricList = new ArrayList<>();
        try{
            dailyFabricList = yesterdayReportMapper.getYesterdayFabricReport();
            return dailyFabricList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return dailyFabricList;
    }
}
