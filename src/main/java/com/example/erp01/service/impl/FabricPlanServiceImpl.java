//package com.example.erp01.service.impl;
//
//import com.example.erp01.mapper.FabricPlanMapper;
//import com.example.erp01.model.FabricPlan;
//import com.example.erp01.service.FabricPlanService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.List;
//@Service
//public class FabricPlanServiceImpl implements FabricPlanService {
//
//    @Autowired
//    private FabricPlanMapper fabricPlanMapper;
//
//    @Override
//    public int addFabricPlanBatch(List<FabricPlan> fabricPlanList) {
//        try{
//            if (fabricPlanList != null && !fabricPlanList.isEmpty()){
//                fabricPlanMapper.addFabricPlanBatch(fabricPlanList);
//            }
//            return 0;
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//        return 1;
//    }
//
//    @Override
//    public int deleteFabricPlan(Integer fabricPlanID) {
//        try{
//            fabricPlanMapper.deleteFabricPlan(fabricPlanID);
//            return 0;
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//        return 1;
//    }
//
//    @Override
//    public List<FabricPlan> getAllFabricPlan() {
//        List<FabricPlan> fabricPlanList = new ArrayList<>();
//        try{
//            fabricPlanList = fabricPlanMapper.getAllFabricPlan();
//            return fabricPlanList;
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//        return fabricPlanList;
//    }
//
//    @Override
//    public int updateFabricPlan(FabricPlan fabricPlan) {
//        try{
//            fabricPlanMapper.updateFabricPlan(fabricPlan);
//            return 0;
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//        return 1;
//    }
//
//    @Override
//    public List<FabricPlan> getUniqueFabricPlan() {
//        List<FabricPlan> fabricPlanList = new ArrayList<>();
//        try{
//            fabricPlanList = fabricPlanMapper.getUniqueFabricPlan();
//            return fabricPlanList;
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//        return fabricPlanList;
//    }
//
//    @Override
//    public List<FabricPlan> getFabricPlanDetailByOrder(String orderName) {
//        List<FabricPlan> fabricPlanList = new ArrayList<>();
//        try{
//            fabricPlanList = fabricPlanMapper.getFabricPlanDetailByOrder(orderName);
//            return fabricPlanList;
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//        return fabricPlanList;
//    }
//
//    @Override
//    public List<FabricPlan> getFabricPlanDetailByOrderSeasonDate(String orderName, Integer orderCount, String season, Date orderDate) {
//        List<FabricPlan> fabricPlanList = new ArrayList<>();
//        try{
//            fabricPlanList = fabricPlanMapper.getFabricPlanDetailByOrderSeasonDate(orderName, orderCount, season, orderDate);
//            return fabricPlanList;
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//        return fabricPlanList;
//    }
//
//    @Override
//    public int deleteFabricPlanByOrder(String orderName) {
//        try{
//            fabricPlanMapper.deleteFabricPlanByOrder(orderName);
//            return 0;
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//        return 1;
//    }
//
//    @Override
//    public int deleteFabricPlanByOrderSeasonDate(String orderName, Integer orderCount, String season, Date orderDate) {
//        try{
//            fabricPlanMapper.deleteFabricPlanByOrderSeasonDate(orderName, orderCount, season, orderDate);
//            return 0;
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//        return 1;
//    }
//
//    @Override
//    public List<String> getFabricNamesByOrder(String orderName) {
//        List<String> fabricNameList = new ArrayList<>();
//        try{
//            fabricNameList = fabricPlanMapper.getFabricNamesByOrder(orderName);
//            return fabricNameList;
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//        return fabricNameList;
//    }
//
//    @Override
//    public List<String> getFabricColorsByOrderFabric(String orderName, String fabricName) {
//        List<String> fabricColorList = new ArrayList<>();
//        try{
//            fabricColorList = fabricPlanMapper.getFabricColorsByOrderFabric(orderName, fabricName);
//            return fabricColorList;
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//        return fabricColorList;
//    }
//
//    @Override
//    public String getColorNameByOrderFabric(String orderName, String fabricName, String fabricColor) {
//        String colorName;
//        try{
//            colorName = fabricPlanMapper.getColorNameByOrderFabric(orderName, fabricName, fabricColor);
//            return colorName;
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//        return null;
//    }
//
//    @Override
//    public Integer getFabricOrderCountByOrderFabricColor(String orderName, String fabricName, String fabricColor) {
//        Integer fabricOrderCount = 0;
//        try{
//            if (fabricPlanMapper.getFabricOrderCountByOrderFabricColor(orderName, fabricName, fabricColor) != null){
//                fabricOrderCount = fabricPlanMapper.getFabricOrderCountByOrderFabricColor(orderName, fabricName, fabricColor);
//            }
//            return fabricOrderCount;
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//        return fabricOrderCount;
//    }
//
//    @Override
//    public String getSupplierByOrderFabricColor(String orderName, String fabricName, String fabricColor) {
//        String supplier = "未知";
//        try{
//            if (fabricPlanMapper.getSupplierByOrderFabricColor(orderName, fabricName, fabricColor) != null){
//                supplier = fabricPlanMapper.getSupplierByOrderFabricColor(orderName, fabricName, fabricColor);
//            }
//            return supplier;
//
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//        return supplier;
//    }
//
//    @Override
//    public List<String> getColorNamesByOrderFabric(String orderName, String fabricName, String fabricColor) {
//        List<String> colorNameList = new ArrayList<>();
//        try{
//            colorNameList = fabricPlanMapper.getColorNamesByOrderFabric(orderName, fabricName, fabricColor);
//            return colorNameList;
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//        return colorNameList;
//    }
//}
