package com.example.erp01.service.impl;

import com.example.erp01.mapper.RankNoticeMapper;
import com.example.erp01.model.RankNotice;
import com.example.erp01.service.RankNoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RankNoticeServiceImpl implements RankNoticeService {

    @Autowired
    private RankNoticeMapper rankNoticeMapper;

    @Override
    public Integer addRankNotice(RankNotice rankNotice) {
        try{
            rankNoticeMapper.addRankNotice(rankNotice);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer deleteRankNotice(Integer id) {
        try{
            rankNoticeMapper.deleteRankNotice(id);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<RankNotice> getRankNoticeByRole(String role) {
        List<RankNotice> rankNoticeList = new ArrayList<>();
        try{
            rankNoticeList = rankNoticeMapper.getRankNoticeByRole(role);
            return rankNoticeList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return rankNoticeList;
    }
}
