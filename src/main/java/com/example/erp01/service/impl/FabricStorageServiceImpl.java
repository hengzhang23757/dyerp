package com.example.erp01.service.impl;

import com.example.erp01.mapper.FabricMapper;
import com.example.erp01.mapper.FabricStorageMapper;
import com.example.erp01.model.FabricStorage;
import com.example.erp01.service.FabricStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
@Service
public class FabricStorageServiceImpl implements FabricStorageService {
    @Autowired
    private FabricStorageMapper fabricStorageMapper;

    @Override
    public Integer addFabricStorageBatch(List<FabricStorage> fabricStorageList) {
        try{
            if (fabricStorageList != null && !fabricStorageList.isEmpty()){
                fabricStorageMapper.addFabricStorageBatch(fabricStorageList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer addFabricStorage(FabricStorage fabricStorage) {
        try{
            if (fabricStorage != null){
                fabricStorageMapper.addFabricStorage(fabricStorage);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer deleteFabricStorage(Integer fabricStorageID) {
        try{
            fabricStorageMapper.deleteFabricStorage(fabricStorageID);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<FabricStorage> getAllFabricStorage() {
        List<FabricStorage> fabricStorageList = new ArrayList<>();
        try{
            fabricStorageList = fabricStorageMapper.getAllFabricStorage();
            return fabricStorageList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return fabricStorageList;
    }

    @Override
    public FabricStorage getFabricStorageByOrderLocationJar(String orderName, String colorName, String fabricName, String fabricColor, String jarName, String location, Integer fabricID) {
        FabricStorage fabricStorage;
        try{
            fabricStorage = fabricStorageMapper.getFabricStorageByOrderLocationJar(orderName, colorName, fabricName, fabricColor, jarName, location, fabricID);
            return fabricStorage;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }


    @Override
    public List<FabricStorage> getFabricStorageByOrder(String orderName) {
        List<FabricStorage> fabricStorageList = new ArrayList<>();
        try{
            fabricStorageList = fabricStorageMapper.getFabricStorageByOrder(orderName);
            return fabricStorageList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return fabricStorageList;
    }

    @Override
    public Integer updateFabricStorage(FabricStorage fabricStorage) {
        try{
            fabricStorageMapper.updateFabricStorage(fabricStorage);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer getFabricBatchNumberByOrderFabricJarLocation(String orderName, String fabricName, String fabricColor, String jarName, String location) {
        Integer batchNumber  = 0;
        try{
            if (fabricStorageMapper.getFabricBatchNumberByOrderFabricJarLocation(orderName, fabricName, fabricColor, jarName, location) != null){
                batchNumber = fabricStorageMapper.getFabricBatchNumberByOrderFabricJarLocation(orderName, fabricName, fabricColor, jarName, location);
            }
            return batchNumber;
        }catch (Exception e){
            e.printStackTrace();
        }
        return batchNumber;
    }

    @Override
    public FabricStorage getFabricStorageByOrderColorLocationJar(String orderName, String fabricName, String colorName, String fabricColor, String jarName, String location, Integer fabricID) {
        FabricStorage fabricStorage;
        try{
            fabricStorage = fabricStorageMapper.getFabricStorageByOrderColorLocationJar(orderName, fabricName, colorName, fabricColor, jarName, location, fabricID);
            return fabricStorage;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public FabricStorage getFabricStorageByID(Integer fabricStorageID) {
        FabricStorage fabricStorage;
        try{
            fabricStorage = fabricStorageMapper.getFabricStorageByID(fabricStorageID);
            return fabricStorage;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<FabricStorage> getFabricStorageByInfo(String orderName, String fabricName, String colorName, String fabricColor, Integer fabricID) {
        List<FabricStorage> fabricStorageList = new ArrayList<>();
        try{
            fabricStorageList = fabricStorageMapper.getFabricStorageByInfo(orderName, fabricName, colorName, fabricColor, fabricID);
            return fabricStorageList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return fabricStorageList;
    }

    @Override
    public Integer getFabricBatchNumberByOrder(String orderName, List<String> colorNameList) {
        Integer batchNumber = 0;
        try{
            if (fabricStorageMapper.getFabricBatchNumberByOrder(orderName, colorNameList) != null){
                batchNumber = fabricStorageMapper.getFabricBatchNumberByOrder(orderName, colorNameList);
            }
            return batchNumber;
        }catch (Exception e){
            e.printStackTrace();
        }
        return batchNumber;
    }

    @Override
    public List<FabricStorage> getFabricStorageByOrderList(List<String> orderNameList) {
        List<FabricStorage> fabricStorageList = new ArrayList<>();
        try{
            fabricStorageList = fabricStorageMapper.getFabricStorageByOrderList(orderNameList);
            return fabricStorageList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return fabricStorageList;
    }

    @Override
    public List<FabricStorage> getFabricStorageByIdList(List<Integer> fabricStorageIDList) {
        List<FabricStorage> fabricStorageList = new ArrayList<>();
        try{
            if (fabricStorageIDList != null && !fabricStorageIDList.isEmpty()){
                fabricStorageList = fabricStorageMapper.getFabricStorageByIdList(fabricStorageIDList);
            }
            return fabricStorageList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return fabricStorageList;
    }

    @Override
    public Integer deleteFabricStorageBatch(List<Integer> fabricStorageIDList) {
        try{
            if (fabricStorageIDList != null && !fabricStorageIDList.isEmpty()){
                fabricStorageMapper.deleteFabricStorageBatch(fabricStorageIDList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer deleteFabricStorageByOrderNameJar(String orderName, String jarName) {
        try{
            fabricStorageMapper.deleteFabricStorageByOrderNameJar(orderName, jarName);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }
}
