package com.example.erp01.service.impl;

import com.example.erp01.mapper.ManufactureFabricMapper;
import com.example.erp01.model.AddFeeVo;
import com.example.erp01.model.FabricAccessoryReview;
import com.example.erp01.model.ManufactureFabric;
import com.example.erp01.service.ManufactureFabricService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class ManufactureFabricServiceImpl implements ManufactureFabricService {

    @Autowired
    private ManufactureFabricMapper manufactureFabricMapper;

    @Override
    public Integer addManufactureFabricBatch(List<ManufactureFabric> manufactureFabricList) {
        try{
            if (manufactureFabricList != null && manufactureFabricList.size() > 0){
                manufactureFabricMapper.addManufactureFabricBatch(manufactureFabricList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<ManufactureFabric> getManufactureFabricByOrder(String orderName, String colorName) {
        List<ManufactureFabric> manufactureFabricList = new ArrayList<>();
        try{
            manufactureFabricList = manufactureFabricMapper.getManufactureFabricByOrder(orderName, colorName);
            return manufactureFabricList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return manufactureFabricList;
    }

    @Override
    public Integer deleteManufactureFabricByName(String orderName) {
        try{
            manufactureFabricMapper.deleteManufactureFabricByName(orderName);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer deleteManufactureFabricByID(Integer fabricID) {
        try{
            manufactureFabricMapper.deleteManufactureFabricByID(fabricID);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer updateManufactureFabric(ManufactureFabric manufactureFabric) {
        try{
            manufactureFabricMapper.updateManufactureFabric(manufactureFabric);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<ManufactureFabric> getManufactureFabricHintByFabricName(String subFabricName,Integer page,Integer limit) {
        List<ManufactureFabric> manufactureFabricList = new ArrayList<>();
        try{
            manufactureFabricList = manufactureFabricMapper.getManufactureFabricHintByFabricName(subFabricName, page, limit);
            return manufactureFabricList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return manufactureFabricList;
    }

    @Override
    public Integer updateManufactureFabricOnPlaceOrder(List<ManufactureFabric> manufactureFabricList) {
        try{
            if (manufactureFabricList != null && manufactureFabricList.size() > 0){
                manufactureFabricMapper.updateManufactureFabricOnPlaceOrder(manufactureFabricList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<String> getDistinctSupplier() {
        List<String> supplierList = new ArrayList<>();
        try{
            supplierList = manufactureFabricMapper.getDistinctSupplier();
            return supplierList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return supplierList;
    }

    @Override
    public List<ManufactureFabric> getManufactureFabricOneYear(String orderName) {
        List<ManufactureFabric> manufactureFabricList = new ArrayList<>();
        try{
            manufactureFabricList = manufactureFabricMapper.getManufactureFabricOneYear(orderName);
            return manufactureFabricList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return manufactureFabricList;
    }

    @Override
    public ManufactureFabric getManufactureFabricByID(Integer fabricID) {
        try{
            return manufactureFabricMapper.getManufactureFabricByID(fabricID);
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<ManufactureFabric> getManufactureFabricByInfo(String orderName, String fabricName, String colorName, String fabricColor) {
        List<ManufactureFabric> manufactureFabricList = new ArrayList<>();
        try{
            manufactureFabricList = manufactureFabricMapper.getManufactureFabricByInfo(orderName, fabricName, colorName, fabricColor);
            return manufactureFabricList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return manufactureFabricList;
    }

    @Override
    public List<ManufactureFabric> getManufactureFabricInPlaceOrder(String orderName, String supplier, String checkState) {
        List<ManufactureFabric> manufactureFabricList = new ArrayList<>();
        try{
            manufactureFabricList = manufactureFabricMapper.getManufactureFabricInPlaceOrder(orderName, supplier, checkState);
            return manufactureFabricList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return manufactureFabricList;
    }

    @Override
    public Integer changeFabricOrderStateInPlaceOrder(List<Integer> fabricIDList) {
        try{
            if (fabricIDList != null && fabricIDList.size() > 0){
                manufactureFabricMapper.changeFabricOrderStateInPlaceOrder(fabricIDList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<ManufactureFabric> getManufactureFabricSummaryByOrder(String orderName) {
        List<ManufactureFabric> manufactureFabricList = new ArrayList<>();
        try{
            manufactureFabricList = manufactureFabricMapper.getManufactureFabricSummaryByOrder(orderName);
            return manufactureFabricList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return manufactureFabricList;
    }

    @Override
    public Integer updateManufactureFabricBatch(List<ManufactureFabric> manufactureFabricList) {
        try{
            if (manufactureFabricList != null && manufactureFabricList.size() > 0){
                manufactureFabricMapper.updateManufactureFabricBatch(manufactureFabricList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer changeFabricCheckStateInPlaceOrder(List<Integer> fabricIDList, String preCheck, String checkState, String preCheckEmp, String checkEmp) {
        try{
            if (fabricIDList != null && fabricIDList.size() > 0){
                manufactureFabricMapper.changeFabricCheckStateInPlaceOrder(fabricIDList, preCheck, checkState, preCheckEmp, checkEmp);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<ManufactureFabric> getManufactureFabricByOrderCheckState(String orderName, String checkState) {
        List<ManufactureFabric> manufactureFabricList = new ArrayList<>();
        try{
            manufactureFabricList = manufactureFabricMapper.getManufactureFabricByOrderCheckState(orderName, checkState);
            return manufactureFabricList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return manufactureFabricList;
    }

    @Override
    public List<ManufactureFabric> getManufactureFabricByOrderListCheckState(List<String> orderNameList, String checkState) {
        List<ManufactureFabric> manufactureFabricList = new ArrayList<>();
        try{
            manufactureFabricList = manufactureFabricMapper.getManufactureFabricByOrderListCheckState(orderNameList, checkState);
            return manufactureFabricList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return manufactureFabricList;
    }

    @Override
    public List<String> getFabricNameHint(String subName) {
        List<String> fabricNameList = new ArrayList<>();
        try{
            fabricNameList = manufactureFabricMapper.getFabricNameHint(subName);
            return fabricNameList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return fabricNameList;
    }

    @Override
    public List<ManufactureFabric> getManufactureFabricInPlaceOrderByInfo(List<String> orderNameList, String supplier, String materialName, String checkState) {
        List<ManufactureFabric> manufactureFabricList = new ArrayList<>();
        try{
            manufactureFabricList = manufactureFabricMapper.getManufactureFabricInPlaceOrderByInfo(orderNameList, supplier, materialName, checkState);
            return manufactureFabricList;
        } catch (Exception e){
            e.printStackTrace();
        }
        return manufactureFabricList;
    }

    @Override
    public List<FabricAccessoryReview> getManufactureFabricReviewState(String checkState) {
        List<FabricAccessoryReview> fabricAccessoryReviewList = new ArrayList<>();
        try{
            fabricAccessoryReviewList = manufactureFabricMapper.getManufactureFabricReviewState(checkState);
            return fabricAccessoryReviewList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return fabricAccessoryReviewList;
    }

    @Override
    public List<ManufactureFabric> getFabricCheckStateRecentYearByOrder(List<String> orderNameList) {
        List<ManufactureFabric> manufactureFabricList = new ArrayList<>();
        try{
            manufactureFabricList = manufactureFabricMapper.getFabricCheckStateRecentYearByOrder(orderNameList);
            return manufactureFabricList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return manufactureFabricList;
    }

    @Override
    public List<ManufactureFabric> getManufactureFabricByCheckNumberState(String checkNumber, String checkState) {
        List<ManufactureFabric> manufactureFabricList = new ArrayList<>();
        try{
            manufactureFabricList = manufactureFabricMapper.getManufactureFabricByCheckNumberState(checkNumber, checkState);
            return manufactureFabricList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return manufactureFabricList;
    }

    @Override
    public Integer destroyFabricCheck(String checkNumber) {
        try{
            manufactureFabricMapper.destroyFabricCheck(checkNumber);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer getFabricCheckNumberCountByState(String checkState) {
        Integer checkNumberCount = 0;
        try{
            if (manufactureFabricMapper.getFabricCheckNumberCountByState(checkState) != null){
                checkNumberCount = manufactureFabricMapper.getFabricCheckNumberCountByState(checkState);
            }
            return checkNumberCount;
        } catch (Exception e){
            e.printStackTrace();
        }
        return checkNumberCount;
    }

    @Override
    public ManufactureFabric getOneManufactureFabricByInfo(String orderName, String colorName, String fabricName) {
        ManufactureFabric manufactureFabric = null;
        try{
            manufactureFabric = manufactureFabricMapper.getOneManufactureFabricByInfo(orderName, colorName, fabricName);
            return manufactureFabric;
        }catch (Exception e){
            e.printStackTrace();
        }
        return manufactureFabric;
    }

    @Override
    public Integer updateManufactureFabricAfterCommit(List<ManufactureFabric> manufactureFabricList) {
        try{
            if (manufactureFabricList != null && !manufactureFabricList.isEmpty()){
                manufactureFabricMapper.updateManufactureFabricAfterCommit(manufactureFabricList);
            }
            return 0;
        } catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer updateFabricOrderStateByCheckNumber(String checkNumber) {
        try{
            manufactureFabricMapper.updateFabricOrderStateByCheckNumber(checkNumber);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<String> getDistinctFabricNameByOrder(String orderName) {
        List<String> fabricNameList = new ArrayList<>();
        try{
            fabricNameList = manufactureFabricMapper.getDistinctFabricNameByOrder(orderName);
            return fabricNameList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return fabricNameList;
    }

    @Override
    public Integer deleteManufactureFabricBatch(List<Integer> fabricIDList) {
        try{
            if (fabricIDList != null && !fabricIDList.isEmpty()){
                manufactureFabricMapper.deleteManufactureFabricBatch(fabricIDList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<ManufactureFabric> getManufactureFabricByFabricIdList(List<Integer> fabricIDList) {
        List<ManufactureFabric> manufactureFabricList = new ArrayList<>();
        try{
            if (fabricIDList != null && !fabricIDList.isEmpty()){
                manufactureFabricList = manufactureFabricMapper.getManufactureFabricByFabricIdList(fabricIDList);
            }
            return manufactureFabricList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return manufactureFabricList;
    }

    @Override
    public Integer reAddManufactureFabric(ManufactureFabric manufactureFabric) {
        try{
            manufactureFabricMapper.reAddManufactureFabric(manufactureFabric);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<String> getFabricCheckNumberHint(String keyWord) {
        return manufactureFabricMapper.getFabricCheckNumberHint(keyWord);
    }

    @Override
    public List<FabricAccessoryReview> getManufactureFabricReviewStateCombine(Map<String, Object> map) {
        return manufactureFabricMapper.getManufactureFabricReviewStateCombine(map);
    }

    @Override
    public void changeFeeStateBatch(List<Integer> fabricIDList, Integer feeState) {
        if (fabricIDList != null && !fabricIDList.isEmpty()){
            manufactureFabricMapper.changeFeeStateBatch(fabricIDList, feeState);
        }
    }

    @Override
    public List<AddFeeVo> getFeeVoByInfo(Map<String, Object> map) {
        return manufactureFabricMapper.getFeeVoByInfo(map);
    }

    @Override
    public void changeOrderName(String orderName, String toOrderName) {
        manufactureFabricMapper.changeOrderName(orderName, toOrderName);
    }
}
