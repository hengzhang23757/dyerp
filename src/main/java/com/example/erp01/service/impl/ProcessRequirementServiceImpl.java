package com.example.erp01.service.impl;

import com.example.erp01.mapper.ProcessRequirementMapper;
import com.example.erp01.model.ProcessRequirement;
import com.example.erp01.service.ProcessRequirementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProcessRequirementServiceImpl implements ProcessRequirementService {

    @Autowired
    private ProcessRequirementMapper processRequirementMapper;

    @Override
    public Integer addProcessRequirementBatch(List<ProcessRequirement> processRequirementList) {
        try{
            if (processRequirementList != null && processRequirementList.size() > 0){
                processRequirementMapper.addProcessRequirementBatch(processRequirementList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<ProcessRequirement> getProcessRequirementByOrder(String orderName) {
        List<ProcessRequirement> processRequirementList = new ArrayList<>();
        try{
            processRequirementList = processRequirementMapper.getProcessRequirementByOrder(orderName);
            return processRequirementList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return processRequirementList;
    }

    @Override
    public Integer deleteProcessRequirementByOrder(String orderName) {
        try{
            processRequirementMapper.deleteProcessRequirementByOrder(orderName);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer deleteProcessRequirementByID(Integer processRequirementID) {
        try{
            processRequirementMapper.deleteProcessRequirementByID(processRequirementID);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer updateProcessRequirement(ProcessRequirement processRequirement) {
        try{
            processRequirementMapper.updateProcessRequirement(processRequirement);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer addProcessRequirement(ProcessRequirement processRequirement) {
        try{
            processRequirementMapper.addProcessRequirement(processRequirement);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }
}
