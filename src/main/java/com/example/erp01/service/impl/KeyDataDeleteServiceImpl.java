package com.example.erp01.service.impl;

import com.example.erp01.mapper.KeyDataDeleteMapper;
import com.example.erp01.model.PieceWork;
import com.example.erp01.model.PieceWorkBack;
import com.example.erp01.model.PieceWorkDelete;
import com.example.erp01.service.KeyDataDeleteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class KeyDataDeleteServiceImpl implements KeyDataDeleteService {

    @Autowired
    private KeyDataDeleteMapper keyDataDeleteMapper;

    @Override
    public PieceWorkDelete getPieceWorkByDeleteId(Integer pieceWorkID) {
        return keyDataDeleteMapper.getPieceWorkByDeleteId(pieceWorkID);
    }

    @Override
    public List<PieceWorkDelete> getPieceWorkListByDeleteId(List<Integer> pieceWorkIDList) {
        List<PieceWorkDelete> pieceWorkDeleteList = new ArrayList<>();
        try{
            if (pieceWorkIDList != null && !pieceWorkIDList.isEmpty()){
                pieceWorkDeleteList = keyDataDeleteMapper.getPieceWorkListByDeleteId(pieceWorkIDList);
            }
            return pieceWorkDeleteList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return pieceWorkDeleteList;
    }

    @Override
    public Integer addPieceWorkDeleteBatch(List<PieceWorkDelete> pieceWorkDeleteList) {
        try{
            if (pieceWorkDeleteList != null && !pieceWorkDeleteList.isEmpty()){
                keyDataDeleteMapper.addPieceWorkDeleteBatch(pieceWorkDeleteList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public String getEarlyCreateTimeOfDeleteData(List<Integer> pieceWorkIDList) {
        String createTime = "2020-08-01";
        try{
            if (pieceWorkIDList != null && !pieceWorkIDList.isEmpty()){
                createTime = keyDataDeleteMapper.getEarlyCreateTimeOfDeleteData(pieceWorkIDList);
            }
            return createTime;
        }catch (Exception e){
            e.printStackTrace();
        }
        return createTime;
    }

    @Override
    public List<PieceWork> getPieceWordByStep(Date pieceWorkDate) {
        return keyDataDeleteMapper.getPieceWordByStep(pieceWorkDate);
    }

    @Override
    public Integer addPieceWordBackBatch(List<PieceWorkBack> pieceWorkBackList) {
        try{
            if (pieceWorkBackList != null && !pieceWorkBackList.isEmpty()){
                keyDataDeleteMapper.addPieceWordBackBatch(pieceWorkBackList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer deletePieceWorkBacked(Date pieceWorkDate) {
        try{
            keyDataDeleteMapper.deletePieceWorkBacked(pieceWorkDate);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<PieceWorkDelete> getPieceWorkDeleteByMap(Map<String, Object> map) {
        return keyDataDeleteMapper.getPieceWorkDeleteByMap(map);
    }

    @Override
    public Integer deleteKeyData(List<Integer> idList) {
        try{
            if (idList != null && !idList.isEmpty()){
                keyDataDeleteMapper.deleteKeyData(idList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }
}
