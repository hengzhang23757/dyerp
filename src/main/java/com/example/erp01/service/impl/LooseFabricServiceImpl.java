package com.example.erp01.service.impl;

import com.example.erp01.mapper.LooseFabricMapper;
import com.example.erp01.model.LooseFabric;
import com.example.erp01.service.LooseFabricService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class LooseFabricServiceImpl implements LooseFabricService {

    @Autowired
    private LooseFabricMapper looseFabricMapper;

    @Override
    public Integer saveLooseFabricData(List<LooseFabric> looseFabricList) {
        try{
            if (looseFabricList != null && !looseFabricList.isEmpty()){
                looseFabricMapper.saveLooseFabricData(looseFabricList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer getFinishLooseFabricCount(String styleNumber, String colorName, String fabricName, String fabricColor, String jarNumber) {
        int finishCount = 0;
        try{
            if (looseFabricMapper.getFinishLooseFabricCount(styleNumber, colorName, fabricName, fabricColor, jarNumber) != null){
                finishCount = looseFabricMapper.getFinishLooseFabricCount(styleNumber, colorName, fabricName, fabricColor, jarNumber);
            }
            return finishCount;
        }catch (Exception e){
            e.printStackTrace();
        }
        return finishCount;
    }

    @Override
    public LooseFabric getLooseFabricByID(Integer qCodeID) {
        LooseFabric looseFabric = null;
        try{
            looseFabric = looseFabricMapper.getLooseFabricByID(qCodeID);
            return looseFabric;
        }catch (Exception e){
            e.printStackTrace();
        }
        return looseFabric;
    }

    @Override
    public List<LooseFabric> getLooseFabricByOrderFabric(String orderName, String colorName, String fabricName, String fabricColor) {
        List<LooseFabric> looseFabricList = new ArrayList<>();
        try{
            looseFabricList = looseFabricMapper.getLooseFabricByOrderFabric(orderName, colorName, fabricName, fabricColor);
            return looseFabricList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return looseFabricList;
    }

    @Override
    public Integer updateLooseFabric(List<LooseFabric> looseFabricList) {
        try{
            if (looseFabricList != null && !looseFabricList.isEmpty()){
                looseFabricMapper.updateLooseFabric(looseFabricList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<LooseFabric> getMaxLooseFabricInfoByJar(String orderName, String fabricName, String fabricColor) {
        List<LooseFabric> looseFabricList = new ArrayList<>();
        try{
            looseFabricList = looseFabricMapper.getMaxLooseFabricInfoByJar(orderName, fabricName, fabricColor);
            return looseFabricList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return looseFabricList;
    }

    @Override
    public List<LooseFabric> getFabricJarByInfo(String orderName, String colorName, String fabricName, String fabricColor) {
        List<LooseFabric> looseFabricList = new ArrayList<>();
        try{
            looseFabricList = looseFabricMapper.getFabricJarByInfo(orderName, colorName, fabricName, fabricColor);
            return looseFabricList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return looseFabricList;
    }

    @Override
    public Integer updateJarByInfo(String orderName, String fabricName, String fabricColor, String jarNumber, String colorName, String updateJar) {
        try{
            looseFabricMapper.updateJarByInfo(orderName, fabricName, fabricColor, jarNumber, colorName, updateJar);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<LooseFabric> getMaxLooseFabricInfoByInfo(String orderName, String colorName, String fabricName, String fabricColor) {
        List<LooseFabric> looseFabricList = new ArrayList<>();
        try{
            looseFabricList = looseFabricMapper.getMaxLooseFabricInfoByInfo(orderName, colorName, fabricName, fabricColor);
            return looseFabricList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return looseFabricList;
    }

    @Override
    public Integer getLooseCountByOrder(String orderName, List<String> colorNameList, Integer cutState) {
        Integer looseCount = 0;
        try{
            if (looseFabricMapper.getLooseCountByOrder(orderName, colorNameList, cutState) != null){
                looseCount = looseFabricMapper.getLooseCountByOrder(orderName, colorNameList, cutState);
            }
            return looseCount;
        } catch (Exception e){
            e.printStackTrace();
        }
        return looseCount;
    }

    @Override
    public Integer changeStateInTailorScan(Integer qCodeID) {
        try{
            looseFabricMapper.changeStateInTailorScan(qCodeID);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Float getPreCutFabricWeightByOrderColor(String orderName, String colorName, String jarNumber) {
        Float weight = 0f;
        try{
            if (looseFabricMapper.getPreCutFabricWeightByOrderColor(orderName, colorName, jarNumber) != null){
                weight = looseFabricMapper.getPreCutFabricWeightByOrderColor(orderName, colorName, jarNumber);
            }
            return weight;
        } catch (Exception e){
            e.printStackTrace();
        }
        return weight;
    }

    @Override
    public Integer deleteLooseFabricByOrderNameJar(String orderName, String jarNumber) {
        try{
            looseFabricMapper.deleteLooseFabricByOrderNameJar(orderName, jarNumber);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }
}
