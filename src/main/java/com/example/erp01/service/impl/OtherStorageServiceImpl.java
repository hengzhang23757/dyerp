package com.example.erp01.service.impl;

import com.example.erp01.mapper.OtherStorageMapper;
import com.example.erp01.model.OtherStorage;
import com.example.erp01.service.OtherStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class OtherStorageServiceImpl implements OtherStorageService {

    @Autowired
    private OtherStorageMapper otherStorageMapper;

    @Override
    public int otherInStore(List<OtherStorage> otherStorageList) {
        try{
            if(otherStorageList!=null && !otherStorageList.isEmpty()){
                otherStorageMapper.otherInStore(otherStorageList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public int otherOutStore(List<OtherStorage> otherStorageList) {
        try{
            if(otherStorageList!=null && !otherStorageList.isEmpty()){
                otherStorageMapper.otherOutStore(otherStorageList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<OtherStorage> searchOtherInStorage(String orderName, String partName, String colorName, String sizeName) {
        List<OtherStorage> otherStorageList = new ArrayList<>();
        try{
            otherStorageList = otherStorageMapper.searchOtherInStorage(orderName, partName, colorName, sizeName);
            return otherStorageList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return otherStorageList;
    }

    @Override
    public List<OtherStorage> searchOtherOutStorage(String orderName, String partName, String colorName, String sizeName) {
        List<OtherStorage> otherStorageList = new ArrayList<>();
        try{
            otherStorageList = otherStorageMapper.searchOtherOutStorage(orderName, partName, colorName, sizeName);
            return otherStorageList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return otherStorageList;
    }


}
