package com.example.erp01.service.impl;

import com.example.erp01.mapper.SizeManageMapper;
import com.example.erp01.model.SizeManage;
import com.example.erp01.service.SizeManageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
@Service
public class SizeManageServiceImpl implements SizeManageService {

    @Autowired
    private SizeManageMapper sizeManageMapper;

    @Override
    public List<SizeManage> getAllSizeManage() {
        List<SizeManage> sizeManageList = new ArrayList<>();
        try{
            sizeManageList = sizeManageMapper.getAllSizeManage();
            return sizeManageList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return sizeManageList;
    }

    @Override
    public Integer addSizeManageOne(SizeManage sizeManage) {
        try{
            sizeManageMapper.addSizeManageOne(sizeManage);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer deleteSizeManageById(Integer sizeManageID) {
        try{
            sizeManageMapper.deleteSizeManageById(sizeManageID);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<String> getAllSizeName() {
        List<String> sizeNameList = new ArrayList<>();
        try{
            sizeNameList = sizeManageMapper.getAllSizeName();
            return sizeNameList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return sizeNameList;
    }
}
