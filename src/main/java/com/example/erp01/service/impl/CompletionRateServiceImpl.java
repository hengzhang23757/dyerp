package com.example.erp01.service.impl;

import com.example.erp01.mapper.CompletionRateMapper;
import com.example.erp01.model.CompletionRate;
import com.example.erp01.service.CompletionRateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class CompletionRateServiceImpl implements CompletionRateService {
    @Autowired
    private CompletionRateMapper completionRateMapper;

    @Override
    public List<CompletionRate> getEmpCountEachDay(Date fromDate, Date toDate, String orderName, String groupName) {
        List<CompletionRate> completionRateList = new ArrayList<>();
        try{
            completionRateList = completionRateMapper.getEmpCountEachDay(fromDate, toDate, orderName, groupName);
            return completionRateList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return completionRateList;
    }

    @Override
    public CompletionRate getOrderBeginDate(String orderName, String groupName) {
        CompletionRate completionRate;
        try{
            completionRate = completionRateMapper.getOrderBeginDate(orderName, groupName);
            return completionRate;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Integer getFinishCount(String orderName, String groupName) {
        Integer finishCount = 0;
        try{
            finishCount = completionRateMapper.getFinishCount(orderName, groupName);
            return finishCount;
        }catch (Exception e){
            e.printStackTrace();
        }
        return finishCount;
    }

    @Override
    public Integer getTodayActualCount(String orderName, String groupName, Date searchDate) {
        Integer todayActualCount = 0;
        try{
            todayActualCount = completionRateMapper.getTodayActualCount(orderName, groupName, searchDate);
            return todayActualCount;
        }catch (Exception e){
            e.printStackTrace();
        }
        return todayActualCount;
    }

    @Override
    public Integer getFinishCountByTime(String orderName, String groupName, Date searchDate) {
        Integer finishCount = 0;
        try{
            finishCount = completionRateMapper.getFinishCountByTime(orderName, groupName, searchDate);
            return finishCount;
        }catch (Exception e){
            e.printStackTrace();
        }
        return finishCount;
    }
}
