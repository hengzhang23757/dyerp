package com.example.erp01.service.impl;

import com.example.erp01.mapper.ClothesDevFabricNumberMapper;
import com.example.erp01.model.ClothesDevFabricNumber;
import com.example.erp01.service.ClothesDevFabricNumberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClothesDevFabricNumberServiceImpl implements ClothesDevFabricNumberService {

    @Autowired
    private ClothesDevFabricNumberMapper clothesDevFabricNumberMapper;

    @Override
    public Integer addClothesDevFabricNumber(ClothesDevFabricNumber clothesDevFabricNumber) {
        try{
            clothesDevFabricNumberMapper.addClothesDevFabricNumber(clothesDevFabricNumber);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer deleteClothesDevFabricNumber(Integer id) {
        try{
            clothesDevFabricNumberMapper.deleteClothesDevFabricNumber(id);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<ClothesDevFabricNumber> getAllClothesDevFabricNumber() {
        return clothesDevFabricNumberMapper.getAllClothesDevFabricNumber();
    }

    @Override
    public Integer updateClothesDevFabricNumber(ClothesDevFabricNumber clothesDevFabricNumber) {
        try{
            clothesDevFabricNumberMapper.updateClothesDevFabricNumber(clothesDevFabricNumber);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }
}
