package com.example.erp01.service.impl;

import com.example.erp01.mapper.EmployeeMapper;
import com.example.erp01.model.Employee;
import com.example.erp01.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeMapper employeeMapper;

    @Override
    public int addEmployee(Employee employee) {
        try{
            employeeMapper.addEmployee(employee);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Employee employeeLogin(String employeeNumber, String passWord) {
        Employee employee;
        try{
            employee = employeeMapper.employeeLogin(employeeNumber, passWord);
            return employee;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public int deleteEmployee(Integer employeeID) {
        try{
            employeeMapper.deleteEmployee(employeeID);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Employee getEmpByID(Integer employeeID) {
        Employee employee = null;
        try{
            employee = employeeMapper.getEmpByID(employeeID);
            return employee;
        }catch (Exception e){
            e.printStackTrace();
        }
        return employee;
    }

    @Override
    public int updateEmployee(Employee employee) {
        try{
            employeeMapper.updateEmployee(employee);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<String> getEmpHint(String subEmpNumber) {
        List<String> employeeList = new ArrayList<>();
        try{
            employeeList = employeeMapper.getEmpHint(subEmpNumber);
            return employeeList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Employee> getAllEmployee() {
        List<Employee> employeeList = new ArrayList<>();
        try{
            employeeList = employeeMapper.getAllEmployee();
            return employeeList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return employeeList;
    }

    @Override
    public String getEmpNameByEmpNum(String employeeNumber) {
        try{
            String empName = employeeMapper.getEmpNameByEmpNum(employeeNumber);
            return empName;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String getGroupNameByEmpNum(String employeeNumber) {
        try{
            String groupName = employeeMapper.getGroupNameByEmpNum(employeeNumber);
            return groupName;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Employee> getEmployeeByGroup(String groupName) {
        List<Employee> employeeList = new ArrayList<>();
        try{
            employeeList = employeeMapper.getEmployeeByGroup(groupName);
            return employeeList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return employeeList;
    }

    @Override
    public int updatePassWord(String employeeNumber, String passWord) {
        try{
            employeeMapper.updatePassWord(employeeNumber, passWord);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<Employee> getEmpByEmpNumber(String employeeNumber) {
        List<Employee> employeeList = new ArrayList<>();
        try{
            employeeList = employeeMapper.getEmpByEmpNumber(employeeNumber);
            return employeeList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return employeeList;
    }

    @Override
    public List<String> getEmpNameHint(String subEmpName) {
        List<String> empNameList = new ArrayList<>();
        try{
            empNameList = employeeMapper.getEmpNameHint(subEmpName);
            return empNameList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return empNameList;
    }

    @Override
    public List<String> getEmpNumbersByName(String employeeName) {
        List<String> employeeNumberList = new ArrayList<>();
        try{
            employeeNumberList = employeeMapper.getEmpNumbersByName(employeeName);
            return employeeNumberList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return employeeNumberList;
    }

    @Override
    public List<Employee> getEmpByIdentifyCard(String identifyCard) {
        List<Employee> empList = new ArrayList<>();
        try{
            empList = employeeMapper.getEmpByIdentifyCard(identifyCard);
            return empList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return empList;
    }

    @Override
    public String getPositionByEmployeeNumber(String employeeNumber) {
        String position = "";
        try{
            position = employeeMapper.getPositionByEmployeeNumber(employeeNumber);
            return position;
        }catch (Exception e){
            e.printStackTrace();
        }
        return position;
    }

    @Override
    public List<Employee> getEmployeeByGroups(List<String> groupList) {
        List<Employee> employeeList = new ArrayList<>();
        try{
            employeeList = employeeMapper.getEmployeeByGroups(groupList);
            return employeeList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return employeeList;
    }

    @Override
    public List<Employee> getAllWorkEmployee() {
        List<Employee> employeeList = new ArrayList<>();
        try{
            employeeList = employeeMapper.getAllWorkEmployee();
            return employeeList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return employeeList;
    }

    @Override
    public List<Employee> getEmployeeByPositions(List<String> positionList) {
        List<Employee> employeeList = new ArrayList<>();
        try{
            employeeList = employeeMapper.getEmployeeByPositions(positionList);
            return employeeList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return employeeList;
    }

    @Override
    public Integer getEmployeeCountByGroup(String groupName) {
        Integer employeeCount = 0;
        try{
            if (employeeMapper.getEmployeeCountByGroup(groupName) != null){
                employeeCount = employeeMapper.getEmployeeCountByGroup(groupName);
            }
            return employeeCount;
        } catch (Exception e){
            e.printStackTrace();
        }
        return employeeCount;
    }

    @Override
    public List<String> getAllGroupName() {
        List<String> groupList = new ArrayList<>();
        try{
            groupList = employeeMapper.getAllGroupName();
            return groupList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return groupList;
    }

    @Override
    public List<Employee> getAllInStateEmployee() {
        List<Employee> employeeList = new ArrayList<>();
        try{
            employeeList = employeeMapper.getAllInStateEmployee();
            return employeeList;
        } catch (Exception e){
            e.printStackTrace();
        }
        return employeeList;
    }

    @Override
    public List<Employee> getAllOffStateEmployee() {
        List<Employee> employeeList = new ArrayList<>();
        try{
            employeeList = employeeMapper.getAllOffStateEmployee();
            return employeeList;
        } catch (Exception e){
            e.printStackTrace();
        }
        return employeeList;
    }

    @Override
    public List<Employee> getCheckEmployee() {
        List<Employee> employeeList = new ArrayList<>();
        try{
            employeeList = employeeMapper.getCheckEmployee();
            return employeeList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return employeeList;
    }

    @Override
    public List<Employee> getEmployeeInfoHit(String keyWord, Integer page, Integer limit) {
        List<Employee> employeeList = new ArrayList<>();
        try{
            employeeList = employeeMapper.getEmployeeInfoHit(keyWord, page, limit);
            return employeeList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return employeeList;
    }

    @Override
    public Integer getEmployeeCountOfHint(String keyWord) {
        int count = 0;
        try{
            if (employeeMapper.getEmployeeCountOfHint(keyWord) != null){
                count = employeeMapper.getEmployeeCountOfHint(keyWord);
            }
            return count;
        }catch (Exception e){
            e.printStackTrace();
        }
        return count;
    }

    @Override
    public List<Employee> getProduceEmployeeByGroup(String groupName) {
        return employeeMapper.getProduceEmployeeByGroup(groupName);
    }

    @Override
    public List<Employee> getEmpByEmpNumberList(List<String> empList) {
        return employeeMapper.getEmpByEmpNumberList(empList);
    }

}
