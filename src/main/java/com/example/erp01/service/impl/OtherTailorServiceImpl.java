package com.example.erp01.service.impl;

import com.example.erp01.mapper.OtherTailorMapper;
import com.example.erp01.model.*;
import com.example.erp01.service.OtherTailorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@com.alibaba.dubbo.config.annotation.Service
@Service
public class OtherTailorServiceImpl implements OtherTailorService {
    @Autowired
    private OtherTailorMapper otherTailorMapper;

    @Override
    public List<OtherTailor> generateOtherTailorData(String jsonStr) {
        List<OtherTailor> otherTailorList = null;
        try{
            otherTailorList = otherTailorMapper.generateOtherTailorData(jsonStr);
            return otherTailorList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return otherTailorList;
    }

    @Override
    public int saveOtherTailorData(List<OtherTailor> otherTailorList) {
        try{
            otherTailorMapper.saveOtherTailorData(otherTailorList);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<OtherTailor> getAllOtherTailorData() {
        List<OtherTailor> otherTailorList = null;
        try{
            otherTailorList = otherTailorMapper.getAllOtherTailorData();
            return otherTailorList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return otherTailorList;
    }

    @Override
    public List<OtherTailor> getAllOtherTailorDataByOrder(String orderName) {
        List<OtherTailor> otherTailorList = null;
        try{
            otherTailorList = otherTailorMapper.getAllOtherTailorDataByOrder(orderName);
            return otherTailorList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return otherTailorList;
    }

    @Override
    public Integer getMaxBedNumberByOrder(String orderName) {
        try{
            Integer bedNumber = otherTailorMapper.getMaxBedNumberByOrder(orderName);
            return bedNumber;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }


    @Override
    public List<OtherTailor> getOtherTailorByOrderNameBedNum(String orderName, int bedNumber) {
        List<OtherTailor> otherTailorList = new ArrayList<>();
        try{
            otherTailorList = otherTailorMapper.getOtherTailorByOrderNameBedNum(orderName,bedNumber);
            return otherTailorList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return otherTailorList;
    }

    @Override
    public int getOtherTailorCountByOrderNameBedNum(String orderName, int bedNumber) {
        try {
            int otherTailorCount =  otherTailorMapper.getOtherTailorCountByOrderNameBedNum(orderName, bedNumber);
            return otherTailorCount;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public List<Integer> getOtherBedNumbersByOrderName(String orderName) {
        List<Integer> bedNumList = new ArrayList<>();
        try{
            bedNumList = otherTailorMapper.getOtherBedNumbersByOrderName(orderName);
            return bedNumList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return bedNumList;
    }

    @Override
    public List<String> getOtherSizeNamesByOrderName(String orderName) {
        List<String> sizeNameList = new ArrayList<>();

        try{
            sizeNameList = otherTailorMapper.getOtherSizeNamesByOrderName(orderName);
            return sizeNameList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return sizeNameList;
    }

    @Override
    public List<String> getOtherPartNamesByOrderName(String orderName) {
        List<String> partNameList = new ArrayList<>();

        try{
            partNameList = otherTailorMapper.getOtherPartNamesByOrderName(orderName);
            return partNameList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return partNameList;
    }

    @Override
    public Integer getOtherTailorCountByOrderNameBedNumPart(String orderName, int bedNumber, String partName) {
        try{
            int tmpCount = otherTailorMapper.getOtherTailorCountByOrderNameBedNumPart(orderName, bedNumber, partName);
            return tmpCount;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public List<OtherTailor> getOtherTailorByOrderNameBedNumPart(String orderName, int bedNumber, String partName) {
        List<OtherTailor> otherTailorList = new ArrayList<>();

        try{
            otherTailorList = otherTailorMapper.getOtherTailorByOrderNameBedNumPart(orderName, bedNumber, partName);
            return otherTailorList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return otherTailorList;
    }

    @Override
    public List<TailorReport> otherTailorReport(String orderName, String partName, Integer bedNumber) {
        List<TailorReport> reportList = new ArrayList<>();
        try{
            reportList = otherTailorMapper.otherTailorReport(orderName, partName, bedNumber);
            return reportList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return reportList;
    }

    @Override
    public Integer getOtherMaxPackageNumberByOrder(String orderName) {
        Integer maxPackageNumber = null;
        try{
            maxPackageNumber = otherTailorMapper.getOtherMaxPackageNumberByOrder(orderName);
            return maxPackageNumber;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<OtherTailor> getDetailOtherTailorByOrderNameBedNumber(String orderName, int bedNumber) {
        List<OtherTailor> otherTailorList = new ArrayList<>();
        try{
            otherTailorList = otherTailorMapper.getDetailOtherTailorByOrderNameBedNumber(orderName, bedNumber);
            return otherTailorList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public int deleteOtherTailor(Integer otherTailorID) {
        try{
            otherTailorMapper.deleteOtherTailor(otherTailorID);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public int deleteOtherTailorByOrderBed(String orderName, Integer bedNumber) {
        try{
            otherTailorMapper.deleteOtherTailorByOrderBed(orderName, bedNumber);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public int deleteOtherTailorByOrderBedPack(String orderName, Integer bedNumber, List<Integer> packageNumberList) {
        try{
            Map<String, Object> map = new HashMap<>();
            map.put("orderName",orderName);
            map.put("bedNumber",bedNumber);
            map.put("list",packageNumberList);
            otherTailorMapper.deleteOtherTailorByOrderBedPack(map);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<OtherTailor> getAllOtherTailorByOrderBed(String orderName, Integer bedNumber, Integer packageNumber) {
        List<OtherTailor> tailorList = new ArrayList<>();
        try{
            tailorList = otherTailorMapper.getAllOtherTailorByOrderBed(orderName, bedNumber,packageNumber);
            return tailorList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return tailorList;
    }

    @Override
    public List<String> getOtherTailorQcodeByTailorQcodeID(List<Integer> tailorQcodeIDList) {
        List<String> tailorQcodeList = new ArrayList<>();
        try{
            tailorQcodeList = otherTailorMapper.getOtherTailorQcodeByTailorQcodeID(tailorQcodeIDList);
            return tailorQcodeList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String getOneOtherTailorQcodeByTailorQcodeID(Integer tailorQcodeID) {
        try{
            String tailorQcode = otherTailorMapper.getOneOtherTailorQcodeByTailorQcodeID(tailorQcodeID);
            return tailorQcode;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<CutQueryLeak> getOtherTailorInfoByNamePart(String orderName, String partName) {
        List<CutQueryLeak> otherTailorInfoList = new ArrayList<>();
        try{
            otherTailorInfoList = otherTailorMapper.getOtherTailorInfoByNamePart(orderName, partName);
            return otherTailorInfoList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return otherTailorInfoList;
    }

    @Override
    public List<Integer> getOtherBedNumbersByOrderPart(String orderName, String partName) {
        List<Integer> bedNumberList = new ArrayList<>();
        try{
            bedNumberList = otherTailorMapper.getOtherBedNumbersByOrderPart(orderName, partName);
            return bedNumberList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return bedNumberList;
    }

    @Override
    public Integer deleteByOrderPartBed(String orderName, String partName, Integer bedNumber) {
        try{
            otherTailorMapper.deleteByOrderPartBed(orderName, partName, bedNumber);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer getOtherMaxPackageNumberByOrderpart(String orderName, String partName) {
        Integer maxPackageNumber = null;
        try{
            maxPackageNumber = otherTailorMapper.getOtherMaxPackageNumberByOrderpart(orderName, partName);
            return maxPackageNumber;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<OtherTailor> getDetailOtherTailorByOrderPartBed(String orderName, String partName, Integer bedNumber) {
        List<OtherTailor> otherTailorList = new ArrayList<>();
        try{
            otherTailorList = otherTailorMapper.getDetailOtherTailorByOrderPartBed(orderName, partName, bedNumber);
            return otherTailorList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return otherTailorList;
    }

    @Override
    public List<Integer> getOtherPackageNumbersByOrderBedColor(String orderName, Integer bedNumber, String colorName) {
        List<Integer> packageNumberList = new ArrayList<>();
        try{
            packageNumberList = otherTailorMapper.getOtherPackageNumbersByOrderBedColor(orderName, bedNumber, colorName);
            return packageNumberList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return packageNumberList;
    }

    @Override
    public Integer getOtherLayerCountByOrderBedPackage(String orderName, Integer bedNumber, Integer packageNumber) {
        Integer layerCount = 0;
        try{
            layerCount = otherTailorMapper.getOtherLayerCountByOrderBedPackage(orderName, bedNumber, packageNumber);
            return layerCount;
        }catch (Exception e){
            e.printStackTrace();
        }
        return layerCount;
    }

    @Override
    public List<OtherTailorMonthReport> getOtherTailorMonthReport(Date from, Date to, String orderName, String groupName) {
        List<OtherTailorMonthReport> otherTailorMonthReportList = new ArrayList<>();
        try{
            otherTailorMonthReportList = otherTailorMapper.getOtherTailorMonthReport(from, to, orderName, groupName);
            return otherTailorMonthReportList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return otherTailorMonthReportList;
    }

    @Override
    public Integer getCutCountByOrderPart(String orderName, String partName) {
        Integer cutCount = null;
        try{
            cutCount = otherTailorMapper.getCutCountByOrderPart(orderName, partName);
            return cutCount;
        }catch (Exception e){
            e.printStackTrace();
        }
        return cutCount;
    }

    @Override
    public Integer updateOtherTailorData(List<OtherTailor> otherTailorList) {
        try{
            otherTailorMapper.updateOtherTailorData(otherTailorList);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<OtherTailor> getOtherTailorDataByOrderBed(String orderName, Integer bedNumber) {
        List<OtherTailor> otherTailorList = new ArrayList<>();
        try{
            otherTailorList = otherTailorMapper.getOtherTailorDataByOrderBed(orderName, bedNumber);
            return otherTailorList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return otherTailorList;
    }

    @Override
    public OtherTailor getOtherTailorByTailorQcodeID(Integer tailorQcodeID) {
        OtherTailor otherTailor = null;
        try{
            otherTailor = otherTailorMapper.getOtherTailorByTailorQcodeID(tailorQcodeID);
            return otherTailor;
        }catch (Exception e){
            e.printStackTrace();
        }
        return otherTailor;
    }

    @Override
    public List<OtherTailor> getOtherTailorByOrderPartColorSize(String orderName, String partName, String colorName, String sizeName) {
        List<OtherTailor> otherTailorList = new ArrayList<>();
        try{
            otherTailorList = otherTailorMapper.getOtherTailorByOrderPartColorSize(orderName, partName, colorName, sizeName);
            return otherTailorList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return otherTailorList;
    }

    @Override
    public OtherTailor getOneOtherTailorByTailorQcodeID(Integer tailorQcodeID) {
        OtherTailor otherTailor = null;
        try{
            otherTailor = otherTailorMapper.getOneOtherTailorByTailorQcodeID(tailorQcodeID);
            return otherTailor;
        }catch (Exception e){
            e.printStackTrace();
        }
        return otherTailor;
    }

}
