package com.example.erp01.service.impl;

import com.example.erp01.mapper.EmbOutStoreMapper;
import com.example.erp01.model.*;
import com.example.erp01.service.EmbOutStoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class EmbOutStoreServiceImpl implements EmbOutStoreService {

    @Autowired
    private EmbOutStoreMapper embOutStoreMapper;


    @Override
    public int addEmbOutStore(List<EmbOutStore> embOutStoreList) {
        try{
            if(embOutStoreList!=null && !embOutStoreList.isEmpty()){
                embOutStoreMapper.addEmbOutStore(embOutStoreList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public int addEmbOutStoreOne(EmbOutStore embOutStore) {
        try{
            embOutStoreMapper.addEmbOutStoreOne(embOutStore);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public int deleteEmbOutStore(Integer embOutStoreID) {
        try{
            embOutStoreMapper.deleteEmbOutStore(embOutStoreID);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer getEmbOutStoreCount(String groupName, String orderName, String colorName, String sizeName, Date embPlanDate) {
        Integer count = 0;
        try{
            count = embOutStoreMapper.getEmbOutStoreCount(groupName, orderName, colorName, sizeName, embPlanDate);
            if (count == null){
                return 0;
            }else {
                return count;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return count;
    }

    @Override
    public List<EmbOutDetail> getEmbOutDetail(Date from, Date to) {
        List<EmbOutDetail> embOutDetailList = new ArrayList<>();
        try{
            embOutDetailList = embOutStoreMapper.getEmbOutDetail(from, to);
            return embOutDetailList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return embOutDetailList;
    }

    @Override
    public List<TailorReport> getEmbOutPackageDetail(String orderName, String from, String to, String groupName) {
        List<TailorReport> tailorReportList = new ArrayList<>();
        try{
            tailorReportList = embOutStoreMapper.getEmbOutPackageDetail(orderName, from, to, groupName);
            return tailorReportList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return tailorReportList;
    }

    @Override
    public List<CutQueryLeak> getEmbOutInfoByOrder(String orderName) {
        List<CutQueryLeak> embOutInfoList = new ArrayList<>();
        try{
            embOutInfoList = embOutStoreMapper.getEmbOutInfoByOrder(orderName);
            return embOutInfoList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return embOutInfoList;
    }

    @Override
    public List<CutLocation> getEmbOutInfo(String orderName, Integer bedNumber,String colorName, String sizeName, Integer packageNumber) {
        List<CutLocation> cutLocationList = new ArrayList<>();
        try{
            cutLocationList = embOutStoreMapper.getEmbOutInfo(orderName, bedNumber, colorName, sizeName, packageNumber);
            return cutLocationList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return cutLocationList;
    }

    @Override
    public List<EmbOutDaily> getEmbOutDaily(String orderName) {
        List<EmbOutDaily> embOutDailyList = new ArrayList<>();
        try{
            embOutDailyList = embOutStoreMapper.getEmbOutDaily(orderName);
            return embOutDailyList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return embOutDailyList;
    }

    @Override
    public List<WeekAmount> getEmbOutWeekAmount() {
        List<WeekAmount> weekAmountList = new ArrayList<>();
        try{
            weekAmountList = embOutStoreMapper.getEmbOutWeekAmount();
            return weekAmountList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return weekAmountList;
    }

    @Override
    public List<CompletionCount> getEmbOutCompletion() {
        List<CompletionCount> completionCountList = new ArrayList<>();
        try{
            completionCountList = embOutStoreMapper.getEmbOutCompletion();
            return completionCountList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return completionCountList;
    }

    @Override
    public Integer getEmbOutSumCount(String orderName) {
        Integer embOutCount = 0;
        try{
            if (embOutStoreMapper.getEmbOutSumCount(orderName) != null){
                embOutCount = embOutStoreMapper.getEmbOutSumCount(orderName);
            }
            return embOutCount;
        }catch (Exception e){
            e.printStackTrace();
        }
        return embOutCount;
    }

    @Override
    public List<EmbStorage> getEmbOutStoreGroupByColor(String orderName) {
        List<EmbStorage> embStorageList = new ArrayList<>();
        try{
            embStorageList = embOutStoreMapper.getEmbOutStoreGroupByColor(orderName);
            return embStorageList;
        } catch (Exception e){
            e.printStackTrace();
        }
        return embStorageList;
    }

    @Override
    public EmbOutStore getEmbOutStoreByInfo(String orderName, Integer bedNumber, Integer packageNumber) {
        EmbOutStore embOutStore = null;
        try{
            embOutStore = embOutStoreMapper.getEmbOutStoreByInfo(orderName, bedNumber, packageNumber);
            return embOutStore;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Integer deleteEmbOutStoreByInfo(String orderName, Integer bedNumber, Integer packageNumber) {
        try{
            embOutStoreMapper.deleteEmbOutStoreByInfo(orderName, bedNumber, packageNumber);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<CutQueryLeak> getEmbOutStoreGroupByColorSize(String orderName, String colorName, String sizeName) {
        List<CutQueryLeak> cutQueryLeakList = new ArrayList<>();
        try{
            cutQueryLeakList = embOutStoreMapper.getEmbOutStoreGroupByColorSize(orderName, colorName, sizeName);
            return cutQueryLeakList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return cutQueryLeakList;
    }

    @Override
    public List<EmbOutDetail> getAmongEmbOut(Date fromDate, Date toDate, String orderName) {
        List<EmbOutDetail> embOutDetailList = new ArrayList<>();
        try{
            embOutDetailList = embOutStoreMapper.getAmongEmbOut(fromDate, toDate, orderName);
            return embOutDetailList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return embOutDetailList;
    }

    @Override
    public List<TailorReport> getEmbOutPackageCountByInfo(String orderName, String from, String to, String groupName) {
        return embOutStoreMapper.getEmbOutPackageCountByInfo(orderName, from, to, groupName);
    }
}
