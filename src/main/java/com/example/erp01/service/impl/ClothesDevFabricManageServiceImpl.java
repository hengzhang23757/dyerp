package com.example.erp01.service.impl;

import com.example.erp01.mapper.ClothesDevFabricManageMapper;
import com.example.erp01.model.ClothesDevFabricManage;
import com.example.erp01.service.ClothesDevFabricManageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class ClothesDevFabricManageServiceImpl implements ClothesDevFabricManageService {

    @Autowired
    private ClothesDevFabricManageMapper clothesDevFabricManageMapper;

    @Override
    public Integer addClothesDevFabricManage(ClothesDevFabricManage clothesDevFabricManage) {
        try{
            clothesDevFabricManageMapper.addClothesDevFabricManage(clothesDevFabricManage);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<ClothesDevFabricManage> getClothesDevFabricManageByMap(Map<String, Object> map) {
        return clothesDevFabricManageMapper.getClothesDevFabricManageByMap(map);
    }

    @Override
    public Integer updateClothesDevFabricManage(ClothesDevFabricManage clothesDevFabricManage) {
        try{
            clothesDevFabricManageMapper.updateClothesDevFabricManage(clothesDevFabricManage);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer deleteClothesDevFabricManage(Integer id) {
        try{
            clothesDevFabricManageMapper.deleteClothesDevFabricManage(id);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<String> getClothsDevFabricNameHint(String keyWord) {
        return clothesDevFabricManageMapper.getClothsDevFabricNameHint(keyWord);
    }

    @Override
    public List<String> getClothsDevFabricNumberHint(String keyWord) {
        return clothesDevFabricManageMapper.getClothsDevFabricNumberHint(keyWord);
    }

    @Override
    public List<ClothesDevFabricManage> getClothesDevFabricManageHint(String subFabricName, Integer page, Integer limit) {
        return clothesDevFabricManageMapper.getClothesDevFabricManageHint(subFabricName, page, limit);
    }

    @Override
    public Integer deleteClothesDevFabricRecordByFabricId(Integer fabricID) {
        try{
            clothesDevFabricManageMapper.deleteClothesDevFabricRecordByFabricId(fabricID);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public ClothesDevFabricManage getClothesDevFabricManageById(Integer id) {
        return clothesDevFabricManageMapper.getClothesDevFabricManageById(id);
    }

    @Override
    public Integer updateClothesDevFabricManageBatch(List<ClothesDevFabricManage> clothesDevFabricManageList) {
        try {
            if (clothesDevFabricManageList != null && !clothesDevFabricManageList.isEmpty()){
                clothesDevFabricManageMapper.updateClothesDevFabricManageBatch(clothesDevFabricManageList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer deleteClothesDevFabricManageBatch(List<Integer> idList) {
        try{
            if (idList != null && !idList.isEmpty()){
                clothesDevFabricManageMapper.deleteClothesDevFabricManageBatch(idList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public String getMaxClothesDevFabricOrderByPreFix(String preFix) {
        String maxOrder = "00000";
        try{
            if (clothesDevFabricManageMapper.getMaxClothesDevFabricOrderByPreFix(preFix) != null){
                maxOrder = clothesDevFabricManageMapper.getMaxClothesDevFabricOrderByPreFix(preFix);
            }
            return maxOrder;
        }catch (Exception e){
            e.printStackTrace();
        }
        return maxOrder;
    }

    @Override
    public List<ClothesDevFabricManage> getClothesDevFabricManageRecordByInfo(Map<String, Object> map) {
        return clothesDevFabricManageMapper.getClothesDevFabricManageRecordByInfo(map);
    }

    @Override
    public Integer quitClothesDevFabricManageCheck(List<Integer> idList) {
        try{
            if (idList != null && !idList.isEmpty()){
                clothesDevFabricManageMapper.quitClothesDevFabricManageCheck(idList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }
}
