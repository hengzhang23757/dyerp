package com.example.erp01.service.impl;

import com.example.erp01.mapper.ManufactureAccessoryMapper;
import com.example.erp01.model.AddFeeVo;
import com.example.erp01.model.FabricAccessoryReview;
import com.example.erp01.model.ManufactureAccessory;
import com.example.erp01.service.ManufactureAccessoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class ManufactureAccessoryServiceImpl implements ManufactureAccessoryService {

    @Autowired
    private ManufactureAccessoryMapper manufactureAccessoryMapper;

    @Override
    public Integer addManufactureAccessoryBatch(List<ManufactureAccessory> manufactureAccessoryList) {
        try{
            if (manufactureAccessoryList != null && manufactureAccessoryList.size() > 0){
                manufactureAccessoryMapper.addManufactureAccessoryBatch(manufactureAccessoryList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<ManufactureAccessory> getManufactureAccessoryByOrder(String orderName, Integer batchOrder) {
        List<ManufactureAccessory> manufactureAccessoryList = new ArrayList<>();
        try{
            manufactureAccessoryList = manufactureAccessoryMapper.getManufactureAccessoryByOrder(orderName, batchOrder);
            return manufactureAccessoryList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return manufactureAccessoryList;
    }

    @Override
    public Integer deleteManufactureAccessoryByName(String orderName) {
        try{
            manufactureAccessoryMapper.deleteManufactureAccessoryByName(orderName);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer deleteManufactureAccessoryByID(Integer accessoryID) {
        try{
            manufactureAccessoryMapper.deleteManufactureAccessoryByID(accessoryID);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer deleteManufactureAccessoryBatch(List<Integer> accessoryIDList) {
        try{
            manufactureAccessoryMapper.deleteManufactureAccessoryBatch(accessoryIDList);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer updateManufactureAccessory(ManufactureAccessory manufactureAccessory) {
        try{
            manufactureAccessoryMapper.updateManufactureAccessory(manufactureAccessory);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<ManufactureAccessory> getManufactureAccessoryHintByName(String subAccessoryName,Integer page,Integer limit) {
        List<ManufactureAccessory> manufactureAccessoryList = new ArrayList<>();
        try{
            manufactureAccessoryList = manufactureAccessoryMapper.getManufactureAccessoryHintByName(subAccessoryName, page, limit);
            return manufactureAccessoryList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return manufactureAccessoryList;
    }

    @Override
    public List<ManufactureAccessory> getManufactureAccessoryHintByNumber(String subAccessoryName, Integer page, Integer limit) {
        List<ManufactureAccessory> manufactureAccessoryList = new ArrayList<>();
        try{
            manufactureAccessoryList = manufactureAccessoryMapper.getManufactureAccessoryHintByNumber(subAccessoryName, page, limit);
            return manufactureAccessoryList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return manufactureAccessoryList;
    }

    @Override
    public Integer updateManufactureAccessoryOnPlaceOrder(List<ManufactureAccessory> manufactureAccessoryList) {
        try{
            if (manufactureAccessoryList != null && manufactureAccessoryList.size() > 0){
                manufactureAccessoryMapper.updateManufactureAccessoryOnPlaceOrder(manufactureAccessoryList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<ManufactureAccessory> getManufactureAccessoryOneYear() {
        List<ManufactureAccessory> manufactureAccessoryList = new ArrayList<>();
        try{
            manufactureAccessoryList = manufactureAccessoryMapper.getManufactureAccessoryOneYear();
            return manufactureAccessoryList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return manufactureAccessoryList;
    }

    @Override
    public List<String> getDistinctAccessorySupplier() {
        List<String> supplierList = new ArrayList<>();
        try{
            supplierList = manufactureAccessoryMapper.getDistinctAccessorySupplier();
            return supplierList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return supplierList;
    }

    @Override
    public ManufactureAccessory getManufactureAccessoryByID(Integer accessoryID) {
        ManufactureAccessory manufactureAccessory;
        try{
            manufactureAccessory = manufactureAccessoryMapper.getManufactureAccessoryByID(accessoryID);
            return manufactureAccessory;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<String> getOrderNamesByAccessoryInfo(String accessoryName, String specification, String accessoryColor) {
        List<String> orderNameList = new ArrayList<>();
        try{
            orderNameList = manufactureAccessoryMapper.getOrderNamesByAccessoryInfo(accessoryName, specification, accessoryColor);
            return orderNameList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return orderNameList;
    }

    @Override
    public ManufactureAccessory getManufactureAccessoryByAccessoryInfo(String orderName, String accessoryName, String specification, String accessoryColor) {
        ManufactureAccessory manufactureAccessory;
        try{
            manufactureAccessory = manufactureAccessoryMapper.getManufactureAccessoryByAccessoryInfo(orderName, accessoryName, specification, accessoryColor);
            return manufactureAccessory;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<ManufactureAccessory> getManufactureAccessoryInPlaceOrderByInfo(List<String> orderNameList, String supplier, String materialName, String checkState) {
        List<ManufactureAccessory> manufactureAccessoryList = new ArrayList<>();
        try{
            manufactureAccessoryList = manufactureAccessoryMapper.getManufactureAccessoryInPlaceOrderByInfo(orderNameList, supplier, materialName, checkState);
            return manufactureAccessoryList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return manufactureAccessoryList;
    }

    @Override
    public List<ManufactureAccessory> getManufactureAccessoryInPlaceOrder(String orderName, String supplier, String checkState) {
        List<ManufactureAccessory> manufactureAccessoryList = new ArrayList<>();
        try{
            manufactureAccessoryList = manufactureAccessoryMapper.getManufactureAccessoryInPlaceOrder(orderName, supplier, checkState);
            return manufactureAccessoryList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return manufactureAccessoryList;
    }

    @Override
    public Integer changeAccessoryOrderStateInPlaceOrder(List<Integer> accessoryIDList) {
        try{
            if (accessoryIDList != null && accessoryIDList.size() > 0){
                manufactureAccessoryMapper.changeAccessoryOrderStateInPlaceOrder(accessoryIDList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer updateManufactureAccessoryBatch(List<ManufactureAccessory> manufactureAccessoryList) {
        try{
            if (manufactureAccessoryList != null && manufactureAccessoryList.size() > 0){
                manufactureAccessoryMapper.updateManufactureAccessoryBatch(manufactureAccessoryList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer changeAccessoryCheckStateInPlaceOrder(List<Integer> accessoryIDList, String preCheck, String checkState, String preCheckEmp, String checkEmp) {
        try{
            if (accessoryIDList != null && accessoryIDList.size() > 0){
                manufactureAccessoryMapper.changeAccessoryCheckStateInPlaceOrder(accessoryIDList, preCheck, checkState, preCheckEmp, checkEmp);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<ManufactureAccessory> getManufactureAccessoryByOrderCheckState(String orderName, String checkState) {
        List<ManufactureAccessory> manufactureAccessoryList = new ArrayList<>();
        try{
            manufactureAccessoryList = manufactureAccessoryMapper.getManufactureAccessoryByOrderCheckState(orderName, checkState);
            return manufactureAccessoryList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return manufactureAccessoryList;
    }

    @Override
    public List<ManufactureAccessory> getManufactureAccessoryByOrderCheckStateOrderList(List<String> orderNameList, String checkState, String preCheck) {
        List<ManufactureAccessory> manufactureAccessoryList = new ArrayList<>();
        try{
            manufactureAccessoryList = manufactureAccessoryMapper.getManufactureAccessoryByOrderCheckStateOrderList(orderNameList, checkState, preCheck);
            return manufactureAccessoryList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return manufactureAccessoryList;
    }

    @Override
    public List<ManufactureAccessory> getBjByOrderCheckStateOrderList(List<String> orderNameList, String checkState) {
        List<ManufactureAccessory> manufactureAccessoryList = new ArrayList<>();
        try{
            manufactureAccessoryList = manufactureAccessoryMapper.getBjByOrderCheckStateOrderList(orderNameList, checkState);
            return manufactureAccessoryList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return manufactureAccessoryList;
    }

    @Override
    public List<String> getManufactureAccessoryNameHint(String subName) {
        List<String> accessoryNameList = new ArrayList<>();
        try{
            accessoryNameList = manufactureAccessoryMapper.getManufactureAccessoryNameHint(subName);
            return accessoryNameList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return accessoryNameList;
    }

    @Override
    public List<ManufactureAccessory> getAccessoryCheckStateRecentYearByOrder(List<String> orderNameList) {
        List<ManufactureAccessory> manufactureAccessoryList = new ArrayList<>();
        try{
            manufactureAccessoryList = manufactureAccessoryMapper.getAccessoryCheckStateRecentYearByOrder(orderNameList);
            return manufactureAccessoryList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return manufactureAccessoryList;
    }

    @Override
    public List<FabricAccessoryReview> getManufactureAccessoryReviewState(String checkState) {
        List<FabricAccessoryReview> fabricAccessoryReviewList = new ArrayList<>();
        try{
            fabricAccessoryReviewList = manufactureAccessoryMapper.getManufactureAccessoryReviewState(checkState);
            return fabricAccessoryReviewList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return fabricAccessoryReviewList;
    }

    @Override
    public List<ManufactureAccessory> getManufactureAccessoryByCheckNumberState(String checkNumber, String checkState) {
        List<ManufactureAccessory> manufactureAccessoryList = new ArrayList<>();
        try{
            manufactureAccessoryList = manufactureAccessoryMapper.getManufactureAccessoryByCheckNumberState(checkNumber, checkState);
            return manufactureAccessoryList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return manufactureAccessoryList;
    }

    @Override
    public Integer destroyAccessoryCheck(String checkNumber) {
        try{
            manufactureAccessoryMapper.destroyAccessoryCheck(checkNumber);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer getAccessoryCheckNumberCountByState(String checkState) {
        Integer checkNumberCount = 0;
        try{
            if (manufactureAccessoryMapper.getAccessoryCheckNumberCountByState(checkState) != null){
                checkNumberCount = manufactureAccessoryMapper.getAccessoryCheckNumberCountByState(checkState);
            }
            return checkNumberCount;
        } catch (Exception e){
            e.printStackTrace();
        }
        return checkNumberCount;
    }

    @Override
    public Integer updateAccessoryAfterCommit(List<ManufactureAccessory> manufactureAccessoryList) {
        try{
            if (manufactureAccessoryList != null && !manufactureAccessoryList.isEmpty()){
                manufactureAccessoryMapper.updateAccessoryAfterCommit(manufactureAccessoryList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<ManufactureAccessory> getManufactureAccessoryByColorSizeList(String orderName, List<String> colorList, List<String> sizeList) {
        List<ManufactureAccessory> manufactureAccessoryList = new ArrayList<>();
        try{
            manufactureAccessoryList = manufactureAccessoryMapper.getManufactureAccessoryByColorSizeList(orderName, colorList, sizeList);
            return manufactureAccessoryList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return manufactureAccessoryList;
    }

    @Override
    public Integer updateAccessoryOrderStateByCheckNumber(String checkNumber) {
        try{
            manufactureAccessoryMapper.updateAccessoryOrderStateByCheckNumber(checkNumber);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<String> getDistinctAccessoryNameByOrder(String orderName) {
        List<String> accessoryNameList = new ArrayList<>();
        try{
            accessoryNameList = manufactureAccessoryMapper.getDistinctAccessoryNameByOrder(orderName);
            return accessoryNameList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return accessoryNameList;
    }

    @Override
    public List<ManufactureAccessory> getManufactureAccessoryByIdList(List<Integer> accessoryIDList) {
        List<ManufactureAccessory> manufactureAccessoryList = new ArrayList<>();
        try{
            if (accessoryIDList != null && !accessoryIDList.isEmpty()){
                manufactureAccessoryList = manufactureAccessoryMapper.getManufactureAccessoryByIdList(accessoryIDList);
            }
            return manufactureAccessoryList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return manufactureAccessoryList;
    }

    @Override
    public List<String> getAccessoryCheckNumberHint(String keyWord) {
        return manufactureAccessoryMapper.getAccessoryCheckNumberHint(keyWord);
    }

    @Override
    public List<ManufactureAccessory> getAccessoryNameByOrderName(String orderName, Integer batchOrder, String accessoryType) {
        return manufactureAccessoryMapper.getAccessoryNameByOrderName(orderName, batchOrder, accessoryType);
    }

    @Override
    public List<ManufactureAccessory> getAccessoryNameChildByOrderName(String orderName, Integer batchOrder, String accessoryType) {
        return manufactureAccessoryMapper.getAccessoryNameChildByOrderName(orderName, batchOrder, accessoryType);
    }

    @Override
    public Integer getMaxAccessoryKey(String orderName, Integer batchOrder) {
        int accessoryKey = 0;
        try{
            if (manufactureAccessoryMapper.getMaxAccessoryKey(orderName, batchOrder) != null){
                accessoryKey = manufactureAccessoryMapper.getMaxAccessoryKey(orderName, batchOrder);
            }
            return (accessoryKey + 1);
        } catch (Exception e){
            e.printStackTrace();
        }
        return (accessoryKey + 1);
    }

    @Override
    public List<ManufactureAccessory> getAccessoryKeyByOrder(String orderName, Integer batchOrder, String accessoryType) {
        return manufactureAccessoryMapper.getAccessoryKeyByOrder(orderName, batchOrder, accessoryType);
    }

    @Override
    public void updateAccessoryCustomerUsage(List<ManufactureAccessory> manufactureAccessoryList) {
        if (manufactureAccessoryList != null && !manufactureAccessoryList.isEmpty()){
            manufactureAccessoryMapper.updateAccessoryCustomerUsage(manufactureAccessoryList);
        }
    }

    @Override
    public List<FabricAccessoryReview> getManufactureAccessoryReviewStateCombine(Map<String, Object> map) {
        return manufactureAccessoryMapper.getManufactureAccessoryReviewStateCombine(map);
    }

    @Override
    public List<FabricAccessoryReview> getBjReviewStateCombine(Map<String, Object> map) {
        return manufactureAccessoryMapper.getBjReviewStateCombine(map);
    }

    @Override
    public void deleteManufactureAccessoryByAccessoryType(String orderName, String accessoryType, Integer batchOrder, String checkState) {
        manufactureAccessoryMapper.deleteManufactureAccessoryByAccessoryType(orderName, accessoryType, batchOrder, checkState);
    }

    @Override
    public List<ManufactureAccessory> getBjByOrder(String orderName, String accessoryType, Integer batchOrder) {
        return manufactureAccessoryMapper.getBjByOrder(orderName, accessoryType, batchOrder);
    }

    @Override
    public void changeFeeStateBatch(List<Integer> accessoryIDList, Integer feeState) {
        if (accessoryIDList != null && !accessoryIDList.isEmpty()){
            manufactureAccessoryMapper.changeFeeStateBatch(accessoryIDList, feeState);
        }
    }

    @Override
    public List<AddFeeVo> getFeeVoByInfo(Map<String, Object> map) {
        return manufactureAccessoryMapper.getFeeVoByInfo(map);
    }

    @Override
    public void changeOrderName(String orderName, String toOrderName) {
        manufactureAccessoryMapper.changeOrderName(orderName, toOrderName);
    }
}
