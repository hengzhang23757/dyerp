package com.example.erp01.service.impl;

import com.example.erp01.mapper.DailyFabricMapper;
import com.example.erp01.model.DailyFabric;
import com.example.erp01.service.DailyFabricService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DailyFabricServiceImpl implements DailyFabricService {

    @Autowired
    private DailyFabricMapper dailyFabricMapper;

    @Override
    public Integer addDailyFabricBatch(List<DailyFabric> dailyFabricList) {
        try{
            if (dailyFabricList.size() != 0 && dailyFabricList != null){
                dailyFabricMapper.addDailyFabricBatch(dailyFabricList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<DailyFabric> getDailyFabricOneMonth() {
        List<DailyFabric> dailyFabricList = new ArrayList<>();
        try{
            dailyFabricList = dailyFabricMapper.getDailyFabricOneMonth();
            return dailyFabricList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return dailyFabricList;
    }
}
