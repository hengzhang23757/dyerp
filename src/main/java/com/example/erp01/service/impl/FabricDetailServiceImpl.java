package com.example.erp01.service.impl;

import com.example.erp01.mapper.FabricDetailMapper;
import com.example.erp01.model.FabricDetail;
import com.example.erp01.model.LooseFabric;
import com.example.erp01.service.FabricDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class FabricDetailServiceImpl implements FabricDetailService {

    @Autowired
    private FabricDetailMapper fabricDetailMapper;

    @Override
    public int addFabricDetailBatch(List<FabricDetail> fabricDetailList) {
        try{
            if (fabricDetailList != null && !fabricDetailList.isEmpty()){
                fabricDetailMapper.addFabricDetailBatch(fabricDetailList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;

    }

    @Override
    public int addFabricDetail(FabricDetail fabricDetail) {
        try{
            if (fabricDetail != null){
                fabricDetailMapper.addFabricDetail(fabricDetail);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public int deleteFabricDetailByID(Integer fabricDetailID) {
        try{
            fabricDetailMapper.deleteFabricDetailByID(fabricDetailID);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public int updateFabricDetail(FabricDetail fabricDetail) {
        try{
            fabricDetailMapper.updateFabricDetail(fabricDetail);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<String> getJarNameByOrderFabricColor(String orderName, String fabricName, String fabricColor) {
        List<String> jarNameList = new ArrayList<>();
        try{
            jarNameList = fabricDetailMapper.getJarNameByOrderFabricColor(orderName, fabricName, fabricColor);
            return jarNameList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return jarNameList;

    }

    @Override
    public Integer getFabricBatchNumberByOrderFabricJar(String orderName, String colorName, String fabricName, String fabricColor, String jarName) {
        int batchNumber = 0;
        try{
            if (fabricDetailMapper.getFabricBatchNumberByOrderFabricJar(orderName, colorName, fabricName, fabricColor, jarName) != null){
                batchNumber = fabricDetailMapper.getFabricBatchNumberByOrderFabricJar(orderName, colorName, fabricName, fabricColor, jarName);
            }
            return batchNumber;
        }catch (Exception e){
            e.printStackTrace();
        }
        return batchNumber;
    }

    @Override
    public List<FabricDetail> getTodayFabricDetail() {
        List<FabricDetail> fabricDetailList = new ArrayList<>();
        try{
            fabricDetailList = fabricDetailMapper.getTodayFabricDetail();
            return fabricDetailList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return fabricDetailList;
    }

    @Override
    public List<FabricDetail> getOneMonthFabricDetail() {
        List<FabricDetail> fabricDetailList = new ArrayList<>();
        try{
            fabricDetailList = fabricDetailMapper.getOneMonthFabricDetail();
            return fabricDetailList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return fabricDetailList;
    }

    @Override
    public List<FabricDetail> getOneYearFabricDetail() {
        List<FabricDetail> fabricDetailList = new ArrayList<>();
        try{
            fabricDetailList = fabricDetailMapper.getOneYearFabricDetail();
            return fabricDetailList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return fabricDetailList;
    }

    @Override
    public List<String> getFabricLocationHint(String orderName, String colorName, String fabricName, String fabricColor, String jarName) {
        List<String> fabricLocation = new ArrayList<>();
        try{
            fabricLocation = fabricDetailMapper.getFabricLocationHint(orderName, colorName, fabricName, fabricColor, jarName);
            return fabricLocation;
        }catch (Exception e){
            e.printStackTrace();
        }
        return fabricLocation;
    }

    @Override
    public List<String> getJarNameByOrder(String orderName) {
        List<String> jarNameList = new ArrayList<>();
        try{
            jarNameList = fabricDetailMapper.getJarNameByOrder(orderName);
            return jarNameList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return jarNameList;
    }

    @Override
    public List<LooseFabric> getFabricBatchInfoOfEachJar(String orderName, String fabricName, String fabricColor) {
        List<LooseFabric> looseFabricList = new ArrayList<>();
        try{
            looseFabricList = fabricDetailMapper.getFabricBatchInfoOfEachJar(orderName, fabricName, fabricColor);
            return looseFabricList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return looseFabricList;
    }

    @Override
    public List<FabricDetail> getFabricDetailByInfo(String orderName, String fabricName, String colorName, String fabricColor, Integer fabricID) {
        List<FabricDetail> fabricDetailList = new ArrayList<>();
        try{
            fabricDetailList = fabricDetailMapper.getFabricDetailByInfo(orderName, fabricName, colorName, fabricColor, fabricID);
            return fabricDetailList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return fabricDetailList;
    }

    @Override
    public List<String> getColorNamesByOrder(String orderName) {
        List<String> colorNameList = new ArrayList<>();
        try{
            colorNameList = fabricDetailMapper.getColorNamesByOrder(orderName);
            return colorNameList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return colorNameList;
    }

    @Override
    public List<String> getFabricNamesByOrderColor(String orderName, String colorName) {
        List<String> fabricNameList = new ArrayList<>();
        try{
            fabricNameList = fabricDetailMapper.getFabricNamesByOrderColor(orderName, colorName);
            return fabricNameList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return fabricNameList;
    }

    @Override
    public List<String> getFabricColorsByOrderColorFabric(String orderName, String colorName, String fabricName) {
        List<String> fabricColorList = new ArrayList<>();
        try{
            fabricColorList = fabricDetailMapper.getFabricColorsByOrderColorFabric(orderName, colorName, fabricName);
            return fabricColorList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return fabricColorList;
    }

    @Override
    public List<LooseFabric> getJarInfoByOrderColorFabricColor(String orderName, String colorName, String fabricName, String fabricColor) {
        List<LooseFabric> looseFabricList = new ArrayList<>();
        try{
            looseFabricList = fabricDetailMapper.getJarInfoByOrderColorFabricColor(orderName, colorName, fabricName, fabricColor);
            return looseFabricList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return looseFabricList;
    }

    @Override
    public String getFabricUnitByInfo(String orderName, String colorName, String fabricName, String fabricColor) {
        String unit = "无";
        try{
            if (fabricDetailMapper.getFabricUnitByInfo(orderName, colorName, fabricName, fabricColor) != null){
                unit = fabricDetailMapper.getFabricUnitByInfo(orderName, colorName, fabricName, fabricColor);
            }
            return unit;
        }catch (Exception e){
            e.printStackTrace();
        }
        return unit;
    }

    @Override
    public FabricDetail getTransFabricDetailByInfo(String orderName, String colorName, String fabricName, String fabricColor, String jarName, String location) {
        FabricDetail fabricDetail;
        try{
            fabricDetail = fabricDetailMapper.getTransFabricDetailByInfo(orderName, colorName, fabricName, fabricColor, jarName, location);
            return fabricDetail;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<FabricDetail> getFabricDetailByOrder(String orderName) {
        List<FabricDetail> fabricDetailList = new ArrayList<>();
        try{
            fabricDetailList = fabricDetailMapper.getFabricDetailByOrder(orderName);
            return fabricDetailList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return fabricDetailList;
    }

    @Override
    public FabricDetail getFabricDetailById(Integer fabricDetailID) {
        FabricDetail fabricDetail;
        try{
            fabricDetail = fabricDetailMapper.getFabricDetailById(fabricDetailID);
            return fabricDetail;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Integer getFabricBatchNumberByInfo(String orderName, List<String> colorNameList) {
        Integer fabricCount = 0;
        try{
            if (fabricDetailMapper.getFabricBatchNumberByInfo(orderName, colorNameList) != null){
                fabricCount = fabricDetailMapper.getFabricBatchNumberByInfo(orderName, colorNameList);
            }
            return fabricCount;
        }catch (Exception e){
            e.printStackTrace();
        }
        return fabricCount;
    }

    @Override
    public List<FabricDetail> getFabricDetailByTime(String from, String to) {
        List<FabricDetail> fabricDetailList = new ArrayList<>();
        try{
            fabricDetailList = fabricDetailMapper.getFabricDetailByTime(from, to);
            return fabricDetailList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return fabricDetailList;
    }

    @Override
    public List<FabricDetail> getFabricDetailByFabricIDList(List<Integer> fabricIDList) {
        List<FabricDetail> fabricDetailList = new ArrayList<>();
        try{
            if (fabricIDList != null && !fabricIDList.isEmpty()){
                fabricDetailList = fabricDetailMapper.getFabricDetailByFabricIDList(fabricIDList);
            }
            return fabricDetailList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return fabricDetailList;
    }

    @Override
    public FabricDetail getFabricDetailByLocationJarFabricID(String location, String jarName, Integer fabricID) {
        return fabricDetailMapper.getFabricDetailByLocationJarFabricID(location, jarName, fabricID);
    }

    @Override
    public Float getHistoryFabricReturnWeight(Integer fabricID) {
        Float weight = 0f;
        try{
            if (fabricDetailMapper.getHistoryFabricReturnWeight(fabricID) != null){
                weight = fabricDetailMapper.getHistoryFabricReturnWeight(fabricID);
            }
            return weight;
        }catch (Exception e){
            e.printStackTrace();
        }
        return weight;
    }

    @Override
    public List<FabricDetail> getFabricDetailByOperateType(String operateType) {
        return fabricDetailMapper.getFabricDetailByOperateType(operateType);
    }

    @Override
    public List<FabricDetail> getFabricDetailSummaryByFabricIdList(List<Integer> fabricIDList) {
        List<FabricDetail> fabricDetailList = new ArrayList<>();
        try{
            if (fabricIDList != null && !fabricIDList.isEmpty()){
                fabricDetailList = fabricDetailMapper.getFabricDetailSummaryByFabricIdList(fabricIDList);
            }
            return fabricDetailList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return fabricDetailList;
    }

    @Override
    public Integer addFabricDetailTmpBatch(List<FabricDetail> fabricDetailList) {
        try{
            if (fabricDetailList != null && !fabricDetailList.isEmpty()){
                fabricDetailMapper.addFabricDetailTmpBatch(fabricDetailList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<FabricDetail> getFabricDetailTmpByFabricId(Integer fabricID, String orderName, String colorName) {
        return fabricDetailMapper.getFabricDetailTmpByFabricId(fabricID, orderName, colorName);
    }

    @Override
    public Integer deleteFabricDetailTmpById(Integer fabricDetailID) {
        try{
            fabricDetailMapper.deleteFabricDetailTmpById(fabricDetailID);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<FabricDetail> getAllTmpFabricDetail(String orderName) {
        return fabricDetailMapper.getAllTmpFabricDetail(orderName);
    }

    @Override
    public Integer updateTmpFabricDetail(FabricDetail fabricDetail) {
        try{
            fabricDetailMapper.updateTmpFabricDetail(fabricDetail);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public FabricDetail getTmpFabricById(Integer fabricDetailID) {
        return fabricDetailMapper.getTmpFabricById(fabricDetailID);
    }

    @Override
    public Integer deleteFabricDetailByOrderJar(String orderName, String jarName) {
        try{
            fabricDetailMapper.deleteFabricDetailByOrderJar(orderName, jarName);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<Integer> getFabricIdListByAddFee(List<Integer> fabricDetailIDList) {
        List<Integer> fabricIDList = new ArrayList<>();
        if (fabricDetailIDList != null && !fabricDetailIDList.isEmpty()){
            fabricIDList = fabricDetailMapper.getFabricIdListByAddFee(fabricDetailIDList);
        }
        return fabricIDList;
    }
}
