package com.example.erp01.service.impl;

import com.example.erp01.mapper.ProcedureReviewMapper;
import com.example.erp01.model.OrderProcedure;
import com.example.erp01.service.ProcedureReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class ProcedureReviewServiceImpl implements ProcedureReviewService {

    @Autowired
    private ProcedureReviewMapper procedureReviewMapper;

    @Override
    public List<OrderProcedure> getOrderProcedureByInfo(String from, String to, String orderName) {
        return procedureReviewMapper.getOrderProcedureByInfo(from, to, orderName);
    }

    @Override
    public Integer updateOrderProcedureByIdBatch(List<Integer> orderProcedureIDList, Integer procedureState) {
        try{
            if (orderProcedureIDList != null && !orderProcedureIDList.isEmpty()){
                procedureReviewMapper.updateOrderProcedureByIdBatch(orderProcedureIDList, procedureState);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<String> getMonthPieceWorkOrderName(String from, String to, String orderName) {
        return procedureReviewMapper.getMonthPieceWorkOrderName(from, to, orderName);
    }
}
