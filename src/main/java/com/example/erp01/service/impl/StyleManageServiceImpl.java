package com.example.erp01.service.impl;

import com.example.erp01.mapper.StyleManageMapper;
import com.example.erp01.model.StyleManage;
import com.example.erp01.service.StyleManageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StyleManageServiceImpl implements StyleManageService {

    @Autowired
    private StyleManageMapper styleManageMapper;

    @Override
    public Integer addStyleManage(StyleManage styleManage) {
        try{
            styleManageMapper.addStyleManage(styleManage);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer deleteStyleManage(Integer id) {
        try{
            styleManageMapper.deleteStyleManage(id);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<StyleManage> getAllStyleManage() {
        return styleManageMapper.getAllStyleManage();
    }

    @Override
    public Integer updateStyleManage(StyleManage styleManage) {
        try{
            styleManageMapper.updateStyleManage(styleManage);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer updateProcessInStyleManage(StyleManage styleManage) {
        try{
            styleManageMapper.updateProcessInStyleManage(styleManage);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer updateSizeInStyleManage(StyleManage styleManage) {
        try{
            styleManageMapper.updateSizeInStyleManage(styleManage);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public StyleManage getStyleManageById(Integer id) {
        return styleManageMapper.getStyleManageById(id);
    }

    @Override
    public List<StyleManage> getStyleManageHintPage(String keyWord, int page, int limit) {
        return styleManageMapper.getStyleManageHintPage(keyWord, page, limit);
    }

    @Override
    public Integer getStyleManageCount(String keyWord) {
        int count = 0;
        try{
            if (styleManageMapper.getStyleManageCount(keyWord) != null){
                count = styleManageMapper.getStyleManageCount(keyWord);
            }
            return count;
        }catch (Exception e){
            e.printStackTrace();
        }
        return count;
    }
}
