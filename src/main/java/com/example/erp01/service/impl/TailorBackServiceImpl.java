package com.example.erp01.service.impl;

import com.example.erp01.mapper.TailorBackMapper;
import com.example.erp01.model.TailorBack;
import com.example.erp01.service.TailorBackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TailorBackServiceImpl implements TailorBackService {

    @Autowired
    private TailorBackMapper tailorBackMapper;

    @Override
    public int saveTailorBackData(List<TailorBack> tailorBackList) {
        try{
            if (tailorBackList != null && !tailorBackList.isEmpty()){
                tailorBackMapper.saveTailorBackData(tailorBackList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }
}
