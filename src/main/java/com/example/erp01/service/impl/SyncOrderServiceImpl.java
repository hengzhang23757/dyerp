package com.example.erp01.service.impl;

import com.example.erp01.model.ManufactureOrder;
import com.example.erp01.model.OrderClothes;
import com.example.erp01.service.SyncOrderService;
import com.example.erp01.ycMapper.SyncManufactureOrderMapper;
import com.example.erp01.ycMapper.SyncOrderMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SyncOrderServiceImpl implements SyncOrderService {

    @Autowired
    private SyncOrderMapper syncOrderMapper;
    @Autowired
    private SyncManufactureOrderMapper syncManufactureOrderMapper;

    @Override
    public ManufactureOrder getManufactureOrderByOrderName(String orderName) {
        return syncManufactureOrderMapper.getManufactureOrderByOrderName(orderName);
    }

    @Override
    public Integer addManufactureOrderOne(ManufactureOrder manufactureOrder) {
        try{
            syncManufactureOrderMapper.addManufactureOrderOne(manufactureOrder);
            return 0;
        } catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<OrderClothes> getOrderClothesByOrderName(String orderName) {
        return syncOrderMapper.getOrderClothesByOrderName(orderName);
    }

    @Override
    public Integer addOrderClothesBatch(List<OrderClothes> orderClothesList) {
        try{
            if (orderClothesList != null && !orderClothesList.isEmpty()){
                syncOrderMapper.addOrderClothesBatch(orderClothesList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }
}
