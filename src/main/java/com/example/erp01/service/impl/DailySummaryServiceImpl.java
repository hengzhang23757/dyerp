package com.example.erp01.service.impl;

import com.example.erp01.mapper.DailySummaryMapper;
import com.example.erp01.model.DailySummary;
import com.example.erp01.model.OrderProcedure;
import com.example.erp01.model.PieceWork;
import com.example.erp01.service.DailySummaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
@Service
public class DailySummaryServiceImpl implements DailySummaryService {

    @Autowired
    private DailySummaryMapper dailySummaryMapper;

    @Override
    public Integer addDailySummaryBatch(List<DailySummary> dailySummaryList) {
        try{
            if (dailySummaryList.size() != 0 && dailySummaryList != null){
                dailySummaryMapper.addDailySummaryBatch(dailySummaryList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<DailySummary> getDailySummaryOneMonth() {
        List<DailySummary> dailySummaryList = new ArrayList<>();
        try{
            dailySummaryList = dailySummaryMapper.getDailySummaryOneMonth();
            return dailySummaryList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return dailySummaryList;
    }

    @Override
    public List<String> getDistinctEmployeeNumberHang(Date from, Date to) {
        List<String> employeeNumberList = new ArrayList<>();
        try{
            employeeNumberList = dailySummaryMapper.getDistinctEmployeeNumberHang(from, to);
            return employeeNumberList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return employeeNumberList;
    }

    @Override
    public List<String> getDistinctOrderNameHang(Date from, Date to) {
        List<String> orderNameList = new ArrayList<>();
        try{
            orderNameList = dailySummaryMapper.getDistinctOrderNameHang(from, to);
            return orderNameList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return orderNameList;
    }

    @Override
    public List<OrderProcedure> getDistinctOrderProcedureHang(Date from, Date to) {
        List<OrderProcedure> orderProcedureList = new ArrayList<>();
        try{
            orderProcedureList = dailySummaryMapper.getDistinctOrderProcedureHang(from, to);
            return orderProcedureList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return orderProcedureList;
    }

    @Override
    public Integer updateHangEmployeeInfo(String employeeNumber, String groupName) {
        try{
            dailySummaryMapper.updateHangEmployeeInfo(employeeNumber, groupName);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer updateHangOrderInfo(String orderName, String clothesVersionNumber) {
        try{
            dailySummaryMapper.updateHangOrderInfo(orderName, clothesVersionNumber);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer updateHangProcedureInfo(String orderName, Integer procedureNumber, String procedureName) {
        try{
            dailySummaryMapper.updateHangProcedureInfo(orderName, procedureNumber, procedureName);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<String> getDistinctEmployeeNumberMini(Date from, Date to) {
        List<String> employeeNumberList = new ArrayList<>();
        try{
            employeeNumberList = dailySummaryMapper.getDistinctEmployeeNumberMini(from, to);
            return employeeNumberList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return employeeNumberList;
    }

    @Override
    public List<String> getDistinctOrderNameMini(Date from, Date to) {
        List<String> orderNameList = new ArrayList<>();
        try{
            orderNameList = dailySummaryMapper.getDistinctOrderNameMini(from, to);
            return orderNameList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return orderNameList;
    }

    @Override
    public List<OrderProcedure> getDistinctOrderProcedureMini(Date from, Date to) {
        List<OrderProcedure> orderProcedureList = new ArrayList<>();
        try{
            orderProcedureList = dailySummaryMapper.getDistinctOrderProcedureMini(from, to);
            return orderProcedureList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return orderProcedureList;
    }

    @Override
    public Integer updateMiniEmployeeInfo(String employeeNumber, String employeeName, String groupName) {
        try{
            dailySummaryMapper.updateMiniEmployeeInfo(employeeNumber, employeeName, groupName);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer updateMiniOrderInfo(String orderName, String clothesVersionNumber) {
        try{
            dailySummaryMapper.updateMiniOrderInfo(orderName, clothesVersionNumber);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer updateMiniProcedureInfo(String orderName, Integer procedureNumber, String procedureName) {
        try{
            dailySummaryMapper.updateMiniProcedureInfo(orderName, procedureNumber, procedureName);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<PieceWork> getPieceWorkFromHang(Date from, Date to) {
        List<PieceWork> pieceWorkList = new ArrayList<>();
        try{
            pieceWorkList = dailySummaryMapper.getPieceWorkFromHang(from, to);
            return pieceWorkList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return pieceWorkList;
    }

    @Override
    public Integer copyPieceWorkFromHangToMini(List<PieceWork> pieceWorkList) {
        try{
            if (pieceWorkList != null && !pieceWorkList.isEmpty()){
                dailySummaryMapper.copyPieceWorkFromHangToMini(pieceWorkList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer getMaxNidFromPieceWork() {
        Integer maxNid = dailySummaryMapper.getMaxNidFromPieceWork();
        if (maxNid != null){
            return maxNid;
        }
        return 0;
    }

    @Override
    public List<PieceWork> getPieceWorkFromHangByNid(Integer nid) {
        return dailySummaryMapper.getPieceWorkFromHangByNid(nid);
    }
}
