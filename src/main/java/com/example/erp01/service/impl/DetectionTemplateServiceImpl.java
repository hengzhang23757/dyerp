package com.example.erp01.service.impl;

import com.example.erp01.mapper.DetectionTemplateMapper;
import com.example.erp01.model.DetectionTemplate;
import com.example.erp01.service.DetectionTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class DetectionTemplateServiceImpl implements DetectionTemplateService {

    @Autowired
    private DetectionTemplateMapper detectionTemplateMapper;

    @Override
    public Integer addDetectionTemplate(DetectionTemplate detectionTemplate) {
        try{
            detectionTemplateMapper.addDetectionTemplate(detectionTemplate);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer updateDetectionTemplate(DetectionTemplate detectionTemplate) {
        try{
            detectionTemplateMapper.updateDetectionTemplate(detectionTemplate);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer deleteDetectionTemplate(Integer id) {
        try{
            detectionTemplateMapper.deleteDetectionTemplate(id);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<DetectionTemplate> getDetectionTemplateByInfo(Map<String, Object> map) {
        return detectionTemplateMapper.getDetectionTemplateByInfo(map);
    }

    @Override
    public DetectionTemplate getDetectionTemplateById(Integer id) {
        return detectionTemplateMapper.getDetectionTemplateById(id);
    }

    @Override
    public List<String> getDetectionNameByDetectionType(String detectionType) {
        return detectionTemplateMapper.getDetectionNameByDetectionType(detectionType);
    }
}
