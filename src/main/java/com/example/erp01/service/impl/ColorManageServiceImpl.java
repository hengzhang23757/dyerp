package com.example.erp01.service.impl;

import com.example.erp01.mapper.ColorManageMapper;
import com.example.erp01.model.ColorManage;
import com.example.erp01.service.ColorManageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ColorManageServiceImpl implements ColorManageService {

    @Autowired
    private ColorManageMapper colorManageMapper;

    @Override
    public Integer addColorManage(ColorManage colorManage) {
        try{
            colorManageMapper.addColorManage(colorManage);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer deleteColorManage(Integer id) {
        try{
            colorManageMapper.deleteColorManage(id);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<ColorManage> getAllColorManage() {
        return colorManageMapper.getAllColorManage();
    }

    @Override
    public Integer updateColorManage(ColorManage colorManage) {
        try{
            colorManageMapper.updateColorManage(colorManage);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }
}
