package com.example.erp01.service.impl;

import com.example.erp01.mapper.DesignDepartMapper;
import com.example.erp01.model.DesignDepart;
import com.example.erp01.service.DesignDepartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DesignDepartServiceImpl implements DesignDepartService {

    @Autowired
    private DesignDepartMapper designDepartMapper;

    @Override
    public Integer addDesignDepart(DesignDepart designDepart) {
        try{
            designDepartMapper.addDesignDepart(designDepart);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer deleteColorManage(Integer id) {
        try{
            designDepartMapper.deleteColorManage(id);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<DesignDepart> getAllDesignDepart() {
        return designDepartMapper.getAllDesignDepart();
    }

    @Override
    public Integer updateDesignDepart(DesignDepart designDepart) {
        try{
            designDepartMapper.updateDesignDepart(designDepart);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }
}
