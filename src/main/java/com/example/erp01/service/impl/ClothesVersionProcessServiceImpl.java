package com.example.erp01.service.impl;

import com.example.erp01.mapper.ClothesVersionProcessMapper;
import com.example.erp01.model.ClothesVersionProcess;
import com.example.erp01.service.ClothesVersionProcessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ClothesVersionProcessServiceImpl implements ClothesVersionProcessService {

    @Autowired
    private ClothesVersionProcessMapper clothesVersionProcessMapper;


    @Override
    public int addClothesVersionProcess(ClothesVersionProcess clothesVersionProcess) {
        try{
            clothesVersionProcessMapper.addClothesVersionProcess(clothesVersionProcess);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<ClothesVersionProcess> getUniqueClothesVersionProcess() {
        List<ClothesVersionProcess> clothesVersionProcessList = new ArrayList<>();
        try{
            clothesVersionProcessList = clothesVersionProcessMapper.getUniqueClothesVersionProcess();
            return clothesVersionProcessList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return clothesVersionProcessList;
    }

    @Override
    public List<ClothesVersionProcess> getClothesVersionProcessByOrder(String orderName) {
        List<ClothesVersionProcess> clothesVersionProcessList = new ArrayList<>();
        try{
            clothesVersionProcessList = clothesVersionProcessMapper.getClothesVersionProcessByOrder(orderName);
            return clothesVersionProcessList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return clothesVersionProcessList;
    }

    @Override
    public int addClothesVersionProcessBatch(List<ClothesVersionProcess> clothesVersionProcessList) {
        try{
            if (clothesVersionProcessList != null && !clothesVersionProcessList.isEmpty()){
                clothesVersionProcessMapper.addClothesVersionProcessBatch(clothesVersionProcessList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public int deleteClothesVersionProcess(Integer clothesVersionProcessID) {
        try{
            clothesVersionProcessMapper.deleteClothesVersionProcess(clothesVersionProcessID);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public int deleteClothesVersionProcessByOrder(String orderName) {
        try{
            clothesVersionProcessMapper.deleteClothesVersionProcessByOrder(orderName);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }
}
