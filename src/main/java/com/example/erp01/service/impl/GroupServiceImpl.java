package com.example.erp01.service.impl;

import com.example.erp01.mapper.GroupMapper;
import com.example.erp01.service.GroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class GroupServiceImpl implements GroupService {

    @Autowired
    private GroupMapper groupMapper;


    @Override
    public List<String> getAllGroupNames() {
        List<String> groupList = new ArrayList<>();
        try{
            groupList = groupMapper.getAllGroupNames();
            return groupList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return groupList;
    }

    @Override
    public List<String> getBigGroupNames() {
        List<String> groupList = new ArrayList<>();
        try{
            groupList = groupMapper.getBigGroupNames();
            return groupList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return groupList;
    }

    @Override
    public List<String> getSewGroupNames() {
        List<String> groupList = new ArrayList<>();
        try{
            groupList = groupMapper.getSewGroupNames();
            return groupList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return groupList;
    }
}
