package com.example.erp01.service.impl;

import com.example.erp01.mapper.FabricAccessoryLeakMapper;
import com.example.erp01.model.*;
import com.example.erp01.service.FabricAccessoryLeakService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
@Service
public class FabricAccessoryLeakServiceImpl implements FabricAccessoryLeakService {

    @Autowired
    private FabricAccessoryLeakMapper fabricAccessoryLeakMapper;


    @Override
    public List<FabricAccessoryLeak> getOrderInfoByOrderList(List<String> orderNameList) {
        List<FabricAccessoryLeak> fabricAccessoryLeakList = new ArrayList<>();
        try{
            if (orderNameList != null && !orderNameList.isEmpty()){
                fabricAccessoryLeakList = fabricAccessoryLeakMapper.getOrderInfoByOrderList(orderNameList);
            }
            return fabricAccessoryLeakList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return fabricAccessoryLeakList;
    }

    @Override
    public List<FabricAccessoryLeak> getCutInfoByOrderList(List<String> orderNameList) {
        List<FabricAccessoryLeak> fabricAccessoryLeakList = new ArrayList<>();
        try{
            if (orderNameList != null && !orderNameList.isEmpty()){
                fabricAccessoryLeakList = fabricAccessoryLeakMapper.getCutInfoByOrderList(orderNameList);
            }
            return fabricAccessoryLeakList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return fabricAccessoryLeakList;
    }

    @Override
    public List<FabricAccessoryLeak> getFabricAccessoryLeakByOrderList(List<String> orderNameList, String type) {
        List<FabricAccessoryLeak> fabricAccessoryLeakList = new ArrayList<>();
        try{
            if (orderNameList != null && !orderNameList.isEmpty()){
                if (type.equals("面料")){
                    fabricAccessoryLeakList = fabricAccessoryLeakMapper.getFabricLeakByOrderList(orderNameList);
                } else if (type.equals("辅料")){
                    fabricAccessoryLeakList = fabricAccessoryLeakMapper.getAccessoryLeakByOrderList(orderNameList);
                }
            }
            return fabricAccessoryLeakList;
        } catch (Exception e){
            e.printStackTrace();
        }
        return fabricAccessoryLeakList;
    }

    @Override
    public List<FabricAccessoryLeak> getFabricAccessoryReturnByOrderList(List<String> orderNameList, String type) {
        List<FabricAccessoryLeak> fabricAccessoryLeakList = new ArrayList<>();
        try{
            if (orderNameList != null && !orderNameList.isEmpty()){
                if (type.equals("面料")){
                    fabricAccessoryLeakList = fabricAccessoryLeakMapper.getFabricReturnByOrderList(orderNameList);
                } else if (type.equals("辅料")){
                    fabricAccessoryLeakList = fabricAccessoryLeakMapper.getAccessoryReturnByOrderList(orderNameList);
                }
            }
            return fabricAccessoryLeakList;
        } catch (Exception e){
            e.printStackTrace();
        }
        return fabricAccessoryLeakList;
    }

    @Override
    public List<ManufactureFabric> getManufactureFabricByOrderList(List<String> orderNameList) {
        List<ManufactureFabric> manufactureFabricList = new ArrayList<>();
        try{
            if (orderNameList != null && !orderNameList.isEmpty()){
                manufactureFabricList = fabricAccessoryLeakMapper.getManufactureFabricByOrderList(orderNameList);
            }
            return manufactureFabricList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return manufactureFabricList;
    }

    @Override
    public List<FabricDetail> getFabricDetailByOrderList(List<String> orderNameList) {
        List<FabricDetail> fabricDetailList = new ArrayList<>();
        try{
            if (orderNameList != null && !orderNameList.isEmpty()){
                fabricDetailList = fabricAccessoryLeakMapper.getFabricDetailByOrderList(orderNameList);
            }
            return fabricDetailList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return fabricDetailList;
    }

    @Override
    public List<ManufactureAccessory> getManufactureAccessoryByOrderList(List<String> orderNameList) {
        List<ManufactureAccessory> manufactureAccessoryList = new ArrayList<>();
        try{
            if (orderNameList != null && !orderNameList.isEmpty()){
                manufactureAccessoryList = fabricAccessoryLeakMapper.getManufactureAccessoryByOrderList(orderNameList);
            }
            return manufactureAccessoryList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return manufactureAccessoryList;
    }

    @Override
    public List<AccessoryInStore> getAccessoryInStoreByOrderList(List<String> orderNameList) {
        List<AccessoryInStore> accessoryInStoreList = new ArrayList<>();
        try{
            if (orderNameList != null && !orderNameList.isEmpty()){
                accessoryInStoreList = fabricAccessoryLeakMapper.getAccessoryInStoreByOrderList(orderNameList);
            }
            return accessoryInStoreList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return accessoryInStoreList;
    }
}
