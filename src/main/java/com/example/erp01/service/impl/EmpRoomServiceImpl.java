package com.example.erp01.service.impl;

import com.example.erp01.mapper.EmpRoomMapper;
import com.example.erp01.model.EmpRoom;
import com.example.erp01.model.RoomState;
import com.example.erp01.service.EmpRoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
@Service
public class EmpRoomServiceImpl implements EmpRoomService {

    @Autowired
    private EmpRoomMapper empRoomMapper;

    @Override
    public int addEmpRoom(EmpRoom empRoom) {
        try{
            empRoomMapper.addEmpRoom(empRoom);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public int deleteEmpRoom(Integer empRoomID) {
        try{
            empRoomMapper.deleteEmpRoom(empRoomID);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public int updateEmpRoom(EmpRoom empRoom) {
        try{
            empRoomMapper.updateEmpRoom(empRoom);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<EmpRoom> getAllEmpRoom() {
        List<EmpRoom> empRoomList = new ArrayList<>();
        try{
            empRoomList = empRoomMapper.getAllEmpRoom();
            return empRoomList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return empRoomList;
    }

    @Override
    public List<RoomState> getRoomStateOfFloor(String floor) {
        List<RoomState> roomStateList = new ArrayList<>();
        try{
            roomStateList = empRoomMapper.getRoomStateOfFloor(floor);
            return roomStateList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return roomStateList;
    }

    @Override
    public List<String> getAllBedNumberOfRoom(String roomNumber) {
        List<String> bedNumberList = new ArrayList<>();
        try{
            bedNumberList = empRoomMapper.getAllBedNumberOfRoom(roomNumber);
            return bedNumberList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return bedNumberList;
    }

    @Override
    public List<EmpRoom> getEmpRoomByRoom(String roomNumber) {
        List<EmpRoom> empRoomList = new ArrayList<>();
        try{
            empRoomList = empRoomMapper.getEmpRoomByRoom(roomNumber);
            return empRoomList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return empRoomList;
    }

}
