package com.example.erp01.service.impl;

import com.example.erp01.mapper.SpecialProcedureMapper;
import com.example.erp01.model.SpecialProcedure;
import com.example.erp01.service.SpecialProcedureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SpecialProcedureServiceImpl implements SpecialProcedureService {

    @Autowired
    private SpecialProcedureMapper specialProcedureMapper;

    @Override
    public int addSpecialProcedureBatch(List<SpecialProcedure> specialProcedureList) {
        try{
            if (specialProcedureList != null && !specialProcedureList.isEmpty()){
                specialProcedureMapper.addSpecialProcedureBatch(specialProcedureList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public int deleteSpecialProcedure(Integer specialID) {
        try{
            specialProcedureMapper.deleteSpecialProcedure(specialID);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<SpecialProcedure> getAllSpecialProcedure() {
        List<SpecialProcedure> specialProcedureList = new ArrayList<>();
        try{
            specialProcedureList = specialProcedureMapper.getAllSpecialProcedure();
            return specialProcedureList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return specialProcedureList;
    }

    @Override
    public List<SpecialProcedure> getSpecialProcedureByOrderProcedure(String orderName, Integer procedureNumber) {
        List<SpecialProcedure> specialProcedureList = new ArrayList<>();
        try{
            specialProcedureList = specialProcedureMapper.getSpecialProcedureByOrderProcedure(orderName, procedureNumber);
            return specialProcedureList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return specialProcedureList;
    }
}
