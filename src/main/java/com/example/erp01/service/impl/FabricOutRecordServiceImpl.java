package com.example.erp01.service.impl;

import com.example.erp01.mapper.FabricOutRecordMapper;
import com.example.erp01.model.FabricOutRecord;
import com.example.erp01.service.FabricOutRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class FabricOutRecordServiceImpl implements FabricOutRecordService {

    @Autowired
    private FabricOutRecordMapper fabricOutRecordMapper;

    @Override
    public Integer addFabricOutRecordBatch(List<FabricOutRecord> fabricOutRecordList) {
        try{
            if (fabricOutRecordList != null && !fabricOutRecordList.isEmpty()){
                fabricOutRecordMapper.addFabricOutRecordBatch(fabricOutRecordList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer addFabricOutRecord(FabricOutRecord fabricOutRecord) {
        try{
            fabricOutRecordMapper.addFabricOutRecord(fabricOutRecord);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer deleteFabricOutRecord(Integer fabricOutRecordID) {
        try{
            fabricOutRecordMapper.deleteFabricOutRecord(fabricOutRecordID);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<FabricOutRecord> getAllFabricOutRecord() {
        List<FabricOutRecord> fabricOutRecordList = new ArrayList<>();
        try{
            fabricOutRecordList = fabricOutRecordMapper.getAllFabricOutRecord();
            return fabricOutRecordList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return fabricOutRecordList;
    }

    @Override
    public List<FabricOutRecord> getFabricOutRecordByOrder(String orderName) {
        List<FabricOutRecord> fabricOutRecordList = new ArrayList<>();
        try{
            fabricOutRecordList = fabricOutRecordMapper.getFabricOutRecordByOrder(orderName);
            return fabricOutRecordList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return fabricOutRecordList;
    }

    @Override
    public List<FabricOutRecord> getTodayFabricOutRecord() {
        List<FabricOutRecord> fabricOutRecordList = new ArrayList<>();
        try{
            fabricOutRecordList = fabricOutRecordMapper.getTodayFabricOutRecord();
            return fabricOutRecordList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return fabricOutRecordList;
    }

    @Override
    public List<FabricOutRecord> getOneMonthFabricOutRecord() {
        List<FabricOutRecord> fabricOutRecordList = new ArrayList<>();
        try{
            fabricOutRecordList = fabricOutRecordMapper.getOneMonthFabricOutRecord();
            return fabricOutRecordList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return fabricOutRecordList;
    }

    @Override
    public List<FabricOutRecord> getThreeMonthsFabricOutRecord() {
        List<FabricOutRecord> fabricOutRecordList = new ArrayList<>();
        try{
            fabricOutRecordList = fabricOutRecordMapper.getThreeMonthsFabricOutRecord();
            return fabricOutRecordList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return fabricOutRecordList;
    }

    @Override
    public List<FabricOutRecord> getFabricOutRecordByInfo(String orderName, String fabricName, String colorName, String fabricColor, Integer fabricID) {
        List<FabricOutRecord> fabricOutRecordList = new ArrayList<>();
        try{
            fabricOutRecordList = fabricOutRecordMapper.getFabricOutRecordByInfo(orderName, fabricName, colorName, fabricColor, fabricID);
            return fabricOutRecordList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return fabricOutRecordList;
    }

    @Override
    public List<FabricOutRecord> getFabricOutRecordByOrderType(String orderName, String operateType, String colorName) {
        List<FabricOutRecord> fabricOutRecordList = new ArrayList<>();
        try{
            fabricOutRecordList = fabricOutRecordMapper.getFabricOutRecordByOrderType(orderName, operateType,colorName);
            return fabricOutRecordList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return fabricOutRecordList;
    }

    @Override
    public List<FabricOutRecord> getFabricOutRecordByTime(String from, String to) {
        List<FabricOutRecord> fabricOutRecordList = new ArrayList<>();
        try{
            fabricOutRecordList = fabricOutRecordMapper.getFabricOutRecordByTime(from, to);
            return fabricOutRecordList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return fabricOutRecordList;
    }

    @Override
    public FabricOutRecord getFabricOutRecordById(Integer fabricOutRecordID) {
        return fabricOutRecordMapper.getFabricOutRecordById(fabricOutRecordID);
    }

    @Override
    public Float getReturnFabricWeightByFabricID(Integer fabricID) {
        Float weight = 0f;
        try{
            if (fabricOutRecordMapper.getReturnFabricWeightByFabricID(fabricID) != null){
                weight = fabricOutRecordMapper.getReturnFabricWeightByFabricID(fabricID);
            }
            return weight;
        }catch (Exception e){
            e.printStackTrace();
        }
        return weight;

    }

    @Override
    public List<FabricOutRecord> getFabricOutRecordByMap(Map<String, Object> map) {
        return fabricOutRecordMapper.getFabricOutRecordByMap(map);
    }

    @Override
    public Integer deleteFabricOutRecordByOrderJar(String orderName, String jarName) {
        try{
            fabricOutRecordMapper.deleteFabricOutRecordByOrderJar(orderName, jarName);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }
}
