package com.example.erp01.service.impl;

import com.example.erp01.mapper.OtherOPAMapper;
import com.example.erp01.model.OtherOPA;
import com.example.erp01.service.OtherOPAService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class OtherOPAServiceImpl implements OtherOPAService {

    @Autowired
    private OtherOPAMapper otherOPAMapper;

    @Override
    public int addOtherOPA(OtherOPA otherOPA) {
        try{
            otherOPAMapper.addOtherOPA(otherOPA);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public int addOtherOPABatch(List<OtherOPA> otherOPAList) {
        try{
            if (otherOPAList != null && !otherOPAList.isEmpty()){
                otherOPAMapper.addOtherOPABatch(otherOPAList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public int deleteOtherOPA(Integer otherOpaID) {
        try{
            otherOPAMapper.deleteOtherOPA(otherOpaID);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<OtherOPA> getAllOtherOPA() {
        List<OtherOPA> otherOPAList = new ArrayList<>();
        try{
            otherOPAList = otherOPAMapper.getAllOtherOPA();
            return otherOPAList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return otherOPAList;
    }

    @Override
    public List<OtherOPA> getTodayOtherOPA() {
        List<OtherOPA> otherOPAList = new ArrayList<>();
        try{
            otherOPAList = otherOPAMapper.getTodayOtherOPA();
            return otherOPAList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return otherOPAList;
    }

    @Override
    public List<OtherOPA> getOneMonthOtherOPA() {
        List<OtherOPA> otherOPAList = new ArrayList<>();
        try{
            otherOPAList = otherOPAMapper.getOneMonthOtherOPA();
            return otherOPAList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return otherOPAList;
    }

    @Override
    public List<OtherOPA> getThreeMonthOtherOPA() {
        List<OtherOPA> otherOPAList = new ArrayList<>();
        try{
            otherOPAList = otherOPAMapper.getThreeMonthOtherOPA();
            return otherOPAList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return otherOPAList;
    }

    @Override
    public List<OtherOPA> getOtherOPAByOrder(String orderName) {
        List<OtherOPA> otherOPAList = new ArrayList<>();
        try{
            otherOPAList = otherOPAMapper.getOtherOPAByOrder(orderName);
            return otherOPAList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return otherOPAList;
    }

    @Override
    public List<OtherOPA> getOtherBedOPAByOrder(String orderName) {
        List<OtherOPA> otherOPAList = new ArrayList<>();
        try{
            otherOPAList = otherOPAMapper.getOtherBedOPAByOrder(orderName);
            return otherOPAList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return otherOPAList;
    }
}
