package com.example.erp01.service.impl;

import com.example.erp01.mapper.EmbStorageMapper;
import com.example.erp01.model.*;
import com.example.erp01.service.EmbStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class EmbStorageServiceImpl implements EmbStorageService {
    @Autowired
    private EmbStorageMapper embStorageMapper;

    @Override
    public int embInStore(List<EmbStorage> embStorageList) {
        try{
            if(embStorageList!=null && !embStorageList.isEmpty()){
                embStorageMapper.embInStore(embStorageList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public int embOutStore(List<String> embOutStoreList) {
        try{
            embStorageMapper.embOutStore(embOutStoreList);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public int embChangeStore(String embChangeStoreJson) {
        try{
            embStorageMapper.embChangeStore(embChangeStoreJson);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<EmbStorageState> getEmbStorageState() {
        List<EmbStorageState> embStorageStateList = new ArrayList<>();
        try{
            embStorageStateList = embStorageMapper.getEmbStorageState();

            return embStorageStateList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return embStorageStateList;
    }

    @Override
    public List<StorageFloor> getEmbStorageFloor() {
        List<StorageFloor> storageFloorList = new ArrayList<>();
        try{
            storageFloorList = embStorageMapper.getEmbStorageFloor();
            return storageFloorList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return storageFloorList;
    }

    @Override
    public List<EmbStorageQuery> embStorageQuery(String orderName, String colorName, String sizeName) {
        List<EmbStorageQuery> queryResult = new ArrayList<>();
        try{
            queryResult = embStorageMapper.embStorageQuery(orderName, colorName, sizeName);
            return queryResult;
        }catch (Exception e){
            e.printStackTrace();
        }
        return queryResult;
    }

    @Override
    public List<String> getEmbOrderHint(String subOrderName) {
        List<String> orderList = new ArrayList<>();
        try{
            orderList = embStorageMapper.getEmbOrderHint(subOrderName);
            return orderList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return orderList;

    }

    @Override
    public List<String> getEmbColorHint(String orderName) {
        List<String> colorList = new ArrayList<>();
        try {
            colorList = embStorageMapper.getEmbColorHint(orderName);
            return colorList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return colorList;
    }

    @Override
    public List<String> getEmbSizeHint(String orderName, String colorName) {
        List<String> sizeList = new ArrayList<>();
        try {
            sizeList = embStorageMapper.getEmbSizeHint(orderName, colorName);
            return sizeList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return sizeList;
    }

    @Override
    public int embOutStoreOne(String subTailorQcode) {
        try{
            embStorageMapper.embOutStoreOne(subTailorQcode);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public String getLocationByQcode(String tailorQcode) {
        String location = null;
        try{
            location = embStorageMapper.getLocationByQcode(tailorQcode);
            return location;
        }catch (Exception e){
            e.printStackTrace();
        }
        return location;
    }

    @Override
    public List<EmbStorage> getEmbStorageByOrderColorSize(String embStoreLocation, String orderName, String colorName, String sizeName) {
        List<EmbStorage> embStorageList = new ArrayList<>();
        try{
            embStorageList = embStorageMapper.getEmbStorageByOrderColorSize(embStoreLocation, orderName, colorName, sizeName);
            return embStorageList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return embStorageList;
    }

    @Override
    public Integer embOutStoreBatch(String embStoreLocation, String orderName, String colorName, String sizeName) {
        try{
            embStorageMapper.embOutStoreBatch(embStoreLocation, orderName, colorName, sizeName);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public String getLocationByOrderColorSizePackage(String orderName, Integer bedNumber, String colorName, String sizeName, Integer packageNumber) {
        String location = null;
        try{
            location = embStorageMapper.getLocationByOrderColorSizePackage(orderName, bedNumber, colorName, sizeName, packageNumber);
            return location;
        }catch (Exception e){
            e.printStackTrace();
        }
        return location;
    }

    @Override
    public List<EmbStorage> getEmbStorageByEmbStoreLocation(String embStoreLocation) {
        List<EmbStorage> embStorageList = new ArrayList<>();
        try{
            embStorageList = embStorageMapper.getEmbStorageByEmbStoreLocation(embStoreLocation);
            return embStorageList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return embStorageList;
    }

    @Override
    public List<EmbStorage> getAllEmbStorage() {
        List<EmbStorage> embStorageList = new ArrayList<>();
        try{
            embStorageList = embStorageMapper.getAllEmbStorage();
            return embStorageList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return embStorageList;
    }

    @Override
    public List<EmbStorage> getEmbStorageByOrderColorSizeLocation(String orderName, String colorName, String sizeName, String embStoreLocation) {
        List<EmbStorage> embStorageList = new ArrayList<>();
        try{
            embStorageList = embStorageMapper.getEmbStorageByOrderColorSizeLocation(orderName, colorName, sizeName, embStoreLocation);
            return embStorageList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return embStorageList;
    }

    @Override
    public Integer embOutStoreSingle(List<EmbOutInfo> embOutInfoList) {
        try{
            if(embOutInfoList!=null && !embOutInfoList.isEmpty()){
                embStorageMapper.embOutStoreSingle(embOutInfoList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer deleteEmbStorageByInfo(String orderName, String colorName, String sizeName, String embStoreLocation) {
        try{
            embStorageMapper.deleteEmbStorageByInfo(orderName, colorName, sizeName, embStoreLocation);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer deleteEmbStorageById(Integer embStorageID) {
        try{
            embStorageMapper.deleteEmbStorageById(embStorageID);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer deleteEmbStorageByOrderBedPackage(String orderName, Integer bedNumber, Integer packageNumber) {
        try{
            embStorageMapper.deleteEmbStorageByOrderBedPackage(orderName, bedNumber, packageNumber);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<EmbStorage> getEmbStorageGroupByColor(String orderName) {
        List<EmbStorage> embStorageList = new ArrayList<>();
        try{
            embStorageList = embStorageMapper.getEmbStorageGroupByColor(orderName);
            return embStorageList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return embStorageList;
    }

    @Override
    public List<EmbStorage> getEmbStorageSummaryByLocation(String embStoreLocation) {
        List<EmbStorage> embStorageList = new ArrayList<>();
        try{
            embStorageList = embStorageMapper.getEmbStorageSummaryByLocation(embStoreLocation);
            return embStorageList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return embStorageList;
    }

    @Override
    public List<EmbStorage> getEmbStorageDetailByOrderColorSize(String orderName, String colorName, String sizeName) {
        List<EmbStorage> embStorageList = new ArrayList<>();
        try{
            embStorageList = embStorageMapper.getEmbStorageDetailByOrderColorSize(orderName, colorName, sizeName);
            return embStorageList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return embStorageList;
    }

    @Override
    public List<EmbStorage> getEmbStorageByOrderColorSizeList(String orderName, List<String> colorList, List<String> sizeList) {
        List<EmbStorage> embStorageList = new ArrayList<>();
        try{
            embStorageList = embStorageMapper.getEmbStorageByOrderColorSizeList(orderName, colorList, sizeList);
            return embStorageList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return embStorageList;

    }

    @Override
    public List<EmbStorage> getEmbStorageByInfo(String orderName, String colorName, String sizeName) {
        List<EmbStorage> embStorageList = new ArrayList<>();
        try{
            embStorageList = embStorageMapper.getEmbStorageByInfo(orderName, colorName, sizeName);
            return embStorageList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return embStorageList;
    }
}
