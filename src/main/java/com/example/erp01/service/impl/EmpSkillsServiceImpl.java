package com.example.erp01.service.impl;

import com.example.erp01.mapper.EmpSkillsMapper;
import com.example.erp01.model.*;
import com.example.erp01.service.EmpSkillsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class EmpSkillsServiceImpl implements EmpSkillsService {

    @Autowired
    private EmpSkillsMapper empSkillsMapper;

    @Override
    public Integer addEmpSkillsBatch(List<EmpSkills> empSkillsList) {
        try{
            if (empSkillsList != null && !empSkillsList.isEmpty()){
                empSkillsMapper.addEmpSkillsBatch(empSkillsList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<EmpSkills> getEmpSkillsByInfo(String groupName, Integer procedureCode, String employeeNumber) {
        List<EmpSkills> empSkillsList = new ArrayList<>();
        try{
            empSkillsList = empSkillsMapper.getEmpSkillsByInfo(groupName, procedureCode, employeeNumber);
            return empSkillsList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return empSkillsList;

    }

    @Override
    public List<EmpSkills> getDistinctHistoryPieceWorkDetail() {
        List<EmpSkills> empSkillsList = new ArrayList<>();
        try{
            empSkillsList = empSkillsMapper.getDistinctHistoryPieceWorkDetail();
            return empSkillsList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return empSkillsList;
    }

    @Override
    public List<Float> getPieceWorkDetailByInfo(Integer procedureCode, String employeeNumber) {
        List<Float> timeCountList = new ArrayList<>();
        try{
            timeCountList = empSkillsMapper.getPieceWorkDetailByInfo(procedureCode, employeeNumber);
            return timeCountList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return timeCountList;
    }

    @Override
    public List<EmpSkills> getYesterdayEmpSkills() {
        List<EmpSkills> empSkillsList = new ArrayList<>();
        try{
            empSkillsList = empSkillsMapper.getYesterdayEmpSkills();
            return empSkillsList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return empSkillsList;
    }

    @Override
    public List<EmpSkills> getEmpSkillsByGroupProcedureCode(String groupName, List<Integer> procedureCodeList) {
        List<EmpSkills> empSkillsList = new ArrayList<>();
        try{
            if (procedureCodeList.size() >0 && !procedureCodeList.isEmpty()){
                empSkillsList = empSkillsMapper.getEmpSkillsByGroupProcedureCode(groupName, procedureCodeList);
            }
            return empSkillsList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return empSkillsList;
    }

    @Override
    public List<EmpSkills> getAllEmpSkills() {
        List<EmpSkills> empSkillsList = new ArrayList<>();
        try{
            empSkillsList = empSkillsMapper.getAllEmpSkills();
            return empSkillsList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return empSkillsList;
    }

    @Override
    public Integer updateEmpSkillsBatch(List<EmpSkills> empSkillsList) {
        try{
            if (empSkillsList != null && !empSkillsList.isEmpty()){
                empSkillsMapper.updateEmpSkillsBatch(empSkillsList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer dailyCleanEmpSkills() {
        try{
            empSkillsMapper.dailyCleanEmpSkills();
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<Integer> getProcedureCodeYesterday() {
        List<Integer> procedureCodeList = new ArrayList<>();
        try{
            procedureCodeList = empSkillsMapper.getProcedureCodeYesterday();
            return procedureCodeList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return procedureCodeList;
    }

    @Override
    public MinMaxNid getYesterdayMinMaxNid() {
        MinMaxNid minMaxNid = null;
        try {
            if (empSkillsMapper.getYesterdayMinMaxNid() != null){
                minMaxNid = empSkillsMapper.getYesterdayMinMaxNid();
                return minMaxNid;
            }
            return minMaxNid;
        } catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<ProcedureTemplate> getProcedureTemplateByProcedureCode(List<Integer> procedureCodeList) {
        List<ProcedureTemplate> procedureTemplateList = new ArrayList<>();
        try{
            procedureTemplateList = empSkillsMapper.getProcedureTemplateByProcedureCode(procedureCodeList);
            return procedureTemplateList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return procedureTemplateList;
    }

    @Override
    public Integer updateSamByProcedureCode(Float SAM, Integer procedureCode, Long beginNid, Long endNid) {
        try{
            empSkillsMapper.updateSamByProcedureCode(SAM, procedureCode, beginNid, endNid);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<Metre> getProcedureCombineOfYesterday() {
        List<Metre> metreList = new ArrayList<>();
        try{
            metreList = empSkillsMapper.getProcedureCombineOfYesterday();
            return metreList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return metreList;
    }

    @Override
    public Integer updateMetreBatch(List<Metre> metreList) {
        try{
            if (metreList != null && !metreList.isEmpty()){
                empSkillsMapper.updateMetreBatch(metreList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<EmpSkills> getSkillsByMap(Map<String, Object> params) {
        return empSkillsMapper.getSkillsByMap(params);
    }
}
