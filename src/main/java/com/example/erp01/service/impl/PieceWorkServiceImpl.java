package com.example.erp01.service.impl;

import com.example.erp01.mapper.PieceWorkMapper;
import com.example.erp01.model.*;
import com.example.erp01.service.PieceWorkService;
import jdk.nashorn.internal.objects.NativeUint8Array;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class PieceWorkServiceImpl implements PieceWorkService {

    @Autowired
    private PieceWorkMapper pieceWorkMapper;

    @Override
    public int addPieceWorkBatch(List<PieceWork> pieceWorkList) {
        try{
            if(pieceWorkList!=null && !pieceWorkList.isEmpty()) {
                pieceWorkMapper.addPieceWorkBatch(pieceWorkList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public int addPieceWork(PieceWork pieceWork) {
        try{
            pieceWorkMapper.addPieceWork(pieceWork);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public int deletePieceWork(Integer id) {
        try{
            pieceWorkMapper.deletePieceWork(id);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public PieceWork getPieceWorkByID(Integer id) {
        PieceWork pieceWork = null;
        try{
            pieceWork = pieceWorkMapper.getPieceWorkByID(id);
            return pieceWork;
        }catch (Exception e){
            e.printStackTrace();
        }
        return pieceWork;

    }

    @Override
    public List<PieceWork> getAllPieceWork() {
        List<PieceWork> pieceWorkList = new ArrayList<>();
        try{
            pieceWorkList = pieceWorkMapper.getAllPieceWork();
            return pieceWorkList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return pieceWorkList;
    }

    @Override
    public List<PieceWork> getPieceWorkByEmpNum(String employeeNumber) {
        List<PieceWork> pieceWorkList = new ArrayList<>();
        try{
            pieceWorkList = pieceWorkMapper.getPieceWorkByEmpNum(employeeNumber);
            return pieceWorkList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return pieceWorkList;
    }

    @Override
    public List<PieceWork> getPieceWorkByTime(Date from, Date to) {
        List<PieceWork> pieceWorkList = new ArrayList<>();
        try{
            pieceWorkList = pieceWorkMapper.getPieceWorkByTime(from,to);
            return pieceWorkList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return pieceWorkList;
    }

    @Override
    public List<PieceWork> getPieceWorkByEmpNumTime(String employeeNumber, Date from, Date to) {
        List<PieceWork> pieceWorkList = new ArrayList<>();
        try{
            pieceWorkList = pieceWorkMapper.getPieceWorkByEmpNumTime(employeeNumber, from, to);
            return pieceWorkList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return pieceWorkList;
    }

    @Override
    public List<PieceWork> getDetailPieceWork(Date from, Date to, String groupName, String employeeNumber) {
        List<PieceWork> pieceWorkList = new ArrayList<>();
        try{
            pieceWorkList = pieceWorkMapper.getDetailPieceWork(from, to, groupName, employeeNumber);
            return pieceWorkList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<PieceWorkResult> getPieceWorkToday() {
        List<PieceWorkResult> pieceWorkList = new ArrayList<>();
        try{
            pieceWorkList = pieceWorkMapper.getPieceWorkToday();
            return pieceWorkList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<PieceWorkResult> getPieceWorkThisMonth() {
        List<PieceWorkResult> pieceWorkList = new ArrayList<>();
        try{
            pieceWorkList = pieceWorkMapper.getPieceWorkThisMonth();
            return pieceWorkList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<PieceWork> getPieceWorkEmpToday(String employeeNumber) {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat s=new SimpleDateFormat("yyyy-MM-dd");
        String curDate = s.format(calendar.getTime());  //
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        String nextDate = s.format(calendar.getTime());  //
        List<PieceWork> pieceWorkList = new ArrayList<>();
        try{
            pieceWorkList = pieceWorkMapper.getPieceWorkEmpToday(employeeNumber, curDate, nextDate);
            return pieceWorkList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<PieceWork> getPieceWorkEmpThisMonth(String employeeNumber) {
        List<PieceWork> pieceWorkList = new ArrayList<>();
        try{
            pieceWorkList = pieceWorkMapper.getPieceWorkEmpThisMonth(employeeNumber);
            return pieceWorkList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Object> queryPieceWorkToday(String groupName, String employeeNumber) {
        List<Object> objectList = new ArrayList<>();
        try{
            objectList = pieceWorkMapper.queryPieceWorkToday(groupName, employeeNumber);
            return objectList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Object> queryPieceWorkThisMonth(String groupName, String employeeNumber) {
        List<Object> objectList = new ArrayList<>();
        try{
            objectList = pieceWorkMapper.queryPieceWorkThisMonth(groupName, employeeNumber);
            return objectList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<SalaryCount> queryPieceWork(Date from, Date to, String groupName, String employeeNumber) {
        List<SalaryCount> salaryCountList = new ArrayList<>();
        try{
            salaryCountList = pieceWorkMapper.queryPieceWork(from, to, groupName, employeeNumber);
            return salaryCountList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return salaryCountList;
    }

    @Override
    public List<PieceWork> getTodaySummary(String employeeNumber) {
        List<PieceWork> pieceWorkList = new ArrayList<>();
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat s=new SimpleDateFormat("yyyy-MM-dd");
        String curDate = s.format(calendar.getTime());  //
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        String nextDate = s.format(calendar.getTime());  //
        try{
            pieceWorkList = pieceWorkMapper.getTodaySummary(employeeNumber, curDate, nextDate);
            return pieceWorkList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return pieceWorkList;
    }

    @Override
    public List<PieceWork> getThisMonthSummary(String employeeNumber) {
        List<PieceWork> pieceWorkList = new ArrayList<>();
        try{
            pieceWorkList = pieceWorkMapper.getThisMonthSummary(employeeNumber);
            return pieceWorkList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return pieceWorkList;
    }

    @Override
    public List<PieceWork> getPieceWorkByOrderBedPackID(String orderName, Integer bedNumber, Integer packageNumber, Integer procedureNumber, Integer tailorQcodeID) {
        List<PieceWork> pieceWorkList = new ArrayList<>();
        try{
            pieceWorkList = pieceWorkMapper.getPieceWorkByOrderBedPackID(orderName, bedNumber, packageNumber, procedureNumber, tailorQcodeID);
            return pieceWorkList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return pieceWorkList;
    }

    @Override
    public List<PieceWork> getPieceWorkByOrderBedPack(String orderName, Integer bedNumber, Integer packageNumber, Integer procedureNumber) {
        List<PieceWork> pieceWorkList = new ArrayList<>();
        try{
            pieceWorkList = pieceWorkMapper.getPieceWorkByOrderBedPack(orderName, bedNumber, packageNumber, procedureNumber);
            return pieceWorkList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return pieceWorkList;
    }


    @Override
    public int addPieceWorkBatchNew(List<PieceWork> pieceWorkList) {
        try{
            if(pieceWorkList!=null && !pieceWorkList.isEmpty()){
                pieceWorkMapper.addPieceWorkBatchNew(pieceWorkList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public int addPieceWorkTotal(PieceWork pieceWork) {
        try{
            pieceWorkMapper.addPieceWorkTotal(pieceWork);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<MiniDetailQuery> getDetailProductionByEmp(Date from, Date to, String employeeNumber, String orderName, String colorName, String sizeName) {
        List<MiniDetailQuery> miniDetailQueryList = new ArrayList<>();
        try{
            miniDetailQueryList = pieceWorkMapper.getDetailProductionByEmp(from, to, employeeNumber, orderName, colorName, sizeName);
            return miniDetailQueryList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return miniDetailQueryList;
    }

    @Override
    public List<GeneralSalary> getMiniSalaryByTimeEmp(Date from, Date to, String employeeNumber) {
        List<GeneralSalary> generalSalaryList = new ArrayList<>();
        try{
            generalSalaryList = pieceWorkMapper.getMiniSalaryByTimeEmp(from, to, employeeNumber);
            return generalSalaryList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return generalSalaryList;

    }

    @Override
    public List<GeneralSalary> getGroupMiniSalaryByTime(Date from, Date to, String groupName) {
        List<GeneralSalary> generalSalaryList = new ArrayList<>();
        try{
            generalSalaryList = pieceWorkMapper.getGroupMiniSalaryByTime(from, to, groupName);
            return generalSalaryList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return generalSalaryList;
    }

    @Override
    public List<PieceWorkEmp> getProcedureInfoByOrderBedPackage(String orderName, Integer bedNumber, Integer packageNumber) {
        List<PieceWorkEmp> pieceWorkEmpList = new ArrayList<>();
        try{
            pieceWorkEmpList = pieceWorkMapper.getProcedureInfoByOrderBedPackage(orderName, bedNumber, packageNumber);
            return pieceWorkEmpList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return pieceWorkEmpList;
    }

    @Override
    public List<ManualInput> getAllManualInput() {
        List<ManualInput> manualInputList = new ArrayList<>();
        try{
            manualInputList = pieceWorkMapper.getAllManualInput();
            return manualInputList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return manualInputList;
    }

    @Override
    public List<Unload> getAllUnloadRecord() {
        List<Unload> unloadList = new ArrayList<>();
        try{
            unloadList = pieceWorkMapper.getAllUnloadRecord();
            return unloadList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return unloadList;
    }

    @Override
    public Integer getPieceCountByOrderColorSize(String orderName, Integer procedureNumber, String colorName, String sizeName) {
        Integer pieceCount = 0;
        try{
            pieceCount = pieceWorkMapper.getPieceCountByOrderColorSize(orderName, procedureNumber, colorName, sizeName);
            if (pieceCount == null){
                pieceCount = 0;
            }
            return pieceCount;
        }catch (Exception e){
            e.printStackTrace();
        }
        return pieceCount;
    }

    @Override
    public List<SalaryCount> getMiniProcedureProgress(Date from, Date to, Integer procedureFrom, Integer procedureTo, String orderName, String groupName) {
        List<SalaryCount> salaryCountList = new ArrayList<>();
        try{
            salaryCountList = pieceWorkMapper.getMiniProcedureProgress(from, to, procedureFrom, procedureTo, orderName, groupName);
            return salaryCountList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return salaryCountList;
    }

    @Override
    public List<CutLocation> getPieceWorkInfo(String orderName, Integer bedNumber, String colorName, String sizeName) {
        List<CutLocation> cutLocationList = new ArrayList<>();
        try{
            cutLocationList = pieceWorkMapper.getPieceWorkInfo(orderName, bedNumber, colorName, sizeName);
            return cutLocationList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return cutLocationList;
    }

    @Override
    public List<WeekAmount> getPieceWeekAmount() {
        List<WeekAmount> weekAmountList = new ArrayList<>();
        try{
            weekAmountList = pieceWorkMapper.getPieceWeekAmount();
            return weekAmountList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return weekAmountList;
    }

    @Override
    public List<WeekAmount> getFinishWeekAmount() {
        List<WeekAmount> weekAmountList = new ArrayList<>();
        try{
            weekAmountList = pieceWorkMapper.getFinishWeekAmount();
            return weekAmountList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return weekAmountList;
    }

    @Override
    public List<CompletionCount> getPieceWorkCompletion(Integer procedureNumber) {
        List<CompletionCount> completionCountList = new ArrayList<>();
        try{
            completionCountList = pieceWorkMapper.getPieceWorkCompletion(procedureNumber);
            return completionCountList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return completionCountList;
    }

    @Override
    public List<SalaryCount> getOrderMiniSalarySummary(String orderName,Date from,Date to) {
        List<SalaryCount> salaryCountList = new ArrayList<>();
        try{
            salaryCountList = pieceWorkMapper.getOrderMiniSalarySummary(orderName,from,to);
            return salaryCountList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return salaryCountList;
    }


    @Override
    public List<ProductionProgressDetail> getUnloadByOrderTime(String orderName, Date from, Date to) {
        List<ProductionProgressDetail> personProductionDetailList = new ArrayList<>();
        try{
            personProductionDetailList = pieceWorkMapper.getUnloadByOrderTime(orderName, from, to);
            return personProductionDetailList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return personProductionDetailList;
    }

    @Override
    public int updateManualInput(PieceWork pieceWork) {
        try{
            pieceWorkMapper.updateManualInput(pieceWork);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<ManualInput> getTodayManualInput() {
        List<ManualInput> manualInputList = new ArrayList<>();
        try{
            manualInputList = pieceWorkMapper.getTodayManualInput();
            return manualInputList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return manualInputList;
    }

    @Override
    public List<ManualInput> getOneMonthManualInput() {
        List<ManualInput> manualInputList = new ArrayList<>();
        try{
            manualInputList = pieceWorkMapper.getOneMonthManualInput();
            return manualInputList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return manualInputList;
    }

    @Override
    public List<ManualInput> getThreeMonthsManualInput() {
        List<ManualInput> manualInputList = new ArrayList<>();
        try{
            manualInputList = pieceWorkMapper.getThreeMonthsManualInput();
            return manualInputList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return manualInputList;
    }

    @Override
    public List<PieceWorkResult> searchPieceWorkManagement(String orderName, String colorName, String sizeName, Integer bedNumber, Integer packageNumber, Integer procedureNumber, Integer page, Integer limit) {
        List<PieceWorkResult> pieceWorkResultList = new ArrayList<>();
        try{
            pieceWorkResultList = pieceWorkMapper.searchPieceWorkManagement(orderName, colorName, sizeName, bedNumber, packageNumber, procedureNumber, page, limit);
            return pieceWorkResultList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return pieceWorkResultList;
    }

    @Override
    public Integer deletePieceWorkBatch(List<Integer> pieceWorkIDList) {
        try{
            if (pieceWorkIDList != null && !pieceWorkIDList.isEmpty()){
                pieceWorkMapper.deletePieceWorkBatch(pieceWorkIDList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<GeneralSalary> getMiniProcedureProgressNew(Date from, Date to, Integer procedureFrom, Integer procedureTo, String orderName, String groupName, String employeeNumber) {
        List<GeneralSalary> generalSalaryList = new ArrayList<>();
        try{
            generalSalaryList = pieceWorkMapper.getMiniProcedureProgressNew(from, to, procedureFrom, procedureTo, orderName, groupName, employeeNumber);
            return generalSalaryList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return generalSalaryList;
    }

    @Override
    public List<CutLocation> getMatchInfo(String orderName, Integer bedNumber, String colorName, String sizeName) {
        List<CutLocation> cutLocationList = new ArrayList<>();
        try{
            cutLocationList = pieceWorkMapper.getMatchInfo(orderName, bedNumber, colorName, sizeName);
            return cutLocationList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return cutLocationList;
    }

    @Override
    public List<GeneralSalary> getMiniSalaryDetail(Date from, Date to, String employeeNumber, String orderName) {
        List<GeneralSalary> generalSalaryList = new ArrayList<>();
        try{
            generalSalaryList = pieceWorkMapper.getMiniSalaryDetail(from, to, employeeNumber, orderName);
            return generalSalaryList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return generalSalaryList;
    }

    @Override
    public List<GeneralSalary> getMiniSalaryByOrderColorSizeEmp(Date from, Date to, String employeeNumber, String orderName, String colorName, String sizeName) {
        List<GeneralSalary> generalSalaryArrayList = new ArrayList<>();
        try{
            generalSalaryArrayList = pieceWorkMapper.getMiniSalaryByOrderColorSizeEmp(from, to, employeeNumber, orderName, colorName, sizeName);
            return generalSalaryArrayList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return generalSalaryArrayList;
    }

    @Override
    public List<GeneralSalary> getMiniProcedureProgressMultiGroup(Date from, Date to, Integer procedureFrom, Integer procedureTo, String orderName, List<String> groupList, String employeeNumber) {
        List<GeneralSalary> generalSalaryList = new ArrayList<>();
        try{
            generalSalaryList = pieceWorkMapper.getMiniProcedureProgressMultiGroup(from, to, procedureFrom, procedureTo, orderName, groupList, employeeNumber);
            return generalSalaryList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return generalSalaryList;
    }

    @Override
    public List<GeneralSalary> getDetailProductionByInfo(String from, String to, String employeeNumber, String orderName, String colorName, String sizeName) {
        List<GeneralSalary> generalSalaryList = new ArrayList<>();
        try{
            SimpleDateFormat s=new SimpleDateFormat("yyyy-MM-dd");
            Calendar calendar = Calendar.getInstance();
            Date fromDate = s.parse(to);
            calendar.setTime(fromDate);
            calendar.add(Calendar.DATE, 1);
            String nextDate = s.format(calendar.getTime());  //
            generalSalaryList = pieceWorkMapper.getDetailProductionByInfo(from, nextDate, employeeNumber, orderName, colorName, sizeName);
            return generalSalaryList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return generalSalaryList;
    }

    @Override
    public List<GeneralSalary> getDetailProductionOfEachLine(Date pieceDate, String employeeNumber, String orderName, Integer procedureNumber, String colorName, String sizeName) {
        List<GeneralSalary> generalSalaryList = new ArrayList<>();
        try{
            generalSalaryList = pieceWorkMapper.getDetailProductionOfEachLine(pieceDate, employeeNumber, orderName, procedureNumber, colorName, sizeName);
            return generalSalaryList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return generalSalaryList;
    }

    @Override
    public List<GeneralSalary> getDetailProductionSummaryByInfo(String from, String to, String employeeNumber, String orderName, String colorName, String sizeName) {
        List<GeneralSalary> generalSalaryList = new ArrayList<>();
        try{
            SimpleDateFormat s=new SimpleDateFormat("yyyy-MM-dd");
            Calendar calendar = Calendar.getInstance();
            Date fromDate = s.parse(to);
            calendar.setTime(fromDate);
            calendar.add(Calendar.DATE, 1);
            String nextDate = s.format(calendar.getTime());  //
            generalSalaryList = pieceWorkMapper.getDetailProductionSummaryByInfo(from, nextDate, employeeNumber, orderName, colorName, sizeName);
            return generalSalaryList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return generalSalaryList;
    }

    @Override
    public List<GeneralSalary> getDetailProductionDetailByInfo(String from, String to, String employeeNumber, String orderName, String colorName, String sizeName) {
        List<GeneralSalary> generalSalaryList = new ArrayList<>();
        try{
            SimpleDateFormat s=new SimpleDateFormat("yyyy-MM-dd");
            Calendar calendar = Calendar.getInstance();
            Date fromDate = s.parse(to);
            calendar.setTime(fromDate);
            calendar.add(Calendar.DATE, 1);
            String nextDate = s.format(calendar.getTime());  //
            generalSalaryList = pieceWorkMapper.getDetailProductionDetailByInfo(from, nextDate, employeeNumber, orderName, colorName, sizeName);
            return generalSalaryList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return generalSalaryList;
    }

    @Override
    public Integer updateProcedureNameByOrderProcedure(String orderName, Integer procedureNumber, String procedureName) {
        try{
            pieceWorkMapper.updateProcedureNameByOrderProcedure(orderName, procedureNumber, procedureName);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<GeneralSalary> getPieceInfoGroupByOrderColor(String orderName, Integer procedureNumber) {
        List<GeneralSalary> generalSalaryList = new ArrayList<>();
        try{
            generalSalaryList = pieceWorkMapper.getPieceInfoGroupByOrderColor(orderName, procedureNumber);
            return generalSalaryList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return generalSalaryList;
    }

    @Override
    public List<PieceWork> getPieceWorkByOrderNameProcedureNumber(String orderName, Integer procedureNumber, String colorName, String sizeName) {
        List<PieceWork> pieceWorkList = new ArrayList<>();
        try{
            pieceWorkList = pieceWorkMapper.getPieceWorkByOrderNameProcedureNumber(orderName, procedureNumber, colorName, sizeName);
            return pieceWorkList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return pieceWorkList;
    }

    @Override
    public Integer updateGroupNameByEmployee(Date from, Date to, String employeeNumber, String groupName) {
        try{
            pieceWorkMapper.updateGroupNameByEmployee(from, to, employeeNumber, groupName);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<GeneralSalary> getGroupPieceWorkSummary(String from, String to, String orderName, String groupName) {
        List<GeneralSalary> generalSalaryList = new ArrayList<>();
        try{
            generalSalaryList = pieceWorkMapper.getGroupPieceWorkSummary(from, to, orderName, groupName);
            return generalSalaryList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return generalSalaryList;
    }

    @Override
    public List<PieceWork> getPieceWorkByOrderNameBedNumberPackage(String orderName, Integer bedNumber, Integer packageNumber) {
        List<PieceWork> pieceWorkList = new ArrayList<>();
        try{
            pieceWorkList = pieceWorkMapper.getPieceWorkByOrderNameBedNumberPackage(orderName, bedNumber, packageNumber);
            return pieceWorkList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return pieceWorkList;
    }

    @Override
    public Integer getPieceCountOfToday(String employeeNumber) {
        Integer pieceCount = 0;
        try{
            if (pieceWorkMapper.getPieceCountOfToday(employeeNumber) != null){
                pieceCount = pieceWorkMapper.getPieceCountOfToday(employeeNumber);
            }
            return pieceCount;
        }catch (Exception e){
            e.printStackTrace();
        }
        return pieceCount;
    }

    @Override
    public List<GeneralSalary> getPackageDetailByInfo(Date from, Date to, String employeeNumber) {
        List<GeneralSalary> generalSalaryList = new ArrayList<>();
        try{
            generalSalaryList = pieceWorkMapper.getPackageDetailByInfo(from, to, employeeNumber);
            return generalSalaryList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return generalSalaryList;
    }

    @Override
    public List<CutQueryLeak> getProductionByProcedureColorSize(String orderName, Integer procedureNumber, String colorName, String sizeName) {
        return pieceWorkMapper.getProductionByProcedureColorSize(orderName, procedureNumber, colorName, sizeName);
    }

    @Override
    public Integer addPieceWorkTotalBatch(List<PieceWork> pieceWorkList) {
        try{
            if (pieceWorkList != null && !pieceWorkList.isEmpty()){
                pieceWorkMapper.addPieceWorkTotalBatch(pieceWorkList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer updatePieceWorkLayerCountFinance(Integer pieceWorkID, Integer layerCount) {
        try{
            pieceWorkMapper.updatePieceWorkLayerCountFinance(pieceWorkID, layerCount);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<ManualInput> getByInfo(Map<String, Object> map) {
        return pieceWorkMapper.getByInfo(map);
    }

}
