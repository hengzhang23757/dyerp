package com.example.erp01.service.impl;

import com.example.erp01.mapper.CheckDetailMapper;
import com.example.erp01.model.CheckDetail;
import com.example.erp01.service.CheckDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class CheckDetailServiceImpl implements CheckDetailService {

    @Autowired
    private CheckDetailMapper checkDetailMapper;

    @Override
    public List<CheckDetail> getCheckDetailByInfo(Date fromDate, Date toDate, String employeeNumber) {
        List<CheckDetail> checkDetailList = new ArrayList<>();
        try{
            checkDetailList = checkDetailMapper.getCheckDetailByInfo(fromDate, toDate, employeeNumber);
            return checkDetailList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return checkDetailList;
    }

    @Override
    public List<CheckDetail> getYesterdayCheckDetail() {
        List<CheckDetail> checkDetailList = new ArrayList<>();
        try{
            checkDetailList = checkDetailMapper.getYesterdayCheckDetail();
            return checkDetailList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return checkDetailList;
    }

    @Override
    public Integer updateCheckDetail(CheckDetail checkDetail) {
        try{
            checkDetailMapper.updateCheckDetail(checkDetail);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }
}
