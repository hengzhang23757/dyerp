package com.example.erp01.service.impl;

import com.example.erp01.mapper.ClothesPriceMapper;
import com.example.erp01.model.ClothesPrice;
import com.example.erp01.service.ClothesPriceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class ClothesPriceServiceImpl implements ClothesPriceService {

    @Autowired
    private ClothesPriceMapper clothesPriceMapper;

    @Override
    public Integer addClothesPriceBatch(List<ClothesPrice> clothesPriceList) {
        try{
            if (clothesPriceList != null && !clothesPriceList.isEmpty()){
                clothesPriceMapper.addClothesPriceBatch(clothesPriceList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer deleteClothesPriceBatch(List<Integer> idList) {
        try{
            if (idList != null && !idList.isEmpty()){
                clothesPriceMapper.deleteClothesPriceBatch(idList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer updateClothesPrice(ClothesPrice clothesPrice) {
        try{
            clothesPriceMapper.updateClothesPrice(clothesPrice);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<ClothesPrice> getClothesPriceByInfo(String orderName, String colorName) {
        return clothesPriceMapper.getClothesPriceByInfo(orderName, colorName);
    }

    @Override
    public Integer updateClothesPriceBatch(List<ClothesPrice> clothesPriceList) {
        try{
            if (clothesPriceList != null && !clothesPriceList.isEmpty()){
                clothesPriceMapper.updateClothesPriceBatch(clothesPriceList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }
}
