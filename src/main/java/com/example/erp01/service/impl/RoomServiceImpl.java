package com.example.erp01.service.impl;

import com.example.erp01.mapper.RoomMapper;
import com.example.erp01.model.Room;
import com.example.erp01.model.RoomState;
import com.example.erp01.service.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RoomServiceImpl implements RoomService {

    @Autowired
    private RoomMapper roomMapper;

    @Override
    public int addRoomBatch(List<Room> roomList) {
        try{
            if (roomList != null && !roomList.isEmpty()){
                roomMapper.addRoomBatch(roomList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public int deleteRoom(Integer roomID) {
        try{
            roomMapper.deleteRoom(roomID);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public int updateRoom(Room room) {
        try{
            roomMapper.updateRoom(room);
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public List<Room> getAllRoom() {
        List<Room> roomList = new ArrayList<>();
        try{
            roomList = roomMapper.getAllRoom();
            return roomList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return roomList;
    }

    @Override
    public List<String> getAllFloor() {
        List<String> floorList = new ArrayList<>();
        try{
            floorList = roomMapper.getAllFloor();
            return floorList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return floorList;
    }

    @Override
    public List<RoomState> getAllRoomInfoByFloor(String floor) {
        List<RoomState> roomStateList = new ArrayList<>();
        try{
            roomStateList = roomMapper.getAllRoomInfoByFloor(floor);
            return roomStateList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return roomStateList;
    }

    @Override
    public Integer getCapacityOfRoom(String roomNumber) {
        Integer capacity = 0;
        try{
            if (roomMapper.getCapacityOfRoom(roomNumber) != null){
                capacity = roomMapper.getCapacityOfRoom(roomNumber);
            }
            return capacity;
        }catch (Exception e){
            e.printStackTrace();
        }
        return capacity;
    }

    @Override
    public Room getRoomByID(Integer roomID) {
        Room room = null;
        try{
            room = roomMapper.getRoomByID(roomID);
            return room;
        }catch (Exception e){
            e.printStackTrace();
        }
        return room;
    }

}
