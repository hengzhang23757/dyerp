package com.example.erp01.service.impl;

import com.example.erp01.mapper.FabricAdjustMapper;
import com.example.erp01.model.*;
import com.example.erp01.service.FabricAdjustService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class FabricAdjustServiceImpl implements FabricAdjustService {

    @Autowired
    private FabricAdjustMapper fabricAdjustMapper;

    @Override
    public List<ManufactureFabric> getAllManufactureFabric() {
        return fabricAdjustMapper.getAllManufactureFabric();
    }

    @Override
    public List<FabricDetail> getAllFabricDetail() {
        return fabricAdjustMapper.getAllFabricDetail();
    }

    @Override
    public List<FabricStorage> getAllFabricStorage() {
        return fabricAdjustMapper.getAllFabricStorage();
    }

    @Override
    public List<FabricOutRecord> getAllFabricOutRecord() {
        return fabricAdjustMapper.getAllFabricOutRecord();
    }

    @Override
    public List<ManufactureAccessory> getAllManufactureAccessory() {
        return fabricAdjustMapper.getAllManufactureAccessory();
    }

    @Override
    public List<AccessoryInStore> getAllAccessoryInStore() {
        return fabricAdjustMapper.getAllAccessoryInStore();
    }

    @Override
    public List<AccessoryStorage> getAllAccessoryStorage() {
        return fabricAdjustMapper.getAllAccessoryStorage();
    }

    @Override
    public List<AccessoryOutRecord> getAllAccessoryOutRecord() {
        return fabricAdjustMapper.getAllAccessoryOutRecord();
    }

    @Override
    public Integer updateFabricDetailIdBatch(List<FabricDetail> fabricDetailList) {
        try{
            if (fabricDetailList != null && !fabricDetailList.isEmpty()){
                fabricAdjustMapper.updateFabricDetailIdBatch(fabricDetailList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer updateFabricStorageIdBatch(List<FabricStorage> fabricStorageList) {
        try{
            if (fabricStorageList != null && !fabricStorageList.isEmpty()){
                fabricAdjustMapper.updateFabricStorageIdBatch(fabricStorageList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer updateFabricOutRecordIdBatch(List<FabricOutRecord> fabricOutRecordList) {
        try{
            if (fabricOutRecordList != null && !fabricOutRecordList.isEmpty()){
                fabricAdjustMapper.updateFabricOutRecordIdBatch(fabricOutRecordList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer updateAccessoryInStoreIdBatch(List<AccessoryInStore> accessoryInStoreList) {
        try{
            if (accessoryInStoreList != null && !accessoryInStoreList.isEmpty()){
                fabricAdjustMapper.updateAccessoryInStoreIdBatch(accessoryInStoreList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer updateAccessoryStorageIdBatch(List<AccessoryStorage> accessoryStorageList) {
        try{
            if (accessoryStorageList != null && !accessoryStorageList.isEmpty()){
                fabricAdjustMapper.updateAccessoryStorageIdBatch(accessoryStorageList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer updateAccessoryOutRecordIdBatch(List<AccessoryOutRecord> accessoryOutRecordList) {
        try{
            if (accessoryOutRecordList != null && !accessoryOutRecordList.isEmpty()){
                fabricAdjustMapper.updateAccessoryOutRecordIdBatch(accessoryOutRecordList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer updateAccessoryAccountInStoreBatch(List<AccessoryInStore> accessoryInStoreList) {
        try{
            if (accessoryInStoreList != null && !accessoryInStoreList.isEmpty()){
                fabricAdjustMapper.updateAccessoryAccountInStoreBatch(accessoryInStoreList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public Integer updateAccessoryInStorePriceBatch(List<AccessoryInStore> accessoryInStoreList) {
        try{
            if (accessoryInStoreList != null && !accessoryInStoreList.isEmpty()){
                fabricAdjustMapper.updateAccessoryInStorePriceBatch(accessoryInStoreList);
            }
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }
}
