package com.example.erp01.service.impl;

import com.example.erp01.mapper.OtherStoreMapper;
import com.example.erp01.model.OtherStore;
import com.example.erp01.service.OtherStoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OtherStoreServiceImpl implements OtherStoreService {

    @Autowired
    private OtherStoreMapper otherStoreMapper;

    @Override
    public OtherStore getOtherStoreByLocation(String otherStoreLocation) {
        try{
            return otherStoreMapper.getOtherStoreByLocation(otherStoreLocation);
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
}
