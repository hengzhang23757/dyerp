package com.example.erp01.service;

import com.example.erp01.model.FabricStorage;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface FabricStorageService {

    Integer addFabricStorageBatch(List<FabricStorage> fabricStorageList);

    Integer addFabricStorage(FabricStorage fabricStorage);

    Integer deleteFabricStorage(Integer fabricStorageID);

    List<FabricStorage> getAllFabricStorage();

    FabricStorage getFabricStorageByOrderLocationJar(String orderName, String colorName, String fabricName, String fabricColor, String jarName, String location, Integer fabricID);

    List<FabricStorage> getFabricStorageByOrder(String orderName);

    Integer updateFabricStorage(FabricStorage fabricStorage);

    Integer getFabricBatchNumberByOrderFabricJarLocation(String orderName, String fabricName, String fabricColor, String jarName, String location);

    FabricStorage getFabricStorageByOrderColorLocationJar(String orderName, String fabricName, String colorName, String fabricColor, String jarName, String location, Integer fabricID);

    FabricStorage getFabricStorageByID(Integer fabricStorageID);

    List<FabricStorage> getFabricStorageByInfo(String orderName, String fabricName, String colorName, String fabricColor, Integer fabricID);

    Integer getFabricBatchNumberByOrder(String orderName, List<String> colorNameList);

    List<FabricStorage> getFabricStorageByOrderList(List<String> orderNameList);

    List<FabricStorage> getFabricStorageByIdList(List<Integer> fabricStorageIDList);

    Integer deleteFabricStorageBatch(List<Integer> fabricStorageIDList);

    Integer deleteFabricStorageByOrderNameJar(String orderName, String jarName);

}
