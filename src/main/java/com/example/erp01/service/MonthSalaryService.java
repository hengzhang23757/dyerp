package com.example.erp01.service;

import com.example.erp01.model.MonthSalary;

import java.util.List;

public interface MonthSalaryService {

    Integer addMonthSalaryBatch(List<MonthSalary> monthSalaryList);

    List<MonthSalary> getMonthSalaryByMonthString(String monthString);

    Integer deleteMonthSalaryByMonthString(String monthString);

}
