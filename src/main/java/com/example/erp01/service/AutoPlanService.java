package com.example.erp01.service;

import com.example.erp01.model.EmpSkills;
import com.example.erp01.model.Employee;
import com.example.erp01.model.ProcedureTemplate;
import com.example.erp01.model.ScheduleRecord;

import java.util.List;
import java.util.Map;

public interface AutoPlanService {
    double[][] generateSkillMatrix(List<String> empList, List<EmpSkills> skills, List<ProcedureTemplate> standardSkills);

    List<ScheduleRecord> generateEmpPlan(double[] xes, List<String> empList, List<ProcedureTemplate> procedureList, List<EmpSkills> skills, List<Employee> employeeList);

    Map<String, Object> calculateBalanceAndEchart(double[] xes, double[][] matrixA, double dworkHour);
}
