package com.example.erp01.service;

import com.example.erp01.model.MatchPieceWork;

import java.util.List;

public interface MatchPieceWorkService {

    Integer addMatchPieceWorkBatch(List<MatchPieceWork> matchPieceWorkList);

    List<MatchPieceWork> getMatchPieceWorkSummary();

    Integer deleteMatchPieceWorkByInfo(String orderName, String colorName, String sizeName);

    Integer deleteMatchPieceWorkByIdList(List<Integer> pieceWorkIdList);

    List<MatchPieceWork> getMatchPieceWorkByInfo(String orderName, String colorName, String sizeName);

    Integer deleteMatchPieceWorkById(Integer pieceWorkID);

    Integer addMatchPieceWork(MatchPieceWork matchPieceWork);

}
