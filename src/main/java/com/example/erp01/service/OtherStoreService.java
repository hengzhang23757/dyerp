package com.example.erp01.service;

import com.example.erp01.model.OtherStore;

//@Service
public interface OtherStoreService {

    OtherStore getOtherStoreByLocation(String otherStoreLocation);

}
