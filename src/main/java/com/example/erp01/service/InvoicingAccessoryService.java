package com.example.erp01.service;

import com.example.erp01.model.AccessoryInStore;
import com.example.erp01.model.AccessoryOutRecord;
import com.example.erp01.model.AccessoryStorage;

import java.util.List;
import java.util.Map;

public interface InvoicingAccessoryService {

    List<Integer> getAccessoryOperateIdList(Map<String, Object> map);

    List<AccessoryInStore> getAccessoryInStoreByIdList(List<Integer> accessoryIDList);

    List<AccessoryStorage> getAccessoryStorageByIdList(List<Integer> accessoryIDList);

    List<AccessoryOutRecord> getAccessoryOutRecordByIdList(List<Integer> accessoryIDList);

    List<AccessoryInStore> getAccessoryInStoreByMap(Map<String, Object> map);

    List<AccessoryStorage> getAccessoryStorageByMap(Map<String, Object> map);

    List<AccessoryOutRecord> getAccessoryOutRecordByMap(Map<String, Object> map);

}
