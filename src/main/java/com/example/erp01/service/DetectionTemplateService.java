package com.example.erp01.service;

import com.example.erp01.model.DetectionTemplate;

import java.util.List;
import java.util.Map;

public interface DetectionTemplateService {

    Integer addDetectionTemplate(DetectionTemplate detectionTemplate);

    Integer updateDetectionTemplate(DetectionTemplate detectionTemplate);

    Integer deleteDetectionTemplate(Integer id);

    List<DetectionTemplate> getDetectionTemplateByInfo(Map<String, Object> map);

    DetectionTemplate getDetectionTemplateById(Integer id);

    List<String> getDetectionNameByDetectionType(String detectionType);

}
