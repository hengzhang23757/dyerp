package com.example.erp01.service;

import com.example.erp01.model.GeneralSalary;

import java.util.List;

public interface CostAccountService {

    Float getFabricCostByOrder(String orderName);

    Float getFabricDeductionCostByOrder(String orderName);

    Float getAccessoryCostByOrder(String orderName);

    List<GeneralSalary> getPieceWorkSummaryByOrder(String orderName);

    List<GeneralSalary> getHourEmpSummaryByOrder(String orderName);

    Float getFabricOrderCost(String orderName);

    Float getAccessoryOrderCost(String orderName);

    Float getFabricAddFee(String orderName);

    Float getAccessoryAddFee(String orderName);

    Float getFabricCustomerCost(String orderName);

    Float getAccessoryCustomerCost(String orderName);

}
