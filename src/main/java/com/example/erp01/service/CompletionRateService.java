package com.example.erp01.service;

import com.example.erp01.model.CompletionRate;

import java.util.Date;
import java.util.List;

public interface CompletionRateService {

    List<CompletionRate> getEmpCountEachDay(Date fromDate, Date toDate, String orderName, String groupName);

    CompletionRate getOrderBeginDate(String orderName, String groupName);

    Integer getFinishCount(String orderName, String groupName);

    Integer getTodayActualCount(String orderName, String groupName, Date searchDate);

    Integer getFinishCountByTime(String orderName, String groupName, Date searchDate);
}
