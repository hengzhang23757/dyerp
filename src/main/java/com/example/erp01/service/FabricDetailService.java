package com.example.erp01.service;

import com.example.erp01.model.FabricDetail;
import com.example.erp01.model.LooseFabric;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface FabricDetailService {

    int addFabricDetailBatch(List<FabricDetail> fabricDetailList);

    int addFabricDetail(FabricDetail fabricDetail);

    int deleteFabricDetailByID(Integer fabricDetailID);

    int updateFabricDetail(FabricDetail fabricDetail);

    List<String> getJarNameByOrderFabricColor(String orderName, String fabricName, String fabricColor);

    Integer getFabricBatchNumberByOrderFabricJar(String orderName, String colorName, String fabricName, String fabricColor, String jarName);

    List<FabricDetail> getTodayFabricDetail();

    List<FabricDetail> getOneMonthFabricDetail();

    List<FabricDetail> getOneYearFabricDetail();

    List<String> getFabricLocationHint(String orderName, String colorName, String fabricName, String fabricColor, String jarName);

    List<String> getJarNameByOrder(String orderName);

    List<LooseFabric> getFabricBatchInfoOfEachJar(String orderName, String fabricName, String fabricColor);

    List<FabricDetail> getFabricDetailByInfo(String orderName, String fabricName, String colorName, String fabricColor, Integer fabricID);

    List<String> getColorNamesByOrder(String orderName);

    List<String> getFabricNamesByOrderColor(String orderName, String colorName);

    List<String> getFabricColorsByOrderColorFabric(String orderName, String colorName, String fabricName);

    List<LooseFabric> getJarInfoByOrderColorFabricColor(String orderName, String colorName, String fabricName, String fabricColor);

    String getFabricUnitByInfo(String orderName, String colorName, String fabricName, String fabricColor);

    FabricDetail getTransFabricDetailByInfo(String orderName, String colorName, String fabricName, String fabricColor, String jarName, String location);

    List<FabricDetail> getFabricDetailByOrder(String orderName);

    FabricDetail getFabricDetailById(Integer fabricDetailID);

    Integer getFabricBatchNumberByInfo(String orderName, List<String> colorNameList);

    List<FabricDetail> getFabricDetailByTime(String from, String to);

    List<FabricDetail> getFabricDetailByFabricIDList(List<Integer> fabricIDList);

    FabricDetail getFabricDetailByLocationJarFabricID(String location, String jarName, Integer fabricID);

    Float getHistoryFabricReturnWeight(Integer fabricID);

    List<FabricDetail> getFabricDetailByOperateType(String operateType);

    List<FabricDetail> getFabricDetailSummaryByFabricIdList(List<Integer> fabricIDList);

    Integer addFabricDetailTmpBatch(List<FabricDetail> fabricDetailList);

    List<FabricDetail> getFabricDetailTmpByFabricId(Integer fabricID, String orderName, String colorName);

    Integer deleteFabricDetailTmpById(Integer fabricDetailID);

    List<FabricDetail> getAllTmpFabricDetail(String orderName);

    Integer updateTmpFabricDetail(FabricDetail fabricDetail);

    FabricDetail getTmpFabricById(Integer fabricDetailID);

    Integer deleteFabricDetailByOrderJar(String orderName, String jarName);

    List<Integer> getFabricIdListByAddFee(@Param("fabricDetailIDList") List<Integer> fabricDetailIDList);

}
