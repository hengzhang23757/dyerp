package com.example.erp01.service;

import com.example.erp01.model.AccessoryStorage;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AccessoryStorageService {

    int addAccessoryStorageBatch(List<AccessoryStorage> accessoryStorageList);

    int deleteAccessoryByID(Integer id);

    List<AccessoryStorage> getAccessoryStorageByOrder(String orderName);

    List<AccessoryStorage> getAllAccessoryStorage();

    AccessoryStorage getAccessoryById(Integer id);

    List<AccessoryStorage> getAccessoryStorageByInfo(String orderName, String accessoryName, String specification, String accessoryColor, String colorName, String sizeName, Integer accessoryID);

    int updateAccessoryStorage(AccessoryStorage accessoryStorage);

    int updateAccessoryStorageBatch(List<AccessoryStorage> accessoryStorageList);

    int deleteAccessoryStorageBatch(List<Integer> idList);

    List<AccessoryStorage> getAccessoryStorageByColorSizeList(String orderName, List<String> colorList, List<String> sizeList);

    List<AccessoryStorage> getAccessoryStorageByAccessoryIdList(List<Integer> accessoryIdList);

    List<AccessoryStorage> getBjStorageByInfo(String orderName);
}
