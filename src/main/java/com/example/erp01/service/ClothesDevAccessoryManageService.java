package com.example.erp01.service;

import com.example.erp01.model.ClothesDevAccessoryManage;

import java.util.List;
import java.util.Map;

public interface ClothesDevAccessoryManageService {

    Integer addClothesDevAccessoryManage(ClothesDevAccessoryManage clothesDevAccessoryManage);

    List<ClothesDevAccessoryManage> getClothesDevAccessoryManageByMap(Map<String, Object> map);

    Integer updateClothesDevAccessoryManage(ClothesDevAccessoryManage clothesDevAccessoryManage);

    Integer deleteClothesDevAccessory(Integer id);

    List<String> getClothsDevAccessoryNameHint(String keyWord);

    List<String> getClothsDevAccessoryNumberHint(String keyWord);

    List<ClothesDevAccessoryManage> getClothesDevAccessoryManageHint(String subAccessoryName, Integer page, Integer limit);

    List<ClothesDevAccessoryManage> getClothesDevAccessoryManageHintByNumber(String subAccessoryNumber, Integer page, Integer limit);

    Integer deleteClothesDevAccessoryRecordByAccessoryId(Integer accessoryID);

    ClothesDevAccessoryManage getClothesDevAccessoryManageById(Integer id);

    Integer updateClothesDevAccessoryManageBatch(List<ClothesDevAccessoryManage> clothesDevAccessoryManageList);

    Integer deleteClothesDevAccessoryManageBatch(List<Integer> idList);

    String getMaxClothesDevAccessoryOrderByPreFix(String preFix);

    List<ClothesDevAccessoryManage> getClothesDevAccessoryManageRecordByInfo(Map<String, Object> map);

    Integer quitClothesDevAccessoryManageCheck(List<Integer> idList);

    List<ClothesDevAccessoryManage> getClothesDevAccessoryManageByAccessoryNumberList(List<String> accessoryNumberList);

    Integer updateClothesDevAccessoryManageOnlyCount(List<ClothesDevAccessoryManage> clothesDevAccessoryManageList);

    Integer addClothesDevAccessoryManageBatch(List<ClothesDevAccessoryManage> clothesDevAccessoryManageList);
}
