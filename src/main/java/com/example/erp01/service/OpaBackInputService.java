package com.example.erp01.service;

import com.example.erp01.model.OpaBackInput;

import java.util.List;

public interface OpaBackInputService {

    int addOpaBackInput(OpaBackInput opaBackInput);

    List<OpaBackInput> getAllOpaBackInput();

    int deleteOpaBackInput(Integer opaBackInputID);

    int addOpaBackInputBatch(List<OpaBackInput> opaBackInputList);

    int updateOpaBackInput(OpaBackInput opaBackInput);

    List<OpaBackInput> getTodayOpaBackInput();

    List<OpaBackInput> getOneMonthOpaBackInput();

    List<OpaBackInput> getThreeMonthOpaBackInput();

    List<OpaBackInput> getOpaBackInputByOrder(String orderName);

    List<OpaBackInput> getBedOpaBackInputByOrder(String orderName);

}
