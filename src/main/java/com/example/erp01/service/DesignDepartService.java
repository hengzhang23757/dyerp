package com.example.erp01.service;

import com.example.erp01.model.DesignDepart;

import java.util.List;

public interface DesignDepartService {

    Integer addDesignDepart(DesignDepart designDepart);

    Integer deleteColorManage(Integer id);

    List<DesignDepart> getAllDesignDepart();

    Integer updateDesignDepart(DesignDepart designDepart);

}
