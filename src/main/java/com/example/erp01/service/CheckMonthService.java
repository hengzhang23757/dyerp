package com.example.erp01.service;

import com.example.erp01.model.CheckMonth;

import java.util.List;
import java.util.Map;

public interface CheckMonthService {

    Integer addCheckMonth(CheckMonth checkMonth);

    Integer deleteCheckMonth(Integer id);

    Integer updateCheckMonth(CheckMonth checkMonth);

    List<CheckMonth> getAllCheckMonth();

    CheckMonth getCheckMonthByInfo(Map<String, Object> map);

}
