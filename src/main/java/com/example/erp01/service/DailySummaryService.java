package com.example.erp01.service;

import com.example.erp01.model.DailySummary;
import com.example.erp01.model.OrderProcedure;
import com.example.erp01.model.PieceWork;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

public interface DailySummaryService {

    Integer addDailySummaryBatch(List<DailySummary> dailySummaryList);

    List<DailySummary> getDailySummaryOneMonth();

    List<String> getDistinctEmployeeNumberHang(Date from, Date to);

    List<String> getDistinctOrderNameHang(Date from, Date to);

    List<OrderProcedure> getDistinctOrderProcedureHang(Date from, Date to);

    Integer updateHangEmployeeInfo(String employeeNumber, String groupName);

    Integer updateHangOrderInfo(String orderName, String clothesVersionNumber);

    Integer updateHangProcedureInfo(String orderName, Integer procedureNumber, String procedureName);

    List<String> getDistinctEmployeeNumberMini(Date from, Date to);

    List<String> getDistinctOrderNameMini(Date from, Date to);

    List<OrderProcedure> getDistinctOrderProcedureMini(Date from, Date to);

    Integer updateMiniEmployeeInfo(String employeeNumber, String employeeName, String groupName);

    Integer updateMiniOrderInfo(String orderName, String clothesVersionNumber);

    Integer updateMiniProcedureInfo(String orderName, Integer procedureNumber, String procedureName);

    List<PieceWork> getPieceWorkFromHang(Date from, Date to);

    Integer copyPieceWorkFromHangToMini(List<PieceWork> pieceWorkList);

    Integer getMaxNidFromPieceWork();

    List<PieceWork> getPieceWorkFromHangByNid(Integer nid);
}
