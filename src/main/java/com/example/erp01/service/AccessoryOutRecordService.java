package com.example.erp01.service;

import com.example.erp01.model.AccessoryOutRecord;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AccessoryOutRecordService {

    Integer addAccessoryOutRecord(AccessoryOutRecord accessoryOutRecord);

    Integer addAccessoryOutRecordBatch(List<AccessoryOutRecord> accessoryOutRecordList);

    List<AccessoryOutRecord> getAccessoryOutRecordByOrder(String orderName);

    List<AccessoryOutRecord> getAccessoryOutRecordByInfo(String orderName, String accessoryName, String specification, String accessoryColor, String colorName, String sizeName, Integer accessoryID);

    List<AccessoryOutRecord> getTodayAccessoryOutRecord();

    List<AccessoryOutRecord> getAccessoryOutRecordByTime(String from, String to);

    List<AccessoryOutRecord> getAccessoryOutRecordByOrderNameList(List<String> orderNameList);

    List<AccessoryOutRecord> getBjOutRecordByInfo(String orderName);

}
