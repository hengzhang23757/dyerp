package com.example.erp01.service;

public interface MaxFabricQcodeIDService {

   Integer getMaxFabricQcodeId();

   Integer updateMaxFabricQcodeId(int qCodeId);

}
