package com.example.erp01.service;

import com.example.erp01.model.EmpRoom;
import com.example.erp01.model.Room;
import com.example.erp01.model.RoomState;

import java.util.List;

public interface EmpRoomService {

    int addEmpRoom(EmpRoom empRoom);

    int deleteEmpRoom(Integer empRoomID);

    int updateEmpRoom(EmpRoom empRoom);

    List<EmpRoom> getAllEmpRoom();

    List<RoomState> getRoomStateOfFloor(String floor);

    List<String> getAllBedNumberOfRoom(String roomNumber);

    List<EmpRoom> getEmpRoomByRoom(String roomNumber);

}
