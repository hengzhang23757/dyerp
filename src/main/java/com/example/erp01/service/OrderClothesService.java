package com.example.erp01.service;

import com.example.erp01.model.Completion;
import com.example.erp01.model.CutQueryLeak;
import com.example.erp01.model.OrderClothes;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface OrderClothesService {

    int addOrderClothes(List<OrderClothes> orderClothesList);

    List<OrderClothes> getAllOrderClothes();

    List<OrderClothes> getOrderSummary();

    List<OrderClothes> getOrderByName(String orderName);

    int deleteOrderByName(String orderName);

    //订单输入下拉提示
    List<String> getOrderHint(String subOrderName);

    //颜色下拉提示
    List<String> getColorHint(String orderName);

    //输入订单自动带出顾客名
    String getCustomerNameByOrderName(String orderName);

    List<CutQueryLeak> getOrderInfoByName(String orderName);

    String getVersionNumberByOrderName(String orderName);

    Integer getOrderTotalCount(String orderName);

    List<String> getOrderSizeNamesByOrder(String orderName);

    String getDescriptionByOrder(String orderName);

    List<String> getVersionHint(String subVersion);

    List<String> getOrderByVersion(String clothesVersionNumber);

    List<Completion> getOrderCompletion();

    String getSeasonByOrder(String orderName);

    List<OrderClothes> getOneMonthOrderClothes();

    List<OrderClothes> getThreeMonthsOrderClothes();

    List<OrderClothes> getOneMonthOrderSummary();

    List<OrderClothes> getThreeMonthsOrderSummary();

    Integer getColorOrderCount(String orderName, String colorName);

    List<String> getAllCustomerName();

    List<OrderClothes> getOrderClothesByOrder(List<String> orderNameList, String customerName);

    List<String> getOrderColorNamesByOrder(String orderName);

    Integer updateOrderClothes(List<OrderClothes> orderClothesList);

    List<OrderClothes> getOneYearOrderSummary(String orderName);

    List<OrderClothes> getOrderSummaryByColor(String orderName);

    List<OrderClothes> getOrderListSummaryByColor(List<String> orderNameList);

    List<OrderClothes> getOrderClothesByOrderName(String orderName);

    Integer updateOrderClothesByID(OrderClothes orderClothes);

    Integer deleteOrderClothesByID(Integer orderClothesID);

    Integer getOrderCountByColorSizeList(String orderName, List<String> colorList, List<String> sizeList);

    List<OrderClothes> getLastSeasonSummary();

    List<OrderClothes> getOrderAndVersionByOrder(String subOrderName);

    List<OrderClothes> getOrderAndVersionByVersionPage(String subVersion,int page,int limit);

    List<OrderClothes> getOrderAndVersionByVersion(String subVersion);

    Integer getCountOrderAndVersionByVersion(String subVersion);

    Integer updateOrderClothesByOrder(String orderName, String styleDescription, String season, String customerName, String userName);

    List<OrderClothes> getOrderClothesSummaryByMap(Map<String, Object> map);

    List<OrderClothes> getOrderCountByOrderNameList(List<String> orderNameList);

    Integer deleteOrderClothesBatch(List<Integer> orderClothesIDList);

    Integer updateOrderStateByOrder(String orderName, Integer orderState);

    List<OrderClothes> getOrderClothesCountGroupByColor(String orderName);

    List<OrderClothes> getOrderClothesCountGroupBySize(String orderName);

    void changeOrderName(String orderName, String toOrderName);

    List<OrderClothes> getOrderAndVersionHint(String keyword);

}
