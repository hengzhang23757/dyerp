package com.example.erp01.mapper;

import com.example.erp01.model.StyleManage;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface StyleManageMapper {

    Integer addStyleManage(@Param("styleManage") StyleManage styleManage);

    Integer deleteStyleManage(@Param("id") Integer id);

    List<StyleManage> getAllStyleManage();

    Integer updateStyleManage(@Param("styleManage") StyleManage styleManage);

    Integer updateProcessInStyleManage(@Param("styleManage") StyleManage styleManage);

    Integer updateSizeInStyleManage(@Param("styleManage") StyleManage styleManage);

    StyleManage getStyleManageById(@Param("id") Integer id);

    List<StyleManage> getStyleManageHintPage(@Param("keyWord") String keyWord, @Param("page") int page, @Param("limit") int limit);

    Integer getStyleManageCount(@Param("keyWord") String keyWord);

}
