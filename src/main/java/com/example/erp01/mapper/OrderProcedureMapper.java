package com.example.erp01.mapper;

import com.example.erp01.model.OrderProcedure;
import com.example.erp01.model.ProcedureDetail;
import com.example.erp01.model.ProcedureInfo;
import com.example.erp01.model.ProcedureTemplate;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface OrderProcedureMapper {

    int addOrderProcedure(OrderProcedure orderProcedure);

    int addOrderProcedureBatch(List<OrderProcedure> orderProcedureList);

    int deleteOrderProcedure(Integer orderProcedureID);

    int deleteOrderProcedureByName(String orderName);

    List<OrderProcedure> getUniqueOrderProcedure(Map<String, Object> map);

    List<ProcedureInfo> getProcedureInfoByOrderName(String orderName);

    List<Integer> getProcedureNumbersByOrderName(String orderName);

    ProcedureInfo getProcedureCodeNameByNumber(String orderName, Integer procedureNumber);

    List<ProcedureDetail> getProcedureDetailByOrder(@Param("orderName") String orderName);

    List<OrderProcedure> getOrderProcedureByOrder(@Param("orderName") String orderName);

    ProcedureInfo getProcedureInfoByOrderProcedureNumber(@Param("orderName") String orderName, @Param("procedureNumber") Integer procedureNumber);

    Integer changeProcedureState(@Param("orderName") String orderName, @Param("procedureState") Integer procedureState);

    Float getSectionSamByOrder(@Param("orderName") String orderName, @Param("procedureSection") String procedureSection);

    Integer updateOrderProcedureByProcedureCode(@Param("procedureTemplate") ProcedureTemplate procedureTemplate);

    List<ProcedureInfo> getSpecialProcedureInfoByOrder(@Param("orderName") String orderName);

    Integer changeProcedureStateBatch(Map map);

    List<OrderProcedure> getOrderProcedureToReview(Map map);

    List<OrderProcedure> getOrderProcedureByState(Map map);

    List<Integer> getDistinctProcedureStateByOrder(@Param("orderName") String orderName);

    List<OrderProcedure> getOrderProcedureByOrderProcedureSection(@Param("orderName") String orderName, @Param("procedureSection") String procedureSection);

    List<OrderProcedure> getOrderProcedureByOrderNames(@Param("orderNameList") List<String> orderNameList, @Param("procedureState") Integer procedureState);

    Integer changeScanPartOfProcedure(@Param("orderName") String orderName, @Param("procedureNumber") Integer procedureNumber, @Param("scanPart") String scanPart);

    Double getSectionPriceByOrder(@Param("orderName") String orderName, @Param("procedureSection") String procedureSection);

    Integer getPendingOrderProcedureCount(@Param("procedureState") Integer procedureState);

    OrderProcedure getOrderProcedureByOrderNameProcedureNumber(@Param("orderName") String orderName, @Param("procedureNumber") Integer procedureNumber);

    List<OrderProcedure> getSewOrderProcedureByOrder(@Param("orderName")String orderName);

    List<OrderProcedure> getOrderProcedureByMap(Map<String, Object> params);
}
