package com.example.erp01.mapper;

import com.example.erp01.model.ClothesDevFabricNumber;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ClothesDevFabricNumberMapper {

    Integer addClothesDevFabricNumber(@Param("clothesDevFabricNumber") ClothesDevFabricNumber clothesDevFabricNumber);

    Integer deleteClothesDevFabricNumber(@Param("id") Integer id);

    List<ClothesDevFabricNumber> getAllClothesDevFabricNumber();

    Integer updateClothesDevFabricNumber(@Param("clothesDevFabricNumber") ClothesDevFabricNumber clothesDevFabricNumber);

}
