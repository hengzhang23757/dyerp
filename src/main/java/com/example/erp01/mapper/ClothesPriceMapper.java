package com.example.erp01.mapper;

import com.example.erp01.model.ClothesPrice;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ClothesPriceMapper {

    Integer addClothesPriceBatch(@Param("clothesPriceList") List<ClothesPrice> clothesPriceList);

    Integer deleteClothesPriceBatch(@Param("idList") List<Integer> idList);

    Integer updateClothesPrice(@Param("clothesPrice") ClothesPrice clothesPrice);

    List<ClothesPrice> getClothesPriceByInfo(@Param("orderName") String orderName, @Param("colorName") String colorName);

    Integer updateClothesPriceBatch(@Param("clothesPriceList") List<ClothesPrice> clothesPriceList);

}
