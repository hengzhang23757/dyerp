package com.example.erp01.mapper;

import com.example.erp01.model.OrderProcedure;
import com.example.erp01.model.ProcedureInfo;
import com.example.erp01.model.ProcedureTemplate;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface ProcedureTemplateMapper {

    Integer addProcedureTemplate(ProcedureTemplate procedureTemplate);

    Integer addProcedureTemplateBatch(List<ProcedureTemplate> procedureTemplateList);

    Integer deleteProcedureTemplate(Integer procedureID);

    Integer deleteProcedureTemplateBatch(@Param("procedureIDList") List<Integer> procedureIDList);

    List<ProcedureTemplate> getAllProcedureTemplate(Map<String, Object> map);

    Integer getMaxProcedureCode();

    ProcedureTemplate getByID(Integer procedureID);

    String getProcedureNumberByProcedureCode(@Param("procedureCode")Integer procedureCode);

    String getProcedureNameByProcedureCode(@Param("procedureCode")Integer procedureCode);

    Float getPiecePriceByProcedureCode(@Param("procedureCode")Integer procedureCode);

    Integer updateProcedureTemplate(ProcedureTemplate procedureTemplate);

    ProcedureInfo getProcedureInfoByID(@Param("procedureID")Integer procedureID);

    List<ProcedureTemplate> getProcedureTemplateByLevel(@Param("procedureLevel") String procedureLevel);

    ProcedureTemplate getProcedureTemplateByProcedureCode(@Param("procedureCode")Integer procedureCode);

    List<ProcedureTemplate> getProcedureTemplateHintByInfo(@Param("procedureNumber")Integer procedureNumber, @Param("subProcedureName")String subProcedureName, @Param("subProcedureDescription")String subProcedureDescription, @Param("subRemark")String subRemark,@Param("page") Integer page,@Param("limit") Integer limit);

    List<ProcedureTemplate> getOrderProcedureByMap(Map<String, Object> params);
}
