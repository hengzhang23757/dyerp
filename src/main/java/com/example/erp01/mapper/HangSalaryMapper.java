package com.example.erp01.mapper;

import com.example.erp01.model.Completion;
import com.example.erp01.model.CompletionCount;
import com.example.erp01.model.HangSalary;
import com.example.erp01.model.WeekAmount;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

@Mapper
public interface HangSalaryMapper {

    List<HangSalary> getHangSalaryByTimeEmp(@Param("from")Date from,@Param("to") Date to,@Param("employeeNumber") String employeeNumber);

    List<HangSalary> getGroupHangSalaryByTime(@Param("from")Date from,@Param("to") Date to,@Param("groupName")  String groupName);

    List<HangSalary> getHangProcedureProgress(@Param("from")Date from, @Param("to")Date to, @Param("procedureFrom")Integer procedureFrom, @Param("procedureTo")Integer procedureTo,@Param("orderName")String orderName,@Param("groupName")String groupName);

    List<WeekAmount> getHangWeekAmount();

    List<WeekAmount> getHangFinishWeekAmount();

    List<CompletionCount> getHangCompletion(@Param("procedureNumber")Integer procedureNumber);

    List<String> getEmployeeNamesByOrderProcedure(@Param("orderName")String orderName, @Param("procedureNumber")Integer procedureNumber);

    List<HangSalary> getOrderHangSalarySummary(@Param("orderName")String orderName,@Param("from")Date from,@Param("to") Date to);

    List<HangSalary> getHangProcedureProgressNew(@Param("from")Date from, @Param("to")Date to, @Param("procedureFrom")Integer procedureFrom, @Param("procedureTo")Integer procedureTo,@Param("orderName")String orderName,@Param("groupName")String groupName,@Param("employeeNumber")String employeeNumber);

    List<HangSalary> getHangSalaryDetail(@Param("from")Date from, @Param("to") Date to, @Param("employeeNumber") String employeeNumber, @Param("orderName") String orderName);

    List<HangSalary> getHangSalaryByOrderColorSizeEmp(@Param("from")Date from, @Param("to") Date to, @Param("employeeNumber") String employeeNumber, @Param("orderName") String orderName, @Param("colorName") String colorName, @Param("sizeName") String sizeName);

    Integer getHangPieceCountByOrderColorSize(@Param("orderName")String orderName,@Param("procedureNumber") Integer procedureNumber, @Param("colorName")String colorName, @Param("sizeName")String sizeName);

    List<HangSalary> getHangProcedureProgressMultiGroup(@Param("from")Date from, @Param("to")Date to, @Param("procedureFrom")Integer procedureFrom, @Param("procedureTo")Integer procedureTo,@Param("orderName")String orderName,@Param("groupList")List<String> groupList,@Param("employeeNumber")String employeeNumber);
}
