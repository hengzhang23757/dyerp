package com.example.erp01.mapper;

import com.example.erp01.model.ProcedureLevel;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ProcedureLevelMapper {

    List<ProcedureLevel> getAllProcedureLevel();

    int addProcedureLevel(@Param("procedureLevel") ProcedureLevel procedureLevel);

    int deleteProcedureLevel(@Param("levelID") Integer levelID);

    int updateProcedureLevel(@Param("procedureLevel") ProcedureLevel procedureLevel);

    Float getLevelValueByName(@Param("level") String level);

}
