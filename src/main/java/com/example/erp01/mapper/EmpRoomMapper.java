package com.example.erp01.mapper;

import com.example.erp01.model.EmpRoom;
import com.example.erp01.model.Room;
import com.example.erp01.model.RoomState;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface EmpRoomMapper {

    int addEmpRoom(EmpRoom empRoom);

    int deleteEmpRoom(@Param("empRoomID") Integer empRoomID);

    int updateEmpRoom(EmpRoom empRoom);

    List<EmpRoom> getAllEmpRoom();

    List<RoomState> getRoomStateOfFloor(@Param("floor") String floor);

    List<String> getAllBedNumberOfRoom(@Param("roomNumber") String roomNumber);

    List<EmpRoom> getEmpRoomByRoom(@Param("roomNumber") String roomNumber);

}
