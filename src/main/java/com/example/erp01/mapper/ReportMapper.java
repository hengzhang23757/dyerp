package com.example.erp01.mapper;

import com.example.erp01.model.*;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

@Mapper
public interface ReportMapper {

    List<ProductionProgress> getProductionProgressByOrderTimeGroup(@Param("orderName")String orderName, @Param("from")Date from, @Param("to")Date to, @Param("groupName")String groupName);

    List<MiniProgress> getProductionProgressByOrder(@Param("orderName")String orderName);

    List<MiniProgress> getProductionProgressByOrderGroup(@Param("orderName")String orderName, @Param("groupName")String groupName);

    List<CutDaily> getCutDailyDetailByOrderTime(@Param("orderName")String orderName, @Param("from")Date from, @Param("to")Date to);

    List<ProductionProgress> getMiniProductionProgressByOrderGroup(@Param("orderName")String orderName, @Param("from")Date from, @Param("to")Date to, @Param("groupName")String groupName);

    List<ProductionProgressDetail> getProductionProgressDetailByOrderTimeGroup(@Param("orderName")String orderName, @Param("from")Date from, @Param("to")Date to, @Param("groupName")String groupName, @Param("procedureNumber")Integer procedureNumber);

    List<ProductionProgressDetail> getMiniProductionProgressDetailByOrderTimeGroup(@Param("orderName")String orderName, @Param("from")Date from, @Param("to")Date to, @Param("groupName")String groupName, @Param("procedureNumber")Integer procedureNumber);

    List<PersonProductionDetail> getPersonProductionOfProcedure(@Param("orderName")String orderName, @Param("from")Date from, @Param("to")Date to, @Param("groupName")String groupName, @Param("procedureNumber")Integer procedureNumber);

    List<PersonProductionDetail> getMiniPersonProductionOfProcedure(@Param("orderName")String orderName, @Param("from")Date from, @Param("to")Date to, @Param("groupName")String groupName, @Param("procedureNumber")Integer procedureNumber);

    List<PersonProductionDetail> getEachPersonProduction(@Param("orderName")String orderName, @Param("from")Date from, @Param("to")Date to, @Param("groupName")String groupName);

    List<PersonProductionDetail> getMiniEachPersonProductionOfProcedure(@Param("orderName")String orderName, @Param("from")Date from, @Param("to")Date to, @Param("groupName")String groupName);

    List<CutQueryLeak> getCutInStoreProgress(@Param("orderName")String orderName, @Param("from")Date from, @Param("to")Date to);

    List<CutQueryLeak> getEmbInStoreProgress(@Param("orderName")String orderName, @Param("from")Date from, @Param("to")Date to);

    List<CutQueryLeak> getEmbOutStoreProgress(@Param("orderName")String orderName, @Param("from")Date from, @Param("to")Date to);

    Integer getCutInStoreCount(@Param("orderName")String orderName, @Param("from")Date from, @Param("to")Date to);

    Integer getEmbInStoreCount(@Param("orderName")String orderName, @Param("from")Date from, @Param("to")Date to);

    Integer getEmbOutStoreCount(@Param("orderName")String orderName, @Param("from")Date from, @Param("to")Date to);

    Date getOtherCreateTimeByOrderBed(@Param("orderName")String orderName, @Param("partName") String partName, @Param("bedNumber")Integer bedNumber);

    List<CutBedColor> getCutBedColorByOrder(@Param("orderName") String orderName, @Param("partName") String partName, @Param("tailorType") Integer tailorType, @Param("from") String from, @Param("to") String to);


    List<TailorMonthReport> getTailorMonthReportOfEachDay(@Param("from")Date from, @Param("to")Date to, @Param("orderName")String orderName, @Param("partName") String partName, @Param("tailorType") Integer tailorType, @Param("groupName")String groupName);

    List<TailorMonthReport> getOtherTailorMonthReportOfEachDay(@Param("from")Date from, @Param("to")Date to, @Param("orderName")String orderName, @Param("partName") String partName, @Param("tailorType") Integer tailorType, @Param("groupName")String groupName);

    Date getCreateTimeByOrderBed(@Param("orderName")String orderName, @Param("bedNumber")Integer bedNumber);

    Integer fillProcedurePrice(@Param("orderProcedureList") List<OrderProcedure> orderProcedureList, @Param("monthString")String monthString);

    List<String> getPieceWorkOrderNameListByMonthString(@Param("startTime")String startTime,@Param("endTime")String endTime);

    List<GeneralSalary> getPieceWorkSummarySalaryByInfo(@Param("from")Date from, @Param("to")Date to, @Param("groupName")String groupName);

    List<GeneralSalary> getPieceWorkGroupSalaryByInfo(@Param("from")Date from, @Param("to")Date to, @Param("groupName")String groupName);

}
