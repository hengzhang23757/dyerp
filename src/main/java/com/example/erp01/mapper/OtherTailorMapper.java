package com.example.erp01.mapper;

import com.example.erp01.model.*;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Mapper
//@Repository
public interface OtherTailorMapper {

    List<OtherTailor> generateOtherTailorData(String jsonStr);

    int saveOtherTailorData(List<OtherTailor> otherTailorList);

    List<OtherTailor> getAllOtherTailorData();

    List<OtherTailor> getAllOtherTailorDataByOrder(String orderName);

    //由订单号获取当前最大床号
    Integer getMaxBedNumberByOrder(@Param("orderName") String orderName);


    List<OtherTailor> getOtherTailorByOrderNameBedNum(@Param("orderName") String orderName, @Param("bedNumber") int bedNumber);

    int getOtherTailorCountByOrderNameBedNum(@Param("orderName") String orderName, @Param("bedNumber") int bedNumber);

    List<Integer> getOtherBedNumbersByOrderName(@Param("orderName") String orderName);

    List<String> getOtherSizeNamesByOrderName(@Param("orderName") String orderName);

    List<String> getOtherPartNamesByOrderName(@Param("orderName") String orderName);

    Integer getOtherTailorCountByOrderNameBedNumPart(@Param("orderName") String orderName, @Param("bedNumber") int bedNumber, @Param("partName") String partName);

    List<OtherTailor> getOtherTailorByOrderNameBedNumPart(@Param("orderName") String orderName, @Param("bedNumber") int bedNumber, @Param("partName") String partName);

    List<TailorReport> otherTailorReport(@Param("orderName") String orderName, @Param("partName") String partName, @Param("bedNumber") Integer bedNumber);

    Integer getOtherMaxPackageNumberByOrder(@Param("orderName") String orderName);

    List<OtherTailor> getDetailOtherTailorByOrderNameBedNumber(@Param("orderName") String orderName, @Param("bedNumber") int bedNumber);

    int deleteOtherTailor(Integer otherTailorID);

    int deleteOtherTailorByOrderBed(@Param("orderName") String orderName, @Param("bedNumber") Integer bedNumber);

    int deleteOtherTailorByOrderBedPack(Map map);

    List<OtherTailor> getAllOtherTailorByOrderBed(@Param("orderName") String orderName, @Param("bedNumber") Integer bedNumber, @Param("packageNumber") Integer packageNumber);

    List<String> getOtherTailorQcodeByTailorQcodeID(List<Integer> tailorQcodeIDList);

    String getOneOtherTailorQcodeByTailorQcodeID(Integer tailorQcodeID);

    List<CutQueryLeak> getOtherTailorInfoByNamePart(@Param("orderName") String orderName, @Param("partName") String partName);

    List<Integer> getOtherBedNumbersByOrderPart(@Param("orderName") String orderName, @Param("partName") String partName);

    Integer deleteByOrderPartBed(@Param("orderName") String orderName, @Param("partName") String partName, @Param("bedNumber") Integer bedNumber);

    Integer getOtherMaxPackageNumberByOrderpart(@Param("orderName") String orderName, @Param("partName") String partName);

    List<OtherTailor> getDetailOtherTailorByOrderPartBed(@Param("orderName") String orderName, @Param("partName") String partName, @Param("bedNumber") Integer bedNumber);

    List<Integer> getOtherPackageNumbersByOrderBedColor(@Param("orderName")String orderName, @Param("bedNumber")Integer bedNumber, @Param("colorName")String colorName);

    Integer getOtherLayerCountByOrderBedPackage(@Param("orderName")String orderName, @Param("bedNumber")Integer bedNumber, @Param("packageNumber")Integer packageNumber);

    List<OtherTailorMonthReport> getOtherTailorMonthReport(@Param("from")Date from, @Param("to")Date to, @Param("orderName")String orderName, @Param("groupName")String groupName);

    Integer getCutCountByOrderPart(@Param("orderName")String orderName, @Param("partName")String partName);

    Integer updateOtherTailorData(@Param("otherTailorList")List<OtherTailor> otherTailorList);

    List<OtherTailor> getOtherTailorDataByOrderBed(@Param("orderName")String orderName, @Param("bedNumber")Integer bedNumber);

    OtherTailor getOtherTailorByTailorQcodeID(@Param("tailorQcodeID") Integer tailorQcodeID);

    List<OtherTailor> getOtherTailorByOrderPartColorSize(@Param("orderName")String orderName, @Param("partName")String partName, @Param("colorName")String colorName, @Param("sizeName")String sizeName);

    OtherTailor getOneOtherTailorByTailorQcodeID(@Param("tailorQcodeID") Integer tailorQcodeID);
}
