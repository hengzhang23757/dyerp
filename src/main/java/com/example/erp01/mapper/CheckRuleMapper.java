package com.example.erp01.mapper;

import com.example.erp01.model.CheckRule;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
@Mapper
public interface CheckRuleMapper {

    List<CheckRule> getAllCheckRule();

    Integer updateCheckRule(@Param("checkRule") CheckRule checkRule);

}
