package com.example.erp01.mapper;

import com.example.erp01.model.MonthSalary;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface MonthSalaryMapper {

    Integer addMonthSalaryBatch(@Param("monthSalaryList") List<MonthSalary> monthSalaryList);

    List<MonthSalary> getMonthSalaryByMonthString(@Param("monthString") String monthString);

    Integer deleteMonthSalaryByMonthString(@Param("monthString") String monthString);

}
