package com.example.erp01.mapper;

import com.example.erp01.model.ManufactureOrder;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ManufactureOrderMapper {

    Integer addManufactureOrder(@Param("manufactureOrder") ManufactureOrder manufactureOrder);

    ManufactureOrder getManufactureOrderByOrderName(@Param("orderName") String orderName);

    Integer deleteManufactureByOrder(@Param("orderName") String orderName);

    Integer updateManufactureOrder(@Param("manufactureOrder") ManufactureOrder manufactureOrder);

    List<String> getOrderNameByCustomerSeason(@Param("season") String season, @Param("customerName") String customerName);

    List<ManufactureOrder> getManufactureOrderByOrderNameList(@Param("orderNameList") List<String> orderNameList);

    List<String> getHistoryDistinctSeason();

    List<String> getHistoryCustomer();

    List<ManufactureOrder> getManufactureOrderListByModelSeason(@Param("modelType") String modelType, @Param("seasonList") List<String> seasonList, @Param("customerNameList") List<String> customerNameList);

    Integer changeModifyStateByOrderName(@Param("orderName") String orderName, @Param("modifyState") String modifyState);

    List<ManufactureOrder> getManufactureOrderByModifyState(@Param("modifyState") String modifyState);

    void changeOrderName(@Param("orderName") String orderName, @Param("toOrderName") String toOrderName);
}
