package com.example.erp01.mapper;

import com.example.erp01.model.BasicFabric;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface BasicFabricMapper {

    int addBasicFabricBatch(@Param("basicFabricList")List<BasicFabric> basicFabricList);

    List<BasicFabric> getAllBasicFabric();

    List<BasicFabric> getThreeMonthsBasicFabric();

    int deleteBasicFabric(@Param("basicFabricID")Integer basicFabricID);

    int updateBasicFabric(@Param("basicFabric")BasicFabric basicFabric);

    List<String> getBasicFabricHint(@Param("subFabricName")String subFabricName);

    List<String> getFabricSupplierHint(@Param("basicFabricName")String basicFabricName);

    String getBasicFabricNumberByNameSupplier(@Param("basicFabricName")String basicFabricName, @Param("basicFabricSupplier")String basicFabricSupplier);

    List<String> getBasicFabricColorByNameSupplier(@Param("basicFabricName")String basicFabricName, @Param("basicFabricSupplier")String basicFabricSupplier);

    String getBasicFabricColorNumberByNameSupplierColor(@Param("basicFabricName")String basicFabricName, @Param("basicFabricSupplier")String basicFabricSupplier, @Param("basicFabricColor")String basicFabricColor);

    List<String> getBasicFabricWidthByNameSupplierColor(@Param("basicFabricName")String basicFabricName, @Param("basicFabricSupplier")String basicFabricSupplier, @Param("basicFabricColor")String basicFabricColor);

    List<String> getBasicFabricWeightByNameSupplierColorWidth(@Param("basicFabricName")String basicFabricName, @Param("basicFabricSupplier")String basicFabricSupplier, @Param("basicFabricColor")String basicFabricColor, @Param("basicFabricWidth")String basicFabricWidth);

}
