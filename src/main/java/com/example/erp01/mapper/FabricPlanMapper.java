package com.example.erp01.mapper;

import com.example.erp01.model.FabricPlan;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.omg.PortableInterceptor.INACTIVE;

import java.util.Date;
import java.util.List;

@Mapper
public interface FabricPlanMapper {

    int addFabricPlanBatch(@Param("fabricPlanList")List<FabricPlan> fabricPlanList);

    int deleteFabricPlan(@Param("fabricPlanID")Integer fabricPlanID);

    List<FabricPlan> getAllFabricPlan();

    int updateFabricPlan(@Param("fabricPlan")FabricPlan fabricPlan);

    List<FabricPlan> getUniqueFabricPlan();

    List<FabricPlan> getFabricPlanDetailByOrder(@Param("orderName")String orderName);

    List<FabricPlan> getFabricPlanDetailByOrderSeasonDate(@Param("orderName")String orderName, @Param("orderCount")Integer orderCount, @Param("season")String season, @Param("orderDate") Date orderDate);

    int deleteFabricPlanByOrder(@Param("orderName")String orderName);

    int deleteFabricPlanByOrderSeasonDate(@Param("orderName")String orderName, @Param("orderCount")Integer orderCount, @Param("season")String season, @Param("orderDate") Date orderDate);

    List<String> getFabricNamesByOrder(@Param("orderName")String orderName);

    List<String> getFabricColorsByOrderFabric(@Param("orderName")String orderName, @Param("fabricName")String fabricName);

    String getColorNameByOrderFabric(@Param("orderName")String orderName, @Param("fabricName")String fabricName, @Param("fabricColor")String fabricColor);

    Integer getFabricOrderCountByOrderFabricColor(@Param("orderName")String orderName, @Param("fabricName")String fabricName, @Param("fabricColor")String fabricColor);

    String getSupplierByOrderFabricColor(@Param("orderName")String orderName, @Param("fabricName")String fabricName, @Param("fabricColor")String fabricColor);

    List<String> getColorNamesByOrderFabric(@Param("orderName")String orderName, @Param("fabricName")String fabricName, @Param("fabricColor")String fabricColor);
}
