package com.example.erp01.mapper;

import com.example.erp01.model.AccessoryStorage;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface AccessoryStorageMapper {

    int addAccessoryStorageBatch(@Param("accessoryStorageList") List<AccessoryStorage> accessoryStorageList);

    int deleteAccessoryByID(@Param("id") Integer id);

    List<AccessoryStorage> getAccessoryStorageByOrder(@Param("orderName") String orderName);

    List<AccessoryStorage> getAllAccessoryStorage();

    AccessoryStorage getAccessoryById(@Param("id") Integer id);

    List<AccessoryStorage> getAccessoryStorageByInfo(@Param("orderName") String orderName, @Param("accessoryName") String accessoryName, @Param("specification") String specification, @Param("accessoryColor") String accessoryColor, @Param("colorName") String colorName, @Param("sizeName") String sizeName, @Param("accessoryID") Integer accessoryID);

    int updateAccessoryStorage(@Param("accessoryStorage") AccessoryStorage accessoryStorage);

    int updateAccessoryStorageBatch(@Param("accessoryStorageList") List<AccessoryStorage> accessoryStorageList);

    int deleteAccessoryStorageBatch(@Param("idList") List<Integer> idList);

    List<AccessoryStorage> getAccessoryStorageByColorSizeList(@Param("orderName") String orderName, @Param("colorList") List<String> colorList, @Param("sizeList") List<String> sizeList);

    List<AccessoryStorage> getAccessoryStorageByAccessoryIdList(@Param("accessoryIdList") List<Integer> accessoryIdList);

    List<AccessoryStorage> getBjStorageByInfo(@Param("orderName") String orderName);
}
