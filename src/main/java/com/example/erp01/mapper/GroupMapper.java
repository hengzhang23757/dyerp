package com.example.erp01.mapper;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface GroupMapper {

    //第一次写，用途：裁片衣胚下货组名称
    List<String> getAllGroupNames();
    //后续添加，用途：获取部门的工序平衡
    List<String> getBigGroupNames();

    //获取车缝组名
    List<String> getSewGroupNames();

}
