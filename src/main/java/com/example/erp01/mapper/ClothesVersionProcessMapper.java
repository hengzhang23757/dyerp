package com.example.erp01.mapper;

import com.example.erp01.model.ClothesVersionProcess;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ClothesVersionProcessMapper {

    int addClothesVersionProcess(@Param("clothesVersionProcess")ClothesVersionProcess clothesVersionProcess);

    List<ClothesVersionProcess> getUniqueClothesVersionProcess();

    List<ClothesVersionProcess> getClothesVersionProcessByOrder(@Param("orderName")String orderName);

    int addClothesVersionProcessBatch(@Param("clothesVersionProcessList")List<ClothesVersionProcess> clothesVersionProcessList);

    int deleteClothesVersionProcess(@Param("clothesVersionProcessID")Integer clothesVersionProcessID);

    int deleteClothesVersionProcessByOrder(@Param("orderName")String orderName);

}
