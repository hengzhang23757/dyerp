package com.example.erp01.mapper;

import com.example.erp01.model.MaxBedNumber;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface MaxBedNumberMapper {

    Integer getMaxBedNumberByOrder(@Param("orderName")String orderName);

    Integer addMaxBedNumber(@Param("maxBedNumber")MaxBedNumber maxBedNumber);

}
