package com.example.erp01.mapper;

import com.example.erp01.model.GeneralSalary;
import com.example.erp01.model.HourEmp;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

@Mapper
public interface HourEmpMapper {

    int addHourEmp(HourEmp hourEmp);

    List<HourEmp> getAllHourEmp();

    int deleteHourEmp(@Param("hourEmpID")Integer hourEmpID);

    List<GeneralSalary> getHourEmpByTimeEmp(@Param("from") Date from, @Param("to")Date to, @Param("employeeNumber")String employeeNumber);

    List<GeneralSalary> getGroupHourEmpByTime(@Param("from") Date from, @Param("to")Date to,@Param("groupName")String groupName);

    List<HourEmp> getHourProcedureProgress(@Param("from")Date from, @Param("to")Date to, @Param("procedureFrom")Integer procedureFrom, @Param("procedureTo")Integer procedureTo,@Param("orderName")String orderName,@Param("groupName")String groupName);

    List<HourEmp> getOrderHourEmpDetail(@Param("orderName")String orderName, @Param("from")Date from, @Param("to")Date to);

    List<HourEmp> getOrderHourEmpSummary(@Param("orderName") String orderName,@Param("from")Date from,@Param("to") Date to);

    int updateHourEmp(HourEmp hourEmp);

    List<HourEmp> getTodayHourEmp();

    List<HourEmp> getOneMonthHourEmp();

    List<HourEmp> getThreeMonthsHourEmp();

    List<GeneralSalary> getHourProcedureProgressNew(@Param("from")Date from, @Param("to")Date to, @Param("procedureFrom")Integer procedureFrom, @Param("procedureTo")Integer procedureTo,@Param("orderName")String orderName,@Param("groupName")String groupName,@Param("employeeNumber")String employeeNumber);

    List<HourEmp> getHourProcedureProgressMultiGroup(@Param("from")Date from, @Param("to")Date to, @Param("procedureFrom")Integer procedureFrom, @Param("procedureTo")Integer procedureTo,@Param("orderName")String orderName,@Param("groupList")List<String> groupList,@Param("employeeNumber")String employeeNumber);
}
