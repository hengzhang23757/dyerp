package com.example.erp01.mapper;

import com.example.erp01.model.FabricOrder;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface FabricOrderMapper {

    List<FabricOrder> getAllFabricOrder();

    List<FabricOrder> getTodayFabricOrder();

    List<FabricOrder> getOneMonthFabricOrder();

    List<FabricOrder> getThreeMonthsFabricOrder();

    Integer addFabricOrderBatch(@Param("fabricOrderList") List<FabricOrder> fabricOrderList);

    Integer updateFabricOrder(@Param("fabricOrder") FabricOrder fabricOrder);

    Integer commitFabricOrder(@Param("fabricOrderIDList") List<Integer> fabricOrderIDList);

    Integer deleteFabricOrderBatch(@Param("fabricOrderIDList") List<Integer> fabricOrderIDList);

    Integer deleteFabricOrder(@Param("fabricOrderID") Integer fabricOrderID);
}
