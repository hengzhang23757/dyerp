package com.example.erp01.mapper;

import com.example.erp01.model.OpaBackInput;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface OpaBackInputMapper {

    int addOpaBackInput(OpaBackInput opaBackInput);

    List<OpaBackInput> getAllOpaBackInput();

    int deleteOpaBackInput(Integer opaBackInputID);

    int addOpaBackInputBatch(List<OpaBackInput> opaBackInputList);

    int updateOpaBackInput(OpaBackInput opaBackInput);

    List<OpaBackInput> getTodayOpaBackInput();

    List<OpaBackInput> getOneMonthOpaBackInput();

    List<OpaBackInput> getThreeMonthOpaBackInput();

    List<OpaBackInput> getOpaBackInputByOrder(@Param("orderName")String orderName);

    List<OpaBackInput> getBedOpaBackInputByOrder(@Param("orderName")String orderName);
}
