package com.example.erp01.mapper;

import com.example.erp01.model.InnerBoard;
import com.example.erp01.model.OtherStorage;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface OtherStorageMapper {

    int otherInStore(@Param("otherStorageList")List<OtherStorage> otherStorageList);

    int otherOutStore(@Param("otherStorageList")List<OtherStorage> otherStorageList);

    List<OtherStorage> searchOtherInStorage(@Param("orderName")String orderName, @Param("partName")String partName, @Param("colorName")String colorName, @Param("sizeName")String sizeName);

    List<OtherStorage> searchOtherOutStorage(@Param("orderName")String orderName, @Param("partName")String partName, @Param("colorName")String colorName, @Param("sizeName")String sizeName);

}
