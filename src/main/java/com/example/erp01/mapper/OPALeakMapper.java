package com.example.erp01.mapper;

import com.example.erp01.model.*;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

@Mapper
public interface OPALeakMapper {

    List<String> getOPAPartNamesByOrder(@Param("orderName")String orderName);

    List<String> getOtherOPAPartNamesByOrder(@Param("orderName")String orderName);

    List<OPALeak> getTailorOPA(@Param("orderName")String orderName, @Param("partName")String partName, @Param("bedNumber")Integer bedNumber);

    List<OPALeak> getOtherTailorOPA(@Param("orderName")String orderName, @Param("partName")String partName, @Param("bedNumber")Integer bedNumber);

    List<OPALeak> getOtherOPA(@Param("orderName")String orderName, @Param("partName")String partName);

    List<OPALeak> getOPABack(@Param("orderName")String orderName, @Param("partName")String partName, @Param("fromDate") Date fromDate, @Param("toDate") Date toDate);

    List<OPALeak> getTailorOPAByBed(@Param("orderName")String orderName, @Param("partName")String partName, @Param("bedNumber")Integer bedNumber);

    List<OPALeak> getOtherTailorOPAByBed(@Param("orderName")String orderName, @Param("partName")String partName, @Param("bedNumber")Integer bedNumber);

    List<OPALeak> getMonthOPAInfo(@Param("fromDate")Date fromDate, @Param("toDate")Date toDate);

    List<OPALeak> getOtherMonthOPAInfo(@Param("fromDate")Date fromDate, @Param("toDate")Date toDate);

    List<OPALeak> getMonthOPABackInfo(@Param("fromDate")Date fromDate, @Param("toDate")Date toDate);

    List<OPALeak> getTailorCountByOrderPart(@Param("orderName")String orderName, @Param("partName")String partName);

    List<OPALeak> getOtherTailorCountByOrderPart(@Param("orderName")String orderName, @Param("partName")String partName);

    List<Integer> getOPABedNumbersByOrderPart(@Param("orderName")String orderName, @Param("partName")String partName);

    List<OPALeak> getTailorInfoByOrderPart(@Param("orderName")String orderName, @Param("partName")String partName);

    List<OPALeak> getOtherTailorInfoByOrderPart(@Param("orderName")String orderName, @Param("partName")String partName);

    List<OPA> getOPAByOrderTime(@Param("orderName")String orderName, @Param("partName")String partName, @Param("fromDate")Date fromDate, @Param("toDate")Date toDate);

    List<OtherOPA> getOtherOPAByOrderTime(@Param("orderName")String orderName, @Param("partName")String partName, @Param("fromDate")Date fromDate, @Param("toDate")Date toDate);

    List<OpaBackInput> getOPABackInputByOrderTime(@Param("orderName")String orderName, @Param("partName")String partName, @Param("fromDate")Date fromDate, @Param("toDate")Date toDate);

}
