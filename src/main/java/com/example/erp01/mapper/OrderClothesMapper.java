package com.example.erp01.mapper;

import com.example.erp01.model.Completion;
import com.example.erp01.model.CutQueryLeak;
import com.example.erp01.model.OrderClothes;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
//@Repository
public interface OrderClothesMapper {

    int addOrderClothes(List<OrderClothes> orderClothesList);

    List<OrderClothes> getAllOrderClothes();

    List<OrderClothes> getOrderSummary();

    List<OrderClothes> getOrderByName(String orderName);

    int deleteOrderByName(@Param("orderName") String orderName);

    //订单输入下拉提示
    List<String> getOrderHint(String subOrderName);

    //颜色下拉提示
    List<String> getColorHint(String orderName);

    //输入订单自动带出顾客名
    String getCustomerNameByOrderName(String orderName);
    //获取订单信息用作裁床查漏报表
    List<CutQueryLeak> getOrderInfoByName(String orderName);

    String getVersionNumberByOrderName(String orderName);

    Integer getOrderTotalCount(String orderName);

    List<String> getOrderSizeNamesByOrder(@Param("orderName") String orderName);

    String getDescriptionByOrder(@Param("orderName") String orderName);

    List<String> getVersionHint(@Param("subVersion") String subVersion);

    List<String> getOrderByVersion(@Param("clothesVersionNumber") String clothesVersionNumber);

    List<Completion> getOrderCompletion();

    String getSeasonByOrder(@Param("orderName") String orderName);

    List<OrderClothes> getOneMonthOrderClothes();

    List<OrderClothes> getThreeMonthsOrderClothes();

    List<OrderClothes> getOneMonthOrderSummary();

    List<OrderClothes> getThreeMonthsOrderSummary();

    List<OrderClothes> getOneYearOrderSummary(@Param("orderName") String orderName);

    Integer getColorOrderCount(@Param("orderName") String orderName, @Param("colorName") String colorName);

    List<String> getAllCustomerName();

    List<OrderClothes> getOrderClothesByOrder(@Param("orderNameList") List<String> orderNameList, @Param("customerName") String customerName);

    List<String> getOrderColorNamesByOrder(@Param("orderName") String orderName);

    Integer updateOrderClothes(@Param("orderClothesList") List<OrderClothes> orderClothesList);

    List<OrderClothes> getOrderSummaryByColor(@Param("orderName") String orderName);

    List<OrderClothes> getOrderListSummaryByColor(@Param("orderNameList") List<String> orderNameList);

    List<OrderClothes> getOrderClothesByOrderName(@Param("orderName") String orderName);

    Integer updateOrderClothesByID(@Param("orderClothes") OrderClothes orderClothes);

    Integer deleteOrderClothesByID(@Param("orderClothesID") Integer orderClothesID);

    Integer getOrderCountByColorSizeList(@Param("orderName") String orderName, @Param("colorList") List<String> colorList, @Param("sizeList") List<String> sizeList);

    List<OrderClothes> getLastSeasonSummary();

    List<OrderClothes> getOrderAndVersionByOrder(@Param("subOrderName") String subOrderName);

    List<OrderClothes> getOrderAndVersionByVersion(@Param("subVersion") String subVersion);

    List<OrderClothes> getOrderAndVersionByVersionPage(@Param("subVersion") String subVersion, @Param("page") int page, @Param("limit") int limit);

    Integer getCountOrderAndVersionByVersion(@Param("subVersion") String subVersion);

    Integer updateOrderClothesByOrder(@Param("orderName") String orderName, @Param("styleDescription") String styleDescription, @Param("season") String season, @Param("customerName") String customerName, @Param("userName") String userName);

    List<OrderClothes> getOrderClothesSummaryByMap(Map<String, Object> map);

    List<OrderClothes> getOrderCountByOrderNameList(@Param("orderNameList") List<String> orderNameList);

    Integer deleteOrderClothesBatch(@Param("orderClothesIDList") List<Integer> orderClothesIDList);

    Integer updateOrderStateByOrder(@Param("orderName") String orderName, @Param("orderState") Integer orderState);

    List<OrderClothes> getOrderClothesCountGroupByColor(@Param("orderName") String orderName);

    List<OrderClothes> getOrderClothesCountGroupBySize(@Param("orderName") String orderName);

    void changeOrderName(@Param("orderName") String orderName, @Param("toOrderName") String toOrderName);


    List<OrderClothes> getOrderAndVersionHint(@Param("keyword") String keyword);
}
