package com.example.erp01.mapper;

import com.example.erp01.model.FabricDetail;
import com.example.erp01.model.FabricOutRecord;
import com.example.erp01.model.FabricStorage;
import com.example.erp01.model.ManufactureFabric;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;
@Mapper
public interface InvoicingFabricMapper {

    List<Integer> getFabricDetailIdList(Map<String, Object> map);

    List<Integer> getFabricOutRecordIdList(Map<String, Object> map);

    List<FabricDetail> getFabricDetailByIdList(@Param("fabricIDList") List<Integer> fabricIDList);

    List<FabricStorage> getFabricStorageByIdList(@Param("fabricIDList") List<Integer> fabricIDList);

    List<FabricOutRecord> getFabricOutRecordByIdList(@Param("fabricIDList") List<Integer> fabricIDList);

    List<FabricDetail> getFabricDetailByMap(Map<String, Object> map);

    List<FabricStorage> getFabricStorageByMap(Map<String, Object> map);

    List<FabricOutRecord> getFabricOutRecordByMap(Map<String, Object> map);

}
