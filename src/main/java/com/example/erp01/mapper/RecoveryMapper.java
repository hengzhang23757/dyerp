package com.example.erp01.mapper;

import com.example.erp01.model.Tailor;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface RecoveryMapper {

    List<Integer> getDeletedBedNumber(@Param("orderName")String orderName);

    List<Tailor> getDeletedTailorByInfo(@Param("orderName")String orderName, @Param("bedNumber")Integer bedNumber);

    List<Tailor> getNormalTailorByInfo(@Param("orderName")String orderName, @Param("bedNumber")Integer bedNumber);

    Integer recoverTailorByInfo(@Param("orderName")String orderName, @Param("tailorIDList")List<Integer> tailorIDList);

    List<Tailor> getSelectedTailorByID(@Param("orderName")String orderName, @Param("tailorIDList")List<Integer> tailorIDList);
}
