package com.example.erp01.mapper;

import com.example.erp01.model.InnerBoard;
import com.example.erp01.model.InnerBoardStorage;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface InnerBoardStorageMapper {

    int innerBoardInStore(InnerBoardStorage innerBoardStorage);

    int innerBoardOutStore(InnerBoardStorage innerBoardStorage);

    int deleteInnerBoardStorage(Integer innerBoardStorageID);

    List<InnerBoardStorage> getAllInnerBoardStorage();

    int updateInnerBoardStorage(InnerBoardStorage innerBoardStorage);

    List<String> getLocationHint(@Param("versionNumber")String versionNumber, @Param("fabricNumber")String fabricNumber,@Param("colorNumber")String colorNumber,@Param("jarNumber")String jarNumber);

    InnerBoardStorage getInnerBoardStorage(@Param("location")String location,@Param("versionNumber")String versionNumber,@Param("fabricNumber")String fabricNumber,@Param("colorNumber")String colorNumber,@Param("jarNumber")String jarNumber);
}
