package com.example.erp01.mapper;

import com.example.erp01.model.AccessoryOutRecord;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface AccessoryOutRecordMapper {

    Integer addAccessoryOutRecord(@Param("accessoryOutRecord") AccessoryOutRecord accessoryOutRecord);

    Integer addAccessoryOutRecordBatch(@Param("accessoryOutRecordList") List<AccessoryOutRecord> accessoryOutRecordList);

    List<AccessoryOutRecord> getAccessoryOutRecordByOrder(@Param("orderName") String orderName);

    List<AccessoryOutRecord> getAccessoryOutRecordByInfo(@Param("orderName") String orderName, @Param("accessoryName") String accessoryName, @Param("specification") String specification, @Param("accessoryColor") String accessoryColor, @Param("colorName") String colorName, @Param("sizeName") String sizeName, @Param("accessoryID") Integer accessoryID);

    List<AccessoryOutRecord> getTodayAccessoryOutRecord();

    List<AccessoryOutRecord> getAccessoryOutRecordByTime(@Param("from") String from, @Param("to") String to);

    List<AccessoryOutRecord> getAccessoryOutRecordByOrderNameList(@Param("orderNameList") List<String> orderNameList);

    List<AccessoryOutRecord> getBjOutRecordByInfo(@Param("orderName") String orderName);

}
