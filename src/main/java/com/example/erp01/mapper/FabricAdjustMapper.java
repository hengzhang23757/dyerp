package com.example.erp01.mapper;

import com.example.erp01.model.*;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface FabricAdjustMapper {

    List<ManufactureFabric> getAllManufactureFabric();

    List<FabricDetail> getAllFabricDetail();

    List<FabricStorage> getAllFabricStorage();

    List<FabricOutRecord> getAllFabricOutRecord();

    List<ManufactureAccessory> getAllManufactureAccessory();

    List<AccessoryInStore> getAllAccessoryInStore();

    List<AccessoryStorage> getAllAccessoryStorage();

    List<AccessoryOutRecord> getAllAccessoryOutRecord();

    Integer updateFabricDetailIdBatch(@Param("fabricDetailList") List<FabricDetail> fabricDetailList);

    Integer updateFabricStorageIdBatch(@Param("fabricStorageList") List<FabricStorage> fabricStorageList);

    Integer updateFabricOutRecordIdBatch(@Param("fabricOutRecordList") List<FabricOutRecord> fabricOutRecordList);

    Integer updateAccessoryInStoreIdBatch(@Param("accessoryInStoreList") List<AccessoryInStore> accessoryInStoreList);

    Integer updateAccessoryStorageIdBatch(@Param("accessoryStorageList") List<AccessoryStorage> accessoryStorageList);

    Integer updateAccessoryOutRecordIdBatch(@Param("accessoryOutRecordList") List<AccessoryOutRecord> accessoryOutRecordList);

    Integer updateAccessoryAccountInStoreBatch(@Param("accessoryInStoreList") List<AccessoryInStore> accessoryInStoreList);

    Integer updateAccessoryInStorePriceBatch(@Param("accessoryInStoreList") List<AccessoryInStore> accessoryInStoreList);
}
