package com.example.erp01.mapper;

import com.example.erp01.model.CompletionRate;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

@Mapper
public interface CompletionRateMapper {

    List<CompletionRate> getEmpCountEachDay(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate, @Param("orderName") String orderName, @Param("groupName") String groupName);

    CompletionRate getOrderBeginDate(@Param("orderName")String orderName, @Param("groupName")String groupName);

    Integer getFinishCount(@Param("orderName")String orderName, @Param("groupName")String groupName);

    Integer getTodayActualCount(@Param("orderName")String orderName, @Param("groupName")String groupName, @Param("searchDate")Date searchDate);

    Integer getFinishCountByTime(@Param("orderName")String orderName, @Param("groupName")String groupName, @Param("searchDate")Date searchDate);

}
