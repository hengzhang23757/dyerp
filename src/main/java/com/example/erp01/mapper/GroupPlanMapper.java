package com.example.erp01.mapper;

import com.example.erp01.model.GroupPlan;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface GroupPlanMapper {

    int addGroupPlan(@Param("groupPlan") GroupPlan groupPlan);

    int deleteGroupPlan(@Param("groupPlanID") Integer groupPlanID);

    List<GroupPlan> getAllGroupPlan();

    GroupPlan getGroupPlanByOrderGroup(@Param("orderName")String orderName, @Param("groupName")String groupName);

    int updateGroupPlan(@Param("groupPlan") GroupPlan groupPlan);
}
