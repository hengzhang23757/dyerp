package com.example.erp01.mapper;

import com.example.erp01.model.DailyAction;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
@Mapper
public interface DailyActionMapper {

    Integer addDailyAction(@Param("dailyAction") DailyAction dailyAction);

    DailyAction getDailyActionByInfo(@Param("orderName") String orderName, @Param("groupName") String groupName, @Param("workDate") Date workDate);

    Integer updateDailyAction(@Param("dailyAction") DailyAction dailyAction);

    Integer getSumLooseFabricCountByTask(@Param("taskID") Integer taskID);
}
