package com.example.erp01.mapper;

import com.example.erp01.model.*;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.omg.PortableInterceptor.INACTIVE;

import java.util.List;
import java.util.Map;

@Mapper
//@Repository
public interface EmbStorageMapper {

    int embInStore(List<EmbStorage> embStorageList);

    int embOutStore(List<String> embOutStoreList);

    int embChangeStore(String embChangeStoreJson);

    List<EmbStorageState> getEmbStorageState();

    List<StorageFloor> getEmbStorageFloor();

    List<EmbStorageQuery> embStorageQuery(@Param("orderName")String orderName, @Param("colorName")String colorName, @Param("sizeName")String sizeName);

    List<String> getEmbOrderHint(String subOrderName);

    List<String> getEmbColorHint(@Param("orderName") String orderName);

    List<String> getEmbSizeHint(@Param("orderName") String orderName, @Param("colorName") String colorName);

    int embOutStoreOne(String subTailorQcode);

    String getLocationByQcode(@Param("tailorQcode") String tailorQcode);

    List<EmbStorage> getEmbStorageByOrderColorSize(@Param("embStoreLocation") String embStoreLocation, @Param("orderName") String orderName, @Param("colorName") String colorName, @Param("sizeName") String sizeName);

    Integer embOutStoreBatch(@Param("embStoreLocation") String embStoreLocation, @Param("orderName") String orderName, @Param("colorName") String colorName, @Param("sizeName") String sizeName);

    String getLocationByOrderColorSizePackage(@Param("orderName") String orderName, @Param("bedNumber") Integer bedNumber, @Param("colorName") String colorName,@Param("sizeName") String sizeName, @Param("packageNumber") Integer packageNumber);

    List<EmbStorage> getEmbStorageByEmbStoreLocation(@Param("embStoreLocation") String embStoreLocation);

    List<EmbStorage> getAllEmbStorage();

    List<EmbStorage> getEmbStorageByOrderColorSizeLocation(@Param("orderName") String orderName, @Param("colorName") String colorName, @Param("sizeName") String sizeName, @Param("embStoreLocation") String embStoreLocation);

    Integer embOutStoreSingle(@Param("embOutInfoList") List<EmbOutInfo> embOutInfoList);

    Integer deleteEmbStorageByInfo(@Param("orderName") String orderName, @Param("colorName") String colorName, @Param("sizeName") String sizeName, @Param("embStoreLocation") String embStoreLocation);

    Integer deleteEmbStorageById(@Param("embStorageID") Integer embStorageID);

    Integer deleteEmbStorageByOrderBedPackage(@Param("orderName") String orderName, @Param("bedNumber") Integer bedNumber, @Param("packageNumber") Integer packageNumber);

    List<EmbStorage> getEmbStorageGroupByColor(@Param("orderName") String orderName);

    List<EmbStorage> getEmbStorageSummaryByLocation(@Param("embStoreLocation") String embStoreLocation);

    List<EmbStorage> getEmbStorageDetailByOrderColorSize(@Param("orderName") String orderName, @Param("colorName") String colorName, @Param("sizeName") String sizeName);

    List<EmbStorage> getEmbStorageByOrderColorSizeList(@Param("orderName") String orderName, @Param("colorList") List<String> colorList, @Param("sizeList") List<String> sizeList);

    List<EmbStorage> getEmbStorageByInfo(@Param("orderName") String orderName, @Param("colorName") String colorName, @Param("sizeName") String sizeName);
}
