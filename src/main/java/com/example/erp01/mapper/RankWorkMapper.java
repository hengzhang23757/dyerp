package com.example.erp01.mapper;

import com.example.erp01.model.GeneralSalary;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
@Mapper
public interface RankWorkMapper {

    List<GeneralSalary> getGroupRank(@Param("groupName")String groupName);
}
