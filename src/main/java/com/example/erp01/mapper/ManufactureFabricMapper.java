package com.example.erp01.mapper;

import com.example.erp01.model.AddFeeVo;
import com.example.erp01.model.FabricAccessoryReview;
import com.example.erp01.model.ManufactureFabric;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface ManufactureFabricMapper {

    Integer addManufactureFabricBatch(@Param("manufactureFabricList") List<ManufactureFabric> manufactureFabricList);

    List<ManufactureFabric> getManufactureFabricByOrder(@Param("orderName") String orderName, @Param("colorName") String colorName);

    Integer deleteManufactureFabricByName(@Param("orderName") String orderName);

    Integer deleteManufactureFabricByID(@Param("fabricID") Integer fabricID);

    Integer updateManufactureFabric(@Param("manufactureFabric") ManufactureFabric manufactureFabric);

    List<ManufactureFabric> getManufactureFabricHintByFabricName(@Param("subFabricName") String subFabricName, @Param("page") Integer page, @Param("limit") Integer limit);

    Integer updateManufactureFabricOnPlaceOrder(@Param("manufactureFabricList") List<ManufactureFabric> manufactureFabricList);

    List<String> getDistinctSupplier();

    List<ManufactureFabric> getManufactureFabricOneYear(@Param("orderName") String orderName);

    ManufactureFabric getManufactureFabricByID(@Param("fabricID") Integer fabricID);

    List<ManufactureFabric> getManufactureFabricByInfo(@Param("orderName") String orderName, @Param("fabricName") String fabricName, @Param("colorName") String colorName, @Param("fabricColor") String fabricColor);

    List<ManufactureFabric> getManufactureFabricInPlaceOrder(@Param("orderName") String orderName, @Param("supplier") String supplier, @Param("checkState") String checkState);

    Integer changeFabricOrderStateInPlaceOrder(@Param("fabricIDList") List<Integer> fabricIDList);

    List<ManufactureFabric> getManufactureFabricSummaryByOrder(@Param("orderName") String orderName);

    Integer updateManufactureFabricBatch(@Param("manufactureFabricList") List<ManufactureFabric> manufactureFabricList);

    Integer changeFabricCheckStateInPlaceOrder(@Param("fabricIDList") List<Integer> fabricIDList, @Param("preCheck") String preCheck, @Param("checkState") String checkState, @Param("preCheckEmp") String preCheckEmp, @Param("checkEmp") String checkEmp);

    List<ManufactureFabric> getManufactureFabricByOrderCheckState(@Param("orderName") String orderName, @Param("checkState") String checkState);

    List<ManufactureFabric> getManufactureFabricByOrderListCheckState(@Param("orderNameList") List<String> orderNameList, @Param("checkState") String checkState);

    List<String> getFabricNameHint(@Param("subName") String subName);

    List<ManufactureFabric> getManufactureFabricInPlaceOrderByInfo(@Param("orderNameList") List<String> orderNameList, @Param("supplier") String supplier, @Param("materialName") String materialName, @Param("checkState") String checkState);

    List<FabricAccessoryReview> getManufactureFabricReviewState(@Param("checkState") String checkState);

    List<ManufactureFabric> getFabricCheckStateRecentYearByOrder(@Param("orderNameList") List<String> orderNameList);

    List<ManufactureFabric> getManufactureFabricByCheckNumberState(@Param("checkNumber") String checkNumber, @Param("checkState") String checkState);

    Integer destroyFabricCheck(@Param("checkNumber") String checkNumber);

    Integer getFabricCheckNumberCountByState(@Param("checkState") String checkState);

    ManufactureFabric getOneManufactureFabricByInfo(@Param("orderName") String orderName, @Param("colorName") String colorName, @Param("fabricName") String fabricName);

    Integer updateManufactureFabricAfterCommit(@Param("manufactureFabricList") List<ManufactureFabric> manufactureFabricList);

    Integer updateFabricOrderStateByCheckNumber(@Param("checkNumber") String checkNumber);

    List<String> getDistinctFabricNameByOrder(@Param("orderName") String orderName);

    Integer deleteManufactureFabricBatch(@Param("fabricIDList") List<Integer> fabricIDList);

    List<ManufactureFabric> getManufactureFabricByFabricIdList(@Param("fabricIDList") List<Integer> fabricIDList);

    Integer reAddManufactureFabric(@Param("manufactureFabric") ManufactureFabric manufactureFabric);

    List<String> getFabricCheckNumberHint(@Param("keyWord") String keyWord);


    List<FabricAccessoryReview> getManufactureFabricReviewStateCombine(Map<String, Object> map);

    void changeFeeStateBatch(@Param("fabricIDList") List<Integer> fabricIDList, @Param("feeState") Integer feeState);

    List<AddFeeVo> getFeeVoByInfo(Map<String, Object> map);

    void changeOrderName(@Param("orderName") String orderName, @Param("toOrderName") String toOrderName);
}
