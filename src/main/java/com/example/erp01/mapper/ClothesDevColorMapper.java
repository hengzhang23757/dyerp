package com.example.erp01.mapper;

import com.example.erp01.model.ClothesDevColor;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface ClothesDevColorMapper {

    Integer addClothesDevColorBatch(@Param("clothesDevColorList") List<ClothesDevColor> clothesDevColorList);

    List<ClothesDevColor> getClothesDevColorByMap(Map<String, Object> map);

    Integer deleteClothesDevColorByDevId(@Param("devId") Integer devId);

    Integer deleteClothesDevColorByDevNumber(@Param("clothesDevNumber") String clothesDevNumber);

}
