package com.example.erp01.mapper;

import com.example.erp01.model.CutStorageQuery;
import com.example.erp01.model.Storage;
import com.example.erp01.model.StorageFloor;
import com.example.erp01.model.StorageState;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
//@Repository
public interface StorageMapper {

    int inStore(List<Storage> storageList);

    int outStore(List<String> subQcodeList);

    int outStoreOne(String subTailorQcode);

    int changeStore(String changestoreJson);

    List<StorageState> getStorageState();

    List<StorageFloor> getStorageFloor();

    List<String> storageReport(String storehouseLocation);

    List<CutStorageQuery> getCutStorageByOrder(@Param("orderName") String orderName, @Param("colorName") String colorName, @Param("sizeName") String sizeName);

    List<String> getStorageColorHint(@Param("orderName") String orderName);

    List<String> getStorageSizeHint(@Param("orderName") String orderName, @Param("colorName") String colorName);

    int deleteStorageOnFinish(@Param("orderName") String orderName, @Param("bedNumber") Integer bedNumber, @Param("colorName") String colorName, @Param("sizeName") String sizeName, @Param("storehouseLocation") String storehouseLocation);

    List<Storage> getStorageByStoreHouseLocation(@Param("storehouseLocation") String storehouseLocation);

    List<Storage> getAllStorage();

    List<Storage> getStorageByOrderColorSizeLocation(@Param("orderName") String orderName, @Param("colorName") String colorName, @Param("sizeName") String sizeName, @Param("storehouseLocation") String storehouseLocation);

    Integer deleteStorageById(@Param("storageID") Integer storageID);

    Integer deleteStorageByInfo(@Param("orderName") String orderName, @Param("bedNumber") Integer bedNumber, @Param("packageNumber") Integer packageNumber);

    List<String> getMatch(@Param("orderName") String orderName, @Param("bedNumber") Integer bedNumber, @Param("packageNumber") Integer packageNumber);

    List<Storage> getStorageByInfo(@Param("orderName") String orderName, @Param("colorName") String colorName, @Param("sizeName") String sizeName);
}
