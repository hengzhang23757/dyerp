package com.example.erp01.mapper;

import com.example.erp01.model.*;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

@Mapper
public interface FabricReportMapper {

    List<FabricPlan> getFabricPlanByOrder(@Param("orderName") String orderName);

    List<FabricDetail> getFabricDetailByOrder(@Param("orderName") String orderName, @Param("colorName") String colorName);

    List<FabricDetail> getAllTypeFabricDetailByOrder(@Param("orderName") String orderName, @Param("colorName") String colorName);

    List<FabricStorage> getFabricStorageByOrder(@Param("orderName") String orderName, @Param("colorName") String colorName);

    List<FabricOutRecord> getFabricOutRecordByOrder(@Param("orderName") String orderName, @Param("colorName") String colorName);

    List<LooseFabric> getLooseFabricByInfo(@Param("from") Date from, @Param("to") Date to, @Param("orderName") String orderName);

}
