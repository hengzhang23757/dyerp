package com.example.erp01.mapper;

import com.example.erp01.model.HomePage;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface HomePageMapper {

    List<HomePage> getUnFinishOrder();

    Integer addHomePageDataBatch(@Param("homePageList")List<HomePage> homePageList);

    Integer updateHomePageData(@Param("homePageList")List<HomePage> homePageList);

    List<HomePage> getHomePageRecentThreeMonth();

    Integer deleteHomePageDataByOrderName(@Param("orderName")String orderName);

}
