package com.example.erp01.mapper;

import com.example.erp01.model.StyleImage;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;


@Mapper
public interface StyleImageMapper {

    Integer addStyleImage(@Param("styleImage") StyleImage styleImage);

    StyleImage getStyleImageByOrder(@Param("orderName") String orderName);

    void changeOrderName(@Param("orderName") String orderName, @Param("toOrderName") String toOrderName);
}
