package com.example.erp01.mapper;

import com.example.erp01.model.DesignDepart;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface DesignDepartMapper {

    Integer addDesignDepart(@Param("designDepart") DesignDepart designDepart);

    Integer deleteColorManage(@Param("id") Integer id);

    List<DesignDepart> getAllDesignDepart();

    Integer updateDesignDepart(@Param("designDepart") DesignDepart designDepart);

}
