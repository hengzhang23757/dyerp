package com.example.erp01.mapper;

import com.example.erp01.model.LooseFabric;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface LooseFabricMapper {

    Integer saveLooseFabricData(@Param("looseFabricList") List<LooseFabric> looseFabricList);

    Integer getFinishLooseFabricCount(@Param("styleNumber")String styleNumber, @Param("colorName")String colorName, @Param("fabricName")String fabricName, @Param("fabricColor")String fabricColor, @Param("jarNumber")String jarNumber);

    LooseFabric getLooseFabricByID(@Param("qCodeID")Integer qCodeID);

    List<LooseFabric> getLooseFabricByOrderFabric(@Param("orderName")String orderName, @Param("colorName")String colorName, @Param("fabricName")String fabricName, @Param("fabricColor")String fabricColor);

    Integer updateLooseFabric(@Param("looseFabricList") List<LooseFabric> looseFabricList);

    List<LooseFabric> getMaxLooseFabricInfoByJar(@Param("orderName")String orderName, @Param("fabricName")String fabricName, @Param("fabricColor")String fabricColor);

    List<LooseFabric> getFabricJarByInfo(@Param("orderName")String orderName, @Param("colorName")String colorName, @Param("fabricName")String fabricName, @Param("fabricColor")String fabricColor);

    Integer updateJarByInfo(@Param("orderName")String orderName, @Param("fabricName")String fabricName, @Param("fabricColor")String fabricColor, @Param("jarNumber")String jarNumber, @Param("colorName")String colorName, @Param("updateJar")String updateJar);

    List<LooseFabric> getMaxLooseFabricInfoByInfo(@Param("orderName")String orderName, @Param("colorName")String colorName, @Param("fabricName")String fabricName, @Param("fabricColor")String fabricColor);

    Integer getLooseCountByOrder(@Param("orderName")String orderName, @Param("colorNameList") List<String> colorNameList, @Param("cutState")Integer cutState);

    Integer changeStateInTailorScan(@Param("qCodeID")Integer qCodeID);

    Float getPreCutFabricWeightByOrderColor(@Param("orderName")String orderName, @Param("colorName")String colorName, @Param("jarNumber")String jarNumber);

    Integer deleteLooseFabricByOrderNameJar(@Param("orderName")String orderName, @Param("jarNumber")String jarNumber);
}
