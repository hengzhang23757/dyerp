package com.example.erp01.mapper;

import com.example.erp01.model.*;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Mapper
public interface PieceWorkMapper {

    int addPieceWorkBatch(List<PieceWork> pieceWorkList);

    int addPieceWork(PieceWork pieceWork);

    int deletePieceWork(Integer id);

    PieceWork getPieceWorkByID(Integer id);

    List<PieceWork> getAllPieceWork();

    List<PieceWork> getPieceWorkByEmpNum(String employeeNumber);

    List<PieceWork> getPieceWorkByTime(Date from, Date to);

    List<PieceWork> getPieceWorkByEmpNumTime(String employeeNumber, Date from, Date to);

    List<PieceWork> getDetailPieceWork(Date from, Date to, String groupName, String employeeNumber);

    List<PieceWorkResult> getPieceWorkToday();

    List<PieceWorkResult> getPieceWorkThisMonth();

    List<PieceWork> getPieceWorkEmpToday(@Param("employeeNumber") String employeeNumber, @Param("from") String from, @Param("to") String to);

    List<PieceWork> getPieceWorkEmpThisMonth(String employeeNumber);

    List<Object> queryPieceWorkToday(String groupName, String employeeNumber);

    List<Object> queryPieceWorkThisMonth(String groupName, String employeeNumber);

    List<SalaryCount> queryPieceWork(Date from, Date to, String groupName, String employeeNumber);

    List<PieceWork> getTodaySummary(@Param("employeeNumber") String employeeNumber, @Param("from") String from, @Param("to") String to);

    List<PieceWork> getThisMonthSummary(String employeeNumber);

    List<PieceWork> getPieceWorkByOrderBedPackID(@Param("orderName")String orderName, @Param("bedNumber")Integer bedNumber, @Param("packageNumber")Integer packageNumber, @Param("procedureNumber")Integer procedureNumber, @Param("tailorQcodeID")Integer tailorQcodeID);

    List<PieceWork> getPieceWorkByOrderBedPack(@Param("orderName")String orderName, @Param("bedNumber")Integer bedNumber, @Param("packageNumber")Integer packageNumber, @Param("procedureNumber")Integer procedureNumber);

    int addPieceWorkBatchNew(List<PieceWork> pieceWorkList);

    int addPieceWorkTotal(PieceWork pieceWork);

    List<MiniDetailQuery> getDetailProductionByEmp(Date from, Date to, String employeeNumber, String orderName, String colorName, String sizeName);

    List<GeneralSalary> getMiniSalaryByTimeEmp(Date from, Date to, String employeeNumber);

    List<GeneralSalary> getGroupMiniSalaryByTime(@Param("from")Date from,@Param("to") Date to,@Param("groupName") String groupName);

    List<PieceWorkEmp> getProcedureInfoByOrderBedPackage(@Param("orderName")String orderName, @Param("bedNumber")Integer bedNumber, @Param("packageNumber")Integer packageNumber);

    List<ManualInput> getAllManualInput();

    List<Unload> getAllUnloadRecord();

    Integer getPieceCountByOrderColorSize(@Param("orderName")String orderName,@Param("procedureNumber") Integer procedureNumber, @Param("colorName")String colorName, @Param("sizeName")String sizeName);

    List<SalaryCount> getMiniProcedureProgress(@Param("from")Date from, @Param("to")Date to, @Param("procedureFrom")Integer procedureFrom, @Param("procedureTo")Integer procedureTo,@Param("orderName")String orderName,@Param("groupName")String groupName);

    List<CutLocation> getPieceWorkInfo(@Param("orderName") String orderName, @Param("bedNumber") Integer bedNumber, @Param("colorName") String colorName, @Param("sizeName") String sizeName);

    List<WeekAmount> getPieceWeekAmount();

    List<WeekAmount> getFinishWeekAmount();

    List<CompletionCount> getPieceWorkCompletion(@Param("procedureNumber") Integer procedureNumber);

    List<SalaryCount> getOrderMiniSalarySummary(@Param("orderName") String orderName,@Param("from")Date from,@Param("to") Date to);

    List<ProductionProgressDetail> getUnloadByOrderTime(@Param("orderName")String orderName, @Param("from")Date from, @Param("to")Date to);

    int updateManualInput(PieceWork pieceWork);

    List<ManualInput> getTodayManualInput();

    List<ManualInput> getOneMonthManualInput();

    List<ManualInput> getThreeMonthsManualInput();

    List<PieceWorkResult> searchPieceWorkManagement(@Param("orderName") String orderName,@Param("colorName") String colorName,@Param("sizeName") String sizeName,@Param("bedNumber") Integer bedNumber,@Param("packageNumber") Integer packageNumber,@Param("procedureNumber") Integer procedureNumber,@Param("page") Integer page,@Param("limit") Integer limit);

    Integer deletePieceWorkBatch(@Param("pieceWorkIDList")List<Integer> pieceWorkIDList);

    List<GeneralSalary> getMiniProcedureProgressNew(@Param("from")Date from, @Param("to")Date to, @Param("procedureFrom")Integer procedureFrom, @Param("procedureTo")Integer procedureTo,@Param("orderName")String orderName,@Param("groupName")String groupName, @Param("employeeNumber")String employeeNumber);

    List<CutLocation> getMatchInfo(@Param("orderName") String orderName, @Param("bedNumber") Integer bedNumber, @Param("colorName") String colorName, @Param("sizeName") String sizeName);

    List<GeneralSalary> getMiniSalaryDetail(@Param("from")Date from, @Param("to") Date to, @Param("employeeNumber") String employeeNumber, @Param("orderName") String orderName);

    List<GeneralSalary> getMiniSalaryByOrderColorSizeEmp(@Param("from")Date from, @Param("to") Date to, @Param("employeeNumber") String employeeNumber, @Param("orderName") String orderName, @Param("colorName") String colorName, @Param("sizeName") String sizeName);

    List<GeneralSalary> getMiniProcedureProgressMultiGroup(@Param("from")Date from, @Param("to")Date to, @Param("procedureFrom")Integer procedureFrom, @Param("procedureTo")Integer procedureTo,@Param("orderName")String orderName,@Param("groupList")List<String> groupList, @Param("employeeNumber")String employeeNumber);

    List<GeneralSalary> getDetailProductionByInfo(@Param("from")String from, @Param("to") String to, @Param("employeeNumber") String employeeNumber, @Param("orderName") String orderName, @Param("colorName") String colorName, @Param("sizeName") String sizeName);

    List<GeneralSalary> getDetailProductionOfEachLine(@Param("pieceDate")Date pieceDate, @Param("employeeNumber") String employeeNumber, @Param("orderName") String orderName, @Param("procedureNumber") Integer procedureNumber, @Param("colorName") String colorName, @Param("sizeName") String sizeName);

    List<GeneralSalary> getDetailProductionSummaryByInfo(@Param("from")String from, @Param("to") String to, @Param("employeeNumber") String employeeNumber, @Param("orderName") String orderName, @Param("colorName") String colorName, @Param("sizeName") String sizeName);

    List<GeneralSalary> getDetailProductionDetailByInfo(@Param("from")String from, @Param("to") String to, @Param("employeeNumber") String employeeNumber, @Param("orderName") String orderName, @Param("colorName") String colorName, @Param("sizeName") String sizeName);

    Integer updateProcedureNameByOrderProcedure(@Param("orderName") String orderName, @Param("procedureNumber") Integer procedureNumber, @Param("procedureName") String procedureName);

    List<GeneralSalary> getPieceInfoGroupByOrderColor(@Param("orderName") String orderName, @Param("procedureNumber") Integer procedureNumber);

    List<PieceWork> getPieceWorkByOrderNameProcedureNumber(@Param("orderName") String orderName, @Param("procedureNumber") Integer procedureNumber, @Param("colorName") String colorName, @Param("sizeName") String sizeName);

    Integer updateGroupNameByEmployee(@Param("from")Date from, @Param("to") Date to, @Param("employeeNumber") String employeeNumber, @Param("groupName") String groupName);

    List<GeneralSalary> getGroupPieceWorkSummary(@Param("from")String from, @Param("to") String to, @Param("orderName") String orderName, @Param("groupName") String groupName);

    List<PieceWork> getPieceWorkByOrderNameBedNumberPackage(@Param("orderName") String orderName, @Param("bedNumber") Integer bedNumber, @Param("packageNumber") Integer packageNumber);

    Integer getPieceCountOfToday(@Param("employeeNumber") String employeeNumber);

    List<GeneralSalary> getPackageDetailByInfo(@Param("from")Date from, @Param("to") Date to, @Param("employeeNumber") String employeeNumber);

    List<CutQueryLeak> getProductionByProcedureColorSize(@Param("orderName") String orderName, @Param("procedureNumber") Integer procedureNumber, @Param("colorName") String colorName, @Param("sizeName") String sizeName);

    Integer addPieceWorkTotalBatch(@Param("pieceWorkList") List<PieceWork> pieceWorkList);

    Integer updatePieceWorkLayerCountFinance(@Param("pieceWorkID") Integer pieceWorkID, @Param("layerCount") Integer layerCount);

    List<ManualInput> getByInfo(Map<String, Object> map);


}
