package com.example.erp01.mapper;

import com.example.erp01.model.FabricDetail;
import com.example.erp01.model.LooseFabric;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface FabricDetailMapper {

    int addFabricDetailBatch(@Param("fabricDetailList") List<FabricDetail> fabricDetailList);

    int addFabricDetail(@Param("fabricDetail")FabricDetail fabricDetail);

    int deleteFabricDetailByID(@Param("fabricDetailID") Integer fabricDetailID);

    int updateFabricDetail(@Param("fabricDetail") FabricDetail fabricDetail);

    List<String> getJarNameByOrderFabricColor(@Param("orderName") String orderName, @Param("fabricName") String fabricName, @Param("fabricColor") String fabricColor);

    Integer getFabricBatchNumberByOrderFabricJar(@Param("orderName") String orderName, @Param("colorName") String colorName, @Param("fabricName") String fabricName, @Param("fabricColor") String fabricColor, @Param("jarName") String jarName);

    List<FabricDetail> getTodayFabricDetail();

    List<FabricDetail> getOneMonthFabricDetail();

    List<FabricDetail> getOneYearFabricDetail();

    List<String> getFabricLocationHint(@Param("orderName") String orderName, @Param("colorName") String colorName, @Param("fabricName") String fabricName, @Param("fabricColor") String fabricColor, @Param("jarName") String jarName);

    List<String> getJarNameByOrder(@Param("orderName") String orderName);

    List<LooseFabric> getFabricBatchInfoOfEachJar(@Param("orderName") String orderName, @Param("fabricName") String fabricName, @Param("fabricColor") String fabricColor);

    List<FabricDetail> getFabricDetailByInfo(@Param("orderName") String orderName, @Param("fabricName") String fabricName, @Param("colorName") String colorName, @Param("fabricColor") String fabricColor, @Param("fabricID") Integer fabricID);

    List<String> getColorNamesByOrder(@Param("orderName")String orderName);

    List<String> getFabricNamesByOrderColor(@Param("orderName")String orderName, @Param("colorName")String colorName);

    List<String> getFabricColorsByOrderColorFabric(@Param("orderName")String orderName, @Param("colorName")String colorName, @Param("fabricName")String fabricName);

    List<LooseFabric> getJarInfoByOrderColorFabricColor(@Param("orderName")String orderName, @Param("colorName")String colorName, @Param("fabricName")String fabricName, @Param("fabricColor")String fabricColor);

    String getFabricUnitByInfo(@Param("orderName")String orderName, @Param("colorName")String colorName, @Param("fabricName")String fabricName, @Param("fabricColor")String fabricColor);

    FabricDetail getTransFabricDetailByInfo(@Param("orderName")String orderName, @Param("colorName")String colorName, @Param("fabricName")String fabricName, @Param("fabricColor")String fabricColor, @Param("jarName")String jarName, @Param("location")String location);

    List<FabricDetail> getFabricDetailByOrder(@Param("orderName")String orderName);

    FabricDetail getFabricDetailById(@Param("fabricDetailID") Integer fabricDetailID);

    Integer getFabricBatchNumberByInfo(@Param("orderName") String orderName, @Param("colorNameList") List<String> colorNameList);

    List<FabricDetail> getFabricDetailByTime(@Param("from") String from, @Param("to") String to);

    List<FabricDetail> getFabricDetailByFabricIDList(@Param("fabricIDList") List<Integer> fabricIDList);

    FabricDetail getFabricDetailByLocationJarFabricID(@Param("location")String location, @Param("jarName")String jarName, @Param("fabricID")Integer fabricID);

    Float getHistoryFabricReturnWeight(@Param("fabricID") Integer fabricID);

    List<FabricDetail> getFabricDetailByOperateType(@Param("operateType") String operateType);

    List<FabricDetail> getFabricDetailSummaryByFabricIdList(@Param("fabricIDList") List<Integer> fabricIDList);

    List<Integer> getFabricIdListByAddFee(@Param("fabricDetailIDList") List<Integer> fabricDetailIDList);



    // 暂存面料
    Integer addFabricDetailTmpBatch(@Param("fabricDetailList") List<FabricDetail> fabricDetailList);

    List<FabricDetail> getFabricDetailTmpByFabricId(@Param("fabricID") Integer fabricID, @Param("orderName") String orderName, @Param("colorName") String colorName);

    Integer deleteFabricDetailTmpById(@Param("fabricDetailID") Integer fabricDetailID);

    List<FabricDetail> getAllTmpFabricDetail(@Param("orderName") String orderName);

    Integer updateTmpFabricDetail(@Param("fabricDetail") FabricDetail fabricDetail);

    FabricDetail getTmpFabricById(@Param("fabricDetailID") Integer fabricDetailID);

    Integer deleteFabricDetailByOrderJar(@Param("orderName") String orderName, @Param("jarName") String jarName);
}
