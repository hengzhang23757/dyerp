package com.example.erp01.mapper;

import com.example.erp01.model.Message;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface MessageMapper {

    Integer addMessageBatch(@Param("messageList") List<Message> messageList);

    List<Message> getMessageByInfo(@Param("receiveUser") String receiveUser, @Param("messageType") String messageType);

    Integer updateMessageReadType(@Param("idList") List<Integer> idList);

    Integer getReadStateCountByMessageType(@Param("receiveUser") String receiveUser, @Param("messageType") String messageType);

    List<Message> getMessageCountGroupByType(@Param("receiveUser") String receiveUser);

    Message getMessageById(@Param("id") Integer id);

    Integer updateMessageReadTypeByReceiveUser(@Param("receiveUser") String receiveUser, @Param("messageType") String messageType);

    List<Message> getSendedMessage(String sendUser);

    Integer countBySender(String sendUser);

    List<Message> getSendedMessageByPage(@Param("sendUser")String sendUser,@Param("offset") Integer offset,@Param("limit") Integer limit);

    List<Message> getUnFinishMessageByType(Map<String, Object> map);

    List<Message> getMessageContentByInfo(Map<String, Object> map);

    Integer updateMessageBatch(@Param("messageList") List<Message> messageList);

    Integer getUnFinishCountTotalCount();

}
