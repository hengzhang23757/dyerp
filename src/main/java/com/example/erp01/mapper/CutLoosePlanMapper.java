package com.example.erp01.mapper;

import com.example.erp01.model.CutLoosePlan;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface CutLoosePlanMapper {

    Integer addCutLoosePlan(@Param("cutLoosePlan")CutLoosePlan cutLoosePlan);

    Integer updateCutLoosePlan(@Param("cutLoosePlan")CutLoosePlan cutLoosePlan);

    Integer updateCutLoosePlanBatch(@Param("cutLoosePlanList")List<CutLoosePlan> cutLoosePlanList);

    Integer deleteCutLoosePlan(@Param("id") Integer id);

    List<CutLoosePlan> getCutLoosePlanByFinishState(@Param("finishState") String finishState);

    Integer updateFinishState(@Param("id") Integer id);

}
