package com.example.erp01.mapper;

import com.example.erp01.model.CheckDetail;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

@Mapper
public interface CheckDetailMapper {

    List<CheckDetail> getCheckDetailByInfo(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate, @Param("employeeNumber") String employeeNumber);

    List<CheckDetail> getYesterdayCheckDetail();

    Integer updateCheckDetail(@Param("checkDetail") CheckDetail checkDetail);

}
