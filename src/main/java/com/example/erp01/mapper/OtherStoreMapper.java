package com.example.erp01.mapper;

import com.example.erp01.model.OtherStore;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
//@Repository
public interface OtherStoreMapper {

    OtherStore getOtherStoreByLocation(@Param("otherStoreLocation") String otherStoreLocation);

}
