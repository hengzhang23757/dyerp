package com.example.erp01.mapper;

import com.example.erp01.model.OrderProcedure;
import com.example.erp01.model.OrderProcedureDetail;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface OrderProcedureDetailMapper {

    int addOrderProcedureDetail(List<OrderProcedureDetail> orderProcedureDetailList);

    int deleteOrderProcedureByOrderName(@Param("orderName") String orderName);

    List<OrderProcedureDetail> getOrderProcedureDetailByOrderName(@Param("orderName") String orderName);

    int updateOrderProcedureDetailBatch(@Param("orderProcedureDetailList") List<OrderProcedureDetail> orderProcedureDetailList);

    int deleteOrderProcedureDetailBatch(@Param("idList") List<Integer> idList);
}
