package com.example.erp01.mapper;

import com.example.erp01.model.Employee;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface EmployeeMapper {

    int addEmployee(Employee employee);

    Employee employeeLogin(@Param("employeeNumber")String employeeNumber, @Param("passWord")String passWord);

    int deleteEmployee(Integer employeeID);

    Employee getEmpByID(Integer employeeID);

    int updateEmployee(Employee employee);

    List<String> getEmpHint(String subEmpNumber);

    List<Employee> getAllEmployee();

    String getEmpNameByEmpNum(String employeeNumber);

    String getGroupNameByEmpNum(String employeeNumber);

    List<Employee> getEmployeeByGroup(String groupName);

    int updatePassWord(@Param("employeeNumber")String employeeNumber, @Param("passWord")String passWord);

    List<Employee> getEmpByEmpNumber(@Param("employeeNumber")String employeeNumber);

    List<String> getEmpNameHint(@Param("subEmpName") String subEmpName);

    List<String> getEmpNumbersByName(@Param("employeeName") String employeeName);

    List<Employee> getEmpByIdentifyCard(@Param("identifyCard")String identifyCard);

    String getPositionByEmployeeNumber(@Param("employeeNumber")String employeeNumber);

    List<Employee> getEmployeeByGroups(@Param("groupList")List<String> groupList);

    List<Employee> getAllWorkEmployee();

    List<Employee> getEmployeeByPositions(@Param("positionList")List<String> positionList);

    Integer getEmployeeCountByGroup(@Param("groupName")String groupName);

    List<String> getAllGroupName();

    List<Employee> getAllInStateEmployee();

    List<Employee> getAllOffStateEmployee();

    List<Employee> getCheckEmployee();

    List<Employee> getEmployeeInfoHit(@Param("keyWord")String keyWord, @Param("page")Integer page, @Param("limit")Integer limit);

    Integer getEmployeeCountOfHint(@Param("keyWord")String keyWord);


    List<Employee> getProduceEmployeeByGroup(@Param("groupName")String groupName);

    List<Employee> getEmpByEmpNumberList(@Param("empList")List<String> empList);
}
