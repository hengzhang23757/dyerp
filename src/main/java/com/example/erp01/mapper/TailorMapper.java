package com.example.erp01.mapper;

import com.example.erp01.model.*;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Mapper
//@Repository
public interface TailorMapper {

    int saveTailorData(List<Tailor> tailorList);

    //由订单号获取当前最大床号
    Integer getMaxBedNumber(String orderName);

    Integer getMaxPackageNumberByOrder(@Param("orderName") String orderName, @Param("tailorType") Integer tailorType);

    int deleteTailor(Integer tailorID);

    int deleteTailorByOrderBed(@Param("orderName")String orderName,@Param("bedNumber")Integer bedNumber);

    int deleteTailorByOrderBedPack(Map map);

    List<Integer> getPackageNumbersByOrderBedColor(@Param("orderName")String orderName, @Param("bedNumber")Integer bedNumber, @Param("colorName")String colorName);

    Integer updateTailorData(@Param("tailorList")List<Tailor> tailorList);

    Tailor getTailorByTailorQcodeID(@Param("tailorQcodeID") Integer tailorQcodeID, @Param("tailorType")Integer tailorType);

    List<Tailor> getTailorsByTailorQcodeIDs(@Param("tailorQcodeIDList") List<Integer> tailorQcodeIDList, @Param("tailorType")Integer tailorType);

    List<Tailor> getBedInfoInDelete(@Param("orderName")String orderName);

    List<Tailor> getPackageInfoInDelete(@Param("orderName") String orderName,@Param("bedNumber") Integer bedNumber);

    List<Integer> getBedNumbersByOrderNamePartNameTailorType(@Param("orderName")String orderName, @Param("partName")String partName, @Param("tailorType")Integer tailorType);

    List<TailorReport> tailorReport(@Param("orderName") String orderName, @Param("bedNumber") Integer bedNumber, @Param("tailorType") Integer tailorType);

    List<CutQueryLeak> getTailorInfoByOrderPartType(@Param("orderName") String orderName, @Param("partName") String partName, @Param("tailorType") Integer tailorType, @Param("from") String from, @Param("to") String to);

    List<TailorMonthReport> tailorMonthReport(@Param("from")Date from, @Param("to")Date to, @Param("orderName")String orderName, @Param("partName")String partName, @Param("tailorType")Integer tailorType, @Param("groupName")String groupName);

    List<TailorMonthReport> otherTailorMonthReport(@Param("from")Date from, @Param("to")Date to, @Param("orderName")String orderName, @Param("partName")String partName, @Param("tailorType")Integer tailorType, @Param("groupName")String groupName);

    List<Tailor> getTailorInfoGroupByColorSizePartType(@Param("orderName")String orderName, @Param("partName")String partName, @Param("tailorType")Integer tailorType);

    List<Tailor> getTailorInfoGroupByColorPartType(@Param("orderName")String orderName, @Param("partName")String partName, @Param("tailorType")Integer tailorType);

    List<Completion> getTailorCompletion(@Param("orderName")String orderName, @Param("partName")String partName, @Param("tailorType")Integer tailorType);

    List<CutLocation> getPackageInfo(@Param("orderName") String orderName,@Param("bedNumber") Integer bedNumber,@Param("colorName") String colorName,@Param("sizeName") String sizeName,@Param("packageNumber") Integer packageNumber);

    List<WeekAmount> getTailorWeekAmount(@Param("partName")String partName, @Param("tailorType")Integer tailorType);

    List<Tailor> getTailorByInfo(@Param("orderName")String orderName, @Param("colorName")String colorName, @Param("sizeName")String sizeName, @Param("partName")String partName, @Param("bedNumber")Integer bedNumber, @Param("packageNumber")Integer packageNumber, @Param("tailorType")Integer tailorType, @Param("isDelete")Integer isDelete);

    Integer getTailorCountByInfo(@Param("orderName")String orderName, @Param("colorName")String colorName, @Param("sizeName")String sizeName, @Param("partName")String partName, @Param("bedNumber")Integer bedNumber, @Param("packageNumber")Integer packageNumber, @Param("tailorType")Integer tailorType);

    Integer getWellCountByInfo(@Param("orderName")String orderName, @Param("colorName")String colorName, @Param("sizeName")String sizeName, @Param("partName")String partName, @Param("bedNumber")Integer bedNumber, @Param("packageNumber")Integer packageNumber, @Param("tailorType")Integer tailorType);

    Integer preDeleteInDeleteBed(@Param("orderName") String orderName,@Param("bedNumber") Integer bedNumber);

    Integer preDeleteInDeletePackage(Map map);

    Integer getWellCountByColorSizeList(@Param("orderName") String orderName, @Param("colorList") List<String> colorList, @Param("sizeList") List<String> sizeList);

    List<Tailor> getTailorByOrderPartNameColorSize(@Param("orderName") String orderName, @Param("tailorType")Integer tailorType, @Param("partName")String partName , @Param("colorName")String colorName , @Param("sizeName")String sizeName, @Param("isDelete")Integer isDelete);

    List<TailorMatch> getRecentWeekCutInfo();

    List<TailorMatch> getMainTailorMatchDataByInfo(@Param("orderNameList") List<String> orderNameList);

    List<TailorMatch> getOtherTailorMatchDataByInfo(@Param("orderNameList") List<String> orderNameList);

    List<CutQueryLeak> getTailorInfoGroupByColorSize(@Param("orderName") String orderName, @Param("colorName") String colorName, @Param("sizeName") String sizeName, @Param("partName") String partName, @Param("tailorType")Integer tailorType);

    Date getLastCutDateByOrderBed(@Param("orderName") String orderName, @Param("bedNumber") Integer bedNumber);

    Integer getMaxBedNumberByOrderTailorType(@Param("orderName") String orderName, @Param("tailorType") Integer tailorType);

    List<Tailor> monthTailorReportGroupByBedNumber(@Param("from") String from, @Param("to") String to, @Param("orderName") String orderName, @Param("groupName") String groupName);

    List<Tailor> otherMonthTailorReportGroupByBedNumber(@Param("from") String from, @Param("to") String to, @Param("orderName") String orderName, @Param("groupName") String groupName);

    Integer updateTailorLayerCountByInfo(@Param("orderName") String orderName, @Param("bedNumber") Integer bedNumber, @Param("packageNumber") Integer packageNumber, @Param("layerCount") Integer layerCount);

    List<Tailor> getTailorSummaryByOrderByOrderList(@Param("orderNameList") List<String> orderNameList);
}
