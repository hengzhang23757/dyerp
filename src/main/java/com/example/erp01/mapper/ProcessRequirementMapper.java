package com.example.erp01.mapper;

import com.example.erp01.model.ProcessRequirement;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ProcessRequirementMapper {

    Integer addProcessRequirementBatch(@Param("processRequirementList") List<ProcessRequirement> processRequirementList);

    List<ProcessRequirement> getProcessRequirementByOrder(@Param("orderName") String orderName);

    Integer deleteProcessRequirementByOrder(@Param("orderName") String orderName);

    Integer deleteProcessRequirementByID(@Param("processRequirementID") Integer processRequirementID);

    Integer updateProcessRequirement(@Param("processRequirement") ProcessRequirement processRequirement);

    Integer addProcessRequirement(@Param("processRequirement") ProcessRequirement processRequirement);

}
