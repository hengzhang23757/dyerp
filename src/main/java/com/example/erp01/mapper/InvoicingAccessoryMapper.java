package com.example.erp01.mapper;

import com.example.erp01.model.AccessoryInStore;
import com.example.erp01.model.AccessoryOutRecord;
import com.example.erp01.model.AccessoryStorage;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface InvoicingAccessoryMapper {

    List<Integer> getAccessoryInStoreIdList(Map<String, Object> map);

    List<Integer> getAccessoryOutRecordIdList(Map<String, Object> map);

    List<AccessoryInStore> getAccessoryInStoreByIdList(@Param("accessoryIDList") List<Integer> accessoryIDList);

    List<AccessoryStorage> getAccessoryStorageByIdList(@Param("accessoryIDList") List<Integer> accessoryIDList);

    List<AccessoryOutRecord> getAccessoryOutRecordByIdList(@Param("accessoryIDList") List<Integer> accessoryIDList);

    List<AccessoryInStore> getAccessoryInStoreByMap(Map<String, Object> map);

    List<AccessoryStorage> getAccessoryStorageByMap(Map<String, Object> map);

    List<AccessoryOutRecord> getAccessoryOutRecordByMap(Map<String, Object> map);

}
