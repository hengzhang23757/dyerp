package com.example.erp01.mapper;

import com.example.erp01.model.ClothesDevFabricManage;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface ClothesDevFabricManageMapper {

    Integer addClothesDevFabricManage(@Param("clothesDevFabricManage") ClothesDevFabricManage clothesDevFabricManage);

    List<ClothesDevFabricManage> getClothesDevFabricManageByMap(Map<String, Object> map);

    Integer updateClothesDevFabricManage(@Param("clothesDevFabricManage") ClothesDevFabricManage clothesDevFabricManage);

    Integer deleteClothesDevFabricManage(@Param("id") Integer id);

    List<String> getClothsDevFabricNameHint(@Param("keyWord") String keyWord);

    List<String> getClothsDevFabricNumberHint(@Param("keyWord") String keyWord);

    List<ClothesDevFabricManage> getClothesDevFabricManageHint(@Param("subFabricName") String subFabricName, @Param("page") Integer page, @Param("limit") Integer limit);

    Integer deleteClothesDevFabricRecordByFabricId(@Param("fabricID") Integer fabricID);

    ClothesDevFabricManage getClothesDevFabricManageById(@Param("id") Integer id);

    Integer updateClothesDevFabricManageBatch(@Param("clothesDevFabricManageList") List<ClothesDevFabricManage> clothesDevFabricManageList);

    Integer deleteClothesDevFabricManageBatch(@Param("idList") List<Integer> idList);

    String getMaxClothesDevFabricOrderByPreFix(@Param("preFix") String preFix);

    List<ClothesDevFabricManage> getClothesDevFabricManageRecordByInfo(Map<String, Object> map);

    Integer quitClothesDevFabricManageCheck(@Param("idList") List<Integer> idList);
}
