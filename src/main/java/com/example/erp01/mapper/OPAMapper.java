package com.example.erp01.mapper;

import com.example.erp01.model.OPA;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface OPAMapper {

    int addOPA(OPA opa);

    int deleteOPA(Integer opaID);

    List<OPA> getAllOPA();

    List<OPA> getTodayOPA();

    List<OPA> getOneMonthOPA();

    List<OPA> getThreeMonthOPA();

    List<OPA> getOPAByOrder(@Param("orderName")String orderName);

    List<OPA> getBedOPAByOrder(@Param("orderName")String orderName);
}
