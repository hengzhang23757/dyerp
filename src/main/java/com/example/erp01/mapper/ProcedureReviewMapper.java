package com.example.erp01.mapper;

import com.example.erp01.model.OrderProcedure;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ProcedureReviewMapper {

    List<OrderProcedure> getOrderProcedureByInfo(@Param("from") String from, @Param("to") String to, @Param("orderName") String orderName);

    Integer updateOrderProcedureByIdBatch(@Param("orderProcedureIDList") List<Integer> orderProcedureIDList, @Param("procedureState") Integer procedureState);

    List<String> getMonthPieceWorkOrderName(@Param("from") String from, @Param("to") String to, @Param("orderName") String orderName);
}
