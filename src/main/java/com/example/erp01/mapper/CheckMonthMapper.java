package com.example.erp01.mapper;

import com.example.erp01.model.CheckMonth;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface CheckMonthMapper {

    Integer addCheckMonth(@Param("checkMonth")CheckMonth checkMonth);

    Integer deleteCheckMonth(@Param("id")Integer id);

    Integer updateCheckMonth(@Param("checkMonth")CheckMonth checkMonth);

    List<CheckMonth> getAllCheckMonth();

    CheckMonth getCheckMonthByInfo(Map<String, Object> map);
}
