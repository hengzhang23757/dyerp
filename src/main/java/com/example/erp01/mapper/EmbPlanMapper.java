package com.example.erp01.mapper;

import com.example.erp01.model.EmbPlan;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

@Mapper
public interface EmbPlanMapper {

    Integer addEmbPlanBatch(@Param("embPlanList") List<EmbPlan> embPlanList);

    Integer addEmbPlan(@Param("embPlan") EmbPlan embPlan);

    Integer deleteEmbPlan(@Param("embPlanID") Integer embPlanID);

    List<EmbPlan> getTodayEmbPlan();

    List<EmbPlan> getOneMonthEmbPlan();

    List<EmbPlan> getThreeMonthEmbPlan();

    Integer updateEmbPlan(@Param("embPlan") EmbPlan embPlan);

    EmbPlan getEmbPlanByOrderGroup(@Param("orderName") String orderName, @Param("groupName") String groupName, @Param("outTime") Date outTime);

    EmbPlan getEmbPlanByOrderGroupDate(@Param("orderName") String orderName, @Param("groupName") String groupName, @Param("outTime") Date outTime);

    Integer updateEmbPlanOnOut(@Param("embPlanID") Integer embPlanID, @Param("actCount") Integer actCount, @Param("achievePercent") Float achievePercent);

    Integer getEmbStorageCountByInfo(@Param("orderName") String orderName, @Param("colorName") String colorName, @Param("sizeName") String sizeName);

    EmbPlan getEmbPlanByOrderColorSizeGroupDate(@Param("orderName") String orderName, @Param("colorName") String colorName, @Param("sizeName") String sizeName, @Param("groupName") String groupName, @Param("outTime") Date outTime);

    List<EmbPlan> getUnFinishEmbPlan();

    Integer updateEmbPlanCountByID(@Param("embPlanID") Integer embPlanID, @Param("planCount") Integer planCount);

    List<EmbPlan> getEmbPlansByOrderGroupDate(@Param("orderName") String orderName, @Param("groupName") String groupName, @Param("outTime") Date outTime);

}
