package com.example.erp01.mapper;

import com.example.erp01.model.Employee;
import com.example.erp01.model.ScheduleRecord;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ScheduleRecordMapper {

    Integer addScheduleRecordBatch(@Param("scheduleRecordList") List<ScheduleRecord> scheduleRecordList);

    List<ScheduleRecord> getScheduleRecordByInfo(@Param("orderName") String orderName, @Param("groupName") String groupName);

    Integer deleteScheduleRecordByInfo(@Param("orderName") String orderName, @Param("groupName") String groupName);

    List<ScheduleRecord> getUniqueScheduleRecordOneYear();

    List<Employee> getEmployeeByOrderGroup(@Param("orderName") String orderName, @Param("groupName") String groupName);

}
