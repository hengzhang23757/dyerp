package com.example.erp01.mapper;

import com.example.erp01.model.ClothesDevFabric;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface ClothesDevFabricMapper {

    Integer addClothesDevFabricBatch(@Param("clothesDevFabricList") List<ClothesDevFabric> clothesDevFabrics);

    Integer deleteClothesDevFabricBatch(@Param("idList") List<Integer> idList);

    List<ClothesDevFabric> getClothesDevFabricByMap(Map<String, Object> map);

    Integer updateClothesDevFabricBatch(@Param("clothesDevFabricList") List<ClothesDevFabric> clothesDevFabricList);

    Integer deleteClothesDevFabricByDevNumber(@Param("clothesDevNumber") String clothesDevNumber);

    List<ClothesDevFabric> getUniqueClothesDevFabricByDevNumber(@Param("clothesDevNumber") String clothesDevNumber);
}
