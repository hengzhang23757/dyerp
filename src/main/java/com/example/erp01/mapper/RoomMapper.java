package com.example.erp01.mapper;

import com.example.erp01.model.Room;
import com.example.erp01.model.RoomState;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface RoomMapper {

    int addRoomBatch(@Param("roomList") List<Room> roomList);

    int deleteRoom(@Param("roomID") Integer roomID);

    int updateRoom(Room room);

    List<Room> getAllRoom();

    List<String> getAllFloor();

    List<RoomState> getAllRoomInfoByFloor(@Param("floor") String floor);

    Integer getCapacityOfRoom(@Param("roomNumber") String roomNumber);

    Room getRoomByID(@Param("roomID") Integer roomID);

}
