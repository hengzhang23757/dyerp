package com.example.erp01.mapper;

import com.example.erp01.model.ClothesDevProcess;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Map;

@Mapper
public interface ClothesDevProcessMapper {

    Integer addClothesDevProcess(@Param("clothesDevProcess") ClothesDevProcess clothesDevProcess);

    Integer updateClothesDevProcess(@Param("clothesDevProcess") ClothesDevProcess clothesDevProcess);

    Integer deleteClothesDevProcessByDevId(@Param("devId") Integer devId);

    ClothesDevProcess getClothesDevProcessByMap(Map<String, Object> map);

    Integer updateClothesDevProcessByDevNumber(@Param("clothesDevProcess") ClothesDevProcess clothesDevProcess);

    Integer deleteClothesDevProcessByDevNumber(@Param("clothesDevNumber") String clothesDevNumber);
}
