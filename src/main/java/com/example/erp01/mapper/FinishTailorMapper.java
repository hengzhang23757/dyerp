package com.example.erp01.mapper;

import com.example.erp01.model.*;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Mapper
//@Repository
public interface FinishTailorMapper {

    List<FinishTailor> generateTailorData(String jsonStr);

    int saveTailorData(List<FinishTailor> tailorList);

    Integer getMaxPackageNumberByOrder(@Param("orderName") String orderName);

    List<String> getTailorQcodeByTailorQcodeID(List<Integer> tailorQcodeIDList);

    String getOneTailorQcodeByTailorQcodeID(Integer tailorQcodeID);

    int deleteTailorByOrderBedPack(Map map);

    List<FinishTailor> getFinishTailorByOrderColorSizePart(@Param("orderName") String orderName, @Param("colorName") String colorName, @Param("sizeName") String sizeName, @Param("partName") String partName);

    List<CutQueryLeak> getFinishTailorInfo(@Param("orderName") String orderName, @Param("partName") String partName);

    Integer getFinishTailorCountByOrderColorSize(@Param("orderName") String orderName, @Param("colorName") String colorName, @Param("sizeName") String sizeName, @Param("partName") String partName);

    Integer updateTailorData(@Param("finishTailorList")List<FinishTailor> finishTailorList);

    List<Integer> getTailorQcodeIDByOrderColorSize(@Param("orderName") String orderName, @Param("colorName") String colorName, @Param("sizeName") String sizeName);

    FinishTailor getFinishTailorByTailorQcodeID(@Param("tailorQcodeID") Integer tailorQcodeID);

    List<FinishTailor> getFinishTailorByOrderPartNameColorSize(@Param("orderName") String orderName, @Param("partName")String partName, @Param("colorName")String colorName, @Param("sizeName")String sizeName);

    Integer updateFinishTailorBatchInCheck(@Param("finishTailorList") List<FinishTailor> finishTailorList);

    List<FinishTailor> getShelfStorageByShelfName(@Param("shelfName") String shelfName);

    Integer updateFinishTailorBatchOutCheck(@Param("finishTailorList") List<FinishTailor> finishTailorList);

    Integer updateFinishTailorBatchReturnCheck(@Param("finishTailorList") List<FinishTailor> finishTailorList);

    List<FinishTailor> getFinishTailorByInfo(Map<String, Object> map);

    Integer updateFinishTailorLayerCountByInfo(@Param("orderName") String orderName, @Param("bedNumber") Integer bedNumber, @Param("packageNumber") Integer packageNumber, @Param("layerCount") Integer layerCount);

}
