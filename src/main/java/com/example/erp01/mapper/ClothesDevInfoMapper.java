package com.example.erp01.mapper;

import com.example.erp01.model.ClothesDevInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface ClothesDevInfoMapper {

    Integer addClothesDevInfo(@Param("clothesDevInfo") ClothesDevInfo clothesDevInfo);

    Integer deleteClothesDevInfo(@Param("id") Integer id);

    ClothesDevInfo getClothesDevInfoById(@Param("id") Integer id);

    List<ClothesDevInfo> getClothesDevInfoByMap(Map<String, Object> map);

    Integer updateClothesDevInfo(@Param("clothesDevInfo") ClothesDevInfo clothesDevInfo);

    String getMaxClothesVersionOrder(@Param("clothesPreFix") String clothesPreFix);

    String getClothesDevOrderNameByPreFix(@Param("preFix") String preFix);

    ClothesDevInfo getOneClothesDevInfoByMap(Map<String, Object> map);

    Integer updateClothesDevInfoByDevNumber(@Param("clothesDevInfo") ClothesDevInfo clothesDevInfo);

    Integer deleteClothesDevInfoByDevNumber(@Param("clothesDevNumber") String clothesDevNumber);

//

}
