package com.example.erp01.mapper;

import com.example.erp01.model.FabricOutRecord;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface FabricOutRecordMapper {

    Integer addFabricOutRecordBatch(@Param("fabricOutRecordList")List<FabricOutRecord> fabricOutRecordList);

    Integer addFabricOutRecord(@Param("fabricOutRecord") FabricOutRecord fabricOutRecord);

    Integer deleteFabricOutRecord(Integer fabricOutRecordID);

    List<FabricOutRecord> getAllFabricOutRecord();

    List<FabricOutRecord> getFabricOutRecordByOrder(@Param("orderName")String orderName);

    List<FabricOutRecord> getTodayFabricOutRecord();

    List<FabricOutRecord> getOneMonthFabricOutRecord();

    List<FabricOutRecord> getThreeMonthsFabricOutRecord();

    List<FabricOutRecord> getFabricOutRecordByInfo(@Param("orderName") String orderName, @Param("fabricName") String fabricName, @Param("colorName") String colorName, @Param("fabricColor") String fabricColor, @Param("fabricID") Integer fabricID);

    List<FabricOutRecord> getFabricOutRecordByOrderType(@Param("orderName")String orderName, @Param("operateType")String operateType, @Param("colorName")String colorName);

    List<FabricOutRecord> getFabricOutRecordByTime(@Param("from") String from, @Param("to") String to);

    FabricOutRecord getFabricOutRecordById(@Param("fabricOutRecordID") Integer fabricOutRecordID);

    Float getReturnFabricWeightByFabricID(@Param("fabricID") Integer fabricID);

    List<FabricOutRecord> getFabricOutRecordByMap(Map<String, Object> map);

    Integer deleteFabricOutRecordByOrderJar(@Param("orderName") String orderName, @Param("jarName") String jarName);
}
