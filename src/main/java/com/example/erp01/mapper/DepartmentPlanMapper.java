package com.example.erp01.mapper;

import com.example.erp01.model.DepartmentPlan;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

@Mapper
public interface DepartmentPlanMapper {

    Integer addDepartmentPlan(@Param("departmentPlan") DepartmentPlan departmentPlan);

    Integer deleteDepartmentPlan(@Param("departmentPlanID") Integer departmentPlanID);

    Integer updateDepartmentPlan(@Param("departmentPlan") DepartmentPlan departmentPlan);

    List<DepartmentPlan> getDepartmentPlanOneMonth();

    List<DepartmentPlan> getTailorPlanDetail(@Param("orderName") String orderName, @Param("beginDate") Date beginDate);

    List<DepartmentPlan> getLoosePlanDetail(@Param("orderName") String orderName, @Param("beginDate") Date beginDate);

    List<String> getTailorOrderNameOfToday();

    List<String> getLooseOrderNameOfToday();

    List<DepartmentPlan> getDepartmentPlanBetweenToday(@Param("planType")String planType, @Param("orderName") String orderName);

    Integer getWellCountByOrderDate(@Param("orderName") String orderName, @Param("beginDate") Date beginDate);

    Integer getLooseCountByOrderDate(@Param("orderName") String orderName, @Param("beginDate") Date beginDate);

    List<DepartmentPlan> getDepartmentPlanByFinishState(@Param("finishState") String finishState, @Param("orderName") String orderName, @Param("colorName") String colorName);

    Integer getMaxPlanOrder();

    Integer updateDepartmentPlanBatch(@Param("departmentPlanList") List<DepartmentPlan> departmentPlanList);

    Integer setDepartmentPlanFinish(@Param("departmentPlanID") Integer departmentPlanID);
}
