package com.example.erp01.mapper;

import com.example.erp01.model.SpecialProcedure;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface SpecialProcedureMapper {

    int addSpecialProcedureBatch(@Param("specialProcedureList")List<SpecialProcedure> specialProcedureList);

    int deleteSpecialProcedure(@Param("specialID") Integer specialID);

    List<SpecialProcedure> getAllSpecialProcedure();

    List<SpecialProcedure> getSpecialProcedureByOrderProcedure(@Param("orderName") String orderName, @Param("procedureNumber") Integer procedureNumber);

}
