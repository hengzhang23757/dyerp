package com.example.erp01.mapper;

import com.example.erp01.model.*;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface FabricAccessoryMonthCheckMapper {

    List<FabricMonthCheck> getFabricDetailCheckByInfo(Map<String, Object> map);

    List<FabricMonthCheck> getFabricOutRecordCheckByInfo(Map<String, Object> map);

    List<String> getAllFabricSupplier();

    List<String> getAllAccessorySupplier();

    Integer updateCheckDetailInCheck(@Param("fabricDetailList") List<FabricDetail> fabricDetailList);

    Integer updateCheckOutRecordInCheck(@Param("fabricOutRecordList") List<FabricOutRecord> fabricOutRecordList);

    List<AccessoryInStore> getAccessoryInStoreByInfo(Map<String, Object> map);

    Integer updateAccessoryInStoreInCheck(@Param("accessoryInStoreList") List<AccessoryInStore> accessoryInStoreList);

    Integer quitFabricDetailBatch(@Param("fabricDetailIDList") List<Integer> fabricDetailIDList);

    Integer quitAccessoryInStoreBatch(@Param("idList") List<Integer> idList);
}
