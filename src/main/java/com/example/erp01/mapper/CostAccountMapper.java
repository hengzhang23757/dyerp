package com.example.erp01.mapper;

import com.example.erp01.model.GeneralSalary;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface CostAccountMapper {

    Float getFabricCostByOrder(@Param("orderName") String orderName);

    Float getFabricDeductionCostByOrder(@Param("orderName") String orderName);

    Float getAccessoryCostByOrder(@Param("orderName") String orderName);

    List<GeneralSalary> getPieceWorkSummaryByOrder(@Param("orderName") String orderName);

    List<GeneralSalary> getHourEmpSummaryByOrder(@Param("orderName") String orderName);

    Float getFabricOrderCost(@Param("orderName") String orderName);

    Float getAccessoryOrderCost(@Param("orderName") String orderName);

    Float getFabricAddFee(@Param("orderName") String orderName);

    Float getAccessoryAddFee(@Param("orderName") String orderName);

    Float getFabricCustomerCost(@Param("orderName") String orderName);

    Float getAccessoryCustomerCost(@Param("orderName") String orderName);

}
