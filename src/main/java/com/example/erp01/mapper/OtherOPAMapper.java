package com.example.erp01.mapper;

import com.example.erp01.model.OtherOPA;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface OtherOPAMapper {

    int addOtherOPA(OtherOPA otherOPA);

    int addOtherOPABatch(List<OtherOPA> otherOPAList);

    int deleteOtherOPA(Integer otherOpaID);

    List<OtherOPA> getAllOtherOPA();

    List<OtherOPA> getTodayOtherOPA();

    List<OtherOPA> getOneMonthOtherOPA();

    List<OtherOPA> getThreeMonthOtherOPA();

    List<OtherOPA> getOtherOPAByOrder(@Param("orderName")String orderName);

    List<OtherOPA> getOtherBedOPAByOrder(@Param("orderName")String orderName);

}
