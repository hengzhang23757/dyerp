package com.example.erp01.mapper;

import com.example.erp01.model.ClothesDevMeasure;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Map;

@Mapper
public interface ClothesDevMeasureMapper {

    Integer addClothesDevMeasure(@Param("clothesDevMeasure") ClothesDevMeasure clothesDevMeasure);

    Integer updateClothesDevMeasure(@Param("clothesDevMeasure") ClothesDevMeasure clothesDevMeasure);

    Integer deleteClothesDevMeasureByDevId(@Param("devId") Integer devId);

    ClothesDevMeasure getClothesDevMeasureByMap(Map<String, Object> map);

    Integer updateClothesDevMeasureByDevNumber(@Param("clothesDevMeasure") ClothesDevMeasure clothesDevMeasure);

    Integer deleteClothesDevMeasureByDevNumber(@Param("clothesDevNumber") String clothesDevNumber);
}
