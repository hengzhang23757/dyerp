package com.example.erp01.mapper;

import com.example.erp01.model.ReWork;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

@Mapper
public interface ReWorkMapper {

    List<ReWork> getReWorkByOrder(@Param("orderName") String orderName);

    Integer getWorkCountByProcedure(@Param("orderName") String orderName, @Param("procedureNumber") Integer procedureNumber, @Param("reWorkDate") Date reWorkDate);

    List<ReWork> getReWorkSummaryByOrder(@Param("orderName") String orderName);
}
