package com.example.erp01.mapper;

import com.example.erp01.model.ColorManage;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ColorManageMapper {

    Integer addColorManage(@Param("colorManage") ColorManage colorManage);

    Integer deleteColorManage(@Param("id") Integer id);

    List<ColorManage> getAllColorManage();

    Integer updateColorManage(@Param("colorManage") ColorManage colorManage);

}
