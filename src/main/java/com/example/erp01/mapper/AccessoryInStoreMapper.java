package com.example.erp01.mapper;

import com.example.erp01.model.AccessoryInStore;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface AccessoryInStoreMapper {

    int addAccessoryInStoreBatch(@Param("accessoryInStoreList") List<AccessoryInStore> accessoryInStoreList);

    int deleteAccessoryInStoreByID(@Param("id") Integer id);

    List<AccessoryInStore> getAccessoryInStoreByOrder(@Param("orderName") String orderName);

    AccessoryInStore getAccessoryInStoreById(@Param("id") Integer id);

    List<AccessoryInStore> getAccessoryInStoreByInfo(@Param("orderName") String orderName, @Param("accessoryName") String accessoryName, @Param("specification") String specification, @Param("accessoryColor") String accessoryColor, @Param("colorName") String colorName, @Param("sizeName") String sizeName, @Param("accessoryID") Integer accessoryID);

    List<AccessoryInStore> getTodayAccessoryInStore();

    List<AccessoryInStore> getAccessoryInStoreByColorSizeList(@Param("orderName") String orderName, @Param("colorList") List<String> colorList, @Param("sizeList") List<String> sizeList);

    List<AccessoryInStore> getAccessoryInStoreByTime(@Param("from") String from, @Param("to") String to);

    List<AccessoryInStore> getAccessoryInStoreByIdList(@Param("accessoryIdList") List<Integer> accessoryIdList);

    Integer updateAccessoryInStoreInChange(@Param("accessoryInStore") AccessoryInStore accessoryInStore);

    List<AccessoryInStore> getBjInStoreByOrder(@Param("orderName") String orderName);

    List<Integer> getAccessoryIdListByAddFee(@Param("idList") List<Integer> idList);
}
