package com.example.erp01.mapper;

import com.example.erp01.model.AttendLog;
import com.example.erp01.model.DailyCheck;
import com.example.erp01.model.SummaryCheck;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
@Mapper
public interface CheckMapper {

    Integer addAttendLogBatch(@Param("attendLogList") List<AttendLog> attendLogList);

    List<AttendLog> getAttendLogByEmp(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate, @Param("ccCount") String ccCount);

    Integer addDailyCheckBatch(@Param("dailyCheckList") List<DailyCheck> dailyCheckList);

    List<DailyCheck> getDailyCheckByMonthGroup(@Param("monthString") String monthString, @Param("groupName") String groupName);

    List<AttendLog> getMonthAttendLogByEmp(@Param("monthString") String monthString, @Param("ccCount") String ccCount);

    Integer addSummaryCheckBatch(@Param("summaryCheckList") List<SummaryCheck> summaryCheckList);

    List<SummaryCheck> getSummaryCheckByMonthGroup(@Param("monthString") String monthString, @Param("groupName") String groupName);
}
