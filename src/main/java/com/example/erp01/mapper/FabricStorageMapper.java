package com.example.erp01.mapper;

import com.example.erp01.model.FabricStorage;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface FabricStorageMapper {

    Integer addFabricStorageBatch(@Param("fabricStorageList")List<FabricStorage> fabricStorageList);

    Integer addFabricStorage(@Param("fabricStorage")FabricStorage fabricStorage);

    Integer deleteFabricStorage(@Param("fabricStorageID")Integer fabricStorageID);

    List<FabricStorage> getAllFabricStorage();

    FabricStorage getFabricStorageByOrderLocationJar(@Param("orderName")String orderName, @Param("colorName")String colorName, @Param("fabricName")String fabricName, @Param("fabricColor")String fabricColor, @Param("jarName")String jarName, @Param("location")String location, @Param("fabricID")Integer fabricID);

    List<FabricStorage> getFabricStorageByOrder(@Param("orderName")String orderName);

    Integer updateFabricStorage(@Param("fabricStorage")FabricStorage fabricStorage);

    Integer getFabricBatchNumberByOrderFabricJarLocation(@Param("orderName")String orderName, @Param("fabricName")String fabricName, @Param("fabricColor")String fabricColor, @Param("jarName")String jarName, @Param("location")String location);

    FabricStorage getFabricStorageByOrderColorLocationJar(@Param("orderName")String orderName, @Param("fabricName")String fabricName, @Param("colorName")String colorName, @Param("fabricColor")String fabricColor, @Param("jarName")String jarName, @Param("location")String location, @Param("fabricID")Integer fabricID);

    FabricStorage getFabricStorageByID(@Param("fabricStorageID")Integer fabricStorageID);

    List<FabricStorage> getFabricStorageByInfo(@Param("orderName") String orderName, @Param("fabricName") String fabricName, @Param("colorName") String colorName, @Param("fabricColor") String fabricColor, @Param("fabricID")Integer fabricID);

    Integer getFabricBatchNumberByOrder(@Param("orderName")String orderName, @Param("colorNameList") List<String> colorNameList);

    List<FabricStorage> getFabricStorageByOrderList(@Param("orderNameList") List<String> orderNameList);

    List<FabricStorage> getFabricStorageByIdList(@Param("fabricStorageIDList")List<Integer> fabricStorageIDList);

    Integer deleteFabricStorageBatch(@Param("fabricStorageIDList")List<Integer> fabricStorageIDList);

    Integer deleteFabricStorageByOrderNameJar(@Param("orderName")String orderName, @Param("jarName")String jarName);

}
