package com.example.erp01.mapper;

import com.example.erp01.model.OrderMeasureItem;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface OrderMeasureItemMapper {

    Integer addOrderMeasureItemBatch(@Param("orderMeasureItemList") List<OrderMeasureItem> orderMeasureItemList);

    List<OrderMeasureItem> getOrderMeasureItemByOrder(@Param("orderName") String orderName);

    Integer deleteOrderMeasureItemByOrder(@Param("orderName") String orderName);

    List<String> getUniquePartCodeListByOrder(@Param("orderName") String orderName);
}
