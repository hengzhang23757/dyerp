package com.example.erp01.mapper;

import com.example.erp01.model.OrderMeasure;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface OrderMeasureMapper {

    Integer addOrderMeasureBatch(@Param("orderMeasureList") List<OrderMeasure> orderMeasureList);

    List<OrderMeasure> getOrderMeasureByOrder(@Param("orderName") String orderName);

    Integer deleteOrderMeasureByOrder(@Param("orderName") String orderName);

    Integer updateOrderMeasure(@Param("orderMeasure") OrderMeasure orderMeasure);

    Integer deleteOrderMeasureByID(@Param("orderMeasureID") Integer orderMeasureID);

    Integer addOrderMeasure(@Param("orderMeasure") OrderMeasure orderMeasure);

    OrderMeasure getOneOrderMeasureByOrder(@Param("orderName") String orderName);

}
