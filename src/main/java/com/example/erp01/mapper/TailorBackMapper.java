package com.example.erp01.mapper;

import com.example.erp01.model.TailorBack;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface TailorBackMapper {

    int saveTailorBackData(List<TailorBack> tailorBackList);

}
