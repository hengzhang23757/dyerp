package com.example.erp01.mapper;

import com.example.erp01.model.*;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface EmpSkillsMapper {

    Integer addEmpSkillsBatch(@Param("empSkillsList") List<EmpSkills> empSkillsList);

    List<EmpSkills> getEmpSkillsByInfo(@Param("groupName") String groupName, @Param("procedureCode") Integer procedureCode, @Param("employeeNumber") String employeeNumber);

    List<EmpSkills> getDistinctHistoryPieceWorkDetail();

    List<Float> getPieceWorkDetailByInfo(@Param("procedureCode") Integer procedureCode, @Param("employeeNumber") String employeeNumber);

    List<EmpSkills> getYesterdayEmpSkills();

    List<EmpSkills> getEmpSkillsByGroupProcedureCode(@Param("groupName") String groupName, @Param("procedureCodeList") List<Integer> procedureCodeList);

    List<EmpSkills> getAllEmpSkills();

    Integer updateEmpSkillsBatch(@Param("empSkillsList") List<EmpSkills> empSkillsList);

    Integer dailyCleanEmpSkills();

    MinMaxNid getYesterdayMinMaxNid();

    List<Integer> getProcedureCodeYesterday();

    List<ProcedureTemplate> getProcedureTemplateByProcedureCode(@Param("procedureCodeList") List<Integer> procedureCodeList);

    Integer updateSamByProcedureCode(@Param("SAM") Float SAM, @Param("procedureCode") Integer procedureCode, @Param("beginNid") Long beginNid, @Param("endNid") Long endNid);

    List<Metre> getProcedureCombineOfYesterday();

    Integer updateMetreBatch(@Param("metreList")List<Metre> metreList);

    List<EmpSkills> getSkillsByMap(Map<String, Object> params);
}
