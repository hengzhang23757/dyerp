package com.example.erp01.mapper;

import com.example.erp01.model.EndProductPay;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface EndProductPayMapper {

    Integer addEndProductPay(@Param("endProductPay") EndProductPay endProductPay);

    Integer addEndProductPayBatch(@Param("endProductPayList") List<EndProductPay> endProductPayList);

    Integer deleteEndProductPay(@Param("id") Integer id);

    Integer updateEndProductPay(@Param("endProductPay") EndProductPay endProductPay);

    List<EndProductPay> getEndProductPaySummary();

    Integer getEndProductPayCountByOrder(@Param("orderName") String orderName);

    List<EndProductPay> getEndProductPayByOrderName(@Param("orderName") String orderName);

    Integer deleteEndProductPayBatch(@Param("idList") List<Integer> idList);

    List<EndProductPay> getEndProductPayByInfo(@Param("customerName") String customerName, @Param("payMonth") String payMonth, @Param("orderName") String orderName, @Param("payType") String payType);

    Integer deleteEndProductPayByPayNumber(@Param("payNumber") String payNumber);

    EndProductPay getEndProductPayByPayNumber(@Param("payNumber") String payNumber);

    List<EndProductPay> monthSummaryByCustomer(@Param("payMonth") String payMonth);

}
