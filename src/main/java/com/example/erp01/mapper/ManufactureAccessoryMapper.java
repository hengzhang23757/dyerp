package com.example.erp01.mapper;

import com.example.erp01.model.AddFeeVo;
import com.example.erp01.model.FabricAccessoryReview;
import com.example.erp01.model.ManufactureAccessory;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface ManufactureAccessoryMapper {

    Integer addManufactureAccessoryBatch(@Param("manufactureAccessoryList") List<ManufactureAccessory> manufactureAccessoryList);

    List<ManufactureAccessory> getManufactureAccessoryByOrder(@Param("orderName") String orderName, @Param("batchOrder") Integer batchOrder);

    Integer deleteManufactureAccessoryByName(@Param("orderName") String orderName);

    Integer deleteManufactureAccessoryByID(@Param("accessoryID") Integer accessoryID);

    Integer deleteManufactureAccessoryBatch(@Param("accessoryIDList") List<Integer> accessoryIDList);

    Integer updateManufactureAccessory(@Param("manufactureAccessory") ManufactureAccessory manufactureAccessory);

    List<ManufactureAccessory> getManufactureAccessoryHintByName(@Param("subAccessoryName") String subAccessoryName, @Param("page") Integer page, @Param("limit") Integer limit);

    List<ManufactureAccessory> getManufactureAccessoryHintByNumber(@Param("subAccessoryNumber") String subAccessoryName, @Param("page") Integer page, @Param("limit") Integer limit);

    Integer updateManufactureAccessoryOnPlaceOrder(@Param("manufactureAccessoryList") List<ManufactureAccessory> manufactureAccessoryList);

    List<ManufactureAccessory> getManufactureAccessoryOneYear();

    List<String> getDistinctAccessorySupplier();

    ManufactureAccessory getManufactureAccessoryByID(@Param("accessoryID") Integer accessoryID);

    List<String> getOrderNamesByAccessoryInfo(@Param("accessoryName") String accessoryName, @Param("specification") String specification, @Param("accessoryColor") String accessoryColor);

    ManufactureAccessory getManufactureAccessoryByAccessoryInfo(@Param("orderName") String orderName, @Param("accessoryName") String accessoryName, @Param("specification") String specification, @Param("accessoryColor") String accessoryColor);

    List<ManufactureAccessory> getManufactureAccessoryInPlaceOrder(@Param("orderName") String orderName, @Param("supplier") String supplier, @Param("checkState") String checkState);

    List<ManufactureAccessory> getManufactureAccessoryInPlaceOrderByInfo(@Param("orderNameList") List<String> orderNameList, @Param("supplier") String supplier, @Param("materialName") String materialName, @Param("checkState") String checkState);

    Integer changeAccessoryOrderStateInPlaceOrder(@Param("accessoryIDList") List<Integer> accessoryIDList);

    Integer updateManufactureAccessoryBatch(@Param("manufactureAccessoryList") List<ManufactureAccessory> manufactureAccessoryList);

    Integer changeAccessoryCheckStateInPlaceOrder(@Param("accessoryIDList") List<Integer> accessoryIDList, @Param("preCheck") String preCheck, @Param("checkState") String checkState, @Param("preCheckEmp") String preCheckEmp, @Param("checkEmp") String checkEmp);

    List<ManufactureAccessory> getManufactureAccessoryByOrderCheckState(@Param("orderName") String orderName, @Param("checkState") String checkState);

    List<ManufactureAccessory> getManufactureAccessoryByOrderCheckStateOrderList(@Param("orderNameList") List<String> orderNameList, @Param("checkState") String checkState, @Param("preCheck") String preCheck);

    List<ManufactureAccessory> getBjByOrderCheckStateOrderList(@Param("orderNameList") List<String> orderNameList, @Param("checkState") String checkState);

    List<String> getManufactureAccessoryNameHint(@Param("subName") String subName);

    List<ManufactureAccessory> getAccessoryCheckStateRecentYearByOrder(@Param("orderNameList") List<String> orderNameList);

    List<FabricAccessoryReview> getManufactureAccessoryReviewState(@Param("checkState") String checkState);

    List<ManufactureAccessory> getManufactureAccessoryByCheckNumberState(@Param("checkNumber") String checkNumber, @Param("checkState") String checkState);

    Integer destroyAccessoryCheck(@Param("checkNumber") String checkNumber);

    Integer getAccessoryCheckNumberCountByState(@Param("checkState") String checkState);

    Integer updateAccessoryAfterCommit(@Param("manufactureAccessoryList") List<ManufactureAccessory> manufactureAccessoryList);

    List<ManufactureAccessory> getManufactureAccessoryByColorSizeList(@Param("orderName") String orderName, @Param("colorList") List<String> colorList, @Param("sizeList") List<String> sizeList);

    Integer updateAccessoryOrderStateByCheckNumber(@Param("checkNumber") String checkNumber);

    List<String> getDistinctAccessoryNameByOrder(@Param("orderName") String orderName);

    List<ManufactureAccessory> getManufactureAccessoryByIdList(@Param("accessoryIDList") List<Integer> accessoryIDList);

    List<String> getAccessoryCheckNumberHint(@Param("keyWord") String keyWord);

    List<ManufactureAccessory> getAccessoryNameByOrderName(@Param("orderName") String orderName, @Param("batchOrder") Integer batchOrder, @Param("accessoryType") String accessoryType);

    List<ManufactureAccessory> getAccessoryNameChildByOrderName(@Param("orderName") String orderName, @Param("batchOrder") Integer batchOrder, @Param("accessoryType") String accessoryType);

    Integer getMaxAccessoryKey(@Param("orderName") String orderName, @Param("batchOrder") Integer batchOrder);

    List<ManufactureAccessory> getAccessoryKeyByOrder(@Param("orderName") String orderName, @Param("batchOrder") Integer batchOrder, @Param("accessoryType") String accessoryType);

    void updateAccessoryCustomerUsage(@Param("manufactureAccessoryList") List<ManufactureAccessory> manufactureAccessoryList);



    List<FabricAccessoryReview> getManufactureAccessoryReviewStateCombine(Map<String, Object> map);

    List<FabricAccessoryReview> getBjReviewStateCombine(Map<String, Object> map);

    void deleteManufactureAccessoryByAccessoryType(@Param("orderName") String orderName, @Param("accessoryType") String accessoryType, @Param("batchOrder") Integer batchOrder, @Param("checkState") String checkState);

    List<ManufactureAccessory> getBjByOrder(@Param("orderName") String orderName, @Param("accessoryType") String accessoryType, @Param("batchOrder") Integer batchOrder);


    void changeFeeStateBatch(@Param("accessoryIDList") List<Integer> accessoryIDList, @Param("feeState") Integer feeState);

    List<AddFeeVo> getFeeVoByInfo(Map<String, Object> map);

    void changeOrderName(@Param("orderName") String orderName, @Param("toOrderName") String toOrderName);


}
