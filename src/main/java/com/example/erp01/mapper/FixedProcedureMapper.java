package com.example.erp01.mapper;

import com.example.erp01.model.FixedProcedure;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface FixedProcedureMapper {

    Integer addFixedProcedure(@Param("fixedProcedure") FixedProcedure fixedProcedure);

    Integer deleteFixedProcedure(@Param("id") Integer id);

    List<FixedProcedure> getAllFixedProcedure();

}
