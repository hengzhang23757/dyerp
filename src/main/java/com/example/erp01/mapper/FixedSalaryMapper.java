package com.example.erp01.mapper;

import com.example.erp01.model.FixedSalary;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface FixedSalaryMapper {

    Integer addFixedSalary(@Param("fixedSalary") FixedSalary fixedSalary);

    Integer deleteFixedSalary(@Param("fixedID") Integer fixedID);

    List<FixedSalary> getAllFixedSalary();

    List<String> getAllFixedSalaryValue();

}
