package com.example.erp01.mapper;

import com.example.erp01.model.SizeManage;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
@Mapper
public interface SizeManageMapper {

    List<SizeManage> getAllSizeManage();

    Integer addSizeManageOne(@Param("sizeManage") SizeManage sizeManage);

    Integer deleteSizeManageById(@Param("sizeManageID") Integer sizeManageID);

    List<String> getAllSizeName();
}
