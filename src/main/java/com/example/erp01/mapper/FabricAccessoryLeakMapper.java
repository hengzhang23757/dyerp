package com.example.erp01.mapper;

import com.example.erp01.model.*;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface FabricAccessoryLeakMapper {

    List<FabricAccessoryLeak> getOrderInfoByOrderList(@Param("orderNameList") List<String> orderNameList);

    List<FabricAccessoryLeak> getCutInfoByOrderList(@Param("orderNameList") List<String> orderNameList);

    List<FabricAccessoryLeak> getFabricLeakByOrderList(@Param("orderNameList") List<String> orderNameList);

    List<FabricAccessoryLeak> getAccessoryLeakByOrderList(@Param("orderNameList") List<String> orderNameList);

    List<FabricAccessoryLeak> getFabricReturnByOrderList(@Param("orderNameList") List<String> orderNameList);

    List<FabricAccessoryLeak> getAccessoryReturnByOrderList(@Param("orderNameList") List<String> orderNameList);

    List<ManufactureFabric> getManufactureFabricByOrderList(@Param("orderNameList") List<String> orderNameList);

    List<FabricDetail> getFabricDetailByOrderList(@Param("orderNameList") List<String> orderNameList);

    List<ManufactureAccessory> getManufactureAccessoryByOrderList(@Param("orderNameList") List<String> orderNameList);

    List<AccessoryInStore> getAccessoryInStoreByOrderList(@Param("orderNameList") List<String> orderNameList);
}
