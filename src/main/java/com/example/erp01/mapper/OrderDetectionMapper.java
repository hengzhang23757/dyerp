package com.example.erp01.mapper;

import com.example.erp01.model.OrderDetection;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface OrderDetectionMapper {

    Integer addOrderDetection(@Param("orderDetection") OrderDetection orderDetection);

    Integer updateOrderDetection(@Param("orderDetection") OrderDetection orderDetection);

    Integer deleteOrderDetection(@Param("id") Integer id);

    List<OrderDetection> getOrderDetectionByInfo(Map<String, Object> map);

    OrderDetection getOrderDetectionById(@Param("id") Integer id);

}
