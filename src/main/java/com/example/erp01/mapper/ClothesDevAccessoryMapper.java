package com.example.erp01.mapper;

import com.example.erp01.model.ClothesDevAccessory;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface ClothesDevAccessoryMapper {

    Integer addClothesDevAccessoryBatch(@Param("clothesDevAccessoryList") List<ClothesDevAccessory> clothesDevAccessoryList);

    Integer deleteClothesDevAccessoryBatch(@Param("idList") List<Integer> idList);

    List<ClothesDevAccessory> getClothesDevAccessoryByMap(Map<String, Object> map);

    Integer updateClothesDevAccessoryBatch(@Param("clothesDevAccessoryList") List<ClothesDevAccessory> clothesDevAccessoryList);

    List<ClothesDevAccessory> getClothesDevAccessoryHint(@Param("subAccessoryName") String subAccessoryName, @Param("page") Integer page, @Param("limit") Integer limit);

    Integer deleteClothesDevAccessoryByDevNumber(@Param("clothesDevNumber") String clothesDevNumber);
}
