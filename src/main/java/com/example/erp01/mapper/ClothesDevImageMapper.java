package com.example.erp01.mapper;

import com.example.erp01.model.ClothesDevImage;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Map;

@Mapper
public interface ClothesDevImageMapper {

    Integer addClothesDevImage(@Param("clothesDevImage") ClothesDevImage clothesDevImage);

    Integer deleteClothesDevImageByDevId(@Param("devId") Integer devId);

    Integer updateClothesDevImage(@Param("clothesDevImage") ClothesDevImage clothesDevImage);

    ClothesDevImage getClothesDevImageByMap(Map<String, Object> map);

    Integer updateClothesDevImageByDevNumber(@Param("clothesDevImage") ClothesDevImage clothesDevImage);

    Integer deleteClothesDevImageByDevNumber(@Param("clothesDevNumber") String clothesDevNumber);
}
