package com.example.erp01.mapper;

import com.example.erp01.model.GeneralSalary;
import com.example.erp01.model.OrderSalary;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface OrderSalaryMapper {

    List<OrderSalary> getOrderSalarySummaryByOrderProcedure(@Param("orderName") String orderName, @Param("procedureNumber") Integer procedureNumber);
    List<OrderSalary> getOrderSalarySummaryByOrderProcedureMonth(@Param("orderName") String orderName, @Param("procedureNumber") Integer procedureNumber);

    // 本月生产的订单list
    List<String> getPieceWorkOrderNameListByMonth(@Param("from") String from, @Param("to") String to);

    // 本月裁剪的订单list
    List<String> getCutOrderNameListByMonth(@Param("from") String from, @Param("to") String to);

    // 全部的订单量
    List<OrderSalary> getOrderCountByOrderNameList(@Param("orderNameList") List<String> orderNameList);

    // 本月的区间裁剪数量/全部的裁剪量
    List<OrderSalary> getMonthCutCountByMonthOrderNameList(@Param("from") String from, @Param("to") String to, @Param("orderNameList") List<String> orderNameList);

    // 本月的生产数据和对应工资/全部的生产数据
    List<OrderSalary> getMonthPieceSalary(@Param("from") String from, @Param("to") String to, @Param("orderNameList") List<String> orderNameList);

    // 本月的生产数据和对应工资/全部的生产数据
    List<OrderSalary> getMonthHourSalary(@Param("from") String from, @Param("to") String to);

    // 获取一个款多个月的裁数
    List<OrderSalary> getOrderCutInfoByMonth(@Param("orderName") String orderName);

    // 获取某个订单-工序 的计件数据汇总
    List<GeneralSalary> getOrderProcedureCountSummary(@Param("orderName") String orderName, @Param("procedureNumber") Integer procedureNumber);

}
