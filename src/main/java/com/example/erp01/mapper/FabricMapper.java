package com.example.erp01.mapper;

import com.example.erp01.model.Fabric;
import com.example.erp01.model.FabricDifference;
import com.example.erp01.model.FabricLeak;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface FabricMapper {

    int addFabricBatch(List<Fabric> fabricList);

    int deleteFabric(Integer fabricID);

    List<Fabric> getAllFabric();

    int updateFabric(Fabric fabric);

    List<String> getStyleNumberHint(String subStyleNumber);

    List<String> getFabricNumberHint(String styleNumber);

    String getVersionNumberHint(String styleNumber);

    List<String> getFabricColorNumberHint(@Param("styleNumber")String styleNumber,@Param("fabricNumber") String fabricNumber);

    List<String> getJarNumberHint(@Param("styleNumber")String styleNumber,@Param("fabricNumber") String fabricNumber,@Param("fabricColor") String fabricColorNumber);

    Float getDeliveryQuantityHint(@Param("styleNumber")String styleNumber,@Param("fabricNumber")String fabricNumber,@Param("fabricColor")String fabricColorNumber,@Param("jarNumber") String jarNumber);

    Integer getBatchNumberHint(@Param("styleNumber")String styleNumber,@Param("fabricNumber")String fabricNumber,@Param("fabricColor")String fabricColorNumber,@Param("jarNumber")String jarNumber);

    List<String> getSeasonHint(String subSeason);

    List<String> getVersionNumberBySeason(@Param("season")String season);

    List<FabricLeak> getFabricLeakQuery(@Param("season")String season,@Param("versionNumber")String versionNumber);

    String getColorNameByNumber(@Param("styleNumber")String styleNumber,@Param("fabricColorNumber")String fabricColorNumber);

    List<String> getFabricColorHint(@Param("styleNumber")String styleNumber,@Param("fabricNumber") String fabricNumber);

    String getColorNumberByName(@Param("styleNumber")String styleNumber,@Param("fabricColor")String fabricColor);

    List<FabricDifference> getFabricDifference(@Param("styleNumber")String styleNumber);

    List<String> getVersionNumberBySubVersion(@Param("subVersion")String subVersion);

    List<String> getStyleNumbersByVersion(@Param("versionNumber")String versionNumber);

    List<Fabric> getOneMonthFabric();

    List<Fabric> getThreeMonthsFabric();

}
