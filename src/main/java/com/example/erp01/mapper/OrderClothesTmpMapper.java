package com.example.erp01.mapper;

import com.example.erp01.model.OrderClothes;
import com.example.erp01.model.OrderClothesTmp;
import com.example.erp01.model.Tailor;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface OrderClothesTmpMapper {

    Integer addOrderClothesTmpBatch(@Param("orderClothesTmpList") List<OrderClothesTmp> orderClothesTmpList);

    List<String> getYesterdayTailorOrderName();

    List<Tailor> getTailorTotalInfoAmongYesterday(@Param("orderNameList") List<String> orderNameList);

    List<OrderClothes> getOrderClothesByOrderNameList(@Param("orderNameList") List<String> orderNameList);
}
