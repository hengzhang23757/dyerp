package com.example.erp01.mapper;

import com.example.erp01.model.GeneralSalary;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface GatherPieceWorkMapper {

    List<GeneralSalary> getGatherPieceWorkAmongSummary(@Param("orderName") String orderName, @Param("groupName") String groupName, @Param("employeeNumber") String employeeNumber, @Param("from") String from, @Param("to") String to, @Param("procedureNumber") Integer procedureNumber);

    List<GeneralSalary> getGatherPieceWorkEachDaySummary(@Param("orderName") String orderName, @Param("groupName") String groupName, @Param("employeeNumber") String employeeNumber, @Param("from") String from, @Param("to") String to, @Param("procedureNumber") Integer procedureNumber);

    List<GeneralSalary> getGatherSalaryEachDaySummary(@Param("orderName") String orderName, @Param("groupName") String groupName, @Param("employeeNumber") String employeeNumber, @Param("from") String from, @Param("to") String to, @Param("procedureNumber") Integer procedureNumber);

}
