package com.example.erp01.mapper;

import com.example.erp01.model.GeneralSalary;
import com.example.erp01.model.PieceWork;
import com.example.erp01.model.TaskData;
import com.example.erp01.model.TaskLinks;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

@Mapper
public interface TaskDataMapper {

    Integer addTaskData(@Param("taskData") TaskData taskData);

    Integer addTaskDataBatch(@Param("taskDataList") List<TaskData> taskDataList);

    List<TaskData> getTaskDataByInfo(@Param("from") Date from, @Param("to") Date to, @Param("groupName") String groupName);

    Integer updateTaskData(@Param("taskData") TaskData taskData);

    Integer deleteTaskDataById(@Param("id") Integer id);

    List<TaskData> getAllTaskData();

    List<TaskData> getTaskDataByTypeGroup(@Param("from") Date from, @Param("to") Date to, @Param("taskTypeList") List<String> taskTypeList, @Param("groupNameList") List<String> groupNameList);

    List<TaskData> getUpdateTaskDataByInfo(@Param("procedureNumber") Integer procedureNumber, @Param("maxNid") Long maxNid);

    Long getMaxNidFromTaskData();

    TaskData getActTaskDataByInfo(@Param("updateTime") Date updateTime, @Param("groupName") String groupName, @Param("orderName") String orderName);

    TaskData getPlanTaskDataByInfo(@Param("startDate") Date startDate, @Param("groupName") String groupName, @Param("orderName") String orderName);

    TaskData getParentTaskDataByGroupName(@Param("groupName") String groupName, @Param("taskType") String taskType);

    List<TaskData> getLooseFabricCountEachOrder();

    List<TaskData> getTaskDataByTaskTypeGroupName(@Param("taskType") String taskType, @Param("groupName") String groupName);

    List<TaskLinks> getAllTaskLinks();

    TaskData getTaskDataById(@Param("id") Integer id);

    List<TaskData> getUnFinishTaskDataByTaskTypeGroupName(@Param("taskType") String taskType, @Param("groupName") String groupName);

    TaskData getCurrentTaskDataByGroupType(@Param("taskType") String taskType, @Param("groupName") String groupName);

    Integer clearTaskDataByGroupType(@Param("taskType") String taskType, @Param("groupName") String groupName);

    Integer addTaskLinksBatch(@Param("taskLinkList") List<TaskLinks> taskLinkList);

    List<PieceWork> getSomeDayFinishCount(@Param("groupName") String groupName, @Param("someDay") Date someDay);

    List<TaskData> getAllTaskDataByType(@Param("taskType") String taskType);

    TaskData getTaskDataByBetweenDateType(@Param("taskType") String taskType, @Param("groupName") String groupName, @Param("orderName") String orderName, @Param("someDay") Date someDay);

    TaskData getPlanTaskDataByOrder(@Param("groupName") String groupName, @Param("orderName") String orderName, @Param("someDay") Date someDay);

    List<GeneralSalary> getGroupDailyPieceWorkByInfo(@Param("groupName") String groupName, @Param("orderName") String orderName, @Param("someDay") Date someDay);

    List<Integer> getActIdByDeliveryState(@Param("deliveryState") String deliveryState);

    Integer getFinishCountByTaskBegin(@Param("groupName") String groupName, @Param("orderName") String orderName, @Param("startDate") Date startDate);
}
