package com.example.erp01.mapper;

import com.example.erp01.model.CutLocation;
import com.example.erp01.model.Storage;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface CutInStoreMapper {

    int cutInStore(@Param("cutInStoreList") List<Storage> cutInStoreList);

    int deleteCutInStore(@Param("tailorQcodeList") List<String> tailorQcodeList);

//    List<String> getCutLocationByColorSize(@Param("orderName") String orderName, @Param("colorName") String colorName, @Param("sizeName") String sizeName);

    List<String> getCutLocationByPackage(@Param("orderName") String orderName, @Param("bedNumber") Integer bedNumber, @Param("packageNumber") Integer packageNumber);

    List<CutLocation> getCutInfo(@Param("orderName") String orderName,@Param("bedNumber") Integer bedNumber, @Param("colorName") String colorName, @Param("sizeName") String sizeName,  @Param("packageNumber") Integer packageNumber);

    Integer deleteCutInStoreByInfo(@Param("orderName") String orderName, @Param("bedNumber") Integer bedNumber, @Param("packageNumber") Integer packageNumber);

}
