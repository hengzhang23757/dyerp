package com.example.erp01.mapper;

import com.example.erp01.model.DepartmentGroup;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
@Mapper
public interface DepartmentGroupMapper {

    Integer addDepartmentGroup(@Param("departmentGroup") DepartmentGroup departmentGroup);

    Integer deleteDepartmentGroup(@Param("id") Integer id);

    List<DepartmentGroup> getAllDepartmentGroup();

    DepartmentGroup getDepartmentGroupByDepartment(@Param("departmentName") String departmentName);

    List<String> getAllCheckGroupName();

    List<String> getAllDepartmentName();

    List<String> getAllGroupName();

    List<String> listGroupNameByDepartment(@Param("departmentName") String departmentName);
}
