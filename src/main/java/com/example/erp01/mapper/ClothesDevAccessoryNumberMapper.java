package com.example.erp01.mapper;

import com.example.erp01.model.ClothesDevAccessoryNumber;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ClothesDevAccessoryNumberMapper {

    Integer addClothesDevAccessoryNumber(@Param("clothesDevAccessoryNumber") ClothesDevAccessoryNumber clothesDevAccessoryNumber);

    Integer deleteClothesDevAccessoryNumber(@Param("id") Integer id);

    List<ClothesDevAccessoryNumber> getAllClothesDevAccessoryNumber();

    Integer updateClothesDevAccessoryNumber(@Param("clothesDevAccessoryNumber") ClothesDevAccessoryNumber clothesDevAccessoryNumber);

}
