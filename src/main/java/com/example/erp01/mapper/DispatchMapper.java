package com.example.erp01.mapper;

import com.example.erp01.model.Dispatch;
import com.example.erp01.model.ProcedureInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface DispatchMapper {

    int addDispatchBatch(List<Dispatch> dispatchList);

    int addDispatch(Dispatch dispatch);

    int deleteDispatch(Integer dispatchID);

    List<Dispatch> getAllDispatch();

    List<Dispatch> getDispatchByOrder(String orderName);

    List<Dispatch> getDispatchByGroup(String groupName);

    List<Dispatch> getDispatchByOrderGroup(@Param("orderName")String orderName,@Param("groupName")String groupName);

    List<Dispatch> getDispatchByEmp(String employeeNumber);

    List<ProcedureInfo> getProcedureInfoByOrderEmp(@Param("orderName")String orderName,@Param("employeeNumber") String employeeNumber);

    List<String> getDistinctEmployeeNameByOrder(@Param("orderName")String orderName, @Param("groupName")String groupName);

    List<Dispatch> getDispatchByOrderGroupEmp(@Param("orderName")String orderName,@Param("groupName")String groupName, @Param("employeeName")String employeeName);

    List<Dispatch> getTodayDispatch();

    List<Dispatch> getOneMonthDispatch();

    List<Dispatch> getThreeMonthsDispatch();

    Integer deleteDispatchByOrderEmployeeList(@Param("orderName")String orderName,@Param("employeeNumberList") List<String> employeeNumberList);
}
