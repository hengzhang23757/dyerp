package com.example.erp01.mapper;

import com.example.erp01.model.EndProduct;
import com.example.erp01.model.EndProductPay;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface EndProductMapper {

    Integer addEndProductBatch(@Param("endProductList") List<EndProduct> endProductList);

    Integer updateEndProduct(@Param("endProduct") EndProduct endProduct);

    Integer updateEndProductBatch(@Param("endProductList") List<EndProduct> endProductList);

    EndProduct getInStoreEndProductByOrderColorSize(@Param("orderName") String orderName, @Param("colorName") String colorName, @Param("sizeName") String sizeName, @Param("endType") String endType);

    Integer getOutStoreProductCountByOrderColorSize(@Param("orderName") String orderName, @Param("colorName") String colorName, @Param("sizeName") String sizeName, @Param("endType") String endType);

    List<EndProduct> getEndProductSummary();

    Integer deleteEndProductBatch(@Param("idList") List<Integer> idList);

    Integer deleteEndProduct(@Param("id") Integer id);

    List<EndProduct> getEndProductInStoreInfo(@Param("orderName") String orderName, @Param("endType") String endType);

    List<EndProduct> getEndProductStorageInfo(@Param("orderName") String orderName, @Param("endType") String endType);

    List<EndProduct> getEndProductOutStoreInfo(@Param("orderName") String orderName, @Param("endType") String endType);

    List<EndProduct> getEndProductStorageSummary(@Param("endType") String endType, @Param("orderName") String orderName);

    List<EndProduct> getEndProductByIdList(@Param("idList") List<Integer> idList);

    List<String> getEndProductStorageColorList(@Param("orderName") String orderName, @Param("endType") String endType);

    List<String> getEndProductStorageSizeList(@Param("orderName") String orderName, @Param("endType") String endType);

    Integer getOutStoreCountByOrderName(@Param("orderName") String orderName, @Param("endType") String endType);

    EndProduct getStorageByOrderColorSize(@Param("orderName") String orderName, @Param("colorName") String colorName, @Param("sizeName") String sizeName, @Param("endType") String endType);

    List<EndProduct> getDefectiveSummary();

    List<EndProduct> getDefectiveEndProductSummary(@Param("orderName") String orderName);

    List<EndProduct> getDefectiveEndProductDetailInByOrder(@Param("orderName") String orderName);

    List<EndProduct> getDefectiveEndProductDetailStorageByOrder(@Param("orderName") String orderName);

    List<EndProduct> getDefectiveEndProductDetailOutByOrder(@Param("orderName") String orderName);

    EndProduct getEndProductById(@Param("id") Integer id);

    Integer endProductPreCheck(@Param("idList") List<Integer> idList);

    List<EndProduct> getPreAccountEndProduct();

    Integer endProductTakeIntoAccount(@Param("idList") List<Integer> idList, @Param("accountDate") String accountDate, @Param("payNumber") String payNumber);

    List<EndProduct> getAllEndProduct();

    List<EndProduct> getEndProductOutStoreByInfo(@Param("monthStr") String monthStr, @Param("customerName") String customerName, @Param("orderName") String orderName, @Param("endType") String endType);

    List<EndProduct> getEndProductAccountByOrderNameList(@Param("orderNameList") List<String> orderNameList);

    List<EndProduct> getEndProductSummaryByCustomer(@Param("from") String from, @Param("to") String to);

    Integer quitAccountBatch(@Param("idList") List<Integer> idList);

    List<EndProductPay> endProductToEndProductPay(@Param("payNumber") String payNumber);

    Integer quitAccountByPayNumber(@Param("payNumber") String payNumber);

    Integer updatePriceByOrderName(@Param("orderName") String orderName, @Param("colorName") String colorName, @Param("price") Float price);

    List<String> getPayNumberByOrderName(@Param("orderName") String orderName);

    Float getSumMoneyByPayNumber(@Param("payNumber") String payNumber);
}
