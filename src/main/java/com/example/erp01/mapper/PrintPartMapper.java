package com.example.erp01.mapper;

import com.example.erp01.model.PrintPart;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface PrintPartMapper {

    int addPrintPart(@Param("printPart") PrintPart printPart);

    int addPrintPartBatch(List<PrintPart> printPartList);

    int deletePrintPart(@Param("printPartID") Integer printPartID);

    List<String> getPrintPartByOrder(@Param("orderName") String orderName);

    List<String> getMainPrintPartByOrder(@Param("orderName") String orderName);

    List<String> getOtherPrintPartByOrder(@Param("orderName") String orderName);

    List<PrintPart> getAllPrintPart();

    List<PrintPart> getAllPrintPartByOrder(@Param("orderName") String orderName);

}
