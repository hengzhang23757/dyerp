package com.example.erp01.mapper;

import com.example.erp01.model.GroupProgress;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

@Mapper
public interface GroupProgressMapper {

    List<GroupProgress> getGroupEmbOutByInfo(@Param("from") Date from, @Param("to") Date to, @Param("orderName") String orderName, @Param("groupList") List<String> groupList);

    List<GroupProgress> getAllGroupEmbOutByInfo(@Param("from") Date from, @Param("to") Date to, @Param("orderName") String orderName);

    List<GroupProgress> getGroupHangCountByInfo(@Param("from") Date from, @Param("to") Date to, @Param("orderName") String orderName, @Param("groupList") List<String> groupList);

    List<GroupProgress> getAllGroupHangCountByInfo(@Param("from") Date from, @Param("to") Date to, @Param("orderName") String orderName);

    List<GroupProgress> getGroupInspectionCountByInfo(@Param("from") Date from, @Param("to") Date to, @Param("orderName") String orderName, @Param("groupList") List<String> groupList);

    List<GroupProgress> getAllGroupInspectionCountByInfo(@Param("from") Date from, @Param("to") Date to, @Param("orderName") String orderName);

    List<GroupProgress> getEachDayGroupEmbOutByInfo(@Param("from") Date from, @Param("to") Date to, @Param("orderName") String orderName, @Param("groupList") List<String> groupList);

    List<GroupProgress> getEachDayGroupHangCountByInfo(@Param("from") Date from, @Param("to") Date to, @Param("orderName") String orderName, @Param("groupList") List<String> groupList);

    List<GroupProgress> getEachDayGroupInspectionCountByInfo(@Param("from") Date from, @Param("to") Date to, @Param("orderName") String orderName, @Param("groupList") List<String> groupList);

    List<GroupProgress> getGroupCheckCountByInfo(@Param("from") Date from, @Param("to") Date to, @Param("orderName") String orderName, @Param("groupList") List<String> groupList);

    List<GroupProgress> getAllGroupCheckCountByInfo(@Param("from") Date from, @Param("to") Date to, @Param("orderName") String orderName);

    List<GroupProgress> getEachDayGroupCheckCountByInfo(@Param("from") Date from, @Param("to") Date to, @Param("orderName") String orderName, @Param("groupList") List<String> groupList);



    List<GroupProgress> getGroupProgressByTimeProcedure(@Param("from") String from, @Param("to") String to, @Param("orderName") String orderName, @Param("groupList") List<String> groupList, @Param("procedureNumberList") List<Integer> procedureNumberList);

    List<GroupProgress> getGroupProgressByTimeProcedureGroupByDay(@Param("from") String from, @Param("to") String to, @Param("orderName") String orderName, @Param("groupList") List<String> groupList, @Param("procedureNumberList") List<Integer> procedureNumberList);
}
