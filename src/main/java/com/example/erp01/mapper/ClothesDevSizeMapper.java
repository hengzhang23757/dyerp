package com.example.erp01.mapper;

import com.example.erp01.model.ClothesDevSize;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface ClothesDevSizeMapper {

    Integer addClothesDevSizeBatch(@Param("clothesDevSizeList") List<ClothesDevSize> clothesDevSizeList);

    List<ClothesDevSize> getClothesDevSizeByMap(Map<String, Object> map);

    Integer deleteClothesDevSizeByDevId(@Param("devId") Integer devId);

    Integer deleteClothesDevSizeByDevNumber(@Param("clothesDevNumber") String clothesDevNumber);

}
