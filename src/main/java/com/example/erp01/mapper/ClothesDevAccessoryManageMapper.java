package com.example.erp01.mapper;

import com.example.erp01.model.ClothesDevAccessoryManage;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface ClothesDevAccessoryManageMapper {

    Integer addClothesDevAccessoryManage(@Param("clothesDevAccessoryManage") ClothesDevAccessoryManage clothesDevAccessoryManage);

    List<ClothesDevAccessoryManage> getClothesDevAccessoryManageByMap(Map<String, Object> map);

    Integer updateClothesDevAccessoryManage(@Param("clothesDevAccessoryManage") ClothesDevAccessoryManage clothesDevAccessoryManage);

    Integer deleteClothesDevAccessory(@Param("id") Integer id);

    List<String> getClothsDevAccessoryNameHint(@Param("keyWord") String keyWord);

    List<String> getClothsDevAccessoryNumberHint(@Param("keyWord") String keyWord);

    List<ClothesDevAccessoryManage> getClothesDevAccessoryManageHint(@Param("subAccessoryName") String subAccessoryName, @Param("page") Integer page, @Param("limit") Integer limit);

    List<ClothesDevAccessoryManage> getClothesDevAccessoryManageHintByNumber(@Param("subAccessoryNumber") String subAccessoryNumber, @Param("page") Integer page, @Param("limit") Integer limit);

    Integer deleteClothesDevAccessoryRecordByAccessoryId(@Param("accessoryID") Integer accessoryID);

    ClothesDevAccessoryManage getClothesDevAccessoryManageById(@Param("id") Integer id);

    Integer updateClothesDevAccessoryManageBatch(@Param("clothesDevAccessoryManageList") List<ClothesDevAccessoryManage> clothesDevAccessoryManageList);

    Integer deleteClothesDevAccessoryManageBatch(@Param("idList") List<Integer> idList);

    String getMaxClothesDevAccessoryOrderByPreFix(@Param("preFix") String preFix);

    List<ClothesDevAccessoryManage> getClothesDevAccessoryManageRecordByInfo(Map<String, Object> map);

    Integer quitClothesDevAccessoryManageCheck(@Param("idList") List<Integer> idList);

    List<ClothesDevAccessoryManage> getClothesDevAccessoryManageByAccessoryNumberList(@Param("accessoryNumberList") List<String> accessoryNumberList);

    Integer updateClothesDevAccessoryManageOnlyCount(@Param("clothesDevAccessoryManageList") List<ClothesDevAccessoryManage> clothesDevAccessoryManageList);

    Integer addClothesDevAccessoryManageBatch(@Param("clothesDevAccessoryManageList") List<ClothesDevAccessoryManage> clothesDevAccessoryManageList);

}
