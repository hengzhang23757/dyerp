package com.example.erp01.mapper;

import com.example.erp01.model.FabricHandOver;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

@Mapper
public interface FabricHandOverMapper {

    Integer addFabricHandOverBatch(@Param("fabricHandOverList") List<FabricHandOver> fabricHandOverList);

    List<FabricHandOver> getFabricHandOverByShelf(@Param("shelfName") String shelfName);

    List<FabricHandOver> getFabricHandOverByInfo(@Param("from") Date from, @Param("to") Date to, @Param("orderName") String orderName, @Param("colorName") String colorName, @Param("fabricColor") String fabricColor);

    Integer deleteFabricHandOverByShelf(@Param("shelfName") String shelfName);

    Integer updateFabricHandOverBatch(@Param("fabricHandOverList") List<FabricHandOver> fabricHandOverList);

    List<FabricHandOver> getFabricHandOverByQcode(@Param("qCodeID") Integer qCodeID);

    List<String> getFabricHandOverColorHint(@Param("orderName") String orderName);

    List<String> getFabricHandOverFabricColorHint(@Param("orderName") String orderName);
}
