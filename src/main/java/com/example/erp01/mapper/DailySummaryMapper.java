package com.example.erp01.mapper;

import com.example.erp01.model.DailySummary;
import com.example.erp01.model.OrderProcedure;
import com.example.erp01.model.PieceWork;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

@Mapper
public interface DailySummaryMapper {

    Integer addDailySummaryBatch(@Param("dailySummaryList") List<DailySummary> dailySummaryList);

    List<DailySummary> getDailySummaryOneMonth();

    List<String> getDistinctEmployeeNumberHang(@Param("from") Date from, @Param("to") Date to);

    List<String> getDistinctOrderNameHang(@Param("from") Date from, @Param("to") Date to);

    List<OrderProcedure> getDistinctOrderProcedureHang(@Param("from") Date from, @Param("to") Date to);

    Integer updateHangEmployeeInfo(@Param("employeeNumber") String employeeNumber, @Param("groupName") String groupName);

    Integer updateHangOrderInfo(@Param("orderName") String orderName, @Param("clothesVersionNumber") String clothesVersionNumber);

    Integer updateHangProcedureInfo(@Param("orderName") String orderName, @Param("procedureNumber") Integer procedureNumber, @Param("procedureName") String procedureName);


    List<String> getDistinctEmployeeNumberMini(@Param("from") Date from, @Param("to") Date to);

    List<String> getDistinctOrderNameMini(@Param("from") Date from, @Param("to") Date to);

    List<OrderProcedure> getDistinctOrderProcedureMini(@Param("from") Date from, @Param("to") Date to);

    Integer updateMiniEmployeeInfo(@Param("employeeNumber") String employeeNumber, @Param("employeeName") String employeeName, @Param("groupName") String groupName);

    Integer updateMiniOrderInfo(@Param("orderName") String orderName, @Param("clothesVersionNumber") String clothesVersionNumber);

    Integer updateMiniProcedureInfo(@Param("orderName") String orderName, @Param("procedureNumber") Integer procedureNumber, @Param("procedureName") String procedureName);

    List<PieceWork> getPieceWorkFromHang(@Param("from") Date from, @Param("to") Date to);

    Integer copyPieceWorkFromHangToMini(@Param("pieceWorkList") List<PieceWork> pieceWorkList);

    Integer getMaxNidFromPieceWork();

    List<PieceWork> getPieceWorkFromHangByNid(@Param("nid") Integer nid);
}
