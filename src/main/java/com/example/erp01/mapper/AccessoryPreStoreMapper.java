package com.example.erp01.mapper;

import com.example.erp01.model.AccessoryPreStore;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.omg.PortableInterceptor.INACTIVE;

import java.util.List;
import java.util.Map;

@Mapper
public interface AccessoryPreStoreMapper {

    Integer addAccessoryPreStoreBatch(@Param("accessoryPreStoreList") List<AccessoryPreStore> accessoryPreStoreList);

    Integer deleteAccessoryPreStoreById(@Param("id") Integer id);

    Integer deleteAccessoryPreStoreBatch(@Param("idList") List<Integer> idList);

    Integer updateAccessoryPreStore(@Param("accessoryPreStore") AccessoryPreStore accessoryPreStore);

    List<AccessoryPreStore> getAllAccessoryPreStore();

    List<AccessoryPreStore> getAccessoryPreStoreHint(@Param("subAccessoryName") String subAccessoryName, @Param("page") Integer page, @Param("limit") Integer limit);

    AccessoryPreStore getAccessoryPreStoreById(@Param("id") Integer id);

    List<AccessoryPreStore> getAccessoryPreStoreByPreId(@Param("preStoreId") Integer preStoreId);

    Integer updateAccessoryPreStoreBatch(@Param("accessoryPreStoreList") List<AccessoryPreStore> accessoryPreStoreList);

    Integer addAccessoryPreStore(@Param("accessoryPreStore") AccessoryPreStore accessoryPreStore);

    List<AccessoryPreStore> getAccessoryPreStoreByAccessoryNumber(@Param("accessoryNumber") String accessoryNumber, @Param("operateType") String operateType);

    String getMaxAccessoryOrderByPreFix(@Param("preFix") String preFix);

    List<AccessoryPreStore> getAccessoryStorageByAccessoryNumberList(@Param("accessoryNumberList") List<String> accessoryNumberList);

    List<AccessoryPreStore> getAccessoryPreStoreByInfo(Map<String, Object> map);

    Integer quitCheckAccessoryPreStoreBatch(@Param("idList") List<Integer> idList);

    List<AccessoryPreStore> getHistoryIndexAccessoryPreStoreByInfo(Map<String, Object> map);

    Integer deleteAccessoryPreStoreByPreId(@Param("preStoreId") Integer preStoreId);

    List<AccessoryPreStore> getAccessoryPreStoreForCombine();

    Integer updateAccessoryPreStoreBatchInCombineOrder(@Param("accessoryPreStoreList") List<AccessoryPreStore> accessoryPreStoreList);

    List<AccessoryPreStore> getAccessoryPreStoreForCheck(Map<String, Object> param);

    List<AccessoryPreStore> getAccessoryPreStoreByCheckNumber(@Param("checkNumber") String checkNumber);

    Integer updateAccessoryPreStoreOrderState(@Param("checkNumber") String checkNumber, @Param("orderState") String orderState);

    Integer updateAccessoryPreStoreCheckStateByCheckNumber(@Param("checkNumber") String checkNumber, @Param("checkState") String checkState);

    Integer getAccessoryPreStoreCheckCount();

    AccessoryPreStore getMaxStorageAccessoryPreStoreByAccessoryNumber(@Param("accessoryNumber") String accessoryNumber, @Param("specification") String specification, @Param("accessoryColor") String accessoryColor);

    Integer updateAccessoryAfterCommit(@Param("id") Integer id, @Param("accessoryCount") Float accessoryCount, @Param("price") Float price);

    Integer updateAccessoryPreStoreInMonthCheck(@Param("accessoryPreStoreList") List<AccessoryPreStore> accessoryPreStoreList);
}
