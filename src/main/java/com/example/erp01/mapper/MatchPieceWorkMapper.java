package com.example.erp01.mapper;

import com.example.erp01.model.MatchPieceWork;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface MatchPieceWorkMapper {

    Integer addMatchPieceWorkBatch(@Param("matchPieceWorkList") List<MatchPieceWork> matchPieceWorkList);

    List<MatchPieceWork> getMatchPieceWorkSummary();

    Integer deleteMatchPieceWorkByInfo(@Param("orderName") String orderName, @Param("colorName") String colorName, @Param("sizeName") String sizeName);

    Integer deleteMatchPieceWorkByIdList(@Param("pieceWorkIdList") List<Integer> pieceWorkIdList);

    List<MatchPieceWork> getMatchPieceWorkByInfo(@Param("orderName") String orderName, @Param("colorName") String colorName, @Param("sizeName") String sizeName);

    Integer deleteMatchPieceWorkById(@Param("pieceWorkID") Integer pieceWorkID);

    Integer addMatchPieceWork(@Param("matchPieceWork") MatchPieceWork matchPieceWork);
}
