package com.example.erp01.mapper;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface MaxTailorQcodeIDMapper {

    int getMaxTailorQcodeId();

    int updateMaxTailorQcodeId(int qCodeId);

}
