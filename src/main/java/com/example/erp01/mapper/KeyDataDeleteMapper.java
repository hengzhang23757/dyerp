package com.example.erp01.mapper;

import com.example.erp01.model.PieceWork;
import com.example.erp01.model.PieceWorkBack;
import com.example.erp01.model.PieceWorkDelete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Mapper
public interface KeyDataDeleteMapper {

    PieceWorkDelete getPieceWorkByDeleteId(@Param("pieceWorkID")Integer pieceWorkID);

    List<PieceWorkDelete> getPieceWorkListByDeleteId(@Param("pieceWorkIDList")List<Integer> pieceWorkIDList);

    Integer addPieceWorkDeleteBatch(@Param("pieceWorkDeleteList")List<PieceWorkDelete> pieceWorkDeleteList);

    String getEarlyCreateTimeOfDeleteData(@Param("pieceWorkIDList")List<Integer> pieceWorkIDList);

    List<PieceWork> getPieceWordByStep(@Param("pieceWorkDate") Date pieceWorkDate);

    Integer addPieceWordBackBatch(@Param("pieceWorkBackList")List<PieceWorkBack> pieceWorkBackList);

    Integer deletePieceWorkBacked(@Param("pieceWorkDate") Date pieceWorkDate);

    List<PieceWorkDelete> getPieceWorkDeleteByMap(Map<String, Object> map);

    Integer deleteKeyData(@Param("idList")List<Integer> idList);

}
