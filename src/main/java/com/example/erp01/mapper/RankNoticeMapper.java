package com.example.erp01.mapper;

import com.example.erp01.model.RankNotice;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface RankNoticeMapper {

    Integer addRankNotice(@Param("rankNotice") RankNotice rankNotice);

    Integer deleteRankNotice(@Param("id") Integer id);

    List<RankNotice> getRankNoticeByRole(@Param("role")String role);

}
