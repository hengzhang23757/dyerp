package com.example.erp01.mapper;

import com.example.erp01.model.UnitConsumption;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface UnitConsumptionMapper {

    Float getPlanConsumptionByJar(@Param("orderName") String orderName, @Param("colorName") String colorName, @Param("jarName") String jarName);

    List<UnitConsumption> getConsumptionInfoByOrder(@Param("orderName") String orderName, @Param("colorName") String colorName);

    Float getReturnFabricCountByOrderColorJar(@Param("orderName") String orderName, @Param("colorName") String colorName, @Param("jarName") String jarName);



    List<UnitConsumption> getOtherConsumptionSummaryOfEachBed(@Param("orderName") String orderName, @Param("colorName") String colorName, @Param("partName") String partName);

    List<UnitConsumption> getOtherBasicInfoOfEachBed(@Param("orderName") String orderName, @Param("colorName") String colorName, @Param("partName") String partName);

    Float getOtherPlanConsumptionByJar(@Param("orderName") String orderName, @Param("colorName") String colorName, @Param("jarName") String jarName);

    Integer getOtherSumLayerCountOfByOrderColorName(@Param("orderName") String orderName, @Param("colorName") String colorName, @Param("partName") String partName);

    List<UnitConsumption> getOtherConsumptionSummaryOfOrder(@Param("orderName") String orderName, @Param("colorName") String colorName, @Param("partName") String partName);

    Float getOtherActConsumptionByColor(@Param("orderName") String orderName, @Param("colorName") String colorName, @Param("partName") String partName);

    Float getOtherReturnFabricCountByOrderColorJar(@Param("orderName") String orderName, @Param("colorName") String colorName, @Param("jarName") String jarName);



    List<UnitConsumption> getConsumptionSummaryOfOrder(@Param("orderName") String orderName, @Param("colorName") String colorName, @Param("partName") String partName, @Param("tailorType") Integer tailorType);

    Float getActConsumptionByColor(@Param("orderName") String orderName, @Param("colorName") String colorName, @Param("partName") String partName, @Param("tailorType") Integer tailorType);

    Integer getSumLayerCountOfByOrderColorName(@Param("orderName") String orderName, @Param("colorName") String colorName, @Param("partName") String partName, @Param("tailorType") Integer tailorType);

    List<UnitConsumption> getBasicInfoOfEachBed(@Param("orderName") String orderName, @Param("colorName") String colorName, @Param("partName") String partName, @Param("tailorType") Integer tailorType);

    List<UnitConsumption> getConsumptionSummaryOfEachBed(@Param("orderName") String orderName, @Param("colorName") String colorName, @Param("partName") String partName, @Param("tailorType") Integer tailorType);

    String getMaxJarNumberByOrderColorPartType(@Param("orderName")String orderName, @Param("colorName")String colorName, @Param("partName")String partName, @Param("tailorType")Integer tailorType);

}
