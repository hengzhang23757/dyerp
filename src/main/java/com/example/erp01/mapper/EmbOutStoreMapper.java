package com.example.erp01.mapper;

import com.example.erp01.model.*;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

@Mapper
public interface EmbOutStoreMapper {

    int addEmbOutStore(List<EmbOutStore> embOutStoreList);

    int addEmbOutStoreOne(EmbOutStore embOutStore);

    int deleteEmbOutStore(Integer embOutStoreID);

    Integer getEmbOutStoreCount(String groupName, String orderName, String colorName, String sizeName, Date embPlanDate);

    List<EmbOutDetail> getEmbOutDetail(@Param("from")Date from,@Param("to") Date to);

    List<TailorReport> getEmbOutPackageDetail(String orderName, String from, String to, String groupName);

    List<CutQueryLeak> getEmbOutInfoByOrder(@Param("orderName") String orderName);

    List<CutLocation> getEmbOutInfo(@Param("orderName") String orderName, @Param("bedNumber") Integer bedNumber, @Param("colorName") String colorName, @Param("sizeName") String sizeName, @Param("packageNumber") Integer packageNumber);

    List<EmbOutDaily> getEmbOutDaily(@Param("orderName") String orderName);

    List<WeekAmount> getEmbOutWeekAmount();

    List<CompletionCount> getEmbOutCompletion();

    Integer getEmbOutSumCount(@Param("orderName") String orderName);

    List<EmbStorage> getEmbOutStoreGroupByColor(@Param("orderName") String orderName);

    EmbOutStore getEmbOutStoreByInfo(@Param("orderName") String orderName, @Param("bedNumber") Integer bedNumber, @Param("packageNumber") Integer packageNumber);

    Integer deleteEmbOutStoreByInfo(@Param("orderName") String orderName, @Param("bedNumber") Integer bedNumber, @Param("packageNumber") Integer packageNumber);

    List<CutQueryLeak> getEmbOutStoreGroupByColorSize(@Param("orderName") String orderName, @Param("colorName") String colorName, @Param("sizeName") String sizeName);

    List<EmbOutDetail> getAmongEmbOut(@Param("fromDate")Date fromDate,@Param("toDate") Date toDate, @Param("orderName") String orderName);

    List<TailorReport> getEmbOutPackageCountByInfo(@Param("orderName") String orderName, @Param("from") String from,  @Param("to") String to, @Param("groupName") String groupName);

}
