package com.example.erp01.mapper;

import com.example.erp01.model.DailyFabric;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface DailyFabricMapper {

    Integer addDailyFabricBatch(@Param("dailyFabricList") List<DailyFabric> dailyFabricList);

    List<DailyFabric> getDailyFabricOneMonth();

}
