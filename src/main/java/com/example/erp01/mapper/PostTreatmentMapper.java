package com.example.erp01.mapper;

import com.example.erp01.model.PieceWork;
import com.example.erp01.model.PostTreatment;
import com.example.erp01.model.Tailor;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface PostTreatmentMapper {

    Integer addPostTreatmentBatch(@Param("postTreatmentList") List<PostTreatment> postTreatmentList);

    List<PostTreatment> getPostTreatmentByOrder(@Param("orderName") String orderName);

    List<Tailor> getWellCountByOrderNameList(@Param("orderNameList") List<String> orderNameList, @Param("from") String from, @Param("to") String to);

    List<PieceWork> getPieceWorkSummaryByOrderNameList(@Param("orderNameList") List<String> orderNameList, @Param("procedureNumberList") List<Integer> procedureNumberList, @Param("from") String from, @Param("to") String to);

    List<PostTreatment> getPostTreatmentByOrderNameList(@Param("orderNameList") List<String> orderNameList, @Param("from") String from, @Param("to") String to);

    List<PostTreatment> getPostTreatmentByOrderType(@Param("orderName") String orderName, @Param("washCount") Integer washCount, @Param("shipCount") Integer shipCount, @Param("surplusCount") Integer surplusCount, @Param("defectiveCount") Integer defectiveCount, @Param("chickenCount") Integer chickenCount, @Param("sampleCount") Integer sampleCount);

    Integer deletePostTreatmentById(@Param("id") Integer id);

    Integer updatePostTreatment(@Param("postTreatment") PostTreatment postTreatment);
}
