package com.example.erp01.mapper;

import com.example.erp01.model.SupplierInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
@Mapper
public interface SupplierInfoMapper {

    Integer addSupplierInfo(@Param("supplierInfo") SupplierInfo supplierInfo);

    List<SupplierInfo> getSupplierInfoHint(@Param("subSupplierName") String subSupplierName,@Param("page") Integer page,@Param("limit") Integer limit);

    List<SupplierInfo> getAllSupplierInfo();

    Integer updateSupplierInfo(@Param("supplierInfo") SupplierInfo supplierInfo);

    Integer deleteSupplierInfo(@Param("id") Integer id);

    List<String> getAllSupplierNameByInfo(@Param("supplierType") String supplierType);
}
