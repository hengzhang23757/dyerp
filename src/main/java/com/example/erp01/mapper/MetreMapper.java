package com.example.erp01.mapper;

import com.example.erp01.model.InnerBoard;
import com.example.erp01.model.Metre;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

@Mapper
public interface MetreMapper {

    List<Metre> getMetreByOrderTimeEmp(@Param("procedureNumber")Integer procedureNumber, @Param("orderName") String orderName, @Param("employeeName")String employeeName, @Param("fromTime")Date fromTime, @Param("toTime")Date toTime);

    List<Metre> getMetreByInfo(@Param("orderName") String orderName, @Param("procedureNumberList") List<Integer> procedureNumberList , @Param("groupNameList") List<String> groupNameList, @Param("employeeNameList") List<String> employeeNameList, @Param("fromTime")Date fromTime, @Param("toTime")Date toTime);

}
