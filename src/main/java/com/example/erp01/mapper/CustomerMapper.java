package com.example.erp01.mapper;

import com.example.erp01.model.Customer;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
public interface CustomerMapper {

    Integer addCustomer(@Param("customer")Customer customer);

    Integer updateCustomer(@Param("customer")Customer customer);

    List<Customer> getAllCustomer();

    Integer deleteCustomer(@Param("id")Integer id);

    String getMaxCustomerCodeByPreFix(@Param("preFix")String preFix);

    List<Customer> getCustomerHintPage(@Param("keyWord")String keyWord, @Param("page")int page, @Param("limit")int limit);

    Integer getCustomerCount(@Param("keyWord")String keyWord);

}
