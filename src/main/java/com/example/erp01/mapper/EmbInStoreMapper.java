package com.example.erp01.mapper;


import com.example.erp01.model.CutLocation;
import com.example.erp01.model.CutQueryLeak;
import com.example.erp01.model.EmbStorage;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface EmbInStoreMapper {

    int addEmbInStore(@Param("embStorageList") List<EmbStorage> embStorageList);

    int deleteEmbInStore(@Param("subQcodeList") List<String> subQcodeList);

    List<CutLocation> getEmbInfo(@Param("orderName") String orderName,@Param("bedNumber") Integer bedNumber,@Param("colorName") String colorName,@Param("sizeName") String sizeName,@Param("packageNumber") Integer packageNumber);

    Integer getTotalEmbInStoreCountByOrder(@Param("orderName") String orderName);

    List<EmbStorage> getEmbInStoreGroupByColor(@Param("orderName") String orderName);

    List<CutQueryLeak> getEmbInStoreInfoGroupByColorSize(@Param("orderName") String orderName, @Param("colorName") String colorName, @Param("sizeName") String sizeName);
}
