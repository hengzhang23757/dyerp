package com.example.erp01.mapper;

import com.example.erp01.model.OrderClothes;
import com.example.erp01.model.PrenatalProgress;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
@Mapper
public interface PrenatalProgressMapper {

    List<OrderClothes> getWorkOrderByTime(@Param("from")String from, @Param("to")String to);

    List<PrenatalProgress> getPreCutCountByOrder(@Param("orderName")String orderName);

}
