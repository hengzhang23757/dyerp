package com.example.erp01.mapper;

import com.example.erp01.model.DetectionTemplate;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface DetectionTemplateMapper {

    Integer addDetectionTemplate(@Param("detectionTemplate") DetectionTemplate detectionTemplate);

    Integer updateDetectionTemplate(@Param("detectionTemplate") DetectionTemplate detectionTemplate);

    Integer deleteDetectionTemplate(@Param("id") Integer id);

    List<DetectionTemplate> getDetectionTemplateByInfo(Map<String, Object> map);

    DetectionTemplate getDetectionTemplateById(@Param("id") Integer id);

    List<String> getDetectionNameByDetectionType(@Param("detectionType") String detectionType);

}
