package com.example.erp01.mapper;

import com.example.erp01.model.*;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface YesterdayReportMapper {

    List<YesterdayTailor> getYesterdayTailorReport();

    List<YesterdayReWork> getYesterdayReWorkReport();

    List<YesterdayWorkShop> getYesterdayHangProduction();

    List<YesterdayWorkShop> getYesterdayMiniProduction();

    Integer getHangWorkCountByProcedureNumber(@Param("orderName")String orderName, @Param("procedureNumber") Integer procedureNumber);

    Integer getMiniWorkCountByProcedureNumber(@Param("orderName")String orderName, @Param("procedureNumber") Integer procedureNumber);

    List<YesterdayFinish> getYesterdayFinishReport();

    List<YesterdayFinish> getYesterdayHangFinishReport();

    List<DailyFabric> getYesterdayFabricReport();
}
