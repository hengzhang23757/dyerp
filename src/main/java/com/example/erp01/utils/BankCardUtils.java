package com.example.erp01.utils;

import java.net.URLEncoder;

/**
 * Created by hujian on 2019/11/20
 */
public class BankCardUtils {

    public static String bankCard(String imgUrl,String accessToken) {
        // 请求url
        String url = "https://aip.baidubce.com/rest/2.0/ocr/v1/bankcard";
        try {
            // 本地文件路径

            String imgParam = URLEncoder.encode(imgUrl, "UTF-8");
            String param = "image=" + imgParam;

            String result = HttpUtil.post(url, accessToken, param);
            System.out.println(result);
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void main(String[] args) {
        String accessToken = AuthService.getAuth("5jGZyD8psDCGeIKAj4R1zQlE","M2VbG3fHTCBDV5HVt0grjnnGcBhSA76u");
        String filePath = "/Users/hujian/Documents/图片/WechatIMG99.jpeg";
        BankCardUtils.bankCard(filePath, accessToken);
    }
}
