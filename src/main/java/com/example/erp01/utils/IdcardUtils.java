package com.example.erp01.utils;

import org.json.JSONObject;

import java.net.URLEncoder;

/**
 * Created by hujian on 2019/11/20
 */
public class IdcardUtils {

    public static String idcard(String imgUrl,String accessToken) {
        // 请求url
        String url = "https://aip.baidubce.com/rest/2.0/ocr/v1/idcard";
        try {
            // 本地文件路径
            String imgParam = URLEncoder.encode(imgUrl, "UTF-8");

            String param = "detect_photo=true&id_card_side=" + "front" + "&image=" + imgParam;

            // 注意这里仅为了简化编码每一次请求都去获取access_token，线上环境access_token有过期时间， 客户端可自行缓存，过期后重新获取。

            String result = HttpUtil.post(url, accessToken, param);
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void main(String[] args) {
        String accessToken = AuthService.getAuth("QvkcnSbQG7vKHeWCgoyrV2DU","wT1UonQzrg0HdgDURPrikTvvW38ne49P");
        String filePath = "C:\\Users\\62533\\Desktop\\id1.jpeg";
        System.out.println(IdcardUtils.idcard(filePath,accessToken));
    }
}
