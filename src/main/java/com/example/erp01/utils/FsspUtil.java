package com.example.erp01.utils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class FsspUtil {

    /***
     *
     * @param empNum 员工人数
     * @param procedureNum 工序数
     * @param skills 员工对应的工序技能产量
     * @return
     */
    public static double[][] createMatricA(int empNum,int procedureNum, double[][] skills){
        double[][] a = new double[empNum+procedureNum-1][];
        for(int i=0;i<empNum;i++){
            double[] tmp = new double[empNum*procedureNum];
            for(int j=i*procedureNum;j<(i+1)*procedureNum;j++){
                tmp[j]=1.0;
            }
            a[i] = tmp;
        }
        for(int i = empNum;i<empNum+procedureNum-1;i++){
            double[] tmp = new double[empNum*procedureNum];
            for(int m=0;m<empNum;m++){
               tmp[(i-empNum)+m*procedureNum]=-skills[m][i-empNum];
               tmp[(i-empNum+1)+m*procedureNum]=skills[m][i-empNum+1];
            }
            a[i] = tmp;
        }

        return a;
    }

    public static double[] createB(int empNum,int procedureNum){
        double[] b = new double[empNum+procedureNum-1];
        for(int i=0;i<empNum;i++){
            b[i] = 1;
        }
        for(int i = empNum;i<empNum+procedureNum-1;i++){
            b[i] = 0;
        }

        return b;
    }

    //总工序产量和最大
    public static double[] createC(int empNum,int procedureNum, double[][] skills) {
        double[] c = new double[empNum*procedureNum];
        for(int i=0;i<empNum;i++){
            for(int j=0;j<procedureNum;j++){
                c[j+i*procedureNum] = skills[i][j];
            }
        }
        return c;
    }
    //最后一个工序值最大为目标
    public static double[] createC2(int empNum,int procedureNum, double[][] skills) {
        double[] c = new double[empNum*procedureNum];
        for(int i=0;i<empNum;i++){
            c[(procedureNum-1)+i*procedureNum] = skills[i][procedureNum-1];
        }
        return c;
    }

    public static Map<String,Object> solve(double[][] skills, int empNum, int procedureNum){
        double[][] matricA = createMatricA(empNum, procedureNum, skills);
        double[] b = createB(empNum,procedureNum);
        double[] c = createC(empNum,procedureNum,skills);
        return LinearProgramming.solveMap(matricA, b, c);
    }

    public static Map<String, Object> calculateBalance(double[] dailyOutput) {
        Map<String, Object> result = new HashMap<>();

        double sumBalance = 0;
        double averageBalance = 0;
        double minDaily = dailyOutput[0];
        for(int i=0; i<dailyOutput.length; i++){
            dailyOutput[i] = Math.floor(dailyOutput[i]);
            minDaily = minDaily<dailyOutput[i]?minDaily:dailyOutput[i];
        }
        for(int i=0; i<dailyOutput.length; i++){
            sumBalance += minDaily/dailyOutput[i];
        }
        averageBalance = sumBalance/dailyOutput.length;
        result.put("averageBalance",averageBalance);
        result.put("dailyOutput",dailyOutput);
        result.put("minDaily",minDaily);
        return result;
    }


    public static void main(String[] args) {
        //挂片/平车领骨/平车0.6宽单线/前领单线/平车车码唛/剪膊绳/四线级骨/四线级骨上袖/四线上罗纹领/冚车捆条/冚车网底/中查
        double[][] skills = new double[][]{
                {290,0,0,0,0,0,0,0,0,0,0,0}, //练红连
                {0,320,0,0,125,0,0,0,0,0,0,0}, //曾培
                {0,0,99,0,0,0,0,0,0,0,0,0}, //黄花梅
                {0,0,125,0,0,0,0,0,0,0,0,0}, //孟小弯
                {0,0,110,0,0,0,0,0,0,310,0,0}, //杨燕
                {0,0,0,170,0,0,0,0,0,0,0,0}, //谭秋萍
                {0,0,0,155,135,0,0,0,0,0,0,0}, //伍梅萍
                {0,0,0,0,160,0,0,0,0,0,0,0}, //黄梦炜
                {0,0,0,0,0,290,0,0,0,0,0,0}, //陈倩
                {0,0,0,0,0,0,55,55,0,0,0,0}, //曾小欣
                {0,0,0,0,0,0,20,20,0,0,0,0}, //黄小红
                {0,0,0,0,0,0,45,45,0,0,0,0}, //唐其端
                {0,0,0,0,0,0,56,56,0,0,0,0}, //邬白雪
                {0,0,0,0,0,0,57,57,0,0,0,0}, //张发芬
                {0,0,0,0,0,0,0,0,170,0,0,0}, //王文
                {0,0,0,0,0,0,0,0,140,0,0,0}, //黄木娇
                {0,0,0,0,0,0,0,0,0,0,115,0}, //王改玲
                {0,0,0,0,0,0,0,0,0,0,120,0}, //赵廷兵
                {0,0,0,0,0,0,0,0,0,0,0,126}, //邵翠映
                {0,0,0,0,0,0,30,30,0,0,0,115}, //张国秀
        };
        double[][] matricA = FsspUtil.createMatricA(20, 12, skills);
        double[] b = FsspUtil.createB(20,12);
        double[] c = FsspUtil.createC(20,12,skills);
        Map test = LinearProgramming.solveMap(matricA, b, c);
        double[] x = (double[])test.get("x");
        double[] sum = new double[12];
        for (int i = 0; i < 12; i++){
            for(int j = 0; j < 20; j++){
                sum[i] = sum[i] + skills[j][i]*x[j*12+i];
            }
        }
        Map<String, Object> res = calculateBalance(sum);
        System.out.println(Arrays.deepToString(matricA));
        System.out.println(Arrays.toString(b));
        System.out.println(Arrays.toString(c));
        System.out.println(test.toString());
        System.out.println(Arrays.toString((double[])test.get("x")));
    }
}
