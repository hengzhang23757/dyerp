package com.example.erp01.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class DateUtils {

    private static final SimpleDateFormat defaultFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static final SimpleDateFormat yyMMFormat = new SimpleDateFormat("yyyy-MM");


    public static String getFirstDayOfMonth(String yearMonth) {
        try {
            Calendar cale = Calendar.getInstance();
            cale.setTime(yyMMFormat.parse(yearMonth));
            cale.add(Calendar.MONTH, 0);
            cale.set(Calendar.DAY_OF_MONTH, 1);
            String firstDayOfMonth = defaultFormat.format(cale.getTime()); // 当月第一天 2019-02-01
            return firstDayOfMonth;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getLastDayOfMonth(String yearMonth) {
        try {
            Calendar cale = Calendar.getInstance();
            cale.setTime(yyMMFormat.parse(yearMonth));
            cale.add(Calendar.MONTH, 1);
            cale.set(Calendar.DAY_OF_MONTH, 0);
            cale.set(Calendar.HOUR , 23);
            cale.set(Calendar.MINUTE , 59);
            cale.set(Calendar.SECOND , 59);
            String firstDayOfMonth = defaultFormat.format(cale.getTime()); // 当月第一天 2019-02-01
            return firstDayOfMonth;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void main(String[] args) {
        System.out.println(getFirstDayOfMonth("2021-02"));
        System.out.println(getLastDayOfMonth("2021-02"));
    }
}
