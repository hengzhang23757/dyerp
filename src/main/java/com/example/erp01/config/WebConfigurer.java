package com.example.erp01.config;

import com.example.erp01.config.Interceptors.UrlInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.ResourceUtils;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.io.File;
import java.io.FileNotFoundException;

/**
 * Created by hujian on 2019/3/23
 */

@Configuration
public class WebConfigurer implements WebMvcConfigurer {

    @Autowired
    private UrlInterceptor urlInterceptor;

    // 这个方法是用来配置静态资源的，比如html，js，css，等等
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        File path = null;
        try {
            path = new File(ResourceUtils.getURL("classpath:").getPath());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        String gitPath="file:"+File.separator+"home"+File.separator+"uploads"+File.separator;
        System.out.println("______________________:"+gitPath);
        registry.addResourceHandler("/uploads/**").addResourceLocations(gitPath);
        registry.addResourceHandler("/**").addResourceLocations("classpath:/static/");

    }

    // 这个方法用来注册拦截器，我们自己写好的拦截器需要通过这里添加注册才能生效
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(urlInterceptor).addPathPatterns("/**")
                .excludePathPatterns("/erp/userlogin","/erp/mini**","/css/**","/js/**","/font/**","/fonts/**","/images/**","/download/**","/uploads/**" ,"/layuiadmin/**");
    }
}
