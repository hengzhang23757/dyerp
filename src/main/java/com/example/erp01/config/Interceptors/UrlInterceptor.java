package com.example.erp01.config.Interceptors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;

/**
 * Created by hujian on 2019/3/23
 */

@Component
public class UrlInterceptor  implements HandlerInterceptor {

    private static final Logger LOGGER = LoggerFactory.getLogger(UrlInterceptor.class);

    public UrlInterceptor(){super();}

    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object o)  {
        String localIP = null;
        try {
            localIP = InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
//        if(localIP==null || !"192.168.3.26".equals(localIP)) {
//            return false;
//        }
        String requestURI = request.getRequestURI();
//        LOGGER.warn("请求url："+requestURI);
        if(!"/".equals(requestURI) && !requestURI.startsWith("/erp")) {
            return false;
        }
        if(!"/".equals(requestURI) && (request.getSession().getAttribute("userName")==null || request.getSession().getAttribute("role")==null)) {
            String path=request.getContextPath();
            String serverName=request.getServerName();
            String basePath = null;
            if (Character.isDigit(serverName.charAt(0))){
                basePath=request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
            }else {
                basePath="https://"+request.getServerName()+path+"/";
            }
            StringBuffer url = request.getRequestURL();
            request.getSession().setAttribute("basePath", basePath);
            request.getSession().setAttribute("currentDate", System.currentTimeMillis());
            try {
                //如果是Ajax请求
                if("XMLHttpRequest".equals(request.getHeader("X-Requested-With"))){
                    sendRedirect(response,basePath);
                }else {
                    request.getRequestDispatcher("/").forward(request, response);
                }
            } catch (ServletException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return true;
        }
        String path=request.getContextPath();
        String serverName=request.getServerName();
        String basePath = null;
        if (Character.isDigit(serverName.charAt(0))){
            basePath=request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
        }else {
            basePath="https://"+request.getServerName()+path+"/";
        }
        request.getSession().setAttribute("basePath", basePath);
        request.getSession().setAttribute("currentDate", System.currentTimeMillis());
        return true;
    }
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) {

    }

    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) {

    }

    private void sendRedirect(HttpServletResponse response, String redirectUrl){
        try {
            //设置跳转地址
            response.setHeader("redirectUrl", redirectUrl);
            //设置跳转使能
            response.setHeader("enableRedirect","true");
            response.flushBuffer();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }
}