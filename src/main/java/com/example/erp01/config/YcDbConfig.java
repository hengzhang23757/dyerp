package com.example.erp01.config;

import com.alibaba.druid.filter.Filter;
import com.alibaba.druid.pool.DruidDataSource;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

@Configuration
@MapperScan(basePackages = YcDbConfig.PACKAGE, sqlSessionTemplateRef = "ycSqlSessionTemplate")
public class YcDbConfig {

    // 精确到 cluster 目录，以便跟其他数据源隔离
    static final String PACKAGE = "com.example.erp01.ycMapper";
    private static final String MAPPER_LOCATION = "classpath*:/ycMapper/*.xml";
    private static final String DOMAIN_PACKAGE = "com.example.erp01.model";


    // 将这个对象放入Spring容器中
    @Bean(name = "ycDataSource")
    // 读取application.properties中的配置参数映射成为一个对象
    // prefix表示参数的前缀
    @ConfigurationProperties(prefix = "spring.db2")
    public DataSource getDataSource2()
    {
//        return DataSourceBuilder.create().type(DruidDataSource.class).build();
        DruidDataSource druidDataSource = new DruidDataSource();
        List<Filter> filterList = new ArrayList<>();
        filterList.add(new DruidConfig().wallFilter());
        filterList.add(new DruidConfig().statFilter());
        druidDataSource.setProxyFilters(filterList);
        return druidDataSource;
    }

    @Bean(name = "ycTransactionManager")
    public DataSourceTransactionManager ycTransactionManager() {
        return new DataSourceTransactionManager(getDataSource2());
    }

    @Bean(name = "ycSqlSessionFactory")
    public SqlSessionFactory ycSqlSessionFactory(@Qualifier("ycDataSource") DataSource ycDataSource)
            throws Exception {
        final SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();
        sessionFactory.setDataSource(ycDataSource);
        sessionFactory.setMapperLocations(new PathMatchingResourcePatternResolver()
                .getResources(YcDbConfig.MAPPER_LOCATION));
        sessionFactory.setTypeAliasesPackage(YcDbConfig.DOMAIN_PACKAGE);
        //mybatis 数据库字段与实体类属性驼峰映射配置
        sessionFactory.getObject().getConfiguration().setMapUnderscoreToCamelCase(true);
        return sessionFactory.getObject();
    }
    @Bean
    public SqlSessionTemplate ycSqlSessionTemplate(@Qualifier("ycSqlSessionFactory") SqlSessionFactory ycSqlSessionFactory) throws Exception{
        SqlSessionTemplate sqlSessionTemplate=new SqlSessionTemplate(ycSqlSessionFactory);
        return sqlSessionTemplate;
    }

}
