package com.example.erp01.ycMapper;

import com.example.erp01.model.OrderProcedure;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
@Mapper
public interface SyncOrderProcedureMapper {

    List<OrderProcedure> getOrderProcedureByOrderName(@Param("orderName") String orderName);

    Integer addOrderProcedureBatch(@Param("orderProcedureList") List<OrderProcedure> orderProcedureList);

}
