package com.example.erp01.ycMapper;

import com.example.erp01.model.PrintPart;
import com.example.erp01.model.Tailor;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface SyncTailorMapper {

    List<Tailor> getTailorByInfo(@Param("orderName") String orderName, @Param("bedNumber") Integer bedNumber, @Param("packageNumber") Integer packageNumber);

    Integer addTailorBatch(@Param("tailorList") List<Tailor> tailorList);

    Integer addPrintPartBatch(@Param("printPartList") List<PrintPart> printPartList);
}
