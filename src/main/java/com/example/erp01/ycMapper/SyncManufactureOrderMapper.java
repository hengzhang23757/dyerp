package com.example.erp01.ycMapper;

import com.example.erp01.model.ManufactureOrder;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface SyncManufactureOrderMapper {

    ManufactureOrder getManufactureOrderByOrderName(@Param("orderName") String orderName);

    Integer addManufactureOrderOne(@Param("manufactureOrder") ManufactureOrder manufactureOrder);

}
