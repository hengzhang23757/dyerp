package com.example.erp01.ycMapper;

import com.example.erp01.model.OrderClothes;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface SyncOrderMapper {

    List<OrderClothes> getOrderClothesByOrderName(@Param("orderName") String orderName);

    Integer addOrderClothesBatch(@Param("orderClothesList") List<OrderClothes> orderClothesList);

}
