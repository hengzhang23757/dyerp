package com.example.erp01;

import com.alibaba.dubbo.spring.boot.annotation.EnableDubboConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.sql.SQLOutput;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@EnableDubboConfiguration
@EnableScheduling
public class DevApplication {

    public static void main(String[] args) {
        try {
            SpringApplication.run(DevApplication.class, args);
        }catch (Exception e){
            System.out.println(e);
        }
    }

}
