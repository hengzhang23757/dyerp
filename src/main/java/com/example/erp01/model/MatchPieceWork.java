package com.example.erp01.model;

public class MatchPieceWork {

    private Integer pieceWorkID;

    private String orderName;

    private String clothesVersionNumber;

    private Integer bedNumber;

    private String colorName;

    private String sizeName;

    private Integer packageNumber;

    private Integer layerCount;

    public Integer getPieceWorkID() {
        return pieceWorkID;
    }

    public void setPieceWorkID(Integer pieceWorkID) {
        this.pieceWorkID = pieceWorkID;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public Integer getBedNumber() {
        return bedNumber;
    }

    public void setBedNumber(Integer bedNumber) {
        this.bedNumber = bedNumber;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public Integer getPackageNumber() {
        return packageNumber;
    }

    public void setPackageNumber(Integer packageNumber) {
        this.packageNumber = packageNumber;
    }

    public Integer getLayerCount() {
        return layerCount;
    }

    public void setLayerCount(Integer layerCount) {
        this.layerCount = layerCount;
    }

    public MatchPieceWork() {
    }

    public MatchPieceWork(String orderName, String clothesVersionNumber, Integer bedNumber, String colorName, String sizeName, Integer packageNumber, Integer layerCount) {
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.bedNumber = bedNumber;
        this.colorName = colorName;
        this.sizeName = sizeName;
        this.packageNumber = packageNumber;
        this.layerCount = layerCount;
    }
}
