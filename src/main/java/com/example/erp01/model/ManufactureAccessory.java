package com.example.erp01.model;

import java.util.Date;

public class ManufactureAccessory {

    private Integer accessoryID;

    private String orderName;

    private String clothesVersionNumber;

    private String season;

    private String customerName;

    private String accessoryNumber = "";

    private String accessoryName;  //辅料名称

    private String specification = "未输";//辅料规格

    private String accessoryUnit;//辅料单位

    private String accessoryColor = "未输";//辅料颜色

    private Float orderPieceUsage = 0f;

    private Float pieceUsage = 0f;//单件用量

    private String sizeName;//尺码

    private String colorName;//颜色

    private String supplier;

    private Float orderPrice = 0f;

    private Float price = 0f;

    private Integer orderCount = 0;

    private Float accessoryCount = 0f;

    private Float accessoryOrderCount = 0f;

    private Float accessoryAdd = 0f;

    private Float sumMoney = 0f;

    private Float orderSumMoney = 0f;

    private Float accessoryReturnCount = 0f;

    private String orderState = "未下单";

    private String checkState;

    private Integer wellCount;

    private String checkNumber;

    private Float accessoryPlanCount = 0f;

    private Date createTime;

    private Float accessoryStorageCount = 0f;

    private Date lastDate;

    private Float lastCount;

    private Float returnCount = 0f;

    private Float returnSumMoney = 0f;

    private String remark;

    private Float shareStorage = 0f;

    private Integer colorChild;

    private Integer sizeChild;

    private String colorProperty;

    private String sizeProperty;

    private Integer batchOrder;

    private String accessoryType;

    private Integer accessoryKey;

    private Float accessoryAccountCount = 0f;

    private Float accessoryReturnCheckCount = 0f;

    private Float accessoryReturnUnCheck = 0f;

    private String tax = "";

    private Float addFee = 0f;

    private String addRemark = "";

    private Integer feeState;

    private String preCheck = "未提交";
    private String preCheckEmp;
    private Date preCheckTime;

    private String checkEmp;
    private Date checkTime;

    public String getPreCheckEmp() {
        return preCheckEmp;
    }

    public void setPreCheckEmp(String preCheckEmp) {
        this.preCheckEmp = preCheckEmp;
    }

    public Date getPreCheckTime() {
        return preCheckTime;
    }

    public void setPreCheckTime(Date preCheckTime) {
        this.preCheckTime = preCheckTime;
    }

    public String getCheckEmp() {
        return checkEmp;
    }

    public void setCheckEmp(String checkEmp) {
        this.checkEmp = checkEmp;
    }

    public Date getCheckTime() {
        return checkTime;
    }

    public void setCheckTime(Date checkTime) {
        this.checkTime = checkTime;
    }

    public Integer getWellCount() {
        return wellCount;
    }

    public void setWellCount(Integer wellCount) {
        this.wellCount = wellCount;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Integer getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(Integer orderCount) {
        this.orderCount = orderCount;
    }

    public Float getAccessoryAdd() {
        return accessoryAdd;
    }

    public void setAccessoryAdd(Float accessoryAdd) {
        this.accessoryAdd = accessoryAdd;
    }

    public Float getSumMoney() {
        return sumMoney;
    }

    public void setSumMoney(Float sumMoney) {
        this.sumMoney = sumMoney;
    }

    public Float getAccessoryCount() {
        return accessoryCount;
    }

    public void setAccessoryCount(Float accessoryCount) {
        this.accessoryCount = accessoryCount;
    }

    public Float getAccessoryStorageCount() {
        return accessoryStorageCount;
    }

    public void setAccessoryStorageCount(Float accessoryStorageCount) {
        this.accessoryStorageCount = accessoryStorageCount;
    }

    public Float getOrderPieceUsage() {
        return orderPieceUsage;
    }

    public void setOrderPieceUsage(Float orderPieceUsage) {
        this.orderPieceUsage = orderPieceUsage;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Float getAccessoryPlanCount() {
        return accessoryPlanCount;
    }

    public void setAccessoryPlanCount(Float accessoryPlanCount) {
        this.accessoryPlanCount = accessoryPlanCount;
    }

    public Date getLastDate() {
        return lastDate;
    }

    public void setLastDate(Date lastDate) {
        this.lastDate = lastDate;
    }

    public Float getLastCount() {
        return lastCount;
    }

    public void setLastCount(Float lastCount) {
        this.lastCount = lastCount;
    }

    public Float getReturnCount() {
        return returnCount;
    }

    public void setReturnCount(Float returnCount) {
        this.returnCount = returnCount;
    }

    public Float getOrderSumMoney() {
        return orderSumMoney;
    }

    public void setOrderSumMoney(Float orderSumMoney) {
        this.orderSumMoney = orderSumMoney;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getColorChild() {
        return colorChild;
    }

    public void setColorChild(Integer colorChild) {
        this.colorChild = colorChild;
    }

    public Integer getSizeChild() {
        return sizeChild;
    }

    public void setSizeChild(Integer sizeChild) {
        this.sizeChild = sizeChild;
    }

    public String getColorProperty() {
        return colorProperty;
    }

    public void setColorProperty(String colorProperty) {
        this.colorProperty = colorProperty;
    }

    public String getSizeProperty() {
        return sizeProperty;
    }

    public void setSizeProperty(String sizeProperty) {
        this.sizeProperty = sizeProperty;
    }


    public Integer getBatchOrder() {
        return batchOrder;
    }

    public void setBatchOrder(Integer batchOrder) {
        this.batchOrder = batchOrder;
    }


    public String getAccessoryType() {
        return accessoryType;
    }

    public void setAccessoryType(String accessoryType) {
        this.accessoryType = accessoryType;
    }


    public Integer getAccessoryKey() {
        return accessoryKey;
    }

    public void setAccessoryKey(Integer accessoryKey) {
        this.accessoryKey = accessoryKey;
    }

    public Float getAccessoryReturnCheckCount() {
        return accessoryReturnCheckCount;
    }

    public void setAccessoryReturnCheckCount(Float accessoryReturnCheckCount) {
        this.accessoryReturnCheckCount = accessoryReturnCheckCount;
    }

    public Float getAccessoryReturnUnCheck() {
        return accessoryReturnUnCheck;
    }

    public void setAccessoryReturnUnCheck(Float accessoryReturnUnCheck) {
        this.accessoryReturnUnCheck = accessoryReturnUnCheck;
    }

    public Float getAccessoryAccountCount() {
        return accessoryAccountCount;
    }

    public void setAccessoryAccountCount(Float accessoryAccountCount) {
        this.accessoryAccountCount = accessoryAccountCount;
    }

    public Float getShareStorage() {
        return shareStorage;
    }

    public void setShareStorage(Float shareStorage) {
        this.shareStorage = shareStorage;
    }

    public Float getAccessoryOrderCount() {
        return accessoryOrderCount;
    }

    public void setAccessoryOrderCount(Float accessoryOrderCount) {
        this.accessoryOrderCount = accessoryOrderCount;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public Float getAddFee() {
        return addFee;
    }

    public void setAddFee(Float addFee) {
        this.addFee = addFee;
    }

    public String getAddRemark() {
        return addRemark;
    }

    public void setAddRemark(String addRemark) {
        this.addRemark = addRemark;
    }

    public Integer getFeeState() {
        return feeState;
    }

    public void setFeeState(Integer feeState) {
        this.feeState = feeState;
    }

    public String getPreCheck() {
        return preCheck;
    }

    public void setPreCheck(String preCheck) {
        this.preCheck = preCheck;
    }

    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public ManufactureAccessory() {
    }

    public ManufactureAccessory(String orderName, String clothesVersionNumber, String accessoryName, String specification, String accessoryUnit, String accessoryColor, Float pieceUsage, String sizeName, String colorName) {
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.accessoryName = accessoryName;
        this.specification = specification;
        this.accessoryUnit = accessoryUnit;
        this.accessoryColor = accessoryColor;
        this.pieceUsage = pieceUsage;
        this.sizeName = sizeName;
        this.colorName = colorName;
    }

    public ManufactureAccessory(Integer accessoryID, String orderName, String clothesVersionNumber, String accessoryName, String specification, String accessoryUnit, String accessoryColor, Float pieceUsage, String sizeName, String colorName) {
        this.accessoryID = accessoryID;
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.accessoryName = accessoryName;
        this.specification = specification;
        this.accessoryUnit = accessoryUnit;
        this.accessoryColor = accessoryColor;
        this.pieceUsage = pieceUsage;
        this.sizeName = sizeName;
        this.colorName = colorName;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public Integer getAccessoryID() {
        return accessoryID;
    }

    public void setAccessoryID(Integer accessoryID) {
        this.accessoryID = accessoryID;
    }

    public String getAccessoryName() {
        return accessoryName;
    }

    public void setAccessoryName(String accessoryName) {
        this.accessoryName = accessoryName;
    }

    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }

    public String getAccessoryUnit() {
        return accessoryUnit;
    }

    public void setAccessoryUnit(String accessoryUnit) {
        this.accessoryUnit = accessoryUnit;
    }

    public String getAccessoryColor() {
        return accessoryColor;
    }

    public void setAccessoryColor(String accessoryColor) {
        this.accessoryColor = accessoryColor;
    }

    public Float getPieceUsage() {
        return pieceUsage;
    }

    public void setPieceUsage(Float pieceUsage) {
        this.pieceUsage = pieceUsage;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public Float getAccessoryReturnCount() {
        return accessoryReturnCount;
    }

    public void setAccessoryReturnCount(Float accessoryReturnCount) {
        this.accessoryReturnCount = accessoryReturnCount;
    }

    public String getOrderState() {
        return orderState;
    }

    public void setOrderState(String orderState) {
        this.orderState = orderState;
    }

    public String getAccessoryNumber() {
        return accessoryNumber;
    }

    public void setAccessoryNumber(String accessoryNumber) {
        this.accessoryNumber = accessoryNumber;
    }

    public String getCheckState() {
        return checkState;
    }

    public void setCheckState(String checkState) {
        this.checkState = checkState;
    }

    public String getCheckNumber() {
        return checkNumber;
    }

    public void setCheckNumber(String checkNumber) {
        this.checkNumber = checkNumber;
    }

    public Float getReturnSumMoney() {
        return returnSumMoney;
    }

    public void setReturnSumMoney(Float returnSumMoney) {
        this.returnSumMoney = returnSumMoney;
    }

    public Float getOrderPrice() {
        return orderPrice;
    }

    public void setOrderPrice(Float orderPrice) {
        this.orderPrice = orderPrice;
    }
}
