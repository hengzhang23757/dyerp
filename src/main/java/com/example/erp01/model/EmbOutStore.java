package com.example.erp01.model;

public class EmbOutStore {

    private Integer embOutStoreID;

    private String groupName;

    private String orderName;

    private Integer bedNumber;

    private String colorName;

    private String sizeName;

    private Integer layerCount;

    private Integer packageNumber;

    private String tailorQcode;

    private Integer embOutStoreCount;

    public Integer getEmbOutStoreCount() {
        return embOutStoreCount;
    }

    public void setEmbOutStoreCount(Integer embOutStoreCount) {
        this.embOutStoreCount = embOutStoreCount;
    }

    public EmbOutStore(Integer embOutStoreID, String groupName, String orderName, Integer bedNumber, String colorName, String sizeName, Integer layerCount, Integer packageNumber, String tailorQcode) {
        this.embOutStoreID = embOutStoreID;
        this.groupName = groupName;
        this.orderName = orderName;
        this.bedNumber = bedNumber;
        this.colorName = colorName;
        this.sizeName = sizeName;
        this.layerCount = layerCount;
        this.packageNumber = packageNumber;
        this.tailorQcode = tailorQcode;
    }

    public EmbOutStore(String groupName, String orderName, Integer bedNumber, String colorName, String sizeName, Integer layerCount, Integer packageNumber, String tailorQcode) {
        this.groupName = groupName;
        this.orderName = orderName;
        this.bedNumber = bedNumber;
        this.colorName = colorName;
        this.sizeName = sizeName;
        this.layerCount = layerCount;
        this.packageNumber = packageNumber;
        this.tailorQcode = tailorQcode;
    }

    public EmbOutStore() {
    }

    public Integer getEmbOutStoreID() {
        return embOutStoreID;
    }

    public void setEmbOutStoreID(Integer embOutStoreID) {
        this.embOutStoreID = embOutStoreID;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public Integer getBedNumber() {
        return bedNumber;
    }

    public void setBedNumber(Integer bedNumber) {
        this.bedNumber = bedNumber;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public Integer getLayerCount() {
        return layerCount;
    }

    public void setLayerCount(Integer layerCount) {
        this.layerCount = layerCount;
    }

    public Integer getPackageNumber() {
        return packageNumber;
    }

    public void setPackageNumber(Integer packageNumber) {
        this.packageNumber = packageNumber;
    }

    public String getTailorQcode() {
        return tailorQcode;
    }

    public void setTailorQcode(String tailorQcode) {
        this.tailorQcode = tailorQcode;
    }
}
