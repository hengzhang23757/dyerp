package com.example.erp01.model;

public class ClothesDevMeasure {

    private Integer id;

    private Integer devId;

    private String clothesDevNumber;

    private String clothesVersionNumber;

    private String orderName;

    private String measureImg;

    public ClothesDevMeasure() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDevId() {
        return devId;
    }

    public void setDevId(Integer devId) {
        this.devId = devId;
    }

    public String getMeasureImg() {
        return measureImg;
    }

    public void setMeasureImg(String measureImg) {
        this.measureImg = measureImg;
    }

    public String getClothesDevNumber() {
        return clothesDevNumber;
    }

    public void setClothesDevNumber(String clothesDevNumber) {
        this.clothesDevNumber = clothesDevNumber;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }
}
