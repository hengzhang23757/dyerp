package com.example.erp01.model;

public class YesterdayWorkShop {

    private String groupName;

    private String clothesVersionNumber;

    private String orderName;

    private Integer orderCount = 0;

    private Integer finishCount = 0;

    private Integer yesterdayCount = 0;

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public Integer getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(Integer orderCount) {
        this.orderCount = orderCount;
    }

    public Integer getFinishCount() {
        return finishCount;
    }

    public void setFinishCount(Integer finishCount) {
        this.finishCount = finishCount;
    }

    public Integer getYesterdayCount() {
        return yesterdayCount;
    }

    public void setYesterdayCount(Integer yesterdayCount) {
        this.yesterdayCount = yesterdayCount;
    }

    public YesterdayWorkShop() {
    }

    public YesterdayWorkShop(String groupName, String clothesVersionNumber, String orderName, Integer orderCount, Integer finishCount, Integer yesterdayCount) {
        this.groupName = groupName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.orderName = orderName;
        this.orderCount = orderCount;
        this.finishCount = finishCount;
        this.yesterdayCount = yesterdayCount;
    }
}
