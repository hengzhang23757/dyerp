package com.example.erp01.model;

import java.util.Date;

public class GroupPlan {

    private Integer groupPlanID;

    private String groupName;

    private String orderName;

    private String clothesVersionNumber;

    private Date endDate;

    private Integer workHour;

    private Integer planCount = 0;

    public Integer getGroupPlanID() {
        return groupPlanID;
    }

    public void setGroupPlanID(Integer groupPlanID) {
        this.groupPlanID = groupPlanID;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Integer getWorkHour() {
        return workHour;
    }

    public void setWorkHour(Integer workHour) {
        this.workHour = workHour;
    }

    public Integer getPlanCount() {
        return planCount;
    }

    public void setPlanCount(Integer planCount) {
        this.planCount = planCount;
    }

    public GroupPlan() {
    }

    public GroupPlan(String groupName, String orderName, String clothesVersionNumber, Date endDate, Integer workHour, Integer planCount) {
        this.groupName = groupName;
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.endDate = endDate;
        this.workHour = workHour;
        this.planCount = planCount;
    }
}
