package com.example.erp01.model;

import java.util.Date;

public class ReWork {

    private String orderName;

    private String clothesVersionNumber;

    private String description;

    private String customer;

    private Date reWorkDate;

    private String groupName;

    private Integer procedureNumber;

    private String procedureName;

    private Integer reWorkCount;

    private Float reWorkRate;

    private Float wellRate;

    public ReWork() {
    }

    public ReWork(String orderName, String clothesVersionNumber, String description, String customer, Date reWorkDate, String groupName, Integer procedureNumber, String procedureName, Integer reWorkCount, Float reWorkRate, Float wellRate) {
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.description = description;
        this.customer = customer;
        this.reWorkDate = reWorkDate;
        this.groupName = groupName;
        this.procedureNumber = procedureNumber;
        this.procedureName = procedureName;
        this.reWorkCount = reWorkCount;
        this.reWorkRate = reWorkRate;
        this.wellRate = wellRate;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public Date getReWorkDate() {
        return reWorkDate;
    }

    public void setReWorkDate(Date reWorkDate) {
        this.reWorkDate = reWorkDate;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public Integer getProcedureNumber() {
        return procedureNumber;
    }

    public void setProcedureNumber(Integer procedureNumber) {
        this.procedureNumber = procedureNumber;
    }

    public String getProcedureName() {
        return procedureName;
    }

    public void setProcedureName(String procedureName) {
        this.procedureName = procedureName;
    }

    public Integer getReWorkCount() {
        return reWorkCount;
    }

    public void setReWorkCount(Integer reWorkCount) {
        this.reWorkCount = reWorkCount;
    }

    public Float getReWorkRate() {
        return reWorkRate;
    }

    public void setReWorkRate(Float reWorkRate) {
        this.reWorkRate = reWorkRate;
    }

    public Float getWellRate() {
        return wellRate;
    }

    public void setWellRate(Float wellRate) {
        this.wellRate = wellRate;
    }
}
