package com.example.erp01.model;

public class TailorMatch {

    private String orderName;

    private String clothesVersionNumber;

    private String colorName;

    private String beginCutDate;

    private String lastCutDate;

    private String partName;

    private Integer orderCount = 0;

    private Integer mainCount = 0;

    private Integer otherCount = 0;

    private Integer matchCount = 0;

    private Integer diffCount = 0;

    private Float matchPercent = 0f;

    public TailorMatch() {
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getBeginCutDate() {
        return beginCutDate;
    }

    public void setBeginCutDate(String beginCutDate) {
        this.beginCutDate = beginCutDate;
    }

    public String getLastCutDate() {
        return lastCutDate;
    }

    public void setLastCutDate(String lastCutDate) {
        this.lastCutDate = lastCutDate;
    }

    public Integer getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(Integer orderCount) {
        this.orderCount = orderCount;
    }

    public Integer getMatchCount() {
        return matchCount;
    }

    public void setMatchCount(Integer matchCount) {
        this.matchCount = matchCount;
    }

    public Integer getDiffCount() {
        return diffCount;
    }

    public void setDiffCount(Integer diffCount) {
        this.diffCount = diffCount;
    }

    public Integer getMainCount() {
        return mainCount;
    }

    public void setMainCount(Integer mainCount) {
        this.mainCount = mainCount;
    }

    public Integer getOtherCount() {
        return otherCount;
    }

    public void setOtherCount(Integer otherCount) {
        this.otherCount = otherCount;
    }

    public Float getMatchPercent() {
        return matchPercent;
    }

    public void setMatchPercent(Float matchPercent) {
        this.matchPercent = matchPercent;
    }

    public String getPartName() {
        return partName;
    }

    public void setPartName(String partName) {
        this.partName = partName;
    }
}
