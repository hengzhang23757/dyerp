package com.example.erp01.model;

public class TailorBack {

    private Integer tailorID;

    private String orderName;

    private String clothesVersionNumber;

    private String customerName;

    private Integer bedNumber;

    private String jarName;

    private String colorName;

    private String sizeName;

    private String partName;

    private Integer layerCount;

    private Integer initCount;

    private Integer packageNumber;

    private Integer tailorReportCount;

    private String tailorQcode;

    private Integer tailorQcodeID;

    private String groupName;

    private Float weight;

    private Integer batch;

    public Integer getTailorID() {
        return tailorID;
    }

    public void setTailorID(Integer tailorID) {
        this.tailorID = tailorID;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public Float getWeight() {
        return weight;
    }

    public void setWeight(Float weight) {
        this.weight = weight;
    }

    public Integer getBatch() {
        return batch;
    }

    public void setBatch(Integer batch) {
        this.batch = batch;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public Integer getBedNumber() {
        return bedNumber;
    }

    public void setBedNumber(Integer bedNumber) {
        this.bedNumber = bedNumber;
    }

    public String getJarName() {
        return jarName;
    }

    public void setJarName(String jarName) {
        this.jarName = jarName;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public String getPartName() {
        return partName;
    }

    public void setPartName(String partName) {
        this.partName = partName;
    }

    public Integer getLayerCount() {
        return layerCount;
    }

    public void setLayerCount(Integer layerCount) {
        this.layerCount = layerCount;
    }

    public Integer getInitCount() {
        return initCount;
    }

    public void setInitCount(Integer initCount) {
        this.initCount = initCount;
    }

    public Integer getPackageNumber() {
        return packageNumber;
    }

    public void setPackageNumber(Integer packageNumber) {
        this.packageNumber = packageNumber;
    }

    public Integer getTailorReportCount() {
        return tailorReportCount;
    }

    public void setTailorReportCount(Integer tailorReportCount) {
        this.tailorReportCount = tailorReportCount;
    }

    public String getTailorQcode() {
        return tailorQcode;
    }

    public void setTailorQcode(String tailorQcode) {
        this.tailorQcode = tailorQcode;
    }

    public Integer getTailorQcodeID() {
        return tailorQcodeID;
    }

    public void setTailorQcodeID(Integer tailorQcodeID) {
        this.tailorQcodeID = tailorQcodeID;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public TailorBack() {
    }

    public TailorBack(String orderName, String clothesVersionNumber, String customerName, Integer bedNumber, String jarName, String colorName, String sizeName, String partName, Integer layerCount, Integer initCount, Integer packageNumber, Integer tailorReportCount, String tailorQcode, Integer tailorQcodeID, String groupName, Float weight, Integer batch) {
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.customerName = customerName;
        this.bedNumber = bedNumber;
        this.jarName = jarName;
        this.colorName = colorName;
        this.sizeName = sizeName;
        this.partName = partName;
        this.layerCount = layerCount;
        this.initCount = initCount;
        this.packageNumber = packageNumber;
        this.tailorReportCount = tailorReportCount;
        this.tailorQcode = tailorQcode;
        this.tailorQcodeID = tailorQcodeID;
        this.groupName = groupName;
        this.weight = weight;
        this.batch = batch;
    }

    public TailorBack(Integer tailorID, String orderName, String clothesVersionNumber, String customerName, Integer bedNumber, String jarName, String colorName, String sizeName, String partName, Integer layerCount, Integer initCount, Integer packageNumber, Integer tailorReportCount, String tailorQcode, Integer tailorQcodeID, String groupName, Float weight, Integer batch) {
        this.tailorID = tailorID;
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.customerName = customerName;
        this.bedNumber = bedNumber;
        this.jarName = jarName;
        this.colorName = colorName;
        this.sizeName = sizeName;
        this.partName = partName;
        this.layerCount = layerCount;
        this.initCount = initCount;
        this.packageNumber = packageNumber;
        this.tailorReportCount = tailorReportCount;
        this.tailorQcode = tailorQcode;
        this.tailorQcodeID = tailorQcodeID;
        this.groupName = groupName;
        this.weight = weight;
        this.batch = batch;
    }
}
