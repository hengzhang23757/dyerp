package com.example.erp01.model;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class InnerBoard {

    private Integer innerBoardID;

    private String orderInfo;

    private String season;

    private String versionNumber;

    private String fabricName;

    private String fabricNumber;

    private String fabricColor;

    private String fabricColorNumber;

    private String clothingFactory;

    private String cooperateMode;

    private String fabricFactory;

    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date deliveryDate;

    private Integer innerBoardCount;

    private String unit;

    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date returnFabricDate;

    private String jarNumber;

    private Float returnFabricCount;

    private Float innerBoardPublishCount;

    private Float innerBoardRemaindCount;

    private Float orderBoardPublishCount;

    private Float orderBoardRemaindCount;

    public String getClothingFactory() {
        return clothingFactory;
    }

    public void setClothingFactory(String clothingFactory) {
        this.clothingFactory = clothingFactory;
    }

    public String getCooperateMode() {
        return cooperateMode;
    }

    public void setCooperateMode(String cooperateMode) {
        this.cooperateMode = cooperateMode;
    }

    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }

    public Integer getInnerBoardID() {
        return innerBoardID;
    }

    public void setInnerBoardID(Integer innerBoardID) {
        this.innerBoardID = innerBoardID;
    }

    public String getOrderInfo() {
        return orderInfo;
    }

    public void setOrderInfo(String orderInfo) {
        this.orderInfo = orderInfo;
    }

    public String getVersionNumber() {
        return versionNumber;
    }

    public void setVersionNumber(String versionNumber) {
        this.versionNumber = versionNumber;
    }

    public String getFabricName() {
        return fabricName;
    }

    public void setFabricName(String fabricName) {
        this.fabricName = fabricName;
    }

    public String getFabricNumber() {
        return fabricNumber;
    }

    public void setFabricNumber(String fabricNumber) {
        this.fabricNumber = fabricNumber;
    }

    public String getFabricColor() {
        return fabricColor;
    }

    public void setFabricColor(String fabricColor) {
        this.fabricColor = fabricColor;
    }

    public String getFabricColorNumber() {
        return fabricColorNumber;
    }

    public void setFabricColorNumber(String fabricColorNumber) {
        this.fabricColorNumber = fabricColorNumber;
    }

    public String getFabricFactory() {
        return fabricFactory;
    }

    public void setFabricFactory(String fabricFactory) {
        this.fabricFactory = fabricFactory;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public Integer getInnerBoardCount() {
        return innerBoardCount;
    }

    public void setInnerBoardCount(Integer innerBoardCount) {
        this.innerBoardCount = innerBoardCount;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Date getReturnFabricDate() {
        return returnFabricDate;
    }

    public void setReturnFabricDate(Date returnFabricDate) {
        this.returnFabricDate = returnFabricDate;
    }

    public String getJarNumber() {
        return jarNumber;
    }

    public void setJarNumber(String jarNumber) {
        this.jarNumber = jarNumber;
    }

    public Float getReturnFabricCount() {
        return returnFabricCount;
    }

    public void setReturnFabricCount(Float returnFabricCount) {
        this.returnFabricCount = returnFabricCount;
    }

    public Float getInnerBoardPublishCount() {
        return innerBoardPublishCount;
    }

    public void setInnerBoardPublishCount(Float innerBoardPublishCount) {
        this.innerBoardPublishCount = innerBoardPublishCount;
    }

    public Float getInnerBoardRemaindCount() {
        return innerBoardRemaindCount;
    }

    public void setInnerBoardRemaindCount(Float innerBoardRemaindCount) {
        this.innerBoardRemaindCount = innerBoardRemaindCount;
    }

    public Float getOrderBoardPublishCount() {
        return orderBoardPublishCount;
    }

    public void setOrderBoardPublishCount(Float orderBoardPublishCount) {
        this.orderBoardPublishCount = orderBoardPublishCount;
    }

    public Float getOrderBoardRemaindCount() {
        return orderBoardRemaindCount;
    }

    public void setOrderBoardRemaindCount(Float orderBoardRemaindCount) {
        this.orderBoardRemaindCount = orderBoardRemaindCount;
    }

    public InnerBoard() {
    }

    public InnerBoard(String orderInfo, String season, String versionNumber, String fabricName, String fabricNumber, String fabricColor, String fabricColorNumber, String clothingFactory, String cooperateMode, String fabricFactory, Date deliveryDate, Integer innerBoardCount, String unit, Date returnFabricDate, String jarNumber, Float returnFabricCount, Float innerBoardPublishCount, Float innerBoardRemaindCount, Float orderBoardPublishCount, Float orderBoardRemaindCount) {
        this.orderInfo = orderInfo;
        this.season = season;
        this.versionNumber = versionNumber;
        this.fabricName = fabricName;
        this.fabricNumber = fabricNumber;
        this.fabricColor = fabricColor;
        this.fabricColorNumber = fabricColorNumber;
        this.clothingFactory = clothingFactory;
        this.cooperateMode = cooperateMode;
        this.fabricFactory = fabricFactory;
        this.deliveryDate = deliveryDate;
        this.innerBoardCount = innerBoardCount;
        this.unit = unit;
        this.returnFabricDate = returnFabricDate;
        this.jarNumber = jarNumber;
        this.returnFabricCount = returnFabricCount;
        this.innerBoardPublishCount = innerBoardPublishCount;
        this.innerBoardRemaindCount = innerBoardRemaindCount;
        this.orderBoardPublishCount = orderBoardPublishCount;
        this.orderBoardRemaindCount = orderBoardRemaindCount;
    }

    public InnerBoard(Integer innerBoardID, String orderInfo, String season, String versionNumber, String fabricName, String fabricNumber, String fabricColor, String fabricColorNumber, String clothingFactory, String cooperateMode, String fabricFactory, Date deliveryDate, Integer innerBoardCount, String unit, Date returnFabricDate, String jarNumber, Float returnFabricCount, Float innerBoardPublishCount, Float innerBoardRemaindCount, Float orderBoardPublishCount, Float orderBoardRemaindCount) {
        this.innerBoardID = innerBoardID;
        this.orderInfo = orderInfo;
        this.season = season;
        this.versionNumber = versionNumber;
        this.fabricName = fabricName;
        this.fabricNumber = fabricNumber;
        this.fabricColor = fabricColor;
        this.fabricColorNumber = fabricColorNumber;
        this.clothingFactory = clothingFactory;
        this.cooperateMode = cooperateMode;
        this.fabricFactory = fabricFactory;
        this.deliveryDate = deliveryDate;
        this.innerBoardCount = innerBoardCount;
        this.unit = unit;
        this.returnFabricDate = returnFabricDate;
        this.jarNumber = jarNumber;
        this.returnFabricCount = returnFabricCount;
        this.innerBoardPublishCount = innerBoardPublishCount;
        this.innerBoardRemaindCount = innerBoardRemaindCount;
        this.orderBoardPublishCount = orderBoardPublishCount;
        this.orderBoardRemaindCount = orderBoardRemaindCount;
    }
}
