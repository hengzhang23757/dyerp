package com.example.erp01.model;

import java.util.Date;

public class OtherTailorMonthReport implements Comparable<OtherTailorMonthReport>{

    private String orderName;

    private String clothesVersionNumber;

    private Integer bedNumberCount;

    private Integer pieceCount;

    private Integer orderCount;

    private String partName;

    private Integer cutCount;

    private Integer differenceCount;

    private Date cutDate;

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public Integer getBedNumberCount() {
        return bedNumberCount;
    }

    public void setBedNumberCount(Integer bedNumberCount) {
        this.bedNumberCount = bedNumberCount;
    }

    public Integer getPieceCount() {
        return pieceCount;
    }

    public void setPieceCount(Integer pieceCount) {
        this.pieceCount = pieceCount;
    }

    public Integer getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(Integer orderCount) {
        this.orderCount = orderCount;
    }

    public String getPartName() {
        return partName;
    }

    public void setPartName(String partName) {
        this.partName = partName;
    }

    public Integer getCutCount() {
        return cutCount;
    }

    public void setCutCount(Integer cutCount) {
        this.cutCount = cutCount;
    }

    public Integer getDifferenceCount() {
        return differenceCount;
    }

    public void setDifferenceCount(Integer differenceCount) {
        this.differenceCount = differenceCount;
    }

    public Date getCutDate() {
        return cutDate;
    }

    public void setCutDate(Date cutDate) {
        this.cutDate = cutDate;
    }

    public OtherTailorMonthReport() {
    }

    public OtherTailorMonthReport(String orderName, String clothesVersionNumber, Integer bedNumberCount, Integer pieceCount, Integer orderCount, String partName, Integer cutCount, Integer differenceCount) {
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.bedNumberCount = bedNumberCount;
        this.pieceCount = pieceCount;
        this.orderCount = orderCount;
        this.partName = partName;
        this.cutCount = cutCount;
        this.differenceCount = differenceCount;
    }

    public OtherTailorMonthReport(String orderName, Integer bedNumberCount, Integer pieceCount, String partName) {
        this.orderName = orderName;
        this.bedNumberCount = bedNumberCount;
        this.pieceCount = pieceCount;
        this.partName = partName;
    }

    @Override
    public int compareTo(OtherTailorMonthReport o) {
        return this.orderName.compareTo(o.orderName);
    }
}
