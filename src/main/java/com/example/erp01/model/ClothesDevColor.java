package com.example.erp01.model;

public class ClothesDevColor {

    private Integer id;

    private Integer devId;

    private String clothesDevNumber;

    private String clothesVersionNumber;

    private String orderName;

    private String colorName;

    private String colorNumber;

    public ClothesDevColor() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDevId() {
        return devId;
    }

    public void setDevId(Integer devId) {
        this.devId = devId;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getColorNumber() {
        return colorNumber;
    }

    public void setColorNumber(String colorNumber) {
        this.colorNumber = colorNumber;
    }

    public String getClothesDevNumber() {
        return clothesDevNumber;
    }

    public void setClothesDevNumber(String clothesDevNumber) {
        this.clothesDevNumber = clothesDevNumber;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }
}
