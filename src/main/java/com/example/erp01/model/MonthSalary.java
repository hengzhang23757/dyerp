package com.example.erp01.model;

public class MonthSalary {

    private String employeeNumber;

    private String employeeName;

    private String groupName;

    private String orderName;

    private String clothesVersionNumber;

    private String salaryType;

    private Integer procedureNumber;

    private String procedureName;

    private Float pieceCount;

    private Float price = 0f;

    private Float salary = 0f;

    private Float priceTwo = 0f;

    private Float salaryTwo = 0f;

    private Float salarySum = 0f;

    private Float subsidy = 0f;

    private String monthString;

    public String getEmployeeNumber() {
        return employeeNumber;
    }

    public void setEmployeeNumber(String employeeNumber) {
        this.employeeNumber = employeeNumber;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public String getSalaryType() {
        return salaryType;
    }

    public void setSalaryType(String salaryType) {
        this.salaryType = salaryType;
    }

    public Integer getProcedureNumber() {
        return procedureNumber;
    }

    public void setProcedureNumber(Integer procedureNumber) {
        this.procedureNumber = procedureNumber;
    }

    public String getProcedureName() {
        return procedureName;
    }

    public void setProcedureName(String procedureName) {
        this.procedureName = procedureName;
    }

    public Float getPieceCount() {
        return pieceCount;
    }

    public void setPieceCount(Float pieceCount) {
        this.pieceCount = pieceCount;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Float getSalary() {
        return salary;
    }

    public void setSalary(Float salary) {
        this.salary = salary;
    }

    public Float getPriceTwo() {
        return priceTwo;
    }

    public void setPriceTwo(Float priceTwo) {
        this.priceTwo = priceTwo;
    }

    public Float getSalaryTwo() {
        return salaryTwo;
    }

    public void setSalaryTwo(Float salaryTwo) {
        this.salaryTwo = salaryTwo;
    }

    public String getMonthString() {
        return monthString;
    }

    public void setMonthString(String monthString) {
        this.monthString = monthString;
    }

    public Float getSalarySum() {
        return salarySum;
    }

    public void setSalarySum(Float salarySum) {
        this.salarySum = salarySum;
    }

    public Float getSubsidy() {
        return subsidy;
    }

    public void setSubsidy(Float subsidy) {
        this.subsidy = subsidy;
    }

    public MonthSalary() {
    }
}
