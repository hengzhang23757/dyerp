package com.example.erp01.model;

public class Completion {

    private String orderName;

    private String clothesVersionNumber;

    private String season;

    private Integer orderCount = 0;

    private Integer tailorCount = 0;

    private Integer wellTailorCount = 0;

    private Double wellPercent;

    private Integer embCount = 0;

    private Integer upCount = 0;

    private Integer inspectCount = 0;

    private Integer pressCount = 0;

    private Integer packageCount = 0;

    private Double completePercent;

    public Completion() {
    }

    public Completion(String orderName, String clothesVersionNumber, String season, Integer orderCount, Integer tailorCount, Integer wellTailorCount, Double wellPercent, Integer embCount, Integer upCount, Integer inspectCount, Integer pressCount, Integer packageCount, Double completePercent) {
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.season = season;
        this.orderCount = orderCount;
        this.tailorCount = tailorCount;
        this.wellTailorCount = wellTailorCount;
        this.wellPercent = wellPercent;
        this.embCount = embCount;
        this.upCount = upCount;
        this.inspectCount = inspectCount;
        this.pressCount = pressCount;
        this.packageCount = packageCount;
        this.completePercent = completePercent;
    }

    public Integer getPackageCount() {
        return packageCount;
    }

    public void setPackageCount(Integer packageCount) {
        this.packageCount = packageCount;
    }

    public Integer getUpCount() {
        return upCount;
    }

    public void setUpCount(Integer upCount) {
        this.upCount = upCount;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }

    public Integer getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(Integer orderCount) {
        this.orderCount = orderCount;
    }

    public Integer getTailorCount() {
        return tailorCount;
    }

    public void setTailorCount(Integer tailorCount) {
        this.tailorCount = tailorCount;
    }

    public Integer getWellTailorCount() {
        return wellTailorCount;
    }

    public void setWellTailorCount(Integer wellTailorCount) {
        this.wellTailorCount = wellTailorCount;
    }

    public Double getWellPercent() {
        return wellPercent;
    }

    public void setWellPercent(Double wellPercent) {
        this.wellPercent = wellPercent;
    }

    public Integer getEmbCount() {
        return embCount;
    }

    public void setEmbCount(Integer embCount) {
        this.embCount = embCount;
    }

    public Integer getInspectCount() {
        return inspectCount;
    }

    public void setInspectCount(Integer inspectCount) {
        this.inspectCount = inspectCount;
    }

    public Integer getPressCount() {
        return pressCount;
    }

    public void setPressCount(Integer pressCount) {
        this.pressCount = pressCount;
    }

    public Double getCompletePercent() {
        return completePercent;
    }

    public void setCompletePercent(Double completePercent) {
        this.completePercent = completePercent;
    }

}
