package com.example.erp01.model;

import java.util.Date;

public class CutLoosePlan {

    private Integer id;

    private String clothesVersionNumber;

    private String orderName;

    private String colorName;

    private Integer orderCount;

    private Integer cutCount;

    private Integer inStoreBatch;

    private Integer storageBatch;

    private Integer preCutBatch;

    private Integer cutBatch;

    private String printingFactory;

    private String printingPart;

    private Date returnDate;

    private Date beginDate;

    private Date endDate;

    private String finishState;

    private String remark = "无";

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public Integer getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(Integer orderCount) {
        this.orderCount = orderCount;
    }

    public Integer getCutCount() {
        return cutCount;
    }

    public void setCutCount(Integer cutCount) {
        this.cutCount = cutCount;
    }

    public Integer getStorageBatch() {
        return storageBatch;
    }

    public void setStorageBatch(Integer storageBatch) {
        this.storageBatch = storageBatch;
    }

    public Integer getPreCutBatch() {
        return preCutBatch;
    }

    public void setPreCutBatch(Integer preCutBatch) {
        this.preCutBatch = preCutBatch;
    }

    public String getPrintingFactory() {
        return printingFactory;
    }

    public void setPrintingFactory(String printingFactory) {
        this.printingFactory = printingFactory;
    }

    public Date getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getFinishState() {
        return finishState;
    }

    public void setFinishState(String finishState) {
        this.finishState = finishState;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getPrintingPart() {
        return printingPart;
    }

    public void setPrintingPart(String printingPart) {
        this.printingPart = printingPart;
    }

    public Date getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(Date returnDate) {
        this.returnDate = returnDate;
    }

    public Integer getInStoreBatch() {
        return inStoreBatch;
    }

    public void setInStoreBatch(Integer inStoreBatch) {
        this.inStoreBatch = inStoreBatch;
    }

    public Integer getCutBatch() {
        return cutBatch;
    }

    public void setCutBatch(Integer cutBatch) {
        this.cutBatch = cutBatch;
    }

    public CutLoosePlan() {
    }
}
