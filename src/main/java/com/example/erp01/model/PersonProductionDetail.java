package com.example.erp01.model;


public class PersonProductionDetail {

    private String groupName;

    private String employeeNumber;

    private String employeeName;

    private Integer procedureNumber;

    private String procedureName;

    private Integer productionCount;

    public String getProcedureName() {
        return procedureName;
    }

    public void setProcedureName(String procedureName) {
        this.procedureName = procedureName;
    }

    public Integer getProcedureNumber() {
        return procedureNumber;
    }

    public void setProcedureNumber(Integer procedureNumber) {
        this.procedureNumber = procedureNumber;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getEmployeeNumber() {
        return employeeNumber;
    }

    public void setEmployeeNumber(String employeeNumber) {
        this.employeeNumber = employeeNumber;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public Integer getProductionCount() {
        return productionCount;
    }

    public void setProductionCount(Integer productionCount) {
        this.productionCount = productionCount;
    }

    public PersonProductionDetail() {
    }

    public PersonProductionDetail(String groupName, String employeeNumber, String employeeName, Integer productionCount) {
        this.groupName = groupName;
        this.employeeNumber = employeeNumber;
        this.employeeName = employeeName;
        this.productionCount = productionCount;
    }

    public PersonProductionDetail(String groupName, String employeeNumber, String employeeName, Integer procedureNumber, String procedureName, Integer productionCount) {
        this.groupName = groupName;
        this.employeeNumber = employeeNumber;
        this.employeeName = employeeName;
        this.procedureNumber = procedureNumber;
        this.procedureName = procedureName;
        this.productionCount = productionCount;
    }
}
