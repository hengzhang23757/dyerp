package com.example.erp01.model;

public class ProductionProgressDetail {

    private String orderName;

    private String clothesVersionNumber;

    private String colorName;

    private String sizeName;

    private String procedureCode;

    private Integer procedureNumber;

    private String procedureName;

    private Integer orderCount;

    private Integer cutCount;

    private Integer productionCount;

    private Integer differenceCount;

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public String getProcedureCode() {
        return procedureCode;
    }

    public void setProcedureCode(String procedureCode) {
        this.procedureCode = procedureCode;
    }

    public Integer getProcedureNumber() {
        return procedureNumber;
    }

    public void setProcedureNumber(Integer procedureNumber) {
        this.procedureNumber = procedureNumber;
    }

    public String getProcedureName() {
        return procedureName;
    }

    public void setProcedureName(String procedureName) {
        this.procedureName = procedureName;
    }

    public Integer getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(Integer orderCount) {
        this.orderCount = orderCount;
    }

    public Integer getCutCount() {
        return cutCount;
    }

    public void setCutCount(Integer cutCount) {
        this.cutCount = cutCount;
    }

    public Integer getProductionCount() {
        return productionCount;
    }

    public void setProductionCount(Integer productionCount) {
        this.productionCount = productionCount;
    }

    public Integer getDifferenceCount() {
        return differenceCount;
    }

    public void setDifferenceCount(Integer differenceCount) {
        this.differenceCount = differenceCount;
    }

    public ProductionProgressDetail() {
    }

    public ProductionProgressDetail(String orderName, String clothesVersionNumber, String colorName, String sizeName, String procedureCode, Integer procedureNumber, String procedureName, Integer orderCount, Integer cutCount, Integer productionCount, Integer differenceCount) {
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.colorName = colorName;
        this.sizeName = sizeName;
        this.procedureCode = procedureCode;
        this.procedureNumber = procedureNumber;
        this.procedureName = procedureName;
        this.orderCount = orderCount;
        this.cutCount = cutCount;
        this.productionCount = productionCount;
        this.differenceCount = differenceCount;
    }

    public ProductionProgressDetail(String orderName, String colorName, String sizeName, String procedureCode, Integer procedureNumber, Integer productionCount) {
        this.orderName = orderName;
        this.colorName = colorName;
        this.sizeName = sizeName;
        this.procedureCode = procedureCode;
        this.procedureNumber = procedureNumber;
        this.productionCount = productionCount;
    }
}
