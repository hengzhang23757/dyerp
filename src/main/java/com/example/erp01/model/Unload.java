package com.example.erp01.model;

import java.util.Date;

public class Unload {

    private String orderName;

    private String clothesVersionNumber;

    private String colorName;

    private String sizeName;

    private Integer unloadCount;

    private Date unloadTime;

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public Integer getUnloadCount() {
        return unloadCount;
    }

    public void setUnloadCount(Integer unloadCount) {
        this.unloadCount = unloadCount;
    }

    public Date getUnloadTime() {
        return unloadTime;
    }

    public void setUnloadTime(Date unloadTime) {
        this.unloadTime = unloadTime;
    }

    public Unload() {
    }

    public Unload(String orderName, String clothesVersionNumber, String colorName, String sizeName, Integer unloadCount, Date unloadTime) {
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.colorName = colorName;
        this.sizeName = sizeName;
        this.unloadCount = unloadCount;
        this.unloadTime = unloadTime;
    }
}
