package com.example.erp01.model;

public class CutStorageQuery {

    private String orderName;

    private Integer bedNumber;

    private String colorName;

    private String sizeName;

    private Integer cutStorageCount;

    private String storehouseLocation;

    public Integer getBedNumber() {
        return bedNumber;
    }

    public void setBedNumber(Integer bedNumber) {
        this.bedNumber = bedNumber;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public Integer getCutStorageCount() {
        return cutStorageCount;
    }

    public void setCutStorageCount(Integer cutStorageCount) {
        this.cutStorageCount = cutStorageCount;
    }

    public String getStorehouseLocation() {
        return storehouseLocation;
    }

    public void setStorehouseLocation(String storehouseLocation) {
        this.storehouseLocation = storehouseLocation;
    }

    public CutStorageQuery() {
    }

    public CutStorageQuery(String orderName, Integer bedNumber, String colorName, String sizeName, Integer cutStorageCount, String storehouseLocation) {
        this.orderName = orderName;
        this.bedNumber = bedNumber;
        this.colorName = colorName;
        this.sizeName = sizeName;
        this.cutStorageCount = cutStorageCount;
        this.storehouseLocation = storehouseLocation;
    }

    public CutStorageQuery(String orderName, String colorName, String sizeName, Integer cutStorageCount, String storehouseLocation) {
        this.orderName = orderName;
        this.colorName = colorName;
        this.sizeName = sizeName;
        this.cutStorageCount = cutStorageCount;
        this.storehouseLocation = storehouseLocation;
    }
}
