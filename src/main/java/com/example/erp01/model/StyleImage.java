package com.example.erp01.model;

public class StyleImage {

    private Integer id;

    private String orderName;

    private String clothesVersionNumber;

    private String imageText;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public String getImageText() {
        return imageText;
    }

    public void setImageText(String imageText) {
        this.imageText = imageText;
    }

    public StyleImage() {
    }

    public StyleImage(String orderName, String clothesVersionNumber, String imageText) {
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.imageText = imageText;
    }
}
