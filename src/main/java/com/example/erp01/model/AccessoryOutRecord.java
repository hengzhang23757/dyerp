package com.example.erp01.model;

import java.util.Date;

public class AccessoryOutRecord {

    private Integer id;

    private String orderName;

    private String clothesVersionNumber;

    private String accessoryName;

    private String accessoryNumber;

    private String specification = "未输";

    private String accessoryUnit;

    private String accessoryColor = "未输";

    private Float pieceUsage = 0f;

    private String sizeName;

    private String colorName;

    private Float outStoreCount = 0f;

    private String accessoryLocation;

    private String supplier;

    private String outStoreTime;

    private Integer orderCount;

    private Integer wellCount = 0;

    private Date createTime;

    private String remark = "无";

    private String receiver;

    private String colorNumber = "";

    private Integer accessoryID;

    private Float finishCount = 0f;

    private Float needCount = 0f;

    private Float diffCount = 0f;

    private String accessoryType;

    public Integer getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(Integer orderCount) {
        this.orderCount = orderCount;
    }

    public Integer getWellCount() {
        return wellCount;
    }

    public void setWellCount(Integer wellCount) {
        this.wellCount = wellCount;
    }

    public AccessoryOutRecord() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public String getAccessoryName() {
        return accessoryName;
    }

    public void setAccessoryName(String accessoryName) {
        this.accessoryName = accessoryName;
    }

    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }

    public String getAccessoryUnit() {
        return accessoryUnit;
    }

    public void setAccessoryUnit(String accessoryUnit) {
        this.accessoryUnit = accessoryUnit;
    }

    public String getAccessoryColor() {
        return accessoryColor;
    }

    public void setAccessoryColor(String accessoryColor) {
        this.accessoryColor = accessoryColor;
    }

    public Float getPieceUsage() {
        return pieceUsage;
    }

    public void setPieceUsage(Float pieceUsage) {
        this.pieceUsage = pieceUsage;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public Float getOutStoreCount() {
        return outStoreCount;
    }

    public void setOutStoreCount(Float outStoreCount) {
        this.outStoreCount = outStoreCount;
    }

    public String getAccessoryLocation() {
        return accessoryLocation;
    }

    public void setAccessoryLocation(String accessoryLocation) {
        this.accessoryLocation = accessoryLocation;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public String getOutStoreTime() {
        return outStoreTime;
    }

    public void setOutStoreTime(String outStoreTime) {
        this.outStoreTime = outStoreTime;
    }

    public String getAccessoryNumber() {
        return accessoryNumber;
    }

    public void setAccessoryNumber(String accessoryNumber) {
        this.accessoryNumber = accessoryNumber;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }


    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }


    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getColorNumber() {
        return colorNumber;
    }

    public void setColorNumber(String colorNumber) {
        this.colorNumber = colorNumber;
    }

    public Integer getAccessoryID() {
        return accessoryID;
    }

    public void setAccessoryID(Integer accessoryID) {
        this.accessoryID = accessoryID;
    }

    public Float getFinishCount() {
        return finishCount;
    }

    public void setFinishCount(Float finishCount) {
        this.finishCount = finishCount;
    }

    public Float getNeedCount() {
        return needCount;
    }

    public void setNeedCount(Float needCount) {
        this.needCount = needCount;
    }

    public Float getDiffCount() {
        return diffCount;
    }

    public void setDiffCount(Float diffCount) {
        this.diffCount = diffCount;
    }

    public String getAccessoryType() {
        return accessoryType;
    }

    public void setAccessoryType(String accessoryType) {
        this.accessoryType = accessoryType;
    }

    @Override
    public String toString() {
        return "AccessoryOutRecord{" +
                "id=" + id +
                ", orderName='" + orderName + '\'' +
                ", clothesVersionNumber='" + clothesVersionNumber + '\'' +
                ", accessoryName='" + accessoryName + '\'' +
                ", accessoryNumber='" + accessoryNumber + '\'' +
                ", specification='" + specification + '\'' +
                ", accessoryUnit='" + accessoryUnit + '\'' +
                ", accessoryColor='" + accessoryColor + '\'' +
                ", pieceUsage=" + pieceUsage +
                ", sizeName='" + sizeName + '\'' +
                ", colorName='" + colorName + '\'' +
                ", outStoreCount=" + outStoreCount +
                ", accessoryLocation='" + accessoryLocation + '\'' +
                ", supplier='" + supplier + '\'' +
                ", outStoreTime='" + outStoreTime + '\'' +
                ", orderCount=" + orderCount +
                ", wellCount=" + wellCount +
                ", createTime=" + createTime +
                ", remark='" + remark + '\'' +
                ", receiver='" + receiver + '\'' +
                ", colorNumber='" + colorNumber + '\'' +
                ", accessoryID=" + accessoryID +
                '}';
    }
}
