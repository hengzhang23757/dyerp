package com.example.erp01.model;

public class Room {

    private Integer roomID;

    private String floor;

    private String roomNumber;

    private Integer capacity;

    private String remark;

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getRoomID() {
        return roomID;
    }

    public void setRoomID(Integer roomID) {
        this.roomID = roomID;
    }

    public String getFloor() {
        return floor;
    }

    public void setFloor(String floor) {
        this.floor = floor;
    }

    public String getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(String roomNumber) {
        this.roomNumber = roomNumber;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    public Room() {
    }

    public Room(String floor, String roomNumber, Integer capacity) {
        this.floor = floor;
        this.roomNumber = roomNumber;
        this.capacity = capacity;
    }

    public Room(String floor, String roomNumber, Integer capacity, String remark) {
        this.floor = floor;
        this.roomNumber = roomNumber;
        this.capacity = capacity;
        this.remark = remark;
    }

    public Room(Integer roomID, String floor, String roomNumber, Integer capacity, String remark) {
        this.roomID = roomID;
        this.floor = floor;
        this.roomNumber = roomNumber;
        this.capacity = capacity;
        this.remark = remark;
    }
}
