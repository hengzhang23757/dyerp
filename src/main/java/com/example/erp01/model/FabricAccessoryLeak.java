package com.example.erp01.model;

public class FabricAccessoryLeak {

    private String customerName;

    private String season;

    private String modelType;

    private String orderName;

    private String clothesVersionNumber;

    private Integer orderCount = 0;

    private Integer wellCount = 0;

    private Float purchaseCount = 0f;

    private Float inStoreCount = 0f;

    private Float readyPercent = 0f;

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }

    public String getModelType() {
        return modelType;
    }

    public void setModelType(String modelType) {
        this.modelType = modelType;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public Integer getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(Integer orderCount) {
        this.orderCount = orderCount;
    }

    public Integer getWellCount() {
        return wellCount;
    }

    public void setWellCount(Integer wellCount) {
        this.wellCount = wellCount;
    }

    public Float getPurchaseCount() {
        return purchaseCount;
    }

    public void setPurchaseCount(Float purchaseCount) {
        this.purchaseCount = purchaseCount;
    }

    public Float getInStoreCount() {
        return inStoreCount;
    }

    public void setInStoreCount(Float inStoreCount) {
        this.inStoreCount = inStoreCount;
    }

    public Float getReadyPercent() {
        return readyPercent;
    }

    public void setReadyPercent(Float readyPercent) {
        this.readyPercent = readyPercent;
    }

    public FabricAccessoryLeak() {
    }
}
