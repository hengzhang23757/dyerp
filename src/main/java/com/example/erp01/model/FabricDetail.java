package com.example.erp01.model;

import java.util.Date;

public class FabricDetail {

    private Integer fabricDetailID;

    private String clothesVersionNumber;

    private String orderName;

    private String fabricName;

    private String fabricNumber;

    private String fabricColor;

    private String fabricColorNumber;

    private String jarName;

    private Integer batchNumber;

    private Float weight = 0f;

    private String location;

    private String operateType;

    private Date returnTime;

    private String supplier;

    private String colorName;

    private String isHit;

    private String unit;

    private Integer fabricID;

    private String checkNumber;

    private Integer orderCount;

    private Float price = 0f;

    private Float fabricActCount = 0f;

    private Float returnMoney = 0f;

    private Float finishWeight = 0f;

    private String reviewType = "面料";

    private String remark;

    private String checkMonth;

    private Float addFee = 0f;

    public String getFabricNumber() {
        return fabricNumber;
    }

    public void setFabricNumber(String fabricNumber) {
        this.fabricNumber = fabricNumber;
    }

    public String getFabricColorNumber() {
        return fabricColorNumber;
    }

    public void setFabricColorNumber(String fabricColorNumber) {
        this.fabricColorNumber = fabricColorNumber;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getIsHit() {
        return isHit;
    }

    public void setIsHit(String isHit) {
        this.isHit = isHit;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getOperateType() {
        return operateType;
    }

    public void setOperateType(String operateType) {
        this.operateType = operateType;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public Integer getFabricDetailID() {
        return fabricDetailID;
    }

    public void setFabricDetailID(Integer fabricDetailID) {
        this.fabricDetailID = fabricDetailID;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getFabricName() {
        return fabricName;
    }

    public void setFabricName(String fabricName) {
        this.fabricName = fabricName;
    }

    public String getFabricColor() {
        return fabricColor;
    }

    public void setFabricColor(String fabricColor) {
        this.fabricColor = fabricColor;
    }

    public String getJarName() {
        return jarName;
    }

    public void setJarName(String jarName) {
        this.jarName = jarName;
    }

    public Integer getBatchNumber() {
        return batchNumber;
    }

    public void setBatchNumber(Integer batchNumber) {
        this.batchNumber = batchNumber;
    }

    public Float getWeight() {
        return weight;
    }

    public void setWeight(Float weight) {
        this.weight = weight;
    }

    public Date getReturnTime() {
        return returnTime;
    }

    public void setReturnTime(Date returnTime) {
        this.returnTime = returnTime;
    }

    public Integer getFabricID() {
        return fabricID;
    }

    public void setFabricID(Integer fabricID) {
        this.fabricID = fabricID;
    }

    public String getCheckNumber() {
        return checkNumber;
    }

    public void setCheckNumber(String checkNumber) {
        this.checkNumber = checkNumber;
    }

    public Integer getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(Integer orderCount) {
        this.orderCount = orderCount;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Float getFabricActCount() {
        return fabricActCount;
    }

    public void setFabricActCount(Float fabricActCount) {
        this.fabricActCount = fabricActCount;
    }

    public Float getReturnMoney() {
        return returnMoney;
    }

    public void setReturnMoney(Float returnMoney) {
        this.returnMoney = returnMoney;
    }

    public Float getFinishWeight() {
        return finishWeight;
    }

    public void setFinishWeight(Float finishWeight) {
        this.finishWeight = finishWeight;
    }

    public String getReviewType() {
        return reviewType;
    }

    public void setReviewType(String reviewType) {
        this.reviewType = reviewType;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCheckMonth() {
        return checkMonth;
    }

    public void setCheckMonth(String checkMonth) {
        this.checkMonth = checkMonth;
    }

    public Float getAddFee() {
        return addFee;
    }

    public void setAddFee(Float addFee) {
        this.addFee = addFee;
    }

    public FabricDetail() {
    }

    public FabricDetail(String clothesVersionNumber, String orderName, String fabricName, String fabricColor, String jarName, Integer batchNumber, Float weight, String location, String operateType, Date returnTime) {
        this.clothesVersionNumber = clothesVersionNumber;
        this.orderName = orderName;
        this.fabricName = fabricName;
        this.fabricColor = fabricColor;
        this.jarName = jarName;
        this.batchNumber = batchNumber;
        this.weight = weight;
        this.location = location;
        this.operateType = operateType;
        this.returnTime = returnTime;
    }

    public FabricDetail(Integer fabricDetailID, String clothesVersionNumber, String orderName, String fabricName, String fabricColor, String jarName, Integer batchNumber, Float weight, String location, String operateType, Date returnTime) {
        this.fabricDetailID = fabricDetailID;
        this.clothesVersionNumber = clothesVersionNumber;
        this.orderName = orderName;
        this.fabricName = fabricName;
        this.fabricColor = fabricColor;
        this.jarName = jarName;
        this.batchNumber = batchNumber;
        this.weight = weight;
        this.location = location;
        this.operateType = operateType;
        this.returnTime = returnTime;
    }

    public FabricDetail(String clothesVersionNumber, String orderName, String fabricName, String fabricColor, String jarName, Integer batchNumber, Float weight, String location, String operateType, Date returnTime, String supplier, String colorName, String isHit, String unit) {
        this.clothesVersionNumber = clothesVersionNumber;
        this.orderName = orderName;
        this.fabricName = fabricName;
        this.fabricColor = fabricColor;
        this.jarName = jarName;
        this.batchNumber = batchNumber;
        this.weight = weight;
        this.location = location;
        this.operateType = operateType;
        this.returnTime = returnTime;
        this.supplier = supplier;
        this.colorName = colorName;
        this.isHit = isHit;
        this.unit = unit;
    }

    @Override
    public String toString() {
        return "FabricDetail{" +
                "fabricDetailID=" + fabricDetailID +
                ", clothesVersionNumber='" + clothesVersionNumber + '\'' +
                ", orderName='" + orderName + '\'' +
                ", fabricName='" + fabricName + '\'' +
                ", fabricNumber='" + fabricNumber + '\'' +
                ", fabricColor='" + fabricColor + '\'' +
                ", fabricColorNumber='" + fabricColorNumber + '\'' +
                ", jarName='" + jarName + '\'' +
                ", batchNumber=" + batchNumber +
                ", weight=" + weight +
                ", location='" + location + '\'' +
                ", operateType='" + operateType + '\'' +
                ", returnTime=" + returnTime +
                ", supplier='" + supplier + '\'' +
                ", colorName='" + colorName + '\'' +
                ", isHit='" + isHit + '\'' +
                ", unit='" + unit + '\'' +
                ", fabricID=" + fabricID +
                '}';
    }
}
