package com.example.erp01.model;

public class DesignDepart {

    private Integer id;

    private String designDepartName;

    private String designDepartCode;

    public DesignDepart() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDesignDepartName() {
        return designDepartName;
    }

    public void setDesignDepartName(String designDepartName) {
        this.designDepartName = designDepartName;
    }

    public String getDesignDepartCode() {
        return designDepartCode;
    }

    public void setDesignDepartCode(String designDepartCode) {
        this.designDepartCode = designDepartCode;
    }
}
