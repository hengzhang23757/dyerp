package com.example.erp01.model;

import java.io.Serializable;
import java.util.Date;

public class XhlTailorReport implements Serializable {

    private String customerName;

    private String orderName;

    private String xhlOrderName;

    private String userName;

    private Integer bedNumber;

    private String printingPart;

    private String colorName;

    private String sizeName;

    private Integer packageCount;

    private Integer totalLayer;

    private Date sendDate;

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getXhlOrderName() {
        return xhlOrderName;
    }

    public void setXhlOrderName(String xhlOrderName) {
        this.xhlOrderName = xhlOrderName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getBedNumber() {
        return bedNumber;
    }

    public void setBedNumber(Integer bedNumber) {
        this.bedNumber = bedNumber;
    }

    public String getPrintingPart() {
        return printingPart;
    }

    public void setPrintingPart(String printingPart) {
        this.printingPart = printingPart;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public Integer getPackageCount() {
        return packageCount;
    }

    public void setPackageCount(Integer packageCount) {
        this.packageCount = packageCount;
    }

    public Integer getTotalLayer() {
        return totalLayer;
    }

    public void setTotalLayer(Integer totalLayer) {
        this.totalLayer = totalLayer;
    }

    public Date getSendDate() {
        return sendDate;
    }

    public void setSendDate(Date sendDate) {
        this.sendDate = sendDate;
    }

    public XhlTailorReport() {
    }

    public XhlTailorReport(String customerName, String orderName, String xhlOrderName, String userName, Integer bedNumber, String printingPart, String colorName, String sizeName, Integer packageCount, Integer totalLayer, Date sendDate) {
        this.customerName = customerName;
        this.orderName = orderName;
        this.xhlOrderName = xhlOrderName;
        this.userName = userName;
        this.bedNumber = bedNumber;
        this.printingPart = printingPart;
        this.colorName = colorName;
        this.sizeName = sizeName;
        this.packageCount = packageCount;
        this.totalLayer = totalLayer;
        this.sendDate = sendDate;
    }
}
