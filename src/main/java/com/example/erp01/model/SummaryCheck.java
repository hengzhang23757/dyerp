package com.example.erp01.model;

import java.util.Date;

public class SummaryCheck {

    private Integer id;

    private String employeeName;

    private String employeeNumber;

    private String departmentName;

    private String groupName;

    private String monthstring;

    private String useraccount;

    private Integer workday;

    private Integer realday;

    private Integer worktime;

    private Float worktimeHour;

    private Integer realtime;

    private Float realtimeHour;

    private Integer notcheckin;

    private Integer notcheckout;

    private Integer patchtimes;

    private Integer latertimes;

    private Integer latetime;

    private Float latetimeHour;

    private Integer earlytimes;

    private Integer earlytime;

    private Float earlytimeHour;

    private Integer absenttimes;

    private Integer absenttime;

    private Float absenttimeHour;

    private Integer applytime;

    private Float applytimeHour;

    private Integer overtime;

    private Float overtimeHour;

    private Integer switchtime;

    private Float switchtimeHour;

    private Integer outtime;

    private Float outtimeHour;

    private Integer holidaytime;

    private Float holidaytimeHour;

    private Integer moretime;

    private Float moretimeHour;

    private Date createTime;

    private Date updateTime;

    public SummaryCheck() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getEmployeeNumber() {
        return employeeNumber;
    }

    public void setEmployeeNumber(String employeeNumber) {
        this.employeeNumber = employeeNumber;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getMonthstring() {
        return monthstring;
    }

    public void setMonthstring(String monthstring) {
        this.monthstring = monthstring;
    }

    public String getUseraccount() {
        return useraccount;
    }

    public void setUseraccount(String useraccount) {
        this.useraccount = useraccount;
    }

    public Integer getWorkday() {
        return workday;
    }

    public void setWorkday(Integer workday) {
        this.workday = workday;
    }

    public Integer getRealday() {
        return realday;
    }

    public void setRealday(Integer realday) {
        this.realday = realday;
    }

    public Integer getWorktime() {
        return worktime;
    }

    public void setWorktime(Integer worktime) {
        this.worktime = worktime;
    }

    public Integer getRealtime() {
        return realtime;
    }

    public void setRealtime(Integer realtime) {
        this.realtime = realtime;
    }

    public Integer getNotcheckin() {
        return notcheckin;
    }

    public void setNotcheckin(Integer notcheckin) {
        this.notcheckin = notcheckin;
    }

    public Integer getNotcheckout() {
        return notcheckout;
    }

    public void setNotcheckout(Integer notcheckout) {
        this.notcheckout = notcheckout;
    }

    public Integer getPatchtimes() {
        return patchtimes;
    }

    public void setPatchtimes(Integer patchtimes) {
        this.patchtimes = patchtimes;
    }

    public Integer getLatertimes() {
        return latertimes;
    }

    public void setLatertimes(Integer latertimes) {
        this.latertimes = latertimes;
    }

    public Integer getLatetime() {
        return latetime;
    }

    public void setLatetime(Integer latetime) {
        this.latetime = latetime;
    }

    public Integer getEarlytimes() {
        return earlytimes;
    }

    public void setEarlytimes(Integer earlytimes) {
        this.earlytimes = earlytimes;
    }

    public Integer getEarlytime() {
        return earlytime;
    }

    public void setEarlytime(Integer earlytime) {
        this.earlytime = earlytime;
    }

    public Integer getAbsenttimes() {
        return absenttimes;
    }

    public void setAbsenttimes(Integer absenttimes) {
        this.absenttimes = absenttimes;
    }

    public Integer getAbsenttime() {
        return absenttime;
    }

    public void setAbsenttime(Integer absenttime) {
        this.absenttime = absenttime;
    }

    public Integer getApplytime() {
        return applytime;
    }

    public void setApplytime(Integer applytime) {
        this.applytime = applytime;
    }

    public Integer getOvertime() {
        return overtime;
    }

    public void setOvertime(Integer overtime) {
        this.overtime = overtime;
    }

    public Integer getSwitchtime() {
        return switchtime;
    }

    public void setSwitchtime(Integer switchtime) {
        this.switchtime = switchtime;
    }

    public Integer getOuttime() {
        return outtime;
    }

    public void setOuttime(Integer outtime) {
        this.outtime = outtime;
    }

    public Integer getHolidaytime() {
        return holidaytime;
    }

    public void setHolidaytime(Integer holidaytime) {
        this.holidaytime = holidaytime;
    }

    public Integer getMoretime() {
        return moretime;
    }

    public void setMoretime(Integer moretime) {
        this.moretime = moretime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Float getWorktimeHour() {
        return worktimeHour;
    }

    public void setWorktimeHour(Float worktimeHour) {
        this.worktimeHour = worktimeHour;
    }

    public Float getRealtimeHour() {
        return realtimeHour;
    }

    public void setRealtimeHour(Float realtimeHour) {
        this.realtimeHour = realtimeHour;
    }

    public Float getLatetimeHour() {
        return latetimeHour;
    }

    public void setLatetimeHour(Float latetimeHour) {
        this.latetimeHour = latetimeHour;
    }

    public Float getEarlytimeHour() {
        return earlytimeHour;
    }

    public void setEarlytimeHour(Float earlytimeHour) {
        this.earlytimeHour = earlytimeHour;
    }

    public Float getAbsenttimeHour() {
        return absenttimeHour;
    }

    public void setAbsenttimeHour(Float absenttimeHour) {
        this.absenttimeHour = absenttimeHour;
    }

    public Float getApplytimeHour() {
        return applytimeHour;
    }

    public void setApplytimeHour(Float applytimeHour) {
        this.applytimeHour = applytimeHour;
    }

    public Float getOvertimeHour() {
        return overtimeHour;
    }

    public void setOvertimeHour(Float overtimeHour) {
        this.overtimeHour = overtimeHour;
    }

    public Float getSwitchtimeHour() {
        return switchtimeHour;
    }

    public void setSwitchtimeHour(Float switchtimeHour) {
        this.switchtimeHour = switchtimeHour;
    }

    public Float getOuttimeHour() {
        return outtimeHour;
    }

    public void setOuttimeHour(Float outtimeHour) {
        this.outtimeHour = outtimeHour;
    }

    public Float getHolidaytimeHour() {
        return holidaytimeHour;
    }

    public void setHolidaytimeHour(Float holidaytimeHour) {
        this.holidaytimeHour = holidaytimeHour;
    }

    public Float getMoretimeHour() {
        return moretimeHour;
    }

    public void setMoretimeHour(Float moretimeHour) {
        this.moretimeHour = moretimeHour;
    }

    @Override
    public String toString() {
        return "SummaryCheck{" +
                "id=" + id +
                ", employeeName='" + employeeName + '\'' +
                ", employeeNumber='" + employeeNumber + '\'' +
                ", departmentName='" + departmentName + '\'' +
                ", groupName='" + groupName + '\'' +
                ", monthstring='" + monthstring + '\'' +
                ", useraccount='" + useraccount + '\'' +
                ", workday=" + workday +
                ", realday=" + realday +
                ", worktime=" + worktime +
                ", realtime=" + realtime +
                ", notcheckin=" + notcheckin +
                ", notcheckout=" + notcheckout +
                ", patchtimes=" + patchtimes +
                ", latertimes=" + latertimes +
                ", latetime=" + latetime +
                ", earlytimes=" + earlytimes +
                ", earlytime=" + earlytime +
                ", absenttimes=" + absenttimes +
                ", absenttime=" + absenttime +
                ", applytime=" + applytime +
                ", overtime=" + overtime +
                ", switchtime=" + switchtime +
                ", outtime=" + outtime +
                ", holidaytime=" + holidaytime +
                ", moretime=" + moretime +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                '}';
    }
}














