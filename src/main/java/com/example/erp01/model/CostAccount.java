package com.example.erp01.model;

public class CostAccount {

    private String clothesVersionNumber;
    private String orderName;
    private Integer orderCount = 0;
    private Integer cutCount = 0;
    private Integer wellCount = 0;
    private Integer endProductCount = 0;
    private Float fabricCost = 0f;
    private Float avgFabricCost = 0f;
    private Float accessoryCost = 0f;
    private Float avgAccessoryCost = 0f;
    private Float cutCost = 0f;
    private Float avgCutCost = 0f;
    private Float sewCost = 0f;
    private Float avgSewCost = 0f;
    private Float hourCost = 0f;
    private Float printCost = 0f;
    private Float avgPrintCost = 0f;
    private Float cutPrice = 0f;
    private Float sumMoney = 0f;
    private Float addFee = 0f;
    private Float fabricCustomerCost = 0f;
    private Float accessoryCustomerCost = 0f;
    private Float fabricReturnCost = 0f;
    private Float accessoryReturnCost = 0f;

    public CostAccount() {
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public Integer getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(Integer orderCount) {
        this.orderCount = orderCount;
    }

    public Integer getCutCount() {
        return cutCount;
    }

    public void setCutCount(Integer cutCount) {
        this.cutCount = cutCount;
    }

    public Integer getWellCount() {
        return wellCount;
    }

    public void setWellCount(Integer wellCount) {
        this.wellCount = wellCount;
    }

    public Float getFabricCost() {
        return fabricCost;
    }

    public void setFabricCost(Float fabricCost) {
        this.fabricCost = fabricCost;
    }

    public Float getAccessoryCost() {
        return accessoryCost;
    }

    public void setAccessoryCost(Float accessoryCost) {
        this.accessoryCost = accessoryCost;
    }

    public Float getCutCost() {
        return cutCost;
    }

    public void setCutCost(Float cutCost) {
        this.cutCost = cutCost;
    }

    public Float getSewCost() {
        return sewCost;
    }

    public void setSewCost(Float sewCost) {
        this.sewCost = sewCost;
    }

    public Float getHourCost() {
        return hourCost;
    }

    public void setHourCost(Float hourCost) {
        this.hourCost = hourCost;
    }

    public Float getPrintCost() {
        return printCost;
    }

    public void setPrintCost(Float printCost) {
        this.printCost = printCost;
    }

    public Float getAvgFabricCost() {
        return avgFabricCost;
    }

    public void setAvgFabricCost(Float avgFabricCost) {
        this.avgFabricCost = avgFabricCost;
    }

    public Float getAvgAccessoryCost() {
        return avgAccessoryCost;
    }

    public void setAvgAccessoryCost(Float avgAccessoryCost) {
        this.avgAccessoryCost = avgAccessoryCost;
    }

    public Float getAvgCutCost() {
        return avgCutCost;
    }

    public void setAvgCutCost(Float avgCutCost) {
        this.avgCutCost = avgCutCost;
    }

    public Float getAvgSewCost() {
        return avgSewCost;
    }

    public void setAvgSewCost(Float avgSewCost) {
        this.avgSewCost = avgSewCost;
    }

    public Float getAvgPrintCost() {
        return avgPrintCost;
    }

    public void setAvgPrintCost(Float avgPrintCost) {
        this.avgPrintCost = avgPrintCost;
    }

    public Integer getEndProductCount() {
        return endProductCount;
    }

    public void setEndProductCount(Integer endProductCount) {
        this.endProductCount = endProductCount;
    }

    public Float getSumMoney() {
        return sumMoney;
    }

    public void setSumMoney(Float sumMoney) {
        this.sumMoney = sumMoney;
    }

    public Float getCutPrice() {
        return cutPrice;
    }

    public void setCutPrice(Float cutPrice) {
        this.cutPrice = cutPrice;
    }

    public Float getAddFee() {
        return addFee;
    }

    public void setAddFee(Float addFee) {
        this.addFee = addFee;
    }

    public Float getFabricCustomerCost() {
        return fabricCustomerCost;
    }

    public void setFabricCustomerCost(Float fabricCustomerCost) {
        this.fabricCustomerCost = fabricCustomerCost;
    }

    public Float getAccessoryCustomerCost() {
        return accessoryCustomerCost;
    }

    public void setAccessoryCustomerCost(Float accessoryCustomerCost) {
        this.accessoryCustomerCost = accessoryCustomerCost;
    }

    public Float getFabricReturnCost() {
        return fabricReturnCost;
    }

    public void setFabricReturnCost(Float fabricReturnCost) {
        this.fabricReturnCost = fabricReturnCost;
    }

    public Float getAccessoryReturnCost() {
        return accessoryReturnCost;
    }

    public void setAccessoryReturnCost(Float accessoryReturnCost) {
        this.accessoryReturnCost = accessoryReturnCost;
    }
}
