package com.example.erp01.model;

public class PrenatalProgress {

    private String orderName;

    private String clothesVersionNumber;

    private String colorName;

    private String sizeName;

    private String partName;

    private Integer orderCount = 0;

    private Integer wellCount = 0;

    private Integer cutDiff = 0;

    private Integer looseCount = 0;

    private Integer opaCount = 0;

    private Integer opaBackCount = 0;

    private Integer opaDiff = 0;

    private Integer matchCount = 0;

    private Integer embInStore = 0;

    private Integer unloadCount = 0;

    private Integer embDiff = 0;

    private Integer hangCount = 0;

    public PrenatalProgress() {
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public Integer getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(Integer orderCount) {
        this.orderCount = orderCount;
    }

    public Integer getWellCount() {
        return wellCount;
    }

    public void setWellCount(Integer wellCount) {
        this.wellCount = wellCount;
    }

    public Integer getCutDiff() {
        return cutDiff;
    }

    public void setCutDiff(Integer cutDiff) {
        this.cutDiff = cutDiff;
    }

    public Integer getOpaCount() {
        return opaCount;
    }

    public void setOpaCount(Integer opaCount) {
        this.opaCount = opaCount;
    }

    public Integer getOpaBackCount() {
        return opaBackCount;
    }

    public void setOpaBackCount(Integer opaBackCount) {
        this.opaBackCount = opaBackCount;
    }

    public Integer getOpaDiff() {
        return opaDiff;
    }

    public void setOpaDiff(Integer opaDiff) {
        this.opaDiff = opaDiff;
    }

    public Integer getMatchCount() {
        return matchCount;
    }

    public void setMatchCount(Integer matchCount) {
        this.matchCount = matchCount;
    }

    public Integer getUnloadCount() {
        return unloadCount;
    }

    public void setUnloadCount(Integer unloadCount) {
        this.unloadCount = unloadCount;
    }

    public Integer getEmbDiff() {
        return embDiff;
    }

    public void setEmbDiff(Integer embDiff) {
        this.embDiff = embDiff;
    }

    public String getPartName() {
        return partName;
    }

    public void setPartName(String partName) {
        this.partName = partName;
    }

    public Integer getEmbInStore() {
        return embInStore;
    }

    public void setEmbInStore(Integer embInStore) {
        this.embInStore = embInStore;
    }

    public Integer getLooseCount() {
        return looseCount;
    }

    public void setLooseCount(Integer looseCount) {
        this.looseCount = looseCount;
    }

    public Integer getHangCount() {
        return hangCount;
    }

    public void setHangCount(Integer hangCount) {
        this.hangCount = hangCount;
    }
}
