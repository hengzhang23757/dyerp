package com.example.erp01.model;

public class MinMaxNid {

    private Long minNid;

    private Long maxNid;

    public MinMaxNid() {
    }

    public Long getMinNid() {
        return minNid;
    }

    public void setMinNid(Long minNid) {
        this.minNid = minNid;
    }

    public Long getMaxNid() {
        return maxNid;
    }

    public void setMaxNid(Long maxNid) {
        this.maxNid = maxNid;
    }
}
