package com.example.erp01.model;

import java.util.Date;

public class WeekAmount implements Comparable<WeekAmount>{

    private Date recordDate;

    private Integer workAmount = 0;

    public Date getRecordDate() {
        return recordDate;
    }

    public void setRecordDate(Date recordDate) {
        this.recordDate = recordDate;
    }

    public Integer getWorkAmount() {
        return workAmount;
    }

    public void setWorkAmount(Integer workAmount) {
        this.workAmount = workAmount;
    }

    public WeekAmount() {
    }

    public WeekAmount(Date recordDate, Integer workAmount) {
        this.recordDate = recordDate;
        this.workAmount = workAmount;
    }

    @Override
    public int compareTo(WeekAmount o) {
        return this.recordDate.compareTo(o.getRecordDate());
    }
}
