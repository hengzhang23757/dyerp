package com.example.erp01.model;

public class SizeUpdate {

    private String size;

    private String toSize;

    public SizeUpdate() {
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getToSize() {
        return toSize;
    }

    public void setToSize(String toSize) {
        this.toSize = toSize;
    }
}
