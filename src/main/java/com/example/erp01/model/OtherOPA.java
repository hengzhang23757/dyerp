package com.example.erp01.model;

import java.util.Date;

public class OtherOPA {

    private Integer otherOpaID;

    private String orderName;

    private String clothesVersionNumber;

    private String customerName;

    private String destination;

    private Integer bedNumber;

    private String partName;

    private String colorName;

    private String sizeName;

    private Integer opaCount;

    private Date opaDate;

    public Integer getOtherOpaID() {
        return otherOpaID;
    }

    public void setOtherOpaID(Integer otherOpaID) {
        this.otherOpaID = otherOpaID;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public Integer getBedNumber() {
        return bedNumber;
    }

    public void setBedNumber(Integer bedNumber) {
        this.bedNumber = bedNumber;
    }

    public String getPartName() {
        return partName;
    }

    public void setPartName(String partName) {
        this.partName = partName;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public Integer getOpaCount() {
        return opaCount;
    }

    public void setOpaCount(Integer opaCount) {
        this.opaCount = opaCount;
    }

    public Date getOpaDate() {
        return opaDate;
    }

    public void setOpaDate(Date opaDate) {
        this.opaDate = opaDate;
    }

    public OtherOPA() {
    }

    public OtherOPA(String orderName, String clothesVersionNumber, String customerName, String destination, Integer bedNumber, String partName, String colorName, String sizeName, Integer opaCount, Date opaDate) {
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.customerName = customerName;
        this.destination = destination;
        this.bedNumber = bedNumber;
        this.partName = partName;
        this.colorName = colorName;
        this.sizeName = sizeName;
        this.opaCount = opaCount;
        this.opaDate = opaDate;
    }

    public OtherOPA(Integer otherOpaID, String orderName, String clothesVersionNumber, String customerName, String destination, Integer bedNumber, String partName, String colorName, String sizeName, Integer opaCount, Date opaDate) {
        this.otherOpaID = otherOpaID;
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.customerName = customerName;
        this.destination = destination;
        this.bedNumber = bedNumber;
        this.partName = partName;
        this.colorName = colorName;
        this.sizeName = sizeName;
        this.opaCount = opaCount;
        this.opaDate = opaDate;
    }
}
