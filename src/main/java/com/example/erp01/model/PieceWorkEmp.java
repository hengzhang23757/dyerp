package com.example.erp01.model;

import java.sql.Timestamp;

public class PieceWorkEmp {

    private String employeeNumber;

    private String employeeName;

    private String orderName;

    private Integer procedureNumber;

    private String procedureName;

    private String pieceTime;

    @Override
    public String toString() {
        return "PieceWorkEmp{" +
                "employeeNumber='" + employeeNumber + '\'' +
                ", employeeName='" + employeeName + '\'' +
                ", orderName='" + orderName + '\'' +
                ", procedureNumber=" + procedureNumber +
                ", procedureName='" + procedureName + '\'' +
                ", pieceTime=" + pieceTime +
                '}';
    }

    public String getPieceTime() {
        return pieceTime;
    }

    public void setPieceTime(String pieceTime) {
        this.pieceTime = pieceTime;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public PieceWorkEmp(String employeeNumber, String employeeName, String orderName, Integer procedureNumber, String procedureName) {
        this.employeeNumber = employeeNumber;
        this.employeeName = employeeName;
        this.orderName = orderName;
        this.procedureNumber = procedureNumber;
        this.procedureName = procedureName;
    }

    public String getEmployeeNumber() {
        return employeeNumber;
    }

    public void setEmployeeNumber(String employeeNumber) {
        this.employeeNumber = employeeNumber;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public Integer getProcedureNumber() {
        return procedureNumber;
    }

    public void setProcedureNumber(Integer procedureNumber) {
        this.procedureNumber = procedureNumber;
    }

    public String getProcedureName() {
        return procedureName;
    }

    public void setProcedureName(String procedureName) {
        this.procedureName = procedureName;
    }

    public PieceWorkEmp() {
    }

    public PieceWorkEmp(String employeeNumber, String employeeName, String orderName, Integer procedureNumber, String procedureName, String pieceTime) {
        this.employeeNumber = employeeNumber;
        this.employeeName = employeeName;
        this.orderName = orderName;
        this.procedureNumber = procedureNumber;
        this.procedureName = procedureName;
        this.pieceTime = pieceTime;
    }

    public PieceWorkEmp(String employeeNumber, String employeeName, Integer procedureNumber, String procedureName) {
        this.employeeNumber = employeeNumber;
        this.employeeName = employeeName;
        this.procedureNumber = procedureNumber;
        this.procedureName = procedureName;
    }
}
