package com.example.erp01.model;

public class ClothesDevProcess {

    private Integer id;

    private Integer devId;

    private String clothesDevNumber;

    private String clothesVersionNumber;

    private String orderName;

    private String processImg;

    public ClothesDevProcess() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getClothesDevNumber() {
        return clothesDevNumber;
    }

    public void setClothesDevNumber(String clothesDevNumber) {
        this.clothesDevNumber = clothesDevNumber;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public Integer getDevId() {
        return devId;
    }

    public void setDevId(Integer devId) {
        this.devId = devId;
    }

    public String getProcessImg() {
        return processImg;
    }

    public void setProcessImg(String processImg) {
        this.processImg = processImg;
    }
}
