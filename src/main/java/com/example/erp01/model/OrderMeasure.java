package com.example.erp01.model;

public class OrderMeasure {

    private Integer orderMeasureID;

    private String orderName;

    private String clothesVersionNumber;

    private String itemName;

    private String itemDescription;

    private String sizeName;

    private String itemValue;

    private String orderMeasureImg;

    public OrderMeasure() {
    }

    public Integer getOrderMeasureID() {
        return orderMeasureID;
    }

    public void setOrderMeasureID(Integer orderMeasureID) {
        this.orderMeasureID = orderMeasureID;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public String getItemValue() {
        return itemValue;
    }

    public void setItemValue(String itemValue) {
        this.itemValue = itemValue;
    }

    public String getOrderMeasureImg() {
        return orderMeasureImg;
    }

    public void setOrderMeasureImg(String orderMeasureImg) {
        this.orderMeasureImg = orderMeasureImg;
    }

    public OrderMeasure(String orderName, String clothesVersionNumber, String orderMeasureImg) {
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.orderMeasureImg = orderMeasureImg;
    }
}
