package com.example.erp01.model;

import java.util.Date;

public class ScheduleRecord {

    private Integer id;

    private String orderName;

    private String clothesVersionNumber;

    private String groupName;

    private String employeeNumber;

    private String employeeName;

    private Integer procedureNumber;

    private Integer procedureCode;

    private String procedureName;

    private Float timeCount;

    private Float sam;

    private Float hourCount;

    private Date createTime;

    public ScheduleRecord() {
    }

    public Float getSam() {
        return sam;
    }

    public void setSam(Float sam) {
        this.sam = sam;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmployeeNumber() {
        return employeeNumber;
    }

    public void setEmployeeNumber(String employeeNumber) {
        this.employeeNumber = employeeNumber;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public Integer getProcedureNumber() {
        return procedureNumber;
    }

    public void setProcedureNumber(Integer procedureNumber) {
        this.procedureNumber = procedureNumber;
    }

    public String getProcedureName() {
        return procedureName;
    }

    public void setProcedureName(String procedureName) {
        this.procedureName = procedureName;
    }

    public Float getTimeCount() {
        return timeCount;
    }

    public void setTimeCount(Float timeCount) {
        this.timeCount = timeCount;
    }

    public Float getHourCount() {
        return hourCount;
    }

    public void setHourCount(Float hourCount) {
        this.hourCount = hourCount;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public Integer getProcedureCode() {
        return procedureCode;
    }

    public void setProcedureCode(Integer procedureCode) {
        this.procedureCode = procedureCode;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
