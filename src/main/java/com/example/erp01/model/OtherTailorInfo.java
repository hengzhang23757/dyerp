package com.example.erp01.model;

public class OtherTailorInfo {

    private String orderName;

    private String partName;

    private Integer bedNumber;

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getPartName() {
        return partName;
    }

    public void setPartName(String partName) {
        this.partName = partName;
    }

    public Integer getBedNumber() {
        return bedNumber;
    }

    public void setBedNumber(Integer bedNumber) {
        this.bedNumber = bedNumber;
    }

    public OtherTailorInfo() {
    }

    public OtherTailorInfo(String orderName, String partName, Integer bedNumber) {
        this.orderName = orderName;
        this.partName = partName;
        this.bedNumber = bedNumber;
    }
}
