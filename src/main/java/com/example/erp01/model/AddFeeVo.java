package com.example.erp01.model;

import org.omg.CORBA.PRIVATE_MEMBER;

public class AddFeeVo {

    private Integer fabricID;

    private Integer accessoryID;

    private Float addFee = 0f;

    private String addRemark = "";

    private Integer feeState;

    private Integer feeCount = 0;

    public AddFeeVo() {
    }

    public Integer getFabricID() {
        return fabricID;
    }

    public void setFabricID(Integer fabricID) {
        this.fabricID = fabricID;
    }

    public Integer getAccessoryID() {
        return accessoryID;
    }

    public void setAccessoryID(Integer accessoryID) {
        this.accessoryID = accessoryID;
    }

    public Float getAddFee() {
        return addFee;
    }

    public void setAddFee(Float addFee) {
        this.addFee = addFee;
    }

    public String getAddRemark() {
        return addRemark;
    }

    public void setAddRemark(String addRemark) {
        this.addRemark = addRemark;
    }

    public Integer getFeeState() {
        return feeState;
    }

    public void setFeeState(Integer feeState) {
        this.feeState = feeState;
    }

    public Integer getFeeCount() {
        return feeCount;
    }

    public void setFeeCount(Integer feeCount) {
        this.feeCount = feeCount;
    }
}
