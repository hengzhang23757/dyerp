package com.example.erp01.model;

public class CompletionCount {

    private String orderName;

    private Integer countNumber;

    public CompletionCount() {
    }

    public CompletionCount(String orderName, Integer countNumber) {
        this.orderName = orderName;
        this.countNumber = countNumber;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public Integer getCountNumber() {
        return countNumber;
    }

    public void setCountNumber(Integer countNumber) {
        this.countNumber = countNumber;
    }
}
