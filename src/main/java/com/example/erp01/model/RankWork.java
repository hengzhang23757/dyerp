package com.example.erp01.model;

import java.util.List;

public class RankWork implements Cloneable {

    private String groupName;

    private String employeeName;

    private String employeeNumber;

    private List<GeneralSalary> generalSalaryList;


    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getEmployeeNumber() {
        return employeeNumber;
    }

    public void setEmployeeNumber(String employeeNumber) {
        this.employeeNumber = employeeNumber;
    }

    public List<GeneralSalary> getGeneralSalaryList() {
        return generalSalaryList;
    }

    public void setGeneralSalaryList(List<GeneralSalary> generalSalaryList) {
        this.generalSalaryList = generalSalaryList;
    }

    public RankWork() {
    }
}
