package com.example.erp01.model;

import java.util.Date;

public class DailyAction {

    private Integer id;

    private String orderName;

    private String clothesVersionNumber;

    private String groupName;

    private Date workDate;

    private String workDateStr;

    private Integer finishCount;

    private Integer taskID;

    public DailyAction() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public Date getWorkDate() {
        return workDate;
    }

    public void setWorkDate(Date workDate) {
        this.workDate = workDate;
    }

    public Integer getFinishCount() {
        return finishCount;
    }

    public void setFinishCount(Integer finishCount) {
        this.finishCount = finishCount;
    }

    public String getWorkDateStr() {
        return workDateStr;
    }

    public void setWorkDateStr(String workDateStr) {
        this.workDateStr = workDateStr;
    }

    public Integer getTaskID() {
        return taskID;
    }

    public void setTaskID(Integer taskID) {
        this.taskID = taskID;
    }

    public DailyAction(String orderName, String clothesVersionNumber, String groupName, Date workDate, Integer finishCount) {
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.groupName = groupName;
        this.workDate = workDate;
        this.finishCount = finishCount;
    }
}
