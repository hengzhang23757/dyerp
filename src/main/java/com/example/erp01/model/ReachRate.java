package com.example.erp01.model;

import java.util.Date;

public class ReachRate {

    private String orderName;

    private String clothesVersionNumber;

    private Integer personPlan = 0;

    private Integer groupPlan = 0;

    private Integer personNumber = 0;

    private Date planDate;

    private Integer actualCount = 0;

    private Float completeRate = 0f;

    public ReachRate() {
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public Integer getPersonPlan() {
        return personPlan;
    }

    public void setPersonPlan(Integer personPlan) {
        this.personPlan = personPlan;
    }

    public Integer getGroupPlan() {
        return groupPlan;
    }

    public void setGroupPlan(Integer groupPlan) {
        this.groupPlan = groupPlan;
    }

    public Integer getPersonNumber() {
        return personNumber;
    }

    public void setPersonNumber(Integer personNumber) {
        this.personNumber = personNumber;
    }

    public Date getPlanDate() {
        return planDate;
    }

    public void setPlanDate(Date planDate) {
        this.planDate = planDate;
    }

    public Integer getActualCount() {
        return actualCount;
    }

    public void setActualCount(Integer actualCount) {
        this.actualCount = actualCount;
    }

    public Float getCompleteRate() {
        return completeRate;
    }

    public void setCompleteRate(Float completeRate) {
        this.completeRate = completeRate;
    }

    public ReachRate(String orderName, String clothesVersionNumber, Integer personPlan, Integer groupPlan, Integer personNumber, Date planDate, Integer actualCount, Float completeRate) {
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.personPlan = personPlan;
        this.groupPlan = groupPlan;
        this.personNumber = personNumber;
        this.planDate = planDate;
        this.actualCount = actualCount;
        this.completeRate = completeRate;
    }
}
