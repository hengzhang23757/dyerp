package com.example.erp01.model;

public class ProcedureDto {

    private String orderName;
    private String employeeNumber;
    private String employeeName;
    private String groupName;
    private Integer procedureNumber;
    private String procedureName;
    private Integer orderCount;
    private Integer wellCount;
    private Integer pieceCount;
    private Integer employeeCount;
    private Integer decreaseCount;
    private Integer overCount;
    private float price;
    private float decreaseMoney;

    public ProcedureDto() {
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getEmployeeNumber() {
        return employeeNumber;
    }

    public void setEmployeeNumber(String employeeNumber) {
        this.employeeNumber = employeeNumber;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public Integer getProcedureNumber() {
        return procedureNumber;
    }

    public void setProcedureNumber(Integer procedureNumber) {
        this.procedureNumber = procedureNumber;
    }

    public String getProcedureName() {
        return procedureName;
    }

    public void setProcedureName(String procedureName) {
        this.procedureName = procedureName;
    }

    public Integer getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(Integer orderCount) {
        this.orderCount = orderCount;
    }

    public Integer getWellCount() {
        return wellCount;
    }

    public void setWellCount(Integer wellCount) {
        this.wellCount = wellCount;
    }

    public Integer getPieceCount() {
        return pieceCount;
    }

    public void setPieceCount(Integer pieceCount) {
        this.pieceCount = pieceCount;
    }

    public Integer getEmployeeCount() {
        return employeeCount;
    }

    public void setEmployeeCount(Integer employeeCount) {
        this.employeeCount = employeeCount;
    }

    public Integer getDecreaseCount() {
        return decreaseCount;
    }

    public void setDecreaseCount(Integer decreaseCount) {
        this.decreaseCount = decreaseCount;
    }

    public Integer getOverCount() {
        return overCount;
    }

    public void setOverCount(Integer overCount) {
        this.overCount = overCount;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public float getDecreaseMoney() {
        return decreaseMoney;
    }

    public void setDecreaseMoney(float decreaseMoney) {
        this.decreaseMoney = decreaseMoney;
    }
}
