package com.example.erp01.model;

import java.util.Date;

public class CompletionRate {

    private Date beginDate;

    private Date recordDate;

    private String groupName;

    private String clothesVersionNumber;

    private String orderName;

    private String styleDescription;

    private Integer planCount = 0;

    private Date deadLine;

    private Integer finishSum = 0;

    private Integer todayPlanCount = 0;

    private Integer todayEmpCount = 0;

    private Integer todayActualCount = 0;

    private Date planFinishDate;

    private Float achieveRate;

    public Date getPlanFinishDate() {
        return planFinishDate;
    }

    public void setPlanFinishDate(Date planFinishDate) {
        this.planFinishDate = planFinishDate;
    }

    public Date getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    public Date getRecordDate() {
        return recordDate;
    }

    public void setRecordDate(Date recordDate) {
        this.recordDate = recordDate;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getStyleDescription() {
        return styleDescription;
    }

    public void setStyleDescription(String styleDescription) {
        this.styleDescription = styleDescription;
    }

    public Integer getPlanCount() {
        return planCount;
    }

    public void setPlanCount(Integer planCount) {
        this.planCount = planCount;
    }

    public Date getDeadLine() {
        return deadLine;
    }

    public void setDeadLine(Date deadLine) {
        this.deadLine = deadLine;
    }

    public Integer getFinishSum() {
        return finishSum;
    }

    public void setFinishSum(Integer finishSum) {
        this.finishSum = finishSum;
    }

    public Integer getTodayPlanCount() {
        return todayPlanCount;
    }

    public void setTodayPlanCount(Integer todayPlanCount) {
        this.todayPlanCount = todayPlanCount;
    }

    public Integer getTodayEmpCount() {
        return todayEmpCount;
    }

    public void setTodayEmpCount(Integer todayEmpCount) {
        this.todayEmpCount = todayEmpCount;
    }

    public Integer getTodayActualCount() {
        return todayActualCount;
    }

    public void setTodayActualCount(Integer todayActualCount) {
        this.todayActualCount = todayActualCount;
    }

    public Float getAchieveRate() {
        return achieveRate;
    }

    public void setAchieveRate(Float achieveRate) {
        this.achieveRate = achieveRate;
    }

    public CompletionRate() {
    }

    public CompletionRate(Date beginDate, Date recordDate, String groupName, String clothesVersionNumber, String orderName, String styleDescription, Integer planCount, Date deadLine, Integer finishSum, Integer todayPlanCount, Integer todayEmpCount, Integer todayActualCount, Date planFinishDate, Float achieveRate) {
        this.beginDate = beginDate;
        this.recordDate = recordDate;
        this.groupName = groupName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.orderName = orderName;
        this.styleDescription = styleDescription;
        this.planCount = planCount;
        this.deadLine = deadLine;
        this.finishSum = finishSum;
        this.todayPlanCount = todayPlanCount;
        this.todayEmpCount = todayEmpCount;
        this.todayActualCount = todayActualCount;
        this.planFinishDate = planFinishDate;
        this.achieveRate = achieveRate;
    }

    public CompletionRate(String groupName, String orderName) {
        this.groupName = groupName;
        this.orderName = orderName;
    }
}
