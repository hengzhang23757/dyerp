package com.example.erp01.model;

public class OrderProcedureDetail {

    private Integer orderProcedureDetailID;

    private String orderName;

    private String procedureNumber;

    private Integer procedureCode;

    private String procedureName;

    private String procedureDescription;

    private String guid;

    public OrderProcedureDetail(String orderName, String procedureNumber, Integer procedureCode, String procedureName, String procedureDescription, String guid) {
        this.orderName = orderName;
        this.procedureNumber = procedureNumber;
        this.procedureCode = procedureCode;
        this.procedureName = procedureName;
        this.procedureDescription = procedureDescription;
        this.guid = guid;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getProcedureDescription() {
        return procedureDescription;
    }

    public void setProcedureDescription(String procedureDescription) {
        this.procedureDescription = procedureDescription;
    }

    public OrderProcedureDetail(String orderName, String procedureNumber, Integer procedureCode, String procedureName, String procedureDescription) {
        this.orderName = orderName;
        this.procedureNumber = procedureNumber;
        this.procedureCode = procedureCode;
        this.procedureName = procedureName;
        this.procedureDescription = procedureDescription;
    }

    public OrderProcedureDetail(Integer orderProcedureDetailID, String orderName, String procedureNumber, Integer procedureCode, String procedureName, String procedureDescription) {
        this.orderProcedureDetailID = orderProcedureDetailID;
        this.orderName = orderName;
        this.procedureNumber = procedureNumber;
        this.procedureCode = procedureCode;
        this.procedureName = procedureName;
        this.procedureDescription = procedureDescription;
    }

    public Integer getOrderProcedureDetailID() {
        return orderProcedureDetailID;
    }

    public void setOrderProcedureDetailID(Integer orderProcedureDetailID) {
        this.orderProcedureDetailID = orderProcedureDetailID;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getProcedureNumber() {
        return procedureNumber;
    }

    public void setProcedureNumber(String procedureNumber) {
        this.procedureNumber = procedureNumber;
    }

    public Integer getProcedureCode() {
        return procedureCode;
    }

    public void setProcedureCode(Integer procedureCode) {
        this.procedureCode = procedureCode;
    }

    public String getProcedureName() {
        return procedureName;
    }

    public void setProcedureName(String procedureName) {
        this.procedureName = procedureName;
    }

//    public OrderProcedureDetail(Integer orderProcedureDetailID, String orderName, String procedureNumber, Integer procedureCode, String procedureName) {
//        this.orderProcedureDetailID = orderProcedureDetailID;
//        this.orderName = orderName;
//        this.procedureNumber = procedureNumber;
//        this.procedureCode = procedureCode;
//        this.procedureName = procedureName;
//    }
//
//    public OrderProcedureDetail(String orderName, String procedureNumber, Integer procedureCode, String procedureName) {
//        this.orderName = orderName;
//        this.procedureNumber = procedureNumber;
//        this.procedureCode = procedureCode;
//        this.procedureName = procedureName;
//    }

    public OrderProcedureDetail() {
    }
}
