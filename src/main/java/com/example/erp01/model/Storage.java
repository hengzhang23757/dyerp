package com.example.erp01.model;

import java.util.Date;

public class Storage {

    private Integer storageID;

    private String storehouseLocation;

    private String orderName;

    private Integer bedNumber;

    private String colorName;

    private String sizeName;

    private Integer layerCount;

    private Integer packageNumber;

    private String partName;

    private String tailorQcode;

    private Integer storageState;

    private Integer storehouseCount;

    private Date createTime;

    private String inStoreTime;

    public String getInStoreTime() {
        return inStoreTime;
    }

    public void setInStoreTime(String inStoreTime) {
        this.inStoreTime = inStoreTime;
    }

    public Storage(String storehouseLocation, String orderName, Integer bedNumber, String colorName, String sizeName, Integer layerCount, Integer packageNumber, String partName, String tailorQcode) {
        this.storehouseLocation = storehouseLocation;
        this.orderName = orderName;
        this.bedNumber = bedNumber;
        this.colorName = colorName;
        this.sizeName = sizeName;
        this.layerCount = layerCount;
        this.packageNumber = packageNumber;
        this.partName = partName;
        this.tailorQcode = tailorQcode;
    }

    public Storage(Integer storageID, String storehouseLocation, String orderName, Integer bedNumber, String colorName, String sizeName, Integer layerCount, Integer packageNumber, String partName, String tailorQcode) {
        this.storageID = storageID;
        this.storehouseLocation = storehouseLocation;
        this.orderName = orderName;
        this.bedNumber = bedNumber;
        this.colorName = colorName;
        this.sizeName = sizeName;
        this.layerCount = layerCount;
        this.packageNumber = packageNumber;
        this.partName = partName;
        this.tailorQcode = tailorQcode;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public Integer getBedNumber() {
        return bedNumber;
    }

    public void setBedNumber(Integer bedNumber) {
        this.bedNumber = bedNumber;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public Integer getLayerCount() {
        return layerCount;
    }

    public void setLayerCount(Integer layerCount) {
        this.layerCount = layerCount;
    }

    public Integer getPackageNumber() {
        return packageNumber;
    }

    public void setPackageNumber(Integer packageNumber) {
        this.packageNumber = packageNumber;
    }

    public String getPartName() {
        return partName;
    }

    public void setPartName(String partName) {
        this.partName = partName;
    }

    public String getStorehouseLocation() {
        return storehouseLocation;
    }

    public void setStorehouseLocation(String storehouseLocation) {
        this.storehouseLocation = storehouseLocation;
    }

    public Integer getStorehouseCount() {
        return storehouseCount;
    }

    public void setStorehouseCount(Integer storehouseCount) {
        this.storehouseCount = storehouseCount;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Storage(String storehouseLocation, Integer storageState, Integer storehouseCount) {
        this.storehouseLocation = storehouseLocation;
        this.storageState = storageState;
        this.storehouseCount = storehouseCount;
    }

    public Storage() {
    }

    public Storage(Integer storageID, String storehouseLocation, String tailorQcode, Integer storageState) {
        this.storageID = storageID;
        this.storehouseLocation = storehouseLocation;
        this.tailorQcode = tailorQcode;
        this.storageState = storageState;
    }

    public Storage(String storehouseLocation, Integer storageState) {
        this.storehouseLocation = storehouseLocation;
        this.storageState = storageState;
    }

    public Integer getStorageState() {
        return storageState;
    }

    public void setStorageState(Integer storageState) {
        this.storageState = storageState;
    }

    public Storage(String storehouseLocation, String tailorQcode) {
        this.storehouseLocation = storehouseLocation;
        this.tailorQcode = tailorQcode;
    }

    public Integer getStorageID() {
        return storageID;
    }

    public void setStorageID(Integer storageID) {
        this.storageID = storageID;
    }

    public String getTailorQcode() {
        return tailorQcode;
    }

    public void setTailorQcode(String tailorQcode) {
        this.tailorQcode = tailorQcode;
    }
}
