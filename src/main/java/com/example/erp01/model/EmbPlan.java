package com.example.erp01.model;

import java.util.Date;

public class EmbPlan {

    private Integer embPlanID;

    private Date beginDate;

    private Date endDate;

    private String orderName;

    private String clothesVersionNumber;

    private String colorName;

    private String sizeName;

    private String groupName;

    private Integer planCount = 0;

    private Float driftPercent = 0.1f;

    private Integer actCount = 0;

    private Float achievePercent = 0f;

    private String beginDateString;

    private String endDateString;

    public EmbPlan() {
    }

    public Integer getEmbPlanID() {
        return embPlanID;
    }

    public void setEmbPlanID(Integer embPlanID) {
        this.embPlanID = embPlanID;
    }

    public Date getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public Integer getPlanCount() {
        return planCount;
    }

    public void setPlanCount(Integer planCount) {
        this.planCount = planCount;
    }

    public float getDriftPercent() {
        return driftPercent;
    }

    public void setDriftPercent(float driftPercent) {
        this.driftPercent = driftPercent;
    }

    public Integer getActCount() {
        return actCount;
    }

    public void setActCount(Integer actCount) {
        this.actCount = actCount;
    }

    public Float getAchievePercent() {
        return achievePercent;
    }

    public void setAchievePercent(Float achievePercent) {
        this.achievePercent = achievePercent;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public void setDriftPercent(Float driftPercent) {
        this.driftPercent = driftPercent;
    }

    public String getBeginDateString() {
        return beginDateString;
    }

    public void setBeginDateString(String beginDateString) {
        this.beginDateString = beginDateString;
    }

    public String getEndDateString() {
        return endDateString;
    }

    public void setEndDateString(String endDateString) {
        this.endDateString = endDateString;
    }
}
