package com.example.erp01.model;

import java.util.Date;

public class ClothesVersionProcess {

    private Integer clothesVersionProcessID;

    private String clothesVersionNumber;

    private String orderName;

    private String styleDescription;

    private Date beginDate;

    private String processType;

    private String processDescription;

    private Date processDate;

    public Integer getClothesVersionProcessID() {
        return clothesVersionProcessID;
    }

    public void setClothesVersionProcessID(Integer clothesVersionProcessID) {
        this.clothesVersionProcessID = clothesVersionProcessID;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getStyleDescription() {
        return styleDescription;
    }

    public void setStyleDescription(String styleDescription) {
        this.styleDescription = styleDescription;
    }

    public Date getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    public String getProcessType() {
        return processType;
    }

    public void setProcessType(String processType) {
        this.processType = processType;
    }

    public String getProcessDescription() {
        return processDescription;
    }

    public void setProcessDescription(String processDescription) {
        this.processDescription = processDescription;
    }

    public Date getProcessDate() {
        return processDate;
    }

    public void setProcessDate(Date processDate) {
        this.processDate = processDate;
    }

    public ClothesVersionProcess(String clothesVersionNumber, String orderName, String styleDescription, Date beginDate, String processType, String processDescription, Date processDate) {
        this.clothesVersionNumber = clothesVersionNumber;
        this.orderName = orderName;
        this.styleDescription = styleDescription;
        this.beginDate = beginDate;
        this.processType = processType;
        this.processDescription = processDescription;
        this.processDate = processDate;
    }

    public ClothesVersionProcess(Integer clothesVersionProcessID, String clothesVersionNumber, String orderName, String styleDescription, Date beginDate, String processType, String processDescription, Date processDate) {
        this.clothesVersionProcessID = clothesVersionProcessID;
        this.clothesVersionNumber = clothesVersionNumber;
        this.orderName = orderName;
        this.styleDescription = styleDescription;
        this.beginDate = beginDate;
        this.processType = processType;
        this.processDescription = processDescription;
        this.processDate = processDate;
    }

    public ClothesVersionProcess() {
    }
}
