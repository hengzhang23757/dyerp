package com.example.erp01.model;

public class ClothesDevAccessory {

    private Integer id;

    private Integer devId;

    private String clothesDevNumber;

    private String clothesVersionNumber;

    private String orderName;

    private String accessoryNumber;

    private String devAccessoryName;

    private String accessoryName;  //辅料名称

    private String specification = "未输";//辅料规格

    private String accessoryUnit;//辅料单位

    private String accessoryColor = "未输";//辅料颜色

    private Float pieceUsage = 0f;//单件用量

    private String sizeName;//尺码

    private String colorName;//颜色

    private String supplier;

    private Float price = 0f;

    private String accessoryType;

    private String accessoryKey;

    private Integer colorChild;

    private Integer sizeChild;

    private String remark;

    public ClothesDevAccessory() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDevId() {
        return devId;
    }

    public void setDevId(Integer devId) {
        this.devId = devId;
    }

    public String getClothesDevNumber() {
        return clothesDevNumber;
    }

    public void setClothesDevNumber(String clothesDevNumber) {
        this.clothesDevNumber = clothesDevNumber;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getAccessoryNumber() {
        return accessoryNumber;
    }

    public void setAccessoryNumber(String accessoryNumber) {
        this.accessoryNumber = accessoryNumber;
    }

    public String getAccessoryName() {
        return accessoryName;
    }

    public void setAccessoryName(String accessoryName) {
        this.accessoryName = accessoryName;
    }

    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }

    public String getAccessoryUnit() {
        return accessoryUnit;
    }

    public void setAccessoryUnit(String accessoryUnit) {
        this.accessoryUnit = accessoryUnit;
    }

    public String getAccessoryColor() {
        return accessoryColor;
    }

    public void setAccessoryColor(String accessoryColor) {
        this.accessoryColor = accessoryColor;
    }

    public Float getPieceUsage() {
        return pieceUsage;
    }

    public void setPieceUsage(Float pieceUsage) {
        this.pieceUsage = pieceUsage;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public String getAccessoryType() {
        return accessoryType;
    }

    public void setAccessoryType(String accessoryType) {
        this.accessoryType = accessoryType;
    }

    public String getAccessoryKey() {
        return accessoryKey;
    }

    public void setAccessoryKey(String accessoryKey) {
        this.accessoryKey = accessoryKey;
    }

    public Integer getColorChild() {
        return colorChild;
    }

    public void setColorChild(Integer colorChild) {
        this.colorChild = colorChild;
    }

    public Integer getSizeChild() {
        return sizeChild;
    }

    public void setSizeChild(Integer sizeChild) {
        this.sizeChild = sizeChild;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getDevAccessoryName() {
        return devAccessoryName;
    }

    public void setDevAccessoryName(String devAccessoryName) {
        this.devAccessoryName = devAccessoryName;
    }
}
