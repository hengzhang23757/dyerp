package com.example.erp01.model;

public class MiniProgress {

    private String orderName;

    private Integer procedureNumber;

    private String procedureName;

    private Integer orderCount;

    private Integer cutCount;

    private Integer productionCount;

    private Integer differenceCount;

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public Integer getProcedureNumber() {
        return procedureNumber;
    }

    public void setProcedureNumber(Integer procedureNumber) {
        this.procedureNumber = procedureNumber;
    }

    public String getProcedureName() {
        return procedureName;
    }

    public void setProcedureName(String procedureName) {
        this.procedureName = procedureName;
    }

    public Integer getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(Integer orderCount) {
        this.orderCount = orderCount;
    }

    public Integer getCutCount() {
        return cutCount;
    }

    public void setCutCount(Integer cutCount) {
        this.cutCount = cutCount;
    }

    public Integer getProductionCount() {
        return productionCount;
    }

    public void setProductionCount(Integer productionCount) {
        this.productionCount = productionCount;
    }

    public Integer getDifferenceCount() {
        return differenceCount;
    }

    public void setDifferenceCount(Integer differenceCount) {
        this.differenceCount = differenceCount;
    }

    public MiniProgress() {
    }

    public MiniProgress(String orderName, Integer procedureNumber, String procedureName, Integer orderCount, Integer cutCount, Integer productionCount, Integer differenceCount) {
        this.orderName = orderName;
        this.procedureNumber = procedureNumber;
        this.procedureName = procedureName;
        this.orderCount = orderCount;
        this.cutCount = cutCount;
        this.productionCount = productionCount;
        this.differenceCount = differenceCount;
    }
}
