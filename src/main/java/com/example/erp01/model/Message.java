package com.example.erp01.model;

import java.util.Date;

public class Message {

    private Integer id;

    private String messageType;

    private String createUser;

    private String receiveUser;

    private String orderName;

    private String clothesVersionNumber;

    private String title;

    private String messageBodyOne;

    private String messageBodyTwo;

    private String messageBodyThree;

    private String messageRemark;

    private String readType;

    private String imgUrl;

    private Date createTime;

    private Date updateTime;

    private Integer messageCount;

    private String partName;

    private Integer bedNumber;

    private Float planConsumption = 0f;

    private Float actConsumption = 0f;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getReceiveUser() {
        return receiveUser;
    }

    public void setReceiveUser(String receiveUser) {
        this.receiveUser = receiveUser;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessageBodyOne() {
        return messageBodyOne;
    }

    public void setMessageBodyOne(String messageBodyOne) {
        this.messageBodyOne = messageBodyOne;
    }

    public String getMessageBodyTwo() {
        return messageBodyTwo;
    }

    public void setMessageBodyTwo(String messageBodyTwo) {
        this.messageBodyTwo = messageBodyTwo;
    }

    public String getMessageBodyThree() {
        return messageBodyThree;
    }

    public void setMessageBodyThree(String messageBodyThree) {
        this.messageBodyThree = messageBodyThree;
    }

    public String getMessageRemark() {
        return messageRemark;
    }

    public void setMessageRemark(String messageRemark) {
        this.messageRemark = messageRemark;
    }

    public String getReadType() {
        return readType;
    }

    public void setReadType(String readType) {
        this.readType = readType;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public Integer getMessageCount() {
        return messageCount;
    }

    public void setMessageCount(Integer messageCount) {
        this.messageCount = messageCount;
    }

    public String getPartName() {
        return partName;
    }

    public void setPartName(String partName) {
        this.partName = partName;
    }

    public Integer getBedNumber() {
        return bedNumber;
    }

    public void setBedNumber(Integer bedNumber) {
        this.bedNumber = bedNumber;
    }

    public Float getPlanConsumption() {
        return planConsumption;
    }

    public void setPlanConsumption(Float planConsumption) {
        this.planConsumption = planConsumption;
    }

    public Float getActConsumption() {
        return actConsumption;
    }

    public void setActConsumption(Float actConsumption) {
        this.actConsumption = actConsumption;
    }

    public Message() {
    }

    @Override
    public String toString() {
        return "Message{" +
                "id=" + id +
                ", messageType='" + messageType + '\'' +
                ", createUser='" + createUser + '\'' +
                ", receiveUser='" + receiveUser + '\'' +
                ", orderName='" + orderName + '\'' +
                ", clothesVersionNumber='" + clothesVersionNumber + '\'' +
                ", title='" + title + '\'' +
                ", messageBodyOne='" + messageBodyOne + '\'' +
                ", messageBodyTwo='" + messageBodyTwo + '\'' +
                ", messageBodyThree='" + messageBodyThree + '\'' +
                ", messageRemark='" + messageRemark + '\'' +
                ", readType='" + readType + '\'' +
                ", imgUrl='" + imgUrl + '\'' +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                ", messageCount=" + messageCount +
                ", partName='" + partName + '\'' +
                ", bedNumber=" + bedNumber +
                ", planConsumption=" + planConsumption +
                ", actConsumption=" + actConsumption +
                '}';
    }
}
