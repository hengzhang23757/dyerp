package com.example.erp01.model;

public class RoomState {

    private String roomNumber;

    private Integer empCount = 0;

    private Integer capacity = 0;

    public RoomState() {
    }

    public RoomState(String roomNumber, Integer empCount, Integer capacity) {
        this.roomNumber = roomNumber;
        this.empCount = empCount;
        this.capacity = capacity;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    public String getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(String roomNumber) {
        this.roomNumber = roomNumber;
    }

    public Integer getEmpCount() {
        return empCount;
    }

    public void setEmpCount(Integer empCount) {
        this.empCount = empCount;
    }
}
