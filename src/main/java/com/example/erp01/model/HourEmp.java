package com.example.erp01.model;

import java.sql.Timestamp;
import java.util.Date;

public class HourEmp {

    private Integer hourEmpID;

    private String employeeNumber;

    private String employeeName;

    private String groupName = "通用";

    private String orderName;

    private String clothesVersionNumber;

    private Float layerCount;

    private Integer procedureNumber;

    private String procedureName;

    private String userName;

    private Date hourEmpDate;

    private Timestamp createTime;

    private Timestamp updateTime;

    public Date getHourEmpDate() {
        return hourEmpDate;
    }

    public void setHourEmpDate(Date hourEmpDate) {
        this.hourEmpDate = hourEmpDate;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public Integer getHourEmpID() {
        return hourEmpID;
    }

    public void setHourEmpID(Integer hourEmpID) {
        this.hourEmpID = hourEmpID;
    }

    public String getEmployeeNumber() {
        return employeeNumber;
    }

    public void setEmployeeNumber(String employeeNumber) {
        this.employeeNumber = employeeNumber;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public Float getLayerCount() {
        return layerCount;
    }

    public void setLayerCount(Float layerCount) {
        this.layerCount = layerCount;
    }

    public Integer getProcedureNumber() {
        return procedureNumber;
    }

    public void setProcedureNumber(Integer procedureNumber) {
        this.procedureNumber = procedureNumber;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }

    public String getProcedureName() {
        return procedureName;
    }

    public void setProcedureName(String procedureName) {
        this.procedureName = procedureName;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public HourEmp() {
    }

    public HourEmp(String employeeNumber, String employeeName, String groupName, String orderName, String clothesVersionNumber, Float layerCount, Integer procedureNumber, String procedureName, String userName, Timestamp createTime) {
        this.employeeNumber = employeeNumber;
        this.employeeName = employeeName;
        this.groupName = groupName;
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.layerCount = layerCount;
        this.procedureNumber = procedureNumber;
        this.procedureName = procedureName;
        this.userName = userName;
        this.createTime = createTime;
    }
}
