package com.example.erp01.model;

public class ClothesDevAccessoryNumber {

    private Integer id;

    private String accessoryNumberName;

    private String accessoryNumberCode;

    public ClothesDevAccessoryNumber() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAccessoryNumberName() {
        return accessoryNumberName;
    }

    public void setAccessoryNumberName(String accessoryNumberName) {
        this.accessoryNumberName = accessoryNumberName;
    }

    public String getAccessoryNumberCode() {
        return accessoryNumberCode;
    }

    public void setAccessoryNumberCode(String accessoryNumberCode) {
        this.accessoryNumberCode = accessoryNumberCode;
    }
}
