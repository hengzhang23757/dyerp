package com.example.erp01.model;

public class CheckDetail {

    private String employeeNumber;

    private String employeeName;

    private String signDate;

    private String in1 = "未签到";

    private String out1 = "未签到";

    private String in2 = "未签到";

    private String out2 = "未签到";

    private String in3 = "未签到";

    private String out3 = "未签到";
    //理论天数
    private Integer planDays;
    //周末天数
    private Integer sunDays;
    //假期天数
    private Integer hdDays;
    //有刷卡天数
    private Integer dutyDays;
    //出勤天数
    private Integer workDays;
    //缺勤天数
    private Integer absentDays;
    //请假天数
    private Integer leaveDays;
    //实际工时
    private Integer factHours;
    //基本工时
    private Integer basicHours;
    //中班工时
    private Integer midHours;
    //夜班工时
    private Integer nsHours;
    //加班
    private Integer otHours;
    //周日加班
    private Integer sunHours;
    //假期加班
    private Integer hdHours;
    //夜班次数
    private Integer nsCount;
    //中班次数
    private Integer midCount;
    //加班次数
    private Integer otCount;
    //事假时数
    private Integer l0Hours;
    //病假时数
    private Integer l1Hours;
    //班次时数
    private Integer shiftHours;
    //迟到分钟
    private Integer lateMinutes1;
    //早退分钟
    private Integer leaveMinutes1;
    //缺席小时
    private Integer absentHours1;
    //结论
    private String notes;

    public String getEmployeeNumber() {
        return employeeNumber;
    }

    public void setEmployeeNumber(String employeeNumber) {
        this.employeeNumber = employeeNumber;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getSignDate() {
        return signDate;
    }

    public void setSignDate(String signDate) {
        this.signDate = signDate;
    }

    public String getIn1() {
        return in1;
    }

    public void setIn1(String in1) {
        this.in1 = in1;
    }

    public String getOut1() {
        return out1;
    }

    public void setOut1(String out1) {
        this.out1 = out1;
    }

    public String getIn2() {
        return in2;
    }

    public void setIn2(String in2) {
        this.in2 = in2;
    }

    public String getOut2() {
        return out2;
    }

    public void setOut2(String out2) {
        this.out2 = out2;
    }

    public String getIn3() {
        return in3;
    }

    public void setIn3(String in3) {
        this.in3 = in3;
    }

    public String getOut3() {
        return out3;
    }

    public void setOut3(String out3) {
        this.out3 = out3;
    }

    public Integer getPlanDays() {
        return planDays;
    }

    public void setPlanDays(Integer planDays) {
        this.planDays = planDays;
    }

    public Integer getSunDays() {
        return sunDays;
    }

    public void setSunDays(Integer sunDays) {
        this.sunDays = sunDays;
    }

    public Integer getHdDays() {
        return hdDays;
    }

    public void setHdDays(Integer hdDays) {
        this.hdDays = hdDays;
    }

    public Integer getDutyDays() {
        return dutyDays;
    }

    public void setDutyDays(Integer dutyDays) {
        this.dutyDays = dutyDays;
    }

    public Integer getWorkDays() {
        return workDays;
    }

    public void setWorkDays(Integer workDays) {
        this.workDays = workDays;
    }

    public Integer getAbsentDays() {
        return absentDays;
    }

    public void setAbsentDays(Integer absentDays) {
        this.absentDays = absentDays;
    }

    public Integer getLeaveDays() {
        return leaveDays;
    }

    public void setLeaveDays(Integer leaveDays) {
        this.leaveDays = leaveDays;
    }

    public Integer getFactHours() {
        return factHours;
    }

    public void setFactHours(Integer factHours) {
        this.factHours = factHours;
    }

    public Integer getBasicHours() {
        return basicHours;
    }

    public void setBasicHours(Integer basicHours) {
        this.basicHours = basicHours;
    }

    public Integer getMidHours() {
        return midHours;
    }

    public void setMidHours(Integer midHours) {
        this.midHours = midHours;
    }

    public Integer getNsHours() {
        return nsHours;
    }

    public void setNsHours(Integer nsHours) {
        this.nsHours = nsHours;
    }

    public Integer getOtHours() {
        return otHours;
    }

    public void setOtHours(Integer otHours) {
        this.otHours = otHours;
    }

    public Integer getSunHours() {
        return sunHours;
    }

    public void setSunHours(Integer sunHours) {
        this.sunHours = sunHours;
    }

    public Integer getHdHours() {
        return hdHours;
    }

    public void setHdHours(Integer hdHours) {
        this.hdHours = hdHours;
    }

    public Integer getNsCount() {
        return nsCount;
    }

    public void setNsCount(Integer nsCount) {
        this.nsCount = nsCount;
    }

    public Integer getMidCount() {
        return midCount;
    }

    public void setMidCount(Integer midCount) {
        this.midCount = midCount;
    }

    public Integer getOtCount() {
        return otCount;
    }

    public void setOtCount(Integer otCount) {
        this.otCount = otCount;
    }

    public Integer getL0Hours() {
        return l0Hours;
    }

    public void setL0Hours(Integer l0Hours) {
        this.l0Hours = l0Hours;
    }

    public Integer getL1Hours() {
        return l1Hours;
    }

    public void setL1Hours(Integer l1Hours) {
        this.l1Hours = l1Hours;
    }

    public Integer getShiftHours() {
        return shiftHours;
    }

    public void setShiftHours(Integer shiftHours) {
        this.shiftHours = shiftHours;
    }

    public Integer getLateMinutes1() {
        return lateMinutes1;
    }

    public void setLateMinutes1(Integer lateMinutes1) {
        this.lateMinutes1 = lateMinutes1;
    }

    public Integer getLeaveMinutes1() {
        return leaveMinutes1;
    }

    public void setLeaveMinutes1(Integer leaveMinutes1) {
        this.leaveMinutes1 = leaveMinutes1;
    }

    public Integer getAbsentHours1() {
        return absentHours1;
    }

    public void setAbsentHours1(Integer absentHours1) {
        this.absentHours1 = absentHours1;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public CheckDetail() {
    }
}
