package com.example.erp01.model;

public class EmbStorageQuery {

    private String orderName;

    private String colorName;

    private String sizeName;

    private String embStoreLocation;

    private Integer embStorageCount;

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public String getEmbStoreLocation() {
        return embStoreLocation;
    }

    public void setEmbStoreLocation(String embStoreLocation) {
        this.embStoreLocation = embStoreLocation;
    }

    public Integer getEmbStorageCount() {
        return embStorageCount;
    }

    public void setEmbStorageCount(Integer embStorageCount) {
        this.embStorageCount = embStorageCount;
    }

    public EmbStorageQuery() {
    }

    public EmbStorageQuery(String orderName, String colorName, String sizeName, String embStoreLocation, Integer embStorageCount) {
        this.orderName = orderName;
        this.colorName = colorName;
        this.sizeName = sizeName;
        this.embStoreLocation = embStoreLocation;
        this.embStorageCount = embStorageCount;
    }
}
