package com.example.erp01.model;

import java.util.Date;

public class ManufactureFabric {

    private Integer fabricID;
    private String orderName;   //订单
    private String clothesVersionNumber;  // 版单
    private String season;
    private String customerName;
    private String fabricNumber;
    private String fabricName;  //面料名称
    private String fabricWidth;  //布封
    private String fabricWeight; //克重
    private String unit;//单位
    private String fabricDeflation;    //面料直缩
    private String fabricShrink;     //面料横缩
    private String clothesDeflation;   //成衣直缩
    private String clothesShrink;     //成衣横缩
    private String partName;//部位
    private Float fabricLoss = 0f;
    private Float orderPieceUsage = 0f;
    private Float pieceUsage = 0f;//单件用量
    private String colorName;//订单颜色
    private String isHit;//是否撞色
    private String fabricColor;//面料颜色
    private String fabricColorNumber;
    private String supplier;
    private Float price = 0f;
    private Float orderPrice = 0f;
    private Integer orderCount = 0;
    private Float fabricCount = 0f;
    private Float fabricActCount = 0f;
    private Float fabricAdd = 0f;
    private Float sumMoney = 0f;
    private Float orderMoney = 0f;
    private String orderState = "未下单";
    private String checkState;
    private String checkNumber;
    private Float fabricReturn = 0f;
    private Float returnSumMoney = 0f;
    private String remark = "";
    private Float fabricReturnCheckCount = 0f;
    private Float fabricReturnUnCheck = 0f;
    private String tax = "";
    private Float addFee = 0f;
    private String addRemark = "";
    private Integer feeState;
    private String preCheck;
    private String preCheckEmp;
    private Date preCheckTime;

    private String checkEmp;
    private Date checkTime;

    public ManufactureFabric() {
    }

    public String getPreCheckEmp() {
        return preCheckEmp;
    }

    public void setPreCheckEmp(String preCheckEmp) {
        this.preCheckEmp = preCheckEmp;
    }

    public Date getPreCheckTime() {
        return preCheckTime;
    }

    public void setPreCheckTime(Date preCheckTime) {
        this.preCheckTime = preCheckTime;
    }

    public String getCheckEmp() {
        return checkEmp;
    }

    public void setCheckEmp(String checkEmp) {
        this.checkEmp = checkEmp;
    }

    public Date getCheckTime() {
        return checkTime;
    }

    public void setCheckTime(Date checkTime) {
        this.checkTime = checkTime;
    }

    public Float getFabricCount() {
        return fabricCount;
    }

    public void setFabricCount(Float fabricCount) {
        this.fabricCount = fabricCount;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Integer getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(Integer orderCount) {
        this.orderCount = orderCount;
    }

    public Float getFabricAdd() {
        return fabricAdd;
    }

    public void setFabricAdd(Float fabricAdd) {
        this.fabricAdd = fabricAdd;
    }

    public Float getSumMoney() {
        return sumMoney;
    }

    public void setSumMoney(Float sumMoney) {
        this.sumMoney = sumMoney;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getIsHit() {
        return isHit;
    }

    public void setIsHit(String isHit) {
        this.isHit = isHit;
    }

    public String getFabricColor() {
        return fabricColor;
    }

    public void setFabricColor(String fabricColor) {
        this.fabricColor = fabricColor;
    }

    public Integer getFabricID() {
        return fabricID;
    }

    public void setFabricID(Integer fabricID) {
        this.fabricID = fabricID;
    }

    public String getFabricName() {
        return fabricName;
    }

    public void setFabricName(String fabricName) {
        this.fabricName = fabricName;
    }

    public String getFabricWidth() {
        return fabricWidth;
    }

    public void setFabricWidth(String fabricWidth) {
        this.fabricWidth = fabricWidth;
    }

    public String getFabricWeight() {
        return fabricWeight;
    }

    public void setFabricWeight(String fabricWeight) {
        this.fabricWeight = fabricWeight;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getFabricDeflation() {
        return fabricDeflation;
    }

    public void setFabricDeflation(String fabricDeflation) {
        this.fabricDeflation = fabricDeflation;
    }

    public String getFabricShrink() {
        return fabricShrink;
    }

    public void setFabricShrink(String fabricShrink) {
        this.fabricShrink = fabricShrink;
    }

    public String getClothesDeflation() {
        return clothesDeflation;
    }

    public void setClothesDeflation(String clothesDeflation) {
        this.clothesDeflation = clothesDeflation;
    }

    public String getClothesShrink() {
        return clothesShrink;
    }

    public void setClothesShrink(String clothesShrink) {
        this.clothesShrink = clothesShrink;
    }

    public String getPartName() {
        return partName;
    }

    public void setPartName(String partName) {
        this.partName = partName;
    }

    public Float getFabricLoss() {
        return fabricLoss;
    }

    public void setFabricLoss(Float fabricLoss) {
        this.fabricLoss = fabricLoss;
    }

    public Float getPieceUsage() {
        return pieceUsage;
    }

    public void setPieceUsage(Float pieceUsage) {
        this.pieceUsage = pieceUsage;
    }

    public String getFabricNumber() {
        return fabricNumber;
    }

    public void setFabricNumber(String fabricNumber) {
        this.fabricNumber = fabricNumber;
    }

    public String getFabricColorNumber() {
        return fabricColorNumber;
    }

    public void setFabricColorNumber(String fabricColorNumber) {
        this.fabricColorNumber = fabricColorNumber;
    }

    public Float getOrderPieceUsage() {
        return orderPieceUsage;
    }

    public void setOrderPieceUsage(Float orderPieceUsage) {
        this.orderPieceUsage = orderPieceUsage;
    }

    public String getOrderState() {
        return orderState;
    }

    public void setOrderState(String orderState) {
        this.orderState = orderState;
    }

    public String getCheckState() {
        return checkState;
    }

    public void setCheckState(String checkState) {
        this.checkState = checkState;
    }


    public Float getFabricActCount() {
        return fabricActCount;
    }

    public void setFabricActCount(Float fabricActCount) {
        this.fabricActCount = fabricActCount;
    }

    public String getCheckNumber() {
        return checkNumber;
    }

    public void setCheckNumber(String checkNumber) {
        this.checkNumber = checkNumber;
    }

    public Float getFabricReturn() {
        return fabricReturn;
    }

    public void setFabricReturn(Float fabricReturn) {
        this.fabricReturn = fabricReturn;
    }


    public Float getReturnSumMoney() {
        return returnSumMoney;
    }

    public void setReturnSumMoney(Float returnSumMoney) {
        this.returnSumMoney = returnSumMoney;
    }


    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Float getFabricReturnCheckCount() {
        return fabricReturnCheckCount;
    }

    public void setFabricReturnCheckCount(Float fabricReturnCheckCount) {
        this.fabricReturnCheckCount = fabricReturnCheckCount;
    }

    public Float getFabricReturnUnCheck() {
        return fabricReturnUnCheck;
    }

    public void setFabricReturnUnCheck(Float fabricReturnUnCheck) {
        this.fabricReturnUnCheck = fabricReturnUnCheck;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }


    public Float getAddFee() {
        return addFee;
    }

    public void setAddFee(Float addFee) {
        this.addFee = addFee;
    }

    public String getAddRemark() {
        return addRemark;
    }

    public void setAddRemark(String addRemark) {
        this.addRemark = addRemark;
    }

    public Integer getFeeState() {
        return feeState;
    }

    public void setFeeState(Integer feeState) {
        this.feeState = feeState;
    }

    public String getPreCheck() {
        return preCheck;
    }

    public void setPreCheck(String preCheck) {
        this.preCheck = preCheck;
    }

    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public Float getOrderPrice() {
        return orderPrice;
    }

    public void setOrderPrice(Float orderPrice) {
        this.orderPrice = orderPrice;
    }

    public Float getOrderMoney() {
        return orderMoney;
    }

    public void setOrderMoney(Float orderMoney) {
        this.orderMoney = orderMoney;
    }

    public ManufactureFabric(String orderName, String clothesVersionNumber, String fabricNumber, String fabricName, String fabricWidth, String fabricWeight, String unit, String partName, Float pieceUsage, String colorName, String fabricColor, String fabricColorNumber, String supplier) {
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.fabricNumber = fabricNumber;
        this.fabricName = fabricName;
        this.fabricWidth = fabricWidth;
        this.fabricWeight = fabricWeight;
        this.unit = unit;
        this.partName = partName;
        this.pieceUsage = pieceUsage;
        this.colorName = colorName;
        this.fabricColor = fabricColor;
        this.fabricColorNumber = fabricColorNumber;
        this.supplier = supplier;
    }

}
