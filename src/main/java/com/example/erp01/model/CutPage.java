package com.example.erp01.model;

import java.util.Date;

public class CutPage {

    private String orderName;

    private String clothesVersionNumber;

    private Integer orderCount = 0;

    private Integer leastCount = 0;

    private Date beginDate;

    private Date updateDate;

    private String colorName;

    private Float balancePercent = 0f;

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public Integer getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(Integer orderCount) {
        this.orderCount = orderCount;
    }

    public Integer getLeastCount() {
        return leastCount;
    }

    public void setLeastCount(Integer leastCount) {
        this.leastCount = leastCount;
    }

    public Date getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public Float getBalancePercent() {
        return balancePercent;
    }

    public void setBalancePercent(Float balancePercent) {
        this.balancePercent = balancePercent;
    }

    public CutPage() {
    }
}
