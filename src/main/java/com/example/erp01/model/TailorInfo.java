package com.example.erp01.model;

public class TailorInfo {

    private String orderName;

    private Integer bedNumber;

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public Integer getBedNumber() {
        return bedNumber;
    }

    public void setBedNumber(Integer bedNumber) {
        this.bedNumber = bedNumber;
    }

    public TailorInfo(String orderName, Integer bedNumber) {
        this.orderName = orderName;
        this.bedNumber = bedNumber;
    }

    public TailorInfo() {
    }
}
