package com.example.erp01.model;

import java.util.Date;

public class CutBedColor {

    private String orderName;

    private Integer bedNumber;

    private Date cutTime;

    private String colorName;

    private String sizeName;

    private Integer pieceCount;

    private Integer packageCount;

    private String groupName;

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public Integer getBedNumber() {
        return bedNumber;
    }

    public void setBedNumber(Integer bedNumber) {
        this.bedNumber = bedNumber;
    }

    public Date getCutTime() {
        return cutTime;
    }

    public void setCutTime(Date cutTime) {
        this.cutTime = cutTime;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public Integer getPieceCount() {
        return pieceCount;
    }

    public void setPieceCount(Integer pieceCount) {
        this.pieceCount = pieceCount;
    }

    public Integer getPackageCount() {
        return packageCount;
    }

    public void setPackageCount(Integer packageCount) {
        this.packageCount = packageCount;
    }

    public CutBedColor() {
    }

    public CutBedColor(String orderName, Integer bedNumber, Date cutTime, String colorName, String sizeName, Integer pieceCount, String groupName) {
        this.orderName = orderName;
        this.bedNumber = bedNumber;
        this.cutTime = cutTime;
        this.colorName = colorName;
        this.sizeName = sizeName;
        this.pieceCount = pieceCount;
        this.groupName = groupName;
    }
}
