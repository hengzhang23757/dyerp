package com.example.erp01.model;

import java.util.Date;

public class EmbStorage {

    private Integer embStorageID;

    private String embStoreLocation;

    private String orderName;

    private Integer bedNumber;

    private String colorName;

    private String sizeName;

    private Integer layerCount;

    private Integer packageNumber;

    private String tailorQcode;

    private Integer embStorageState;

    private Integer embStoreCount;

    private Integer embStorageQueryCount;

    private Date createTime;

    private String embInStoreTime;

    public String getEmbInStoreTime() {
        return embInStoreTime;
    }

    public void setEmbInStoreTime(String embInStoreTime) {
        this.embInStoreTime = embInStoreTime;
    }

    public EmbStorage(Integer embStorageID, String embStoreLocation, String orderName, Integer bedNumber, String colorName, String sizeName, Integer layerCount, Integer packageNumber, String tailorQcode) {
        this.embStorageID = embStorageID;
        this.embStoreLocation = embStoreLocation;
        this.orderName = orderName;
        this.bedNumber = bedNumber;
        this.colorName = colorName;
        this.sizeName = sizeName;
        this.layerCount = layerCount;
        this.packageNumber = packageNumber;
        this.tailorQcode = tailorQcode;
    }

    public EmbStorage(String embStoreLocation, String orderName, Integer bedNumber, String colorName, String sizeName, Integer layerCount, Integer packageNumber, String tailorQcode) {
        this.embStoreLocation = embStoreLocation;
        this.orderName = orderName;
        this.bedNumber = bedNumber;
        this.colorName = colorName;
        this.sizeName = sizeName;
        this.layerCount = layerCount;
        this.packageNumber = packageNumber;
        this.tailorQcode = tailorQcode;
    }

    public EmbStorage(String embStoreLocation, String orderName, Integer bedNumber, String colorName, String sizeName, Integer layerCount, Integer packageNumber) {
        this.embStoreLocation = embStoreLocation;
        this.orderName = orderName;
        this.bedNumber = bedNumber;
        this.colorName = colorName;
        this.sizeName = sizeName;
        this.layerCount = layerCount;
        this.packageNumber = packageNumber;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getOrderName() {
        return orderName;
    }

    public Integer getPackageNumber() {
        return packageNumber;
    }

    public void setPackageNumber(Integer packageNumber) {
        this.packageNumber = packageNumber;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public Integer getBedNumber() {
        return bedNumber;
    }

    public void setBedNumber(Integer bedNumber) {
        this.bedNumber = bedNumber;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public Integer getLayerCount() {
        return layerCount;
    }

    public void setLayerCount(Integer layerCount) {
        this.layerCount = layerCount;
    }

    public Integer getEmbStorageQueryCount() {
        return embStorageQueryCount;
    }

    public void setEmbStorageQueryCount(Integer embStorageQueryCount) {
        this.embStorageQueryCount = embStorageQueryCount;
    }

    public String getEmbStoreLocation() {
        return embStoreLocation;
    }

    public void setEmbStoreLocation(String embStoreLocation) {
        this.embStoreLocation = embStoreLocation;
    }

    public Integer getEmbStoreCount() {
        return embStoreCount;
    }

    public void setEmbStoreCount(Integer embStoreCount) {
        this.embStoreCount = embStoreCount;
    }

    public EmbStorage(String embStoreLocation, Integer embStorageState, Integer embStoreCount) {
        this.embStoreLocation = embStoreLocation;
        this.embStorageState = embStorageState;
        this.embStoreCount = embStoreCount;
    }


    public EmbStorage(String embStoreLocation, Integer embStorageState) {
        this.embStoreLocation = embStoreLocation;
        this.embStorageState = embStorageState;
    }

    public Integer getEmbStorageState() {
        return embStorageState;
    }

    public void setEmbStorageState(Integer embStorageState) {
        this.embStorageState = embStorageState;
    }

    public EmbStorage(String embStoreLocation, String tailorQcode) {
        this.embStoreLocation = embStoreLocation;
        this.tailorQcode = tailorQcode;
    }

    public EmbStorage(Integer embStorageID, String embStoreLocation, String tailorQcode) {
        this.embStorageID = embStorageID;
        this.embStoreLocation = embStoreLocation;
        this.tailorQcode = tailorQcode;
    }

    public EmbStorage() {
    }

    public Integer getEmbStorageID() {
        return embStorageID;
    }

    public void setEmbStorageID(Integer embStorageID) {
        this.embStorageID = embStorageID;
    }


    public String getTailorQcode() {
        return tailorQcode;
    }

    public void setTailorQcode(String tailorQcode) {
        this.tailorQcode = tailorQcode;
    }
}
