package com.example.erp01.model;

public class FabricMonthCheck {

    private Integer id;

    private String clothesVersionNumber;

    private String orderName;

    private String customerName;

    private String season;

    private String fabricName;

    private String fabricNumber;

    private String fabricColor;

    private String fabricColorNumber;

    private String jarName;

    private String batchNumber;

    private Float weight = 0f;

    private String operateType;

    private String returnTime;

    private String supplier;

    private String colorName;

    private String unit;

    private Integer fabricID;

    private String checkNumber;

    private Integer orderCount = 0;

    private Float discount = 0f;

    private Float price = 0f;

    private Float fabricActCount = 0f;

    private Float returnMoney = 0f;

    private Float finishWeight = 0f;

    private String reviewType = "面料";

    private String remark;

    private String checkMonth;

    private String sourceType;

    private Float addFee = 0f;

    private String addRemark = "";

    private Integer feeState;

    private Integer feeCount = 0;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getFabricName() {
        return fabricName;
    }

    public void setFabricName(String fabricName) {
        this.fabricName = fabricName;
    }

    public String getFabricNumber() {
        return fabricNumber;
    }

    public void setFabricNumber(String fabricNumber) {
        this.fabricNumber = fabricNumber;
    }

    public String getFabricColor() {
        return fabricColor;
    }

    public void setFabricColor(String fabricColor) {
        this.fabricColor = fabricColor;
    }

    public String getFabricColorNumber() {
        return fabricColorNumber;
    }

    public void setFabricColorNumber(String fabricColorNumber) {
        this.fabricColorNumber = fabricColorNumber;
    }

    public String getJarName() {
        return jarName;
    }

    public void setJarName(String jarName) {
        this.jarName = jarName;
    }

    public String getBatchNumber() {
        return batchNumber;
    }

    public void setBatchNumber(String batchNumber) {
        this.batchNumber = batchNumber;
    }

    public Float getWeight() {
        return weight;
    }

    public void setWeight(Float weight) {
        this.weight = weight;
    }

    public String getOperateType() {
        return operateType;
    }

    public void setOperateType(String operateType) {
        this.operateType = operateType;
    }

    public String getReturnTime() {
        return returnTime;
    }

    public void setReturnTime(String returnTime) {
        this.returnTime = returnTime;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Integer getFabricID() {
        return fabricID;
    }

    public void setFabricID(Integer fabricID) {
        this.fabricID = fabricID;
    }

    public String getCheckNumber() {
        return checkNumber;
    }

    public void setCheckNumber(String checkNumber) {
        this.checkNumber = checkNumber;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Float getFabricActCount() {
        return fabricActCount;
    }

    public void setFabricActCount(Float fabricActCount) {
        this.fabricActCount = fabricActCount;
    }

    public Float getReturnMoney() {
        return returnMoney;
    }

    public void setReturnMoney(Float returnMoney) {
        this.returnMoney = returnMoney;
    }

    public Float getFinishWeight() {
        return finishWeight;
    }

    public void setFinishWeight(Float finishWeight) {
        this.finishWeight = finishWeight;
    }

    public String getReviewType() {
        return reviewType;
    }

    public void setReviewType(String reviewType) {
        this.reviewType = reviewType;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCheckMonth() {
        return checkMonth;
    }

    public void setCheckMonth(String checkMonth) {
        this.checkMonth = checkMonth;
    }

    public Float getDiscount() {
        return discount;
    }

    public void setDiscount(Float discount) {
        this.discount = discount;
    }

    public Integer getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(Integer orderCount) {
        this.orderCount = orderCount;
    }

    public String getSourceType() {
        return sourceType;
    }

    public void setSourceType(String sourceType) {
        this.sourceType = sourceType;
    }

    public Float getAddFee() {
        return addFee;
    }

    public void setAddFee(Float addFee) {
        this.addFee = addFee;
    }

    public String getAddRemark() {
        return addRemark;
    }

    public void setAddRemark(String addRemark) {
        this.addRemark = addRemark;
    }

    public Integer getFeeState() {
        return feeState;
    }

    public void setFeeState(Integer feeState) {
        this.feeState = feeState;
    }

    public Integer getFeeCount() {
        return feeCount;
    }

    public void setFeeCount(Integer feeCount) {
        this.feeCount = feeCount;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }

    public FabricMonthCheck() {
    }
}
