package com.example.erp01.model;

/**
 * Created by hujian on 2019/4/20
 */
public class CutQueryLeak {

    private String orderName;
    private String colorName;
    private String sizeName;
    private Integer orderCount = 0;
    private Integer layerCount = 0;
    private Integer wellCount = 0;
    private Integer finishCount = 0;
    private Integer inStoreCount = 0;
    private Integer outStoreCount = 0;
    private Integer storageCount = 0;


    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public Integer getFinishCount() {
        return finishCount;
    }

    public void setFinishCount(Integer finishCount) {
        this.finishCount = finishCount;
    }

    public Integer getWellCount() {
        return wellCount;
    }

    public void setWellCount(Integer wellCount) {
        this.wellCount = wellCount;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public Integer getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(Integer orderCount) {
        this.orderCount = orderCount;
    }

    public Integer getLayerCount() {
        return layerCount;
    }

    public void setLayerCount(Integer layerCount) {
        this.layerCount = layerCount;
    }

    public Integer getInStoreCount() {
        return inStoreCount;
    }

    public void setInStoreCount(Integer inStoreCount) {
        this.inStoreCount = inStoreCount;
    }

    public Integer getOutStoreCount() {
        return outStoreCount;
    }

    public void setOutStoreCount(Integer outStoreCount) {
        this.outStoreCount = outStoreCount;
    }

    public Integer getStorageCount() {
        return storageCount;
    }

    public void setStorageCount(Integer storageCount) {
        this.storageCount = storageCount;
    }

    public CutQueryLeak() {
    }
}
