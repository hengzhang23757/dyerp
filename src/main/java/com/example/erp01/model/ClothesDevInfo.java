package com.example.erp01.model;

import java.util.Date;

public class ClothesDevInfo {

    private Integer id;

    // 开发号
    private String clothesDevNumber;

    // 版单号
    private String clothesVersionNumber;

    // 款号
    private String orderName;

    // 款名
    private String styleName;

    // 款式类别1
    private String styleTypeOne;

    // 款式类别2
    private String styleTypeTwo;

    // 款式类别3
    private String styleTypeThree;

    // 设计师
    private String designer;

    // 款号
    private String styleNumber;

    private String styleCategory;

    private String categoryCode;

    // 季度
    private String season;

    //年份
    private String  clothesYear;

    private String stylePattern;

    private String designDepartName;

    private String designDepartCode;

    private Date requireDate;

    private Date expectDate;

    private Integer clothesCount;

    private String customerCode;

    private String customerName;

    private Date createTime;

    private Date updateTime;

    public ClothesDevInfo() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getClothesDevNumber() {
        return clothesDevNumber;
    }

    public void setClothesDevNumber(String clothesDevNumber) {
        this.clothesDevNumber = clothesDevNumber;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getStyleName() {
        return styleName;
    }

    public void setStyleName(String styleName) {
        this.styleName = styleName;
    }

    public String getStyleTypeOne() {
        return styleTypeOne;
    }

    public void setStyleTypeOne(String styleTypeOne) {
        this.styleTypeOne = styleTypeOne;
    }

    public String getStyleTypeTwo() {
        return styleTypeTwo;
    }

    public void setStyleTypeTwo(String styleTypeTwo) {
        this.styleTypeTwo = styleTypeTwo;
    }

    public String getStyleTypeThree() {
        return styleTypeThree;
    }

    public void setStyleTypeThree(String styleTypeThree) {
        this.styleTypeThree = styleTypeThree;
    }

    public String getDesigner() {
        return designer;
    }

    public void setDesigner(String designer) {
        this.designer = designer;
    }

    public String getStyleNumber() {
        return styleNumber;
    }

    public void setStyleNumber(String styleNumber) {
        this.styleNumber = styleNumber;
    }

    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }

    public String getStyleCategory() {
        return styleCategory;
    }

    public void setStyleCategory(String styleCategory) {
        this.styleCategory = styleCategory;
    }

    public String getCategoryCode() {
        return categoryCode;
    }

    public void setCategoryCode(String categoryCode) {
        this.categoryCode = categoryCode;
    }

    public String getClothesYear() {
        return clothesYear;
    }

    public void setClothesYear(String clothesYear) {
        this.clothesYear = clothesYear;
    }

    public String getDesignDepartName() {
        return designDepartName;
    }

    public void setDesignDepartName(String designDepartName) {
        this.designDepartName = designDepartName;
    }

    public String getDesignDepartCode() {
        return designDepartCode;
    }

    public void setDesignDepartCode(String designDepartCode) {
        this.designDepartCode = designDepartCode;
    }

    public Date getRequireDate() {
        return requireDate;
    }

    public void setRequireDate(Date requireDate) {
        this.requireDate = requireDate;
    }

    public Date getExpectDate() {
        return expectDate;
    }

    public void setExpectDate(Date expectDate) {
        this.expectDate = expectDate;
    }

    public Integer getClothesCount() {
        return clothesCount;
    }

    public void setClothesCount(Integer clothesCount) {
        this.clothesCount = clothesCount;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getStylePattern() {
        return stylePattern;
    }

    public void setStylePattern(String stylePattern) {
        this.stylePattern = stylePattern;
    }
}
