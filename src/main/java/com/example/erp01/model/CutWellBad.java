package com.example.erp01.model;

public class CutWellBad {

    private Integer orderCount = 0;

    private Integer cutCount = 0;

    private Integer wellCount = 0;

    private Integer finishCount = 0;

    private Integer diffCount = 0;

    public Integer getFinishCount() {
        return finishCount;
    }

    public void setFinishCount(Integer finishCount) {
        this.finishCount = finishCount;
    }

    public Integer getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(Integer orderCount) {
        this.orderCount = orderCount;
    }

    public Integer getCutCount() {
        return cutCount;
    }

    public void setCutCount(Integer cutCount) {
        this.cutCount = cutCount;
    }

    public Integer getWellCount() {
        return wellCount;
    }

    public void setWellCount(Integer wellCount) {
        this.wellCount = wellCount;
    }

    public Integer getDiffCount() {
        return diffCount;
    }

    public void setDiffCount(Integer diffCount) {
        this.diffCount = diffCount;
    }

    public CutWellBad() {
    }

    public CutWellBad(Integer orderCount, Integer cutCount, Integer wellCount, Integer finishCount, Integer diffCount) {
        this.orderCount = orderCount;
        this.cutCount = cutCount;
        this.wellCount = wellCount;
        this.finishCount = finishCount;
        this.diffCount = diffCount;
    }

    public CutWellBad(Integer orderCount, Integer cutCount, Integer wellCount, Integer diffCount) {
        this.orderCount = orderCount;
        this.cutCount = cutCount;
        this.wellCount = wellCount;
        this.diffCount = diffCount;
    }
}
