package com.example.erp01.model;

import java.util.List;

public class CutInDto {

    private String cutStoreLocation;

    private List<Integer> tailorQcodeIDList;

    @Override
    public String toString() {
        return "CutInDto{" +
                "cutStoreLocation='" + cutStoreLocation + '\'' +
                ", tailorQcodeIDList=" + tailorQcodeIDList +
                '}';
    }

    public String getCutStoreLocation() {
        return cutStoreLocation;
    }

    public void setCutStoreLocation(String cutStoreLocation) {
        this.cutStoreLocation = cutStoreLocation;
    }

    public List<Integer> getTailorQcodeIDList() {
        return tailorQcodeIDList;
    }

    public void setTailorQcodeIDList(List<Integer> tailorQcodeIDList) {
        this.tailorQcodeIDList = tailorQcodeIDList;
    }

    public CutInDto() {
    }
}
