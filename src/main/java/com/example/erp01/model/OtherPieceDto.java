package com.example.erp01.model;

import java.util.List;

public class OtherPieceDto {

    private Integer tailorQcodeID;

    private String employeeNumber;

    private String employeeName;

    private String groupName;

    private List<Integer> procedureNumberList;

    private boolean isOutBound;

    private String outBoundGroupName;

    @Override
    public String toString() {
        return "OtherPieceDto{" +
                "tailorQcodeID=" + tailorQcodeID +
                ", employeeNumber='" + employeeNumber + '\'' +
                ", employeeName='" + employeeName + '\'' +
                ", groupName='" + groupName + '\'' +
                ", procedureNumberList=" + procedureNumberList +
                ", isOutBound=" + isOutBound +
                ", outBoundGroupName='" + outBoundGroupName + '\'' +
                '}';
    }

    public OtherPieceDto() {
    }

    public Integer getTailorQcodeID() {
        return tailorQcodeID;
    }

    public void setTailorQcodeID(Integer tailorQcodeID) {
        this.tailorQcodeID = tailorQcodeID;
    }

    public String getEmployeeNumber() {
        return employeeNumber;
    }

    public void setEmployeeNumber(String employeeNumber) {
        this.employeeNumber = employeeNumber;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public List<Integer> getProcedureNumberList() {
        return procedureNumberList;
    }

    public void setProcedureNumberList(List<Integer> procedureNumberList) {
        this.procedureNumberList = procedureNumberList;
    }

    public boolean isOutBound() {
        return isOutBound;
    }

    public void setOutBound(boolean outBound) {
        isOutBound = outBound;
    }

    public String getOutBoundGroupName() {
        return outBoundGroupName;
    }

    public void setOutBoundGroupName(String outBoundGroupName) {
        this.outBoundGroupName = outBoundGroupName;
    }
}
