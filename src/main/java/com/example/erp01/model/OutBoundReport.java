package com.example.erp01.model;

import java.io.Serializable;
import java.util.Date;

public class OutBoundReport implements Serializable {

    private String customerName;

    private String orderName;

    private String clothesVersionNumber;

    private String printingPart;

    private String outOrderName;

    private String colorName;

    private Integer bedNumber;

    private String sizeName;

    private Integer packageNumber;

    private Integer layerCount;

    private Integer defectiveCount = 0;

    private Integer rottenCount = 0;

    private Integer lackCount = 0;

    private Date outDate;

    public String getPrintingPart() {
        return printingPart;
    }

    public void setPrintingPart(String printingPart) {
        this.printingPart = printingPart;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getOutOrderName() {
        return outOrderName;
    }

    public void setOutOrderName(String outOrderName) {
        this.outOrderName = outOrderName;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public Integer getBedNumber() {
        return bedNumber;
    }

    public void setBedNumber(Integer bedNumber) {
        this.bedNumber = bedNumber;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public Integer getPackageNumber() {
        return packageNumber;
    }

    public void setPackageNumber(Integer packageNumber) {
        this.packageNumber = packageNumber;
    }

    public Integer getLayerCount() {
        return layerCount;
    }

    public void setLayerCount(Integer layerCount) {
        this.layerCount = layerCount;
    }

    public Integer getDefectiveCount() {
        return defectiveCount;
    }

    public void setDefectiveCount(Integer defectiveCount) {
        this.defectiveCount = defectiveCount;
    }

    public Integer getRottenCount() {
        return rottenCount;
    }

    public void setRottenCount(Integer rottenCount) {
        this.rottenCount = rottenCount;
    }

    public Integer getLackCount() {
        return lackCount;
    }

    public void setLackCount(Integer lackCount) {
        this.lackCount = lackCount;
    }

    public Date getOutDate() {
        return outDate;
    }

    public void setOutDate(Date outDate) {
        this.outDate = outDate;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public OutBoundReport() {
    }

    public OutBoundReport(String customerName, String orderName, String printingPart, String outOrderName, String colorName, Integer bedNumber, String sizeName, Integer packageNumber, Integer layerCount, Integer defectiveCount, Integer rottenCount, Integer lackCount) {
        this.customerName = customerName;
        this.orderName = orderName;
        this.printingPart = printingPart;
        this.outOrderName = outOrderName;
        this.colorName = colorName;
        this.bedNumber = bedNumber;
        this.sizeName = sizeName;
        this.packageNumber = packageNumber;
        this.layerCount = layerCount;
        this.defectiveCount = defectiveCount;
        this.rottenCount = rottenCount;
        this.lackCount = lackCount;
    }

    public OutBoundReport(String sizeName, Integer packageNumber, Integer layerCount, Integer defectiveCount, Integer rottenCount, Integer lackCount) {
        this.sizeName = sizeName;
        this.packageNumber = packageNumber;
        this.layerCount = layerCount;
        this.defectiveCount = defectiveCount;
        this.rottenCount = rottenCount;
        this.lackCount = lackCount;
    }
}
