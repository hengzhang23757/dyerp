package com.example.erp01.model;

public class ClothesPrice {

    private Integer id;

    private String orderName;

    private String clothesVersionNumber;

    private String colorName;

    private Float clothesPrice = 0f;

    private Float printPrice = 0f;

    private Float sewPrice = 0f;

    public ClothesPrice() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public Float getClothesPrice() {
        return clothesPrice;
    }

    public void setClothesPrice(Float clothesPrice) {
        this.clothesPrice = clothesPrice;
    }

    public Float getPrintPrice() {
        return printPrice;
    }

    public void setPrintPrice(Float printPrice) {
        this.printPrice = printPrice;
    }

    public Float getSewPrice() {
        return sewPrice;
    }

    public void setSewPrice(Float sewPrice) {
        this.sewPrice = sewPrice;
    }
}
