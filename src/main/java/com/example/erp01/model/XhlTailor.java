package com.example.erp01.model;

import java.io.Serializable;
import java.util.Date;

public class XhlTailor implements Serializable {

    private Integer tailorID;

    private String customerName;

    private String orderName;

    private String xhlOrderName;

    private String userName;

    private Integer bedNumber;

    private String printingPart;

    private String location;

    private String colorName;

    private String sizeName;

    private Integer packageNumber;

    private Integer layerCount;

    private Integer tailorQcode;

    private Date sendDate;

    private Integer printTimes;


    public Integer getPrintTimes() {
        return printTimes;
    }

    public void setPrintTimes(Integer printTimes) {
        this.printTimes = printTimes;
    }

    public Integer getTailorID() {
        return tailorID;
    }

    public void setTailorID(Integer tailorID) {
        this.tailorID = tailorID;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getXhlOrderName() {
        return xhlOrderName;
    }

    public void setXhlOrderName(String xhlOrderName) {
        this.xhlOrderName = xhlOrderName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getBedNumber() {
        return bedNumber;
    }

    public void setBedNumber(Integer bedNumber) {
        this.bedNumber = bedNumber;
    }

    public String getPrintingPart() {
        return printingPart;
    }

    public void setPrintingPart(String printingPart) {
        this.printingPart = printingPart;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public Integer getPackageNumber() {
        return packageNumber;
    }

    public void setPackageNumber(Integer packageNumber) {
        this.packageNumber = packageNumber;
    }

    public Integer getLayerCount() {
        return layerCount;
    }

    public void setLayerCount(Integer layerCount) {
        this.layerCount = layerCount;
    }

    public Integer getTailorQcode() {
        return tailorQcode;
    }

    public void setTailorQcode(Integer tailorQcode) {
        this.tailorQcode = tailorQcode;
    }

    public Date getSendDate() {
        return sendDate;
    }

    public void setSendDate(Date sendDate) {
        this.sendDate = sendDate;
    }

    public XhlTailor() {
    }

    public XhlTailor(String customerName, String orderName, String xhlOrderName, String userName, Integer bedNumber, String printingPart, String location, String colorName, String sizeName, Integer packageNumber, Integer layerCount, Integer tailorQcode, Date sendDate) {
        this.customerName = customerName;
        this.orderName = orderName;
        this.xhlOrderName = xhlOrderName;
        this.userName = userName;
        this.bedNumber = bedNumber;
        this.printingPart = printingPart;
        this.location = location;
        this.colorName = colorName;
        this.sizeName = sizeName;
        this.packageNumber = packageNumber;
        this.layerCount = layerCount;
        this.tailorQcode = tailorQcode;
        this.sendDate = sendDate;
    }

    public XhlTailor(Integer tailorID, String customerName, String orderName, String xhlOrderName, String userName, Integer bedNumber, String printingPart, String location, String colorName, String sizeName, Integer packageNumber, Integer layerCount, Integer tailorQcode, Date sendDate) {
        this.tailorID = tailorID;
        this.customerName = customerName;
        this.orderName = orderName;
        this.xhlOrderName = xhlOrderName;
        this.userName = userName;
        this.bedNumber = bedNumber;
        this.printingPart = printingPart;
        this.location = location;
        this.colorName = colorName;
        this.sizeName = sizeName;
        this.packageNumber = packageNumber;
        this.layerCount = layerCount;
        this.tailorQcode = tailorQcode;
        this.sendDate = sendDate;
    }

    public XhlTailor(Integer tailorQcode, Integer printTimes) {
        this.tailorQcode = tailorQcode;
        this.printTimes = printTimes;
    }
}
