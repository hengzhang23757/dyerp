package com.example.erp01.model;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class FabricPlan {

    private Integer fabricPlanID;

    private String season;

    private String clothesVersionNumber;

    private String orderName;

    private String customerName;

    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date orderDate;

    private Integer orderCount;

    private String colorName;

    private Integer colorOrderCount;

    private String supplier;

    private String fabricName;

    private String fabricColor;

    private String fabricWidth;

    private String fabricWeight;

    private Float pieceUsage;

    private Float fabricAdd;

    private Float fabricOrderCount;

    private Float fabricPrice;

    private Float sumMoney;

    private String fabricUse;

    private String remark;

    private String userName;

    private String createTime;

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getColorOrderCount() {
        return colorOrderCount;
    }

    public void setColorOrderCount(Integer colorOrderCount) {
        this.colorOrderCount = colorOrderCount;
    }

    public Float getSumMoney() {
        return sumMoney;
    }

    public void setSumMoney(Float sumMoney) {
        this.sumMoney = sumMoney;
    }

    public Integer getFabricPlanID() {
        return fabricPlanID;
    }

    public void setFabricPlanID(Integer fabricPlanID) {
        this.fabricPlanID = fabricPlanID;
    }

    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public Integer getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(Integer orderCount) {
        this.orderCount = orderCount;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public String getFabricName() {
        return fabricName;
    }

    public void setFabricName(String fabricName) {
        this.fabricName = fabricName;
    }

    public String getFabricColor() {
        return fabricColor;
    }

    public void setFabricColor(String fabricColor) {
        this.fabricColor = fabricColor;
    }


    public String getFabricWidth() {
        return fabricWidth;
    }

    public void setFabricWidth(String fabricWidth) {
        this.fabricWidth = fabricWidth;
    }

    public String getFabricWeight() {
        return fabricWeight;
    }

    public void setFabricWeight(String fabricWeight) {
        this.fabricWeight = fabricWeight;
    }

    public Float getPieceUsage() {
        return pieceUsage;
    }

    public void setPieceUsage(Float pieceUsage) {
        this.pieceUsage = pieceUsage;
    }

    public Float getFabricAdd() {
        return fabricAdd;
    }

    public void setFabricAdd(Float fabricAdd) {
        this.fabricAdd = fabricAdd;
    }

    public Float getFabricOrderCount() {
        return fabricOrderCount;
    }

    public void setFabricOrderCount(Float fabricOrderCount) {
        this.fabricOrderCount = fabricOrderCount;
    }

    public Float getFabricPrice() {
        return fabricPrice;
    }

    public void setFabricPrice(Float fabricPrice) {
        this.fabricPrice = fabricPrice;
    }

    public String getFabricUse() {
        return fabricUse;
    }

    public void setFabricUse(String fabricUse) {
        this.fabricUse = fabricUse;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public FabricPlan() {
    }

    public FabricPlan(String season, String clothesVersionNumber, String orderName, String customerName, Date orderDate, Integer orderCount, String colorName, Integer colorOrderCount, String supplier, String fabricName, String fabricColor, String fabricWidth, String fabricWeight, Float pieceUsage, Float fabricAdd, Float fabricOrderCount, Float fabricPrice, Float sumMoney, String fabricUse, String remark, String userName) {
        this.season = season;
        this.clothesVersionNumber = clothesVersionNumber;
        this.orderName = orderName;
        this.customerName = customerName;
        this.orderDate = orderDate;
        this.orderCount = orderCount;
        this.colorName = colorName;
        this.colorOrderCount = colorOrderCount;
        this.supplier = supplier;
        this.fabricName = fabricName;
        this.fabricColor = fabricColor;
        this.fabricWidth = fabricWidth;
        this.fabricWeight = fabricWeight;
        this.pieceUsage = pieceUsage;
        this.fabricAdd = fabricAdd;
        this.fabricOrderCount = fabricOrderCount;
        this.fabricPrice = fabricPrice;
        this.sumMoney = sumMoney;
        this.fabricUse = fabricUse;
        this.remark = remark;
        this.userName = userName;
    }

    public FabricPlan(Integer fabricPlanID, String season, String clothesVersionNumber, String orderName, String customerName, Date orderDate, Integer orderCount, String colorName, Integer colorOrderCount, String supplier, String fabricName, String fabricColor, String fabricWidth, String fabricWeight, Float pieceUsage, Float fabricAdd, Float fabricOrderCount, Float fabricPrice, Float sumMoney, String fabricUse, String remark, String userName) {
        this.fabricPlanID = fabricPlanID;
        this.season = season;
        this.clothesVersionNumber = clothesVersionNumber;
        this.orderName = orderName;
        this.customerName = customerName;
        this.orderDate = orderDate;
        this.orderCount = orderCount;
        this.colorName = colorName;
        this.colorOrderCount = colorOrderCount;
        this.supplier = supplier;
        this.fabricName = fabricName;
        this.fabricColor = fabricColor;
        this.fabricWidth = fabricWidth;
        this.fabricWeight = fabricWeight;
        this.pieceUsage = pieceUsage;
        this.fabricAdd = fabricAdd;
        this.fabricOrderCount = fabricOrderCount;
        this.fabricPrice = fabricPrice;
        this.sumMoney = sumMoney;
        this.fabricUse = fabricUse;
        this.remark = remark;
        this.userName = userName;
    }
}
