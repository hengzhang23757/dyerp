package com.example.erp01.model;

public class ColorUpdate {

    private String color;

    private String toColor;

    public ColorUpdate() {
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getToColor() {
        return toColor;
    }

    public void setToColor(String toColor) {
        this.toColor = toColor;
    }
}
