package com.example.erp01.model;

import org.hibernate.validator.internal.constraintvalidators.bv.time.future.FutureValidatorForZonedDateTime;

import java.sql.Time;
import java.sql.Timestamp;

public class Metre {

    private Long nid;

    private String employeeNumber;

    private String employeeName;

    private String groupName;

    private Timestamp beginTime;

    private Timestamp endTime;

    private Double timeCount;

    private String orderName;

    private String clothesVersionNumber;

    private String colorName;

    private String sizeName;

    private Integer procedureNumber;

    private String procedureName;

    private Integer procedureCode;

    private Float SAM;

    private Integer layerCount;

    private Boolean isMerge;

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public String getProcedureName() {
        return procedureName;
    }

    public void setProcedureName(String procedureName) {
        this.procedureName = procedureName;
    }

    public String getEmployeeNumber() {
        return employeeNumber;
    }

    public void setEmployeeNumber(String employeeNumber) {
        this.employeeNumber = employeeNumber;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public Timestamp getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(Timestamp beginTime) {
        this.beginTime = beginTime;
    }

    public Timestamp getEndTime() {
        return endTime;
    }

    public void setEndTime(Timestamp endTime) {
        this.endTime = endTime;
    }

    public Double getTimeCount() {
        return timeCount;
    }

    public void setTimeCount(Double timeCount) {
        this.timeCount = timeCount;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public Integer getProcedureNumber() {
        return procedureNumber;
    }

    public void setProcedureNumber(Integer procedureNumber) {
        this.procedureNumber = procedureNumber;
    }

    public Integer getLayerCount() {
        return layerCount;
    }

    public void setLayerCount(Integer layerCount) {
        this.layerCount = layerCount;
    }


    public Boolean getMerge() {
        return isMerge;
    }

    public void setMerge(Boolean merge) {
        isMerge = merge;
    }

    public Integer getProcedureCode() {
        return procedureCode;
    }

    public void setProcedureCode(Integer procedureCode) {
        this.procedureCode = procedureCode;
    }

    public Long getNid() {
        return nid;
    }

    public void setNid(Long nid) {
        this.nid = nid;
    }


    public Float getSAM() {
        return SAM;
    }

    public void setSAM(Float SAM) {
        this.SAM = SAM;
    }

    public Metre() {
    }

    public Metre(String employeeNumber, String employeeName, String groupName, Timestamp beginTime, Timestamp endTime, Double timeCount, String orderName, String clothesVersionNumber, String colorName, String sizeName, Integer procedureNumber, String procedureName, Integer layerCount) {
        this.employeeNumber = employeeNumber;
        this.employeeName = employeeName;
        this.groupName = groupName;
        this.beginTime = beginTime;
        this.endTime = endTime;
        this.timeCount = timeCount;
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.colorName = colorName;
        this.sizeName = sizeName;
        this.procedureNumber = procedureNumber;
        this.procedureName = procedureName;
        this.layerCount = layerCount;
    }
}
