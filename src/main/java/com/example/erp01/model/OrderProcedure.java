package com.example.erp01.model;


import java.util.Date;

public class OrderProcedure {
    private Integer orderProcedureID;
    private String orderName;
    private String clothesVersionNumber;
    private String styleDescription;
    private String customerName;
    private Date beginDate;
    private Integer orderCount;
    private String styleType;
    private String partName;
    private Integer procedureCode;
    private Integer procedureNumber;
    private String procedureName;
    private String procedureSection;
    private String procedureDescription;
    private String remark;
    private String segment;
    private String equType;
    private String procedureLevel;
    private double SAM;
    private double packagePrice;
    private double piecePrice;
    private double piecePriceTwo = 0;
    private Integer procedureState;
    private Float subsidy = 0f;
    private String scanPart;
    private String specialProcedure;
    private Integer reviewState = 0;
    private Date createTime;
    private String colorName;
    private String sizeName;
    private Integer isScan = 0;
    private Float initPrice = 0f;
    private Date lastUpdateTime;
    private String lastUpdateUser;
    private Date lastReviewTime;

    public double getPiecePriceTwo() {
        return piecePriceTwo;
    }

    public void setPiecePriceTwo(double piecePriceTwo) {
        this.piecePriceTwo = piecePriceTwo;
    }

    public Integer getReviewState() {
        return reviewState;
    }

    public void setReviewState(Integer reviewState) {
        this.reviewState = reviewState;
    }

    public String getSpecialProcedure() {
        return specialProcedure;
    }

    public void setSpecialProcedure(String specialProcedure) {
        this.specialProcedure = specialProcedure;
    }

    public String getScanPart() {
        return scanPart;
    }

    public void setScanPart(String scanPart) {
        this.scanPart = scanPart;
    }

    public Float getSubsidy() {
        return subsidy;
    }

    public void setSubsidy(Float subsidy) {
        this.subsidy = subsidy;
    }

    public Integer getOrderProcedureID() {
        return orderProcedureID;
    }

    public void setOrderProcedureID(Integer orderProcedureID) {
        this.orderProcedureID = orderProcedureID;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public String getStyleDescription() {
        return styleDescription;
    }

    public void setStyleDescription(String styleDescription) {
        this.styleDescription = styleDescription;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public Date getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    public Integer getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(Integer orderCount) {
        this.orderCount = orderCount;
    }

    public String getStyleType() {
        return styleType;
    }

    public void setStyleType(String styleType) {
        this.styleType = styleType;
    }

    public String getPartName() {
        return partName;
    }

    public void setPartName(String partName) {
        this.partName = partName;
    }

    public Integer getProcedureCode() {
        return procedureCode;
    }

    public void setProcedureCode(Integer procedureCode) {
        this.procedureCode = procedureCode;
    }

    public Integer getProcedureNumber() {
        return procedureNumber;
    }

    public void setProcedureNumber(Integer procedureNumber) {
        this.procedureNumber = procedureNumber;
    }

    public String getProcedureName() {
        return procedureName;
    }

    public void setProcedureName(String procedureName) {
        this.procedureName = procedureName;
    }

    public String getProcedureSection() {
        return procedureSection;
    }

    public void setProcedureSection(String procedureSection) {
        this.procedureSection = procedureSection;
    }

    public String getProcedureDescription() {
        return procedureDescription;
    }

    public void setProcedureDescription(String procedureDescription) {
        this.procedureDescription = procedureDescription;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getSegment() {
        return segment;
    }

    public void setSegment(String segment) {
        this.segment = segment;
    }

    public String getEquType() {
        return equType;
    }

    public void setEquType(String equType) {
        this.equType = equType;
    }

    public String getProcedureLevel() {
        return procedureLevel;
    }

    public void setProcedureLevel(String procedureLevel) {
        this.procedureLevel = procedureLevel;
    }

    public double getSAM() {
        return SAM;
    }

    public void setSAM(double SAM) {
        this.SAM = SAM;
    }

    public double getPackagePrice() {
        return packagePrice;
    }

    public void setPackagePrice(double packagePrice) {
        this.packagePrice = packagePrice;
    }

    public double getPiecePrice() {
        return piecePrice;
    }

    public void setPiecePrice(double piecePrice) {
        this.piecePrice = piecePrice;
    }

    public Integer getProcedureState() {
        return procedureState;
    }

    public void setProcedureState(Integer procedureState) {
        this.procedureState = procedureState;
    }


    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public Integer getIsScan() {
        return isScan;
    }

    public void setIsScan(Integer isScan) {
        this.isScan = isScan;
    }

    public Float getInitPrice() {
        return initPrice;
    }

    public void setInitPrice(Float initPrice) {
        this.initPrice = initPrice;
    }

    public Date getLastUpdateTime() {
        return lastUpdateTime;
    }

    public void setLastUpdateTime(Date lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }

    public String getLastUpdateUser() {
        return lastUpdateUser;
    }

    public void setLastUpdateUser(String lastUpdateUser) {
        this.lastUpdateUser = lastUpdateUser;
    }

    public Date getLastReviewTime() {
        return lastReviewTime;
    }

    public void setLastReviewTime(Date lastReviewTime) {
        this.lastReviewTime = lastReviewTime;
    }

    public OrderProcedure() {
    }

    public OrderProcedure(String orderName, String clothesVersionNumber, String styleDescription, String customerName, Date beginDate, Integer orderCount, String styleType, String partName, Integer procedureCode, Integer procedureNumber, String procedureName, String procedureSection, String procedureDescription, String remark, String segment, String equType, String procedureLevel, double SAM, double packagePrice, double piecePrice, Integer procedureState, Float subsidy, String scanPart, String specialProcedure) {
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.styleDescription = styleDescription;
        this.customerName = customerName;
        this.beginDate = beginDate;
        this.orderCount = orderCount;
        this.styleType = styleType;
        this.partName = partName;
        this.procedureCode = procedureCode;
        this.procedureNumber = procedureNumber;
        this.procedureName = procedureName;
        this.procedureSection = procedureSection;
        this.procedureDescription = procedureDescription;
        this.remark = remark;
        this.segment = segment;
        this.equType = equType;
        this.procedureLevel = procedureLevel;
        this.SAM = SAM;
        this.packagePrice = packagePrice;
        this.piecePrice = piecePrice;
        this.procedureState = procedureState;
        this.subsidy = subsidy;
        this.scanPart = scanPart;
        this.specialProcedure = specialProcedure;
    }

    public OrderProcedure(Integer orderProcedureID, String orderName, String clothesVersionNumber, String styleDescription, String customerName, Date beginDate, Integer orderCount, String styleType, String partName, Integer procedureCode, Integer procedureNumber, String procedureName, String procedureSection, String procedureDescription, String remark, String segment, String equType, String procedureLevel, double SAM, double packagePrice, double piecePrice, Integer procedureState, Float subsidy, String scanPart, String specialProcedure) {
        this.orderProcedureID = orderProcedureID;
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.styleDescription = styleDescription;
        this.customerName = customerName;
        this.beginDate = beginDate;
        this.orderCount = orderCount;
        this.styleType = styleType;
        this.partName = partName;
        this.procedureCode = procedureCode;
        this.procedureNumber = procedureNumber;
        this.procedureName = procedureName;
        this.procedureSection = procedureSection;
        this.procedureDescription = procedureDescription;
        this.remark = remark;
        this.segment = segment;
        this.equType = equType;
        this.procedureLevel = procedureLevel;
        this.SAM = SAM;
        this.packagePrice = packagePrice;
        this.piecePrice = piecePrice;
        this.procedureState = procedureState;
        this.subsidy = subsidy;
        this.scanPart = scanPart;
        this.specialProcedure = specialProcedure;
    }

    public OrderProcedure(Integer orderProcedureID, String orderName, String clothesVersionNumber, String styleDescription, String customerName, Date beginDate, Integer orderCount, String styleType, String partName, Integer procedureCode, Integer procedureNumber, String procedureName, String procedureSection, String procedureDescription, String remark, String segment, String equType, String procedureLevel, double SAM, double packagePrice, double piecePrice, Integer procedureState, Float subsidy, String scanPart, String specialProcedure, Integer reviewState) {
        this.orderProcedureID = orderProcedureID;
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.styleDescription = styleDescription;
        this.customerName = customerName;
        this.beginDate = beginDate;
        this.orderCount = orderCount;
        this.styleType = styleType;
        this.partName = partName;
        this.procedureCode = procedureCode;
        this.procedureNumber = procedureNumber;
        this.procedureName = procedureName;
        this.procedureSection = procedureSection;
        this.procedureDescription = procedureDescription;
        this.remark = remark;
        this.segment = segment;
        this.equType = equType;
        this.procedureLevel = procedureLevel;
        this.SAM = SAM;
        this.packagePrice = packagePrice;
        this.piecePrice = piecePrice;
        this.procedureState = procedureState;
        this.subsidy = subsidy;
        this.scanPart = scanPart;
        this.specialProcedure = specialProcedure;
        this.reviewState = reviewState;
    }
}
