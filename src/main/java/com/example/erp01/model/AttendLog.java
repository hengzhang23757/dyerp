package com.example.erp01.model;

import java.util.Date;

public class AttendLog {

    private Long id;

    private Long atten_id;

    private String atten_uid;

    private Long atten_time;

    private Date atten_date;

    private String realname;

    private Date atten_datetime;

    private String dateString;

    private String dateTimeString;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAtten_id() {
        return atten_id;
    }

    public void setAtten_id(Long atten_id) {
        this.atten_id = atten_id;
    }

    public String getAtten_uid() {
        return atten_uid;
    }

    public void setAtten_uid(String atten_uid) {
        this.atten_uid = atten_uid;
    }

    public Long getAtten_time() {
        return atten_time;
    }

    public void setAtten_time(Long atten_time) {
        this.atten_time = atten_time;
    }

    public Date getAtten_date() {
        return atten_date;
    }

    public void setAtten_date(Date atten_date) {
        this.atten_date = atten_date;
    }

    public String getRealname() {
        return realname;
    }

    public void setRealname(String realname) {
        this.realname = realname;
    }

    public Date getAtten_datetime() {
        return atten_datetime;
    }

    public void setAtten_datetime(Date atten_datetime) {
        this.atten_datetime = atten_datetime;
    }

    public String getDateTimeString() {
        return dateTimeString;
    }

    public void setDateTimeString(String dateTimeString) {
        this.dateTimeString = dateTimeString;
    }

    public String getDateString() {
        return dateString;
    }

    public void setDateString(String dateString) {
        this.dateString = dateString;
    }

    public AttendLog() {
    }

    @Override
    public String toString() {
        return "AttendLog{" +
                "id=" + id +
                ", atten_id=" + atten_id +
                ", atten_uid='" + atten_uid + '\'' +
                ", atten_time=" + atten_time +
                ", atten_date=" + atten_date +
                ", realname='" + realname + '\'' +
                ", atten_datetime=" + atten_datetime +
                '}';
    }
}
