package com.example.erp01.model;

import java.util.Date;

public class TaskData {

    private Integer id;

    private String text;

    private String orderName;

    private String clothesVersionNumber;

    private String groupName;

    private Integer planOrder;

    private Date startDate;

    private String start_date;

    private Date endDate;

    private Integer duration;

    private Integer parent;

    private Float progress;

    private String taskOpen;

    private String open;

    private String backType;

    private String taskType;

    private Float sam;

    private Integer taskCount;

    private Long maxNid;

    private Integer finishCount;

    private Float dayPercent;

    private Integer isFirst = 0;

    private Integer dayOrder = 0;

    private Integer source;

    private Integer target;

    private Date deadLine;

    private String remark;

    private String deliveryState;

    private String colorName;

    private String sizeName;

    private Integer dailyCount;

    public Integer getDailyCount() {
        return dailyCount;
    }

    public void setDailyCount(Integer dailyCount) {
        this.dailyCount = dailyCount;
    }

    public TaskData() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Integer getParent() {
        return parent;
    }

    public void setParent(Integer parent) {
        this.parent = parent;
    }

    public Float getProgress() {
        return progress;
    }

    public void setProgress(Float progress) {
        this.progress = progress;
    }

    public String getTaskOpen() {
        return taskOpen;
    }

    public void setTaskOpen(String taskOpen) {
        this.taskOpen = taskOpen;
    }

    public String getBackType() {
        return backType;
    }

    public void setBackType(String backType) {
        this.backType = backType;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getOpen() {
        return open;
    }

    public void setOpen(String open) {
        this.open = open;
    }

    public String getTaskType() {
        return taskType;
    }

    public void setTaskType(String taskType) {
        this.taskType = taskType;
    }

    public Float getSam() {
        return sam;
    }

    public void setSam(Float sam) {
        this.sam = sam;
    }

    public Integer getTaskCount() {
        return taskCount;
    }

    public Long getMaxNid() {
        return maxNid;
    }

    public void setMaxNid(Long maxNid) {
        this.maxNid = maxNid;
    }

    public Integer getFinishCount() {
        return finishCount;
    }

    public void setFinishCount(Integer finishCount) {
        this.finishCount = finishCount;
    }

    public Float getDayPercent() {
        return dayPercent;
    }

    public void setDayPercent(Float dayPercent) {
        this.dayPercent = dayPercent;
    }

    public void setTaskCount(Integer taskCount) {
        this.taskCount = taskCount;
    }

    public Integer getPlanOrder() {
        return planOrder;
    }

    public void setPlanOrder(Integer planOrder) {
        this.planOrder = planOrder;
    }

    public Integer getIsFirst() {
        return isFirst;
    }

    public void setIsFirst(Integer isFirst) {
        this.isFirst = isFirst;
    }

    public Integer getDayOrder() {
        return dayOrder;
    }

    public void setDayOrder(Integer dayOrder) {
        this.dayOrder = dayOrder;
    }

    public Integer getSource() {
        return source;
    }

    public void setSource(Integer source) {
        this.source = source;
    }

    public Integer getTarget() {
        return target;
    }

    public void setTarget(Integer target) {
        this.target = target;
    }

    public Date getDeadLine() {
        return deadLine;
    }

    public void setDeadLine(Date deadLine) {
        this.deadLine = deadLine;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getDeliveryState() {
        return deliveryState;
    }

    public void setDeliveryState(String deliveryState) {
        this.deliveryState = deliveryState;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public TaskData(String text, String orderName, String clothesVersionNumber, String groupName, Date startDate, Date endDate, Integer duration, Integer parent, Float progress, String taskOpen, String backType, String taskType, Float sam, Integer taskCount, Long maxNid, Integer finishCount, Float dayPercent) {
        this.text = text;
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.groupName = groupName;
        this.startDate = startDate;
        this.endDate = endDate;
        this.duration = duration;
        this.parent = parent;
        this.progress = progress;
        this.taskOpen = taskOpen;
        this.backType = backType;
        this.taskType = taskType;
        this.sam = sam;
        this.taskCount = taskCount;
        this.maxNid = maxNid;
        this.finishCount = finishCount;
        this.dayPercent = dayPercent;
    }
}
