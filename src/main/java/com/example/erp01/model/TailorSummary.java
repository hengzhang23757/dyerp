package com.example.erp01.model;

public class TailorSummary {

    private String sizeName;

    private Integer sizeCount;

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public Integer getSizeCount() {
        return sizeCount;
    }

    public void setSizeCount(Integer sizeCount) {
        this.sizeCount = sizeCount;
    }

    public TailorSummary() {
    }

    public TailorSummary(String sizeName, Integer sizeCount) {
        this.sizeName = sizeName;
        this.sizeCount = sizeCount;
    }
}
