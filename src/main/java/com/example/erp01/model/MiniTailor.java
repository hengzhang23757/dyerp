package com.example.erp01.model;

import java.util.List;

public class MiniTailor {

    private String orderName;

    private String clothesVersionNumber;

    private List<MiniTailorLayerInfo> miniTailorLayerInfoList;

    private List<MiniMatchRatio> miniMatchRatioList;

    private List<String> partNameList;

    private Integer fixedNumber;

    public Integer getFixedNumber() {
        return fixedNumber;
    }

    public void setFixedNumber(Integer fixedNumber) {
        this.fixedNumber = fixedNumber;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public List<MiniTailorLayerInfo> getMiniTailorLayerInfoList() {
        return miniTailorLayerInfoList;
    }

    public void setMiniTailorLayerInfoList(List<MiniTailorLayerInfo> miniTailorLayerInfoList) {
        this.miniTailorLayerInfoList = miniTailorLayerInfoList;
    }

    public List<MiniMatchRatio> getMiniMatchRatioList() {
        return miniMatchRatioList;
    }

    public void setMiniMatchRatioList(List<MiniMatchRatio> miniMatchRatioList) {
        this.miniMatchRatioList = miniMatchRatioList;
    }

    public List<String> getPartNameList() {
        return partNameList;
    }

    public void setPartNameList(List<String> partNameList) {
        this.partNameList = partNameList;
    }

    public MiniTailor() {
    }

    public MiniTailor(String orderName, String clothesVersionNumber, List<MiniTailorLayerInfo> miniTailorLayerInfoList, List<MiniMatchRatio> miniMatchRatioList, List<String> partNameList, Integer fixedNumber) {
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.miniTailorLayerInfoList = miniTailorLayerInfoList;
        this.miniMatchRatioList = miniMatchRatioList;
        this.partNameList = partNameList;
        this.fixedNumber = fixedNumber;
    }

    public MiniTailor(String orderName, String clothesVersionNumber, List<MiniTailorLayerInfo> miniTailorLayerInfoList, List<MiniMatchRatio> miniMatchRatioList, List<String> partNameList) {
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.miniTailorLayerInfoList = miniTailorLayerInfoList;
        this.miniMatchRatioList = miniMatchRatioList;
        this.partNameList = partNameList;
    }
}
