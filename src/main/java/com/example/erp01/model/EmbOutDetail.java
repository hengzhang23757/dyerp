package com.example.erp01.model;

public class EmbOutDetail {

    private String orderName;

    private String clothesVersionNumber;

    private String groupName;

    private String colorName;

    private String sizeName;

    private Integer embOutCount;

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public EmbOutDetail(String orderName, String clothesVersionNumber, String groupName, String colorName, String sizeName, Integer embOutCount) {
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.groupName = groupName;
        this.colorName = colorName;
        this.sizeName = sizeName;
        this.embOutCount = embOutCount;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public Integer getEmbOutCount() {
        return embOutCount;
    }

    public void setEmbOutCount(Integer embOutCount) {
        this.embOutCount = embOutCount;
    }

    public EmbOutDetail(String orderName, String groupName, String colorName, String sizeName, Integer embOutCount) {
        this.orderName = orderName;
        this.groupName = groupName;
        this.colorName = colorName;
        this.sizeName = sizeName;
        this.embOutCount = embOutCount;
    }

    public EmbOutDetail() {
    }
}
