package com.example.erp01.model;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class Fabric {

    private Integer fabricID;

    private String customerName;

    private String season;

    private String versionNumber;

    private String styleNumber;

    private String supplier;

    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date orderDate;

    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date deliveryDate;

    private String fabricName;

    private String fabricNumber;

    private String fabricColor;

    private String fabricColorNumber;

    private String fabricUse;

    private String unit;

    private Float pieceUsage;

    private Integer clothesCount;

    private Float fabricCount;

    private Float price;

    private Float fabricWidth;

    private Float fabricWeight;

    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date actualDeliveryDate;

    private float deliveryQuantity;

    private Integer batchNumber;

    private String jarNumber;

    private String remark;

    public Integer getFabricID() {
        return fabricID;
    }

    public void setFabricID(Integer fabricID) {
        this.fabricID = fabricID;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }

    public String getVersionNumber() {
        return versionNumber;
    }

    public void setVersionNumber(String versionNumber) {
        this.versionNumber = versionNumber;
    }

    public String getStyleNumber() {
        return styleNumber;
    }

    public void setStyleNumber(String styleNumber) {
        this.styleNumber = styleNumber;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String getFabricName() {
        return fabricName;
    }

    public void setFabricName(String fabricName) {
        this.fabricName = fabricName;
    }

    public String getFabricNumber() {
        return fabricNumber;
    }

    public void setFabricNumber(String fabricNumber) {
        this.fabricNumber = fabricNumber;
    }

    public String getFabricColor() {
        return fabricColor;
    }

    public void setFabricColor(String fabricColor) {
        this.fabricColor = fabricColor;
    }

    public String getFabricColorNumber() {
        return fabricColorNumber;
    }

    public void setFabricColorNumber(String fabricColorNumber) {
        this.fabricColorNumber = fabricColorNumber;
    }

    public String getFabricUse() {
        return fabricUse;
    }

    public void setFabricUse(String fabricUse) {
        this.fabricUse = fabricUse;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Float getPieceUsage() {
        return pieceUsage;
    }

    public void setPieceUsage(Float pieceUsage) {
        this.pieceUsage = pieceUsage;
    }

    public Integer getClothesCount() {
        return clothesCount;
    }

    public void setClothesCount(Integer clothesCount) {
        this.clothesCount = clothesCount;
    }

    public Float getFabricCount() {
        return fabricCount;
    }

    public void setFabricCount(Float fabricCount) {
        this.fabricCount = fabricCount;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Float getFabricWidth() {
        return fabricWidth;
    }

    public void setFabricWidth(Float fabricWidth) {
        this.fabricWidth = fabricWidth;
    }

    public Float getFabricWeight() {
        return fabricWeight;
    }

    public void setFabricWeight(Float fabricWeight) {
        this.fabricWeight = fabricWeight;
    }

    public Date getActualDeliveryDate() {
        return actualDeliveryDate;
    }

    public void setActualDeliveryDate(Date actualDeliveryDate) {
        this.actualDeliveryDate = actualDeliveryDate;
    }

    public float getDeliveryQuantity() {
        return deliveryQuantity;
    }

    public void setDeliveryQuantity(float deliveryQuantity) {
        this.deliveryQuantity = deliveryQuantity;
    }

    public Integer getBatchNumber() {
        return batchNumber;
    }

    public void setBatchNumber(Integer batchNumber) {
        this.batchNumber = batchNumber;
    }

    public String getJarNumber() {
        return jarNumber;
    }

    public void setJarNumber(String jarNumber) {
        this.jarNumber = jarNumber;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Fabric() {
    }

    public Fabric(String customerName, String season, String versionNumber, String styleNumber, String supplier, Date orderDate, Date deliveryDate, String fabricName, String fabricNumber, String fabricColor, String fabricColorNumber, String fabricUse, String unit, Float pieceUsage, Integer clothesCount, Float fabricCount, Float price, Float fabricWidth, Float fabricWeight, Date actualDeliveryDate, float deliveryQuantity, Integer batchNumber, String jarNumber, String remark) {
        this.customerName = customerName;
        this.season = season;
        this.versionNumber = versionNumber;
        this.styleNumber = styleNumber;
        this.supplier = supplier;
        this.orderDate = orderDate;
        this.deliveryDate = deliveryDate;
        this.fabricName = fabricName;
        this.fabricNumber = fabricNumber;
        this.fabricColor = fabricColor;
        this.fabricColorNumber = fabricColorNumber;
        this.fabricUse = fabricUse;
        this.unit = unit;
        this.pieceUsage = pieceUsage;
        this.clothesCount = clothesCount;
        this.fabricCount = fabricCount;
        this.price = price;
        this.fabricWidth = fabricWidth;
        this.fabricWeight = fabricWeight;
        this.actualDeliveryDate = actualDeliveryDate;
        this.deliveryQuantity = deliveryQuantity;
        this.batchNumber = batchNumber;
        this.jarNumber = jarNumber;
        this.remark = remark;
    }

    public Fabric(Integer fabricID, String customerName, String season, String versionNumber, String styleNumber, String supplier, Date orderDate, Date deliveryDate, String fabricName, String fabricNumber, String fabricColor, String fabricColorNumber, String fabricUse, String unit, Float pieceUsage, Integer clothesCount, Float fabricCount, Float price, Float fabricWidth, Float fabricWeight, Date actualDeliveryDate, float deliveryQuantity, Integer batchNumber, String jarNumber, String remark) {
        this.fabricID = fabricID;
        this.customerName = customerName;
        this.season = season;
        this.versionNumber = versionNumber;
        this.styleNumber = styleNumber;
        this.supplier = supplier;
        this.orderDate = orderDate;
        this.deliveryDate = deliveryDate;
        this.fabricName = fabricName;
        this.fabricNumber = fabricNumber;
        this.fabricColor = fabricColor;
        this.fabricColorNumber = fabricColorNumber;
        this.fabricUse = fabricUse;
        this.unit = unit;
        this.pieceUsage = pieceUsage;
        this.clothesCount = clothesCount;
        this.fabricCount = fabricCount;
        this.price = price;
        this.fabricWidth = fabricWidth;
        this.fabricWeight = fabricWeight;
        this.actualDeliveryDate = actualDeliveryDate;
        this.deliveryQuantity = deliveryQuantity;
        this.batchNumber = batchNumber;
        this.jarNumber = jarNumber;
        this.remark = remark;
    }
}
