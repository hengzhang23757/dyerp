package com.example.erp01.model;

public class ReportCompare {

    private Integer planCount;

    private Integer actualCount;

    private Integer contrastCount;

    public Integer getPlanCount() {
        return planCount;
    }

    public void setPlanCount(Integer planCount) {
        this.planCount = planCount;
    }

    public Integer getActualCount() {
        return actualCount;
    }

    public void setActualCount(Integer actualCount) {
        this.actualCount = actualCount;
    }

    public Integer getContrastCount() {
        return contrastCount;
    }

    public void setContrastCount(Integer contrastCount) {
        this.contrastCount = contrastCount;
    }

    public ReportCompare() {
    }

    public ReportCompare(Integer planCount, Integer actualCount, Integer contrastCount) {
        this.planCount = planCount;
        this.actualCount = actualCount;
        this.contrastCount = contrastCount;
    }
}
