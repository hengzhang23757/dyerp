package com.example.erp01.model;

import java.util.Date;

public class EndProduct {

    private Integer id;

    private String orderName;

    private String clothesVersionNumber;

    private Float price = 0f;

    private String colorName;

    private String sizeName;

    private Integer inStoreCount = 0;

    private Integer storageCount = 0;

    private Integer outStoreCount = 0;

    private String defectiveType;

    private Integer defectiveInStore = 0;

    private Integer defectiveStorage = 0;

    private Integer defectiveOutStore = 0;

    private Date operateDate;

    private String operateType;

    private String endType;

    private Date createTime;

    private Integer storageId;

    private String customerName;

    private String accountState = "未入账";

    private Integer accountCount = 0;

    private Date accountDate;

    private Float totalMoney = 0f;

    private String payNumber;

    private String remark = "";

    public EndProduct() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public Integer getInStoreCount() {
        return inStoreCount;
    }

    public void setInStoreCount(Integer inStoreCount) {
        this.inStoreCount = inStoreCount;
    }

    public Integer getStorageCount() {
        return storageCount;
    }

    public void setStorageCount(Integer storageCount) {
        this.storageCount = storageCount;
    }

    public Integer getOutStoreCount() {
        return outStoreCount;
    }

    public void setOutStoreCount(Integer outStoreCount) {
        this.outStoreCount = outStoreCount;
    }

    public Date getOperateDate() {
        return operateDate;
    }

    public void setOperateDate(Date operateDate) {
        this.operateDate = operateDate;
    }

    public String getOperateType() {
        return operateType;
    }

    public void setOperateType(String operateType) {
        this.operateType = operateType;
    }

    public String getEndType() {
        return endType;
    }

    public void setEndType(String endType) {
        this.endType = endType;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getDefectiveInStore() {
        return defectiveInStore;
    }

    public void setDefectiveInStore(Integer defectiveInStore) {
        this.defectiveInStore = defectiveInStore;
    }

    public Integer getDefectiveOutStore() {
        return defectiveOutStore;
    }

    public void setDefectiveOutStore(Integer defectiveOutStore) {
        this.defectiveOutStore = defectiveOutStore;
    }

    public Integer getDefectiveStorage() {
        return defectiveStorage;
    }

    public void setDefectiveStorage(Integer defectiveStorage) {
        this.defectiveStorage = defectiveStorage;
    }

    public String getDefectiveType() {
        return defectiveType;
    }

    public void setDefectiveType(String defectiveType) {
        this.defectiveType = defectiveType;
    }

    public Integer getStorageId() {
        return storageId;
    }

    public void setStorageId(Integer storageId) {
        this.storageId = storageId;
    }

    public String getAccountState() {
        return accountState;
    }

    public void setAccountState(String accountState) {
        this.accountState = accountState;
    }

    public Integer getAccountCount() {
        return accountCount;
    }

    public void setAccountCount(Integer accountCount) {
        this.accountCount = accountCount;
    }

    public Date getAccountDate() {
        return accountDate;
    }

    public void setAccountDate(Date accountDate) {
        this.accountDate = accountDate;
    }

    public Float getTotalMoney() {
        return totalMoney;
    }

    public void setTotalMoney(Float totalMoney) {
        this.totalMoney = totalMoney;
    }

    public String getPayNumber() {
        return payNumber;
    }

    public void setPayNumber(String payNumber) {
        this.payNumber = payNumber;
    }


    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
