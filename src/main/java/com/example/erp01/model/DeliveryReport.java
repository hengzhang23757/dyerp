package com.example.erp01.model;

public class DeliveryReport {

    private String customerName;

    private String orderName;

    private String outOrderName;

    private String printingPart;

    private Integer bedNumber;

    private String colorName;

    private String sizeName;

    private Integer packageCount;

    private Integer totalLayer;

    public Integer getBedNumber() {
        return bedNumber;
    }

    public void setBedNumber(Integer bedNumber) {
        this.bedNumber = bedNumber;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getOutOrderName() {
        return outOrderName;
    }

    public void setOutOrderName(String outOrderName) {
        this.outOrderName = outOrderName;
    }

    public String getPrintingPart() {
        return printingPart;
    }

    public void setPrintingPart(String printingPart) {
        this.printingPart = printingPart;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public Integer getPackageCount() {
        return packageCount;
    }

    public void setPackageCount(Integer packageCount) {
        this.packageCount = packageCount;
    }

    public Integer getTotalLayer() {
        return totalLayer;
    }

    public void setTotalLayer(Integer totalLayer) {
        this.totalLayer = totalLayer;
    }

    public DeliveryReport() {
    }

    public DeliveryReport(String customerName, String orderName, String outOrderName, String printingPart, Integer bedNumber, String colorName, String sizeName, Integer packageCount, Integer totalLayer) {
        this.customerName = customerName;
        this.orderName = orderName;
        this.outOrderName = outOrderName;
        this.printingPart = printingPart;
        this.bedNumber = bedNumber;
        this.colorName = colorName;
        this.sizeName = sizeName;
        this.packageCount = packageCount;
        this.totalLayer = totalLayer;
    }

    public DeliveryReport(Integer packageCount, Integer totalLayer) {
        this.packageCount = packageCount;
        this.totalLayer = totalLayer;
    }
}
