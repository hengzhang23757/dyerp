package com.example.erp01.model;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class OrderClothes {

    private Integer orderClothesID;

    private String customerName;

    private String purchaseMethod;

    private String orderName;

    private String styleDescription;

    private String clothesVersionNumber;

    private String colorNumber;

    private String colorName;

    private String sizeName;

    private Integer count;

    private String season;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date deadLine;

    private Integer orderSum;

    private String userName;

    private String guid;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date deliveryDate;

    private String fabricCheck = "未输入";

    private String accessoryCheck = "未输入";

    private Integer orderState;

    private String orderNameTwo;

    private String clothesVersionNumberTwo;

    private String modifyState = "锁定";

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getOrderSum() {
        return orderSum;
    }

    public void setOrderSum(Integer orderSum) {
        this.orderSum = orderSum;
    }

    public OrderClothes() {
    }

    public Integer getOrderClothesID() {
        return orderClothesID;
    }

    public void setOrderClothesID(Integer orderClothesID) {
        this.orderClothesID = orderClothesID;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getPurchaseMethod() {
        return purchaseMethod;
    }

    public void setPurchaseMethod(String purchaseMethod) {
        this.purchaseMethod = purchaseMethod;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getStyleDescription() {
        return styleDescription;
    }

    public void setStyleDescription(String styleDescription) {
        this.styleDescription = styleDescription;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public Integer getOrderState() {
        return orderState;
    }

    public void setOrderState(Integer orderState) {
        this.orderState = orderState;
    }

    public String getColorNumber() {
        return colorNumber;
    }

    public void setColorNumber(String colorNumber) {
        this.colorNumber = colorNumber;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }

    public Date getDeadLine() {
        return deadLine;
    }

    public void setDeadLine(Date deadLine) {
        this.deadLine = deadLine;
    }

    public String getOrderNameTwo() {
        return orderNameTwo;
    }

    public void setOrderNameTwo(String orderNameTwo) {
        this.orderNameTwo = orderNameTwo;
    }

    public String getClothesVersionNumberTwo() {
        return clothesVersionNumberTwo;
    }

    public void setClothesVersionNumberTwo(String clothesVersionNumberTwo) {
        this.clothesVersionNumberTwo = clothesVersionNumberTwo;
    }

    public OrderClothes(Integer orderClothesID, String customerName, String purchaseMethod, String orderName, String styleDescription, String clothesVersionNumber, String colorName, String sizeName, Integer count, String season, String userName) {
        this.orderClothesID = orderClothesID;
        this.customerName = customerName;
        this.purchaseMethod = purchaseMethod;
        this.orderName = orderName;
        this.styleDescription = styleDescription;
        this.clothesVersionNumber = clothesVersionNumber;
        this.colorName = colorName;
        this.sizeName = sizeName;
        this.count = count;
        this.season = season;
        this.userName = userName;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String getFabricCheck() {
        return fabricCheck;
    }

    public void setFabricCheck(String fabricCheck) {
        this.fabricCheck = fabricCheck;
    }

    public String getAccessoryCheck() {
        return accessoryCheck;
    }

    public void setAccessoryCheck(String accessoryCheck) {
        this.accessoryCheck = accessoryCheck;
    }

    public String getModifyState() {
        return modifyState;
    }

    public void setModifyState(String modifyState) {
        this.modifyState = modifyState;
    }
}
