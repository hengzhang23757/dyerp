package com.example.erp01.model;

public class ClothesDevFabricNumber {

    private Integer id;

    private String fabricNumberName;

    private String fabricNumberCode;

    public ClothesDevFabricNumber() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFabricNumberName() {
        return fabricNumberName;
    }

    public void setFabricNumberName(String fabricNumberName) {
        this.fabricNumberName = fabricNumberName;
    }

    public String getFabricNumberCode() {
        return fabricNumberCode;
    }

    public void setFabricNumberCode(String fabricNumberCode) {
        this.fabricNumberCode = fabricNumberCode;
    }
}
