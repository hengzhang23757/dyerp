package com.example.erp01.model;

public class SpecialProcedure {

    private Integer specialID;

    private String orderName;

    private String clothesVersionNumber;

    private Integer procedureNumber;

    private String procedureName;

    private String colorName;

    private String sizeName;

    public Integer getSpecialID() {
        return specialID;
    }

    public void setSpecialID(Integer specialID) {
        this.specialID = specialID;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public Integer getProcedureNumber() {
        return procedureNumber;
    }

    public void setProcedureNumber(Integer procedureNumber) {
        this.procedureNumber = procedureNumber;
    }

    public String getProcedureName() {
        return procedureName;
    }

    public void setProcedureName(String procedureName) {
        this.procedureName = procedureName;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public SpecialProcedure(String orderName, String clothesVersionNumber, Integer procedureNumber, String procedureName, String colorName, String sizeName) {
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.procedureNumber = procedureNumber;
        this.procedureName = procedureName;
        this.colorName = colorName;
        this.sizeName = sizeName;
    }

    public SpecialProcedure(Integer specialID, String orderName, String clothesVersionNumber, Integer procedureNumber, String procedureName, String colorName, String sizeName) {
        this.specialID = specialID;
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.procedureNumber = procedureNumber;
        this.procedureName = procedureName;
        this.colorName = colorName;
        this.sizeName = sizeName;
    }

    public SpecialProcedure() {
    }
}
