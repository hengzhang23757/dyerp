package com.example.erp01.model;

public class InnerBoardStorage {

    private Integer innerBoardStorageID;

    private String location;

    private String versionNumber;

    private String fabricNumber;

    private String colorNumber;

    private String jarNumber;

    private Float fabricCount;

    public Integer getInnerBoardStorageID() {
        return innerBoardStorageID;
    }

    public void setInnerBoardStorageID(Integer innerBoardStorageID) {
        this.innerBoardStorageID = innerBoardStorageID;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getVersionNumber() {
        return versionNumber;
    }

    public void setVersionNumber(String versionNumber) {
        this.versionNumber = versionNumber;
    }

    public String getFabricNumber() {
        return fabricNumber;
    }

    public void setFabricNumber(String fabricNumber) {
        this.fabricNumber = fabricNumber;
    }

    public String getColorNumber() {
        return colorNumber;
    }

    public void setColorNumber(String colorNumber) {
        this.colorNumber = colorNumber;
    }

    public String getJarNumber() {
        return jarNumber;
    }

    public void setJarNumber(String jarNumber) {
        this.jarNumber = jarNumber;
    }

    public Float getFabricCount() {
        return fabricCount;
    }

    public void setFabricCount(Float fabricCount) {
        this.fabricCount = fabricCount;
    }

    public InnerBoardStorage() {
    }

    public InnerBoardStorage(String location, String versionNumber, String fabricNumber, String colorNumber, String jarNumber, Float fabricCount) {
        this.location = location;
        this.versionNumber = versionNumber;
        this.fabricNumber = fabricNumber;
        this.colorNumber = colorNumber;
        this.jarNumber = jarNumber;
        this.fabricCount = fabricCount;
    }

    public InnerBoardStorage(Integer innerBoardStorageID, String location, String versionNumber, String fabricNumber, String colorNumber, String jarNumber, Float fabricCount) {
        this.innerBoardStorageID = innerBoardStorageID;
        this.location = location;
        this.versionNumber = versionNumber;
        this.fabricNumber = fabricNumber;
        this.colorNumber = colorNumber;
        this.jarNumber = jarNumber;
        this.fabricCount = fabricCount;
    }
}
