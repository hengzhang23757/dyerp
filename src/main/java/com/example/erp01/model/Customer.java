package com.example.erp01.model;

public class Customer {

    private Integer id;

    private String customerName;

    private String customerCode;

    private String companyName;

    private String linkmanName;

    private String linkmanPhone;

    private String companyAddress;

    private String changeCustomerCode;

    private String changeCustomerName;


    public Customer() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getLinkmanName() {
        return linkmanName;
    }

    public void setLinkmanName(String linkmanName) {
        this.linkmanName = linkmanName;
    }

    public String getLinkmanPhone() {
        return linkmanPhone;
    }

    public void setLinkmanPhone(String linkmanPhone) {
        this.linkmanPhone = linkmanPhone;
    }

    public String getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getChangeCustomerCode() {
        return changeCustomerCode;
    }

    public void setChangeCustomerCode(String changeCustomerCode) {
        this.changeCustomerCode = changeCustomerCode;
    }

    public String getChangeCustomerName() {
        return changeCustomerName;
    }

    public void setChangeCustomerName(String changeCustomerName) {
        this.changeCustomerName = changeCustomerName;
    }
}
