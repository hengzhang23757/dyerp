package com.example.erp01.model;

public class MiniTailorLayerInfo {

    private String colorName;

    private String jarName;

    private Integer layerCount;

    private Integer batch;

    private Float weight;

    public Integer getBatch() {
        return batch;
    }

    public void setBatch(Integer batch) {
        this.batch = batch;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getJarName() {
        return jarName;
    }

    public void setJarName(String jarName) {
        this.jarName = jarName;
    }

    public Integer getLayerCount() {
        return layerCount;
    }

    public void setLayerCount(Integer layerCount) {
        this.layerCount = layerCount;
    }

    public Float getWeight() {
        return weight;
    }

    public void setWeight(Float weight) {
        this.weight = weight;
    }

    public MiniTailorLayerInfo() {
    }

    public MiniTailorLayerInfo(String colorName, String jarName, Integer layerCount, Integer batch, Float weight) {
        this.colorName = colorName;
        this.jarName = jarName;
        this.layerCount = layerCount;
        this.batch = batch;
        this.weight = weight;
    }
}
