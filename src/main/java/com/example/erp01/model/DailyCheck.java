package com.example.erp01.model;

import java.util.Date;

public class DailyCheck {

    private Integer id;

    private String employeeName;

    private String employeeNumber;

    private String groupName;

    private String useraccount;

    private Date date;

    private String work;

    private String offwork;

    private Integer checkin;

    private Integer checkout;

    private Integer late;

    private Float lateHour;

    private Integer leaveearly;

    private Float leaveearlyHour;

    private Integer worktime;

    private Float worktimeHour;

    private Integer realtime;

    private Float realtimeHour;

    private Integer absent;

    private Float absentHour;

    private Date createTime;

    private Date updateTime;

    public String getUseraccount() {
        return useraccount;
    }

    public void setUseraccount(String useraccount) {
        this.useraccount = useraccount;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getWork() {
        return work;
    }

    public void setWork(String work) {
        this.work = work;
    }

    public String getOffwork() {
        return offwork;
    }

    public void setOffwork(String offwork) {
        this.offwork = offwork;
    }

    public Integer getCheckin() {
        return checkin;
    }

    public void setCheckin(Integer checkin) {
        this.checkin = checkin;
    }

    public Integer getCheckout() {
        return checkout;
    }

    public void setCheckout(Integer checkout) {
        this.checkout = checkout;
    }

    public Integer getLate() {
        return late;
    }

    public void setLate(Integer late) {
        this.late = late;
    }

    public Integer getLeaveearly() {
        return leaveearly;
    }

    public void setLeaveearly(Integer leaveearly) {
        this.leaveearly = leaveearly;
    }

    public Integer getWorktime() {
        return worktime;
    }

    public void setWorktime(Integer worktime) {
        this.worktime = worktime;
    }

    public Integer getRealtime() {
        return realtime;
    }

    public void setRealtime(Integer realtime) {
        this.realtime = realtime;
    }

    public Integer getAbsent() {
        return absent;
    }

    public void setAbsent(Integer absent) {
        this.absent = absent;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getEmployeeNumber() {
        return employeeNumber;
    }

    public void setEmployeeNumber(String employeeNumber) {
        this.employeeNumber = employeeNumber;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Float getLateHour() {
        return lateHour;
    }

    public void setLateHour(Float lateHour) {
        this.lateHour = lateHour;
    }

    public Float getLeaveearlyHour() {
        return leaveearlyHour;
    }

    public void setLeaveearlyHour(Float leaveearlyHour) {
        this.leaveearlyHour = leaveearlyHour;
    }

    public Float getWorktimeHour() {
        return worktimeHour;
    }

    public void setWorktimeHour(Float worktimeHour) {
        this.worktimeHour = worktimeHour;
    }

    public Float getRealtimeHour() {
        return realtimeHour;
    }

    public void setRealtimeHour(Float realtimeHour) {
        this.realtimeHour = realtimeHour;
    }

    public Float getAbsentHour() {
        return absentHour;
    }

    public void setAbsentHour(Float absentHour) {
        this.absentHour = absentHour;
    }

    public DailyCheck() {
    }

    @Override
    public String toString() {
        return "DailyCheck{" +
                "employeeName='" + employeeName + '\'' +
                ", employeeNumber='" + employeeNumber + '\'' +
                ", groupName='" + groupName + '\'' +
                ", useraccount='" + useraccount + '\'' +
                ", date=" + date +
                ", work='" + work + '\'' +
                ", offwork='" + offwork + '\'' +
                ", checkin=" + checkin +
                ", checkout=" + checkout +
                ", late=" + late +
                ", leaveearly=" + leaveearly +
                ", worktime=" + worktime +
                ", realtime=" + realtime +
                ", absent=" + absent +
                '}';
    }
}
