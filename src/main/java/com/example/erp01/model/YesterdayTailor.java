package com.example.erp01.model;

public class YesterdayTailor {

    private String clothesVersionNumber;

    private String orderName;

    private Integer orderCount;

    private Integer tailorCount;

    private Integer yesterdayCount;

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public Integer getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(Integer orderCount) {
        this.orderCount = orderCount;
    }

    public Integer getTailorCount() {
        return tailorCount;
    }

    public void setTailorCount(Integer tailorCount) {
        this.tailorCount = tailorCount;
    }

    public Integer getYesterdayCount() {
        return yesterdayCount;
    }

    public void setYesterdayCount(Integer yesterdayCount) {
        this.yesterdayCount = yesterdayCount;
    }

    public YesterdayTailor() {
    }

    public YesterdayTailor(String clothesVersionNumber, String orderName, Integer orderCount, Integer tailorCount, Integer yesterdayCount) {
        this.clothesVersionNumber = clothesVersionNumber;
        this.orderName = orderName;
        this.orderCount = orderCount;
        this.tailorCount = tailorCount;
        this.yesterdayCount = yesterdayCount;
    }
}
