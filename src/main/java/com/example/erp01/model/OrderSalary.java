package com.example.erp01.model;

public class OrderSalary {

    private String orderName;

    private String clothesVersionNumber;

    private String monthString;

    private Integer procedureNumber;

    private String procedureName;

    private Double priceSalary = 0d;

    private Double pieceSalaryTwo = 0d;

    private Float addition = 0f;

    private Double sumPrice = 0d;

    private Integer orderCount = 0;

    private Integer cutCount = 0;

    private Integer wellCount = 0;

    private Integer amongCutCount = 0;

    private Float pieceCount = 0f;

    private Float amongPieceCount = 0f;

    private Double sumSalary = 0d;

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public Float getPieceCount() {
        return pieceCount;
    }

    public void setPieceCount(Float pieceCount) {
        this.pieceCount = pieceCount;
    }

    public Float getAmongPieceCount() {
        return amongPieceCount;
    }

    public void setAmongPieceCount(Float amongPieceCount) {
        this.amongPieceCount = amongPieceCount;
    }

    public Integer getCutCount() {
        return cutCount;
    }

    public void setCutCount(Integer cutCount) {
        this.cutCount = cutCount;
    }

    public Integer getAmongCutCount() {
        return amongCutCount;
    }

    public void setAmongCutCount(Integer amongCutCount) {
        this.amongCutCount = amongCutCount;
    }

    public Integer getProcedureNumber() {
        return procedureNumber;
    }

    public void setProcedureNumber(Integer procedureNumber) {
        this.procedureNumber = procedureNumber;
    }

    public String getProcedureName() {
        return procedureName;
    }

    public void setProcedureName(String procedureName) {
        this.procedureName = procedureName;
    }

    public Double getPriceSalary() {
        return priceSalary;
    }

    public void setPriceSalary(Double priceSalary) {
        this.priceSalary = priceSalary;
    }

    public Double getPieceSalaryTwo() {
        return pieceSalaryTwo;
    }

    public void setPieceSalaryTwo(Double pieceSalaryTwo) {
        this.pieceSalaryTwo = pieceSalaryTwo;
    }

    public Float getAddition() {
        return addition;
    }

    public void setAddition(Float addition) {
        this.addition = addition;
    }

    public Double getSumPrice() {
        return sumPrice;
    }

    public void setSumPrice(Double sumPrice) {
        this.sumPrice = sumPrice;
    }

    public Integer getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(Integer orderCount) {
        this.orderCount = orderCount;
    }

    public Integer getWellCount() {
        return wellCount;
    }

    public void setWellCount(Integer wellCount) {
        this.wellCount = wellCount;
    }

    public Double getSumSalary() {
        return sumSalary;
    }

    public void setSumSalary(Double sumSalary) {
        this.sumSalary = sumSalary;
    }

    public String getMonthString() {
        return monthString;
    }

    public void setMonthString(String monthString) {
        this.monthString = monthString;
    }

    public OrderSalary() {
    }

    public OrderSalary(String orderName, String clothesVersionNumber, Integer procedureNumber, String procedureName) {
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.procedureNumber = procedureNumber;
        this.procedureName = procedureName;
    }

    public OrderSalary(String orderName, String clothesVersionNumber, Integer procedureNumber, String procedureName, Double priceSalary, Double pieceSalaryTwo, Float addition, Double sumPrice, Integer orderCount, Integer wellCount, Float pieceCount, Double sumSalary) {
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.procedureNumber = procedureNumber;
        this.procedureName = procedureName;
        this.priceSalary = priceSalary;
        this.pieceSalaryTwo = pieceSalaryTwo;
        this.addition = addition;
        this.sumPrice = sumPrice;
        this.orderCount = orderCount;
        this.wellCount = wellCount;
        this.pieceCount = pieceCount;
        this.sumSalary = sumSalary;
    }
}
