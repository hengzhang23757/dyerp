package com.example.erp01.model;

public class OutputReport {

    private String orderName;

    private String colorName;

    private String sizeName;

    private String procedureCode;

    private Integer procedureNumber;

    private Integer outputCount;

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public String getProcedureCode() {
        return procedureCode;
    }

    public void setProcedureCode(String procedureCode) {
        this.procedureCode = procedureCode;
    }

    public Integer getProcedureNumber() {
        return procedureNumber;
    }

    public void setProcedureNumber(Integer procedureNumber) {
        this.procedureNumber = procedureNumber;
    }

    public Integer getOutputCount() {
        return outputCount;
    }

    public void setOutputCount(Integer outputCount) {
        this.outputCount = outputCount;
    }

    public OutputReport() {
    }

    public OutputReport(String orderName, String colorName, String sizeName, String procedureCode, Integer procedureNumber, Integer outputCount) {
        this.orderName = orderName;
        this.colorName = colorName;
        this.sizeName = sizeName;
        this.procedureCode = procedureCode;
        this.procedureNumber = procedureNumber;
        this.outputCount = outputCount;
    }
}
