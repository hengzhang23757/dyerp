package com.example.erp01.model;

import java.io.Serializable;
import java.util.Date;

public class XhlPieceWork implements Serializable {

    private Integer pieceWorkID;

    private String customerName;

    private String orderName;

    private String printingPart;

    private String colorName;

    private String sizeName;

    private Integer bedNumber;

    private Integer packageNumber;

    private Integer layerCount;

    private Integer procedureNumber;

    private String procedureName;

    private Integer tailorQcode;

    private String employeeNumber;

    private String employeeName;

    private String xhlOrderName;

    private Integer defectiveCount;

    private Integer rottenCount;

    private Integer lackCount;

    private String inspectionOrderName;

    private String outOrderName;

    private Date pieceDate;

    private Date createTime;

    private String outLock;

    public String getOutLock() {
        return outLock;
    }

    public void setOutLock(String outLock) {
        this.outLock = outLock;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getPieceDate() {
        return pieceDate;
    }

    public void setPieceDate(Date pieceDate) {
        this.pieceDate = pieceDate;
    }

    public Integer getDefectiveCount() {
        return defectiveCount;
    }

    public void setDefectiveCount(Integer defectiveCount) {
        this.defectiveCount = defectiveCount;
    }

    public Integer getRottenCount() {
        return rottenCount;
    }

    public void setRottenCount(Integer rottenCount) {
        this.rottenCount = rottenCount;
    }

    public Integer getLackCount() {
        return lackCount;
    }

    public void setLackCount(Integer lackCount) {
        this.lackCount = lackCount;
    }

    public String getInspectionOrderName() {
        return inspectionOrderName;
    }

    public void setInspectionOrderName(String inspectionOrderName) {
        this.inspectionOrderName = inspectionOrderName;
    }

    public String getOutOrderName() {
        return outOrderName;
    }

    public void setOutOrderName(String outOrderName) {
        this.outOrderName = outOrderName;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getXhlOrderName() {
        return xhlOrderName;
    }

    public void setXhlOrderName(String xhlOrderName) {
        this.xhlOrderName = xhlOrderName;
    }

    public String getProcedureName() {
        return procedureName;
    }

    public void setProcedureName(String procedureName) {
        this.procedureName = procedureName;
    }

    public Integer getPieceWorkID() {
        return pieceWorkID;
    }

    public void setPieceWorkID(Integer pieceWorkID) {
        this.pieceWorkID = pieceWorkID;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getPrintingPart() {
        return printingPart;
    }

    public void setPrintingPart(String printingPart) {
        this.printingPart = printingPart;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public Integer getBedNumber() {
        return bedNumber;
    }

    public void setBedNumber(Integer bedNumber) {
        this.bedNumber = bedNumber;
    }

    public Integer getPackageNumber() {
        return packageNumber;
    }

    public void setPackageNumber(Integer packageNumber) {
        this.packageNumber = packageNumber;
    }

    public Integer getLayerCount() {
        return layerCount;
    }

    public void setLayerCount(Integer layerCount) {
        this.layerCount = layerCount;
    }

    public Integer getProcedureNumber() {
        return procedureNumber;
    }

    public void setProcedureNumber(Integer procedureNumber) {
        this.procedureNumber = procedureNumber;
    }

    public Integer getTailorQcode() {
        return tailorQcode;
    }

    public void setTailorQcode(Integer tailorQcode) {
        this.tailorQcode = tailorQcode;
    }

    public String getEmployeeNumber() {
        return employeeNumber;
    }

    public void setEmployeeNumber(String employeeNumber) {
        this.employeeNumber = employeeNumber;
    }

    public XhlPieceWork() {
    }

    public XhlPieceWork(String customerName, String orderName, String printingPart, String colorName, String sizeName, Integer bedNumber, Integer packageNumber, Integer layerCount, Integer procedureNumber, String procedureName, Integer tailorQcode, String employeeNumber, String employeeName, String xhlOrderName) {
        this.customerName = customerName;
        this.orderName = orderName;
        this.printingPart = printingPart;
        this.colorName = colorName;
        this.sizeName = sizeName;
        this.bedNumber = bedNumber;
        this.packageNumber = packageNumber;
        this.layerCount = layerCount;
        this.procedureNumber = procedureNumber;
        this.procedureName = procedureName;
        this.tailorQcode = tailorQcode;
        this.employeeNumber = employeeNumber;
        this.employeeName = employeeName;
        this.xhlOrderName = xhlOrderName;
    }

    public XhlPieceWork(String customerName, String orderName, String printingPart, String colorName, String sizeName, Integer bedNumber, Integer packageNumber, Integer layerCount, Integer procedureNumber, String procedureName, Integer tailorQcode, String employeeNumber, String employeeName, String xhlOrderName, String outOrderName) {
        this.customerName = customerName;
        this.orderName = orderName;
        this.printingPart = printingPart;
        this.colorName = colorName;
        this.sizeName = sizeName;
        this.bedNumber = bedNumber;
        this.packageNumber = packageNumber;
        this.layerCount = layerCount;
        this.procedureNumber = procedureNumber;
        this.procedureName = procedureName;
        this.tailorQcode = tailorQcode;
        this.employeeNumber = employeeNumber;
        this.employeeName = employeeName;
        this.xhlOrderName = xhlOrderName;
        this.outOrderName = outOrderName;
    }

    public XhlPieceWork(String customerName, String orderName, String printingPart, String colorName, String sizeName, Integer bedNumber, Integer packageNumber, Integer layerCount, Integer procedureNumber, String procedureName, Integer tailorQcode, String employeeNumber, String employeeName, String xhlOrderName, Integer defectiveCount, Integer rottenCount, Integer lackCount, String inspectionOrderName) {
        this.customerName = customerName;
        this.orderName = orderName;
        this.printingPart = printingPart;
        this.colorName = colorName;
        this.sizeName = sizeName;
        this.bedNumber = bedNumber;
        this.packageNumber = packageNumber;
        this.layerCount = layerCount;
        this.procedureNumber = procedureNumber;
        this.procedureName = procedureName;
        this.tailorQcode = tailorQcode;
        this.employeeNumber = employeeNumber;
        this.employeeName = employeeName;
        this.xhlOrderName = xhlOrderName;
        this.defectiveCount = defectiveCount;
        this.rottenCount = rottenCount;
        this.lackCount = lackCount;
        this.inspectionOrderName = inspectionOrderName;
    }
}
