package com.example.erp01.model;

public class InnerBoardOutRecord {

    private Integer innerBoardOutRecordID;

    private String location;

    private String versionNumber;

    private String fabricNumber;

    private String colorNumber;

    private String jarNumber;

    private Float fabricCount;

    private String operateType;

    public Integer getInnerBoardOutRecordID() {
        return innerBoardOutRecordID;
    }

    public void setInnerBoardOutRecordID(Integer innerBoardOutRecordID) {
        this.innerBoardOutRecordID = innerBoardOutRecordID;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getVersionNumber() {
        return versionNumber;
    }

    public void setVersionNumber(String versionNumber) {
        this.versionNumber = versionNumber;
    }

    public String getFabricNumber() {
        return fabricNumber;
    }

    public void setFabricNumber(String fabricNumber) {
        this.fabricNumber = fabricNumber;
    }

    public String getColorNumber() {
        return colorNumber;
    }

    public void setColorNumber(String colorNumber) {
        this.colorNumber = colorNumber;
    }

    public String getJarNumber() {
        return jarNumber;
    }

    public void setJarNumber(String jarNumber) {
        this.jarNumber = jarNumber;
    }

    public Float getFabricCount() {
        return fabricCount;
    }

    public void setFabricCount(Float fabricCount) {
        this.fabricCount = fabricCount;
    }

    public String getOperateType() {
        return operateType;
    }

    public void setOperateType(String operateType) {
        this.operateType = operateType;
    }

    public InnerBoardOutRecord() {
    }

    public InnerBoardOutRecord(String location, String versionNumber, String fabricNumber, String colorNumber, String jarNumber, Float fabricCount, String operateType) {
        this.location = location;
        this.versionNumber = versionNumber;
        this.fabricNumber = fabricNumber;
        this.colorNumber = colorNumber;
        this.jarNumber = jarNumber;
        this.fabricCount = fabricCount;
        this.operateType = operateType;
    }

    public InnerBoardOutRecord(Integer innerBoardOutRecordID, String location, String versionNumber, String fabricNumber, String colorNumber, String jarNumber, Float fabricCount, String operateType) {
        this.innerBoardOutRecordID = innerBoardOutRecordID;
        this.location = location;
        this.versionNumber = versionNumber;
        this.fabricNumber = fabricNumber;
        this.colorNumber = colorNumber;
        this.jarNumber = jarNumber;
        this.fabricCount = fabricCount;
        this.operateType = operateType;
    }
}
