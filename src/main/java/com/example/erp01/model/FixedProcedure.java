package com.example.erp01.model;

public class FixedProcedure {

    private Integer id;

    private Integer procedureNumber;

    private String procedureName;

    private String procedureSection;

    private String scanPart;

    public FixedProcedure() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getProcedureNumber() {
        return procedureNumber;
    }

    public void setProcedureNumber(Integer procedureNumber) {
        this.procedureNumber = procedureNumber;
    }

    public String getProcedureName() {
        return procedureName;
    }

    public void setProcedureName(String procedureName) {
        this.procedureName = procedureName;
    }

    public String getProcedureSection() {
        return procedureSection;
    }

    public void setProcedureSection(String procedureSection) {
        this.procedureSection = procedureSection;
    }

    public String getScanPart() {
        return scanPart;
    }

    public void setScanPart(String scanPart) {
        this.scanPart = scanPart;
    }
}
