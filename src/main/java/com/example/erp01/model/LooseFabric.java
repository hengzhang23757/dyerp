package com.example.erp01.model;

import java.util.Date;

public class LooseFabric {

    private Integer looseFabricID;

    private String orderName;

    private String clothesVersionNumber;

    private String colorName;

    private String jarNumber;

    private Integer batchOrder;

    private String batchNumber;

    private Integer totalBatch;

    private Float weight;

    private String fabricName;

    private String fabricColor;

    private Integer qCodeID;

    private String operateType;

    private String fromLocation;

    private String remark;

    private Date looseTime;

    private Integer looseHour;

    private Integer printTimes = 0;

    private String unit;

    private Integer cutState = 0;

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getBatchNumber() {
        return batchNumber;
    }

    public void setBatchNumber(String batchNumber) {
        this.batchNumber = batchNumber;
    }

    public String getFromLocation() {
        return fromLocation;
    }

    public void setFromLocation(String fromLocation) {
        this.fromLocation = fromLocation;
    }

    public Integer getPrintTimes() {
        return printTimes;
    }

    public void setPrintTimes(Integer printTimes) {
        this.printTimes = printTimes;
    }

    public Float getWeight() {
        return weight;
    }

    public void setWeight(Float weight) {
        this.weight = weight;
    }

    public Integer getLooseHour() {
        return looseHour;
    }

    public void setLooseHour(Integer looseHour) {
        this.looseHour = looseHour;
    }

    public Date getLooseTime() {
        return looseTime;
    }

    public void setLooseTime(Date looseTime) {
        this.looseTime = looseTime;
    }

    public String getOperateType() {
        return operateType;
    }

    public void setOperateType(String operateType) {
        this.operateType = operateType;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getLooseFabricID() {
        return looseFabricID;
    }

    public void setLooseFabricID(Integer looseFabricID) {
        this.looseFabricID = looseFabricID;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }


    public Integer getTotalBatch() {
        return totalBatch;
    }

    public void setTotalBatch(Integer totalBatch) {
        this.totalBatch = totalBatch;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getJarNumber() {
        return jarNumber;
    }

    public void setJarNumber(String jarNumber) {
        this.jarNumber = jarNumber;
    }

    public Integer getBatchOrder() {
        return batchOrder;
    }

    public void setBatchOrder(Integer batchOrder) {
        this.batchOrder = batchOrder;
    }

    public String getFabricName() {
        return fabricName;
    }

    public void setFabricName(String fabricName) {
        this.fabricName = fabricName;
    }

    public String getFabricColor() {
        return fabricColor;
    }

    public void setFabricColor(String fabricColor) {
        this.fabricColor = fabricColor;
    }

    public Integer getqCodeID() {
        return qCodeID;
    }

    public void setqCodeID(Integer qCodeID) {
        this.qCodeID = qCodeID;
    }

    public Integer getCutState() {
        return cutState;
    }

    public void setCutState(Integer cutState) {
        this.cutState = cutState;
    }

    public LooseFabric() {
    }

    public LooseFabric(String orderName, String clothesVersionNumber, String colorName, String jarNumber, Integer batchOrder, String batchNumber, Integer totalBatch, Float weight, String fabricName, String fabricColor, Integer qCodeID, String operateType, String fromLocation, String remark, Date looseTime, Integer looseHour, Integer printTimes, String unit) {
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.colorName = colorName;
        this.jarNumber = jarNumber;
        this.batchOrder = batchOrder;
        this.batchNumber = batchNumber;
        this.totalBatch = totalBatch;
        this.weight = weight;
        this.fabricName = fabricName;
        this.fabricColor = fabricColor;
        this.qCodeID = qCodeID;
        this.operateType = operateType;
        this.fromLocation = fromLocation;
        this.remark = remark;
        this.looseTime = looseTime;
        this.looseHour = looseHour;
        this.printTimes = printTimes;
        this.unit = unit;
    }

    public LooseFabric(Integer looseFabricID, String orderName, String clothesVersionNumber, String colorName, String jarNumber, Integer batchOrder, String batchNumber, Integer totalBatch, Float weight, String fabricName, String fabricColor, Integer qCodeID, String operateType, String fromLocation, String remark, Date looseTime, Integer looseHour, Integer printTimes, String unit) {
        this.looseFabricID = looseFabricID;
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.colorName = colorName;
        this.jarNumber = jarNumber;
        this.batchOrder = batchOrder;
        this.batchNumber = batchNumber;
        this.totalBatch = totalBatch;
        this.weight = weight;
        this.fabricName = fabricName;
        this.fabricColor = fabricColor;
        this.qCodeID = qCodeID;
        this.operateType = operateType;
        this.fromLocation = fromLocation;
        this.remark = remark;
        this.looseTime = looseTime;
        this.looseHour = looseHour;
        this.printTimes = printTimes;
        this.unit = unit;
    }
}
