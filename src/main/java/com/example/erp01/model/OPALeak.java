package com.example.erp01.model;

public class OPALeak {

    private String orderName;

    private String clothesVersionNumber;

    private Integer bedNumber;

    private String partName;

    private String colorName;

    private String sizeName;

    private Integer orderCount = 0;

    private Integer cutCount = 0;

    private Integer wellCount = 0;

    private Integer opaCount = 0;

    private Integer otherOPACount = 0;

    private Integer pieceCount = 0;

    private Integer opaBackCount = 0;

    private Integer regionBack = 0;

    private Integer diffCount = 0;

    private Integer defectiveCount = 0;

    private Integer rottenCount = 0;

    private Integer lackCount = 0;

    public Integer getOtherOPACount() {
        return otherOPACount;
    }

    public void setOtherOPACount(Integer otherOPACount) {
        this.otherOPACount = otherOPACount;
    }

    public Integer getBedNumber() {
        return bedNumber;
    }

    public void setBedNumber(Integer bedNumber) {
        this.bedNumber = bedNumber;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public String getPartName() {
        return partName;
    }

    public void setPartName(String partName) {
        this.partName = partName;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public Integer getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(Integer orderCount) {
        this.orderCount = orderCount;
    }

    public Integer getCutCount() {
        return cutCount;
    }

    public void setCutCount(Integer cutCount) {
        this.cutCount = cutCount;
    }

    public Integer getWellCount() {
        return wellCount;
    }

    public void setWellCount(Integer wellCount) {
        this.wellCount = wellCount;
    }

    public Integer getOpaCount() {
        return opaCount;
    }

    public void setOpaCount(Integer opaCount) {
        this.opaCount = opaCount;
    }

    public Integer getOpaBackCount() {
        return opaBackCount;
    }

    public void setOpaBackCount(Integer opaBackCount) {
        this.opaBackCount = opaBackCount;
    }

    public Integer getDiffCount() {
        return diffCount;
    }

    public void setDiffCount(Integer diffCount) {
        this.diffCount = diffCount;
    }


    public Integer getPieceCount() {
        return pieceCount;
    }

    public void setPieceCount(Integer pieceCount) {
        this.pieceCount = pieceCount;
    }

    public Integer getDefectiveCount() {
        return defectiveCount;
    }

    public void setDefectiveCount(Integer defectiveCount) {
        this.defectiveCount = defectiveCount;
    }

    public Integer getRottenCount() {
        return rottenCount;
    }

    public void setRottenCount(Integer rottenCount) {
        this.rottenCount = rottenCount;
    }

    public Integer getLackCount() {
        return lackCount;
    }

    public void setLackCount(Integer lackCount) {
        this.lackCount = lackCount;
    }

    public Integer getRegionBack() {
        return regionBack;
    }

    public void setRegionBack(Integer regionBack) {
        this.regionBack = regionBack;
    }

    public OPALeak() {
    }

    public OPALeak(String orderName, String clothesVersionNumber, Integer bedNumber, String partName, String colorName, String sizeName, Integer orderCount, Integer cutCount, Integer wellCount, Integer opaCount, Integer otherOPACount, Integer opaBackCount, Integer diffCount) {
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.bedNumber = bedNumber;
        this.partName = partName;
        this.colorName = colorName;
        this.sizeName = sizeName;
        this.orderCount = orderCount;
        this.cutCount = cutCount;
        this.wellCount = wellCount;
        this.opaCount = opaCount;
        this.otherOPACount = otherOPACount;
        this.opaBackCount = opaBackCount;
        this.diffCount = diffCount;
    }

    public OPALeak(String orderName, String clothesVersionNumber, String partName, String colorName, String sizeName, Integer wellCount, Integer opaCount, Integer pieceCount, Integer opaBackCount, Integer regionBack, Integer diffCount, Integer defectiveCount, Integer rottenCount, Integer lackCount) {
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.partName = partName;
        this.colorName = colorName;
        this.sizeName = sizeName;
        this.wellCount = wellCount;
        this.opaCount = opaCount;
        this.pieceCount = pieceCount;
        this.opaBackCount = opaBackCount;
        this.regionBack = regionBack;
        this.diffCount = diffCount;
        this.defectiveCount = defectiveCount;
        this.rottenCount = rottenCount;
        this.lackCount = lackCount;
    }
}
