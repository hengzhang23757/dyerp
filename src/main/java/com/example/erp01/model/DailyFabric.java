package com.example.erp01.model;

import java.util.Date;

public class DailyFabric {

    private Integer dailyFabricID;

    private String orderName;

    private String clothesVersionNumber;

    private String colorName;

    private String jarNumber;

    private String fabricName;

    private String fabricColor;

    private Integer batchCount;

    private Float totalWeight;

    private Date fabricDate;

    public Date getFabricDate() {
        return fabricDate;
    }

    public void setFabricDate(Date fabricDate) {
        this.fabricDate = fabricDate;
    }

    public DailyFabric() {
    }

    public DailyFabric(String orderName, String clothesVersionNumber, String colorName, String jarNumber, String fabricName, String fabricColor, Integer batchCount, Float totalWeight) {
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.colorName = colorName;
        this.jarNumber = jarNumber;
        this.fabricName = fabricName;
        this.fabricColor = fabricColor;
        this.batchCount = batchCount;
        this.totalWeight = totalWeight;
    }

    public DailyFabric(Integer dailyFabricID, String orderName, String clothesVersionNumber, String colorName, String jarNumber, String fabricName, String fabricColor, Integer batchCount, Float totalWeight) {
        this.dailyFabricID = dailyFabricID;
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.colorName = colorName;
        this.jarNumber = jarNumber;
        this.fabricName = fabricName;
        this.fabricColor = fabricColor;
        this.batchCount = batchCount;
        this.totalWeight = totalWeight;
    }

    public Integer getDailyFabricID() {
        return dailyFabricID;
    }

    public void setDailyFabricID(Integer dailyFabricID) {
        this.dailyFabricID = dailyFabricID;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getJarNumber() {
        return jarNumber;
    }

    public void setJarNumber(String jarNumber) {
        this.jarNumber = jarNumber;
    }

    public String getFabricName() {
        return fabricName;
    }

    public void setFabricName(String fabricName) {
        this.fabricName = fabricName;
    }

    public String getFabricColor() {
        return fabricColor;
    }

    public void setFabricColor(String fabricColor) {
        this.fabricColor = fabricColor;
    }

    public Integer getBatchCount() {
        return batchCount;
    }

    public void setBatchCount(Integer batchCount) {
        this.batchCount = batchCount;
    }

    public Float getTotalWeight() {
        return totalWeight;
    }

    public void setTotalWeight(Float totalWeight) {
        this.totalWeight = totalWeight;
    }
}
