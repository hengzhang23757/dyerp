package com.example.erp01.model;

public class PieceWorkSummary {

    private Integer packageCount;

    private Integer pieceCount;

    private float sumSalary;

    public Integer getPackageCount() {
        return packageCount;
    }

    public void setPackageCount(Integer packageCount) {
        this.packageCount = packageCount;
    }

    public Integer getPieceCount() {
        return pieceCount;
    }

    public void setPieceCount(Integer pieceCount) {
        this.pieceCount = pieceCount;
    }

    public float getSumSalary() {
        return sumSalary;
    }

    public void setSumSalary(float sumSalary) {
        this.sumSalary = sumSalary;
    }

    public PieceWorkSummary() {
    }

    public PieceWorkSummary(Integer packageCount, Integer pieceCount, float sumSalary) {
        this.packageCount = packageCount;
        this.pieceCount = pieceCount;
        this.sumSalary = sumSalary;
    }
}
