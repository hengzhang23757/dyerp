package com.example.erp01.model;

public class UnitConsumption {

    private String orderName;

    private String clothesVersionNumber;

    private Integer bedNumber;

    private String cutTime;

    private String colorName;

    private String sizeName;

    private Integer layerCount;

    private String groupName;

    private String jarName;

    private Float totalWeight = 0f;

    private Integer totalBatch = 0;

    private Float planConsumption = 0f;

    private Float averageConsumption = 0f;

    private Float totalReturnCount = 0f;

    private Float actTotalConsumption = 0f;

    public Float getActTotalConsumption() {
        return actTotalConsumption;
    }

    public void setActTotalConsumption(Float actTotalConsumption) {
        this.actTotalConsumption = actTotalConsumption;
    }

    public Float getTotalReturnCount() {
        return totalReturnCount;
    }

    public void setTotalReturnCount(Float totalReturnCount) {
        this.totalReturnCount = totalReturnCount;
    }

    public String getJarName() {
        return jarName;
    }

    public void setJarName(String jarName) {
        this.jarName = jarName;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public Integer getBedNumber() {
        return bedNumber;
    }

    public void setBedNumber(Integer bedNumber) {
        this.bedNumber = bedNumber;
    }

    public String getCutTime() {
        return cutTime;
    }

    public void setCutTime(String cutTime) {
        this.cutTime = cutTime;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public Integer getLayerCount() {
        return layerCount;
    }

    public void setLayerCount(Integer layerCount) {
        this.layerCount = layerCount;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public Float getTotalWeight() {
        return totalWeight;
    }

    public void setTotalWeight(Float totalWeight) {
        this.totalWeight = totalWeight;
    }

    public Integer getTotalBatch() {
        return totalBatch;
    }

    public void setTotalBatch(Integer totalBatch) {
        this.totalBatch = totalBatch;
    }

    public Float getPlanConsumption() {
        return planConsumption;
    }

    public void setPlanConsumption(Float planConsumption) {
        this.planConsumption = planConsumption;
    }

    public Float getAverageConsumption() {
        return averageConsumption;
    }

    public void setAverageConsumption(Float averageConsumption) {
        this.averageConsumption = averageConsumption;
    }

    public UnitConsumption() {
    }
}
