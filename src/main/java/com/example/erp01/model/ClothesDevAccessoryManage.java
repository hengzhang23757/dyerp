package com.example.erp01.model;

import java.util.Date;

public class ClothesDevAccessoryManage {

    private Integer id;

    private Integer accessoryID;

    private String orderName;

    private String clothesVersionNumber;

    private String devAccessoryName;

    private String accessoryName;

    private String accessoryNumber;

    private String specification;

    private String accessoryColor;

    private String accessoryColorNumber;

    private String unit;

    private String accessoryUnit;

    private Float pieceUsage = 0f;

    private Float price = 0f;

    private Float inStoreCount = 0f;

    private Float storageCount = 0f;

    private Float outStoreCount = 0f;

    private String supplier;

    private String storageType;

    private String remark;

    private Date operateDate;

    private String location;

    private String payState;

    private String checkMonth;

    private String checkNumber;

    public ClothesDevAccessoryManage() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAccessoryID() {
        return accessoryID;
    }

    public void setAccessoryID(Integer accessoryID) {
        this.accessoryID = accessoryID;
    }

    public String getDevAccessoryName() {
        return devAccessoryName;
    }

    public void setDevAccessoryName(String devAccessoryName) {
        this.devAccessoryName = devAccessoryName;
    }

    public String getAccessoryName() {
        return accessoryName;
    }

    public void setAccessoryName(String accessoryName) {
        this.accessoryName = accessoryName;
    }

    public String getAccessoryNumber() {
        return accessoryNumber;
    }

    public void setAccessoryNumber(String accessoryNumber) {
        this.accessoryNumber = accessoryNumber;
    }

    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }

    public String getAccessoryColor() {
        return accessoryColor;
    }

    public void setAccessoryColor(String accessoryColor) {
        this.accessoryColor = accessoryColor;
    }

    public String getAccessoryColorNumber() {
        return accessoryColorNumber;
    }

    public void setAccessoryColorNumber(String accessoryColorNumber) {
        this.accessoryColorNumber = accessoryColorNumber;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Float getPieceUsage() {
        return pieceUsage;
    }

    public void setPieceUsage(Float pieceUsage) {
        this.pieceUsage = pieceUsage;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Float getInStoreCount() {
        return inStoreCount;
    }

    public void setInStoreCount(Float inStoreCount) {
        this.inStoreCount = inStoreCount;
    }

    public Float getStorageCount() {
        return storageCount;
    }

    public void setStorageCount(Float storageCount) {
        this.storageCount = storageCount;
    }

    public Float getOutStoreCount() {
        return outStoreCount;
    }

    public void setOutStoreCount(Float outStoreCount) {
        this.outStoreCount = outStoreCount;
    }

    public String getStorageType() {
        return storageType;
    }

    public void setStorageType(String storageType) {
        this.storageType = storageType;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public String getAccessoryUnit() {
        return accessoryUnit;
    }

    public void setAccessoryUnit(String accessoryUnit) {
        this.accessoryUnit = accessoryUnit;
    }

    public Date getOperateDate() {
        return operateDate;
    }

    public void setOperateDate(Date operateDate) {
        this.operateDate = operateDate;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public String getPayState() {
        return payState;
    }

    public void setPayState(String payState) {
        this.payState = payState;
    }

    public String getCheckMonth() {
        return checkMonth;
    }

    public void setCheckMonth(String checkMonth) {
        this.checkMonth = checkMonth;
    }

    public String getCheckNumber() {
        return checkNumber;
    }

    public void setCheckNumber(String checkNumber) {
        this.checkNumber = checkNumber;
    }

    public ClothesDevAccessoryManage(Integer accessoryID, String accessoryName, String accessoryNumber, String specification, String accessoryColor, String accessoryColorNumber, String unit, Float pieceUsage, Float price, Float inStoreCount, Float storageCount, Float outStoreCount, String supplier, String storageType, String remark, String payState, String location) {
        this.accessoryID = accessoryID;
        this.accessoryName = accessoryName;
        this.accessoryNumber = accessoryNumber;
        this.specification = specification;
        this.accessoryColor = accessoryColor;
        this.accessoryColorNumber = accessoryColorNumber;
        this.unit = unit;
        this.pieceUsage = pieceUsage;
        this.price = price;
        this.inStoreCount = inStoreCount;
        this.storageCount = storageCount;
        this.outStoreCount = outStoreCount;
        this.supplier = supplier;
        this.storageType = storageType;
        this.remark = remark;
        this.payState = payState;
        this.location = location;
    }
}
