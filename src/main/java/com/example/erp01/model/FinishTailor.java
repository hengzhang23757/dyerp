package com.example.erp01.model;

import java.util.Date;

public class FinishTailor {

    private Integer tailorID;

    private String orderName;

    private String clothesVersionNumber;

    private String customerName;

    private Integer bedNumber;

    private String jarName;

    private String colorName;

    private String sizeName;

    private String partName;

    private Integer layerCount;

    private Integer initCount;

    private Integer packageNumber;

    private Integer tailorReportCount;

    private String tailorQcode;

    private Integer tailorQcodeID;

    private String loColor;

    private Integer printTimes = 0;

    private Integer isScan = 0;

    private String shelfName;

    private Date shelfInTime;

    private Integer shelfOut = 0;

    private Date shelfOutTime;

    private String shelfEmp;

    private String shelfOutEmp;

    public Integer getIsScan() {
        return isScan;
    }

    public void setIsScan(Integer isScan) {
        this.isScan = isScan;
    }

    public Integer getPrintTimes() {
        return printTimes;
    }

    public void setPrintTimes(Integer printTimes) {
        this.printTimes = printTimes;
    }

    public String getLoColor() {
        return loColor;
    }

    public void setLoColor(String loColor) {
        this.loColor = loColor;
    }

    public Integer getTailorID() {
        return tailorID;
    }

    public void setTailorID(Integer tailorID) {
        this.tailorID = tailorID;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public Integer getBedNumber() {
        return bedNumber;
    }

    public void setBedNumber(Integer bedNumber) {
        this.bedNumber = bedNumber;
    }

    public String getJarName() {
        return jarName;
    }

    public void setJarName(String jarName) {
        this.jarName = jarName;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public String getPartName() {
        return partName;
    }

    public void setPartName(String partName) {
        this.partName = partName;
    }

    public Integer getLayerCount() {
        return layerCount;
    }

    public void setLayerCount(Integer layerCount) {
        this.layerCount = layerCount;
    }

    public Integer getInitCount() {
        return initCount;
    }

    public void setInitCount(Integer initCount) {
        this.initCount = initCount;
    }

    public Integer getPackageNumber() {
        return packageNumber;
    }

    public void setPackageNumber(Integer packageNumber) {
        this.packageNumber = packageNumber;
    }

    public Integer getTailorReportCount() {
        return tailorReportCount;
    }

    public void setTailorReportCount(Integer tailorReportCount) {
        this.tailorReportCount = tailorReportCount;
    }

    public String getTailorQcode() {
        return tailorQcode;
    }

    public void setTailorQcode(String tailorQcode) {
        this.tailorQcode = tailorQcode;
    }

    public Integer getTailorQcodeID() {
        return tailorQcodeID;
    }

    public void setTailorQcodeID(Integer tailorQcodeID) {
        this.tailorQcodeID = tailorQcodeID;
    }

    public String getShelfName() {
        return shelfName;
    }

    public void setShelfName(String shelfName) {
        this.shelfName = shelfName;
    }

    public Date getShelfInTime() {
        return shelfInTime;
    }

    public void setShelfInTime(Date shelfInTime) {
        this.shelfInTime = shelfInTime;
    }

    public Integer getShelfOut() {
        return shelfOut;
    }

    public void setShelfOut(Integer shelfOut) {
        this.shelfOut = shelfOut;
    }

    public Date getShelfOutTime() {
        return shelfOutTime;
    }

    public void setShelfOutTime(Date shelfOutTime) {
        this.shelfOutTime = shelfOutTime;
    }

    public String getShelfEmp() {
        return shelfEmp;
    }

    public void setShelfEmp(String shelfEmp) {
        this.shelfEmp = shelfEmp;
    }

    public String getShelfOutEmp() {
        return shelfOutEmp;
    }

    public void setShelfOutEmp(String shelfOutEmp) {
        this.shelfOutEmp = shelfOutEmp;
    }

    public FinishTailor() {
    }

    public FinishTailor(String orderName, String clothesVersionNumber, String customerName, Integer bedNumber, String jarName, String colorName, String sizeName, String partName, Integer layerCount, Integer initCount, Integer packageNumber, String tailorQcode, Integer tailorQcodeID, String loColor, Integer printTimes) {
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.customerName = customerName;
        this.bedNumber = bedNumber;
        this.jarName = jarName;
        this.colorName = colorName;
        this.sizeName = sizeName;
        this.partName = partName;
        this.layerCount = layerCount;
        this.initCount = initCount;
        this.packageNumber = packageNumber;
        this.tailorQcode = tailorQcode;
        this.tailorQcodeID = tailorQcodeID;
        this.loColor = loColor;
        this.printTimes = printTimes;
    }

    public FinishTailor(Integer tailorID, Integer layerCount, String tailorQcode, Integer printTimes) {
        this.tailorID = tailorID;
        this.layerCount = layerCount;
        this.tailorQcode = tailorQcode;
        this.printTimes = printTimes;
    }

    public FinishTailor(Integer tailorID, String orderName, String clothesVersionNumber, String customerName, Integer bedNumber, String jarName, String colorName, String sizeName, String partName, Integer layerCount, Integer initCount, Integer packageNumber, String tailorQcode, Integer tailorQcodeID, String loColor, Integer printTimes) {
        this.tailorID = tailorID;
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.customerName = customerName;
        this.bedNumber = bedNumber;
        this.jarName = jarName;
        this.colorName = colorName;
        this.sizeName = sizeName;
        this.partName = partName;
        this.layerCount = layerCount;
        this.initCount = initCount;
        this.packageNumber = packageNumber;
        this.tailorQcode = tailorQcode;
        this.tailorQcodeID = tailorQcodeID;
        this.loColor = loColor;
        this.printTimes = printTimes;
    }
}
