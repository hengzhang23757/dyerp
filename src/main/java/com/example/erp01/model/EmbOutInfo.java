package com.example.erp01.model;

public class EmbOutInfo {

    private String orderName;

    private Integer bedNumber;

    private Integer packageNumber;

    public EmbOutInfo() {
    }

    public EmbOutInfo(String orderName, Integer bedNumber, Integer packageNumber) {
        this.orderName = orderName;
        this.bedNumber = bedNumber;
        this.packageNumber = packageNumber;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public Integer getBedNumber() {
        return bedNumber;
    }

    public void setBedNumber(Integer bedNumber) {
        this.bedNumber = bedNumber;
    }

    public Integer getPackageNumber() {
        return packageNumber;
    }

    public void setPackageNumber(Integer packageNumber) {
        this.packageNumber = packageNumber;
    }
}
