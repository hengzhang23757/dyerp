package com.example.erp01.model;

import java.util.List;

public class EmbOutDto {

    private String groupName;

    private List<Integer> tailorQcodeIDList;

    public EmbOutDto() {
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public List<Integer> getTailorQcodeIDList() {
        return tailorQcodeIDList;
    }

    public void setTailorQcodeIDList(List<Integer> tailorQcodeIDList) {
        this.tailorQcodeIDList = tailorQcodeIDList;
    }
}
