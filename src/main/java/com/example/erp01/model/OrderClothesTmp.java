package com.example.erp01.model;

import java.util.Date;

public class OrderClothesTmp {

    private Integer orderClothesID;

    private String customerName;

    private String purchaseMethod;

    private String orderName;

    private String clothesVersionNumber;

    private String styleDescription;

    private String colorName;

    private String sizeName;

    private Integer count;

    private String season;

    private Date deadLine;

    private String opType;

    private String guid;

    public Integer getOrderClothesID() {
        return orderClothesID;
    }

    public void setOrderClothesID(Integer orderClothesID) {
        this.orderClothesID = orderClothesID;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getPurchaseMethod() {
        return purchaseMethod;
    }

    public void setPurchaseMethod(String purchaseMethod) {
        this.purchaseMethod = purchaseMethod;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public String getStyleDescription() {
        return styleDescription;
    }

    public void setStyleDescription(String styleDescription) {
        this.styleDescription = styleDescription;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }

    public Date getDeadLine() {
        return deadLine;
    }

    public void setDeadLine(Date deadLine) {
        this.deadLine = deadLine;
    }

    public String getOpType() {
        return opType;
    }

    public void setOpType(String opType) {
        this.opType = opType;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public OrderClothesTmp() {
    }

    public OrderClothesTmp(Integer orderClothesID, String customerName, String purchaseMethod, String orderName, String clothesVersionNumber, String styleDescription, String colorName, String sizeName, Integer count, String season, Date deadLine, String opType, String guid) {
        this.orderClothesID = orderClothesID;
        this.customerName = customerName;
        this.purchaseMethod = purchaseMethod;
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.styleDescription = styleDescription;
        this.colorName = colorName;
        this.sizeName = sizeName;
        this.count = count;
        this.season = season;
        this.deadLine = deadLine;
        this.opType = opType;
        this.guid = guid;
    }
}
