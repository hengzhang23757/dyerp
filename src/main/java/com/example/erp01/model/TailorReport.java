package com.example.erp01.model;

public class TailorReport implements Comparable<TailorReport>{

    private String colorName;

    private String sizeName;

    private Integer bedNumber;

    private Integer packageNumber;

    private Integer layerCount;

    private String jarName;

    private Integer packageCount = 0;

    public String getJarName() {
        return jarName;
    }

    public void setJarName(String jarName) {
        this.jarName = jarName;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public Integer getPackageNumber() {
        return packageNumber;
    }

    public void setPackageNumber(Integer packageNumber) {
        this.packageNumber = packageNumber;
    }

    public Integer getLayerCount() {
        return layerCount;
    }

    public void setLayerCount(Integer layerCount) {
        this.layerCount = layerCount;
    }

    public Integer getBedNumber() {
        return bedNumber;
    }

    public void setBedNumber(Integer bedNumber) {
        this.bedNumber = bedNumber;
    }

    public Integer getPackageCount() {
        return packageCount;
    }

    public void setPackageCount(Integer packageCount) {
        this.packageCount = packageCount;
    }

    public TailorReport() {
    }

    public TailorReport(String colorName, String sizeName, Integer packageNumber, Integer layerCount) {
        this.colorName = colorName;
        this.sizeName = sizeName;
        this.packageNumber = packageNumber;
        this.layerCount = layerCount;
    }

    public TailorReport(String colorName, String sizeName, Integer packageNumber, Integer layerCount, String jarName) {
        this.colorName = colorName;
        this.sizeName = sizeName;
        this.packageNumber = packageNumber;
        this.layerCount = layerCount;
        this.jarName = jarName;
    }

    @Override
    public int compareTo(TailorReport o) {
        return this.sizeName.compareTo(o.sizeName);
    }
}
