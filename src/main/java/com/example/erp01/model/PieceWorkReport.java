package com.example.erp01.model;

import java.io.Serializable;
import java.util.Date;

public class PieceWorkReport implements Serializable {

    private Date pieceDate;

    private String customerName;

    private String orderName;

    private Integer bedNumber;

    private String printingPart;

    private String procedureName;

    private String colorName;

    private String sizeName;

    private Integer packageCount;

    private Integer totalLayer;

    private String employeeNumber;

    private String employeeName;

    private Integer inStoreCount;

    private Integer inStorePackage;

    public Integer getInStoreCount() {
        return inStoreCount;
    }

    public void setInStoreCount(Integer inStoreCount) {
        this.inStoreCount = inStoreCount;
    }

    public Integer getInStorePackage() {
        return inStorePackage;
    }

    public void setInStorePackage(Integer inStorePackage) {
        this.inStorePackage = inStorePackage;
    }

    public Date getPieceDate() {
        return pieceDate;
    }

    public void setPieceDate(Date pieceDate) {
        this.pieceDate = pieceDate;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public Integer getBedNumber() {
        return bedNumber;
    }

    public void setBedNumber(Integer bedNumber) {
        this.bedNumber = bedNumber;
    }

    public String getPrintingPart() {
        return printingPart;
    }

    public void setPrintingPart(String printingPart) {
        this.printingPart = printingPart;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public Integer getPackageCount() {
        return packageCount;
    }

    public void setPackageCount(Integer packageCount) {
        this.packageCount = packageCount;
    }

    public Integer getTotalLayer() {
        return totalLayer;
    }

    public void setTotalLayer(Integer totalLayer) {
        this.totalLayer = totalLayer;
    }

    public String getEmployeeNumber() {
        return employeeNumber;
    }

    public void setEmployeeNumber(String employeeNumber) {
        this.employeeNumber = employeeNumber;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getProcedureName() {
        return procedureName;
    }

    public void setProcedureName(String procedureName) {
        this.procedureName = procedureName;
    }

    public PieceWorkReport() {
    }

    public PieceWorkReport(Date pieceDate, String customerName, String orderName, Integer bedNumber, String printingPart, String procedureName, String colorName, String sizeName, Integer packageCount, Integer totalLayer, String employeeNumber, String employeeName) {
        this.pieceDate = pieceDate;
        this.customerName = customerName;
        this.orderName = orderName;
        this.bedNumber = bedNumber;
        this.printingPart = printingPart;
        this.procedureName = procedureName;
        this.colorName = colorName;
        this.sizeName = sizeName;
        this.packageCount = packageCount;
        this.totalLayer = totalLayer;
        this.employeeNumber = employeeNumber;
        this.employeeName = employeeName;
    }

}
