package com.example.erp01.model;

import java.util.Date;

public class ClothesDevFabricManage {

    private Integer id;

    private Integer fabricID;

    private String orderName;

    private String clothesVersionNumber;

    private String devFabricName;

    private String fabricNumber;

    private String fabricName;  //面料名称

    private String fabricWidth;  //布封

    private String fabricWeight; //克重

    private String fabricContent; // 成分

    private String unit;//单位

    private String unitTwo;//多单位

    private Float fabricInCount = 0f;

    private Float fabricStorageCount = 0f;

    private Float fabricOutCount = 0f;

    private Float fabricInCountTwo = 0f;

    private Float fabricStorageCountTwo = 0f;

    private Float fabricOutCountTwo = 0f;

    private Float ratio = 1f;

    private String fabricColor;//面料颜色

    private String fabricColorNumber;

    private String supplier;

    private Float price = 0f;

    private Float priceTwo = 0f;

    private Integer batchNumberIn = 0;

    private Integer batchNumberStorage = 0;

    private Integer batchNumberOut = 0;

    private String storageType;

    private String remark;

    private Date createTime;

    private Date updateTime;

    private Date operateDate;

    private String jarName;

    private String location;

    private String payState;

    private String checkMonth;

    private String checkNumber;

    public ClothesDevFabricManage() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getFabricID() {
        return fabricID;
    }

    public void setFabricID(Integer fabricID) {
        this.fabricID = fabricID;
    }

    public String getDevFabricName() {
        return devFabricName;
    }

    public void setDevFabricName(String devFabricName) {
        this.devFabricName = devFabricName;
    }

    public String getStorageType() {
        return storageType;
    }

    public void setStorageType(String storageType) {
        this.storageType = storageType;
    }

    public String getFabricNumber() {
        return fabricNumber;
    }

    public void setFabricNumber(String fabricNumber) {
        this.fabricNumber = fabricNumber;
    }

    public String getFabricName() {
        return fabricName;
    }

    public void setFabricName(String fabricName) {
        this.fabricName = fabricName;
    }

    public String getFabricWidth() {
        return fabricWidth;
    }

    public void setFabricWidth(String fabricWidth) {
        this.fabricWidth = fabricWidth;
    }

    public String getFabricWeight() {
        return fabricWeight;
    }

    public void setFabricWeight(String fabricWeight) {
        this.fabricWeight = fabricWeight;
    }

    public String getFabricContent() {
        return fabricContent;
    }

    public void setFabricContent(String fabricContent) {
        this.fabricContent = fabricContent;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getUnitTwo() {
        return unitTwo;
    }

    public void setUnitTwo(String unitTwo) {
        this.unitTwo = unitTwo;
    }

    public String getFabricColor() {
        return fabricColor;
    }

    public void setFabricColor(String fabricColor) {
        this.fabricColor = fabricColor;
    }

    public String getFabricColorNumber() {
        return fabricColorNumber;
    }

    public void setFabricColorNumber(String fabricColorNumber) {
        this.fabricColorNumber = fabricColorNumber;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Float getPriceTwo() {
        return priceTwo;
    }

    public void setPriceTwo(Float priceTwo) {
        this.priceTwo = priceTwo;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Float getFabricInCount() {
        return fabricInCount;
    }

    public void setFabricInCount(Float fabricInCount) {
        this.fabricInCount = fabricInCount;
    }

    public Float getFabricStorageCount() {
        return fabricStorageCount;
    }

    public void setFabricStorageCount(Float fabricStorageCount) {
        this.fabricStorageCount = fabricStorageCount;
    }

    public Float getFabricOutCount() {
        return fabricOutCount;
    }

    public void setFabricOutCount(Float fabricOutCount) {
        this.fabricOutCount = fabricOutCount;
    }

    public Float getFabricInCountTwo() {
        return fabricInCountTwo;
    }

    public void setFabricInCountTwo(Float fabricInCountTwo) {
        this.fabricInCountTwo = fabricInCountTwo;
    }

    public Float getFabricStorageCountTwo() {
        return fabricStorageCountTwo;
    }

    public void setFabricStorageCountTwo(Float fabricStorageCountTwo) {
        this.fabricStorageCountTwo = fabricStorageCountTwo;
    }

    public Float getFabricOutCountTwo() {
        return fabricOutCountTwo;
    }

    public void setFabricOutCountTwo(Float fabricOutCountTwo) {
        this.fabricOutCountTwo = fabricOutCountTwo;
    }

    public Integer getBatchNumberIn() {
        return batchNumberIn;
    }

    public void setBatchNumberIn(Integer batchNumberIn) {
        this.batchNumberIn = batchNumberIn;
    }

    public Integer getBatchNumberStorage() {
        return batchNumberStorage;
    }

    public void setBatchNumberStorage(Integer batchNumberStorage) {
        this.batchNumberStorage = batchNumberStorage;
    }

    public Integer getBatchNumberOut() {
        return batchNumberOut;
    }

    public void setBatchNumberOut(Integer batchNumberOut) {
        this.batchNumberOut = batchNumberOut;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Float getRatio() {
        return ratio;
    }

    public void setRatio(Float ratio) {
        this.ratio = ratio;
    }

    public String getJarName() {
        return jarName;
    }

    public void setJarName(String jarName) {
        this.jarName = jarName;
    }

    public Date getOperateDate() {
        return operateDate;
    }

    public void setOperateDate(Date operateDate) {
        this.operateDate = operateDate;
    }


    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public String getPayState() {
        return payState;
    }

    public void setPayState(String payState) {
        this.payState = payState;
    }

    public String getCheckMonth() {
        return checkMonth;
    }

    public void setCheckMonth(String checkMonth) {
        this.checkMonth = checkMonth;
    }

    public String getCheckNumber() {
        return checkNumber;
    }

    public void setCheckNumber(String checkNumber) {
        this.checkNumber = checkNumber;
    }

    public ClothesDevFabricManage(Integer fabricID, String fabricNumber, String fabricName, String fabricWidth, String fabricWeight, String fabricContent, String unit, String unitTwo, Float fabricInCount, Float fabricStorageCount, Float fabricOutCount, Float fabricInCountTwo, Float fabricStorageCountTwo, Float fabricOutCountTwo, Float ratio, String fabricColor, String fabricColorNumber, String supplier, Float price, Float priceTwo, Integer batchNumberIn, Integer batchNumberStorage, Integer batchNumberOut, String storageType, String remark, String payState) {
        this.fabricID = fabricID;
        this.fabricNumber = fabricNumber;
        this.fabricName = fabricName;
        this.fabricWidth = fabricWidth;
        this.fabricWeight = fabricWeight;
        this.fabricContent = fabricContent;
        this.unit = unit;
        this.unitTwo = unitTwo;
        this.fabricInCount = fabricInCount;
        this.fabricStorageCount = fabricStorageCount;
        this.fabricOutCount = fabricOutCount;
        this.fabricInCountTwo = fabricInCountTwo;
        this.fabricStorageCountTwo = fabricStorageCountTwo;
        this.fabricOutCountTwo = fabricOutCountTwo;
        this.ratio = ratio;
        this.fabricColor = fabricColor;
        this.fabricColorNumber = fabricColorNumber;
        this.supplier = supplier;
        this.price = price;
        this.priceTwo = priceTwo;
        this.batchNumberIn = batchNumberIn;
        this.batchNumberStorage = batchNumberStorage;
        this.batchNumberOut = batchNumberOut;
        this.storageType = storageType;
        this.remark = remark;
        this.payState = payState;
    }
}
