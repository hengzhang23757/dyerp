package com.example.erp01.model;

public class CutLocation {

    private String clothesVersionNumber;

    private String orderName;

    private Integer bedNumber;

    private String colorName;

    private String sizeName;

    private Integer packageNumber;

    private Integer layerCount;

    private String cutLocation = "0";

    private String embLocation = "0";

    private String workshop = "0";

    private String pieceWork = "0";

    private String matchWork = "0";

    public String getMatchWork() {
        return matchWork;
    }

    public void setMatchWork(String matchWork) {
        this.matchWork = matchWork;
    }

    public Integer getLayerCount() {
        return layerCount;
    }

    public void setLayerCount(Integer layerCount) {
        this.layerCount = layerCount;
    }

    public String getPieceWork() {
        return pieceWork;
    }

    public void setPieceWork(String pieceWork) {
        this.pieceWork = pieceWork;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public Integer getBedNumber() {
        return bedNumber;
    }

    public void setBedNumber(Integer bedNumber) {
        this.bedNumber = bedNumber;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public Integer getPackageNumber() {
        return packageNumber;
    }

    public void setPackageNumber(Integer packageNumber) {
        this.packageNumber = packageNumber;
    }

    public String getCutLocation() {
        return cutLocation;
    }

    public void setCutLocation(String cutLocation) {
        this.cutLocation = cutLocation;
    }

    public String getEmbLocation() {
        return embLocation;
    }

    public void setEmbLocation(String embLocation) {
        this.embLocation = embLocation;
    }

    public String getWorkshop() {
        return workshop;
    }

    public void setWorkshop(String workshop) {
        this.workshop = workshop;
    }

    public CutLocation() {
    }

    public CutLocation(String clothesVersionNumber, String orderName, Integer bedNumber, String colorName, String sizeName, Integer packageNumber, Integer layerCount, String cutLocation, String embLocation, String workshop, String pieceWork, String matchWork) {
        this.clothesVersionNumber = clothesVersionNumber;
        this.orderName = orderName;
        this.bedNumber = bedNumber;
        this.colorName = colorName;
        this.sizeName = sizeName;
        this.packageNumber = packageNumber;
        this.layerCount = layerCount;
        this.cutLocation = cutLocation;
        this.embLocation = embLocation;
        this.workshop = workshop;
        this.pieceWork = pieceWork;
        this.matchWork = matchWork;
    }
}
