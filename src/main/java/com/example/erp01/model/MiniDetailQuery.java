package com.example.erp01.model;

public class MiniDetailQuery {

    private String orderName;

    private Integer procedureNumber;

    private String procedureName;

    private String colorName;

    private String sizeName;

    private Float pieceCount;

    private Float salary;

    public Integer getProcedureNumber() {
        return procedureNumber;
    }

    public void setProcedureNumber(Integer procedureNumber) {
        this.procedureNumber = procedureNumber;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getProcedureName() {
        return procedureName;
    }

    public void setProcedureName(String procedureName) {
        this.procedureName = procedureName;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public Float getPieceCount() {
        return pieceCount;
    }

    public void setPieceCount(Float pieceCount) {
        this.pieceCount = pieceCount;
    }

    public Float getSalary() {
        return salary;
    }

    public void setSalary(Float salary) {
        this.salary = salary;
    }

    public MiniDetailQuery() {
    }

    public MiniDetailQuery(String orderName, Integer procedureNumber, String procedureName, String colorName, String sizeName, Float pieceCount, Float salary) {
        this.orderName = orderName;
        this.procedureNumber = procedureNumber;
        this.procedureName = procedureName;
        this.colorName = colorName;
        this.sizeName = sizeName;
        this.pieceCount = pieceCount;
        this.salary = salary;
    }
}
