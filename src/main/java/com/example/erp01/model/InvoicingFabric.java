package com.example.erp01.model;

import java.util.Date;

public class InvoicingFabric {

    private Integer fabricID;

    private String orderName;

    private String clothesVersionNumber;

    private String fabricName;

    private String fabricNumber;

    private String fabricColor;

    private String fabricColorNumber;

    private String jarName;

    private Integer batchNumber = 0;

    private Integer inStoreBatch = 0;

    private Integer storageBatch = 0;

    private Integer outStoreBatch = 0;

    private Integer totalInBatch = 0;

    private Integer totalStorageBatch = 0;

    private Integer totalOutBatch = 0;

    private Float weight = 0f;

    private Float inStoreWeight = 0f;

    private Float storageWeight = 0f;

    private Float outStoreWeight = 0f;

    private Float totalInStore = 0f;

    private Float totalStorage = 0f;

    private Float totalOutStore = 0f;

    private String location;

    private String operateType;

    private Date returnTime;

    private String supplier;

    private String colorName;

    private String isHit;

    private String unit;

    public InvoicingFabric() {
    }

    public Integer getFabricID() {
        return fabricID;
    }

    public void setFabricID(Integer fabricID) {
        this.fabricID = fabricID;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public String getFabricName() {
        return fabricName;
    }

    public void setFabricName(String fabricName) {
        this.fabricName = fabricName;
    }

    public String getFabricNumber() {
        return fabricNumber;
    }

    public void setFabricNumber(String fabricNumber) {
        this.fabricNumber = fabricNumber;
    }

    public String getFabricColor() {
        return fabricColor;
    }

    public void setFabricColor(String fabricColor) {
        this.fabricColor = fabricColor;
    }

    public String getFabricColorNumber() {
        return fabricColorNumber;
    }

    public void setFabricColorNumber(String fabricColorNumber) {
        this.fabricColorNumber = fabricColorNumber;
    }

    public String getJarName() {
        return jarName;
    }

    public void setJarName(String jarName) {
        this.jarName = jarName;
    }

    public Integer getBatchNumber() {
        return batchNumber;
    }

    public void setBatchNumber(Integer batchNumber) {
        this.batchNumber = batchNumber;
    }

    public Float getWeight() {
        return weight;
    }

    public void setWeight(Float weight) {
        this.weight = weight;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getOperateType() {
        return operateType;
    }

    public void setOperateType(String operateType) {
        this.operateType = operateType;
    }

    public Date getReturnTime() {
        return returnTime;
    }

    public void setReturnTime(Date returnTime) {
        this.returnTime = returnTime;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getIsHit() {
        return isHit;
    }

    public void setIsHit(String isHit) {
        this.isHit = isHit;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Integer getInStoreBatch() {
        return inStoreBatch;
    }

    public void setInStoreBatch(Integer inStoreBatch) {
        this.inStoreBatch = inStoreBatch;
    }

    public Integer getStorageBatch() {
        return storageBatch;
    }

    public void setStorageBatch(Integer storageBatch) {
        this.storageBatch = storageBatch;
    }

    public Integer getOutStoreBatch() {
        return outStoreBatch;
    }

    public void setOutStoreBatch(Integer outStoreBatch) {
        this.outStoreBatch = outStoreBatch;
    }

    public Float getInStoreWeight() {
        return inStoreWeight;
    }

    public void setInStoreWeight(Float inStoreWeight) {
        this.inStoreWeight = inStoreWeight;
    }

    public Float getStorageWeight() {
        return storageWeight;
    }

    public void setStorageWeight(Float storageWeight) {
        this.storageWeight = storageWeight;
    }

    public Float getOutStoreWeight() {
        return outStoreWeight;
    }

    public void setOutStoreWeight(Float outStoreWeight) {
        this.outStoreWeight = outStoreWeight;
    }

    public Float getTotalInStore() {
        return totalInStore;
    }

    public void setTotalInStore(Float totalInStore) {
        this.totalInStore = totalInStore;
    }

    public Float getTotalStorage() {
        return totalStorage;
    }

    public void setTotalStorage(Float totalStorage) {
        this.totalStorage = totalStorage;
    }

    public Float getTotalOutStore() {
        return totalOutStore;
    }

    public void setTotalOutStore(Float totalOutStore) {
        this.totalOutStore = totalOutStore;
    }

    public Integer getTotalInBatch() {
        return totalInBatch;
    }

    public void setTotalInBatch(Integer totalInBatch) {
        this.totalInBatch = totalInBatch;
    }

    public Integer getTotalStorageBatch() {
        return totalStorageBatch;
    }

    public void setTotalStorageBatch(Integer totalStorageBatch) {
        this.totalStorageBatch = totalStorageBatch;
    }

    public Integer getTotalOutBatch() {
        return totalOutBatch;
    }

    public void setTotalOutBatch(Integer totalOutBatch) {
        this.totalOutBatch = totalOutBatch;
    }

    public InvoicingFabric(Integer fabricID, String orderName, String clothesVersionNumber, String fabricName, String fabricNumber, String fabricColor, String fabricColorNumber, String supplier, String colorName, String unit) {
        this.fabricID = fabricID;
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.fabricName = fabricName;
        this.fabricNumber = fabricNumber;
        this.fabricColor = fabricColor;
        this.fabricColorNumber = fabricColorNumber;
        this.supplier = supplier;
        this.colorName = colorName;
        this.unit = unit;
    }
}
