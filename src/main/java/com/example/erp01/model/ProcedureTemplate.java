package com.example.erp01.model;

public class ProcedureTemplate {

    private Integer procedureID;

    private String styleType;

    private String partName;

    private Integer procedureCode;

    private Integer procedureNumber;

    private String procedureName;

    private String procedureSection;

    private String procedureDescription;

    private String remark;

    private String segment;

    private String equType;

    private String procedureLevel;

    private double SAM;

    private double packagePrice;

    private double piecePrice;

    public Integer getHourCap() {
        return hourCap;
    }

    public void setHourCap(Integer hourCap) {
        this.hourCap = hourCap;
    }

    private String guid;

    private Integer hourCap;

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public Integer getProcedureID() {
        return procedureID;
    }

    public void setProcedureID(Integer procedureID) {
        this.procedureID = procedureID;
    }

    public String getStyleType() {
        return styleType;
    }

    public void setStyleType(String styleType) {
        this.styleType = styleType;
    }

    public String getPartName() {
        return partName;
    }

    public void setPartName(String partName) {
        this.partName = partName;
    }

    public Integer getProcedureCode() {
        return procedureCode;
    }

    public void setProcedureCode(Integer procedureCode) {
        this.procedureCode = procedureCode;
    }

    public Integer getProcedureNumber() {
        return procedureNumber;
    }

    public void setProcedureNumber(Integer procedureNumber) {
        this.procedureNumber = procedureNumber;
    }

    public String getProcedureName() {
        return procedureName;
    }

    public void setProcedureName(String procedureName) {
        this.procedureName = procedureName;
    }

    public String getProcedureSection() {
        return procedureSection;
    }

    public void setProcedureSection(String procedureSection) {
        this.procedureSection = procedureSection;
    }

    public String getProcedureDescription() {
        return procedureDescription;
    }

    public void setProcedureDescription(String procedureDescription) {
        this.procedureDescription = procedureDescription;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getSegment() {
        return segment;
    }

    public void setSegment(String segment) {
        this.segment = segment;
    }

    public String getEquType() {
        return equType;
    }

    public void setEquType(String equType) {
        this.equType = equType;
    }

    public String getProcedureLevel() {
        return procedureLevel;
    }

    public void setProcedureLevel(String procedureLevel) {
        this.procedureLevel = procedureLevel;
    }

    public double getSAM() {
        return SAM;
    }

    public void setSAM(double SAM) {
        this.SAM = SAM;
    }

    public double getPackagePrice() {
        return packagePrice;
    }

    public void setPackagePrice(double packagePrice) {
        this.packagePrice = packagePrice;
    }

    public double getPiecePrice() {
        return piecePrice;
    }

    public void setPiecePrice(double piecePrice) {
        this.piecePrice = piecePrice;
    }

    public ProcedureTemplate() {
    }

    public ProcedureTemplate(String styleType, String partName, Integer procedureNumber, String procedureName, String procedureSection, String procedureDescription, String remark, String segment, String equType, String procedureLevel, double SAM, double packagePrice, double piecePrice) {
        this.styleType = styleType;
        this.partName = partName;
        this.procedureNumber = procedureNumber;
        this.procedureName = procedureName;
        this.procedureSection = procedureSection;
        this.procedureDescription = procedureDescription;
        this.remark = remark;
        this.segment = segment;
        this.equType = equType;
        this.procedureLevel = procedureLevel;
        this.SAM = SAM;
        this.packagePrice = packagePrice;
        this.piecePrice = piecePrice;
    }

    public ProcedureTemplate(String styleType, String partName, Integer procedureCode, Integer procedureNumber, String procedureName, String procedureSection, String procedureDescription, String remark, String segment, String equType, String procedureLevel, double SAM, double packagePrice, double piecePrice) {
        this.styleType = styleType;
        this.partName = partName;
        this.procedureCode = procedureCode;
        this.procedureNumber = procedureNumber;
        this.procedureName = procedureName;
        this.procedureSection = procedureSection;
        this.procedureDescription = procedureDescription;
        this.remark = remark;
        this.segment = segment;
        this.equType = equType;
        this.procedureLevel = procedureLevel;
        this.SAM = SAM;
        this.packagePrice = packagePrice;
        this.piecePrice = piecePrice;
    }

    public ProcedureTemplate(Integer procedureID, String styleType, String partName, Integer procedureCode, Integer procedureNumber, String procedureName, String procedureSection, String procedureDescription, String remark, String segment, String equType, String procedureLevel, double SAM, double packagePrice, double piecePrice) {
        this.procedureID = procedureID;
        this.styleType = styleType;
        this.partName = partName;
        this.procedureCode = procedureCode;
        this.procedureNumber = procedureNumber;
        this.procedureName = procedureName;
        this.procedureSection = procedureSection;
        this.procedureDescription = procedureDescription;
        this.remark = remark;
        this.segment = segment;
        this.equType = equType;
        this.procedureLevel = procedureLevel;
        this.SAM = SAM;
        this.packagePrice = packagePrice;
        this.piecePrice = piecePrice;
    }
}
