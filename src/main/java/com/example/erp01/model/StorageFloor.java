package com.example.erp01.model;

/**
 * Created by hujian on 2019/10/26
 */
public class StorageFloor {

    private String location;
    private Integer num;

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public StorageFloor() {
    }
}
