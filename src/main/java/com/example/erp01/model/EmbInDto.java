package com.example.erp01.model;

import java.util.List;

public class EmbInDto {

    private String embStoreLocation;

    private List<Integer> tailorQcodeIDList;

    public EmbInDto() {
    }

    public String getEmbStoreLocation() {
        return embStoreLocation;
    }

    public void setEmbStoreLocation(String embStoreLocation) {
        this.embStoreLocation = embStoreLocation;
    }

    public List<Integer> getTailorQcodeIDList() {
        return tailorQcodeIDList;
    }

    public void setTailorQcodeIDList(List<Integer> tailorQcodeIDList) {
        this.tailorQcodeIDList = tailorQcodeIDList;
    }
}
