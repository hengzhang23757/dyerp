package com.example.erp01.model;

import java.util.Date;

public class SalaryCount {

    private String employeeNumber;

    private String employeeName;

    private String groupName = "通用";

    private String orderName;

    private Integer procedureCode;

    private Integer procedureNumber;

    private String procedureName;

    private String colorName;

    private String sizeName;

    private Integer pieceCount;

    private Float salary;

    private Date salaryDate;

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public Date getSalaryDate() {
        return salaryDate;
    }

    public void setSalaryDate(Date salaryDate) {
        this.salaryDate = salaryDate;
    }

    public Integer getProcedureCode() {
        return procedureCode;
    }

    public void setProcedureCode(Integer procedureCode) {
        this.procedureCode = procedureCode;
    }

    public String getEmployeeNumber() {
        return employeeNumber;
    }

    public void setEmployeeNumber(String employeeNumber) {
        this.employeeNumber = employeeNumber;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public Integer getProcedureNumber() {
        return procedureNumber;
    }

    public void setProcedureNumber(Integer procedureNumber) {
        this.procedureNumber = procedureNumber;
    }

    public String getProcedureName() {
        return procedureName;
    }

    public void setProcedureName(String procedureName) {
        this.procedureName = procedureName;
    }

    public Integer getPieceCount() {
        return pieceCount;
    }

    public void setPieceCount(Integer pieceCount) {
        this.pieceCount = pieceCount;
    }

    public Float getSalary() {
        return salary;
    }

    public void setSalary(Float salary) {
        this.salary = salary;
    }

    public SalaryCount() {
    }

    public SalaryCount(String employeeNumber, String employeeName, String groupName, String orderName, Integer procedureCode, Integer procedureNumber, String procedureName, String colorName, String sizeName, Integer pieceCount, Float salary, Date salaryDate) {
        this.employeeNumber = employeeNumber;
        this.employeeName = employeeName;
        this.groupName = groupName;
        this.orderName = orderName;
        this.procedureCode = procedureCode;
        this.procedureNumber = procedureNumber;
        this.procedureName = procedureName;
        this.colorName = colorName;
        this.sizeName = sizeName;
        this.pieceCount = pieceCount;
        this.salary = salary;
        this.salaryDate = salaryDate;
    }

    public SalaryCount(String employeeNumber, String employeeName, String groupName, String orderName, Integer procedureCode, Integer procedureNumber, String procedureName, String colorName, String sizeName, Integer pieceCount, Float salary) {
        this.employeeNumber = employeeNumber;
        this.employeeName = employeeName;
        this.groupName = groupName;
        this.orderName = orderName;
        this.procedureCode = procedureCode;
        this.procedureNumber = procedureNumber;
        this.procedureName = procedureName;
        this.colorName = colorName;
        this.sizeName = sizeName;
        this.pieceCount = pieceCount;
        this.salary = salary;
    }

    public SalaryCount(String employeeNumber, String employeeName, String groupName, String orderName, Integer procedureCode, Integer procedureNumber, String procedureName, Integer pieceCount) {
        this.employeeNumber = employeeNumber;
        this.employeeName = employeeName;
        this.groupName = groupName;
        this.orderName = orderName;
        this.procedureCode = procedureCode;
        this.procedureNumber = procedureNumber;
        this.procedureName = procedureName;
        this.pieceCount = pieceCount;
    }
}
