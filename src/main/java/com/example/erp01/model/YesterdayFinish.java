package com.example.erp01.model;

public class YesterdayFinish {

    private String clothesVersionNumber;

    private String orderName;

    private Integer orderCount = 0;

    private Integer tailorCount = 0;

    private Integer wellCount = 0;

    private Integer embCount = 0;

    private Integer workShopCount = 0;

    private Integer finishCount = 0;

    private Integer yesterdayFinishCount = 0;

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public Integer getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(Integer orderCount) {
        this.orderCount = orderCount;
    }

    public Integer getTailorCount() {
        return tailorCount;
    }

    public void setTailorCount(Integer tailorCount) {
        this.tailorCount = tailorCount;
    }

    public Integer getWellCount() {
        return wellCount;
    }

    public void setWellCount(Integer wellCount) {
        this.wellCount = wellCount;
    }

    public Integer getEmbCount() {
        return embCount;
    }

    public void setEmbCount(Integer embCount) {
        this.embCount = embCount;
    }

    public Integer getWorkShopCount() {
        return workShopCount;
    }

    public void setWorkShopCount(Integer workShopCount) {
        this.workShopCount = workShopCount;
    }

    public Integer getFinishCount() {
        return finishCount;
    }

    public void setFinishCount(Integer finishCount) {
        this.finishCount = finishCount;
    }

    public Integer getYesterdayFinishCount() {
        return yesterdayFinishCount;
    }

    public void setYesterdayFinishCount(Integer yesterdayFinishCount) {
        this.yesterdayFinishCount = yesterdayFinishCount;
    }

    public YesterdayFinish() {
    }

    public YesterdayFinish(String clothesVersionNumber, String orderName, Integer orderCount, Integer tailorCount, Integer wellCount, Integer embCount, Integer workShopCount, Integer finishCount, Integer yesterdayFinishCount) {
        this.clothesVersionNumber = clothesVersionNumber;
        this.orderName = orderName;
        this.orderCount = orderCount;
        this.tailorCount = tailorCount;
        this.wellCount = wellCount;
        this.embCount = embCount;
        this.workShopCount = workShopCount;
        this.finishCount = finishCount;
        this.yesterdayFinishCount = yesterdayFinishCount;
    }
}
