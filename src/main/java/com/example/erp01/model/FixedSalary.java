package com.example.erp01.model;

public class FixedSalary {

    private Integer fixedID;

    private String fixedValue;

    public Integer getFixedID() {
        return fixedID;
    }

    public void setFixedID(Integer fixedID) {
        this.fixedID = fixedID;
    }

    public String getFixedValue() {
        return fixedValue;
    }

    public void setFixedValue(String fixedValue) {
        this.fixedValue = fixedValue;
    }

    public FixedSalary(Integer fixedID, String fixedValue) {
        this.fixedID = fixedID;
        this.fixedValue = fixedValue;
    }

    public FixedSalary(String fixedValue) {
        this.fixedValue = fixedValue;
    }

    public FixedSalary() {
    }
}
