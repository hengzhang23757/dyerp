package com.example.erp01.model;

public class CutInStore {

    private Integer cutInstoreID;

    private String storehouseLocation;

    private String orderName;

    private Integer bedNumber;

    private String colorName;

    private String sizeName;

    private Integer layerCount;

    private Integer packageNumber;

    private String partName;

    private String tailorQcode;

    public Integer getCutInstoreID() {
        return cutInstoreID;
    }

    public void setCutInstoreID(Integer cutInstoreID) {
        this.cutInstoreID = cutInstoreID;
    }

    public String getStorehouseLocation() {
        return storehouseLocation;
    }

    public void setStorehouseLocation(String storehouseLocation) {
        this.storehouseLocation = storehouseLocation;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public Integer getBedNumber() {
        return bedNumber;
    }

    public void setBedNumber(Integer bedNumber) {
        this.bedNumber = bedNumber;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public Integer getLayerCount() {
        return layerCount;
    }

    public void setLayerCount(Integer layerCount) {
        this.layerCount = layerCount;
    }

    public Integer getPackageNumber() {
        return packageNumber;
    }

    public void setPackageNumber(Integer packageNumber) {
        this.packageNumber = packageNumber;
    }

    public String getPartName() {
        return partName;
    }

    public void setPartName(String partName) {
        this.partName = partName;
    }

    public String getTailorQcode() {
        return tailorQcode;
    }

    public void setTailorQcode(String tailorQcode) {
        this.tailorQcode = tailorQcode;
    }

    public CutInStore() {
    }

    public CutInStore(String storehouseLocation, String orderName, Integer bedNumber, String colorName, String sizeName, Integer layerCount, Integer packageNumber, String partName, String tailorQcode) {
        this.storehouseLocation = storehouseLocation;
        this.orderName = orderName;
        this.bedNumber = bedNumber;
        this.colorName = colorName;
        this.sizeName = sizeName;
        this.layerCount = layerCount;
        this.packageNumber = packageNumber;
        this.partName = partName;
        this.tailorQcode = tailorQcode;
    }

    public CutInStore(Integer cutInstoreID, String storehouseLocation, String orderName, Integer bedNumber, String colorName, String sizeName, Integer layerCount, Integer packageNumber, String partName, String tailorQcode) {
        this.cutInstoreID = cutInstoreID;
        this.storehouseLocation = storehouseLocation;
        this.orderName = orderName;
        this.bedNumber = bedNumber;
        this.colorName = colorName;
        this.sizeName = sizeName;
        this.layerCount = layerCount;
        this.packageNumber = packageNumber;
        this.partName = partName;
        this.tailorQcode = tailorQcode;
    }
}
