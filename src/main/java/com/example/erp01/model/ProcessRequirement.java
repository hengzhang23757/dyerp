package com.example.erp01.model;

public class ProcessRequirement {

    private Integer processRequirementID;

    private String orderName;

    private String clothesVersionNumber;

    private String requirementCategory;

    private String requirement;

    private String isImportant;

    private String processRequirementImg;

    public String getRequirement() {
        return requirement;
    }

    public void setRequirement(String requirement) {
        this.requirement = requirement;
    }

    public ProcessRequirement() {
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public Integer getProcessRequirementID() {
        return processRequirementID;
    }

    public void setProcessRequirementID(Integer processRequirementID) {
        this.processRequirementID = processRequirementID;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getRequirementCategory() {
        return requirementCategory;
    }

    public void setRequirementCategory(String requirementCategory) {
        this.requirementCategory = requirementCategory;
    }

    public String getIsImportant() {
        return isImportant;
    }

    public void setIsImportant(String isImportant) {
        this.isImportant = isImportant;
    }

    public String getProcessRequirementImg() {
        return processRequirementImg;
    }

    public void setProcessRequirementImg(String processRequirementImg) {
        this.processRequirementImg = processRequirementImg;
    }

    public ProcessRequirement(String orderName, String clothesVersionNumber, String requirement) {
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.requirement = requirement;
    }
}
