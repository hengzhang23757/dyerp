package com.example.erp01.model;

public class OtherStore {

    private Integer otherStoreID;

    private String otherStoreLocation;

    private Integer otherStoreCount;

    public Integer getOtherStoreID() {
        return otherStoreID;
    }

    public void setOtherStoreID(Integer otherStoreID) {
        this.otherStoreID = otherStoreID;
    }

    public String getOtherStoreLocation() {
        return otherStoreLocation;
    }

    public void setOtherStoreLocation(String otherStoreLocation) {
        this.otherStoreLocation = otherStoreLocation;
    }

    public Integer getOtherStoreCount() {
        return otherStoreCount;
    }

    public void setOtherStoreCount(Integer otherStoreCount) {
        this.otherStoreCount = otherStoreCount;
    }

    public OtherStore() {
    }

    public OtherStore(Integer otherStoreID, String otherStoreLocation, Integer otherStoreCount) {
        this.otherStoreID = otherStoreID;
        this.otherStoreLocation = otherStoreLocation;
        this.otherStoreCount = otherStoreCount;
    }

    public OtherStore(String otherStoreLocation, Integer otherStoreCount) {
        this.otherStoreLocation = otherStoreLocation;
        this.otherStoreCount = otherStoreCount;
    }
}
