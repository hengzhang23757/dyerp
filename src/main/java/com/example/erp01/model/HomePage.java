package com.example.erp01.model;

import java.util.Date;

public class HomePage {

    private String clothesVersionNumber;

    private String orderName;

    private String season;

    private String customerName;

    private Integer orderCount;

    private Date receivedDate;

    private Integer orderInfoPart;

    private Float returnFabricPercent;

    private Float looseFabricPercent;

    private Integer cutCount;

    private Integer embCount;

    private Integer hangCount;

    private Integer clothesCount;

    private Integer packageCount;

    private String isFinish;

    private Float orderPartPercent;

    private Float cutPercent;

    private Float embPercent;

    private Float hangPercent;

    private Float clothesPercent;

    private Float packagePercent;


    public HomePage() {
    }

    public Integer getHangCount() {
        return hangCount;
    }

    public void setHangCount(Integer hangCount) {
        this.hangCount = hangCount;
    }

    public Float getHangPercent() {
        return hangPercent;
    }

    public void setHangPercent(Float hangPercent) {
        this.hangPercent = hangPercent;
    }

    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public Integer getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(Integer orderCount) {
        this.orderCount = orderCount;
    }

    public Date getReceivedDate() {
        return receivedDate;
    }

    public void setReceivedDate(Date receivedDate) {
        this.receivedDate = receivedDate;
    }

    public Integer getOrderInfoPart() {
        return orderInfoPart;
    }

    public void setOrderInfoPart(Integer orderInfoPart) {
        this.orderInfoPart = orderInfoPart;
    }

    public Float getReturnFabricPercent() {
        return returnFabricPercent;
    }

    public void setReturnFabricPercent(Float returnFabricPercent) {
        this.returnFabricPercent = returnFabricPercent;
    }

    public Float getLooseFabricPercent() {
        return looseFabricPercent;
    }

    public void setLooseFabricPercent(Float looseFabricPercent) {
        this.looseFabricPercent = looseFabricPercent;
    }

    public Integer getCutCount() {
        return cutCount;
    }

    public void setCutCount(Integer cutCount) {
        this.cutCount = cutCount;
    }

    public Integer getEmbCount() {
        return embCount;
    }

    public void setEmbCount(Integer embCount) {
        this.embCount = embCount;
    }

    public Integer getClothesCount() {
        return clothesCount;
    }

    public void setClothesCount(Integer clothesCount) {
        this.clothesCount = clothesCount;
    }

    public Integer getPackageCount() {
        return packageCount;
    }

    public void setPackageCount(Integer packageCount) {
        this.packageCount = packageCount;
    }

    public String getIsFinish() {
        return isFinish;
    }

    public void setIsFinish(String isFinish) {
        this.isFinish = isFinish;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public Float getOrderPartPercent() {
        return orderPartPercent;
    }

    public void setOrderPartPercent(Float orderPartPercent) {
        this.orderPartPercent = orderPartPercent;
    }

    public Float getCutPercent() {
        return cutPercent;
    }

    public void setCutPercent(Float cutPercent) {
        this.cutPercent = cutPercent;
    }

    public Float getEmbPercent() {
        return embPercent;
    }

    public void setEmbPercent(Float embPercent) {
        this.embPercent = embPercent;
    }

    public Float getClothesPercent() {
        return clothesPercent;
    }

    public void setClothesPercent(Float clothesPercent) {
        this.clothesPercent = clothesPercent;
    }

    public Float getPackagePercent() {
        return packagePercent;
    }

    public void setPackagePercent(Float packagePercent) {
        this.packagePercent = packagePercent;
    }

    @Override
    public String toString() {
        return "HomePage{" +
                "clothesVersionNumber='" + clothesVersionNumber + '\'' +
                ", orderName='" + orderName + '\'' +
                ", season='" + season + '\'' +
                ", customerName='" + customerName + '\'' +
                ", orderCount=" + orderCount +
                ", receivedDate=" + receivedDate +
                ", orderInfoPart=" + orderInfoPart +
                ", returnFabricPercent=" + returnFabricPercent +
                ", looseFabricPercent=" + looseFabricPercent +
                ", cutCount=" + cutCount +
                ", embCount=" + embCount +
                ", clothesCount=" + clothesCount +
                ", packageCount=" + packageCount +
                ", isFinish='" + isFinish + '\'' +
                '}';
    }
}
