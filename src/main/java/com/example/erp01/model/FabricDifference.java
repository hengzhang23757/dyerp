package com.example.erp01.model;

import java.util.Date;

public class FabricDifference {

    private String orderName;

    private String clothesVersionNumber;

    private String season;

    private String fabricName;

    private String fabricNumber;

    private String supplier;

    private String fabricColor;

    private String fabricUse;

    private Float orderCount;

    private Float actualCount;

    private Float differenceCount;

    private Integer batchNumber;

    private String colorName;

    private String unit = "未入";

    private Date lastDate;

    private Float lastCount = 0f;

    private String remark = "";

    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }

    public String getFabricName() {
        return fabricName;
    }

    public void setFabricName(String fabricName) {
        this.fabricName = fabricName;
    }

    public String getFabricUse() {
        return fabricUse;
    }

    public void setFabricUse(String fabricUse) {
        this.fabricUse = fabricUse;
    }

    public String getFabricColor() {
        return fabricColor;
    }

    public void setFabricColor(String fabricColor) {
        this.fabricColor = fabricColor;
    }

    public Float getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(Float orderCount) {
        this.orderCount = orderCount;
    }

    public Float getActualCount() {
        return actualCount;
    }

    public void setActualCount(Float actualCount) {
        this.actualCount = actualCount;
    }

    public Float getDifferenceCount() {
        return differenceCount;
    }

    public void setDifferenceCount(Float differenceCount) {
        this.differenceCount = differenceCount;
    }

    public Integer getBatchNumber() {
        return batchNumber;
    }

    public void setBatchNumber(Integer batchNumber) {
        this.batchNumber = batchNumber;
    }

    public FabricDifference() {
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public Date getLastDate() {
        return lastDate;
    }

    public void setLastDate(Date lastDate) {
        this.lastDate = lastDate;
    }

    public Float getLastCount() {
        return lastCount;
    }

    public void setLastCount(Float lastCount) {
        this.lastCount = lastCount;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public String getFabricNumber() {
        return fabricNumber;
    }

    public void setFabricNumber(String fabricNumber) {
        this.fabricNumber = fabricNumber;
    }

    public FabricDifference(String clothesVersionNumber, String orderName, String fabricName, String fabricNumber, String supplier, String fabricColor, String fabricUse, Float orderCount, Float actualCount, Float differenceCount, Integer batchNumber, String colorName, String unit) {
        this.clothesVersionNumber = clothesVersionNumber;
        this.orderName = orderName;
        this.fabricName = fabricName;
        this.fabricNumber = fabricNumber;
        this.supplier = supplier;
        this.fabricColor = fabricColor;
        this.fabricUse = fabricUse;
        this.orderCount = orderCount;
        this.actualCount = actualCount;
        this.differenceCount = differenceCount;
        this.batchNumber = batchNumber;
        this.colorName = colorName;
        this.unit = unit;
    }

    public FabricDifference(String fabricName, String supplier, String fabricColor, String fabricUse, Float orderCount, Float actualCount, Float differenceCount, Integer batchNumber, String colorName, String unit, Date lastDate, Float lastCount) {
        this.fabricName = fabricName;
        this.supplier = supplier;
        this.fabricColor = fabricColor;
        this.fabricUse = fabricUse;
        this.orderCount = orderCount;
        this.actualCount = actualCount;
        this.differenceCount = differenceCount;
        this.batchNumber = batchNumber;
        this.colorName = colorName;
        this.unit = unit;
        this.lastDate = lastDate;
        this.lastCount = lastCount;
    }
}
