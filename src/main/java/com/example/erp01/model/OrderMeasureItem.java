package com.example.erp01.model;

public class OrderMeasureItem {

    private Integer id;

    private String orderName;

    private String clothesVersionNumber;

    private String partCode;

    private String partName;

    private String sizeName;

    private String sizeValue;

    private String diffSize;

    private Float classSize = 0f;

    public OrderMeasureItem() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public String getPartCode() {
        return partCode;
    }

    public void setPartCode(String partCode) {
        this.partCode = partCode;
    }

    public String getPartName() {
        return partName;
    }

    public void setPartName(String partName) {
        this.partName = partName;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public String getDiffSize() {
        return diffSize;
    }

    public void setDiffSize(String diffSize) {
        this.diffSize = diffSize;
    }

    public Float getClassSize() {
        return classSize;
    }

    public void setClassSize(Float classSize) {
        this.classSize = classSize;
    }

    public String getSizeValue() {
        return sizeValue;
    }

    public void setSizeValue(String sizeValue) {
        this.sizeValue = sizeValue;
    }
}
