package com.example.erp01.model;

import java.util.Date;

public class CutDaily {

    private Date detailTime;

    private String colorName;

    private String sizeName;

    private Integer pieceCount;

    public Date getDetailTime() {
        return detailTime;
    }

    public void setDetailTime(Date detailTime) {
        this.detailTime = detailTime;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public Integer getPieceCount() {
        return pieceCount;
    }

    public void setPieceCount(Integer pieceCount) {
        this.pieceCount = pieceCount;
    }

    public CutDaily() {
    }

    public CutDaily(Date detailTime, String colorName, String sizeName, Integer pieceCount) {
        this.detailTime = detailTime;
        this.colorName = colorName;
        this.sizeName = sizeName;
        this.pieceCount = pieceCount;
    }
}
