package com.example.erp01.model;

import java.util.Date;

public class OpaBackInput {

    private Integer opaBackInputID;

    private String orderName;

    private String clothesVersionNumber;

    private Integer bedNumber;

    private String colorName;

    private String sizeName;

    private String partName;

    private Integer opaBackCount;

    private Integer packageCount;

    private Date opaBackDate;

    public Date getOpaBackDate() {
        return opaBackDate;
    }

    public void setOpaBackDate(Date opaBackDate) {
        this.opaBackDate = opaBackDate;
    }

    public Integer getOpaBackInputID() {
        return opaBackInputID;
    }

    public void setOpaBackInputID(Integer opaBackInputID) {
        this.opaBackInputID = opaBackInputID;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public Integer getBedNumber() {
        return bedNumber;
    }

    public void setBedNumber(Integer bedNumber) {
        this.bedNumber = bedNumber;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public String getPartName() {
        return partName;
    }

    public void setPartName(String partName) {
        this.partName = partName;
    }

    public Integer getOpaBackCount() {
        return opaBackCount;
    }

    public void setOpaBackCount(Integer opaBackCount) {
        this.opaBackCount = opaBackCount;
    }

    public Integer getPackageCount() {
        return packageCount;
    }

    public void setPackageCount(Integer packageCount) {
        this.packageCount = packageCount;
    }

    public OpaBackInput() {
    }

    public OpaBackInput(String orderName, Integer bedNumber, String colorName, String sizeName, String partName, Integer opaBackCount, Integer packageCount) {
        this.orderName = orderName;
        this.bedNumber = bedNumber;
        this.colorName = colorName;
        this.sizeName = sizeName;
        this.partName = partName;
        this.opaBackCount = opaBackCount;
        this.packageCount = packageCount;
    }

    public OpaBackInput(String orderName, String clothesVersionNumber, Integer bedNumber, String colorName, String sizeName, String partName, Integer opaBackCount, Integer packageCount, Date opaBackDate) {
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.bedNumber = bedNumber;
        this.colorName = colorName;
        this.sizeName = sizeName;
        this.partName = partName;
        this.opaBackCount = opaBackCount;
        this.packageCount = packageCount;
        this.opaBackDate = opaBackDate;
    }

    public OpaBackInput(Integer opaBackInputID, String orderName, String clothesVersionNumber, Integer bedNumber, String colorName, String sizeName, String partName, Integer opaBackCount, Integer packageCount, Date opaBackDate) {
        this.opaBackInputID = opaBackInputID;
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.bedNumber = bedNumber;
        this.colorName = colorName;
        this.sizeName = sizeName;
        this.partName = partName;
        this.opaBackCount = opaBackCount;
        this.packageCount = packageCount;
        this.opaBackDate = opaBackDate;
    }

    @Override
    public String toString() {
        return "OpaBackInput{" +
                "opaBackInputID=" + opaBackInputID +
                ", orderName='" + orderName + '\'' +
                ", clothesVersionNumber='" + clothesVersionNumber + '\'' +
                ", bedNumber=" + bedNumber +
                ", colorName='" + colorName + '\'' +
                ", sizeName='" + sizeName + '\'' +
                ", partName='" + partName + '\'' +
                ", opaBackCount=" + opaBackCount +
                ", packageCount=" + packageCount +
                ", opaBackDate=" + opaBackDate +
                '}';
    }
}
