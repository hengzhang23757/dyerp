package com.example.erp01.model;

import java.util.Date;

public class EmpSkills {

    private Integer empSkillsID;

    private String employeeNumber;

    private String employeeName;

    private String groupName;

    private Integer procedureNumber;

    private Integer procedureCode;

    private String procedureName;

    private Float sam;

    private Float timeCount;

    public Integer getHourCap() {
        return hourCap;
    }

    public void setHourCap(Integer hourCap) {
        this.hourCap = hourCap;
    }

    private Date updateTime;

    private Integer hourCap;

    public EmpSkills() {
    }

    public Integer getEmpSkillsID() {
        return empSkillsID;
    }

    public void setEmpSkillsID(Integer empSkillsID) {
        this.empSkillsID = empSkillsID;
    }

    public String getEmployeeNumber() {
        return employeeNumber;
    }

    public void setEmployeeNumber(String employeeNumber) {
        this.employeeNumber = employeeNumber;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public Integer getProcedureNumber() {
        return procedureNumber;
    }

    public void setProcedureNumber(Integer procedureNumber) {
        this.procedureNumber = procedureNumber;
    }

    public Integer getProcedureCode() {
        return procedureCode;
    }

    public void setProcedureCode(Integer procedureCode) {
        this.procedureCode = procedureCode;
    }

    public String getProcedureName() {
        return procedureName;
    }

    public void setProcedureName(String procedureName) {
        this.procedureName = procedureName;
    }

    public Float getTimeCount() {
        return timeCount;
    }

    public void setTimeCount(Float timeCount) {
        this.timeCount = timeCount;
    }

    public Float getSam() {
        return sam;
    }

    public void setSam(Float sam) {
        this.sam = sam;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "EmpSkills{" +
                "empSkillsID=" + empSkillsID +
                ", employeeNumber='" + employeeNumber + '\'' +
                ", employeeName='" + employeeName + '\'' +
                ", groupName='" + groupName + '\'' +
                ", procedureNumber=" + procedureNumber +
                ", procedureCode=" + procedureCode +
                ", procedureName='" + procedureName + '\'' +
                ", sam=" + sam +
                ", timeCount=" + timeCount +
                '}';
    }
}
