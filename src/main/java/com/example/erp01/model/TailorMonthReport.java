package com.example.erp01.model;

import java.util.Date;

public class TailorMonthReport {

    private String orderName;

    private String clothesVersionNumber;

    private String customerName;

    private String groupName;

    private Integer bedNumberCount = 0;

    private Integer pieceCount = 0;

    private Integer orderCount = 0;

    private Integer cutCount = 0;

    private Integer differenceCount = 0;

    private Date cutDate;

    private String partName;

    private Float price = 0f;

    private Float priceTwo = 0f;

    private Float sumMoney = 0f;

    public Integer getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(Integer orderCount) {
        this.orderCount = orderCount;
    }

    public Integer getCutCount() {
        return cutCount;
    }

    public void setCutCount(Integer cutCount) {
        this.cutCount = cutCount;
    }

    public Integer getDifferenceCount() {
        return differenceCount;
    }

    public void setDifferenceCount(Integer differenceCount) {
        this.differenceCount = differenceCount;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public TailorMonthReport(String orderName, String clothesVersionNumber, Integer bedNumberCount, Integer pieceCount, Integer orderCount, Integer cutCount, Integer differenceCount) {
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.bedNumberCount = bedNumberCount;
        this.pieceCount = pieceCount;
        this.orderCount = orderCount;
        this.cutCount = cutCount;
        this.differenceCount = differenceCount;
    }

    public TailorMonthReport(String orderName, String clothesVersionNumber, Integer bedNumberCount, Integer pieceCount) {
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.bedNumberCount = bedNumberCount;
        this.pieceCount = pieceCount;
    }

    public TailorMonthReport(String orderName, Integer bedNumberCount, Integer pieceCount) {
        this.orderName = orderName;
        this.bedNumberCount = bedNumberCount;
        this.pieceCount = pieceCount;
    }

    public TailorMonthReport() {
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public Integer getBedNumberCount() {
        return bedNumberCount;
    }

    public void setBedNumberCount(Integer bedNumberCount) {
        this.bedNumberCount = bedNumberCount;
    }

    public Integer getPieceCount() {
        return pieceCount;
    }

    public void setPieceCount(Integer pieceCount) {
        this.pieceCount = pieceCount;
    }

    public Date getCutDate() {
        return cutDate;
    }

    public void setCutDate(Date cutDate) {
        this.cutDate = cutDate;
    }

    public String getPartName() {
        return partName;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Float getSumMoney() {
        return sumMoney;
    }

    public void setSumMoney(Float sumMoney) {
        this.sumMoney = sumMoney;
    }

    public void setPartName(String partName) {
        this.partName = partName;
    }

    public Float getPriceTwo() {
        return priceTwo;
    }

    public void setPriceTwo(Float priceTwo) {
        this.priceTwo = priceTwo;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public TailorMonthReport(String orderName, String clothesVersionNumber, String customerName, Integer bedNumberCount, Integer pieceCount, Integer orderCount, Integer cutCount, Integer differenceCount, String partName, String groupName) {
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.customerName = customerName;
        this.bedNumberCount = bedNumberCount;
        this.pieceCount = pieceCount;
        this.orderCount = orderCount;
        this.cutCount = cutCount;
        this.differenceCount = differenceCount;
        this.partName = partName;
        this.groupName = groupName;
    }

    public TailorMonthReport(String orderName, String clothesVersionNumber, String customerName, Integer bedNumberCount, Integer pieceCount, Integer orderCount, Integer cutCount, Integer differenceCount, Date cutDate, String partName, String groupName) {
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.customerName = customerName;
        this.bedNumberCount = bedNumberCount;
        this.pieceCount = pieceCount;
        this.orderCount = orderCount;
        this.cutCount = cutCount;
        this.differenceCount = differenceCount;
        this.cutDate = cutDate;
        this.partName = partName;
        this.groupName = groupName;
    }
}
