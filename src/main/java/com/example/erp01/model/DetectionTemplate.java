package com.example.erp01.model;

import java.util.Date;

public class DetectionTemplate {

    private Integer id;

    private String detectionType;

    private String detectionName;

    private String detectionUrl;

    private Date createTime;

    private Date updateTime;

    public DetectionTemplate() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDetectionType() {
        return detectionType;
    }

    public void setDetectionType(String detectionType) {
        this.detectionType = detectionType;
    }

    public String getDetectionName() {
        return detectionName;
    }

    public void setDetectionName(String detectionName) {
        this.detectionName = detectionName;
    }

    public String getDetectionUrl() {
        return detectionUrl;
    }

    public void setDetectionUrl(String detectionUrl) {
        this.detectionUrl = detectionUrl;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
