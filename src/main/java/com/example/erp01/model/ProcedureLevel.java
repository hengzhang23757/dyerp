package com.example.erp01.model;

public class ProcedureLevel {

    private Integer levelID;

    private String level;

    private Float levelValue;

    public Integer getLevelID() {
        return levelID;
    }

    public void setLevelID(Integer levelID) {
        this.levelID = levelID;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public Float getLevelValue() {
        return levelValue;
    }

    public void setLevelValue(Float levelValue) {
        this.levelValue = levelValue;
    }

    public ProcedureLevel() {
    }

    public ProcedureLevel(String level, Float levelValue) {
        this.level = level;
        this.levelValue = levelValue;
    }

    public ProcedureLevel(Integer levelID, String level, Float levelValue) {
        this.levelID = levelID;
        this.level = level;
        this.levelValue = levelValue;
    }
}
