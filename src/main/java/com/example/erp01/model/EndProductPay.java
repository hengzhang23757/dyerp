package com.example.erp01.model;

import java.util.Date;

public class EndProductPay {

    private Integer id;

    private String orderName;

    private String clothesVersionNumber;

    private Integer payCount;

    private Float price = 0f;

    private String remark;

    private Date payDate;

    private Float sumMoney = 0f;

    private String payType;

    private String payNumber;

    private String payMonth;

    private String customerName;

    private Float decreaseMoney = 0f;

    private Float increaseMoney = 0f;

    private Float balanceMoney = 0f;

    public EndProductPay() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public Integer getPayCount() {
        return payCount;
    }

    public void setPayCount(Integer payCount) {
        this.payCount = payCount;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Date getPayDate() {
        return payDate;
    }

    public void setPayDate(Date payDate) {
        this.payDate = payDate;
    }

    public Float getSumMoney() {
        return sumMoney;
    }

    public void setSumMoney(Float sumMoney) {
        this.sumMoney = sumMoney;
    }

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    public String getPayNumber() {
        return payNumber;
    }

    public void setPayNumber(String payNumber) {
        this.payNumber = payNumber;
    }

    public String getPayMonth() {
        return payMonth;
    }

    public void setPayMonth(String payMonth) {
        this.payMonth = payMonth;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public Float getDecreaseMoney() {
        return decreaseMoney;
    }

    public void setDecreaseMoney(Float decreaseMoney) {
        this.decreaseMoney = decreaseMoney;
    }

    public Float getIncreaseMoney() {
        return increaseMoney;
    }

    public void setIncreaseMoney(Float increaseMoney) {
        this.increaseMoney = increaseMoney;
    }

    public Float getBalanceMoney() {
        return balanceMoney;
    }

    public void setBalanceMoney(Float balanceMoney) {
        this.balanceMoney = balanceMoney;
    }
}
