package com.example.erp01.model;

public class ProcedureInfo {

    private Integer procedureCode;

    private Integer procedureNumber;

    private String procedureName;

    private Float SAM;

    private Float piecePrice;

    private Float piecePriceTwo = 0f;

    private String procedureSection;

    private Float subsidy;

    private String scanPart;

    private String specialProcedure;

    private Integer procedureState;

    private String procedureDescription;

    private String colorName = "全部";

    private String sizeName = "全部";

    public Float getPiecePriceTwo() {
        return piecePriceTwo;
    }

    public void setPiecePriceTwo(Float piecePriceTwo) {
        this.piecePriceTwo = piecePriceTwo;
    }

    public String getProcedureDescription() {
        return procedureDescription;
    }

    public void setProcedureDescription(String procedureDescription) {
        this.procedureDescription = procedureDescription;
    }

    public Integer getProcedureState() {
        return procedureState;
    }

    public void setProcedureState(Integer procedureState) {
        this.procedureState = procedureState;
    }

    public String getSpecialProcedure() {
        return specialProcedure;
    }

    public void setSpecialProcedure(String specialProcedure) {
        this.specialProcedure = specialProcedure;
    }

    public String getScanPart() {
        return scanPart;
    }

    public void setScanPart(String scanPart) {
        this.scanPart = scanPart;
    }

    public Float getSAM() {
        return SAM;
    }

    public void setSAM(Float SAM) {
        this.SAM = SAM;
    }

    public Float getPiecePrice() {
        return piecePrice;
    }

    public void setPiecePrice(Float piecePrice) {
        this.piecePrice = piecePrice;
    }

    public Integer getProcedureCode() {
        return procedureCode;
    }

    public void setProcedureCode(Integer procedureCode) {
        this.procedureCode = procedureCode;
    }

    public Integer getProcedureNumber() {
        return procedureNumber;
    }

    public void setProcedureNumber(Integer procedureNumber) {
        this.procedureNumber = procedureNumber;
    }

    public String getProcedureName() {
        return procedureName;
    }

    public void setProcedureName(String procedureName) {
        this.procedureName = procedureName;
    }

    public String getProcedureSection() {
        return procedureSection;
    }

    public void setProcedureSection(String procedureSection) {
        this.procedureSection = procedureSection;
    }

    public Float getSubsidy() {
        return subsidy;
    }

    public void setSubsidy(Float subsidy) {
        this.subsidy = subsidy;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public ProcedureInfo() {
    }

    public ProcedureInfo(Integer procedureCode, String procedureName) {
        this.procedureCode = procedureCode;
        this.procedureName = procedureName;
    }

    public ProcedureInfo(Integer procedureCode, Integer procedureNumber, String procedureName) {
        this.procedureCode = procedureCode;
        this.procedureNumber = procedureNumber;
        this.procedureName = procedureName;
    }

    public ProcedureInfo(Integer procedureCode, Integer procedureNumber, String procedureName, String scanPart) {
        this.procedureCode = procedureCode;
        this.procedureNumber = procedureNumber;
        this.procedureName = procedureName;
        this.scanPart = scanPart;
    }

    public ProcedureInfo(Integer procedureCode, Integer procedureNumber, String procedureName, Float SAM, Float piecePrice) {
        this.procedureCode = procedureCode;
        this.procedureNumber = procedureNumber;
        this.procedureName = procedureName;
        this.SAM = SAM;
        this.piecePrice = piecePrice;
    }

//    public ProcedureInfo(Integer procedureCode, Integer procedureNumber, String procedureName, Float SAM, Float piecePrice, String procedureSection, Float subsidy, String scanPart, String specialProcedure) {
//        this.procedureCode = procedureCode;
//        this.procedureNumber = procedureNumber;
//        this.procedureName = procedureName;
//        this.SAM = SAM;
//        this.piecePrice = piecePrice;
//        this.procedureSection = procedureSection;
//        this.subsidy = subsidy;
//        this.scanPart = scanPart;
//        this.specialProcedure = specialProcedure;
//    }

    public ProcedureInfo(Integer procedureCode, Integer procedureNumber, String procedureName, Float SAM, Float piecePrice, String procedureSection, Float subsidy, String scanPart, String specialProcedure, Integer procedureState) {
        this.procedureCode = procedureCode;
        this.procedureNumber = procedureNumber;
        this.procedureName = procedureName;
        this.SAM = SAM;
        this.piecePrice = piecePrice;
        this.procedureSection = procedureSection;
        this.subsidy = subsidy;
        this.scanPart = scanPart;
        this.specialProcedure = specialProcedure;
        this.procedureState = procedureState;
    }

    @Override
    public String toString() {
        return "ProcedureInfo{" +
                "procedureCode=" + procedureCode +
                ", procedureNumber=" + procedureNumber +
                ", procedureName='" + procedureName + '\'' +
                ", SAM=" + SAM +
                ", piecePrice=" + piecePrice +
                ", procedureSection='" + procedureSection + '\'' +
                ", subsidy=" + subsidy +
                ", scanPart='" + scanPart + '\'' +
                ", specialProcedure='" + specialProcedure + '\'' +
                ", procedureState=" + procedureState +
                '}';
    }
}
