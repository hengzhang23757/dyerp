package com.example.erp01.model;

import java.util.Date;

public class AccessoryPreStore {

    private Integer id;

    private String orderName;

    private String clothesVersionNumber;

    private String preAccessoryName;

    private String accessoryName;

    private String accessoryNumber;

    private String specification = "未输";

    private String accessoryColor = "未输";

    private String accessoryUnit;

    private Float pieceUsage;

    private String colorName;

    private String sizeName;

    private String supplier;

    private Float price;

    private Float accessoryCount  = 0f;

    private Float inStoreCount = 0f;

    private Float outStoreCount = 0f;

    private Float storageCount = 0f;

    private Float accountCount = 0f;

    private String accessoryLocation;

    private Integer preStoreId;

    private String operateType;

    private Date createTime;

    private String checkNumber;

    private String checkMonth;

    private Float sumMoney = 0f;

    private Float returnMoney = 0f;

    private Float payMoney = 0f;

    private String reviewType = "共用辅料";

    private String checkState;

    private String orderState = "未下单";

    private String preCheck;

    public AccessoryPreStore() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public String getPreAccessoryName() {
        return preAccessoryName;
    }

    public void setPreAccessoryName(String preAccessoryName) {
        this.preAccessoryName = preAccessoryName;
    }

    public String getAccessoryName() {
        return accessoryName;
    }

    public void setAccessoryName(String accessoryName) {
        this.accessoryName = accessoryName;
    }

    public String getAccessoryNumber() {
        return accessoryNumber;
    }

    public void setAccessoryNumber(String accessoryNumber) {
        this.accessoryNumber = accessoryNumber;
    }

    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }

    public String getAccessoryColor() {
        return accessoryColor;
    }

    public void setAccessoryColor(String accessoryColor) {
        this.accessoryColor = accessoryColor;
    }

    public String getAccessoryUnit() {
        return accessoryUnit;
    }

    public void setAccessoryUnit(String accessoryUnit) {
        this.accessoryUnit = accessoryUnit;
    }

    public Float getPieceUsage() {
        return pieceUsage;
    }

    public void setPieceUsage(Float pieceUsage) {
        this.pieceUsage = pieceUsage;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Float getAccessoryCount() {
        return accessoryCount;
    }

    public void setAccessoryCount(Float accessoryCount) {
        this.accessoryCount = accessoryCount;
    }

    public String getAccessoryLocation() {
        return accessoryLocation;
    }

    public void setAccessoryLocation(String accessoryLocation) {
        this.accessoryLocation = accessoryLocation;
    }

    public Float getOutStoreCount() {
        return outStoreCount;
    }

    public void setOutStoreCount(Float outStoreCount) {
        this.outStoreCount = outStoreCount;
    }

    public Float getStorageCount() {
        return storageCount;
    }

    public void setStorageCount(Float storageCount) {
        this.storageCount = storageCount;
    }

    public Integer getPreStoreId() {
        return preStoreId;
    }

    public void setPreStoreId(Integer preStoreId) {
        this.preStoreId = preStoreId;
    }

    public String getOperateType() {
        return operateType;
    }

    public void setOperateType(String operateType) {
        this.operateType = operateType;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCheckNumber() {
        return checkNumber;
    }

    public void setCheckNumber(String checkNumber) {
        this.checkNumber = checkNumber;
    }

    public String getCheckMonth() {
        return checkMonth;
    }

    public void setCheckMonth(String checkMonth) {
        this.checkMonth = checkMonth;
    }

    public Float getReturnMoney() {
        return returnMoney;
    }

    public void setReturnMoney(Float returnMoney) {
        this.returnMoney = returnMoney;
    }

    public Float getPayMoney() {
        return payMoney;
    }

    public void setPayMoney(Float payMoney) {
        this.payMoney = payMoney;
    }

    public String getReviewType() {
        return reviewType;
    }

    public void setReviewType(String reviewType) {
        this.reviewType = reviewType;
    }

    public String getCheckState() {
        return checkState;
    }

    public void setCheckState(String checkState) {
        this.checkState = checkState;
    }

    public Float getInStoreCount() {
        return inStoreCount;
    }

    public void setInStoreCount(Float inStoreCount) {
        this.inStoreCount = inStoreCount;
    }

    public Float getAccountCount() {
        return accountCount;
    }

    public void setAccountCount(Float accountCount) {
        this.accountCount = accountCount;
    }

    public Float getSumMoney() {
        return sumMoney;
    }

    public void setSumMoney(Float sumMoney) {
        this.sumMoney = sumMoney;
    }

    public String getOrderState() {
        return orderState;
    }

    public void setOrderState(String orderState) {
        this.orderState = orderState;
    }

    public String getPreCheck() {
        return preCheck;
    }

    public void setPreCheck(String preCheck) {
        this.preCheck = preCheck;
    }

    public AccessoryPreStore(String accessoryName, String accessoryNumber, String specification, String accessoryColor, String accessoryUnit, String supplier, Float price, Float accessoryCount, Float outStoreCount, Float storageCount, String accessoryLocation, Integer preStoreId, String operateType) {
        this.accessoryName = accessoryName;
        this.accessoryNumber = accessoryNumber;
        this.specification = specification;
        this.accessoryColor = accessoryColor;
        this.accessoryUnit = accessoryUnit;
        this.supplier = supplier;
        this.price = price;
        this.accessoryCount = accessoryCount;
        this.outStoreCount = outStoreCount;
        this.storageCount = storageCount;
        this.accessoryLocation = accessoryLocation;
        this.preStoreId = preStoreId;
        this.operateType = operateType;
    }

    public AccessoryPreStore(String orderName, String clothesVersionNumber, String accessoryName, String accessoryNumber, String specification, String accessoryColor, String accessoryUnit, String supplier, Float price, Float accessoryCount, Float outStoreCount, Float storageCount, String accessoryLocation, Integer preStoreId, String operateType) {
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.accessoryName = accessoryName;
        this.accessoryNumber = accessoryNumber;
        this.specification = specification;
        this.accessoryColor = accessoryColor;
        this.accessoryUnit = accessoryUnit;
        this.supplier = supplier;
        this.price = price;
        this.accessoryCount = accessoryCount;
        this.outStoreCount = outStoreCount;
        this.storageCount = storageCount;
        this.accessoryLocation = accessoryLocation;
        this.preStoreId = preStoreId;
        this.operateType = operateType;
    }
}
