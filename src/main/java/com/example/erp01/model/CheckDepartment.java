package com.example.erp01.model;

public class CheckDepartment {

    private Integer id;

    private String name;

    public CheckDepartment() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "CheckDepartment{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
