package com.example.erp01.model;

import java.util.Date;

public class FabricHandOver {

    private Integer fabricHandOverID;

    private String orderName;

    private String clothesVersionNumber;

    private String colorName;

    private String jarNumber;

    private Integer batchOrder;

    private String fabricName;

    private String fabricColor;

    private Integer qCodeID;

    private String fromLocation;

    private String shelfName;

    private Date loadingTime;

    private Date unLoadTime;

    public FabricHandOver(String orderName, String clothesVersionNumber, String colorName, String jarNumber, Integer batchOrder, String fabricName, String fabricColor, Integer qCodeID, String fromLocation, String shelfName, Date loadingTime, Date unLoadTime) {
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.colorName = colorName;
        this.jarNumber = jarNumber;
        this.batchOrder = batchOrder;
        this.fabricName = fabricName;
        this.fabricColor = fabricColor;
        this.qCodeID = qCodeID;
        this.fromLocation = fromLocation;
        this.shelfName = shelfName;
        this.loadingTime = loadingTime;
        this.unLoadTime = unLoadTime;
    }

    public FabricHandOver() {
    }

    public FabricHandOver(Integer fabricHandOverID, String orderName, String clothesVersionNumber, String colorName, String jarNumber, Integer batchOrder, String fabricName, String fabricColor, Integer qCodeID, String fromLocation, String shelfName, Date loadingTime, Date unLoadTime) {
        this.fabricHandOverID = fabricHandOverID;
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.colorName = colorName;
        this.jarNumber = jarNumber;
        this.batchOrder = batchOrder;
        this.fabricName = fabricName;
        this.fabricColor = fabricColor;
        this.qCodeID = qCodeID;
        this.fromLocation = fromLocation;
        this.shelfName = shelfName;
        this.loadingTime = loadingTime;
        this.unLoadTime = unLoadTime;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public Integer getFabricHandOverID() {
        return fabricHandOverID;
    }

    public void setFabricHandOverID(Integer fabricHandOverID) {
        this.fabricHandOverID = fabricHandOverID;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public String getJarNumber() {
        return jarNumber;
    }

    public void setJarNumber(String jarNumber) {
        this.jarNumber = jarNumber;
    }

    public Integer getBatchOrder() {
        return batchOrder;
    }

    public void setBatchOrder(Integer batchOrder) {
        this.batchOrder = batchOrder;
    }

    public String getFabricName() {
        return fabricName;
    }

    public void setFabricName(String fabricName) {
        this.fabricName = fabricName;
    }

    public String getFabricColor() {
        return fabricColor;
    }

    public void setFabricColor(String fabricColor) {
        this.fabricColor = fabricColor;
    }

    public Integer getqCodeID() {
        return qCodeID;
    }

    public void setqCodeID(Integer qCodeID) {
        this.qCodeID = qCodeID;
    }

    public String getFromLocation() {
        return fromLocation;
    }

    public void setFromLocation(String fromLocation) {
        this.fromLocation = fromLocation;
    }

    public String getShelfName() {
        return shelfName;
    }

    public void setShelfName(String shelfName) {
        this.shelfName = shelfName;
    }

    public Date getLoadingTime() {
        return loadingTime;
    }

    public void setLoadingTime(Date loadingTime) {
        this.loadingTime = loadingTime;
    }

    public Date getUnLoadTime() {
        return unLoadTime;
    }

    public void setUnLoadTime(Date unLoadTime) {
        this.unLoadTime = unLoadTime;
    }
}
