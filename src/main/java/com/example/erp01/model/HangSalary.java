package com.example.erp01.model;

import java.util.Date;

public class HangSalary {

    private String employeeNumber;

    private String employeeName;

    private String groupName = "通用";

    private String orderName;

    private Integer procedureNumber;

    private String colorName;

    private String sizeName;

    private Integer pieceCount;

    private Date hangSalaryDate;

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public Date getHangSalaryDate() {
        return hangSalaryDate;
    }

    public void setHangSalaryDate(Date hangSalaryDate) {
        this.hangSalaryDate = hangSalaryDate;
    }

    public String getEmployeeNumber() {
        return employeeNumber;
    }

    public void setEmployeeNumber(String employeeNumber) {
        this.employeeNumber = employeeNumber;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public Integer getProcedureNumber() {
        return procedureNumber;
    }

    public void setProcedureNumber(Integer procedureNumber) {
        this.procedureNumber = procedureNumber;
    }

    public Integer getPieceCount() {
        return pieceCount;
    }

    public void setPieceCount(Integer pieceCount) {
        this.pieceCount = pieceCount;
    }

    public HangSalary() {
    }

    public HangSalary(String employeeNumber, String employeeName, String groupName, String orderName, Integer procedureNumber, Integer pieceCount, Date hangSalaryDate) {
        this.employeeNumber = employeeNumber;
        this.employeeName = employeeName;
        this.groupName = groupName;
        this.orderName = orderName;
        this.procedureNumber = procedureNumber;
        this.pieceCount = pieceCount;
        this.hangSalaryDate = hangSalaryDate;
    }

    public HangSalary(String employeeNumber, String employeeName, String groupName, String orderName, Integer procedureNumber, Integer pieceCount) {
        this.employeeNumber = employeeNumber;
        this.employeeName = employeeName;
        this.groupName = groupName;
        this.orderName = orderName;
        this.procedureNumber = procedureNumber;
        this.pieceCount = pieceCount;
    }

    public HangSalary(String employeeNumber, String employeeName, String groupName, String orderName, Integer procedureNumber, String colorName, String sizeName, Integer pieceCount, Date hangSalaryDate) {
        this.employeeNumber = employeeNumber;
        this.employeeName = employeeName;
        this.groupName = groupName;
        this.orderName = orderName;
        this.procedureNumber = procedureNumber;
        this.colorName = colorName;
        this.sizeName = sizeName;
        this.pieceCount = pieceCount;
        this.hangSalaryDate = hangSalaryDate;
    }

    @Override
    public String toString() {
        return "HangSalary{" +
                "employeeNumber='" + employeeNumber + '\'' +
                ", employeeName='" + employeeName + '\'' +
                ", groupName='" + groupName + '\'' +
                ", orderName='" + orderName + '\'' +
                ", procedureNumber=" + procedureNumber +
                ", colorName='" + colorName + '\'' +
                ", sizeName='" + sizeName + '\'' +
                ", pieceCount=" + pieceCount +
                ", hangSalaryDate=" + hangSalaryDate +
                '}';
    }
}
