package com.example.erp01.model;

public class FabricLeak {

    private String customerName;

    private String season;

    private String styleNumber;

    private String versionNumber;

    private String fabricNumber;

    private String fabricColor;

    private String fabricColorNumber;

    private String jarNumber;

    private Float fabricCount;

    private Integer batchNumber;

    private String location;

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }

    public String getStyleNumber() {
        return styleNumber;
    }

    public void setStyleNumber(String styleNumber) {
        this.styleNumber = styleNumber;
    }

    public String getVersionNumber() {
        return versionNumber;
    }

    public void setVersionNumber(String versionNumber) {
        this.versionNumber = versionNumber;
    }

    public String getFabricNumber() {
        return fabricNumber;
    }

    public void setFabricNumber(String fabricNumber) {
        this.fabricNumber = fabricNumber;
    }

    public String getFabricColor() {
        return fabricColor;
    }

    public void setFabricColor(String fabricColor) {
        this.fabricColor = fabricColor;
    }

    public String getFabricColorNumber() {
        return fabricColorNumber;
    }

    public void setFabricColorNumber(String fabricColorNumber) {
        this.fabricColorNumber = fabricColorNumber;
    }

    public String getJarNumber() {
        return jarNumber;
    }

    public void setJarNumber(String jarNumber) {
        this.jarNumber = jarNumber;
    }

    public Float getFabricCount() {
        return fabricCount;
    }

    public void setFabricCount(Float fabricCount) {
        this.fabricCount = fabricCount;
    }

    public Integer getBatchNumber() {
        return batchNumber;
    }

    public void setBatchNumber(Integer batchNumber) {
        this.batchNumber = batchNumber;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public FabricLeak() {
    }

    public FabricLeak(String customerName, String season, String styleNumber, String versionNumber, String fabricNumber, String fabricColor, String fabricColorNumber, String jarNumber, Float fabricCount, Integer batchNumber, String location) {
        this.customerName = customerName;
        this.season = season;
        this.styleNumber = styleNumber;
        this.versionNumber = versionNumber;
        this.fabricNumber = fabricNumber;
        this.fabricColor = fabricColor;
        this.fabricColorNumber = fabricColorNumber;
        this.jarNumber = jarNumber;
        this.fabricCount = fabricCount;
        this.batchNumber = batchNumber;
        this.location = location;
    }
}
