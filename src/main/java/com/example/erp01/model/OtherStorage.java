package com.example.erp01.model;

public class OtherStorage {

    private Integer storageID;

    private String otherStoreLocation;

    private String orderName;

    private String clothesVersionNumber;

    private Integer bedNumber;

    private String colorName;

    private String sizeName;

    private Integer layerCount;

    private Integer packageNumber;

    private String partName;

    private Integer tailorQcodeID;

    private String isIn;

    private String isOut;

    public String getIsIn() {
        return isIn;
    }

    public void setIsIn(String isIn) {
        this.isIn = isIn;
    }

    public String getIsOut() {
        return isOut;
    }

    public void setIsOut(String isOut) {
        this.isOut = isOut;
    }

    public Integer getStorageID() {
        return storageID;
    }

    public void setStorageID(Integer storageID) {
        this.storageID = storageID;
    }

    public String getOtherStoreLocation() {
        return otherStoreLocation;
    }

    public void setOtherStoreLocation(String otherStoreLocation) {
        this.otherStoreLocation = otherStoreLocation;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public Integer getBedNumber() {
        return bedNumber;
    }

    public void setBedNumber(Integer bedNumber) {
        this.bedNumber = bedNumber;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public Integer getLayerCount() {
        return layerCount;
    }

    public void setLayerCount(Integer layerCount) {
        this.layerCount = layerCount;
    }

    public Integer getPackageNumber() {
        return packageNumber;
    }

    public void setPackageNumber(Integer packageNumber) {
        this.packageNumber = packageNumber;
    }

    public String getPartName() {
        return partName;
    }

    public void setPartName(String partName) {
        this.partName = partName;
    }

    public Integer getTailorQcodeID() {
        return tailorQcodeID;
    }

    public void setTailorQcodeID(Integer tailorQcodeID) {
        this.tailorQcodeID = tailorQcodeID;
    }

    public OtherStorage() {
    }

    public OtherStorage(String otherStoreLocation, String orderName, String clothesVersionNumber, Integer bedNumber, String colorName, String sizeName, Integer layerCount, Integer packageNumber, String partName, Integer tailorQcodeID) {
        this.otherStoreLocation = otherStoreLocation;
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.bedNumber = bedNumber;
        this.colorName = colorName;
        this.sizeName = sizeName;
        this.layerCount = layerCount;
        this.packageNumber = packageNumber;
        this.partName = partName;
        this.tailorQcodeID = tailorQcodeID;
    }

    public OtherStorage(String orderName, String clothesVersionNumber, Integer bedNumber, String colorName, String sizeName, Integer layerCount, Integer packageNumber, String partName, Integer tailorQcodeID) {
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.bedNumber = bedNumber;
        this.colorName = colorName;
        this.sizeName = sizeName;
        this.layerCount = layerCount;
        this.packageNumber = packageNumber;
        this.partName = partName;
        this.tailorQcodeID = tailorQcodeID;
    }

    public OtherStorage(String otherStoreLocation, String orderName, String clothesVersionNumber, Integer bedNumber, String colorName, String sizeName, Integer layerCount, Integer packageNumber, String partName, Integer tailorQcodeID, String isIn, String isOut) {
        this.otherStoreLocation = otherStoreLocation;
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.bedNumber = bedNumber;
        this.colorName = colorName;
        this.sizeName = sizeName;
        this.layerCount = layerCount;
        this.packageNumber = packageNumber;
        this.partName = partName;
        this.tailorQcodeID = tailorQcodeID;
        this.isIn = isIn;
        this.isOut = isOut;
    }

    public OtherStorage(Integer storageID, String otherStoreLocation, String orderName, String clothesVersionNumber, Integer bedNumber, String colorName, String sizeName, Integer layerCount, Integer packageNumber, String partName, Integer tailorQcodeID, String isIn, String isOut) {
        this.storageID = storageID;
        this.otherStoreLocation = otherStoreLocation;
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.bedNumber = bedNumber;
        this.colorName = colorName;
        this.sizeName = sizeName;
        this.layerCount = layerCount;
        this.packageNumber = packageNumber;
        this.partName = partName;
        this.tailorQcodeID = tailorQcodeID;
        this.isIn = isIn;
        this.isOut = isOut;
    }

    public OtherStorage(Integer bedNumber, String colorName, String sizeName, Integer layerCount, Integer packageNumber, String partName, String isIn, String isOut) {
        this.bedNumber = bedNumber;
        this.colorName = colorName;
        this.sizeName = sizeName;
        this.layerCount = layerCount;
        this.packageNumber = packageNumber;
        this.partName = partName;
        this.isIn = isIn;
        this.isOut = isOut;
    }

    public OtherStorage(String orderName, String clothesVersionNumber, Integer bedNumber, String colorName, String sizeName, Integer layerCount, Integer packageNumber, String partName, Integer tailorQcodeID, String isOut) {
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.bedNumber = bedNumber;
        this.colorName = colorName;
        this.sizeName = sizeName;
        this.layerCount = layerCount;
        this.packageNumber = packageNumber;
        this.partName = partName;
        this.tailorQcodeID = tailorQcodeID;
        this.isOut = isOut;
    }
}
