package com.example.erp01.model;

import java.util.Date;

public class ManualInput implements Comparable<ManualInput>{

    private Integer pieceWorkID;

    private String employeeName;

    private String employeeNumber;

    private String groupName;

    private String orderName;

    private String clothesVersionNumber;

    private String colorName;

    private String sizeName;

    private Integer layerCount;

    private Integer procedureNumber;

    private String procedureName;

    private String userName;

    private Date createTime;

    private Date updateTime;

    private String remark;

    public ManualInput(String employeeName, String employeeNumber, String groupName, String orderName, String clothesVersionNumber, String colorName, String sizeName, Integer layerCount, Integer procedureNumber, String procedureName, String userName, Date createTime, Date updateTime) {
        this.employeeName = employeeName;
        this.employeeNumber = employeeNumber;
        this.groupName = groupName;
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.colorName = colorName;
        this.sizeName = sizeName;
        this.layerCount = layerCount;
        this.procedureNumber = procedureNumber;
        this.procedureName = procedureName;
        this.userName = userName;
        this.createTime = createTime;
        this.updateTime = updateTime;
    }

    public ManualInput(Integer pieceWorkID, String employeeName, String employeeNumber, String groupName, String orderName, String clothesVersionNumber, String colorName, String sizeName, Integer layerCount, Integer procedureNumber, String procedureName, String userName, Date createTime, Date updateTime) {
        this.pieceWorkID = pieceWorkID;
        this.employeeName = employeeName;
        this.employeeNumber = employeeNumber;
        this.groupName = groupName;
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.colorName = colorName;
        this.sizeName = sizeName;
        this.layerCount = layerCount;
        this.procedureNumber = procedureNumber;
        this.procedureName = procedureName;
        this.userName = userName;
        this.createTime = createTime;
        this.updateTime = updateTime;
    }

    public Integer getPieceWorkID() {
        return pieceWorkID;
    }

    public void setPieceWorkID(Integer pieceWorkID) {
        this.pieceWorkID = pieceWorkID;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getEmployeeNumber() {
        return employeeNumber;
    }

    public void setEmployeeNumber(String employeeNumber) {
        this.employeeNumber = employeeNumber;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public Integer getLayerCount() {
        return layerCount;
    }

    public void setLayerCount(Integer layerCount) {
        this.layerCount = layerCount;
    }

    public Integer getProcedureNumber() {
        return procedureNumber;
    }

    public void setProcedureNumber(Integer procedureNumber) {
        this.procedureNumber = procedureNumber;
    }

    public String getProcedureName() {
        return procedureName;
    }

    public void setProcedureName(String procedureName) {
        this.procedureName = procedureName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public ManualInput() {
    }

    @Override
    public int compareTo(ManualInput o) {
        return o.getUpdateTime().compareTo(this.getUpdateTime());
    }
}
