package com.example.erp01.model;

public class PrintPart {

    private Integer printPartID;

    private String orderName;

    private String clothesVersionNumber;

    private String printPartName;

    public Integer getPrintPartID() {
        return printPartID;
    }

    public void setPrintPartID(Integer printPartID) {
        this.printPartID = printPartID;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public String getPrintPartName() {
        return printPartName;
    }

    public void setPrintPartName(String printPartName) {
        this.printPartName = printPartName;
    }

    public PrintPart() {
    }

    public PrintPart(Integer printPartID, String orderName, String clothesVersionNumber, String printPartName) {
        this.printPartID = printPartID;
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.printPartName = printPartName;
    }

    public PrintPart(String orderName, String clothesVersionNumber, String printPartName) {
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.printPartName = printPartName;
    }
}
