package com.example.erp01.model;

import java.util.Date;

public class ManufactureOrder {

    private Integer manufactureOrderID;

    private String styleDescription;

    private String customerName;

    private String purchaseMethod;

    private String area;

    private String orderName;

    private String styleName;

    private String factoryOrder;

    private String styleNumber;

    private String colorInfo;

    private String sizeInfo;

    private String mainFabricInfo;

    private String mainFabricElement;

    private String otherFabricInfo;

    private String otherFabricElement;

    private String unit;

    private String modelType;

    private String remark = "";

    private String clothesVersionNumber;

    private String securityCategory;

    private String buyer;

    private String technicalStandard;

    private String season;

    private Date deadLine;

    private String imgUrl1;

    private String imgUrl2;

    private Date deliveryDate;

    private Float additional;

    private Integer orderState = 0;

    private Float price = 0f;

    private Float printPrice = 0f;

    private Float washPrice = 0f;

    private String modifyState = "锁定";

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }

    public String getImgUrl1() {
        return imgUrl1;
    }

    public void setImgUrl1(String imgUrl1) {
        this.imgUrl1 = imgUrl1;
    }

    public String getImgUrl2() {
        return imgUrl2;
    }

    public void setImgUrl2(String imgUrl2) {
        this.imgUrl2 = imgUrl2;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getManufactureOrderID() {
        return manufactureOrderID;
    }

    public void setManufactureOrderID(Integer manufactureOrderID) {
        this.manufactureOrderID = manufactureOrderID;
    }

    public String getStyleDescription() {
        return styleDescription;
    }

    public void setStyleDescription(String styleDescription) {
        this.styleDescription = styleDescription;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getStyleName() {
        return styleName;
    }

    public void setStyleName(String styleName) {
        this.styleName = styleName;
    }

    public String getFactoryOrder() {
        return factoryOrder;
    }

    public void setFactoryOrder(String factoryOrder) {
        this.factoryOrder = factoryOrder;
    }

    public String getStyleNumber() {
        return styleNumber;
    }

    public void setStyleNumber(String styleNumber) {
        this.styleNumber = styleNumber;
    }

    public String getColorInfo() {
        return colorInfo;
    }

    public void setColorInfo(String colorInfo) {
        this.colorInfo = colorInfo;
    }

    public String getSizeInfo() {
        return sizeInfo;
    }

    public void setSizeInfo(String sizeInfo) {
        this.sizeInfo = sizeInfo;
    }

    public String getMainFabricInfo() {
        return mainFabricInfo;
    }

    public void setMainFabricInfo(String mainFabricInfo) {
        this.mainFabricInfo = mainFabricInfo;
    }

    public String getMainFabricElement() {
        return mainFabricElement;
    }

    public void setMainFabricElement(String mainFabricElement) {
        this.mainFabricElement = mainFabricElement;
    }

    public String getOtherFabricInfo() {
        return otherFabricInfo;
    }

    public void setOtherFabricInfo(String otherFabricInfo) {
        this.otherFabricInfo = otherFabricInfo;
    }

    public String getOtherFabricElement() {
        return otherFabricElement;
    }

    public void setOtherFabricElement(String otherFabricElement) {
        this.otherFabricElement = otherFabricElement;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getModelType() {
        return modelType;
    }

    public void setModelType(String modelType) {
        this.modelType = modelType;
    }

    public String getSecurityCategory() {
        return securityCategory;
    }

    public void setSecurityCategory(String securityCategory) {
        this.securityCategory = securityCategory;
    }

    public String getBuyer() {
        return buyer;
    }

    public void setBuyer(String buyer) {
        this.buyer = buyer;
    }

    public String getTechnicalStandard() {
        return technicalStandard;
    }

    public void setTechnicalStandard(String technicalStandard) {
        this.technicalStandard = technicalStandard;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getPurchaseMethod() {
        return purchaseMethod;
    }

    public void setPurchaseMethod(String purchaseMethod) {
        this.purchaseMethod = purchaseMethod;
    }


    public Date getDeadLine() {
        return deadLine;
    }

    public void setDeadLine(Date deadLine) {
        this.deadLine = deadLine;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public Float getAdditional() {
        return additional;
    }

    public void setAdditional(Float additional) {
        this.additional = additional;
    }

    public Integer getOrderState() {
        return orderState;
    }

    public void setOrderState(Integer orderState) {
        this.orderState = orderState;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Float getPrintPrice() {
        return printPrice;
    }

    public void setPrintPrice(Float printPrice) {
        this.printPrice = printPrice;
    }

    public Float getWashPrice() {
        return washPrice;
    }

    public void setWashPrice(Float washPrice) {
        this.washPrice = washPrice;
    }

    public String getModifyState() {
        return modifyState;
    }

    public void setModifyState(String modifyState) {
        this.modifyState = modifyState;
    }

    public ManufactureOrder() {
    }

    public ManufactureOrder(String styleDescription, String customerName, String purchaseMethod, String orderName, String styleName, String colorInfo, String sizeInfo, String clothesVersionNumber, String buyer, String season, Date deadLine, Date deliveryDate, Float additional) {
        this.styleDescription = styleDescription;
        this.customerName = customerName;
        this.purchaseMethod = purchaseMethod;
        this.orderName = orderName;
        this.styleName = styleName;
        this.colorInfo = colorInfo;
        this.sizeInfo = sizeInfo;
        this.clothesVersionNumber = clothesVersionNumber;
        this.buyer = buyer;
        this.season = season;
        this.deadLine = deadLine;
        this.deliveryDate = deliveryDate;
        this.additional = additional;
    }
}
