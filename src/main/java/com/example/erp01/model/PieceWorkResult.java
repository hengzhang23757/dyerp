package com.example.erp01.model;

import java.util.Date;

public class PieceWorkResult {

    private Integer pieceWorkID;

    private String employeeNumber;

    private String employeeName;

    private String groupName;

    private String orderName;

    private String clothesVersionNumber;

    private Integer bedNumber;

    private String colorName;

    private String sizeName;

    private Integer packageNumber;

    private Integer layerCount;

    private Integer procedureNumber;

    private String procedureName;

    private String pieceType;

    private Long nid;

    private String userName;

    private Date createTime;

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public String getProcedureName() {
        return procedureName;
    }

    public void setProcedureName(String procedureName) {
        this.procedureName = procedureName;
    }

    public Integer getPieceWorkID() {
        return pieceWorkID;
    }

    public void setPieceWorkID(Integer pieceWorkID) {
        this.pieceWorkID = pieceWorkID;
    }

    public String getEmployeeNumber() {
        return employeeNumber;
    }

    public void setEmployeeNumber(String employeeNumber) {
        this.employeeNumber = employeeNumber;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public Integer getBedNumber() {
        return bedNumber;
    }

    public void setBedNumber(Integer bedNumber) {
        this.bedNumber = bedNumber;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public Integer getPackageNumber() {
        return packageNumber;
    }

    public void setPackageNumber(Integer packageNumber) {
        this.packageNumber = packageNumber;
    }

    public Integer getLayerCount() {
        return layerCount;
    }

    public void setLayerCount(Integer layerCount) {
        this.layerCount = layerCount;
    }

    public Integer getProcedureNumber() {
        return procedureNumber;
    }

    public void setProcedureNumber(Integer procedureNumber) {
        this.procedureNumber = procedureNumber;
    }

    public String getPieceType() {
        return pieceType;
    }

    public void setPieceType(String pieceType) {
        this.pieceType = pieceType;
    }

    public Long getNid() {
        return nid;
    }

    public void setNid(Long nid) {
        this.nid = nid;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public PieceWorkResult() {
    }

    public PieceWorkResult(Integer pieceWorkID, String employeeNumber, String employeeName, String groupName, String orderName, Integer bedNumber, String colorName, String sizeName, Integer packageNumber, Integer layerCount, Integer procedureNumber, String procedureName) {
        this.pieceWorkID = pieceWorkID;
        this.employeeNumber = employeeNumber;
        this.employeeName = employeeName;
        this.groupName = groupName;
        this.orderName = orderName;
        this.bedNumber = bedNumber;
        this.colorName = colorName;
        this.sizeName = sizeName;
        this.packageNumber = packageNumber;
        this.layerCount = layerCount;
        this.procedureNumber = procedureNumber;
        this.procedureName = procedureName;
    }
}
