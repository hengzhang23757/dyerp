package com.example.erp01.model;

import java.time.DateTimeException;
import java.util.Date;

public class BasicFabric {

    private Integer basicFabricID;

    private String basicFabricName;

    private String basicFabricNumber;

    private String basicFabricColor;

    private String basicFabricColorNumber;

    private String basicFabricWidth;

    private String basicFabricWeight;

    private String basicFabricUnit;

    private String basicFabricSupplier;

    private Date updateTime;

    public Integer getBasicFabricID() {
        return basicFabricID;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public void setBasicFabricID(Integer basicFabricID) {
        this.basicFabricID = basicFabricID;
    }

    public String getBasicFabricName() {
        return basicFabricName;
    }

    public void setBasicFabricName(String basicFabricName) {
        this.basicFabricName = basicFabricName;
    }

    public String getBasicFabricNumber() {
        return basicFabricNumber;
    }

    public void setBasicFabricNumber(String basicFabricNumber) {
        this.basicFabricNumber = basicFabricNumber;
    }

    public String getBasicFabricColor() {
        return basicFabricColor;
    }

    public void setBasicFabricColor(String basicFabricColor) {
        this.basicFabricColor = basicFabricColor;
    }

    public String getBasicFabricColorNumber() {
        return basicFabricColorNumber;
    }

    public void setBasicFabricColorNumber(String basicFabricColorNumber) {
        this.basicFabricColorNumber = basicFabricColorNumber;
    }

    public String getBasicFabricWidth() {
        return basicFabricWidth;
    }

    public void setBasicFabricWidth(String basicFabricWidth) {
        this.basicFabricWidth = basicFabricWidth;
    }

    public String getBasicFabricWeight() {
        return basicFabricWeight;
    }

    public void setBasicFabricWeight(String basicFabricWeight) {
        this.basicFabricWeight = basicFabricWeight;
    }

    public String getBasicFabricUnit() {
        return basicFabricUnit;
    }

    public void setBasicFabricUnit(String basicFabricUnit) {
        this.basicFabricUnit = basicFabricUnit;
    }

    public String getBasicFabricSupplier() {
        return basicFabricSupplier;
    }

    public void setBasicFabricSupplier(String basicFabricSupplier) {
        this.basicFabricSupplier = basicFabricSupplier;
    }

    public BasicFabric() {
    }

    public BasicFabric(String basicFabricName, String basicFabricColor, String basicFabricWidth, String basicFabricWeight, String basicFabricSupplier) {
        this.basicFabricName = basicFabricName;
        this.basicFabricColor = basicFabricColor;
        this.basicFabricWidth = basicFabricWidth;
        this.basicFabricWeight = basicFabricWeight;
        this.basicFabricSupplier = basicFabricSupplier;
    }

    public BasicFabric(String basicFabricName, String basicFabricNumber, String basicFabricColor, String basicFabricColorNumber, String basicFabricWidth, String basicFabricWeight, String basicFabricUnit, String basicFabricSupplier, Date updateTime) {
        this.basicFabricName = basicFabricName;
        this.basicFabricNumber = basicFabricNumber;
        this.basicFabricColor = basicFabricColor;
        this.basicFabricColorNumber = basicFabricColorNumber;
        this.basicFabricWidth = basicFabricWidth;
        this.basicFabricWeight = basicFabricWeight;
        this.basicFabricUnit = basicFabricUnit;
        this.basicFabricSupplier = basicFabricSupplier;
        this.updateTime = updateTime;
    }

    public BasicFabric(Integer basicFabricID, String basicFabricName, String basicFabricNumber, String basicFabricColor, String basicFabricColorNumber, String basicFabricWidth, String basicFabricWeight, String basicFabricUnit, String basicFabricSupplier, Date updateTime) {
        this.basicFabricID = basicFabricID;
        this.basicFabricName = basicFabricName;
        this.basicFabricNumber = basicFabricNumber;
        this.basicFabricColor = basicFabricColor;
        this.basicFabricColorNumber = basicFabricColorNumber;
        this.basicFabricWidth = basicFabricWidth;
        this.basicFabricWeight = basicFabricWeight;
        this.basicFabricUnit = basicFabricUnit;
        this.basicFabricSupplier = basicFabricSupplier;
        this.updateTime = updateTime;
    }
}
