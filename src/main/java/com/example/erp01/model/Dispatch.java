package com.example.erp01.model;

public class Dispatch implements Comparable<Dispatch> {

    private Integer dispatchID;

    private String employeeNumber;

    private String employeeName;

    private String groupName;

    private String orderName;

    private Integer procedureCode;

    private Integer procedureNumber;

    private String procedureName;

    public Integer getDispatchID() {
        return dispatchID;
    }

    public void setDispatchID(Integer dispatchID) {
        this.dispatchID = dispatchID;
    }

    public String getEmployeeNumber() {
        return employeeNumber;
    }

    public void setEmployeeNumber(String employeeNumber) {
        this.employeeNumber = employeeNumber;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public Integer getProcedureCode() {
        return procedureCode;
    }

    public void setProcedureCode(Integer procedureCode) {
        this.procedureCode = procedureCode;
    }

    public Integer getProcedureNumber() {
        return procedureNumber;
    }

    public void setProcedureNumber(Integer procedureNumber) {
        this.procedureNumber = procedureNumber;
    }

    public String getProcedureName() {
        return procedureName;
    }

    public void setProcedureName(String procedureName) {
        this.procedureName = procedureName;
    }

    public Dispatch() {
    }

    public Dispatch(String employeeNumber, String employeeName, String groupName, String orderName, Integer procedureCode, Integer procedureNumber, String procedureName) {
        this.employeeNumber = employeeNumber;
        this.employeeName = employeeName;
        this.groupName = groupName;
        this.orderName = orderName;
        this.procedureCode = procedureCode;
        this.procedureNumber = procedureNumber;
        this.procedureName = procedureName;
    }

    public Dispatch(Integer dispatchID, String employeeNumber, String employeeName, String groupName, String orderName, Integer procedureCode, Integer procedureNumber, String procedureName) {
        this.dispatchID = dispatchID;
        this.employeeNumber = employeeNumber;
        this.employeeName = employeeName;
        this.groupName = groupName;
        this.orderName = orderName;
        this.procedureCode = procedureCode;
        this.procedureNumber = procedureNumber;
        this.procedureName = procedureName;
    }

    @Override
    public int compareTo(Dispatch o) {
        return this.getOrderName().compareTo(o.getOrderName());
    }
}
