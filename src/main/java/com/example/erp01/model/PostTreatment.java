package com.example.erp01.model;

public class PostTreatment {

    private Integer id;

    private String clothesVersionNumber;

    private String orderName;

    private String colorName;

    private String sizeName;

    private Integer orderCount = 0;

    private Integer wellCount = 0;

    private Integer checkCount = 0;

    private Integer hangCount = 0;

    private Integer inspectionCount = 0;

    private Integer washCount = 0;

    private Integer ironCount = 0;

    private Integer tailCheckCount = 0;

    private Integer packageCount = 0;

    private Integer shipCount = 0;

    private Integer surplusCount = 0;

    private String surplusLocation = "-";

    private Integer defectiveCount = 0;

    private String defectiveLocation = "-";

    private Integer chickenCount = 0;

    private String chickenLocation = "-";

    private Integer sampleCount = 0;

    private String sampleLocation = "-";

    private Integer diffCount = 0;

    private String createTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }


    public Integer getCheckCount() {
        return checkCount;
    }

    public void setCheckCount(Integer checkCount) {
        this.checkCount = checkCount;
    }

    public Integer getHangCount() {
        return hangCount;
    }

    public void setHangCount(Integer hangCount) {
        this.hangCount = hangCount;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public Integer getWellCount() {
        return wellCount;
    }

    public void setWellCount(Integer wellCount) {
        this.wellCount = wellCount;
    }

    public Integer getWashCount() {
        return washCount;
    }

    public void setWashCount(Integer washCount) {
        this.washCount = washCount;
    }

    public Integer getShipCount() {
        return shipCount;
    }

    public void setShipCount(Integer shipCount) {
        this.shipCount = shipCount;
    }

    public Integer getSurplusCount() {
        return surplusCount;
    }

    public void setSurplusCount(Integer surplusCount) {
        this.surplusCount = surplusCount;
    }

    public String getSurplusLocation() {
        return surplusLocation;
    }

    public void setSurplusLocation(String surplusLocation) {
        this.surplusLocation = surplusLocation;
    }

    public Integer getDefectiveCount() {
        return defectiveCount;
    }

    public void setDefectiveCount(Integer defectiveCount) {
        this.defectiveCount = defectiveCount;
    }

    public String getDefectiveLocation() {
        return defectiveLocation;
    }

    public void setDefectiveLocation(String defectiveLocation) {
        this.defectiveLocation = defectiveLocation;
    }

    public Integer getChickenCount() {
        return chickenCount;
    }

    public void setChickenCount(Integer chickenCount) {
        this.chickenCount = chickenCount;
    }

    public String getChickenLocation() {
        return chickenLocation;
    }

    public void setChickenLocation(String chickenLocation) {
        this.chickenLocation = chickenLocation;
    }

    public Integer getDiffCount() {
        return diffCount;
    }

    public void setDiffCount(Integer diffCount) {
        this.diffCount = diffCount;
    }

    public Integer getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(Integer orderCount) {
        this.orderCount = orderCount;
    }

    public Integer getInspectionCount() {
        return inspectionCount;
    }

    public void setInspectionCount(Integer inspectionCount) {
        this.inspectionCount = inspectionCount;
    }

    public Integer getIronCount() {
        return ironCount;
    }

    public void setIronCount(Integer ironCount) {
        this.ironCount = ironCount;
    }

    public Integer getTailCheckCount() {
        return tailCheckCount;
    }

    public void setTailCheckCount(Integer tailCheckCount) {
        this.tailCheckCount = tailCheckCount;
    }

    public Integer getPackageCount() {
        return packageCount;
    }

    public void setPackageCount(Integer packageCount) {
        this.packageCount = packageCount;
    }


    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public Integer getSampleCount() {
        return sampleCount;
    }

    public void setSampleCount(Integer sampleCount) {
        this.sampleCount = sampleCount;
    }

    public String getSampleLocation() {
        return sampleLocation;
    }

    public void setSampleLocation(String sampleLocation) {
        this.sampleLocation = sampleLocation;
    }

    public PostTreatment() {
    }

    public PostTreatment(String clothesVersionNumber, String orderName, String colorName, String sizeName, Integer orderCount) {
        this.clothesVersionNumber = clothesVersionNumber;
        this.orderName = orderName;
        this.colorName = colorName;
        this.sizeName = sizeName;
        this.orderCount = orderCount;
    }

    public PostTreatment(String clothesVersionNumber, String orderName, Integer orderCount) {
        this.clothesVersionNumber = clothesVersionNumber;
        this.orderName = orderName;
        this.orderCount = orderCount;
    }
}
