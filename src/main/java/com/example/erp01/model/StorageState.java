package com.example.erp01.model;

public class StorageState {

    private String storehouseLocation;

    private Integer storagePlanCount;

    private Integer storageCount;

    private Integer floor;

    public String getStorehouseLocation() {
        return storehouseLocation;
    }

    public void setStorehouseLocation(String storehouseLocation) {
        this.storehouseLocation = storehouseLocation;
    }

    public Integer getStoragePlanCount() {
        return storagePlanCount;
    }

    public void setStoragePlanCount(Integer storagePlanCount) {
        this.storagePlanCount = storagePlanCount;
    }

    public Integer getStorageCount() {
        return storageCount;
    }

    public void setStorageCount(Integer storageCount) {
        this.storageCount = storageCount;
    }

    public Integer getFloor() {
        return floor;
    }

    public void setFloor(Integer floor) {
        this.floor = floor;
    }

    public StorageState() {
    }

    public StorageState(String storehouseLocation, Integer storagePlanCount, Integer storageCount) {
        this.storehouseLocation = storehouseLocation;
        this.storagePlanCount = storagePlanCount;
        this.storageCount = storageCount;
    }
}
