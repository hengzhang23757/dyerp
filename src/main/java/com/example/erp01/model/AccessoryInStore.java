package com.example.erp01.model;

import java.util.Date;

public class AccessoryInStore {

    private Integer id;

    private String orderName;

    private String clothesVersionNumber;

    private String customerName;

    private String season;

    private String accessoryName;

    private String accessoryNumber;

    private String specification = "未输";

    private String accessoryUnit;

    private String accessoryColor = "未输";

    private Float pieceUsage = 0f;

    private String sizeName;

    private String colorName;

    private Float useCount = 0f;

    private Float inStoreCount = 0f;

    private Float accountCount = 0f;

    private String accessoryLocation;

    private String supplier;

    private String inStoreTime;

    private Integer orderCount;

    private Integer wellCount;

    private Date createTime;

    private Integer accessoryID;

    private String colorNumber;

    private String checkNumber;

    private Float price = 0f;

    private Float accessoryCount = 0f;

    private Float returnMoney = 0f;

    private String reviewType = "辅料";

    private String checkMonth;

    private String inStoreType;

    private Float payMoney = 0f;

    private String accessoryType;

    private Float addFee = 0f;

    private String addRemark = "";

    public Integer getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(Integer orderCount) {
        this.orderCount = orderCount;
    }

    public Integer getWellCount() {
        return wellCount;
    }

    public void setWellCount(Integer wellCount) {
        this.wellCount = wellCount;
    }

    public Float getAddFee() {
        return addFee;
    }

    public void setAddFee(Float addFee) {
        this.addFee = addFee;
    }

    public String getAddRemark() {
        return addRemark;
    }

    public void setAddRemark(String addRemark) {
        this.addRemark = addRemark;
    }

    public AccessoryInStore() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public String getAccessoryName() {
        return accessoryName;
    }

    public void setAccessoryName(String accessoryName) {
        this.accessoryName = accessoryName;
    }

    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }

    public String getAccessoryUnit() {
        return accessoryUnit;
    }

    public void setAccessoryUnit(String accessoryUnit) {
        this.accessoryUnit = accessoryUnit;
    }

    public String getAccessoryColor() {
        return accessoryColor;
    }

    public void setAccessoryColor(String accessoryColor) {
        this.accessoryColor = accessoryColor;
    }

    public Float getPieceUsage() {
        return pieceUsage;
    }

    public void setPieceUsage(Float pieceUsage) {
        this.pieceUsage = pieceUsage;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public Float getInStoreCount() {
        return inStoreCount;
    }

    public void setInStoreCount(Float inStoreCount) {
        this.inStoreCount = inStoreCount;
    }

    public String getAccessoryLocation() {
        return accessoryLocation;
    }

    public void setAccessoryLocation(String accessoryLocation) {
        this.accessoryLocation = accessoryLocation;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public String getInStoreTime() {
        return inStoreTime;
    }

    public void setInStoreTime(String inStoreTime) {
        this.inStoreTime = inStoreTime;
    }


    public String getAccessoryNumber() {
        return accessoryNumber;
    }

    public void setAccessoryNumber(String accessoryNumber) {
        this.accessoryNumber = accessoryNumber;
    }


    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getAccessoryID() {
        return accessoryID;
    }

    public void setAccessoryID(Integer accessoryID) {
        this.accessoryID = accessoryID;
    }

    public String getColorNumber() {
        return colorNumber;
    }

    public void setColorNumber(String colorNumber) {
        this.colorNumber = colorNumber;
    }

    public String getCheckNumber() {
        return checkNumber;
    }

    public void setCheckNumber(String checkNumber) {
        this.checkNumber = checkNumber;
    }


    public Float getReturnMoney() {
        return returnMoney;
    }

    public void setReturnMoney(Float returnMoney) {
        this.returnMoney = returnMoney;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Float getAccessoryCount() {
        return accessoryCount;
    }

    public void setAccessoryCount(Float accessoryCount) {
        this.accessoryCount = accessoryCount;
    }


    public Float getAccountCount() {
        return accountCount;
    }

    public void setAccountCount(Float accountCount) {
        this.accountCount = accountCount;
    }

    public String getReviewType() {
        return reviewType;
    }

    public void setReviewType(String reviewType) {
        this.reviewType = reviewType;
    }

    public String getCheckMonth() {
        return checkMonth;
    }

    public void setCheckMonth(String checkMonth) {
        this.checkMonth = checkMonth;
    }

    public Float getUseCount() {
        return useCount;
    }

    public void setUseCount(Float useCount) {
        this.useCount = useCount;
    }

    public String getInStoreType() {
        return inStoreType;
    }

    public void setInStoreType(String inStoreType) {
        this.inStoreType = inStoreType;
    }

    public Float getPayMoney() {
        return payMoney;
    }

    public void setPayMoney(Float payMoney) {
        this.payMoney = payMoney;
    }

    public String getAccessoryType() {
        return accessoryType;
    }

    public void setAccessoryType(String accessoryType) {
        this.accessoryType = accessoryType;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }

    @Override
    public String toString() {
        return "AccessoryInStore{" +
                "id=" + id +
                ", orderName='" + orderName + '\'' +
                ", clothesVersionNumber='" + clothesVersionNumber + '\'' +
                ", accessoryName='" + accessoryName + '\'' +
                ", accessoryNumber='" + accessoryNumber + '\'' +
                ", specification='" + specification + '\'' +
                ", accessoryUnit='" + accessoryUnit + '\'' +
                ", accessoryColor='" + accessoryColor + '\'' +
                ", pieceUsage=" + pieceUsage +
                ", sizeName='" + sizeName + '\'' +
                ", colorName='" + colorName + '\'' +
                ", inStoreCount=" + inStoreCount +
                ", accessoryLocation='" + accessoryLocation + '\'' +
                ", supplier='" + supplier + '\'' +
                ", inStoreTime='" + inStoreTime + '\'' +
                ", orderCount=" + orderCount +
                ", wellCount=" + wellCount +
                ", createTime=" + createTime +
                ", accessoryID=" + accessoryID +
                ", colorNumber='" + colorNumber + '\'' +
                '}';
    }
}
