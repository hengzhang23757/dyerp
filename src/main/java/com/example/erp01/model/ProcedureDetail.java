package com.example.erp01.model;

import java.sql.Date;

public class ProcedureDetail {

    private Integer orderProcedureID;

    private String orderName;

    private String styleDescription;

    private String clothesVersionNumber;

    private String customerName;

    private Date beginDate;

    private Integer orderCount;

    private String styleType;

    private String partName;

    private Integer procedureID;

    private Integer procedureCode;

    private Integer procedureNumber;

    private String procedureName;

    private String procedureSection;

    private String procedureDescription;

    private String remark;

    private String segment;

    private String equType;

    private String procedureLevel;

    private double SAM;

    private double packagePrice;

    private double piecePrice;

    private String imagePath;

    public Integer getProcedureID() {
        return procedureID;
    }

    public void setProcedureID(Integer procedureID) {
        this.procedureID = procedureID;
    }

    public Integer getOrderProcedureID() {
        return orderProcedureID;
    }

    public void setOrderProcedureID(Integer orderProcedureID) {
        this.orderProcedureID = orderProcedureID;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getStyleDescription() {
        return styleDescription;
    }

    public void setStyleDescription(String styleDescription) {
        this.styleDescription = styleDescription;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public Date getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    public Integer getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(Integer orderCount) {
        this.orderCount = orderCount;
    }

    public String getStyleType() {
        return styleType;
    }

    public void setStyleType(String styleType) {
        this.styleType = styleType;
    }

    public String getPartName() {
        return partName;
    }

    public void setPartName(String partName) {
        this.partName = partName;
    }

    public Integer getProcedureCode() {
        return procedureCode;
    }

    public void setProcedureCode(Integer procedureCode) {
        this.procedureCode = procedureCode;
    }

    public Integer getProcedureNumber() {
        return procedureNumber;
    }

    public void setProcedureNumber(Integer procedureNumber) {
        this.procedureNumber = procedureNumber;
    }

    public String getProcedureName() {
        return procedureName;
    }

    public void setProcedureName(String procedureName) {
        this.procedureName = procedureName;
    }

    public String getProcedureSection() {
        return procedureSection;
    }

    public void setProcedureSection(String procedureSection) {
        this.procedureSection = procedureSection;
    }

    public String getProcedureDescription() {
        return procedureDescription;
    }

    public void setProcedureDescription(String procedureDescription) {
        this.procedureDescription = procedureDescription;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getSegment() {
        return segment;
    }

    public void setSegment(String segment) {
        this.segment = segment;
    }

    public String getEquType() {
        return equType;
    }

    public void setEquType(String equType) {
        this.equType = equType;
    }

    public String getProcedureLevel() {
        return procedureLevel;
    }

    public void setProcedureLevel(String procedureLevel) {
        this.procedureLevel = procedureLevel;
    }

    public double getSAM() {
        return SAM;
    }

    public void setSAM(double SAM) {
        this.SAM = SAM;
    }

    public double getPackagePrice() {
        return packagePrice;
    }

    public void setPackagePrice(double packagePrice) {
        this.packagePrice = packagePrice;
    }

    public double getPiecePrice() {
        return piecePrice;
    }

    public void setPiecePrice(double piecePrice) {
        this.piecePrice = piecePrice;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public ProcedureDetail() {
    }

    public ProcedureDetail(String orderName, String styleDescription, String clothesVersionNumber, String customerName, Date beginDate, Integer orderCount, String styleType, String partName, Integer procedureID, Integer procedureCode, Integer procedureNumber, String procedureName, String procedureSection, String procedureDescription, String remark, String segment, String equType, String procedureLevel, double SAM, double packagePrice, double piecePrice, String imagePath) {
        this.orderName = orderName;
        this.styleDescription = styleDescription;
        this.clothesVersionNumber = clothesVersionNumber;
        this.customerName = customerName;
        this.beginDate = beginDate;
        this.orderCount = orderCount;
        this.styleType = styleType;
        this.partName = partName;
        this.procedureID = procedureID;
        this.procedureCode = procedureCode;
        this.procedureNumber = procedureNumber;
        this.procedureName = procedureName;
        this.procedureSection = procedureSection;
        this.procedureDescription = procedureDescription;
        this.remark = remark;
        this.segment = segment;
        this.equType = equType;
        this.procedureLevel = procedureLevel;
        this.SAM = SAM;
        this.packagePrice = packagePrice;
        this.piecePrice = piecePrice;
        this.imagePath = imagePath;
    }

    public ProcedureDetail(Integer orderProcedureID, String orderName, String styleDescription, String clothesVersionNumber, String customerName, Date beginDate, Integer orderCount, String styleType, String partName, Integer procedureID, Integer procedureCode, Integer procedureNumber, String procedureName, String procedureSection, String procedureDescription, String remark, String segment, String equType, String procedureLevel, double SAM, double packagePrice, double piecePrice, String imagePath) {
        this.orderProcedureID = orderProcedureID;
        this.orderName = orderName;
        this.styleDescription = styleDescription;
        this.clothesVersionNumber = clothesVersionNumber;
        this.customerName = customerName;
        this.beginDate = beginDate;
        this.orderCount = orderCount;
        this.styleType = styleType;
        this.partName = partName;
        this.procedureID = procedureID;
        this.procedureCode = procedureCode;
        this.procedureNumber = procedureNumber;
        this.procedureName = procedureName;
        this.procedureSection = procedureSection;
        this.procedureDescription = procedureDescription;
        this.remark = remark;
        this.segment = segment;
        this.equType = equType;
        this.procedureLevel = procedureLevel;
        this.SAM = SAM;
        this.packagePrice = packagePrice;
        this.piecePrice = piecePrice;
        this.imagePath = imagePath;
    }
}
