package com.example.erp01.model;

import java.util.Date;

public class DepartmentPlan {

    private Integer departmentPlanID;

    private String planType;

    private String clothesVersionNumber;

    private String orderName;

    private Date beginDate;

    private Date endDate;

    private Integer totalCount = 0;

    private Integer finishCount = 0;

    private Integer planCount = 0;

    private Integer actCount = 0;

    private Integer cutCount = 0;

    private Float completionRate = 0f;

    private Float completionRateCut = 0f;

    private String remark;

    private String colorName;

    private String sizeName;

    private Integer workCount;

    private Date workDate;

    private Integer looseHour;

    private Date looseTime;

    private String finishState;

    private Integer planOrder;

    public Integer getDepartmentPlanID() {
        return departmentPlanID;
    }

    public void setDepartmentPlanID(Integer departmentPlanID) {
        this.departmentPlanID = departmentPlanID;
    }

    public String getPlanType() {
        return planType;
    }

    public void setPlanType(String planType) {
        this.planType = planType;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public Date getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public Integer getFinishCount() {
        return finishCount;
    }

    public void setFinishCount(Integer finishCount) {
        this.finishCount = finishCount;
    }

    public Integer getPlanCount() {
        return planCount;
    }

    public void setPlanCount(Integer planCount) {
        this.planCount = planCount;
    }

    public Integer getActCount() {
        return actCount;
    }

    public void setActCount(Integer actCount) {
        this.actCount = actCount;
    }

    public Float getCompletionRate() {
        return completionRate;
    }

    public void setCompletionRate(Float completionRate) {
        this.completionRate = completionRate;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public Integer getWorkCount() {
        return workCount;
    }

    public void setWorkCount(Integer workCount) {
        this.workCount = workCount;
    }

    public Date getWorkDate() {
        return workDate;
    }

    public void setWorkDate(Date workDate) {
        this.workDate = workDate;
    }

    public Integer getLooseHour() {
        return looseHour;
    }

    public void setLooseHour(Integer looseHour) {
        this.looseHour = looseHour;
    }

    public Date getLooseTime() {
        return looseTime;
    }

    public void setLooseTime(Date looseTime) {
        this.looseTime = looseTime;
    }

    public String getFinishState() {
        return finishState;
    }

    public void setFinishState(String finishState) {
        this.finishState = finishState;
    }

    public Integer getPlanOrder() {
        return planOrder;
    }

    public void setPlanOrder(Integer planOrder) {
        this.planOrder = planOrder;
    }

    public Float getCompletionRateCut() {
        return completionRateCut;
    }

    public void setCompletionRateCut(Float completionRateCut) {
        this.completionRateCut = completionRateCut;
    }

    public Integer getCutCount() {
        return cutCount;
    }

    public void setCutCount(Integer cutCount) {
        this.cutCount = cutCount;
    }

    public DepartmentPlan() {
    }
}
