package com.example.erp01.model;

public class EmbStorageState {

    private String embStoreLocation;

    private Integer embPlanCount;

    private Integer embStorageCount;

    private Integer floor;

    public String getEmbStoreLocation() {
        return embStoreLocation;
    }

    public void setEmbStoreLocation(String embStoreLocation) {
        this.embStoreLocation = embStoreLocation;
    }

    public Integer getEmbPlanCount() {
        return embPlanCount;
    }

    public void setEmbPlanCount(Integer embPlanCount) {
        this.embPlanCount = embPlanCount;
    }

    public Integer getEmbStorageCount() {
        return embStorageCount;
    }

    public void setEmbStorageCount(Integer embStorageCount) {
        this.embStorageCount = embStorageCount;
    }

    public Integer getFloor() {
        return floor;
    }

    public void setFloor(Integer floor) {
        this.floor = floor;
    }

    public EmbStorageState() {
    }

    public EmbStorageState(String embStoreLocation, Integer embPlanCount, Integer embStorageCount) {
        this.embStoreLocation = embStoreLocation;
        this.embPlanCount = embPlanCount;
        this.embStorageCount = embStorageCount;
    }
}
