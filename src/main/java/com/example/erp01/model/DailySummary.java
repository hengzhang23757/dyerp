package com.example.erp01.model;

import java.util.Date;

public class DailySummary {

    private Integer dailySummaryID;

    private String groupName;

    private String itemName;

    private Date itemDate;

    private String clothesVersionNumber;

    private String orderName;

    private Integer itemCount;

    public DailySummary() {
    }

    public DailySummary(String groupName, String itemName, Date itemDate, String clothesVersionNumber, String orderName, Integer itemCount) {
        this.groupName = groupName;
        this.itemName = itemName;
        this.itemDate = itemDate;
        this.clothesVersionNumber = clothesVersionNumber;
        this.orderName = orderName;
        this.itemCount = itemCount;
    }

    public DailySummary(Integer dailySummaryID, String groupName, String itemName, Date itemDate, String clothesVersionNumber, String orderName, Integer itemCount) {
        this.dailySummaryID = dailySummaryID;
        this.groupName = groupName;
        this.itemName = itemName;
        this.itemDate = itemDate;
        this.clothesVersionNumber = clothesVersionNumber;
        this.orderName = orderName;
        this.itemCount = itemCount;
    }

    public Integer getDailySummaryID() {
        return dailySummaryID;
    }

    public void setDailySummaryID(Integer dailySummaryID) {
        this.dailySummaryID = dailySummaryID;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public Date getItemDate() {
        return itemDate;
    }

    public void setItemDate(Date itemDate) {
        this.itemDate = itemDate;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public Integer getItemCount() {
        return itemCount;
    }

    public void setItemCount(Integer itemCount) {
        this.itemCount = itemCount;
    }
}
