package com.example.erp01.model;

import java.util.Date;

public class GeneralSalary implements Comparable<GeneralSalary> {

    private String employeeNumber;

    private String employeeName;

    private String groupName;

    private String orderName;

    private String clothesVersionNumber;

    private String salaryType;

    private Integer procedureNumber;

    private String procedureName;

    private String colorName;

    private String sizeName;

    private Float pieceCount;

    private Float price = 0f;

    private Float salary = 0f;

    private Float priceTwo = 0f;

    private Float salaryTwo = 0f;

    private Float salarySum = 0f;

    private Date generalDate;

    private String stringGeneralDate;

    private String procedureSection;

    private Integer id;

    private Integer bedNumber;

    private Integer packageNumber;

    private Float sumPrice = 0f;

    private Float addition = 0f;

    public String getStringGeneralDate() {
        return stringGeneralDate;
    }

    public void setStringGeneralDate(String stringGeneralDate) {
        this.stringGeneralDate = stringGeneralDate;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public String getProcedureSection() {
        return procedureSection;
    }

    public void setProcedureSection(String procedureSection) {
        this.procedureSection = procedureSection;
    }

    public Date getGeneralDate() {
        return generalDate;
    }

    public void setGeneralDate(Date generalDate) {
        this.generalDate = generalDate;
    }

    public String getEmployeeNumber() {
        return employeeNumber;
    }

    public void setEmployeeNumber(String employeeNumber) {
        this.employeeNumber = employeeNumber;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public Integer getProcedureNumber() {
        return procedureNumber;
    }

    public void setProcedureNumber(Integer procedureNumber) {
        this.procedureNumber = procedureNumber;
    }

    public String getProcedureName() {
        return procedureName;
    }

    public void setProcedureName(String procedureName) {
        this.procedureName = procedureName;
    }

    public Float getPieceCount() {
        return pieceCount;
    }

    public void setPieceCount(Float pieceCount) {
        this.pieceCount = pieceCount;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Float getSalary() {
        return salary;
    }

    public void setSalary(Float salary) {
        this.salary = salary;
    }

    public Float getPriceTwo() {
        return priceTwo;
    }

    public void setPriceTwo(Float priceTwo) {
        this.priceTwo = priceTwo;
    }

    public Float getSalaryTwo() {
        return salaryTwo;
    }

    public void setSalaryTwo(Float salaryTwo) {
        this.salaryTwo = salaryTwo;
    }


    public Float getSalarySum() {
        return salarySum;
    }

    public void setSalarySum(Float salarySum) {
        this.salarySum = salarySum;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getBedNumber() {
        return bedNumber;
    }

    public void setBedNumber(Integer bedNumber) {
        this.bedNumber = bedNumber;
    }

    public Integer getPackageNumber() {
        return packageNumber;
    }

    public void setPackageNumber(Integer packageNumber) {
        this.packageNumber = packageNumber;
    }

    public Float getSumPrice() {
        return sumPrice;
    }

    public void setSumPrice(Float sumPrice) {
        this.sumPrice = sumPrice;
    }

    public Float getAddition() {
        return addition;
    }

    public void setAddition(Float addition) {
        this.addition = addition;
    }

    public GeneralSalary() {
    }

    public String getSalaryType() {
        return salaryType;
    }

    public void setSalaryType(String salaryType) {
        this.salaryType = salaryType;
    }

    public GeneralSalary(String employeeNumber, String employeeName, String groupName, String orderName, String clothesVersionNumber, String salaryType, Integer procedureNumber, String procedureName, String colorName, String sizeName, Float pieceCount, Float price, Float salary, Float priceTwo, Float salaryTwo) {
        this.employeeNumber = employeeNumber;
        this.employeeName = employeeName;
        this.groupName = groupName;
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.salaryType = salaryType;
        this.procedureNumber = procedureNumber;
        this.procedureName = procedureName;
        this.colorName = colorName;
        this.sizeName = sizeName;
        this.pieceCount = pieceCount;
        this.price = price;
        this.salary = salary;
        this.priceTwo = priceTwo;
        this.salaryTwo = salaryTwo;
    }

    public GeneralSalary(String employeeNumber, String employeeName, String groupName, String orderName, String clothesVersionNumber, String salaryType, Integer procedureNumber, String procedureName, Float pieceCount, Float price, Float salary, Date generalDate) {
        this.employeeNumber = employeeNumber;
        this.employeeName = employeeName;
        this.groupName = groupName;
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.salaryType = salaryType;
        this.procedureNumber = procedureNumber;
        this.procedureName = procedureName;
        this.pieceCount = pieceCount;
        this.price = price;
        this.salary = salary;
        this.generalDate = generalDate;
    }

    public GeneralSalary(String employeeNumber, String employeeName, String groupName, String orderName, String clothesVersionNumber, String salaryType, Integer procedureNumber, String procedureName, String colorName, String sizeName, Float pieceCount, Float price, Float salary, Float priceTwo, Float salaryTwo, Date generalDate) {
        this.employeeNumber = employeeNumber;
        this.employeeName = employeeName;
        this.groupName = groupName;
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.salaryType = salaryType;
        this.procedureNumber = procedureNumber;
        this.procedureName = procedureName;
        this.colorName = colorName;
        this.sizeName = sizeName;
        this.pieceCount = pieceCount;
        this.price = price;
        this.salary = salary;
        this.priceTwo = priceTwo;
        this.salaryTwo = salaryTwo;
        this.generalDate = generalDate;
    }

    public GeneralSalary(String orderName, String clothesVersionNumber, String salaryType, Integer procedureNumber, String procedureName, Float pieceCount, Float price, Float salary, Float priceTwo, Float salaryTwo, Date generalDate, String procedureSection) {
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.salaryType = salaryType;
        this.procedureNumber = procedureNumber;
        this.procedureName = procedureName;
        this.pieceCount = pieceCount;
        this.price = price;
        this.salary = salary;
        this.priceTwo = priceTwo;
        this.salaryTwo = salaryTwo;
        this.generalDate = generalDate;
        this.procedureSection = procedureSection;
    }

    public GeneralSalary(String employeeNumber, String employeeName, String groupName, String orderName, String clothesVersionNumber, String salaryType, Integer procedureNumber, String procedureName, String colorName, String sizeName, Float pieceCount, Float price, Float salary, Float priceTwo, Float salaryTwo, String stringGeneralDate) {
        this.employeeNumber = employeeNumber;
        this.employeeName = employeeName;
        this.groupName = groupName;
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.salaryType = salaryType;
        this.procedureNumber = procedureNumber;
        this.procedureName = procedureName;
        this.colorName = colorName;
        this.sizeName = sizeName;
        this.pieceCount = pieceCount;
        this.price = price;
        this.salary = salary;
        this.priceTwo = priceTwo;
        this.salaryTwo = salaryTwo;
        this.stringGeneralDate = stringGeneralDate;
    }

    @Override
    public String toString() {
        return "GeneralSalary{" +
                "employeeNumber='" + employeeNumber + '\'' +
                ", employeeName='" + employeeName + '\'' +
                ", groupName='" + groupName + '\'' +
                ", orderName='" + orderName + '\'' +
                ", clothesVersionNumber='" + clothesVersionNumber + '\'' +
                ", salaryType='" + salaryType + '\'' +
                ", procedureNumber=" + procedureNumber +
                ", procedureName='" + procedureName + '\'' +
                ", pieceCount=" + pieceCount +
                ", price=" + price +
                ", salary=" + salary +
                ", priceTwo=" + priceTwo +
                ", salaryTwo=" + salaryTwo +
                ", generalDate=" + generalDate +
                ", procedureSection='" + procedureSection + '\'' +
                '}';
    }

    @Override
    public int compareTo(GeneralSalary o) {
        return this.getEmployeeNumber().compareTo(o.getEmployeeNumber());
    }
}
