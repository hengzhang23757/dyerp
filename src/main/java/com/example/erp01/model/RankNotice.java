package com.example.erp01.model;

import java.util.Date;

public class RankNotice {

    private Integer id;

    private String title;

    private String noticeOne;

    private String noticeTwo;

    private String noticeThree;

    private String noticeFour;

    private String noticeFive;

    private String category;

    private String author;

    private  String role;

    private Date beginDate;

    private Date endDate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNoticeOne() {
        return noticeOne;
    }

    public void setNoticeOne(String noticeOne) {
        this.noticeOne = noticeOne;
    }

    public String getNoticeTwo() {
        return noticeTwo;
    }

    public void setNoticeTwo(String noticeTwo) {
        this.noticeTwo = noticeTwo;
    }

    public String getNoticeThree() {
        return noticeThree;
    }

    public void setNoticeThree(String noticeThree) {
        this.noticeThree = noticeThree;
    }

    public String getNoticeFour() {
        return noticeFour;
    }

    public void setNoticeFour(String noticeFour) {
        this.noticeFour = noticeFour;
    }

    public String getNoticeFive() {
        return noticeFive;
    }

    public void setNoticeFive(String noticeFive) {
        this.noticeFive = noticeFive;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Date getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public RankNotice() {
    }
}
