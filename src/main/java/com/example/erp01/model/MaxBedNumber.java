package com.example.erp01.model;

public class MaxBedNumber {

    private Integer maxBedNumberID;

    private String orderName;

    private Integer bedNumber;

    public Integer getMaxBedNumberID() {
        return maxBedNumberID;
    }

    public void setMaxBedNumberID(Integer maxBedNumberID) {
        this.maxBedNumberID = maxBedNumberID;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public Integer getBedNumber() {
        return bedNumber;
    }

    public void setBedNumber(Integer bedNumber) {
        this.bedNumber = bedNumber;
    }

    public MaxBedNumber() {
    }

    public MaxBedNumber(String orderName, Integer bedNumber) {
        this.orderName = orderName;
        this.bedNumber = bedNumber;
    }

    public MaxBedNumber(Integer maxBedNumberID, String orderName, Integer bedNumber) {
        this.maxBedNumberID = maxBedNumberID;
        this.orderName = orderName;
        this.bedNumber = bedNumber;
    }
}
