package com.example.erp01.model;

import java.util.Date;

public class OrderDetection {

    private Integer id;

    private String orderName;

    private String clothesVersionNumber;

    private String customerName;

    private String colorName;

    private String detectionType;

    private String detectionName;

    private String detectionUrl;

    private Date sendDate;

    private Date detectionDate;

    private String userName;

    private String detectionDepartment;

    private String detectionEmployee;

    private Date createTime;

    private Date updateTime;

    public OrderDetection() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getDetectionType() {
        return detectionType;
    }

    public void setDetectionType(String detectionType) {
        this.detectionType = detectionType;
    }

    public String getDetectionName() {
        return detectionName;
    }

    public void setDetectionName(String detectionName) {
        this.detectionName = detectionName;
    }

    public String getDetectionUrl() {
        return detectionUrl;
    }

    public void setDetectionUrl(String detectionUrl) {
        this.detectionUrl = detectionUrl;
    }

    public Date getDetectionDate() {
        return detectionDate;
    }

    public void setDetectionDate(Date detectionDate) {
        this.detectionDate = detectionDate;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getDetectionDepartment() {
        return detectionDepartment;
    }

    public void setDetectionDepartment(String detectionDepartment) {
        this.detectionDepartment = detectionDepartment;
    }

    public String getDetectionEmployee() {
        return detectionEmployee;
    }

    public void setDetectionEmployee(String detectionEmployee) {
        this.detectionEmployee = detectionEmployee;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Date getSendDate() {
        return sendDate;
    }

    public void setSendDate(Date sendDate) {
        this.sendDate = sendDate;
    }
}
