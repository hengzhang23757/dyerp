package com.example.erp01.model;

import java.util.Date;

public class AccessoryStorage {

    private Integer id;

    private String orderName;

    private String clothesVersionNumber;

    private String accessoryName;

    private String accessoryNumber;

    private String specification = "未输";

    private String accessoryUnit;

    private String accessoryColor = "未输";

    private Float pieceUsage = 0f;

    private String sizeName;

    private String colorName;

    private Float storageCount = 0f;

    private String accessoryLocation;

    private String supplier;

    private Integer orderCount;

    private Integer wellCount;

    private Date updateTime;

    private Integer accessoryID;

    private String accessoryType;

    public Integer getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(Integer orderCount) {
        this.orderCount = orderCount;
    }

    public Integer getWellCount() {
        return wellCount;
    }

    public void setWellCount(Integer wellCount) {
        this.wellCount = wellCount;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public String getAccessoryName() {
        return accessoryName;
    }

    public void setAccessoryName(String accessoryName) {
        this.accessoryName = accessoryName;
    }

    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }

    public String getAccessoryUnit() {
        return accessoryUnit;
    }

    public void setAccessoryUnit(String accessoryUnit) {
        this.accessoryUnit = accessoryUnit;
    }

    public String getAccessoryColor() {
        return accessoryColor;
    }

    public void setAccessoryColor(String accessoryColor) {
        this.accessoryColor = accessoryColor;
    }

    public Float getPieceUsage() {
        return pieceUsage;
    }

    public void setPieceUsage(Float pieceUsage) {
        this.pieceUsage = pieceUsage;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public Float getStorageCount() {
        return storageCount;
    }

    public void setStorageCount(Float storageCount) {
        this.storageCount = storageCount;
    }

    public String getAccessoryLocation() {
        return accessoryLocation;
    }

    public void setAccessoryLocation(String accessoryLocation) {
        this.accessoryLocation = accessoryLocation;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public AccessoryStorage() {
    }

    public String getAccessoryNumber() {
        return accessoryNumber;
    }

    public void setAccessoryNumber(String accessoryNumber) {
        this.accessoryNumber = accessoryNumber;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getAccessoryID() {
        return accessoryID;
    }

    public void setAccessoryID(Integer accessoryID) {
        this.accessoryID = accessoryID;
    }

    public String getAccessoryType() {
        return accessoryType;
    }

    public void setAccessoryType(String accessoryType) {
        this.accessoryType = accessoryType;
    }

    @Override
    public String toString() {
        return "AccessoryStorage{" +
                "id=" + id +
                ", orderName='" + orderName + '\'' +
                ", clothesVersionNumber='" + clothesVersionNumber + '\'' +
                ", accessoryName='" + accessoryName + '\'' +
                ", accessoryNumber='" + accessoryNumber + '\'' +
                ", specification='" + specification + '\'' +
                ", accessoryUnit='" + accessoryUnit + '\'' +
                ", accessoryColor='" + accessoryColor + '\'' +
                ", pieceUsage=" + pieceUsage +
                ", sizeName='" + sizeName + '\'' +
                ", colorName='" + colorName + '\'' +
                ", storageCount=" + storageCount +
                ", accessoryLocation='" + accessoryLocation + '\'' +
                ", supplier='" + supplier + '\'' +
                ", orderCount=" + orderCount +
                ", wellCount=" + wellCount +
                ", updateTime=" + updateTime +
                ", accessoryID=" + accessoryID +
                '}';
    }
}
