package com.example.erp01.model;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class Employee {

    private Integer employeeID;

    private String employeeName;

    private String gender;

    private String employeeNumber;

    private String passWord;

    private String groupName;

    private String identifyCard;

    private String position;

    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date birthday;

    private String nation;

    private String education;

    private String province;

    private String birthPlace;

    private String telephone;

    private String IDCard;

    private String maritalState;

    private String emergencyContact;

    private String relation;

    private String emergencyPhone;

    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date beginDate;

    private String positionState;

    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date leaveDate;

    private String role;

    private String imgUrl;

    private String tmpImgUrl;

    private String bankCardNumber;

    private String cardOfBank;

    private String isManagement;

    private String department;

    private String salaryType;

    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date contractBegin;

    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date contractEnd;

    private String turner;

    private String guid;

    private String ccCount;

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getTurner() {
        return turner;
    }

    public void setTurner(String turner) {
        this.turner = turner;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getIsManagement() {
        return isManagement;
    }

    public void setIsManagement(String isManagement) {
        this.isManagement = isManagement;
    }

    public String getTmpImgUrl() {
        return tmpImgUrl;
    }

    public void setTmpImgUrl(String tmpImgUrl) {
        this.tmpImgUrl = tmpImgUrl;
    }

    public Date getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    public String getPositionState() {
        return positionState;
    }

    public void setPositionState(String positionState) {
        this.positionState = positionState;
    }

    public Date getLeaveDate() {
        return leaveDate;
    }

    public void setLeaveDate(Date leaveDate) {
        this.leaveDate = leaveDate;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public Integer getEmployeeID() {
        return employeeID;
    }

    public void setEmployeeID(Integer employeeID) {
        this.employeeID = employeeID;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmployeeNumber() {
        return employeeNumber;
    }

    public void setEmployeeNumber(String employeeNumber) {
        this.employeeNumber = employeeNumber;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getIdentifyCard() {
        return identifyCard;
    }

    public void setIdentifyCard(String identifyCard) {
        this.identifyCard = identifyCard;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getNation() {
        return nation;
    }

    public void setNation(String nation) {
        this.nation = nation;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getBirthPlace() {
        return birthPlace;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getIDCard() {
        return IDCard;
    }

    public void setIDCard(String IDCard) {
        this.IDCard = IDCard;
    }

    public String getMaritalState() {
        return maritalState;
    }

    public void setMaritalState(String maritalState) {
        this.maritalState = maritalState;
    }

    public String getEmergencyContact() {
        return emergencyContact;
    }

    public void setEmergencyContact(String emergencyContact) {
        this.emergencyContact = emergencyContact;
    }

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    public String getEmergencyPhone() {
        return emergencyPhone;
    }

    public void setEmergencyPhone(String emergencyPhone) {
        this.emergencyPhone = emergencyPhone;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getBankCardNumber() {
        return bankCardNumber;
    }

    public void setBankCardNumber(String bankCardNumber) {
        this.bankCardNumber = bankCardNumber;
    }

    public String getCardOfBank() {
        return cardOfBank;
    }

    public void setCardOfBank(String cardOfBank) {
        this.cardOfBank = cardOfBank;
    }

    public String getSalaryType() {
        return salaryType;
    }

    public void setSalaryType(String salaryType) {
        this.salaryType = salaryType;
    }

    public Date getContractBegin() {
        return contractBegin;
    }

    public void setContractBegin(Date contractBegin) {
        this.contractBegin = contractBegin;
    }

    public Date getContractEnd() {
        return contractEnd;
    }

    public void setContractEnd(Date contractEnd) {
        this.contractEnd = contractEnd;
    }

    public String getCcCount() {
        return ccCount;
    }

    public void setCcCount(String ccCount) {
        this.ccCount = ccCount;
    }

    public Employee() {
    }

    public Employee(String employeeName, String gender, String employeeNumber, String passWord, String groupName, String identifyCard, String position, Date birthday, String nation, String education, String province, String birthPlace, String telephone, String IDCard, String maritalState, String emergencyContact, String relation, String emergencyPhone, Date beginDate, String positionState, Date leaveDate, String role, String imgUrl, String tmpImgUrl, String bankCardNumber, String cardOfBank, String isManagement, String department, String salaryType, Date contractBegin, Date contractEnd, String turner) {
        this.employeeName = employeeName;
        this.gender = gender;
        this.employeeNumber = employeeNumber;
        this.passWord = passWord;
        this.groupName = groupName;
        this.identifyCard = identifyCard;
        this.position = position;
        this.birthday = birthday;
        this.nation = nation;
        this.education = education;
        this.province = province;
        this.birthPlace = birthPlace;
        this.telephone = telephone;
        this.IDCard = IDCard;
        this.maritalState = maritalState;
        this.emergencyContact = emergencyContact;
        this.relation = relation;
        this.emergencyPhone = emergencyPhone;
        this.beginDate = beginDate;
        this.positionState = positionState;
        this.leaveDate = leaveDate;
        this.role = role;
        this.imgUrl = imgUrl;
        this.tmpImgUrl = tmpImgUrl;
        this.bankCardNumber = bankCardNumber;
        this.cardOfBank = cardOfBank;
        this.isManagement = isManagement;
        this.department = department;
        this.salaryType = salaryType;
        this.contractBegin = contractBegin;
        this.contractEnd = contractEnd;
        this.turner = turner;
    }

    public Employee(Integer employeeID, String employeeName, String gender, String employeeNumber, String passWord, String groupName, String identifyCard, String position, Date birthday, String nation, String education, String province, String birthPlace, String telephone, String IDCard, String maritalState, String emergencyContact, String relation, String emergencyPhone, Date beginDate, String positionState, Date leaveDate, String role, String imgUrl, String tmpImgUrl, String bankCardNumber, String cardOfBank, String isManagement, String department, String salaryType, Date contractBegin, Date contractEnd, String turner) {
        this.employeeID = employeeID;
        this.employeeName = employeeName;
        this.gender = gender;
        this.employeeNumber = employeeNumber;
        this.passWord = passWord;
        this.groupName = groupName;
        this.identifyCard = identifyCard;
        this.position = position;
        this.birthday = birthday;
        this.nation = nation;
        this.education = education;
        this.province = province;
        this.birthPlace = birthPlace;
        this.telephone = telephone;
        this.IDCard = IDCard;
        this.maritalState = maritalState;
        this.emergencyContact = emergencyContact;
        this.relation = relation;
        this.emergencyPhone = emergencyPhone;
        this.beginDate = beginDate;
        this.positionState = positionState;
        this.leaveDate = leaveDate;
        this.role = role;
        this.imgUrl = imgUrl;
        this.tmpImgUrl = tmpImgUrl;
        this.bankCardNumber = bankCardNumber;
        this.cardOfBank = cardOfBank;
        this.isManagement = isManagement;
        this.department = department;
        this.salaryType = salaryType;
        this.contractBegin = contractBegin;
        this.contractEnd = contractEnd;
        this.turner = turner;
    }
}
