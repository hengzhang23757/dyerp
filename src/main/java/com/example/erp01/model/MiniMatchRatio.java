package com.example.erp01.model;

public class MiniMatchRatio {

    private String sizeName;

    private Integer ratio;

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public Integer getRatio() {
        return ratio;
    }

    public void setRatio(Integer ratio) {
        this.ratio = ratio;
    }

    public MiniMatchRatio() {
    }

    public MiniMatchRatio(String sizeName, Integer ratio) {
        this.sizeName = sizeName;
        this.ratio = ratio;
    }
}
