package com.example.erp01.model;

public class SizeManage {

    private Integer sizeManageID;

    private String sizeGroup;

    private String sizeName;

    public SizeManage() {
    }

    public SizeManage(Integer sizeManageID, String sizeGroup, String sizeName) {
        this.sizeManageID = sizeManageID;
        this.sizeGroup = sizeGroup;
        this.sizeName = sizeName;
    }

    public SizeManage(String sizeGroup, String sizeName) {
        this.sizeGroup = sizeGroup;
        this.sizeName = sizeName;
    }

    public Integer getSizeManageID() {
        return sizeManageID;
    }

    public void setSizeManageID(Integer sizeManageID) {
        this.sizeManageID = sizeManageID;
    }

    public String getSizeGroup() {
        return sizeGroup;
    }

    public void setSizeGroup(String sizeGroup) {
        this.sizeGroup = sizeGroup;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }
}
