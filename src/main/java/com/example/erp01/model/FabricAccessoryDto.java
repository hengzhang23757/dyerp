package com.example.erp01.model;

import java.util.List;

public class FabricAccessoryDto {

    private String orderName;

    private List<String> customerList;

    private List<String> seasonList;

    private String modelType;

    private String type;

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public List<String> getCustomerList() {
        return customerList;
    }

    public void setCustomerList(List<String> customerList) {
        this.customerList = customerList;
    }

    public List<String> getSeasonList() {
        return seasonList;
    }

    public void setSeasonList(List<String> seasonList) {
        this.seasonList = seasonList;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getModelType() {
        return modelType;
    }

    public void setModelType(String modelType) {
        this.modelType = modelType;
    }

    public FabricAccessoryDto() {
    }
}
