package com.example.erp01.model;

public class GroupProgress {

    private String groupName;

    private String orderName;

    private String clothesVersionNumber;

    private String customerName;

    private String procedureName;

    private Integer layerCount = 0;

    private Integer checkCount = 0;

    private Integer embOutCount = 0;

    private Integer hangCount = 0;

    private Integer inspectionCount = 0;

    private String progressDate;

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public Integer getEmbOutCount() {
        return embOutCount;
    }

    public void setEmbOutCount(Integer embOutCount) {
        this.embOutCount = embOutCount;
    }

    public Integer getHangCount() {
        return hangCount;
    }

    public void setHangCount(Integer hangCount) {
        this.hangCount = hangCount;
    }

    public Integer getInspectionCount() {
        return inspectionCount;
    }

    public void setInspectionCount(Integer inspectionCount) {
        this.inspectionCount = inspectionCount;
    }

    public String getProgressDate() {
        return progressDate;
    }

    public void setProgressDate(String progressDate) {
        this.progressDate = progressDate;
    }

    public Integer getCheckCount() {
        return checkCount;
    }

    public void setCheckCount(Integer checkCount) {
        this.checkCount = checkCount;
    }

    public Integer getLayerCount() {
        return layerCount;
    }

    public void setLayerCount(Integer layerCount) {
        this.layerCount = layerCount;
    }


    public String getProcedureName() {
        return procedureName;
    }

    public void setProcedureName(String procedureName) {
        this.procedureName = procedureName;
    }

    public GroupProgress() {
    }

    public GroupProgress(String groupName, String orderName, String clothesVersionNumber, String customerName, Integer embOutCount, Integer hangCount, Integer inspectionCount) {
        this.groupName = groupName;
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.customerName = customerName;
        this.embOutCount = embOutCount;
        this.hangCount = hangCount;
        this.inspectionCount = inspectionCount;
    }
}
