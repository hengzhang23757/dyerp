package com.example.erp01.model;

public class ClothesDevFabric {

    private Integer id;

    private Integer devId;

    private String clothesDevNumber;

    private String clothesVersionNumber;

    private String orderName;

    private String fabricNumber;

    private String fabricName;  //面料名称

    private String fabricWidth;  //布封

    private String fabricWeight; //克重

    private String fabricContent; // 成分

    private String unit;//单位

    private String unitTwo;//多单位

    private Float ratio = 1f;

    private String partName;//部位

    private Float fabricLoss = 0f;

    private Float pieceUsage = 0f;//单件用量

    private Float pieceUsageTwo = 0f;

    private String colorName;//订单颜色

    private String colorNumber; // 订单色号

    private String isHit;//是否撞色

    private String fabricColor;//面料颜色

    private String fabricColorNumber;

    private String supplier;

    private Float price = 0f;

    private Float priceTwo = 0f;

    private Integer fabricKey;

    private String remark;

    public ClothesDevFabric() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDevId() {
        return devId;
    }

    public void setDevId(Integer devId) {
        this.devId = devId;
    }

    public String getClothesDevNumber() {
        return clothesDevNumber;
    }

    public void setClothesDevNumber(String clothesDevNumber) {
        this.clothesDevNumber = clothesDevNumber;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getFabricNumber() {
        return fabricNumber;
    }

    public void setFabricNumber(String fabricNumber) {
        this.fabricNumber = fabricNumber;
    }

    public String getFabricName() {
        return fabricName;
    }

    public void setFabricName(String fabricName) {
        this.fabricName = fabricName;
    }

    public String getFabricWidth() {
        return fabricWidth;
    }

    public void setFabricWidth(String fabricWidth) {
        this.fabricWidth = fabricWidth;
    }

    public String getFabricWeight() {
        return fabricWeight;
    }

    public void setFabricWeight(String fabricWeight) {
        this.fabricWeight = fabricWeight;
    }

    public String getFabricContent() {
        return fabricContent;
    }

    public void setFabricContent(String fabricContent) {
        this.fabricContent = fabricContent;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getUnitTwo() {
        return unitTwo;
    }

    public void setUnitTwo(String unitTwo) {
        this.unitTwo = unitTwo;
    }

    public String getPartName() {
        return partName;
    }

    public void setPartName(String partName) {
        this.partName = partName;
    }

    public Float getFabricLoss() {
        return fabricLoss;
    }

    public void setFabricLoss(Float fabricLoss) {
        this.fabricLoss = fabricLoss;
    }

    public Float getPieceUsage() {
        return pieceUsage;
    }

    public void setPieceUsage(Float pieceUsage) {
        this.pieceUsage = pieceUsage;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getColorNumber() {
        return colorNumber;
    }

    public void setColorNumber(String colorNumber) {
        this.colorNumber = colorNumber;
    }

    public String getIsHit() {
        return isHit;
    }

    public void setIsHit(String isHit) {
        this.isHit = isHit;
    }

    public String getFabricColor() {
        return fabricColor;
    }

    public void setFabricColor(String fabricColor) {
        this.fabricColor = fabricColor;
    }

    public String getFabricColorNumber() {
        return fabricColorNumber;
    }

    public void setFabricColorNumber(String fabricColorNumber) {
        this.fabricColorNumber = fabricColorNumber;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Float getPriceTwo() {
        return priceTwo;
    }

    public void setPriceTwo(Float priceTwo) {
        this.priceTwo = priceTwo;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Float getPieceUsageTwo() {
        return pieceUsageTwo;
    }

    public void setPieceUsageTwo(Float pieceUsageTwo) {
        this.pieceUsageTwo = pieceUsageTwo;
    }

    public Integer getFabricKey() {
        return fabricKey;
    }

    public void setFabricKey(Integer fabricKey) {
        this.fabricKey = fabricKey;
    }

    public Float getRatio() {
        return ratio;
    }

    public void setRatio(Float ratio) {
        this.ratio = ratio;
    }
}
