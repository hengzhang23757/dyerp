package com.example.erp01.model;

import java.util.Date;

public class HomePageInfo {

    private Integer infoID;

    private Integer clothesVersionNumber;

    private Integer orderName;

    private String infoType;

    private String infoDetail;

    private String infoDateStr;

    private String infoState;

    public Integer getInfoID() {
        return infoID;
    }

    public void setInfoID(Integer infoID) {
        this.infoID = infoID;
    }

    public Integer getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(Integer clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public Integer getOrderName() {
        return orderName;
    }

    public void setOrderName(Integer orderName) {
        this.orderName = orderName;
    }

    public String getInfoType() {
        return infoType;
    }

    public void setInfoType(String infoType) {
        this.infoType = infoType;
    }

    public String getInfoDetail() {
        return infoDetail;
    }

    public void setInfoDetail(String infoDetail) {
        this.infoDetail = infoDetail;
    }

    public String getInfoDateStr() {
        return infoDateStr;
    }

    public void setInfoDateStr(String infoDateStr) {
        this.infoDateStr = infoDateStr;
    }

    public String getInfoState() {
        return infoState;
    }

    public void setInfoState(String infoState) {
        this.infoState = infoState;
    }

    public HomePageInfo() {
    }
}
