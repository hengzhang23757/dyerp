package com.example.erp01.model;

import java.util.Date;

public class EmbOutDaily {

    private String colorName;

    private String sizeName;

    private String groupName;

    private Date outDate;

    private Integer outCount;

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public Date getOutDate() {
        return outDate;
    }

    public void setOutDate(Date outDate) {
        this.outDate = outDate;
    }

    public Integer getOutCount() {
        return outCount;
    }

    public void setOutCount(Integer outCount) {
        this.outCount = outCount;
    }

    public EmbOutDaily() {
    }

    public EmbOutDaily(String colorName, String sizeName, String groupName, Date outDate, Integer outCount) {
        this.colorName = colorName;
        this.sizeName = sizeName;
        this.groupName = groupName;
        this.outDate = outDate;
        this.outCount = outCount;
    }
}
