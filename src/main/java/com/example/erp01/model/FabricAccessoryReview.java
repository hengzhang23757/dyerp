package com.example.erp01.model;

import java.util.Date;

public class FabricAccessoryReview {

    private String reviewType;

    private String orderName;

    private String clothesVersionNumber;

    private String materialName;

    private String supplier;

    private String checkNumber;

    private String checkState;

    private String orderState;

    private String season;

    private String customerName;

    private String checkDate;

    private String preCheck;

    private String preCheckEmp;
    private String checkEmp;
    private Date preCheckTime;
    private Date checkTime;

    public String getReviewType() {
        return reviewType;
    }

    public void setReviewType(String reviewType) {
        this.reviewType = reviewType;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getMaterialName() {
        return materialName;
    }

    public void setMaterialName(String materialName) {
        this.materialName = materialName;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public String getCheckNumber() {
        return checkNumber;
    }

    public void setCheckNumber(String checkNumber) {
        this.checkNumber = checkNumber;
    }

    public String getCheckState() {
        return checkState;
    }

    public void setCheckState(String checkState) {
        this.checkState = checkState;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public String getOrderState() {
        return orderState;
    }

    public void setOrderState(String orderState) {
        this.orderState = orderState;
    }

    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCheckDate() {
        return checkDate;
    }

    public void setCheckDate(String checkDate) {
        this.checkDate = checkDate;
    }

    public String getPreCheck() {
        return preCheck;
    }

    public void setPreCheck(String preCheck) {
        this.preCheck = preCheck;
    }

    public String getPreCheckEmp() {
        return preCheckEmp;
    }

    public void setPreCheckEmp(String preCheckEmp) {
        this.preCheckEmp = preCheckEmp;
    }

    public String getCheckEmp() {
        return checkEmp;
    }

    public void setCheckEmp(String checkEmp) {
        this.checkEmp = checkEmp;
    }

    public Date getPreCheckTime() {
        return preCheckTime;
    }

    public void setPreCheckTime(Date preCheckTime) {
        this.preCheckTime = preCheckTime;
    }

    public Date getCheckTime() {
        return checkTime;
    }

    public void setCheckTime(Date checkTime) {
        this.checkTime = checkTime;
    }

    public FabricAccessoryReview() {
    }
}
