package com.example.erp01.model;

public class YesterdayReWork {

    private String clothesVersionNumber;

    private String orderName;

    private Integer procedureNumber;

    private String procedureName;

    private Integer reWorkCount = 0;

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public Integer getProcedureNumber() {
        return procedureNumber;
    }

    public void setProcedureNumber(Integer procedureNumber) {
        this.procedureNumber = procedureNumber;
    }

    public String getProcedureName() {
        return procedureName;
    }

    public void setProcedureName(String procedureName) {
        this.procedureName = procedureName;
    }

    public Integer getReWorkCount() {
        return reWorkCount;
    }

    public void setReWorkCount(Integer reWorkCount) {
        this.reWorkCount = reWorkCount;
    }

    public YesterdayReWork() {
    }

    public YesterdayReWork(String clothesVersionNumber, String orderName, Integer procedureNumber, String procedureName, Integer reWorkCount) {
        this.clothesVersionNumber = clothesVersionNumber;
        this.orderName = orderName;
        this.procedureNumber = procedureNumber;
        this.procedureName = procedureName;
        this.reWorkCount = reWorkCount;
    }
}
