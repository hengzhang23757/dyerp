package com.example.erp01.model;

import java.util.Date;

public class FabricStorage {

    private Integer fabricStorageID;

    private String location;

    private String orderName;

    private String clothesVersionNumber;

    private String fabricName;

    private String fabricNumber;

    private String fabricColor;

    private String fabricColorNumber;

    private String jarName;

    private Float weight;

    private Integer batchNumber;

    private String operateType;

    private Date returnTime;

    private String colorName;

    private String unit;

    private String isHit;

    private String supplier;

    private Integer fabricID;

    public String getFabricNumber() {
        return fabricNumber;
    }

    public void setFabricNumber(String fabricNumber) {
        this.fabricNumber = fabricNumber;
    }

    public String getFabricColorNumber() {
        return fabricColorNumber;
    }

    public void setFabricColorNumber(String fabricColorNumber) {
        this.fabricColorNumber = fabricColorNumber;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Integer getFabricStorageID() {
        return fabricStorageID;
    }

    public void setFabricStorageID(Integer fabricStorageID) {
        this.fabricStorageID = fabricStorageID;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public String getFabricName() {
        return fabricName;
    }

    public void setFabricName(String fabricName) {
        this.fabricName = fabricName;
    }

    public String getFabricColor() {
        return fabricColor;
    }

    public void setFabricColor(String fabricColor) {
        this.fabricColor = fabricColor;
    }

    public String getJarName() {
        return jarName;
    }

    public void setJarName(String jarName) {
        this.jarName = jarName;
    }

    public Float getWeight() {
        return weight;
    }

    public void setWeight(Float weight) {
        this.weight = weight;
    }

    public Integer getBatchNumber() {
        return batchNumber;
    }

    public void setBatchNumber(Integer batchNumber) {
        this.batchNumber = batchNumber;
    }

    public String getOperateType() {
        return operateType;
    }

    public void setOperateType(String operateType) {
        this.operateType = operateType;
    }

    public Date getReturnTime() {
        return returnTime;
    }

    public void setReturnTime(Date returnTime) {
        this.returnTime = returnTime;
    }

    public String getIsHit() {
        return isHit;
    }

    public void setIsHit(String isHit) {
        this.isHit = isHit;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public Integer getFabricID() {
        return fabricID;
    }

    public void setFabricID(Integer fabricID) {
        this.fabricID = fabricID;
    }

    public FabricStorage() {
    }

    public FabricStorage(String location, String orderName, String clothesVersionNumber, String fabricName, String fabricNumber, String fabricColor, String fabricColorNumber, String jarName, Float weight, Integer batchNumber, String operateType, Date returnTime, String colorName, String unit, String isHit, String supplier, Integer fabricID) {
        this.location = location;
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.fabricName = fabricName;
        this.fabricNumber = fabricNumber;
        this.fabricColor = fabricColor;
        this.fabricColorNumber = fabricColorNumber;
        this.jarName = jarName;
        this.weight = weight;
        this.batchNumber = batchNumber;
        this.operateType = operateType;
        this.returnTime = returnTime;
        this.colorName = colorName;
        this.unit = unit;
        this.isHit = isHit;
        this.supplier = supplier;
        this.fabricID = fabricID;
    }

    @Override
    public String toString() {
        return "FabricStorage{" +
                "fabricStorageID=" + fabricStorageID +
                ", location='" + location + '\'' +
                ", orderName='" + orderName + '\'' +
                ", clothesVersionNumber='" + clothesVersionNumber + '\'' +
                ", fabricName='" + fabricName + '\'' +
                ", fabricNumber='" + fabricNumber + '\'' +
                ", fabricColor='" + fabricColor + '\'' +
                ", fabricColorNumber='" + fabricColorNumber + '\'' +
                ", jarName='" + jarName + '\'' +
                ", weight=" + weight +
                ", batchNumber=" + batchNumber +
                ", operateType='" + operateType + '\'' +
                ", returnTime=" + returnTime +
                ", colorName='" + colorName + '\'' +
                ", unit='" + unit + '\'' +
                ", isHit='" + isHit + '\'' +
                ", supplier='" + supplier + '\'' +
                ", fabricID=" + fabricID +
                '}';
    }
}
