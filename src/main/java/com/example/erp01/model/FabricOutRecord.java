package com.example.erp01.model;

import java.util.Date;

public class FabricOutRecord {

    private Integer fabricOutRecordID;

    private String location;

    private String orderName;

    private String clothesVersionNumber;

    private String colorName;

    private String fabricName;

    private String fabricColor;

    private String jarName;

    private String operateType;

    private Float weight;

    private Integer batchNumber;

    private String remark;

    private Date returnTime;

    private String isHit;

    private String supplier;

    private String fabricNumber;

    private String fabricColorNumber;

    private String unit;

    private String receiver;

    private Integer fabricID;

    private String checkNumber;

    private String checkMonth;

    private Float returnMoney = 0f;

    public Integer getFabricOutRecordID() {
        return fabricOutRecordID;
    }

    public void setFabricOutRecordID(Integer fabricOutRecordID) {
        this.fabricOutRecordID = fabricOutRecordID;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public String getFabricName() {
        return fabricName;
    }

    public void setFabricName(String fabricName) {
        this.fabricName = fabricName;
    }

    public String getFabricColor() {
        return fabricColor;
    }

    public void setFabricColor(String fabricColor) {
        this.fabricColor = fabricColor;
    }

    public String getJarName() {
        return jarName;
    }

    public void setJarName(String jarName) {
        this.jarName = jarName;
    }

    public String getOperateType() {
        return operateType;
    }

    public void setOperateType(String operateType) {
        this.operateType = operateType;
    }

    public Float getWeight() {
        return weight;
    }

    public void setWeight(Float weight) {
        this.weight = weight;
    }

    public Integer getBatchNumber() {
        return batchNumber;
    }

    public void setBatchNumber(Integer batchNumber) {
        this.batchNumber = batchNumber;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Date getReturnTime() {
        return returnTime;
    }

    public void setReturnTime(Date returnTime) {
        this.returnTime = returnTime;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getIsHit() {
        return isHit;
    }

    public void setIsHit(String isHit) {
        this.isHit = isHit;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public String getFabricNumber() {
        return fabricNumber;
    }

    public void setFabricNumber(String fabricNumber) {
        this.fabricNumber = fabricNumber;
    }

    public String getFabricColorNumber() {
        return fabricColorNumber;
    }

    public void setFabricColorNumber(String fabricColorNumber) {
        this.fabricColorNumber = fabricColorNumber;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public Integer getFabricID() {
        return fabricID;
    }

    public void setFabricID(Integer fabricID) {
        this.fabricID = fabricID;
    }

    public String getCheckNumber() {
        return checkNumber;
    }

    public void setCheckNumber(String checkNumber) {
        this.checkNumber = checkNumber;
    }

    public String getCheckMonth() {
        return checkMonth;
    }

    public void setCheckMonth(String checkMonth) {
        this.checkMonth = checkMonth;
    }

    public FabricOutRecord() {
    }

    public FabricOutRecord(String location, String orderName, String clothesVersionNumber, String colorName, String fabricName, String fabricColor, String jarName, String operateType, Float weight, Integer batchNumber, String remark, Date returnTime, String isHit, String supplier, String fabricNumber, String fabricColorNumber, String unit) {
        this.location = location;
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.colorName = colorName;
        this.fabricName = fabricName;
        this.fabricColor = fabricColor;
        this.jarName = jarName;
        this.operateType = operateType;
        this.weight = weight;
        this.batchNumber = batchNumber;
        this.remark = remark;
        this.returnTime = returnTime;
        this.isHit = isHit;
        this.supplier = supplier;
        this.fabricNumber = fabricNumber;
        this.fabricColorNumber = fabricColorNumber;
        this.unit = unit;
    }

    public FabricOutRecord(Integer fabricOutRecordID, String location, String orderName, String clothesVersionNumber, String colorName, String fabricName, String fabricColor, String jarName, String operateType, Float weight, Integer batchNumber, String remark, Date returnTime, String isHit, String supplier, String fabricNumber, String fabricColorNumber, String unit) {
        this.fabricOutRecordID = fabricOutRecordID;
        this.location = location;
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.colorName = colorName;
        this.fabricName = fabricName;
        this.fabricColor = fabricColor;
        this.jarName = jarName;
        this.operateType = operateType;
        this.weight = weight;
        this.batchNumber = batchNumber;
        this.remark = remark;
        this.returnTime = returnTime;
        this.isHit = isHit;
        this.supplier = supplier;
        this.fabricNumber = fabricNumber;
        this.fabricColorNumber = fabricColorNumber;
        this.unit = unit;
    }

    @Override
    public String toString() {
        return "FabricOutRecord{" +
                "fabricOutRecordID=" + fabricOutRecordID +
                ", location='" + location + '\'' +
                ", orderName='" + orderName + '\'' +
                ", clothesVersionNumber='" + clothesVersionNumber + '\'' +
                ", colorName='" + colorName + '\'' +
                ", fabricName='" + fabricName + '\'' +
                ", fabricColor='" + fabricColor + '\'' +
                ", jarName='" + jarName + '\'' +
                ", operateType='" + operateType + '\'' +
                ", weight=" + weight +
                ", batchNumber=" + batchNumber +
                ", remark='" + remark + '\'' +
                ", returnTime=" + returnTime +
                ", isHit='" + isHit + '\'' +
                ", supplier='" + supplier + '\'' +
                ", fabricNumber='" + fabricNumber + '\'' +
                ", fabricColorNumber='" + fabricColorNumber + '\'' +
                ", unit='" + unit + '\'' +
                ", receiver='" + receiver + '\'' +
                ", fabricID=" + fabricID +
                '}';
    }
}
