package com.example.erp01.model;

public class Group {

    private Integer groupID;

    private String groupName;

    private String type;

    public Group(Integer groupID, String groupName) {
        this.groupID = groupID;
        this.groupName = groupName;
    }

    public Group() {
    }

    public Integer getGroupID() {
        return groupID;
    }

    public void setGroupID(Integer groupID) {
        this.groupID = groupID;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
