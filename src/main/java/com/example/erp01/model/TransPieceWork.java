package com.example.erp01.model;

public class TransPieceWork {

    private String employeeNumber;

    private String groupName;

    private String employeeName;

    private String orderName;

    private String clothesVersionNumber;

    private String procedureNumber;

    private String procedureName;

    private Integer pieceCount;

    private Double piecePrice;

    private Double salary;

    public String getEmployeeNumber() {
        return employeeNumber;
    }

    public void setEmployeeNumber(String employeeNumber) {
        this.employeeNumber = employeeNumber;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public String getProcedureNumber() {
        return procedureNumber;
    }

    public void setProcedureNumber(String procedureNumber) {
        this.procedureNumber = procedureNumber;
    }

    public String getProcedureName() {
        return procedureName;
    }

    public void setProcedureName(String procedureName) {
        this.procedureName = procedureName;
    }

    public Integer getPieceCount() {
        return pieceCount;
    }

    public void setPieceCount(Integer pieceCount) {
        this.pieceCount = pieceCount;
    }

    public Double getPiecePrice() {
        return piecePrice;
    }

    public void setPiecePrice(Double piecePrice) {
        this.piecePrice = piecePrice;
    }

    public Double getSalary() {
        return salary;
    }

    public void setSalary(Double salary) {
        this.salary = salary;
    }

    public TransPieceWork() {
    }

    public TransPieceWork(String employeeNumber, String groupName, String employeeName, String orderName, String clothesVersionNumber, String procedureNumber, String procedureName, Integer pieceCount, Double piecePrice, Double salary) {
        this.employeeNumber = employeeNumber;
        this.groupName = groupName;
        this.employeeName = employeeName;
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.procedureNumber = procedureNumber;
        this.procedureName = procedureName;
        this.pieceCount = pieceCount;
        this.piecePrice = piecePrice;
        this.salary = salary;
    }
}
