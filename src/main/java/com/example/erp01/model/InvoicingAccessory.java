package com.example.erp01.model;

public class InvoicingAccessory {

    private Integer accessoryID;

    private String orderName;

    private String clothesVersionNumber;

    private String accessoryName;

    private String accessoryNumber;

    private String specification;

    private String accessoryUnit;

    private String accessoryColor;

    private String sizeName;

    private String colorName;

    private String supplier;

    private Float totalInStore = 0f;

    private Float totalStorage = 0f;

    private Float totalOutStore = 0f;

    private Float inStoreCount = 0f;

    private Float outStoreCount = 0f;

    public InvoicingAccessory() {
    }

    public Integer getAccessoryID() {
        return accessoryID;
    }

    public void setAccessoryID(Integer accessoryID) {
        this.accessoryID = accessoryID;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public String getAccessoryName() {
        return accessoryName;
    }

    public void setAccessoryName(String accessoryName) {
        this.accessoryName = accessoryName;
    }

    public String getAccessoryNumber() {
        return accessoryNumber;
    }

    public void setAccessoryNumber(String accessoryNumber) {
        this.accessoryNumber = accessoryNumber;
    }

    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }

    public String getAccessoryUnit() {
        return accessoryUnit;
    }

    public void setAccessoryUnit(String accessoryUnit) {
        this.accessoryUnit = accessoryUnit;
    }

    public String getAccessoryColor() {
        return accessoryColor;
    }

    public void setAccessoryColor(String accessoryColor) {
        this.accessoryColor = accessoryColor;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public Float getTotalInStore() {
        return totalInStore;
    }

    public void setTotalInStore(Float totalInStore) {
        this.totalInStore = totalInStore;
    }

    public Float getTotalStorage() {
        return totalStorage;
    }

    public void setTotalStorage(Float totalStorage) {
        this.totalStorage = totalStorage;
    }

    public Float getTotalOutStore() {
        return totalOutStore;
    }

    public void setTotalOutStore(Float totalOutStore) {
        this.totalOutStore = totalOutStore;
    }

    public Float getInStoreCount() {
        return inStoreCount;
    }

    public void setInStoreCount(Float inStoreCount) {
        this.inStoreCount = inStoreCount;
    }

    public Float getOutStoreCount() {
        return outStoreCount;
    }

    public void setOutStoreCount(Float outStoreCount) {
        this.outStoreCount = outStoreCount;
    }

    public InvoicingAccessory(Integer accessoryID, String orderName, String clothesVersionNumber, String accessoryName, String accessoryNumber, String specification, String accessoryUnit, String accessoryColor, String sizeName, String colorName, String supplier) {
        this.accessoryID = accessoryID;
        this.orderName = orderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.accessoryName = accessoryName;
        this.accessoryNumber = accessoryNumber;
        this.specification = specification;
        this.accessoryUnit = accessoryUnit;
        this.accessoryColor = accessoryColor;
        this.sizeName = sizeName;
        this.colorName = colorName;
        this.supplier = supplier;
    }
}
