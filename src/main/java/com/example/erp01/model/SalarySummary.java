package com.example.erp01.model;

public class SalarySummary {

    private String employeeNumber;

    private String employeeName;

    private String groupName;

    private String position;

    private Float pieceSalary = 0f;

    private Float pieceSalaryTwo = 0f;

    private Float hourSalary = 0f;

    private Float bonus;

    private Float sumSalary;

    public String getEmployeeNumber() {
        return employeeNumber;
    }

    public void setEmployeeNumber(String employeeNumber) {
        this.employeeNumber = employeeNumber;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public Float getPieceSalary() {
        return pieceSalary;
    }

    public void setPieceSalary(Float pieceSalary) {
        this.pieceSalary = pieceSalary;
    }

    public Float getPieceSalaryTwo() {
        return pieceSalaryTwo;
    }

    public void setPieceSalaryTwo(Float pieceSalaryTwo) {
        this.pieceSalaryTwo = pieceSalaryTwo;
    }

    public Float getHourSalary() {
        return hourSalary;
    }

    public void setHourSalary(Float hourSalary) {
        this.hourSalary = hourSalary;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public SalarySummary() {
    }

    public Float getBonus() {
        return bonus;
    }

    public void setBonus(Float bonus) {
        this.bonus = bonus;
    }

    public Float getSumSalary() {
        return sumSalary;
    }

    public void setSumSalary(Float sumSalary) {
        this.sumSalary = sumSalary;
    }

    public SalarySummary(String employeeNumber, String employeeName, String groupName, String position, Float pieceSalary, Float pieceSalaryTwo, Float hourSalary) {
        this.employeeNumber = employeeNumber;
        this.employeeName = employeeName;
        this.groupName = groupName;
        this.position = position;
        this.pieceSalary = pieceSalary;
        this.pieceSalaryTwo = pieceSalaryTwo;
        this.hourSalary = hourSalary;
    }

    @Override
    public String toString() {
        return "SalarySummary{" +
                "employeeNumber='" + employeeNumber + '\'' +
                ", employeeName='" + employeeName + '\'' +
                ", groupName='" + groupName + '\'' +
                ", position='" + position + '\'' +
                ", pieceSalary=" + pieceSalary +
                ", pieceSalaryTwo=" + pieceSalaryTwo +
                ", hourSalary=" + hourSalary +
                ", bonus=" + bonus +
                ", sumSalary=" + sumSalary +
                '}';
    }
}
