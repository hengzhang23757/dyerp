package com.example.erp01.model;

public class ProductionDetailLeak {

    private Integer orderCount = 0;

    private Integer cutCount = 0;

    private Integer productionCount = 0;

    private Integer finishCount = 0;

    private Integer leakCount = 0;

    public Integer getFinishCount() {
        return finishCount;
    }

    public void setFinishCount(Integer finishCount) {
        this.finishCount = finishCount;
    }

    public Integer getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(Integer orderCount) {
        this.orderCount = orderCount;
    }

    public Integer getCutCount() {
        return cutCount;
    }

    public void setCutCount(Integer cutCount) {
        this.cutCount = cutCount;
    }

    public Integer getProductionCount() {
        return productionCount;
    }

    public void setProductionCount(Integer productionCount) {
        this.productionCount = productionCount;
    }

    public Integer getLeakCount() {
        return leakCount;
    }

    public void setLeakCount(Integer leakCount) {
        this.leakCount = leakCount;
    }

    public ProductionDetailLeak() {
    }

    public ProductionDetailLeak(Integer orderCount, Integer cutCount, Integer productionCount, Integer leakCount) {
        this.orderCount = orderCount;
        this.cutCount = cutCount;
        this.productionCount = productionCount;
        this.leakCount = leakCount;
    }

    public ProductionDetailLeak(Integer orderCount, Integer cutCount, Integer productionCount, Integer finishCount, Integer leakCount) {
        this.orderCount = orderCount;
        this.cutCount = cutCount;
        this.productionCount = productionCount;
        this.finishCount = finishCount;
        this.leakCount = leakCount;
    }
}
