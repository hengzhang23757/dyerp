package com.example.erp01.model;

public class CheckMonth {

    private Integer id;

    private String checkMonth;

    public CheckMonth() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCheckMonth() {
        return checkMonth;
    }

    public void setCheckMonth(String checkMonth) {
        this.checkMonth = checkMonth;
    }
}
