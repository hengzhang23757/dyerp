package com.example.erp01.model;

import java.util.Date;

public class EmpRoom {

    private Integer empRoomID;

    private String employeeNumber;

    private String employeeName;

    private String groupName;

    private String floor;

    private String roomNumber;

    private String bedNumber;

    private Date checkInDate;

    private Integer capacity;

    public Integer getCapacity() {
        return capacity;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    public EmpRoom() {
    }

    public EmpRoom(String employeeNumber, String employeeName, String groupName, String floor, String roomNumber, String bedNumber, Date checkInDate) {
        this.employeeNumber = employeeNumber;
        this.employeeName = employeeName;
        this.groupName = groupName;
        this.floor = floor;
        this.roomNumber = roomNumber;
        this.bedNumber = bedNumber;
        this.checkInDate = checkInDate;
    }

    public EmpRoom(String employeeNumber, String employeeName, String groupName, String floor, String roomNumber, String bedNumber, Date checkInDate, Integer capacity) {
        this.employeeNumber = employeeNumber;
        this.employeeName = employeeName;
        this.groupName = groupName;
        this.floor = floor;
        this.roomNumber = roomNumber;
        this.bedNumber = bedNumber;
        this.checkInDate = checkInDate;
        this.capacity = capacity;
    }

    public EmpRoom(Integer empRoomID, String employeeNumber, String employeeName, String groupName, String floor, String roomNumber, String bedNumber, Date checkInDate, Integer capacity) {
        this.empRoomID = empRoomID;
        this.employeeNumber = employeeNumber;
        this.employeeName = employeeName;
        this.groupName = groupName;
        this.floor = floor;
        this.roomNumber = roomNumber;
        this.bedNumber = bedNumber;
        this.checkInDate = checkInDate;
        this.capacity = capacity;
    }

    public EmpRoom(Integer empRoomID, String employeeNumber, String employeeName, String groupName, String floor, String roomNumber, String bedNumber, Date checkInDate) {
        this.empRoomID = empRoomID;
        this.employeeNumber = employeeNumber;
        this.employeeName = employeeName;
        this.groupName = groupName;
        this.floor = floor;
        this.roomNumber = roomNumber;
        this.bedNumber = bedNumber;
        this.checkInDate = checkInDate;
    }

    public Integer getEmpRoomID() {
        return empRoomID;
    }

    public void setEmpRoomID(Integer empRoomID) {
        this.empRoomID = empRoomID;
    }

    public String getEmployeeNumber() {
        return employeeNumber;
    }

    public void setEmployeeNumber(String employeeNumber) {
        this.employeeNumber = employeeNumber;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getFloor() {
        return floor;
    }

    public void setFloor(String floor) {
        this.floor = floor;
    }

    public String getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(String roomNumber) {
        this.roomNumber = roomNumber;
    }

    public String getBedNumber() {
        return bedNumber;
    }

    public void setBedNumber(String bedNumber) {
        this.bedNumber = bedNumber;
    }

    public Date getCheckInDate() {
        return checkInDate;
    }

    public void setCheckInDate(Date checkInDate) {
        this.checkInDate = checkInDate;
    }
}
