package com.example.erp01.model;

public class FabricOrder {

    private Integer fabricOrderID;

    private String fabricOrderName;

    private String clothesVersionNumber;

    private String orderName;

    private String fabricName;

    private String fabricNumber;

    private String fabricTypeNumber;

    private String fabricColor;

    private String fabricColorNumber;

    private String skcColor;

    private String fabricUse;

    private Float singleCount;

    private Integer orderCount;

    private Float fabricSumCount;

    private String fabricSupplier;

    private Float price;

    private Float total;

    private String deliveryDate;

    private Float width;

    private Float weight;

    private String filling;

    private String twist;

    private String standard;

    private String remark;

    private Integer stateType;

    public String getFabricTypeNumber() {
        return fabricTypeNumber;
    }

    public void setFabricTypeNumber(String fabricTypeNumber) {
        this.fabricTypeNumber = fabricTypeNumber;
    }

    public String getFabricSupplier() {
        return fabricSupplier;
    }

    public void setFabricSupplier(String fabricSupplier) {
        this.fabricSupplier = fabricSupplier;
    }

    public Integer getStateType() {
        return stateType;
    }

    public void setStateType(Integer stateType) {
        this.stateType = stateType;
    }

    public Integer getFabricOrderID() {
        return fabricOrderID;
    }

    public void setFabricOrderID(Integer fabricOrderID) {
        this.fabricOrderID = fabricOrderID;
    }

    public String getFabricOrderName() {
        return fabricOrderName;
    }

    public void setFabricOrderName(String fabricOrderName) {
        this.fabricOrderName = fabricOrderName;
    }

    public String getClothesVersionNumber() {
        return clothesVersionNumber;
    }

    public void setClothesVersionNumber(String clothesVersionNumber) {
        this.clothesVersionNumber = clothesVersionNumber;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getFabricName() {
        return fabricName;
    }

    public void setFabricName(String fabricName) {
        this.fabricName = fabricName;
    }

    public String getFabricNumber() {
        return fabricNumber;
    }

    public void setFabricNumber(String fabricNumber) {
        this.fabricNumber = fabricNumber;
    }

    public String getFabricColor() {
        return fabricColor;
    }

    public void setFabricColor(String fabricColor) {
        this.fabricColor = fabricColor;
    }

    public String getFabricColorNumber() {
        return fabricColorNumber;
    }

    public void setFabricColorNumber(String fabricColorNumber) {
        this.fabricColorNumber = fabricColorNumber;
    }

    public String getSkcColor() {
        return skcColor;
    }

    public void setSkcColor(String skcColor) {
        this.skcColor = skcColor;
    }

    public String getFabricUse() {
        return fabricUse;
    }

    public void setFabricUse(String fabricUse) {
        this.fabricUse = fabricUse;
    }

    public Float getSingleCount() {
        return singleCount;
    }

    public void setSingleCount(Float singleCount) {
        this.singleCount = singleCount;
    }

    public Integer getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(Integer orderCount) {
        this.orderCount = orderCount;
    }

    public Float getFabricSumCount() {
        return fabricSumCount;
    }

    public void setFabricSumCount(Float fabricSumCount) {
        this.fabricSumCount = fabricSumCount;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Float getTotal() {
        return total;
    }

    public void setTotal(Float total) {
        this.total = total;
    }

    public String getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public Float getWidth() {
        return width;
    }

    public void setWidth(Float width) {
        this.width = width;
    }

    public Float getWeight() {
        return weight;
    }

    public void setWeight(Float weight) {
        this.weight = weight;
    }

    public String getFilling() {
        return filling;
    }

    public void setFilling(String filling) {
        this.filling = filling;
    }

    public String getTwist() {
        return twist;
    }

    public void setTwist(String twist) {
        this.twist = twist;
    }

    public String getStandard() {
        return standard;
    }

    public void setStandard(String standard) {
        this.standard = standard;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public FabricOrder() {
    }

    public FabricOrder(String fabricOrderName, String clothesVersionNumber, String orderName, String fabricName, String fabricNumber, String fabricTypeNumber, String fabricColor, String fabricColorNumber, String skcColor, String fabricUse, Float singleCount, Integer orderCount, Float fabricSumCount, String fabricSupplier, Float price, Float total, String deliveryDate, Float width, Float weight, String filling, String twist, String standard, String remark, Integer stateType) {
        this.fabricOrderName = fabricOrderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.orderName = orderName;
        this.fabricName = fabricName;
        this.fabricNumber = fabricNumber;
        this.fabricTypeNumber = fabricTypeNumber;
        this.fabricColor = fabricColor;
        this.fabricColorNumber = fabricColorNumber;
        this.skcColor = skcColor;
        this.fabricUse = fabricUse;
        this.singleCount = singleCount;
        this.orderCount = orderCount;
        this.fabricSumCount = fabricSumCount;
        this.fabricSupplier = fabricSupplier;
        this.price = price;
        this.total = total;
        this.deliveryDate = deliveryDate;
        this.width = width;
        this.weight = weight;
        this.filling = filling;
        this.twist = twist;
        this.standard = standard;
        this.remark = remark;
        this.stateType = stateType;
    }

    public FabricOrder(Integer fabricOrderID, String fabricOrderName, String clothesVersionNumber, String orderName, String fabricName, String fabricNumber, String fabricTypeNumber, String fabricColor, String fabricColorNumber, String skcColor, String fabricUse, Float singleCount, Integer orderCount, Float fabricSumCount, String fabricSupplier, Float price, Float total, String deliveryDate, Float width, Float weight, String filling, String twist, String standard, String remark, Integer stateType) {
        this.fabricOrderID = fabricOrderID;
        this.fabricOrderName = fabricOrderName;
        this.clothesVersionNumber = clothesVersionNumber;
        this.orderName = orderName;
        this.fabricName = fabricName;
        this.fabricNumber = fabricNumber;
        this.fabricTypeNumber = fabricTypeNumber;
        this.fabricColor = fabricColor;
        this.fabricColorNumber = fabricColorNumber;
        this.skcColor = skcColor;
        this.fabricUse = fabricUse;
        this.singleCount = singleCount;
        this.orderCount = orderCount;
        this.fabricSumCount = fabricSumCount;
        this.fabricSupplier = fabricSupplier;
        this.price = price;
        this.total = total;
        this.deliveryDate = deliveryDate;
        this.width = width;
        this.weight = weight;
        this.filling = filling;
        this.twist = twist;
        this.standard = standard;
        this.remark = remark;
        this.stateType = stateType;
    }



}
