package com.example.erp01.action;

import com.example.erp01.model.*;
import com.example.erp01.service.*;
import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import org.apache.ibatis.annotations.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.SimpleDateFormat;
import java.util.*;

//裁片基本操作，包括生成裁片信息，保存裁片信息，查询裁片数据等
@Controller
@RequestMapping(value = "erp")
public class OtherTailorController {
    private static final Logger log = LoggerFactory.getLogger(OtherTailorController.class);
    @Autowired
    private OtherTailorService otherTailorService;

    @Autowired
    private OrderClothesService orderClothesService;

    @Autowired
    private MaxTailorQcodeIDService maxTailorQcodeIDService;

    @Autowired
    private TailorService tailorService;

    @Autowired
    private TailorBackService tailorBackService;

    @Autowired
    private MaxBedNumberService maxBedNumberService;

    private static int tmpPackageNumber;


    @RequestMapping("/bedOtherTailorInfoStart")
    public String bedOtherTailorInfoStart(){
        return "otherTailor/bedOtherTailorInfo";
    }


    @RequestMapping("/otherTailorReprintStart")
    public String tailorReprintStart(Model model){
        model.addAttribute("bigMenuTag",3);
        model.addAttribute("menuTag",35);
        return "otherTailor/otherTailorReprint";
    }

    @RequestMapping("/otherTailorChangeCodeStart")
    public String otherTailorChangeCodeStart(Model model){
        model.addAttribute("bigMenuTag",3);
        model.addAttribute("menuTag",35);
        return "otherTailor/otherTailorChangeCode";
    }

    @RequestMapping("/addOtherSmallMultiTailorStart")
    public String addOtherSmallMultiTailorStart(Model model,String orderName){
        if(orderName == null) {
            model.addAttribute("type", "add");
        }else {
            model.addAttribute("orderName", orderName);
            model.addAttribute("type", "detail");
        }
        return "otherTailor/fb_addOtherSmallMultiTailor";
    }

    @RequestMapping("/otherCutReportStart")
    public String cutReportStart(Model model){
        model.addAttribute("bigMenuTag",4);
        model.addAttribute("menuTag",44);
        return "otherTailor/otherCutReport";
    }

    @RequestMapping("/oneOtherTailorInfoStart")
    public String oneOtherTailorInfoStart(Model model){
        model.addAttribute("bigMenuTag",3);
        model.addAttribute("menuTag",34);
        return "otherTailor/oneOtherTailorInfo";
    }


    @RequestMapping("/otherMultiTailorStart")
    public String otherMultiTailorStart(Model model){
        model.addAttribute("bigMenuTag",3);
        model.addAttribute("menuTag",36);
        return "otherTailor/otherMultiTailor";
    }

    @RequestMapping("/addMultiOtherTailorStart")
    public String addMultiOtherTailorStart(Model model,String orderName){
        if(orderName == null) {
            model.addAttribute("type", "add");
        }else {
            model.addAttribute("orderName", orderName);
            model.addAttribute("type", "detail");
        }
        return "otherTailor/fb_addOtherMultiTailor";
    }

    @RequestMapping(value = "/saveothertailordata")
    @ResponseBody
    public int saveTailorData(@RequestParam("tailorList")String tailorList,
                              @RequestParam("groupName")String groupName){
        Gson gson = new Gson();
        List<Tailor> tailorList1 = gson.fromJson(tailorList,new TypeToken<List<Tailor>>(){}.getType());
        for (Tailor tailor : tailorList1){
            tailor.setGroupName(groupName);
            tailor.setInitCount(tailor.getLayerCount());
        }
        int res1 = tailorService.saveTailorData(tailorList1);
        if (res1 == 0){
            return 0;
        }else {
            return 1;
        }
    }

    /**
     * 获取所有的裁床数据
     * @return
     */
    @RequestMapping(value = "/getallothertailordata", method = RequestMethod.GET)
    @ResponseBody
    public List<OtherTailor> getAllTailorData(){
        List<OtherTailor> tailorList = otherTailorService.getAllOtherTailorData();
        return tailorList;
    }


    @RequestMapping(value = "/getothertailordatabyorder", method = RequestMethod.GET)
    @ResponseBody
    public List<OtherTailor> getAllOtherTailorDataByOrder(@RequestParam("orderName")String orderName){
        List<OtherTailor> tailorList = otherTailorService.getAllOtherTailorDataByOrder(orderName);
        return tailorList;
    }


    @RequestMapping(value = "/othertailorreport", method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getTailorReport(@RequestParam("orderName")String orderName,
                                              @RequestParam("bedNumber")String bedNumber,
                                              @RequestParam("partName")String partName){
        Integer bn;
        if ("".equals(bedNumber)){
            bn = null;
        }else {
            bn = Integer.parseInt(bedNumber);
        }
        Map<String,Object> map = new LinkedHashMap<>();
        List<TailorReport> reportList = otherTailorService.otherTailorReport(orderName, partName, bn);
        List<String> sizeList = new ArrayList<>();
        List<String> colorList = new ArrayList<>();
        for (int i=0;i<reportList.size();i++){
            String tmpSize = reportList.get(i).getSizeName();
            String tmpColor = reportList.get(i).getColorName();
            if (!sizeList.contains(tmpSize)){
                sizeList.add(tmpSize);
            }
            if (!colorList.contains(tmpColor)){
                colorList.add(tmpColor);
            }
        }
        List<String> desiredOrder = Arrays.asList("XXXXXS","5XS","XXXXS","4XS","XXXS","3XS","XXS","2XS","XS","XS/25","xs","S","S/27","M","M/29","L","L/31","XL",
                "XL/33","XXL","2XL","XXL/36","XXXL","3XL","XXXL/38","4XL","XXXXL","5XL","XXXXXL","6XL","XXXXXXL","80","090","90","100","110","110/27",
                "120","120/29","130","130/31","140","140/33","150","150/36","155","160","165","170","175","180","185","190");
        Comparator<String> sizeOrder = Comparator.comparingInt(desiredOrder::indexOf);
        sizeList.sort(sizeOrder);
        Map<String, Object> colorMap = new LinkedHashMap<>();
        for (String color : colorList){
            Map<String, Object> sizeMap = new LinkedHashMap<>();
            for (String size : sizeList){
                int tmpSizeCount = 0;
                for (TailorReport tailorReport : reportList){
                    if (tailorReport.getColorName().equals(color) && tailorReport.getSizeName().equals(size)){
                        tmpSizeCount += tailorReport.getLayerCount();
                    }
                }
                sizeMap.put(size,tmpSizeCount);
            }
            colorMap.put(color,sizeMap);
        }
        int oneLength = 0;
        int twoLength = 0;
        int threeLength = 0;
        if (reportList.size() == 0){
            oneLength = 0;
            twoLength = 0;
            threeLength = 0;
        } else if (reportList.size() == 1){
            oneLength = 1;
            twoLength = 0;
            threeLength = 0;
        } else if (reportList.size() == 2){
            oneLength = 1;
            twoLength = 1;
            threeLength = 0;
        } else if (reportList.size()%3 == 0){
            oneLength = reportList.size()/3;
            twoLength = reportList.size()/3;
            threeLength = reportList.size()/3;
        } else if (reportList.size()%3 == 1){
            oneLength = reportList.size()/3 + 1;
            twoLength = reportList.size()/3;
            threeLength = reportList.size()/3;
        }else if (reportList.size()%3 == 2){
            oneLength = reportList.size()/3 + 1;
            twoLength = reportList.size()/3 + 1;
            threeLength = reportList.size()/3;
        }
        map.put("detail",reportList);
        map.put("color",colorMap);
        map.put("size", sizeList);
        map.put("oneLength",oneLength);
        map.put("twoLength",twoLength);
        map.put("threeLength",threeLength);
        return map;
    }



    /**
     * 由订单号生成床号的提示
     * @param orderName
     * @return
     */
    @RequestMapping(value = "/getotherbednumbersbyordername", method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getOtherBedNumbersByOrderName(@RequestParam("orderName")String orderName){
        Map<String,Object> result = new HashMap<>();
        List<Integer> bedNumList = otherTailorService.getOtherBedNumbersByOrderName(orderName);
        result.put("bedNumList",bedNumList);
        return result;
    }

    /**
     * 由订单号生成尺码的提示
     * @param orderName
     * @return
     */
    @RequestMapping(value = "/getothersizenamesbyordername", method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getOtherSizeNamesByOrderName(@RequestParam("orderName")String orderName){
        Map<String,Object> result = new HashMap<>();
        List<String> sizeNameList = otherTailorService.getOtherSizeNamesByOrderName(orderName);
        result.put("sizeNameList",sizeNameList);
        return result;
    }


    @RequestMapping(value = "/getothertailorinfobyordername",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getOtherTailorInfoByOrderName(@RequestParam("orderName")String orderName){
        Map<String,Object> map = new HashMap<>();
        List<Integer> bedNumberList = otherTailorService.getOtherBedNumbersByOrderName(orderName);
        List<TailorInfo> otherTailorInfoList = new ArrayList<>();
        for (int i = 0; i < bedNumberList.size(); i++) {
            TailorInfo tailorInfo = new TailorInfo(orderName,bedNumberList.get(i));
            otherTailorInfoList.add(tailorInfo);
        }
        map.put("otherTailorInfoList",otherTailorInfoList);
        return map;
    }


    @RequestMapping(value = "/deleteothertailorbyorderbed",method = RequestMethod.POST)
    @ResponseBody
    public int deleteOtherTailorByOrderBed(@RequestParam("orderName")String orderName,
                            @RequestParam("bedNumber")Integer bedNumber){
        int res = otherTailorService.deleteOtherTailorByOrderBed(orderName, bedNumber);
        return res;
    }

    @RequestMapping(value = "/getallothertailorbyorderbed",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getAllTailorByOrderBed(@RequestParam("orderName")String orderName,
                                                     @RequestParam("bedNumber")String bedNumber,
                                                     @RequestParam("packageNumber")String packageNumber){
        Integer pn;
        Integer bn;
        if ("".equals(packageNumber)){
            pn = null;
        }else {
            pn = Integer.parseInt(packageNumber);
        }
        if ("".equals(bedNumber)){
            bn = null;
        }else {
            bn = Integer.parseInt(bedNumber);
        }
        Map<String,Object> map = new HashMap<>();
        List<OtherTailor> otherTailorList = otherTailorService.getAllOtherTailorByOrderBed(orderName, bn,pn);
        map.put("otherTailorList",otherTailorList);
        return map;

    }

    @RequestMapping(value = "/getotherpartnamesbyordername",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getOtherPartNamesByOrderName(@RequestParam("orderName")String orderName){
        Map<String,Object> map = new HashMap<>();
        List<String> otherPartNameList = otherTailorService.getOtherPartNamesByOrderName(orderName);
        map.put("otherPartNameList",otherPartNameList);
        return map;
    }

    @RequestMapping(value = "/minigetotherpartnamesbyordername",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> miniGetOtherPartNamesByOrderName(@RequestParam("orderName")String orderName){
        Map<String,Object> map = new HashMap<>();
        List<String> otherPartNameList = otherTailorService.getOtherPartNamesByOrderName(orderName);
        map.put("otherPartNameList",otherPartNameList);
        return map;
    }



    @RequestMapping(value = "/getothertailorinfobyorderpart",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getTailorInfoByOrderName(@RequestParam("orderName")String orderName,
                                                       @RequestParam("partName")String partName){
        Map<String,Object> map = new HashMap<>();
        List<Integer> bedNumberList = otherTailorService.getOtherBedNumbersByOrderPart(orderName, partName);
        List<OtherTailorInfo> otherTailorInfoList = new ArrayList<>();
        for (int i = 0; i < bedNumberList.size(); i++) {
            OtherTailorInfo otherTailorInfo = new OtherTailorInfo(orderName,partName,bedNumberList.get(i));
            otherTailorInfoList.add(otherTailorInfo);
        }
        map.put("otherTailorInfoList",otherTailorInfoList);
        return map;
    }


    @RequestMapping(value = "/deleteothertailorbyorderpartbed",method = RequestMethod.POST)
    @ResponseBody
    public int deleteOtherTailorByOrderBed(@RequestParam("orderName")String orderName,
                                           @RequestParam("partName")String partName,
                                           @RequestParam("bedNumber")Integer bedNumber,
                                           @RequestParam("userName")String userName){
        log.warn("配料按床删除---"+"订单号："+orderName+"---部位："+partName+"---床号："+bedNumber+"---操作用户："+userName);
        int res = otherTailorService.deleteByOrderPartBed(orderName, partName, bedNumber);
        return res;
    }

    @RequestMapping(value = "/getotherbednumbersbyorderpart",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getOtherBedNumbersByOrderPart(@RequestParam("orderName")String orderName,
                                                            @RequestParam("partName")String partName){
        Map<String,Object> map = new HashMap<>();
        List<Integer> otherBedNumberList = otherTailorService.getOtherBedNumbersByOrderPart(orderName, partName);
        map.put("otherBedNumberList",otherBedNumberList);
        return map;
    }

    @RequestMapping(value = "/getdetailothertailorbyorderpartbed",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getDetailOtherTailorByOrderPartBed(@RequestParam("orderName")String orderName,
                                                            @RequestParam("partName")String partName,
                                                                 @RequestParam("bedNumber")Integer bedNumber){
        Map<String,Object> map = new HashMap<>();
        List<OtherTailor> otherTailorDetailList = otherTailorService.getDetailOtherTailorByOrderPartBed(orderName, partName, bedNumber);
        map.put("otherTailorDetailList",otherTailorDetailList);
        return map;
    }

    @RequestMapping(value = "/deleteothertailorbyorderbedpack",method = RequestMethod.POST)
    @ResponseBody
    public int deleteOtherTailorByOrderBedPack(@RequestParam("orderName")String orderName,
                                               @RequestParam("bedNumber")Integer bedNumber,
                                               @RequestParam("userName")String userName,
                                               @RequestParam("packageNumberList")List<Integer> packageNumberList){
        log.warn("配料按扎删除---"+"订单号："+orderName+"---床号："+bedNumber+"---操作用户："+userName+"---删除扎号："+packageNumberList);
        int res = otherTailorService.deleteOtherTailorByOrderBedPack(orderName, bedNumber, packageNumberList);
        return res;
    }

    @RequestMapping(value = "/getotherpackagenumbersbyorderbedcolor",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getOtherPackageNumbersByOrderBedColor(@RequestParam("orderName")String orderName,
                                                               @RequestParam("bedNumber")Integer bedNumber,
                                                               @RequestParam("colorName")String colorName){
        Map<String,Object> map = new HashMap<>();
        List<Integer> packageNumberList = otherTailorService.getOtherPackageNumbersByOrderBedColor(orderName, bedNumber, colorName);
        map.put("packageNumberList",packageNumberList);
        return map;
    }

    @RequestMapping(value = "/getotherlayercountbyorderbedpackage",method = RequestMethod.GET)
    @ResponseBody
    public Integer getOtherLayerCountByOrderBedPackage(@RequestParam("orderName")String orderName,
                                                  @RequestParam("bedNumber")Integer bedNumber,
                                                  @RequestParam("packageNumber")Integer packageNumber){
        Integer res = 0;
        res = otherTailorService.getOtherLayerCountByOrderBedPackage(orderName, bedNumber, packageNumber);
        return res;
    }

    @RequestMapping(value = "/updateothertailordata", method = RequestMethod.POST)
    @ResponseBody
    public int updateOtherTailorData(@RequestParam("otherTailorList")String otherTailorList){
        Gson gson = new Gson();
        List<OtherTailor> otherTailorList1 = gson.fromJson(otherTailorList,new TypeToken<List<OtherTailor>>(){}.getType());
        List<OtherTailor> otherTailorList2 = new ArrayList<>();
        for (OtherTailor otherTailor : otherTailorList1){
            String orderName = otherTailor.getOrderName();
            String customerName = otherTailor.getCustomerName();
            String bedNumber = otherTailor.getBedNumber().toString();
            String jarName = otherTailor.getJarName();
            String colorName = otherTailor.getColorName();
            String sizeName = otherTailor.getSizeName();
            String layerCount = otherTailor.getLayerCount().toString();
            String packageNumber = otherTailor.getPackageNumber().toString();
            String partName = otherTailor.getPartName();
            String tailorQcode = orderName+"-"+customerName+"-"+bedNumber+"-"+jarName+"-"+colorName+"-"+sizeName+"-"+layerCount+"-"+packageNumber+"-"+partName;
            OtherTailor ot = new OtherTailor(otherTailor.getTailorID(),otherTailor.getLayerCount(),otherTailor.getInitCount(),tailorQcode, otherTailor.getGroupName(),otherTailor.getWeight());
            otherTailorList2.add(ot);
        }
        return otherTailorService.updateOtherTailorData(otherTailorList2);
    }



    @RequestMapping(value = "/minigenerateothertailordata", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> miniGenerateMainTailorData(@RequestParam("otherTailorJson")String otherTailorJson){
        System.out.println(otherTailorJson);
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        Map<String, Object> map = new LinkedHashMap<>();
        MiniTailor miniTailor = gson.fromJson(otherTailorJson,new TypeToken<MiniTailor>(){}.getType());
        String orderName = miniTailor.getOrderName();
        String clothesVersionNumber = miniTailor.getClothesVersionNumber();
        String customerName = orderClothesService.getCustomerNameByOrderName(orderName);
        List<MiniTailorLayerInfo> miniTailorLayerInfoList = miniTailor.getMiniTailorLayerInfoList();
        List<MiniMatchRatio> miniMatchRatioList = miniTailor.getMiniMatchRatioList();
        List<String> partNameList = miniTailor.getPartNameList();
        List<OtherTailor> otherTailorList = new ArrayList<>();
        tmpPackageNumber = 10000;
        Integer tmpMaxPackageNumber = tailorService.getMaxPackageNumberByOrder(orderName, 1);
        if(tmpMaxPackageNumber != null){
            tmpPackageNumber = tmpMaxPackageNumber > 10000 ? tmpMaxPackageNumber : 10000;
        }
        synchronized (this) {
            int tmpMaxTailorQcodeID = maxTailorQcodeIDService.getMaxTailorQcodeId();
            int bedNumber = tailorService.getMaxBedNumberByOrderTailorType(orderName, 1) + 1;
            for (int i = 0; i < partNameList.size(); i++) {
                int tmpNumber = tmpPackageNumber;
                for (MiniMatchRatio miniMatchRatio : miniMatchRatioList){
                    for (MiniTailorLayerInfo miniTailorLayerInfo : miniTailorLayerInfoList){
                        tmpNumber += 1;
                        tmpMaxTailorQcodeID += 1;
                        String tmpTailorQcode = orderName + "-" + customerName + "-" + bedNumber + "-" + miniTailorLayerInfo.getJarName() + "-" + miniTailorLayerInfo.getColorName() + "-" + miniMatchRatio.getSizeName() + "-" + miniTailorLayerInfo.getLayerCount()*miniMatchRatio.getRatio() + "-" + tmpNumber + "-" + partNameList.get(i);
                        OtherTailor otherTailor = new OtherTailor(orderName, clothesVersionNumber, customerName, bedNumber, miniTailorLayerInfo.getJarName(), miniTailorLayerInfo.getColorName(), miniMatchRatio.getSizeName(), partNameList.get(i), miniTailorLayerInfo.getLayerCount()*miniMatchRatio.getRatio(), tmpNumber, tmpTailorQcode, tmpMaxTailorQcodeID, miniTailorLayerInfo.getWeight(), miniTailorLayerInfo.getBatch());
                        otherTailorList.add(otherTailor);
                    }
                }
            }
            maxTailorQcodeIDService.updateMaxTailorQcodeId(tmpMaxTailorQcodeID);
        }
        map.put("tailorList", otherTailorList);
        return map;
    }

    @RequestMapping(value = "/generateothertailordata", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> generateMainTailorData(@RequestParam("pcOtherTailorJson")String pcOtherTailorJson,
                                                      @RequestParam(value = "ratioSplit", required = false)Integer ratioSplit){
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        Map<String, Object> map = new LinkedHashMap<>();
        PcTailor pcTailor = gson.fromJson(pcOtherTailorJson,new TypeToken<PcTailor>(){}.getType());
        String orderName = pcTailor.getOrderName();
        String clothesVersionNumber = pcTailor.getClothesVersionNumber();
        String customerName = pcTailor.getCustomerName();
        Integer bedNumber = pcTailor.getBedNumber();
        List<MiniTailorLayerInfo> miniTailorLayerInfoList = pcTailor.getMiniTailorLayerInfoList();
        List<MiniMatchRatio> miniMatchRatioList = pcTailor.getMiniMatchRatioList();
        List<String> partNameList = pcTailor.getPartNameList();
        List<Tailor> tailorList = new ArrayList<>();
        tmpPackageNumber = 10000;
        Integer tmpMaxPackageNumber = tailorService.getMaxPackageNumberByOrder(orderName, 1);
        if(tmpMaxPackageNumber != null){
            tmpPackageNumber = tmpMaxPackageNumber > 10000 ? tmpMaxPackageNumber : 10000;
        }
        if (null == ratioSplit || ratioSplit.equals(0)){
            synchronized (this) {
                int tmpMaxTailorQcodeID = maxTailorQcodeIDService.getMaxTailorQcodeId();
                for (int i = 0; i < partNameList.size(); i++) {
                    int tmpNumber = tmpPackageNumber;
                    for (MiniMatchRatio miniMatchRatio : miniMatchRatioList){
                        for (MiniTailorLayerInfo miniTailorLayerInfo : miniTailorLayerInfoList){
                            tmpNumber += 1;
                            tmpMaxTailorQcodeID += 1;
                            String tmpTailorQcode = orderName + "-" + customerName + "-" + bedNumber + "-" + miniTailorLayerInfo.getJarName() + "-" + miniTailorLayerInfo.getColorName() + "-" + miniMatchRatio.getSizeName() + "-" + miniTailorLayerInfo.getLayerCount()*miniMatchRatio.getRatio() + "-" + tmpNumber + "-" + partNameList.get(i);
                            Tailor tailor = new Tailor(orderName, clothesVersionNumber, customerName, bedNumber, miniTailorLayerInfo.getJarName(), miniTailorLayerInfo.getColorName(), miniMatchRatio.getSizeName(), partNameList.get(i), miniTailorLayerInfo.getLayerCount()*miniMatchRatio.getRatio(), tmpNumber, tmpTailorQcode, tmpMaxTailorQcodeID, miniTailorLayerInfo.getWeight(), miniTailorLayerInfo.getBatch());
                            tailorList.add(tailor);
                        }
                    }
                }
                maxTailorQcodeIDService.updateMaxTailorQcodeId(tmpMaxTailorQcodeID);
            }
        } else {
            synchronized (this) {
                int tmpMaxTailorQcodeID = maxTailorQcodeIDService.getMaxTailorQcodeId();
                for (int i = 0; i < partNameList.size(); i++) {
                    int tmpNumber = tmpPackageNumber;
                    for (MiniMatchRatio miniMatchRatio : miniMatchRatioList){
                        for (int ratioIndex = 0; ratioIndex < miniMatchRatio.getRatio(); ratioIndex ++){
                            for (MiniTailorLayerInfo miniTailorLayerInfo : miniTailorLayerInfoList){
                                tmpNumber += 1;
                                tmpMaxTailorQcodeID += 1;
                                String tmpTailorQcode = orderName + "-" + customerName + "-" + bedNumber + "-" + miniTailorLayerInfo.getJarName() + "-" + miniTailorLayerInfo.getColorName() + "-" + miniMatchRatio.getSizeName() + "-" + miniTailorLayerInfo.getLayerCount()*ratioSplit + "-" + tmpNumber + "-" + partNameList.get(i);
                                Tailor tailor = new Tailor(orderName, clothesVersionNumber, customerName, bedNumber, miniTailorLayerInfo.getJarName(), miniTailorLayerInfo.getColorName(), miniMatchRatio.getSizeName(), partNameList.get(i), miniTailorLayerInfo.getLayerCount()*ratioSplit, tmpNumber, tmpTailorQcode, tmpMaxTailorQcodeID, miniTailorLayerInfo.getWeight(), miniTailorLayerInfo.getBatch());
                                tailorList.add(tailor);
                            }
                        }

                    }
                }
                maxTailorQcodeIDService.updateMaxTailorQcodeId(tmpMaxTailorQcodeID);
            }
        }

        map.put("tailorList", tailorList);
        return map;
    }

    @RequestMapping(value = "/getOtherMaxTailorQcodeId")
    @ResponseBody
    public int getOtherMaxTailorQcodeId(int number) {
        int maxTailorQcodeID = maxTailorQcodeIDService.getMaxTailorQcodeId();
        maxTailorQcodeIDService.updateMaxTailorQcodeId(maxTailorQcodeID+number);
        return maxTailorQcodeID;
    }

    @RequestMapping(value = "/getOtherMaxPackageNumberByOrder")
    @ResponseBody
    public int getOtherMaxPackageNumberByOrder(String orderName) {
        tmpPackageNumber = 0;
        Integer tmpMaxPackageNumber = otherTailorService.getOtherMaxPackageNumberByOrder(orderName);
        if(tmpMaxPackageNumber != null){
            tmpPackageNumber = tmpMaxPackageNumber;
        }
        return tmpPackageNumber;
    }

    @RequestMapping(value = "/minigenerateothertailordatacombine", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> miniGenerateMainTailorDataCombine(@RequestParam("otherTailorJson")String otherTailorJson){
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        Map<String, Object> map = new LinkedHashMap<>();
        MiniTailor miniTailor = gson.fromJson(otherTailorJson,new TypeToken<MiniTailor>(){}.getType());
        String orderName = miniTailor.getOrderName();
        String clothesVersionNumber = miniTailor.getClothesVersionNumber();
        String customerName = orderClothesService.getCustomerNameByOrderName(orderName);
        List<MiniTailorLayerInfo> miniTailorLayerInfoList = miniTailor.getMiniTailorLayerInfoList();
        List<MiniMatchRatio> miniMatchRatioList = miniTailor.getMiniMatchRatioList();
        List<String> partNameList = miniTailor.getPartNameList();
        List<OtherTailor> tailorList = new ArrayList<>();
        Integer fixedNumber = miniTailor.getFixedNumber();
        tmpPackageNumber = 10000;
        Integer tmpMaxPackageNumber = tailorService.getMaxPackageNumberByOrder(orderName, 1);
        if(tmpMaxPackageNumber != null){
            tmpPackageNumber = tmpMaxPackageNumber > 10000 ? tmpMaxPackageNumber : 10000;
        }
        synchronized (this) {
            int tmpMaxTailorQcodeID = maxTailorQcodeIDService.getMaxTailorQcodeId();
            int bedNumber = tailorService.getMaxBedNumberByOrderTailorType(orderName, 1) + 1;
            for (int i = 0; i < partNameList.size(); i++) {
                int tmpNumber = tmpPackageNumber;
                for (MiniMatchRatio miniMatchRatio : miniMatchRatioList){
                    Integer ratio = miniMatchRatio.getRatio();
                    for (int j = 0; j < ratio; j++){
                        for (MiniTailorLayerInfo miniTailorLayerInfo : miniTailorLayerInfoList){
                            tmpNumber += 1;
                            tmpMaxTailorQcodeID += 1;
                            String tmpTailorQcode = orderName + "-" + customerName + "-" + bedNumber + "-" + miniTailorLayerInfo.getJarName() + "-" + miniTailorLayerInfo.getColorName() + "-" + miniMatchRatio.getSizeName() + "-" + miniTailorLayerInfo.getLayerCount()*fixedNumber + "-" + tmpNumber + "-" + partNameList.get(i);
                            OtherTailor otherTailor = new OtherTailor(orderName, clothesVersionNumber, customerName, bedNumber, miniTailorLayerInfo.getJarName(), miniTailorLayerInfo.getColorName(), miniMatchRatio.getSizeName(), partNameList.get(i), miniTailorLayerInfo.getLayerCount()*fixedNumber, tmpNumber, tmpTailorQcode, tmpMaxTailorQcodeID, miniTailorLayerInfo.getWeight(), miniTailorLayerInfo.getBatch());
                            tailorList.add(otherTailor);
                        }
                    }

                }
            }
            maxTailorQcodeIDService.updateMaxTailorQcodeId(tmpMaxTailorQcodeID);
        }
        map.put("tailorList", tailorList);
        return map;
    }


    @RequestMapping(value = "/minisaveothertailordata", method = RequestMethod.POST)
    @ResponseBody
    public int miniSaveMainTailorData(@RequestParam("tailorList")String tailorList,
                                      @RequestParam("groupName")String groupName){
        Gson gson = new Gson();
        List<Tailor> tailorList1 = gson.fromJson(tailorList,new TypeToken<List<Tailor>>(){}.getType());
        for (Tailor tailor : tailorList1){
            int initCount = tailor.getLayerCount();
            tailor.setGroupName(groupName);
            tailor.setInitCount(initCount);
            tailor.setIsDelete(0);
            tailor.setTailorType(1);
            tailor.setPrintTimes(0);
        }
        int res1 = tailorService.saveTailorData(tailorList1);
        if (res1 == 0){
            return 0;
        }else {
            return 1;
        }
    }

    @RequestMapping(value = "/minigetotherbednumbersbyordername", method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> miniGetOtherBedNumbersByOrderName(@RequestParam("orderName")String orderName){
        Map<String,Object> map = new LinkedHashMap<>();
        List<Integer> bedNumList = otherTailorService.getOtherBedNumbersByOrderName(orderName);
        map.put("bedNumList",bedNumList);
        return map;
    }


    @RequestMapping(value = "/minigetothertailordatabyordernamebednumber", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> miniGetOtherTailorDataByOrderNameBedNumber(@RequestParam("orderName")String orderName,
                                                                          @RequestParam("bedNumber")int bedNumber){
        Map<String, Object> map = new LinkedHashMap<>();
        List<OtherTailor> otherTailorList = otherTailorService.getOtherTailorDataByOrderBed(orderName,bedNumber);
        map.put("tailorList", otherTailorList);
        return map;
    }

    @RequestMapping(value = "/minigetothertailorbytailorqcodeid", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> miniGetOtherTailorByTailorQcodeID(@RequestParam("tailorQcodeID") Integer tailorQcodeID){
        Map<String, Object> map = new LinkedHashMap<>();
        Tailor tailor = tailorService.getTailorByTailorQcodeID(tailorQcodeID, 1);
        map.put("otherTailor", tailor);
        return map;
    }

    @RequestMapping(value = "/generateothersmalltailor")
    @ResponseBody
    public Map<String, Object> generateMainSmallTailor(@RequestParam("tailorListJson")String tailorListJson,
                                                       @RequestParam("partNameListJson")String partNameListJson,
                                                       @RequestParam("orderName")String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new Gson();
        List<String> partNameList = gson.fromJson(partNameListJson,new TypeToken<List<String>>(){}.getType());
        List<Tailor> tailorList1 = gson.fromJson(tailorListJson,new TypeToken<List<Tailor>>(){}.getType());
        tmpPackageNumber = 10000;
        Integer tmpMaxPackageNumber = tailorService.getMaxPackageNumberByOrder(orderName, 1);
        if(tmpMaxPackageNumber != null){
            tmpPackageNumber = tmpMaxPackageNumber > 10000 ? tmpMaxPackageNumber : 10000;
        }
        List<Tailor> tailorList = new ArrayList<>();
        synchronized (this) {
            int tmpMaxTailorQcodeID = maxTailorQcodeIDService.getMaxTailorQcodeId();
            for (String partName : partNameList){
                int tmpNumber = tmpPackageNumber;
                for (Tailor tailor : tailorList1){
                    tmpNumber += 1;
                    tmpMaxTailorQcodeID += 1;
                    Tailor destTailor = new Tailor();
                    destTailor.setOrderName(tailor.getOrderName());
                    destTailor.setClothesVersionNumber(tailor.getClothesVersionNumber());
                    destTailor.setPartName(partName);
                    destTailor.setJarName(tailor.getJarName());
                    destTailor.setTailorQcodeID(tmpMaxTailorQcodeID);
                    destTailor.setPackageNumber(tmpNumber);
                    destTailor.setBedNumber(tailor.getBedNumber());
                    destTailor.setIsDelete(0);
                    destTailor.setPrintTimes(0);
                    destTailor.setWeight(1f);
                    destTailor.setBatch(1);
                    destTailor.setTailorType(1);
                    destTailor.setLayerCount(tailor.getLayerCount());
                    destTailor.setInitCount(tailor.getInitCount());
                    destTailor.setCustomerName(tailor.getCustomerName());
                    destTailor.setColorName(tailor.getColorName());
                    destTailor.setSizeName(tailor.getSizeName());
                    tailorList.add(destTailor);
                }
            }
            maxTailorQcodeIDService.updateMaxTailorQcodeId(tmpMaxTailorQcodeID);
        }
        map.put("tailorList", tailorList);
        return map;
    }


}
