package com.example.erp01.action;

import com.example.erp01.service.GroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "erp")
public class GroupController {
    @Autowired
    private GroupService groupService;

    @RequestMapping(value = "/getallgroupname",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getAllGroupName(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> groupList = groupService.getAllGroupNames();
        map.put("groupList",groupList);
        return map;
    }

    @RequestMapping(value = "/minigetallgroupname",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> miniGetAllGroupName(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> groupList = groupService.getAllGroupNames();
        map.put("groupList",groupList);
        return map;
    }

    @RequestMapping(value = "/getbiggroupname",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getBigGroupName(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> groupList = groupService.getBigGroupNames();
        map.put("groupList",groupList);
        return map;
    }

    @RequestMapping(value = "/minigetbiggroupname",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> miniGetBigGroupName(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> groupList = groupService.getBigGroupNames();
        map.put("groupList",groupList);
        return map;
    }

    @RequestMapping(value = "/getsewgroupname",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getSewGroupName(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> groupList = groupService.getSewGroupNames();
        map.put("groupList",groupList);
        return map;
    }

}
