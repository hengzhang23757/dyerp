package com.example.erp01.action;

import com.example.erp01.model.BasicFabric;
import com.example.erp01.service.BasicFabricService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "erp")
public class BasicFabricController {

    @Autowired
    private BasicFabricService basicFabricService;

    @RequestMapping("/basicFabricStart")
    public String basicFabricStart(){
        return "basicInfo/basicFabric";
    }

    @RequestMapping("/addBasicFabricStart")
    public String addBasicFabricStart(){
        return "basicInfo/fb_basicFabricAdd";
    }


    @RequestMapping(value = "/addbasicfabricbatch",method = RequestMethod.POST)
    @ResponseBody
    public int addBasicFabricBatch(@RequestParam("basicFabricJson")String basicFabricJson){
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        List<BasicFabric> basicFabricList = gson.fromJson(basicFabricJson,new TypeToken<List<BasicFabric>>(){}.getType());
        return basicFabricService.addBasicFabricBatch(basicFabricList);
    }

    @RequestMapping(value = "/getallbasicfabric",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getAllBasicFabric(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<BasicFabric> basicFabricList = basicFabricService.getAllBasicFabric();
        map.put("basicFabricList", basicFabricList);
        return map;
    }

    @RequestMapping(value = "/getthreemonthsbasicfabric",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getThreeMonthsBasicFabric(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<BasicFabric> basicFabricList = basicFabricService.getThreeMonthsBasicFabric();
        map.put("basicFabricList", basicFabricList);
        return map;
    }

    @RequestMapping(value = "/deletebasicfabric",method = RequestMethod.POST)
    @ResponseBody
    public int deleteBasicFabric(@RequestParam("basicFabricID")Integer basicFabricID){
        return basicFabricService.deleteBasicFabric(basicFabricID);
    }

    @RequestMapping(value = "/updatebasicfabric",method = RequestMethod.POST)
    @ResponseBody
    public int updateBasicFabric(BasicFabric basicFabric){
        return basicFabricService.updateBasicFabric(basicFabric);
    }

    @RequestMapping(value = "/getbasicfabrichint", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getBasicFabricHint(@RequestParam("keywords")String keywords){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> basicFabricNameList = basicFabricService.getBasicFabricHint(keywords);
        map.put("content",basicFabricNameList);
        map.put("type","success");
        map.put("code",0);
        return map;
    }

    @RequestMapping(value = "/getfabricsupplierhint", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getFabricSupplierHint(@RequestParam("basicFabricName")String basicFabricName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> fabricSupplierList = basicFabricService.getFabricSupplierHint(basicFabricName);
        map.put("fabricSupplierList", fabricSupplierList);
        return map;
    }

    @RequestMapping(value = "/getbasicfabricnumberbynamesupplier", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getBasicFabricNumberByNameSupplier(@RequestParam("basicFabricName")String basicFabricName,
                                                                  @RequestParam("basicFabricSupplier")String basicFabricSupplier){
        Map<String, Object> map = new LinkedHashMap<>();
        String basicFabricNumber = basicFabricService.getBasicFabricNumberByNameSupplier(basicFabricName, basicFabricSupplier);
        map.put("basicFabricNumber", basicFabricNumber);
        return map;
    }

    @RequestMapping(value = "/getbasicfabriccolorbynamesupplier", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getBasicFabricColorByNameSupplier(@RequestParam("basicFabricName")String basicFabricName,
                                                                 @RequestParam("basicFabricSupplier")String basicFabricSupplier){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> basicFabricColorList = basicFabricService.getBasicFabricColorByNameSupplier(basicFabricName, basicFabricSupplier);
        map.put("basicFabricColorList", basicFabricColorList);
        return map;
    }

    @RequestMapping(value = "/getbasicfabriccolornumberbynamesuppliercolor", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getBasicFabricColorNumberByNameSupplierColor(@RequestParam("basicFabricName")String basicFabricName,
                                                                            @RequestParam("basicFabricSupplier")String basicFabricSupplier,
                                                                            @RequestParam("basicFabricColor")String basicFabricColor){
        Map<String, Object> map = new LinkedHashMap<>();
        String basicFabricColorNumber = basicFabricService.getBasicFabricColorNumberByNameSupplierColor(basicFabricName, basicFabricSupplier, basicFabricColor);
        map.put("basicFabricColorNumber", basicFabricColorNumber);
        return map;
    }

    @RequestMapping(value = "/getbasicfabricwidthbynamesuppliercolor", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getBasicFabricWidthByNameSupplierColor(@RequestParam("basicFabricName")String basicFabricName,
                                                                      @RequestParam("basicFabricSupplier")String basicFabricSupplier,
                                                                      @RequestParam("basicFabricColor")String basicFabricColor){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> basicFabricWidthList = basicFabricService.getBasicFabricWidthByNameSupplierColor(basicFabricName, basicFabricSupplier, basicFabricColor);
        map.put("basicFabricWidthList", basicFabricWidthList);
        return map;
    }

    @RequestMapping(value = "/getbasicfabricweightbynamesuppliercolorwidth", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getBasicFabricWeightByNameSupplierColorWidth(@RequestParam("basicFabricName")String basicFabricName,
                                                                            @RequestParam("basicFabricSupplier")String basicFabricSupplier,
                                                                            @RequestParam("basicFabricColor")String basicFabricColor,
                                                                            @RequestParam("basicFabricWidth")String basicFabricWidth){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> basicFabricWeightList = basicFabricService.getBasicFabricWeightByNameSupplierColorWidth(basicFabricName, basicFabricSupplier, basicFabricColor, basicFabricWidth);
        map.put("basicFabricWeightList", basicFabricWeightList);
        return map;
    }

}
