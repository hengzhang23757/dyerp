package com.example.erp01.action;

import com.example.erp01.model.*;
import com.example.erp01.service.CutInStoreService;
import com.example.erp01.service.StorageService;
import com.example.erp01.service.StoreHouseService;
import com.example.erp01.service.TailorService;
import com.google.gson.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

//裁片出入库基本操作，包括裁片入库，裁片调库，裁片出库等
@Controller
@RequestMapping(value = "erp")
public class StorageController {

    private static final Logger log = LoggerFactory.getLogger(StorageController.class);

    @Autowired
    private StorageService storageService;
    @Autowired
    private TailorService tailorService;
    @Autowired
    private CutInStoreService cutInStoreService;
    @Autowired
    private StoreHouseService storeHouseService;

    @RequestMapping(value = "/inStore", method = RequestMethod.POST)
    @ResponseBody
    public int inStore(@RequestParam("inStoreJson")String instoreJson){
        JsonParser jsonParser = new JsonParser();
        int result = 0;
        try{
            JsonObject json = (JsonObject) jsonParser.parse(instoreJson);
            String storehouseLocation = json.get("cutStoreLocation").getAsString();
            JsonArray jsonTailorQcode = json.get("tailorQcode").getAsJsonArray();
            Iterator iterator = jsonTailorQcode.iterator();
            List<Integer> tailorQcodeIDList = new ArrayList<>();
            while(iterator.hasNext()){
                JsonPrimitive next = (JsonPrimitive) iterator.next();
                tailorQcodeIDList.add(Integer.parseInt(next.toString().replace("\"","")));
            }
            List<Tailor> tailorList = tailorService.getTailorsByTailorQcodeIDs(tailorQcodeIDList, 0);
            List<Storage> storageList = new ArrayList<>();
            Tailor firstTailor = tailorList.get(0);
            List<Tailor> selectedTailorList = tailorService.getTailorByInfo(firstTailor.getOrderName(),firstTailor.getColorName(),firstTailor.getSizeName(),firstTailor.getPartName(),firstTailor.getBedNumber(),null,null,0);
            for (Tailor t : selectedTailorList) {
                Storage storage = new Storage(storehouseLocation,t.getOrderName(),t.getBedNumber(),t.getColorName(),t.getSizeName(),t.getLayerCount(),t.getPackageNumber(),t.getPartName(),t.getTailorQcode());
                storageList.add(storage);
            }
            int instoreCount = storageList.size();
            int res1 = storageService.inStore(storageList);
            int res2 = cutInStoreService.cutInStore(storageList);
            if (0 == res1 && 0 == res2){
                result = instoreCount;
            }
        }catch (JsonIOException e){
            e.printStackTrace();
        }catch (JsonSyntaxException e){
            e.printStackTrace();
        }
        return result;
    }


    @RequestMapping(value = "/miniinstore", method = RequestMethod.POST)
    @ResponseBody
    public int miniInStore(@RequestParam("inStoreJson")String instoreJson){
        JsonParser jsonParser = new JsonParser();
        int result = 0;
        try{
            JsonObject json = (JsonObject) jsonParser.parse(instoreJson);
            String storehouseLocation = json.get("cutStoreLocation").getAsString();
            JsonArray jsonTailorQcode = json.get("tailorQcode").getAsJsonArray();
            Iterator iterator = jsonTailorQcode.iterator();
            List<Integer> tailorQcodeIDList = new ArrayList<>();
            while(iterator.hasNext()){
                JsonPrimitive next = (JsonPrimitive) iterator.next();
                tailorQcodeIDList.add(Integer.parseInt(next.toString().replace("\"","")));
            }
            List<Tailor> tailorList = tailorService.getTailorsByTailorQcodeIDs(tailorQcodeIDList, 0);
            List<Storage> storageList = new ArrayList<>();
            Tailor firstTailor = tailorList.get(0);
            List<Tailor> selectedTailorList = tailorService.getTailorByInfo(firstTailor.getOrderName(),firstTailor.getColorName(),firstTailor.getSizeName(),firstTailor.getPartName(),firstTailor.getBedNumber(),null,null,0);
            for (Tailor t : selectedTailorList) {
                Storage storage = new Storage(storehouseLocation,t.getOrderName(),t.getBedNumber(),t.getColorName(),t.getSizeName(),t.getLayerCount(),t.getPackageNumber(),t.getPartName(),t.getTailorQcode());
                storageList.add(storage);
            }
            int instoreCount = storageList.size();
            int res1 = storageService.inStore(storageList);
            int res2 = cutInStoreService.cutInStore(storageList);
            if (0 == res1 && 0 == res2){
                result = instoreCount;
            }
        }catch (JsonIOException e){
            e.printStackTrace();
        }catch (JsonSyntaxException e){
            e.printStackTrace();
        }
        return result;
    }


    @RequestMapping(value = "/miniinstoresingle", method = RequestMethod.POST)
    @ResponseBody
    public int miniInStoreSingle(@RequestParam("inStoreJson")String instoreJson){
        JsonParser jsonParser = new JsonParser();
        int result = 0;
        try{
            JsonObject json = (JsonObject) jsonParser.parse(instoreJson);
            String storehouseLocation = json.get("cutStoreLocation").getAsString();
            JsonArray jsonTailorQcode = json.get("tailors").getAsJsonArray();
            Iterator iterator = jsonTailorQcode.iterator();
            List<Integer> tailorQcodeIDList = new ArrayList<>();
            while(iterator.hasNext()){
                JsonObject tailor = (JsonObject) iterator.next();
                if(!"".equals(tailor.get("tailorQcodeID").getAsString())) {
                    if (tailor.get("tailorQcodeID").getAsString().length() == 9) {
                        tailorQcodeIDList.add(tailor.get("tailorQcodeID").getAsInt());
                    } else if (tailor.get("tailorQcodeID").getAsString().length() == 18) {
                        Integer tailorQcodeIDOne = Integer.parseInt(tailor.get("tailorQcodeID").toString().substring(0, 9));
                        Integer tailorQcodeIDTwo = Integer.parseInt(tailor.get("tailorQcodeID").toString().substring(9));
                        if (!tailorQcodeIDList.contains(tailorQcodeIDOne)) {
                            tailorQcodeIDList.add(tailorQcodeIDOne);
                        }
                        if (!tailorQcodeIDList.contains(tailorQcodeIDTwo)) {
                            tailorQcodeIDList.add(tailorQcodeIDTwo);
                        }
                    } else if (tailor.get("tailorQcodeID").getAsString().length() == 27) {
                        Integer tailorQcodeIDOne = Integer.parseInt(tailor.get("tailorQcodeID").toString().substring(0, 9));
                        Integer tailorQcodeIDTwo = Integer.parseInt(tailor.get("tailorQcodeID").toString().substring(9, 18));
                        Integer tailorQcodeIDThree = Integer.parseInt(tailor.get("tailorQcodeID").toString().substring(18));
                        if (!tailorQcodeIDList.contains(tailorQcodeIDOne)) {
                            tailorQcodeIDList.add(tailorQcodeIDOne);
                        }
                        if (!tailorQcodeIDList.contains(tailorQcodeIDTwo)) {
                            tailorQcodeIDList.add(tailorQcodeIDTwo);
                        }
                        if (!tailorQcodeIDList.contains(tailorQcodeIDThree)) {
                            tailorQcodeIDList.add(tailorQcodeIDThree);
                        }
                    } else if (tailor.get("tailorQcodeID").getAsString().length() > 9) {
                        Integer tailorQcodeIDOne = Integer.parseInt(tailor.get("tailorQcodeID").toString().substring(0, 9));
                        if (!tailorQcodeIDList.contains(tailorQcodeIDOne)) {
                            tailorQcodeIDList.add(tailorQcodeIDOne);
                        }
                    }
                }
            }
            List<Tailor> tailorList = tailorService.getTailorsByTailorQcodeIDs(tailorQcodeIDList, 0);
            List<Storage> storageList = new ArrayList<>();
            for (Tailor tailor : tailorList){
                Storage storage = new Storage(storehouseLocation,tailor.getOrderName(),tailor.getBedNumber(),tailor.getColorName(),tailor.getSizeName(),tailor.getLayerCount(),tailor.getPackageNumber(),tailor.getPartName(),tailor.getTailorQcode());
                storageList.add(storage);
            }
            int instoreCount = storageList.size();
            int res1 = storageService.inStore(storageList);
            int res2 = cutInStoreService.cutInStore(storageList);
            if (0 == res1 && 0 == res2){
                result = instoreCount;
            }
        }catch (JsonIOException e){
            e.printStackTrace();
        }catch (JsonSyntaxException e){
            e.printStackTrace();
        }
        return result;
    }


    @RequestMapping(value = "/inStoreNum", method = RequestMethod.POST)
    @ResponseBody
    public int inStoreNum(@RequestParam("inStoreJson")String instoreJson){
        JsonParser jsonParser = new JsonParser();
        int result = 0;
        try{
            JsonObject json = (JsonObject) jsonParser.parse(instoreJson);
            String storehouseLocation = json.get("cutStoreLocation").getAsString();
            JsonArray jsonTailorQcode = json.get("tailorQcode").getAsJsonArray();
            Iterator iterator = jsonTailorQcode.iterator();
            List<Integer> tailorQcodeIDList = new ArrayList<>();
            while(iterator.hasNext()){
                JsonPrimitive next = (JsonPrimitive) iterator.next();
                tailorQcodeIDList.add(Integer.parseInt(next.toString().replace("\"","")));
            }
            List<Tailor> tailorList = tailorService.getTailorsByTailorQcodeIDs(tailorQcodeIDList, 0);
            if (tailorList == null || tailorList.size() == 0){
                return result;
            }
            Tailor firstTailor = tailorList.get(0);
            List<String> locationList = cutInStoreService.getCutLocationByPackage(firstTailor.getOrderName(), firstTailor.getBedNumber(), firstTailor.getPackageNumber());
            if (storeHouseService.getStoreHouseByLocation(storehouseLocation) == null){
                result = -5;
                return result;
            }
            if (locationList != null && locationList.size() > 0){
                result = -2;
                return result;
            }
            List<Tailor> selectedTailorList = tailorService.getTailorByInfo(firstTailor.getOrderName(),firstTailor.getColorName(),firstTailor.getSizeName(),firstTailor.getPartName(),firstTailor.getBedNumber(),null,null,0);
            result = selectedTailorList.size();
        }catch (JsonIOException e){
            e.printStackTrace();
            result = -1;
        }catch (JsonSyntaxException e){
            e.printStackTrace();
            result = -1;
        }catch (Exception e) {
            e.printStackTrace();
            result = -1;
        }
        return result;
    }


    @RequestMapping(value = "/miniinstorenum", method = RequestMethod.POST)
    @ResponseBody
    public int miniInStoreNum(@RequestParam("inStoreJson")String instoreJson){
        JsonParser jsonParser = new JsonParser();
        int result = 0;
        try{
            JsonObject json = (JsonObject) jsonParser.parse(instoreJson);
            JsonArray jsonTailorQcode = json.get("tailorQcode").getAsJsonArray();
            Iterator iterator = jsonTailorQcode.iterator();
            List<Integer> tailorQcodeIDList = new ArrayList<>();
            while(iterator.hasNext()){
                JsonPrimitive next = (JsonPrimitive) iterator.next();
                tailorQcodeIDList.add(Integer.parseInt(next.toString().replace("\"","")));
            }
            List<Tailor> tailorList = tailorService.getTailorsByTailorQcodeIDs(tailorQcodeIDList, 0);
            if (tailorList == null || tailorList.size() == 0){
                return result;
            }
            Tailor firstTailor = tailorList.get(0);
            List<String> locationList = cutInStoreService.getCutLocationByPackage(firstTailor.getOrderName(), firstTailor.getBedNumber(), firstTailor.getPackageNumber());
            if (locationList != null && locationList.size() > 0){
                result = -2;
                return result;
            }
            List<Tailor> selectedTailorList = tailorService.getTailorByInfo(firstTailor.getOrderName(),firstTailor.getColorName(),firstTailor.getSizeName(),firstTailor.getPartName(),firstTailor.getBedNumber(),null,null,0);
            result = selectedTailorList.size();
        }catch (JsonIOException e){
            e.printStackTrace();
            result = -1;
        }catch (JsonSyntaxException e){
            e.printStackTrace();
            result = -1;
        }catch (Exception e) {
            e.printStackTrace();
            result = -1;
        }
        return result;
    }


    @RequestMapping(value = "/minigetinstoredatabyid", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> miniGetInStoreDataById(@RequestParam("tailorQcodeID")Integer tailorQcodeID){
        Map<String, Object> map = new LinkedHashMap<>();
        Tailor tailor = tailorService.getTailorByTailorQcodeID(tailorQcodeID, 0);
        if (tailor == null){
            map.put("error", "二维码信息不存在");
            return map;
        }
        List<CutLocation> cutLocationList = tailorService.getPackageInfo(tailor.getOrderName(), tailor.getBedNumber(), tailor.getColorName(), tailor.getSizeName(), null);
        map.put("tailorList", cutLocationList);
        return map;
    }


    @RequestMapping(value = "/minicutinstorebatch", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> miniCutInStoreBatch(@RequestParam("tailorQcodeID")Integer tailorQcodeID,
                                                   @RequestParam("cutStoreLocation")String cutStoreLocation){
        Map<String, Object> map = new LinkedHashMap<>();
        Tailor tailor = tailorService.getTailorByTailorQcodeID(tailorQcodeID, 0);
        List<Storage> storageList = new ArrayList<>();
        List<CutLocation> cutLocationList = tailorService.getPackageInfo(tailor.getOrderName(), tailor.getBedNumber(), tailor.getColorName(), tailor.getSizeName(), null);
        for (CutLocation cutLocation : cutLocationList) {
            Storage storage = new Storage(cutStoreLocation,cutLocation.getOrderName(),cutLocation.getBedNumber(),cutLocation.getColorName(),cutLocation.getSizeName(),cutLocation.getLayerCount(),cutLocation.getPackageNumber(),tailor.getPartName(), "通用");
            storageList.add(storage);
        }
        int res1 = storageService.inStore(storageList);
        int res2 = cutInStoreService.cutInStore(storageList);
        if (0 == res1 && 0 == res2){
            map.put("inStoreCount", cutLocationList.size());
        } else {
            map.put("error", "入库失败");
        }
        return map;
    }


    @RequestMapping(value = "/minicutinstoresinglebatch", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> miniCutInStoreSingleBatch(CutInDto cutInDto){
        Map<String, Object> map = new LinkedHashMap<>();
        List<Tailor> tailorList = tailorService.getTailorsByTailorQcodeIDs(cutInDto.getTailorQcodeIDList(), 0);
        List<Storage> storageList = new ArrayList<>();
        for (Tailor tailor : tailorList) {
            Storage storage = new Storage(cutInDto.getCutStoreLocation(),tailor.getOrderName(),tailor.getBedNumber(),tailor.getColorName(),tailor.getSizeName(),tailor.getLayerCount(),tailor.getPackageNumber(),tailor.getPartName(), tailor.getTailorQcode());
            storageList.add(storage);
        }
        int res1 = storageService.inStore(storageList);
        int res2 = cutInStoreService.cutInStore(storageList);
        if (0 == res1 && 0 == res2){
            map.put("inStoreCount", tailorList.size());
        } else {
            map.put("error", "入库失败");
        }
        return map;
    }

//    @RequestMapping(value = "/minioutstoresingle", method = RequestMethod.POST)
//    @ResponseBody
//    public Map<String, Object> miniOutStoreSingle(@RequestParam("tailorQcodeIDList[]") List<Integer> tailorQcodeIDList){
//        Map<String, Object> map = new LinkedHashMap<>();
//        List<Tailor> tailorList = tailorService.getTailorsByTailorQcodeIDs(tailorQcodeIDList, 0);
//        for (Tailor tailor : tailorList){
//            storageService.deleteStorageByInfo(tailor.getOrderName(), tailor.getBedNumber(), tailor.getPackageNumber());
//            cutInStoreService.deleteCutInStoreByInfo(tailor.getOrderName(), tailor.getBedNumber(), tailor.getPackageNumber());
//        }
//        map.put("outStoreCount", tailorList.size());
//        return map;
//    }



    @RequestMapping(value = "/outstore", method = RequestMethod.POST)
    @ResponseBody
    public int outStore(@RequestParam("outStoreJson")String outStoreJson){
        JsonParser jsonParser = new JsonParser();
        int result = 1;
        try{
            JsonObject json = (JsonObject) jsonParser.parse(outStoreJson);
            JsonArray jsonTailorQcode = json.get("tailorQcode").getAsJsonArray();
            Iterator iterator = jsonTailorQcode.iterator();
            List<Integer> tailorQcodeIDList = new ArrayList<>();
            while(iterator.hasNext()){
                JsonPrimitive next = (JsonPrimitive) iterator.next();
                tailorQcodeIDList.add(Integer.parseInt(next.toString().replace("\"","")));
            }
            List<Tailor> tailorList = tailorService.getTailorsByTailorQcodeIDs(tailorQcodeIDList, 0);
            for (Tailor tailor : tailorList){
                storageService.deleteStorageByInfo(tailor.getOrderName(), tailor.getBedNumber(), tailor.getPackageNumber());
                cutInStoreService.deleteCutInStoreByInfo(tailor.getOrderName(), tailor.getBedNumber(), tailor.getPackageNumber());
            }
            result = 0;
        }catch (JsonIOException e){
            e.printStackTrace();
        }catch (JsonSyntaxException e){
            e.printStackTrace();
        }
        return result;
    }



    @RequestMapping(value = "/minioutstore", method = RequestMethod.POST)
    @ResponseBody
    public int miniOutStore(@RequestParam("outStoreJson")String outStoreJson){
        JsonParser jsonParser = new JsonParser();
        int result = 1;
        try{
            JsonObject json = (JsonObject) jsonParser.parse(outStoreJson);
            JsonArray jsonTailorQcode = json.get("tailorQcode").getAsJsonArray();
            Iterator iterator = jsonTailorQcode.iterator();
            List<Integer> tailorQcodeIDList = new ArrayList<>();
            while(iterator.hasNext()){
                JsonPrimitive next = (JsonPrimitive) iterator.next();
                tailorQcodeIDList.add(Integer.parseInt(next.toString().replace("\"","")));
            }
            List<Tailor> tailorList = tailorService.getTailorsByTailorQcodeIDs(tailorQcodeIDList, 0);
            for (Tailor tailor : tailorList){
                storageService.deleteStorageByInfo(tailor.getOrderName(), tailor.getBedNumber(), tailor.getPackageNumber());
                cutInStoreService.deleteCutInStoreByInfo(tailor.getOrderName(), tailor.getBedNumber(), tailor.getPackageNumber());
            }
            result = 0;
        }catch (JsonIOException e){
            e.printStackTrace();
        }catch (JsonSyntaxException e){
            e.printStackTrace();
        }
        return result;
    }


    @RequestMapping(value = "/minioutstoresingle", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> miniOutStoreSingle(CutInDto cutInDto){
        Map<String, Object> map = new LinkedHashMap<>();
        List<Tailor> tailorList = tailorService.getTailorsByTailorQcodeIDs(cutInDto.getTailorQcodeIDList(), 0);
        for (Tailor tailor : tailorList){
            storageService.deleteStorageByInfo(tailor.getOrderName(), tailor.getBedNumber(), tailor.getPackageNumber());
            cutInStoreService.deleteCutInStoreByInfo(tailor.getOrderName(), tailor.getBedNumber(), tailor.getPackageNumber());
        }
        map.put("outStoreCount", tailorList.size());
        return map;
    }



    @RequestMapping(value = "/changeStore",method = RequestMethod.POST)
    @ResponseBody
    public int changeStore(@RequestParam("changestoreJson")String changestoreJson){
        JsonParser jsonParser = new JsonParser();
        int result = 1;
        try{
            JsonObject json = (JsonObject) jsonParser.parse(changestoreJson);
            String storehouseLocation = json.get("storehouseLocation").getAsString();
            if (storeHouseService.getStoreHouseByLocation(storehouseLocation) == null){
                result = 3;
                return result;
            }
            JsonArray jsonTailorQcode = json.get("tailorQcode").getAsJsonArray();
            Iterator iterator2 = jsonTailorQcode.iterator();
            List<Integer> tailorQcodeIDList = new ArrayList<>();
            while(iterator2.hasNext()){
                JsonPrimitive next = (JsonPrimitive) iterator2.next();
                tailorQcodeIDList.add(Integer.parseInt(next.toString().replace("\"","")));
            }
            List<Tailor> tailorList = tailorService.getTailorsByTailorQcodeIDs(tailorQcodeIDList, 0);
            List<Storage> storageList = new ArrayList<>();
            for (Tailor tailor : tailorList){
                storageService.deleteStorageByInfo(tailor.getOrderName(), tailor.getBedNumber(), tailor.getPackageNumber());
                cutInStoreService.deleteCutInStoreByInfo(tailor.getOrderName(), tailor.getBedNumber(), tailor.getPackageNumber());
                Storage storage = new Storage(storehouseLocation,tailor.getOrderName(),tailor.getBedNumber(),tailor.getColorName(),tailor.getSizeName(),tailor.getLayerCount(),tailor.getPackageNumber(),tailor.getPartName(),tailor.getTailorQcode());
                storageList.add(storage);
            }
            int res3 = storageService.inStore(storageList);
            int res4 = cutInStoreService.cutInStore(storageList);
            if (0 == res3 && 0 == res4){
                result = 0;
            }
        }catch (JsonIOException e){
            e.printStackTrace();
        }catch (JsonSyntaxException e){
            e.printStackTrace();
        }
        return result;
    }


    @RequestMapping(value = "/minichangestore",method = RequestMethod.POST)
    @ResponseBody
    public boolean miniChangeStore(@RequestParam("changestoreJson")String changestoreJson){
        JsonParser jsonParser = new JsonParser();
        boolean result = false;
        try{
            JsonObject json = (JsonObject) jsonParser.parse(changestoreJson);
            String storehouseLocation = json.get("storehouseLocation").getAsString();
            JsonArray jsonTailorQcode = json.get("tailorQcode").getAsJsonArray();
            Iterator iterator2 = jsonTailorQcode.iterator();
            List<Integer> tailorQcodeIDList = new ArrayList<>();
            while(iterator2.hasNext()){
                JsonPrimitive next = (JsonPrimitive) iterator2.next();
                tailorQcodeIDList.add(Integer.parseInt(next.toString().replace("\"","")));
            }
            List<Tailor> tailorList = tailorService.getTailorsByTailorQcodeIDs(tailorQcodeIDList, 0);
            List<Storage> storageList = new ArrayList<>();
            for (Tailor tailor : tailorList){
                storageService.deleteStorageByInfo(tailor.getOrderName(), tailor.getBedNumber(), tailor.getPackageNumber());
                cutInStoreService.deleteCutInStoreByInfo(tailor.getOrderName(), tailor.getBedNumber(), tailor.getPackageNumber());
                Storage storage = new Storage(storehouseLocation,tailor.getOrderName(),tailor.getBedNumber(),tailor.getColorName(),tailor.getSizeName(),tailor.getLayerCount(),tailor.getPackageNumber(),tailor.getPartName(),tailor.getTailorQcode());
                storageList.add(storage);
            }
            int res3 = storageService.inStore(storageList);
            int res4 = cutInStoreService.cutInStore(storageList);
            if (0 == res3 && 0 == res4){
                result = true;
            }
        }catch (JsonIOException e){
            e.printStackTrace();
        }catch (JsonSyntaxException e){
            e.printStackTrace();
        }
        return result;
    }


    /**
     * 进入库存信息页面
     * @param model
     * @return
     */
    @RequestMapping("/storageStateStart")
    public String storageStateStart(Model model){
        model.addAttribute("bigMenuTag",5);
        model.addAttribute("menuTag",54);
        return "opaMsg/storageState";
    }

//    获取仓库存储状态信息
    @RequestMapping(value = "/getstoragestate", method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getStorageState(){
        List<StorageState> storageStateList = new ArrayList<>();
        Map<String,Object> map = new HashMap<>();
        storageStateList = storageService.getStorageState();
        List<StorageFloor> storageFloorList = storageService.getStorageFloor();
        map.put("storageStateList",storageStateList);
        map.put("storageFloorList",storageFloorList);
        return map;
    }

    @RequestMapping(value = "/minigetstoragestate", method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> miniGetStorageState(){
        Map<String,Object> map = new HashMap<>();
        List<StorageState> storageStateList = storageService.getStorageState();
        List<StorageFloor> storageFloorList = storageService.getStorageFloor();
        if(!storageStateList.isEmpty()) {
            for(StorageState storageState : storageStateList) {
                for(StorageFloor storageFloor : storageFloorList) {
                    if(storageState.getStorehouseLocation().startsWith(storageFloor.getLocation())) {
                        storageState.setFloor(storageFloor.getNum());
                    }
                }
            }
        }
        map.put("storageStateList",storageStateList);
        return map;
    }


    @RequestMapping(value = "/instoreStart")
    public String inStoreStart(Model model){
        model.addAttribute("bigMenuTag", 5);
        model.addAttribute("menuTag", 51);
        return "cutMarket/cutInStore";
    }

    @RequestMapping(value = "/outstoreStart")
    public String outStoreStart(Model model){
        model.addAttribute("bigMenuTag", 5);
        model.addAttribute("menuTag", 53);
        return "cutMarket/cutOutStore";
    }

    @RequestMapping(value = "/cutStorageQueryStart")
    public String cutStorageQuerySatrt(Model model){
        model.addAttribute("bigMenuTag", 7);
        model.addAttribute("menuTag", 71);
        return "cutMarket/cutStorageQuery";
    }

    @RequestMapping(value = "/allStorageStart")
    public String allStorageStart(){
        return "cutMarket/allStorage";
    }

    @RequestMapping(value = "/getAllCutStorage", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> getAllCutStorage(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<Storage> storageList = storageService.getAllStorage();
        map.put("data", storageList);
        map.put("code", 0);
        map.put("count", storageList.size());
        return map;
    }

//    @RequestMapping(value = "/getAllCutStorage")
//    public String getAllCutStorage(Model model){
//        List<Storage> storageList = storageService.getAllStorage();
//        model.addAttribute("storageList", storageList);
//        return "cutMarket/fb_allStorageList";
//    }

    @RequestMapping(value = "/storagereport",method = RequestMethod.GET)
    @ResponseBody
    public List<String> storageReport(@RequestParam("storehouseLocation")String storehouseLocation){
        List<String> storageList = storageService.storageReport(storehouseLocation);
        return storageList;
    }



    @RequestMapping(value = "/getMatch", method = RequestMethod.GET)
    @ResponseBody
    public List<String> getMatch(@RequestParam("matchJson")String matchJson,
                                 ModelMap map){
        JsonParser jsonParser = new JsonParser();
        try{
            JsonObject json = (JsonObject) jsonParser.parse(matchJson);
            JsonArray jsonTailorQcode = json.get("tailorQcode").getAsJsonArray();
            Iterator iterator = jsonTailorQcode.iterator();
            List<Integer> tailorQcodeIDList = new ArrayList<>();
            while(iterator.hasNext()){
                JsonPrimitive next = (JsonPrimitive) iterator.next();
                String tailorQcodeID = next.toString().replace("\"", "");
                tailorQcodeIDList.add(Integer.parseInt(tailorQcodeID));
            }
            List<Tailor> tailorList = tailorService.getTailorsByTailorQcodeIDs(tailorQcodeIDList, 0);
            List<String> locationList = new ArrayList<>();
            if (tailorList == null || tailorList.isEmpty()){
                locationList.add("裁片未入库");
                return locationList;
            }
            Tailor firstTailor = tailorList.get(0);
            locationList = storageService.getMatch(firstTailor.getOrderName(), firstTailor.getBedNumber(), firstTailor.getPackageNumber());
            return locationList;
        }catch (JsonIOException e){
            e.printStackTrace();
        }catch (JsonSyntaxException e){
            e.printStackTrace();
        }catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }

    @RequestMapping(value = "/minigetmatch", method = RequestMethod.GET)
    @ResponseBody
    public List<String> miniGetMatch(@RequestParam("matchJson")String matchJson,
                                 ModelMap map){
        JsonParser jsonParser = new JsonParser();
        try{
            JsonObject json = (JsonObject) jsonParser.parse(matchJson);
            JsonArray jsonTailorQcode = json.get("tailorQcode").getAsJsonArray();
            Iterator iterator = jsonTailorQcode.iterator();
            List<Integer> tailorQcodeIDList = new ArrayList<>();
            while(iterator.hasNext()){
                JsonPrimitive next = (JsonPrimitive) iterator.next();
                String tailorQcodeID = next.toString().replace("\"", "");
                tailorQcodeIDList.add(Integer.parseInt(tailorQcodeID));
            }
            List<Tailor> tailorList = tailorService.getTailorsByTailorQcodeIDs(tailorQcodeIDList, 0);
            List<String> locationList = new ArrayList<>();
            if (tailorList == null || tailorList.isEmpty()){
                locationList.add("裁片未入库");
                return locationList;
            }
            Tailor firstTailor = tailorList.get(0);
            locationList = storageService.getMatch(firstTailor.getOrderName(), firstTailor.getBedNumber(), firstTailor.getPackageNumber());
            return locationList;
        }catch (JsonIOException e){
            e.printStackTrace();
        }catch (JsonSyntaxException e){
            e.printStackTrace();
        }catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }


    @RequestMapping(value = "/minicutmatch", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> miniCutMatch(@RequestParam("tailorQcodeID")Integer tailorQcodeID){
        Map<String, Object> map = new LinkedHashMap<>();
        Tailor tailor = tailorService.getTailorByTailorQcodeID(tailorQcodeID, 0);
        if (tailor == null){
            map.put("location", "裁片信息不存在");
            return map;
        }
        List<String> locationList = storageService.getMatch(tailor.getOrderName(), tailor.getBedNumber(), tailor.getPackageNumber());
        if (locationList == null || locationList.isEmpty()){
            map.put("location", "裁片信息不存在");
            return map;
        }
        map.put("location", locationList.get(0));
        return map;

    }

    @RequestMapping(value = "/querycutstoragebyorder",method = RequestMethod.GET)
    public String getFabricLeakQuery(@RequestParam("orderName")String orderName,
                                     @RequestParam("colorName")String colorName,
                                     @RequestParam("sizeName")String sizeName,
                                     Model model){
        if ("".equals(colorName)){
            colorName = null;
        }
        if ("".equals(sizeName)){
            sizeName = null;
        }
        List<CutStorageQuery> cutStorageQueryList = storageService.getCutStorageByOrder(orderName, colorName, sizeName);
        model.addAttribute("cutStorageQueryList",cutStorageQueryList);
        return "cutMarket/fb_cutStorageQueryList";
    }

    @RequestMapping(value = "/miniquerycutstoragebyorder",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> miniQueryCutStorageByOrder(@RequestParam("orderName")String orderName,
                                                      @RequestParam("colorName")String colorName,
                                                      @RequestParam("sizeName")String sizeName){
        Map<String, Object> map = new HashMap<>();
        if ("".equals(colorName)){
            colorName = null;
        }
        if ("".equals(sizeName)){
            sizeName = null;
        }
        List<CutStorageQuery> cutStorageQueryList = storageService.getCutStorageByOrder(orderName, colorName, sizeName);
        map.put("cutStorageQueryList",cutStorageQueryList);
        return map;
    }

    @RequestMapping(value = "/getstoragecolorhint",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getStorageColorHint(@RequestParam("orderName")String orderName){
        Map<String,Object> result = new LinkedHashMap<>();
        List<String> storageColorList = storageService.getStorageColorHint(orderName);
        result.put("storageColorList",storageColorList);
        return result;
    }

    @RequestMapping(value = "/minigetstoragecolorhint",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> miniGetStorageColorHint(@RequestParam("orderName")String orderName){
        Map<String,Object> result = new LinkedHashMap<>();
        List<String> storageColorList = storageService.getStorageColorHint(orderName);
        result.put("storageColorList",storageColorList);
        return result;
    }

    @RequestMapping(value = "/getstoragesizehint",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getStorageColorHint(@RequestParam("orderName")String orderName,
                                                  @RequestParam("colorName")String colorName){
        Map<String,Object> result = new LinkedHashMap<>();
        List<String> storageSizeList = storageService.getStorageSizeHint(orderName, colorName);
        result.put("storageSizeList",storageSizeList);
        return result;
    }

    @RequestMapping(value = "/minigetstoragesizehint",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> miniGetStorageColorHint(@RequestParam("orderName")String orderName,
                                                  @RequestParam("colorName")String colorName){
        Map<String,Object> result = new LinkedHashMap<>();
        List<String> storageSizeList = storageService.getStorageSizeHint(orderName, colorName);
        result.put("storageSizeList",storageSizeList);
        return result;
    }

    @RequestMapping(value = "/deletestorageonfinish",method = RequestMethod.POST)
    @ResponseBody
    public int deleteStorageOnFinish(@RequestParam("orderName")String orderName,
                                     @RequestParam("bedNumber")Integer bedNumber,
                                     @RequestParam("colorName")String colorName,
                                     @RequestParam("sizeName")String sizeName,
                                     @RequestParam("storehouseLocation")String storehouseLocation,
                                     @RequestParam("userName")String userName){
        log.warn("删除裁片库存---"+"订单号："+orderName+"---床号："+bedNumber+"---颜色："+colorName+"---尺码："+sizeName+"---位置："+storehouseLocation+"---操作用户："+userName);
        return storageService.deleteStorageOnFinish(orderName, bedNumber, colorName, sizeName, storehouseLocation);
    }

    @RequestMapping(value = "/minigetstoragebystorehouselocation",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> miniGetStorageByStoreHouseLocation(@RequestParam("storehouseLocation")String storehouseLocation){
        Map<String,Object> result = new LinkedHashMap<>();
        List<Storage> storageList = storageService.getStorageByStoreHouseLocation(storehouseLocation);
        int packageCount = 0;
        int layerSum = 0;
        for (Storage storage : storageList){
            storage.setStorehouseLocation(storehouseLocation);
            packageCount += 1;
            layerSum += storage.getLayerCount();
        }
        result.put("storageList",storageList);
        result.put("packageCount",packageCount);
        result.put("layerSum",layerSum);
        return result;
    }

    @RequestMapping(value = "/minigetstoragebyordercolorsizelocation",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> miniGetStorageByOrderColorSizeLocation(@RequestParam("orderName")String orderName,
                                                                     @RequestParam("colorName")String colorName,
                                                                     @RequestParam("sizeName")String sizeName,
                                                                     @RequestParam("storehouseLocation")String storehouseLocation){
        Map<String,Object> result = new LinkedHashMap<>();
        List<Storage> storageList = storageService.getStorageByOrderColorSizeLocation(orderName, colorName, sizeName, storehouseLocation);
        int packageCount = 0;
        int layerSum = 0;
        for (Storage storage : storageList){
            packageCount += 1;
            layerSum += storage.getLayerCount();
        }
        result.put("storageList",storageList);
        result.put("packageCount",packageCount);
        result.put("layerSum",layerSum);
        return result;
    }


    @RequestMapping(value = "/deletestoragebyid",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> deleteStorageById(@RequestParam("storageID")Integer storageID){
        Map<String, Object> map = new LinkedHashMap<>();
        int res = storageService.deleteStorageById(storageID);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/getCutStorageByInfo", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getCutStorageByInfo(@RequestParam("orderName")String orderName,
                                                   @RequestParam(value = "colorName", required = false)String colorName,
                                                   @RequestParam(value = "sizeName", required = false)String sizeName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<Storage> storageList = storageService.getStorageByInfo(orderName, colorName, sizeName);
        map.put("data", storageList);
        map.put("code", 0);
        map.put("count", storageList.size());
        return map;
    }


}
