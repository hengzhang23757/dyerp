//package com.example.erp01.action;
//
//import com.example.erp01.model.CompletionRate;
//import com.example.erp01.model.GroupPlan;
//import com.example.erp01.service.CompletionRateService;
//import com.example.erp01.service.GroupPlanService;
//import com.example.erp01.service.OrderClothesService;
//import com.example.erp01.service.OrderProcedureService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.ResponseBody;
//
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.*;
//
//@Controller
//@RequestMapping(value = "erp")
//public class CompletionRateController {
//
//    @Autowired
//    private CompletionRateService completionRateService;
//    @Autowired
//    private OrderProcedureService orderProcedureService;
//    @Autowired
//    private GroupPlanService groupPlanService;
//    @Autowired
//    private OrderClothesService orderClothesService;
//
//    @RequestMapping("/completionRateStart")
//    public String completionStart(Model model){
//        model.addAttribute("bigMenuTag",9);
//        model.addAttribute("menuTag",98);
//        return "ieInfo/completionRate";
//    }
//
//
//    @RequestMapping(value = "/getcompletionratebytime",method = RequestMethod.GET)
//    @ResponseBody
//    public Map<String, Object> getCompletionRateByTime(@RequestParam("from") String from,
//                                                       @RequestParam("to") String to,
//                                                       @RequestParam(value = "orderName", required = false)String orderName,
//                                                       @RequestParam(value = "groupName", required = false)String groupName,
//                                                       @RequestParam("workHour")Integer workHour) {
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//        Date fromDate = new Date();
//        Date toDate = new Date();
//        try {
//            fromDate = sdf.parse(from);
//            toDate = sdf.parse(to);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        // 分组、按天获取到员工数量
//        List<CompletionRate> completionRateList = new ArrayList<>();
//        List<CompletionRate> completionRateList1 = completionRateService.getEmpCountEachDay(fromDate, toDate, orderName, groupName);
//        Set<String> groupSet = new HashSet<>();
//        for (CompletionRate completionRate : completionRateList1){
//            groupSet.add(completionRate.getGroupName());
//        }
//        int rangeDate = differentDays(fromDate, toDate);
//        for (String group : groupSet){
//            for (int i = 0; i < rangeDate; i++) {
//                Date tmpDate = getSomeDay(fromDate, i);
//            }
//        }
//    }
//
//
//
//    @RequestMapping(value = "/getcompletionratebytime",method = RequestMethod.GET)
//    @ResponseBody
//    public Map<Integer,Object> getCompletionRateByTime(@RequestParam("from") String from,
//                                                       @RequestParam("to") String to,
//                                                       @RequestParam("orderName")String orderName,
//                                                       @RequestParam("groupName")String groupName) {
//        Map<Integer, Object> map = new LinkedHashMap<>();
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//        Date fromDate = new Date();
//        Date toDate = new Date();
//        try {
//            fromDate = sdf.parse(from);
//            toDate = sdf.parse(to);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        String on;
//        String gn;
//        if ("".equals(orderName)) {
//            on = null;
//        } else {
//            on = orderName;
//        }
//        if ("".equals(groupName)) {
//            gn = null;
//        } else {
//            gn = groupName;
//        }
//        int index = 0;
//        int rangeDate = differentDays(fromDate, toDate);
//        List<String> dateList = new ArrayList<>();
//        for (int i = 0; i < rangeDate; i++) {
//            Date tmpDate = getSomeDay(fromDate, i);
//            dateList.add(sdf.format(tmpDate));
//        }
//        List<CompletionRate> completionRateList = new ArrayList<>();
//        List<CompletionRate> completionRateList1 = completionRateService.getEmpCountEachDay(fromDate, toDate, on, gn);
//        if (completionRateList1 != null && completionRateList1.size() > 0) {
//            //按组、按款进行去重
//            for (CompletionRate completionRate1 : completionRateList1) {
//                if (completionRateList.isEmpty() || completionRateList.size() == 0) {
//                    CompletionRate cr = new CompletionRate(completionRate1.getGroupName(), completionRate1.getOrderName());
//                    completionRateList.add(cr);
//                } else {
//                    boolean flag = true;
//                    for (CompletionRate completionRate2 : completionRateList) {
//                        if (completionRate1.getGroupName().equals(completionRate2.getGroupName()) && completionRate1.getOrderName().equals(completionRate2.getOrderName())) {
//                            flag = false;
//                        }
//                    }
//                    if (flag) {
//                        completionRateList.add(new CompletionRate(completionRate1.getGroupName(), completionRate1.getOrderName()));
//                    }
//                }
//            }
//            for (CompletionRate completionRate3 : completionRateList) {
//                List<CompletionRate> completionRateList2 = new ArrayList<>();
//                String tmpOrderName = completionRate3.getOrderName();
//                String tmpGroupName = completionRate3.getGroupName();
//                CompletionRate cr = completionRateService.getOrderBeginDate(tmpOrderName, tmpGroupName);
//                Date tmpBeginDate = cr.getBeginDate();
//                String versionNumber = orderClothesService.getVersionNumberByOrderName(tmpOrderName);
//                String styleDescription = orderClothesService.getDescriptionByOrder(tmpOrderName);
//                GroupPlan groupPlan = groupPlanService.getGroupPlanByOrderGroup(tmpOrderName, tmpGroupName);
//                Date deadLine = new Date();
//                int workHour = 10;
//                int groupPlanCount = 0;
//                if (groupPlan != null) {
//                    workHour = groupPlan.getWorkHour();
//                    groupPlanCount = groupPlan.getPlanCount();
//                    deadLine = groupPlan.getEndDate();
//                }
//                int diffDate = differentDays(tmpBeginDate, fromDate);
//                Float samValue = orderProcedureService.getSectionSamByOrder(tmpOrderName, "车缝");
//                if (diffDate < 0) {
//                    int firstIndex = 0;
//                    for (int i = 0; i < rangeDate; i++) {
//                        Date tmpRecordDate = getSomeDay(fromDate, i);
//                        if (tmpRecordDate.compareTo(tmpBeginDate) < 0) {
//                            CompletionRate cr1 = new CompletionRate(tmpBeginDate, tmpRecordDate, tmpGroupName, versionNumber, tmpOrderName, styleDescription, 0, deadLine, 0, 0, 0, 0, deadLine, 0f);
//                            completionRateList2.add(cr1);
//                        } else {
//                            int tmpEmpCount = 0;
//                            /**
//                             * 1.获取该天上线人数
//                             * 2.获取该天实际做了多少
//                             * 3.分情况计算该天的计划产能
//                             */
//                            for (CompletionRate completionRate4 : completionRateList1) {
//                                if (tmpOrderName.equals(completionRate4.getOrderName()) && tmpGroupName.equals(completionRate4.getGroupName()) && tmpRecordDate.compareTo(completionRate4.getRecordDate()) == 0) {
//                                    tmpEmpCount = completionRate4.getTodayEmpCount();
//                                }
//                            }
//                            int finishSum = 0;
//                            if (completionRateService.getFinishCountByTime(tmpOrderName, tmpGroupName, tmpRecordDate) != null){
//                                finishSum = completionRateService.getFinishCountByTime(tmpOrderName, tmpGroupName, tmpRecordDate);
//                            }
//                            int tmpTodayPlanCount;
//                            int todayActualCount = 0;
//                            if (completionRateService.getTodayActualCount(tmpOrderName, tmpGroupName, tmpRecordDate) != null) {
//                                todayActualCount = completionRateService.getTodayActualCount(tmpOrderName, tmpGroupName, tmpRecordDate);
//                            }
//                            if (firstIndex == 0) {
//                                if (samValue != 0) {
//                                    if (isWeekend(tmpRecordDate)) {
//                                        tmpTodayPlanCount = (int) (8 * 60 * tmpEmpCount * 0.3 / samValue);
//                                    } else {
//                                        tmpTodayPlanCount = (int) (workHour * 60 * tmpEmpCount * 0.3 / samValue);
//                                    }
//                                } else {
//                                    if (isWeekend(tmpRecordDate)) {
//                                        tmpTodayPlanCount = (int) (8 * 60 * tmpEmpCount * 0.3 / 0.001);
//                                    } else {
//                                        tmpTodayPlanCount = (int) (workHour * 60 * tmpEmpCount * 0.3 / 0.001);
//                                    }
//                                }
//                                firstIndex++;
//                            } else if (firstIndex == 1) {
//                                if (samValue != 0) {
//                                    if (isWeekend(tmpRecordDate)) {
//                                        tmpTodayPlanCount = (int) (8 * 60 * tmpEmpCount * 0.5 / samValue);
//                                    } else {
//                                        tmpTodayPlanCount = (int) (workHour * 60 * tmpEmpCount * 0.5 / samValue);
//                                    }
//                                } else {
//                                    if (isWeekend(tmpRecordDate)) {
//                                        tmpTodayPlanCount = (int) (8 * 60 * tmpEmpCount * 0.5 / 0.001);
//                                    } else {
//                                        tmpTodayPlanCount = (int) (workHour * 60 * tmpEmpCount * 0.5 / 0.001);
//                                    }
//                                }
//                                firstIndex++;
//                            } else if (firstIndex == 2) {
//                                if (samValue != 0) {
//                                    if (isWeekend(tmpRecordDate)) {
//                                        tmpTodayPlanCount = (int) (8 * 60 * tmpEmpCount * 0.7 / samValue);
//                                    } else {
//                                        tmpTodayPlanCount = (int) (workHour * 60 * tmpEmpCount * 0.7 / samValue);
//                                    }
//                                } else {
//                                    if (isWeekend(tmpRecordDate)) {
//                                        tmpTodayPlanCount = (int) (8 * 60 * tmpEmpCount * 0.7 / 0.001);
//                                    } else {
//                                        tmpTodayPlanCount = (int) (workHour * 60 * tmpEmpCount * 0.7 / 0.001);
//                                    }
//                                }
//                                firstIndex++;
//                            } else {
//                                if (samValue != 0) {
//                                    if (isWeekend(tmpRecordDate)) {
//                                        tmpTodayPlanCount = (int) (8 * 60 * tmpEmpCount * 0.85 / samValue);
//                                    } else {
//                                        tmpTodayPlanCount = (int) (workHour * 60 * tmpEmpCount * 0.85 / samValue);
//                                    }
//                                } else {
//                                    if (isWeekend(tmpRecordDate)) {
//                                        tmpTodayPlanCount = (int) (8 * 60 * tmpEmpCount * 0.85 / 0.001);
//                                    } else {
//                                        tmpTodayPlanCount = (int) (workHour * 60 * tmpEmpCount * 0.85 / 0.001);
//                                    }
//                                }
//                                firstIndex++;
//                            }
//                            int remainCount = groupPlanCount - finishSum;
//                            Date planFinishDate;
//                            Float achieveRate = 0f;
//                            if (remainCount <= 0) {
//                                planFinishDate = tmpRecordDate;
//                                if (todayActualCount != 0 && tmpTodayPlanCount != 0){
//                                    achieveRate = (float) (todayActualCount) / (float) (tmpTodayPlanCount);
//                                }
//                            } else {
//                                if (tmpTodayPlanCount == 0 && todayActualCount == 0) {
//                                    planFinishDate = deadLine;
//                                } else if (tmpTodayPlanCount == 0) {
//                                    planFinishDate = getSomeDay(tmpRecordDate, remainCount / todayActualCount);
//                                } else {
//                                    planFinishDate = getSomeDay(tmpRecordDate, remainCount / tmpTodayPlanCount);
//                                    achieveRate = (float) (todayActualCount) / (float) (tmpTodayPlanCount);
//                                }
//
//                            }
//                            CompletionRate cr1 = new CompletionRate(tmpBeginDate, tmpRecordDate, tmpGroupName, versionNumber, tmpOrderName, styleDescription, groupPlanCount, deadLine, finishSum, tmpTodayPlanCount, tmpEmpCount, todayActualCount, planFinishDate, achieveRate);
//                            completionRateList2.add(cr1);
//                        }
//
//                    }
//                } else if (diffDate == 0) {
//                    int firstIndex = 0;
//                    for (int i = 0; i < rangeDate; i++) {
//                        Date tmpRecordDate = getSomeDay(fromDate, i);
//                        int tmpEmpCount = 0;
//                        /**
//                         * 1.获取该天上线人数
//                         * 2.获取该天实际做了多少
//                         * 3.分情况计算该天的计划产能
//                         */
//                        for (CompletionRate completionRate4 : completionRateList1) {
//                            if (tmpOrderName.equals(completionRate4.getOrderName()) && tmpGroupName.equals(completionRate4.getGroupName()) && tmpRecordDate.compareTo(completionRate4.getRecordDate()) == 0) {
//                                tmpEmpCount = completionRate4.getTodayEmpCount();
//                            }
//                        }
//                        int finishSum = 0;
//                        if (completionRateService.getFinishCountByTime(tmpOrderName, tmpGroupName, tmpRecordDate) != null){
//                            finishSum = completionRateService.getFinishCountByTime(tmpOrderName, tmpGroupName, tmpRecordDate);
//                        }
//                        int tmpTodayPlanCount;
//                        int todayActualCount = 0;
//                        if (completionRateService.getTodayActualCount(tmpOrderName, tmpGroupName, tmpRecordDate) != null) {
//                            todayActualCount = completionRateService.getTodayActualCount(tmpOrderName, tmpGroupName, tmpRecordDate);
//                        }
//                        if (firstIndex == 0) {
//                            if (samValue != 0) {
//                                if (isWeekend(tmpRecordDate)) {
//                                    tmpTodayPlanCount = (int) (8 * 60 * tmpEmpCount * 0.3 / samValue);
//                                } else {
//                                    tmpTodayPlanCount = (int) (workHour * 60 * tmpEmpCount * 0.3 / samValue);
//                                }
//                            } else {
//                                if (isWeekend(tmpRecordDate)) {
//                                    tmpTodayPlanCount = (int) (8 * 60 * tmpEmpCount * 0.3 / 0.001);
//                                } else {
//                                    tmpTodayPlanCount = (int) (workHour * 60 * tmpEmpCount * 0.3 / 0.001);
//                                }
//                            }
//                            firstIndex++;
//                        } else if (firstIndex == 1) {
//                            if (samValue != 0) {
//                                if (isWeekend(tmpRecordDate)) {
//                                    tmpTodayPlanCount = (int) (8 * 60 * tmpEmpCount * 0.5 / samValue);
//                                } else {
//                                    tmpTodayPlanCount = (int) (workHour * 60 * tmpEmpCount * 0.5 / samValue);
//                                }
//                            } else {
//                                if (isWeekend(tmpRecordDate)) {
//                                    tmpTodayPlanCount = (int) (8 * 60 * tmpEmpCount * 0.5 / 0.001);
//                                } else {
//                                    tmpTodayPlanCount = (int) (workHour * 60 * tmpEmpCount * 0.5 / 0.001);
//                                }
//                            }
//                            firstIndex++;
//                        } else if (firstIndex == 2) {
//                            if (samValue != 0) {
//                                if (isWeekend(tmpRecordDate)) {
//                                    tmpTodayPlanCount = (int) (8 * 60 * tmpEmpCount * 0.7 / samValue);
//                                } else {
//                                    tmpTodayPlanCount = (int) (workHour * 60 * tmpEmpCount * 0.7 / samValue);
//                                }
//                            } else {
//                                if (isWeekend(tmpRecordDate)) {
//                                    tmpTodayPlanCount = (int) (8 * 60 * tmpEmpCount * 0.7 / 0.001);
//                                } else {
//                                    tmpTodayPlanCount = (int) (workHour * 60 * tmpEmpCount * 0.7 / 0.001);
//                                }
//                            }
//                            firstIndex++;
//                        } else {
//                            if (samValue != 0) {
//                                if (isWeekend(tmpRecordDate)) {
//                                    tmpTodayPlanCount = (int) (8 * 60 * tmpEmpCount * 0.85 / samValue);
//                                } else {
//                                    tmpTodayPlanCount = (int) (workHour * 60 * tmpEmpCount * 0.85 / samValue);
//                                }
//                            } else {
//                                if (isWeekend(tmpRecordDate)) {
//                                    tmpTodayPlanCount = (int) (8 * 60 * tmpEmpCount * 0.85 / 0.001);
//                                } else {
//                                    tmpTodayPlanCount = (int) (workHour * 60 * tmpEmpCount * 0.85 / 0.001);
//                                }
//                            }
//                            firstIndex++;
//                        }
//                        int remainCount = groupPlanCount - finishSum;
//                        Date planFinishDate;
//                        Float achieveRate = 0f;
//                        if (remainCount <= 0) {
//                            planFinishDate = tmpRecordDate;
//                            if (todayActualCount != 0 && tmpTodayPlanCount != 0){
//                                achieveRate = (float) (todayActualCount) / (float) (tmpTodayPlanCount);
//                            }
//                        } else {
//                            if (tmpTodayPlanCount == 0 && todayActualCount == 0) {
//                                planFinishDate = deadLine;
//                            } else if (tmpTodayPlanCount == 0) {
//                                planFinishDate = getSomeDay(tmpRecordDate, remainCount / todayActualCount);
//                            } else {
//                                planFinishDate = getSomeDay(tmpRecordDate, remainCount / tmpTodayPlanCount);
//                                achieveRate = (float) (todayActualCount) / (float) (tmpTodayPlanCount);
//                            }
//
//                        }
//                        CompletionRate cr1 = new CompletionRate(tmpBeginDate, tmpRecordDate, tmpGroupName, versionNumber, tmpOrderName, styleDescription, groupPlanCount, deadLine, finishSum, tmpTodayPlanCount, tmpEmpCount, todayActualCount, planFinishDate, achieveRate);
//                        completionRateList2.add(cr1);
//
//                    }
//                } else if (diffDate == 1) {
//                    int firstIndex = 1;
//                    for (int i = 0; i < rangeDate; i++) {
//                        Date tmpRecordDate = getSomeDay(fromDate, i);
//                        int tmpEmpCount = 0;
//                        /**
//                         * 1.获取该天上线人数
//                         * 2.获取该天实际做了多少
//                         * 3.分情况计算该天的计划产能
//                         */
//                        for (CompletionRate completionRate4 : completionRateList1) {
//                            if (tmpOrderName.equals(completionRate4.getOrderName()) && tmpGroupName.equals(completionRate4.getGroupName()) && tmpRecordDate.compareTo(completionRate4.getRecordDate()) == 0) {
//                                tmpEmpCount = completionRate4.getTodayEmpCount();
//                            }
//                        }
//                        int finishSum = completionRateService.getFinishCountByTime(tmpOrderName, tmpGroupName, tmpRecordDate);
//                        int tmpTodayPlanCount;
//                        int todayActualCount = 0;
//                        if (completionRateService.getTodayActualCount(tmpOrderName, tmpGroupName, tmpRecordDate) != null) {
//                            todayActualCount = completionRateService.getTodayActualCount(tmpOrderName, tmpGroupName, tmpRecordDate);
//                        }
//                        if (firstIndex == 1) {
//                            if (samValue != 0) {
//                                if (isWeekend(tmpRecordDate)) {
//                                    tmpTodayPlanCount = (int) (8 * 60 * tmpEmpCount * 0.5 / samValue);
//                                } else {
//                                    tmpTodayPlanCount = (int) (workHour * 60 * tmpEmpCount * 0.5 / samValue);
//                                }
//                            } else {
//                                if (isWeekend(tmpRecordDate)) {
//                                    tmpTodayPlanCount = (int) (8 * 60 * tmpEmpCount * 0.5 / 0.001);
//                                } else {
//                                    tmpTodayPlanCount = (int) (workHour * 60 * tmpEmpCount * 0.5 / 0.001);
//                                }
//                            }
//                            firstIndex++;
//                        } else if (firstIndex == 2) {
//                            if (samValue != 0) {
//                                if (isWeekend(tmpRecordDate)) {
//                                    tmpTodayPlanCount = (int) (8 * 60 * tmpEmpCount * 0.7 / samValue);
//                                } else {
//                                    tmpTodayPlanCount = (int) (workHour * 60 * tmpEmpCount * 0.7 / samValue);
//                                }
//                            } else {
//                                if (isWeekend(tmpRecordDate)) {
//                                    tmpTodayPlanCount = (int) (8 * 60 * tmpEmpCount * 0.7 / 0.001);
//                                } else {
//                                    tmpTodayPlanCount = (int) (workHour * 60 * tmpEmpCount * 0.7 / 0.001);
//                                }
//                            }
//                            firstIndex++;
//                        } else {
//                            if (samValue != 0) {
//                                if (isWeekend(tmpRecordDate)) {
//                                    tmpTodayPlanCount = (int) (8 * 60 * tmpEmpCount * 0.85 / samValue);
//                                } else {
//                                    tmpTodayPlanCount = (int) (workHour * 60 * tmpEmpCount * 0.85 / samValue);
//                                }
//                            } else {
//                                if (isWeekend(tmpRecordDate)) {
//                                    tmpTodayPlanCount = (int) (8 * 60 * tmpEmpCount * 0.85 / 0.001);
//                                } else {
//                                    tmpTodayPlanCount = (int) (workHour * 60 * tmpEmpCount * 0.85 / 0.001);
//                                }
//                            }
//                            firstIndex++;
//                        }
//                        int remainCount = groupPlanCount - finishSum;
//                        Date planFinishDate;
//                        Float achieveRate = 0f;
//                        if (remainCount <= 0) {
//                            planFinishDate = tmpRecordDate;
//                            if (todayActualCount != 0 && tmpTodayPlanCount != 0){
//                                achieveRate = (float) (todayActualCount) / (float) (tmpTodayPlanCount);
//                            }
//                        } else {
//                            if (tmpTodayPlanCount == 0 && todayActualCount == 0) {
//                                planFinishDate = deadLine;
//                            } else if (tmpTodayPlanCount == 0) {
//                                planFinishDate = getSomeDay(tmpRecordDate, remainCount / todayActualCount);
//                            } else {
//                                planFinishDate = getSomeDay(tmpRecordDate, remainCount / tmpTodayPlanCount);
//                                achieveRate = (float) (todayActualCount) / (float) (tmpTodayPlanCount);
//                            }
//
//                        }
//                        CompletionRate cr1 = new CompletionRate(tmpBeginDate, tmpRecordDate, tmpGroupName, versionNumber, tmpOrderName, styleDescription, groupPlanCount, deadLine, finishSum, tmpTodayPlanCount, tmpEmpCount, todayActualCount, planFinishDate, achieveRate);
//                        completionRateList2.add(cr1);
//
//                    }
//                } else if (diffDate == 2) {
//                    int firstIndex = 2;
//                    for (int i = 0; i < rangeDate; i++) {
//                        Date tmpRecordDate = getSomeDay(fromDate, i);
//                        int tmpEmpCount = 0;
//                        /**
//                         * 1.获取该天上线人数
//                         * 2.获取该天实际做了多少
//                         * 3.分情况计算该天的计划产能
//                         */
//                        for (CompletionRate completionRate4 : completionRateList1) {
//                            if (tmpOrderName.equals(completionRate4.getOrderName()) && tmpGroupName.equals(completionRate4.getGroupName()) && tmpRecordDate.compareTo(completionRate4.getRecordDate()) == 0) {
//                                tmpEmpCount = completionRate4.getTodayEmpCount();
//                            }
//                        }
//                        int finishSum = completionRateService.getFinishCountByTime(tmpOrderName, tmpGroupName, tmpRecordDate);
//                        int tmpTodayPlanCount;
//                        int todayActualCount = 0;
//                        if (completionRateService.getTodayActualCount(tmpOrderName, tmpGroupName, tmpRecordDate) != null) {
//                            todayActualCount = completionRateService.getTodayActualCount(tmpOrderName, tmpGroupName, tmpRecordDate);
//                        }
//                        if (firstIndex == 2) {
//                            if (samValue != 0) {
//                                if (isWeekend(tmpRecordDate)) {
//                                    tmpTodayPlanCount = (int) (8 * 60 * tmpEmpCount * 0.7 / samValue);
//                                } else {
//                                    tmpTodayPlanCount = (int) (workHour * 60 * tmpEmpCount * 0.7 / samValue);
//                                }
//                            } else {
//                                if (isWeekend(tmpRecordDate)) {
//                                    tmpTodayPlanCount = (int) (8 * 60 * tmpEmpCount * 0.7 / 0.001);
//                                } else {
//                                    tmpTodayPlanCount = (int) (workHour * 60 * tmpEmpCount * 0.7 / 0.001);
//                                }
//                            }
//                            firstIndex++;
//                        } else {
//                            if (samValue != 0) {
//                                if (isWeekend(tmpRecordDate)) {
//                                    tmpTodayPlanCount = (int) (8 * 60 * tmpEmpCount * 0.85 / samValue);
//                                } else {
//                                    tmpTodayPlanCount = (int) (workHour * 60 * tmpEmpCount * 0.85 / samValue);
//                                }
//                            } else {
//                                if (isWeekend(tmpRecordDate)) {
//                                    tmpTodayPlanCount = (int) (8 * 60 * tmpEmpCount * 0.85 / 0.001);
//                                } else {
//                                    tmpTodayPlanCount = (int) (workHour * 60 * tmpEmpCount * 0.85 / 0.001);
//                                }
//                            }
//                            firstIndex++;
//                        }
//                        int remainCount = groupPlanCount - finishSum;
//                        Date planFinishDate;
//                        Float achieveRate = 0f;
//                        if (remainCount <= 0) {
//                            planFinishDate = tmpRecordDate;
//                            if (todayActualCount != 0 && tmpTodayPlanCount != 0){
//                                achieveRate = (float) (todayActualCount) / (float) (tmpTodayPlanCount);
//                            }
//                        } else {
//                            if (tmpTodayPlanCount == 0 && todayActualCount == 0) {
//                                planFinishDate = deadLine;
//                            } else if (tmpTodayPlanCount == 0) {
//                                planFinishDate = getSomeDay(tmpRecordDate, remainCount / todayActualCount);
//                            } else {
//                                planFinishDate = getSomeDay(tmpRecordDate, remainCount / tmpTodayPlanCount);
//                                achieveRate = (float) (todayActualCount) / (float) (tmpTodayPlanCount);
//                            }
//
//                        }
//                        CompletionRate cr1 = new CompletionRate(tmpBeginDate, tmpRecordDate, tmpGroupName, versionNumber, tmpOrderName, styleDescription, groupPlanCount, deadLine, finishSum, tmpTodayPlanCount, tmpEmpCount, todayActualCount, planFinishDate, achieveRate);
//                        completionRateList2.add(cr1);
//
//                    }
//                } else {
//                    for (int i = 0; i < rangeDate; i++) {
//                        Date tmpRecordDate = getSomeDay(fromDate, i);
//                        int tmpEmpCount = 0;
//                        /**
//                         * 1.获取该天上线人数
//                         * 2.获取该天实际做了多少
//                         * 3.分情况计算该天的计划产能
//                         */
//                        for (CompletionRate completionRate4 : completionRateList1) {
//                            if (tmpOrderName.equals(completionRate4.getOrderName()) && tmpGroupName.equals(completionRate4.getGroupName()) && tmpRecordDate.compareTo(completionRate4.getRecordDate()) == 0) {
//                                tmpEmpCount = completionRate4.getTodayEmpCount();
//                            }
//                        }
//                        int finishSum = completionRateService.getFinishCountByTime(tmpOrderName, tmpGroupName, tmpRecordDate);
//                        int tmpTodayPlanCount;
//                        int todayActualCount = 0;
//                        if (completionRateService.getTodayActualCount(tmpOrderName, tmpGroupName, tmpRecordDate) != null) {
//                            todayActualCount = completionRateService.getTodayActualCount(tmpOrderName, tmpGroupName, tmpRecordDate);
//                        }
//                        if (samValue != 0) {
//                            if (isWeekend(tmpRecordDate)) {
//                                tmpTodayPlanCount = (int) (8 * 60 * tmpEmpCount * 0.85 / samValue);
//                            } else {
//                                tmpTodayPlanCount = (int) (workHour * 60 * tmpEmpCount * 0.85 / samValue);
//                            }
//                        } else {
//                            if (isWeekend(tmpRecordDate)) {
//                                tmpTodayPlanCount = (int) (8 * 60 * tmpEmpCount * 0.85 / 0.001);
//                            } else {
//                                tmpTodayPlanCount = (int) (workHour * 60 * tmpEmpCount * 0.85 / 0.001);
//                            }
//                        }
//                        int remainCount = groupPlanCount - finishSum;
//                        Date planFinishDate;
//                        Float achieveRate = 0f;
//                        if (remainCount <= 0) {
//                            planFinishDate = tmpRecordDate;
//                            if (todayActualCount != 0 && tmpTodayPlanCount != 0){
//                                achieveRate = (float) (todayActualCount) / (float) (tmpTodayPlanCount);
//                            }
//                        } else {
//                            if (tmpTodayPlanCount == 0 && todayActualCount == 0) {
//                                planFinishDate = deadLine;
//                            } else if (tmpTodayPlanCount == 0) {
//                                planFinishDate = getSomeDay(tmpRecordDate, remainCount / todayActualCount);
//                            } else {
//                                planFinishDate = getSomeDay(tmpRecordDate, remainCount / tmpTodayPlanCount);
//                                achieveRate = (float) (todayActualCount) / (float) (tmpTodayPlanCount);
//                            }
//
//                        }
//                        CompletionRate cr1 = new CompletionRate(tmpBeginDate, tmpRecordDate, tmpGroupName, versionNumber, tmpOrderName, styleDescription, groupPlanCount, deadLine, finishSum, tmpTodayPlanCount, tmpEmpCount, todayActualCount, planFinishDate, achieveRate);
//                        completionRateList2.add(cr1);
//                    }
//                }
//                map.put(index,completionRateList2);
//                index ++;
//            }
//        }
//        return map;
//    }
//
////    @RequestMapping(value = "/getcompletionratebytime",method = RequestMethod.GET)
////    @ResponseBody
////    public Map<String,Object> getCompletionRateByTime(@RequestParam("from") String from,
////                                                      @RequestParam("to") String to,
////                                                      @RequestParam("orderName")String orderName,
////                                                      @RequestParam("groupName")String groupName){
////        Map<String,Object> map = new LinkedHashMap<>();
////        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
////        Date fromDate = new Date();
////        Date toDate = new Date();
////        try{
////            fromDate = sdf.parse(from);
////            toDate = sdf.parse(to);
////        }catch (ParseException e){
////            e.printStackTrace();
////        }
////
////        String on;
////        String gn;
////        if ("".equals(orderName)){
////            on = null;
////        }else {
////            on = orderName;
////        }
////        if ("".equals(groupName)){
////            gn = null;
////        }else {
////            gn = groupName;
////        }
////        List<CompletionRate> completionRateList = new ArrayList<>();
////        List<CompletionRate> completionRateList1 = completionRateService.getEmpCountEachDay(fromDate,toDate,on,gn);
////        if (completionRateList1 != null && completionRateList1.size() > 0){
////            for (CompletionRate completionRate : completionRateList1){
////                String tmpOrderName = completionRate.getOrderName();
////                String tmpGroupName = completionRate.getGroupName();
////                CompletionRate cr = completionRateService.getOrderBeginDate(tmpOrderName, tmpGroupName);
////                Date tmpBeginDate = cr.getBeginDate();
////                Date tmpRecordDate = completionRate.getRecordDate();
////                int tmpEmpCount = completionRate.getTodayEmpCount();
////                int diffDate = differentDays(tmpBeginDate, fromDate);
////                String versionNumber = orderClothesService.getVersionNumberByOrderName(tmpOrderName);
////                String styleDescription = orderClothesService.getDescriptionByOrder(tmpOrderName);
////                GroupPlan groupPlan = groupPlanService.getGroupPlanByOrderGroup(tmpOrderName, tmpGroupName);
////                int finishSum = 0;
////                if (completionRateService.getFinishCount(tmpOrderName, tmpGroupName) != null){
////                    finishSum = completionRateService.getFinishCount(tmpOrderName, tmpGroupName);
////                }
////                Date deadLine = tmpRecordDate;
////                int workHour = 10;
////                int groupPlanCount = finishSum;
////                if (groupPlan != null){
////                    workHour = groupPlan.getWorkHour();
////                    groupPlanCount = groupPlan.getPlanCount();
////                    deadLine = groupPlan.getEndDate();
////                }
////                int tmpTodayPlanCount = 0;
////                int todayActualCount = 0;
////                if (completionRateService.getTodayActualCount(tmpOrderName, tmpGroupName, tmpRecordDate) != null){
////                    todayActualCount = completionRateService.getTodayActualCount(tmpOrderName, tmpGroupName, tmpRecordDate);
////                }
////                Float samValue = orderProcedureService.getSectionSamByOrder(tmpOrderName,"车缝");
////                if (samValue != 0){
////                    if (diffDate >= 3){
////                        if (isWeekend(tmpRecordDate)){
////                            tmpTodayPlanCount = (int)(8*60*tmpEmpCount*0.85/samValue);
////                        }else {
////                            tmpTodayPlanCount = (int)(workHour*60*tmpEmpCount*0.85/samValue);
////                        }
////
////                    }else if (diffDate == 1){
////                        if (isWeekend(tmpRecordDate)){
////                            tmpTodayPlanCount = (int)(8*60*tmpEmpCount*0.5/samValue);
////                        }else {
////                            tmpTodayPlanCount = (int)(workHour*60*tmpEmpCount*0.5/samValue);
////                        }
////                    }else if (diffDate == 2){
////                        if (isWeekend(tmpRecordDate)){
////                            tmpTodayPlanCount = (int)(8*60*tmpEmpCount*0.7/samValue);
////                        }else {
////                            tmpTodayPlanCount = (int)(workHour*60*tmpEmpCount*0.7/samValue);
////                        }
////                    }else {
////                        if (isWeekend(tmpRecordDate)){
////                            tmpTodayPlanCount = (int)(8*60*tmpEmpCount*0.3/samValue);
////                        }else {
////                            tmpTodayPlanCount = (int)(workHour*60*tmpEmpCount*0.3/samValue);
////                        }
////                    }
////                }
////                int remainCount = groupPlanCount - finishSum;
////                Date planFinishDate;
////                if (remainCount <= 0){
////                    planFinishDate = tmpRecordDate;
////                }else {
////                    if (tmpTodayPlanCount == 0 && todayActualCount == 0){
////                        planFinishDate = deadLine;
////                    }else if (tmpTodayPlanCount == 0){
////                        planFinishDate = getSomeDay(tmpRecordDate, remainCount/todayActualCount);
////                    }else {
////                        planFinishDate = getSomeDay(tmpRecordDate, remainCount/tmpTodayPlanCount + 1);
////                    }
////
////                }
////                Float achieveRate = (float) (todayActualCount)/(float) (tmpTodayPlanCount);
////                CompletionRate completionRate1 = new CompletionRate(tmpBeginDate, tmpRecordDate, tmpGroupName, versionNumber, tmpOrderName, styleDescription, groupPlanCount, deadLine, finishSum, tmpTodayPlanCount, tmpEmpCount, todayActualCount, planFinishDate, achieveRate);
////                completionRateList.add(completionRate1);
////            }
////        }
////        map.put("completionRateList",completionRateList);
////        return map;
////    }
//
//
//
//    public static int differentDays(Date date1,Date date2) {
//        Calendar cal1 = Calendar.getInstance();
//        cal1.setTime(date1);
//        Calendar cal2 = Calendar.getInstance();
//        cal2.setTime(date2);
//        int day1= cal1.get(Calendar.DAY_OF_YEAR);
//        int day2 = cal2.get(Calendar.DAY_OF_YEAR);
//        int year1 = cal1.get(Calendar.YEAR);
//        int year2 = cal2.get(Calendar.YEAR);
//        if(year1 != year2) {
//            int timeDistance = 0 ;
//            for(int i = year1 ; i < year2 ; i ++) {
//                if(i%4==0 && i%100!=0 || i%400==0) {
//                    timeDistance += 366;
//                } else {
//                    timeDistance += 365;
//                }
//            }
//
//            return timeDistance + (day2-day1) ;
//        }
//        else {
//            return day2-day1;
//        }
//    }
//
//    public static boolean isWeekend(Date bDate) {
//        Calendar cal = Calendar.getInstance();
//        cal.setTime(bDate);
//        if(cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY){
//            return true;
//        } else{
//            return false;
//        }
//
//    }
//
//    public static Date getSomeDay(Date date, int day){
//        Calendar calendar = Calendar.getInstance();
//        calendar.setTime(date);
//        calendar.add(Calendar.DATE, day);
//        return calendar.getTime();
//    }
//
//
//}
