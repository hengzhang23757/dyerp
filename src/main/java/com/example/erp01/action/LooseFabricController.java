package com.example.erp01.action;

import com.example.erp01.model.*;
import com.example.erp01.service.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.omg.CORBA.PRIVATE_MEMBER;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.SimpleDateFormat;
import java.util.*;

@Controller
@RequestMapping(value = "erp")
public class LooseFabricController {

    @Autowired
    private LooseFabricService looseFabricService;
    @Autowired
    private MaxFabricQcodeIDService maxFabricQcodeIDService;
    @Autowired
    private FabricDetailService fabricDetailService;
    @Autowired
    private FabricStorageService fabricStorageService;
    @Autowired
    private FabricOutRecordService fabricOutRecordService;
    @Autowired
    private ManufactureFabricService manufactureFabricService;
    @Autowired
    private DepartmentPlanService departmentPlanService;
    @Autowired
    private UnitConsumptionService unitConsumptionService;

    @RequestMapping("/looseFabricStart")
    public String looseFabricStart(){
        return "looseFabric/looseFabricTailor";
    }


    @RequestMapping("/looseFabricPrintStart")
    public String looseFabricPrintStart(){
        return "looseFabric/looseFabricPrint";
    }

    @RequestMapping("/fabricLoChangeStart")
    public String fabricLoChangeStart(){
        return "looseFabric/fabricLoChange";
    }

    @RequestMapping("/addLooseFabricStart")
    public String addLooseFabricStart(){
        return "looseFabric/fb_addLooseFabricTailor";
    }

    @RequestMapping(value = "/getloosefabriccolornamehint", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getColorNamesByOrder(@RequestParam("orderName")String orderName){
        List<String> colorNameList = fabricDetailService.getColorNamesByOrder(orderName);
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("colorNameList", colorNameList);
        return map;
    }

    @RequestMapping(value = "/getloosefabricfabricnamehint", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getFabricNameHint(@RequestParam("colorName")String colorName,
                                                 @RequestParam("orderName")String orderName){
        List<String> fabricNameList = fabricDetailService.getFabricNamesByOrderColor(orderName, colorName);
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("fabricNameList", fabricNameList);
        return map;
    }

    @RequestMapping(value = "/getloosefabriccolorhint", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getFabricColorByStyleFabricName(@RequestParam("orderName")String orderName,
                                                               @RequestParam("colorName")String colorName,
                                                               @RequestParam("fabricName")String fabricName){
        List<String> fabricColorList = fabricDetailService.getFabricColorsByOrderColorFabric(orderName, colorName, fabricName);
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("fabricColorList", fabricColorList);
        return map;
    }

    @RequestMapping(value = "/getloosefabricjarnameofunprint", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getJarNumberByStyleFabric(@RequestParam("orderName")String orderName,
                                                         @RequestParam("colorName")String colorName,
                                                         @RequestParam("fabricName")String fabricName,
                                                         @RequestParam("fabricColor")String fabricColor){
        List<LooseFabric> jarInfoList = fabricDetailService.getJarInfoByOrderColorFabricColor(orderName, colorName, fabricName, fabricColor);
        List<LooseFabric> looseFabricList = looseFabricService.getMaxLooseFabricInfoByInfo(orderName, colorName, fabricName, fabricColor);
        List<String> unFinishList = new ArrayList<>();
        for (LooseFabric looseFabric1 : jarInfoList){
            boolean isPrint = false;
            for (LooseFabric looseFabric2 : looseFabricList){
                if (looseFabric1.getJarNumber().equals(looseFabric2.getJarNumber().split("#")[0])){
                    if (looseFabric2.getBatchOrder().equals(looseFabric1.getTotalBatch())){
                        isPrint = true;
                    }
                }
            }
            if (!isPrint){
                unFinishList.add(looseFabric1.getJarNumber());
            }
        }
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("jarNumberList", unFinishList);
        return map;
    }

    @RequestMapping(value = "/getloosefabricbatchbystylecolorjar", method = RequestMethod.GET)
    @ResponseBody
    public int getFabricBatchByStyleColorJar(@RequestParam("orderName")String orderName,
                                             @RequestParam("colorName")String colorName,
                                             @RequestParam("fabricName")String fabricName,
                                             @RequestParam("fabricColor")String fabricColor,
                                             @RequestParam("jarNumber")String jarNumber){
        return fabricDetailService.getFabricBatchNumberByOrderFabricJar(orderName, colorName, fabricName, fabricColor, jarNumber);

    }

    @RequestMapping(value = "/getloosefabricunitbyinfo", method = RequestMethod.GET)
    @ResponseBody
    public String getFabricUnitByInfo(@RequestParam("orderName")String orderName,
                                   @RequestParam("colorName")String colorName,
                                   @RequestParam("fabricName")String fabricName,
                                   @RequestParam("fabricColor")String fabricColor){
        return fabricDetailService.getFabricUnitByInfo(orderName, colorName, fabricName, fabricColor);

    }

    @RequestMapping(value = "/getloosefabricinfobyjarlocation", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getFinishLooseFabricCount(@RequestParam("orderName")String orderName,
                                                         @RequestParam("colorName")String colorName,
                                                         @RequestParam("fabricName")String fabricName,
                                                         @RequestParam("fabricColor")String fabricColor,
                                                         @RequestParam("jarNumber")String jarNumber,
                                                         @RequestParam("location")String location){
        Map<String, Object> map = new LinkedHashMap<>();
        int finishCount = looseFabricService.getFinishLooseFabricCount(orderName, colorName, fabricName, fabricColor, jarNumber+"#");
        int totalCount =  fabricDetailService.getFabricBatchNumberByOrderFabricJar(orderName, colorName, fabricName, fabricColor, jarNumber);
        int locationCount = fabricStorageService.getFabricBatchNumberByOrderFabricJarLocation(orderName, fabricName, fabricColor, jarNumber, location);
        int remainCount = totalCount - finishCount;
        map.put("finish",finishCount);
        map.put("remain",remainCount);
        map.put("locationCount",locationCount);
        return map;

    }

    @RequestMapping(value = "/generateloosefabricinfo", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> generateLooseFabricInfo(@RequestParam("looseFabricJson")String looseFabricJson){
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        List<LooseFabric> looseFabricList = gson.fromJson(looseFabricJson,new TypeToken<List<LooseFabric>>(){}.getType());
        synchronized (this) {
            int tmpMaxFabricQcodeID = maxFabricQcodeIDService.getMaxFabricQcodeId();
            for (LooseFabric looseFabric : looseFabricList){
                tmpMaxFabricQcodeID += 1;
                looseFabric.setqCodeID(tmpMaxFabricQcodeID);
                looseFabric.setLooseTime(new Date());
            }
            maxFabricQcodeIDService.updateMaxFabricQcodeId(tmpMaxFabricQcodeID);
        }
        map.put("looseFabricList", looseFabricList);
        return map;

    }

    @RequestMapping(value = "/saveloosefabricdata", method = RequestMethod.POST)
    @ResponseBody
    public int saveLooseFabricData(@RequestParam("looseFabricJson")String looseFabricJson){
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
        List<LooseFabric> looseFabricList = gson.fromJson(looseFabricJson,new TypeToken<List<LooseFabric>>(){}.getType());
        int batchNumber = looseFabricList.size();
        Float weight = 0f;
        LooseFabric lf = looseFabricList.get(0);
        String orderName = lf.getOrderName();
        String colorName = lf.getColorName();
        String fabricName = lf.getFabricName();
        String fabricColor = lf.getFabricColor();
        String jarName = lf.getJarNumber().split("#")[0];
        String location = lf.getFromLocation();
        String unit = lf.getUnit();
        ManufactureFabric manufactureFabric = manufactureFabricService.getOneManufactureFabricByInfo(orderName, colorName, fabricName);
        String supplier = "";
        String fabricNumber = "";
        String fabricColorNumber = "";
        String isHint = "否";
        List<DepartmentPlan> departmentPlanList = departmentPlanService.getDepartmentPlanByFinishState("未完成", orderName, colorName);
        if (departmentPlanList != null && !departmentPlanList.isEmpty()){
            for (DepartmentPlan departmentPlan : departmentPlanList){
                departmentPlan.setActCount(departmentPlan.getActCount() + 1);
                departmentPlan.setCompletionRate((float)departmentPlan.getActCount()/departmentPlan.getPlanCount());
            }
            departmentPlanService.updateDepartmentPlanBatch(departmentPlanList);
        }
        if (manufactureFabric != null){
            if (manufactureFabric.getSupplier() != null){
                supplier = manufactureFabric.getSupplier();
            }
            if (manufactureFabric.getFabricNumber() != null){
                fabricNumber = manufactureFabric.getFabricNumber();
            }
            if (manufactureFabric.getFabricColorNumber() != null){
                fabricColorNumber = manufactureFabric.getFabricColorNumber();
            }
            if (manufactureFabric.getIsHit() != null){
                isHint = manufactureFabric.getIsHit();
            }
        }
        FabricStorage fabricStorage = fabricStorageService.getFabricStorageByOrderLocationJar(orderName, colorName, fabricName, fabricColor, jarName, location, null);
        List<FabricOutRecord> fabricOutRecordList = new ArrayList<>();
        for (LooseFabric looseFabric : looseFabricList){
            weight += looseFabric.getWeight();
            FabricOutRecord fabricOutRecord = new FabricOutRecord(looseFabric.getFromLocation(), looseFabric.getOrderName(), looseFabric.getClothesVersionNumber(), looseFabric.getColorName(), looseFabric.getFabricName(), looseFabric.getFabricColor(), looseFabric.getJarNumber().split("#")[0], looseFabric.getOperateType(), looseFabric.getWeight(), 1, "无", looseFabric.getLooseTime(), isHint, supplier, fabricNumber, fabricColorNumber, unit);
            fabricOutRecord.setFabricID(fabricStorage.getFabricID());
            fabricOutRecord.setReceiver("裁床");
            fabricOutRecordList.add(fabricOutRecord);
        }
        int res1;
        if (fabricStorage.getBatchNumber().equals(batchNumber)){
            res1 = fabricStorageService.deleteFabricStorage(fabricStorage.getFabricStorageID());
        }else {
            fabricStorage.setBatchNumber(fabricStorage.getBatchNumber() - batchNumber);
            fabricStorage.setWeight(fabricStorage.getWeight() - weight);
            res1 = fabricStorageService.updateFabricStorage(fabricStorage);
        }
        int res2 = fabricOutRecordService.addFabricOutRecordBatch(fabricOutRecordList);
        int res3 = looseFabricService.saveLooseFabricData(looseFabricList);
        if (res1 == 0 && res2 == 0 && res3 == 0){
            return 0;
        }else {
            return 1;
        }

    }

    @RequestMapping(value = "/minigetloosefabricbyid", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> miniGetLooseFabricByID(@RequestParam("qCodeID")Integer qCodeID){
        Map<String, Object> map = new LinkedHashMap<>();
        LooseFabric looseFabric = looseFabricService.getLooseFabricByID(qCodeID);
        if (looseFabric.getCutState() == null || looseFabric.getCutState().equals(0)){
            List<DepartmentPlan> departmentPlanList = departmentPlanService.getDepartmentPlanByFinishState("未完成", looseFabric.getOrderName(), looseFabric.getColorName());
            if (departmentPlanList != null && !departmentPlanList.isEmpty()){
                for (DepartmentPlan departmentPlan : departmentPlanList){
                    departmentPlan.setCutCount(departmentPlan.getCutCount() + 1);
                    departmentPlan.setCompletionRateCut((float)departmentPlan.getCutCount()/departmentPlan.getPlanCount());
                    if (departmentPlan.getCutCount() >= departmentPlan.getPlanCount()){
                        departmentPlan.setFinishState("完成");
                    }
                }
                departmentPlanService.updateDepartmentPlanBatch(departmentPlanList);
            }
        }
        looseFabricService.changeStateInTailorScan(qCodeID);
        if (looseFabric != null){
            Float pieceUsage = 0f;
            String tmpJar = looseFabric.getJarNumber();
            int charLength  = tmpJar.length();
            if (tmpJar.endsWith("#")){
                tmpJar = tmpJar.substring(0, tmpJar.length() -1);
            } else if (charLength >= 2 && tmpJar.charAt(charLength - 2) == '#'){
                tmpJar = tmpJar.substring(0, tmpJar.length() - 2);
            }
            if (unitConsumptionService.getPlanConsumptionByJar(looseFabric.getOrderName(), looseFabric.getColorName(), tmpJar) != null){
                pieceUsage = unitConsumptionService.getPlanConsumptionByJar(looseFabric.getOrderName(), looseFabric.getColorName(), tmpJar);
            }
            map.put("pieceUsage", pieceUsage);
        }

        map.put("looseFabric", looseFabric);
        return map;
    }

    @RequestMapping(value = "/searchloosefabricinfo", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> miniGetLooseFabricByID(@RequestParam("orderName")String orderName,
                                                      @RequestParam("colorName")String colorName,
                                                      @RequestParam("fabricName")String fabricName,
                                                      @RequestParam("fabricColor")String fabricColor){
        Map<String, Object> map = new LinkedHashMap<>();
        String fn = null;
        String fc = null;
        String cn = null;
        if (!"".equals(fabricName)){
            fn = fabricName;
        }
        if (!"".equals(fabricColor)){
            fc = fabricColor;
        }
        if (!"".equals(colorName)){
            cn = colorName;
        }
        List<LooseFabric> looseFabricList = looseFabricService.getLooseFabricByOrderFabric(orderName, cn, fn, fc);
        map.put("looseFabricList", looseFabricList);
        return map;
    }

    @RequestMapping(value = "/updateloosefabricinprint", method = RequestMethod.POST)
    @ResponseBody
    public int updateLooseFabricInPrint(@RequestParam("looseFabricJson")String looseFabricJson){
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        List<LooseFabric> looseFabricList = gson.fromJson(looseFabricJson,new TypeToken<List<LooseFabric>>(){}.getType());
        return looseFabricService.updateLooseFabric(looseFabricList);

    }


    @RequestMapping(value = "/searchjarbyinfo", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> searchJarByInfo(@RequestParam("orderName")String orderName,
                                               @RequestParam("colorName")String colorName,
                                               @RequestParam("fabricName")String fabricName,
                                               @RequestParam("fabricColor")String fabricColor){
        Map<String, Object> map = new LinkedHashMap<>();
        String fn = null;
        String fc = null;
        String cn = null;
        if (!"".equals(fabricName)){
            fn = fabricName;
        }
        if (!"".equals(fabricColor)){
            fc = fabricColor;
        }
        if (!"".equals(colorName)){
            cn = colorName;
        }
        List<LooseFabric> looseFabricList = looseFabricService.getFabricJarByInfo(orderName, cn, fn, fc);
        map.put("looseFabricList", looseFabricList);
        return map;
    }

    @RequestMapping(value = "/updatejarbyinfo", method = RequestMethod.POST)
    @ResponseBody
    public int updateJarByInfo(@RequestParam("orderName")String orderName,
                               @RequestParam("fabricName")String fabricName,
                               @RequestParam("fabricColor")String fabricColor,
                               @RequestParam("jarNumber")String jarNumber,
                               @RequestParam("colorName")String colorName,
                               @RequestParam("updateJar")String updateJar){
        return looseFabricService.updateJarByInfo(orderName, fabricName, fabricColor, jarNumber, colorName, updateJar);
    }


}
