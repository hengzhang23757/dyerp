package com.example.erp01.action;

import com.example.erp01.model.ManufactureOrder;
import com.example.erp01.model.OrderClothes;
import com.example.erp01.service.ManufactureOrderService;
import com.example.erp01.service.OrderClothesService;
import com.example.erp01.service.SyncOrderService;
import com.example.erp01.ycMapper.SyncManufactureOrderMapper;
import com.example.erp01.ycMapper.SyncOrderMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "erp")
public class SyncOrderController {

    @Autowired
    private SyncOrderService syncOrderService;
    @Autowired
    private ManufactureOrderService manufactureOrderService;
    @Autowired
    private OrderClothesService orderClothesService;

    @RequestMapping(value = "/checkorderclothesexist", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> checkOrderClothesExist(@RequestParam("orderName")String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<OrderClothes> orderClothesList = syncOrderService.getOrderClothesByOrderName(orderName);
        if (orderClothesList != null && !orderClothesList.isEmpty()){
            map.put("result", 1);
        } else {
            map.put("result", 0);
        }
        return map;
    }

    @RequestMapping(value = "/syncorderclothestoyc", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> syncOrderClothesToYc(@RequestParam("orderName")String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        ManufactureOrder manufactureOrder = manufactureOrderService.getManufactureOrderByOrderName(orderName);
        syncOrderService.addManufactureOrderOne(manufactureOrder);
        List<OrderClothes> orderClothesList = orderClothesService.getOrderClothesByOrderName(orderName);
        int res = syncOrderService.addOrderClothesBatch(orderClothesList);
        map.put("result", res);
        return map;
    }

}
