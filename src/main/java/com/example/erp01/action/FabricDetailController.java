package com.example.erp01.action;

import com.example.erp01.mapper.LooseFabricMapper;
import com.example.erp01.model.*;
import com.example.erp01.service.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

@Controller
@RequestMapping(value = "erp")
public class FabricDetailController {

    @Autowired
    private FabricDetailService fabricDetailService;
    @Autowired
    private FabricStorageService fabricStorageService;
    @Autowired
    private DepartmentPlanService departmentPlanService;
    @Autowired
    private ManufactureFabricService manufactureFabricService;
    @Autowired
    private FabricOutRecordService fabricOutRecordService;
    @Autowired
    private LooseFabricService looseFabricService;

    @RequestMapping("/fabricDetailStart")
    public String fabricDetailStart(){
        return "basicInfo/fabricDetail";
    }

    @RequestMapping("/fabricAccessoryOverReviewStart")
    public String fabricAccessoryOverReviewStart(){
        return "console/fabricAccessoryOverReview";
    }

//    @RequestMapping(value = "/addfabricdetailbatch", method = RequestMethod.POST)
//    @ResponseBody
//    public int addFabricDetailBatch(@RequestParam("fabricDetailJson")String fabricDetailJson){
//        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
//        List<FabricDetail> fabricDetailList = gson.fromJson(fabricDetailJson,new TypeToken<List<FabricDetail>>(){}.getType());
//        int res1 = 0;
//        for (FabricDetail fabricDetail : fabricDetailList){
//            String orderName = fabricDetail.getOrderName();
//            String clothesVersionNumber = fabricDetail.getClothesVersionNumber();
//            String colorName = fabricDetail.getColorName();
//            String fabricName = fabricDetail.getFabricName();
//            String fabricNumber = fabricDetail.getFabricNumber();
//            String fabricColorNumber = fabricDetail.getFabricColorNumber();
//            String fabricColor = fabricDetail.getFabricColor();
//            String jarName = fabricDetail.getJarName();
//            String location = fabricDetail.getLocation();
//            String operateType = fabricDetail.getOperateType();
//            String unit = fabricDetail.getUnit();
//            String supplier = fabricDetail.getSupplier();
//            String isHit = fabricDetail.getIsHit();
//            Float weight = fabricDetail.getWeight();
//            Integer batchNumber = fabricDetail.getBatchNumber();
//            Date returnTime = fabricDetail.getReturnTime();
//            FabricStorage fabricStorage = fabricStorageService.getFabricStorageByOrderLocationJar(orderName, colorName, fabricName, fabricColor, jarName, location);
//            if (fabricStorage == null){
//                FabricStorage fs = new FabricStorage(location, orderName, clothesVersionNumber, fabricName, fabricNumber, fabricColor, fabricColorNumber, jarName, weight, batchNumber, operateType, returnTime, colorName, unit, isHit, supplier);
//                res1 += fabricStorageService.addFabricStorage(fs);
//            }else{
//                fabricStorage.setWeight(fabricStorage.getWeight() + fabricDetail.getWeight());
//                fabricStorage.setBatchNumber(fabricStorage.getBatchNumber() + fabricDetail.getBatchNumber());
//                res1 += fabricStorageService.updateFabricStorage(fabricStorage);
//            }
//        }
//        int res2 = fabricDetailService.addFabricDetailBatch(fabricDetailList);
//        if (res1 == 0 && res2 == 0){
//            return 0;
//        }else {
//            return 1;
//        }
//    }

    @RequestMapping(value = "/returnfabricreturn", method = RequestMethod.POST)
    @ResponseBody
    public int addFabricDetailBatch(@RequestParam("fabricOutRecordJson")String fabricOutRecordJson,
                                    @RequestParam("newLocation")String newLocation){
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        FabricOutRecord fabricOutRecord = gson.fromJson(fabricOutRecordJson,new TypeToken<FabricOutRecord>(){}.getType());
        FabricStorage fs = fabricStorageService.getFabricStorageByOrderLocationJar(null, null, null, null, fabricOutRecord.getJarName(), fabricOutRecord.getLocation(), fabricOutRecord.getFabricID());
        int res1,res2;
        if (fs == null){
            ManufactureFabric manufactureFabric = manufactureFabricService.getManufactureFabricByID(fabricOutRecord.getFabricID());
            FabricStorage fabricStorage = new FabricStorage(newLocation, manufactureFabric.getOrderName(), manufactureFabric.getClothesVersionNumber(), manufactureFabric.getFabricName(), manufactureFabric.getFabricNumber() == null ? "" : manufactureFabric.getFabricNumber(), manufactureFabric.getFabricColor(), "", fabricOutRecord.getJarName(), fabricOutRecord.getWeight(), fabricOutRecord.getBatchNumber(), "余布退库", new Date(), manufactureFabric.getColorName(), manufactureFabric.getUnit(), "", manufactureFabric.getSupplier(), fabricOutRecord.getFabricID());
            res1 = fabricStorageService.addFabricStorage(fabricStorage);
        } else {
            fs.setBatchNumber(fs.getBatchNumber() + fabricOutRecord.getBatchNumber());
            fs.setWeight(fs.getWeight() + fabricOutRecord.getWeight());
            res1 = fabricStorageService.updateFabricStorage(fs);
        }
        fabricOutRecord.setWeight(-fabricOutRecord.getWeight());
        fabricOutRecord.setBatchNumber(-fabricOutRecord.getBatchNumber());
        res2 = fabricOutRecordService.addFabricOutRecord(fabricOutRecord);
        if (res1 == 0 && res2 == 0){
            return 0;
        }else {
            return 1;
        }
    }

    @RequestMapping(value = "/deletefabricdetailbyid", method = RequestMethod.POST)
    @ResponseBody
    public int deleteFabricDetailByID(@RequestParam("fabricDetailID")Integer fabricDetailID){

        FabricDetail fd2 = fabricDetailService.getFabricDetailById(fabricDetailID);
        FabricStorage fs = fabricStorageService.getFabricStorageByOrderLocationJar(null, null, null, null, fd2.getJarName(), fd2.getLocation(), fd2.getFabricID());
        if (fs == null){
            return 3;
        }
        int res1,res2;
        if (fs.getBatchNumber().equals(fd2.getBatchNumber()) && fs.getWeight().equals(fd2.getWeight())){
            res1 = fabricStorageService.deleteFabricStorage(fs.getFabricStorageID());
        } else if (fs.getBatchNumber() < fd2.getBatchNumber() || fs.getWeight() < fd2.getWeight()){
            return 3;
        } else {
            fs.setWeight(fs.getWeight() - fd2.getWeight());
            fs.setBatchNumber(fs.getBatchNumber() - fd2.getBatchNumber());
            res1 = fabricStorageService.updateFabricStorage(fs);
        }
        res2 = fabricDetailService.deleteFabricDetailByID(fabricDetailID);
        if (res1 == 0 && res2 == 0){
            return 0;
        } else {
            return 1;
        }
    }

    @RequestMapping(value = "/deletefabricalldatabyjar", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> deleteFabricAllDataByJar(@RequestParam("orderName")String orderName,
                                                        @RequestParam("jarName")String jarName){
        Map<String, Object> map = new HashMap<>();
        String jarNumber = jarName + "#";
        int res1 = fabricDetailService.deleteFabricDetailByOrderJar(orderName, jarName);
        int res2 = fabricStorageService.deleteFabricStorageByOrderNameJar(orderName, jarName);
        int res3 = fabricOutRecordService.deleteFabricOutRecordByOrderJar(orderName, jarName);
        int res4 = looseFabricService.deleteLooseFabricByOrderNameJar(orderName, jarNumber);
        map.put("result", res1 + res2 + res3 + res4);
        return map;
    }

    @RequestMapping(value = "/gettodayfabricdetail", method = RequestMethod.GET)
    public String getTodayFabricDetail(Model model){
        List<FabricDetail> fabricDetailList = fabricDetailService.getTodayFabricDetail();
        model.addAttribute("fabricDetailList",fabricDetailList);
        return "basicInfo/fb_fabricDetailList";
    }

    @RequestMapping(value = "/getonemonthfabricdetail", method = RequestMethod.GET)
    public String getOneMonthFabricDetail(Model model){
        List<FabricDetail> fabricDetailList = fabricDetailService.getOneMonthFabricDetail();
        model.addAttribute("fabricDetailList",fabricDetailList);
        return "basicInfo/fb_fabricDetailList";
    }


    @RequestMapping(value = "/getoneyearfabricdetail", method = RequestMethod.GET)
    public String getOneYearFabricDetail(Model model){
        List<FabricDetail> fabricDetailList = fabricDetailService.getOneYearFabricDetail();
        model.addAttribute("fabricDetailList",fabricDetailList);
        return "basicInfo/fb_fabricDetailList";
    }

    @RequestMapping(value = "/updatefabricdetail", method = RequestMethod.POST)
    @ResponseBody
    public int updateFabricDetail(@RequestParam("fabricDetail") String fabricDetail){

        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        FabricDetail fd1 = gson.fromJson(fabricDetail,new TypeToken<FabricDetail>(){}.getType());
        FabricDetail fd2 = fabricDetailService.getFabricDetailById(fd1.getFabricDetailID());
        FabricStorage fs = fabricStorageService.getFabricStorageByOrderLocationJar(null, null, null, null, fd2.getJarName(), fd2.getLocation(), fd2.getFabricID());
        if (fs == null){
            return 3;
        }
        if (fd2.getWeight().equals(fs.getWeight()) && fd2.getBatchNumber().equals(fs.getBatchNumber())){
            fs.setJarName(fd1.getJarName());
            fs.setBatchNumber(fd1.getBatchNumber());
            fs.setWeight(fd1.getWeight());
            fs.setLocation(fd1.getLocation());
        } else if (fd1.getBatchNumber() < fs.getBatchNumber() || fd1.getWeight() < fs.getWeight()) {
            return 4; // 库存不足  无法修改
        } else {
            fs.setJarName(fd1.getJarName());
            fs.setBatchNumber(fs.getBatchNumber() + fd1.getBatchNumber() - fd2.getBatchNumber());
            fs.setWeight(fs.getWeight() + fd1.getWeight() - fd2.getWeight());
            fs.setLocation(fd1.getLocation());
        }
        int res1 = fabricDetailService.updateFabricDetail(fd1);
        int res2 = fabricStorageService.updateFabricStorage(fs);
        if (res1 == 0 && res2 == 0){
            return 0;
        } else {
            return 1;
        }
    }

    @RequestMapping(value = "/getfabriclocationhint", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getFabricLocationHint(@RequestParam("orderName")String orderName,
                                                     @RequestParam("colorName")String colorName,
                                                     @RequestParam("fabricName")String fabricName,
                                                     @RequestParam("fabricColor")String fabricColor,
                                                     @RequestParam("jarName")String jarName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> fabricLocationList = fabricDetailService.getFabricLocationHint(orderName, colorName, fabricName, fabricColor, jarName);
        map.put("fabricLocationList", fabricLocationList);
        return map;
    }


    @RequestMapping(value = "/getjarnamehintbyorder", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getJarNameHintByOrder(@RequestParam("orderName")String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> jarNameList = fabricDetailService.getJarNameByOrder(orderName);
        map.put("jarNameList", jarNameList);
        return map;
    }

    @RequestMapping(value = "/getfabricdetailbyinfo", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getFabricDetailByInfo(@RequestParam("fabricID")Integer fabricID){
        Map<String, Object> map = new LinkedHashMap<>();
        List<FabricDetail> fabricDetailList = fabricDetailService.getFabricDetailByInfo(null, null, null, null, fabricID);
        map.put("data", fabricDetailList);
        map.put("count", fabricDetailList.size());
        map.put("code",0);
        return map;
    }


    @RequestMapping(value = "/addfabricreturndetailbatch", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> addFabricReturnDetailBatch(@RequestParam("fabricDetailJson")String fabricDetailJson,
                                                          @RequestParam("fabricID")Integer fabricID,
                                                          @RequestParam("fabricActCount")Float fabricActCount,
                                                          @RequestParam("thisTimeWeight")Float thisTimeWeight){
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        Map<String, Object> map = new LinkedHashMap<>();
        Float historyWeight = fabricDetailService.getHistoryFabricReturnWeight(fabricID);
        Float returnWeight = fabricOutRecordService.getReturnFabricWeightByFabricID(fabricID);
        historyWeight -= returnWeight;
        boolean overFlag = false;
        Float overWeight = 0f;
        Float availableWeight = 0f;
        if (fabricActCount <= 50){
            if (historyWeight + thisTimeWeight - fabricActCount > (15 + 0.01)){
                overFlag = true;
                availableWeight = fabricActCount + 15 - historyWeight;
                overWeight = 15f;
            }
        } else if (fabricActCount > 50 && fabricActCount <= 500){
            if (historyWeight + thisTimeWeight - fabricActCount > fabricActCount * 0.05 + 0.01){
                overFlag = true;
                availableWeight = fabricActCount + fabricActCount * 0.05f - historyWeight;
                overWeight = fabricActCount * 0.05f;
            }
        } else if (fabricActCount > 500 && fabricActCount <= 1000){
            if (historyWeight + thisTimeWeight - fabricActCount > fabricActCount * 0.03 + 0.01){
                overFlag = true;
                availableWeight = fabricActCount + fabricActCount * 0.03f - historyWeight;
                overWeight = fabricActCount * 0.03f;
            }
        } else if (fabricActCount > 1000 && fabricActCount <= 5000){
            if (historyWeight + thisTimeWeight - fabricActCount > fabricActCount * 0.02 + 0.01){
                overFlag = true;
                availableWeight = fabricActCount + fabricActCount * 0.02f - historyWeight;
                overWeight = fabricActCount * 0.02f;
            }
        } else if (fabricActCount > 5000 && fabricActCount <= 10000){
            if (historyWeight + thisTimeWeight - fabricActCount > fabricActCount * 0.015 + 0.01){
                overFlag = true;
                availableWeight = fabricActCount + fabricActCount * 0.015f - historyWeight;
                overWeight = fabricActCount * 0.015f;
            }
        } else if (fabricActCount > 10000 && fabricActCount <= 20000){
            if (historyWeight + thisTimeWeight - fabricActCount > fabricActCount * 0.01 + 0.01){
                overFlag = true;
                availableWeight = fabricActCount + fabricActCount * 0.01f - historyWeight;
                overWeight = fabricActCount * 0.01f;
            }
        } else {
            if (historyWeight + thisTimeWeight - fabricActCount > 200  + 0.01){
                overFlag = true;
                availableWeight = fabricActCount + 200 - historyWeight;
                overWeight = 200f;
            }
        }
        if (overFlag){
            map.put("over", 1);
            map.put("fabricActCount", fabricActCount);
            map.put("historyWeight", historyWeight);
            map.put("availableWeight", availableWeight);
            map.put("overWeight", overWeight);
            return map;
        }
        ManufactureFabric manufactureFabric = manufactureFabricService.getManufactureFabricByID(fabricID);
        if (!manufactureFabric.getCheckState().equals("通过")){
            map.put("result", 3);
            return map;
        }
        List<FabricDetail> fabricDetailList = gson.fromJson(fabricDetailJson,new TypeToken<List<FabricDetail>>(){}.getType());
        int res1 = 0;
        for (FabricDetail fabricDetail : fabricDetailList){
            String orderName = fabricDetail.getOrderName();
            String clothesVersionNumber = fabricDetail.getClothesVersionNumber();
            String fabricName = fabricDetail.getFabricName();
            String fabricNumber = fabricDetail.getFabricNumber();
            String fabricColorNumber = fabricDetail.getFabricColorNumber();
            String fabricColor = fabricDetail.getFabricColor();
            String jarName = fabricDetail.getJarName();
            String location = fabricDetail.getLocation();
            String operateType = fabricDetail.getOperateType();
            String colorName = fabricDetail.getColorName();
            String unit = fabricDetail.getUnit();
            String supplier = fabricDetail.getSupplier();
            String isHit = fabricDetail.getIsHit();
            Float weight = fabricDetail.getWeight();
            Integer batchNumber = fabricDetail.getBatchNumber();
            Date returnTime = fabricDetail.getReturnTime();
            List<DepartmentPlan> departmentPlanList = departmentPlanService.getDepartmentPlanByFinishState("未完成", orderName, colorName);
            FabricStorage fabricStorage = fabricStorageService.getFabricStorageByOrderColorLocationJar(null, null, null, null, jarName, location, fabricID);
            if (departmentPlanList != null && !departmentPlanList.isEmpty()){
                for (DepartmentPlan departmentPlan : departmentPlanList){
                    departmentPlan.setTotalCount(departmentPlan.getTotalCount() + batchNumber);
                    departmentPlan.setPlanCount(departmentPlan.getPlanCount() + batchNumber);
                    departmentPlan.setCompletionRate((float)departmentPlan.getActCount()/departmentPlan.getPlanCount());
                    departmentPlan.setCompletionRateCut((float)departmentPlan.getCutCount()/departmentPlan.getPlanCount());
                }
                departmentPlanService.updateDepartmentPlanBatch(departmentPlanList);
            }
            if (fabricStorage == null){
                FabricStorage fs = new FabricStorage(location, orderName, clothesVersionNumber, fabricName, fabricNumber, fabricColor, fabricColorNumber, jarName, weight, batchNumber, operateType, returnTime, colorName, unit, isHit, supplier, fabricID);
                res1 += fabricStorageService.addFabricStorage(fs);
            }else{
                fabricStorage.setWeight(fabricStorage.getWeight() + fabricDetail.getWeight());
                fabricStorage.setBatchNumber(fabricStorage.getBatchNumber() + fabricDetail.getBatchNumber());
                res1 += fabricStorageService.updateFabricStorage(fabricStorage);
            }
        }
        int res2 = fabricDetailService.addFabricDetailBatch(fabricDetailList);
        if (res1 == 0 && res2 == 0){
            map.put("result", 0);
        }else {
            map.put("result", 1);
        }
        return map;
    }


    @RequestMapping(value = "/addfabricdetailbatchtotmpstorage", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> addFabricReturnDetailBatchForReview(@RequestParam("fabricDetailJson")String fabricDetailJson,
                                                                   @RequestParam("fabricID")Integer fabricID){
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        Map<String, Object> map = new LinkedHashMap<>();
        ManufactureFabric manufactureFabric = manufactureFabricService.getManufactureFabricByID(fabricID);
        if (!manufactureFabric.getCheckState().equals("通过")){
            map.put("result", 3);
            return map;
        }
        List<FabricDetail> fabricDetailList = gson.fromJson(fabricDetailJson,new TypeToken<List<FabricDetail>>(){}.getType());
        int res2 = fabricDetailService.addFabricDetailTmpBatch(fabricDetailList);
        map.put("result", res2);
        return map;
    }



    @RequestMapping(value = "/getfabricdetailreviewdatacount", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getFabricDetailReviewDataCount(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<FabricDetail> fabricDetailList = fabricDetailService.getFabricDetailByOperateType("待审核");
        map.put("count", fabricDetailList.size());
        map.put("code",0);
        return map;
    }

    @RequestMapping(value = "/getfabricdetailreviewdata", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getFabricDetailReviewData(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<FabricDetail> fabricDetailList = fabricDetailService.getFabricDetailByOperateType("待审核");
        List<Integer> fabricIDList = new ArrayList<>();
        for (FabricDetail fabricDetail : fabricDetailList){
            fabricIDList.add(fabricDetail.getFabricID());
        }
        List<ManufactureFabric> manufactureFabricList = manufactureFabricService.getManufactureFabricByFabricIdList(fabricIDList);
        List<FabricDetail> fabricDetailList1 = fabricDetailService.getFabricDetailByFabricIDList(fabricIDList);
        for (FabricDetail fabricDetail : fabricDetailList){
            for (ManufactureFabric manufactureFabric : manufactureFabricList){
                if (fabricDetail.getFabricID().equals(manufactureFabric.getFabricID())){
                    fabricDetail.setFabricActCount(manufactureFabric.getFabricActCount());
                }
            }
            for (FabricDetail fabricDetail1 : fabricDetailList1){
                if (fabricDetail.getFabricID().equals(fabricDetail1.getFabricID()) && fabricDetail1.getOperateType().equals("待审核")){
                    fabricDetail.setWeight(fabricDetail1.getWeight());
                } else {
                    fabricDetail.setFinishWeight(fabricDetail1.getWeight());
                }
            }
        }
        map.put("data", fabricDetailList);
        map.put("count", fabricDetailList.size());
        map.put("code",0);
        return map;
    }


    @RequestMapping(value = "/changefabricdetailoperatetype", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> changeFabricDetailOperateType(@RequestParam("fabricDetailID")Integer fabricDetailID,
                                                             @RequestParam("operateType")String operateType){
        Map<String, Object> map = new LinkedHashMap<>();
        FabricDetail fabricDetail = new FabricDetail();
        fabricDetail.setFabricDetailID(fabricDetailID);
        fabricDetail.setOperateType(operateType);
        fabricDetail.setWeight(null);
        fabricDetail.setPrice(null);
        fabricDetail.setFabricActCount(null);
        int res = fabricDetailService.updateFabricDetail(fabricDetail);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/changefabricdiscountbyid", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> changeFabricDiscountById(@RequestParam("fabricDetailID")Integer fabricDetailID,
                                                        @RequestParam("discount")Float discount){
        Map<String, Object> map = new LinkedHashMap<>();
        FabricDetail fabricDetail = new FabricDetail();
        fabricDetail.setFabricDetailID(fabricDetailID);
        fabricDetail.setOperateType("入库");
        fabricDetail.setWeight(null);
        fabricDetail.setPrice(null);
        fabricDetail.setFabricActCount(null);
        int res = fabricDetailService.updateFabricDetail(fabricDetail);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/getfabricdetailtmpbyid", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getFabricDetailTmpById(@RequestParam("fabricID")Integer fabricID){
        Map<String, Object> map = new LinkedHashMap<>();
        List<FabricDetail> fabricDetailList = fabricDetailService.getFabricDetailTmpByFabricId(fabricID, null, null);
        map.put("data", fabricDetailList);
        map.put("count", fabricDetailList.size());
        map.put("code",0);
        return map;
    }


    @RequestMapping(value = "/deletefabricdetailtmpbyid", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> deleteFabricDetailTmpById(@RequestParam("fabricDetailID")Integer fabricDetailID){
        Map<String, Object> map = new LinkedHashMap<>();
        int res = fabricDetailService.deleteFabricDetailTmpById(fabricDetailID);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/changefabricorder", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> changeFabricOrder(@RequestParam("fabricStorageID")Integer fabricStorageID,
                                                 @RequestParam("initID")Integer initID,
                                                 @RequestParam("fabricID")Integer fabricID,
                                                 @RequestParam("changeWeight")Float changeWeight,
                                                 @RequestParam("changeBatch")Integer changeBatch){
        Map<String, Object> map = new LinkedHashMap<>();
        FabricStorage fabricStorage = fabricStorageService.getFabricStorageByID(fabricStorageID);
        FabricDetail initFabricDetail = fabricDetailService.getFabricDetailByLocationJarFabricID(fabricStorage.getLocation(), fabricStorage.getJarName(), fabricStorage.getFabricID());
        if (initFabricDetail == null){
            map.put("result", 4);
            return map;
        }
        // 判断是更新还是删除库存记录
        if (fabricStorage.getWeight() - changeWeight < 1){
            fabricStorageService.deleteFabricStorage(fabricStorageID);
        } else {
            if (fabricStorage.getBatchNumber() - changeBatch == 0){
                fabricStorage.setBatchNumber(1);
            } else {
                fabricStorage.setBatchNumber(fabricStorage.getBatchNumber() - changeBatch);
            }
            fabricStorage.setWeight(fabricStorage.getWeight() - changeWeight);
            fabricStorageService.updateFabricStorage(fabricStorage);
        }
        // 添加一个负数往直前的入库记录
        List<FabricDetail> fabricDetailList = new ArrayList<>();
        initFabricDetail.setBatchNumber(-changeBatch);
        initFabricDetail.setWeight(-changeWeight);
        initFabricDetail.setRemark("换款减数");
        initFabricDetail.setOperateType("换款");
        initFabricDetail.setCheckMonth(null);
        initFabricDetail.setCheckNumber(null);
        fabricDetailList.add(initFabricDetail);
        int res1 = fabricDetailService.addFabricDetailBatch(fabricDetailList);
        // 添加调到的面料信息
        ManufactureFabric manufactureFabric = manufactureFabricService.getManufactureFabricByID(fabricID);
        FabricDetail fabricDetail = new FabricDetail();
        fabricDetail.setOrderName(manufactureFabric.getOrderName());
        fabricDetail.setClothesVersionNumber(manufactureFabric.getClothesVersionNumber());
        fabricDetail.setFabricName(manufactureFabric.getFabricName());
        fabricDetail.setFabricNumber(manufactureFabric.getFabricNumber());
        fabricDetail.setFabricColor(manufactureFabric.getFabricColor());
        fabricDetail.setFabricColorNumber(manufactureFabric.getFabricColorNumber());
        fabricDetail.setColorName(manufactureFabric.getColorName());
        fabricDetail.setJarName(initFabricDetail.getJarName());
        fabricDetail.setBatchNumber(changeBatch);
        fabricDetail.setWeight(changeWeight);
        fabricDetail.setLocation(initFabricDetail.getLocation());
        fabricDetail.setSupplier(initFabricDetail.getSupplier());
        fabricDetail.setUnit(initFabricDetail.getUnit());
        fabricDetail.setFabricID(manufactureFabric.getFabricID());
        fabricDetail.setIsHit(manufactureFabric.getIsHit());
        fabricDetail.setReturnTime(initFabricDetail.getReturnTime());
        fabricDetail.setOperateType("换款调用");
        fabricDetail.setRemark("由" + initFabricDetail.getOrderName() + "款调用");
        List<FabricDetail> addFabricDetailList = new ArrayList<>();
        addFabricDetailList.add(fabricDetail);
        Float historyWeight = fabricDetailService.getHistoryFabricReturnWeight(fabricID);
        Float returnWeight = fabricOutRecordService.getReturnFabricWeightByFabricID(fabricID);
        historyWeight -= returnWeight;
        boolean overFlag = false;
        Float overWeight = 0f;
        Float availableWeight = 0f;
        Float fabricActCount = manufactureFabric.getFabricActCount();
        if (fabricActCount <= 50){
            if (historyWeight + changeWeight - fabricActCount > 15){
                overFlag = true;
                availableWeight = fabricActCount + 15 - historyWeight;
                overWeight = 15f;
            }
        } else if (fabricActCount > 50 && fabricActCount <= 500){
            if (historyWeight + changeWeight - fabricActCount > fabricActCount * 0.05){
                overFlag = true;
                availableWeight = fabricActCount + fabricActCount * 0.05f - historyWeight;
                overWeight = fabricActCount * 0.05f;
            }
        } else if (fabricActCount > 500 && fabricActCount <= 1000){
            if (historyWeight + changeWeight - fabricActCount > fabricActCount * 0.03){
                overFlag = true;
                availableWeight = fabricActCount + fabricActCount * 0.03f - historyWeight;
                overWeight = fabricActCount * 0.03f;
            }
        } else if (fabricActCount > 1000 && fabricActCount <= 5000){
            if (historyWeight + changeWeight - fabricActCount > fabricActCount * 0.02){
                overFlag = true;
                availableWeight = fabricActCount + fabricActCount * 0.02f - historyWeight;
                overWeight = fabricActCount * 0.02f;
            }
        } else if (fabricActCount > 5000 && fabricActCount <= 10000){
            if (historyWeight + changeWeight - fabricActCount > fabricActCount * 0.015){
                overFlag = true;
                availableWeight = fabricActCount + fabricActCount * 0.015f - historyWeight;
                overWeight = fabricActCount * 0.015f;
            }
        } else if (fabricActCount > 10000 && fabricActCount <= 20000){
            if (historyWeight + changeWeight - fabricActCount > fabricActCount * 0.01){
                overFlag = true;
                availableWeight = fabricActCount + fabricActCount * 0.01f - historyWeight;
                overWeight = fabricActCount * 0.01f;
            }
        } else {
            if (historyWeight + changeWeight - fabricActCount > 200){
                overFlag = true;
                availableWeight = fabricActCount + 200 - historyWeight;
                overWeight = 200f;
            }
        }
        if (overFlag){
            map.put("over", 1);
            map.put("fabricActCount", fabricActCount);
            map.put("historyWeight", historyWeight);
            map.put("availableWeight", availableWeight);
            map.put("overWeight", overWeight);
            return map;
        }
        if (!manufactureFabric.getCheckState().equals("通过")){
            map.put("result", 3);
            return map;
        }
        int res3 = 0;
        FabricStorage newFabricStorage = fabricStorageService.getFabricStorageByOrderColorLocationJar(null, null, null, null, initFabricDetail.getJarName(), initFabricDetail.getLocation(), fabricID);
        if (newFabricStorage == null){
            FabricStorage fs = new FabricStorage(initFabricDetail.getLocation(), manufactureFabric.getOrderName(), manufactureFabric.getClothesVersionNumber(), manufactureFabric.getFabricName(), manufactureFabric.getFabricNumber(), manufactureFabric.getFabricColor(), manufactureFabric.getFabricColorNumber(), initFabricDetail.getJarName(), changeWeight, changeBatch, "换款", initFabricDetail.getReturnTime(), manufactureFabric.getColorName(), manufactureFabric.getUnit(), manufactureFabric.getIsHit(), manufactureFabric.getSupplier(), fabricID);
            res3 += fabricStorageService.addFabricStorage(fs);
        }else{
            newFabricStorage.setWeight(newFabricStorage.getWeight() + changeWeight);
            newFabricStorage.setBatchNumber(newFabricStorage.getBatchNumber() + changeBatch);
            res3 += fabricStorageService.updateFabricStorage(newFabricStorage);
        }
        int res2 = fabricDetailService.addFabricDetailBatch(addFabricDetailList);
        if (res1 == 0 && res2 == 0 && res3 == 0){
            map.put("result", 0);
        } else {
            map.put("result", 1);
        }
        return map;
    }


    @RequestMapping(value = "/changetmpfabricorder", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> changeTmpFabricOrder(@RequestParam("fabricDetailID")Integer fabricDetailID,
                                                    @RequestParam("fabricID")Integer fabricID,
                                                    @RequestParam("changeWeight")Float changeWeight,
                                                    @RequestParam("changeBatch")Integer changeBatch){
        Map<String, Object> map = new LinkedHashMap<>();
        FabricDetail fabricDetail = fabricDetailService.getTmpFabricById(fabricDetailID);
        if (fabricDetail == null){
            map.put("result", 4);
            return map;
        }
        // 添加调到的面料信息
        ManufactureFabric manufactureFabric = manufactureFabricService.getManufactureFabricByID(fabricID);
        FabricDetail fabricDetailNew = new FabricDetail();
        fabricDetailNew.setOrderName(manufactureFabric.getOrderName());
        fabricDetailNew.setClothesVersionNumber(manufactureFabric.getClothesVersionNumber());
        fabricDetailNew.setFabricName(manufactureFabric.getFabricName());
        fabricDetailNew.setFabricNumber(manufactureFabric.getFabricNumber());
        fabricDetailNew.setFabricColor(manufactureFabric.getFabricColor());
        fabricDetailNew.setFabricColorNumber(manufactureFabric.getFabricColorNumber());
        fabricDetailNew.setColorName(manufactureFabric.getColorName());
        fabricDetailNew.setJarName(fabricDetail.getJarName());
        fabricDetailNew.setBatchNumber(changeBatch);
        fabricDetailNew.setWeight(changeWeight);
        fabricDetailNew.setLocation(fabricDetail.getLocation());
        fabricDetailNew.setSupplier(fabricDetail.getSupplier());
        fabricDetailNew.setUnit(fabricDetail.getUnit());
        fabricDetailNew.setFabricID(manufactureFabric.getFabricID());
        fabricDetailNew.setIsHit(manufactureFabric.getIsHit());
        fabricDetailNew.setReturnTime(fabricDetail.getReturnTime());
        fabricDetailNew.setOperateType("暂存面料换款调用");
        fabricDetailNew.setRemark("由" + fabricDetail.getOrderName() + "款的暂存面料调用");
        List<FabricDetail> addFabricDetailList = new ArrayList<>();
        addFabricDetailList.add(fabricDetailNew);
        Float historyWeight = fabricDetailService.getHistoryFabricReturnWeight(fabricID);
        Float returnWeight = fabricOutRecordService.getReturnFabricWeightByFabricID(fabricID);
        historyWeight -= returnWeight;
        boolean overFlag = false;
        Float overWeight = 0f;
        Float availableWeight = 0f;
        Float fabricActCount = manufactureFabric.getFabricActCount();
        if (fabricActCount <= 50){
            if (historyWeight + changeWeight - fabricActCount > 15){
                overFlag = true;
                availableWeight = fabricActCount + 15 - historyWeight;
                overWeight = 15f;
            }
        } else if (fabricActCount > 50 && fabricActCount <= 500){
            if (historyWeight + changeWeight - fabricActCount > fabricActCount * 0.05){
                overFlag = true;
                availableWeight = fabricActCount + fabricActCount * 0.05f - historyWeight;
                overWeight = fabricActCount * 0.05f;
            }
        } else if (fabricActCount > 500 && fabricActCount <= 1000){
            if (historyWeight + changeWeight - fabricActCount > fabricActCount * 0.03){
                overFlag = true;
                availableWeight = fabricActCount + fabricActCount * 0.03f - historyWeight;
                overWeight = fabricActCount * 0.03f;
            }
        } else if (fabricActCount > 1000 && fabricActCount <= 5000){
            if (historyWeight + changeWeight - fabricActCount > fabricActCount * 0.02){
                overFlag = true;
                availableWeight = fabricActCount + fabricActCount * 0.02f - historyWeight;
                overWeight = fabricActCount * 0.02f;
            }
        } else if (fabricActCount > 5000 && fabricActCount <= 10000){
            if (historyWeight + changeWeight - fabricActCount > fabricActCount * 0.015){
                overFlag = true;
                availableWeight = fabricActCount + fabricActCount * 0.015f - historyWeight;
                overWeight = fabricActCount * 0.015f;
            }
        } else if (fabricActCount > 10000 && fabricActCount <= 20000){
            if (historyWeight + changeWeight - fabricActCount > fabricActCount * 0.01){
                overFlag = true;
                availableWeight = fabricActCount + fabricActCount * 0.01f - historyWeight;
                overWeight = fabricActCount * 0.01f;
            }
        } else {
            if (historyWeight + changeWeight - fabricActCount > 200){
                overFlag = true;
                availableWeight = fabricActCount + 200 - historyWeight;
                overWeight = 200f;
            }
        }
        if (overFlag){
            map.put("over", 1);
            map.put("fabricActCount", fabricActCount);
            map.put("historyWeight", historyWeight);
            map.put("availableWeight", availableWeight);
            map.put("overWeight", overWeight);
            return map;
        }
        if (!manufactureFabric.getCheckState().equals("通过")){
            map.put("result", 3);
            return map;
        }
        int res3 = 0;
        FabricStorage newFabricStorage = fabricStorageService.getFabricStorageByOrderColorLocationJar(null, null, null, null, fabricDetail.getJarName(), fabricDetail.getLocation(), fabricID);
        if (newFabricStorage == null){
            FabricStorage fs = new FabricStorage(fabricDetail.getLocation(), manufactureFabric.getOrderName(), manufactureFabric.getClothesVersionNumber(), manufactureFabric.getFabricName(), manufactureFabric.getFabricNumber(), manufactureFabric.getFabricColor(), manufactureFabric.getFabricColorNumber(), fabricDetail.getJarName(), changeWeight, changeBatch, "换款", fabricDetail.getReturnTime(), manufactureFabric.getColorName(), manufactureFabric.getUnit(), manufactureFabric.getIsHit(), manufactureFabric.getSupplier(), fabricID);
            res3 += fabricStorageService.addFabricStorage(fs);
        }else{
            newFabricStorage.setWeight(newFabricStorage.getWeight() + changeWeight);
            newFabricStorage.setBatchNumber(newFabricStorage.getBatchNumber() + changeBatch);
            res3 += fabricStorageService.updateFabricStorage(newFabricStorage);
        }
        int res2 = fabricDetailService.addFabricDetailBatch(addFabricDetailList);
        if (res2 == 0 && res3 == 0){
            map.put("result", 0);
        } else {
            map.put("result", 1);
        }
        // 判断是更新还是删除库存记录
        if (fabricDetail.getWeight() - changeWeight < 1){
            fabricDetailService.deleteFabricDetailTmpById(fabricDetailID);
        } else {
            if (fabricDetail.getBatchNumber() - changeBatch == 0){
                fabricDetail.setBatchNumber(1);
            } else {
                fabricDetail.setBatchNumber(fabricDetail.getBatchNumber() - changeBatch);
            }
            fabricDetail.setWeight(fabricDetail.getWeight() - changeWeight);
            fabricDetailService.updateTmpFabricDetail(fabricDetail);
        }
        return map;
    }


}
