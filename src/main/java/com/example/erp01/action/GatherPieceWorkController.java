package com.example.erp01.action;

import com.example.erp01.model.GeneralSalary;
import com.example.erp01.model.OrderProcedure;
import com.example.erp01.service.GatherPieceWorkService;
import com.example.erp01.service.OrderProcedureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

@Controller
@RequestMapping(value = "erp")
public class GatherPieceWorkController {

    @Autowired
    private GatherPieceWorkService gatherPieceWorkService;
    @Autowired
    private OrderProcedureService orderProcedureService;

    @RequestMapping(value = "/gatherPieceWorkReportStart")
    public String gatherPieceWorkReportStart(){
        return "pieceWork/gatherPieceWorkReport";
    }

    @RequestMapping(value = "/searchgatherpieceworkbyinfo", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> searchGatherPieceWorkByInfo(@RequestParam(value = "orderName", required = false) String orderName,
                                                           @RequestParam(value = "groupName", required = false) String groupName,
                                                           @RequestParam(value = "employeeNumber", required = false) String employeeNumber,
                                                           @RequestParam(value = "from", required = false) String from,
                                                           @RequestParam(value = "to", required = false) String to,
                                                           @RequestParam(value = "procedureNumber", required = false) Integer procedureNumber,
                                                           @RequestParam(value = "searchType", required = false) String searchType,
                                                           @RequestParam(value = "procedureState", required = false) Integer procedureState){
        Map<String, Object> map = new LinkedHashMap<>();
        List<GeneralSalary> generalSalaryList = new ArrayList<>();
        if (searchType.equals("产能详情")){
            generalSalaryList = gatherPieceWorkService.getGatherPieceWorkEachDaySummary(orderName, groupName, employeeNumber, from, to, procedureNumber);
        } else if (searchType.equals("产能汇总")){
            generalSalaryList = gatherPieceWorkService.getGatherPieceWorkAmongSummary(orderName, groupName, employeeNumber, from, to, procedureNumber);
        } else if (searchType.equals("金额汇总")){
            List<GeneralSalary> pieceWorkList = gatherPieceWorkService.getGatherPieceWorkAmongSummary(orderName, groupName, employeeNumber, from, to, procedureNumber);
            Set<String> orderNameSet = new HashSet<>();
            Set<String> employeeSet = new HashSet<>();
            for (GeneralSalary generalSalary : pieceWorkList){
                orderNameSet.add(generalSalary.getOrderName());
                employeeSet.add(generalSalary.getEmployeeNumber());
            }
            List<OrderProcedure> orderProcedureList = orderProcedureService.getOrderProcedureByOrderNames(new ArrayList<>(orderNameSet), procedureState);
            for (GeneralSalary generalSalary : pieceWorkList){
                for (OrderProcedure orderProcedure : orderProcedureList){
                    if (generalSalary.getOrderName().equals(orderProcedure.getOrderName()) && generalSalary.getProcedureNumber().equals(orderProcedure.getProcedureNumber())){
                        generalSalary.setPrice((float)orderProcedure.getPiecePrice());
                        generalSalary.setPriceTwo((float) orderProcedure.getPiecePriceTwo());
                        generalSalary.setSalary(generalSalary.getPieceCount()*generalSalary.getPrice());
                        generalSalary.setSalaryTwo(generalSalary.getPieceCount() * generalSalary.getPriceTwo());
                        generalSalary.setSalarySum(generalSalary.getSalary() + generalSalary.getSalaryTwo());
                    }
                }
            }
            for (String empNumber : employeeSet){
                GeneralSalary empGeneralSalary = new GeneralSalary();
                empGeneralSalary.setEmployeeNumber(empNumber);
                for (GeneralSalary generalSalary : pieceWorkList){
                    if (empNumber.equals(generalSalary.getEmployeeNumber())){
                        empGeneralSalary.setEmployeeName(generalSalary.getEmployeeName());
                        empGeneralSalary.setGroupName(generalSalary.getGroupName());
                        empGeneralSalary.setSalary(empGeneralSalary.getSalary() + generalSalary.getSalary());
                        empGeneralSalary.setSalaryTwo(empGeneralSalary.getSalaryTwo() + generalSalary.getSalaryTwo());
                        empGeneralSalary.setSalarySum(empGeneralSalary.getSalary() + empGeneralSalary.getSalaryTwo());
                    }
                }
                generalSalaryList.add(empGeneralSalary);
            }
        } else if (searchType.equals("金额详情")){
            generalSalaryList = gatherPieceWorkService.getGatherSalaryEachDaySummary(orderName, groupName, employeeNumber, from, to, procedureNumber);
            Set<String> orderNameSet = new HashSet<>();
            for (GeneralSalary generalSalary : generalSalaryList){
                orderNameSet.add(generalSalary.getOrderName());
            }
            List<OrderProcedure> orderProcedureList = orderProcedureService.getOrderProcedureByOrderNames(new ArrayList<>(orderNameSet), procedureState);
            for (GeneralSalary generalSalary : generalSalaryList){
                for (OrderProcedure orderProcedure : orderProcedureList){
                    if (generalSalary.getOrderName().equals(orderProcedure.getOrderName()) && generalSalary.getProcedureNumber().equals(orderProcedure.getProcedureNumber())){
                        generalSalary.setPrice((float)orderProcedure.getPiecePrice());
                        generalSalary.setPriceTwo((float) orderProcedure.getPiecePriceTwo());
                        generalSalary.setSalary(generalSalary.getPieceCount()*generalSalary.getPrice());
                        generalSalary.setSalaryTwo(generalSalary.getPieceCount() * generalSalary.getPriceTwo());
                        generalSalary.setSalarySum(generalSalary.getSalary() + generalSalary.getSalaryTwo());
                    }
                }
            }
        }
        map.put("generalSalaryList", generalSalaryList);
        return map;
    }


}
