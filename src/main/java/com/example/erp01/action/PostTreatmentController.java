package com.example.erp01.action;

import com.example.erp01.model.*;
import com.example.erp01.service.ManufactureOrderService;
import com.example.erp01.service.OrderClothesService;
import com.example.erp01.service.PostTreatmentService;
import com.example.erp01.service.TailorService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "erp")
public class PostTreatmentController {

    @Autowired
    private PostTreatmentService postTreatmentService;
    @Autowired
    private OrderClothesService orderClothesService;
    @Autowired
    private TailorService tailorService;
    @Autowired
    private ManufactureOrderService manufactureOrderService;

    @RequestMapping("/postTreatmentStart")
    public String postTreatmentStart(){
        return "postTreatment/postTreatment";
    }


    @RequestMapping(value = "/getposttreatmentbyorder", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getPostTreatmentByOrder(@RequestParam("orderName")String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<OrderClothes> orderClothesList = orderClothesService.getOrderClothesByOrderName(orderName);
        List<CutQueryLeak> cutQueryLeakList = tailorService.getTailorInfoGroupByColorSize(orderName, null, null, null, 0);
        List<PostTreatment> postTreatments = postTreatmentService.getPostTreatmentByOrderName(orderName);
        List<PostTreatment> postTreatmentList = new ArrayList<>();
        for (OrderClothes orderClothes : orderClothesList){
            PostTreatment pt = new PostTreatment(orderClothes.getClothesVersionNumber(), orderClothes.getOrderName(), orderClothes.getColorName(), orderClothes.getSizeName(), orderClothes.getCount());
            for (CutQueryLeak cutQueryLeak : cutQueryLeakList){
                if (orderClothes.getColorName().equals(cutQueryLeak.getColorName()) && orderClothes.getSizeName().equals(cutQueryLeak.getSizeName())){
                    pt.setWellCount(cutQueryLeak.getLayerCount());
                }
            }
            for (PostTreatment postTreatment : postTreatments){
                if (orderClothes.getColorName().equals(postTreatment.getColorName()) && orderClothes.getSizeName().equals(postTreatment.getSizeName())){
                    pt.setWashCount(postTreatment.getWashCount());
                    pt.setShipCount(postTreatment.getShipCount());
                    pt.setSurplusCount(postTreatment.getSurplusCount());
                    pt.setSurplusLocation(postTreatment.getSurplusLocation());
                    pt.setDefectiveCount(postTreatment.getDefectiveCount());
                    pt.setDefectiveLocation(postTreatment.getDefectiveLocation());
                    pt.setChickenCount(postTreatment.getChickenCount());
                    pt.setChickenLocation(postTreatment.getChickenLocation());
                    pt.setDiffCount(postTreatment.getDiffCount());
                }
            }
            postTreatmentList.add(pt);
        }
        map.put("postTreatmentList", postTreatmentList);
        return map;
    }


    @RequestMapping(value = "/searchposttreatmentbyinfo", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> searchPostTreatmentByInfo(@RequestParam(value = "season", required = false)String season,
                                                         @RequestParam(value = "customerName", required = false)String customerName,
                                                         @RequestParam(value = "orderName", required = false)String orderName,
                                                         @RequestParam(value = "from", required = false)String from,
                                                         @RequestParam(value = "to", required = false)String to){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> orderNameList = new ArrayList<>();
        if (orderName != null){
            orderNameList.add(orderName);
        } else if (season != null || customerName != null){
            orderNameList = manufactureOrderService.getOrderNameByCustomerSeason(season, customerName);
        }
        List<Integer> procedureNumberList1 = new ArrayList<>();  // 查片
        List<Integer> procedureNumberList2 = new ArrayList<>();  // 挂片
        List<Integer> procedureNumberList3 = new ArrayList<>();  // 中查
        List<Integer> procedureNumberList4 = new ArrayList<>();  // 大烫
        List<Integer> procedureNumberList5 = new ArrayList<>();  // 尾查
        List<Integer> procedureNumberList6 = new ArrayList<>();  // 装箱
        procedureNumberList1.add(11);
        procedureNumberList2.add(1000);
        procedureNumberList3.add(5555);
        procedureNumberList4.add(5557);
        procedureNumberList5.add(5560);
        procedureNumberList6.add(5569);
        // 制单
        List<OrderClothes> orderClothesList = orderClothesService.getOrderCountByOrderNameList(orderNameList);
        // 好片
        List<Tailor> tailorList = postTreatmentService.getWellCountByOrderNameList(orderNameList, from , to);
        // 查片
        List<PieceWork> pieceWorkList1 = postTreatmentService.getPieceWorkSummaryByOrderNameList(orderNameList, procedureNumberList1, from, to);
        // 挂片
        List<PieceWork> pieceWorkList2 = postTreatmentService.getPieceWorkSummaryByOrderNameList(orderNameList, procedureNumberList2, from, to);
        // 中查
        List<PieceWork> pieceWorkList3 = postTreatmentService.getPieceWorkSummaryByOrderNameList(orderNameList, procedureNumberList3, from, to);
        // 大烫
        List<PieceWork> pieceWorkList4 = postTreatmentService.getPieceWorkSummaryByOrderNameList(orderNameList, procedureNumberList4, from, to);
        // 尾查
        List<PieceWork> pieceWorkList5 = postTreatmentService.getPieceWorkSummaryByOrderNameList(orderNameList, procedureNumberList5, from, to);
        // 装箱
        List<PieceWork> pieceWorkList6 = postTreatmentService.getPieceWorkSummaryByOrderNameList(orderNameList, procedureNumberList6, from, to);
        // 各种手输尾数
        List<PostTreatment> postTreatmentList = postTreatmentService.getPostTreatmentByOrderNameList(orderNameList, from, to);
        List<PostTreatment> destTreatmentList = new ArrayList<>();
        for (OrderClothes orderClothes : orderClothesList){
            PostTreatment postTreatment = new PostTreatment(orderClothes.getClothesVersionNumber(), orderClothes.getOrderName(), orderClothes.getOrderSum());
            for (Tailor tailor : tailorList){
                if (tailor.getOrderName().equals(orderClothes.getOrderName())){
                    postTreatment.setWellCount(tailor.getLayerCount());
                }
            }
            for (PieceWork pieceWork1 : pieceWorkList1){
                if (pieceWork1.getOrderName().equals(orderClothes.getOrderName())){
                    postTreatment.setCheckCount(pieceWork1.getLayerCount());
                }
            }
            for (PieceWork pieceWork2 : pieceWorkList2){
                if (pieceWork2.getOrderName().equals(orderClothes.getOrderName())){
                    postTreatment.setHangCount(pieceWork2.getLayerCount());
                }
            }
            for (PieceWork pieceWork3 : pieceWorkList3){
                if (pieceWork3.getOrderName().equals(orderClothes.getOrderName())){
                    postTreatment.setInspectionCount(pieceWork3.getLayerCount());
                }
            }
            for (PieceWork pieceWork4 : pieceWorkList4){
                if (pieceWork4.getOrderName().equals(orderClothes.getOrderName())){
                    postTreatment.setIronCount(pieceWork4.getLayerCount());
                }
            }
            for (PieceWork pieceWork5 : pieceWorkList5){
                if (pieceWork5.getOrderName().equals(orderClothes.getOrderName())){
                    postTreatment.setTailCheckCount(pieceWork5.getLayerCount());
                }
            }
            for (PieceWork pieceWork6 : pieceWorkList6){
                if (pieceWork6.getOrderName().equals(orderClothes.getOrderName())){
                    postTreatment.setPackageCount(pieceWork6.getLayerCount());
                }
            }
            for (PostTreatment postTreatment1 : postTreatmentList){
                if (postTreatment1.getOrderName().equals(orderClothes.getOrderName())){
                    postTreatment.setWashCount(postTreatment1.getWashCount());
                    postTreatment.setShipCount(postTreatment1.getShipCount());
                    postTreatment.setSurplusCount(postTreatment1.getSurplusCount());
                    postTreatment.setDefectiveCount(postTreatment1.getDefectiveCount());
                    postTreatment.setChickenCount(postTreatment1.getChickenCount());
                    postTreatment.setSampleCount(postTreatment1.getSampleCount());
                }
            }
            postTreatment.setDiffCount(postTreatment.getShipCount() + postTreatment.getSurplusCount() + postTreatment.getDefectiveCount() + postTreatment.getChickenCount() + postTreatment.getSampleCount() - postTreatment.getWellCount());
            destTreatmentList.add(postTreatment);
        }
        map.put("postTreatmentList", destTreatmentList);
        return map;
    }


    @RequestMapping(value = "/saveposttreatmentdatabatch", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> savePostTreatmentDataBatch(@RequestParam("postTreatmentJson")String postTreatmentJson){
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        List<PostTreatment> postTreatmentList = gson.fromJson(postTreatmentJson,new TypeToken<List<PostTreatment>>(){}.getType());
        int res = postTreatmentService.addPostTreatmentBatch(postTreatmentList);
        map.put("data", res);
        return map;
    }

    @RequestMapping(value = "/getposttreatmentbyordertype", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getPostTreatmentByOrderType(@RequestParam("orderName")String orderName,
                                                           @RequestParam(value = "washCount", required = false)Integer washCount,
                                                           @RequestParam(value = "shipCount", required = false)Integer shipCount,
                                                           @RequestParam(value = "surplusCount", required = false)Integer surplusCount,
                                                           @RequestParam(value = "defectiveCount", required = false)Integer defectiveCount,
                                                           @RequestParam(value = "chickenCount", required = false)Integer chickenCount,
                                                           @RequestParam(value = "sampleCount", required = false)Integer sampleCount){
        Map<String, Object> map = new LinkedHashMap<>();
        List<PostTreatment> postTreatmentList = postTreatmentService.getPostTreatmentByOrderType(orderName, washCount, shipCount, surplusCount, defectiveCount, chickenCount, sampleCount);
        map.put("data", postTreatmentList);
        map.put("count", postTreatmentList.size());
        map.put("code",0);
        return map;
    }

    @RequestMapping(value = "/deleteposttreatmentbyid", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> deletePostTreatmentById(@RequestParam("id")Integer id){
        Map<String, Object> map = new LinkedHashMap<>();
        int res = postTreatmentService.deletePostTreatmentById(id);
        map.put("data", res);
        return map;
    }

    @RequestMapping(value = "/updateposttreatment", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> updatePostTreatment(@RequestParam("postTreatmentJson")String postTreatmentJson){
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        PostTreatment postTreatment = gson.fromJson(postTreatmentJson, PostTreatment.class);
        int res = postTreatmentService.updatePostTreatment(postTreatment);
        map.put("data", res);
        return map;
    }

}
