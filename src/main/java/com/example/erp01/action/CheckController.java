package com.example.erp01.action;

import com.example.erp01.model.AttendLog;
import com.example.erp01.model.DailyCheck;
import com.example.erp01.model.Employee;
import com.example.erp01.model.SummaryCheck;
import com.example.erp01.service.CheckService;
import com.example.erp01.service.EmployeeService;
import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.io.IOException;
import java.math.BigInteger;
import java.net.URI;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.*;

@Configuration      //1.主要用于标记配置类，兼备Component的效果。
@EnableScheduling   // 2.开启定时任务
public class CheckController {

    @Autowired
    private CheckService checkService;
    @Autowired
    private EmployeeService employeeService;

//    @Scheduled(cron = "0 0 0/1 * * ? ")
//    public void syncCheckLog() throws Exception{
//        Map<String, Object> map = getCheckDetailFromCheckFirst();
//        int totalPage = (int)map.get("totalpage");
//        System.out.println("totalPage:" + totalPage);
//        List<AttendLog> attendLogList = (List<AttendLog>) map.get("attendLogList");
//        for (AttendLog attendLog : attendLogList){
//            attendLog.setAtten_datetime(new Date(attendLog.getAtten_time() * 1000L));
//            if (attendLog.getRealname() == null){
//                attendLog.setRealname("");
//            } else {
//                attendLog.setRealname(unicodeToCn(attendLog.getRealname()));
//            }
//        }
//        checkService.addAttendLogBatch(attendLogList);
//        if (totalPage > 1){
//            for (int i = 2; i < totalPage + 1; i ++){
//                Thread.sleep(21000);
//                List<AttendLog> attendLogList1 = getCheckDetailFromCheck(i);
//                for (AttendLog attendLog1 : attendLogList1){
//                    attendLog1.setAtten_datetime(timeStampToDate(attendLog1.getAtten_time()));
//                    attendLog1.setRealname(unicodeToCn(attendLog1.getRealname()));
//                }
//                checkService.addAttendLogBatch(attendLogList1);
//            }
//        }
//        System.out.println("考勤数据同步完成");
//    }


//    @Scheduled(cron = "0 0 5 * * ? ")
//    public void syncDayCheckReport() throws Exception{
//        List<Employee> employeeList = employeeService.getCheckEmployee();
//        if (employeeList != null && !employeeList.isEmpty()){
//            for (Employee employee : employeeList){
//                List<DailyCheck> dailyCheckList = getDailyCheckByUserAccount(employee.getCcCount());
//                if (dailyCheckList != null && !dailyCheckList.isEmpty()){
//                    for (DailyCheck dailyCheck : dailyCheckList){
//                        dailyCheck.setEmployeeName(employee.getEmployeeName());
//                        dailyCheck.setEmployeeNumber(employee.getEmployeeNumber());
//                        dailyCheck.setGroupName(employee.getGroupName());
//                    }
//                    checkService.addDailyCheckBatch(dailyCheckList);
//                }
//                Thread.sleep(21000);
//            }
//        }
//        System.out.println("考勤日报同步完成");
//    }


//    @Scheduled(cron = "0 0 5 * * ? ")
//    public void syncSummaryCheckReport() throws Exception{
//        List<Employee> employeeList = employeeService.getCheckEmployee();
//        if (employeeList != null && !employeeList.isEmpty()){
//            for (Employee employee : employeeList){
//                List<SummaryCheck> summaryCheckList = getSummaryCheckByUserAccount(employee.getCcCount());
//                if (summaryCheckList != null && !summaryCheckList.isEmpty()){
//                    for (SummaryCheck summaryCheck : summaryCheckList){
//                        String departmentName = "";
//                        String groupName = "";
//                        if (employee.getDepartment() != null){
//                            departmentName = employee.getDepartment();
//                            groupName = employee.getDepartment();
//                        }
//                        if (employee.getGroupName() != null){
//                            groupName = employee.getGroupName();
//                        }
//                        summaryCheck.setEmployeeName(employee.getEmployeeName());
//                        summaryCheck.setEmployeeNumber(employee.getEmployeeNumber());
//                        summaryCheck.setGroupName(groupName);
//                        summaryCheck.setDepartmentName(departmentName);
//                        summaryCheck.setMonthstring(getMonthBegin().substring(0,7));
//                        System.out.println(getMonthBegin().substring(0,7));
//                    }
//                    checkService.addSummaryCheckBatch(summaryCheckList);
//                }
//                Thread.sleep(21000);
//            }
//        }
//        System.out.println("考勤汇总同步完成");
//    }

    //第一页打卡记录
    public Map<String, Object> getCheckDetailFromCheckFirst() {
        CloseableHttpClient httpclient = HttpClients.createDefault();
        String resultString = "";
        int totalPage = 0;
        Map<String, Object> map = new HashMap<>();
        List<AttendLog> attendLogList = new ArrayList<>();
        CloseableHttpResponse response = null;
        try {
            // 创建uri
            String url = "http://yun.kqapi.com/Api/Api/recordlog";
            URIBuilder builder = new URIBuilder(url);
            Long currentTime = new Date().getTime();
            Map<String, String> param = new LinkedHashMap<>();
            param.put("account", "b8fe2958b0aec8eb5c70b039832d989a");
            param.put("requesttime", currentTime.toString());
            String planText = param.get("account") + param.get("requesttime") + "andy171fr";
            String md5code = encryption(planText);
            param.put("sign", md5code);
            if (param != null) {
                for (String key : param.keySet()) {
                    builder.addParameter(key, param.get(key));
                }
            }
            URI uri = builder.build();
            // 创建http GET请求
            HttpGet httpGet = new HttpGet(uri);
            // 执行请求
            response = httpclient.execute(httpGet);
            // 判断返回状态是否为200
            if (response.getStatusLine().getStatusCode() == 200) {
                resultString = EntityUtils.toString(response.getEntity(), "UTF-8");
            }
            System.out.println(resultString);
            JsonParser jsonParser = new JsonParser();
            JsonObject json = (JsonObject) jsonParser.parse(resultString);
            String status = json.get("status").getAsString();
            if (status.equals("1") && json.get("data") instanceof JsonObject){
                JsonObject dataJson = json.get("data").getAsJsonObject();
                totalPage = dataJson.get("totalpage").getAsInt();
                String attendatajson = dataJson.get("attendata").getAsJsonArray().toString();
                Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
                attendLogList = gson.fromJson(attendatajson,new TypeToken<List<AttendLog>>(){}.getType());
                map.put("totalpage", totalPage);
                map.put("attendLogList", attendLogList);
            } else {
                map.put("totalpage", totalPage);
                map.put("attendLogList", attendLogList);
            }
            return map;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (response != null) {
                    response.close();
                }
                httpclient.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        map.put("totalpage", totalPage);
        map.put("attendLogList", attendLogList);
        return map;
    }

    //第n页打卡记录
    public List<AttendLog> getCheckDetailFromCheck(int page) {
        CloseableHttpClient httpclient = HttpClients.createDefault();
        String resultString = "";
        List<AttendLog> attendLogList = new ArrayList<>();
        CloseableHttpResponse response = null;
        try {
            // 创建uri
            String url = "http://yun.kqapi.com/Api/Api/recordlog";
            URIBuilder builder = new URIBuilder(url);
            Long currentTime = new Date().getTime();
            Map<String, String> param = new LinkedHashMap<>();
            param.put("account", "b8fe2958b0aec8eb5c70b039832d989a");
            param.put("requesttime", currentTime.toString());
            param.put("page", String.valueOf(page));
            String planText = param.get("account") + param.get("page") + param.get("requesttime") + "andy171fr";
            String md5code = encryption(planText);
            param.put("sign", md5code);
            if (param != null) {
                for (String key : param.keySet()) {
                    builder.addParameter(key, param.get(key));
                }
            }
            URI uri = builder.build();
            // 创建http GET请求
            HttpGet httpGet = new HttpGet(uri);
            // 执行请求
            response = httpclient.execute(httpGet);
            // 判断返回状态是否为200
            if (response.getStatusLine().getStatusCode() == 200) {
                resultString = EntityUtils.toString(response.getEntity(), "UTF-8");
            }
            JsonParser jsonParser = new JsonParser();
            JsonObject json = (JsonObject) jsonParser.parse(resultString);
            String status = json.get("status").getAsString();
            if (status.equals("1")){
                JsonObject dataJson = json.get("data").getAsJsonObject();
                String attendatajson = dataJson.get("attendata").getAsJsonArray().toString();
                Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
                attendLogList = gson.fromJson(attendatajson,new TypeToken<List<AttendLog>>(){}.getType());
                return attendLogList;
            }
            return attendLogList;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (response != null) {
                    response.close();
                }
                httpclient.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return attendLogList;
    }

    public List<DailyCheck> getDailyCheckByUserAccount(String userAccount) {
        CloseableHttpClient httpclient = HttpClients.createDefault();
        String resultString = "";
        CloseableHttpResponse response = null;
        List<DailyCheck> dailyCheckList = new ArrayList<>();
        try {
            // 创建uri
            String url = "http://yun.kqapi.com/Api/Api/dayReport";
            URIBuilder builder = new URIBuilder(url);
            Long currentTime = new Date().getTime();
            Map<String, String> param = new LinkedHashMap<>();
            param.put("account", "b8fe2958b0aec8eb5c70b039832d989a");
            param.put("end", getSomeDay(0));
            param.put("requesttime", currentTime.toString());
            param.put("start", getSomeDay(-10));
            param.put("useraccount", userAccount);
            String planText = param.get("account") + param.get("end") + param.get("requesttime") + param.get("start") + param.get("useraccount") + "andy171fr";
            String md5code = encryption(planText);
            param.put("sign", md5code);
            if (param != null) {
                for (String key : param.keySet()) {
                    builder.addParameter(key, param.get(key));
                }
            }
            URI uri = builder.build();
            // 创建http GET请求
            HttpGet httpGet = new HttpGet(uri);
            // 执行请求
            response = httpclient.execute(httpGet);
            // 判断返回状态是否为200
            if (response.getStatusLine().getStatusCode() == 200) {
                resultString = EntityUtils.toString(response.getEntity(), "UTF-8");
            }
            System.out.println(resultString);
            JsonParser jsonParser = new JsonParser();
            JsonObject json = (JsonObject) jsonParser.parse(resultString);
            String status = json.get("status").getAsString();
            if (status.equals("1") && json.get("data") instanceof JsonArray){
                JsonArray dataJson = json.get("data").getAsJsonArray();
                Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
                dailyCheckList = gson.fromJson(dataJson,new TypeToken<List<DailyCheck>>(){}.getType());
            }
            return dailyCheckList;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (response != null) {
                    response.close();
                }
                httpclient.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return dailyCheckList;
    }

    public List<SummaryCheck> getSummaryCheckByUserAccount(String userAccount) {
        CloseableHttpClient httpclient = HttpClients.createDefault();
        String resultString = "";
        CloseableHttpResponse response = null;
        List<SummaryCheck> summaryCheckList = new ArrayList<>();
        try {
            // 创建uri
            String url = "http://yun.kqapi.com/Api/Api/Report";
            URIBuilder builder = new URIBuilder(url);
            Long currentTime = new Date().getTime();
            Map<String, String> param = new LinkedHashMap<>();
            param.put("account", "b8fe2958b0aec8eb5c70b039832d989a");
            param.put("end", getMonthEnd());
            param.put("requesttime", getMonthBegin());
            param.put("start", getSomeDay(-20));
            param.put("useraccount", userAccount);
            String planText = param.get("account") + param.get("end") + param.get("requesttime") + param.get("start") + param.get("useraccount") + "andy171fr";
            String md5code = encryption(planText);
            param.put("sign", md5code);
            if (param != null) {
                for (String key : param.keySet()) {
                    builder.addParameter(key, param.get(key));
                }
            }
            URI uri = builder.build();
            // 创建http GET请求
            HttpGet httpGet = new HttpGet(uri);
            // 执行请求
            response = httpclient.execute(httpGet);
            // 判断返回状态是否为200
            if (response.getStatusLine().getStatusCode() == 200) {
                resultString = EntityUtils.toString(response.getEntity(), "UTF-8");
            }
            JsonParser jsonParser = new JsonParser();
            JsonObject json = (JsonObject) jsonParser.parse(resultString);
            String status = json.get("status").getAsString();
            if (status.equals("1") && json.get("data") instanceof JsonObject){
                JsonObject dataObject = json.get("data").getAsJsonObject();
                if (dataObject.get("data") instanceof  JsonArray){
                    JsonArray dataJson = dataObject.get("data").getAsJsonArray();
                    Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
                    summaryCheckList = gson.fromJson(dataJson,new TypeToken<List<SummaryCheck>>(){}.getType());
                }

            }
            return summaryCheckList;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (response != null) {
                    response.close();
                }
                httpclient.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return summaryCheckList;
    }

    public String encryption(String plainText) {
        String hashtext = new String();
        try {
            MessageDigest m = MessageDigest.getInstance("MD5");
            m.reset();
            m.update(plainText.getBytes());
            byte[] digest = m.digest();
            BigInteger bigInt = new BigInteger(1,digest);
            hashtext = bigInt.toString(16);
            while(hashtext.length() < 32 ){
                hashtext = "0" + hashtext;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return hashtext;
    }

    //Unicode转中文方法
    public static String unicodeToCn(String unicode) {
        if (unicode == null){
            return "";
        }
        /** 以 \ u 分割，因为java注释也能识别unicode，因此中间加了一个空格*/
        String[] strs = unicode.split("\\\\u");
        String returnStr = "";
        // 由于unicode字符串以 \ u 开头，因此分割出的第一个字符是""。
        for (int i = 1; i < strs.length; i++) {
            returnStr += (char) Integer.valueOf(strs[i], 16).intValue();
        }
        return returnStr;
    }


    public static Date timeStampToDate(Long timeStamp){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        new Date(timeStamp *1000L);
        return new Date(Long.parseLong(String.valueOf(timeStamp)));
    }


    public static String getSomeDay(int diffDay){
        Date date=new Date();//取时间
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        //整数往后推,负数往前移动
        calendar.add(Calendar.DATE, diffDay);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(calendar.getTime());
    }

    public static String getMonthBegin(){
        // 获取当月第一天和最后一天
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String firstday;
        // 获取当月第一天和最后一天
        Calendar cale = Calendar.getInstance();
        cale.add(Calendar.MONTH, 0);
        cale.set(Calendar.DAY_OF_MONTH, 1);
        firstday = format.format(cale.getTime());
        return firstday;

    }

    public static String getMonthEnd(){
        // 获取当月第一天和最后一天
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String lastday;
        // 获取前月的最后一天
        Calendar cale = Calendar.getInstance();
        lastday = format.format(cale.getTime());
        return lastday;
    }

}


