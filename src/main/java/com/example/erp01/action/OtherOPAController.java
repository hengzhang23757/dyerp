package com.example.erp01.action;

import com.example.erp01.model.OtherOPA;
import com.example.erp01.service.OtherOPAService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping(value = "erp")
public class OtherOPAController {

    @Autowired
    private OtherOPAService otherOPAService;


    @RequestMapping("/otherOpaStart")
    public String opaStart(Model model){
        model.addAttribute("bigMenuTag",17);
        model.addAttribute("menuTag",172);
        return "opa/otherOPA";
    }

    @RequestMapping(value = "/addotheropabatch", method = RequestMethod.POST)
    @ResponseBody
    public int addOpaBackInput(@RequestParam("otherOpaJson")String otherOpaJson){
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        List<OtherOPA> otherOpaList = gson.fromJson(otherOpaJson,new TypeToken<List<OtherOPA>>(){}.getType());
        return otherOPAService.addOtherOPABatch(otherOpaList);
    }

    @RequestMapping(value = "/deleteotheropa",method = RequestMethod.POST)
    @ResponseBody
    public int deleteOPA(@RequestParam("otherOpaID")Integer otherOpaID){
        return otherOPAService.deleteOtherOPA(otherOpaID);
    }

    @RequestMapping(value = "/gettodayotheropa", method = RequestMethod.GET)
    public String getTodayOtherOPA(Model model){
        List<OtherOPA> otherOpaList = otherOPAService.getTodayOtherOPA();
        model.addAttribute("otherOpaList",otherOpaList);
        return "opa/fb_otherOPAList";
    }

    @RequestMapping(value = "/getonemonthotheropa", method = RequestMethod.GET)
    public String getOneMonthOtherOPA(Model model){
        List<OtherOPA> otherOpaList = otherOPAService.getOneMonthOtherOPA();
        model.addAttribute("otherOpaList",otherOpaList);
        return "opa/fb_otherOPAList";
    }

    @RequestMapping(value = "/getthreemonthotheropa", method = RequestMethod.GET)
    public String getThreeMonthOtherOPA(Model model){
        List<OtherOPA> otherOpaList = otherOPAService.getThreeMonthOtherOPA();
        model.addAttribute("otherOpaList",otherOpaList);
        return "opa/fb_otherOPAList";
    }

}
