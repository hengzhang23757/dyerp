package com.example.erp01.action;

import com.example.erp01.model.*;
import com.example.erp01.service.*;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
@RequestMapping(value = "erp")
public class ReportController {

    @Autowired
    private ReportService reportService;
    @Autowired
    private OrderClothesService orderClothesService;
    @Autowired
    private OrderProcedureService orderProcedureService;
    @Autowired
    private TailorService tailorService;
    @Autowired
    private HangSalaryService hangSalaryService;
    @Autowired
    private PieceWorkService pieceWorkService;
    @Autowired
    private ProcedureTemplateService procedureTemplateService;
    @Autowired
    private HourEmpService hourEmpService;
    @Autowired
    private EmployeeService employeeService;
    @Autowired
    private EmbOutStoreService embOutStoreService;
    @Autowired
    private SpecialProcedureService specialProcedureService;


    @RequestMapping("/procedureBalanceStart")
    public String procedureBalanceStart(){
        return "report/procedureBalance";
    }

    @RequestMapping("/procedureBalanceNewStart")
    public String procedureBalanceNewStart(){
        return "productionReport/procedureBalance";
    }

    @RequestMapping("/empSalaryStart")
    public String empSalaryStart(Model model){
        model.addAttribute("bigMenuTag",11);
        model.addAttribute("menuTag",111);
        return "report/empSalary";
    }

    @RequestMapping("/pieceWorkDetailStart")
    public String pieceWorkDetailStart(){
        return "report/pieceWorkDetail";
    }

    @RequestMapping("/cutDailyDetailStart")
    public String cutDailyDetailStart(Model model){
        model.addAttribute("bigMenuTag",11);
        model.addAttribute("menuTag",113);
        return "report/cutDailyDetail";
    }

    @RequestMapping("/groupSalaryStart")
    public String groupSalaryStart(Model model){
        model.addAttribute("bigMenuTag",11);
        model.addAttribute("menuTag",112);
        return "report/groupSalary";
    }

    @RequestMapping("/getCutBedColorStart")
    public String getCutBedColorStart(Model model){
        model.addAttribute("bigMenuTag",4);
        model.addAttribute("menuTag",46);
        return "report/cutBedColor";
    }

    @RequestMapping("/procedureProgressStart")
    public String procedureProgressStart(Model model){
        model.addAttribute("bigMenuTag",9);
        model.addAttribute("menuTag",93);
        return "report/procedureProgress";
    }

    @RequestMapping("/orderSalarySummaryStart")
    public String orderSalarySummaryStart(Model model){
        model.addAttribute("bigMenuTag",11);
        model.addAttribute("menuTag",115);
        return "report/orderSalarySummary";
    }


    @RequestMapping(value = "/getproductionprogressbyordertimegroup", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getProductionProgressByOrderTimeGroup(@RequestParam("orderName") String orderName,
                                                                     @RequestParam("clothesVersionNumber") String clothesVersionNumber,
                                                                     @RequestParam("from") String from,
                                                                     @RequestParam("to")String to,
                                                                     @RequestParam("groupName")String groupName){
        if("".equals(groupName)){
            groupName = null;
        }
        if("".equals(from)){
            from = "1970-01-01";
        }
        if("".equals(to)){
            to = "2050-01-01";
        }
        Map<String, Object> map = new HashMap<>();
        try{
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date fromDate = sdf.parse(from);
            Date toDate = sdf.parse(to);
            List<OrderProcedure> orderProcedureList = orderProcedureService.getOrderProcedureByOrder(orderName);
            List<ProductionProgress> miniProductionList = reportService.getMiniProductionProgressByOrderGroup(orderName,fromDate,toDate,groupName);
            Integer orderCount = orderClothesService.getOrderTotalCount(orderName);
            Integer cutCount = tailorService.getWellCountByInfo(orderName, null,null,null,null, null, 0);
            Integer cutInStoreCount = reportService.getCutInStoreCount(orderName,fromDate,toDate);
            Integer embInStoreCount = reportService.getEmbInStoreCount(orderName,fromDate,toDate);
            Integer embOutStoreCount = reportService.getEmbOutStoreCount(orderName,fromDate,toDate);
            if (orderCount == null){
                orderCount = 0;
            }
            if (cutCount == null){
                cutCount = 0;
            }
            List<ProductionProgress> pgl = new ArrayList<>();
            pgl.add(new ProductionProgress(orderName, clothesVersionNumber, "0", 0, "入裁片","入裁片", orderCount, cutCount, cutInStoreCount, cutInStoreCount-cutCount));
            pgl.add(new ProductionProgress(orderName, clothesVersionNumber, "0", 0, "入衣胚","入衣胚", orderCount, cutCount, embInStoreCount, embInStoreCount-cutCount));
            pgl.add(new ProductionProgress(orderName, clothesVersionNumber, "0", 0, "下衣胚","下衣胚", orderCount, cutCount, embOutStoreCount, embOutStoreCount-cutCount));
            for (OrderProcedure orderProcedure : orderProcedureList){
                int thisOrderCount = 0;
                int thisCutCount = 0;
                if (orderProcedure.getColorName().equals("全部") && orderProcedure.getSizeName().equals("全部")){
                    thisOrderCount = orderCount;
                    thisCutCount = cutCount;
                } else if (orderProcedure.getColorName().equals("全部")){
                    List<String> sizeList = Arrays.asList(orderProcedure.getSizeName().split(","));
                    thisOrderCount = orderClothesService.getOrderCountByColorSizeList(orderName, null, sizeList);
                    thisCutCount = tailorService.getWellCountByColorSizeList(orderName, null, sizeList);
                } else if (orderProcedure.getSizeName().equals("全部")){
                    List<String> colorList = Arrays.asList(orderProcedure.getColorName().split(","));
                    thisOrderCount = orderClothesService.getOrderCountByColorSizeList(orderName, colorList, null);
                    thisCutCount = tailorService.getWellCountByColorSizeList(orderName, colorList, null);
                } else {
                    List<String> colorList = Arrays.asList(orderProcedure.getColorName().split(","));
                    List<String> sizeList = Arrays.asList(orderProcedure.getSizeName().split(","));
                    thisOrderCount = orderClothesService.getOrderCountByColorSizeList(orderName, colorList, sizeList);
                    thisCutCount = tailorService.getWellCountByColorSizeList(orderName, colorList, sizeList);
                }
                Integer procedureNumber = orderProcedure.getProcedureNumber();
                String tmpCode = orderProcedure.getProcedureCode().toString();
                int productionCount = 0;
                for (ProductionProgress miniProduction : miniProductionList){
                    if (procedureNumber.equals(miniProduction.getProcedureNumber())){
                        productionCount += miniProduction.getProductionCount();
                    }
                }
                ProductionProgress pp = new ProductionProgress(orderName, clothesVersionNumber, tmpCode, procedureNumber, orderProcedure.getProcedureName(), orderProcedure.getProcedureDescription(), thisOrderCount, thisCutCount, productionCount, productionCount-thisCutCount);
                pgl.add(pp);
            }
            map.put("productionProgressList",pgl);
            List<PersonProductionDetail> miniPersonProductionDetailList = reportService.getMiniEachPersonProductionOfProcedure(orderName,fromDate,toDate,groupName);
            Map<String, Object> personMap = new LinkedHashMap<>();
            for (OrderProcedure orderProcedure : orderProcedureList){
                String procedureString = orderProcedure.getProcedureName();
                boolean isPerson = false;
                Map<String, Object> procedureMap = new LinkedHashMap<>();
                for (PersonProductionDetail ppd2:miniPersonProductionDetailList){
                    if (ppd2.getProcedureNumber() != null){
                        if (ppd2.getProcedureNumber().equals(orderProcedure.getProcedureNumber())){
                            String personString = ppd2.getEmployeeNumber() + "-" + ppd2.getEmployeeName();
                            if (ppd2.getProductionCount() != null){
                                isPerson = true;
                                procedureMap.put(personString,ppd2.getProductionCount());
                            }
                        }
                    }
                }
                if (isPerson){
                    personMap.put(procedureString, procedureMap);
                }
            }
            map.put("person", personMap);
            return map;
        }catch (ParseException e){
            e.printStackTrace();
        }
        return map;
    }

    @RequestMapping(value = "/minigetproductionprogressbyordertimegroup", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> miniGetProductionProgressByOrderTimeGroup(@RequestParam("orderName") String orderName,
                                                                         @RequestParam("from") String from,
                                                                         @RequestParam("to")String to,
                                                                         @RequestParam("groupName")String groupName){
        if("".equals(groupName)){
            groupName = null;
        }
        if("".equals(from)){
            from = "1970-01-01";
        }
        if("".equals(to)){
            to = "2050-01-01";
        }
        Map<String, Object> map = new HashMap<>();
        try{
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date fromDate = sdf.parse(from);
            Date toDate = sdf.parse(to);
            String clothesVersionNumber = orderClothesService.getVersionNumberByOrderName(orderName);
            List<ProcedureInfo> procedureInfoList = orderProcedureService.getProcedureInfoByOrderName(orderName);
            List<ProductionProgress> miniProductionList = reportService.getMiniProductionProgressByOrderGroup(orderName,fromDate,toDate,groupName);
            Integer orderCount = orderClothesService.getOrderTotalCount(orderName);
            Integer cutCount = tailorService.getWellCountByInfo(orderName, null,null,null,null, null,0);
            Integer cutInStoreCount = reportService.getCutInStoreCount(orderName,fromDate,toDate);
            Integer embInStoreCount = reportService.getEmbInStoreCount(orderName,fromDate,toDate);
            Integer embOutStoreCount = reportService.getEmbOutStoreCount(orderName,fromDate,toDate);
            if (orderCount == null){
                orderCount = 0;
            }
            if (cutCount == null){
                cutCount = 0;
            }
            List<ProductionProgress> pgl = new ArrayList<>();
            pgl.add(new ProductionProgress(orderName, clothesVersionNumber, "0", 0, "入裁片","入裁片", orderCount, cutCount, cutInStoreCount, cutInStoreCount-cutCount));
            pgl.add(new ProductionProgress(orderName, clothesVersionNumber, "0", 0, "入衣胚","入衣胚", orderCount, cutCount, embInStoreCount, embInStoreCount-cutCount));
            pgl.add(new ProductionProgress(orderName, clothesVersionNumber, "0", 0, "下衣胚","下衣胚", orderCount, cutCount, embOutStoreCount, embOutStoreCount-cutCount));
            for (ProcedureInfo procedureInfo : procedureInfoList){
                Integer procedureNumber = procedureInfo.getProcedureNumber();
                String tmpCode = procedureInfo.getProcedureCode().toString();
                int productionCount = 0;
                if (procedureNumber == 1){
                    productionCount = cutCount;
                }
                for (ProductionProgress miniProduction : miniProductionList){
                    if (procedureNumber.equals(miniProduction.getProcedureNumber())){
                        productionCount += miniProduction.getProductionCount();
                    }
                }
                ProductionProgress pp = new ProductionProgress(orderName, clothesVersionNumber, tmpCode, procedureNumber, procedureInfo.getProcedureName(), procedureInfo.getProcedureDescription(), orderCount, cutCount, productionCount, productionCount-cutCount);
                pgl.add(pp);
            }
            map.put("productionProgressList",pgl);
            return map;
        }catch (ParseException e){
            e.printStackTrace();
        }
        return map;
    }


    @RequestMapping(value = "/minigetproductionprogressbyorder", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getProductionProgressByOrderTimeGroup(@RequestParam("orderName") String orderName){
        Map<String, Object> map = new HashMap<>();
        List<ProcedureInfo> procedureInfoList = orderProcedureService.getProcedureInfoByOrderName(orderName);
        List<MiniProgress> miniProgressList = reportService.getProductionProgressByOrder(orderName);
        Integer orderCount = orderClothesService.getOrderTotalCount(orderName);
        Integer cutCount = tailorService.getWellCountByInfo(orderName, null,null,null,null,null,0);
        if (orderCount == null){
            orderCount = 0;
        }
        if (cutCount == null){
            cutCount = 0;
        }
        List<MiniProgress> pgl = new ArrayList<>();
        for (ProcedureInfo procedureInfo : procedureInfoList){
            Integer tmpNumber = procedureInfo.getProcedureNumber();
            boolean flag = true;
            for (MiniProgress miniProgress : miniProgressList){
                if (tmpNumber.equals(miniProgress.getProcedureNumber())){
                    MiniProgress mp = new MiniProgress(orderName, tmpNumber,procedureInfo.getProcedureName(), orderCount, cutCount, miniProgress.getProductionCount(), miniProgress.getProductionCount()-cutCount);
                    pgl.add(mp);
                    flag = false;
                }
            }
            if(flag){
                MiniProgress mp = new MiniProgress(orderName, tmpNumber, procedureInfo.getProcedureName(), orderCount, cutCount, 0, 0-cutCount);
                pgl.add(mp);
            }
        }
        map.put("miniProductionList",pgl);
        return map;
    }

    @RequestMapping(value = "/minigetproductionprogressbyordergroup", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getProductionProgressByOrderTimeGroup(@RequestParam("orderName") String orderName,
                                                                     @RequestParam("groupName") String groupName){
        Map<String, Object> map = new HashMap<>();
        if ("".equals(groupName)){
            groupName = null;
        }
        List<ProcedureInfo> procedureInfoList = orderProcedureService.getProcedureInfoByOrderName(orderName);
        List<MiniProgress> miniProgressList = reportService.getProductionProgressByOrderGroup(orderName, groupName);
        Integer orderCount = orderClothesService.getOrderTotalCount(orderName);
        Integer cutCount = tailorService.getWellCountByInfo(orderName, null,null,null,null,null, 0);
        if (orderCount == null){
            orderCount = 0;
        }
        if (cutCount == null){
            cutCount = 0;
        }
        List<MiniProgress> pgl = new ArrayList<>();
        for (ProcedureInfo procedureInfo : procedureInfoList){
            Integer tmpNumber = procedureInfo.getProcedureNumber();
            boolean flag = true;
            for (MiniProgress miniProgress : miniProgressList){
                if (tmpNumber.equals(miniProgress.getProcedureNumber())){
                    MiniProgress mp = new MiniProgress(orderName, tmpNumber,procedureInfo.getProcedureName(), orderCount, cutCount, miniProgress.getProductionCount(), miniProgress.getProductionCount()-cutCount);
                    pgl.add(mp);
                    flag = false;
                }
            }
            if(flag){
                MiniProgress mp = new MiniProgress(orderName, tmpNumber, procedureInfo.getProcedureName(), orderCount, cutCount, 0, 0-cutCount);
                pgl.add(mp);
            }
        }
        map.put("miniProductionList",pgl);
        return map;
    }

    @RequestMapping(value = "/getsalarybytimeemp", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getSalaryByTimeEmp(@RequestParam("from") String from,
                                                  @RequestParam("to") String to,
                                                  @RequestParam("employeeNumber")String employeeNumber){
        Map<String, Object> map = new HashMap<>();
        try{
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date fromDate = sdf.parse(from);
            Date toDate = sdf.parse(to);
            List<GeneralSalary> generalSalaryList = pieceWorkService.getMiniSalaryByTimeEmp(fromDate, toDate, employeeNumber);
            List<GeneralSalary> generalSalaryList1 = hourEmpService.getHourEmpByTimeEmp(fromDate,toDate,employeeNumber);
            if (generalSalaryList != null && generalSalaryList.size() > 0){
                for (GeneralSalary generalSalary : generalSalaryList){
                    String tmpOrderName = generalSalary.getOrderName();
                    Integer procedureNumber = generalSalary.getProcedureNumber();
                    Float miniPrice = 0f;
                    Float miniPriceTwo = 0f;
                    ProcedureInfo pi = orderProcedureService.getProcedureInfoByOrderProcedureNumber(tmpOrderName, procedureNumber);
                    if (pi != null){
                        if (pi.getProcedureState().equals(2)){
                            if (pi.getPiecePrice() != null){
                                miniPrice = pi.getPiecePrice() + pi.getPiecePriceTwo();
                            }
                            if (pi.getProcedureSection() != null && pi.getSubsidy() != null){
                                if (pi.getProcedureSection().equals("车缝")){
                                    miniPriceTwo = miniPrice*pi.getSubsidy();
                                }
                            }
                        }
                    }
                    generalSalary.setSalaryType("计件");
                    generalSalary.setPrice(miniPrice);
                    generalSalary.setSalary(miniPrice*generalSalary.getPieceCount());
                    generalSalary.setPriceTwo(miniPriceTwo);
                    generalSalary.setSalaryTwo(miniPriceTwo*generalSalary.getPieceCount());
                }
            }
            if (generalSalaryList1 != null && generalSalaryList1.size() > 0){
                for (GeneralSalary hourEmp : generalSalaryList1){
                    String tmpOrderName = hourEmp.getOrderName();
                    Float hourPrice = 0f;
                    ProcedureInfo pi = orderProcedureService.getProcedureInfoByOrderProcedureNumber(tmpOrderName, 11111);
                    if (pi != null){
                        if (pi.getPiecePrice() != null){
                            hourPrice = pi.getPiecePrice() + pi.getPiecePriceTwo();
                        }
                    }
                    hourEmp.setProcedureName("时工");
                    hourEmp.setSalaryType("计时");
                    hourEmp.setPrice(hourPrice);
                    hourEmp.setSalary(hourPrice*hourEmp.getPieceCount());
                }
                generalSalaryList.addAll(generalSalaryList1);
            }

            if (generalSalaryList != null && !generalSalaryList.isEmpty()){
                map.put("salaryList",generalSalaryList);
            }
            return map;
        }catch (ParseException e){
            e.printStackTrace();
        }
        return map;
    }

    @RequestMapping(value = "/getcutdailydetailbyordertime", method = RequestMethod.GET)
    @ResponseBody
    public Map<Date,Object> getCutDailyDetailByOrderTime(@RequestParam("from") String from,
                                                         @RequestParam("to") String to,
                                                         @RequestParam("orderName")String orderName){
        if("".equals(from)){
            from = "1970-01-01";
        }
        if("".equals(to)){
            to = "2050-01-01";
        }
        Map<Date, Object> map = new LinkedHashMap<>();
        try{
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date fromDate = sdf.parse(from);
            Date toDate = sdf.parse(to);
            List<CutDaily> cutDailyList = reportService.getCutDailyDetailByOrderTime(orderName, fromDate, toDate);
            List<String> colorList = new ArrayList<>();
            List<String> sizeList = new ArrayList<>();
            List<Date> dateList = new ArrayList<>();
            for (CutDaily cutDaily : cutDailyList){
                if (!colorList.contains(cutDaily.getColorName())){
                    colorList.add(cutDaily.getColorName());
                }
                if (!sizeList.contains(cutDaily.getSizeName())){
                    sizeList.add(cutDaily.getSizeName());
                }
                if (!dateList.contains(cutDaily.getDetailTime())){
                    dateList.add(cutDaily.getDetailTime());
                }
            }
            List<String> desiredOrder = Arrays.asList("XXXXXS","5XS","XXXXS","4XS","XXXS","3XS","XXS","2XS","XS","XS/25","xs","S","S/27","M","M/29","L","L/31","XL",
                    "XL/33","XXL","2XL","XXL/36","XXXL","3XL","XXXL/38","4XL","XXXXL","5XL","XXXXXL","6XL","XXXXXXL","80","090","90","100","110","110/27",
                    "120","120/29","130","130/31","140","140/33","150","150/36","155","160","165","170","175","180","185","190");
            Comparator<String> sizeOrder = Comparator.comparingInt(desiredOrder::indexOf);
            sizeList.sort(sizeOrder);
            for (Date date : dateList){
                Map<String, Object> colorMap = new LinkedHashMap<>();
                for (String color : colorList){
                    Map<String, Object> sizeMap = new LinkedHashMap<>();
                    int countSize = 0;
                    for (String size : sizeList){
                        boolean sizeFlag = true;
                        for (CutDaily cutDaily : cutDailyList){
                            if (date.equals(cutDaily.getDetailTime()) && color.equals(cutDaily.getColorName()) && size.equals(cutDaily.getSizeName())){
                                sizeMap.put(size,cutDaily.getPieceCount());
                                sizeFlag = false;
                                countSize ++;
                            }
                            if (sizeFlag) {
                                sizeMap.put(size,0);
                            }
                        }
                    }
                    if (countSize > 0){
                        colorMap.put(color, sizeMap);
                    }
                }
                map.put(date, colorMap);
            }
            return map;
        }catch (ParseException e){
            e.printStackTrace();
        }
        return null;
    }


    @RequestMapping(value = "/getgroupsalarybytime", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getGroupSalaryByTime(@RequestParam("from") String from,
                                                    @RequestParam("to") String to,
                                                    @RequestParam("groupName")String groupName,
                                                    @RequestParam("salaryType")String salaryType){
        Map<String, Object> map = new LinkedHashMap<>();
        if ("".equals(groupName)){
            groupName = null;
        }
        try{
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date fromDate = sdf.parse(from);
            Date toDate = sdf.parse(to);
            if (salaryType.equals("计时")){
                List<GeneralSalary> generalSalaryList = hourEmpService.getGroupHourEmpByTime(fromDate, toDate, groupName);
                if (generalSalaryList != null && generalSalaryList.size() > 0){
                    Float hourPrice = 0f;
                    if (procedureTemplateService.getPiecePriceByProcedureCode(562) != null){
                        hourPrice = procedureTemplateService.getPiecePriceByProcedureCode(562);
                    }
                    for (GeneralSalary generalSalary : generalSalaryList){
                        String tmpOrderName = generalSalary.getOrderName();
                        generalSalary.setProcedureName("时工");
                        generalSalary.setSalaryType("计时");
                        generalSalary.setPrice(hourPrice);
                        generalSalary.setSalary(hourPrice*generalSalary.getPieceCount());
                    }
                }
                Collections.sort(generalSalaryList);
                map.put("type","计时");
                map.put("emp",generalSalaryList);
                return map;
            }else if (salaryType.equals("汇总")){
                List<GeneralSalary> generalSalaryList = reportService.getPieceWorkSummarySalaryByInfo(fromDate, toDate, groupName);
                if (generalSalaryList != null && generalSalaryList.size() > 0){
                    for (GeneralSalary generalSalary : generalSalaryList){
                        generalSalary.setSalaryType("计件");
                    }
                }
                List<GeneralSalary> hourGeneralList = hourEmpService.getGroupHourEmpByTime(fromDate, toDate, groupName);
                if (hourGeneralList != null && hourGeneralList.size() > 0){
                    Float hourPrice = 0f;
                    if (procedureTemplateService.getPiecePriceByProcedureCode(562) != null){
                        hourPrice = procedureTemplateService.getPiecePriceByProcedureCode(562);
                    }
                    for (GeneralSalary hourGeneral : hourGeneralList){
                        hourGeneral.setProcedureName("时工");
                        hourGeneral.setSalaryType("计时");
                        hourGeneral.setPrice(hourPrice);
                        hourGeneral.setSalary(hourPrice*hourGeneral.getPieceCount());
                    }
                }
                Collections.sort(generalSalaryList);
                Set<String> empSet = new HashSet<>();
                for (GeneralSalary generalSalary : generalSalaryList){
                    empSet.add(generalSalary.getEmployeeNumber());
                }
                for (GeneralSalary generalSalary2 : hourGeneralList){
                    empSet.add(generalSalary2.getEmployeeNumber());
                }
                List<SalarySummary> salarySummaryList = new ArrayList<>();
                List<Employee> employeeList = employeeService.getAllInStateEmployee();
                for (String empNumber : empSet){
                    String gn = "";
                    String employeeName = "通用";
                    String position = "通用";
                    Float pieceSalary = 0f;
                    Float pieceSalaryTwo = 0f;
                    Float hourSalary = 0f;
                    Float bonus = 0f;
                    for (Employee employee : employeeList){
                        if (empNumber.equals(employee.getEmployeeNumber())){
                            if (employee.getPosition() != null){
                                position = employee.getPosition();
                            }
                        }
                    }
                    for (GeneralSalary generalSalary : generalSalaryList){
                        if (empNumber.equals(generalSalary.getEmployeeNumber())){
                            employeeName = generalSalary.getEmployeeName();
                            gn = generalSalary.getGroupName();
                            pieceSalary += generalSalary.getSalary();
                            pieceSalaryTwo += generalSalary.getSalaryTwo();
                        }
                    }
                    for (GeneralSalary generalSalary2 : hourGeneralList){
                        if (empNumber.equals(generalSalary2.getEmployeeNumber())){
                            hourSalary += generalSalary2.getSalary();
                        }
                    }
                    if (!position.equals("装箱")){
                        if (pieceSalary + pieceSalaryTwo + hourSalary < 2000){
                            bonus = (pieceSalary + pieceSalaryTwo + hourSalary)*0.08f;
                        }else{
                            bonus = (pieceSalary + pieceSalaryTwo + hourSalary)*0.12f;
                        }
                    }
                    SalarySummary salarySummary = new SalarySummary(empNumber, employeeName, gn, position, pieceSalary, pieceSalaryTwo, hourSalary);
                    salarySummary.setBonus(bonus);
                    salarySummary.setSumSalary(pieceSalary + pieceSalaryTwo + hourSalary + bonus);
                    salarySummaryList.add(salarySummary);
                }
                map.put("type","汇总");
                map.put("emp",salarySummaryList);
                return map;
            }else {
                List<GeneralSalary> generalSalaryList = reportService.getPieceWorkGroupSalaryByInfo(fromDate, toDate, groupName);
                if (generalSalaryList != null && generalSalaryList.size() > 0){
                    for (GeneralSalary generalSalary : generalSalaryList){
                        generalSalary.setSalaryType("计件");
                    }
                }
                Collections.sort(generalSalaryList);
                map.put("type","计件");
                map.put("emp",generalSalaryList);
                return map;
            }
        }catch (ParseException e){
            e.printStackTrace();
        }
        return map;
    }

    @RequestMapping(value = "/getcutbedcolorbyorder", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getCutBedColorByOrder(@RequestParam("orderName")String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        Map<String, Object> colorMap = new LinkedHashMap<>();
        List<CutQueryLeak> orderInfoList = orderClothesService.getOrderInfoByName(orderName);
        List<CutBedColor> cutBedColorList = reportService.getCutBedColorByOrder(orderName, null, 0, null, null);
        List<CutQueryLeak> tailorInfoList = tailorService.getTailorInfoByOrderPartType(orderName, null, 0, null, null);
        List<String> colorList = new ArrayList<>();
        List<String> sizeList = new ArrayList<>();
        List<Integer> bedList = new ArrayList<>();
        List<String> bedTimeGroupList = new ArrayList<>();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        for (CutBedColor cutBedColor : cutBedColorList){
            if (!colorList.contains(cutBedColor.getColorName())){
                colorList.add(cutBedColor.getColorName());
            }
            if (!bedList.contains(cutBedColor.getBedNumber())){
                bedList.add(cutBedColor.getBedNumber());
                String cutTimeString = formatter.format(cutBedColor.getCutTime());
                String bedTimeGroupKey = cutBedColor.getBedNumber().toString()+"#"+cutTimeString+"#"+cutBedColor.getGroupName();
                bedTimeGroupList.add(bedTimeGroupKey);
            }
            if (!sizeList.contains(cutBedColor.getSizeName())){
                sizeList.add(cutBedColor.getSizeName());
            }
        }
        List<String> desiredOrder = Arrays.asList("XXXXXS","5XS","XXXXS","4XS","XXXS","3XS","XXS","2XS","XS","XS/25","xs","S","S/27","M","M/29","L","L/31","XL",
                "XL/33","XXL","2XL","XXL/36","XXXL","3XL","XXXL/38","4XL","XXXXL","5XL","XXXXXL","6XL","XXXXXXL","80","090","90","100","110","110/27",
                "120","120/29","130","130/31","140","140/33","150","150/36","155","160","165","170","175","180","185","190");
        Comparator<String> sizeOrder = Comparator.comparingInt(desiredOrder::indexOf);
        sizeList.sort(sizeOrder);
        Map<String, Object> orderColorMap = new LinkedHashMap<>();
        for (String color : colorList){
            Map<String, Object> orderSizeMap = new LinkedHashMap<>();
            for (String size : sizeList){
                int sizeCount = 0;
                for (CutQueryLeak cutQueryLeak : orderInfoList){
                    if (color.equals(cutQueryLeak.getColorName()) && size.equals(cutQueryLeak.getSizeName())){
                        sizeCount += cutQueryLeak.getOrderCount();
                    }
                }
                orderSizeMap.put(size,sizeCount);
            }
            orderColorMap.put(color, orderSizeMap);
        }
        map.put("orderInfo",orderColorMap);
        for (String color : colorList){
            Map<String, Object> bedMap = new LinkedHashMap<>();
            for (int i=0; i<bedList.size(); i++){
                Integer bed = bedList.get(i);
                Map<String, Object> sizeMap = new LinkedHashMap<>();
                boolean bedFlag = true;
                for (String size : sizeList){
                    int cutSizeCount = 0;
                    for (CutBedColor cutBedColor : cutBedColorList){
                        if (color.equals(cutBedColor.getColorName()) && bed.equals(cutBedColor.getBedNumber()) && size.equals(cutBedColor.getSizeName())){
                            cutSizeCount += cutBedColor.getPieceCount();
                            bedFlag = false;
                        }
                    }
                    sizeMap.put(size,cutSizeCount);
                }
                if (!bedFlag){
                    bedMap.put(bedTimeGroupList.get(i),sizeMap);
                }
            }
            colorMap.put(color,bedMap);
        }
        map.put("cutInfo",colorMap);
        Map<String, Object> tailorColorMap = new LinkedHashMap<>();
        for (String color : colorList){
            Map<String, Object> tailorSizeMap = new LinkedHashMap<>();
            for (String size : sizeList){
                int tailorCount = 0;
                for (CutQueryLeak cutQueryLeak : tailorInfoList){
                    if (color.equals(cutQueryLeak.getColorName()) && size.equals(cutQueryLeak.getSizeName())){
                        tailorCount += cutQueryLeak.getLayerCount();
                    }
                }
                tailorSizeMap.put(size,tailorCount);
            }
            tailorColorMap.put(color, tailorSizeMap);
        }
        map.put("tailorInfo",tailorColorMap);
        Map<String, Object> diffColorMap = new LinkedHashMap<>();
        Map<String, Object> percentColorMap = new LinkedHashMap<>();
        for (String color : colorList){
            Map<String, Object> diffSizeMap = new LinkedHashMap<>();
            Map<String, Object> percentSizeMap = new LinkedHashMap<>();
            for (String size : sizeList){
                int orderCount = 0;
                int tailorCount = 0;
                for (CutQueryLeak cutQueryLeak1 : orderInfoList){
                    if (color.equals(cutQueryLeak1.getColorName()) && size.equals(cutQueryLeak1.getSizeName())){
                        orderCount += cutQueryLeak1.getOrderCount();
                    }
                }
                for (CutQueryLeak cutQueryLeak2 : tailorInfoList){
                    if (color.equals(cutQueryLeak2.getColorName()) && size.equals(cutQueryLeak2.getSizeName())){
                        tailorCount += cutQueryLeak2.getLayerCount();
                    }
                }
                diffSizeMap.put(size,tailorCount-orderCount);
                if (orderCount == 0){
                    percentSizeMap.put(size,0);
                }else{
                    float diffPercent = (float) (tailorCount-orderCount)/(orderCount);
                    percentSizeMap.put(size,diffPercent);
                }
            }
            diffColorMap.put(color, diffSizeMap);
            percentColorMap.put(color, percentSizeMap);
        }
        map.put("diffInfo",diffColorMap);
        map.put("percentInfo",percentColorMap);
        return map;
    }

    @RequestMapping(value = "/procedureBalanceDetail", method = RequestMethod.GET)
    public String procedureBalanceDetail(Model model,@RequestParam("orderName") String orderName,
                                         @RequestParam("from") String from,
                                         @RequestParam("to")String to,
                                         @RequestParam("groupName")String groupName,
                                         @RequestParam("procedureNumber")String procedureNumber) throws Exception{
//        orderName = java.net.URLDecoder.decode(orderName,"UTF-8");
        model.addAttribute("orderName",orderName);
        model.addAttribute("from",from);
        model.addAttribute("to",to);
        model.addAttribute("groupName",groupName);
        model.addAttribute("procedureNumber",procedureNumber);
        return "report/fb_procedureBalanceDetail";
    }

    @RequestMapping(value = "/getproductionprogressdetailbyordertimegroup", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getProductionProgressDetailByOrderTimeGroup(@RequestParam("orderName") String orderName,
                                                                           @RequestParam("from") String from,
                                                                           @RequestParam("to")String to,
                                                                           @RequestParam("groupName")String groupName,
                                                                           @RequestParam("procedureNumber")String procedureNumber){
        if("".equals(groupName)){
            groupName = null;
        }
        if("".equals(from)){
            from = "1970-01-01";
        }
        if("".equals(to)){
            to = "2050-01-01";
        }
        Map<String, Object> map = new LinkedHashMap<>();
        Map<String, Object> colorMap = new LinkedHashMap<>();
        try{
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date fromDate = sdf.parse(from);
            Date toDate = sdf.parse(to);
            List<ProductionProgressDetail> miniProductionDetailList = reportService.getMiniProductionProgressDetailByOrderTimeGroup(orderName,fromDate,toDate,groupName,Integer.parseInt(procedureNumber.trim()));
            List<PersonProductionDetail> miniPersonProductionDetailList = reportService.getMiniPersonProductionOfProcedure(orderName,fromDate,toDate,groupName,Integer.parseInt(procedureNumber.trim()));
            List<CutQueryLeak> orderInfoList = orderClothesService.getOrderInfoByName(orderName);
            List<CutQueryLeak> tailorInfoList = tailorService.getTailorInfoByOrderPartType(orderName, null, 0, null, null);
            List<String> colorList = new ArrayList<>();
            List<String> sizeList = new ArrayList<>();
            for(CutQueryLeak cutQueryLeak : orderInfoList){
                if (!colorList.contains(cutQueryLeak.getColorName())){
                    colorList.add(cutQueryLeak.getColorName());
                }
                if (!sizeList.contains(cutQueryLeak.getSizeName())){
                    sizeList.add(cutQueryLeak.getSizeName());
                }
            }
            for(CutQueryLeak cutQueryLeak : tailorInfoList){
                if (!colorList.contains(cutQueryLeak.getColorName())){
                    colorList.add(cutQueryLeak.getColorName());
                }
                if (!sizeList.contains(cutQueryLeak.getSizeName())){
                    sizeList.add(cutQueryLeak.getSizeName());
                }
            }
            for (ProductionProgressDetail ppd : miniProductionDetailList){
                if (!colorList.contains(ppd.getColorName())){
                    colorList.add(ppd.getColorName());
                }
                if (!sizeList.contains(ppd.getSizeName())){
                    sizeList.add(ppd.getSizeName());
                }
            }
            List<String> desiredOrder = Arrays.asList("XXXXXS","5XS","XXXXS","4XS","XXXS","3XS","XXS","2XS","XS","XS/25","xs","S","S/27","M","M/29","L","L/31","XL",
                    "XL/33","XXL","2XL","XXL/36","XXXL","3XL","XXXL/38","4XL","XXXXL","5XL","XXXXXL","6XL","XXXXXXL","80","090","90","100","110","110/27",
                    "120","120/29","130","130/31","140","140/33","150","150/36","155","160","165","170","175","180","185","190");
            Comparator<String> sizeOrder = Comparator.comparingInt(desiredOrder::indexOf);
            sizeList.sort(sizeOrder);
            for (String color : colorList){
                Map<String, Object> sizeMap = new LinkedHashMap<>();
                for (String size : sizeList){
                    int pc = 0;
                    ProductionDetailLeak pdl = new ProductionDetailLeak();
                    for (ProductionProgressDetail ppd : miniProductionDetailList){
                        if (color.equals(ppd.getColorName()) && size.equals(ppd.getSizeName())){
                            pc += ppd.getProductionCount();
                        }
                    }
                    for (CutQueryLeak cutQueryLeak : orderInfoList){
                        if (color.equals(cutQueryLeak.getColorName()) && size.equals(cutQueryLeak.getSizeName())){
                            pdl.setOrderCount(cutQueryLeak.getOrderCount());
                        }
                    }
                    for (CutQueryLeak cutQueryLeak : tailorInfoList){
                        if (color.equals(cutQueryLeak.getColorName()) && size.equals(cutQueryLeak.getSizeName())){
                            pdl.setCutCount(cutQueryLeak.getWellCount());
                        }
                    }
                    pdl.setProductionCount(pc);
                    pdl.setLeakCount(pc-pdl.getCutCount());
                    sizeMap.put(size,pdl);
                }
                colorMap.put(color,sizeMap);
            }
            map.put("color",colorMap);
            map.put("colorList",colorList);
            map.put("sizeList",sizeList);
            map.put("person",miniPersonProductionDetailList);
            return map;
        }catch (ParseException e){
            e.printStackTrace();
        }
        return map;
    }


    @RequestMapping(value = "/minigetproductionprogressdetailbyordertimegroup", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> miniGetProductionProgressDetailByOrderTimeGroup(@RequestParam("orderName") String orderName,
                                                                               @RequestParam("from") String from,
                                                                               @RequestParam("to")String to,
                                                                               @RequestParam("groupName")String groupName,
                                                                               @RequestParam("procedureNumber")String procedureNumber){
        if("".equals(groupName)){
            groupName = null;
        }
        if("".equals(from)){
            from = "1970-01-01";
        }
        if("".equals(to)){
            to = "2050-01-01";
        }
        Map<String, Object> map = new LinkedHashMap<>();
        Map<String, Object> colorMap = new LinkedHashMap<>();
        try{
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date fromDate = sdf.parse(from);
            Date toDate = sdf.parse(to);
            List<ProductionProgressDetail> miniProductionDetailList = reportService.getMiniProductionProgressDetailByOrderTimeGroup(orderName,fromDate,toDate,groupName,Integer.parseInt(procedureNumber.trim()));
            List<PersonProductionDetail> miniPersonProductionDetailList = reportService.getMiniPersonProductionOfProcedure(orderName,fromDate,toDate,groupName,Integer.parseInt(procedureNumber.trim()));
            List<CutQueryLeak> orderInfoList = orderClothesService.getOrderInfoByName(orderName);
            List<CutQueryLeak> tailorInfoList = tailorService.getTailorInfoByOrderPartType(orderName, null, 0, null, null);
            List<String> colorList = new ArrayList<>();
            List<String> sizeList = new ArrayList<>();
            for(CutQueryLeak cutQueryLeak : orderInfoList){
                if (!colorList.contains(cutQueryLeak.getColorName())){
                    colorList.add(cutQueryLeak.getColorName());
                }
                if (!sizeList.contains(cutQueryLeak.getSizeName())){
                    sizeList.add(cutQueryLeak.getSizeName());
                }
            }
            for(CutQueryLeak cutQueryLeak : tailorInfoList){
                if (!colorList.contains(cutQueryLeak.getColorName())){
                    colorList.add(cutQueryLeak.getColorName());
                }
                if (!sizeList.contains(cutQueryLeak.getSizeName())){
                    sizeList.add(cutQueryLeak.getSizeName());
                }
            }
            for (ProductionProgressDetail ppd : miniProductionDetailList){
                if (!colorList.contains(ppd.getColorName())){
                    colorList.add(ppd.getColorName());
                }
                if (!sizeList.contains(ppd.getSizeName())){
                    sizeList.add(ppd.getSizeName());
                }
            }
            List<String> desiredOrder = Arrays.asList("XXXXXS","5XS","XXXXS","4XS","XXXS","3XS","XXS","2XS","XS","XS/25","xs","S","S/27","M","M/29","L","L/31","XL",
                    "XL/33","XXL","2XL","XXL/36","XXXL","3XL","XXXL/38","4XL","XXXXL","5XL","XXXXXL","6XL","XXXXXXL","80","090","90","100","110","110/27",
                    "120","120/29","130","130/31","140","140/33","150","150/36","155","160","165","170","175","180","185","190");
            Comparator<String> sizeOrder = Comparator.comparingInt(desiredOrder::indexOf);
            sizeList.sort(sizeOrder);
            for (String color : colorList){
                Map<String, Object> sizeMap = new LinkedHashMap<>();
                for (String size : sizeList){
                    int pc = 0;
                    ProductionDetailLeak pdl = new ProductionDetailLeak();
                    for (ProductionProgressDetail ppd : miniProductionDetailList){
                        if (color.equals(ppd.getColorName()) && size.equals(ppd.getSizeName())){
                            pc += ppd.getProductionCount();
                        }
                    }
                    for (CutQueryLeak cutQueryLeak : orderInfoList){
                        if (color.equals(cutQueryLeak.getColorName()) && size.equals(cutQueryLeak.getSizeName())){
                            pdl.setOrderCount(cutQueryLeak.getOrderCount());
                        }
                    }
                    for (CutQueryLeak cutQueryLeak : tailorInfoList){
                        if (color.equals(cutQueryLeak.getColorName()) && size.equals(cutQueryLeak.getSizeName())){
                            pdl.setCutCount(cutQueryLeak.getWellCount());
                        }
                    }
                    pdl.setProductionCount(pc);
                    pdl.setLeakCount(pc-pdl.getCutCount());
                    sizeMap.put(size,pdl);
                }
                colorMap.put(color,sizeMap);
            }
            map.put("color",colorMap);
            map.put("person",miniPersonProductionDetailList);
            return map;
        }catch (ParseException e){
            e.printStackTrace();
        }
        return map;
    }




    @RequestMapping(value = "/getprocedureprogress", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getProcedureProgress(@RequestParam("from") String from,
                                                    @RequestParam("to") String to,
                                                    @RequestParam("procedureFrom") String procedureFrom,
                                                    @RequestParam("procedureTo") String procedureTo,
                                                    @RequestParam("orderName") String orderName,
                                                    @RequestParam("groupName")String groupName,
                                                    @RequestParam("employeeNumber")String employeeNumber){
        Map<String, Object> map = new LinkedHashMap<>();
        Integer pFrom;
        Integer pTo;
        if ("".equals(groupName)){
            groupName = null;
        }
        if ("".equals(employeeNumber)){
            employeeNumber = null;
        }
        if ("".equals(orderName)){
            orderName = null;
        }
        if ("".equals(procedureFrom)){
            pFrom = null;
        }else {
            pFrom = Integer.parseInt(procedureFrom);
        }
        if ("".equals(procedureTo)){
            pTo = null;
        }else {
            pTo = Integer.parseInt(procedureTo);
        }

        try{
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date fromDate = sdf.parse(from);
            Date toDate = sdf.parse(to);
            List<GeneralSalary> generalSalaryList = pieceWorkService.getMiniProcedureProgressNew(fromDate,toDate,pFrom,pTo,orderName,groupName,employeeNumber);
            List<GeneralSalary> hourGeneralList = hourEmpService.getHourProcedureProgressNew(fromDate,toDate,pFrom,pTo,orderName,groupName,employeeNumber);
            if (hourGeneralList != null && hourGeneralList.size() > 0){
                for (GeneralSalary hourGeneral : hourGeneralList){
                    String tmpOrderName = hourGeneral.getOrderName();
                    hourGeneral.setClothesVersionNumber(orderClothesService.getVersionNumberByOrderName(tmpOrderName));
                    hourGeneral.setSalaryType("计时");
                    hourGeneral.setProcedureName("时工");
                }
            }
            generalSalaryList.addAll(hourGeneralList);
            Collections.sort(generalSalaryList);
            Set<String> empSet = new HashSet<>();
            for (GeneralSalary generalSalary : generalSalaryList){
                empSet.add(generalSalary.getEmployeeNumber());
            }
            for (String empNumber : empSet){
                List<GeneralSalary> empGeneralSalaryList = new ArrayList<>();
                for (GeneralSalary generalSalary : generalSalaryList){
                    if (empNumber.equals(generalSalary.getEmployeeNumber())){
                        empGeneralSalaryList.add(generalSalary);
                    }
                }
                map.put(empNumber,empGeneralSalaryList);
            }
            return map;
        }catch (ParseException e){
            e.printStackTrace();
        }
        return map;
    }

    @RequestMapping(value = "/getprocedureprogressmultigroup", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getProcedureProgress(@RequestParam("from") String from,
                                                    @RequestParam("to") String to,
                                                    @RequestParam(value = "procedureFrom", required = false) Integer procedureFrom,
                                                    @RequestParam(value = "procedureTo", required = false) Integer procedureTo,
                                                    @RequestParam(value = "orderName", required = false) String orderName,
                                                    @RequestParam(value = "groupList[]", required = false) List<String> groupList,
                                                    @RequestParam(value = "employeeNumber", required = false)String employeeNumber){
        Map<String, Object> map = new LinkedHashMap<>();
        try{
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date fromDate = sdf.parse(from);
            Date toDate = sdf.parse(to);
            List<GeneralSalary> generalSalaryList = pieceWorkService.getMiniProcedureProgressMultiGroup(fromDate,toDate,procedureFrom,procedureTo,orderName,groupList,employeeNumber);
            Set<String> orderNameSet = new HashSet<>();
            Set<String> empSet = new HashSet<>();
            for (GeneralSalary generalSalary : generalSalaryList){
                empSet.add(generalSalary.getEmployeeNumber());
                orderNameSet.add(generalSalary.getOrderName());
            }
            List<String> orderNameList = new ArrayList<>(orderNameSet);
            List<OrderProcedure> orderProcedureList = orderProcedureService.getOrderProcedureByOrderNames(orderNameList, null);
            for (GeneralSalary generalSalary : generalSalaryList){
                Float miniPrice = 0f;
                Float miniPriceTwo = 0f;
                for (OrderProcedure orderProcedure : orderProcedureList){
                    if (generalSalary.getOrderName().equals(orderProcedure.getOrderName()) && generalSalary.getProcedureNumber().equals(orderProcedure.getProcedureNumber())){
                        miniPrice = (float) (orderProcedure.getPiecePrice() + orderProcedure.getPiecePriceTwo());
                        if (orderProcedure.getProcedureSection().equals("车缝")){
                            miniPriceTwo = miniPrice*orderProcedure.getSubsidy();
                        }
                    }
                }
                generalSalary.setSalaryType("计件");
                generalSalary.setPrice(miniPrice);
                generalSalary.setSalary(miniPrice*generalSalary.getPieceCount());
                generalSalary.setPriceTwo(miniPriceTwo);
                generalSalary.setSalaryTwo(miniPriceTwo*generalSalary.getPieceCount());
            }
            Collections.sort(generalSalaryList);
            for (String empNumber : empSet){
                List<GeneralSalary> empGeneralSalaryList = new ArrayList<>();
                for (GeneralSalary generalSalary : generalSalaryList){
                    if (empNumber.equals(generalSalary.getEmployeeNumber())){
                        empGeneralSalaryList.add(generalSalary);
                    }
                }
                map.put(empNumber,empGeneralSalaryList);
            }
            return map;
        }catch (ParseException e){
            e.printStackTrace();
        }
        return map;
    }


    @RequestMapping(value = "/getweekworkamount", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getProcedureProgress() throws ParseException{
        Map<String, Object> map = new LinkedHashMap<>();
        List<WeekAmount> weekAmountList1 = tailorService.getTailorWeekAmount(null, 0);
        List<WeekAmount> weekAmountList2 = embOutStoreService.getEmbOutWeekAmount();
        List<WeekAmount> weekAmountList4 = pieceWorkService.getPieceWeekAmount();
        List<WeekAmount> weekAmountList5 = pieceWorkService.getFinishWeekAmount();
        List<WeekAmount> weekAmountList = new ArrayList<>();
        Map<String, Object> map1 = new LinkedHashMap<>();
        Map<String, Object> map2 = new LinkedHashMap<>();
        Map<String, Object> map3 = new LinkedHashMap<>();
        Map<String, Object> map4 = new LinkedHashMap<>();
        List<String> dateList = new ArrayList<>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        for (int i = 6; i >= 0; i--){
            String tmpDate = getPastDate(i);
            dateList.add(tmpDate);
            boolean flag1 = true;
            for (WeekAmount weekAmount1 : weekAmountList1){
                if (sdf.format(weekAmount1.getRecordDate()).equals(tmpDate)){
                    flag1 = false;
                    map1.put(tmpDate, weekAmount1.getWorkAmount());
                }
            }
            if (flag1){
                map1.put(tmpDate, 0);
            }
            boolean flag2 = true;
            for (WeekAmount weekAmount2 : weekAmountList2){
                if (sdf.format(weekAmount2.getRecordDate()).equals(tmpDate)){
                    flag2 = false;
                    map2.put(tmpDate, weekAmount2.getWorkAmount());
                }
            }
            if (flag2){
                map2.put(tmpDate, 0);
            }
            int tmpWorkAmount = 0;
            for (WeekAmount weekAmount4 : weekAmountList4){
                if (sdf.format(weekAmount4.getRecordDate()).equals(tmpDate)){
                    tmpWorkAmount += weekAmount4.getWorkAmount();
                }
            }
            map3.put(tmpDate, tmpWorkAmount);
            int tmpFinishAmount = 0;
            for (WeekAmount weekAmount5 : weekAmountList5){
                if (sdf.format(weekAmount5.getRecordDate()).equals(tmpDate)){
                    tmpFinishAmount += weekAmount5.getWorkAmount();
                }
            }
            map4.put(tmpDate, tmpFinishAmount);
        }
        map.put("dateList", dateList);
        map.put("1",map1);
        map.put("2",map2);
        map.put("3",map3);
        map.put("4",map4);
        return map;
    }

    public String getPastDate(int n) throws ParseException{
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, -n);
        Date time = c.getTime();
        return sdf.format(time);
    }


    @RequestMapping(value = "/getordersalarysummary", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getOrderSalarySummary(@RequestParam("orderName") String orderName,
                                                     @RequestParam("from") String from,
                                                     @RequestParam("to") String to){
        Map<String, Object> map = new LinkedHashMap<>();
        if("".equals(from)){
            from = "1970-01-01";
        }
        if("".equals(to)){
            to = "2050-01-01";
        }
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date fromDate = sdf.parse(from);
            Date toDate = sdf.parse(to);
            List<GeneralSalary> salaryList = new ArrayList<>();
            List<SalaryCount> salaryCountList = pieceWorkService.getOrderMiniSalarySummary(orderName,fromDate,toDate);
            List<HourEmp> hourEmpList = hourEmpService.getOrderHourEmpSummary(orderName,fromDate,toDate);
            if (salaryCountList != null && salaryCountList.size() > 0){
                for (SalaryCount salaryCount : salaryCountList){
                    String tmpOrderName = salaryCount.getOrderName();
                    Float miniPrice = 0f;
                    Float miniPriceTwo = 0f;
                    String procedureName = "通用";
                    String miniProcedureSection = "通用";
                    ProcedureInfo pi = orderProcedureService.getProcedureInfoByOrderProcedureNumber(tmpOrderName, salaryCount.getProcedureNumber());
                    if (pi != null){
                        if (pi.getPiecePrice() != null){
                            miniPrice = pi.getPiecePrice() + pi.getPiecePriceTwo();
                        }
                        if (pi.getProcedureName() != null){
                            procedureName = pi.getProcedureName();
                        }
                        if (pi.getProcedureSection() != null){
                            miniProcedureSection = pi.getProcedureSection();
                        }
                        if (salaryCount.getGroupName().equals("车缝A组") || salaryCount.getGroupName().equals("车缝B组") || salaryCount.getGroupName().equals("车缝C组") || salaryCount.getGroupName().equals("车缝D组")){
                            if (pi.getProcedureSection() != null && pi.getSubsidy() != null){
                                miniProcedureSection = pi.getProcedureSection();
                                if (miniProcedureSection.equals("车缝")){
                                    miniPriceTwo = miniPrice*pi.getSubsidy();
                                }
                            }
                        }
                    }
                    GeneralSalary tmpGeneralSalary = new GeneralSalary(tmpOrderName,orderClothesService.getVersionNumberByOrderName(tmpOrderName),"计件",salaryCount.getProcedureNumber(),procedureName,salaryCount.getPieceCount().floatValue(),miniPrice,miniPrice*salaryCount.getPieceCount(),miniPriceTwo,miniPriceTwo*salaryCount.getPieceCount(),salaryCount.getSalaryDate(),miniProcedureSection);
                    salaryList.add(tmpGeneralSalary);
                }
            }
            if (hourEmpList != null && hourEmpList.size() > 0){
                for (HourEmp hourEmp : hourEmpList){
                    String tmpOrderName = hourEmp.getOrderName();
                    Float hourPrice = 0f;
                    ProcedureInfo pi = orderProcedureService.getProcedureInfoByOrderProcedureNumber(tmpOrderName, 11111);
                    if (pi != null){
                        if (pi.getPiecePrice() != null){
                            hourPrice = pi.getPiecePrice() + pi.getPiecePriceTwo();
                        }
                    }
                    GeneralSalary tmpGeneralSalary = new GeneralSalary(tmpOrderName,orderClothesService.getVersionNumberByOrderName(tmpOrderName),"计时",11111,"时工",hourEmp.getLayerCount(),hourPrice,hourPrice*hourEmp.getLayerCount(),0f,0f,hourEmp.getHourEmpDate(),"时工");
                    salaryList.add(tmpGeneralSalary);
                }
            }
            map.put("orderSalarySummary",salaryList);
            return map;
        }catch (ParseException e){
            e.printStackTrace();
        }
        return map;

    }


    @RequestMapping(value = "/geteachpersonproduction", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getEachPersonProduction(@RequestParam("orderName") String orderName,
                                                       @RequestParam("from") String from,
                                                       @RequestParam("to")String to,
                                                       @RequestParam("groupName")String groupName){
        if("".equals(groupName)){
            groupName = null;
        }
        if("".equals(from)){
            from = "1970-01-01";
        }
        if("".equals(to)){
            to = "2050-01-01";
        }
        Map<String, Object> map = new LinkedHashMap<>();
        try{
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date fromDate = sdf.parse(from);
            Date toDate = sdf.parse(to);
            List<ProcedureInfo> procedureInfoList = orderProcedureService.getProcedureInfoByOrderName(orderName);
            List<PersonProductionDetail> miniPersonProductionDetailList = reportService.getMiniEachPersonProductionOfProcedure(orderName,fromDate,toDate,groupName);
            for (ProcedureInfo procedureInfo : procedureInfoList){
                String procedureString = procedureInfo.getProcedureName();
                boolean isPerson = false;
                Map<String, Object> procedureMap = new LinkedHashMap<>();
                for (PersonProductionDetail ppd2:miniPersonProductionDetailList){
                    if (ppd2.getProcedureNumber() != null){
                        if (ppd2.getProcedureNumber().equals(procedureInfo.getProcedureNumber())){
                            String personString = ppd2.getEmployeeNumber() + "-" + ppd2.getEmployeeName();
                            if (ppd2.getProductionCount() != null){
                                isPerson = true;
                                procedureMap.put(personString,ppd2.getProductionCount());
                            }
                        }
                    }
                }
                if (isPerson){
                    map.put(procedureString, procedureMap);
                }
            }
            return map;
        }catch (ParseException e){
            e.printStackTrace();
        }
        return map;
    }


    @RequestMapping(value = "/getotherproductionprogressdetailbyordertime", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getOtherProductionProgressDetailByOrderTimeGroup(@RequestParam("orderName") String orderName,
                                                                                @RequestParam("from") String from,
                                                                                @RequestParam("to")String to,
                                                                                @RequestParam("procedureName")String procedureName){
        if("".equals(from)){
            from = "1970-01-01";
        }
        if("".equals(to)){
            to = "2050-01-01";
        }
        Map<String, Object> map = new LinkedHashMap<>();
        Map<String, Object> colorMap = new LinkedHashMap<>();
        try{
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date fromDate = sdf.parse(from);
            Date toDate = sdf.parse(to);
            List<CutQueryLeak> orderInfoList = orderClothesService.getOrderInfoByName(orderName);
            List<CutQueryLeak> tailorInfoList = tailorService.getTailorInfoByOrderPartType(orderName, null, 0, null, null);
            List<CutQueryLeak> cutEmbStoreList = new ArrayList<>();
            if ("入裁片".equals(procedureName)){
                cutEmbStoreList = reportService.getCutInStoreProgress(orderName,fromDate,toDate);
            }else if ("入衣胚".equals(procedureName)){
                cutEmbStoreList = reportService.getEmbInStoreProgress(orderName,fromDate,toDate);
            }else {
                cutEmbStoreList = reportService.getEmbOutStoreProgress(orderName,fromDate,toDate);
            }
            List<String> colorList = new ArrayList<>();
            List<String> sizeList = new ArrayList<>();
            for(CutQueryLeak cutQueryLeak : orderInfoList){
                if (!colorList.contains(cutQueryLeak.getColorName())){
                    colorList.add(cutQueryLeak.getColorName());
                }
                if (!sizeList.contains(cutQueryLeak.getSizeName())){
                    sizeList.add(cutQueryLeak.getSizeName());
                }
            }
            for(CutQueryLeak cutQueryLeak : tailorInfoList){
                if (!colorList.contains(cutQueryLeak.getColorName())){
                    colorList.add(cutQueryLeak.getColorName());
                }
                if (!sizeList.contains(cutQueryLeak.getSizeName())){
                    sizeList.add(cutQueryLeak.getSizeName());
                }
            }
            List<String> desiredOrder = Arrays.asList("XXXXXS","5XS","XXXXS","4XS","XXXS","3XS","XXS","2XS","XS","XS/25","xs","S","S/27","M","M/29","L","L/31","XL",
                    "XL/33","XXL","2XL","XXL/36","XXXL","3XL","XXXL/38","4XL","XXXXL","5XL","XXXXXL","6XL","XXXXXXL","80","090","90","100","110","110/27",
                    "120","120/29","130","130/31","140","140/33","150","150/36","155","160","165","170","175","180","185","190");
            Comparator<String> sizeOrder = Comparator.comparingInt(desiredOrder::indexOf);
            sizeList.sort(sizeOrder);
            for (String color : colorList){
                Map<String, Object> sizeMap = new LinkedHashMap<>();
                for (String size : sizeList){
                    int pc = 0;
                    ProductionDetailLeak pdl = new ProductionDetailLeak();
                    for (CutQueryLeak cutQueryLeak : orderInfoList){
                        if (color.equals(cutQueryLeak.getColorName()) && size.equals(cutQueryLeak.getSizeName())){
                            pdl.setOrderCount(cutQueryLeak.getOrderCount());
                        }
                    }
                    for (CutQueryLeak cutQueryLeak : tailorInfoList){
                        if (color.equals(cutQueryLeak.getColorName()) && size.equals(cutQueryLeak.getSizeName())){
                            pdl.setCutCount(cutQueryLeak.getWellCount());
                        }
                    }
                    for (CutQueryLeak cutQueryLeak : cutEmbStoreList){
                        if (color.equals(cutQueryLeak.getColorName()) && size.equals(cutQueryLeak.getSizeName())){
                            pc = cutQueryLeak.getLayerCount();
                        }
                    }
                    pdl.setProductionCount(pc);
                    pdl.setLeakCount(pc-pdl.getCutCount());
                    sizeMap.put(size,pdl);
                }
                colorMap.put(color,sizeMap);
            }
            map.put("color",colorMap);
            map.put("sizeList", sizeList);
            map.put("colorList", colorList);
            return map;
        }catch (ParseException e){
            e.printStackTrace();
        }
        return map;
    }


    @RequestMapping(value = "/minigetotherproductionprogressdetailbyordertime", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> miniGetOtherProductionProgressDetailByOrderTimeGroup(@RequestParam("orderName") String orderName,
                                                                                    @RequestParam("from") String from,
                                                                                    @RequestParam("to")String to,
                                                                                    @RequestParam("procedureName")String procedureName){
        if("".equals(from)){
            from = "1970-01-01";
        }
        if("".equals(to)){
            to = "2050-01-01";
        }
        Map<String, Object> map = new LinkedHashMap<>();
        Map<String, Object> colorMap = new LinkedHashMap<>();
        try{
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date fromDate = sdf.parse(from);
            Date toDate = sdf.parse(to);
            List<CutQueryLeak> orderInfoList = orderClothesService.getOrderInfoByName(orderName);
            List<CutQueryLeak> tailorInfoList = tailorService.getTailorInfoByOrderPartType(orderName, null, 0, null, null);
            List<CutQueryLeak> cutEmbStoreList = new ArrayList<>();
            if ("入裁片".equals(procedureName)){
                cutEmbStoreList = reportService.getCutInStoreProgress(orderName,fromDate,toDate);
            }else if ("入衣胚".equals(procedureName)){
                cutEmbStoreList = reportService.getEmbInStoreProgress(orderName,fromDate,toDate);
            }else {
                cutEmbStoreList = reportService.getEmbOutStoreProgress(orderName,fromDate,toDate);
            }
            List<String> colorList = new ArrayList<>();
            List<String> sizeList = new ArrayList<>();
            for(CutQueryLeak cutQueryLeak : orderInfoList){
                if (!colorList.contains(cutQueryLeak.getColorName())){
                    colorList.add(cutQueryLeak.getColorName());
                }
                if (!sizeList.contains(cutQueryLeak.getSizeName())){
                    sizeList.add(cutQueryLeak.getSizeName());
                }
            }
            for(CutQueryLeak cutQueryLeak : tailorInfoList){
                if (!colorList.contains(cutQueryLeak.getColorName())){
                    colorList.add(cutQueryLeak.getColorName());
                }
                if (!sizeList.contains(cutQueryLeak.getSizeName())){
                    sizeList.add(cutQueryLeak.getSizeName());
                }
            }
            List<String> desiredOrder = Arrays.asList("XXXXXS","5XS","XXXXS","4XS","XXXS","3XS","XXS","2XS","XS","XS/25","xs","S","S/27","M","M/29","L","L/31","XL",
                    "XL/33","XXL","2XL","XXL/36","XXXL","3XL","XXXL/38","4XL","XXXXL","5XL","XXXXXL","6XL","XXXXXXL","80","090","90","100","110","110/27",
                    "120","120/29","130","130/31","140","140/33","150","150/36","155","160","165","170","175","180","185","190");
            Comparator<String> sizeOrder = Comparator.comparingInt(desiredOrder::indexOf);
            sizeList.sort(sizeOrder);
            for (String color : colorList){
                Map<String, Object> sizeMap = new LinkedHashMap<>();
                for (String size : sizeList){
                    int pc = 0;
                    ProductionDetailLeak pdl = new ProductionDetailLeak();
                    for (CutQueryLeak cutQueryLeak : orderInfoList){
                        if (color.equals(cutQueryLeak.getColorName()) && size.equals(cutQueryLeak.getSizeName())){
                            pdl.setOrderCount(cutQueryLeak.getOrderCount());
                        }
                    }
                    for (CutQueryLeak cutQueryLeak : tailorInfoList){
                        if (color.equals(cutQueryLeak.getColorName()) && size.equals(cutQueryLeak.getSizeName())){
                            pdl.setCutCount(cutQueryLeak.getWellCount());
                        }
                    }
                    for (CutQueryLeak cutQueryLeak : cutEmbStoreList){
                        if (color.equals(cutQueryLeak.getColorName()) && size.equals(cutQueryLeak.getSizeName())){
                            pc = cutQueryLeak.getLayerCount();
                        }
                    }
                    pdl.setProductionCount(pc);
                    pdl.setLeakCount(pc-pdl.getCutCount());
                    sizeMap.put(size,pdl);
                }
                colorMap.put(color,sizeMap);
            }
            map.put("color",colorMap);
            map.put("person","");
            return map;
        }catch (ParseException e){
            e.printStackTrace();
        }
        return map;
    }




    @RequestMapping(value = "/getpieceworkdetail", method = RequestMethod.GET)
    public String getPieceWorkDetail(Model model,@RequestParam("from") String from,
                                     @RequestParam("to") String to,
                                     @RequestParam("employeeNumber")String employeeNumber,
                                     @RequestParam("orderName")String orderName){
        if("".equals(employeeNumber)){
            employeeNumber = null;
        }
        if("".equals(orderName)){
            orderName = null;
        }
        if("".equals(from)){
            from = "1970-01-01";
        }
        if("".equals(to)){
            to = "2050-01-01";
        }
        Map<String, Object> map = new LinkedHashMap<>();
        try{
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date fromDate = sdf.parse(from);
            Date toDate = sdf.parse(to);
            List<GeneralSalary> salaryCountList = pieceWorkService.getMiniSalaryDetail(fromDate, toDate, employeeNumber, orderName);
            if (salaryCountList != null && salaryCountList.size() > 0){
                for (GeneralSalary generalSalary : salaryCountList){
                    String tmpOrderName = generalSalary.getOrderName();
                    Integer procedureNumber = generalSalary.getProcedureNumber();
                    Float miniPrice = 0f;
                    Float miniPriceTwo = 0f;
                    ProcedureInfo pi = orderProcedureService.getProcedureInfoByOrderProcedureNumber(tmpOrderName,procedureNumber);
                    if (pi != null){
                        if (pi.getProcedureState().equals(2)){
                            if (pi.getPiecePrice() != null){
                                miniPrice = pi.getPiecePrice() + pi.getPiecePriceTwo();
                            }
                            if (pi.getProcedureSection() != null && pi.getSubsidy() != null){
                                if (pi.getProcedureSection().equals("车缝")){
                                    miniPriceTwo = miniPrice*pi.getSubsidy();
                                }
                            }
                        }
                    }
                    generalSalary.setSalaryType("计件");
                    generalSalary.setPrice(miniPrice);
                    generalSalary.setSalary(miniPrice*generalSalary.getPieceCount());
                    generalSalary.setPriceTwo(miniPriceTwo);
                    generalSalary.setSalaryTwo(miniPriceTwo*generalSalary.getPieceCount());
                }
            }
            Collections.sort(salaryCountList);
            Set<String> empSet = new HashSet<>();
            for (GeneralSalary generalSalary : salaryCountList){
                empSet.add(generalSalary.getEmployeeNumber());
            }
            List<GeneralSalary> totalEmpSalaryList = new ArrayList<>();
            for (String empNumber : empSet){
                for (GeneralSalary generalSalary : salaryCountList){
                    if (empNumber.equals(generalSalary.getEmployeeNumber())){
                        totalEmpSalaryList.add(generalSalary);
                    }
                }
            }
            model.addAttribute("pieceWorkDetailList",totalEmpSalaryList);

        }catch (ParseException e){
            e.printStackTrace();
        }
        return "report/fb_pieceWorkDetailList";
    }


}


