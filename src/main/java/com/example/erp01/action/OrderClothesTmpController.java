package com.example.erp01.action;

import com.example.erp01.model.OrderClothes;
import com.example.erp01.model.OrderClothesTmp;
import com.example.erp01.model.Tailor;
import com.example.erp01.service.OrderClothesTmpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.ArrayList;
import java.util.List;

@Configuration      //1.主要用于标记配置类，兼备Component的效果。
@EnableScheduling   // 2.开启定时任务
public class OrderClothesTmpController {

    @Autowired
    private OrderClothesTmpService orderClothesTmpService;

    @Scheduled(cron = "0 0 3 * * ? ")
    public void orderClothesTmpSync(){
        List<String> orderNameList = orderClothesTmpService.getYesterdayTailorOrderName();
        List<Tailor> tailorList = orderClothesTmpService.getTailorTotalInfoAmongYesterday(orderNameList);
        List<OrderClothes> orderClothesList = orderClothesTmpService.getOrderClothesByOrderNameList(orderNameList);
        if (tailorList != null && !tailorList.isEmpty()){
            List<OrderClothesTmp> orderClothesTmpList = new ArrayList<>();
            for (Tailor tailor : tailorList){
                for (OrderClothes orderClothes : orderClothesList){
                    if (tailor.getOrderName().equals(orderClothes.getOrderName()) && tailor.getColorName().equals(orderClothes.getColorName()) && tailor.getSizeName().equals(orderClothes.getSizeName())){
                        OrderClothesTmp orderClothesTmp = new OrderClothesTmp(orderClothes.getOrderClothesID(), orderClothes.getCustomerName(), orderClothes.getPurchaseMethod(), orderClothes.getOrderName(), orderClothes.getClothesVersionNumber(), orderClothes.getStyleDescription(), orderClothes.getColorName(), orderClothes.getSizeName(), tailor.getLayerCount(), orderClothes.getSeason(), orderClothes.getDeadLine(), "U", orderClothes.getGuid());
                        orderClothesTmpList.add(orderClothesTmp);
                    }
                }
            }
            if (orderClothesTmpList != null && !orderClothesTmpList.isEmpty()){
                orderClothesTmpService.addOrderClothesTmpBatch(orderClothesTmpList);
                System.out.println("同步完成");
            }
        }

    }


}
