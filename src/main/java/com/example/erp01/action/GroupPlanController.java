package com.example.erp01.action;

import com.example.erp01.model.GroupPlan;
import com.example.erp01.service.GroupPlanService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "erp")
public class GroupPlanController {

    @Autowired
    private GroupPlanService groupPlanService;

    @RequestMapping(value = "/groupPlanStart")
    public String printPartStart(Model model){
        model.addAttribute("bigMenuTag",9);
        model.addAttribute("menuTag",97);
        return "ieInfo/groupPlan";
    }

    @RequestMapping(value = "/addgroupplan",method = RequestMethod.POST)
    @ResponseBody
    public int addGroupPlan(@RequestParam("groupName") String groupName,
                                 @RequestParam("orderName") String orderName,
                                 @RequestParam("clothesVersionNumber") String clothesVersionNumber,
                                 @RequestParam("to") String to,
                                 @RequestParam("workHour") Integer workHour,
                                 @RequestParam("planCount") Integer planCount){
        try{
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date endDate = sdf.parse(to);
            GroupPlan groupPlan = new GroupPlan(groupName, orderName, clothesVersionNumber, endDate, workHour, planCount);
            return groupPlanService.addGroupPlan(groupPlan);
        }catch (ParseException e){
            e.printStackTrace();
        }
        return 1;
    }

    @RequestMapping(value = "/deletegroupplan",method = RequestMethod.POST)
    @ResponseBody
    public int deleteGroupPlan(@RequestParam("groupPlanID") Integer groupPlanID){
        return groupPlanService.deleteGroupPlan(groupPlanID);
    }

    @RequestMapping(value = "/getallgroupplan",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getAllGroupPlan(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<GroupPlan> groupPlanList = groupPlanService.getAllGroupPlan();
        map.put("groupPlanList",groupPlanList);
        return map;
    }

    @RequestMapping(value = "/updategroupplan",method = RequestMethod.POST)
    @ResponseBody
    public int deleteGroupPlan(@RequestParam("groupPlan") String groupPlan){

        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        GroupPlan gp = gson.fromJson(groupPlan, GroupPlan.class);
        return groupPlanService.updateGroupPlan(gp);
    }

}
