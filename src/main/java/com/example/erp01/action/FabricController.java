//package com.example.erp01.action;
//
//import com.example.erp01.model.*;
//import com.example.erp01.service.FabricOutRecordService;
//import com.example.erp01.service.FabricService;
//import com.example.erp01.service.FabricStorageService;
//import com.google.gson.*;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.ResponseBody;
//
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.*;
//
//@Controller
//@RequestMapping(value = "erp")
//public class FabricController {
//    private static final Logger log = LoggerFactory.getLogger(FabricController.class);
//    @Autowired
//    private FabricService fabricService;
//
//    @Autowired
//    private FabricStorageService fabricStorageService;
//
//    @Autowired
//    private FabricOutRecordService fabricOutRecordService;
//
//
//    @RequestMapping("/fabricStart")
//    public String fabricStart(Model model){
//        model.addAttribute("bigMenuTag",8);
//        model.addAttribute("menuTag",81);
//        return "fabric/fabric";
//    }
//
//    @RequestMapping("/innerBoardStart")
//    public String innerBoardStart(Model model){
//        model.addAttribute("bigMenuTag",8);
//        model.addAttribute("menuTag",82);
//        return "fabric/innerBoard";
//    }
//
//    @RequestMapping("/fabricInStoreStart")
//    public String fabricInStoreStart(){
//        return "fabric/fabricInStore";
//    }
//
//    @RequestMapping("/fabricOutStoreStart")
//    public String fabricOutStoreStart(){
//        return "fabric/fabricOutStore";
//    }
//
//    @RequestMapping("/fabricQueryStart")
//    public String fabricQueryStart(Model model){
//        model.addAttribute("bigMenuTag",8);
//        model.addAttribute("menuTag",85);
//        List<FabricStorage> fabricStorageList = fabricStorageService.getAllFabricStorage();
//        model.addAttribute("fabricStorageList",fabricStorageList);
//        return "fabric/fabricQuery";
//    }
//
//    @RequestMapping("/fabricLeakQueryStart")
//    public String fabricLeakQueryStart(Model model){
//        model.addAttribute("bigMenuTag",8);
//        model.addAttribute("menuTag",811);
//        return "fabric/fabricLeakQuery";
//    }
//
////    @RequestMapping("/fabricDetailStart")
////    public String fabricDetailStart(Model model){
////        model.addAttribute("bigMenuTag",8);
////        model.addAttribute("menuTag",812);
////        return "fabric/fabricDetail";
////    }
//
//    @RequestMapping("/addFabricStart")
//    public String addOrderProcedureStart(Model model){
//        model.addAttribute("type", "add");
//        return "fabric/fb_fabricAdd";
//    }
//
//    @RequestMapping("/addInnerBoardStart")
//    public String addInnerBoardStart(Model model){
//        model.addAttribute("type", "add");
//        return "fabric/fb_innerBoardAdd";
//    }
//
//    @RequestMapping(value = "/addfabricbatch",method = RequestMethod.POST)
//    @ResponseBody
//    public int addFabricBatch(@RequestParam("fabricJson")String fabricJson,
//                              @RequestParam("userName")String userName){
//        log.warn("面料录入："+fabricJson.toString()+"---操作用户："+userName);
//        JsonParser jsonParser = new JsonParser();
//        int result = 1;
//        try{
//            JsonArray fabricArray = (JsonArray) jsonParser.parse(fabricJson);
//            List<JsonObject> objectList = new ArrayList<>();
//            List<Fabric> fabricList = new ArrayList<>();
//            for (int i=0; i<fabricArray.size();i++){
//                JsonObject fabric = fabricArray.get(i).getAsJsonObject();
//                String customerName = fabric.get("customerName").toString().replace("\"","").trim();
//                String season = fabric.get("season").toString().replace("\"","").trim();
//                String versionNumber = fabric.get("versionNumber").toString().replace("\"","").trim();
//                String styleNumber = fabric.get("styleNumber").toString().replace("\"","").trim();
//                String supplier = fabric.get("supplier").toString().replace("\"","").trim();
//                String orderDate = fabric.get("orderDate").toString().replace("\"","").trim();
//                String deliveryDate = fabric.get("deliveryDate").toString().replace("\"","").trim();
//                String fabricName = fabric.get("fabricName").toString().replace("\"","").trim();
//                String fabricNumber = fabric.get("fabricNumber").toString().replace("\"","").trim();
//                String fabricColor = fabric.get("fabricColor").toString().replace("\"","").trim();
//                String fabricColorNumber = fabric.get("fabricColorNumber").toString().replace("\"","").trim();
//                String fabricUse = fabric.get("fabricUse").toString().replace("\"","").trim();
//                String unit = fabric.get("unit").toString().replace("\"","").trim();
//                //float pieceUsage = fabric.get("pieceUsage").getAsFloat();
//                String pieceUsageString = fabric.get("pieceUsage").toString().replace("\"","").trim();
//                //Integer clothesCount = fabric.get("clothesCount").getAsInt();
//                String clothesCountString = fabric.get("clothesCount").toString().replace("\"","").trim();
//                //float fabricCount = fabric.get("fabricCount").getAsFloat();
//                String fabricCountString = fabric.get("fabricCount").toString().replace("\"","").trim();
//                //float price = fabric.get("price").getAsFloat();
//                String priceString = fabric.get("price").toString().replace("\"","").trim();
//                //float fabricWidth = fabric.get("fabricWidth").getAsFloat();
//                String fabricWidthString = fabric.get("fabricWidth").toString().replace("\"","").trim();
//                //float fabricWeight = fabric.get("fabricWeight").getAsFloat();
//                String fabricWeightString = fabric.get("fabricWeight").toString().replace("\"","").trim();
//                String actualDeliveryDate = fabric.get("actualDeliveryDate").toString().replace("\"","").trim();
//                //float deliveryQuantity = fabric.get("deliveryQuantity").getAsFloat();
//                Float deliveryQuantity = Float.parseFloat(fabric.get("deliveryQuantity").toString().replace("\"","").trim());
//                //Float deliveryQuantity = fabric.get("deliveryQuantity").getAsFloat();
//                //Integer batchNumber = fabric.get("batchNumber").getAsInt();
//                Integer batchNumber = Integer.parseInt(fabric.get("batchNumber").toString().replace("\"","").trim());
//                String jarNumber = fabric.get("jarNumber").toString().replace("\"","").trim();
//                String remark = fabric.get("remark").toString().replace("\"","").trim();
//                Float pieceUsage = null;
//                Integer clothesCount = null;
//                Float fabricCount = null;
//                Float price = null;
//                Float fabricWidth = null;
//                Float fabricWeight = null;
//                if(!pieceUsageString.equals("") && !pieceUsageString.equals("null")){
//                    pieceUsage = Float.parseFloat(pieceUsageString);
//                }
//                if(!clothesCountString.equals("") && !clothesCountString.equals("null")){
//                    clothesCount = Integer.parseInt(clothesCountString);
//                }
//                if(!fabricCountString.equals("") && !fabricCountString.equals("null")){
//                    fabricCount = Float.parseFloat(fabricCountString);
//                }
//                if(!priceString.equals("") && !priceString.equals("null")){
//                    price = Float.parseFloat(priceString);
//                }
//                if(!fabricWidthString.equals("") && !fabricWidthString.equals("null")){
//                    fabricWidth = Float.parseFloat(fabricWidthString);
//                }
//                if(!fabricWeightString.equals("") && !fabricWeightString.equals("null")){
//                    fabricWeight = Float.parseFloat(fabricWeightString);
//                }
//                try {
//                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
//                    Date tmpOrderDate = new Date(0,0,1);
//                    Date tmpDeliveryDate = new Date(0,0,1);
//                    Date tmpActualDeliveryDate = new Date(0,0,1);
//                    if (!orderDate.equals("") && !orderDate.equals("null")){
//                        tmpOrderDate = sdf.parse(orderDate);
//                    }
//                    if (!deliveryDate.equals("") && !deliveryDate.equals("null")){
//                        tmpDeliveryDate = sdf.parse(deliveryDate);
//                    }
//                    if (!actualDeliveryDate.equals("") && !actualDeliveryDate.equals("null")){
//                        tmpActualDeliveryDate = sdf.parse(actualDeliveryDate);
//                    }
//                    Fabric fb = new Fabric(customerName, season, versionNumber, styleNumber, supplier, tmpOrderDate, tmpDeliveryDate, fabricName,fabricNumber,fabricColor,fabricColorNumber,fabricUse,unit,pieceUsage,clothesCount,fabricCount,price,fabricWidth,
//                            fabricWeight,tmpActualDeliveryDate,deliveryQuantity,batchNumber,jarNumber,remark);
//                    fabricList.add(fb);
//                }catch (ParseException e) {
//                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//                    Date tmpOrderDate = new Date(0,0,1);
//                    Date tmpDeliveryDate = new Date(0,0,1);
//                    Date tmpActualDeliveryDate = new Date(0,0,1);
//                    if (!orderDate.equals("") && !orderDate.equals("null")){
//                        tmpOrderDate = sdf.parse(orderDate);
//                    }
//                    if (!deliveryDate.equals("") && !deliveryDate.equals("null")){
//                        tmpDeliveryDate = sdf.parse(deliveryDate);
//                    }
//                    if (!actualDeliveryDate.equals("") && !deliveryDate.equals("null")){
//                        tmpActualDeliveryDate = sdf.parse(actualDeliveryDate);
//                    }
//                    Fabric fb = new Fabric(customerName, season, versionNumber, styleNumber, supplier, tmpOrderDate, tmpDeliveryDate, fabricName,fabricNumber,fabricColor,fabricColorNumber,fabricUse,unit,pieceUsage,clothesCount,fabricCount,price,fabricWidth,
//                            fabricWeight,tmpActualDeliveryDate,deliveryQuantity,batchNumber,jarNumber,remark);
//                    fabricList.add(fb);
//                }
//
//            }
//            result = fabricService.addFabricBatch(fabricList);
//        }catch (JsonIOException e){
//            e.printStackTrace();
//        }catch (JsonSyntaxException e){
//            e.printStackTrace();
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//        return result;
//    }
//
//    @RequestMapping(value = "/getallfabric",method = RequestMethod.GET)
//    public String getAllFabric(Model model){
//        List<Fabric> fabricList = fabricService.getAllFabric();
//        model.addAttribute("fabricList",fabricList);
//        return "fabric/fb_fabricList";
//    }
//
//    @RequestMapping(value = "/updatefabric",method = RequestMethod.POST)
//    @ResponseBody
//    public int updateFabric(Fabric fabric){
//        int res = fabricService.updateFabric(fabric);
//        return res;
//    }
//
//    @RequestMapping(value = "/deletefabric",method = RequestMethod.POST)
//    @ResponseBody
//    public int deleteFabric(@RequestParam("fabricID")Integer fabricID,
//                            @RequestParam("userName")String userName){
//        log.warn("面料删除："+fabricID+"---操作用户："+userName);
//        int res = fabricService.deleteFabric(fabricID);
//        return res;
//    }
//
////    @RequestMapping(value = "/getstylenumberhint",method = RequestMethod.GET)
////    @ResponseBody
////    public Map<String,Object> getStyleNumberHint(@RequestParam("keywords")String keywords){
////        Map<String,Object> map = new HashMap<>();
////        List<String> styleNumberList = fabricService.getStyleNumberHint(keywords);
////        map.put("content",styleNumberList);
////        map.put("code",0);
////        map.put("type","success");
////        return map;
////    }
//
////    @RequestMapping(value = "/getfabricversionnumberhint",method = RequestMethod.GET)
////    @ResponseBody
////    public Map<String,Object> getFabricVersionNumberHint(@RequestParam("keywords")String keywords){
////        Map<String,Object> map = new HashMap<>();
////        List<String> versionNumberList = fabricService.getVersionNumberBySubVersion(keywords);
////        map.put("content",versionNumberList);
////        map.put("code",0);
////        map.put("type","success");
////        return map;
////    }
//
//    @RequestMapping(value = "/getfabricnumberhint",method = RequestMethod.GET)
//    @ResponseBody
//    public Map<String,Object> getFabricNumberHint(@RequestParam("styleNumber")String styleNumber){
//        Map<String,Object> map = new HashMap<>();
//        List<String> fabricNumberList = fabricService.getFabricNumberHint(styleNumber);
//        map.put("fabricNumberList",fabricNumberList);
//        return map;
//    }
//
//
//
//    @RequestMapping(value = "/getfabriccolornumberhint",method = RequestMethod.GET)
//    @ResponseBody
//    public Map<String,Object> getFabricColorNumberHint(@RequestParam("styleNumber")String styleNumber,
//                                                       @RequestParam("fabricNumber")String fabricNumber){
//        Map<String,Object> map = new HashMap<>();
//        List<String> fabricColorNumberList = fabricService.getFabricColorNumberHint(styleNumber, fabricNumber);
//        map.put("fabricColorNumberList",fabricColorNumberList);
//        return map;
//    }
//
//    @RequestMapping(value = "/getjarnumberhint",method = RequestMethod.GET)
//    @ResponseBody
//    public Map<String,Object> getJarNumberHint(@RequestParam("styleNumber")String styleNumber,
//                                                @RequestParam("fabricNumber")String fabricNumber,
//                                                @RequestParam("fabricColor")String fabricColor){
//        Map<String,Object> map = new HashMap<>();
//        List<String> jarNumberList = fabricService.getJarNumberHint(styleNumber, fabricNumber, fabricColor);
//        map.put("jarNumberList",jarNumberList);
//        return map;
//    }
//    @RequestMapping(value = "/getdeliveryquantityhint",method = RequestMethod.GET)
//    @ResponseBody
//    public Float getReturnFabricCountHint(@RequestParam("styleNumber")String styleNumber,
//                                          @RequestParam("fabricNumber")String fabricNumber,
//                                          @RequestParam("fabricColor")String fabricColor,
//                                          @RequestParam("jarNumber")String jarNumber){
//        Float deliveryQuantity = fabricService.getDeliveryQuantityHint(styleNumber, fabricNumber, fabricColor, jarNumber);
//        return deliveryQuantity;
//    }
//
//    @RequestMapping(value = "/getbatchnumberhint",method = RequestMethod.GET)
//    @ResponseBody
//    public Integer getBatchNumberHint(@RequestParam("styleNumber")String styleNumber,
//                                      @RequestParam("fabricNumber")String fabricNumber,
//                                      @RequestParam("fabricColor")String fabricColor,
//                                      @RequestParam("jarNumber")String jarNumber){
//        Integer batchNumber = fabricService.getBatchNumberHint(styleNumber, fabricNumber, fabricColor, jarNumber);
//        return batchNumber;
//    }
//
//    @RequestMapping(value = "/getseasonhint",method = RequestMethod.GET)
//    @ResponseBody
//    public Map<String,Object> getSeasonHint(@RequestParam("keywords")String keywords){
//        Map<String,Object> map = new HashMap<>();
//        List<String> seasonList = fabricService.getSeasonHint(keywords);
//        map.put("content",seasonList);
//        map.put("code",0);
//        map.put("type","success");
//        return map;
//    }
//
//    @RequestMapping(value = "/getversionnumberbyseason",method = RequestMethod.GET)
//    @ResponseBody
//    public Map<String,Object> getVersionNumberBySeason(@RequestParam("season")String season){
//        Map<String,Object> map = new HashMap<>();
//        List<String> versionNumberList = fabricService.getVersionNumberBySeason(season);
//        map.put("versionNumberList",versionNumberList);
//        return map;
//    }
//
//    @RequestMapping(value = "/getfabricleakquery",method = RequestMethod.GET)
//    public String getFabricLeakQuery(@RequestParam("season")String season,
//                                                 @RequestParam("versionNumber")String versionNumber,
//                                     Model model){
//        List<FabricLeak> fabricLeakList = fabricService.getFabricLeakQuery(season, versionNumber);
//        model.addAttribute("fabricLeakList",fabricLeakList);
//        return "fabric/fb_fabricLeakQueryList";
//    }
//
//    @RequestMapping(value = "/getfabriccolorhint",method = RequestMethod.GET)
//    @ResponseBody
//    public Map<String,Object> getFabricColorHint(@RequestParam("styleNumber")String styleNumber,
//                                                       @RequestParam("fabricNumber")String fabricNumber){
//        Map<String,Object> map = new HashMap<>();
//        List<String> fabricColorList = fabricService.getFabricColorHint(styleNumber, fabricNumber);
//        map.put("fabricColorList",fabricColorList);
//        return map;
//    }
//
//    @RequestMapping(value = "/getversionnumberbysubversion",method = RequestMethod.GET)
//    @ResponseBody
//    public Map<String,Object> getVersionNumberBySubVersion(@RequestParam("keywords")String keywords){
//        Map<String,Object> map = new LinkedHashMap<>();
//        List<String> versionNumberList = fabricService.getVersionNumberBySubVersion(keywords);
//        map.put("content",versionNumberList);
//        map.put("type","success");
//        map.put("code",0);
//        return map;
//    }
//
//    @RequestMapping(value = "/getstylenumbersbyversion",method = RequestMethod.GET)
//    @ResponseBody
//    public Map<String,Object> getStyleNumbersByVersion(@RequestParam("versionNumber")String versionNumber){
//        Map<String,Object> map = new LinkedHashMap<>();
//        List<String> styleNumberList = fabricService.getStyleNumbersByVersion(versionNumber);
//        map.put("styleNumberList",styleNumberList);
//        return map;
//    }
//
//
//    @RequestMapping(value = "/getfabircdetail",method = RequestMethod.GET)
//    @ResponseBody
//    public Map<String,Object> getFabricDetail(@RequestParam("styleNumber")String styleNumber){
//        Map<String, Object> map = new LinkedHashMap<>();
//        List<FabricDifference> fabricDifferenceList = fabricService.getFabricDifference(styleNumber);
//        List<FabricStorage> fabricStorageList = fabricStorageService.getFabricStorageByStyle(styleNumber);
//        List<FabricOutRecord> fabricOutRecordList = fabricOutRecordService.getFabricOutRecordByStyle(styleNumber);
//        if (fabricDifferenceList != null && !fabricDifferenceList.isEmpty()){
//            for (FabricDifference fd : fabricDifferenceList){
//                Float tmpActualCount = 0f;
//                Float tmpOrderCount = 0f;
//                if (fd.getActualCount() != null){
//                    tmpActualCount = fd.getActualCount();
//                }
//                if (fd.getOrderCount() != null){
//                    tmpOrderCount = fd.getOrderCount();
//                }
//                fd.setDifferenceCount(tmpActualCount - tmpOrderCount);
//            }
//        }
//        map.put("difference",fabricDifferenceList);
//        if (fabricStorageList != null && !fabricStorageList.isEmpty()){
//            map.put("storage",fabricStorageList);
//        }
//        if (fabricOutRecordList != null && !fabricOutRecordList.isEmpty()){
//            map.put("out",fabricOutRecordList);
//        }
//        return map;
//    }
//
//    @RequestMapping(value = "/getonemonthfabric",method = RequestMethod.GET)
//    public String getOneMonthFabric(Model model){
//        List<Fabric> fabricList = fabricService.getOneMonthFabric();
//        model.addAttribute("fabricList",fabricList);
//        return "fabric/fb_fabricList";
//    }
//
//    @RequestMapping(value = "/getthreemonthsfabric",method = RequestMethod.GET)
//    public String getThreeMonthsFabric(Model model){
//        List<Fabric> fabricList = fabricService.getThreeMonthsFabric();
//        model.addAttribute("fabricList",fabricList);
//        return "fabric/fb_fabricList";
//    }
//
//
//}
