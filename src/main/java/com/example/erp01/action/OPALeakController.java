package com.example.erp01.action;

import com.alibaba.dubbo.config.annotation.Reference;
import com.example.erp01.model.*;
import com.example.erp01.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
@RequestMapping(value = "erp")
public class OPALeakController {

    @Autowired
    private OPALeakService opaLeakService;
    @Autowired
    private OrderClothesService orderClothesService;
    @Autowired
    private OPAService opaService;
    @Autowired
    private OtherOPAService otherOPAService;
    @Autowired
    private OpaBackInputService opaBackInputService;

    @RequestMapping("/opaReportStart")
    public String opaStart(){
        return "opa/opaReport";
    }

    @RequestMapping("/monthOpaInfoStart")
    public String monthOpaInfoStart(Model model){
        return "opa/monthOpaInfo";
    }

//    @RequestMapping(value = "/opaleaksearch",method = RequestMethod.GET)
//    @ResponseBody
//    public Map<String,Object> getOpaLeakSearch(@RequestParam("orderName")String orderName,
//                                               @RequestParam("from")String from,
//                                               @RequestParam("to")String to){
//        Map<String,Object> map = new LinkedHashMap<>();
//        Map<String, Object> summaryMap = new LinkedHashMap<>();
//        List<String> partNameList = opaLeakService.getOPAPartNamesByOrder(orderName);
//        List<String> otherPartNameList = opaLeakService.getOtherOPAPartNamesByOrder(orderName);
//        List<CutQueryLeak> orderInfoList = orderClothesService.getOrderInfoByName(orderName);
//        if("".equals(from)){
//            from = "1970-01-01";
//        }
//        if("".equals(to)){
//            to = "2050-01-01";
//        }
//        List<String> colorList = new ArrayList<>();
//        List<String> sizeList = new ArrayList<>();
//        for (CutQueryLeak cutQueryLeak : orderInfoList){
//            if (!colorList.contains(cutQueryLeak.getColorName())){
//                colorList.add(cutQueryLeak.getColorName());
//            }
//            if (!sizeList.contains(cutQueryLeak.getSizeName())){
//                sizeList.add(cutQueryLeak.getSizeName());
//            }
//        }
//        List<String> desiredOrder = Arrays.asList("XXXXXS","5XS","XXXXS","4XS","XXXS","3XS","XXS","2XS","XS","XS/25","xs","S","S/27","M","M/29","L","L/31","XL",
//                "XL/33","XXL","2XL","XXL/36","XXXL","3XL","XXXL/38","4XL","XXXXL","5XL","XXXXXL","6XL","XXXXXXL","80","090","90","100","110","110/27",
//                "120","120/29","130","130/31","140","140/33","150","150/36","155","160","165","170","175","180","185","190");
//        Comparator<String> sizeOrder = Comparator.comparingInt(desiredOrder::indexOf);
//        sizeList.sort(sizeOrder);
//        if (otherPartNameList != null && otherPartNameList.size() > 0){
//            for (String otherPartName : otherPartNameList){
//                if (!partNameList.contains(otherPartName)){
//                    partNameList.add(otherPartName);
//                }
//            }
//        }
//        try {
//            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//            Date fromDate = sdf.parse(from);
//            Date toDate = sdf.parse(to);
//            if (partNameList != null && partNameList.size() > 0){
//                for (String partName : partNameList){
//                    //根据花片外发信息获取外发详情
//                    List<OPALeak> opaLeakList1 = new ArrayList<>();
//                    List<Integer> bedNumberList = opaLeakService.getOPABedNumbersByOrderPart(orderName, partName);
//                    for (Integer bedNumber : bedNumberList){
//                        List<OPALeak> tmpOpaLeakList = opaLeakService.getTailorOPA(orderName, partName, bedNumber);
//                        if (tmpOpaLeakList == null || tmpOpaLeakList.size() == 0){
//                            tmpOpaLeakList = opaLeakService.getOtherTailorOPA(orderName, partName, bedNumber);
//                        }
//                        opaLeakList1.addAll(tmpOpaLeakList);
//
//                    }
//
//                    //获取士啤外发详情
//                    List<OPALeak> opaLeakList2 = opaLeakService.getOtherOPA(orderName, partName);
//                    //获取花片回厂详情
//                    List<OPALeak> opaLeakList3 = opaLeakService.getOPABack(orderName, partName,fromDate,toDate);
//                    //获取裁数
//                    List<OPALeak> opaLeakList4 = new ArrayList<>();
//                    opaLeakList4 = opaLeakService.getTailorInfoByOrderPart(orderName, partName);
//                    if (opaLeakList4 == null || opaLeakList4.size() == 0){
//                        opaLeakList4 = opaLeakService.getOtherTailorInfoByOrderPart(orderName, partName);
//                    }
//                    Map<String, Object> colorMap = new LinkedHashMap<>();
//                    for (String color : colorList){
//                        Map<String, Object> sizeMap = new LinkedHashMap<>();
//                        for (String size : sizeList){
//                            OPALeak opaLeak = new OPALeak();
//                            for (CutQueryLeak cutQueryLeak : orderInfoList){
//                                if (color.equals(cutQueryLeak.getColorName()) && size.equals(cutQueryLeak.getSizeName())){
//                                    opaLeak.setOrderCount(cutQueryLeak.getOrderCount());
//                                }
//                            }
//                            for (OPALeak opaLeak1 : opaLeakList1){
//                                if (color.equals(opaLeak1.getColorName()) && size.equals(opaLeak1.getSizeName())){
//                                    opaLeak.setOpaCount(opaLeak.getOpaCount() + opaLeak1.getOpaCount());
//                                }
//                            }
//                            for (OPALeak opaLeak2 : opaLeakList2){
//                                if (color.equals(opaLeak2.getColorName()) && size.equals(opaLeak2.getSizeName())){
//                                    opaLeak.setOtherOPACount(opaLeak.getOtherOPACount() + opaLeak2.getOtherOPACount());
//                                }
//                            }
//                            for (OPALeak opaLeak3 : opaLeakList3){
//                                if (color.equals(opaLeak3.getColorName()) && size.equals(opaLeak3.getSizeName())){
//                                    opaLeak.setOpaBackCount(opaLeak.getOpaBackCount() + opaLeak3.getOpaBackCount());
//                                }
//                            }
//                            for (OPALeak opaLeak4 : opaLeakList4){
//                                if (color.equals(opaLeak4.getColorName()) && size.equals(opaLeak4.getSizeName())){
//                                    opaLeak.setCutCount(opaLeak4.getCutCount());
//                                }
//                            }
//                            opaLeak.setDiffCount(opaLeak.getOpaBackCount() - opaLeak.getOtherOPACount() - opaLeak.getOpaCount());
//                            sizeMap.put(size,opaLeak);
//                        }
//                        colorMap.put(color, sizeMap);
//                    }
//                    summaryMap.put(partName,colorMap);
//                }
//            }
//            map.put("summary",summaryMap);
//            Map<String, Object> partOPAMap = new LinkedHashMap<>();
//            //获取外发的时间
//            List<OPA> opaList = opaLeakService.getOPAByOrderTime(orderName, null, fromDate, toDate);
//            List<OtherOPA> otherOPAList = opaLeakService.getOtherOPAByOrderTime(orderName, null, fromDate, toDate);
//            List<Date> dateList = new ArrayList<>();
//            for (OPA opa : opaList){
//                if (!dateList.contains(opa.getOpaDate())){
//                    dateList.add(opa.getOpaDate());
//                }
//            }
//            for (OtherOPA otherOPA : otherOPAList){
//                if (!dateList.contains(otherOPA.getOpaDate())){
//                    dateList.add(otherOPA.getOpaDate());
//                }
//            }
//            if (partNameList != null && partNameList.size() > 0){
//                for (String partName : partNameList){
//                    Map<String, Object> dateOPAMap = new LinkedHashMap<>();
//                    for (Date date : dateList){
//                        List<OPALeak> opaLeakList5 = new ArrayList<>();
//                        String opaDateString = sdf.format(date);
//                        for (OPA opa : opaList){
//                            if (partName.equals(opa.getPartName()) && date.compareTo(opa.getOpaDate()) == 0){
//                                List<OPALeak> tmpOpaLeakList = opaLeakService.getTailorOPA(orderName, partName, opa.getBedNumber());
//                                if (tmpOpaLeakList == null || tmpOpaLeakList.size() == 0){
//                                    tmpOpaLeakList = opaLeakService.getOtherTailorOPA(orderName, partName, opa.getBedNumber());
//                                }
//                                opaLeakList5.addAll(tmpOpaLeakList);
//                            }
//                        }
//                        for (OtherOPA otherOPA : otherOPAList){
//                            if (partName.equals(otherOPA.getPartName()) && date.compareTo(otherOPA.getOpaDate()) == 0){
//                                OPALeak opaLeak7 = new OPALeak(otherOPA.getOrderName(),otherOPA.getClothesVersionNumber(),0,partName,otherOPA.getColorName(),otherOPA.getSizeName(),0,0,0,otherOPA.getOpaCount(),0,0,0);
//                                opaLeakList5.add(opaLeak7);
//                            }
//                        }
//
//                        Map<String, Object> colorOPAMap = new LinkedHashMap<>();
//                        for (String color : colorList){
//                            boolean colorFlag = false;
//                            Map<String, Object> sizeOPAMap = new LinkedHashMap<>();
//                            for (String size : sizeList){
//                                OPALeak opaLeak = new OPALeak();
//                                for (OPALeak opaLeak5 : opaLeakList5){
//                                    if (color.equals(opaLeak5.getColorName()) && size.equals(opaLeak5.getSizeName())){
//                                        opaLeak.setOpaCount(opaLeak.getOpaCount() + opaLeak5.getOpaCount());
//                                        colorFlag = true;
//                                    }
//                                }
//                                sizeOPAMap.put(size, opaLeak);
//                            }
//                            if (colorFlag){
//                                colorOPAMap.put(color, sizeOPAMap);
//                            }
//                        }
//                        dateOPAMap.put(opaDateString, colorOPAMap);
//                    }
//                    partOPAMap.put(partName, dateOPAMap);
//                }
//            }
//            map.put("opa",partOPAMap);
//            Map<String, Object> partOPABackMap = new LinkedHashMap<>();
//            List<Date> opaBackInputDateList = new ArrayList<>();
//            List<String> opaBackPartNameList = new ArrayList<>();
//            List<OpaBackInput> opaBackInputList = opaLeakService.getOPABackInputByOrderTime(orderName, null, fromDate, toDate);
//            for (OpaBackInput opaBackInput : opaBackInputList){
//                if (!opaBackInputDateList.contains(opaBackInput.getOpaBackDate())){
//                    opaBackInputDateList.add(opaBackInput.getOpaBackDate());
//                }
//                if (!opaBackPartNameList.contains(opaBackInput.getPartName())){
//                    opaBackPartNameList.add(opaBackInput.getPartName());
//                }
//            }
//            for (String partName : opaBackPartNameList){
//                Map<String, Object> dateOPABackMap = new LinkedHashMap<>();
//                for (Date date : opaBackInputDateList){
//                    boolean dateFlag = false;
//                    String opaDateString = sdf.format(date);
//                    Map<String, Object> colorOPABackMap = new LinkedHashMap<>();
//                    for (String color : colorList){
//                        boolean backColorFlag = false;
//                        Map<String, Object> sizeOPABackMap = new LinkedHashMap<>();
//                        for (String size : sizeList){
//                            OPALeak opaLeak = new OPALeak();
//                            for (OpaBackInput opaBackInput : opaBackInputList){
//                                if (color.equals(opaBackInput.getColorName()) && size.equals(opaBackInput.getSizeName()) && partName.equals(opaBackInput.getPartName()) && date.compareTo(opaBackInput.getOpaBackDate()) == 0){
//                                    backColorFlag = true;
//                                    dateFlag = true;
//                                    opaLeak.setOpaBackCount(opaLeak.getOpaBackCount() + opaBackInput.getOpaBackCount());
//                                }
//                            }
//                            sizeOPABackMap.put(size, opaLeak);
//                        }
//                        if (backColorFlag){
//                            colorOPABackMap.put(color, sizeOPABackMap);
//                        }
//                    }
//                    if (dateFlag){
//                        dateOPABackMap.put(opaDateString, colorOPABackMap);
//                    }
//                }
//                partOPABackMap.put(partName, dateOPABackMap);
//            }
//            map.put("opaBack",partOPABackMap);
//            return map;
//        }catch (ParseException e){
//            e.printStackTrace();
//        }
//        return map;
//    }
//
//
//    @RequestMapping(value = "/opabedleaksearch",method = RequestMethod.GET)
//    @ResponseBody
//    public Map<String,Object> getOpaBedLeakSearch(@RequestParam("orderName")String orderName){
//        Map<String,Object> map = new LinkedHashMap<>();
//        List<OPA> opaList = opaService.getBedOPAByOrder(orderName);
//        List<OtherOPA> otherOPAList = otherOPAService.getOtherBedOPAByOrder(orderName);
//        List<OpaBackInput> opaBackInputList = opaBackInputService.getBedOpaBackInputByOrder(orderName);
//        List<OPALeak> opaLeakList = new ArrayList<>();
//        if (opaList != null && opaList.size() > 0){
//            for (OPA opa : opaList){
//                Integer bedNumber = opa.getBedNumber();
//                String partName = opa.getPartName();
//                Integer opaBackCount = 0;
//
//                for (OpaBackInput opaBackInput : opaBackInputList){
//                    if (bedNumber.equals(opaBackInput.getBedNumber()) && partName.equals(opaBackInput.getPartName())){
//                        opaBackCount += opaBackInput.getOpaBackCount();
//                    }
//                }
//                OPALeak opaLeak = new OPALeak(orderName, opa.getClothesVersionNumber(), bedNumber, partName, opa.getOpaCount(), opaBackCount, opaBackCount - opa.getOpaCount());
//                opaLeakList.add(opaLeak);
//            }
//        }
//        map.put("opa",opaLeakList);
//        List<OPALeak> otherOpaLeakList = new ArrayList<>();
//        if (otherOPAList != null && otherOPAList.size() > 0){
//            for (OtherOPA otherOPA : otherOPAList){
//                String partName = otherOPA.getPartName();
//                Integer otherOpaBackCount = 0;
//                for (OpaBackInput opaBackInput : opaBackInputList){
//                    if (opaBackInput.getBedNumber().equals(0) && partName.equals(opaBackInput.getPartName())){
//                        otherOpaBackCount += opaBackInput.getOpaBackCount();
//                    }
//                }
//                OPALeak opaLeak = new OPALeak(orderName, otherOPA.getClothesVersionNumber(), 0, partName, otherOPA.getOpaCount(), otherOpaBackCount, otherOpaBackCount - otherOPA.getOpaCount());
//                otherOpaLeakList.add(opaLeak);
//            }
//        }
//        map.put("otherOpa", otherOpaLeakList);
//        return map;
//    }
//
//
//
//
//    @RequestMapping(value = "/opamonthreport",method = RequestMethod.GET)
//    @ResponseBody
//    public Map<String,Object> getPartNamesByOrderName(@RequestParam("from")String from,
//                                                      @RequestParam("to")String to){
//        Map<String,Object> map = new LinkedHashMap<>();
//        try {
//            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//            Date fromDate = sdf.parse(from);
//            Date toDate = sdf.parse(to);
//            List<OPALeak> opaLeakList1 = opaLeakService.getMonthOPAInfo(fromDate, toDate);
//            List<OPALeak> opaLeakList2 = opaLeakService.getOtherMonthOPAInfo(fromDate, toDate);
//            List<OPALeak> opaLeakList3 = opaLeakService.getMonthOPABackInfo(fromDate, toDate);
//            for (OPALeak opaLeak1 : opaLeakList1){
//                String orderName = opaLeak1.getOrderName();
//                String partName = opaLeak1.getPartName();
//                List<OPALeak> opaLeakList4 = opaLeakService.getTailorCountByOrderPart(orderName, partName);
//                if (null == opaLeakList4 || opaLeakList4.size() == 0){
//                    opaLeakList4 = opaLeakService.getOtherTailorCountByOrderPart(orderName, partName);
//                }
//                opaLeak1.setCutCount(opaLeakList4.get(0).getCutCount());
//                opaLeak1.setOrderCount(opaLeakList4.get(0).getOrderCount());
//                opaLeak1.setWellCount(opaLeakList4.get(0).getWellCount());
//                for (OPALeak opaLeak2 : opaLeakList2){
//                    if (opaLeak2.getOrderName().equals(opaLeak1.getOrderName()) && opaLeak2.getPartName().equals(opaLeak1.getPartName())){
//                        opaLeak1.setOpaCount(opaLeak1.getOpaCount() + opaLeak2.getOpaCount());
//                    }
//                }
//                for (OPALeak opaLeak3 : opaLeakList3){
//                    if (opaLeak3.getOrderName().equals(opaLeak1.getOrderName()) && opaLeak3.getPartName().equals(opaLeak1.getPartName())){
//                        opaLeak1.setOpaBackCount(opaLeak3.getOpaBackCount());
//                    }
//                }
//            }
//            map.put("opaMonthInfo",opaLeakList1);
//            return map;
//        }catch (ParseException e){
//            e.printStackTrace();
//        }
//        return map;
//    }




//    @RequestMapping(value = "/xhloutboundsearch",method = RequestMethod.GET)
//    @ResponseBody
//    public Map<String,Object> xhlOutBoundSearch(@RequestParam("orderName")String orderName){
//        List<>
//    }



}
