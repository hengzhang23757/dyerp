package com.example.erp01.action;

import com.example.erp01.model.*;
import com.example.erp01.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

@Controller
@RequestMapping(value = "erp")
public class CostAccountController {

    @Autowired
    private ManufactureOrderService manufactureOrderService;
    @Autowired
    private ManufactureFabricService manufactureFabricService;
    @Autowired
    private ManufactureAccessoryService manufactureAccessoryService;
    @Autowired
    private OrderClothesService orderClothesService;
    @Autowired
    private OrderProcedureService orderProcedureService;
    @Autowired
    private FabricDetailService fabricDetailService;
    @Autowired
    private AccessoryInStoreService accessoryInStoreService;
    @Autowired
    private TailorService tailorService;
    @Autowired
    private CostAccountService costAccountService;
    @Autowired
    private EndProductService endProductService;
    @Autowired
    private FabricOutRecordService fabricOutRecordService;

    @RequestMapping("/costAccountStart")
    public String costAccountStart(){
        return "finance/costAccount";
    }

    @RequestMapping("/costBalanceStart")
    public String costBalanceStart(){
        return "cost/costBalance";
    }

    @RequestMapping(value = "/getcostaccountbyorder",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getCostAccountByOrder(@RequestParam("orderName")String orderName,
                                                     @RequestParam("pieceCost")Float pieceCost){
        Map<String, Object> map = new LinkedHashMap<>();
        ManufactureOrder manufactureOrder = manufactureOrderService.getManufactureOrderByOrderName(orderName);
        List<ManufactureFabric> manufactureFabricList = manufactureFabricService.getManufactureFabricByOrder(orderName, null);
        List<ManufactureAccessory> manufactureAccessoryList = manufactureAccessoryService.getManufactureAccessoryByOrder(orderName, null);
        List<FabricDetail> fabricDetailList = fabricDetailService.getFabricDetailByOrder(orderName);
        List<AccessoryInStore> accessoryInStoreList = accessoryInStoreService.getAccessoryInStoreByOrder(orderName);
        Integer orderCount = orderClothesService.getOrderTotalCount(orderName);
        float customerTotalCost = pieceCost * orderCount;
        String clothesVersionNumber = manufactureOrder.getClothesVersionNumber();
        String customerName = manufactureOrder.getCustomerName();
        map.put("customerName", customerName);
        map.put("clothesVersionNumber", clothesVersionNumber);
        map.put("orderName", orderName);
        map.put("orderCount", orderCount);
        map.put("pieceCost", pieceCost);
        map.put("customerTotalCost", customerTotalCost);
        float totalFabricCost = 0;
        float totalAccessoryCost = 0;
        for (FabricDetail fabricDetail : fabricDetailList){
            totalFabricCost += (fabricDetail.getWeight() * fabricDetail.getPrice());
        }
        for (AccessoryInStore accessoryInStore : accessoryInStoreList){
            totalAccessoryCost += (accessoryInStore.getPrice() * accessoryInStore.getInStoreCount());
        }
        float pieceFabricCost = totalFabricCost/orderCount;
        float pieceAccessoryCost = totalAccessoryCost/orderCount;
        map.put("totalFabricCost", totalFabricCost);
        map.put("pieceFabricCost", pieceFabricCost);
        map.put("totalAccessoryCost", totalAccessoryCost);
        map.put("pieceAccessoryCost", pieceAccessoryCost);
        double cutPrice = orderProcedureService.getSectionPriceByOrder(orderName, "裁床");
        double sewPrice = orderProcedureService.getSectionPriceByOrder(orderName, "车缝");
        double finishPrice = orderProcedureService.getSectionPriceByOrder(orderName, "后整");
        double cutSumPrice = cutPrice * orderCount * 1.1;
        double sewSumPrice = sewPrice * orderCount * 1.1;
        double finishSumPrice = finishPrice * orderCount * 1.1;
        map.put("cutSumPrice", cutSumPrice);
        map.put("sewSumPrice", sewSumPrice);
        map.put("finishSumPrice", finishSumPrice);
        double pieceTotalCost = cutPrice + sewPrice + finishPrice + pieceFabricCost + pieceAccessoryCost;
        double pieceProcessCost = cutPrice + sewPrice + finishPrice;
        double pieceFabricAccessoryCost = pieceFabricCost + pieceAccessoryCost;
        double magnification = pieceCost/pieceTotalCost;
        map.put("pieceTotalCost", pieceTotalCost);
        map.put("pieceProcessCost", pieceProcessCost);
        map.put("pieceFabricAccessoryCost", pieceFabricAccessoryCost);
        map.put("magnification", magnification);
        return map;
    }


    @RequestMapping(value = "/getcostbalancesummarybyorder",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getCostBalanceSummaryByOrder(@RequestParam("orderName")String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        int orderCount = orderClothesService.getOrderTotalCount(orderName);
        int tailorCount = tailorService.getTailorCountByInfo(orderName, null, null, null, null, null, 0);
        int wellCount = tailorService.getWellCountByInfo(orderName, null, null, null, null, null, 0);
        float fabricCost = costAccountService.getFabricCostByOrder(orderName);
        float fabricDeductionCost = costAccountService.getFabricDeductionCostByOrder(orderName);
//        float accessoryCost = costAccountService.getAccessoryCostByOrder(orderName);
        List<OrderProcedure> orderProcedureList = orderProcedureService.getOrderProcedureByOrder(orderName);
        List<GeneralSalary> pieceWorkList = costAccountService.getPieceWorkSummaryByOrder(orderName);
        List<GeneralSalary> hourEmpList = costAccountService.getHourEmpSummaryByOrder(orderName);
        List<EndProduct> endProductList = endProductService.getEndProductInStoreInfo(orderName, "正品");
        List<AccessoryInStore> accessoryInStoreList = accessoryInStoreService.getAccessoryInStoreByOrder(orderName);
        List<ManufactureAccessory> manufactureAccessoryList = manufactureAccessoryService.getManufactureAccessoryByOrder(orderName, null);
        float accessoryCost = 0f;
        for (AccessoryInStore accessoryInStore : accessoryInStoreList){
            if (accessoryInStore.getInStoreType().equals("分款")){
                float price = 0f;
                for (ManufactureAccessory manufactureAccessory : manufactureAccessoryList){
                    if (accessoryInStore.getAccessoryID().equals(manufactureAccessory.getAccessoryID())){
                        price = manufactureAccessory.getPrice();
                    }
                }
                accessoryCost += accessoryInStore.getInStoreCount() * price;
            } else {
                accessoryCost += accessoryInStore.getInStoreCount() * accessoryInStore.getPrice();
            }

        }
        float cutCost = 0f;
        float sewCost = 0f;
        float hourCost = 0f;
        float printCost = 0f;
        int endProductCount = 0;
        for (OrderProcedure orderProcedure : orderProcedureList){
            if (orderProcedure.getProcedureNumber().equals(2000)){
                cutCost = tailorCount * (float)(orderProcedure.getPiecePrice() + orderProcedure.getPiecePriceTwo());
            }
            for (GeneralSalary pieceWork : pieceWorkList){
                if (orderProcedure.getProcedureNumber().equals(pieceWork.getProcedureNumber())){
                    sewCost += pieceWork.getPieceCount() * ((float)(orderProcedure.getPiecePrice() + orderProcedure.getPiecePriceTwo())*(1+orderProcedure.getSubsidy()));
                }
            }
        }
        for (GeneralSalary hourEmp : hourEmpList){
            hourCost += hourEmp.getPieceCount() * 10f;
        }
        for (EndProduct endProduct : endProductList){
            endProductCount += endProduct.getInStoreCount();
        }
        CostAccount costAccount = new CostAccount();
        costAccount.setCutCost(cutCost);
        costAccount.setOrderCount(orderCount);
        costAccount.setCutCount(tailorCount);
        costAccount.setWellCount(orderCount);
        costAccount.setEndProductCount(endProductCount);
        costAccount.setFabricCost(fabricCost - fabricDeductionCost);
        costAccount.setAccessoryCost(accessoryCost);
        costAccount.setSewCost(sewCost + hourCost);
        costAccount.setPrintCost(printCost);
        if (endProductCount > 0){
            costAccount.setAvgFabricCost(costAccount.getFabricCost()/endProductCount);
            costAccount.setAvgAccessoryCost(costAccount.getAccessoryCost()/endProductCount);
            costAccount.setAvgSewCost(costAccount.getSewCost()/endProductCount);
            costAccount.setAvgPrintCost(costAccount.getPrintCost()/endProductCount);
            costAccount.setAvgCutCost(costAccount.getCutCost()/endProductCount);
        }
        map.put("costAccount", costAccount);
        return map;
    }

    @RequestMapping(value = "/getcostbalancesummarybyinstore",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getCostBalanceSummaryByInStore(@RequestParam("orderName")String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        int orderCount = orderClothesService.getOrderTotalCount(orderName);
        int tailorCount = tailorService.getTailorCountByInfo(orderName, null, null, null, null, null, 0);
        int wellCount = tailorService.getWellCountByInfo(orderName, null, null, null, null, null, 0);
        float fabricCost = costAccountService.getFabricOrderCost(orderName);
        float accessoryCost = costAccountService.getAccessoryOrderCost(orderName);
        float fabricAddFee = costAccountService.getFabricAddFee(orderName);
        float accessoryAddFee = costAccountService.getAccessoryAddFee(orderName);
        List<OrderProcedure> orderProcedureList = orderProcedureService.getOrderProcedureByOrder(orderName);
        List<GeneralSalary> pieceWorkList = costAccountService.getPieceWorkSummaryByOrder(orderName);
        List<GeneralSalary> hourEmpList = costAccountService.getHourEmpSummaryByOrder(orderName);
        List<EndProduct> endProductList = endProductService.getEndProductInStoreInfo(orderName, "正品");
        float cutCost = 0f;
        float sewCost = 0f;
        float hourCost = 0f;
        float printCost = 0f;
        int endProductCount = 0;
        for (OrderProcedure orderProcedure : orderProcedureList){
            if (orderProcedure.getProcedureNumber().equals(2000)){
                cutCost = tailorCount * (float)(orderProcedure.getPiecePrice() + orderProcedure.getPiecePriceTwo());
            }
            for (GeneralSalary pieceWork : pieceWorkList){
                if (orderProcedure.getProcedureNumber().equals(pieceWork.getProcedureNumber())){
                    sewCost += pieceWork.getPieceCount() * ((float)(orderProcedure.getPiecePrice() + orderProcedure.getPiecePriceTwo())*(1+orderProcedure.getSubsidy()));
                }
            }
        }
        for (GeneralSalary hourEmp : hourEmpList){
            hourCost += hourEmp.getPieceCount() * 10f;
        }
        for (EndProduct endProduct : endProductList){
            endProductCount += endProduct.getInStoreCount();
        }
        CostAccount costAccount = new CostAccount();
        costAccount.setCutCost(cutCost);
        costAccount.setOrderCount(orderCount);
        costAccount.setCutCount(tailorCount);
        costAccount.setWellCount(orderCount);
        costAccount.setEndProductCount(endProductCount);
        costAccount.setFabricCost(fabricCost + fabricAddFee);
        costAccount.setAccessoryCost(accessoryCost + accessoryAddFee);
        costAccount.setSewCost(sewCost + hourCost);
        costAccount.setPrintCost(printCost);
        if (endProductCount > 0){
            costAccount.setAvgFabricCost(costAccount.getFabricCost()/endProductCount);
            costAccount.setAvgAccessoryCost(costAccount.getAccessoryCost()/endProductCount);
            costAccount.setAvgSewCost(costAccount.getSewCost()/endProductCount);
            costAccount.setAvgPrintCost(costAccount.getPrintCost()/endProductCount);
            costAccount.setAvgCutCost(costAccount.getCutCost()/endProductCount);
        }
        map.put("costAccount", costAccount);
        return map;
    }

    @RequestMapping(value = "/getCostBalanceOrderSummary",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getCostBalanceOrderSummary(@RequestParam("orderName")String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        int orderCount = orderClothesService.getOrderTotalCount(orderName);
        int tailorCount = tailorService.getTailorCountByInfo(orderName, null, null, null, null, null, 0);
        float fabricCost = costAccountService.getFabricOrderCost(orderName);
        float accessoryCost = costAccountService.getAccessoryOrderCost(orderName);
        float fabricCustomerCost = costAccountService.getFabricCustomerCost(orderName);
        float accessoryCustomerCost = costAccountService.getAccessoryCustomerCost(orderName);
        float fabricAddFee = costAccountService.getFabricAddFee(orderName);
        float accessoryAddFee = costAccountService.getAccessoryAddFee(orderName);
        List<ManufactureFabric> manufactureFabricList = manufactureFabricService.getManufactureFabricByOrder(orderName, null);
        List<ManufactureAccessory> manufactureAccessoryList = manufactureAccessoryService.getManufactureAccessoryByOrder(orderName, null);
        List<FabricDetail> fabricDetailList = fabricDetailService.getFabricDetailByOrder(orderName);
        List<FabricOutRecord> fabricOutRecordList = fabricOutRecordService.getFabricOutRecordByOrder(orderName);
        List<AccessoryInStore> accessoryInStoreList = accessoryInStoreService.getAccessoryInStoreByOrder(orderName);
        float fabricReturnCost = 0f;
        float accessoryReturnCost = 0f;
        for (FabricDetail fabricDetail : fabricDetailList){
            for (ManufactureFabric manufactureFabric : manufactureFabricList){
                if (fabricDetail.getFabricID().equals(manufactureFabric.getFabricID())){
                    fabricReturnCost += fabricDetail.getWeight() * manufactureFabric.getPrice();
                }
            }
        }
        for (FabricOutRecord fabricOutRecord : fabricOutRecordList){
            if ("退面料厂".equals(fabricOutRecord.getOperateType())){
                for (ManufactureFabric manufactureFabric : manufactureFabricList){
                    if (fabricOutRecord.getFabricID().equals(manufactureFabric.getFabricID())){
                        fabricReturnCost -= fabricOutRecord.getWeight() * manufactureFabric.getPrice();
                    }
                }
            }
        }
        for (AccessoryInStore accessoryInStore : accessoryInStoreList){
            for (ManufactureAccessory manufactureAccessory : manufactureAccessoryList){
                if (accessoryInStore.getAccessoryID().equals(manufactureAccessory.getAccessoryID())){
                    accessoryReturnCost += accessoryInStore.getAccessoryCount() * manufactureAccessory.getPrice();
                }
            }
        }
        List<OrderProcedure> orderProcedureList = orderProcedureService.getOrderProcedureByOrder(orderName);
        List<GeneralSalary> pieceWorkList = costAccountService.getPieceWorkSummaryByOrder(orderName);
        List<GeneralSalary> hourEmpList = costAccountService.getHourEmpSummaryByOrder(orderName);
        List<EndProduct> endProductList = endProductService.getEndProductInStoreInfo(orderName, "正品");
        float cutCost = 0f;
        float sewCost = 0f;
        float hourCost = 0f;
        float printCost = 0f;
        int endProductCount = 0;
        for (OrderProcedure orderProcedure : orderProcedureList){
            if (orderProcedure.getProcedureNumber().equals(2000)){
                cutCost = tailorCount * (float)(orderProcedure.getPiecePrice() + orderProcedure.getPiecePriceTwo());
            }
            for (GeneralSalary pieceWork : pieceWorkList){
                if (orderProcedure.getProcedureNumber().equals(pieceWork.getProcedureNumber())){
                    sewCost += pieceWork.getPieceCount() * ((float)(orderProcedure.getPiecePrice() + orderProcedure.getPiecePriceTwo())*(1+orderProcedure.getSubsidy()));
                }
            }
        }
        for (GeneralSalary hourEmp : hourEmpList){
            hourCost += hourEmp.getPieceCount() * 10f;
        }
        for (EndProduct endProduct : endProductList){
            endProductCount += endProduct.getInStoreCount();
        }
        CostAccount costAccount = new CostAccount();
        costAccount.setCutCost(cutCost);
        costAccount.setOrderCount(orderCount);
        costAccount.setCutCount(tailorCount);
        costAccount.setWellCount(orderCount);
        costAccount.setEndProductCount(endProductCount);
        costAccount.setFabricCost(fabricCost + fabricAddFee);
        costAccount.setFabricCustomerCost(fabricCustomerCost);
        costAccount.setAccessoryCustomerCost(accessoryCustomerCost);
        costAccount.setFabricReturnCost(fabricReturnCost + fabricAddFee);
        costAccount.setAccessoryReturnCost(accessoryReturnCost + accessoryAddFee);
        costAccount.setAccessoryCost(accessoryCost + accessoryAddFee);
        costAccount.setSewCost(sewCost + hourCost);
        costAccount.setPrintCost(printCost);
        if (endProductCount > 0){
            costAccount.setAvgFabricCost(costAccount.getFabricReturnCost()/endProductCount);
            costAccount.setAvgAccessoryCost(costAccount.getAccessoryReturnCost()/endProductCount);
            costAccount.setAvgSewCost(costAccount.getSewCost()/endProductCount);
            costAccount.setAvgPrintCost(costAccount.getPrintCost()/endProductCount);
            costAccount.setAvgCutCost(costAccount.getCutCost()/endProductCount);
        }
        map.put("costAccount", costAccount);
        return map;
    }


    @RequestMapping(value = "/getcostdetailbyorderitem",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getCostDetailByOrderItem(@RequestParam("orderName")String orderName,
                                                        @RequestParam("itemName")String itemName){
        Map<String, Object> map = new LinkedHashMap<>();
        if (itemName.equals("面料金额")){
            List<FabricDetail> fabricDetailList = fabricDetailService.getFabricDetailByOrder(orderName);
            List<ManufactureFabric> manufactureFabricList = manufactureFabricService.getManufactureFabricByOrder(orderName, null);
            for (FabricDetail fabricDetail : fabricDetailList){
                float price = 0f;
                for (ManufactureFabric manufactureFabric : manufactureFabricList){
                    if (fabricDetail.getFabricID().equals(manufactureFabric.getFabricID())){
                        price = manufactureFabric.getPrice();
                    }
                }
                fabricDetail.setPrice(price);
                fabricDetail.setReturnMoney(fabricDetail.getWeight() * price);
            }
            Map<String, Object> param = new HashMap<>();
            param.put("orderName", orderName);
            param.put("operateType", "退面料厂");
            List<FabricOutRecord> fabricOutRecordList = fabricOutRecordService.getFabricOutRecordByMap(param);
            for (FabricOutRecord fabricOutRecord : fabricOutRecordList){
                float price = 0f;
                for (ManufactureFabric manufactureFabric : manufactureFabricList){
                    if (fabricOutRecord.getFabricID().equals(manufactureFabric.getFabricID())){
                        price = manufactureFabric.getPrice();
                    }
                }
                FabricDetail fabricDetail = new FabricDetail();
                fabricDetail.setFabricName(fabricOutRecord.getFabricName());
                fabricDetail.setColorName(fabricOutRecord.getColorName());
                fabricDetail.setFabricColor(fabricOutRecord.getFabricColor());
                fabricDetail.setJarName(fabricOutRecord.getJarName());
                fabricDetail.setUnit(fabricOutRecord.getUnit());
                fabricDetail.setBatchNumber(-fabricOutRecord.getBatchNumber());
                fabricDetail.setWeight(-fabricOutRecord.getWeight());
                fabricDetail.setPrice(price);
                fabricDetail.setReturnMoney(fabricDetail.getWeight() * price);
                fabricDetailList.add(fabricDetail);
            }
            map.put("data", fabricDetailList);
        }
        else if (itemName.equals("辅料金额")){
            List<AccessoryInStore> accessoryInStoreList = accessoryInStoreService.getAccessoryInStoreByOrder(orderName);
            List<ManufactureAccessory> manufactureAccessoryList = manufactureAccessoryService.getManufactureAccessoryByOrder(orderName, null);
            for (AccessoryInStore accessoryInStore : accessoryInStoreList){
                if (accessoryInStore.getInStoreType().equals("分款")){
                    float price = 0f;
                    for (ManufactureAccessory manufactureAccessory : manufactureAccessoryList){
                        if (accessoryInStore.getAccessoryID().equals(manufactureAccessory.getAccessoryID())){
                            price = manufactureAccessory.getPrice();
                        }
                    }
                    accessoryInStore.setReturnMoney(accessoryInStore.getInStoreCount() * price);
                } else {
                    accessoryInStore.setReturnMoney(accessoryInStore.getInStoreCount() * accessoryInStore.getPrice());
                }

            }
            map.put("data", accessoryInStoreList);
        }
        else if (itemName.equals("裁剪金额")){
            int cutCount = tailorService.getTailorCountByInfo(orderName, null, null, null, null, null, 0);
            List<CostAccount> costAccountList = new ArrayList<>();
            List<OrderProcedure> orderProcedureList = orderProcedureService.getOrderProcedureByOrder(orderName);
            float cutPrice = 0f;
            for (OrderProcedure orderProcedure : orderProcedureList){
                if (orderProcedure.getProcedureNumber().equals(2000)){
                    cutPrice = (float)(orderProcedure.getPiecePrice() + orderProcedure.getPiecePriceTwo());
                }
            }
            CostAccount costAccount = new CostAccount();
            costAccount.setCutCount(cutCount);
            costAccount.setCutPrice(cutPrice);
            costAccount.setSumMoney(cutCount * cutPrice);
            costAccountList.add(costAccount);
            map.put("data", costAccountList);
        }
        else if (itemName.equals("车缝后整金额")){
            List<GeneralSalary> pieceWorkList = costAccountService.getPieceWorkSummaryByOrder(orderName);
            List<GeneralSalary> hourEmpList = costAccountService.getHourEmpSummaryByOrder(orderName);
            List<OrderProcedure> orderProcedureList = orderProcedureService.getOrderProcedureByOrder(orderName);
            for (OrderProcedure orderProcedure : orderProcedureList){
                for (GeneralSalary pieceWork : pieceWorkList){
                    if (orderProcedure.getProcedureNumber().equals(pieceWork.getProcedureNumber())){
                        pieceWork.setPrice((float)orderProcedure.getPiecePrice());
                        pieceWork.setPriceTwo((float)orderProcedure.getPiecePriceTwo());
                        pieceWork.setSalarySum(pieceWork.getPieceCount() * (pieceWork.getPrice() + pieceWork.getPriceTwo()));
                    }
                }
            }
            for (GeneralSalary hourEmp : hourEmpList){
                hourEmp.setPrice(10f);
                hourEmp.setSalarySum(hourEmp.getPieceCount() * 10f);
            }
            pieceWorkList.addAll(hourEmpList);
            map.put("data", pieceWorkList);
        }
        else if (itemName.equals("印绣花金额")){
            map.put("data", new ArrayList<String>());
        }
        return map;
    }

    @RequestMapping(value = "/getcostdetailbyorderitemfororder",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getCostDetailByOrderItemForOrder(@RequestParam("orderName")String orderName,
                                                                @RequestParam("itemName")String itemName){
        Map<String, Object> map = new LinkedHashMap<>();
        if (itemName.equals("面料金额")){
            List<ManufactureFabric> manufactureFabricList = manufactureFabricService.getManufactureFabricByOrder(orderName, null);
            map.put("data", manufactureFabricList);
        } else if (itemName.equals("辅料金额")){
            List<ManufactureAccessory> manufactureAccessoryList = manufactureAccessoryService.getManufactureAccessoryByOrder(orderName, null);
            map.put("data", manufactureAccessoryList);
        } else if (itemName.equals("裁剪金额")){
            int cutCount = tailorService.getTailorCountByInfo(orderName, null, null, null, null, null, 0);
            List<CostAccount> costAccountList = new ArrayList<>();
            List<OrderProcedure> orderProcedureList = orderProcedureService.getOrderProcedureByOrder(orderName);
            float cutPrice = 0f;
            for (OrderProcedure orderProcedure : orderProcedureList){
                if (orderProcedure.getProcedureNumber().equals(2000)){
                    cutPrice = (float)(orderProcedure.getPiecePrice() + orderProcedure.getPiecePriceTwo());
                }
            }
            CostAccount costAccount = new CostAccount();
            costAccount.setCutCount(cutCount);
            costAccount.setCutPrice(cutPrice);
            costAccount.setSumMoney(cutCount * cutPrice);
            costAccountList.add(costAccount);
            map.put("data", costAccountList);
        } else if (itemName.equals("车缝后整金额")){
            List<GeneralSalary> pieceWorkList = costAccountService.getPieceWorkSummaryByOrder(orderName);
            List<GeneralSalary> hourEmpList = costAccountService.getHourEmpSummaryByOrder(orderName);
            List<OrderProcedure> orderProcedureList = orderProcedureService.getOrderProcedureByOrder(orderName);
            for (OrderProcedure orderProcedure : orderProcedureList){
                for (GeneralSalary pieceWork : pieceWorkList){
                    if (orderProcedure.getProcedureNumber().equals(pieceWork.getProcedureNumber())){
                        pieceWork.setPrice((float)orderProcedure.getPiecePrice());
                        pieceWork.setAddition(orderProcedure.getSubsidy());
                        pieceWork.setPriceTwo((float)orderProcedure.getPiecePriceTwo());
                        pieceWork.setSumPrice((float)(orderProcedure.getPiecePrice() + orderProcedure.getPiecePriceTwo())*(1+orderProcedure.getSubsidy()));
                        pieceWork.setSalarySum(pieceWork.getPieceCount() * pieceWork.getSumPrice());
                    }
                }
            }
            for (GeneralSalary hourEmp : hourEmpList){
                hourEmp.setPrice(10f);
                hourEmp.setSalarySum(hourEmp.getPieceCount() * 10f);
            }
            pieceWorkList.addAll(hourEmpList);
            map.put("data", pieceWorkList);
        } else if (itemName.equals("印绣花金额")){
            map.put("data", new ArrayList<String>());
        }
        return map;
    }


    @RequestMapping(value = "/getCostDetailBySummaryItem",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getCostDetailBySummaryItem(@RequestParam("orderName")String orderName,
                                                          @RequestParam("itemName")String itemName){
        Map<String, Object> map = new LinkedHashMap<>();
        if ("面料金额".equals(itemName)){
            List<FabricDetail> fabricDetailList = fabricDetailService.getFabricDetailByOrder(orderName);
            List<ManufactureFabric> manufactureFabricList = manufactureFabricService.getManufactureFabricByOrder(orderName, null);
            Map<String, Object> param = new HashMap<>();
            param.put("orderName", orderName);
            param.put("operateType", "退面料厂");
            List<FabricOutRecord> fabricOutRecordList = fabricOutRecordService.getFabricOutRecordByMap(param);
            for (ManufactureFabric manufactureFabric : manufactureFabricList){
                float returnCount = 0f;
                float returnMoney = 0f;
                manufactureFabric.setFabricCount(manufactureFabric.getOrderPieceUsage() * manufactureFabric.getOrderCount());
                manufactureFabric.setOrderMoney(manufactureFabric.getFabricCount() * manufactureFabric.getOrderPrice());
                for (FabricDetail fabricDetail : fabricDetailList){
                    if (fabricDetail.getFabricID().equals(manufactureFabric.getFabricID())){
                        returnCount += fabricDetail.getWeight();
                        returnMoney += fabricDetail.getWeight() * manufactureFabric.getPrice();
                    }
                }
                for (FabricOutRecord fabricOutRecord : fabricOutRecordList){
                    if (fabricOutRecord.getFabricID().equals(manufactureFabric.getFabricID())){
                        returnCount -= fabricOutRecord.getWeight();
                        returnMoney -= fabricOutRecord.getWeight() * manufactureFabric.getPrice();
                    }
                }
                manufactureFabric.setFabricReturn(returnCount);
                manufactureFabric.setReturnSumMoney(returnMoney);
            }
            map.put("data", manufactureFabricList);
        }
        else if ("辅料金额".equals(itemName)){
            List<ManufactureAccessory> manufactureAccessoryList = manufactureAccessoryService.getManufactureAccessoryByOrder(orderName, null);
            List<String> nameList = new ArrayList<>();
            List<ManufactureAccessory> destList = new ArrayList<>();
            for (ManufactureAccessory manufactureAccessory : manufactureAccessoryList){
                if (!nameList.contains(manufactureAccessory.getAccessoryName())){
                    ManufactureAccessory manufactureAccessory1 = new ManufactureAccessory();
                    manufactureAccessory1.setAccessoryName(manufactureAccessory.getAccessoryName());
                    destList.add(manufactureAccessory1);
                    nameList.add(manufactureAccessory.getAccessoryName());
                }
            }
            for (ManufactureAccessory manufactureAccessory : destList){
                for (ManufactureAccessory manufactureAccessory2 : manufactureAccessoryList){
                    if (manufactureAccessory.getAccessoryName().equals(manufactureAccessory2.getAccessoryName())){
                        manufactureAccessory.setSumMoney(manufactureAccessory.getSumMoney() + manufactureAccessory2.getSumMoney());
                        manufactureAccessory.setAddFee(manufactureAccessory.getAddFee() + manufactureAccessory2.getAddFee());
                        manufactureAccessory.setOrderSumMoney(manufactureAccessory.getOrderSumMoney() + manufactureAccessory2.getOrderPrice() * manufactureAccessory2.getOrderCount());
                    }
                }
            }
            map.put("data", destList);
        }
        else if ("裁剪金额".equals(itemName)){
            int cutCount = tailorService.getTailorCountByInfo(orderName, null, null, null, null, null, 0);
            List<CostAccount> costAccountList = new ArrayList<>();
            List<OrderProcedure> orderProcedureList = orderProcedureService.getOrderProcedureByOrder(orderName);
            float cutPrice = 0f;
            for (OrderProcedure orderProcedure : orderProcedureList){
                if (orderProcedure.getProcedureNumber().equals(2000)){
                    cutPrice = (float)(orderProcedure.getPiecePrice() + orderProcedure.getPiecePriceTwo());
                }
            }
            CostAccount costAccount = new CostAccount();
            costAccount.setCutCount(cutCount);
            costAccount.setCutPrice(cutPrice);
            costAccount.setSumMoney(cutCount * cutPrice);
            costAccountList.add(costAccount);
            map.put("data", costAccountList);
        }
        else if ("车缝后整金额".equals(itemName)){
            List<GeneralSalary> pieceWorkList = costAccountService.getPieceWorkSummaryByOrder(orderName);
            List<GeneralSalary> hourEmpList = costAccountService.getHourEmpSummaryByOrder(orderName);
            List<OrderProcedure> orderProcedureList = orderProcedureService.getOrderProcedureByOrder(orderName);
            for (OrderProcedure orderProcedure : orderProcedureList){
                for (GeneralSalary pieceWork : pieceWorkList){
                    if (orderProcedure.getProcedureNumber().equals(pieceWork.getProcedureNumber())){
                        pieceWork.setPrice((float)orderProcedure.getPiecePrice());
                        pieceWork.setPriceTwo((float)orderProcedure.getPiecePriceTwo());
                        pieceWork.setSalarySum(pieceWork.getPieceCount() * (pieceWork.getPrice() + pieceWork.getPriceTwo()));
                    }
                }
            }
            for (GeneralSalary hourEmp : hourEmpList){
                hourEmp.setPrice(10f);
                hourEmp.setSalarySum(hourEmp.getPieceCount() * 10f);
            }
            pieceWorkList.addAll(hourEmpList);
            map.put("data", pieceWorkList);
        }
        else if ("印绣花金额".equals(itemName)){
            map.put("data", new ArrayList<String>());
        }
        return map;
    }


}
