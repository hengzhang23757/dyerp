package com.example.erp01.action;

import com.example.erp01.model.*;
import com.example.erp01.service.*;
import com.example.erp01.utils.AjaxResult;
import com.example.erp01.utils.FsspUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RequestMapping(value = "erp")
@Controller
public class AutoPlanController {

    @Autowired
    private EmployeeService employeeService;
    @Autowired
    private OrderProcedureService orderProcedureService;
    @Autowired
    private ProcedureTemplateService procedureTemplateService;
    @Autowired
    private EmpSkillsService skillsService;
    @Autowired
    private AutoPlanService autoPlanService;

    @RequestMapping("/autoPlanStart")
    public String autoPlan(){
        return "aps/autoPlan";
    }

    @RequestMapping("/getEmpListByGroupName")
    @ResponseBody
    public AjaxResult getEmpListByGroupName(@RequestParam("groupName") String groupName){
        List<Employee> employeeByGroup = employeeService.getProduceEmployeeByGroup(groupName);
        return AjaxResult.success(employeeByGroup);
    }

    @RequestMapping("/getProduresByOrderName")
    @ResponseBody
    public AjaxResult getProduresByOrderName(@RequestParam("orderName") String orderName){
        List<OrderProcedure> procedureList = orderProcedureService.getOrderProcedureByOrderProcedureSection(orderName,"车缝");
        return AjaxResult.success(procedureList);
    }

    @RequestMapping("/autoPlanProfileInfo")
    @ResponseBody
    public AjaxResult autoPlanProfileInfo(@RequestParam("groupName") String groupName,@RequestParam("orderName") String orderName){
        List<Employee> employeeByGroup = employeeService.getProduceEmployeeByGroup(groupName);
        List<OrderProcedure> procedureList = orderProcedureService.getOrderProcedureByOrderProcedureSection(orderName,"车缝");
        Map<String,Object> result = new HashMap<String,Object>();
        result.put("employeeByGroup",employeeByGroup);
        result.put("procedureList",procedureList);
        return AjaxResult.success(result);
    }

    @RequestMapping("/autoPlanCalculate")
    @ResponseBody
    public AjaxResult autoPlanCalculate(@RequestParam("groupName") String groupName,
                                        @RequestParam("orderName") String orderName,
                                        @RequestParam("workHour") String workHour,
                                        @RequestParam("empList") String empJson,
                                        @RequestParam("procedureList") String procedureJson){
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        List<String> empList = gson.fromJson(empJson,new TypeToken<List<String>>(){}.getType());
        List<String> procedureCodeList = gson.fromJson(procedureJson,new TypeToken<List<String>>(){}.getType());
        Map<String,Object> params = new HashMap<String,Object>();
        params.put("groupName", groupName);
        params.put("orderName", orderName);
        //去除工序sam值为0的
        params.put("sam", 1);
        params.put("empList", empList);
        params.put("procedureSection", "车缝");
        params.put("procedureCodeList", procedureCodeList);
        List<EmpSkills> skills = skillsService.getSkillsByMap(params);
        List<ProcedureTemplate> procedureList = procedureTemplateService.getOrderProcedureByMap(params);
//        procedureList = procedureList.stream().sorted(Comparator.comparing(ProcedureTemplate::getProcedureCode)).collect(Collectors.toList());
        double[][] matrixA = autoPlanService.generateSkillMatrix(empList,skills, procedureList);
        Map<String, Object> solve = FsspUtil.solve(matrixA, empList.size(), procedureList.size());
        double dworkHour = Double.parseDouble(workHour);
        double[] xes = ((double[]) solve.get("x"));
        for (int i = 0; i < xes.length; i++){
            xes[i] = xes[i]*dworkHour;
        }
        List<Employee> employeeList = employeeService.getEmpByEmpNumberList(empList);
//        List<ScheduleRecord> scheduleRecords = autoPlanService.generateEmpPlan(xes,empList,procedureList,skills,employeeList);
        Map<String,Object> echart = autoPlanService.calculateBalanceAndEchart(xes,matrixA,dworkHour);

        Map<String,Object> result = new HashMap<String,Object>();
        result.put("x", xes);
        result.put("employeeList", employeeList);
        result.put("procedureList", procedureList);
        result.put("skills",matrixA);
        result.put("standardSkills",procedureList.stream().map(ProcedureTemplate::getHourCap).collect(Collectors.toList()));
        result.put("xLabelData",procedureList.stream().map(ProcedureTemplate::getProcedureName).collect(Collectors.toList()));
        result.put("y1Data",(double[])echart.get("dailyOutput"));
        result.put("averageBalance",echart.get("averageBalance"));
        result.put("minDaily",echart.get("minDaily"));
        result.put("y2Data",procedureList.stream().map(ProcedureTemplate::getHourCap).collect(Collectors.toList()));
        result.put("y3Data",procedureList.stream().map(o->o.getHourCap()*dworkHour).collect(Collectors.toList()));
        return AjaxResult.success(result);
    }

}
