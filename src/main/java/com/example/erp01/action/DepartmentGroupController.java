package com.example.erp01.action;

import com.example.erp01.model.DepartmentGroup;
import com.example.erp01.service.DepartmentGroupService;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.math.BigInteger;
import java.net.URI;
import java.security.MessageDigest;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
@Configuration      //1.主要用于标记配置类，兼备Component的效果。
@EnableScheduling   // 2.开启定时任务
@Controller
@RequestMapping(value = "erp")
public class DepartmentGroupController {

    @Autowired
    private DepartmentGroupService departmentGroupService;

    @RequestMapping("/departmentGroupStart")
    public String departmentGroupStart(){
        return "factoryMsg/departmentGroup";
    }


    @RequestMapping(value = "/getalldepartmentgroup",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getAllSize(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<DepartmentGroup> departmentGroupList = departmentGroupService.getAllDepartmentGroup();
        map.put("data", departmentGroupList);
        map.put("code", 0);
        map.put("count", departmentGroupList.size());
        return map;
    }

    @RequestMapping(value = "/listGroupNameByDepartment",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> listGroupNameByDepartment(@RequestParam("departmentName")String departmentName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> groupNameList = departmentGroupService.listGroupNameByDepartment(departmentName);
        map.put("data", groupNameList);
        map.put("code", 0);
        map.put("count", groupNameList.size());
        return map;
    }

    @RequestMapping(value = "/getalldepartmentnamelist",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getAllDepartmentNameList(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> departmentNameList = departmentGroupService.getAllDepartmentName();
        map.put("data", departmentNameList);
        map.put("code", 0);
        map.put("count", departmentNameList.size());
        return map;
    }

    @RequestMapping(value = "/getallgroupnamelist",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getAllGroupNameList(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> groupNameList = departmentGroupService.getAllGroupName();
        map.put("data", groupNameList);
        map.put("code", 0);
        map.put("count", groupNameList.size());
        return map;
    }

    @RequestMapping(value = "/getallcheckgroupname",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getAllCheckGroupName(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> groupNameList = departmentGroupService.getAllCheckGroupName();
        map.put("data", groupNameList);
        map.put("code", 0);
        map.put("count", groupNameList.size());
        return map;
    }


    @RequestMapping(value = "/adddepartmentgroup",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> addDepartmentGroup(@RequestParam("departmentName")String departmentName,
                                                  @RequestParam("groupName")String groupName){
        Map<String, Object> map = new LinkedHashMap<>();
        DepartmentGroup departmentGroup = new DepartmentGroup();
        departmentGroup.setDepartmentName(departmentName);
        departmentGroup.setGroupName(groupName);
        DepartmentGroup dg = departmentGroupService.getDepartmentGroupByDepartment(departmentName);
        if (dg != null){
            departmentGroup.setDepartmentId(dg.getDepartmentId());
        } else {
            departmentGroup.setDepartmentId(syncDepartmentToCheck(departmentName));
        }
        int res = departmentGroupService.addDepartmentGroup(departmentGroup);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/deletedepartmentgroup",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> deleteDepartmentGroup(@RequestParam("id") Integer id){
        Map<String, Object> map = new LinkedHashMap<>();
        int res = departmentGroupService.deleteDepartmentGroup(id);
        map.put("result", res);
        return map;
    }


    private String getCheckDepartment() {
        CloseableHttpClient httpclient = HttpClients.createDefault();
        String resultString = "";
        String ccCount = "0";
        CloseableHttpResponse response = null;
        try {
            // 创建uri
            String url = "http://yun.kqapi.com/Api/Api/getDepartment";
            URIBuilder builder = new URIBuilder(url);
            Long currentTime = new Date().getTime();
            Map<String, String> param = new LinkedHashMap<>();
            param.put("account", "b8fe2958b0aec8eb5c70b039832d989a");
            param.put("requesttime", currentTime.toString());
            String planText = param.get("account") + param.get("requesttime") + "andy171fr";
            String md5code = encryption(planText);
            param.put("sign", md5code);
            if (param != null) {
                for (String key : param.keySet()) {
                    builder.addParameter(key, param.get(key));
                }
            }
            URI uri = builder.build();
            // 创建http GET请求
            HttpGet httpGet = new HttpGet(uri);
            // 执行请求
            response = httpclient.execute(httpGet);
            // 判断返回状态是否为200
            if (response.getStatusLine().getStatusCode() == 200) {
                resultString = EntityUtils.toString(response.getEntity(), "UTF-8");
            }
            System.out.println(resultString);
            return ccCount;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (response != null) {
                    response.close();
                }
                httpclient.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return ccCount;
    }

    private String syncDepartmentToCheck(String departmentName) {
        CloseableHttpClient httpclient = HttpClients.createDefault();
        String resultString = "";
        String deptId = "0";
        CloseableHttpResponse response = null;
        try {
            // 创建uri
            String url = "http://yun.kqapi.com/Api/Api/addDepartment";
            URIBuilder builder = new URIBuilder(url);
            Long currentTime = new Date().getTime();
            Map<String, String> param = new LinkedHashMap<>();
            param.put("account", "b8fe2958b0aec8eb5c70b039832d989a");
            param.put("requesttime", currentTime.toString());
            param.put("parentid", "0");
            param.put("deptname", departmentName);
            String planText = param.get("account") + param.get("deptname") + param.get("parentid") + param.get("requesttime") + "andy171fr";
            String md5code = encryption(planText);
            param.put("sign", md5code);
            if (param != null) {
                for (String key : param.keySet()) {
                    builder.addParameter(key, param.get(key));
                }
            }
            URI uri = builder.build();
            // 创建http GET请求
            HttpGet httpGet = new HttpGet(uri);
            // 执行请求
            response = httpclient.execute(httpGet);
            // 判断返回状态是否为200
            if (response.getStatusLine().getStatusCode() == 200) {
                resultString = EntityUtils.toString(response.getEntity(), "UTF-8");
            }
            JsonParser jsonParser = new JsonParser();
            JsonObject json = (JsonObject) jsonParser.parse(resultString);
            String status = json.get("status").getAsString();
            if (status.equals("1")){
                deptId = json.get("data").getAsString();
            }
            return deptId;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (response != null) {
                    response.close();
                }
                httpclient.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return deptId;
    }

    public String encryption(String plainText) {
        String hashtext = new String();
        try {
            MessageDigest m = MessageDigest.getInstance("MD5");
            m.reset();
            m.update(plainText.getBytes());
            byte[] digest = m.digest();
            BigInteger bigInt = new BigInteger(1,digest);
            hashtext = bigInt.toString(16);
            while(hashtext.length() < 32 ){
                hashtext = "0" + hashtext;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return hashtext;
    }

}
