package com.example.erp01.action;

import com.example.erp01.model.HourEmp;
import com.example.erp01.model.OpaBackInput;
import com.example.erp01.service.EmployeeService;
import com.example.erp01.service.HourEmpService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
@RequestMapping(value = "erp")
public class HourEmpController {

    @Autowired
    private HourEmpService hourEmpService;
    @Autowired
    private EmployeeService employeeService;

    @RequestMapping(value = "/hourEmpStart")
    public String hourEmpStart(Model model){
        model.addAttribute("bigMenuTag",10);
        model.addAttribute("menuTag",109);
        return "miniProgram/hourEmp";
    }

    @RequestMapping(value = "/orderHourEmpDetailStart")
    public String orderHourEmpDetailStart(Model model){
        model.addAttribute("bigMenuTag",11);
        model.addAttribute("menuTag",114);
        return "miniProgram/orderHourEmpDetail";
    }

    @RequestMapping(value = "/addhouremp",method = RequestMethod.POST)
    @ResponseBody
    public int addHourEmp(@RequestParam("employeeNumber")String employeeNumber,
                          @RequestParam("employeeName")String employeeName,
                          @RequestParam("groupName")String groupName,
                          @RequestParam("orderName")String orderName,
                          @RequestParam("clothesVersionNumber")String clothesVersionNumber,
                          @RequestParam("layerCount")float layerCount,
                          @RequestParam("userName")String userName,
                          @RequestParam("createTime")String createTime){
        Timestamp ts = new Timestamp(System.currentTimeMillis());
        ts = Timestamp.valueOf(createTime.concat(" 12:00:00"));
        HourEmp he = new HourEmp(employeeNumber,employeeName,groupName,orderName,clothesVersionNumber,layerCount,11111,"时工",userName,ts);
        return hourEmpService.addHourEmp(he);
    }

    @RequestMapping(value = "/deletehouremp",method = RequestMethod.POST)
    @ResponseBody
    public int deleteHourEmp(@RequestParam("hourEmpID")Integer hourEmpID){
        return hourEmpService.deleteHourEmp(hourEmpID);
    }

    @RequestMapping(value = "/getallhouremp",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getAllHourEmp(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<HourEmp> hourEmpList = hourEmpService.getAllHourEmp();
        map.put("allHourEmp",hourEmpList);
        return map;
    }

    @RequestMapping(value = "/getorderhourempdetail",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getOrderHourEmpDetail(@RequestParam("orderName")String orderName,
                                                     @RequestParam("from")String from,
                                                     @RequestParam("to")String to){
        Map<String, Object> map = new LinkedHashMap<>();
        if("".equals(from)){
            from = "1970-01-01";
        }
        if("".equals(to)){
            to = "2050-01-01";
        }
        try{
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date fromDate = sdf.parse(from);
            Date toDate = sdf.parse(to);
            List<HourEmp> hourEmpList = hourEmpService.getOrderHourEmpDetail(orderName, fromDate,toDate);
            map.put("hourEmpList",hourEmpList);
            return map;
        }catch (ParseException e){
            e.printStackTrace();
        }
        return map;
    }

    @RequestMapping(value = "/updatehouremp",method = RequestMethod.POST)
    @ResponseBody
    public int updateHourEmp(@RequestParam("hourEmp")String hourEmp){
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        HourEmp hourEmp1 = gson.fromJson(hourEmp, HourEmp.class);
        return hourEmpService.updateHourEmp(hourEmp1);
    }

    @RequestMapping(value = "/gettodayhouremp",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getTodayHourEmp(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<HourEmp> hourEmpList = hourEmpService.getTodayHourEmp();
        map.put("todayHourEmp",hourEmpList);
        return map;
    }

    @RequestMapping(value = "/getonemonthhouremp",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getOneMonthHourEmp(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<HourEmp> hourEmpList = hourEmpService.getOneMonthHourEmp();
        map.put("oneMonthHourEmp",hourEmpList);
        return map;
    }

    @RequestMapping(value = "/getthreemonthshouremp",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getThreeMonthsHourEmp(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<HourEmp> hourEmpList = hourEmpService.getThreeMonthsHourEmp();
        map.put("threeMonthsHourEmp",hourEmpList);
        return map;
    }

}
