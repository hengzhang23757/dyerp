package com.example.erp01.action;

import com.example.erp01.model.*;
import com.example.erp01.service.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

@Controller
@RequestMapping(value = "erp")
public class FabricStorageController {

    @Autowired
    private FabricStorageService fabricStorageService;
    @Autowired
    private FabricDetailService fabricDetailService;
    @Autowired
    private DepartmentPlanService departmentPlanService;
    @Autowired
    private CutLoosePlanService cutLoosePlanService;

    @RequestMapping("/fabricInStoreStart")
    public String fabricInStoreStart(){
        return "fabric/fabricInStore";
    }

    @RequestMapping("/fabricQueryStart")
    public String fabricQueryStart(){
        return "fabric/fabricQuery";
    }

    @RequestMapping("/screenFabricQuery")
    public String screenFabricQuery(){
        return "screen/fabricQuery";
    }



    @RequestMapping(value = "/getallfabricstorage", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> getAllFabricStorage(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<FabricStorage> fabricStorageList = fabricStorageService.getAllFabricStorage();
        map.put("data", fabricStorageList);
        map.put("code", 0);
        map.put("count", fabricStorageList.size());
        return map;
    }

    @RequestMapping(value = "/getscreenfabricstorage", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> getScreenFabricStorage(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> orderNameList = new ArrayList<>();
        List<CutLoosePlan> cutLoosePlanList = cutLoosePlanService.getCutLoosePlanByFinishState("未完成");
        for (CutLoosePlan cutLoosePlan : cutLoosePlanList){
            orderNameList.add(cutLoosePlan.getOrderName());
        }
        List<FabricStorage> fabricStorageList = new ArrayList<>();
        if (orderNameList != null && !orderNameList.isEmpty()){
//            fabricStorageList = fabricStorageService.getFabricStorageByOrderList(orderNameList);
            fabricStorageList = fabricStorageService.getAllFabricStorage();
        }
        map.put("data", fabricStorageList);
        map.put("code", 0);
        map.put("count", fabricStorageList.size());
        return map;
    }

    @RequestMapping(value = "/deletefabricstorage",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> deleteFabricStorage(@RequestParam("fabricStorageID") Integer fabricStorageID){
        Map<String, Object> map = new LinkedHashMap<>();
        FabricStorage fabricStorage = fabricStorageService.getFabricStorageByID(fabricStorageID);
        if (fabricStorage == null){
            map.put("error", "记录异常:库存记录为空！");
            return map;
        }
        String orderName = fabricStorage.getOrderName();
        String colorName = fabricStorage.getColorName();
        String fabricName = fabricStorage.getFabricName();
        String fabricColor = fabricStorage.getFabricColor();
        String jarName = fabricStorage.getJarName();
        String location = fabricStorage.getLocation();
        Float weight = fabricStorage.getWeight();
        Integer batchNumber = fabricStorage.getBatchNumber();
        FabricDetail fabricDetail = fabricDetailService.getTransFabricDetailByInfo(orderName, colorName, fabricName, fabricColor, jarName, location);
        if (fabricDetail == null){
            map.put("error", "记录异常:入库记录为空！");
            return map;
        }
        if (fabricDetail.getWeight() < weight || fabricDetail.getBatchNumber() < batchNumber || fabricStorage.getWeight() < weight || fabricStorage.getBatchNumber() < batchNumber){
            map.put("error", "数据异常:出库卷数超库存数！");
            return map;
        }
        String info = "";

        if (fabricDetail.getBatchNumber().equals(batchNumber)){
            int res1 = fabricDetailService.deleteFabricDetailByID(fabricDetail.getFabricDetailID());
            if (res1 == 0){
                info += "入库记录已删除,";
            }else{
                info += "入库记录删除异常,";
            }
        }else {
            fabricDetail.setWeight(fabricDetail.getWeight() - weight);
            fabricDetail.setBatchNumber(fabricDetail.getBatchNumber() - batchNumber);
            int res2 = fabricDetailService.updateFabricDetail(fabricDetail);
            if (res2 == 0){
                info += "入库记录已更新";
            }else{
                info += "入库记录更新异常,";
            }
        }
        int res3 = fabricStorageService.deleteFabricStorage(fabricStorageID);
        if (res3 == 0){
            info += "库存删除成功";
        }else {
            info += "库存删除失败";
        }
        map.put("info", info);
        return map;
    }

//    @RequestMapping(value = "/getweightbatchinout",method = RequestMethod.GET)
//    @ResponseBody
//    public Map<String,Object> getFabricStorageByOrderLocationJar(@RequestParam("orderName") String orderName,
//                                                                 @RequestParam("colorName") String colorName,
//                                                                 @RequestParam("fabricName") String fabricName,
//                                                                 @RequestParam("fabricColor") String fabricColor,
//                                                                 @RequestParam("jarName") String jarName,
//                                                                 @RequestParam("location") String location){
//        Map<String, Object> map = new HashMap<>();
//        FabricStorage fabricStorage = fabricStorageService.getFabricStorageByOrderLocationJar(orderName, colorName, fabricName, fabricColor, jarName, location);
//        map.put("weight",fabricStorage.getWeight());
//        map.put("batchNumber",fabricStorage.getBatchNumber());
//        return map;
//    }

    @RequestMapping(value = "/getfabricstoragebyinfo", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getFabricStorageByInfo(@RequestParam("fabricID")Integer fabricID){
        Map<String, Object> map = new LinkedHashMap<>();
        List<FabricStorage> fabricStorageList = fabricStorageService.getFabricStorageByInfo(null, null, null, null, fabricID);
        map.put("data", fabricStorageList);
        map.put("count", fabricStorageList.size());
        map.put("code",0);
        return map;
    }


    @RequestMapping(value = "/updatefabricstorage", method = RequestMethod.POST)
    @ResponseBody
    public int updateFabricStorage(@RequestParam("fabricStorageID") Integer fabricStorageID,
                                   @RequestParam("location") String location){
        int res1 = 0,res2;
        FabricStorage fabricStorage = fabricStorageService.getFabricStorageByID(fabricStorageID);
        FabricDetail fabricDetail = fabricDetailService.getFabricDetailByLocationJarFabricID(fabricStorage.getLocation(), fabricStorage.getJarName(), fabricStorage.getFabricID());
        fabricStorage.setLocation(location);
        if (fabricDetail != null){
            fabricDetail.setLocation(location);
            res1 = fabricDetailService.updateFabricDetail(fabricDetail);
        }
        res2 = fabricStorageService.updateFabricStorage(fabricStorage);
        if (res1 == 0 && res2 == 0){
            return 0;
        } else {
            return 1;
        }
    }

    @RequestMapping(value = "/deletefabricstoragebyid", method = RequestMethod.POST)
    @ResponseBody
    public int deleteFabricStorageByID(@RequestParam("fabricStorageID")Integer fabricStorageID){
        return fabricStorageService.deleteFabricStorage(fabricStorageID);
    }

    @RequestMapping(value = "/getbatchnumberbyorder", method = RequestMethod.GET)
    @ResponseBody
    public int getBatchNumberByOrder(@RequestParam("orderName")String orderName){
        return fabricStorageService.getFabricBatchNumberByOrder(orderName, null);
    }

}
