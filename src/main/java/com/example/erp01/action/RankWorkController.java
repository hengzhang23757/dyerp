package com.example.erp01.action;

import com.example.erp01.model.AccessoryInStore;
import com.example.erp01.model.GeneralSalary;
import com.example.erp01.model.RankWork;
import com.example.erp01.service.RankWorkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

@Controller
@RequestMapping(value = "erp")
public class RankWorkController {

    @Autowired
    private RankWorkService rankWorkService;

    @RequestMapping(value = "/miniGetRankWorkByGroup",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> miniGetRankWorkByGroup(@RequestParam("groupName")String groupName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<GeneralSalary> generalSalaryList = rankWorkService.getGroupRank(groupName);
        Set<String> employeeSet = new HashSet<>();
        for (GeneralSalary generalSalary : generalSalaryList){
            employeeSet.add(generalSalary.getEmployeeNumber());
        }
        map.put("generalSalaryList", generalSalaryList);
        map.put("employee", employeeSet);
        return map;
    }


}
