package com.example.erp01.action;

import com.example.erp01.model.*;
import com.example.erp01.service.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
@RequestMapping(value = "erp")
public class OrderClothesController {
    private static final Logger log = LoggerFactory.getLogger(OrderClothesController.class);

    @Autowired
    private OrderClothesService orderClothesService;

    @Autowired
    private TailorService tailorService;

    @Autowired
    private OtherTailorService otherTailorService;

    @Autowired
    private ReportService reportService;

    @Autowired
    private ManufactureOrderService manufactureOrderService;

    @Autowired
    private OrderMeasureService orderMeasureService;

    @Autowired
    private ManufactureFabricService manufactureFabricService;

    @Autowired
    private ManufactureAccessoryService manufactureAccessoryService;

    @Autowired
    private ProcessRequirementService processRequirementService;

    @Autowired
    private PrintPartService printPartService;

    @Autowired
    private StyleImageService styleImageService;
    /**
     * 进入订单信息页面
     * @param model
     * @return
     */
    @RequestMapping("/orderStart")
    public String orderStart(Model model){
        model.addAttribute("bigMenuTag",1);
        model.addAttribute("menuTag",11);
        return "orderMsg/order";
    }

    /**
     * 进入订单录入页面
     * @param model
     * @return
     */
    @RequestMapping("/addOrderStart")
    public String addOrderStart(Model model,String orderName) throws Exception{
        if(orderName == null) {
            model.addAttribute("type", "add");
        }else {
//            orderName = java.net.URLDecoder.decode(orderName,"UTF-8");
            model.addAttribute("orderName", orderName);
            model.addAttribute("type", "detail");
        }
        return "orderMsg/fb_addOrder";
    }


    @RequestMapping("/updateOrderStart")
    public String updateOrderStart(Model model,String orderName) throws Exception{
        model.addAttribute("orderName", orderName);
        model.addAttribute("type", "update");
        return "orderMsg/fb_updateOrder";
    }


    @RequestMapping("/detailOrderStart")
    public String detailOrderStart(Model model, String clothesVersionNumber, String orderName, String customerName, String styleDescription, String userName) throws Exception{
        model.addAttribute("clothesVersionNumber", clothesVersionNumber);
        model.addAttribute("orderName", orderName);
        model.addAttribute("customerName", customerName);
        model.addAttribute("styleDescription", styleDescription);
        model.addAttribute("userName", userName);
        model.addAttribute("type", "detail");
        return "orderMsg/fb_detailOrder";
    }

    /**
     * 进入报表查询
     * @param model
     * @return
     */
    @RequestMapping("/cutReportStart")
    public String cutReportStart(Model model){
        model.addAttribute("bigMenuTag",4);
        model.addAttribute("menuTag",41);
        return "orderMsg/cutReport";
    }

    /**
     * 进入裁床查漏页面
     * @param model
     * @return
     */
//    @RequestMapping("/cutQueryLeakStartOther")
//    public String cutQueryLeakStart(Model model, HttpServletRequest request){
//        String orderName = request.getParameter("orderName");
//        String clothesVersionNumber = request.getParameter("clothesVersionNumber");
//        if (orderName != null){
//            model.addAttribute("detailOrderName", orderName);
//            model.addAttribute("detailClothesVersionNumber", clothesVersionNumber);
//        }
//        return "orderMsg/cutQueryLeak";
//    }

    @RequestMapping("/cutQueryLeakStart")
    public String cutQueryLeakStart(Model model, HttpServletRequest request){
        String orderName = request.getParameter("orderName");
        String clothesVersionNumber = request.getParameter("clothesVersionNumber");
        String type = request.getParameter("type");
        if (orderName != null){
            model.addAttribute("detailOrderName", orderName);
            model.addAttribute("detailClothesVersionNumber", clothesVersionNumber);
        }
        if (type != null){
            model.addAttribute("type", type);
        } else {
            model.addAttribute("type", "search");
        }
        return "orderMsg/cutQueryLeak";
    }

    @RequestMapping("/cutMonthReportStart")
    public String cutMonthReportStart(Model model){
        model.addAttribute("bigMenuTag",4);
        model.addAttribute("menuTag",43);
        return "orderMsg/cutMonthReport";
    }

    /**
     * 进入花片查漏页面
     * @param model
     * @return
     */
    @RequestMapping("/opaQueryLeakStart")
    public String opaQueryLeakStart(Model model){
        model.addAttribute("bigMenuTag",1);
        model.addAttribute("menuTag",16);
        return "orderMsg/opaQueryLeak";
    }


    @RequestMapping(value = "/commitorderclothes",method = RequestMethod.POST)
    @ResponseBody
    public int addOrderClothes(@RequestParam("orderClothesJson")String orderClothesJson){
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        List<OrderClothes> orderClothesList = gson.fromJson(orderClothesJson,new TypeToken<List<OrderClothes>>(){}.getType());
        for (OrderClothes orderClothes : orderClothesList){
            String uuid = UUID.randomUUID().toString();
            orderClothes.setGuid(uuid);
        }
        return orderClothesService.addOrderClothes(orderClothesList);
    }


    @RequestMapping(value = "/getallorderclothes", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getAllOrderClothes(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<OrderClothes> orderClothesList = new ArrayList<>();
        orderClothesList = orderClothesService.getAllOrderClothes();
        map.put("data",orderClothesList);
        return map;
    }

    @RequestMapping(value = "/getordersummary", method = RequestMethod.GET)
    public String getOrderSummary(Model model){
        List<OrderClothes> orderClothesList = orderClothesService.getOrderSummary();
        model.addAttribute("orderClothesList",orderClothesList);
        return "orderMsg/fb_orderList";
    }

    @RequestMapping(value = "/getorderbyname", method = RequestMethod.GET)
    @ResponseBody
    public List<OrderClothes> getByName(@RequestParam("orderName")String orderName){
        List<OrderClothes> orderClothesList = new ArrayList<>();
        orderClothesList = orderClothesService.getOrderByName(orderName);
        return orderClothesList;
    }

    @RequestMapping(value = "/deleteorderbyname")
    @ResponseBody
    public int deleteByName(@RequestParam("orderName")String orderName,
                            @RequestParam("userName")String userName){
        log.warn("删除订单："+orderName+"----操作用户："+userName);
        List<Integer> bedList1 = tailorService.getBedNumbersByOrderNamePartNameTailorType(orderName, null, 0);
        List<Integer> bedList2 = otherTailorService.getOtherBedNumbersByOrderName(orderName);
        if ((bedList1 != null && bedList1.size() > 0) || (bedList2 != null && bedList2.size() > 0)){
            return 100;
        }
        return orderClothesService.deleteOrderByName(orderName);
    }

    @RequestMapping(value = "/minigetorderhint",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> miniGetOrderHint(@RequestParam("subOrderName")String subOrderName){
        Map<String,Object> result = new HashMap<>();
        List<String> orderNameList = orderClothesService.getOrderHint(subOrderName);
        result.put("orderNameList",orderNameList);
        return result;
    }

    @RequestMapping(value = "/getorderhint",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getOrderHint(@RequestParam("keywords")String keywords){
        Map<String,Object> result = new HashMap<>();
        List<String> orderNameList = orderClothesService.getOrderHint(keywords);
        result.put("content",orderNameList);
        result.put("type","success");
        result.put("code",0);
        return result;
    }


    @RequestMapping(value = "/getcolorhint",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getColorHint(@RequestParam("orderName")String orderName){
        Map<String,Object> result = new HashMap<>();
        List<String> colorList = orderClothesService.getColorHint(orderName);
        result.put("colorList",colorList);
        return result;
    }

    @RequestMapping(value = "/getordercolornamesbyorder",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getOrderColorHint(@RequestParam("orderName")String orderName){
        Map<String,Object> result = new LinkedHashMap<>();
        List<String> colorNameList = orderClothesService.getOrderColorNamesByOrder(orderName);
        result.put("colorNameList",colorNameList);
        return result;
    }

    @RequestMapping(value = "/minigetordercolornamesbyorder",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> miniGetOrderColorNamesByOrder(@RequestParam("orderName")String orderName){
        Map<String,Object> result = new LinkedHashMap<>();
        List<String> colorNameList = orderClothesService.getOrderColorNamesByOrder(orderName);
        result.put("colorNameList",colorNameList);
        return result;
    }

    @RequestMapping(value = "/minigetcolorhint",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> miniGetColorHint(@RequestParam("orderName")String orderName){
        Map<String,Object> result = new HashMap<>();
        List<String> colorList = orderClothesService.getColorHint(orderName);
        result.put("colorList",colorList);
        return result;
    }

    @RequestMapping(value = "/getcustomernamebyordername",method = RequestMethod.GET)
    @ResponseBody
    public String getCustomerNameByOrderName(@RequestParam("orderName")String orderName){
        return orderClothesService.getCustomerNameByOrderName(orderName);
    }

    /**
     * 查询订单信息以用作裁床查漏
     * @param orderName
     * @return
     */
    @RequestMapping(value = "/getorderinfobyname",method = RequestMethod.GET)
    @ResponseBody
    public String getOrderInfoByName(@RequestParam("orderName")String orderName){
        List<CutQueryLeak> orderInfoList = orderClothesService.getOrderInfoByName(orderName);
        Gson gson = new Gson();
        String res = gson.toJson(orderInfoList);
        return res;
    }

    @RequestMapping(value = "/searchCutQueryLeak",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> searchCutQueryLeak(@RequestParam("orderName")String orderName,
                                                  @RequestParam(value = "colorName", required = false)String colorName,
                                                  @RequestParam(value = "from", required = false)String from,
                                                  @RequestParam(value = "to", required = false)String to) {
        List<CutQueryLeak> orderInfoList = orderClothesService.getOrderInfoByName(orderName);
        List<CutQueryLeak> tailorInfoList = tailorService.getTailorInfoByOrderPartType(orderName, null, 0, from, to);
        List<String> partNameList = printPartService.getOtherPrintPartByOrder(orderName);
        List<CutBedColor> cutBedColorList = reportService.getCutBedColorByOrder(orderName, null, 0, from, to);
        Map<String,Object> partMap = new LinkedHashMap<>();
        Map<String, Object> partTimeMap = new LinkedHashMap<>();
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> colorList = new ArrayList<>();
        List<String> sizeList = new ArrayList<>();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        if (colorName != null){
            colorList.add(colorName);
            for(CutQueryLeak cutQueryLeak : orderInfoList){
                if (!sizeList.contains(cutQueryLeak.getSizeName())){
                    sizeList.add(cutQueryLeak.getSizeName());
                }
            }
        } else {
            for(CutQueryLeak cutQueryLeak : orderInfoList){
                if (!colorList.contains(cutQueryLeak.getColorName())){
                    colorList.add(cutQueryLeak.getColorName());
                }
                if (!sizeList.contains(cutQueryLeak.getSizeName())){
                    sizeList.add(cutQueryLeak.getSizeName());
                }
            }
        }
        if(tailorInfoList==null || tailorInfoList.size()==0) {
            Map<String, Object> colorMap = new LinkedHashMap<>();
            for (String color : colorList) {
                Map<String, Object> tmpSizeMap = new LinkedHashMap<>();
                for (String size : sizeList) {
                    int tmpOrderCount = 0;
                    int tmpCutCount = 0;
                    int tmpWellCount = 0;
                    for (CutQueryLeak cutQueryLeak : orderInfoList) {
                        if (cutQueryLeak.getColorName().equals(color) && cutQueryLeak.getSizeName().equals(size)) {
                            tmpOrderCount = cutQueryLeak.getOrderCount();
                        }
                    }
                    CutWellBad cwb = new CutWellBad(tmpOrderCount,tmpCutCount,tmpWellCount,tmpWellCount-tmpOrderCount);
                    tmpSizeMap.put(size, cwb);
                }
                colorMap.put(color, tmpSizeMap);
            }
            partMap.put("主身布",colorMap);
        }else{
            Map<String, Object> colorMap = new LinkedHashMap<>();
            for (String color : colorList) {
                Map<String, Object> tmpSizeMap = new LinkedHashMap<>();
                for (String size : sizeList) {
                    int tmpOrderCount = 0;
                    int tmpCutCount = 0;
                    int tmpWellCount = 0;
                    for (CutQueryLeak cutQueryLeak1 : orderInfoList) {
                        if (cutQueryLeak1.getColorName().equals(color) && cutQueryLeak1.getSizeName().equals(size)) {
                            tmpOrderCount += cutQueryLeak1.getOrderCount();
                        }
                    }
                    for (CutQueryLeak cutQueryLeak2 : tailorInfoList) {
                        if (cutQueryLeak2.getColorName().equals(color) && cutQueryLeak2.getSizeName().equals(size)) {
                            tmpCutCount += cutQueryLeak2.getLayerCount();
                            tmpWellCount += cutQueryLeak2.getWellCount();
                        }
                    }
                    CutWellBad cwb = new CutWellBad(tmpOrderCount, tmpCutCount, tmpWellCount, tmpWellCount - tmpOrderCount);
                    tmpSizeMap.put(size, cwb);
                }
                colorMap.put(color, tmpSizeMap);
            }
            partMap.put("主身布",colorMap);
            List<Integer> bedList = new ArrayList<>();
            List<String> bedTimeGroupList = new ArrayList<>();
            for (CutBedColor cutBedColor : cutBedColorList){
                if (!bedList.contains(cutBedColor.getBedNumber())){
                    bedList.add(cutBedColor.getBedNumber());
                    Date createTime = reportService.getCreateTimeByOrderBed(orderName, cutBedColor.getBedNumber());
                    String cutTimeString = formatter.format(createTime);
                    String bedTimeGroupKey = cutBedColor.getBedNumber().toString()+"#"+cutTimeString+"#"+cutBedColor.getGroupName();
                    bedTimeGroupList.add(bedTimeGroupKey);
                }
            }
            Map<String, Object> colorGroupMap = new LinkedHashMap<>();
            for (String color : colorList){
                Map<String, Object> bedGroupMap = new LinkedHashMap<>();
                for (int i=0; i<bedList.size(); i++){
                    Integer bed = bedList.get(i);
                    Map<String, Object> sizeGroupMap = new LinkedHashMap<>();
                    boolean bedFlag = true;
                    int packageCount = 0;
                    for (String size : sizeList){
                        int cutSizeCount = 0;
                        for (CutBedColor cutBedColor : cutBedColorList){
                            if (color.equals(cutBedColor.getColorName()) && bed.equals(cutBedColor.getBedNumber()) && size.equals(cutBedColor.getSizeName())){
                                cutSizeCount += cutBedColor.getPieceCount();
                                packageCount += cutBedColor.getPackageCount();
                                bedFlag = false;
                            }
                        }
                        sizeGroupMap.put(size,cutSizeCount);
                    }
                    sizeGroupMap.put("packageCount", packageCount);
                    if (!bedFlag){
                        bedGroupMap.put(bedTimeGroupList.get(i),sizeGroupMap);
                    }
                }
                colorGroupMap.put(color,bedGroupMap);
            }
            partTimeMap.put("主身布", colorGroupMap);
        }
        if (partNameList != null && partNameList.size()>0){
            for (String partName : partNameList){
                List<CutQueryLeak> otherTailorInfoList = tailorService.getTailorInfoByOrderPartType(orderName, partName, 1, from, to);
                List<CutBedColor> otherCutBedColorList = reportService.getCutBedColorByOrder(orderName, partName, 1, from, to);
                Map<String, Object> colorMap = new LinkedHashMap<>();
                for (String color : colorList){
                    Map<String,Object> tmpSizeMap = new LinkedHashMap<>();
                    for (String size : sizeList){
                        int tmpOrderCount = 0;
                        int tmpCutCount = 0;
                        int tmpWellCount = 0;
                        for (CutQueryLeak cutQueryLeak1 : orderInfoList) {
                            if (cutQueryLeak1.getColorName().equals(color) && cutQueryLeak1.getSizeName().equals(size)) {
                                tmpOrderCount += cutQueryLeak1.getOrderCount();
                            }
                        }
                        for (CutQueryLeak cutQueryLeak2 : otherTailorInfoList) {
                            if (cutQueryLeak2.getColorName().equals(color) && cutQueryLeak2.getSizeName().equals(size)) {
                                tmpWellCount += cutQueryLeak2.getWellCount();
                            }
                        }
                        for (CutQueryLeak cutQueryLeak3 : tailorInfoList){
                            if (cutQueryLeak3.getColorName().equals(color) && cutQueryLeak3.getSizeName().equals(size)) {
                                tmpCutCount += cutQueryLeak3.getWellCount();
                            }
                        }
                        CutWellBad cwb = new CutWellBad(tmpOrderCount, tmpCutCount, tmpWellCount, tmpWellCount - tmpCutCount);
                        tmpSizeMap.put(size, cwb);
                    }
                    colorMap.put(color,tmpSizeMap);
                }
                partMap.put(partName,colorMap);
                List<Integer> bedList = new ArrayList<>();
                List<String> bedTimeGroupList = new ArrayList<>();
                for (CutBedColor cutBedColor : otherCutBedColorList){
                    if (!bedList.contains(cutBedColor.getBedNumber())){
                        bedList.add(cutBedColor.getBedNumber());
                        Date otherCreateTime = reportService.getCreateTimeByOrderBed(orderName, cutBedColor.getBedNumber());
                        String cutTimeString = formatter.format(otherCreateTime);
                        String bedTimeGroupKey = cutBedColor.getBedNumber().toString()+"#"+cutTimeString+"#"+cutBedColor.getGroupName();
                        bedTimeGroupList.add(bedTimeGroupKey);
                    }
                }
                Map<String, Object> colorGroupMap = new LinkedHashMap<>();
                for (String color : colorList){
                    Map<String, Object> bedGroupMap = new LinkedHashMap<>();
                    for (int i=0; i<bedList.size(); i++){
                        Integer bed = bedList.get(i);
                        Map<String, Object> sizeGroupMap = new LinkedHashMap<>();
                        boolean bedFlag = true;
                        int packageCount = 0;
                        for (String size : sizeList){
                            int cutSizeCount = 0;
                            for (CutBedColor cutBedColor : otherCutBedColorList){
                                if (color.equals(cutBedColor.getColorName()) && bed.equals(cutBedColor.getBedNumber()) && size.equals(cutBedColor.getSizeName())){
                                    cutSizeCount += cutBedColor.getPieceCount();
                                    packageCount += cutBedColor.getPackageCount();
                                    bedFlag = false;
                                }
                            }
                            sizeGroupMap.put("packageCount", packageCount);
                            sizeGroupMap.put(size,cutSizeCount);
                        }
                        if (!bedFlag){
                            bedGroupMap.put(bedTimeGroupList.get(i),sizeGroupMap);
                        }
                    }
                    colorGroupMap.put(color,bedGroupMap);
                }
                partTimeMap.put(partName, colorGroupMap);
            }
        }
        map.put("part",partMap);
        map.put("partTime",partTimeMap);
        return map;
    }


    @RequestMapping(value = "/getversionnumberbyordername",method = RequestMethod.GET)
    @ResponseBody
    public String getVersionNumberByOrderName(@RequestParam("orderName")String orderName){
        String versionNumber = orderClothesService.getVersionNumberByOrderName(orderName);
        return versionNumber;
    }

    @RequestMapping(value = "/minigetversionnumberbyordername",method = RequestMethod.GET)
    @ResponseBody
    public String miniGetVersionNumberByOrderName(@RequestParam("orderName")String orderName){
        String versionNumber = orderClothesService.getVersionNumberByOrderName(orderName);
        return versionNumber;
    }

    @RequestMapping(value = "/getordersizenamesbyorder",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getOrderSizeNamesByOrder(@RequestParam("orderName")String orderName){
        Map<String,Object> result = new HashMap<>();
        List<String> sizeNameList = orderClothesService.getOrderSizeNamesByOrder(orderName);
        result.put("sizeNameList",sizeNameList);
        return result;
    }

    @RequestMapping(value = "/minigetordersizenamesbyorder",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> miniGetOrderSizeNamesByOrder(@RequestParam("orderName")String orderName){
        Map<String,Object> result = new HashMap<>();
        List<String> sizeNameList = orderClothesService.getOrderSizeNamesByOrder(orderName);
        result.put("sizeNameList",sizeNameList);
        return result;
    }

    @RequestMapping(value = "/getsizeandcolorbyorder",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getSizeAndColorbyorder(@RequestParam("orderName")String orderName){
        Map<String,Object> result = new HashMap<>();
        List<String> sizeNameList = orderClothesService.getOrderSizeNamesByOrder(orderName);
        result.put("sizeNameList",sizeNameList);
        List<String> colorNameList = orderClothesService.getOrderColorNamesByOrder(orderName);
        result.put("colorNameList",colorNameList);
        return result;
    }

    @RequestMapping(value = "/minigetsizehint",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> miniGetSizeHint(@RequestParam("orderName")String orderName){
        Map<String,Object> result = new HashMap<>();
        List<String> sizeNameList = orderClothesService.getOrderSizeNamesByOrder(orderName);
        result.put("sizeNameList",sizeNameList);
        return result;
    }

    @RequestMapping(value = "/getstyledescriptionbyorder",method = RequestMethod.GET)
    @ResponseBody
    public String getDescriptionByOrder(@RequestParam("orderName")String orderName){
        String styleDescription = orderClothesService.getDescriptionByOrder(orderName);
        return styleDescription;
    }

    @RequestMapping(value = "/getordertotalcount",method = RequestMethod.GET)
    @ResponseBody
    public Integer getOrderTotalCount(@RequestParam("orderName")String orderName){
        Integer styleDescription = orderClothesService.getOrderTotalCount(orderName);
        if (styleDescription == null){
            return 0;
        }
        return styleDescription;
    }

    @RequestMapping(value = "/getversionhint", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getVersionHint(@RequestParam("keywords")String keywords){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> versionList = orderClothesService.getVersionHint(keywords);
        map.put("content",versionList);
        map.put("type","success");
        map.put("code",0);
        return map;
    }

    @RequestMapping(value = "/minigetversionhint", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> miniGetVersionHint(@RequestParam("versionNumber")String versionNumber){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> versionList = orderClothesService.getVersionHint(versionNumber);
        map.put("versionList",versionList);
        map.put("type","success");
        map.put("code",0);
        return map;
    }

    @RequestMapping(value = "/getorderbyversion", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getOrderByVersion(@RequestParam("clothesVersionNumber")String clothesVersionNumber){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> orderList = orderClothesService.getOrderByVersion(clothesVersionNumber);
        map.put("orderList",orderList);
        return map;
    }

    @RequestMapping(value = "/minigetorderbyversion", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> miniGetOrderByVersion(@RequestParam("clothesVersionNumber")String clothesVersionNumber){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> orderList = orderClothesService.getOrderByVersion(clothesVersionNumber);
        map.put("orderList",orderList);
        return map;
    }

    @RequestMapping(value = "/getonemonthorderclothes", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getOneMonthOrderClothes(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<OrderClothes> orderClothesList = new ArrayList<>();
        orderClothesList = orderClothesService.getOneMonthOrderClothes();
        map.put("data",orderClothesList);
        return map;
    }

    @RequestMapping(value = "/getthreemonthsorderclothes", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getThreeMonthsOrderClothes(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<OrderClothes> orderClothesList = new ArrayList<>();
        orderClothesList = orderClothesService.getThreeMonthsOrderClothes();
        map.put("data",orderClothesList);
        return map;
    }

    @RequestMapping(value = "/getoneyearordersummarylay", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> getOneMonthOrderSummaryLay(@RequestParam(value = "orderName", required = false)String orderName,
                                                          @RequestParam(value = "season", required = false)String season,
                                                          @RequestParam(value = "orderState", required = false)Integer orderState,
                                                          @RequestParam(value = "customerName", required = false)String customerName){
        Map<String, Object> map = new LinkedHashMap<>();
        Map<String, Object> param = new HashMap<>();
        param.put("orderName", orderName);
        param.put("season", season);
        param.put("customerName", customerName);
        param.put("orderState", orderState);
        List<OrderClothes> orderClothesList = orderClothesService.getOrderClothesSummaryByMap(param);
        map.put("count",orderClothesList.size());
        List<String> orderNameList = new ArrayList<>();
        for (OrderClothes orderClothes : orderClothesList){
            orderNameList.add(orderClothes.getOrderName());
        }
        List<ManufactureOrder> manufactureOrderList = manufactureOrderService.getManufactureOrderByOrderNameList(orderNameList);
        List<ManufactureAccessory> manufactureAccessoryList = manufactureAccessoryService.getAccessoryCheckStateRecentYearByOrder(orderNameList);
        List<ManufactureFabric> manufactureFabricList = manufactureFabricService.getFabricCheckStateRecentYearByOrder(orderNameList);
        for (OrderClothes orderClothes : orderClothesList){
            Set<String> accessorySet = new HashSet<>();
            Set<String> fabricSet = new HashSet<>();
            for (ManufactureAccessory manufactureAccessory : manufactureAccessoryList){
                if (orderClothes.getOrderName().equals(manufactureAccessory.getOrderName())){
                    accessorySet.add(manufactureAccessory.getCheckState());
                }
            }
            for (ManufactureFabric manufactureFabric : manufactureFabricList){
                if (orderClothes.getOrderName().equals(manufactureFabric.getOrderName())){
                    fabricSet.add(manufactureFabric.getCheckState());
                }
            }
            for (ManufactureOrder manufactureOrder : manufactureOrderList){
                if (orderClothes.getOrderName().equals(manufactureOrder.getOrderName())){
                    orderClothes.setModifyState(manufactureOrder.getModifyState());
                }
            }
            if (accessorySet != null && !accessorySet.isEmpty()){
                orderClothes.setAccessoryCheck(accessorySet.toString());
            }
            if (fabricSet != null && !fabricSet.isEmpty()){
                orderClothes.setFabricCheck(fabricSet.toString());
            }
        }
        map.put("data", orderClothesList);
        map.put("code",0);
        map.put("msg",null);
        return map;
    }

    @RequestMapping(value = "/getonemonthordersummary", method = RequestMethod.GET)
    public String getOneMonthOrderSummary(Model model){
        List<OrderClothes> orderClothesList = orderClothesService.getOneMonthOrderSummary();
        model.addAttribute("orderClothesList",orderClothesList);
        return "orderMsg/fb_orderList";
    }

    @RequestMapping(value = "/getthreemonthordersummary", method = RequestMethod.GET)
    public String getThreeMonthOrderSummary(Model model){
        List<OrderClothes> orderClothesList = orderClothesService.getThreeMonthsOrderSummary();
        model.addAttribute("orderClothesList",orderClothesList);
        return "orderMsg/fb_orderList";
    }


    @RequestMapping(value = "/getcolorordercount",method = RequestMethod.GET)
    @ResponseBody
    public Integer getColorOrderCount(@RequestParam("orderName")String orderName,
                                      @RequestParam("colorName")String colorName){
        return orderClothesService.getColorOrderCount(orderName, colorName);
    }


    @RequestMapping(value = "/getordercolorandsizenamesbyorder", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getOrderColorAndSizeNamesByOrder(@RequestParam("orderName") String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> colorNameList = orderClothesService.getOrderColorNamesByOrder(orderName);
        List<String> sizeNameList = orderClothesService.getOrderSizeNamesByOrder(orderName);
        map.put("colorNameList", colorNameList);
        map.put("sizeNameList", sizeNameList);
        return map;
    }

    @RequestMapping(value = "/updateorderclothesbatch",method = RequestMethod.POST)
    @ResponseBody
    public int updateOrderClothesBatch(@RequestParam("orderClothesJson")String orderClothesJson){
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        List<OrderClothes> orderClothesList = gson.fromJson(orderClothesJson,new TypeToken<List<OrderClothes>>(){}.getType());
        String orderName = orderClothesList.get(0).getOrderName();
        int res = orderClothesService.updateOrderClothes(orderClothesList);
        if (res == 0){
            // 刷新一下面辅料的数据
            refreshFabricData(orderName);
            refreshAccessoryData(orderName);
            return 0;
        } else {
            return 1;
        }
    }

    @RequestMapping(value = "/addadditionalcolorname",method = RequestMethod.POST)
    @ResponseBody
    public int addAdditionalColorName(@RequestParam("orderClothesListJson")String orderClothesListJson,
                                      @RequestParam("colorName")String colorName,
                                      @RequestParam("orderName")String orderName){
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        OrderClothes std = orderClothesService.getOrderClothesByOrderName(orderName).get(0);
        List<OrderClothes> orderClothesList = gson.fromJson(orderClothesListJson,new TypeToken<List<OrderClothes>>(){}.getType());
        int orderCount = 0;
        for (OrderClothes orderClothes : orderClothesList){
            orderClothes.setStyleDescription(std.getStyleDescription());
            orderClothes.setUserName(std.getUserName());
            orderClothes.setColorName(colorName);
            orderClothes.setCustomerName(std.getCustomerName());
            String uuid = UUID.randomUUID().toString();
            orderClothes.setGuid(uuid);
            orderClothes.setDeadLine(std.getDeadLine());
            orderClothes.setDeliveryDate(std.getDeliveryDate());
            orderClothes.setPurchaseMethod(std.getPurchaseMethod());
            orderClothes.setSeason(std.getSeason());
            orderCount += orderClothes.getCount();
        }
        int res = orderClothesService.addOrderClothes(orderClothesList);
        if (res == 0){
            // 处理面辅料的问题
            List<ManufactureFabric> manufactureFabricList = manufactureFabricService.getManufactureFabricByOrder(orderName, null);
            List<String> fabricNameList = new ArrayList<>();
            List<ManufactureFabric> addManufactureFabricList = new ArrayList<>();
            for (ManufactureFabric manufactureFabric : manufactureFabricList){
                if (!fabricNameList.contains(manufactureFabric.getFabricName())){
                    fabricNameList.add(manufactureFabric.getFabricName());
                    manufactureFabric.setColorName(colorName);
                    manufactureFabric.setOrderCount(orderCount);
                    manufactureFabric.setCheckNumber(null);
                    manufactureFabric.setCheckState("未提交");
                    manufactureFabric.setOrderState("未下单");
                    manufactureFabric.setFabricCount(manufactureFabric.getOrderCount() * manufactureFabric.getOrderPieceUsage() * (1 + manufactureFabric.getFabricAdd()/100));
                    manufactureFabric.setFabricActCount(manufactureFabric.getOrderCount() * manufactureFabric.getPieceUsage() * (1 + manufactureFabric.getFabricAdd()/100));
                    manufactureFabric.setSumMoney(manufactureFabric.getFabricActCount() * manufactureFabric.getPrice());
                    addManufactureFabricList.add(manufactureFabric);
                }
            }
            manufactureFabricService.addManufactureFabricBatch(addManufactureFabricList);
            // 分色分码的要新增，分色不分码的也要新增，分码不分色的要改变数量，不分色不分码的要改变数量
            List<ManufactureAccessory> manufactureAccessoryList = manufactureAccessoryService.getManufactureAccessoryByOrder(orderName, 0);
            List<ManufactureAccessory> accessoryKeyList = manufactureAccessoryService.getAccessoryKeyByOrder(orderName, 0, null);
            List<ManufactureAccessory> addManufactureAccessoryList = new ArrayList<>();
            if (manufactureAccessoryList != null && !manufactureAccessoryList.isEmpty()){
                for (ManufactureAccessory accessoryKey : accessoryKeyList){
                    if (accessoryKey.getColorChild().equals(1) && accessoryKey.getSizeChild().equals(0)){
                        boolean flag = false;
                        for (ManufactureAccessory manufactureAccessory : manufactureAccessoryList){
                            if (manufactureAccessory.getAccessoryKey().equals(accessoryKey.getAccessoryKey()) && !flag){
                                flag = true;
                                ManufactureAccessory destManufactureAccessory = manufactureAccessory;
                                destManufactureAccessory.setColorName(colorName);
                                destManufactureAccessory.setOrderCount(0);
                                destManufactureAccessory.setAccessoryCount(0f);
                                destManufactureAccessory.setAccessoryAdd(0f);
                                destManufactureAccessory.setSumMoney(0f);
                                destManufactureAccessory.setCheckState("未提交");
                                destManufactureAccessory.setCheckNumber(null);
                                destManufactureAccessory.setOrderState("未下单");
                                addManufactureAccessoryList.add(destManufactureAccessory);
                            }
                        }
                    }
                    if (accessoryKey.getColorChild().equals(1) && (accessoryKey.getSizeChild().equals(1) || accessoryKey.getSizeChild().equals(2))){
                        for (ManufactureAccessory manufactureAccessory : manufactureAccessoryList){
                            if (manufactureAccessory.getAccessoryKey().equals(accessoryKey.getAccessoryKey())){
                                ManufactureAccessory destManufactureAccessory = manufactureAccessory;
                                destManufactureAccessory.setColorName(colorName);
                                destManufactureAccessory.setOrderCount(0);
                                destManufactureAccessory.setAccessoryCount(0f);
                                destManufactureAccessory.setAccessoryAdd(0f);
                                destManufactureAccessory.setSumMoney(0f);
                                destManufactureAccessory.setCheckState("未提交");
                                destManufactureAccessory.setCheckNumber(null);
                                destManufactureAccessory.setOrderState("未下单");
                                addManufactureAccessoryList.add(destManufactureAccessory);
                            }
                        }
                    }
                }
                manufactureAccessoryService.addManufactureAccessoryBatch(addManufactureAccessoryList);
                refreshAccessoryData(orderName);
            }
            return 0;
        } else {
            return 1;
        }
    }


    @RequestMapping(value = "/addadditionalsizename",method = RequestMethod.POST)
    @ResponseBody
    public int addAdditionalSizeName(@RequestParam("orderClothesListJson")String orderClothesListJson,
                                       @RequestParam("sizeName")String sizeName,
                                       @RequestParam("orderName")String orderName){
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        OrderClothes std = orderClothesService.getOrderClothesByOrderName(orderName).get(0);
        List<OrderClothes> orderClothesList = gson.fromJson(orderClothesListJson,new TypeToken<List<OrderClothes>>(){}.getType());
        for (OrderClothes orderClothes : orderClothesList){
            orderClothes.setStyleDescription(std.getStyleDescription());
            orderClothes.setUserName(std.getUserName());
            orderClothes.setSizeName(sizeName);
            orderClothes.setCustomerName(std.getCustomerName());
            String uuid = UUID.randomUUID().toString();
            orderClothes.setGuid(uuid);
            orderClothes.setDeadLine(std.getDeadLine());
            orderClothes.setDeliveryDate(std.getDeliveryDate());
            orderClothes.setPurchaseMethod(std.getPurchaseMethod());
            orderClothes.setSeason(std.getSeason());
        }
        int res = orderClothesService.addOrderClothes(orderClothesList);
        if (res == 0){
            // 更新面料的数量
            refreshFabricData(orderName);
            // 分色分码的要新增，分色不分码的也要新增，分码不分色的要改变数量，不分色不分码的要改变数量
            List<ManufactureAccessory> manufactureAccessoryList = manufactureAccessoryService.getManufactureAccessoryByOrder(orderName, 0);
            List<ManufactureAccessory> accessoryKeyList = manufactureAccessoryService.getAccessoryKeyByOrder(orderName, 0, null);
            List<ManufactureAccessory> addManufactureAccessoryList = new ArrayList<>();
            if (manufactureAccessoryList != null && !manufactureAccessoryList.isEmpty()){
                for (ManufactureAccessory accessoryKey : accessoryKeyList){
                    if (accessoryKey.getColorChild().equals(0) && accessoryKey.getSizeChild().equals(1)){
                        boolean flag = false;
                        for (ManufactureAccessory manufactureAccessory : manufactureAccessoryList){
                            if (manufactureAccessory.getAccessoryKey().equals(accessoryKey.getAccessoryKey()) && !flag){
                                flag = true;
                                ManufactureAccessory destManufactureAccessory = manufactureAccessory;
                                destManufactureAccessory.setSizeName(sizeName);
                                destManufactureAccessory.setOrderCount(0);
                                destManufactureAccessory.setAccessoryCount(0f);
                                destManufactureAccessory.setAccessoryAdd(0f);
                                destManufactureAccessory.setSumMoney(0f);
                                destManufactureAccessory.setCheckState("未提交");
                                destManufactureAccessory.setCheckNumber(null);
                                destManufactureAccessory.setOrderState("未下单");
                                addManufactureAccessoryList.add(destManufactureAccessory);
                            }
                        }
                    }
                    if (accessoryKey.getSizeChild().equals(1) && (accessoryKey.getColorChild().equals(1) || accessoryKey.getColorChild().equals(2))){
                        for (ManufactureAccessory manufactureAccessory : manufactureAccessoryList){
                            if (manufactureAccessory.getAccessoryKey().equals(accessoryKey.getAccessoryKey())){
                                ManufactureAccessory destManufactureAccessory = manufactureAccessory;
                                destManufactureAccessory.setSizeName(sizeName);
                                destManufactureAccessory.setOrderCount(0);
                                destManufactureAccessory.setAccessoryCount(0f);
                                destManufactureAccessory.setAccessoryAdd(0f);
                                destManufactureAccessory.setSumMoney(0f);
                                destManufactureAccessory.setCheckState("未提交");
                                destManufactureAccessory.setCheckNumber(null);
                                destManufactureAccessory.setOrderState("未下单");
                                addManufactureAccessoryList.add(destManufactureAccessory);
                            }
                        }
                    }

                }
                manufactureAccessoryService.addManufactureAccessoryBatch(addManufactureAccessoryList);
                refreshAccessoryData(orderName);
            }
            return 0;
        } else {
            return 1;
        }
    }


    @RequestMapping(value = "/getorderclothesdetailbyorder", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getOrderClothesDetailByOrder(@RequestParam("orderName")String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<CutQueryLeak> cutQueryLeakList = orderClothesService.getOrderInfoByName(orderName);
        List<String> colorList = new ArrayList<>();
        List<String> sizeList = new ArrayList<>();
        for (CutQueryLeak cutQueryLeak : cutQueryLeakList){
            if (!colorList.contains(cutQueryLeak.getColorName())){
                colorList.add(cutQueryLeak.getColorName());
            }
            if (!sizeList.contains(cutQueryLeak.getSizeName())){
                sizeList.add(cutQueryLeak.getSizeName());
            }
        }
        for (String color : colorList){
            Map<String, Object> sizeMap = new LinkedHashMap<>();
            for (String size : sizeList){
                int orderCount = 0;
                for (CutQueryLeak cutQueryLeak : cutQueryLeakList){
                    if (color.equals(cutQueryLeak.getColorName()) && size.equals(cutQueryLeak.getSizeName())){
                        orderCount = cutQueryLeak.getOrderCount();
                    }
                }
                sizeMap.put(size, orderCount);
            }
            map.put(color, sizeMap);
        }
        return map;
    }

    @RequestMapping(value = "/getallcustomername", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getAllCustomerName(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> customerNameList = orderClothesService.getAllCustomerName();
        map.put("customerNameList", customerNameList);
        return map;
    }

    @RequestMapping(value = "/getAllProcessDetail",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getAllProcessDetail(@RequestParam("orderName")String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        ManufactureOrder manufactureOrder = manufactureOrderService.getManufactureOrderByOrderName(orderName);
        StyleImage styleImage = styleImageService.getStyleImageByOrder(orderName);
        int orderCount = orderClothesService.getOrderTotalCount(orderName);
        map.put("orderCount", orderCount);
        if (manufactureOrder != null){
            map.put("manufactureOrder", manufactureOrder);
        }
        List<OrderMeasure> orderMeasureList = orderMeasureService.getOrderMeasureByOrder(orderName);
        if (orderMeasureList != null && orderMeasureList.size() > 0){
            OrderMeasure orderMeasure = orderMeasureList.get(0);
            map.put("orderMeasure", orderMeasure);
        }
        if (styleImage != null){
            map.put("styleImage", styleImage);
        }
        List<String> fabricNameList = new ArrayList<>();
        List<OrderClothes> orderClothesList = orderClothesService.getOrderClothesByOrderName(orderName);
        if (orderClothesList != null && orderClothesList.size() > 0){
            map.put("orderClothesList", orderClothesList);
        }
        List<ManufactureFabric> manufactureFabricList = manufactureFabricService.getManufactureFabricByOrder(orderName, null);
        if (manufactureFabricList != null && manufactureFabricList.size() > 0){
            map.put("manufactureFabricList", manufactureFabricList);
            for (ManufactureFabric manufactureFabric : manufactureFabricList){
                if (!fabricNameList.contains(manufactureFabric.getFabricName())){
                    fabricNameList.add(manufactureFabric.getFabricName());
                }
            }
        }
        if (fabricNameList.size() > 0){
            map.put("fabricNameList", fabricNameList);
        }
        List<ManufactureAccessory> manufactureAccessoryList = manufactureAccessoryService.getManufactureAccessoryByOrder(orderName, 0);
        if (manufactureAccessoryList != null && manufactureAccessoryList.size() > 0){
            map.put("manufactureAccessoryList", manufactureAccessoryList);
        }
        List<ManufactureAccessory> sewAccessoryNameList = manufactureAccessoryService.getAccessoryNameByOrderName(orderName, 0, "车缝");
        if (sewAccessoryNameList != null && sewAccessoryNameList.size() > 0){
            map.put("sewAccessoryNameList", sewAccessoryNameList);
        }
        List<ManufactureAccessory> packAccessoryNameList = manufactureAccessoryService.getAccessoryNameByOrderName(orderName, 0, "包装");
        if (packAccessoryNameList != null && packAccessoryNameList.size() > 0){
            map.put("packAccessoryNameList", packAccessoryNameList);
        }
        List<String> colorList = orderClothesService.getOrderColorNamesByOrder(orderName);
        if (colorList != null && colorList.size() > 0){
            map.put("colorList", colorList);
        }
        return map;
    }


    @RequestMapping(value = "/getorderclothesbyordername", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getOrderClothesByOrderName(@RequestParam("orderName")String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<OrderClothes> orderClothesList = orderClothesService.getOrderClothesByOrderName(orderName);
        map.put("data", orderClothesList);
        map.put("count", orderClothesList.size());
        map.put("code", 0);
        return map;
    }


    @RequestMapping(value = "/getorderclothesandcolorsizebyordername", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getOrderClothesAndColorSizeByOrderName(@RequestParam("orderName")String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<OrderClothes> orderClothesList = orderClothesService.getOrderClothesByOrderName(orderName);
        List<String> colorNameList = orderClothesService.getOrderColorNamesByOrder(orderName);
        List<String> sizeNameList = orderClothesService.getOrderSizeNamesByOrder(orderName);
        map.put("colorNameList", colorNameList);
        map.put("sizeNameList", sizeNameList);
        map.put("data", orderClothesList);
        map.put("count", orderClothesList.size());
        map.put("code", 0);
        return map;
    }

    @RequestMapping(value = "/updateorderclothesbyid", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> updateOrderClothesByID(OrderClothes orderClothes){
        Map<String, Object> map = new LinkedHashMap<>();
        int res = orderClothesService.updateOrderClothesByID(orderClothes);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/deleteorderclothesbyid", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> deleteOrderClothesByID(@RequestParam("orderClothesID") Integer orderClothesID){
        Map<String, Object> map = new LinkedHashMap<>();
        int res = orderClothesService.deleteOrderClothesByID(orderClothesID);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/deleteorderclothesbatch", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> deleteOrderClothesBatch(@RequestParam("orderClothesIDList")List<Integer> orderClothesIDList){
        Map<String, Object> map = new LinkedHashMap<>();
        int res = orderClothesService.deleteOrderClothesBatch(orderClothesIDList);
        map.put("result", res);
        return map;
    }


    @RequestMapping(value = "/getorderandversionbysuborder", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getOrderAndVersionBySubOrder(@RequestParam("subOrderName")String subOrderName,@RequestParam("page") int page,@RequestParam("limit") int limit){
        Map<String, Object> map = new LinkedHashMap<>();
//        Integer count = orderClothesService.getCountOrderAndVersionByVersion(subOrderName);
        List<OrderClothes> orderClothesList = orderClothesService.getOrderAndVersionByVersionPage(subOrderName,(page-1)*limit,limit);
        map.put("data",orderClothesList);
        map.put("msg","");
        map.put("count", 20);
        map.put("code",0);
        return map;
    }

    @RequestMapping(value = "/minigetorderandversionbysuborder", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> miniGetOrderAndVersionBySubOrder(@RequestParam("subOrderName")String subOrderName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<OrderClothes> orderClothesList = orderClothesService.getOrderAndVersionHint(subOrderName);
        map.put("data",orderClothesList);
        return map;
    }

    @RequestMapping(value = "/getorderandversionbysubversion", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getOrderAndVersionBySubVersion(@RequestParam("subVersion") String subVersion,@RequestParam("page") int page,@RequestParam("limit") int limit){
        Map<String, Object> map = new LinkedHashMap<>();
        Integer count = orderClothesService.getCountOrderAndVersionByVersion(subVersion);
        List<OrderClothes> orderClothesList = orderClothesService.getOrderAndVersionByVersionPage(subVersion,(page-1)*limit,limit);
        map.put("data",orderClothesList);
        map.put("msg","");
        map.put("count",count==null?0:count);
        map.put("code",0);
        return map;
    }

    @RequestMapping(value = "/minigetorderandversionbysubversion", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> miniGetOrderAndVersionBySubVersion(@RequestParam("subVersion") String subVersion){
        Map<String, Object> map = new LinkedHashMap<>();
        List<OrderClothes> orderClothesList = orderClothesService.getOrderAndVersionHint(subVersion);
        map.put("data",orderClothesList);
        return map;
    }


    @RequestMapping(value = "/getordercountbycolorsizelist", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getPlanInfoByOrderType(@RequestParam("orderName")String orderName,
                                                      @RequestParam("colorStr")String colorStr,
                                                      @RequestParam("sizeStr")String sizeStr){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> colorList = new ArrayList<>();
        List<String> sizeList = new ArrayList<>();
        if ("".equals(colorStr) || colorStr.contains("全部")){
            colorList = null;
        } else {
            colorList = Arrays.asList(colorStr.split(","));
        }
        if ("".equals(sizeStr) || sizeStr.contains("全部")){
            sizeList = null;
        } else {
            sizeList = Arrays.asList(sizeStr.split(","));
        }
        int planCount = orderClothesService.getOrderCountByColorSizeList(orderName, colorList, sizeList);
        map.put("planCount", planCount);
        return map;

    }

    @RequestMapping(value = "/changeorderstate",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> changeOrderState(@RequestParam("orderName")String orderName,
                                      @RequestParam("orderState")Integer orderState){
        Map<String, Object> map = new LinkedHashMap<>();
        int res = orderClothesService.updateOrderStateByOrder(orderName, orderState);
        map.put("result", res);
        return map;
    }


    // 刷新数据

    private void refreshAccessoryData(String orderName){
        List<ManufactureAccessory> manufactureAccessoryList = manufactureAccessoryService.getManufactureAccessoryByOrder(orderName, null);
        for (ManufactureAccessory manufactureAccessory : manufactureAccessoryList){
            if (manufactureAccessory.getColorName().equals("通用") && manufactureAccessory.getSizeName().equals("通用")){
                int orderCount1 = orderClothesService.getOrderTotalCount(orderName);
                manufactureAccessory.setOrderCount(orderCount1);
            } else if (manufactureAccessory.getColorName().equals("通用")){
                List<String> sizeList = Arrays.asList(manufactureAccessory.getSizeName().split(","));
                int orderCount1 = orderClothesService.getOrderCountByColorSizeList(orderName, null, sizeList);
                manufactureAccessory.setOrderCount(orderCount1);
            } else if (manufactureAccessory.getSizeName().equals("通用")){
                List<String> colorList = Arrays.asList(manufactureAccessory.getColorName().split(","));
                int orderCount1 = orderClothesService.getOrderCountByColorSizeList(orderName, colorList, null);
                manufactureAccessory.setOrderCount(orderCount1);
            } else {
                List<String> colorList = Arrays.asList(manufactureAccessory.getColorName().split(","));
                List<String> sizeList = Arrays.asList(manufactureAccessory.getSizeName().split(","));
                int orderCount1 = orderClothesService.getOrderCountByColorSizeList(orderName, colorList, sizeList);
                manufactureAccessory.setOrderCount(orderCount1);
            }
            manufactureAccessory.setCheckNumber(null);
            manufactureAccessory.setCheckState(null);
            manufactureAccessory.setOrderState(null);
            manufactureAccessory.setAccessoryCount(null);
            manufactureAccessory.setSumMoney(null);
        }
        manufactureAccessoryService.updateManufactureAccessoryBatch(manufactureAccessoryList);
    }


    private void refreshFabricData(String orderName){
        List<ManufactureFabric> manufactureFabricList = manufactureFabricService.getManufactureFabricByOrder(orderName, null);
        List<OrderClothes> orderClothesList = orderClothesService.getOrderClothesCountGroupByColor(orderName);
        for (ManufactureFabric manufactureFabric : manufactureFabricList){
            for (OrderClothes orderClothes : orderClothesList){
                if (manufactureFabric.getColorName().equals(orderClothes.getColorName())){
                    manufactureFabric.setOrderCount(orderClothes.getCount());
                    manufactureFabric.setFabricCount(null);
                    manufactureFabric.setFabricActCount(null);
                    manufactureFabric.setSumMoney(null);
                    manufactureFabric.setCheckNumber(null);
                    manufactureFabric.setOrderState(null);
                    manufactureFabric.setCheckState(null);
                }
            }
        }
        manufactureFabricService.updateManufactureFabricBatch(manufactureFabricList);
    }


}
