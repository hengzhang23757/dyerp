package com.example.erp01.action;

import com.example.erp01.model.*;
import com.example.erp01.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


@Configuration      //1.主要用于标记配置类，兼备Component的效果。
@EnableScheduling   // 2.开启定时任务
@Controller
@RequestMapping(value = "erp")
public class HomePageController {

    @Autowired
    private HomePageService homePageService;
    @Autowired
    private OrderClothesService orderClothesService;
    @Autowired
    private ManufactureFabricService manufactureFabricService;
    @Autowired
    private ManufactureAccessoryService manufactureAccessoryService;
    @Autowired
    private ProcessRequirementService processRequirementService;
    @Autowired
    private OrderMeasureService orderMeasureService;
    @Autowired
    private TailorService tailorService;
    @Autowired
    private FabricDetailService fabricDetailService;
    @Autowired
    private LooseFabricService looseFabricService;
    @Autowired
    private EmbInStoreService embInStoreService;
    @Autowired
    private PieceWorkService pieceWorkService;


    @RequestMapping("/weekReportStart")
    public String weekReportStart(){
        return "homepage/weekReport";
    }


//    @Scheduled(cron = "0 0 0/2 * * ? ")
    public void homePageInit(){
        List<OrderClothes> orderClothesList = orderClothesService.getLastSeasonSummary();
        List<HomePage> homePageList = new ArrayList<>();
        System.out.println("开始执行====================");
        for (OrderClothes orderClothes : orderClothesList){
            String orderName = orderClothes.getOrderName();
            HomePage homePage = new HomePage();
            homePage.setOrderName(orderName);
            homePage.setClothesVersionNumber(orderClothes.getClothesVersionNumber());
            homePage.setOrderCount(orderClothes.getOrderSum());
            homePage.setSeason(orderClothes.getSeason());
            homePage.setCustomerName(orderClothes.getCustomerName());
            homePage.setReceivedDate(orderClothes.getDeadLine());
            List<ManufactureFabric> manufactureFabricList = manufactureFabricService.getManufactureFabricSummaryByOrder(orderName);
            List<ManufactureAccessory> manufactureAccessoryList = manufactureAccessoryService.getManufactureAccessoryByOrder(orderName, null);
            List<ProcessRequirement> processRequirementList = processRequirementService.getProcessRequirementByOrder(orderName);
            List<OrderMeasure> orderMeasureList = orderMeasureService.getOrderMeasureByOrder(orderName);
            List<FabricDetail> fabricDetailList = fabricDetailService.getFabricDetailByOrder(orderName);
            List<LooseFabric> looseFabricList = looseFabricService.getLooseFabricByOrderFabric(orderName, null, null, null);
            int orderInfoPart = 1;
            float returnFabricPercent = 0f;
            float looseFabricPercent = 0f;
            if (manufactureFabricList != null && !manufactureFabricList.isEmpty()){
                float fabricDetailCount = 0;
                float fabricCount = 0;
                float looseFabricCount = 0;
                if (fabricDetailList != null && !fabricDetailList.isEmpty()){
                    for (ManufactureFabric manufactureFabric : manufactureFabricList){
                        fabricCount += manufactureFabric.getFabricCount();
                    }
                    for (FabricDetail fabricDetail : fabricDetailList){
                        fabricDetailCount += fabricDetail.getWeight();
                    }
                    if (fabricCount > 0){
                        returnFabricPercent =  fabricDetailCount/fabricCount;
                    }
                }
                if (looseFabricList != null && !looseFabricList.isEmpty()) {
                    if (fabricDetailCount > 0) {
                        for (LooseFabric looseFabric : looseFabricList){
                            looseFabricCount += looseFabric.getWeight();
                        }
                        looseFabricPercent = looseFabricCount/fabricDetailCount;
                    }
                }
                orderInfoPart += 1;
            }
            homePage.setReturnFabricPercent(returnFabricPercent);
            homePage.setLooseFabricPercent(looseFabricPercent);
            if (manufactureAccessoryList != null && !manufactureAccessoryList.isEmpty()){
                orderInfoPart += 1;
            }
            if (processRequirementList != null && !processRequirementList.isEmpty()){
                orderInfoPart += 1;
            }
            if (orderMeasureList != null && !orderMeasureList.isEmpty()){
                orderInfoPart += 1;
            }
            homePage.setOrderInfoPart(orderInfoPart);
            int cutCount = tailorService.getWellCountByInfo(orderName, null,null,null,null,null,0);
            homePage.setCutCount(cutCount);
            int embInStoreCount = embInStoreService.getTotalEmbInStoreCountByOrder(orderName);
            homePage.setEmbCount(embInStoreCount);
            int hangCount1 = pieceWorkService.getPieceCountByOrderColorSize(orderName, 25, null, null);
            int hangCount2 = pieceWorkService.getPieceCountByOrderColorSize(orderName, 2025, null, null);
            homePage.setHangCount(hangCount1+hangCount2);
            int clothesCount1 = pieceWorkService.getPieceCountByOrderColorSize(orderName, 555, null, null);
            int clothesCount2 = pieceWorkService.getPieceCountByOrderColorSize(orderName, 6555, null, null);
            homePage.setClothesCount(clothesCount1 + clothesCount2);
            int packageCount1 = pieceWorkService.getPieceCountByOrderColorSize(orderName, 564, null, null);
            int packageCount2 = pieceWorkService.getPieceCountByOrderColorSize(orderName, 6563, null, null);
            homePage.setPackageCount(packageCount1 + packageCount2);
            if ((clothesCount1 + clothesCount2) >= orderClothes.getOrderSum()){
                homePage.setIsFinish("完工");
            } else{
                homePage.setIsFinish("未完工");
            }
            homePageList.add(homePage);
        }
        List<HomePage> insertHomepageList = new ArrayList<>();
        List<HomePage> updateHomePageList = new ArrayList<>();
        List<HomePage> threeMonthList = homePageService.getHomePageRecentThreeMonth();
        for (HomePage homePage1 : homePageList){
            boolean flag = false;
            for (HomePage homePage2 : threeMonthList){
                if (homePage1.getOrderName().equals(homePage2.getOrderName())){
                    if (homePage2.getIsFinish().equals("完工")){
                        homePage1.setIsFinish("完工");
                    }
                    updateHomePageList.add(homePage1);
                    flag = true;
                }
            }
            if (!flag){
                insertHomepageList.add(homePage1);
            }
        }
        int res1 = homePageService.addHomePageDataBatch(insertHomepageList);
        int res2 = homePageService.updateHomePageData(updateHomePageList);
        /**
         * 只获取updateTime为近三个月的
         * 如果orderName 存在 就修改  如果不存在就插入
         * 获取后根据updateTime排序  最近更新的放在最前面
         *
         * **/
        if (res1 == 0 && res2 == 0){
            System.out.println("首页更新完成");
        } else {
            System.out.println("首页更新失败");
        }

    }


    @RequestMapping(value = "/gethomepagereport", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> getHomePageReport(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<HomePage> homePageList = homePageService.getHomePageRecentThreeMonth();
        for (HomePage homePage : homePageList){
            homePage.setOrderPartPercent((float)homePage.getOrderInfoPart()/5);
            homePage.setCutPercent((float)homePage.getCutCount()/homePage.getOrderCount());
            homePage.setEmbPercent((float)homePage.getEmbCount()/homePage.getOrderCount());
            homePage.setClothesPercent((float)homePage.getClothesCount()/homePage.getOrderCount());
            homePage.setHangPercent((float)homePage.getHangCount()/homePage.getOrderCount());
        }
        map.put("data", homePageList);
        map.put("code", 0);
        map.put("count", homePageList.size());
        return map;
    }


}
