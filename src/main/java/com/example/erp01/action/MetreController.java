package com.example.erp01.action;

import com.example.erp01.model.InnerBoard;
import com.example.erp01.model.Metre;
import com.example.erp01.model.OrderProcedure;
import com.example.erp01.model.ProcedureInfo;
import com.example.erp01.service.HangSalaryService;
import com.example.erp01.service.MetreService;
import com.example.erp01.service.OrderClothesService;
import com.example.erp01.service.OrderProcedureService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
@RequestMapping(value = "erp")
public class MetreController {

    @Autowired
    private MetreService metreService;

    @Autowired
    private OrderProcedureService orderProcedureService;

    @Autowired
    private OrderClothesService orderClothesService;

    @Autowired
    private HangSalaryService hangSalaryService;

    @RequestMapping("/metreStart")
    public String metreStart(Model model){
        model.addAttribute("bigMenuTag",9);
        model.addAttribute("menuTag",95);
        return "report/metre";
    }

    @RequestMapping("/beatStart")
    public String beatStart(Model model){
        return "ieInfo/beat";
    }

    @RequestMapping(value = "/getemployeenamesbyorderprocedure",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getEmployeeNamesByOrderProcedure(@RequestParam("orderName") String orderName,
                                                               @RequestParam("procedureNumber")Integer procedureNumber){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> employeeNameList = hangSalaryService.getEmployeeNamesByOrderProcedure(orderName, procedureNumber);
        map.put("employeeNameList",employeeNameList);
        return map;
    }

    @RequestMapping(value = "/getmetrebyordertimeemp",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getMetreByOrderTimeEmp(@RequestParam("procedureNumber")Integer procedureNumber,
                                                     @RequestParam("orderName") String orderName,
                                                     @RequestParam("employeeName") String employeeName,
                                                     @RequestParam("from") String from,
                                                     @RequestParam("to") String to){
        Map<String, Object> map = new LinkedHashMap<>();
        if ("".equals(employeeName)){
            employeeName = null;
        }
        try{
            Date fromDate;
            if ("".equals(from)){
                fromDate = null;
            }else{
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                fromDate = sdf.parse(from);
            }
            Date toDate;
            if ("".equals(to)){
                toDate = null;
            }else{
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                toDate = sdf.parse(to);
            }
            List<Metre> metreList = metreService.getMetreByOrderTimeEmp(procedureNumber, orderName, employeeName, fromDate, toDate);
            String tmpClothesVersionNumber = orderClothesService.getVersionNumberByOrderName(orderName);
            ProcedureInfo pi = orderProcedureService.getProcedureInfoByOrderProcedureNumber(orderName, procedureNumber);
            String procedureName = pi.getProcedureName();
            for (Metre metre : metreList){
                metre.setProcedureName(procedureName);
                metre.setClothesVersionNumber(tmpClothesVersionNumber);
            }
            map.put("metreList",metreList);
            return map;
        }catch (ParseException e){
            e.printStackTrace();
        }
        return map;
    }

    @RequestMapping(value = "/getmetrebyinfo",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getMetreByInfo(@RequestParam(value = "orderName") String orderName,
                                             @RequestParam(value = "procedureNumberList[]")List<Integer> procedureNumberList,
                                             @RequestParam(value = "groupNameList[]", required = false) List<String> groupNameList,
                                             @RequestParam(value = "employeeNameList[]", required = false) List<String> employeeNameList,
                                             @RequestParam(value = "procedureInfo") String procedureInfo,
                                             @RequestParam(value = "from", required = false) String from,
                                             @RequestParam(value = "to", required = false) String to,
                                             @RequestParam(value = "page", required = false) Integer page,
                                             @RequestParam(value = "limit", required = false) Integer limit){
        Map<String, Object> map = new LinkedHashMap<>();
        try{
            Gson gson = new Gson();
            Map<Integer, String> procedureMap = gson.fromJson(procedureInfo,new TypeToken<Map<Integer,String>>() {}.getType());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date fromDate;
            Date toDate;
            if (from != null){
                fromDate = sdf.parse(from);
            }else{
                fromDate = null;
            }
            if (to != null){
                toDate = sdf.parse(to);
            }else {
                toDate = null;
            }
            List<Metre> metreList = metreService.getMetreByInfo(orderName, procedureNumberList, groupNameList, employeeNameList, fromDate, toDate);
            for (Metre metre : metreList){
                metre.setProcedureName(procedureMap.get(metre.getProcedureNumber()));
                if (metre.getTimeCount() == 0){
                    metre.setTimeCount((double)(metre.getEndTime().getTime() - metre.getBeginTime().getTime())/(1000*60));
                }
            }
            map.put("metreList",metreList);
            map.put("metreList",metreList);
            return map;
        }catch (ParseException e){
            e.printStackTrace();
        }
        return map;
    }

}
