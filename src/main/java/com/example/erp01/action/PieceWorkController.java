package com.example.erp01.action;

import com.example.erp01.model.*;
import com.example.erp01.service.*;
import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
@RequestMapping(value = "erp")
public class PieceWorkController {
    private static final Logger log = LoggerFactory.getLogger(PieceWorkController.class);
    @Autowired
    private PieceWorkService pieceWorkService;
    @Autowired
    private DispatchService dispatchService;
    @Autowired
    private ProcedureTemplateService procedureTemplateService;
    @Autowired
    private OrderProcedureService orderProcedureService;
    @Autowired
    private TailorService tailorService;
    @Autowired
    private StorageService storageService;
    @Autowired
    private OrderClothesService orderClothesService;
    @Autowired
    private HangSalaryService hangSalaryService;
    @Autowired
    private HourEmpService hourEmpService;
    @Autowired
    private SpecialProcedureService specialProcedureService;
    @Autowired
    private FixedSalaryService fixedSalaryService;
    @Autowired
    private CutInStoreService cutInStoreService;
    @Autowired
    private EmbInStoreService embInStoreService;
    @Autowired
    private EmbOutStoreService embOutStoreService;
    @Autowired
    private FinishTailorService finishTailorService;
    @Autowired
    private EmployeeService employeeService;
    @Autowired
    private KeyDataDeleteService keyDataDeleteService;




    @RequestMapping(value = "/pieceWorkStatisticStart")
    public String pieceWorkStatisticStart(Model model){
        model.addAttribute("bigMenuTag",10);
        model.addAttribute("menuTag",103);
        return "miniProgram/pieceWorkStatistic";
    }

    @RequestMapping(value = "/pieceWorkLeakStart")
    public String pieceWorkLeakStart(){
        return "pieceWork/pieceWorkLeak";
    }

    @RequestMapping(value = "/pieceWorkManageStart")
    public String pieceWorkManageStart(){
        return "pieceWork/pieceWorkManage";
    }

    @RequestMapping(value = "/pieceWorkDetailQueryStart")
    public String pieceWorkDetailQueryStart(Model model){
        model.addAttribute("bigMenuTag",5);
        model.addAttribute("menuTag",59);
        return "miniProgram/pieceWorkDetailQuery";
    }

    @RequestMapping(value = "/manualInputStart")
    public String manualInputStart(Model model){
        model.addAttribute("bigMenuTag",10);
        model.addAttribute("menuTag",105);
        return "miniProgram/manualInput";
    }

    @RequestMapping(value = "/unloadStart")
    public String unloadStart(Model model){
        model.addAttribute("bigMenuTag",14);
        model.addAttribute("menuTag",141);
        return "unload/unload";
    }

    @RequestMapping(value = "/unloadReportStart")
    public String unloadReportStart(Model model){
        model.addAttribute("bigMenuTag",14);
        model.addAttribute("menuTag",142);
        return "unload/unloadReport";
    }

    // 修改 ： 如果一个工人被派了多个工序，只要其中一个扫描部位正确就计件
    @RequestMapping(value = "/miniaddpieceworkbatchnew",method = RequestMethod.POST)
    @ResponseBody
    public int miniAddPieceWorkBatchNew(@RequestParam("pieceWorkJson") String pieceWorkJson,
                                        @RequestParam("pieceType") Integer pieceType){
        JsonParser jsonParser = new JsonParser();
        try{
            JsonObject json = (JsonObject) jsonParser.parse(pieceWorkJson);
            String employeeNumber = json.get("employeeNumber").getAsString();
            String tailorQcodeIDString = json.get("tailorQcodeID").getAsString();
            Integer tailorQcodeID = Integer.parseInt(tailorQcodeIDString.trim());
            String orderName = json.get("orderName").getAsString();
            int bedNumber = Integer.parseInt(json.get("bedNumber").getAsString());
            String colorName = json.get("colorName").getAsString();
            String sizeName = json.get("sizeName").getAsString();
            String partName = json.get("partName").getAsString();
            int packageNumber = Integer.parseInt(json.get("packageNumber").getAsString());
            int layerCount = Integer.parseInt(json.get("layerCount").getAsString());
            List<PieceWork> pieceWorkList = new ArrayList<>();
            List<ProcedureInfo> procedureInfoList = dispatchService.getProcedureInfoByOrderEmp(orderName,employeeNumber);
            if (pieceType == 0){
                boolean isPart = false;
                for (ProcedureInfo procedureInfo:procedureInfoList) {
                    int tmpProcedureNumber = procedureInfo.getProcedureNumber();
                    ProcedureInfo procedureInfo1 = orderProcedureService.getProcedureInfoByOrderProcedureNumber(orderName, tmpProcedureNumber);
                    if (procedureInfo1 != null){
                        if (procedureInfo1.getScanPart() != null){
                            if (partName.contains(procedureInfo1.getScanPart())){
                                PieceWork pieceWork = new PieceWork(employeeNumber,orderName,bedNumber,colorName,sizeName,packageNumber,layerCount,tmpProcedureNumber,tailorQcodeID);
                                pieceWorkList.add(pieceWork);
                                isPart = true;
                            }
                        }
                    }

                }
                if (!isPart){
                    return 3;
                }
                return pieceWorkService.addPieceWorkBatchNew(pieceWorkList);
            }else {
                boolean isPart = false;
                for (ProcedureInfo procedureInfo:procedureInfoList) {
                    int tmpProcedureNumber = procedureInfo.getProcedureNumber();
                    List<PieceWork> tmpPieceWorkList = pieceWorkService.getPieceWorkByOrderBedPack(orderName, bedNumber, packageNumber, tmpProcedureNumber);
                    if (tmpPieceWorkList == null || tmpPieceWorkList.size() == 0){
                        ProcedureInfo procedureInfo1 = orderProcedureService.getProcedureInfoByOrderProcedureNumber(orderName, tmpProcedureNumber);
                        if (procedureInfo1 != null){
                            if (procedureInfo1.getScanPart() != null){
                                if (partName.contains(procedureInfo1.getScanPart())){
                                    PieceWork pieceWork = new PieceWork(employeeNumber,orderName,bedNumber,colorName,sizeName,packageNumber,layerCount,tmpProcedureNumber,tailorQcodeID);
                                    pieceWorkList.add(pieceWork);
                                    isPart = true;
                                }
                            }
                        }
                    }
                }
                if (!isPart){
                    return 3;
                }
                return pieceWorkService.addPieceWorkBatchNew(pieceWorkList);
            }
        }catch (JsonIOException e){
            e.printStackTrace();
        }catch (JsonSyntaxException e){
            e.printStackTrace();
        }
        return 1;
    }


    @RequestMapping(value = "/miniaddpieceworkbatchnewram",method = RequestMethod.POST)
    @ResponseBody
    public int miniAddPieceWorkBatchNewRam(@RequestParam("pieceWorkJson") String pieceWorkJson,
                                           @RequestParam("pieceType") Integer pieceType){
        JsonParser jsonParser = new JsonParser();
        try{
            JsonObject json = (JsonObject) jsonParser.parse(pieceWorkJson);
            String employeeNumber = json.get("employeeNumber").getAsString();
            String employeeName = json.get("employeeName").getAsString();
            String groupName = json.get("groupName").getAsString();
            String tailorQcodeIDString = json.get("tailorQcodeID").getAsString();
            Integer tailorQcodeID = Integer.parseInt(tailorQcodeIDString.trim());
            String orderName = json.get("orderName").getAsString();
            String clothesVersionNumber = json.get("clothesVersionNumber").getAsString();
            int bedNumber = Integer.parseInt(json.get("bedNumber").getAsString());
            String colorName = json.get("colorName").getAsString();
            String sizeName = json.get("sizeName").getAsString();
            String partName = json.get("partName").getAsString();
            int packageNumber = Integer.parseInt(json.get("packageNumber").getAsString());
            int layerCount = Integer.parseInt(json.get("layerCount").getAsString());
            List<ProcedureInfo> procedureInfoList = dispatchService.getProcedureInfoByOrderEmp(orderName,employeeNumber);
            List<PieceWork> pieceWorkList = new ArrayList<>();
            if (pieceType == 0){
                boolean isPart = false;
                for (ProcedureInfo procedureInfo:procedureInfoList) {
                    int tmpProcedureNumber = procedureInfo.getProcedureNumber();
                    String tmpProcedureName = procedureInfo.getProcedureName();
                    ProcedureInfo procedureInfo1 = orderProcedureService.getProcedureInfoByOrderProcedureNumber(orderName, tmpProcedureNumber);
                    if (procedureInfo1 != null){
                        if (procedureInfo1.getScanPart() != null){
                            if (partName.contains(procedureInfo1.getScanPart())){
                                PieceWork pieceWork = new PieceWork(employeeNumber,employeeName,groupName,orderName,clothesVersionNumber,bedNumber,colorName,sizeName,packageNumber,layerCount,tmpProcedureNumber,tmpProcedureName,tailorQcodeID);
                                pieceWorkList.add(pieceWork);
                                isPart = true;
                            }
                        }
                    } else {
                        PieceWork pieceWork = new PieceWork(employeeNumber,employeeName,groupName,orderName,clothesVersionNumber,bedNumber,colorName,sizeName,packageNumber,layerCount,tmpProcedureNumber,tmpProcedureName,tailorQcodeID);
                        pieceWorkList.add(pieceWork);
                    }
                }
                if (!isPart){
                    return 3;
                }
                return pieceWorkService.addPieceWorkBatchNew(pieceWorkList);
            }else {
                boolean isPart = false;
                for (ProcedureInfo procedureInfo:procedureInfoList) {
                    int tmpProcedureNumber = procedureInfo.getProcedureNumber();
                    String tmpProcedureName = procedureInfo.getProcedureName();
                    List<PieceWork> tmpPieceWorkList = pieceWorkService.getPieceWorkByOrderBedPack(orderName, bedNumber, packageNumber, tmpProcedureNumber);
                    if (tmpPieceWorkList == null || tmpPieceWorkList.size() == 0){
                        ProcedureInfo procedureInfo1 = orderProcedureService.getProcedureInfoByOrderProcedureNumber(orderName, tmpProcedureNumber);
                        if (procedureInfo1 != null){
                            if (procedureInfo1.getScanPart() != null){
                                if (partName.contains(procedureInfo1.getScanPart())){
                                    PieceWork pieceWork = new PieceWork(employeeNumber,employeeName,groupName,orderName,clothesVersionNumber,bedNumber,colorName,sizeName,packageNumber,layerCount,tmpProcedureNumber,tmpProcedureName,tailorQcodeID);
                                    pieceWorkList.add(pieceWork);
                                    isPart = true;
                                }
                            }
                        }else {
                            PieceWork pieceWork = new PieceWork(employeeNumber,employeeName,groupName,orderName,clothesVersionNumber,bedNumber,colorName,sizeName,packageNumber,layerCount,tmpProcedureNumber,tmpProcedureName,tailorQcodeID);
                            pieceWorkList.add(pieceWork);
                        }
                    }
                }
                if (!isPart){
                    return 3;
                }
                return pieceWorkService.addPieceWorkBatchNew(pieceWorkList);
            }
        }catch (JsonIOException e){
            e.printStackTrace();
        }catch (JsonSyntaxException e){
            e.printStackTrace();
        }
        return 1;

    }




    @RequestMapping(value = "/minideletepiecework",method = RequestMethod.POST)
    @ResponseBody
    public int miniDeletePieceWork(Integer pieceWorkID){
        PieceWorkDelete pieceWorkDelete = keyDataDeleteService.getPieceWorkByDeleteId(pieceWorkID);
        pieceWorkDelete.setOperateUser(pieceWorkDelete.getEmployeeName());
        List<PieceWorkDelete> pieceWorkDeleteList = new ArrayList<>();
        pieceWorkDeleteList.add(pieceWorkDelete);
        keyDataDeleteService.addPieceWorkDeleteBatch(pieceWorkDeleteList);
        return pieceWorkService.deletePieceWork(pieceWorkID);
    }

    @RequestMapping(value = "/deletepiecework",method = RequestMethod.POST)
    @ResponseBody
    public int deletePieceWork(Integer pieceWorkID){
        SimpleDateFormat sformat = new SimpleDateFormat("yyyy-MM-dd");//日期格式
        PieceWork pieceWork = pieceWorkService.getPieceWorkByID(pieceWorkID);
        List<String> fixedSalaryList = fixedSalaryService.getAllFixedSalaryValue();
        String createTime = sformat.format(pieceWork.getCreateTime());
        String createMonth = createTime.substring(0, 7);
        for (String fixedValue : fixedSalaryList){
            if (createMonth.equals(fixedValue)){
                return 6;
            }
        }
        return pieceWorkService.deletePieceWork(pieceWorkID);
    }

    @RequestMapping(value = "/getallpiecework",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getAllPieceWork(){
        Map<String,Object> map = new HashMap<>();
        List<PieceWork> pieceWorkList = new ArrayList<>();
        pieceWorkList = pieceWorkService.getAllPieceWork();
        map.put("data",pieceWorkList);
        return map;
    }

    @RequestMapping(value = "/getdetailpiecework",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getDetailPieceWork(@RequestParam("from") Date from,
                                                 @RequestParam("to")Date to,
                                                 @RequestParam("groupName")String groupName,
                                                 @RequestParam("employeeNumber")String employeeNumber){
        if("".equals(groupName)){
            groupName = null;
        }
        if("".equals(employeeNumber)){
            employeeNumber = null;
        }
        Map<String,Object> map = new HashMap<>();
        List<PieceWork> pieceWorkList = pieceWorkService.getDetailPieceWork(from, to, groupName, employeeNumber);
        map.put("detailPieceWork",pieceWorkList);
        return map;

    }


    @RequestMapping(value = "/getpieceworktoday",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getPieceWorkToday(){
        Map<String,Object> map = new HashMap<>();
        List<PieceWorkResult> pieceWorkList = pieceWorkService.getPieceWorkToday();
        for (PieceWorkResult pieceWorkResult : pieceWorkList){
            String orderName = pieceWorkResult.getOrderName();
            Integer procedureNumber = pieceWorkResult.getProcedureNumber();
            String procedureName = "通用";
            ProcedureInfo pi = orderProcedureService.getProcedureInfoByOrderProcedureNumber(orderName,procedureNumber);
            if (pi != null){
                procedureName = pi.getProcedureName();
            }
            pieceWorkResult.setProcedureName(procedureName);
        }
        map.put("pieceWorkToday",pieceWorkList);
        return map;
    }

    @RequestMapping(value = "/getpieceworkthismonth",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getPieceWorkThisMonth(){

        Map<String,Object> map = new HashMap<>();
        List<PieceWorkResult> pieceWorkList = pieceWorkService.getPieceWorkThisMonth();
        for (PieceWorkResult pieceWorkResult : pieceWorkList){
            String orderName = pieceWorkResult.getOrderName();
            Integer procedureNumber = pieceWorkResult.getProcedureNumber();
            String procedureName = "通用";
            ProcedureInfo pi = orderProcedureService.getProcedureInfoByOrderProcedureNumber(orderName,procedureNumber);
            if (pi != null){
                procedureName = pi.getProcedureName();
            }
            pieceWorkResult.setProcedureName(procedureName);
        }
        map.put("pieceWorkThisMonth",pieceWorkList);
        return map;
    }

    @RequestMapping(value = "/minigetpieceworkemptoday",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getPieceWorkEmpToday(@RequestParam("employeeNumber")String employeeNumber){
        Map<String,Object> map = new HashMap<>();
        List<PieceWork> pieceWorkList = pieceWorkService.getPieceWorkEmpToday(employeeNumber);
        map.put("pieceWorkEmpToday",pieceWorkList);
        return map;
    }

    @RequestMapping(value = "/minigetpieceworkempthismonth",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getPieceWorkEmpThisMonth(@RequestParam("employeeNumber")String employeeNumber){
        Map<String,Object> map = new HashMap<>();
        List<PieceWork> pieceWorkList = pieceWorkService.getPieceWorkEmpThisMonth(employeeNumber);
        map.put("pieceWorkEmpThisMonth",pieceWorkList);
        return map;
    }

    @RequestMapping(value = "/querypiecework",method = RequestMethod.GET)
    public String queryPieceWork(@RequestParam("from") String from,
                                             @RequestParam("to")String to,
                                             @RequestParam("groupName")String groupName,
                                             @RequestParam("employeeNumber")String employeeNumber,
                                 Model model){
        if("".equals(groupName)){
            groupName = null;
        }
        if("".equals(employeeNumber)){
            employeeNumber = null;
        }
        Map<String,Object> map = new HashMap<>();
        try{
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date fromDate = sdf.parse(from);
            Date toDate = sdf.parse(to);
            List<SalaryCount> pieceWorkList = pieceWorkService.queryPieceWork(fromDate, toDate, groupName, employeeNumber);
            for (SalaryCount salaryCount : pieceWorkList){
                String orderName = salaryCount.getOrderName();
                Float price = 0f;
                String procedureName = "通用";
                Integer procedureCode = 0;
                Integer procedureNumber = salaryCount.getProcedureNumber();
                ProcedureInfo pi = orderProcedureService.getProcedureInfoByOrderProcedureNumber(orderName, procedureNumber);
                if (pi != null){
                    if (pi.getPiecePrice() != null){
                        price = pi.getPiecePrice();
                    }
                    if (pi.getProcedureName() != null){
                        procedureName = pi.getProcedureName();
                    }
                    if (pi.getProcedureCode() != null){
                        procedureCode = pi.getProcedureCode();
                    }

                }
                salaryCount.setProcedureCode(procedureCode);
                salaryCount.setProcedureName(procedureName);
                int pieceCount = salaryCount.getPieceCount();
                salaryCount.setSalary(price*pieceCount);
            }
            map.put("queryPieceWork",pieceWorkList);
            model.addAttribute("queryPieceWork",pieceWorkList);
        }catch (ParseException e){
            e.printStackTrace();
        }
        return "miniProgram/fb_pieceWorkStatisticList";

    }


    @RequestMapping(value = "/minigetthismonthsummary",method = RequestMethod.GET)
    @ResponseBody
    public PieceWorkSummary getThisMonthSummary(@RequestParam("employeeNumber")String employeeNumber){
        PieceWorkSummary ps = new PieceWorkSummary(0, 0, 0);
        return ps;
    }


    @RequestMapping(value = "/minigettodaysummary",method = RequestMethod.GET)
    @ResponseBody
    public PieceWorkSummary getTodaySummary(@RequestParam("employeeNumber")String employeeNumber){
        List<PieceWork> pieceWorkList = pieceWorkService.getTodaySummary(employeeNumber);
        int packageCount = 0;
        int pieceCount = 0;
        float sumSalary = 0;
        for (PieceWork pieceWork : pieceWorkList){
//            ProcedureInfo pi = orderProcedureService.getProcedureInfoByOrderProcedureNumber(pieceWork.getOrderName(),pieceWork.getProcedureNumber());
            Float price = 0f;
            sumSalary += pieceWork.getLayerCount()*price;
            pieceCount += pieceWork.getLayerCount();
            packageCount ++;
        }
        PieceWorkSummary ps = new PieceWorkSummary(packageCount, pieceCount, sumSalary);
//        PieceWorkSummary ps = new PieceWorkSummary(0, 0, 0);
        return ps;
    }

    @RequestMapping(value = "/minicheckpieceworknew",method = RequestMethod.POST)
    @ResponseBody
    public int miniCheckPieceworkNew(@RequestParam("pieceWorkJson") String pieceWorkJson){
        JsonParser jsonParser = new JsonParser();
        int res = 0;
        try{
            JsonObject json = (JsonObject) jsonParser.parse(pieceWorkJson);
            String employeeNumber = json.get("employeeNumber").getAsString();
            String orderName = json.get("orderName").getAsString();
            String partName = json.get("partName").getAsString();
            int bedNumber = Integer.parseInt(json.get("bedNumber").getAsString());
            int packageNumber = Integer.parseInt(json.get("packageNumber").getAsString());
            List<ProcedureInfo> procedureInfoList = dispatchService.getProcedureInfoByOrderEmp(orderName,employeeNumber);
            int sizeCount = 0;
            List<ProcedureInfo> rightProcedureInfoList = new ArrayList<>();
            List<OrderProcedure> orderProcedureList = orderProcedureService.getOrderProcedureByOrder(orderName);
            for (ProcedureInfo procedureInfo : procedureInfoList){
                for (OrderProcedure orderProcedure : orderProcedureList){
                    if(orderProcedure.getProcedureNumber().equals(procedureInfo.getProcedureNumber()) && partName.contains(orderProcedure.getScanPart())){
                        rightProcedureInfoList.add(procedureInfo);
                    }
                }
            }
            for (ProcedureInfo procedureInfo : rightProcedureInfoList) {
                Integer procedureNumber = procedureInfo.getProcedureNumber();
                List<PieceWork> pieceWorkList = pieceWorkService.getPieceWorkByOrderBedPack(orderName, bedNumber, packageNumber, procedureNumber);
                if (pieceWorkList != null && pieceWorkList.size() > 0){
                    sizeCount += 1;
                }
            }
            if (sizeCount == 0){
                res = 0;
                return res;
            }else if(sizeCount == rightProcedureInfoList.size()){
                res = 1;
                return res;
            }else{
                res = 2;
                return res;
            }
        }catch (JsonIOException e){
            e.printStackTrace();
        }catch (JsonSyntaxException e){
            e.printStackTrace();
        }
        return res;
    }

    @RequestMapping(value = "/minicheckpieceworknewram",method = RequestMethod.POST)
    @ResponseBody
    public int miniCheckPieceworkNewRam(@RequestParam("pieceWorkJson") String pieceWorkJson){
        JsonParser jsonParser = new JsonParser();
        int res = 0;
        try{
            JsonObject json = (JsonObject) jsonParser.parse(pieceWorkJson);
            String employeeNumber = json.get("employeeNumber").getAsString();
            String orderName = json.get("orderName").getAsString();
            String colorName = json.get("colorName").getAsString();
            String sizeName = json.get("sizeName").getAsString();
            String partName = json.get("partName").getAsString();
            int bedNumber = Integer.parseInt(json.get("bedNumber").getAsString());
            int packageNumber = Integer.parseInt(json.get("packageNumber").getAsString());
            List<ProcedureInfo> procedureInfoList = dispatchService.getProcedureInfoByOrderEmp(orderName,employeeNumber);
            int sizeCount = 0;
            List<ProcedureInfo> rightProcedureInfoList = new ArrayList<>();
            List<OrderProcedure> orderProcedureList = orderProcedureService.getOrderProcedureByOrder(orderName);
            for (ProcedureInfo procedureInfo : procedureInfoList){
                for (OrderProcedure orderProcedure : orderProcedureList){
                    if(orderProcedure.getProcedureNumber().equals(procedureInfo.getProcedureNumber()) && partName.contains(orderProcedure.getScanPart())){
                        rightProcedureInfoList.add(procedureInfo);
                    }
                }
            }
            for (ProcedureInfo procedureInfo : rightProcedureInfoList) {
                Integer procedureNumber = procedureInfo.getProcedureNumber();
                List<PieceWork> pieceWorkList = pieceWorkService.getPieceWorkByOrderBedPack(orderName, bedNumber, packageNumber, procedureNumber);
                List<SpecialProcedure> specialProcedureList = specialProcedureService.getSpecialProcedureByOrderProcedure(orderName, procedureNumber);
                if (pieceWorkList != null && pieceWorkList.size() > 0){
                    sizeCount += 1;
                }
                if (specialProcedureList != null && specialProcedureList.size() > 0){
                    boolean specialFlag = false;
                    for (SpecialProcedure specialProcedure : specialProcedureList){
                        if ((specialProcedure.getSizeName().equals("全部") || specialProcedure.getSizeName().equals(sizeName)) && (specialProcedure.getColorName().equals("全部") || specialProcedure.getColorName().equals(colorName))){
                            specialFlag = true;
                        }
                    }
                    if (!specialFlag){
                        return 3;
                    }
                }
            }
            if (sizeCount == 0){
                return  0;
            }else if(sizeCount == procedureInfoList.size()){
                return 1;
            }else{
                return 2;
            }
        }catch (JsonIOException e){
            e.printStackTrace();
        }catch (JsonSyntaxException e){
            e.printStackTrace();
        }
        return res;
    }

    @RequestMapping(value = "/minigetpieceworkempinfo",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> miniGetPieceWorkEmpInfo(@RequestParam("orderName")String orderName,
                                                      @RequestParam("bedNumber")Integer bedNumber,
                                                      @RequestParam("packageNumber")Integer packageNumber,
                                                      @RequestParam("tailorQcodeID")Integer tailorQcodeID){
        Map<String, Object> map = new HashMap<>();
        List<PieceWorkEmp> pieceWorkEmpList = pieceWorkService.getProcedureInfoByOrderBedPackage(orderName, bedNumber, packageNumber);
        FinishTailor finishTailor = finishTailorService.getFinishTailorByTailorQcodeID(tailorQcodeID);
        PieceWorkEmp pieceWorkInEmp = new PieceWorkEmp();
        PieceWorkEmp pieceWorkOutEmp = new PieceWorkEmp();
        if (finishTailor != null){
            if (finishTailor.getShelfEmp() != null){
                pieceWorkInEmp.setEmployeeName(finishTailor.getShelfEmp());
                pieceWorkInEmp.setEmployeeNumber("");
                pieceWorkInEmp.setOrderName(finishTailor.getOrderName());
                pieceWorkInEmp.setProcedureNumber(0);
                pieceWorkInEmp.setProcedureName("上车对接");
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                pieceWorkInEmp.setPieceTime(formatter.format(finishTailor.getShelfInTime()));
                pieceWorkEmpList.add(pieceWorkInEmp);
            }
            if (finishTailor.getShelfOutEmp() != null){
                pieceWorkOutEmp.setEmployeeName(finishTailor.getShelfEmp());
                pieceWorkOutEmp.setEmployeeNumber("");
                pieceWorkOutEmp.setOrderName(finishTailor.getOrderName());
                pieceWorkOutEmp.setProcedureNumber(0);
                pieceWorkOutEmp.setProcedureName("下车对接");
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                pieceWorkOutEmp.setPieceTime(formatter.format(finishTailor.getShelfOutTime()));
                pieceWorkEmpList.add(pieceWorkOutEmp);
            }
        }
        map.put("pieceWorkEmpList",pieceWorkEmpList);
        return map;
    }

    @RequestMapping(value = "/addpieceworktotal", method = RequestMethod.POST)
    @ResponseBody
    public int addPieceWorkTotal(@RequestParam("employeeNumber") String employeeNumber,
                                 @RequestParam("employeeName") String employeeName,
                                 @RequestParam("groupName") String groupName,
                                 @RequestParam("orderName") String orderName,
                                 @RequestParam("clothesVersionNumber") String clothesVersionNumber,
                                 @RequestParam("colorName") String colorName,
                                 @RequestParam("sizeName") String sizeName,
                                 @RequestParam("layerCount") Integer layerCount,
                                 @RequestParam("procedureNumber") String procedureNumber,
                                 @RequestParam("procedureName") String procedureName,
                                 @RequestParam("userName") String userName,
                                 @RequestParam("remark") String remark,
                                 @RequestParam("createTime")String createTime){
        if ("".equals(colorName)){
            colorName = null;
        }
        if ("".equals(sizeName)){
            sizeName = null;
        }
        List<String> fixedSalaryList = fixedSalaryService.getAllFixedSalaryValue();
        String createMonth = createTime.substring(0, 7);
        for (String fixedValue : fixedSalaryList){
            if (createMonth.equals(fixedValue)){
                return 4;
            }
        }
        Employee employee = employeeService.getEmpByEmpNumber(employeeNumber).get(0);
        employeeName = employee.getEmployeeName();
        groupName = employee.getGroupName();
        Timestamp ts = new Timestamp(System.currentTimeMillis());
        ts = Timestamp.valueOf(createTime.concat(" 12:00:00"));
        Integer tailorCount = tailorService.getWellCountByInfo(orderName, colorName, sizeName, null,null,null, 0);
        Integer totalTailorCount = tailorService.getWellCountByInfo(orderName, null, null, null, null, null,0);
        Integer tmpProcedureNumber = Integer.parseInt(procedureNumber.trim());
        int totalCount = pieceWorkService.getPieceCountByOrderColorSize(orderName,tmpProcedureNumber, null,null);
        int pieceCount = pieceWorkService.getPieceCountByOrderColorSize(orderName,tmpProcedureNumber,colorName,sizeName);
        if (tailorCount == null){
            return 2;
        }
        if (layerCount > tailorCount){
            return 5;
        }
        if (totalCount + layerCount > totalTailorCount){
            return 7;
        }
        if (layerCount + pieceCount > tailorCount){
            return 3;
        }

        if (colorName == null){
            colorName = "0";
        }
        if (sizeName == null){
            sizeName = "0";
        }
        PieceWork pw = new PieceWork(employeeNumber, employeeName, groupName, orderName, clothesVersionNumber, colorName,sizeName,layerCount,tmpProcedureNumber,procedureName,userName,ts,remark);
        return pieceWorkService.addPieceWorkTotal(pw);
    }

    @RequestMapping(value = "/miniaddpieceworktotal", method = RequestMethod.POST)
    @ResponseBody
    public int miniAddPieceWorkTotal(@RequestParam("employeeNumber") String employeeNumber,
                                     @RequestParam("employeeName") String employeeName,
                                     @RequestParam("groupName") String groupName,
                                     @RequestParam("orderName") String orderName,
                                     @RequestParam("clothesVersionNumber") String clothesVersionNumber,
                                     @RequestParam("colorName") String colorName,
                                     @RequestParam("sizeName") String sizeName,
                                     @RequestParam("layerCount") Integer layerCount,
                                     @RequestParam("procedureNumber") String procedureNumber,
                                     @RequestParam("procedureName") String procedureName,
                                     @RequestParam("userName") String userName){
        if ("".equals(colorName) || "全部".equals(colorName) || colorName == null){
            colorName = null;
        }
        if ("".equals(sizeName) || "全部".equals(sizeName) || sizeName == null){
            sizeName = null;
        }
        Timestamp createTime = new Timestamp(new Date().getTime());
        Integer tmpProcedureNumber = Integer.parseInt(procedureNumber.trim());
        OrderProcedure orderProcedure = orderProcedureService.getOrderProcedureByOrderNameProcedureNumber(orderName, tmpProcedureNumber);
        int totalCount = pieceWorkService.getPieceCountByOrderColorSize(orderName,tmpProcedureNumber, null,null);
        int pieceCount = pieceWorkService.getPieceCountByOrderColorSize(orderName,tmpProcedureNumber,colorName,sizeName);
        if ("全部".equals(orderProcedure.getColorName()) && "全部".equals(orderProcedure.getSizeName())){
            Integer tailorCount = tailorService.getWellCountByInfo(orderName, colorName, sizeName, null,null,null, 0);
            Integer totalTailorCount = tailorService.getWellCountByInfo(orderName, null, null, null, null, null,0);
            if (tailorCount == null){
                return 2;
            }
            if (layerCount > tailorCount){
                return 5;
            }
            if (totalCount + layerCount > totalTailorCount){
                return 7;
            }
            if (layerCount + pieceCount > tailorCount){
                return 3;
            }
        }
        else {
            List<String> sizeList = Arrays.asList(orderProcedure.getSizeName().split(","));
            List<String> colorList = Arrays.asList(orderProcedure.getColorName().split(","));
            int tailorCount;
            if ("全部".equals(orderProcedure.getColorName())){
                tailorCount = tailorService.getWellCountByColorSizeList(orderName, null, sizeList);
            } else if ("全部".equals(orderProcedure.getSizeName())){
                tailorCount = tailorService.getWellCountByColorSizeList(orderName, colorList, null);
            } else {
                tailorCount = tailorService.getWellCountByColorSizeList(orderName, colorList, sizeList);
            }
            if (layerCount > tailorCount){
                return 5;
            }
            if (totalCount + layerCount > tailorCount){
                return 7;
            }
            if (layerCount + pieceCount > tailorCount){
                return 3;
            }
        }
        if (colorName == null){
            colorName = "0";
        }
        if (sizeName == null){
            sizeName = "0";
        }
        PieceWork pw = new PieceWork(employeeNumber, employeeName, groupName, orderName, clothesVersionNumber, colorName,sizeName,layerCount,tmpProcedureNumber,procedureName,userName,createTime,"手机");
        return pieceWorkService.addPieceWorkTotal(pw);
    }


    @RequestMapping(value = "/miniaddpieceworktotalmultiprocedurenumber", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> miniAddPieceWorkTotalMultiProcedureNumber(@RequestParam("employeeNumber") String employeeNumber,
                                                                         @RequestParam("employeeName") String employeeName,
                                                                         @RequestParam("groupName") String groupName,
                                                                         @RequestParam("orderName") String orderName,
                                                                         @RequestParam("clothesVersionNumber") String clothesVersionNumber,
                                                                         @RequestParam("colorName") String colorName,
                                                                         @RequestParam("sizeName") String sizeName,
                                                                         @RequestParam("layerCount") Integer layerCount,
                                                                         @RequestParam("procedureNumberJson") String procedureNumberJson,
                                                                         @RequestParam("userName") String userName){
        Map<String, Object> map = new HashMap<>();
        if ("".equals(colorName) || "全部".equals(colorName) || colorName == null){
            colorName = null;
        }
        if ("".equals(sizeName) || "全部".equals(sizeName) || sizeName == null){
            sizeName = null;
        }
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        List<Integer> procedureNumberList = gson.fromJson(procedureNumberJson,new TypeToken<List<Integer>>(){}.getType());
        StringBuilder msg = new StringBuilder();
        boolean pieceOver = false;
        List<PieceWork> pieceWorkList = new ArrayList<>();
        Timestamp createTime = new Timestamp(new Date().getTime());
        List<OrderProcedure> orderProcedureList = orderProcedureService.getOrderProcedureByOrder(orderName);
        for (Integer pn : procedureNumberList){
            OrderProcedure tmpOrderProcedure = new OrderProcedure();
            for (OrderProcedure orderProcedure : orderProcedureList){
                if (pn.equals(orderProcedure.getProcedureNumber())){
                    tmpOrderProcedure = orderProcedure;
                }
            }
            int totalCount = pieceWorkService.getPieceCountByOrderColorSize(orderName,pn, null,null);
            int pieceCount = pieceWorkService.getPieceCountByOrderColorSize(orderName,pn,colorName,sizeName);
            if ("全部".equals(tmpOrderProcedure.getColorName()) && "全部".equals(tmpOrderProcedure.getSizeName())){
                Integer tailorCount = tailorService.getWellCountByInfo(orderName, colorName, sizeName, null,null,null, 0);
                if (tailorCount == null){
                    map.put("data", 2);
                    map.put("msg", "没有裁数");
                    return map;
                }
                Integer totalTailorCount = tailorService.getWellCountByInfo(orderName, null, null, null, null, null,0);
                if (layerCount > tailorCount){
                    pieceOver = true;
                    msg.append(pn).append("超数;");
                }
                if (totalCount + layerCount > totalTailorCount){
                    pieceOver = true;
                    msg.append(pn).append("超数;");
                }
                if (layerCount + pieceCount > tailorCount){
                    pieceOver = true;
                    msg.append(pn).append("超数;");
                }
            }
            else {
                List<String> sizeList = Arrays.asList(tmpOrderProcedure.getSizeName().split(","));
                List<String> colorList = Arrays.asList(tmpOrderProcedure.getColorName().split(","));
                int tailorCount;
                if ("全部".equals(tmpOrderProcedure.getColorName())){
                    tailorCount = tailorService.getWellCountByColorSizeList(orderName, null, sizeList);
                } else if ("全部".equals(tmpOrderProcedure.getSizeName())){
                    tailorCount = tailorService.getWellCountByColorSizeList(orderName, colorList, null);
                } else {
                    tailorCount = tailorService.getWellCountByColorSizeList(orderName, colorList, sizeList);
                }
                if (layerCount > tailorCount){
                    pieceOver = true;
                    msg.append(pn).append("超数;");
                }
                if (totalCount + layerCount > tailorCount){
                    pieceOver = true;
                    msg.append(pn).append("超数;");
                }
                if (layerCount + pieceCount > tailorCount){
                    pieceOver = true;
                    msg.append(pn).append("超数;");
                }
            }
            if (colorName == null){
                colorName = "0";
            }
            if (sizeName == null){
                sizeName = "0";
            }
            String newGroupName = employeeService.getGroupNameByEmpNum(employeeNumber);
            PieceWork pw = new PieceWork(employeeNumber, employeeName, newGroupName, orderName, clothesVersionNumber, colorName, sizeName, layerCount, pn, tmpOrderProcedure.getProcedureName(), userName, createTime, "手机");
            pieceWorkList.add(pw);
        }
        if (!pieceOver){
            pieceWorkService.addPieceWorkBatchNew(pieceWorkList);
            map.put("data", 0);
            return map;
        }
        else {
            map.put("data", 5);
            map.put("msg", msg);
            return map;
        }

    }



    @RequestMapping(value = "/minigetdetailproduction", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> miniGetDetailProduction(@RequestParam("from") String from,
                                                       @RequestParam("to")String to,
                                                       @RequestParam("employeeNumber")String employeeNumber,
                                                       @RequestParam("orderName")String orderName,
                                                       @RequestParam("colorName")String colorName,
                                                       @RequestParam("sizeName")String sizeName){
        Map<String, Object> map = new HashMap<>();
        if ("".equals(orderName) || "请选择款号".equals(orderName)){
            orderName = null;
        }
        if ("".equals(colorName) || "全部".equals(colorName)){
            colorName = null;
        }
        if ("".equals(sizeName) || "全部".equals(sizeName)){
            sizeName = null;
        }
        try{
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date fromDate = sdf.parse(from);
            Date toDate = sdf.parse(to);
            List<String> orderNameList = new ArrayList<>();
            List<GeneralSalary> generalSalaryList = pieceWorkService.getMiniSalaryByOrderColorSizeEmp(fromDate, toDate, employeeNumber, orderName, colorName, sizeName);
            if (generalSalaryList != null && generalSalaryList.size() > 0){
                for (GeneralSalary generalSalary : generalSalaryList){
                    orderNameList.add(generalSalary.getOrderName());
                }
            }
            List<OrderProcedure> orderProcedureList = orderProcedureService.getOrderProcedureByOrderNames(orderNameList, 2);
            if (generalSalaryList != null && generalSalaryList.size() > 0){
                for (GeneralSalary generalSalary : generalSalaryList){
                    Float miniPrice = 0f;
                    Float miniPriceTwo = 0f;
                    for (OrderProcedure orderProcedure : orderProcedureList){
                        if (generalSalary.getOrderName().equals(orderProcedure.getOrderName()) && generalSalary.getProcedureNumber().equals(orderProcedure.getProcedureNumber())){
                            miniPrice = (float) (orderProcedure.getPiecePrice() + orderProcedure.getPiecePriceTwo());
                            if (orderProcedure.getProcedureSection().equals("车缝")){
                                miniPriceTwo = miniPrice*orderProcedure.getSubsidy();
                            }
                        }
                    }
                    generalSalary.setSalaryType("计件");
                    generalSalary.setPrice(miniPrice);
                    generalSalary.setSalary(miniPrice*generalSalary.getPieceCount());
                    generalSalary.setPriceTwo(miniPriceTwo);
                    generalSalary.setSalaryTwo(miniPriceTwo*generalSalary.getPieceCount());
                }
                map.put("miniDetailQueryList", generalSalaryList);
            }
            return map;
        }catch (ParseException e){
            e.printStackTrace();
        }
        return map;
    }


    @RequestMapping(value = "/minigetdetailproductionbyinfo", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> miniGetDetailProductionByInfo(@RequestParam("from") String from,
                                                             @RequestParam("to")String to,
                                                             @RequestParam("employeeNumber")String employeeNumber,
                                                             @RequestParam("orderName")String orderName,
                                                             @RequestParam("colorName")String colorName,
                                                             @RequestParam("sizeName")String sizeName,
                                                             @RequestParam("operateType")String operateType){
        Map<String, Object> map = new HashMap<>();
        if ("".equals(orderName) || "请选择款号".equals(orderName)){
            orderName = null;
        }
        if ("".equals(colorName) || "全部".equals(colorName)){
            colorName = null;
        }
        if ("".equals(sizeName) || "全部".equals(sizeName)){
            sizeName = null;
        }
        List<String> orderNameList = new ArrayList<>();
        List<GeneralSalary> generalSalaryList = new ArrayList<>();
        if (operateType.equals("按款号-工序汇总")){
            generalSalaryList = pieceWorkService.getDetailProductionSummaryByInfo(from, to, employeeNumber, orderName, colorName, sizeName);
        } else if (operateType.equals("按款号-工序-日期汇总")){
            generalSalaryList = pieceWorkService.getDetailProductionByInfo(from, to, employeeNumber, orderName, colorName, sizeName);
        } else {
            generalSalaryList = pieceWorkService.getDetailProductionDetailByInfo(from, to, employeeNumber, orderName, colorName, sizeName);
        }
        if (generalSalaryList != null && generalSalaryList.size() > 0){
            for (GeneralSalary generalSalary : generalSalaryList){
                orderNameList.add(generalSalary.getOrderName());
            }
        }
        map.put("miniDetailQueryList", generalSalaryList);
        List<OrderProcedure> orderProcedureList = orderProcedureService.getOrderProcedureByOrderNames(orderNameList, 2);
        map.put("orderProcedureList", orderProcedureList);
        return map;
    }


    @RequestMapping(value = "/getemployeedetailproductionbyinfo", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getEmployeeDetailProductionByInfo(@RequestParam("from") String from,
                                                                 @RequestParam("to")String to,
                                                                 @RequestParam("employeeNumber")String employeeNumber){
        Map<String, Object> map = new HashMap<>();
        List<GeneralSalary> generalSalaryList = pieceWorkService.getDetailProductionSummaryByInfo(from, to, employeeNumber, null, null, null);
        map.put("salaryList", generalSalaryList);
        return map;
    }


    @RequestMapping(value = "/getallmanualinput",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getAllManualInput(){
        Map<String,Object> map = new LinkedHashMap<>();
        List<ManualInput> manualInputList = pieceWorkService.getAllManualInput();
        for (ManualInput manualInput : manualInputList){
            String orderName = manualInput.getOrderName();
            Integer procedureNumber = manualInput.getProcedureNumber();


            String procedureName = "通用";
            ProcedureInfo pi = orderProcedureService.getProcedureInfoByOrderProcedureNumber(orderName, procedureNumber);
            if (pi != null){
                if (pi.getProcedureName() != null){
                    procedureName = pi.getProcedureName();
                }
            }
            manualInput.setClothesVersionNumber(orderClothesService.getVersionNumberByOrderName(orderName));
            manualInput.setProcedureName(procedureName);
        }
        Collections.sort(manualInputList);
        map.put("manualInputList",manualInputList);
        return map;
    }

    @RequestMapping(value = "/unload", method = RequestMethod.POST)
    @ResponseBody
    public int inStore(@RequestParam("unloadJson")String unloadJson){
        JsonParser jsonParser = new JsonParser();
        int result = 0;
        try{
            JsonObject json = (JsonObject) jsonParser.parse(unloadJson);
            JsonArray jsonTailorQcode = json.get("tailorQcode").getAsJsonArray();
            Iterator iterator = jsonTailorQcode.iterator();
            List<Integer> tailorQcodeIDList = new ArrayList<>();
            while(iterator.hasNext()){
                JsonPrimitive next = (JsonPrimitive) iterator.next();
                tailorQcodeIDList.add(Integer.parseInt(next.toString().replace("\"","")));
            }
            List<Tailor> tailorList = tailorService.getTailorsByTailorQcodeIDs(tailorQcodeIDList, 0);
            for (Tailor tailor : tailorList){
                PieceWork pw = new PieceWork("5555555",tailor.getOrderName(),tailor.getBedNumber(),tailor.getColorName(),tailor.getSizeName(),tailor.getPackageNumber(),tailor.getLayerCount(),556,0);
                result += pieceWorkService.addPieceWork(pw);
            }
        }catch (JsonIOException e){
            e.printStackTrace();
        }catch (JsonSyntaxException e){
            e.printStackTrace();
        }
        return result;
    }

    @RequestMapping(value = "/unloadreport", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getUnloadByOrderTime(@RequestParam("orderName") String orderName,
                                                    @RequestParam("from") String from,
                                                    @RequestParam("to") String to){
        if("".equals(from)){
            from = "1970-01-01 00:00:00";
        }
        if("".equals(to)){
            to = "2050-01-01 00:00:00";
        }
        Map<String, Object> map = new LinkedHashMap<>();
        Map<String, Object> colorMap = new LinkedHashMap<>();
        try{
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date fromDate = sdf.parse(from);
            Date toDate = sdf.parse(to);
            String initString = "1970-01-01 00:00:00";
            String finalString = "2050-01-01 00:00:00";
            Date initDate = sdf.parse(initString);
            Date finalDate = sdf.parse(finalString);
            List<ProductionProgressDetail> productionProgressDetailList1 = pieceWorkService.getUnloadByOrderTime(orderName, fromDate, toDate);
            List<ProductionProgressDetail> productionProgressDetailList2 = pieceWorkService.getUnloadByOrderTime(orderName, initDate, finalDate);
            List<CutQueryLeak> orderInfoList = orderClothesService.getOrderInfoByName(orderName);
            List<CutQueryLeak> tailorInfoList = tailorService.getTailorInfoByOrderPartType(orderName, null, 0, null, null);
            List<String> colorList = new ArrayList<>();
            List<String> sizeList = new ArrayList<>();
            for(CutQueryLeak cutQueryLeak : orderInfoList){
                if (!colorList.contains(cutQueryLeak.getColorName())){
                    colorList.add(cutQueryLeak.getColorName());
                }
                if (!sizeList.contains(cutQueryLeak.getSizeName())){
                    sizeList.add(cutQueryLeak.getSizeName());
                }
            }
            for(CutQueryLeak cutQueryLeak : tailorInfoList){
                if (!colorList.contains(cutQueryLeak.getColorName())){
                    colorList.add(cutQueryLeak.getColorName());
                }
                if (!sizeList.contains(cutQueryLeak.getSizeName())){
                    sizeList.add(cutQueryLeak.getSizeName());
                }
            }
            for (ProductionProgressDetail ppd : productionProgressDetailList1){
                if (!colorList.contains(ppd.getColorName())){
                    colorList.add(ppd.getColorName());
                }
                if (!sizeList.contains(ppd.getSizeName())){
                    sizeList.add(ppd.getSizeName());
                }
            }
            for (ProductionProgressDetail ppd : productionProgressDetailList2){
                if (!colorList.contains(ppd.getColorName())){
                    colorList.add(ppd.getColorName());
                }
                if (!sizeList.contains(ppd.getSizeName())){
                    sizeList.add(ppd.getSizeName());
                }
            }
            List<String> desiredOrder = Arrays.asList("XXXXXS","5XS","XXXXS","4XS","XXXS","3XS","XXS","2XS","XS","XS/25","xs","S","S/27","M","M/29","L","L/31","XL",
                    "XL/33","XXL","2XL","XXL/36","XXXL","3XL","XXXL/38","4XL","XXXXL","5XL","XXXXXL","6XL","XXXXXXL","80","090","90","100","110","110/27",
                    "120","120/29","130","130/31","140","140/33","150","150/36","155","160","165","170","175","180","185","190");
            Comparator<String> sizeOrder = Comparator.comparingInt(desiredOrder::indexOf);
            sizeList.sort(sizeOrder);
            for (String color : colorList){
                Map<String, Object> sizeMap = new LinkedHashMap<>();
                for (String size : sizeList){
                    ProductionDetailLeak pdl = new ProductionDetailLeak();
                    for (ProductionProgressDetail ppd : productionProgressDetailList1){
                        if (color.equals(ppd.getColorName()) && size.equals(ppd.getSizeName())){
                            pdl.setProductionCount(ppd.getProductionCount());
                        }
                    }
                    for (ProductionProgressDetail ppd : productionProgressDetailList2){
                        if (color.equals(ppd.getColorName()) && size.equals(ppd.getSizeName())){
                            pdl.setFinishCount(ppd.getProductionCount());
                        }
                    }
                    for (CutQueryLeak cutQueryLeak : orderInfoList){
                        if (color.equals(cutQueryLeak.getColorName()) && size.equals(cutQueryLeak.getSizeName())){
                            pdl.setOrderCount(cutQueryLeak.getOrderCount());
                        }
                    }
                    for (CutQueryLeak cutQueryLeak : tailorInfoList){
                        if (color.equals(cutQueryLeak.getColorName()) && size.equals(cutQueryLeak.getSizeName())){
                            pdl.setCutCount(cutQueryLeak.getLayerCount());
                        }
                    }
                    pdl.setLeakCount(pdl.getProductionCount()-pdl.getCutCount());
                    sizeMap.put(size,pdl);
                }
                colorMap.put(color,sizeMap);
            }
            map.put("color",colorMap);
            return map;
        }catch (ParseException e){
            e.printStackTrace();
        }
        return map;
    }

    @RequestMapping(value = "/updatemanualinput",method = RequestMethod.POST)
    @ResponseBody
    public int updateManualInput(@RequestParam("manualInput")String manualInput){
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        PieceWork pieceWork = gson.fromJson(manualInput, PieceWork.class);
        SimpleDateFormat sformat = new SimpleDateFormat("yyyy-MM-dd");//日期格式
        String createTime = sformat.format(pieceWork.getCreateTime());
        List<String> fixedSalaryList = fixedSalaryService.getAllFixedSalaryValue();
        PieceWork pieceWork1 = pieceWorkService.getPieceWorkByID(pieceWork.getPieceWorkID());
        String createTime1 = sformat.format(pieceWork1.getCreateTime());
        String createMonth = createTime.substring(0, 7);
        String createMonth1 = createTime1.substring(0, 7);
        for (String fixedValue : fixedSalaryList){
            if (createMonth.equals(fixedValue) || createMonth1.equals(fixedValue)){
                return 4;
            }
        }
        String colorName = pieceWork.getColorName();
        String sizeName = pieceWork.getSizeName();
        if (pieceWork.getColorName().equals("0")){
            colorName = null;
        }
        if (pieceWork.getSizeName().equals("0")){
            sizeName = null;
        }
        Timestamp ts = new Timestamp(System.currentTimeMillis());
        ts = Timestamp.valueOf(createTime.concat(" 12:00:00"));
        int layerCount = pieceWork.getLayerCount();

        int layerCount1 = pieceWork1.getLayerCount();
        Integer tailorCount = tailorService.getWellCountByInfo(pieceWork.getOrderName(), colorName, sizeName, null, null,null,0);
        Integer totalTailorCount = tailorService.getWellCountByInfo(pieceWork.getOrderName(), null, null, null, null, null,0);
        Integer tmpProcedureNumber = pieceWork.getProcedureNumber();
        int totalCount = pieceWorkService.getPieceCountByOrderColorSize(pieceWork.getOrderName(),tmpProcedureNumber, null,null);
        int pieceCount = pieceWorkService.getPieceCountByOrderColorSize(pieceWork.getOrderName(),tmpProcedureNumber, colorName, sizeName);
        if (tailorCount == null){
            return 2;
        }
        if (totalCount + layerCount - layerCount1 > totalTailorCount){
            return 7;
        }
        if (layerCount > tailorCount){
            return 5;
        }
        if (layerCount + pieceCount - layerCount1 > tailorCount){
            return 3;
        }
        if (colorName == null){
            colorName = "0";
        }
        if (sizeName == null){
            sizeName = "0";
        }
        pieceWork.setCreateTime(ts);
        return pieceWorkService.updateManualInput(pieceWork);

    }


    @RequestMapping(value = "/gettodaymanualinput",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getTodayManualInput(){
        Map<String,Object> map = new LinkedHashMap<>();
        List<ManualInput> manualInputList = pieceWorkService.getTodayManualInput();
        Collections.sort(manualInputList);
        map.put("todayManualInputList",manualInputList);
        return map;
    }

    @RequestMapping(value = "/getonemonthmanualinput",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getOneMonthManualInput(){
        Map<String,Object> map = new LinkedHashMap<>();
        List<ManualInput> manualInputList = pieceWorkService.getOneMonthManualInput();
        Collections.sort(manualInputList);
        map.put("oneMonthManualInputList",manualInputList);
        return map;
    }

    @RequestMapping(value = "/getthreemonthsmanualinput",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getThreeMonthsManualInput(){
        Map<String,Object> map = new LinkedHashMap<>();
        List<ManualInput> manualInputList = pieceWorkService.getThreeMonthsManualInput();
        Collections.sort(manualInputList);
        map.put("threeMonthsManualInputList",manualInputList);
        return map;
    }

    @RequestMapping(value = "/searchpieceworkmanagement",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> searchPieceWorkManagement(@RequestParam("orderName")String orderName,
                                                        @RequestParam(value = "colorName", required = false)String colorName,
                                                        @RequestParam(value = "sizeName", required = false)String sizeName,
                                                        @RequestParam(value = "bedNumber", required = false)Integer bedNumber,
                                                        @RequestParam(value = "packageNumber", required = false)Integer packageNumber,
                                                        @RequestParam("procedureNumber")Integer procedureNumber){
        Map<String,Object> map = new LinkedHashMap<>();
        List<PieceWorkResult> pieceWorkList = pieceWorkService.searchPieceWorkManagement(orderName, colorName, sizeName, bedNumber, packageNumber, procedureNumber, null, null);
        for (PieceWorkResult pieceWorkResult : pieceWorkList){
            if (pieceWorkResult.getUserName() != null){
                pieceWorkResult.setPieceType("手输");
            } else if (pieceWorkResult.getNid() != null){
                pieceWorkResult.setPieceType("吊挂");
            } else {
                pieceWorkResult.setPieceType("手机");
            }
        }
        map.put("data",pieceWorkList);
        map.put("code",0);
        map.put("msg",null);
        return map;
    }

    @RequestMapping(value = "/deletepieceworkbatch",method = RequestMethod.POST)
    @ResponseBody
    public int deletePieceWorkBatch(@RequestParam("pieceWorkIDList")List<Integer> pieceWorkIDList,
                                    @RequestParam("userName")String userName){
        log.warn("删除计件---"+"计件ID："+pieceWorkIDList+"---操作用户："+userName);
        List<String> fixedSalaryList = fixedSalaryService.getAllFixedSalaryValue();
        String createTime = keyDataDeleteService.getEarlyCreateTimeOfDeleteData(pieceWorkIDList);
        String createMonth = createTime.substring(0, 7);
        for (String fixedValue : fixedSalaryList){
            if (createMonth.equals(fixedValue)){
                return 6;
            }
        }
        List<PieceWorkDelete> pieceWorkDeleteList = keyDataDeleteService.getPieceWorkListByDeleteId(pieceWorkIDList);
        for (PieceWorkDelete pieceWorkDelete : pieceWorkDeleteList){
            pieceWorkDelete.setOperateUser(userName);
        }
        keyDataDeleteService.addPieceWorkDeleteBatch(pieceWorkDeleteList);
        return pieceWorkService.deletePieceWorkBatch(pieceWorkIDList);
    }


    @RequestMapping(value = "/searchpieceworkleak",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> searchPieceworkLeak(Model model,@RequestParam("orderName")String orderName,
                                                  @RequestParam(value = "colorName", required = false)String colorName,
                                                  @RequestParam(value = "sizeName", required = false)String sizeName,
                                                  @RequestParam(value = "bedNumber", required = false)Integer bedNumber,
                                                  @RequestParam(value = "packageNumber", required = false)Integer packageNumber,
                                                  @RequestParam("procedureNumber")Integer procedureNumber){
        Map<String, Object> map = new LinkedHashMap<>();
        List<PieceWorkResult> pieceWorkList = pieceWorkService.searchPieceWorkManagement(orderName, colorName, sizeName, bedNumber, packageNumber, procedureNumber, null, null);
        List<CutLocation> cutLocationList1 = tailorService.getPackageInfo(orderName, bedNumber, colorName, sizeName, packageNumber);
        List<CutLocation> cutLocationList2 = cutInStoreService.getCutInfo(orderName, bedNumber, colorName, sizeName, packageNumber);
        List<CutLocation> cutLocationList3 = embInStoreService.getEmbInfo(orderName, bedNumber, colorName, sizeName, packageNumber);
        List<CutLocation> cutLocationList4 = embOutStoreService.getEmbOutInfo(orderName, bedNumber, colorName, sizeName, packageNumber);
        for (CutLocation cutLocation1 : cutLocationList1){
            for (CutLocation cutLocation2 : cutLocationList2){
                if (cutLocation1.getBedNumber().equals(cutLocation2.getBedNumber()) && cutLocation1.getPackageNumber().equals(cutLocation2.getPackageNumber())){
                    cutLocation1.setCutLocation(cutLocation2.getCutLocation());
                }
            }
            for (CutLocation cutLocation3 : cutLocationList3){
                if (cutLocation1.getBedNumber().equals(cutLocation3.getBedNumber()) && cutLocation1.getPackageNumber().equals(cutLocation3.getPackageNumber())){
                    cutLocation1.setEmbLocation(cutLocation3.getEmbLocation());
                }

            }
            for (CutLocation cutLocation4 : cutLocationList4){
                if (cutLocation1.getBedNumber().equals(cutLocation4.getBedNumber()) && cutLocation1.getPackageNumber().equals(cutLocation4.getPackageNumber())){
                    cutLocation1.setWorkshop(cutLocation4.getWorkshop());
                }
            }
            for (PieceWorkResult pieceWorkResult : pieceWorkList){
                if (cutLocation1.getBedNumber().equals(pieceWorkResult.getBedNumber()) && cutLocation1.getPackageNumber().equals(pieceWorkResult.getPackageNumber())){
                    cutLocation1.setPieceWork(pieceWorkResult.getEmployeeName());
                }
            }
        }
        map.put("cutLocationList", cutLocationList1);
        return map;
    }



    @RequestMapping(value = "/miniaddpieceworkmultitask",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> miniCheckPieceworkNewRam(@RequestParam("orderName") String orderName,
                                                        @RequestParam("clothesVersionNumber") String clothesVersionNumber,
                                                        @RequestParam("groupName") String groupName,
                                                        @RequestParam("employeeName") String employeeName,
                                                        @RequestParam("employeeNumber") String employeeNumber,
                                                        @RequestParam("colorName") String colorName,
                                                        @RequestParam("sizeName") String sizeName,
                                                        @RequestParam("bedNumber") Integer bedNumber,
                                                        @RequestParam("packageNumber") Integer packageNumber,
                                                        @RequestParam("layerCount") Integer layerCount,
                                                        @RequestParam("partName") String partName,
                                                        @RequestParam("tailorQcodeID") Integer tailorQcodeID,
                                                        @RequestParam("procedureInfo") String procedureInfo){
        Map<String, Object> map = new LinkedHashMap<>();
        Integer procedureNumber = Integer.parseInt(procedureInfo.split("-")[0]);
        String procedureName = procedureInfo.split("-")[1];
        ProcedureInfo pi = orderProcedureService.getProcedureInfoByOrderProcedureNumber(orderName, procedureNumber);
        if (!partName.contains(pi.getScanPart())){
            map.put("error", "扫描部位不正确");
            List<PieceWorkEmp> pieceWorkEmpList = pieceWorkService.getProcedureInfoByOrderBedPackage(orderName, bedNumber, packageNumber);
            map.put("pieceWorkEmpList",pieceWorkEmpList);
            return map;
        }
        List<PieceWork> pieceWorkList = pieceWorkService.getPieceWorkByOrderBedPack(orderName, bedNumber, packageNumber, procedureNumber);
        if (pieceWorkList != null && !pieceWorkList.isEmpty()){
            map.put("error", "重复计件");
            List<PieceWorkEmp> pieceWorkEmpList = pieceWorkService.getProcedureInfoByOrderBedPackage(orderName, bedNumber, packageNumber);
            map.put("pieceWorkEmpList",pieceWorkEmpList);
            return map;
        }
        List<PieceWork> pieceWorkList1 = new ArrayList<>();
        PieceWork pieceWork = new PieceWork(employeeNumber, employeeName, groupName, orderName, clothesVersionNumber, bedNumber, colorName, sizeName, packageNumber, layerCount, procedureNumber, procedureName, tailorQcodeID);
        pieceWorkList1.add(pieceWork);
        int res = pieceWorkService.addPieceWorkBatchNew(pieceWorkList1);
        if (res == 0){
            map.put("success", "计件成功");
            List<PieceWorkEmp> pieceWorkEmpList = pieceWorkService.getProcedureInfoByOrderBedPackage(orderName, bedNumber, packageNumber);
            map.put("pieceWorkEmpList",pieceWorkEmpList);
            return map;
        } else {
            map.put("fail", "计件失败");
            List<PieceWorkEmp> pieceWorkEmpList = pieceWorkService.getProcedureInfoByOrderBedPackage(orderName, bedNumber, packageNumber);
            map.put("pieceWorkEmpList",pieceWorkEmpList);
            return map;
        }
    }


    @RequestMapping(value = "/miniaddpieceworkbatchupdate",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> miniAddPieceWorkBatchNewRam(@RequestParam("orderName") String orderName,
                                                           @RequestParam("clothesVersionNumber") String clothesVersionNumber,
                                                           @RequestParam("groupName") String groupName,
                                                           @RequestParam("employeeName") String employeeName,
                                                           @RequestParam("employeeNumber") String employeeNumber,
                                                           @RequestParam("colorName") String colorName,
                                                           @RequestParam("sizeName") String sizeName,
                                                           @RequestParam("bedNumber") Integer bedNumber,
                                                           @RequestParam("packageNumber") Integer packageNumber,
                                                           @RequestParam("layerCount") Integer layerCount,
                                                           @RequestParam("partName") String partName,
                                                           @RequestParam("tailorQcodeID") Integer tailorQcodeID){
        Map<String, Object> map = new LinkedHashMap<>();
        List<ProcedureInfo> procedureInfoList = dispatchService.getProcedureInfoByOrderEmp(orderName,employeeNumber);
        List<PieceWork> pieceWorkList = new ArrayList<>();
        if (procedureInfoList != null && !procedureInfoList.isEmpty()){
            String pieceInfo = "";
            for (ProcedureInfo procedureInfo : procedureInfoList){
                ProcedureInfo procedureInfo1 = orderProcedureService.getProcedureInfoByOrderProcedureNumber(orderName, procedureInfo.getProcedureNumber());
                List<PieceWork> finishPieceWorkList = pieceWorkService.getPieceWorkByOrderBedPack(orderName, bedNumber, packageNumber, procedureInfo.getProcedureNumber());
                boolean colorSpecial = false;
                boolean sizeSpecial = false;
                if (finishPieceWorkList == null || finishPieceWorkList.isEmpty()){
                    if (procedureInfo1 == null){
                        pieceInfo += procedureInfo.getProcedureNumber() + "-" + procedureInfo.getProcedureName() + "工序被删除,请联系IE\n";
                    } else {
                        if (partName.contains(procedureInfo1.getScanPart())){
                            int totalCount = pieceWorkService.getPieceCountByOrderColorSize(orderName,procedureInfo.getProcedureNumber(), null,null);
                            int pieceCount = pieceWorkService.getPieceCountByOrderColorSize(orderName,procedureInfo.getProcedureNumber(),colorName,sizeName);
                            int tailorCount = tailorService.getWellCountByInfo(orderName, colorName, sizeName, null,null,null, 0);
                            int totalTailorCount = tailorService.getWellCountByInfo(orderName, null, null, null,null, null, 0);
//                            if (((totalCount + layerCount <= totalTailorCount) && (pieceCount + layerCount <= tailorCount)) || (!partName.contains("主身") && !partName.equals("成品") && !partName.equals("包装") && !partName.equals("返工"))){
                            if ((totalCount + layerCount <= totalTailorCount) && (pieceCount + layerCount <= tailorCount)){
                                if (procedureInfo1.getColorName().equals("全部")){
                                    colorSpecial = true;
                                } else {
                                    String strColor = procedureInfo1.getColorName();
                                    List<String> specialColorList = Arrays.asList(strColor.split(","));
                                    for (String color : specialColorList){
                                        if (color.equals(colorName)){
                                            colorSpecial = true;
                                        }
                                    }
                                }
                                if (procedureInfo1.getSizeName().equals("全部")){
                                    sizeSpecial = true;
                                } else {
                                    String strSize = procedureInfo1.getSizeName();
                                    List<String> specialSizeList = Arrays.asList(strSize.split(","));
                                    for (String size : specialSizeList){
                                        if (size.equals(sizeName)){
                                            sizeSpecial = true;
                                        }
                                    }
                                }
                                if (colorSpecial && sizeSpecial){
                                    PieceWork pieceWork = new PieceWork(employeeNumber,employeeName,groupName,orderName,clothesVersionNumber,bedNumber,colorName,sizeName,packageNumber,layerCount,procedureInfo.getProcedureNumber(),procedureInfo.getProcedureName(),tailorQcodeID);
                                    pieceWorkList.add(pieceWork);
                                    pieceInfo += procedureInfo.getProcedureNumber() + "-" + procedureInfo.getProcedureName() + "计件成功\n";
                                } else {
                                    pieceInfo += procedureInfo.getProcedureNumber() + "-" + procedureInfo.getProcedureName() + "该颜色/尺码不用计件\n";
                                }
                            } else {
                                pieceInfo += procedureInfo.getProcedureNumber() + "-" + procedureInfo.getProcedureName() + "爆数\n";
                            }

                        }else {
                            pieceInfo += procedureInfo.getProcedureNumber() + "-" + procedureInfo.getProcedureName() + "部位不对应\n";
                        }
                    }
                } else {
                    pieceInfo += procedureInfo.getProcedureNumber() + "-" + procedureInfo.getProcedureName() + "重复计件\n";
                }
            }
            if (pieceWorkList != null && pieceWorkList.size() > 0){
                int res = pieceWorkService.addPieceWorkBatchNew(pieceWorkList);
                if (res == 0){
                    map.put("success", pieceInfo);
                    List<PieceWorkEmp> pieceWorkEmpList = pieceWorkService.getProcedureInfoByOrderBedPackage(orderName, bedNumber, packageNumber);
                    map.put("pieceWorkEmpList",pieceWorkEmpList);
                    map.put("dispatchProcedureList",procedureInfoList);
                    return map;
                } else {
                    map.put("fail", "计件失败");
                    List<PieceWorkEmp> pieceWorkEmpList = pieceWorkService.getProcedureInfoByOrderBedPackage(orderName, bedNumber, packageNumber);
                    map.put("pieceWorkEmpList",pieceWorkEmpList);
                    map.put("dispatchProcedureList",procedureInfoList);
                    return map;
                }
            } else {
                List<PieceWorkEmp> pieceWorkEmpList = pieceWorkService.getProcedureInfoByOrderBedPackage(orderName, bedNumber, packageNumber);
                map.put("dispatchProcedureList",procedureInfoList);
                map.put("pieceWorkEmpList",pieceWorkEmpList);
                map.put("error", pieceInfo);
                return map;
            }
        } else {
            List<PieceWorkEmp> pieceWorkEmpList = pieceWorkService.getProcedureInfoByOrderBedPackage(orderName, bedNumber, packageNumber);
            map.put("pieceWorkEmpList",pieceWorkEmpList);
            map.put("procedureEmpty", "无派工信息");
            return map;
        }
    }

    @RequestMapping(value = "/minigetpieceworkdetailofeachline", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> miniGetPieceWorkDetailOfEachLine(@RequestParam("pieceDate") String pieceDate,
                                                                @RequestParam("employeeNumber")String employeeNumber,
                                                                @RequestParam("orderName")String orderName,
                                                                @RequestParam("procedureNumber")Integer procedureNumber,
                                                                @RequestParam("colorName")String colorName,
                                                                @RequestParam("sizeName")String sizeName){
        Map<String, Object> map = new HashMap<>();
        if ("".equals(colorName) || "全部".equals(colorName)){
            colorName = null;
        }
        if ("".equals(sizeName) || "全部".equals(sizeName)){
            sizeName = null;
        }
        try{
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date fromDate = sdf.parse(pieceDate);
            int layerSum = 0;
            List<GeneralSalary> generalSalaryList = pieceWorkService.getDetailProductionOfEachLine(fromDate, employeeNumber, orderName, procedureNumber, colorName, sizeName);
            if (generalSalaryList != null && generalSalaryList.size() > 0){
                for (GeneralSalary generalSalary : generalSalaryList){
                    layerSum += generalSalary.getPieceCount();
                }
                map.put("eachLineDetailList", generalSalaryList);
                map.put("layerSum", layerSum);
            }
            return map;
        }catch (ParseException e){
            e.printStackTrace();
        }
        return map;
    }


    @RequestMapping(value = "/updateprocedurenamebyorderprocedure", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> getProcedureNameByOrderProcedure(@RequestParam("orderName")String orderName,
                                                                @RequestParam("procedureName")String procedureName,
                                                                @RequestParam("procedureNumber")Integer procedureNumber) {
        Map<String, Object> map = new HashMap<>();
        int res = pieceWorkService.updateProcedureNameByOrderProcedure(orderName, procedureNumber, procedureName);
        map.put("result", res);
        return map;
    }


    @RequestMapping(value = "/miniaddpieceworkonce", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> miniAddPieceWorkOnce(@RequestParam("orderName")String orderName,
                                                    @RequestParam("procedureNumberJson")String procedureNumberJson,
                                                    @RequestParam("employeeNumber")String employeeNumber,
                                                    @RequestParam("employeeName")String employeeName,
                                                    @RequestParam("colorName")String colorName,
                                                    @RequestParam("sizeName")String sizeName,
                                                    @RequestParam("groupName")String groupName) {
        Map<String, Object> map = new HashMap<>();
//        if (colorName.equals("") || colorName.equals("全部")){
//            colorName = null;
//        }
//        if (sizeName.equals("") || sizeName.equals("全部")){
//            sizeName = null;
//        }
//        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
//        List<Integer> procedureNumberList = gson.fromJson(procedureNumberJson,new TypeToken<List<Integer>>(){}.getType());
//        //检测有没有计过件   1.代表已经开始计件  2.代表没有开始裁  3. 代表计件失败
//        boolean pieceFlag = false;
//        boolean tailorFlag = false;
//        StringBuilder msg = new StringBuilder();
//        for (Integer pn : procedureNumberList){
//            List<PieceWork> pieceWorkList = pieceWorkService.getPieceWorkByOrderNameProcedureNumber(orderName, pn, colorName, sizeName);
//            if (pieceWorkList != null && !pieceWorkList.isEmpty()){
//                pieceFlag = true;
//                msg.append(pn).append("已计件");
//            }
//        }
//        if (pieceFlag){
//            map.put("data", 1);
//            map.put("msg", msg);
//            return map;
//        }
//        List<OrderProcedure> orderProcedureList = orderProcedureService.getOrderProcedureByOrder(orderName);
//        List<PieceWork> pieceWorkListNew = new ArrayList<>();
//        for (Integer pn : procedureNumberList){
//            String procedureName = "";
//            String scanPart = "主身";
//            for (OrderProcedure orderProcedure : orderProcedureList){
//                if (pn.equals(orderProcedure.getProcedureNumber())){
//                    procedureName = orderProcedure.getProcedureName();
//                    scanPart = orderProcedure.getScanPart();
//                }
//            }
//            if (scanPart.equals("主身")){
//                List<Tailor> tailorList = tailorService.getTailorByOrderPartNameColorSize(orderName, 0, null, colorName, sizeName,0);
//                if (tailorList != null && !tailorList.isEmpty()){
//                    for (Tailor tailor : tailorList){
//                        PieceWork pieceWork = new PieceWork(employeeNumber, employeeName, groupName, orderName, tailor.getClothesVersionNumber(), tailor.getBedNumber(), tailor.getColorName(), tailor.getSizeName(), tailor.getPackageNumber(), tailor.getLayerCount(), pn, procedureName, 0);
//                        pieceWorkListNew.add(pieceWork);
//                    }
//                } else {
//                    tailorFlag = true;
//                }
//            }
//            else if (scanPart.equals("成品")){
//                List<FinishTailor> finishTailorList = finishTailorService.getFinishTailorByOrderPartNameColorSize(orderName, scanPart, colorName, sizeName);
//                if (finishTailorList != null && !finishTailorList.isEmpty()){
//                    for (FinishTailor tailor : finishTailorList){
//                        PieceWork pieceWork = new PieceWork(employeeNumber, employeeName, groupName, orderName, tailor.getClothesVersionNumber(), tailor.getBedNumber(), tailor.getColorName(), tailor.getSizeName(), tailor.getPackageNumber(), tailor.getLayerCount(), pn, procedureName, 0);
//                        pieceWorkListNew.add(pieceWork);
//                    }
//                } else {
//                    tailorFlag = true;
//                }
//            }
//            else {
//                List<Tailor> tailorList = tailorService.getTailorByOrderPartNameColorSize(orderName, 1, scanPart, colorName, sizeName,0);
//                if (tailorList != null && !tailorList.isEmpty()){
//                    for (Tailor tailor : tailorList){
//                        PieceWork pieceWork = new PieceWork(employeeNumber, employeeName, groupName, orderName, tailor.getClothesVersionNumber(), tailor.getBedNumber(), tailor.getColorName(), tailor.getSizeName(), tailor.getPackageNumber(), tailor.getLayerCount(), pn, procedureName, 0);
//                        pieceWorkListNew.add(pieceWork);
//                    }
//                } else {
//                    tailorFlag = true;
//                }
//            }
//        }
//        if (tailorFlag){
//            map.put("data", 2);
//            msg.append("没有裁剪记录");
//            return map;
//        }
//        pieceWorkService.addPieceWorkBatchNew(pieceWorkListNew);
//        map.put("data", 0);
//        map.put("msg", "计件成功");
//        return map;

        map.put("data", 10);
        return map;
    }

    @RequestMapping(value = "/getmanualinputinfobyorderprocedure", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getManualInputInfoByOrderProcedure(@RequestParam("orderName")String orderName,
                                                                  @RequestParam("procedureNumber")Integer procedureNumber) {
        Map<String, Object> map = new HashMap<>();
        ProcedureInfo procedureInfo = orderProcedureService.getProcedureInfoByOrderProcedureNumber(orderName, procedureNumber);
        List<String> sizeList = Arrays.asList(procedureInfo.getSizeName().split(","));
        List<String> colorList = Arrays.asList(procedureInfo.getColorName().split(","));
        map.put("colorNameOption", procedureInfo.getColorName());
        map.put("sizeNameOption", procedureInfo.getSizeName());
        int wellCount = 0;
        if (procedureInfo.getColorName().equals("全部") && procedureInfo.getSizeName().equals("全部")){
            wellCount = tailorService.getWellCountByColorSizeList(orderName, null, null);
        } else if (procedureInfo.getColorName().equals("全部")){
            wellCount = tailorService.getWellCountByColorSizeList(orderName, null, sizeList);
        } else if (procedureInfo.getSizeName().equals("全部")){
            wellCount = tailorService.getWellCountByColorSizeList(orderName, colorList, null);
        } else {
            wellCount = tailorService.getWellCountByColorSizeList(orderName, colorList, sizeList);
        }
        map.put("wellCount", wellCount);
        int pieceCount = pieceWorkService.getPieceCountByOrderColorSize(orderName, procedureNumber, null, null);
        map.put("pieceCount", pieceCount);
        map.put("availableCount", wellCount - pieceCount);
        return map;
    }

    @RequestMapping(value = "/updatesalarygroupnamebyemployeenumber",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> updateSalaryGroupNameByEmployeeNumber(@RequestParam("from") String from,
                                                                     @RequestParam("to")String to,
                                                                     @RequestParam("employeeNumber")String employeeNumber,
                                                                     @RequestParam("groupName")String groupName) throws Exception{
        Map<String, Object> map = new LinkedHashMap<>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date fromDate = sdf.parse(from);
        Date toDate = sdf.parse(to);
        int res = pieceWorkService.updateGroupNameByEmployee(fromDate, toDate, employeeNumber, groupName);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/minisearchgrouppieceworksummary", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> miniAddPieceWorkOnce(@RequestParam("from")String from,
                                                    @RequestParam("to")String to,
                                                    @RequestParam(value = "orderName", required = false)String orderName,
                                                    @RequestParam(value = "groupName", required = false)String groupName) {
        Map<String, Object> map = new HashMap<>();
        List<GeneralSalary> generalSalaryList = pieceWorkService.getGroupPieceWorkSummary(from, to, orderName, groupName);
        map.put("generalSalaryList", generalSalaryList);
        return map;
    }

    @RequestMapping(value = "/minigetonetailorbytailorqcodeidandprocedure", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> miniGetOneTailorByTailorQcodeID(@RequestParam("tailorQcodeID") Integer tailorQcodeID,
                                                               @RequestParam("employeeNumber") String employeeNumber){
        Map<String, Object> map = new LinkedHashMap<>();
        if (tailorService.getTailorByTailorQcodeID(tailorQcodeID, null) != null){
            Tailor tailor = tailorService.getTailorByTailorQcodeID(tailorQcodeID, null);
            map.put("tailor", tailor);
            String orderName = tailor.getOrderName();
            List<ProcedureInfo> procedureInfoList = dispatchService.getProcedureInfoByOrderEmp(orderName, employeeNumber);
            if (procedureInfoList == null || procedureInfoList.isEmpty()){
                List<OrderProcedure> orderProcedureList = orderProcedureService.getOrderProcedureByOrder(orderName);
                map.put("orderProcedureList", orderProcedureList);
            }
            return map;
        } else if (finishTailorService.getFinishTailorByTailorQcodeID(tailorQcodeID) != null){
            FinishTailor finishTailor = finishTailorService.getFinishTailorByTailorQcodeID(tailorQcodeID);
            map.put("tailor", finishTailor);
            String orderName = finishTailor.getOrderName();
            List<ProcedureInfo> procedureInfoList = dispatchService.getProcedureInfoByOrderEmp(orderName, employeeNumber);
            if (procedureInfoList == null || procedureInfoList.isEmpty()){
                List<OrderProcedure> orderProcedureList = orderProcedureService.getOrderProcedureByOrder(orderName);
                map.put("orderProcedureList", orderProcedureList);
            }
            return map;
        } else {
            return map;
        }
    }

    @RequestMapping(value = "/minigetpieceworkbyordernamebednumberpackage", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> miniGetPieceWorkByOrderNameBedNumberPackage(@RequestParam("orderName") String orderName,
                                                                           @RequestParam("bedNumber") Integer bedNumber,
                                                                           @RequestParam("packageNumber") Integer packageNumber){
        Map<String, Object> map = new LinkedHashMap<>();
        List<PieceWork> pieceWorkList = pieceWorkService.getPieceWorkByOrderNameBedNumberPackage(orderName, bedNumber, packageNumber);
        map.put("pieceWorkList", pieceWorkList);
        return map;
    }

    @RequestMapping(value = "/minideletepieceworkbatch", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> miniDeletePieceWorkBatch(@RequestParam("pieceWorkIDList") List<Integer> pieceWorkIDList,
                                                        @RequestParam(value = "employeeName", required = false) String employeeName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> fixedSalaryList = fixedSalaryService.getAllFixedSalaryValue();
        String createTime = keyDataDeleteService.getEarlyCreateTimeOfDeleteData(pieceWorkIDList);
        String createMonth = createTime.substring(0, 7);
        for (String fixedValue : fixedSalaryList){
            if (createMonth.equals(fixedValue)){
                map.put("result", 1);
                return map;
            }
        }
        List<PieceWorkDelete> pieceWorkDeleteList = keyDataDeleteService.getPieceWorkListByDeleteId(pieceWorkIDList);
        for (PieceWorkDelete pieceWorkDelete : pieceWorkDeleteList){
            pieceWorkDelete.setOperateUser(employeeName);
        }
        keyDataDeleteService.addPieceWorkDeleteBatch(pieceWorkDeleteList);
        int res = pieceWorkService.deletePieceWorkBatch(pieceWorkIDList);
        map.put("result", res);
        return map;
    }


    @RequestMapping(value = "/updatepieceworklayercountfinance", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> updatePieceWorkLayerCountFinance(@RequestParam("pieceWorkID") Integer pieceWorkID,
                                                                @RequestParam("layerCount") Integer layerCount){
        Map<String, Object> map = new LinkedHashMap<>();
        int res = pieceWorkService.updatePieceWorkLayerCountFinance(pieceWorkID, layerCount);
        map.put("result", res);
        return map;
    }


    @RequestMapping(value = "/getmanualinputbyinfo",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getManualInputByInfo(@RequestParam(value = "from", required = false) String from,
                                                   @RequestParam(value = "to", required = false) String to,
                                                   @RequestParam(value = "orderName", required = false) String orderName){
        Map<String,Object> map = new LinkedHashMap<>();
        Map<String,Object> param = new HashMap<>();
        param.put("from", from);
        param.put("to", to);
        param.put("orderName", orderName);
        List<ManualInput> manualInputList = pieceWorkService.getByInfo(param);
        map.put("data",manualInputList);
        map.put("code",0);
        return map;
    }


}
