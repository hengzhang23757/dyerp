package com.example.erp01.action;

import com.example.erp01.model.OrderProcedure;
import com.example.erp01.service.OrderProcedureService;
import com.example.erp01.service.ProcedureReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "erp")
public class ProcedureReviewController {

    @Autowired
    private ProcedureReviewService procedureReviewService;
    @Autowired
    private OrderProcedureService orderProcedureService;

    @RequestMapping("/procedureReviewStart")
    public String procedureReviewStart(){
        return "ieInfo/procedureReview";
    }

    @RequestMapping(value = "/monthprocedurereviewreport",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> monthProcedureReviewReport(@RequestParam("from")String from,
                                                          @RequestParam("to")String to,
                                                          @RequestParam(value = "orderName", required = false)String orderName,
                                                          @RequestParam(value = "procedureState", required = false)Integer procedureState){
        Map<String, Object> map = new LinkedHashMap<>();
        List<OrderProcedure> orderProcedureList = procedureReviewService.getOrderProcedureByInfo(from, to, orderName);
        List<String> orderNameList = procedureReviewService.getMonthPieceWorkOrderName(from, to, orderName);
        List<OrderProcedure> initOrderProcedureList = orderProcedureService.getOrderProcedureByOrderNames(orderNameList, procedureState);
        List<OrderProcedure> destOrderProcedureList = new ArrayList<>();
        for (OrderProcedure initOrderProcedure : initOrderProcedureList){
            for (OrderProcedure orderProcedure : orderProcedureList){
                if (initOrderProcedure.getOrderName().equals(orderProcedure.getOrderName()) && initOrderProcedure.getProcedureNumber().equals(orderProcedure.getProcedureNumber())){
                    destOrderProcedureList.add(initOrderProcedure);
                }
            }
        }
        map.put("data", destOrderProcedureList);
        return map;
    }

    @RequestMapping(value = "/batchcommitorderprocedure",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> monthProcedureReviewReport(@RequestParam("orderProcedureIDList")List<Integer> orderProcedureIDList,
                                                          @RequestParam("procedureState")Integer procedureState){
        Map<String, Object> map = new LinkedHashMap<>();
        int res = procedureReviewService.updateOrderProcedureByIdBatch(orderProcedureIDList, procedureState);
        map.put("result", res);
        return map;
    }
}
