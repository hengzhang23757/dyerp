package com.example.erp01.action;

import com.example.erp01.model.ColorManage;
import com.example.erp01.service.ColorManageService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "erp")
public class ColorManageController {

    @Autowired
    private ColorManageService colorManageService;

    @RequestMapping("/colorManageStart")
    public String colorManageStart(){
        return "basicData/colorManage";
    }

    @RequestMapping(value = "/addcolormanage",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> addColorManage(@RequestParam("colorManageJson")String colorManageJson){
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        ColorManage colorManage = gson.fromJson(colorManageJson,new TypeToken<ColorManage>(){}.getType());
        int res = colorManageService.addColorManage(colorManage);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/deletecolormanage",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> deleteCustomer(@RequestParam("id")Integer id){
        Map<String, Object> map = new LinkedHashMap<>();
        int res = colorManageService.deleteColorManage(id);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/getallcolormanage",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getAllCustomer(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<ColorManage> colorManageList = colorManageService.getAllColorManage();
        map.put("data",colorManageList);
        return map;
    }

    @RequestMapping(value = "/updatecolormanage",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> updateCustomer(@RequestParam("colorManageJson")String colorManageJson){
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        ColorManage colorManage = gson.fromJson(colorManageJson,new TypeToken<ColorManage>(){}.getType());
        int res = colorManageService.updateColorManage(colorManage);
        map.put("result", res);
        return map;
    }


}
