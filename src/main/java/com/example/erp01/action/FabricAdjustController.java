package com.example.erp01.action;

import com.example.erp01.model.*;
import com.example.erp01.service.FabricAdjustService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Configuration      //1.主要用于标记配置类，兼备Component的效果。
@EnableScheduling   // 2.开启定时任务
@Controller
@RequestMapping(value = "erp")
public class FabricAdjustController {

    @Autowired
    private FabricAdjustService fabricAdjustService;

//    @Scheduled(cron = "0 20 11 * * ? ")
//    public void fabricAccessoryAdjust(){
//        System.out.println("开始=======");
//        List<ManufactureAccessory> manufactureAccessoryList = fabricAdjustService.getAllManufactureAccessory();
//        List<AccessoryOutRecord> accessoryOutRecordList = fabricAdjustService.getAllAccessoryOutRecord();
//        for (AccessoryOutRecord accessoryOutRecord : accessoryOutRecordList){
//            boolean flag = false;
//            for (ManufactureAccessory manufactureAccessory : manufactureAccessoryList){
//                if (accessoryOutRecord.getOrderName().equals(manufactureAccessory.getOrderName()) && accessoryOutRecord.getAccessoryName().equals(manufactureAccessory.getAccessoryName()) && accessoryOutRecord.getColorName().equals(manufactureAccessory.getColorName()) && accessoryOutRecord.getSizeName().equals(manufactureAccessory.getSizeName())){
//                    accessoryOutRecord.setAccessoryID(manufactureAccessory.getAccessoryID());
//                    flag = true;
//                }
//            }
//            if (!flag){
//                System.out.println(accessoryOutRecord);
//            }
//        }
//        int res = fabricAdjustService.updateAccessoryOutRecordIdBatch(accessoryOutRecordList);
//        System.out.println("结束=======" + res);
//    }

//    @Scheduled(cron = "0 19 16 * * ? ")
//    public void accessoryAccountSync(){
//        System.out.println("开始=======");
//        List<ManufactureAccessory> manufactureAccessoryList = fabricAdjustService.getAllManufactureAccessory();
//        List<AccessoryInStore> accessoryInStoreList = fabricAdjustService.getAllAccessoryInStore();
//        for (AccessoryInStore accessoryInStore : accessoryInStoreList){
//            for (ManufactureAccessory manufactureAccessory : manufactureAccessoryList){
//                if (accessoryInStore.getAccessoryID().equals(manufactureAccessory.getAccessoryID())){
//                    accessoryInStore.setPrice(manufactureAccessory.getPrice());
//                }
//            }
//        }
//        int res = fabricAdjustService.updateAccessoryInStorePriceBatch(accessoryInStoreList);
//        System.out.println("结束=======" + res);
//    }

}
