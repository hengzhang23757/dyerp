package com.example.erp01.action;

import com.example.erp01.model.ProcedureInfo;
import com.example.erp01.model.ReWork;
import com.example.erp01.service.HangSalaryService;
import com.example.erp01.service.OrderClothesService;
import com.example.erp01.service.OrderProcedureService;
import com.example.erp01.service.ReWorkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "erp")
public class ReWorkController {

    @Autowired
    private ReWorkService reWorkService;
    @Autowired
    private OrderClothesService orderClothesService;
    @Autowired
    private OrderProcedureService orderProcedureService;
    @Autowired
    private HangSalaryService hangSalaryService;

    @RequestMapping("/reWorkStart")
    public String completionStart(Model model){
        model.addAttribute("bigMenuTag",12);
        model.addAttribute("menuTag",123);
        return "report/reWork";
    }

    @RequestMapping(value = "/getreworkreport",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getReWorkReport(@RequestParam("orderName")String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<ReWork> reWorkList = reWorkService.getReWorkByOrder(orderName);
        for (ReWork reWork : reWorkList){
            String tmpOrderName = reWork.getOrderName();
            int procedureNumber = reWork.getProcedureNumber();
            Date reWorkDate = reWork.getReWorkDate();
            ProcedureInfo pi = orderProcedureService.getProcedureInfoByOrderProcedureNumber(tmpOrderName, procedureNumber);
            String procedureName = "通用";
            reWork.setClothesVersionNumber(orderClothesService.getVersionNumberByOrderName(tmpOrderName));
            reWork.setCustomer(orderClothesService.getCustomerNameByOrderName(tmpOrderName));
            reWork.setDescription(orderClothesService.getDescriptionByOrder(tmpOrderName));
            if (pi != null){
                procedureName = pi.getProcedureName();
            }
            reWork.setProcedureName(procedureName);
            int thisDayCount = 1;
            if (reWorkService.getWorkCountByProcedure(tmpOrderName, procedureNumber, reWorkDate) != null){
                thisDayCount = reWorkService.getWorkCountByProcedure(tmpOrderName, procedureNumber, reWorkDate);
            }
            reWork.setReWorkRate((float) reWork.getReWorkCount()/(float) thisDayCount);
            reWork.setWellRate(1-reWork.getReWorkRate());
        }
        map.put("reWorkList",reWorkList);
        return map;
    }

    @RequestMapping(value = "/getreworksummaryreport",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getReWorkSummaryReport(@RequestParam("orderName")String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<ReWork> reWorkList = reWorkService.getReWorkSummaryByOrder(orderName);
        for (ReWork reWork : reWorkList){
            String tmpOrderName = reWork.getOrderName();
            int procedureNumber = reWork.getProcedureNumber();
            ProcedureInfo pi = orderProcedureService.getProcedureInfoByOrderProcedureNumber(tmpOrderName, procedureNumber);
            String procedureName = "通用";
            reWork.setClothesVersionNumber(orderClothesService.getVersionNumberByOrderName(tmpOrderName));
            reWork.setCustomer(orderClothesService.getCustomerNameByOrderName(tmpOrderName));
            reWork.setDescription(orderClothesService.getDescriptionByOrder(tmpOrderName));
            if (pi != null){
                procedureName = pi.getProcedureName();
            }
            reWork.setProcedureName(procedureName);
            int totalCount1 = reWorkService.getWorkCountByProcedure(tmpOrderName, 555, null);
            int totalCount2 = reWorkService.getWorkCountByProcedure(tmpOrderName, 6555, null);
            reWork.setReWorkRate((float) reWork.getReWorkCount()/(float) (totalCount1 + totalCount2));
            reWork.setWellRate(1-reWork.getReWorkRate());
        }
        map.put("reWorkList",reWorkList);
        return map;
    }


}
