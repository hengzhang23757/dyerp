package com.example.erp01.action;

import com.example.erp01.model.Employee;
import com.example.erp01.model.OrderClothes;
import com.example.erp01.model.PieceWork;
import com.example.erp01.model.PieceWorkDelete;
import com.example.erp01.service.KeyDataDeleteService;
import com.example.erp01.service.PieceWorkService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

@Configuration      //1.主要用于标记配置类，兼备Component的效果。
@EnableScheduling   // 2.开启定时任务
@Controller
@RequestMapping(value = "erp")
public class KeyDataDeleteController {

    @Autowired
    private KeyDataDeleteService keyDataDeleteService;
    @Autowired
    private PieceWorkService pieceWorkService;

    @RequestMapping(value = "/keyPieceWorkDataDeleteStart")
    public String keyPieceWorkDataDeleteStart(){
        return "pieceWorkDelete/pieceWorkDelete";
    }


    @RequestMapping(value = "/getDeletePieceWorkByInfo",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getDeletePieceWorkByInfo(@RequestParam(value = "from", required = false) String from,
                                                       @RequestParam(value = "to", required = false) String to,
                                                       @RequestParam(value = "orderName", required = false)String orderName){
        Map<String,Object> map = new HashMap<>();
        Map<String,Object> param = new HashMap<>();
        param.put("from", from);
        param.put("to", to);
        param.put("orderName",orderName);
        List<PieceWorkDelete> pieceWorkDeleteList = keyDataDeleteService.getPieceWorkDeleteByMap(param);
        map.put("data", pieceWorkDeleteList);
        return map;
    }

    @RequestMapping(value = "/pieceWorkDataRecover",method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> pieceWorkDataRecover(@RequestParam("pieceWorkDeleteJson") String pieceWorkDeleteJson){
        Map<String,Object> map = new HashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        List<PieceWork> pieceWorkList = gson.fromJson(pieceWorkDeleteJson,new TypeToken<List<PieceWork>>(){}.getType());
        List<PieceWorkDelete> pieceWorkDeleteList = gson.fromJson(pieceWorkDeleteJson,new TypeToken<List<PieceWorkDelete>>(){}.getType());
        List<Integer> idList = new ArrayList<>();
        for (PieceWorkDelete pieceWork : pieceWorkDeleteList){
            idList.add(pieceWork.getId());
            if (pieceWork.getBedNumber() != null && pieceWork.getProcedureNumber() != null){
                List<PieceWork> pieceWorkList1 = pieceWorkService.getPieceWorkByOrderBedPack(pieceWork.getOrderName(), pieceWork.getBedNumber(), pieceWork.getPackageNumber(), pieceWork.getProcedureNumber());
                if (pieceWorkList1 != null && !pieceWorkList1.isEmpty()){
                    map.put("msg", "款号:"+pieceWork.getOrderName() + "床号:" + pieceWork.getBedNumber() + "扎号:" + pieceWork.getPackageNumber() + "工序号:" + pieceWork.getProcedureNumber() + "已有新的计件记录");
                    map.put("result", 4);
                    return map;
                }
            }
        }
        int res1 = keyDataDeleteService.deleteKeyData(idList);
        int res2 = pieceWorkService.addPieceWorkBatchNew(pieceWorkList);
        map.put("result", res1 + res2);
        return map;
    }


}
