package com.example.erp01.action;

import com.example.erp01.model.*;
import com.example.erp01.service.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.awt.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;

@Controller
@RequestMapping(value = "erp")
public class ManufactureOrderController {

    @Autowired
    private ManufactureOrderService manufactureOrderService;

    @Autowired
    private ProcessRequirementService processRequirementService;

    @Autowired
    private OrderMeasureService orderMeasureService;

    @Autowired
    private OrderClothesService orderClothesService;

    @Autowired
    private ManufactureFabricService manufactureFabricService;

    @Autowired
    private ManufactureAccessoryService manufactureAccessoryService;

    @Autowired
    private HomePageService homePageService;

    @Autowired
    private MessageService messageService;

    @Autowired
    private TailorService tailorService;

    @Autowired
    private StyleImageService styleImageService;

    @RequestMapping("/processOrderStart")
    public String orderStart(){
        return "orderMsg/processOrder";
    }

    @RequestMapping("/manufactureOrderModifyReviewStart")
    public String manufactureOrderModifyReviewStart(){
        return "console/modifyReview";
    }

    @RequestMapping(value = "/addmanufactureorder", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> addEmbPlanBatch(@RequestParam("manufactureOrderJson")String manufactureOrderJson,
                                               @RequestParam("orderClothesJson")String orderClothesJson){
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        ManufactureOrder manufactureOrder = gson.fromJson(manufactureOrderJson, ManufactureOrder.class);
        List<OrderClothes> orderClothesList = gson.fromJson(orderClothesJson,new TypeToken<List<OrderClothes>>(){}.getType());
        for (OrderClothes orderClothes : orderClothesList){
            String uuid = UUID.randomUUID().toString();
            orderClothes.setGuid(uuid);
        }
        String orderName = orderClothesList.get(0).getOrderName();
        String clothesVersionNumber = orderClothesList.get(0).getClothesVersionNumber();
        List<OrderClothes> orderClothesList1 = orderClothesService.getOrderClothesByOrderName(orderName);
        if (orderClothesList1 != null && !orderClothesList1.isEmpty()){
            map.put("result", 4);
            return map;
        }
        int res1 = manufactureOrderService.addManufactureOrder(manufactureOrder);
        int res2 = orderClothesService.addOrderClothes(orderClothesList);
        if (res2 == 100){
            map.put("error", "该款号的颜色-尺码已经录入,不能重复录入");
            return map;
        }
        if (res1 == 0 && res2 == 0){
            map.put("result", 0);
            Message fabricMessage = new Message();
            fabricMessage.setMessageType("面料");
            fabricMessage.setTitle("录入面料提醒");
            fabricMessage.setMessageBodyOne("<p><b><font size='5' color='#f9963b'>款号:"+ orderName + "单号:" + clothesVersionNumber+"<br>" + "制单信息已经录入，请尽快录入面料</font></b></p>");
            fabricMessage.setClothesVersionNumber(clothesVersionNumber);
            fabricMessage.setOrderName(orderName);
            messageService.autoSendMessageToDepartment("面料跟单", fabricMessage);
            Message accessoryMessage = new Message();
            accessoryMessage.setMessageType("辅料");
            accessoryMessage.setTitle("录入辅料提醒");
            accessoryMessage.setMessageBodyOne("<p><b><font size='5' color='#f9963b'>款号:"+ orderName + "单号:" + clothesVersionNumber+"<br>" + "制单信息已经录入，请尽快录入辅料</font></b></p>");
            accessoryMessage.setClothesVersionNumber(clothesVersionNumber);
            accessoryMessage.setOrderName(orderName);
            messageService.autoSendMessageToDepartment("辅料跟单", accessoryMessage);
            messageService.autoSendMessageToDepartment("面料跟单", fabricMessage);
            Message ieMessage = new Message();
            accessoryMessage.setMessageType("IE");
            accessoryMessage.setTitle("录入订单工序提醒");
            accessoryMessage.setMessageBodyOne("<p><b><font size='5' color='#f9963b'>款号:"+ orderName + "单号:" + clothesVersionNumber+"<br>" + "制单信息已经录入，请尽快添加工序表</font></b></p>");
            accessoryMessage.setClothesVersionNumber(clothesVersionNumber);
            accessoryMessage.setOrderName(orderName);
            messageService.autoSendMessageToDepartment("IE", accessoryMessage);
        }else{
            map.put("result", 1);
        }
        return map;
    }

    @RequestMapping(value = "/getmanufactureorderbyordername", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getManufactureOrderByOrderName(@RequestParam("orderName")String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        ManufactureOrder manufactureOrder = manufactureOrderService.getManufactureOrderByOrderName(orderName);
        List<OrderClothes> orderClothesList = orderClothesService.getOrderByName(orderName);
        List<ManufactureFabric> manufactureFabricList = manufactureFabricService.getManufactureFabricByOrder(orderName, null);
        List<ManufactureAccessory> manufactureAccessoryList = manufactureAccessoryService.getManufactureAccessoryByOrder(orderName, null);
        List<OrderMeasure> orderMeasureList = orderMeasureService.getOrderMeasureByOrder(orderName);
        List<ProcessRequirement> processRequirementList = processRequirementService.getProcessRequirementByOrder(orderName);
        map.put("manufactureOrder", manufactureOrder);
        map.put("orderMeasureList", orderMeasureList);
        map.put("processRequirementList", processRequirementList);
        map.put("orderClothesList", orderClothesList);
        map.put("manufactureFabricList", manufactureFabricList);
        map.put("manufactureAccessoryList", manufactureAccessoryList);
        return map;
    }

    @RequestMapping(value = "/deletemanufactureorderbyordername", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> deleteManufactureOrderByOrderName(@RequestParam("orderName")String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        int res1 = manufactureOrderService.deleteManufactureByOrder(orderName);
        if (res1 == 0){
            map.put("result", 0);
        }else{
            map.put("result", 1);
        }
        return map;
    }

    @RequestMapping(value = "/updatemanufactureorderbasicinfo", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> updateManufactureOrder(@RequestParam("manufactureOrderJson")String manufactureOrderJson){
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        ManufactureOrder manufactureOrder = gson.fromJson(manufactureOrderJson,new TypeToken<ManufactureOrder>(){}.getType());
        orderClothesService.updateOrderClothesByOrder(manufactureOrder.getOrderName(), manufactureOrder.getStyleName(), manufactureOrder.getSeason(), manufactureOrder.getCustomerName(), manufactureOrder.getBuyer());
        int res1 = manufactureOrderService.updateManufactureOrder(manufactureOrder);
        if (res1 == 0){
            map.put("result", 0);
        }else{
            map.put("result", 1);
        }
        return map;
    }


    @RequestMapping(value = "/deleteallmanufactureorderinfobyordername", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> deleteAllManufactureOrderInfoByOrderName(@RequestParam("orderName")String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<Integer> bedNumberList = tailorService.getBedNumbersByOrderNamePartNameTailorType(orderName, null, null);
        if (bedNumberList != null && !bedNumberList.isEmpty()){
            map.put("result", 4);
            return map;
        }
        int res1 = manufactureOrderService.deleteManufactureByOrder(orderName);
        int res2 = orderMeasureService.deleteOrderMeasureByOrder(orderName);
        int res3 = processRequirementService.deleteProcessRequirementByOrder(orderName);
        int res4 = orderClothesService.deleteOrderByName(orderName);
        int res5 = manufactureFabricService.deleteManufactureFabricByName(orderName);
        int res6 = manufactureAccessoryService.deleteManufactureAccessoryByName(orderName);
        int res7 = homePageService.deleteHomePageDataByOrderName(orderName);
        if (res1 == 0 && res2 == 0 && res3 == 0 && res4 == 0 && res5 == 0 && res6 == 0){
            map.put("result", 0);
        }else{
            map.put("result", 1);
        }
        return map;
    }

    @RequestMapping(value = "/getmanufactureorderinfobyordername", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getManufactureOrderInfoByOrderName(@RequestParam("orderName")String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        ManufactureOrder manufactureOrder = manufactureOrderService.getManufactureOrderByOrderName(orderName);
        List<ManufactureOrder> list = new ArrayList<>();
        if(manufactureOrder!=null) {
            list.add(manufactureOrder);
        }
        map.put("data", list);
        map.put("count", list.size());
        map.put("code",0);
        return map;
    }

    @RequestMapping(value = "/getplaceorderbyinfo", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getPlaceOrderByInfo(@RequestParam(value = "orderName", required = false)String orderName,
                                                   @RequestParam("searchType")String searchType,
                                                   @RequestParam(value = "supplier", required = false)String supplier,
                                                   @RequestParam(value = "materialName", required = false)String materialName,
                                                   @RequestParam(value = "season", required = false)String season,
                                                   @RequestParam(value = "customerName", required = false)String customerName,
                                                   @RequestParam("checkState")String checkState){
        Map<String, Object> map = new LinkedHashMap<>();
        if (checkState.equals("全部")){
            checkState = null;
        }
        if (searchType.equals("面料")){
            List<ManufactureFabric> manufactureFabricList = manufactureFabricService.getManufactureFabricInPlaceOrder(orderName, supplier, checkState);
            map.put("data", manufactureFabricList);
        }else if (searchType.equals("辅料")){
            List<ManufactureAccessory> manufactureAccessoryList = manufactureAccessoryService.getManufactureAccessoryInPlaceOrder(orderName, supplier, checkState);
            map.put("data", manufactureAccessoryList);
        }
        return map;
    }

    @RequestMapping(value = "/getordercheckbyinfo", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getOrderCheckByInfo(@RequestParam(value = "orderName", required = false)String orderName,
                                                   @RequestParam("searchType")String searchType,
                                                   @RequestParam(value = "supplier", required = false)String supplier,
                                                   @RequestParam(value = "materialName", required = false)String materialName,
                                                   @RequestParam(value = "season", required = false)String season,
                                                   @RequestParam(value = "customerName", required = false)String customerName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> orderNameList = new ArrayList<>();
        if (orderName != null){
            orderNameList.add(orderName);
        } else if (season != null || customerName != null){
            orderNameList = manufactureOrderService.getOrderNameByCustomerSeason(season, customerName);
        }
        if (searchType.equals("面料")){
            List<ManufactureFabric> manufactureFabricList = manufactureFabricService.getManufactureFabricInPlaceOrderByInfo(orderNameList, supplier, materialName, null);
            map.put("manufactureFabricList", manufactureFabricList);
        }else if (searchType.equals("辅料")){
            List<ManufactureAccessory> manufactureAccessoryList = manufactureAccessoryService.getManufactureAccessoryInPlaceOrderByInfo(orderNameList, supplier, materialName, null);
            map.put("manufactureAccessoryList", manufactureAccessoryList);
        }
        return map;
    }

    @RequestMapping(value = "/changeClothesImage", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> changeClothesImage(@RequestParam("orderName")String orderName,
                                                  @RequestParam("imgUrl")String imgUrl){
        Map<String, Object> map = new LinkedHashMap<>();
        ManufactureOrder manufactureOrder = manufactureOrderService.getManufactureOrderByOrderName(orderName);
        manufactureOrder.setImgUrl1(imgUrl);
        int res = manufactureOrderService.updateManufactureOrder(manufactureOrder);
        map.put("result", res);
        return map;
    }


    @RequestMapping(value = "/copymanufactureorder", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> copyManufactureOrder(@RequestParam("manufactureOrderJson")String manufactureOrderJson,
                                                    @RequestParam("orderClothesJson")String orderClothesJson,
                                                    @RequestParam("initOrderName")String initOrderName,
                                                    @RequestParam("toOrderName")String toOrderName){
        Map<String, Object> map = new LinkedHashMap<>();
        ManufactureOrder initManufactureOrder = manufactureOrderService.getManufactureOrderByOrderName(toOrderName);
        List<OrderClothes> initOrderClothesList = orderClothesService.getOrderClothesByOrderName(toOrderName);
        if (initManufactureOrder != null || (!initOrderClothesList.isEmpty() && initOrderClothesList != null)){
            map.put("error", "替换到的款号已存在,款号不能重复！");
            return map;
        }
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        ManufactureOrder manufactureOrder = gson.fromJson(manufactureOrderJson, ManufactureOrder.class);
        List<OrderClothes> orderClothesList = gson.fromJson(orderClothesJson,new TypeToken<List<OrderClothes>>(){}.getType());
        for (OrderClothes orderClothes : orderClothesList){
            String uuid = UUID.randomUUID().toString();
            orderClothes.setGuid(uuid);
        }
        String orderName = orderClothesList.get(0).getOrderName();
        String clothesVersionNumber = orderClothesList.get(0).getClothesVersionNumber();
        List<OrderClothes> orderClothesList1 = orderClothesService.getOrderClothesByOrderName(orderName);
        if (orderClothesList1 != null && !orderClothesList1.isEmpty()){
            map.put("result", 4);
            return map;
        }
        int res1 = manufactureOrderService.addManufactureOrder(manufactureOrder);
        int res2 = orderClothesService.addOrderClothes(orderClothesList);
        if (res2 == 100){
            map.put("error", "该款号的颜色-尺码已经录入,不能重复录入");
            return map;
        }
        if (res1 == 0 && res2 == 0){
            map.put("result", 0);
            Message fabricMessage = new Message();
            fabricMessage.setMessageType("面料");
            fabricMessage.setTitle("制单录入提醒");
            fabricMessage.setMessageBodyOne("<p><b><font size='5' color='#f9963b'>款号:"+ orderName + "单号:" + clothesVersionNumber+"<br>" + "制单信息已经录入，请尽快录入面料</font></b></p>");
            fabricMessage.setClothesVersionNumber(clothesVersionNumber);
            fabricMessage.setOrderName(orderName);
            messageService.autoSendMessageToDepartment("业务部", fabricMessage);
            Message accessoryMessage = new Message();
            accessoryMessage.setMessageType("辅料");
            accessoryMessage.setTitle("制单录入提醒");
            accessoryMessage.setMessageBodyOne("<p><b><font size='5' color='#f9963b'>款号:"+ orderName + "单号:" + clothesVersionNumber+"<br>" + "制单信息已经录入，请尽快录入辅料</font></b></p>");
            accessoryMessage.setClothesVersionNumber(clothesVersionNumber);
            accessoryMessage.setOrderName(orderName);
            messageService.autoSendMessageToDepartment("业务部", accessoryMessage);
        }else{
            map.put("result", 1);
        }
        // 尺寸工艺复制
        OrderMeasure orderMeasure = orderMeasureService.getOneOrderMeasureByOrder(initOrderName);
        if (orderMeasure != null){
            orderMeasure.setOrderName(toOrderName);
            orderMeasure.setClothesVersionNumber(manufactureOrder.getClothesVersionNumber());
            orderMeasureService.addOrderMeasure(orderMeasure);
        }
        // 公仔图复制
        StyleImage styleImage = styleImageService.getStyleImageByOrder(initOrderName);
        if (styleImage != null){
            styleImage.setOrderName(toOrderName);
            styleImage.setClothesVersionNumber(manufactureOrder.getClothesVersionNumber());
            styleImageService.addStyleImage(styleImage);
        }
        if (res1 == 0 && res2 == 0){
            map.put("success", "复制成功");
        }
        return map;
    }

    @RequestMapping(value = "/gethistoryseason", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getHistorySeason(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> seasonList = manufactureOrderService.getHistoryDistinctSeason();
        map.put("seasonList", seasonList);
        return map;
    }

    @RequestMapping(value = "/gethistorycustomer", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getHistoryCustomer(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> customerList = manufactureOrderService.getHistoryCustomer();
        map.put("customerList", customerList);
        return map;
    }

    @RequestMapping(value = "/updatedeadlinebyorder", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> updateDeadLineByOrder(@RequestParam("orderName")String orderName,
                                                     @RequestParam("toDeadLine")String toDeadLine) throws Exception{
        Map<String, Object> map = new LinkedHashMap<>();
        ManufactureOrder manufactureOrder = manufactureOrderService.getManufactureOrderByOrderName(orderName);
        Date deliveryDate = new SimpleDateFormat("yyyy-MM-dd").parse(toDeadLine);
        manufactureOrder.setDeliveryDate(deliveryDate);
        int res1 = manufactureOrderService.updateManufactureOrder(manufactureOrder);
        List<OrderClothes> orderClothesList = orderClothesService.getOrderClothesByOrderName(orderName);
        for (OrderClothes  orderClothes : orderClothesList){
            orderClothes.setDeliveryDate(deliveryDate);
        }
        int res2 = orderClothesService.updateOrderClothes(orderClothesList);
        if (res1 == 0 && res2 == 0){
            map.put("result", 0);
        } else {
            map.put("result", 1);
        }
        return map;
    }

    @RequestMapping(value = "/updateseasonbyorder", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> updateSeasonByOrder(@RequestParam("orderName")String orderName,
                                                   @RequestParam("toSeason")String toSeason) {
        Map<String, Object> map = new LinkedHashMap<>();
        ManufactureOrder manufactureOrder = manufactureOrderService.getManufactureOrderByOrderName(orderName);
        manufactureOrder.setSeason(toSeason);
        int res1 = manufactureOrderService.updateManufactureOrder(manufactureOrder);
        List<OrderClothes> orderClothesList = orderClothesService.getOrderClothesByOrderName(orderName);
        for (OrderClothes  orderClothes : orderClothesList){
            orderClothes.setSeason(toSeason);
        }
        int res2 = orderClothesService.updateOrderClothes(orderClothesList);
        if (res1 == 0 && res2 == 0){
            map.put("result", 0);
        } else {
            map.put("result", 1);
        }
        return map;
    }

    @RequestMapping(value = "/updateordercolordata", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> updateOrderColorData(@RequestParam("orderName")String orderName,
                                                   @RequestParam("orderColorJson")String orderColorJson) {
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        List<Tailor> tailorList = tailorService.getTailorByInfo(orderName, null, null, null, null, null, null, 0);
        if (tailorList != null && !tailorList.isEmpty()){
            map.put("result", 3);
            return map;
        }
        List<ColorUpdate> colorUpdateList = gson.fromJson(orderColorJson,new TypeToken<List<ColorUpdate>>(){}.getType());
        List<OrderClothes> orderClothesList = orderClothesService.getOrderClothesByOrderName(orderName);
        List<OrderClothes> updateOrderClothesList = new ArrayList<>();
        List<String> newColorList = new ArrayList<>();
        List<ManufactureFabric> manufactureFabricList = manufactureFabricService.getManufactureFabricByOrder(orderName, null);
        List<ManufactureAccessory> manufactureAccessoryList = manufactureAccessoryService.getManufactureAccessoryByOrder(orderName, null);
        for (ColorUpdate colorUpdate : colorUpdateList){
            newColorList.add(colorUpdate.getToColor());
            if (!colorUpdate.getColor().equals(colorUpdate.getToColor())){
                for (OrderClothes orderClothes : orderClothesList){
                    if (orderClothes.getColorName().equals(colorUpdate.getColor())){
                        orderClothes.setColorName(colorUpdate.getToColor());
                        updateOrderClothesList.add(orderClothes);
                    }
                }
                for (ManufactureFabric manufactureFabric : manufactureFabricList){
                    if (manufactureFabric.getColorName().equals(colorUpdate.getColor())){
                        manufactureFabric.setColorName(colorUpdate.getToColor());
                    }
                }
                for (ManufactureAccessory manufactureAccessory : manufactureAccessoryList){
                    if (manufactureAccessory.getColorName().equals(colorUpdate.getColor())){
                        manufactureAccessory.setColorName(colorUpdate.getToColor());
                    }
                }
            }
        }
        manufactureAccessoryService.updateManufactureAccessoryBatch(manufactureAccessoryList);
        manufactureFabricService.updateManufactureFabricBatch(manufactureFabricList);
        ManufactureOrder manufactureOrder = manufactureOrderService.getManufactureOrderByOrderName(orderName);
        manufactureOrder.setColorInfo(newColorList.toString());
        int res1 = manufactureOrderService.updateManufactureOrder(manufactureOrder);
        int res2 = orderClothesService.updateOrderClothes(updateOrderClothesList);
        if (res1 == 0 && res2 == 0){
            map.put("result", 0);
        } else {
            map.put("result", 1);
        }
        return map;
    }


    @RequestMapping(value = "/updateordersizedata", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> updateOrderSizeData(@RequestParam("orderName")String orderName,
                                                   @RequestParam("orderSizeJson")String orderSizeJson) {
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        List<Tailor> tailorList = tailorService.getTailorByInfo(orderName, null, null, null, null, null, null, 0);
        if (tailorList != null && !tailorList.isEmpty()){
            map.put("result", 3);
            return map;
        }
        List<SizeUpdate> sizeUpdateList = gson.fromJson(orderSizeJson,new TypeToken<List<SizeUpdate>>(){}.getType());
        List<OrderClothes> orderClothesList = orderClothesService.getOrderClothesByOrderName(orderName);
        List<OrderClothes> updateOrderClothesList = new ArrayList<>();
        List<ManufactureAccessory> manufactureAccessoryList = manufactureAccessoryService.getManufactureAccessoryByOrder(orderName, null);
        List<String> newSizeList = new ArrayList<>();
        for (SizeUpdate sizeUpdate : sizeUpdateList){
            newSizeList.add(sizeUpdate.getToSize());
            if (!sizeUpdate.getSize().equals(sizeUpdate.getToSize())){
                for (OrderClothes orderClothes : orderClothesList){
                    if (orderClothes.getSizeName().equals(sizeUpdate.getSize())){
                        orderClothes.setSizeName(sizeUpdate.getToSize());
                        updateOrderClothesList.add(orderClothes);
                    }
                }
                for (ManufactureAccessory manufactureAccessory : manufactureAccessoryList){
                    if (manufactureAccessory.getSizeName().equals(sizeUpdate.getSize())){
                        manufactureAccessory.setSizeName(sizeUpdate.getToSize());
                    }
                }
            }
        }
        manufactureAccessoryService.updateManufactureAccessoryBatch(manufactureAccessoryList);
        ManufactureOrder manufactureOrder = manufactureOrderService.getManufactureOrderByOrderName(orderName);
        manufactureOrder.setSizeInfo(newSizeList.toString());
        int res1 = manufactureOrderService.updateManufactureOrder(manufactureOrder);
        int res2 = orderClothesService.updateOrderClothes(updateOrderClothesList);
        if (res1 == 0 && res2 == 0){
            map.put("result", 0);
        } else {
            map.put("result", 1);
        }
        return map;
    }

    @RequestMapping(value = "/getcopymanufactureorderinfo", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getCopyManufactureOrderInfo(@RequestParam("orderName")String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        ManufactureOrder manufactureOrder = manufactureOrderService.getManufactureOrderByOrderName(orderName);
        List<String> colorNameList = orderClothesService.getOrderColorNamesByOrder(orderName);
        List<String> sizeNameList = orderClothesService.getOrderSizeNamesByOrder(orderName);
        map.put("colorNameList", colorNameList);
        map.put("sizeNameList", sizeNameList);
        map.put("manufactureOrder", manufactureOrder);
        map.put("code", 0);
        return map;
    }

    @RequestMapping(value = "/decideorderclothescountupdatestate", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> decideOrderClothesCountUpdateState(@RequestParam("orderName")String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        ManufactureOrder manufactureOrder = manufactureOrderService.getManufactureOrderByOrderName(orderName);
        if (manufactureOrder.getModifyState().equals("锁定")){
            List<ManufactureFabric> manufactureFabricList = manufactureFabricService.getManufactureFabricByOrder(orderName, null);
            List<ManufactureAccessory> manufactureAccessoryList = manufactureAccessoryService.getManufactureAccessoryByOrder(orderName, null);
            if ((manufactureFabricList == null || manufactureFabricList.isEmpty()) && (manufactureAccessoryList == null || manufactureAccessoryList.isEmpty())){
                map.put("state", "yes");
            } else {
                map.put("state", "no");
            }
        } else if (manufactureOrder.getModifyState().equals("开放")) {
            map.put("state", "yes");
        } else {
            map.put("state", "no");
        }
        return map;
    }

    @RequestMapping(value = "/getmanufactureordercountreviewdatacount", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getManufactureOrderCountReviewDataCount(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<ManufactureOrder> manufactureOrderList = manufactureOrderService.getManufactureOrderByModifyState("反审");
        map.put("count", manufactureOrderList.size());
        return map;
    }

    @RequestMapping(value = "/getmanufactureordercountreviewdata", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getManufactureOrderCountReviewData(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<ManufactureOrder> manufactureOrderList = manufactureOrderService.getManufactureOrderByModifyState("反审");
        for (ManufactureOrder manufactureOrder : manufactureOrderList){
            manufactureOrder.setRemark("修改数量");
        }
        map.put("data", manufactureOrderList);
        return map;
    }

    @RequestMapping(value = "/updatemanufactureordermodifystate", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> updateManufactureOrderModifyState(@RequestParam("orderName")String orderName,
                                                                 @RequestParam("modifyState")String modifyState){
        Map<String, Object> map = new LinkedHashMap<>();
        int res = manufactureOrderService.changeModifyStateByOrderName(orderName, modifyState);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/changeordername", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> changeOrderName(@RequestParam("orderName")String orderName,
                                               @RequestParam("toOrderName")String toOrderName){
        Map<String, Object> map = new LinkedHashMap<>();
        manufactureOrderService.changeOrderName(orderName, toOrderName);
        manufactureAccessoryService.changeOrderName(orderName, toOrderName);
        manufactureFabricService.changeOrderName(orderName, toOrderName);
        orderClothesService.changeOrderName(orderName, toOrderName);
        styleImageService.changeOrderName(orderName, toOrderName);
        map.put("result", 0);
        return map;
    }

}
