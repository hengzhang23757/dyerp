package com.example.erp01.action;

import com.example.erp01.model.EmpRoom;
import com.example.erp01.model.Room;
import com.example.erp01.service.EmpRoomService;
import com.example.erp01.service.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.*;

/**
 * Created by hujian on 2019/11/16
 */
@Controller
@RequestMapping(value = "erp")
public class accommodationController {



    @Autowired
    private RoomService roomService;

    @Autowired
    private EmpRoomService empRoomService;

    @RequestMapping("/accommodationStart")
    public String accommodationStart(Model model){
        Map<String, Object> map = new LinkedHashMap<>();
        List<Room> roomList = roomService.getAllRoom();
        List<String> floorList = roomService.getAllFloor();
        List<EmpRoom> empRoomList = empRoomService.getAllEmpRoom();
        for(String floor : floorList) {
            Map<String,Object> roomMap = new LinkedHashMap<>();
            Iterator<Room> roomIterator = roomList.iterator();
            while(roomIterator.hasNext()) {
                Room room = roomIterator.next();
                if(floor.equals(room.getFloor())) {
                    String roomNumber = room.getRoomNumber();
                    int capacity = room.getCapacity();
                    List<EmpRoom> empRoomList1 = new ArrayList<>();
                    Iterator<EmpRoom> empRoomIterator = empRoomList.iterator();
                    while(empRoomIterator.hasNext()) {
                        EmpRoom empRoom = empRoomIterator.next();
                        if (roomNumber.equals(empRoom.getRoomNumber())) {
                            empRoom.setCapacity(capacity);
                            empRoomList1.add(empRoom);
                            empRoomIterator.remove();
                        }else {
                            break;
                        }
                    }
                    roomMap.put(roomNumber, empRoomList1);
                    roomIterator.remove();
                }else {
                    break;
                }
            }
            map.put(floor,roomMap);
        }
        model.addAttribute("data",map);
        return "logistic/accommodation";
    }

}
