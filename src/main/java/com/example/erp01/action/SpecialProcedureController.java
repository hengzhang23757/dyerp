package com.example.erp01.action;

import com.example.erp01.model.SpecialProcedure;
import com.example.erp01.service.SpecialProcedureService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "erp")
public class SpecialProcedureController {

    @Autowired
    private SpecialProcedureService specialProcedureService;

    @RequestMapping("/specialProcedureStart")
    public String opaBackInputStart(){
        return "ieInfo/specialProcedure";
    }

    @RequestMapping(value = "/addspecialprocedurebatch", method = RequestMethod.POST)
    @ResponseBody
    public int addSpecialProcedureBatch(@RequestParam("specialProcedureJson")String opaBackJson){
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        List<SpecialProcedure> specialProcedureList = gson.fromJson(opaBackJson,new TypeToken<List<SpecialProcedure>>(){}.getType());
        return specialProcedureService.addSpecialProcedureBatch(specialProcedureList);
    }

    @RequestMapping(value = "/deletespecialprocedure", method = RequestMethod.POST)
    @ResponseBody
    public int deleteSpecialProcedure(@RequestParam("specialID")Integer specialID){
        return specialProcedureService.deleteSpecialProcedure(specialID);
    }

    @RequestMapping(value = "/getallspecialprocedure", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getAllSpecialProcedure(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<SpecialProcedure> specialProcedureList = specialProcedureService.getAllSpecialProcedure();
        map.put("specialProcedureList", specialProcedureList);
        return map;
    }
}
