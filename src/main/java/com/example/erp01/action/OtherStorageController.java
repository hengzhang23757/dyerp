package com.example.erp01.action;

import com.example.erp01.model.OtherOutDto;
import com.example.erp01.model.OtherStorage;
import com.example.erp01.model.OtherTailor;
import com.example.erp01.model.Tailor;
import com.example.erp01.service.OtherStorageService;
import com.example.erp01.service.OtherTailorService;
import com.example.erp01.service.TailorService;
import com.google.gson.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

@Controller
@RequestMapping(value = "erp")
public class OtherStorageController {

    @Autowired
    private OtherStorageService otherStorageService;

    @Autowired
    private OtherTailorService otherTailorService;
    @Autowired
    private TailorService tailorService;


    @RequestMapping(value = "/minigetotherinstoredatabyid", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> miniGetOtherInStoreDataById(@RequestParam("tailorQcodeID")Integer tailorQcodeID){
        Map<String, Object> map = new LinkedHashMap<>();
        Tailor tailor = tailorService.getTailorByTailorQcodeID(tailorQcodeID, 1);
        if (tailor == null){
            map.put("error", "二维码信息不存在");
            return map;
        }
        List<Tailor> tailorList = tailorService.getTailorByInfo(tailor.getOrderName(), tailor.getColorName(), tailor.getSizeName(), tailor.getPartName(), tailor.getBedNumber(), null, 1, 0);
        map.put("tailorList", tailorList);
        return map;
    }


    @RequestMapping(value = "/miniothercutinstorebatch", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> miniOtherCutInStoreBatch(@RequestParam("tailorQcodeID")Integer tailorQcodeID,
                                                        @RequestParam("otherCutStoreLocation")String otherCutStoreLocation){
        Map<String, Object> map = new LinkedHashMap<>();
        Tailor tailor = tailorService.getTailorByTailorQcodeID(tailorQcodeID, 1);
        List<OtherStorage> otherStorageList = new ArrayList<>();
        List<Tailor> tailorList = tailorService.getTailorByInfo(tailor.getOrderName(), tailor.getColorName(), tailor.getSizeName(), tailor.getPartName(), tailor.getBedNumber(), null, 1, 0);
        for (Tailor tailor1 : tailorList){
            OtherStorage otherStorage = new OtherStorage(otherCutStoreLocation, tailor1.getOrderName(), tailor1.getClothesVersionNumber(), tailor1.getBedNumber(), tailor1.getColorName(), tailor1.getSizeName(), tailor1.getLayerCount(), tailor1.getPackageNumber(), tailor1.getPartName(), tailor1.getTailorQcodeID());
            otherStorageList.add(otherStorage);
        }
        int res = otherStorageService.otherInStore(otherStorageList);
        if (res == 0){
            map.put("inStoreCount", tailorList.size());
        } else {
            map.put("error", "入库失败");
        }
        return map;
    }

    @RequestMapping(value = "/miniotherinstore", method = RequestMethod.POST)
    @ResponseBody
    public int miniOtherInStore(@RequestParam("otherInStoreJson")String otherInStoreJson){
        JsonParser jsonParser = new JsonParser();
        int result = 0;
        try{
            JsonObject json = (JsonObject) jsonParser.parse(otherInStoreJson);
            String otherStoreLocation = json.get("otherStoreLocation").getAsString();
            JsonArray jsonTailorQcode = json.get("tailorQcode").getAsJsonArray();
            Iterator iterator = jsonTailorQcode.iterator();
            List<OtherStorage> otherStorageList = new ArrayList<>();
            while(iterator.hasNext()){
                JsonPrimitive next = (JsonPrimitive) iterator.next();
                Integer tmpID = Integer.parseInt(next.toString().replace("\"",""));
                OtherTailor otherTailor = otherTailorService.getOtherTailorByTailorQcodeID(tmpID);
                if (null == otherTailor){
                    continue;
                } else{
                    OtherStorage otherStorage = new OtherStorage(otherStoreLocation, otherTailor.getOrderName(), otherTailor.getClothesVersionNumber(), otherTailor.getBedNumber(), otherTailor.getColorName(), otherTailor.getSizeName(), otherTailor.getLayerCount(), otherTailor.getPackageNumber(), otherTailor.getPartName(), otherTailor.getTailorQcodeID());
                    otherStorageList.add(otherStorage);
                }
            }
            return otherStorageService.otherInStore(otherStorageList);
        }catch (JsonIOException e){
            e.printStackTrace();
        }catch (JsonSyntaxException e){
            e.printStackTrace();
        }
        return result;
    }

    @RequestMapping(value = "/miniotheroutstore", method = RequestMethod.POST)
    @ResponseBody
    public int miniOtherOutStore(@RequestParam("otherOutStoreJson")String otherOutStoreJson){
        JsonParser jsonParser = new JsonParser();
        int result = 1;
        try{
            JsonObject json = (JsonObject) jsonParser.parse(otherOutStoreJson);
            String groupName = json.get("groupName").getAsString();
            JsonArray jsonTailorQcode = json.get("tailorQcode").getAsJsonArray();
            Iterator iterator = jsonTailorQcode.iterator();
            List<OtherStorage> otherStorageList = new ArrayList<>();
            while(iterator.hasNext()){
                JsonPrimitive next = (JsonPrimitive) iterator.next();
                Integer tmpID = Integer.parseInt(next.toString().replace("\"",""));
                OtherTailor otherTailor = otherTailorService.getOtherTailorByTailorQcodeID(tmpID);
                if (null == otherTailor){
                    continue;
                } else{
                    OtherStorage otherStorage = new OtherStorage(otherTailor.getOrderName(), otherTailor.getClothesVersionNumber(), otherTailor.getBedNumber(), otherTailor.getColorName(), otherTailor.getSizeName(), otherTailor.getLayerCount(), otherTailor.getPackageNumber(), otherTailor.getPartName(), otherTailor.getTailorQcodeID(), groupName);
                    otherStorageList.add(otherStorage);
                }
            }
            return otherStorageService.otherOutStore(otherStorageList);
        }catch (JsonIOException e){
            e.printStackTrace();
        }catch (JsonSyntaxException e){
            e.printStackTrace();
        }
        return result;
    }


    @RequestMapping(value = "/miniotheroutstorebatch", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> miniOtherOutStore(OtherOutDto otherOutDto){
        List<Tailor> tailorList = tailorService.getTailorsByTailorQcodeIDs(otherOutDto.getTailorQcodeIDList(), 1);
        Map<String, Object> map = new LinkedHashMap<>();
        List<OtherStorage> otherStorageList = new ArrayList<>();
        for (Tailor tailor : tailorList){
            OtherStorage otherStorage = new OtherStorage(tailor.getOrderName(), tailor.getClothesVersionNumber(), tailor.getBedNumber(), tailor.getColorName(), tailor.getSizeName(), tailor.getLayerCount(), tailor.getPackageNumber(), tailor.getPartName(), tailor.getTailorQcodeID(), otherOutDto.getGroupName());
            otherStorageList.add(otherStorage);
        }
        int res = otherStorageService.otherOutStore(otherStorageList);
        if (res == 0){
            map.put("outStoreCount", tailorList.size());
        } else {
            map.put("error", "出库失败");
        }
        return map;
    }

    @RequestMapping(value = "/minisearchotherstorage", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> miniSearchOtherStorage(@RequestParam("orderName")String orderName,
                                                      @RequestParam("partName")String partName,
                                                      @RequestParam("colorName")String colorName,
                                                      @RequestParam("sizeName")String sizeName){
        Map<String, Object> map = new LinkedHashMap<>();
        if ("".equals(partName)){
            partName = null;
        }
        if ("".equals(colorName)){
            colorName = null;
        }
        if ("".equals(sizeName)){
            sizeName = null;
        }
        List<Tailor> tailorList = tailorService.getTailorByInfo(orderName, colorName, sizeName, partName, null, null, 1, 0);
        List<OtherStorage> otherInStorageList = otherStorageService.searchOtherInStorage(orderName, partName, colorName, sizeName);
        List<OtherStorage> otherOutStorageList = otherStorageService.searchOtherOutStorage(orderName, partName, colorName, sizeName);
        List<OtherStorage> otherStorageList = new ArrayList<>();
        String isIn;
        String isOut;
        int tailorCount = 0;
        int inCount = 0;
        int outCount = 0;
        for (Tailor tailor : tailorList){
            isIn = "否";
            isOut = "否";
            tailorCount += tailor.getLayerCount();
            for (OtherStorage otherInStorage : otherInStorageList){
                if (tailor.getTailorQcodeID().equals(otherInStorage.getTailorQcodeID())){
                    isIn = otherInStorage.getOtherStoreLocation();
                    inCount += otherInStorage.getLayerCount();
                }
            }
            for (OtherStorage otherOutStorage : otherOutStorageList){
                if (tailor.getTailorQcodeID().equals(otherOutStorage.getTailorQcodeID())){
                    isOut = otherOutStorage.getIsOut();
                    outCount += otherOutStorage.getLayerCount();
                }
            }
            OtherStorage storage = new OtherStorage(tailor.getBedNumber(), tailor.getColorName(), tailor.getSizeName(), tailor.getLayerCount(), tailor.getPackageNumber(), tailor.getPartName(), isIn, isOut);
            otherStorageList.add(storage);
        }
        map.put("otherStorageSize", otherStorageList.size());
        if(otherStorageList.size()>1000) {
            otherStorageList = otherStorageList.subList(0,1000);
        }
        map.put("otherStorageList", otherStorageList);
        map.put("tailorCount", tailorCount);
        map.put("inCount", inCount);
        map.put("outCount", outCount);
        return map;
    }
}
