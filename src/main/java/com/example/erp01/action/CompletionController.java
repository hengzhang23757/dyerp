//package com.example.erp01.action;
//
//import com.example.erp01.model.Completion;
//import com.example.erp01.model.CompletionCount;
//import com.example.erp01.service.*;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.ResponseBody;
//
//import java.util.*;
//
//@Controller
//@RequestMapping(value = "erp")
//public class CompletionController {
//
//    @Autowired
//    private OrderClothesService orderClothesService;
//
//    @Autowired
//    private TailorService tailorService;
//
//    @Autowired
//    private EmbOutStoreService embOutStoreService;
//
//    @Autowired
//    private HangSalaryService hangSalaryService;
//
//    @Autowired
//    private PieceWorkService pieceWorkService;
//
//    @RequestMapping("/completionStart")
//    public String completionStart(Model model){
//        model.addAttribute("bigMenuTag",12);
//        model.addAttribute("menuTag",122);
//        return "report/completion";
//    }
//
////    @RequestMapping(value = "/getcompletionreport",method = RequestMethod.GET)
////    @ResponseBody
////    public Map<String, Object> getCompletionReport(){
////        Map<String, Object> map = new LinkedHashMap<>();
////        List<Completion> completionList1 = orderClothesService.getOrderCompletion();
////        List<Completion> completionList2 = tailorService.getTailorCompletion();
////        List<CompletionCount> completionList3 = embOutStoreService.getEmbOutCompletion();
////        List<CompletionCount> completionList4 = hangSalaryService.getHangCompletion(25);
////        List<CompletionCount> completionList5 = hangSalaryService.getHangCompletion(555);
////        List<CompletionCount> completionList6 = pieceWorkService.getPieceWorkCompletion(556);
////        List<CompletionCount> completionList7 = pieceWorkService.getPieceWorkCompletion(557);
////        List<CompletionCount> completionList8 = pieceWorkService.getPieceWorkCompletion(564);
////        List<Completion> completionList = new ArrayList<>();
////        for (Completion completion2 : completionList2){
////            boolean flag = true;
////            String tmpOrderName = completion2.getOrderName();
////            String tmpClothesVersionNumber = orderClothesService.getVersionNumberByOrderName(tmpOrderName);
////            completion2.setClothesVersionNumber(tmpClothesVersionNumber);
////            completion2.setSeason(orderClothesService.getSeasonByOrder(tmpOrderName));
////            completion2.setWellPercent((double) completion2.getWellTailorCount()/completion2.getTailorCount());
////            for (Completion completion1 : completionList1){
////                if (completion2.getOrderName().equals(completion1.getOrderName())){
////                    completion2.setOrderCount(completion1.getOrderCount());
////                }
////            }
////            for (CompletionCount completionCount3 : completionList3){
////                if (completion2.getOrderName().equals(completionCount3.getOrderName())){
////                    if (completionCount3.getCountNumber() > 0){
////                        completion2.setEmbCount(completionCount3.getCountNumber());
////                    }
////                }
////            }
////            for (CompletionCount completionCount4 : completionList4){
////                if (completion2.getOrderName().equals(completionCount4.getOrderName())){
////                    completion2.setUpCount(completionCount4.getCountNumber());
////                }
////            }
////            int tmpInspectCount = 0;
////            for (CompletionCount completionCount5 : completionList5){
////                if (completion2.getOrderName().equals(completionCount5.getOrderName())){
////                    tmpInspectCount += completionCount5.getCountNumber();
////                }
////            }
////            for (CompletionCount completionCount6 : completionList6){
////                if (completion2.getOrderName().equals(completionCount6.getOrderName())){
////                    tmpInspectCount += completionCount6.getCountNumber();
////                }
////            }
////            completion2.setInspectCount(tmpInspectCount);
////            for (CompletionCount completionCount7 : completionList7){
////                if (completion2.getOrderName().equals(completionCount7.getOrderName())){
////                    completion2.setPressCount(completionCount7.getCountNumber());
////                }
////            }
////            for (CompletionCount completionCount8 : completionList8){
////                if (completion2.getOrderName().equals(completionCount8.getOrderName())){
////                    completion2.setPackageCount(completionCount8.getCountNumber());
////                    completion2.setCompletePercent((double) completionCount8.getCountNumber()/completion2.getOrderCount());
////                }
////            }
////            if (completion2.getOrderCount() > 0){
////                completionList.add(completion2);
////            }
////        }
//////        completionList = completionList.stream().sorted(Comparator.comparing(Completion::getCompletePercent)).collect(Collectors.toList());
////        map.put("completionList",completionList);
////        return map;
////
////    }
//
//}
