package com.example.erp01.action;

import com.example.erp01.model.*;
import com.example.erp01.service.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;
import java.util.stream.Collectors;

@Controller
@RequestMapping(value = "erp")
public class ManufactureAccessoryController {

    private static final Logger log = LoggerFactory.getLogger(ManufactureAccessoryController.class);

    @Autowired
    private ManufactureAccessoryService manufactureAccessoryService;
    @Autowired
    private OrderClothesService orderClothesService;
    @Autowired
    private ManufactureOrderService manufactureOrderService;
    @Autowired
    private TailorService tailorService;
    @Autowired
    private ManufactureFabricService manufactureFabricService;
    @Autowired
    private AccessoryPreStoreService accessoryPreStoreService;
    @Autowired
    private MessageService messageService;

    @RequestMapping("/fabricAccessoryPreCheckStart")
    public String fabricAccessoryPreCheckStart(){
        return "basicInfo/fabricAccessoryPreCheck";
    }


    @RequestMapping(value = "/addmanufactureaccessorybatch", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> addManufactureAccessoryBatch(@RequestParam("manufactureAccessoryJson")String manufactureAccessoryJson,
                                                            @RequestParam("orderName")String orderName,
                                                            @RequestParam(value = "userName", required = false)String userName){
        Map<String, Object> map = new LinkedHashMap<>();
        log.warn("添加辅料："+manufactureAccessoryJson+"---操作员工："+userName);
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        List<ManufactureAccessory> manufactureAccessoryList = gson.fromJson(manufactureAccessoryJson,new TypeToken<List<ManufactureAccessory>>(){}.getType());
        int maxAccessoryKey = manufactureAccessoryService.getMaxAccessoryKey(orderName, 0);
        for (ManufactureAccessory manufactureAccessory : manufactureAccessoryList){
            manufactureAccessory.setAccessoryKey(maxAccessoryKey + manufactureAccessory.getAccessoryKey());
            manufactureAccessory.setCheckState("未提交");
            manufactureAccessory.setPreCheck("未提交");
        }
        int res = manufactureAccessoryService.addManufactureAccessoryBatch(manufactureAccessoryList);
        updateMessageAfterDoSomething(orderName, "辅料");
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/getmanufactureaccessorybyorder", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getManufactureAccessoryByOrder(@RequestParam("orderName")String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<ManufactureAccessory> manufactureAccessoryList = manufactureAccessoryService.getManufactureAccessoryByOrder(orderName, null);
        for (ManufactureAccessory manufactureAccessory : manufactureAccessoryList){
            manufactureAccessory.setAccessoryOrderCount(manufactureAccessory.getOrderPieceUsage() * manufactureAccessory.getOrderCount());
            manufactureAccessory.setOrderSumMoney(manufactureAccessory.getOrderPrice() * manufactureAccessory.getAccessoryOrderCount());
        }
        map.put("data", manufactureAccessoryList);
        map.put("count",manufactureAccessoryList.size());
        map.put("code",0);
        return map;
    }

    @RequestMapping(value = "/getbjbyorder", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getBjByOrder(@RequestParam("orderName")String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<ManufactureAccessory> manufactureAccessoryList = manufactureAccessoryService.getBjByOrder(orderName, "扁机", 0);
        for (ManufactureAccessory manufactureAccessory : manufactureAccessoryList){
            manufactureAccessory.setAccessoryOrderCount(manufactureAccessory.getOrderPieceUsage() * manufactureAccessory.getOrderCount());
            manufactureAccessory.setOrderSumMoney(manufactureAccessory.getOrderPrice() * manufactureAccessory.getAccessoryOrderCount());
        }
        map.put("data", manufactureAccessoryList);
        map.put("count",manufactureAccessoryList.size());
        map.put("code",0);
        return map;
    }

    @RequestMapping(value = "/getmanufactureaccessorybyorderforinstore", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getManufactureAccessoryByOrderForInStore(@RequestParam("orderName")String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<ManufactureAccessory> manufactureAccessoryList = manufactureAccessoryService.getManufactureAccessoryByOrder(orderName, null);
        List<String> fabricNumberList = new ArrayList<>();
        for (ManufactureAccessory manufactureAccessory : manufactureAccessoryList){
            fabricNumberList.add(manufactureAccessory.getAccessoryNumber());
        }
        List<AccessoryPreStore> accessoryPreStoreList = accessoryPreStoreService.getAccessoryStorageByAccessoryNumberList(fabricNumberList);
        for (ManufactureAccessory manufactureAccessory : manufactureAccessoryList){
            for (AccessoryPreStore accessoryPreStore : accessoryPreStoreList){
                if (manufactureAccessory.getAccessoryNumber() != null){
                    if (manufactureAccessory.getAccessoryNumber().equals(accessoryPreStore.getAccessoryNumber())){
                        manufactureAccessory.setShareStorage(accessoryPreStore.getStorageCount() > manufactureAccessory.getShareStorage() ? accessoryPreStore.getStorageCount() : manufactureAccessory.getShareStorage());
                    }
                }
            }
        }
        map.put("data", manufactureAccessoryList);
        map.put("count",manufactureAccessoryList.size());
        map.put("code",0);
        return map;
    }

    @RequestMapping(value = "/getbjbyorderforinstore", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getBjByOrderForInStore(@RequestParam("orderName")String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<ManufactureAccessory> manufactureAccessoryList = manufactureAccessoryService.getBjByOrder(orderName, "扁机", 0);
        map.put("data", manufactureAccessoryList);
        map.put("count",manufactureAccessoryList.size());
        map.put("code",0);
        return map;
    }

    @RequestMapping(value = "/deletemanufactureaccessorybyorder", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> deleteManufactureAccessoryByName(@RequestParam("orderName")String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        int res = manufactureAccessoryService.deleteManufactureAccessoryByName(orderName);
        map.put("result", res);
        return map;
    }


    @RequestMapping(value = "/deletemanufactureaccessorybatch", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> deleteManufactureAccessoryBatch(@RequestParam("accessoryIDList")List<Integer> accessoryIDList){
        Map<String, Object> map = new LinkedHashMap<>();
        int res = manufactureAccessoryService.deleteManufactureAccessoryBatch(accessoryIDList);
        map.put("result", res);
        return map;
    }


    @RequestMapping(value = "/updatemanufactureaccessoryaftercommit", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> updateManufactureAccessoryAfterCommit(@RequestParam("manufactureAccessoryJson")String manufactureAccessoryJson){
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        List<ManufactureAccessory> manufactureAccessoryList = gson.fromJson(manufactureAccessoryJson,new TypeToken<List<ManufactureAccessory>>(){}.getType());
        int res = manufactureAccessoryService.updateAccessoryAfterCommit(manufactureAccessoryList);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/getoneyearmanufactureaccessorylay", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getOneYearManufactureAccessoryLay(@RequestParam(value = "orderName", required = false)String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<OrderClothes> orderClothesList = orderClothesService.getOneYearOrderSummary(orderName);
        List<Completion> completionList = tailorService.getTailorCompletion(orderName, null, 0);
        List<AccessoryReturn> accessoryReturnList = new ArrayList<>();
        for (OrderClothes orderClothes : orderClothesList){
            AccessoryReturn accessoryReturn = new AccessoryReturn();
            accessoryReturn.setOrderName(orderClothes.getOrderName());
            accessoryReturn.setClothesVersionNumber(orderClothes.getClothesVersionNumber());
            accessoryReturn.setOrderCount(orderClothes.getOrderSum());
            for (Completion completion : completionList){
                if (completion.getOrderName().equals(orderClothes.getOrderName())){
                    accessoryReturn.setWellCount(completion.getWellTailorCount());
                }
            }
            accessoryReturnList.add(accessoryReturn);
        }
        map.put("data", accessoryReturnList);
        map.put("code", 0);
        map.put("count", accessoryReturnList.size());
        return map;
    }

    @RequestMapping(value = "/deletemanufactureaccessorybyid", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> deleteManufactureAccessoryByID(@RequestParam("accessoryID")Integer accessoryID){
        Map<String, Object> map = new LinkedHashMap<>();
        int res = manufactureAccessoryService.deleteManufactureAccessoryByID(accessoryID);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/updatemanufactureaccessory", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> updateManufactureAccessory(ManufactureAccessory manufactureAccessory){
        Map<String, Object> map = new LinkedHashMap<>();
        int res = manufactureAccessoryService.updateManufactureAccessory(manufactureAccessory);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/getmanufactureaccessoryhintbyname", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getManufactureAccessoryHintByName(@RequestParam("subAccessoryName")String subAccessoryName
            ,@RequestParam("page")Integer page,@RequestParam("limit")Integer limit){
        Map<String, Object> map = new LinkedHashMap<>();
        List<ManufactureAccessory> countList = manufactureAccessoryService.getManufactureAccessoryHintByName(subAccessoryName,null,null);
        List<ManufactureAccessory> manufactureAccessoryList = manufactureAccessoryService.getManufactureAccessoryHintByName(subAccessoryName,(page-1)*limit,limit);
        map.put("data",manufactureAccessoryList);
        map.put("msg","");
        map.put("count",countList.size());
        map.put("code",0);
        return map;
    }

    @RequestMapping(value = "/getmanufactureaccessoryhintbynumber", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getManufactureAccessoryHintByNumber(@RequestParam("subAccessoryNumber")String subAccessoryNumber
            ,@RequestParam("page")Integer page,@RequestParam("limit")Integer limit){
        Map<String, Object> map = new LinkedHashMap<>();
        List<ManufactureAccessory> countList = manufactureAccessoryService.getManufactureAccessoryHintByNumber(subAccessoryNumber,null,null);
        List<ManufactureAccessory> manufactureAccessoryList = manufactureAccessoryService.getManufactureAccessoryHintByNumber(subAccessoryNumber,(page-1)*limit,limit);
        map.put("data",manufactureAccessoryList);
        map.put("msg","");
        map.put("count",countList.size());
        map.put("code",0);
        return map;
    }

    @RequestMapping(value = "/addmanufactureaccessoryplaceorderbatch", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> addPlaceOrderBatch(@RequestParam("manufactureAccessoryJson")String manufactureAccessoryJson,
                                                  @RequestParam(value = "userName", required = false)String userName){
        Map<String, Object> map = new LinkedHashMap<>();
        log.warn("辅料下单："+manufactureAccessoryJson+"---操作员工："+userName);
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        List<ManufactureAccessory> manufactureAccessoryList = gson.fromJson(manufactureAccessoryJson,new TypeToken<List<ManufactureAccessory>>(){}.getType());
        int res = manufactureAccessoryService.updateManufactureAccessoryOnPlaceOrder(manufactureAccessoryList);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/getmanufactureaccessoryplaceorderinfo", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getManufactureAccessoryPlaceOrderInfo(@RequestParam("orderName")String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> supplierList = manufactureAccessoryService.getDistinctAccessorySupplier();
        List<ManufactureAccessory> manufactureAccessoryList = manufactureAccessoryService.getManufactureAccessoryByOrderCheckState(orderName, "未提交");
        for (ManufactureAccessory manufactureAccessory : manufactureAccessoryList){
            if (manufactureAccessory.getColorName().equals("通用") && manufactureAccessory.getSizeName().equals("通用")){
                int orderCount = orderClothesService.getOrderTotalCount(orderName);
                manufactureAccessory.setOrderCount(orderCount);
            } else if (manufactureAccessory.getColorName().equals("通用")){
                List<String> sizeList = Arrays.asList(manufactureAccessory.getSizeName().split(","));
                int orderCount = orderClothesService.getOrderCountByColorSizeList(orderName, null, sizeList);
                manufactureAccessory.setOrderCount(orderCount);
            } else if (manufactureAccessory.getSizeName().equals("通用")){
                List<String> colorList = Arrays.asList(manufactureAccessory.getColorName().split(","));
                int orderCount = orderClothesService.getOrderCountByColorSizeList(orderName, colorList, null);
                manufactureAccessory.setOrderCount(orderCount);
            } else {
                List<String> colorList = Arrays.asList(manufactureAccessory.getColorName().split(","));
                List<String> sizeList = Arrays.asList(manufactureAccessory.getSizeName().split(","));
                int orderCount = orderClothesService.getOrderCountByColorSizeList(orderName, colorList, sizeList);
                manufactureAccessory.setOrderCount(orderCount);
            }
        }
        map.put("manufactureAccessoryList", manufactureAccessoryList);
        map.put("supplierList", supplierList);
        return map;
    }

    @RequestMapping(value = "/getmanufactureaccessoryplaceorderinfoorderlist", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getManufactureAccessoryPlaceOrderInfoOrderList(@RequestParam("orderNameList")List<String> orderNameList){
        Map<String, Object> map = new LinkedHashMap<>();
        List<ManufactureAccessory> destManufactureAccessoryList = new ArrayList<>();
        List<String> supplierList = manufactureAccessoryService.getDistinctAccessorySupplier();
        List<ManufactureAccessory> manufactureAccessoryList = manufactureAccessoryService.getManufactureAccessoryByOrderCheckStateOrderList(orderNameList, null, "未提交");
        for (ManufactureAccessory manufactureAccessory : manufactureAccessoryList){
            if (manufactureAccessory.getColorName().equals("通用") && manufactureAccessory.getSizeName().equals("通用")){
                int orderCount = orderClothesService.getOrderTotalCount(manufactureAccessory.getOrderName());
                manufactureAccessory.setOrderCount(orderCount);
            } else if (manufactureAccessory.getColorName().equals("通用")){
                List<String> sizeList = Arrays.asList(manufactureAccessory.getSizeName().split(","));
                int orderCount = orderClothesService.getOrderCountByColorSizeList(manufactureAccessory.getOrderName(), null, sizeList);
                manufactureAccessory.setOrderCount(orderCount);
            } else if (manufactureAccessory.getSizeName().equals("通用")){
                List<String> colorList = Arrays.asList(manufactureAccessory.getColorName().split(","));
                int orderCount = orderClothesService.getOrderCountByColorSizeList(manufactureAccessory.getOrderName(), colorList, null);
                manufactureAccessory.setOrderCount(orderCount);
            } else {
                List<String> colorList = Arrays.asList(manufactureAccessory.getColorName().split(","));
                List<String> sizeList = Arrays.asList(manufactureAccessory.getSizeName().split(","));
                int orderCount = orderClothesService.getOrderCountByColorSizeList(manufactureAccessory.getOrderName(), colorList, sizeList);
                manufactureAccessory.setOrderCount(orderCount);
            }
            if (manufactureAccessory.getOrderCount() > 0){
                destManufactureAccessoryList.add(manufactureAccessory);
            }
        }
        map.put("manufactureAccessoryList", destManufactureAccessoryList);
        map.put("supplierList", supplierList);
        return map;
    }

    @RequestMapping(value = "/getbjplaceorderinfoorderlist", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getBjPlaceOrderInfoOrderList(@RequestParam("orderNameList")List<String> orderNameList){
        Map<String, Object> map = new LinkedHashMap<>();
        List<ManufactureAccessory> destManufactureAccessoryList = new ArrayList<>();
        List<String> supplierList = manufactureAccessoryService.getDistinctAccessorySupplier();
        List<ManufactureAccessory> manufactureAccessoryList = manufactureAccessoryService.getBjByOrderCheckStateOrderList(orderNameList, "未提交");
        for (ManufactureAccessory manufactureAccessory : manufactureAccessoryList){
            if (manufactureAccessory.getColorName().equals("通用") && manufactureAccessory.getSizeName().equals("通用")){
                int orderCount = orderClothesService.getOrderTotalCount(manufactureAccessory.getOrderName());
                manufactureAccessory.setOrderCount(orderCount);
            } else if (manufactureAccessory.getColorName().equals("通用")){
                List<String> sizeList = Arrays.asList(manufactureAccessory.getSizeName().split(","));
                int orderCount = orderClothesService.getOrderCountByColorSizeList(manufactureAccessory.getOrderName(), null, sizeList);
                manufactureAccessory.setOrderCount(orderCount);
            } else if (manufactureAccessory.getSizeName().equals("通用")){
                List<String> colorList = Arrays.asList(manufactureAccessory.getColorName().split(","));
                int orderCount = orderClothesService.getOrderCountByColorSizeList(manufactureAccessory.getOrderName(), colorList, null);
                manufactureAccessory.setOrderCount(orderCount);
            } else {
                List<String> colorList = Arrays.asList(manufactureAccessory.getColorName().split(","));
                List<String> sizeList = Arrays.asList(manufactureAccessory.getSizeName().split(","));
                int orderCount = orderClothesService.getOrderCountByColorSizeList(manufactureAccessory.getOrderName(), colorList, sizeList);
                manufactureAccessory.setOrderCount(orderCount);
            }
            if (manufactureAccessory.getOrderCount() > 0){
                destManufactureAccessoryList.add(manufactureAccessory);
            }
        }
        map.put("manufactureAccessoryList", destManufactureAccessoryList);
        map.put("supplierList", supplierList);
        return map;
    }

    @RequestMapping(value = "/getmanufactureaccessorybyid", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getManufactureAccessoryByID(@RequestParam("accessoryID")Integer accessoryID){
        Map<String, Object> map = new LinkedHashMap<>();
        ManufactureAccessory manufactureAccessory =  manufactureAccessoryService.getManufactureAccessoryByID(accessoryID);
        map.put("manufactureAccessory", manufactureAccessory);
        return map;
    }

    @RequestMapping(value = "/getordernamesbyaccessoryinfo", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getOrderNamesByAccessoryInfo(@RequestParam("accessoryName")String accessoryName,
                                                            @RequestParam("specification")String specification,
                                                            @RequestParam("accessoryColor")String accessoryColor){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> orderNameList = manufactureAccessoryService.getOrderNamesByAccessoryInfo(accessoryName, specification, accessoryColor);
        map.put("orderNameList", orderNameList);
        return map;
    }

    @RequestMapping(value = "/getdistinctaccessorysupplier", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getDistinctSupplier(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> supplierList = manufactureAccessoryService.getDistinctAccessorySupplier();
        map.put("supplierList", supplierList);
        return map;
    }

    @RequestMapping(value = "/changeaccessoryorderstateinplaceorder", method = RequestMethod.POST)
    @ResponseBody
    public int changeAccessoryOrderStateInPlaceOrder(@RequestParam("accessoryIDList[]")List<Integer> accessoryIDList){
        return manufactureAccessoryService.changeAccessoryOrderStateInPlaceOrder(accessoryIDList);
    }

    @RequestMapping(value = "/updatemanufactureaccessorybatch", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> updateManufactureFabricBatch(@RequestParam("manufactureAccessoryJson")String manufactureAccessoryJson){
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        List<ManufactureAccessory> manufactureAccessoryList = gson.fromJson(manufactureAccessoryJson,new TypeToken<List<ManufactureAccessory>>(){}.getType());
        int res = manufactureAccessoryService.updateManufactureAccessoryBatch(manufactureAccessoryList);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/changeaccessorycheckstateinplaceorderpre", method = RequestMethod.POST)
    @ResponseBody
    public int changeAccessoryCheckStateInPlaceOrderPre(@RequestParam("accessoryIDList")List<Integer> accessoryIDList,
                                                     @RequestParam(value = "preCheck", required = false)String preCheck,
                                                     @RequestParam(value = "userName", required = false)String userName,
                                                     @RequestParam(value = "checkState", required = false)String checkState){
        log.warn("辅料预审核："+accessoryIDList+"---操作员工："+userName);
        return manufactureAccessoryService.changeAccessoryCheckStateInPlaceOrder(accessoryIDList, preCheck, checkState, userName, null);
    }

    @RequestMapping(value = "/changeaccessorycheckstateinplaceordercheck", method = RequestMethod.POST)
    @ResponseBody
    public int changeAccessoryCheckStateInPlaceOrderCheck(@RequestParam("accessoryIDList")List<Integer> accessoryIDList,
                                                     @RequestParam(value = "preCheck", required = false)String preCheck,
                                                     @RequestParam(value = "userName", required = false)String userName,
                                                     @RequestParam(value = "checkState", required = false)String checkState){
        log.warn("辅料审核："+accessoryIDList+"---操作员工："+userName);
        return manufactureAccessoryService.changeAccessoryCheckStateInPlaceOrder(accessoryIDList, preCheck, checkState, null, userName);
    }


    @RequestMapping(value = "/getaccessorynamehint", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getVersionHint(@RequestParam("keywords")String keywords){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> accessoryNameList = manufactureAccessoryService.getManufactureAccessoryNameHint(keywords);
        map.put("content",accessoryNameList);
        map.put("type","success");
        map.put("code",0);
        return map;
    }

    @RequestMapping(value = "/getfabricaccessoryreviewdata", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getFabricAccessoryReviewData(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<FabricAccessoryReview> accessoryReviewList = manufactureAccessoryService.getManufactureAccessoryReviewState(null);
        List<FabricAccessoryReview> fabricReviewList = manufactureFabricService.getManufactureFabricReviewState(null);
        Set<String> accessoryCheckNumberSet = new HashSet<>();
        Set<String> fabricCheckNumberSet = new HashSet<>();
        for (FabricAccessoryReview fabricAccessoryReview : accessoryReviewList){
            accessoryCheckNumberSet.add(fabricAccessoryReview.getCheckNumber());
        }
        for (FabricAccessoryReview fabricAccessoryReview : fabricReviewList){
            fabricCheckNumberSet.add(fabricAccessoryReview.getCheckNumber());
        }
        List<FabricAccessoryReview> resultList = new ArrayList<>();
        for (String checkNumber : accessoryCheckNumberSet){
            FabricAccessoryReview fabricAccessoryReviewTest = new FabricAccessoryReview();
            Set<String> orderNameSet = new HashSet<>();
            Set<String> clothesVersionNumberSet = new HashSet<>();
            Set<String> materialSet = new HashSet<>();
            Set<String> supplierSet = new HashSet<>();
            Set<String> checkStateSet = new HashSet<>();
            Set<String> orderStateSet = new HashSet<>();
            Set<String> seasonSet = new HashSet<>();
            Set<String> customerNameSet = new HashSet<>();
            for (FabricAccessoryReview fabricAccessoryReview : accessoryReviewList){
                if (checkNumber.equals(fabricAccessoryReview.getCheckNumber())){
                    orderNameSet.add(fabricAccessoryReview.getOrderName());
                    clothesVersionNumberSet.add(fabricAccessoryReview.getClothesVersionNumber());
                    materialSet.add(fabricAccessoryReview.getMaterialName());
                    supplierSet.add(fabricAccessoryReview.getSupplier());
                    checkStateSet.add(fabricAccessoryReview.getCheckState());
                    orderStateSet.add(fabricAccessoryReview.getOrderState());
                }
            }
            List<String> orderNameList = new ArrayList<>(orderNameSet);
            List<ManufactureOrder> manufactureOrderList = manufactureOrderService.getManufactureOrderByOrderNameList(orderNameList);
            for (ManufactureOrder manufactureOrder : manufactureOrderList){
                customerNameSet.add(manufactureOrder.getCustomerName());
                seasonSet.add(manufactureOrder.getSeason());
            }
            fabricAccessoryReviewTest.setCheckNumber(checkNumber);
            fabricAccessoryReviewTest.setOrderName(orderNameSet.toString());
            fabricAccessoryReviewTest.setClothesVersionNumber(clothesVersionNumberSet.toString());
            fabricAccessoryReviewTest.setMaterialName(materialSet.toString());
            fabricAccessoryReviewTest.setSupplier(supplierSet.toString());
            fabricAccessoryReviewTest.setCheckState(checkStateSet.toString());
            fabricAccessoryReviewTest.setReviewType("辅料");
            fabricAccessoryReviewTest.setSeason(seasonSet.toString());
            fabricAccessoryReviewTest.setCustomerName(customerNameSet.toString());
            fabricAccessoryReviewTest.setCheckDate(checkNumber.substring(0, 4) + "-" + checkNumber.substring(4, 6) + "-" + checkNumber.substring(6, 8));
            fabricAccessoryReviewTest.setOrderState(orderStateSet.toString());
            resultList.add(fabricAccessoryReviewTest);
        }
        for (String checkNumber : fabricCheckNumberSet){
            FabricAccessoryReview fabricReview = new FabricAccessoryReview();
            Set<String> orderNameSet = new HashSet<>();
            Set<String> clothesVersionNumberSet = new HashSet<>();
            Set<String> materialSet = new HashSet<>();
            Set<String> supplierSet = new HashSet<>();
            Set<String> checkStateSet = new HashSet<>();
            Set<String> orderStateSet = new HashSet<>();
            Set<String> seasonSet = new HashSet<>();
            Set<String> customerNameSet = new HashSet<>();
            for (FabricAccessoryReview fabricAccessoryReview : fabricReviewList){
                if (checkNumber.equals(fabricAccessoryReview.getCheckNumber())){
                    orderNameSet.add(fabricAccessoryReview.getOrderName());
                    clothesVersionNumberSet.add(fabricAccessoryReview.getClothesVersionNumber());
                    materialSet.add(fabricAccessoryReview.getMaterialName());
                    supplierSet.add(fabricAccessoryReview.getSupplier());
                    checkStateSet.add(fabricAccessoryReview.getCheckState());
                    orderStateSet.add(fabricAccessoryReview.getOrderState());
                }
            }
            List<String> orderNameList = new ArrayList<>(orderNameSet);
            List<ManufactureOrder> manufactureOrderList = manufactureOrderService.getManufactureOrderByOrderNameList(orderNameList);
            for (ManufactureOrder manufactureOrder : manufactureOrderList){
                customerNameSet.add(manufactureOrder.getCustomerName());
                seasonSet.add(manufactureOrder.getSeason());
            }
            fabricReview.setCheckNumber(checkNumber);
            fabricReview.setOrderName(orderNameSet.toString());
            fabricReview.setClothesVersionNumber(clothesVersionNumberSet.toString());
            fabricReview.setMaterialName(materialSet.toString());
            fabricReview.setSupplier(supplierSet.toString());
            fabricReview.setCheckState(checkStateSet.toString());
            fabricReview.setReviewType("面料");
            fabricReview.setCheckDate(checkNumber.substring(0, 4) + "-" + checkNumber.substring(4, 6) + "-" + checkNumber.substring(6, 8));
            fabricReview.setOrderState(orderStateSet.toString());
            fabricReview.setSeason(seasonSet.toString());
            fabricReview.setCustomerName(customerNameSet.toString());
            resultList.add(fabricReview);
        }
        map.put("data", resultList);
        map.put("code", 0);
        map.put("count", resultList.size());
        return map;
    }

    @RequestMapping(value = "/getfabricreviewdata", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getFabricReviewData(@RequestParam(value = "orderName", required = false)String orderName,
                                                   @RequestParam(value = "preCheck", required = false)String preCheck){
        Map<String, Object> map = new LinkedHashMap<>();
        Map<String, Object> param = new HashMap<>();
        param.put("orderName", orderName);
        param.put("preCheck", preCheck);
        param.put("type", 1);
        List<FabricAccessoryReview> fabricReviewList = manufactureFabricService.getManufactureFabricReviewStateCombine(param);
        map.put("data", fabricReviewList);
        map.put("code", 0);
        map.put("count", fabricReviewList.size());
        return map;
    }

    @RequestMapping(value = "/getaccessoryreviewdata", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getAccessoryReviewData(@RequestParam(value = "orderName", required = false)String orderName,
                                                      @RequestParam(value = "preCheck", required = false)String preCheck,
                                                      @RequestParam(value = "checkState", required = false)String checkState){
        Map<String, Object> map = new LinkedHashMap<>();
        Map<String, Object> param = new HashMap<>();
        param.put("orderName", orderName);
        param.put("preCheck", preCheck);
        param.put("checkState", checkState);
        param.put("type", 0);
        if (!StringUtils.isEmpty(orderName) || !StringUtils.isEmpty(preCheck) || !StringUtils.isEmpty(checkState)){
            param.put("type", 1);
        }
        List<FabricAccessoryReview> accessoryReviewList = manufactureAccessoryService.getManufactureAccessoryReviewStateCombine(param);
        map.put("data", accessoryReviewList);
        map.put("code", 0);
        map.put("count", accessoryReviewList.size());
        return map;
    }

    @RequestMapping(value = "/getbjreviewdata", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getBjReviewData(@RequestParam(value = "orderName", required = false)String orderName,
                                               @RequestParam(value = "preCheck", required = false)String preCheck,
                                               @RequestParam(value = "checkState", required = false)String checkState){
        Map<String, Object> map = new LinkedHashMap<>();
        Map<String, Object> param = new HashMap<>();
        param.put("orderName", orderName);
        param.put("preCheck", preCheck);
        param.put("checkState", checkState);
        param.put("type", 1);
        List<FabricAccessoryReview> accessoryReviewList = manufactureAccessoryService.getBjReviewStateCombine(param);
        map.put("data", accessoryReviewList);
        map.put("code", 0);
        map.put("count", accessoryReviewList.size());
        return map;
    }


//    @RequestMapping(value = "/getfabricaccessoryonlyreviewdata", method = RequestMethod.GET)
//    @ResponseBody
//    public Map<String, Object> getFabricAccessoryOnlyReviewData(){
//        Map<String, Object> map = new LinkedHashMap<>();
//        List<FabricAccessoryReview> accessoryReviewList = manufactureAccessoryService.getManufactureAccessoryReviewState("已提交");
//        List<FabricAccessoryReview> fabricReviewList = manufactureFabricService.getManufactureFabricReviewState("已提交");
//        List<AccessoryPreStore> accessoryPreStoreList = accessoryPreStoreService.getAccessoryPreStoreForCheck("已提交");
//        Set<String> accessoryCheckNumberSet = new HashSet<>();
//        Set<String> fabricCheckNumberSet = new HashSet<>();
//        for (FabricAccessoryReview fabricAccessoryReview : accessoryReviewList){
//            accessoryCheckNumberSet.add(fabricAccessoryReview.getCheckNumber());
//        }
//        for (FabricAccessoryReview fabricAccessoryReview : fabricReviewList){
//            fabricCheckNumberSet.add(fabricAccessoryReview.getCheckNumber());
//        }
//        List<FabricAccessoryReview> accessoryResultList = new ArrayList<>();
//        List<FabricAccessoryReview> fabricResultList = new ArrayList<>();
//        for (String checkNumber : accessoryCheckNumberSet){
//            FabricAccessoryReview fabricAccessoryReviewTest = new FabricAccessoryReview();
//            Set<String> orderNameSet = new HashSet<>();
//            Set<String> clothesVersionNumberSet = new HashSet<>();
//            Set<String> materialSet = new HashSet<>();
//            Set<String> supplierSet = new HashSet<>();
//            Set<String> checkStateSet = new HashSet<>();
//            Set<String> orderStateSet = new HashSet<>();
//            Set<String> seasonSet = new HashSet<>();
//            Set<String> customerNameSet = new HashSet<>();
//            for (FabricAccessoryReview fabricAccessoryReview : accessoryReviewList){
//                if (checkNumber.equals(fabricAccessoryReview.getCheckNumber())){
//                    orderNameSet.add(fabricAccessoryReview.getOrderName());
//                    clothesVersionNumberSet.add(fabricAccessoryReview.getClothesVersionNumber());
//                    materialSet.add(fabricAccessoryReview.getMaterialName());
//                    supplierSet.add(fabricAccessoryReview.getSupplier());
//                    checkStateSet.add(fabricAccessoryReview.getCheckState());
//                    orderStateSet.add(fabricAccessoryReview.getOrderState());
//                }
//            }
//            List<String> orderNameList = new ArrayList<>(orderNameSet);
//            List<ManufactureOrder> manufactureOrderList = manufactureOrderService.getManufactureOrderByOrderNameList(orderNameList);
//            for (ManufactureOrder manufactureOrder : manufactureOrderList){
//                customerNameSet.add(manufactureOrder.getCustomerName());
//                seasonSet.add(manufactureOrder.getSeason());
//            }
//            fabricAccessoryReviewTest.setCheckNumber(checkNumber);
//            fabricAccessoryReviewTest.setOrderName(orderNameSet.toString());
//            fabricAccessoryReviewTest.setClothesVersionNumber(clothesVersionNumberSet.toString());
//            fabricAccessoryReviewTest.setMaterialName(materialSet.toString());
//            fabricAccessoryReviewTest.setSupplier(supplierSet.toString());
//            fabricAccessoryReviewTest.setCheckState(checkStateSet.toString());
//            fabricAccessoryReviewTest.setReviewType("辅料");
//            fabricAccessoryReviewTest.setSeason(seasonSet.toString());
//            fabricAccessoryReviewTest.setCustomerName(customerNameSet.toString());
//            fabricAccessoryReviewTest.setCheckDate(checkNumber.substring(0, 4) + "-" + checkNumber.substring(4, 6) + "-" + checkNumber.substring(6, 8));
//            fabricAccessoryReviewTest.setOrderState(orderStateSet.toString());
//            accessoryResultList.add(fabricAccessoryReviewTest);
//        }
//        for (String checkNumber : fabricCheckNumberSet){
//            FabricAccessoryReview fabricReview = new FabricAccessoryReview();
//            Set<String> orderNameSet = new HashSet<>();
//            Set<String> clothesVersionNumberSet = new HashSet<>();
//            Set<String> materialSet = new HashSet<>();
//            Set<String> supplierSet = new HashSet<>();
//            Set<String> checkStateSet = new HashSet<>();
//            Set<String> orderStateSet = new HashSet<>();
//            Set<String> seasonSet = new HashSet<>();
//            Set<String> customerNameSet = new HashSet<>();
//            for (FabricAccessoryReview fabricAccessoryReview : fabricReviewList){
//                if (checkNumber.equals(fabricAccessoryReview.getCheckNumber())){
//                    orderNameSet.add(fabricAccessoryReview.getOrderName());
//                    clothesVersionNumberSet.add(fabricAccessoryReview.getClothesVersionNumber());
//                    materialSet.add(fabricAccessoryReview.getMaterialName());
//                    supplierSet.add(fabricAccessoryReview.getSupplier());
//                    checkStateSet.add(fabricAccessoryReview.getCheckState());
//                    orderStateSet.add(fabricAccessoryReview.getOrderState());
//                }
//            }
//            List<String> orderNameList = new ArrayList<>(orderNameSet);
//            List<ManufactureOrder> manufactureOrderList = manufactureOrderService.getManufactureOrderByOrderNameList(orderNameList);
//            for (ManufactureOrder manufactureOrder : manufactureOrderList){
//                customerNameSet.add(manufactureOrder.getCustomerName());
//                seasonSet.add(manufactureOrder.getSeason());
//            }
//            fabricReview.setCheckNumber(checkNumber);
//            fabricReview.setOrderName(orderNameSet.toString());
//            fabricReview.setClothesVersionNumber(clothesVersionNumberSet.toString());
//            fabricReview.setMaterialName(materialSet.toString());
//            fabricReview.setSupplier(supplierSet.toString());
//            fabricReview.setCheckState(checkStateSet.toString());
//            fabricReview.setReviewType("面料");
//            fabricReview.setCheckDate(checkNumber.substring(0, 4) + "-" + checkNumber.substring(4, 6) + "-" + checkNumber.substring(6, 8));
//            fabricReview.setOrderState(orderStateSet.toString());
//            fabricReview.setSeason(seasonSet.toString());
//            fabricReview.setCustomerName(customerNameSet.toString());
//            fabricResultList.add(fabricReview);
//        }
//        map.put("accessoryReviewList", accessoryResultList);
//        map.put("fabricReviewList", fabricResultList);
//        map.put("accessoryPreStoreList", accessoryPreStoreList);
//        map.put("code", 0);
//        return map;
//    }

    @RequestMapping(value = "/getfabricaccessoryonlyreviewdata", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getFabricAccessoryOnlyReviewData(){
        Map<String, Object> map = new LinkedHashMap<>();
        Map<String, Object> param = new HashMap<>();
        param.put("preCheck", "通过");
        param.put("checkState", "已提交");
        param.put("type", 1);
        List<FabricAccessoryReview> accessoryReviewList = manufactureAccessoryService.getManufactureAccessoryReviewStateCombine(param);
        List<FabricAccessoryReview> bjReviewList = manufactureAccessoryService.getBjReviewStateCombine(param);
        List<FabricAccessoryReview> fabricReviewList = manufactureFabricService.getManufactureFabricReviewStateCombine(param);
        List<AccessoryPreStore> accessoryPreStoreList = accessoryPreStoreService.getAccessoryPreStoreForCheck(param);
        List<String> orderNameList = fabricReviewList.stream().map(FabricAccessoryReview::getOrderName).collect(Collectors.toList());
        List<ManufactureOrder> manufactureOrderList = manufactureOrderService.getManufactureOrderByOrderNameList(orderNameList);
        for (FabricAccessoryReview fabricAccessoryReview : fabricReviewList){
            for (ManufactureOrder manufactureOrder : manufactureOrderList){
                if (fabricAccessoryReview.getOrderName().equals(manufactureOrder.getOrderName())){
                    fabricAccessoryReview.setCustomerName(manufactureOrder.getCustomerName());
                }
            }
        }
        map.put("accessoryReviewList", accessoryReviewList);
        map.put("fabricReviewList", fabricReviewList);
        map.put("bjReviewList", bjReviewList);
        map.put("accessoryPreStoreList", accessoryPreStoreList);
        map.put("code", 0);
        return map;
    }


    @RequestMapping(value = "/getreviewdatabytypechecknumber", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getReviewDataByTypeCheckNumber(@RequestParam("checkNumber")String checkNumber,
                                                              @RequestParam("reviewType")String reviewType,
                                                              @RequestParam(value = "checkState", required = false)String checkState){
        Map<String, Object> map = new LinkedHashMap<>();
        if (reviewType.equals("面料")){
            List<ManufactureFabric> manufactureFabricList = manufactureFabricService.getManufactureFabricByCheckNumberState(checkNumber, checkState);
            map.put("data", manufactureFabricList);
            map.put("code", 0);
            map.put("count", manufactureFabricList.size());
        } else {
            List<ManufactureAccessory> manufactureAccessoryList = manufactureAccessoryService.getManufactureAccessoryByCheckNumberState(checkNumber, checkState);
            map.put("data", manufactureAccessoryList);
            map.put("code", 0);
            map.put("count", manufactureAccessoryList.size());
        }
        return map;
    }


    @RequestMapping(value = "/getprintdatabytypechecknumber", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getPrintDataByTypeCheckNumber(@RequestParam("checkNumber")String checkNumber,
                                                              @RequestParam("reviewType")String reviewType){
        Map<String, Object> map = new LinkedHashMap<>();
        if (reviewType.equals("面料")){
            List<ManufactureFabric> manufactureFabricList = manufactureFabricService.getManufactureFabricByCheckNumberState(checkNumber, null);
            map.put("data", manufactureFabricList);
            map.put("code", 0);
            map.put("count", manufactureFabricList.size());
        } else {
            List<ManufactureAccessory> manufactureAccessoryList = manufactureAccessoryService.getManufactureAccessoryByCheckNumberState(checkNumber, null);
            map.put("data", manufactureAccessoryList);
            map.put("code", 0);
            map.put("count", manufactureAccessoryList.size());
        }
        return map;
    }

    @RequestMapping(value = "/destroycheckbychecknumber", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> destroyCheckByCheckNumber(@RequestParam("checkNumber")String checkNumber,
                                                         @RequestParam("reviewType")String reviewType){
        Map<String, Object> map = new LinkedHashMap<>();
        if (reviewType.equals("面料")){
            int res = manufactureFabricService.destroyFabricCheck(checkNumber);
            map.put("data", res);
            map.put("code", 0);
            map.put("count", 0);
        } else {
            int res = manufactureAccessoryService.destroyAccessoryCheck(checkNumber);
            map.put("data", res);
            map.put("code", 0);
            map.put("count", 0);
        }
        return map;
    }

    @RequestMapping(value = "/getchecknumbercount", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getCheckNumberCount(){
        Map<String, Object> map = new LinkedHashMap<>();
        int accessoryCheckNumberCount = manufactureAccessoryService.getAccessoryCheckNumberCountByState("已提交");
        int fabricCheckNumberCount = manufactureFabricService.getFabricCheckNumberCountByState("已提交");
        int accessoryPreStoreCount = accessoryPreStoreService.getAccessoryPreStoreCheckCount();
        map.put("checkNumberCount", accessoryCheckNumberCount + fabricCheckNumberCount + accessoryPreStoreCount);
        return map;
    }

    @RequestMapping(value = "/updateorderstatebychecknumber", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> updateOrderStateByCheckNumber(@RequestParam("checkNumber")String checkNumber,
                                                             @RequestParam("reviewType")String reviewType){
        Map<String, Object> map = new LinkedHashMap<>();
        if (reviewType.equals("面料")){
            int res = manufactureFabricService.updateFabricOrderStateByCheckNumber(checkNumber);
            map.put("data", res);
            map.put("code", 0);
            map.put("count", 0);
        } else {
            int res = manufactureAccessoryService.updateAccessoryOrderStateByCheckNumber(checkNumber);
            map.put("data", res);
            map.put("code", 0);
            map.put("count", 0);
        }
        return map;
    }

    @RequestMapping(value = "/getaccessorycardinfobyordername", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getCutFabricByOrderName(@RequestParam("orderName")String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> colorList = orderClothesService.getOrderColorNamesByOrder(orderName);
        List<String> fabricNameList = manufactureFabricService.getDistinctFabricNameByOrder(orderName);
        List<String> sizeList = orderClothesService.getOrderSizeNamesByOrder(orderName);
        int orderSum = orderClothesService.getOrderTotalCount(orderName);
        List<String> accessoryNameList = manufactureAccessoryService.getDistinctAccessoryNameByOrder(orderName);
        List<ManufactureAccessory> manufactureAccessoryList = manufactureAccessoryService.getManufactureAccessoryByOrder(orderName, null);
        ManufactureOrder manufactureOrder = manufactureOrderService.getManufactureOrderByOrderName(orderName);
        Set<String> accessoryNameList1 = new HashSet<>(); // 通用辅料
        Set<String> accessoryNameList2 = new HashSet<>(); // 分色辅料
        Set<String> accessoryNameList3 = new HashSet<>(); // 分码辅料
        Set<String> accessoryNameList4 = new HashSet<>(); // 分色分码辅料
        for (ManufactureAccessory manufactureAccessory : manufactureAccessoryList){
            if (manufactureAccessory.getColorName().equals("通用") && manufactureAccessory.getSizeName().equals("通用")){
                accessoryNameList1.add(manufactureAccessory.getAccessoryName());
            } else if (manufactureAccessory.getSizeName().equals("通用")){
                accessoryNameList2.add(manufactureAccessory.getAccessoryName());
            } else if (manufactureAccessory.getColorName().equals("通用")){
                accessoryNameList3.add(manufactureAccessory.getAccessoryName());
            } else {
                accessoryNameList4.add(manufactureAccessory.getAccessoryName());
            }
        }
        map.put("manufactureOrder", manufactureOrder);
        map.put("colorList", colorList);
        map.put("sizeList", sizeList);
        map.put("fabricNameList", fabricNameList);
        map.put("accessoryNameList", accessoryNameList);
        map.put("manufactureAccessoryList1", accessoryNameList1);
        map.put("manufactureAccessoryList2", accessoryNameList2);
        map.put("manufactureAccessoryList3", accessoryNameList3);
        map.put("manufactureAccessoryList4", accessoryNameList4);
        map.put("orderSum", orderSum);
        map.put("code", 0);
        return map;
    }

    @RequestMapping(value = "/getaccessorychecknumberhint", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getFabricCheckNumberHint(@RequestParam("keywords")String keywords){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> checkNumberList = manufactureAccessoryService.getAccessoryCheckNumberHint(keywords);
        map.put("content",checkNumberList);
        map.put("type","success");
        map.put("code",0);
        return map;
    }


    @RequestMapping(value = "/getmanufactureaccessorybychecknumber", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getManufactureAccessoryByCheckNumber(@RequestParam(value = "checkNumber")String checkNumber){
        Map<String, Object> map = new LinkedHashMap<>();
        List<ManufactureAccessory> manufactureAccessoryList = manufactureAccessoryService.getManufactureAccessoryByCheckNumberState(checkNumber, null);
        Set<String> materialSet = new HashSet<>();
        Set<String> accessoryNumberSet = new HashSet<>();
        Set<String> orderNameSet = new HashSet<>();
        Set<String> clothesVersionNumberSet = new HashSet<>();
        Set<String> checkStateSet = new HashSet<>();
        for (ManufactureAccessory manufactureAccessory : manufactureAccessoryList){
            if (manufactureAccessory.getAccessoryNumber() != null){
                accessoryNumberSet.add(manufactureAccessory.getAccessoryNumber());
            }
            materialSet.add(manufactureAccessory.getAccessoryName());
            orderNameSet.add(manufactureAccessory.getOrderName());
            clothesVersionNumberSet.add(manufactureAccessory.getClothesVersionNumber());
            checkStateSet.add(manufactureAccessory.getCheckState());
        }
        ManufactureAccessory manufactureAccessory = new ManufactureAccessory();
        manufactureAccessory.setCheckState(checkStateSet.toString());
        manufactureAccessory.setCheckNumber(checkNumber);
        manufactureAccessory.setOrderName(orderNameSet.toString());
        manufactureAccessory.setClothesVersionNumber(clothesVersionNumberSet.toString());
        manufactureAccessory.setAccessoryName(materialSet.toString());
        manufactureAccessory.setAccessoryNumber(accessoryNumberSet.toString());
        List<ManufactureAccessory> resultList = new ArrayList<>();
        resultList.add(manufactureAccessory);
        map.put("data", resultList);
        map.put("code", 0);
        map.put("count", 1);
        return map;
    }

    @RequestMapping(value = "/getmanufactureaccessorydetailbychecknumber", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getManufactureAccessoryDetailByCheckNumber(@RequestParam(value = "checkNumber")String checkNumber){
        Map<String, Object> map = new LinkedHashMap<>();
        List<ManufactureAccessory> manufactureAccessoryList = manufactureAccessoryService.getManufactureAccessoryByCheckNumberState(checkNumber, null);
        map.put("data", manufactureAccessoryList);
        map.put("code", 0);
        map.put("count", 1);
        return map;
    }


    @RequestMapping(value = "/getmanufactureaccessorykeydata", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getManufactureAccessoryKeyData(@RequestParam(value = "orderName")String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<ManufactureAccessory> manufactureAccessoryList = manufactureAccessoryService.getAccessoryKeyByOrder(orderName, 0, null);
        map.put("data", manufactureAccessoryList);
        map.put("code", 0);
        map.put("count", 1);
        return map;
    }

    @RequestMapping(value = "/updateaccessorycustomerusagebatch", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> updateAccessoryCustomerUsageBatch(@RequestParam("manufactureAccessoryJson")String manufactureAccessoryJson){
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        List<ManufactureAccessory> manufactureAccessoryList = gson.fromJson(manufactureAccessoryJson,new TypeToken<List<ManufactureAccessory>>(){}.getType());
        manufactureAccessoryService.updateAccessoryCustomerUsage(manufactureAccessoryList);
        map.put("result", 0);
        return map;
    }



    private void updateMessageAfterDoSomething(String orderName, String messageType){
        Map<String, Object> param = new HashMap<>();
        param.put("orderName", orderName);
        param.put("messageType", messageType);
        param.put("readType", "未完成");
        List<Message> messageList = messageService.getMessageContentByInfo(param);
        for (Message message : messageList){
            message.setReadType("已完成");
        }
        messageService.updateMessageBatch(messageList);
    }

}
