package com.example.erp01.action;

import com.example.erp01.model.*;
import com.example.erp01.service.*;
import com.google.gson.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;


//衣胚出入库相关操作，出库，入库等
@Controller
@RequestMapping(value = "erp")
public class EmbStorageController {

    @Autowired
    private EmbStorageService embStorageService;
    @Autowired
    private StorageService storageService;
    @Autowired
    private TailorService tailorService;
    @Autowired
    private EmbInStoreService embInStoreService;
    @Autowired
    private EmbStoreService embStoreService;
    @Autowired
    private EmbOutStoreService embOutStoreService;

//    @RequestMapping(value = "/embinstore", method = RequestMethod.POST)
//    @ResponseBody
//    public int embInStore(@RequestParam("embInStoreJson") String embInStoreJson){
//        JsonParser jsonParser = new JsonParser();
//        int result = 1;
//        try{
//            JsonObject json = (JsonObject) jsonParser.parse(embInStoreJson);
//            String embStoreLocation = json.get("embStoreLocation").getAsString();
//            if (embStoreService.getEmbStoreByLocation(embStoreLocation) == null){
//                result = 3;
//                return result;
//            }
//            JsonArray jsonTailorQcode = json.get("tailorQcode").getAsJsonArray();
//            Iterator iterator = jsonTailorQcode.iterator();
//            List<Integer> tailorQcodeList = new ArrayList<>();
//            while(iterator.hasNext()){
//                JsonPrimitive tailorQcode = (JsonPrimitive) iterator.next();
//                tailorQcodeList.add(Integer.parseInt(tailorQcode.toString().replace("\"","")));
//            }
//            List<String> tt = tailorService.getTailorQcodeByTailorQcodeID(tailorQcodeList);
//            if (tt == null || tt.size() == 0){
//                result = 2;
//                return result;
//            }
//            List<EmbStorage> embStorageList = new ArrayList<>();
//            List<String> subQcodeList = new ArrayList<>();
//            for (String tmpQcode : tt){
//                String tmpSubQcode = tmpQcode.substring(0,tmpQcode.lastIndexOf("-"));
//                subQcodeList.add(tmpSubQcode);
//                String[] subQcode = tmpQcode.split("-");
//                String orderName = subQcode[0];
//                String bedNumber = subQcode[2];
//                String colorName = subQcode[4];
//                String sizeName = subQcode[5];
//                String layerCount = subQcode[6];
//                String packageNumber = subQcode[7];
//                EmbStorage tmpEmbStorage = new EmbStorage(embStoreLocation,orderName,Integer.parseInt(bedNumber),colorName,sizeName,Integer.parseInt(layerCount),Integer.parseInt(packageNumber),tmpQcode);
//                embStorageList.add(tmpEmbStorage);
//            }
//            int res1 = storageService.outStore(subQcodeList);
//            int res2 = embStorageService.embInStore(embStorageList);
//            int res3 = embInStoreService.addEmbInStore(embStorageList);
//            if (0 == res1 && 0 == res2 && 0 == res3){
//                result = 0;
//            }else{
//                result = 1;
//            }
//        }catch (JsonIOException e){
//            e.printStackTrace();
//        }catch (JsonSyntaxException e){
//            e.printStackTrace();
//        }
//        return result;
//
//    }

    @RequestMapping(value = "/allEmbStorageStart")
    public String allEmbStorageStart(){
        return "embMarket/allEmbStorage";
    }

    @RequestMapping(value = "/getAllEmbStorage", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> getAllEmbStorage(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<EmbStorage> embStorageList = embStorageService.getAllEmbStorage();
        map.put("data", embStorageList);
        map.put("code", 0);
        map.put("count", embStorageList.size());
        return map;
    }



    @RequestMapping(value = "/miniembinstore", method = RequestMethod.POST)
    @ResponseBody
    public int miniEmbInStore(@RequestParam("embInStoreJson") String embInStoreJson){
        JsonParser jsonParser = new JsonParser();
        int result = 1;
        try{
            JsonObject json = (JsonObject) jsonParser.parse(embInStoreJson);
            String embStoreLocation = json.get("embStoreLocation").getAsString();
            JsonArray tailors = json.get("tailors").getAsJsonArray();
            Iterator iterator = tailors.iterator();
            List<Integer> tailorQcodeList = new ArrayList<>();
            while(iterator.hasNext()){
                JsonObject tailor = (JsonObject) iterator.next();
                if(!"".equals(tailor.get("tailorQcodeID").getAsString())) {
                    tailorQcodeList.add(tailor.get("tailorQcodeID").getAsInt());
                }
            }
            List<Tailor> tailorList = tailorService.getTailorsByTailorQcodeIDs(tailorQcodeList, 0);
            if (tailorList == null || tailorList.size() == 0){
                result = 2;
                return result;
            }
            List<EmbStorage> embStorageList = new ArrayList<>();
            for (Tailor tailor : tailorList){
                storageService.deleteStorageByInfo(tailor.getOrderName(), tailor.getBedNumber(), tailor.getPackageNumber());
                EmbStorage tmpEmbStorage = new EmbStorage(embStoreLocation,tailor.getOrderName(),tailor.getBedNumber(),tailor.getColorName(),tailor.getSizeName(),tailor.getLayerCount(),tailor.getPackageNumber(),tailor.getTailorQcode());
                embStorageList.add(tmpEmbStorage);
            }
            int res2 = embStorageService.embInStore(embStorageList);
            int res3 = embInStoreService.addEmbInStore(embStorageList);
            if (0 == res2 && 0 == res3){
                result = 0;
            }else{
                result = 1;
            }
        }catch (JsonIOException e){
            e.printStackTrace();
        }catch (JsonSyntaxException e){
            e.printStackTrace();
        }
        return result;

    }


    @RequestMapping(value = "/miniembinstorescaner", method = RequestMethod.POST)
    @ResponseBody
    public int miniEmbInStoreScaner(@RequestParam("embInStoreJson") String embInStoreJson){
        JsonParser jsonParser = new JsonParser();
        System.out.println(embInStoreJson);
        int result = 1;
        try{
            JsonObject json = (JsonObject) jsonParser.parse(embInStoreJson);
            String embStoreLocation = json.get("embStoreLocation").getAsString();
            JsonArray tailors = json.get("tailors").getAsJsonArray();
            Iterator iterator = tailors.iterator();
            List<Integer> tailorQcodeList = new ArrayList<>();
            while(iterator.hasNext()){
                JsonObject tailor = (JsonObject) iterator.next();
                if(!"".equals(tailor.get("tailorQcodeID").getAsString())) {
                    if (tailor.get("tailorQcodeID").getAsString().length() == 9) {
                        tailorQcodeList.add(tailor.get("tailorQcodeID").getAsInt());
                    } else if (tailor.get("tailorQcodeID").getAsString().length() == 18) {
                        Integer tailorQcodeIDOne = Integer.parseInt(tailor.get("tailorQcodeID").toString().substring(0, 9));
                        Integer tailorQcodeIDTwo = Integer.parseInt(tailor.get("tailorQcodeID").toString().substring(9));
                        if (!tailorQcodeList.contains(tailorQcodeIDOne)) {
                            tailorQcodeList.add(tailorQcodeIDOne);
                        }
                        if (!tailorQcodeList.contains(tailorQcodeIDTwo)) {
                            tailorQcodeList.add(tailorQcodeIDTwo);
                        }
                    } else if (tailor.get("tailorQcodeID").getAsString().length() == 27) {
                        Integer tailorQcodeIDOne = Integer.parseInt(tailor.get("tailorQcodeID").toString().substring(0, 9));
                        Integer tailorQcodeIDTwo = Integer.parseInt(tailor.get("tailorQcodeID").toString().substring(9, 18));
                        Integer tailorQcodeIDThree = Integer.parseInt(tailor.get("tailorQcodeID").toString().substring(18));
                        if (!tailorQcodeList.contains(tailorQcodeIDOne)) {
                            tailorQcodeList.add(tailorQcodeIDOne);
                        }
                        if (!tailorQcodeList.contains(tailorQcodeIDTwo)) {
                            tailorQcodeList.add(tailorQcodeIDTwo);
                        }
                        if (!tailorQcodeList.contains(tailorQcodeIDThree)) {
                            tailorQcodeList.add(tailorQcodeIDThree);
                        }
                    } else if (tailor.get("tailorQcodeID").getAsString().length() > 9) {
                        Integer tailorQcodeIDOne = Integer.parseInt(tailor.get("tailorQcodeID").toString().substring(0, 9));
                        if (!tailorQcodeList.contains(tailorQcodeIDOne)) {
                            tailorQcodeList.add(tailorQcodeIDOne);
                        }
                    }
                }
            }
            List<Tailor> tailorList = tailorService.getTailorsByTailorQcodeIDs(tailorQcodeList, 0);
            if (tailorList == null || tailorList.size() == 0){
                result = 2;
                return result;
            }
            List<EmbStorage> embStorageList = new ArrayList<>();
            for (Tailor tailor : tailorList){
                embOutStoreService.deleteEmbOutStoreByInfo(tailor.getOrderName(), tailor.getBedNumber(), tailor.getPackageNumber());
                storageService.deleteStorageByInfo(tailor.getOrderName(), tailor.getBedNumber(), tailor.getPackageNumber());
                EmbStorage tmpEmbStorage = new EmbStorage(embStoreLocation,tailor.getOrderName(),tailor.getBedNumber(),tailor.getColorName(),tailor.getSizeName(),tailor.getLayerCount(),tailor.getPackageNumber(),tailor.getTailorQcode());
                embStorageList.add(tmpEmbStorage);
            }
            int res2 = embStorageService.embInStore(embStorageList);
            int res3 = embInStoreService.addEmbInStore(embStorageList);
            if (0 == res2 && 0 == res3){
                result = 0;
            }else{
                result = 1;
            }
        }catch (JsonIOException e){
            e.printStackTrace();
        }catch (JsonSyntaxException e){
            e.printStackTrace();
        }
        return result;

    }


    @RequestMapping(value = "/miniembinstoresinglebatch", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> miniEmbInStoreSingleBatch(EmbInDto embInDto){
        List<Tailor> tailorList = tailorService.getTailorsByTailorQcodeIDs(embInDto.getTailorQcodeIDList(), 0);
        Map<String, Object> map = new LinkedHashMap<>();
        List<EmbStorage> embStorageList = new ArrayList<>();
        for (Tailor tailor : tailorList){
            embOutStoreService.deleteEmbOutStoreByInfo(tailor.getOrderName(), tailor.getBedNumber(), tailor.getPackageNumber());
            storageService.deleteStorageByInfo(tailor.getOrderName(), tailor.getBedNumber(), tailor.getPackageNumber());
            EmbStorage tmpEmbStorage = new EmbStorage(embInDto.getEmbStoreLocation(),tailor.getOrderName(),tailor.getBedNumber(),tailor.getColorName(),tailor.getSizeName(),tailor.getLayerCount(),tailor.getPackageNumber(),tailor.getTailorQcode());
            embStorageList.add(tmpEmbStorage);
        }
        int res2 = embStorageService.embInStore(embStorageList);
        int res3 = embInStoreService.addEmbInStore(embStorageList);
        if (0 == res2 && 0 == res3){
            map.put("embInStoreCount", tailorList.size());
        }else{
            map.put("error", "入库失败");
        }
        return map;
    }

    /**
     * 进入衣胚库存页面
     * @param model
     * @return
     */
    @RequestMapping("/embStorageStateStart")
    public String embStorageStateStart(Model model){
        model.addAttribute("bigMenuTag",6);
        model.addAttribute("menuTag",64);
        return "embMarket/embStorageState";
    }

//    获取裁片仓库信息
    @RequestMapping(value = "/getembstoragestate",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getEmbStorageState(){
        List<EmbStorageState> embStorageStateList = new ArrayList<>();
        Map<String,Object> map = new HashMap<>();
        embStorageStateList = embStorageService.getEmbStorageState();
        List<StorageFloor> storageFloorList = embStorageService.getEmbStorageFloor();
        map.put("storageFloorList",storageFloorList);
        map.put("embStorageStateList",embStorageStateList);
        return map;
    }

    @RequestMapping(value = "/minigetembstoragestate",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> miniGetEmbStorageState(){
        Map<String,Object> map = new HashMap<>();
        List<EmbStorageState> embStorageStateList = embStorageService.getEmbStorageState();
        List<StorageFloor> storageFloorList = embStorageService.getEmbStorageFloor();
        if(!embStorageStateList.isEmpty()) {
            for(EmbStorageState embStorageState : embStorageStateList) {
                for(StorageFloor storageFloor : storageFloorList) {
                    if(embStorageState.getEmbStoreLocation().startsWith(storageFloor.getLocation())) {
                        embStorageState.setFloor(storageFloor.getNum());
                    }
                }
            }
        }
        map.put("embStorageStateList",embStorageStateList);
        return map;
    }

    /**
     * 进入衣胚查询页面
     * @param model
     * @return
     */
    @RequestMapping("/embStorageQueryStart")
    public String embStorageQueryStart(Model model){
        model.addAttribute("bigMenuTag",7);
        model.addAttribute("menuTag",72);
        return "embMarket/embStorageQuery";
    }

    @RequestMapping(value = "/embstoragequery", method = RequestMethod.GET)
    public String embStorageQuery(@RequestParam("orderName")String orderName,
                                  @RequestParam("colorName")String colorName,
                                  @RequestParam("sizeName")String sizeName,
                                  Model model){
        if("".equals(colorName)) {
            colorName = null;
        }
        if("".equals(sizeName)) {
            sizeName = null;
        }
        List<EmbStorageQuery> queryResult = embStorageService.embStorageQuery(orderName, colorName, sizeName);
        model.addAttribute("embStorageQueryList",queryResult);
        return "embMarket/fb_embStorageQueryList";
    }

    @RequestMapping(value = "/miniembstoragequery", method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> miniEmbStorageQuery(@RequestParam("orderName")String orderName,
                                              @RequestParam("colorName")String colorName,
                                              @RequestParam("sizeName")String sizeName){
        if("".equals(colorName) || "全部".equals(colorName)) {
            colorName = null;
        }
        if("".equals(sizeName) || "全部".equals(sizeName)) {
            sizeName = null;
        }
        Map<String, Object> map = new HashMap<>();
        List<EmbStorageQuery> queryResult = embStorageService.embStorageQuery(orderName, colorName, sizeName);
        map.put("queryResult",queryResult);
        return map;
    }


    @RequestMapping(value = "/miniembstoragedetailquery", method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> miniEmbStorageDetailQuery(@RequestParam("orderName")String orderName,
                                                        @RequestParam("colorName")String colorName,
                                                        @RequestParam("sizeName")String sizeName){
        if("".equals(colorName) || "全部".equals(colorName)) {
            colorName = null;
        }
        if("".equals(sizeName) || "全部".equals(sizeName)) {
            sizeName = null;
        }
        Map<String, Object> map = new HashMap<>();
        List<EmbStorage> embStorageList = embStorageService.getEmbStorageDetailByOrderColorSize(orderName, colorName, sizeName);
        map.put("queryResult",embStorageList);
        return map;
    }


    @RequestMapping(value = "/getemborderhint",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getEmbOrderHint(@RequestParam("keywords")String keywords){
        Map<String,Object> result = new HashMap<>();
        List<String> embOrderNameList = embStorageService.getEmbOrderHint(keywords);
        result.put("content",embOrderNameList);
        result.put("code",0);
        result.put("type","success");
        return result;
    }

    @RequestMapping(value = "/minigetemborderhint",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> miniGetEmbOrderHint(@RequestParam("subOrderName")String subOrderName){
        Map<String,Object> result = new HashMap<>();
        List<String> embOrderNameList = embStorageService.getEmbOrderHint(subOrderName);
        result.put("embOrderNameList",embOrderNameList);
        return result;
    }


    @RequestMapping(value = "/getembcolorhint",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getEmbColorHint(@RequestParam("orderName")String orderName){
        Map<String,Object> result = new HashMap<>();
        List<String> embColorList = embStorageService.getEmbColorHint(orderName);
        result.put("embColorList",embColorList);
        return result;
    }

    @RequestMapping(value = "/minigetembcolorhint",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> miniGetEmbColorHint(@RequestParam("orderName")String orderName){
        Map<String,Object> result = new HashMap<>();
        List<String> embColorList = embStorageService.getEmbColorHint(orderName);
        result.put("embColorList",embColorList);
        return result;
    }


    @RequestMapping(value = "/getembsizehint",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getEmbSizeHint(@RequestParam("orderName")String orderName,
                                             @RequestParam("colorName")String colorName){
        Map<String,Object> result = new HashMap<>();
        List<String> embSizeList = embStorageService.getEmbSizeHint(orderName, colorName);
        result.put("embSizeList",embSizeList);
        return result;
    }

    @RequestMapping(value = "/minigetembsizehint",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> miniGetEmbSizeHint(@RequestParam("orderName")String orderName,
                                             @RequestParam("colorName")String colorName){
        Map<String,Object> result = new HashMap<>();
        List<String> embSizeList = embStorageService.getEmbSizeHint(orderName, colorName);
        result.put("embSizeList",embSizeList);
        return result;
    }

    @RequestMapping(value = "/minigetembstoragebyembstorelocation",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> miniGetEmbStorageByEmbStoreLocation(@RequestParam("embStoreLocation")String embStoreLocation){
        Map<String,Object> result = new HashMap<>();
        List<EmbStorage> embStorageList = embStorageService.getEmbStorageByEmbStoreLocation(embStoreLocation);
        int packageCount = 0;
        int layerSum = 0;
        for (EmbStorage embStorage : embStorageList){
            embStorage.setEmbStoreLocation(embStoreLocation);
            packageCount += 1;
            layerSum += embStorage.getLayerCount();
        }
        result.put("embStorageList", embStorageList);
        result.put("packageCount", packageCount);
        result.put("layerSum", layerSum);
        return result;
    }

    @RequestMapping(value = "/minigetembstoragebyordercolorsizelocation",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> miniGetEmbStorageByOrderColorSizeLocation(@RequestParam("orderName")String orderName,
                                                                        @RequestParam("colorName")String colorName,
                                                                        @RequestParam("sizeName")String sizeName,
                                                                        @RequestParam("embStoreLocation")String embStoreLocation){
        Map<String,Object> result = new HashMap<>();
        List<EmbStorage> embStorageList = embStorageService.getEmbStorageByOrderColorSizeLocation(orderName, colorName, sizeName, embStoreLocation);
        int packageCount = 0;
        int layerSum = 0;
        for (EmbStorage embStorage : embStorageList){
            packageCount += 1;
            layerSum += embStorage.getLayerCount();
        }
        result.put("embStorageList", embStorageList);
        result.put("packageCount", packageCount);
        result.put("layerSum", layerSum);
        return result;
    }

    @RequestMapping(value = "/minigetlocationinfobyid",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> miniGetLocationInfoByID(@RequestParam("tailorQcodeID") Integer tailorQcodeID){
        Map<String,Object> result = new HashMap<>();
        Tailor tailor = tailorService.getTailorByTailorQcodeID(tailorQcodeID, 0);
        String orderName = tailor.getOrderName();
        String colorName = tailor.getColorName();
        String sizeName = tailor.getSizeName();
        Integer bedNumber = tailor.getBedNumber();
        Integer packageNumber = tailor.getPackageNumber();
        Integer layerCount = tailor.getLayerCount();
        String embStoreLocation = embStorageService.getLocationByOrderColorSizePackage(orderName, bedNumber, colorName, sizeName, packageNumber);
        if (embStoreLocation == null){
            return result;
        }
        EmbStorage es = new EmbStorage(embStoreLocation, orderName, bedNumber, colorName, sizeName, layerCount, packageNumber);
        List<EmbStorage> embStorageList = embStorageService.getEmbStorageByOrderColorSize(embStoreLocation,orderName,colorName,sizeName);
        Integer packageCount = 0;
        Integer layerSum = 0;
        for (EmbStorage embStorage : embStorageList){
            packageCount += 1;
            layerSum += embStorage.getLayerCount();
        }
        result.put("embStorage", es);
        result.put("packageCount", packageCount);
        result.put("layerSum", layerSum);
        return result;
    }

    @RequestMapping(value = "/deleteembstoragebyinfo", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> deleteEmbStorageByInfo(@RequestParam("orderName")String orderName,
                                                      @RequestParam("colorName")String colorName,
                                                      @RequestParam("sizeName")String sizeName,
                                                      @RequestParam("embStoreLocation")String embStoreLocation){
        Map<String, Object> map = new LinkedHashMap<>();
        int res = embStorageService.deleteEmbStorageByInfo(orderName, colorName, sizeName, embStoreLocation);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/deleteembstoragebyid", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> deleteEmbStorageById(@RequestParam("embStorageID")Integer embStorageID){
        Map<String, Object> map = new LinkedHashMap<>();
        int res = embStorageService.deleteEmbStorageById(embStorageID);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/minigetembstoragesummarybylocation",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> miniGetEmbStorageSummaryByLocation(@RequestParam("embStoreLocation")String embStoreLocation){
        List<EmbStorage> embStorageList = new ArrayList<>();
        Map<String,Object> map = new HashMap<>();
        embStorageList = embStorageService.getEmbStorageSummaryByLocation(embStoreLocation);
        map.put("embStorageList", embStorageList);
        return map;
    }

    @RequestMapping(value = "/getEmbStorageByInfo", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getEmbStorageByInfo(@RequestParam("orderName")String orderName,
                                                   @RequestParam(value = "colorName", required = false)String colorName,
                                                   @RequestParam(value = "sizeName", required = false)String sizeName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<EmbStorage> embStorageList = embStorageService.getEmbStorageByInfo(orderName, colorName, sizeName);
        map.put("data", embStorageList);
        map.put("code", 0);
        map.put("count", embStorageList.size());
        return map;
    }

}
