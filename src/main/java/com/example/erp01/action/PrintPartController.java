package com.example.erp01.action;

import com.example.erp01.model.OpaBackInput;
import com.example.erp01.model.PrintPart;
import com.example.erp01.service.PrintPartService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "erp")
public class PrintPartController {

    @Autowired
    private PrintPartService printPartService;

    @RequestMapping(value = "/printPartStart")
    public String printPartStart(Model model){
        model.addAttribute("bigMenuTag",9);
        model.addAttribute("menuTag",96);
        return "ieInfo/printPart";
    }

    @RequestMapping(value = "/addprintpart",method = RequestMethod.POST)
    @ResponseBody
    public int addPrintPart(PrintPart printPart){
        return printPartService.addPrintPart(printPart);
    }

    @RequestMapping(value = "/deleteprintpart",method = RequestMethod.POST)
    @ResponseBody
    public int deletePrintPart(@RequestParam("printPartID") Integer printPartID){
        return printPartService.deletePrintPart(printPartID);
    }

    @RequestMapping(value = "/getprintpartnamesbyorder",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getPrintPartNameByOrder(@RequestParam("orderName") String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> printPartNameList = printPartService.getPrintPartByOrder(orderName);
        map.put("printPartNameList",printPartNameList);
        return map;
    }

    @RequestMapping(value = "/minigetmainprintpartnamesbyorder",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> miniGetMainPrintPartNameByOrder(@RequestParam("orderName") String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> printPartNameList = printPartService.getMainPrintPartByOrder(orderName);
        map.put("printPartNameList",printPartNameList);
        return map;
    }

    @RequestMapping(value = "/getmainprintpartnamesbyorder",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getMainPrintPartNameByOrder(@RequestParam("orderName") String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> printPartNameList = printPartService.getMainPrintPartByOrder(orderName);
        map.put("printPartNameList",printPartNameList);
        return map;
    }

    @RequestMapping(value = "/minigetotherprintpartnamesbyorder",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> miniGetOtherPrintPartNameByOrder(@RequestParam("orderName") String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> printPartNameList = printPartService.getOtherPrintPartByOrder(orderName);
        List<String> printPartNameFilterList = new ArrayList<>();
        for (String partName : printPartNameList){
            if (!partName.startsWith("士啤")){
                printPartNameFilterList.add(partName);
            }
        }
        map.put("printPartNameList",printPartNameFilterList);
        return map;
    }

    @RequestMapping(value = "/getotherprintpartnamesbyorder",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getOtherPrintPartNameByOrder(@RequestParam("orderName") String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> printPartNameList = printPartService.getOtherPrintPartByOrder(orderName);
        map.put("printPartNameList",printPartNameList);
        return map;
    }


    @RequestMapping(value = "/getallprintpart",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getAllPrintPart(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<PrintPart> printPartList = printPartService.getAllPrintPart();
        map.put("printPartList",printPartList);
        return map;
    }

    @RequestMapping(value = "/addprintpartbatch",method = RequestMethod.POST)
    @ResponseBody
    public int addPrintPart(@RequestParam("printPartJson")String printPartJson){

        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        List<PrintPart> printPartList = gson.fromJson(printPartJson,new TypeToken<List<PrintPart>>(){}.getType());
        return printPartService.addPrintPartBatch(printPartList);
    }

}
