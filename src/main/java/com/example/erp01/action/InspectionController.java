package com.example.erp01.action;

import com.example.erp01.model.Inspection;
import com.example.erp01.model.PieceWork;
import com.example.erp01.model.ProcedureInfo;
import com.example.erp01.service.DispatchService;
import com.example.erp01.service.InspectionService;
import com.example.erp01.service.OrderProcedureService;
import com.example.erp01.service.PieceWorkService;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

@Controller
@RequestMapping(value = "erp")
public class InspectionController {

    @Autowired
    private InspectionService inspectionService;
    @Autowired
    private PieceWorkService pieceWorkService;
    @Autowired
    private DispatchService dispatchService;
    @Autowired
    private OrderProcedureService orderProcedureService;


    @RequestMapping(value = "/inspectionStart")
    public String inspectionStart(Model model){
        model.addAttribute("bigMenuTag",10);
        model.addAttribute("menuTag",107);
        return "miniProgram/inspection";
    }

    @RequestMapping(value = "/miniaddinspection",method = RequestMethod.POST)
    @ResponseBody
    public int miniAddInspectionNew(@RequestParam("inspectionJson") String inspectionJson,
                                    @RequestParam("pieceType") Integer pieceType){
        JsonParser jsonParser = new JsonParser();
        try{
            JsonObject json = (JsonObject) jsonParser.parse(inspectionJson);
            String employeeNumber = json.get("employeeNumber").getAsString();
            String tailorQcodeIDString = json.get("tailorQcodeID").getAsString();
            Integer tailorQcodeID = Integer.parseInt(tailorQcodeIDString);
            String orderName = json.get("orderName").getAsString();
            int bedNumber = Integer.parseInt(json.get("bedNumber").getAsString());
            String colorName = json.get("colorName").getAsString();
            String sizeName = json.get("sizeName").getAsString();
            String partName = json.get("partName").getAsString();
            int packageNumber = Integer.parseInt(json.get("packageNumber").getAsString());
            int layerCount = Integer.parseInt(json.get("layerCount").getAsString());
            String wrongCode = json.get("wrongCode").getAsString();
            int wrongQuantity = Integer.parseInt(json.get("wrongQuantity").getAsString());
            List<ProcedureInfo> procedureInfoList = dispatchService.getProcedureInfoByOrderEmp(orderName,employeeNumber);
            List<PieceWork> pieceWorkList = new ArrayList<>();
            int res1 = 1;
            if (pieceType == 0){
                for (ProcedureInfo procedureInfo:procedureInfoList) {
                    int tmpProcedureNumber = procedureInfo.getProcedureNumber();
                    ProcedureInfo procedureInfo1 = orderProcedureService.getProcedureInfoByOrderProcedureNumber(orderName, tmpProcedureNumber);
                    if (procedureInfo1 != null && procedureInfo1.getScanPart() != null){
                        if (!partName.contains(procedureInfo1.getScanPart())){
                            return 3;
                        }
                    }
                    PieceWork pieceWork = new PieceWork(employeeNumber,orderName,bedNumber,colorName,sizeName,packageNumber,layerCount,tmpProcedureNumber,tailorQcodeID);
                    pieceWorkList.add(pieceWork);
                }
                res1 = pieceWorkService.addPieceWorkBatchNew(pieceWorkList);
            }else {
                for (ProcedureInfo procedureInfo:procedureInfoList) {
                    int tmpProcedureNumber = procedureInfo.getProcedureNumber();
                    List<PieceWork> tmpPieceWorkList = pieceWorkService.getPieceWorkByOrderBedPack(orderName, bedNumber, packageNumber, tmpProcedureNumber);
                    if (tmpPieceWorkList == null || tmpPieceWorkList.size() == 0){
                        ProcedureInfo procedureInfo1 = orderProcedureService.getProcedureInfoByOrderProcedureNumber(orderName, tmpProcedureNumber);
                        if (procedureInfo1 != null && procedureInfo1.getScanPart() != null){
                            if (!partName.contains(procedureInfo1.getScanPart())){
                                return 3;
                            }
                        }
                        PieceWork pieceWork = new PieceWork(employeeNumber,orderName,bedNumber,colorName,sizeName,packageNumber,layerCount,tmpProcedureNumber,tailorQcodeID);
                        pieceWorkList.add(pieceWork);
                    }
                }
                res1 = pieceWorkService.addPieceWorkBatchNew(pieceWorkList);
            }
            Inspection inspection = new Inspection(employeeNumber,orderName,bedNumber,packageNumber,layerCount,wrongCode,wrongQuantity);
            int res2 = inspectionService.addInspection(inspection);
            if (res1 == 0 && res2 == 0){
                return 0;
            }else {
                return 1;
            }
        }catch (JsonIOException e){
            e.printStackTrace();
        }catch (JsonSyntaxException e){
            e.printStackTrace();
        }
        return 1;
    }


    @RequestMapping(value = "/miniaddinspectionnew",method = RequestMethod.POST)
    @ResponseBody
    public int miniAddInspectionNew(@RequestParam("inspectionJson") String inspectionJson){
        JsonParser jsonParser = new JsonParser();
        try{
            JsonObject json = (JsonObject) jsonParser.parse(inspectionJson);
            String employeeNumber = json.get("employeeNumber").getAsString();
            String tailorQcodeIDString = json.get("tailorQcodeID").getAsString();
            Integer tailorQcodeID = Integer.parseInt(tailorQcodeIDString);
            String orderName = json.get("orderName").getAsString();
            int bedNumber = Integer.parseInt(json.get("bedNumber").getAsString());
            String colorName = json.get("colorName").getAsString();
            String sizeName = json.get("sizeName").getAsString();
            int packageNumber = Integer.parseInt(json.get("packageNumber").getAsString());
            int layerCount = Integer.parseInt(json.get("layerCount").getAsString());
            String wrongCode = json.get("wrongCode").getAsString();
            int wrongQuantity = Integer.parseInt(json.get("wrongQuantity").getAsString());
            List<ProcedureInfo> procedureInfoList = dispatchService.getProcedureInfoByOrderEmp(orderName,employeeNumber);
            List<PieceWork> pieceWorkList = new ArrayList<>();
            int res1 = 1;
            for (ProcedureInfo procedureInfo:procedureInfoList) {
                int tmpProcedureNumber = procedureInfo.getProcedureNumber();
                PieceWork pieceWork = new PieceWork(employeeNumber,orderName,bedNumber,colorName,sizeName,packageNumber,layerCount,tmpProcedureNumber,tailorQcodeID);
                pieceWorkList.add(pieceWork);
            }
            Inspection inspection = new Inspection(employeeNumber,orderName,bedNumber,packageNumber,layerCount,wrongCode,wrongQuantity);
            int res2 = inspectionService.addInspection(inspection);
            if (res1 == 0 && res2 == 0){
                return 0;
            }else {
                return 1;
            }
        }catch (JsonIOException e){
            e.printStackTrace();
        }catch (JsonSyntaxException e){
            e.printStackTrace();
        }
        return 1;
    }

    @RequestMapping(value = "/addinspection",method = RequestMethod.POST)
    @ResponseBody
    public int addInspection(Inspection inspection){
        int res = inspectionService.addInspection(inspection);
        return res;
    }

    @RequestMapping(value = "/minideleteinspection",method = RequestMethod.POST)
    @ResponseBody
    public int miniDeleteInspection(Integer inspectionID){
        int res = inspectionService.deleteInspection(inspectionID);
        return res;
    }


    @RequestMapping(value = "/deleteinspection",method = RequestMethod.POST)
    @ResponseBody
    public int deleteInspection(Integer inspectionID){
        int res = inspectionService.deleteInspection(inspectionID);
        return res;
    }


    @RequestMapping(value = "/getallinspection",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getAllInspection(){
        Map<String,Object> map = new HashMap<>();
        List<Inspection> inspectionList = inspectionService.getAllInspection();
        Collections.sort(inspectionList);
        map.put("allInspection",inspectionList);
        return map;
    }

    @RequestMapping(value = "/getinspectionbyorder",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getInspectionByOrder(@RequestParam("orderName")String orderName){
        Map<String,Object> map = new HashMap<>();
        List<Inspection> inspectionList = inspectionService.getInspectionByOrder(orderName);
        map.put("inspectionByOrder",inspectionList);
        return map;
    }
    @RequestMapping(value = "/getinspectionbyorderbed",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getInspectionByOrderBed(@RequestParam("orderName")String orderName,
                                                      @RequestParam("bedNumber")Integer bedNumber){
        Map<String,Object> map = new HashMap<>();
        List<Inspection> inspectionList = inspectionService.getInspectionByOrderBed(orderName, bedNumber);
        map.put("inspectionByOrderBed",inspectionList);
        return map;
    }

    @RequestMapping(value = "/minigetinspectionbyemporder",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> miniGetInspectionByEmpOrder(@RequestParam("employeeNumber")String employeeNumber,
                                                      @RequestParam("orderName")String orderName){
        Map<String,Object> map = new HashMap<>();
        List<Inspection> inspectionList = inspectionService.getInspectionByEmpOrder(orderName,employeeNumber);
        map.put("inspectionByEmpOrder",inspectionList);
        return map;
    }

    @RequestMapping(value = "/getinspectionbyemporder",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getInspectionByEmpOrder(@RequestParam("employeeNumber")String employeeNumber,
                                                      @RequestParam("orderName")String orderName){
        Map<String,Object> map = new HashMap<>();
        List<Inspection> inspectionList = inspectionService.getInspectionByEmpOrder(orderName,employeeNumber);
        map.put("inspectionByEmpOrder",inspectionList);
        return map;
    }

    @RequestMapping(value = "/getinspectiontoday",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getInspectionToday(){
        Map<String,Object> map = new HashMap<>();
        List<Inspection> inspectionList = inspectionService.getInspectionToday();
        map.put("inspectionToday",inspectionList);
        return map;
    }

    @RequestMapping(value = "/getinspectionthismonth",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getInspectionThisMonth(){
        Map<String,Object> map = new HashMap<>();
        List<Inspection> inspectionList = inspectionService.getInspectionThisMonth();
        map.put("inspectionThisMonth",inspectionList);
        return map;
    }

    @RequestMapping(value = "/getinspectionemptoday",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getInspectionEmpToday(@RequestParam("employeeNumber")String employeeNumber){
        Map<String,Object> map = new HashMap<>();
        List<Inspection> inspectionList = inspectionService.getInspectionEmpToday(employeeNumber);
        map.put("inspectionEmpToday",inspectionList);
        return map;
    }

    @RequestMapping(value = "/getinspectionempthismonth",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getInspectionEmpThisMonth(@RequestParam("employeeNumber")String employeeNumber){
        Map<String,Object> map = new HashMap<>();
        List<Inspection> inspectionList = inspectionService.getInspectionEmpThisMonth(employeeNumber);
        map.put("inspectionEmpThisMonth",inspectionList);
        return map;
    }

    @RequestMapping(value = "/getinspectionsummary",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getInspectionSummary(){
        Map<String,Object> map = new HashMap<>();
        List<Object> inspectionList = inspectionService.getInspectionSummary();
        map.put("data",inspectionList);
        return map;
    }

}
