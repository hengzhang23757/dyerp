package com.example.erp01.action;

import com.alibaba.dubbo.config.annotation.Reference;
import com.example.erp01.model.*;
import com.example.erp01.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

/**
 * Created by hujian on 2019/12/29
 */
@Controller
@RequestMapping(value = "erp")
public class RpcTestController {

    @Reference(check=false,timeout = 30000,url = "dubbo://115.28.184.180:20883",version = "2.0.0")
    private RpcService xhlRpcServiceRef;
    @Autowired
    private OPAService opaService;
    @Autowired
    private OtherOPAService otherOPAService;
    @Autowired
    private OpaBackInputService opaBackInputService;
    @Autowired
    private TailorService tailorService;

    @RequestMapping(value = "/getopareportsummary", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getOPATailorSummary(@RequestParam("orderName")String orderName,
                                                   @RequestParam("clothesVersionNumber")String clothesVersionNumber,
                                                   @RequestParam("partName")String partName,
                                                   @RequestParam(value = "from", required = false)String from,
                                                   @RequestParam(value = "to", required = false)String to){
        Map<String, Object> map = new LinkedHashMap<>();
        /***
         * 1.获取手动输入的花片和士啤
         * 2.获取回厂数据
         * 3.获取新华隆入仓数据
         * 4.获取新华隆出仓数据
         */
        List<Tailor> tailorList = new ArrayList<>();
        if (partName.equals("主身布")){
            tailorList = tailorService.getTailorInfoGroupByColorSizePartType(orderName, null, 0);
        } else {
            tailorList = tailorService.getTailorInfoGroupByColorSizePartType(orderName, partName, 1);
        }
        List<XhlTailorReport> xhlTailorReportList = xhlRpcServiceRef.getInStoreSummary("德悦", orderName);
        List<OutBoundReport> outBoundReportList1 = xhlRpcServiceRef.getOutBoundSummary("德悦", orderName, null, null);
        List<OutBoundReport> outBoundReportList3 = xhlRpcServiceRef.getOutBoundSummary("德悦", orderName, from, to);
        List<OutBoundReport> outBoundReportList2 = xhlRpcServiceRef.getInspectionSummary("德悦", orderName);
        List<OPALeak> opaLeakList = new ArrayList<>();
        if (xhlTailorReportList != null && xhlTailorReportList.size() > 0){
            for (XhlTailorReport xhlTailorReport : xhlTailorReportList){
                String colorName = xhlTailorReport.getColorName();
                String sizeName = xhlTailorReport.getSizeName();
                String partName1 = xhlTailorReport.getPrintingPart();
                Integer opaBackCount = 0;
                Integer regionBack = 0;
                Integer wellCount = 0;
                Integer defectiveCount = 0;
                Integer rottenCount = 0;
                Integer lackCount = 0;
                for (OutBoundReport outBoundReport1 : outBoundReportList1){
                    if (colorName.equals(outBoundReport1.getColorName()) && sizeName.equals(outBoundReport1.getSizeName()) && partName1.equals(outBoundReport1.getPrintingPart())){
                        opaBackCount += outBoundReport1.getLayerCount();
                    }
                }
                for (OutBoundReport outBoundReport3 : outBoundReportList3){
                    if (colorName.equals(outBoundReport3.getColorName()) && sizeName.equals(outBoundReport3.getSizeName()) && partName1.equals(outBoundReport3.getPrintingPart())){
                        regionBack += outBoundReport3.getLayerCount();
                    }
                }
                for (OutBoundReport outBoundReport2 : outBoundReportList2){
                    if (colorName.equals(outBoundReport2.getColorName()) && sizeName.equals(outBoundReport2.getSizeName()) && partName1.equals(outBoundReport2.getPrintingPart())){
                        defectiveCount += outBoundReport2.getDefectiveCount();
                        rottenCount += outBoundReport2.getRottenCount();
                        lackCount += outBoundReport2.getLackCount();
                    }
                }
                for (Tailor tailor : tailorList){
                    if (colorName.equals(tailor.getColorName()) && sizeName.equals(tailor.getSizeName()) && partName1.equals(tailor.getPartName())){
                        wellCount += tailor.getTailorReportCount();
                    }
                }
                OPALeak opaLeak = new OPALeak(orderName, clothesVersionNumber, partName1, colorName, sizeName, wellCount, xhlTailorReport.getTotalLayer(), 0, opaBackCount, regionBack,opaBackCount - xhlTailorReport.getTotalLayer(), defectiveCount, rottenCount, lackCount);
                opaLeakList.add(opaLeak);
            }
            map.put("xhl",opaLeakList);
        }
        return map;
    }

    @RequestMapping(value = "/getopareportdailysummary", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getOpaReportDailySummary(@RequestParam("orderName")String orderName,
                                                        @RequestParam("clothesVersionNumber")String clothesVersionNumber,
                                                        @RequestParam("partName")String partName,
                                                        @RequestParam(value = "from", required = false)String from,
                                                        @RequestParam(value = "to", required = false)String to){
        Map<String, Object> map = new LinkedHashMap<>();
        List<OutBoundReport> outBoundReportList = xhlRpcServiceRef.getOutBoundSummary("德悦", orderName, from, to);
        for (OutBoundReport outBoundReport : outBoundReportList){
            outBoundReport.setClothesVersionNumber(clothesVersionNumber);
        }
        map.put("xhl", outBoundReportList);
        return map;
    }


    @RequestMapping(value = "/getopareportdetail", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getOPAReportDetail(@RequestParam("orderName")String orderName,
                                                  @RequestParam("sizeName")String sizeName,
                                                  @RequestParam("printingPart")String printingPart,
                                                  @RequestParam("colorName")String colorName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<XhlTailor> xhlTailorList = xhlRpcServiceRef.getInStoreDetail("德悦", orderName, colorName, sizeName, printingPart);
        List<XhlPieceWork> xhlPieceWorkList1 = xhlRpcServiceRef.getOutBoundDetail("德悦", orderName, colorName, sizeName, printingPart);
        List<XhlPieceWork> xhlPieceWorkList2 = xhlRpcServiceRef.getInspectionDetail("德悦", orderName, colorName, sizeName, printingPart);
        int finishCount = 0;
        List<XhlPieceWork> xhlPieceWorkList3 = new ArrayList<>();
        List<XhlPieceWork> xhlPieceWorkList4 = new ArrayList<>();
        for (XhlTailor xhlTailor : xhlTailorList){
            boolean flag = true;
            int defectiveCount = 0;
            int rottenCount = 0;
            int lackCount = 0;
            Date createTime = null;
            for (XhlPieceWork xhlPieceWork1 : xhlPieceWorkList1){
                if (xhlTailor.getPackageNumber().equals(xhlPieceWork1.getPackageNumber())){
                    xhlTailor.setLocation("已做");
                    finishCount += xhlTailor.getLayerCount();
                    createTime = xhlPieceWork1.getCreateTime();
                    flag = false;
                }
            }
            for (XhlPieceWork xhlPieceWork2 : xhlPieceWorkList2){
                if (xhlTailor.getPackageNumber().equals(xhlPieceWork2.getPackageNumber())){
                    xhlTailor.setLocation("已做");
                    defectiveCount = xhlPieceWork2.getDefectiveCount();
                    rottenCount = xhlPieceWork2.getRottenCount();
                    lackCount = xhlPieceWork2.getLackCount();
                    createTime = xhlPieceWork2.getCreateTime();
                    flag = false;
                }
            }
            if (flag){
                XhlPieceWork xhlPieceWork = new XhlPieceWork("德悦", orderName, printingPart, colorName, xhlTailor.getSizeName(), xhlTailor.getBedNumber(), xhlTailor.getPackageNumber(), xhlTailor.getLayerCount(), null, null, null, null, null, null, defectiveCount, rottenCount, lackCount, "未做");
                xhlPieceWorkList4.add(xhlPieceWork);
            }else{
                XhlPieceWork xhlPieceWork = new XhlPieceWork("德悦", orderName, printingPart, colorName, xhlTailor.getSizeName(), xhlTailor.getBedNumber(), xhlTailor.getPackageNumber(), xhlTailor.getLayerCount(), null, null, null, null, null, null, defectiveCount, rottenCount, lackCount, "已做");
                xhlPieceWork.setCreateTime(createTime);
                xhlPieceWorkList3.add(xhlPieceWork);
            }
        }
        xhlPieceWorkList4.addAll(xhlPieceWorkList3);
        map.put("finishCount", finishCount);
        map.put("xhlTailorList", xhlPieceWorkList4);
        return map;
    }


}
