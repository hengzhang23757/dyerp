package com.example.erp01.action;

import com.example.erp01.model.OpaBackInput;
import com.example.erp01.service.OpaBackInputService;
import com.example.erp01.service.OrderClothesService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "erp")
public class OpaBackInputController {

    @Autowired
    private OpaBackInputService opaBackInputService;
    @Autowired
    private OrderClothesService orderClothesService;


    @RequestMapping("/opaBackInputStart")
    public String opaBackInputStart(Model model){
        model.addAttribute("bigMenuTag",17);
        model.addAttribute("menuTag",173);
        return "opa/opaBackInput";
    }

    @RequestMapping(value = "/addopabackinput", method = RequestMethod.POST)
    @ResponseBody
    public int addOpaBackInput(OpaBackInput opaBackInput){
        String versionNumber = orderClothesService.getVersionNumberByOrderName(opaBackInput.getOrderName());
        opaBackInput.setClothesVersionNumber(versionNumber);
        int res = opaBackInputService.addOpaBackInput(opaBackInput);
        return res;
    }

    @RequestMapping(value = "/addopabackinputbatch", method = RequestMethod.POST)
    @ResponseBody
    public int addOpaBackInput(@RequestParam("opaBackJson")String opaBackJson){
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        List<OpaBackInput> opaBackInputList = gson.fromJson(opaBackJson,new TypeToken<List<OpaBackInput>>(){}.getType());
        int res = opaBackInputService.addOpaBackInputBatch(opaBackInputList);
        return res;
    }

    @RequestMapping(value = "/deleteopabackinput", method = RequestMethod.POST)
    @ResponseBody
    public int deleteOpaBackInput(@RequestParam("opaBackInputID")Integer opaBackInputID){
        int res = opaBackInputService.deleteOpaBackInput(opaBackInputID);
        return res;
    }

    @RequestMapping(value = "/getallopabackinput", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getAllOpaBackInput(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<OpaBackInput> opaBackInputList = opaBackInputService.getAllOpaBackInput();
        map.put("opaBackInputList",opaBackInputList);
        return map;
    }

    @RequestMapping(value = "/updateopabackinput", method = RequestMethod.POST)
    @ResponseBody
    public int updateOpaBackInput(@RequestParam("opaBackInput")String opaBackInput){
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        OpaBackInput obi = gson.fromJson(opaBackInput, OpaBackInput.class);
        return opaBackInputService.updateOpaBackInput(obi);
    }

    @RequestMapping(value = "/gettodayopabackinput", method = RequestMethod.GET)
    public String getTodayOpaBackInput(Model model){
        List<OpaBackInput> opaBackInputList = opaBackInputService.getTodayOpaBackInput();
        model.addAttribute("opaBackInputList",opaBackInputList);
        return "opa/fb_opaBackInputList";
    }

    @RequestMapping(value = "/getonemonthopabackinput", method = RequestMethod.GET)
    public String getOneMonthOpaBackInput(Model model){
        List<OpaBackInput> opaBackInputList = opaBackInputService.getOneMonthOpaBackInput();
        model.addAttribute("opaBackInputList",opaBackInputList);
        return "opa/fb_opaBackInputList";
    }

    @RequestMapping(value = "/getthreemonthopabackinput", method = RequestMethod.GET)
    public String getThreeMonthOpaBackInput(Model model){
        List<OpaBackInput> opaBackInputList = opaBackInputService.getThreeMonthOpaBackInput();
        model.addAttribute("opaBackInputList",opaBackInputList);
        return "opa/fb_opaBackInputList";
    }

}
