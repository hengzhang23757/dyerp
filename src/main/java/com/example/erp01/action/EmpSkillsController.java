package com.example.erp01.action;

import com.example.erp01.model.*;
import com.example.erp01.service.EmpSkillsService;
import com.example.erp01.service.EmployeeService;
import com.example.erp01.service.ProcedureTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

@Configuration      //1.主要用于标记配置类，兼备Component的效果。
@EnableScheduling   // 2.开启定时任务
@Controller
@RequestMapping(value = "erp")
public class EmpSkillsController {

    @Autowired
    private EmpSkillsService empSkillsService;
    @Autowired
    private ProcedureTemplateService procedureTemplateService;
    @Autowired
    private EmployeeService employeeService;

    @RequestMapping("/empSkillsStart")
    public String empSkillsStart(){
        return "plan/empSkills";
    }



//    @Scheduled(cron = "0 0 4 * * ? ")
//    public void empSkillsPreProcess(){
//        /****
//         *  1. 查找昨天的节拍数据，更新sam值
//         *  2. 找到昨天sam值是0的数据，按照nid排序
//         *  3.
//         *
//         */
//        System.out.println("员工节拍修正开始==================");
//        MinMaxNid minMaxNid = empSkillsService.getYesterdayMinMaxNid();
//        if (minMaxNid != null){
//            List<Integer> procedureCodeList = empSkillsService.getProcedureCodeYesterday();
//            List<ProcedureTemplate> procedureTemplateList = empSkillsService.getProcedureTemplateByProcedureCode(procedureCodeList);
//            if (procedureTemplateList != null && !procedureTemplateList.isEmpty()){
//                for (ProcedureTemplate procedureTemplate : procedureTemplateList){
//                    empSkillsService.updateSamByProcedureCode((float)procedureTemplate.getSAM(), procedureTemplate.getProcedureCode(), minMaxNid.getMinNid(), minMaxNid.getMaxNid());
//                    System.out.println("SAM更新一个-------------");
//                }
//            }
//            List<Metre> metreList = empSkillsService.getProcedureCombineOfYesterday();
//            int lengthCount = 0;
//            if (metreList != null && metreList.size() >= 5){
//                for (int i = 2; i < metreList.size(); i ++){
//                    if (metreList.get(i).getMerge() && !metreList.get(i - 1).getMerge()){
//                        int step = 1;
//                        float sam = metreList.get(i - 1).getSAM() + metreList.get(i).getSAM();
//                        Double timeCount = (double)(metreList.get(i).getEndTime().getTime() - metreList.get(i).getBeginTime().getTime())/(1000*60);
//                        while (i + step < metreList.size() && metreList.get(i + step).getMerge()){
//                            sam += metreList.get(i + step).getSAM();
//                            step ++;
//                        }
//                        if (step > 1){
//                            for (int j = -1; j < step; j ++){
//                                if (sam == 0) {
//                                    metreList.get(i + j).setTimeCount(0d);
//                                } else {
//                                    metreList.get(i + j).setTimeCount(timeCount*metreList.get(i + j).getSAM()/sam);
//                                }
//
//                            }
//                        } else {
//                            if (sam == 0){
//                                metreList.get(i - 1).setTimeCount(0d);
//                                metreList.get(i).setTimeCount(0d);
//                            } else {
//                                metreList.get(i - 1).setTimeCount(timeCount*metreList.get(i - 1).getSAM()/sam);
//                                metreList.get(i).setTimeCount(timeCount*metreList.get(i).getSAM()/sam);
//                            }
//
//                        }
//                    }
//                }
//                lengthCount = metreList.size()/1000;
//            }
//            if (lengthCount > 0){
//                for (int k = 0; k < lengthCount; k ++){
//                    List<Metre> tmpMetreList1 = metreList.subList(k * 1000, (k + 1) * 1000);
//                    int res = empSkillsService.updateMetreBatch(tmpMetreList1);
//                    if (res == 0){
//                        System.out.println("员工节拍修正成功1000-----------------");
//                    } else {
//                        System.out.println("员工节拍修正失败1000-----------------");
//                    }
//                }
//                List<Metre> tmpMetreList2 = metreList.subList(lengthCount * 1000, metreList.size());
//                int res = empSkillsService.updateMetreBatch(tmpMetreList2);
//                if (res == 0){
//                    System.out.println("员工节拍修正成功-----------------");
//                } else {
//                    System.out.println("员工节拍修正失败-----------------");
//                }
//
//            }
//            System.out.println("员工节拍修正完成===============");
//        }
//
//    }

//    @Scheduled(cron = "0 0 5 * * ? ")
//    public void empSkillTest(){
//        empSkillsService.dailyCleanEmpSkills();
//        List<EmpSkills> empSkillsList = empSkillsService.getYesterdayEmpSkills();
//        List<EmpSkills> destEmpSkillsList = new ArrayList<>();
//        for (EmpSkills empSkills : empSkillsList){
//            List<Float> empSamList = empSkillsService.getPieceWorkDetailByInfo(empSkills.getProcedureCode(), empSkills.getEmployeeNumber());
//            ProcedureTemplate procedureTemplate = procedureTemplateService.getProcedureTemplateByProcedureCode(empSkills.getProcedureCode());
//            if (employeeService.getEmpByEmpNumber(empSkills.getEmployeeNumber()) == null || employeeService.getEmpByEmpNumber(empSkills.getEmployeeNumber()).isEmpty()){
//                continue;
//            }
//            if (procedureTemplate == null){
//                continue;
//            }
//            Employee employee = employeeService.getEmpByEmpNumber(empSkills.getEmployeeNumber()).get(0);
//            Float sam = (float)procedureTemplate.getSAM();
//            empSkills.setProcedureName(procedureTemplate.getProcedureName());
//            empSkills.setProcedureNumber(procedureTemplate.getProcedureNumber());
//            empSkills.setSam((float)procedureTemplate.getSAM());
//            Float empSam = 0f;
//            Iterator <Float> it = empSamList.iterator();
//            while (it.hasNext()){
//                Float thisSam = it.next();
//                if (thisSam > 2*sam || thisSam < 0.3*sam){
//                    it.remove();
//                }
//            }
//            if (empSamList == null || empSamList.size() < 500){
//                continue;
//            }else {
//                Collections.sort(empSamList);
//                int size = empSamList.size();
//                if(size % 2 == 1){
//                    empSam = empSamList.get((size-1)/2);
//                }else {
//                    //加0.0是为了把int转成double类型，否则除以2会算错
//                    empSam = (empSamList.get(size/2-1) + empSamList.get(size/2) + 0f)/2;
//                }
//            }
//            empSkills.setGroupName(employee.getGroupName());
//            empSkills.setTimeCount(empSam);
//            destEmpSkillsList.add(empSkills);
//        }
//        int res = empSkillsService.addEmpSkillsBatch(destEmpSkillsList);
//        if (res == 0){
//            System.out.println("员工技能汇总成功");
//        }else {
//            System.out.println("员工技能汇总失败");
//        }
//    }


//    @Scheduled(cron = "0 12 16 * * ? ")
//    public void empSkillUpdate(){
//        System.out.println("开始更新！");
//        List<EmpSkills> empSkillsList = empSkillsService.getAllEmpSkills();
//        for (EmpSkills empSkills : empSkillsList){
//            ProcedureTemplate procedureTemplate = procedureTemplateService.getProcedureTemplateByProcedureCode(empSkills.getProcedureCode());
//            empSkills.setProcedureNumber(procedureTemplate.getProcedureNumber());
//        }
//        int res = empSkillsService.updateEmpSkillsBatch(empSkillsList);
//        if (res == 0){
//            System.out.println("更新完成！");
//        } else {
//            System.out.println("更新失败！");
//        }
//    }


    @RequestMapping(value = "/getallempskills", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> getAllEmpSkills(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<EmpSkills> empSkillsList = empSkillsService.getAllEmpSkills();
        map.put("data", empSkillsList);
        map.put("code", 0);
        map.put("count", empSkillsList.size());
        return map;
    }


}
