package com.example.erp01.action;

import com.example.erp01.model.*;
import com.example.erp01.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "erp")
public class YesterdayReportController {

    @Autowired
    private YesterdayReportService yesterdayReportService;
    @Autowired
    private TailorService tailorService;
    @Autowired
    private OrderClothesService orderClothesService;
    @Autowired
    private OrderProcedureService orderProcedureService;
    @Autowired
    private EmbOutStoreService embOutStoreService;


    @RequestMapping(value = "/yesterdaytailorreport", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getYesterdayTailorReport(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<YesterdayTailor> yesterdayTailorList = yesterdayReportService.getYesterdayTailorReport();
        for (YesterdayTailor yesterdayTailor : yesterdayTailorList) {
            String orderName = yesterdayTailor.getOrderName();
            Integer tailorCount = tailorService.getTailorCountByInfo(orderName, null,null,null,null,null,0);
            Integer orderCount = orderClothesService.getOrderTotalCount(orderName);
            yesterdayTailor.setOrderCount(orderCount);
            yesterdayTailor.setTailorCount(tailorCount);
        }
        map.put("yesterdayTailorList",yesterdayTailorList);
        return map;
    }

    @RequestMapping(value = "/yesterdayworkshopreport", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getYesterdayWorkShopReport(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<YesterdayWorkShop> yesterdayMiniList = yesterdayReportService.getYesterdayMiniProduction();
        for (YesterdayWorkShop yesterdayWorkShop : yesterdayMiniList){
            String orderName = yesterdayWorkShop.getOrderName();
            Integer orderCount = orderClothesService.getOrderTotalCount(orderName);
            Integer finishCount1 = yesterdayReportService.getMiniWorkCountByProcedureNumber(orderName, 555);
            Integer finishCount2 = yesterdayReportService.getMiniWorkCountByProcedureNumber(orderName, 6555);
            yesterdayWorkShop.setOrderCount(orderCount);
            yesterdayWorkShop.setFinishCount(finishCount1 + finishCount2);
        }
        map.put("yesterdayHangList",yesterdayMiniList);
        return map;
    }


    @RequestMapping(value = "/yesterdayfinishreport", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getYesterdayFinishReport(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<YesterdayFinish> yesterdayFinishList1 = yesterdayReportService.getYesterdayFinishReport();
        for (YesterdayFinish yesterdayFinish : yesterdayFinishList1){
            String orderName = yesterdayFinish.getOrderName();
            Integer tailorCount = tailorService.getTailorCountByInfo(orderName, null,null,null,null,null,0);
            Integer orderCount = orderClothesService.getOrderTotalCount(orderName);
            Integer wellCount = tailorService.getWellCountByInfo(orderName, null,null,null,null,null,0);
            Integer embOutCount = embOutStoreService.getEmbOutSumCount(orderName);
            Integer productCount1 = yesterdayReportService.getMiniWorkCountByProcedureNumber(orderName, 555);
            Integer productCount2 = yesterdayReportService.getMiniWorkCountByProcedureNumber(orderName, 6555);
            Integer finishSumCount1 = yesterdayReportService.getMiniWorkCountByProcedureNumber(orderName, 557);
            Integer finishSumCount2 = yesterdayReportService.getMiniWorkCountByProcedureNumber(orderName, 6557);
            yesterdayFinish.setOrderCount(orderCount);
            yesterdayFinish.setTailorCount(tailorCount);
            yesterdayFinish.setWellCount(wellCount);
            yesterdayFinish.setEmbCount(embOutCount);
            yesterdayFinish.setWorkShopCount(productCount1 + productCount2);
            yesterdayFinish.setFinishCount(finishSumCount1 + finishSumCount2);
        }
        map.put("yesterdayFinishList",yesterdayFinishList1);
        return map;
    }


}
