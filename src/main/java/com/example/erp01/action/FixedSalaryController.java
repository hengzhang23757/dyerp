package com.example.erp01.action;

import com.example.erp01.model.FixedSalary;
import com.example.erp01.model.OrderProcedure;
import com.example.erp01.service.FixedSalaryService;
import com.example.erp01.service.OrderProcedureService;
import com.example.erp01.service.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "erp")
public class FixedSalaryController {

    @Autowired
    private FixedSalaryService fixedSalaryService;
    @Autowired
    private ReportService reportService;
    @Autowired
    private OrderProcedureService orderProcedureService;

    @RequestMapping(value = "/fixedSalaryStart")
    public String printPartStart(){
        return "ieInfo/fixedSalary";
    }

    @RequestMapping(value = "/getallfixedsalary",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getAllFixedSalary(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<FixedSalary> fixedSalaryList = fixedSalaryService.getAllFixedSalary();
        map.put("fixedSalaryList", fixedSalaryList);
        return map;
    }

//    @RequestMapping(value = "/addfixedsalary",method = RequestMethod.POST)
//    @ResponseBody
//    public int addFixedSalary(@RequestParam("fixedValue") String fixedValue){
//        FixedSalary fixedSalary = new FixedSalary(fixedValue);
//        return fixedSalaryService.addFixedSalary(fixedSalary);
//    }

    @RequestMapping(value = "/addfixedsalary",method = RequestMethod.POST)
    @ResponseBody
    public int addFixedSalary(@RequestParam("fixedValue") String fixedValue){
        FixedSalary fixedSalary = new FixedSalary(fixedValue);
        fixedSalaryService.addFixedSalary(fixedSalary);
        List<String> orderNameList = reportService.getPieceWorkOrderNameListByMonthString(fixedValue);
        List<OrderProcedure> orderProcedureList = orderProcedureService.getOrderProcedureByOrderNames(orderNameList, 2);
        return reportService.fillProcedurePrice(orderProcedureList, fixedValue);

    }

    @RequestMapping(value = "/refixedsalary",method = RequestMethod.POST)
    @ResponseBody
    public int reFixedSalary(@RequestParam("fixedValue") String fixedValue){
        List<String> orderNameList = reportService.getPieceWorkOrderNameListByMonthString(fixedValue);
        List<OrderProcedure> orderProcedureList = orderProcedureService.getOrderProcedureByOrderNames(orderNameList, 2);
        return reportService.fillProcedurePrice(orderProcedureList, fixedValue);

    }

    @RequestMapping(value = "/deletefixedsalary",method = RequestMethod.POST)
    @ResponseBody
    public int deleteFixedSalary(@RequestParam("fixedID") Integer fixedID){
        return fixedSalaryService.deleteFixedSalary(fixedID);
    }

}
