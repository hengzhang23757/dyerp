package com.example.erp01.action;

import com.example.erp01.model.SupplierInfo;
import com.example.erp01.service.SupplierInfoService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "erp")
public class SupplierInfoController {

    @Autowired
    private SupplierInfoService supplierInfoService;

    @RequestMapping("/supplierInfoStart")
    public String supplierInfoStart(){
        return "basicData/supplierInfo";
    }

    @RequestMapping(value = "/addsupplierinfo",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> addSupplierInfo(@RequestParam("supplierInfoJson")String supplierInfoJson){
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        SupplierInfo supplierInfo = gson.fromJson(supplierInfoJson,new TypeToken<SupplierInfo>(){}.getType());
        int res = supplierInfoService.addSupplierInfo(supplierInfo);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/deletesupplierinfo",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> deleteSupplierInfo(@RequestParam("id")Integer id){
        Map<String, Object> map = new LinkedHashMap<>();
        int res = supplierInfoService.deleteSupplierInfo(id);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/getsupplierinfohint",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getSupplierInfoHint(@RequestParam("subSupplierName")String subSupplierName
        ,@RequestParam("page")Integer page
        ,@RequestParam("limit")Integer limit){
        Map<String, Object> map = new LinkedHashMap<>();
        List<SupplierInfo> countList = supplierInfoService.getSupplierInfoHint(subSupplierName,null,null);
        List<SupplierInfo> supplierInfoList = supplierInfoService.getSupplierInfoHint(subSupplierName,(page-1)*limit,limit);
        map.put("data",supplierInfoList);
        map.put("msg","");
        map.put("count",countList.size());
        map.put("code",0);
        return map;
    }

    @RequestMapping(value = "/getallsupplierinfo",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getAllSupplierInfo(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<SupplierInfo> supplierInfoList = supplierInfoService.getAllSupplierInfo();
        map.put("data",supplierInfoList);
        map.put("count",supplierInfoList.size());
        map.put("code",0);
        return map;
    }

    @RequestMapping(value = "/updatesupplierinfo",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> updateSupplierInfo(@RequestParam("supplierInfoJson")String supplierInfoJson){
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        SupplierInfo supplierInfo = gson.fromJson(supplierInfoJson,new TypeToken<SupplierInfo>(){}.getType());
        int res = supplierInfoService.updateSupplierInfo(supplierInfo);
        map.put("result", res);
        return map;
    }
}
