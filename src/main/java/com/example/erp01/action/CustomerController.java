package com.example.erp01.action;

import com.example.erp01.model.Customer;
import com.example.erp01.service.CustomerService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

//顾客信息相关操作

@Controller
@RequestMapping(value = "erp")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @RequestMapping("/customerStart")
    public String customerStart(){
        return "basicData/customer";
    }

    @RequestMapping(value = "/addcustomer",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> addCustomer(@RequestParam("customerJson")String customerJson){
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        Customer customer = gson.fromJson(customerJson,new TypeToken<Customer>(){}.getType());
        int res = customerService.addCustomer(customer);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/deletecustomer",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> deleteCustomer(@RequestParam("id")Integer id){
        Map<String, Object> map = new LinkedHashMap<>();
        int res = customerService.deleteCustomer(id);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/getallcustomer",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getAllCustomer(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<Customer> customerList = customerService.getAllCustomer();
        map.put("data",customerList);
        return map;
    }

    @RequestMapping(value = "/updatecustomer",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> updateCustomer(@RequestParam("customerJson")String customerJson){
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        Customer customer = gson.fromJson(customerJson,new TypeToken<Customer>(){}.getType());
        int res = customerService.updateCustomer(customer);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/getcustomercodebyprefix", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getAccessoryNameOrderByPreFix(){
        Map<String, Object> map = new LinkedHashMap<>();
        String maxOrder = customerService.getMaxCustomerCodeByPreFix("C");
        int a = Integer.valueOf(maxOrder).intValue();
        String orderStr = String.format("%04d", a + 1);
        map.put("customerCode", "C" + orderStr);
        return map;
    }

    @RequestMapping(value = "/getcustomerhint", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getCustomerHint(@RequestParam("keyWord")String keyWord,@RequestParam("page") int page,@RequestParam("limit") int limit){
        Map<String, Object> map = new LinkedHashMap<>();
        Integer count = customerService.getCustomerCount(keyWord);
        List<Customer> customerList = customerService.getCustomerHintPage(keyWord,(page-1)*limit,limit);
        map.put("data",customerList);
        map.put("msg","");
        map.put("count",count==null?0:count);
        map.put("code",0);
        return map;
    }

}
