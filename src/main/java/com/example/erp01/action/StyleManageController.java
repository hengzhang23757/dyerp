package com.example.erp01.action;

import com.example.erp01.model.StyleManage;
import com.example.erp01.service.StyleManageService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "erp")
public class StyleManageController {

    @Autowired
    private StyleManageService styleManageService;

    @RequestMapping("/styleManageStart")
    public String styleManageStart(){
        return "basicData/styleManage";
    }


    @RequestMapping(value = "/addstylemanage",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> addStyleManage(@RequestParam("styleManageJson")String styleManageJson){
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        StyleManage styleManage = gson.fromJson(styleManageJson,new TypeToken<StyleManage>(){}.getType());
        int res = styleManageService.addStyleManage(styleManage);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/deletestylemanage",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> deleteStyleManage(@RequestParam("id")Integer id){
        Map<String, Object> map = new LinkedHashMap<>();
        int res = styleManageService.deleteStyleManage(id);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/getallstylemanage",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getAllStyleManage(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<StyleManage> styleManageList = styleManageService.getAllStyleManage();
        map.put("data",styleManageList);
        return map;
    }

    @RequestMapping(value = "/updatestylemanage",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> updateStyleManage(@RequestParam("styleManageJson")String styleManageJson){
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        StyleManage styleManage = gson.fromJson(styleManageJson,new TypeToken<StyleManage>(){}.getType());
        int res = styleManageService.updateStyleManage(styleManage);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/updateprocessinstylemanage",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> updateProcessInStyleManage(@RequestParam("id")Integer id,
                                                          @RequestParam("processText")String processText){
        Map<String, Object> map = new LinkedHashMap<>();
        StyleManage styleManage = new StyleManage();
        styleManage.setId(id);
        styleManage.setProcessText(processText);
        int res = styleManageService.updateProcessInStyleManage(styleManage);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/getstylemanagebyid",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getStyleManageById(@RequestParam("id")Integer id){
        Map<String, Object> map = new LinkedHashMap<>();
        StyleManage styleManage = styleManageService.getStyleManageById(id);
        map.put("data",styleManage);
        return map;
    }

    @RequestMapping(value = "/updatesizeinstylemanage",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> updateSizeInStyleManage(@RequestParam("id")Integer id,
                                                       @RequestParam("sizeText")String sizeText){
        Map<String, Object> map = new LinkedHashMap<>();
        StyleManage styleManage = new StyleManage();
        styleManage.setId(id);
        styleManage.setSizeText(sizeText);
        int res = styleManageService.updateSizeInStyleManage(styleManage);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/getstylemanagehint", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getStyleManageHint(@RequestParam("keyWord")String keyWord,@RequestParam("page") int page,@RequestParam("limit") int limit){
        Map<String, Object> map = new LinkedHashMap<>();
        Integer count = styleManageService.getStyleManageCount(keyWord);
        List<StyleManage> styleManageList = styleManageService.getStyleManageHintPage(keyWord,(page-1)*limit,limit);
        map.put("data",styleManageList);
        map.put("msg","");
        map.put("count",count==null?0:count);
        map.put("code",0);
        return map;
    }

}
