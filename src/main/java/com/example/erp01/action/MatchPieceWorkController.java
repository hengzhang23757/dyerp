package com.example.erp01.action;

import com.example.erp01.model.EmbStorage;
import com.example.erp01.model.MatchPieceWork;
import com.example.erp01.service.EmbInStoreService;
import com.example.erp01.service.EmbStorageService;
import com.example.erp01.service.EmbStoreService;
import com.example.erp01.service.MatchPieceWorkService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "erp")
public class MatchPieceWorkController {

    @Autowired
    private MatchPieceWorkService matchPieceWorkService;
    @Autowired
    private EmbStoreService embStoreService;
    @Autowired
    private EmbStorageService embStorageService;
    @Autowired
    private EmbInStoreService embInStoreService;


    @RequestMapping(value = "/minigetmatchpieceworksummary", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getMatchPieceWorkSummary(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<MatchPieceWork> matchPieceWorkList = matchPieceWorkService.getMatchPieceWorkSummary();
        map.put("matchPieceWorkList", matchPieceWorkList);
        return map;
    }

    @RequestMapping(value = "/deletematchpieceworkbyinfo", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> deleteMatchPieceWorkByInfo(@RequestParam("orderName")String orderName,
                                                                @RequestParam("colorName")String colorName,
                                                                @RequestParam("sizeName")String sizeName){
        Map<String, Object> map = new LinkedHashMap<>();
        int res = matchPieceWorkService.deleteMatchPieceWorkByInfo(orderName, colorName, sizeName);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/deletematchpieceworkbyidlist", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> deleteMatchPieceWorkByIdList(@RequestParam("pieceWorkIdList[]")List<Integer> pieceWorkIdList){
        Map<String, Object> map = new LinkedHashMap<>();
        int res = matchPieceWorkService.deleteMatchPieceWorkByIdList(pieceWorkIdList);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/minideletematchpieceworkbyid", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> miniDeleteMatchPieceWorkById(@RequestParam("pieceWorkID") Integer pieceWorkID){
        Map<String, Object> map = new LinkedHashMap<>();
        int res = matchPieceWorkService.deleteMatchPieceWorkById(pieceWorkID);
        map.put("result", res);
        return map;
    }


    @RequestMapping(value = "/minigetmatchpieceworkdatabyinfo", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getMatchPieceWorkDataByInfo(@RequestParam("orderName")String orderName,
                                                        @RequestParam("colorName")String colorName,
                                                        @RequestParam("sizeName")String sizeName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<MatchPieceWork> matchPieceWorkList = matchPieceWorkService.getMatchPieceWorkByInfo(orderName, colorName, sizeName);
        map.put("matchPieceWorkList", matchPieceWorkList);
        return map;
    }

    @RequestMapping(value = "/miniembinstorebypiecework", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> miniEmbInStoreByPieceWork(@RequestParam("embStoreLocation") String embStoreLocation,
                                                         @RequestParam("matchPieceWorkJson") String matchPieceWorkJson){
        Map<String, Object> map = new LinkedHashMap<>();
        embStoreLocation = embStoreLocation.replace("\n", "");
        if (embStoreService.getEmbStoreByLocation(embStoreLocation) == null){
            map.put("location", "位置为空");
            return map;
        } else {
            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
            List<MatchPieceWork> matchPieceWorkList = gson.fromJson(matchPieceWorkJson,new TypeToken<List<MatchPieceWork>>(){}.getType());
            if (matchPieceWorkList != null && !matchPieceWorkList.isEmpty()){
                List<EmbStorage> embStorageList = new ArrayList<>();
                List<Integer> pieceWorkIDList = new ArrayList<>();
                for (MatchPieceWork matchPieceWork : matchPieceWorkList){
                    pieceWorkIDList.add(matchPieceWork.getPieceWorkID());
                    EmbStorage embStorage = new EmbStorage(embStoreLocation, matchPieceWork.getOrderName(), matchPieceWork.getBedNumber(), matchPieceWork.getColorName(), matchPieceWork.getSizeName(), matchPieceWork.getLayerCount(), matchPieceWork.getPackageNumber());
                    embStorageList.add(embStorage);
                }
                int res1 = embInStoreService.addEmbInStore(embStorageList);
                int res2 = embStorageService.embInStore(embStorageList);
                int res3 = matchPieceWorkService.deleteMatchPieceWorkByIdList(pieceWorkIDList);
                if (res1 == 0 && res2 == 0 && res3 == 0){
                    map.put("success", "入库成功");
                } else {
                    map.put("fail", "入库失败");
                }
            }
            return map;
        }
    }
}
