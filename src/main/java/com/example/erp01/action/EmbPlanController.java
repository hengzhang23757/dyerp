package com.example.erp01.action;

import com.example.erp01.model.EmbPlan;
import com.example.erp01.model.FabricPlan;
import com.example.erp01.service.EmbOutStoreService;
import com.example.erp01.service.EmbPlanService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;


@Controller
@RequestMapping(value = "erp")
public class EmbPlanController {

    @Autowired
    private EmbPlanService embPlanService;

    @RequestMapping("/embPlanStart")
    public String embPlanStart(){
        return "plan/embPlan";
    }



    @RequestMapping(value = "/addembplanbatch", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> addEmbPlanBatch(@RequestParam("embPlanJson")String embPlanJson){
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        List<EmbPlan> embPlans = gson.fromJson(embPlanJson,new TypeToken<List<EmbPlan>>(){}.getType());
        List<EmbPlan> embPlanList2 = new ArrayList<>();
        for (EmbPlan embPlan : embPlans){
            String orderName = embPlan.getOrderName();
            String colorName = embPlan.getColorName();
            String sizeName = embPlan.getSizeName();
            String groupName = embPlan.getGroupName();
            Date outTime = embPlan.getEndDate();
            List<EmbPlan> embPlanList = embPlanService.getEmbPlansByOrderGroupDate(orderName, groupName, outTime);
            if (embPlanList != null && !embPlanList.isEmpty()){
                for (EmbPlan embPlan1 : embPlanList){
                    if (colorName.equals("全部") || sizeName.equals("全部")){
                        map.put("error", orderName +"-"+ groupName+"当日已有计划！");
                        return map;
                    }
                    if ((embPlan1.getColorName() == null || embPlan1.getColorName().equals("全部") || embPlan1.getColorName().equals(colorName)) && (embPlan1.getSizeName() == null || embPlan1.getSizeName().equals("全部") || embPlan1.getSizeName().equals(sizeName))){
                        map.put("error", orderName +"-"+ groupName+"当日已有计划！");
                        return map;
                    }
                }
            }
            Calendar calendar=Calendar.getInstance();
            calendar.setTime(outTime);
            calendar.add(Calendar.DAY_OF_MONTH, -1);//昨天
            calendar.set(Calendar.HOUR_OF_DAY, 18);
            embPlan.setBeginDate(calendar.getTime());
            calendar.setTime(outTime);
            calendar.set(Calendar.HOUR_OF_DAY, 18);
            embPlan.setEndDate(calendar.getTime());
            embPlanList2.add(embPlan);
        }

        if (embPlanService.addEmbPlanBatch(embPlanList2) == 0){
            map.put("success", "添加成功！");
        }else {
            map.put("error", "添加失败！");
        }
        return map;
    }

    @RequestMapping(value = "/miniaddembplan", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> miniAddEmbPlanBatch(@RequestParam("embPlanJson")String embPlanJson){
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        EmbPlan embPlan = gson.fromJson(embPlanJson,new TypeToken<EmbPlan>(){}.getType());
        String orderName = embPlan.getOrderName();
        String colorName = embPlan.getColorName();
        String sizeName = embPlan.getSizeName();
        String groupName = embPlan.getGroupName();
        Date outTime = embPlan.getEndDate();
        List<EmbPlan> embPlanList = embPlanService.getEmbPlansByOrderGroupDate(orderName, groupName, outTime);
        if (embPlanList != null && !embPlanList.isEmpty()){
            for (EmbPlan embPlan1 : embPlanList){
                if ((embPlan1.getColorName() == null || embPlan1.getColorName().equals("全部") || embPlan1.getColorName().equals(colorName)) && (embPlan1.getSizeName() == null || embPlan1.getSizeName().equals("全部") || embPlan1.getSizeName().equals(sizeName))){
                    map.put("error", orderName +"-"+ groupName+"当日已有计划！");
                    return map;
                }
            }
        }
        Calendar calendar=Calendar.getInstance();
        calendar.setTime(outTime);
        calendar.add(Calendar.DAY_OF_MONTH, -1);//昨天
        calendar.set(Calendar.HOUR_OF_DAY, 18);
        embPlan.setBeginDate(calendar.getTime());
        calendar.setTime(outTime);
        calendar.set(Calendar.HOUR_OF_DAY, 18);
        embPlan.setEndDate(calendar.getTime());
        if (embPlanService.addEmbPlan(embPlan) == 0){
            map.put("embPlan", embPlan);
            List<EmbPlan> embPlanList1 = embPlanService.getUnFinishEmbPlan();
            map.put("unFinish", embPlanList1);
            map.put("success", "添加成功！");
        }else {
            map.put("error", "添加失败！");
        }
        return map;
    }

    @RequestMapping(value = "/deleteembplan", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> deleteEmbPlan(@RequestParam("embPlanID")Integer embPlanID){
        Map<String, Object> map = new LinkedHashMap<>();
        if (embPlanService.deleteEmbPlan(embPlanID) == 0){
            map.put("success", "删除成功！");
        } else {
            map.put("error", "删除失败！");
        }
        return map;
    }

    @RequestMapping(value = "/minideleteembplan", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> miniDeleteEmbPlan(@RequestParam("embPlanID")Integer embPlanID){
        Map<String, Object> map = new LinkedHashMap<>();
        if (embPlanService.deleteEmbPlan(embPlanID) == 0){
            List<EmbPlan> embPlanList = embPlanService.getUnFinishEmbPlan();
            map.put("unFinish", embPlanList);
            map.put("success", "删除成功！");
        } else {
            map.put("error", "删除失败！");
        }
        return map;
    }

    @RequestMapping(value = "/gettodayembplan", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getTodayEmbPlan(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<EmbPlan> embPlanList = embPlanService.getTodayEmbPlan();
        map.put("today", embPlanList);
        return map;
    }

    @RequestMapping(value = "/minigettodayembplan", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> miniGetTodayEmbPlan(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<EmbPlan> embPlanList = embPlanService.getTodayEmbPlan();
        map.put("today", embPlanList);
        return map;
    }

    @RequestMapping(value = "/getonemonthembplan", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getOneMonthEmbPlan(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<EmbPlan> embPlanList = embPlanService.getOneMonthEmbPlan();
        map.put("oneMonth", embPlanList);
        return map;
    }

    @RequestMapping(value = "/getthreemonthembplan", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getThreeMonthEmbPlan(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<EmbPlan> embPlanList = embPlanService.getThreeMonthEmbPlan();
        map.put("threeMonth", embPlanList);
        return map;
    }

    @RequestMapping(value = "/updateembplan", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> updateEmbPlan(@RequestParam("embPlanJson")String embPlanJson){
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        EmbPlan embPlan = gson.fromJson(embPlanJson,new TypeToken<EmbPlan>(){}.getType());
        Date outTime = embPlan.getEndDate();
        Calendar calendar=Calendar.getInstance();
        calendar.setTime(outTime);
        calendar.add(Calendar.DAY_OF_MONTH, -1);//昨天
        calendar.set(Calendar.HOUR_OF_DAY, 18);
        embPlan.setBeginDate(calendar.getTime());
        calendar.setTime(outTime);
        calendar.set(Calendar.HOUR_OF_DAY, 18);
        embPlan.setEndDate(calendar.getTime());
        if(embPlanService.updateEmbPlan(embPlan) == 0){
            map.put("success", "更新成功！");
        }else{
            map.put("error", "更新失败！");
        }
        return map;
    }

    @RequestMapping(value = "/miniupdateembplan", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> miniUpdateEmbPlan(@RequestParam("updateEmbPlanID")Integer updateEmbPlanID,
                                                 @RequestParam("updateEmbPlanCount")Integer updateEmbPlanCount){
        Map<String, Object> map = new LinkedHashMap<>();
        int res =  embPlanService.updateEmbPlanCountByID(updateEmbPlanID, updateEmbPlanCount);
        if (res == 0){
            List<EmbPlan> embPlanList = embPlanService.getUnFinishEmbPlan();
            map.put("unFinish", embPlanList);
            map.put("success", "修改成功");
        } else {
            map.put("error", "修改失败");
        }
        return map;
    }

    @RequestMapping(value = "/minigetembstoragecountbyinfo", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> miniGetEmbStorageCountByInfo(@RequestParam("orderName") String orderName,
                                                            @RequestParam("colorName") String colorName,
                                                            @RequestParam("sizeName") String sizeName){
        Map<String, Object> map = new LinkedHashMap<>();
        if (colorName.equals("") || colorName.equals("全部")){
            colorName = null;
        }
        if (sizeName.equals("") || sizeName.equals("全部")){
            sizeName = null;
        }
        Integer storageCount = embPlanService.getEmbStorageCountByInfo(orderName, colorName, sizeName);
        map.put("storageCount", storageCount);
        return map;
    }

    @RequestMapping(value = "/minigetunfinishembplan", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getUnFinishEmbPlan(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<EmbPlan> embPlanList = embPlanService.getUnFinishEmbPlan();
        map.put("unFinish", embPlanList);
        return map;
    }


}
