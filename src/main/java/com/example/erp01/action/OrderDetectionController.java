package com.example.erp01.action;

import com.example.erp01.model.OrderDetection;
import com.example.erp01.service.OrderDetectionService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "erp")
public class OrderDetectionController {

    @Autowired
    private OrderDetectionService orderDetectionService;

    @RequestMapping("/orderDetectionStart")
    public String orderDetectionStart(){
        return "IQC/orderDetection";
    }

    @RequestMapping(value = "/addorderdetection",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> addDetectionTemplate(@RequestParam("orderDetectionJson")String orderDetectionJson){
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        OrderDetection orderDetection = gson.fromJson(orderDetectionJson, OrderDetection.class);
        int res = orderDetectionService.addOrderDetection(orderDetection);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/updateorderdetection",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> updateOrderDetection(@RequestParam("orderDetectionJson")String orderDetectionJson){
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        OrderDetection orderDetection = gson.fromJson(orderDetectionJson, OrderDetection.class);
        int res = orderDetectionService.updateOrderDetection(orderDetection);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/deleteorderdetection",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> deleteOrderDetection(@RequestParam("id")Integer id){
        Map<String, Object> map = new LinkedHashMap<>();
        int res = orderDetectionService.deleteOrderDetection(id);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/getorderdetectionbyinfo",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getOrderDetectionByInfo(@RequestParam(value = "orderName", required = false)String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        Map<String, Object> param = new HashMap<>();
        param.put("orderName", orderName);
        List<OrderDetection> orderDetectionList = orderDetectionService.getOrderDetectionByInfo(param);
        map.put("orderDetectionList", orderDetectionList);
        return map;
    }

    @RequestMapping(value = "/getorderdetectionbyid",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getOrderDetectionById(@RequestParam("id")Integer id){
        Map<String, Object> map = new LinkedHashMap<>();
        OrderDetection orderDetection = orderDetectionService.getOrderDetectionById(id);
        map.put("orderDetection", orderDetection);
        return map;
    }


}
