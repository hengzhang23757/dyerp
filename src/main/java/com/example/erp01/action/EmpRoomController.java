package com.example.erp01.action;

import com.example.erp01.model.EmpRoom;
import com.example.erp01.model.Room;
import com.example.erp01.model.RoomState;
import com.example.erp01.service.EmpRoomService;
import com.example.erp01.service.EmployeeService;
import com.example.erp01.service.RoomService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.internal.LinkedTreeMap;
import javafx.beans.property.adapter.ReadOnlyJavaBeanBooleanProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

@Controller
@RequestMapping(value = "erp")
public class EmpRoomController {

    @Autowired
    private RoomService roomService;

    @Autowired
    private EmpRoomService empRoomService;

    @Autowired
    private EmployeeService employeeService;

    @RequestMapping("/empRoomStart")
    public String empRoomStart(){
        return "logistic/empRoom";
    }

    @RequestMapping(value = "/addemproom", method = RequestMethod.POST)
    @ResponseBody
    public int addEmpRoom(@RequestParam("empRoom")String empRoom){
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        EmpRoom ep = gson.fromJson(empRoom, EmpRoom.class);
        return empRoomService.addEmpRoom(ep);
    }

    @RequestMapping(value = "/deleteemproom", method = RequestMethod.POST)
    @ResponseBody
    public int deleteEmpRoom(@RequestParam("empRoomID") Integer empRoomID){
        return empRoomService.deleteEmpRoom(empRoomID);
    }

    @RequestMapping(value = "/updateemproom", method = RequestMethod.POST)
    @ResponseBody
    public int updateEmpRoom(EmpRoom empRoom){
        return empRoomService.updateEmpRoom(empRoom);
    }

    @RequestMapping(value = "/getallemproom", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getAllEmpRoom(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<EmpRoom> empRoomList = empRoomService.getAllEmpRoom();
        map.put("empRoom",empRoomList);
        return map;
    }

    @RequestMapping(value = "/getavailableroomoffloor", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getAvailableRoomOfFloor(@RequestParam("floor") String floor){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> roomNumberList = new ArrayList<>();
        List<RoomState> roomStateList1 = roomService.getAllRoomInfoByFloor(floor);
        List<RoomState> roomStateList2 = empRoomService.getRoomStateOfFloor(floor);
        if (roomStateList2 == null || roomNumberList.size() == 0){
            for (RoomState roomState1 : roomStateList1){
                roomNumberList.add(roomState1.getRoomNumber());
            }
        }else{
            for (RoomState roomState1 : roomStateList1){
                for (RoomState roomState2 : roomStateList2){
                    if (roomState1.getRoomNumber().equals(roomState2.getRoomNumber())){
                        if (roomState2.getEmpCount() < roomState1.getCapacity()){
                            roomNumberList.add(roomState1.getRoomNumber());
                        }
                    }
                }
            }
        }
        map.put("roomNumberList",roomNumberList);
        return map;
    }

    @RequestMapping(value = "/getavailablebednumber", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getAllEmpRoom(@RequestParam("roomNumber") String roomNumber){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> bedNumberList1 = empRoomService.getAllBedNumberOfRoom(roomNumber);
        int capacity = roomService.getCapacityOfRoom(roomNumber);
        List<String> bedNumberList2 =  Arrays.asList("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T");
        List<String> bedNumberList3 = new ArrayList<>();
        if (capacity <= 20){
            for (int i=0;i<capacity;i++){
                String tmpBedNumber = bedNumberList2.get(i);
                boolean flag = true;
                for (String bn : bedNumberList1){
                    if (tmpBedNumber.equals(bn)){
                        flag = false;
                    }
                }
                if (flag){
                    bedNumberList3.add(tmpBedNumber);
                }
            }
        }
        map.put("bedNumberList",bedNumberList3);
        return map;
    }

}
