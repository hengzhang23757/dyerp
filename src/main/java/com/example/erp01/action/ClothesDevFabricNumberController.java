package com.example.erp01.action;

import com.example.erp01.model.ClothesDevFabricNumber;
import com.example.erp01.service.ClothesDevFabricNumberService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "erp")
public class ClothesDevFabricNumberController {

    @Autowired
    private ClothesDevFabricNumberService clothesDevFabricNumberService;

    @RequestMapping("/clothesDevFabricNumberStart")
    public String clothesDevFabricNumberStart(){
        return "basicData/fabricNumberManage";
    }

    @RequestMapping(value = "/addclothesdevfabricnumber",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> addClothesDevFabricNumber(@RequestParam("clothesDevFabricNumberJson")String clothesDevFabricNumberJson){
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        ClothesDevFabricNumber clothesDevFabricNumber = gson.fromJson(clothesDevFabricNumberJson, ClothesDevFabricNumber.class);
        int res = clothesDevFabricNumberService.addClothesDevFabricNumber(clothesDevFabricNumber);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/deleteclothesdevfabricnumber",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> deleteClothesDevFabricNumber(@RequestParam("id")Integer id){
        Map<String, Object> map = new LinkedHashMap<>();
        int res = clothesDevFabricNumberService.deleteClothesDevFabricNumber(id);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/getallclothesdevfabricnumber",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getAllCustomer(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<ClothesDevFabricNumber> clothesDevFabricNumberList = clothesDevFabricNumberService.getAllClothesDevFabricNumber();
        map.put("data",clothesDevFabricNumberList);
        return map;
    }

    @RequestMapping(value = "/updateclothesdevfabricnumber",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> updateClothesDevFabricNumber(@RequestParam("clothesDevFabricNumberJson")String clothesDevFabricNumberJson){
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        ClothesDevFabricNumber clothesDevFabricNumber = gson.fromJson(clothesDevFabricNumberJson, ClothesDevFabricNumber.class);
        int res = clothesDevFabricNumberService.updateClothesDevFabricNumber(clothesDevFabricNumber);
        map.put("result", res);
        return map;
    }


}
