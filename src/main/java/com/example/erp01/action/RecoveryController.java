package com.example.erp01.action;

import com.example.erp01.model.Tailor;
import com.example.erp01.service.RecoveryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "erp")
public class RecoveryController {

    @Autowired
    private RecoveryService recoveryService;

    @RequestMapping("/recoveryStart")
    public String recoveryStart(){
        return "opaMsg/recovery";
    }

    @RequestMapping(value = "/getdeletedbednumbers", method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getDeletedBedNumbers(@RequestParam("orderName")String orderName){
        Map<String,Object> result = new HashMap<>();
        List<Integer> bedNumberList = recoveryService.getDeletedBedNumber(orderName);
        result.put("bedNumberList",bedNumberList);
        return result;
    }


    @RequestMapping(value = "/getdeletedtailorbyinfo", method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getDeletedTailorByInfo(@RequestParam("orderName")String orderName,
                                                     @RequestParam("bedNumber")Integer bedNumber){
        Map<String,Object> result = new HashMap<>();
        List<Tailor> tailorList = recoveryService.getDeletedTailorByInfo(orderName, bedNumber);
        result.put("tailorList",tailorList);
        return result;
    }


    @RequestMapping(value = "/deletedtailorrecovery", method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> deletedTailorRecovery(@RequestParam("orderName")String orderName,
                                                    @RequestParam("bedNumber")Integer bedNumber,
                                                    @RequestParam("tailorIDList")List<Integer> tailorIDList){
        Map<String,Object> result = new HashMap<>();
        List<Tailor> tailorList1 = recoveryService.getNormalTailorByInfo(orderName, bedNumber);
        List<Tailor> tailorList2 = recoveryService.getSelectedTailorByID(orderName, tailorIDList);
        for (Tailor tailor1 : tailorList1){
            for (Tailor tailor2 : tailorList2){
                if (tailor2.getBedNumber().equals(tailor1.getBedNumber()) && tailor2.getPackageNumber().equals(tailor1.getPackageNumber()) && tailor2.getPartName().equals(tailor1.getPartName())){
                    result.put("msg", tailor2.getBedNumber() + "床" + tailor2.getPackageNumber() + "扎" + tailor2.getPartName() + "与已有数据冲突");
                    return result;
                }
            }
        }
        int res = recoveryService.recoverTailorByInfo(orderName, tailorIDList);
        result.put("res", res);
        return result;
    }




}
