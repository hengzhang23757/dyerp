package com.example.erp01.action;

import com.example.erp01.model.*;
import com.example.erp01.service.EmpSkillsService;
import com.example.erp01.service.EmployeeService;
import com.example.erp01.service.OrderProcedureService;
import com.example.erp01.service.ScheduleRecordService;
import com.example.erp01.utils.LinearProgramming;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

@Controller
@RequestMapping(value = "erp")
public class ScheduleController {

    @Autowired
    private OrderProcedureService orderProcedureService;
    @Autowired
    private EmployeeService employeeService;
    @Autowired
    private EmpSkillsService empSkillsService;
    @Autowired
    private ScheduleRecordService scheduleRecordService;

    @RequestMapping("/orderScheduleStart")
    public String orderScheduleStart(){
        return "plan/orderSchedule";
    }

    @RequestMapping(value = "/getorderscheduledata", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getOrderScheduleData(@RequestParam("orderName")String orderName,
                                                    @RequestParam("workHour")Integer workHour,
                                                    @RequestParam("groupName")String groupName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<OrderProcedure> orderProcedureList = orderProcedureService.getOrderProcedureByOrderProcedureSection(orderName, "车缝");
        List<Integer> procedureCodeList = new ArrayList<>();
        if (orderProcedureList.size() > 0 && !orderProcedureList.isEmpty()){
            for (OrderProcedure orderProcedure : orderProcedureList){
                procedureCodeList.add(orderProcedure.getProcedureCode());
            }
            map.put("orderProcedureList", orderProcedureList);
        }
        List<String> groupList = new ArrayList<>();
        groupList.add(groupName);
        List<Employee> employeeList = employeeService.getEmployeeByGroups(groupList);
        if (employeeList.size() > 0 && !employeeList.isEmpty()){
            map.put("employeeList", employeeList);
        }
        List<EmpSkills> empSkillsList = empSkillsService.getEmpSkillsByGroupProcedureCode(groupName, procedureCodeList);
        if (empSkillsList.size() >0 && !empSkillsList.isEmpty()){
            map.put("empSkillsList", empSkillsList);
        }
        return map;
    }


    @RequestMapping(value = "/getsewsectionprocedure", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getSewSectionProcedureData(@RequestParam("orderName")String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<OrderProcedure> orderProcedureList = orderProcedureService.getOrderProcedureByOrderProcedureSection(orderName, "车缝");
        map.put("orderProcedureList", orderProcedureList);
        return map;
    }


//    @RequestMapping(value = "/getscheduleofordergroup", method = RequestMethod.GET)
//    @ResponseBody
//    public Map<String, Object> getScheduleOfOrderGroup(@RequestParam("selectData") String selectData,
//                                                       @RequestParam(value = "workHour", required = false) Integer workHour){
//        Gson gson = new Gson();
//        Map<String, Object> map = new LinkedHashMap<>();
//        List<Map<String, String>> selectDataList = gson.fromJson(selectData,
//                new TypeToken<List<Map<String, String>>>(){}.getType());
//        int x = selectDataList.size();
//        if (x == 0){
//            map.put("error", "数据为空");
//            return map;
//        }
//        int y = selectDataList.get(0).size() - 1;
//        List<String> keySet = new ArrayList<>(selectDataList.get(0).keySet());
//        keySet.remove("employeeInfo");
//        double[][] prepareData = new double [x][y];
//        for (int i = 0; i < selectDataList.size(); i ++){
//            for (int j = 0; j < keySet.size(); j ++){
//                prepareData[i][j] = Double.valueOf(selectDataList.get(i).get(keySet.get(j)));
//            }
//        }
//        if (workHour == null){
//            workHour = 11;
//        }
//        //确定未知数的个数和按列取值时的位置以及c数组的值
//        int xCount = 0;
//        String[] yLocationString = new String[y];
//        List<Double> cList = new ArrayList<>();
//        for (int i = 0; i < x; i ++){
//            for (int j = 0; j < y; j ++){
//                if (prepareData[i][j] > 0){
//                    cList.add(prepareData[i][j]);
//                    if (yLocationString[j] == null){
//                        yLocationString[j] = String.valueOf(xCount);
//                    } else {
//                        yLocationString[j] = yLocationString[j] + "," + String.valueOf(xCount);
//                    }
//                    xCount ++;
//                }
//            }
//        }
//        List<List<Integer>> yLocationList = new ArrayList<>();
//        for (int i = 0; i < y; i ++){
//            System.out.println(yLocationString[i]);
//            String[] stringLocation = yLocationString[i].split(",");
//            List<Integer> tmpList = new ArrayList<>();
//            for (int j = 0; j < stringLocation.length; j ++){
//                tmpList.add(Integer.parseInt(stringLocation[j]));
//            }
//            yLocationList.add(tmpList);
//        }
//        //填充c数组
//        double[] c = new double[xCount];
//        for (int i = 0; i < xCount; i ++){
//            c[i] = cList.get(i);
//        }
//        //参数矩阵是一个(x+y)*xCount的矩阵  初始化一个xCount的行向量 并填充0
//        double[] stdArr = new double[xCount];
//        double[][] parMatrix = new double[x+y][xCount];
//        for (int i = 0; i < xCount; i++){
//            stdArr[i] = 0;
//        }
//        //用来填充工作时长的参数部分
//        int totalX = 0;//用来累计每一行的x的数量
//        for (int i = 0; i < x; i ++){
//            double[] tmpArr = stdArr.clone();
//            for (int j = 0; j < y; j ++){
//                if (prepareData[i][j] > 0){
////                    tmpArr[totalX] = prepareData[i][j];
//                    tmpArr[totalX] = 1;
//                    totalX ++;
//                }
//            }
//            for (int k = 0; k < xCount; k ++){
//                parMatrix[i][k] = tmpArr[k];
//            }
//        }
//        //用来填充工序平衡的参数部分
//        double[] firstCol = stdArr.clone();
//        double[] oneCol;
//        double[] anOtherCol = stdArr.clone();
//        for (int j = 0; j < y; j ++){
//            int colIndex = 0;
//            oneCol = anOtherCol;
//            anOtherCol = stdArr.clone();
//            for (int k = 0; k < x; k ++){
//                if (prepareData[k][j] > 0){
//                    if (j == 0){
//                        firstCol[yLocationList.get(j).get(colIndex)] = prepareData[k][j];
//                        anOtherCol[yLocationList.get(j).get(colIndex)] = prepareData[k][j];
//                        colIndex ++;
//                    } else {
//                        anOtherCol[yLocationList.get(j).get(colIndex)] = prepareData[k][j];
//                        colIndex ++;
//                    }
//                }
//            }
//            if (j > 0){
//                for (int i = 0; i < xCount; i ++){
//                    parMatrix[x + j - 1][i] = anOtherCol[i] - oneCol[i];
//                }
//            }
//        }
//        //循环相减，第一个减去最后一个
//        for (int i = 0; i < xCount; i ++){
//            parMatrix[x+y-1][i] = firstCol[i] - anOtherCol[i];
//        }
//        //填充b,c的向量
//        double[] b = new double[x + y];
//        for (int i = 0; i < x; i ++){
//            b[i] = workHour;
//        }
//        for (int j = 0; j < y; j ++){
//            b[x + j] = 0;
//        }
//        //求解
//        LinearProgramming lp = new LinearProgramming();
//        double[] result = lp.test(parMatrix, b, c);
//        //把结果装进一个map中返回 需要修改一下
//        List<Map<String, Object>> scheduleList = new ArrayList<>();
//        int procedureIndex = 0;
//        double[] summaryData = new double[y];
//        for (int i = 0; i < y; i ++){
//            summaryData[i] = 0;
//        }
////        double [][] resultMatrix = new double [x][y];
////        for (int i = 0; i < x; i ++){
////            Map<String, Object> personSchedule = new LinkedHashMap<>();
////            personSchedule.put("employeeInfo", selectDataList.get(i).get("employeeInfo"));
////            for (int j = 0; j < y; j ++){
////                if (prepareData[i][j] > 0){
////                    double hour = Math.round(result[procedureIndex]);
////                    personSchedule.put(keySet.get(j), String.valueOf(prepareData[i][j]) + "*" + String.valueOf(hour) + "=" + String.valueOf(Math.round(hour * prepareData[i][j])));
////                    summaryData[j] += Math.round(result[procedureIndex] * prepareData[i][j]);
////                    procedureIndex ++;
////                } else {
////                    personSchedule.put(keySet.get(j), "0");
////                }
////            }
////            scheduleList.add(personSchedule);
////        }
//
//        double [][] resultMatrix = new double [x][y];
//        for (int i = 0; i < x; i ++){
//            for (int j = 0; j < y; j ++){
//                if (prepareData[i][j] > 0){
//                    resultMatrix[i][j] = Math.round(result[procedureIndex]);
//                    summaryData[j] += Math.round(resultMatrix[i][j] * prepareData[i][j]);
//                    procedureIndex ++;
//                } else {
//                    resultMatrix[i][j] = 100;
//                }
//            }
//        }
//        /***
//         * 第二部优化
//         */
//
//        double minValue = summaryData[0];
//        int minIndex = 0;
//        for (int i = 1; i < y; i ++){
//            if (summaryData[i] - minValue < 0){
//                minIndex = i;
//                minValue = summaryData[i];
//            }
//        }
//        int iterCount = 0;
//        while(!checkEachPersonWorkHour(resultMatrix, workHour)){
//            if (iterCount > 100){
//                break;
//            }
//            iterCount ++;
//            double distanceValue = 1000000;
//            System.out.println("最小列：" + minIndex);
//            //找到minIndex列有值的  并且该行未达到11的
//            boolean fullFlag = false;
//            for (int j = 0; j < x; j ++){
//                if (resultMatrix[j][minIndex] != 100){
//                    double sumHour = 0;
//                    for (int k = 0; k < y; k ++){
//                        if (resultMatrix[j][k] != 100){
//                            sumHour += resultMatrix[j][k];
//                        }
//                    }
//                    if (sumHour < workHour){
//                        fullFlag = true;
//                        resultMatrix[j][minIndex] += workHour - sumHour;
//                        summaryData [minIndex] += (workHour - sumHour)*prepareData[j][minIndex];
//                    }
//                }
//            }
//            if (!fullFlag){
//                minValue = summaryData[minIndex];
//                for (int i = 0; i < y; i ++){
//                    if (summaryData[i] - minValue > 0 && summaryData[i] - minValue < distanceValue){
//                        minIndex = i;
//                        distanceValue = summaryData[i] - minValue;
//                    }
//                }
//            } else {
//                minIndex = 0;
//                minValue = summaryData[0];
//                for (int i = 1; i < y; i ++){
//                    if (summaryData[i] - minValue < 0){
//                        minIndex = i;
//                        minValue = summaryData[i];
//                    }
//                }
//            }
//
//        }
//        System.out.println("优化结束===========");
//
//
//        for (int i = 0; i < x; i ++){
//            Map<String, Object> personSchedule = new LinkedHashMap<>();
//            personSchedule.put("employeeInfo", selectDataList.get(i).get("employeeInfo"));
//            for (int j = 0; j < y; j ++){
//                if (prepareData[i][j] > 0){
//                    double hour = resultMatrix[i][j];
//                    personSchedule.put(keySet.get(j), String.valueOf(prepareData[i][j]) + "*" + String.valueOf(hour) + "=" + String.valueOf(Math.round(hour * prepareData[i][j])));
//                    procedureIndex ++;
//                } else {
//                    personSchedule.put(keySet.get(j), "0");
//                }
//            }
//            scheduleList.add(personSchedule);
//        }
//
//
//        Map<String, Object> summaryMap = new LinkedHashMap<>();
//        summaryMap.put("employeeInfo", "汇总");
//        for (int j = 0; j < y; j ++){
//            summaryMap.put(keySet.get(j), String.valueOf(summaryData[j]));
//        }
//        map.put("scheduleList", scheduleList);
//        map.put("summaryData", summaryMap);
//        return map;
//    }


    @RequestMapping(value = "/getscheduleofordergroup", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getScheduleOfOrderGroup(@RequestParam("selectData") String selectData,
                                                       @RequestParam(value = "workHour", required = false) Integer workHour,
                                                       @RequestParam(value = "exceptCount", required = false) Integer exceptCount){
        Gson gson = new Gson();
        Map<String, Object> map = new LinkedHashMap<>();
        List<Map<String, String>> selectDataList = gson.fromJson(selectData,
                new TypeToken<List<Map<String, String>>>(){}.getType());
        int x = selectDataList.size();
        if (x == 0){
            map.put("error", "数据为空");
            return map;
        }
        int y = selectDataList.get(0).size() - 1;
        List<String> keySet = new ArrayList<>(selectDataList.get(0).keySet());
        keySet.remove("employeeInfo");
        double[][] prepareData = new double [x][y];
        for (int i = 0; i < selectDataList.size(); i ++){
            for (int j = 0; j < keySet.size(); j ++){
                prepareData[i][j] = Double.valueOf(selectDataList.get(i).get(keySet.get(j)));
            }
        }
        if (workHour == null){
            workHour = 11;
        }
        if (exceptCount == null){
            exceptCount = 2000;
        }
        //确定未知数的个数和按列取值时的位置以及c数组的值
        int xCount = 0;
        String[] yLocationString = new String[y];
        List<Double> cList = new ArrayList<>();
        for (int i = 0; i < x; i ++){
            for (int j = 0; j < y; j ++){
                if (prepareData[i][j] > 0){
                    cList.add(prepareData[i][j]);
                    if (yLocationString[j] == null){
                        yLocationString[j] = String.valueOf(xCount);
                    } else {
                        yLocationString[j] = yLocationString[j] + "," + String.valueOf(xCount);
                    }
                    xCount ++;
                }
            }
        }
        List<List<Integer>> yLocationList = new ArrayList<>();
        for (int i = 0; i < y; i ++){
            String[] stringLocation = yLocationString[i].split(",");
            List<Integer> tmpList = new ArrayList<>();
            for (int j = 0; j < stringLocation.length; j ++){
                tmpList.add(Integer.parseInt(stringLocation[j]));
            }
            yLocationList.add(tmpList);
        }
        //填充c数组
        double[] c = new double[xCount];
        for (int i = 0; i < xCount; i ++){
            c[i] = cList.get(i);
        }
        //参数矩阵是一个(x+y)*xCount的矩阵  初始化一个xCount的行向量 并填充0
        double[] stdArr = new double[xCount];
        double[][] parMatrix = new double[x+y][xCount];
        for (int i = 0; i < xCount; i++){
            stdArr[i] = 0;
        }
        //用来填充工作时长的参数部分
        int totalX = 0;//用来累计每一行的x的数量
        for (int i = 0; i < x; i ++){
            double[] tmpArr = stdArr.clone();
            for (int j = 0; j < y; j ++){
                if (prepareData[i][j] > 0){
//                    tmpArr[totalX] = prepareData[i][j];
                    tmpArr[totalX] = 1;
                    totalX ++;
                }
            }
            for (int k = 0; k < xCount; k ++){
                parMatrix[i][k] = tmpArr[k];
            }
        }
        //用来填充工序平衡的参数部分
        double[] anOtherCol;
        for (int j = 0; j < y; j ++){
            int colIndex = 0;
            anOtherCol = stdArr.clone();
            for (int k = 0; k < x; k ++){
                if (prepareData[k][j] > 0){
                    anOtherCol[yLocationList.get(j).get(colIndex)] = prepareData[k][j];
                    colIndex ++;
                }
            }
            for (int i = 0; i < xCount; i ++){
                parMatrix[x + j][i] = anOtherCol[i];
            }
        }
        //填充b,c的向量
        double[] b = new double[x + y];
        for (int i = 0; i < x; i ++){
            b[i] = workHour;
        }
        for (int j = 0; j < y; j ++){
            b[x + j] = exceptCount;
        }
        //求解
        LinearProgramming lp = new LinearProgramming();
        double[] result = lp.test(parMatrix, b, c);
        //把结果装进一个map中返回 需要修改一下
        List<Map<String, Object>> scheduleList = new ArrayList<>();
        int procedureIndex = 0;
        double[] summaryData = new double[y];
        for (int i = 0; i < y; i ++){
            summaryData[i] = 0;
        }
        for (int i = 0; i < x; i ++){
            Map<String, Object> personSchedule = new LinkedHashMap<>();
            personSchedule.put("employeeInfo", selectDataList.get(i).get("employeeInfo"));
            for (int j = 0; j < y; j ++){
                if (prepareData[i][j] > 0){
                    double hour = Math.round(result[procedureIndex]);
                    if (hour > 0){
                        personSchedule.put(keySet.get(j), String.valueOf(prepareData[i][j]) + "*" + String.valueOf(hour) + "=" + String.valueOf(Math.round(hour * prepareData[i][j])));
                    } else {
                        personSchedule.put(keySet.get(j), "0");
                    }
                    summaryData[j] += Math.round(result[procedureIndex] * prepareData[i][j]);
                    procedureIndex ++;
                } else {
                    personSchedule.put(keySet.get(j), "0");
                }
            }
            scheduleList.add(personSchedule);
        }

        Map<String, Object> summaryMap = new LinkedHashMap<>();
        summaryMap.put("employeeInfo", "汇总");
        for (int j = 0; j < y; j ++){
            summaryMap.put(keySet.get(j), String.valueOf(summaryData[j]));
        }
        map.put("scheduleList", scheduleList);
        map.put("summaryData", summaryMap);
        return map;
    }



    private boolean checkEachPersonWorkHour(double[][] resultMatrix, Integer workHour){
        int x = resultMatrix.length;
        int y = resultMatrix[0].length;
        for (int i = 0; i < x; i ++){
            int hourSum = 0;
            for (int j = 0; j < y; j ++){
                if (resultMatrix[i][j] != 100){
                    hourSum += resultMatrix[i][j];
                }
            }
            if (hourSum != workHour){
                return false;
            }
        }
        return true;
    }


    @RequestMapping(value = "/getschedulerecorddata", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getScheduleRecordData(@RequestParam("orderName")String orderName,
                                                     @RequestParam("groupName")String groupName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<OrderProcedure> orderProcedureList = orderProcedureService.getOrderProcedureByOrderProcedureSection(orderName, "车缝");
        List<Integer> procedureCodeList = new ArrayList<>();
        if (orderProcedureList !=null && !orderProcedureList.isEmpty()){
            for (OrderProcedure orderProcedure : orderProcedureList){
                procedureCodeList.add(orderProcedure.getProcedureCode());
            }
            map.put("orderProcedureList", orderProcedureList);
        }
        List<String> groupList = new ArrayList<>();
        groupList.add(groupName);
        List<Employee> employeeList = employeeService.getEmployeeByGroups(groupList);
        if (employeeList !=null && !employeeList.isEmpty()){
            map.put("employeeList", employeeList);
        }
        List<EmpSkills> empSkillsList = empSkillsService.getEmpSkillsByGroupProcedureCode(groupName, procedureCodeList);
        if (empSkillsList !=null && !empSkillsList.isEmpty()){
            map.put("empSkillsList", empSkillsList);
        }
        List<ScheduleRecord> scheduleRecordList = scheduleRecordService.getScheduleRecordByInfo(orderName, groupName);
        if (scheduleRecordList != null && !scheduleRecordList.isEmpty()){
            map.put("scheduleRecordList", scheduleRecordList);
        }
        return map;
    }

    @RequestMapping(value = "/addschedulerecordbatch", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> addScheduleRecordBatch(@RequestParam("scheduleRecordJson")String scheduleRecordJson,
                                                      @RequestParam("orderName")String orderName,
                                                      @RequestParam("groupName")String groupName){
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        List<ScheduleRecord> scheduleRecordList = gson.fromJson(scheduleRecordJson, new TypeToken<List<ScheduleRecord>>(){}.getType());
        scheduleRecordService.deleteScheduleRecordByInfo(orderName, groupName);
        int res = scheduleRecordService.addScheduleRecordBatch(scheduleRecordList);
        map.put("result", res);
        return map;
    }


    @RequestMapping(value = "/getuniqueschedulerecorddata", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getUniqueScheduleRecordData(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<ScheduleRecord> scheduleRecordList = scheduleRecordService.getUniqueScheduleRecordOneYear();
        map.put("data", scheduleRecordList);
        map.put("code", 0);
        map.put("count", scheduleRecordList.size());
        return map;
    }

    @RequestMapping(value = "/getscheduledetailrecorddata", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getScheduleRecordDetailData(@RequestParam("orderName")String orderName,
                                                           @RequestParam("groupName")String groupName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<ScheduleRecord> scheduleRecordList = scheduleRecordService.getScheduleRecordByInfo(orderName, groupName);
        if (scheduleRecordList != null && !scheduleRecordList.isEmpty()){
            map.put("scheduleRecordList", scheduleRecordList);
        }
        List<OrderProcedure> orderProcedureList = orderProcedureService.getOrderProcedureByOrderProcedureSection(orderName, "车缝");
        List<OrderProcedure> savedProcedureList = new ArrayList<>();
        List<Integer> procedureCodeList = new ArrayList<>();
        if (orderProcedureList != null && !orderProcedureList.isEmpty() && scheduleRecordList != null && !scheduleRecordList.isEmpty()){
            for (OrderProcedure orderProcedure : orderProcedureList){
                boolean flag = false;
                for (ScheduleRecord  scheduleRecord : scheduleRecordList){
                    if (orderProcedure.getProcedureNumber().equals(scheduleRecord.getProcedureNumber()) && !flag){
                        savedProcedureList.add(orderProcedure);
                        procedureCodeList.add(orderProcedure.getProcedureCode());
                        flag = true;
                    }
                }
            }
        }
        map.put("orderProcedureList", savedProcedureList);
        map.put("totalOrderProcedureList", orderProcedureList);
        List<String> groupList = new ArrayList<>();
        groupList.add(groupName);
        List<Employee> employeeList = employeeService.getEmployeeByGroups(groupList);
        if (employeeList != null && !employeeList.isEmpty()){
            map.put("employeeList", employeeList);
        }
        List<EmpSkills> empSkillsList = empSkillsService.getEmpSkillsByGroupProcedureCode(groupName, procedureCodeList);
        if (empSkillsList != null && !empSkillsList.isEmpty()){
            map.put("empSkillsList", empSkillsList);
        }
        return map;
    }

    @RequestMapping(value = "/deleteschedulerecordbyordergroup", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> deleteScheduleRecordByOrderGroup(@RequestParam("orderName")String orderName,
                                                                @RequestParam("groupName")String groupName){
        Map<String, Object> map = new LinkedHashMap<>();
        int res = scheduleRecordService.deleteScheduleRecordByInfo(orderName, groupName);
        map.put("result", res);
        return map;
    }



}
