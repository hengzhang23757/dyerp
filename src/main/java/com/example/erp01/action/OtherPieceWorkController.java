package com.example.erp01.action;

import com.example.erp01.model.*;
import com.example.erp01.service.*;
import com.google.gson.*;
import jdk.nashorn.internal.objects.NativeUint8Array;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Controller
@RequestMapping(value = "erp")
public class OtherPieceWorkController {

    @Autowired
    private PieceWorkService pieceWorkService;
    @Autowired
    private TailorService tailorService;
    @Autowired
    private DispatchService dispatchService;
    @Autowired
    private OrderProcedureService orderProcedureService;
    @Autowired
    private FinishTailorService finishTailorService;
    @Autowired
    private EmbInStoreService embInStoreService;
    @Autowired
    private EmbOutStoreService embOutStoreService;
    @Autowired
    private EmbStorageService embStorageService;

    public static final List<Integer> expList = Stream.of(666, 777, 6557, 6559).collect(Collectors.toList());

    @RequestMapping(value = "/otherPieceWorkStart")
    public String pieceWorkStatisticStart(){
        return "otherPieceWork/otherPieceWork";
    }

    @RequestMapping(value = "/addotherpieceworkbatch",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> addOtherPieceWorkBatch(@RequestParam("tailorQcodeID") Integer tailorQcodeID,
                                                      @RequestParam("employeeNumber") String employeeNumber,
                                                      @RequestParam("employeeName") String employeeName,
                                                      @RequestParam("procedureNumber") Integer procedureNumber,
                                                      @RequestParam(value = "isOutBound", required = false) boolean isOutBound,
                                                      @RequestParam(value = "outBoundGroupName", required = false) String outBoundGroupName,
                                                      @RequestParam("groupName") String groupName){
        Map<String, Object> map = new LinkedHashMap<>();
        Tailor tailor = new Tailor();
        Tailor tailor1 = tailorService.getTailorByTailorQcodeID(tailorQcodeID, null);
        if (tailor1 == null){
            FinishTailor finishTailor = finishTailorService.getFinishTailorByTailorQcodeID(tailorQcodeID);
            if (finishTailor == null){
                map.put("msg", "二维码信息不存在");
                map.put("code", 2);
                return map;
            }
            else if ((finishTailor.getShelfOut() == null || finishTailor.getShelfOut() == 0) && !expList.contains(procedureNumber)){
                tailor.setOrderName(finishTailor.getOrderName());
                tailor.setCustomerName(finishTailor.getCustomerName());
                tailor.setClothesVersionNumber(finishTailor.getClothesVersionNumber());
                tailor.setLayerCount(finishTailor.getLayerCount());
                tailor.setInitCount(finishTailor.getInitCount());
                tailor.setBedNumber(finishTailor.getBedNumber());
                tailor.setPackageNumber(finishTailor.getPackageNumber());
                tailor.setPartName(finishTailor.getPartName());
                tailor.setTailorQcodeID(finishTailor.getTailorQcodeID());
                tailor.setTailorQcode(finishTailor.getTailorQcode());
                tailor.setColorName(finishTailor.getColorName());
                tailor.setSizeName(finishTailor.getSizeName());
                map.put("msg", "成品未对接,请先对接");
                map.put("tailor", tailor);
                map.put("code", 10);
                return map;
            }
            else {
                tailor.setOrderName(finishTailor.getOrderName());
                tailor.setCustomerName(finishTailor.getCustomerName());
                tailor.setClothesVersionNumber(finishTailor.getClothesVersionNumber());
                tailor.setLayerCount(finishTailor.getLayerCount());
                tailor.setInitCount(finishTailor.getInitCount());
                tailor.setBedNumber(finishTailor.getBedNumber());
                tailor.setPackageNumber(finishTailor.getPackageNumber());
                tailor.setPartName(finishTailor.getPartName());
                tailor.setTailorQcodeID(finishTailor.getTailorQcodeID());
                tailor.setTailorQcode(finishTailor.getTailorQcode());
                tailor.setColorName(finishTailor.getColorName());
                tailor.setSizeName(finishTailor.getSizeName());
            }
        }
        else {
            tailor.setOrderName(tailor1.getOrderName());
            tailor.setCustomerName(tailor1.getCustomerName());
            tailor.setClothesVersionNumber(tailor1.getClothesVersionNumber());
            tailor.setLayerCount(tailor1.getLayerCount());
            tailor.setInitCount(tailor1.getInitCount());
            tailor.setBedNumber(tailor1.getBedNumber());
            tailor.setPackageNumber(tailor1.getPackageNumber());
            tailor.setPartName(tailor1.getPartName());
            tailor.setTailorQcodeID(tailor1.getTailorQcodeID());
            tailor.setTailorQcode(tailor1.getTailorQcode());
            tailor.setColorName(tailor1.getColorName());
            tailor.setSizeName(tailor1.getSizeName());
        }
        OrderProcedure orderProcedure = orderProcedureService.getOrderProcedureByOrderNameProcedureNumber(tailor.getOrderName(), procedureNumber);
        if (orderProcedure == null){
            map.put("msg", "该制单无此工序,请联系IE");
            map.put("tailor",tailor);
            map.put("code",3);
            return map;
        }
        boolean colorFlag = false;
        boolean sizeFlag = false;
        if (orderProcedure.getColorName().equals("全部")){
            colorFlag = true;
        } else {
            String strColor = orderProcedure.getColorName();
            List<String> specialColorList = Arrays.asList(strColor.split(","));
            for (String color : specialColorList){
                if (tailor.getColorName().equals(color)){
                    colorFlag = true;
                }
            }
        }
        if (orderProcedure.getSizeName().equals("全部")){
            sizeFlag = true;
        } else {
            String strSize = orderProcedure.getSizeName();
            List<String> specialSizeList = Arrays.asList(strSize.split(","));
            for (String size : specialSizeList){
                if (tailor.getSizeName().equals(size)){
                    sizeFlag = true;
                }
            }
        }
        if (!(colorFlag && sizeFlag)){
            map.put("msg", "该颜色/尺码不用计件~~");
            map.put("tailor",tailor);
            map.put("code",12);
            return map;
        }
        //直接入库再出库
        if (isOutBound){
            if (tailor != null && tailor.getPartName().contains("主身")){
                boolean flag = true;
                int pn = 0;
                if (orderProcedure.getProcedureName().contains("配扎")){
                    flag = false;
                }
                if (flag){
                    map.put("msg", "选择的不是配扎工序");
                    map.put("tailor",tailor);
                    map.put("code",3);
                    return map;
                }
                List<PieceWork> pieceWorkList1 = pieceWorkService.getPieceWorkByOrderBedPack(tailor.getOrderName(), tailor.getBedNumber(), tailor.getPackageNumber(), procedureNumber);
                if (pieceWorkList1 != null && pieceWorkList1.size() == 1){
                    map.put("msg", "重复计件");
                    map.put("tailor",tailor);
                    map.put("code",4);
                    return map;
                }
                List<PieceWork> pieceWorkList = new ArrayList<>();
                PieceWork pieceWork = new PieceWork(employeeNumber,employeeName,groupName,tailor.getOrderName(),tailor.getClothesVersionNumber(), tailor.getBedNumber(), tailor.getColorName(), tailor.getSizeName(),tailor.getPackageNumber(), tailor.getLayerCount(),pn,"配扎",tailor.getTailorQcodeID());
                pieceWorkList.add(pieceWork);
                List<EmbStorage> embStorageList = new ArrayList<>();
                List<EmbOutStore> embOutStoreList = new ArrayList<>();
                EmbStorage embStorage = new EmbStorage("B1-1-1", tailor.getOrderName(), tailor.getBedNumber(), tailor.getColorName(), tailor.getSizeName(), tailor.getLayerCount(), tailor.getPackageNumber(), tailor.getTailorQcode());
                embStorageList.add(embStorage);
                int res1 = pieceWorkService.addPieceWorkBatchNew(pieceWorkList);
                int res2 = embInStoreService.addEmbInStore(embStorageList);
                EmbOutStore eos = new EmbOutStore(outBoundGroupName,tailor.getOrderName(), tailor.getBedNumber(), tailor.getColorName(), tailor.getSizeName(), tailor.getLayerCount(), tailor.getPackageNumber(), tailor.getTailorQcode());
                embOutStoreList.add(eos);
                int res3 = embOutStoreService.addEmbOutStore(embOutStoreList);
                if (res1 == 0 && res2 == 0 && res3 == 0){
                    map.put("msg", "计件成功");
                    map.put("tailor",tailor);
                    map.put("code",0);
                    return map;
                }else {
                    map.put("msg", "计件失败");
                    map.put("tailor",tailor);
                    map.put("code",1);
                    return map;
                }
            } else {
                map.put("msg", "请扫描主身");
                map.put("code",7);
                return map;
            }
        }
        List<PieceWork> pieceWorkList = new ArrayList<>();
        List<PieceWorkEmp> pieceWorkEmpList = pieceWorkService.getProcedureInfoByOrderBedPackage(tailor.getOrderName(), tailor.getBedNumber(), tailor.getPackageNumber());
        map.put("pieceWorkSummary", pieceWorkEmpList);
        map.put("tailor", tailor);
        if (!tailor.getPartName().contains(orderProcedure.getScanPart())){
            map.put("msg", "扫描部位不正确");
            map.put("code",6);
            return map;
        }
        List<PieceWork> pieceWorkList1 = pieceWorkService.getPieceWorkByOrderBedPack(tailor.getOrderName(), tailor.getBedNumber(), tailor.getPackageNumber(), procedureNumber);
        if (pieceWorkList1 != null && pieceWorkList1.size() == 1){
            map.put("msg", "重复计件");
            map.put("code",4);
            return map;
        }
        PieceWork pieceWork = new PieceWork(employeeNumber,employeeName,groupName,tailor.getOrderName(),tailor.getClothesVersionNumber(), tailor.getBedNumber(), tailor.getColorName(), tailor.getSizeName(),tailor.getPackageNumber(), tailor.getLayerCount(),procedureNumber,orderProcedure.getProcedureName(),tailor.getTailorQcodeID());
        pieceWorkList.add(pieceWork);
        int res1 = pieceWorkService.addPieceWorkBatchNew(pieceWorkList);
        if (res1 == 0){
            map.put("msg", "计件成功");
            map.put("code",0);
            return map;
        }else {
            map.put("msg", "计件失败");
            map.put("code",1);
            return map;
        }
    }

//    @RequestMapping(value = "/miniaddotherpieceworkbatch",method = RequestMethod.POST)
//    @ResponseBody
//    public Map<String, Object> miniAddOtherPieceWorkBatch(@RequestParam("tailorQcodeID") Integer tailorQcodeID,
//                                                          @RequestParam("employeeNumber") String employeeNumber,
//                                                          @RequestParam("employeeName") String employeeName,
//                                                          @RequestParam("procedureNumber") Integer procedureNumber,
//                                                          @RequestParam(value = "isOutBound", required = false) boolean isOutBound,
//                                                          @RequestParam(value = "outBoundGroupName", required = false) String outBoundGroupName,
//                                                          @RequestParam("groupName") String groupName){
//        Map<String, Object> map = new LinkedHashMap<>();
//        Tailor tailor = new Tailor();
//        Tailor tailor1 = tailorService.getTailorByTailorQcodeID(tailorQcodeID, null);
//        if (tailor1 == null){
//            FinishTailor finishTailor = finishTailorService.getFinishTailorByTailorQcodeID(tailorQcodeID);
//            if (finishTailor == null){
//                map.put("msg", "二维码信息不存在");
//                map.put("code", 2);
//                return map;
//            } else {
//                tailor.setOrderName(finishTailor.getOrderName());
//                tailor.setCustomerName(finishTailor.getCustomerName());
//                tailor.setClothesVersionNumber(finishTailor.getClothesVersionNumber());
//                tailor.setLayerCount(finishTailor.getLayerCount());
//                tailor.setInitCount(finishTailor.getInitCount());
//                tailor.setBedNumber(finishTailor.getBedNumber());
//                tailor.setPackageNumber(finishTailor.getPackageNumber());
//                tailor.setPartName(finishTailor.getPartName());
//                tailor.setTailorQcodeID(finishTailor.getTailorQcodeID());
//                tailor.setTailorQcode(finishTailor.getTailorQcode());
//                tailor.setColorName(finishTailor.getColorName());
//                tailor.setSizeName(finishTailor.getSizeName());
//            }
//        } else {
//            tailor.setOrderName(tailor1.getOrderName());
//            tailor.setCustomerName(tailor1.getCustomerName());
//            tailor.setClothesVersionNumber(tailor1.getClothesVersionNumber());
//            tailor.setLayerCount(tailor1.getLayerCount());
//            tailor.setInitCount(tailor1.getInitCount());
//            tailor.setBedNumber(tailor1.getBedNumber());
//            tailor.setPackageNumber(tailor1.getPackageNumber());
//            tailor.setPartName(tailor1.getPartName());
//            tailor.setTailorQcodeID(tailor1.getTailorQcodeID());
//            tailor.setTailorQcode(tailor1.getTailorQcode());
//            tailor.setColorName(tailor1.getColorName());
//            tailor.setSizeName(tailor1.getSizeName());
//        }
//        ProcedureInfo procedureInfo = orderProcedureService.getProcedureInfoByOrderProcedureNumber(tailor.getOrderName(), procedureNumber);
//        if (procedureInfo == null){
//            map.put("todayCount", 0);
//            map.put("todayMoney", 0);
//            map.put("msg", "该制单无此工序,请联系IE");
//            map.put("tailor",tailor);
//            map.put("code",3);
//            return map;
//        }
//        //直接入库再出库
//        if (isOutBound){
//            if (tailor != null && tailor.getPartName().contains("主身")){
//                boolean flag = true;
//                int pn = 0;
//                if (procedureInfo.getProcedureName().contains("配扎")){
//                    flag = false;
//                }
//                if (flag){
//                    map.put("todayCount", 0);
//                    map.put("todayMoney", 0);
//                    map.put("msg", "选择的不是配扎工序");
//                    map.put("tailor",tailor);
//                    map.put("code",3);
//                    return map;
//                }
//                List<PieceWork> pieceWorkList1 = pieceWorkService.getPieceWorkByOrderBedPack(tailor.getOrderName(), tailor.getBedNumber(), tailor.getPackageNumber(), procedureNumber);
//                if (pieceWorkList1 != null && pieceWorkList1.size() == 1){
//                    map.put("todayCount", 0);
//                    map.put("todayMoney", 0);
//                    map.put("msg", "重复计件");
//                    map.put("tailor",tailor);
//                    map.put("code",4);
//                    return map;
//                }
//                List<PieceWork> pieceWorkList = new ArrayList<>();
//                PieceWork pieceWork = new PieceWork(employeeNumber,employeeName,groupName,tailor.getOrderName(),tailor.getClothesVersionNumber(), tailor.getBedNumber(), tailor.getColorName(), tailor.getSizeName(),tailor.getPackageNumber(), tailor.getLayerCount(),pn,"配扎",tailor.getTailorQcodeID());
//                pieceWorkList.add(pieceWork);
//                List<EmbStorage> embStorageList = new ArrayList<>();
//                List<EmbOutStore> embOutStoreList = new ArrayList<>();
//                EmbStorage embStorage = new EmbStorage("B1-1-1", tailor.getOrderName(), tailor.getBedNumber(), tailor.getColorName(), tailor.getSizeName(), tailor.getLayerCount(), tailor.getPackageNumber(), tailor.getTailorQcode());
//                embStorageList.add(embStorage);
//                int res1 = pieceWorkService.addPieceWorkBatchNew(pieceWorkList);
//                int res2 = embInStoreService.addEmbInStore(embStorageList);
//                EmbOutStore eos = new EmbOutStore(outBoundGroupName,tailor.getOrderName(), tailor.getBedNumber(), tailor.getColorName(), tailor.getSizeName(), tailor.getLayerCount(), tailor.getPackageNumber(), tailor.getTailorQcode());
//                embOutStoreList.add(eos);
//                int res3 = embOutStoreService.addEmbOutStore(embOutStoreList);
//                if (res1 == 0 && res2 == 0 && res3 == 0){
//                    map.put("todayCount", 0);
//                    map.put("todayMoney", 0);
//                    map.put("msg", "计件成功");
//                    map.put("tailor",tailor);
//                    map.put("code",0);
//                    return map;
//                }else {
//                    map.put("todayCount", 0);
//                    map.put("todayMoney", 0);
//                    map.put("msg", "计件失败");
//                    map.put("tailor",tailor);
//                    map.put("code",1);
//                    return map;
//                }
//            } else {
//                map.put("todayCount", 0);
//                map.put("todayMoney", 0);
//                map.put("msg", "请扫描主身");
//                map.put("code",7);
//                return map;
//            }
//        }
//        List<PieceWork> pieceWorkList = new ArrayList<>();
//        List<PieceWorkEmp> pieceWorkEmpList = pieceWorkService.getProcedureInfoByOrderBedPackage(tailor.getOrderName(), tailor.getBedNumber(), tailor.getPackageNumber());
//        map.put("pieceWorkSummary", pieceWorkEmpList);
//        map.put("tailor", tailor);
//        if (!tailor.getPartName().contains(procedureInfo.getScanPart())){
//            map.put("todayCount", 0);
//            map.put("todayMoney", 0);
//            map.put("msg", "扫描部位不正确");
//            map.put("code",6);
//            return map;
//        }
//        List<PieceWork> pieceWorkList1 = pieceWorkService.getPieceWorkByOrderBedPack(tailor.getOrderName(), tailor.getBedNumber(), tailor.getPackageNumber(), procedureNumber);
//        if (pieceWorkList1 != null && pieceWorkList1.size() == 1){
//            map.put("todayCount", 0);
//            map.put("todayMoney", 0);
//            map.put("msg", "重复计件");
//            map.put("code",4);
//            return map;
//        }
//        PieceWork pieceWork = new PieceWork(employeeNumber,employeeName,groupName,tailor.getOrderName(),tailor.getClothesVersionNumber(), tailor.getBedNumber(), tailor.getColorName(), tailor.getSizeName(),tailor.getPackageNumber(), tailor.getLayerCount(),procedureNumber,procedureInfo.getProcedureName(),tailor.getTailorQcodeID());
//        pieceWorkList.add(pieceWork);
//        int res1 = pieceWorkService.addPieceWorkBatchNew(pieceWorkList);
//        map.put("todayCount", 0);
//        map.put("todayMoney", 0);
//        if (res1 == 0){
//            map.put("msg", "计件成功");
//            map.put("code",0);
//            return map;
//        }else {
//            map.put("msg", "计件失败");
//            map.put("code",1);
//            return map;
//        }
//    }


    @RequestMapping(value = "/miniaddotherpieceworkbatch",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> miniAddOtherPieceWorkBatch(OtherPieceDto otherPieceDto){
        Map<String, Object> map = new LinkedHashMap<>();
        Tailor tailor = new Tailor();
        String employeeNumber = otherPieceDto.getEmployeeNumber();
        String employeeName = otherPieceDto.getEmployeeName();
        String groupName = otherPieceDto.getGroupName();
        String outBoundGroupName = otherPieceDto.getOutBoundGroupName();
        Tailor tailor1 = tailorService.getTailorByTailorQcodeID(otherPieceDto.getTailorQcodeID(), null);
        if (tailor1 == null){
            FinishTailor finishTailor = finishTailorService.getFinishTailorByTailorQcodeID(otherPieceDto.getTailorQcodeID());
            if (finishTailor == null){
                map.put("msg", "二维码信息不存在");
                map.put("code", 2);
                return map;
            } else {
                tailor.setOrderName(finishTailor.getOrderName());
                tailor.setCustomerName(finishTailor.getCustomerName());
                tailor.setClothesVersionNumber(finishTailor.getClothesVersionNumber());
                tailor.setLayerCount(finishTailor.getLayerCount());
                tailor.setInitCount(finishTailor.getInitCount());
                tailor.setBedNumber(finishTailor.getBedNumber());
                tailor.setPackageNumber(finishTailor.getPackageNumber());
                tailor.setPartName(finishTailor.getPartName());
                tailor.setTailorQcodeID(finishTailor.getTailorQcodeID());
                tailor.setTailorQcode(finishTailor.getTailorQcode());
                tailor.setColorName(finishTailor.getColorName());
                tailor.setSizeName(finishTailor.getSizeName());
            }
        } else {
            tailor.setOrderName(tailor1.getOrderName());
            tailor.setCustomerName(tailor1.getCustomerName());
            tailor.setClothesVersionNumber(tailor1.getClothesVersionNumber());
            tailor.setLayerCount(tailor1.getLayerCount());
            tailor.setInitCount(tailor1.getInitCount());
            tailor.setBedNumber(tailor1.getBedNumber());
            tailor.setPackageNumber(tailor1.getPackageNumber());
            tailor.setPartName(tailor1.getPartName());
            tailor.setTailorQcodeID(tailor1.getTailorQcodeID());
            tailor.setTailorQcode(tailor1.getTailorQcode());
            tailor.setColorName(tailor1.getColorName());
            tailor.setSizeName(tailor1.getSizeName());
        }
        List<Integer> procedureNumberList = otherPieceDto.getProcedureNumberList();
        if (otherPieceDto.isOutBound()){
            if (procedureNumberList.size() != 1){
                map.put("todayCount", 0);
                map.put("todayMoney", 0);
                map.put("msg", "请只选择配扎工序");
                map.put("tailor",tailor);
                map.put("code",3);
                return map;
            }
            ProcedureInfo procedureInfo = orderProcedureService.getProcedureInfoByOrderProcedureNumber(tailor.getOrderName(), procedureNumberList.get(0));
            if (tailor != null && tailor.getPartName().contains("主身")){
                boolean flag = true;
                if (procedureInfo.getProcedureName().contains("配扎")){
                    flag = false;
                }
                if (flag){
                    map.put("todayCount", 0);
                    map.put("todayMoney", 0);
                    map.put("msg", "选择的不是配扎工序");
                    map.put("tailor",tailor);
                    map.put("code",3);
                    return map;
                }
                List<PieceWork> pieceWorkList1 = pieceWorkService.getPieceWorkByOrderBedPack(tailor.getOrderName(), tailor.getBedNumber(), tailor.getPackageNumber(), procedureInfo.getProcedureNumber());
                if (pieceWorkList1 != null && pieceWorkList1.size() == 1){
                    map.put("todayCount", 0);
                    map.put("todayMoney", 0);
                    map.put("msg", procedureInfo.getProcedureNumber() + "号工序重复计件");
                    map.put("tailor",tailor);
                    map.put("code",4);
                    return map;
                }
                List<PieceWork> pieceWorkList = new ArrayList<>();
                PieceWork pieceWork = new PieceWork(employeeNumber,employeeName,groupName,tailor.getOrderName(),tailor.getClothesVersionNumber(), tailor.getBedNumber(), tailor.getColorName(), tailor.getSizeName(),tailor.getPackageNumber(), tailor.getLayerCount(),procedureInfo.getProcedureNumber(),"配扎",tailor.getTailorQcodeID());
                pieceWorkList.add(pieceWork);
                List<EmbStorage> embStorageList = new ArrayList<>();
                List<EmbOutStore> embOutStoreList = new ArrayList<>();
                EmbStorage embStorage = new EmbStorage("B1-1-1", tailor.getOrderName(), tailor.getBedNumber(), tailor.getColorName(), tailor.getSizeName(), tailor.getLayerCount(), tailor.getPackageNumber(), tailor.getTailorQcode());
                embStorageList.add(embStorage);
                int res1 = pieceWorkService.addPieceWorkBatchNew(pieceWorkList);
                int res2 = embInStoreService.addEmbInStore(embStorageList);
                EmbOutStore eos = new EmbOutStore(outBoundGroupName,tailor.getOrderName(), tailor.getBedNumber(), tailor.getColorName(), tailor.getSizeName(), tailor.getLayerCount(), tailor.getPackageNumber(), tailor.getTailorQcode());
                embOutStoreList.add(eos);
                int res3 = embOutStoreService.addEmbOutStore(embOutStoreList);
                if (res1 == 0 && res2 == 0 && res3 == 0){
                    map.put("todayCount", 0);
                    map.put("todayMoney", 0);
                    map.put("msg", "计件成功");
                    map.put("tailor",tailor);
                    map.put("code",0);
                    return map;
                }else {
                    map.put("todayCount", 0);
                    map.put("todayMoney", 0);
                    map.put("msg", "计件失败");
                    map.put("tailor",tailor);
                    map.put("code",1);
                    return map;
                }
            } else {
                map.put("todayCount", 0);
                map.put("todayMoney", 0);
                map.put("msg", "请扫描主身");
                map.put("code",7);
                return map;
            }
        }
        List<PieceWork> pieceWorkList = new ArrayList<>();
        List<PieceWorkEmp> pieceWorkEmpList = pieceWorkService.getProcedureInfoByOrderBedPackage(tailor.getOrderName(), tailor.getBedNumber(), tailor.getPackageNumber());
        map.put("pieceWorkSummary", pieceWorkEmpList);
        map.put("tailor", tailor);
        for (Integer procedureNumber : procedureNumberList){
            ProcedureInfo procedureInfo = orderProcedureService.getProcedureInfoByOrderProcedureNumber(tailor.getOrderName(), procedureNumber);
            if (procedureInfo == null){
                map.put("todayCount", 0);
                map.put("todayMoney", 0);
                map.put("msg", "工序表中没有" + procedureNumber + "号工序");
                map.put("tailor",tailor);
                map.put("code",3);
                return map;
            }
            if (!tailor.getPartName().contains(procedureInfo.getScanPart())){
                map.put("todayCount", 0);
                map.put("todayMoney", 0);
                map.put("msg", procedureNumber + "号工序扫描部位不正确");
                map.put("code",6);
                return map;
            }
            List<PieceWork> pieceWorkList1 = pieceWorkService.getPieceWorkByOrderBedPack(tailor.getOrderName(), tailor.getBedNumber(), tailor.getPackageNumber(), procedureNumber);
            if (pieceWorkList1 != null && pieceWorkList1.size() == 1){
                map.put("todayCount", 0);
                map.put("todayMoney", 0);
                map.put("msg", procedureNumber + "号工序重复计件");
                map.put("code",4);
                return map;
            }
            PieceWork pieceWork = new PieceWork(employeeNumber,employeeName,groupName,tailor.getOrderName(),tailor.getClothesVersionNumber(), tailor.getBedNumber(), tailor.getColorName(), tailor.getSizeName(),tailor.getPackageNumber(), tailor.getLayerCount(),procedureNumber,procedureInfo.getProcedureName(),tailor.getTailorQcodeID());
            pieceWorkList.add(pieceWork);
        }
        int res1 = pieceWorkService.addPieceWorkBatchNew(pieceWorkList);
        map.put("todayCount", 0);
        map.put("todayMoney", 0);
        if (res1 == 0){
            map.put("msg", "计件成功");
            map.put("code",0);
            return map;
        }else {
            map.put("msg", "计件失败");
            map.put("code",1);
            return map;
        }
    }

    @RequestMapping(value = "/getdispatchbyordernameemployeenumber",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> miniGetDispatchByOrderEmployeeNumber(@RequestParam("orderName")String orderName,
                                                                   @RequestParam("employeeNumber")String employeeNumber){
        Map<String,Object> map = new LinkedHashMap<>();
        List<ProcedureInfo> procedureInfoEmpList = dispatchService.getProcedureInfoByOrderEmp(orderName, employeeNumber);
        map.put("dispatchList",procedureInfoEmpList);
        return map;
    }


    @RequestMapping(value = "/minigetpadproductionbyinfo", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> miniGetDetailProductionByInfo(@RequestParam("from") String from,
                                                             @RequestParam("to")String to,
                                                             @RequestParam("employeeNumber")String employeeNumber,
                                                             @RequestParam("type")String type){
        Map<String, Object> map = new HashMap<>();
        // 1. 扎号  2. 款号-工序-日期汇总 3. 款号-工序汇总  4. 款号-工序-日期-颜色-尺码汇总
        try{
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date fromDate = sdf.parse(from);
            Date toDate = sdf.parse(to);
            List<GeneralSalary> generalSalaryList = new ArrayList<>();
            if (type.equals("1")){
                generalSalaryList = pieceWorkService.getPackageDetailByInfo(fromDate, toDate, employeeNumber);
            } else if (type.equals("2")){
                generalSalaryList = pieceWorkService.getDetailProductionByInfo(from, to, employeeNumber, null, null, null);
            } else if (type.equals("3")) {
                generalSalaryList = pieceWorkService.getDetailProductionSummaryByInfo(from, to, employeeNumber, null, null, null);
            } else if (type.equals("4")) {
                generalSalaryList = pieceWorkService.getDetailProductionDetailByInfo(from, to, employeeNumber, null, null, null);
            }
            map.put("generalSalaryList", generalSalaryList);
            return map;
        }catch (ParseException e){
            e.printStackTrace();
        }
        return map;
    }


}
