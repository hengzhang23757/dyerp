package com.example.erp01.action;

import com.example.erp01.model.ClothesVersionProcess;
import com.example.erp01.service.ClothesVersionProcessService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "erp")
public class ClothesVersionProcessController {

    @Autowired
    private ClothesVersionProcessService clothesVersionProcessService;

    @RequestMapping("/clothesVersionProcessStart")
    public String clothesVersionProcessStart(){
        return "versionProcess/clothesVersionProcess";
    }

    @RequestMapping("/clothesVersionProcessDetailStart")
    public String addOrderStart(Model model, String orderName){
        model.addAttribute("orderName", orderName);
        return "versionProcess/fb_clothesVersionProcessDetail";
    }

    @RequestMapping(value = "/addclothesversionprocessbatch", method = RequestMethod.POST)
    @ResponseBody
    public int addClothesVersionProcessBatch(@RequestParam("clothesVersionProcessJson")String clothesVersionProcessJson){
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        List<ClothesVersionProcess> clothesVersionProcessList = gson.fromJson(clothesVersionProcessJson,new TypeToken<List<ClothesVersionProcess>>(){}.getType());
        return clothesVersionProcessService.addClothesVersionProcessBatch(clothesVersionProcessList);
    }

    @RequestMapping(value = "/getuniqueclothesversionprocess", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getUniqueClothesVersionProcess(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<ClothesVersionProcess> clothesVersionProcessList = clothesVersionProcessService.getUniqueClothesVersionProcess();
        map.put("clothesVersionProcessList",clothesVersionProcessList);
        return map;
    }

    @RequestMapping(value = "/getclothesversionprocessbyorder", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getClothesVersionProcessByOrder(@RequestParam("orderName")String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<ClothesVersionProcess> clothesVersionProcessList = clothesVersionProcessService.getClothesVersionProcessByOrder(orderName);
        map.put("clothesVersionProcessList",clothesVersionProcessList);
        return map;
    }

    @RequestMapping(value = "/deleteclothesversionprocessbyid", method = RequestMethod.POST)
    @ResponseBody
    public int deleteClothesVersionProcess(@RequestParam("clothesVersionProcessID")Integer clothesVersionProcessID){
        return clothesVersionProcessService.deleteClothesVersionProcess(clothesVersionProcessID);
    }

    @RequestMapping(value = "/deleteclothesversionprocessbyorder", method = RequestMethod.POST)
    @ResponseBody
    public int deleteClothesVersionProcessByOrder(@RequestParam("orderName")String orderName){
        return clothesVersionProcessService.deleteClothesVersionProcessByOrder(orderName);
    }

}
