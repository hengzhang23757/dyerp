package com.example.erp01.action;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.example.erp01.model.StyleImage;
import com.example.erp01.service.StyleImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
@RequestMapping(value = "erp")
public class StyleImageController {

    @Autowired
    private StyleImageService styleImageService;

    @RequestMapping(value = "/addstyleimage", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> addStyleImage(@RequestParam("orderName")String orderName,
                                             @RequestParam("clothesVersionNumber")String clothesVersionNumber,
                                             @RequestParam("imageText")String imageText){
        Map<String, Object> map = new LinkedHashMap<>();
        StyleImage styleImage = new StyleImage(orderName, clothesVersionNumber, imageText);
        int res = styleImageService.addStyleImage(styleImage);
        map.put("result", res);
        return map;
    }


    @RequestMapping(value = "/getstyleimagebyordername", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getStyleImageByOrderName(@RequestParam("orderName")String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        StyleImage styleImage = styleImageService.getStyleImageByOrder(orderName);
        if (styleImage != null){
            map.put("styleImage", styleImage);
        }
        return map;
    }


    @RequestMapping(value = "/wangEditorUploadImg")
    @ResponseBody
    public Map<String,Object> wangEditorUploadImg(@RequestParam(name = "file",required= false) MultipartFile file, HttpServletRequest request){
        Map<String,Object> result = new HashMap<String, Object>();
        try{
            /***
             * 下面贴key  4行
             */
            String endPoint = "http://oss-cn-beijing.aliyuncs.com";
            String accessKeyId = "LTAI4GJH7FZWj86YK3MDNv4Z";
            String accessKeySecret = "vznlyo2fKbrT86mWJMxetwf7IJcVsi";
            String bucketName = "hengdaerp";
            if(file != null) {
                String fileName = file.getOriginalFilename();
                String uuid = UUID.randomUUID().toString().replaceAll("-", "");
                String objectName = "dy/" + uuid + fileName;
                OSS ossClient = new OSSClientBuilder().build(endPoint, accessKeyId, accessKeySecret);
                InputStream input = file.getInputStream();
                ossClient.putObject(bucketName, objectName, input);
                Date expiration = new Date(System.currentTimeMillis() + 3600 * 1000 * 24 * 36500);
//                String url = ossClient.generatePresignedUrl(bucketName, objectName, expiration).toString();
                input.close();
                ossClient.shutdown();
                File downDir = new File("/home/uploads/");
                if(!downDir.exists()){
                    downDir.mkdirs();
                }
                File img = new File(downDir.getPath()+"/"+uuid + fileName);
                OutputStream os = new FileOutputStream(img);
                InputStream inputStream = file.getInputStream();
                int bytesRead = 0;
                byte[] buffer = new byte[8192];
                while ((bytesRead = inputStream.read(buffer, 0, 8192)) != -1) {
                    os.write(buffer, 0, bytesRead);
                }
                os.close();
                inputStream.close();
                result.put("errno",0);
                List list = new ArrayList();
                String path=request.getContextPath();
                String serverName=request.getServerName();
                String basePath = null;
                if (Character.isDigit(serverName.charAt(0))){
                    basePath=request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
                }else {
                    basePath="https://"+request.getServerName()+path+"/";
                }
                list.add(basePath+"uploads/"+uuid + fileName);
                result.put("data", list);
            }else {
                result.put("errno",-1);
            }
        }catch (IOException e){
            e.printStackTrace();
            result.put("errno",-1);
        }finally {

        }
        return result;
    }


}
