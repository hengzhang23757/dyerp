package com.example.erp01.action;

import com.example.erp01.model.Customer;
import com.example.erp01.model.EndProduct;
import com.example.erp01.model.EndProductPay;
import com.example.erp01.service.CustomerService;
import com.example.erp01.service.EndProductPayService;
import com.example.erp01.service.EndProductService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.SimpleDateFormat;
import java.util.*;

@Configuration      //1.主要用于标记配置类，兼备Component的效果。
@EnableScheduling   // 2.开启定时任务
@Controller
@RequestMapping(value = "erp")
public class EndProductPayController {

    @Autowired
    private EndProductPayService endProductPayService;
    @Autowired
    private EndProductService endProductService;
    @Autowired
    private CustomerService customerService;
    @RequestMapping("/endProductStart")
    public String endProductStart(){
        return "endProduct/endProduct";
    }

    @RequestMapping(value = "/getendproductpaysummary", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getEndProductAllTypeRecord(@RequestParam(value = "orderName", required = false)String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<EndProduct> endProductList = endProductService.getEndProductStorageSummary("正品", orderName);
        List<String> orderNameList = new ArrayList<>();
        for (EndProduct endProduct : endProductList){
            orderNameList.add(endProduct.getOrderName());
        }
        List<EndProduct> endAccountList = endProductService.getEndProductAccountByOrderNameList(orderNameList);
        for (EndProduct endProduct : endProductList){
            for (EndProduct endProduct1 : endAccountList){
                if (endProduct.getOrderName().equals(endProduct1.getOrderName())){
                    endProduct.setAccountCount(endProduct1.getAccountCount());
                    endProduct.setTotalMoney(endProduct.getPrice() * endProduct.getAccountCount());
                }
            }
        }
        map.put("endProductPayList", endProductList);
        return map;
    }

    @RequestMapping(value = "/addendproductpay", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> addEndProductPay(@RequestParam("endProductPayJson")String endProductPayJson){
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        EndProductPay endProductPay = gson.fromJson(endProductPayJson,new TypeToken<EndProductPay>(){}.getType());
        int res = endProductPayService.addEndProductPay(endProductPay);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/getpaycountbyordername", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getOutStoreCountByOrderName(@RequestParam("orderName")String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("payCount", endProductPayService.getEndProductPayCountByOrder(orderName));
        return map;
    }

    @RequestMapping(value = "/getendproductpaybyordername", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getEndProductPayByOrderName(@RequestParam("orderName")String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<EndProductPay> endProductPayList = endProductPayService.getEndProductPayByOrderName(orderName);
        map.put("data", endProductPayList);
        map.put("count", endProductPayList.size());
        map.put("code",0);
        return map;
    }

    @RequestMapping(value = "/deleteendproductpaybatch", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> deleteEndProductOutBatch(@RequestParam("idList")List<Integer> idList){
        Map<String, Object> map = new LinkedHashMap<>();
        int res = endProductPayService.deleteEndProductPayBatch(idList);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/getendproductpaybyinfo", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getEndProductPayByInfo(@RequestParam(value = "monthStr", required = false)String monthStr,
                                                      @RequestParam(value = "customerName", required = false)String customerName,
                                                      @RequestParam(value = "orderName", required = false)String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<EndProductPay> endProductPayList = endProductPayService.getEndProductPayByInfo(customerName, monthStr, orderName, null);
        map.put("data", endProductPayList);
        return map;
    }

    @RequestMapping(value = "/deleteendproductpay", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> deleteEndProductOutBatch(@RequestParam("id") Integer id){
        Map<String, Object> map = new LinkedHashMap<>();
        int res = endProductPayService.deleteEndProductPay(id);
        map.put("result", res);
        return map;
    }


//    @Scheduled(cron = "0 0 0 1,2,3 * ?")
//    @Scheduled(cron = "0 28 9 * * ?")
    public void monthTranslate() {
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
        String thisMonth = sdf.format(date);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.MONTH, -1);
        String lastMonth = sdf.format(calendar.getTime());
        List<EndProductPay> endProductPayListThisMonth = endProductPayService.getEndProductPayByInfo(null, thisMonth, null, "上月结转");
        if (endProductPayListThisMonth == null || endProductPayListThisMonth.isEmpty()){
            List<EndProductPay> endProductPayList = endProductPayService.monthSummaryByCustomer(lastMonth);
            List<Customer> customerList = customerService.getAllCustomer();
            List<EndProductPay> endProductPayList1 = new ArrayList<>();
            for (Customer customer : customerList){
                EndProductPay endProductPay1 = new EndProductPay();
                endProductPay1.setCustomerName(customer.getCustomerName());
                endProductPay1.setPayType("上月结转");
                endProductPay1.setPayMonth(thisMonth);
                endProductPay1.setPayDate(date);
                for (EndProductPay endProductPay : endProductPayList){
                    if (endProductPay.getCustomerName().equals(customer.getCustomerName())){
                        endProductPay1.setSumMoney(endProductPay1.getSumMoney() + endProductPay.getSumMoney());
                        endProductPay1.setIncreaseMoney(endProductPay1.getIncreaseMoney() + endProductPay.getIncreaseMoney());
                        endProductPay1.setDecreaseMoney(endProductPay1.getDecreaseMoney() + endProductPay.getDecreaseMoney());
                        endProductPay1.setBalanceMoney(endProductPay1.getBalanceMoney() + endProductPay.getBalanceMoney());
                    }
                }
                endProductPayList1.add(endProductPay1);
            }
            endProductPayService.addEndProductPayBatch(endProductPayList1);
        }
    }


}
