package com.example.erp01.action;

import com.example.erp01.model.*;
import com.example.erp01.service.*;
import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.NoUniqueBeanDefinitionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

@Controller
@RequestMapping(value = "erp")
public class FinishTailorController {

    @Autowired
    private FinishTailorService finishTailorService;

    @Autowired
    private MaxFinishTailorQcodeIDService maxFinishTailorQcodeIDService;

    @Autowired
    private OrderClothesService orderClothesService;

    @Autowired
    private TailorService tailorService;

    @Autowired
    private PieceWorkService pieceWorkService;

    private static int tmpPackageNumber;

    @RequestMapping("/finishTailorStart")
    public String finishTailorStart(Model model){
        model.addAttribute("bigMenuTag",16);
        model.addAttribute("menuTag",161);
        return "finishTailor/finishTailor";
    }
    @RequestMapping("/finishTailorPrintStart")
    public String finishTailorPrintStart(){
        return "finishTailor/finishReprint";
    }
    @RequestMapping("/finishTailorLeakStart")
    public String finishTailorLeakStart(Model model){
        model.addAttribute("bigMenuTag",16);
        model.addAttribute("menuTag",163);
        return "finishTailor/finishTailorLeak";
    }

    @RequestMapping("/addFinishTailorStart")
    public String addFinishTailorStart(Model model,String orderName){
        if(orderName == null) {
            model.addAttribute("type", "add");
        }else {
            model.addAttribute("orderName", orderName);
            model.addAttribute("type", "detail");
        }
        return "finishTailor/fb_addFinishTailor";
    }

    @RequestMapping("/finishTailorDeleteStart")
    public String finishTailorDeleteStart(Model model){
        model.addAttribute("bigMenuTag",16);
        model.addAttribute("menuTag",162);
        return "finishTailor/finishTailorDelete";
    }

    @RequestMapping(value = "/generatefinishtailordata", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> generateSingleFinishTailorData(@RequestParam("tailorDataJson")String tailorDataJson){
        Map<String, Object> map = new LinkedHashMap<>();
        JsonParser jsonParser = new JsonParser();
        try{
            JsonObject json = (JsonObject) jsonParser.parse(tailorDataJson);
            String orderName = json.get("orderName").getAsString();
            String partName = json.get("partName").getAsString();
            String clothesVersionNumber = json.get("clothesVersionNumber").getAsString();
            String customerName = json.get("customerName").getAsString();
            JsonObject jsonColorList = json.get("colorList").getAsJsonObject();
            JsonObject jsonLoColorList = json.get("loColorList").getAsJsonObject();
            int layerCount = Integer.parseInt(json.get("layerCount").getAsString());
            JsonObject jsonSizeNameList = json.get("sizeNameList").getAsJsonObject();
            JsonObject jsonSizeCountList = json.get("sizeCountList").getAsJsonObject();
            Iterator iterator1 = jsonSizeNameList.entrySet().iterator();
            Iterator iterator2 = jsonSizeCountList.entrySet().iterator();
            List<String> sizeNameList = new ArrayList<>();
            List<Integer> sizeCountList = new ArrayList<>();
            while (iterator1.hasNext()){
                Map.Entry entry = (Map.Entry) iterator1.next();
                sizeNameList.add(entry.getValue().toString().replace("\"",""));
            }
            while (iterator2.hasNext()){
                Map.Entry entry = (Map.Entry) iterator2.next();
                int tmpvalue = Integer.parseInt(entry.getValue().toString().replace("\"",""));
                sizeCountList.add(tmpvalue);
            }
            List<String> loColorList = new ArrayList<>();
            Iterator iterator3 = jsonLoColorList.entrySet().iterator();
            while(iterator3.hasNext()){
                Map.Entry entry = (Map.Entry) iterator3.next();
                loColorList.add(entry.getValue().toString().replace("\"",""));
            }

            List<String> colorList = new ArrayList<>();
            Iterator iterator5 = jsonColorList.entrySet().iterator();
            while(iterator5.hasNext()){
                Map.Entry entry = (Map.Entry) iterator5.next();
                colorList.add(entry.getValue().toString().replace("\"",""));
            }
            List<FinishTailor> finishTailorList = new ArrayList<>();
            tmpPackageNumber = 0;
            Integer tmpMaxPackageNumber = finishTailorService.getMaxPackageNumberByOrder(orderName);
            if(tmpMaxPackageNumber != null){
                tmpPackageNumber = tmpMaxPackageNumber;
            }
            synchronized (this) {
                int tmpMaxTailorQcodeID = maxFinishTailorQcodeIDService.getMaxTailorQcodeId();
                int tmpNumber = tmpPackageNumber;
                for (int i = 0; i < sizeNameList.size(); i++) {
                    for (int j = 0; j < sizeCountList.get(i); j++) {
                        for (int k = 0; k < colorList.size(); k++){
                            String colorName = colorList.get(k);
                            String sizeName = sizeNameList.get(i);
                            if (partName.equals("成品") || partName.equals("包装")){
                                int wellTailorCount = tailorService.getWellCountByInfo(orderName, colorName, sizeNameList.get(i), null, null, null, 0);
                                int finishTailorCount = finishTailorService.getFinishTailorCountByOrderColorSize(orderName, colorName, sizeNameList.get(i), partName);
                                int thisTailorCount = layerCount * sizeCountList.get(i);
                                if (finishTailorCount + thisTailorCount > wellTailorCount) {
                                    String msg = "颜色:" + colorName + "尺码:" + sizeName + "超过好片数！";
                                    map.put("msg", msg);
                                    return map;
                                }
                            }
                            tmpNumber += 1;
                            tmpMaxTailorQcodeID += 1;
                            if (partName.equals("成品")){
                                String tmpTailorQcode = orderName + "-" + customerName + "-1000-" + "xxx" + "-" + colorName + "-" + sizeName + "-" + layerCount + "-" + tmpNumber + "-" + partName;
                                FinishTailor finishTailor = new FinishTailor(orderName, clothesVersionNumber, customerName, 1000, "xxx", colorName, sizeName, partName, layerCount, layerCount, tmpNumber, tmpTailorQcode, tmpMaxTailorQcodeID, loColorList.get(k),0);
                                finishTailorList.add(finishTailor);
                            }else if (partName.equals("返工")){
                                String tmpTailorQcode = orderName + "-" + customerName + "-1001-" + "xxx" + "-" + colorName + "-" + sizeName + "-" + layerCount + "-" + tmpNumber + "-" + partName;
                                FinishTailor finishTailor = new FinishTailor(orderName, clothesVersionNumber, customerName, 1001, "xxx", colorName, sizeName, partName, layerCount, layerCount, tmpNumber, tmpTailorQcode, tmpMaxTailorQcodeID, loColorList.get(k),0);
                                finishTailorList.add(finishTailor);
                            }else if (partName.equals("包装")){
                                String tmpTailorQcode = orderName + "-" + customerName + "-1002-" + "xxx" + "-" + colorName + "-" + sizeName + "-" + layerCount + "-" + tmpNumber + "-" + partName;
                                FinishTailor finishTailor = new FinishTailor(orderName, clothesVersionNumber, customerName, 1002, "xxx", colorName, sizeName, partName, layerCount, layerCount, tmpNumber, tmpTailorQcode, tmpMaxTailorQcodeID, loColorList.get(k),0);
                                finishTailorList.add(finishTailor);
                            }

                        }
                    }
                }
                maxFinishTailorQcodeIDService.updateMaxTailorQcodeId(tmpMaxTailorQcodeID);
            }
            map.put("finishTailorList", finishTailorList);
            return map;
        }catch (JsonIOException e){
            e.printStackTrace();
        }catch (JsonSyntaxException e){
            e.printStackTrace();
        }
        return null;

    }

    @RequestMapping(value = "/savefinishtailordata")
    @ResponseBody
    public int saveFinishTailorData(@RequestParam("tailorList")String tailorList){
        Gson gson = new Gson();
        List<FinishTailor> finishTailorList = gson.fromJson(tailorList,new TypeToken<List<FinishTailor>>(){}.getType());
        for (FinishTailor finishTailor : finishTailorList){
            int initCount = finishTailor.getLayerCount();
            finishTailor.setInitCount(initCount);
        }
        int res = finishTailorService.saveTailorData(finishTailorList);
        return res;
    }

    @RequestMapping(value = "/getfinishtailorbyordercolorsize",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getFinishTailorByOrderColorSize(@RequestParam("orderName")String orderName,
                                                               @RequestParam(value = "colorName", required = false)String colorName,
                                                               @RequestParam(value = "sizeName", required = false)String sizeName,
                                                               @RequestParam("partName")String partName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<FinishTailor> finishTailorList = finishTailorService.getFinishTailorByOrderColorSizePart(orderName, colorName, sizeName, partName);
        List<Integer> tailorQcodeIDList = finishTailorService.getTailorQcodeIDByOrderColorSize(orderName, colorName, sizeName);
        for (FinishTailor finishTailor : finishTailorList){
            for (Integer tailorQcodeID : tailorQcodeIDList){
                if (tailorQcodeID.equals(finishTailor.getTailorQcodeID())){
                    finishTailor.setIsScan(1);
                }
            }
        }
        map.put("finishTailorList",finishTailorList);
        return map;
    }

    @RequestMapping(value = "/deletefinishtailorbyorderbedpack",method = RequestMethod.POST)
    @ResponseBody
    public int deleteFinishTailorByOrderBedPack(@RequestParam("orderName")String orderName,
                                          @RequestParam("packageNumberList")List<Integer> packageNumberList){
        int res = finishTailorService.deleteTailorByOrderBedPack(orderName, packageNumberList);
        return res;
    }


    @RequestMapping(value = "/searchfinishcutqueryleak",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> searchFinishCutQueryLeak(@RequestParam("orderName")String orderName,
                                                        @RequestParam("partName")String partName) {
        List<CutQueryLeak> orderInfoList = orderClothesService.getOrderInfoByName(orderName);
        List<CutQueryLeak> tailorInfoList = tailorService.getTailorInfoByOrderPartType(orderName, null, 0, null, null);
        List<CutQueryLeak> finishInfoList = finishTailorService.getFinishTailorInfo(orderName, partName);
        List<String> colorList = new ArrayList<>();
        List<String> sizeList = new ArrayList<>();
        for(CutQueryLeak cutQueryLeak : orderInfoList){
            if (!colorList.contains(cutQueryLeak.getColorName())){
                colorList.add(cutQueryLeak.getColorName());
            }
            if (!sizeList.contains(cutQueryLeak.getSizeName())){
                sizeList.add(cutQueryLeak.getSizeName());
            }
        }
        List<String> desiredOrder = Arrays.asList("XXXXXS","5XS","XXXXS","4XS","XXXS","3XS","XXS","2XS","XS","XS/25","xs","S","S/27","M","M/29","L","L/31","XL",
                "XL/33","XXL","2XL","XXL/36","XXXL","3XL","XXXL/38","4XL","XXXXL","5XL","XXXXXL","6XL","XXXXXXL","80","090","90","100","110","110/27",
                "120","120/29","130","130/31","140","140/33","150","150/36","155","160","165","170","175","180","185","190");
        Comparator<String> sizeOrder = Comparator.comparingInt(desiredOrder::indexOf);
        sizeList.sort(sizeOrder);
        Map<String, Object> colorMap = new LinkedHashMap<>();
        if(tailorInfoList==null || tailorInfoList.size()==0) {
            for (String color : colorList) {
                Map<String, Object> tmpSizeMap = new LinkedHashMap<>();
                for (String size : sizeList) {
                    int tmpOrderCount = 0;
                    int tmpCutCount = 0;
                    int tmpWellCount = 0;
                    int tmpFinishCount = 0;
                    for (CutQueryLeak cutQueryLeak1 : orderInfoList) {
                        if (cutQueryLeak1.getColorName().equals(color) && cutQueryLeak1.getSizeName().equals(size)) {
                            tmpOrderCount = cutQueryLeak1.getOrderCount();
                        }
                    }
                    if (finishInfoList != null && finishInfoList.size() > 0){
                        for (CutQueryLeak cutQueryLeak2 : finishInfoList){
                            if (cutQueryLeak2.getColorName().equals(color) && cutQueryLeak2.getSizeName().equals(size)) {
                                tmpFinishCount = cutQueryLeak2.getFinishCount();
                            }
                        }
                    }
                    CutWellBad cwb = new CutWellBad(tmpOrderCount,tmpCutCount,tmpWellCount,tmpFinishCount,tmpFinishCount-tmpWellCount);
                    tmpSizeMap.put(size, cwb);
                }
                colorMap.put(color, tmpSizeMap);
            }
        }else{
            for(CutQueryLeak cutQueryLeak : tailorInfoList){
                if (!colorList.contains(cutQueryLeak.getColorName())){
                    colorList.add(cutQueryLeak.getColorName());
                }
                if (!sizeList.contains(cutQueryLeak.getSizeName())){
                    sizeList.add(cutQueryLeak.getSizeName());
                }
            }
            sizeList.sort(sizeOrder);
            for (String color : colorList) {
                Map<String, Object> tmpSizeMap = new LinkedHashMap<>();
                for (String size : sizeList) {
                    int tmpOrderCount = 0;
                    int tmpCutCount = 0;
                    int tmpWellCount = 0;
                    int tmpFinishCount = 0;
                    for (CutQueryLeak cutQueryLeak1 : orderInfoList) {
                        if (cutQueryLeak1.getColorName().equals(color) && cutQueryLeak1.getSizeName().equals(size)) {
                            tmpOrderCount += cutQueryLeak1.getOrderCount();
                        }
                    }
                    for (CutQueryLeak cutQueryLeak2 : tailorInfoList) {
                        if (cutQueryLeak2.getColorName().equals(color) && cutQueryLeak2.getSizeName().equals(size)) {
                            tmpCutCount += cutQueryLeak2.getLayerCount();
                            tmpWellCount += cutQueryLeak2.getWellCount();
                        }
                    }
                    if (finishInfoList != null && finishInfoList.size() > 0){
                        for (CutQueryLeak cutQueryLeak3 : finishInfoList){
                            if (cutQueryLeak3.getColorName().equals(color) && cutQueryLeak3.getSizeName().equals(size)) {
                                tmpFinishCount = cutQueryLeak3.getFinishCount();
                            }
                        }
                    }
                    CutWellBad cwb = new CutWellBad(tmpOrderCount, tmpCutCount, tmpWellCount,tmpFinishCount,  tmpFinishCount - tmpWellCount);
                    tmpSizeMap.put(size, cwb);
                }
                colorMap.put(color, tmpSizeMap);
            }
        }
        return colorMap;

    }


    @RequestMapping(value = "/updatefinishtailordata", method = RequestMethod.POST)
    @ResponseBody
    public int updateTailorData(@RequestParam("finishTailorList")String finishTailorList){
        Gson gson = new Gson();
        List<FinishTailor> tailorList1 = gson.fromJson(finishTailorList,new TypeToken<List<FinishTailor>>(){}.getType());
        List<FinishTailor> tailorList2 = new ArrayList<>();
        for (FinishTailor tailor : tailorList1){
            String orderName = tailor.getOrderName();
            String customerName = tailor.getCustomerName();
            String bedNumber = tailor.getBedNumber().toString();
            String jarName = tailor.getJarName();
            String colorName = tailor.getColorName();
            String sizeName = tailor.getSizeName();
            String layerCount = tailor.getLayerCount().toString();
            String packageNumber = tailor.getPackageNumber().toString();
            String partName = tailor.getPartName();
            int printTimes = tailor.getPrintTimes();
            String tailorQcode = orderName+"-"+customerName+"-"+bedNumber+"-"+jarName+"-"+colorName+"-"+sizeName+"-"+layerCount+"-"+packageNumber+"-"+partName;
            FinishTailor tt = new FinishTailor(tailor.getTailorID(),tailor.getLayerCount(),tailorQcode,printTimes+1);
            tailorList2.add(tt);
        }
        return finishTailorService.updateTailorData(tailorList2);
    }


    @RequestMapping(value = "/minigetfinishtailorbyqcode",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> miniGetFinishTailorByQcode(@RequestParam("qrCode")Integer qrCode) {
        Map<String, Object> map = new LinkedHashMap<>();
        FinishTailor finishTailor = finishTailorService.getFinishTailorByTailorQcodeID(qrCode);
        if (finishTailor != null){
            map.put("finishTailor", finishTailor);
        }
        return map;
    }

    @RequestMapping(value = "/miniupdatefinishtailorincheck",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> miniUpdateFinishTailorInCheck(@RequestParam("finishTailorJson")String finishTailorJson,
                                                             @RequestParam("shelfName")String shelfName,
                                                             @RequestParam("shelfEmp")String shelfEmp) {
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        List<FinishTailor> finishTailorList = gson.fromJson(finishTailorJson,new TypeToken<List<FinishTailor>>(){}.getType());
        for (FinishTailor finishTailor : finishTailorList){
            finishTailor.setShelfName(shelfName);
            finishTailor.setShelfEmp(shelfEmp);
            finishTailor.setShelfInTime(new Date());
        }
        int res = finishTailorService.updateFinishTailorBatchInCheck(finishTailorList);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/minigetshelfstoragetailorbyshelfname",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> miniGetShelfStorageTailorByShelfName(@RequestParam("shelfName")String shelfName) {
        Map<String, Object> map = new LinkedHashMap<>();
        List<FinishTailor> finishTailorList = finishTailorService.getShelfStorageByShelfName(shelfName);
        if (finishTailorList != null && !finishTailorList.isEmpty()){
            map.put("finishTailorList", finishTailorList);
        }
        return map;
    }

    @RequestMapping(value = "/miniupdatefinishtailoroutcheck",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> miniUpdateFinishTailorOutCheck(@RequestParam("finishTailorJson")String finishTailorJson,
                                                              @RequestParam("shelfOutEmp")String shelfOutEmp) {
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy/MM/dd").create();
        List<FinishTailor> finishTailorList = gson.fromJson(finishTailorJson,new TypeToken<List<FinishTailor>>(){}.getType());
        for (FinishTailor finishTailor : finishTailorList){
            finishTailor.setShelfOut(1);
            finishTailor.setShelfOutEmp(shelfOutEmp);
            finishTailor.setShelfOutTime(new Date());
        }
        int res = finishTailorService.updateFinishTailorBatchOutCheck(finishTailorList);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/minireturnclothesshelf",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> miniReturnClothesShelf(@RequestParam("finishTailorJson")String finishTailorJson) {
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy/MM/dd").create();
        List<FinishTailor> finishTailorList = gson.fromJson(finishTailorJson,new TypeToken<List<FinishTailor>>(){}.getType());
        int res = finishTailorService.updateFinishTailorBatchReturnCheck(finishTailorList);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/miniclotheschecksearch",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> miniClothesCheckSearch(@RequestParam(value = "orderName", required = false)String orderName,
                                                      @RequestParam(value = "from", required = false)String from,
                                                      @RequestParam(value = "to", required = false)String to,
                                                      @RequestParam(value = "colorName", required = false)String colorName,
                                                      @RequestParam(value = "sizeName", required = false)String sizeName) {
        Map<String, Object> map = new LinkedHashMap<>();
        Map<String, Object> param = new HashMap<>();
        param.put("orderName", orderName);
        param.put("from", from);
        param.put("to", to);
        param.put("colorName", colorName);
        param.put("sizeName", sizeName);
        List<FinishTailor> finishTailorList = finishTailorService.getFinishTailorByInfo(param);
        map.put("finishTailorList", finishTailorList);
        return map;
    }

}
