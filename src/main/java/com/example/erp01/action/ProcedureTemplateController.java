package com.example.erp01.action;

import com.example.erp01.model.ProcedureTemplate;
import com.example.erp01.model.Tailor;
import com.example.erp01.service.OrderProcedureService;
import com.example.erp01.service.ProcedureLevelService;
import com.example.erp01.service.ProcedureTemplateService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

@Controller
@RequestMapping(value = "erp")
public class ProcedureTemplateController {

    @Autowired
    private ProcedureTemplateService procedureTemplateService;
    @Autowired
    private ProcedureLevelService procedureLevelService;
    @Autowired
    private OrderProcedureService orderProcedureService;

    private static Integer tmpMaxProcedureCode;

    @RequestMapping("/procedureTemplateStart")
    public String procedureTemplateStart(){
        return "ieInfo/procedureTemplate";
    }

    @RequestMapping("/addProcedureTemplateStart")
    public String addOrderProcedureStart(Model model){
        model.addAttribute("type", "add");
        return "miniProgram/fb_procedureTemplateAdd";
    }

    @RequestMapping(value = "/addproceduretemplate",method = RequestMethod.POST)
    @ResponseBody
    public int addProcedureTemplate(ProcedureTemplate procedureTemplate){
        tmpMaxProcedureCode = 1;
        if (procedureTemplateService.getMaxProcedureCode() != null){
            tmpMaxProcedureCode = procedureTemplateService.getMaxProcedureCode() + 1;
        }
        String level = procedureTemplate.getProcedureLevel();
        String uuid = UUID.randomUUID().toString();
        Float levelValue = procedureLevelService.getLevelValueByName(level);
        double SAM = procedureTemplate.getSAM();
        procedureTemplate.setPiecePrice(levelValue*SAM);
        procedureTemplate.setPackagePrice(levelValue*SAM*12);
        procedureTemplate.setProcedureCode(tmpMaxProcedureCode);
        procedureTemplate.setGuid(uuid);
        int res = procedureTemplateService.addProcedureTemplate(procedureTemplate);
        return res;
    }


    @RequestMapping(value = "/addproceduretemplatebatch",method = RequestMethod.POST)
    @ResponseBody
    public int addProcedureTemplateBatch(@RequestParam("procedureTemplateJson") String procedureTemplateJson){
        Gson gson = new Gson();
        List<ProcedureTemplate> procedureTemplateList = gson.fromJson(procedureTemplateJson,new TypeToken<List<ProcedureTemplate>>(){}.getType());
        tmpMaxProcedureCode = 1;
        if (procedureTemplateService.getMaxProcedureCode() != null){
            tmpMaxProcedureCode = procedureTemplateService.getMaxProcedureCode() + 1;
        }
        for (ProcedureTemplate procedureTemplate : procedureTemplateList){
            String uuid = UUID.randomUUID().toString();
            procedureTemplate.setGuid(uuid);
            procedureTemplate.setProcedureCode(tmpMaxProcedureCode);
            tmpMaxProcedureCode ++;
        }
        int res = procedureTemplateService.addProcedureTemplateBatch(procedureTemplateList);
        return res;
    }


    @RequestMapping(value = "/deleteproceduretemplate",method = RequestMethod.POST)
    @ResponseBody
    public int deleteProcedureTemplate(@RequestParam("procedureID") Integer procedureID){
        return procedureTemplateService.deleteProcedureTemplate(procedureID);
    }

    @RequestMapping(value = "/deleteproceduretemplatebatch", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> deleteProcedureTemplateBatch(@RequestParam("procedureIDList")List<Integer> procedureIDList){
        Map<String, Object> map = new LinkedHashMap<>();
        int res = procedureTemplateService.deleteProcedureTemplateBatch(procedureIDList);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/getallproceduretemplate",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getAllProcedureTemplate(@RequestParam(value = "procedureNumber", required = false) Integer procedureNumber,
                                                      @RequestParam(value = "procedureCode", required = false) Integer procedureCode,
                                                      @RequestParam(value = "subProcedureName", required = false) String subProcedureName,
                                                      @RequestParam(value = "subProcedureDescription", required = false) String subProcedureDescription,
                                                      @RequestParam(value = "subSegment", required = false) String subSegment,
                                                      @RequestParam(value = "subRemark", required = false) String subRemark,
                                                      @RequestParam(value = "page", required = false)int page,
                                                      @RequestParam(value = "limit", required = false)int limit){
        Map<String,Object> map = new HashMap<>();
        Map<String, Object> paramMaps = new HashMap<>();
        paramMaps.put("procedureNumber",procedureNumber);
        paramMaps.put("procedureCode",procedureCode);
        paramMaps.put("subProcedureName",subProcedureName);
        paramMaps.put("subProcedureDescription",subProcedureDescription);
        paramMaps.put("subSegment",subSegment);
        paramMaps.put("subRemark",subRemark);
        List<ProcedureTemplate> countList = procedureTemplateService.getAllProcedureTemplate(paramMaps);
        map.put("count",countList.size());
        paramMaps.put("page",(page-1)*limit);
        paramMaps.put("limit",limit);
        List<ProcedureTemplate> procedureTemplateList = procedureTemplateService.getAllProcedureTemplate(paramMaps);
        map.put("data",procedureTemplateList);
        map.put("code",0);
        map.put("msg",null);
        return map;
    }

    @RequestMapping(value = "/updateproceduretemplate",method = RequestMethod.POST)
    @ResponseBody
    public int updateFabric(ProcedureTemplate procedureTemplate){
        Integer procedureCode = procedureTemplate.getProcedureCode();
        int res1 = 0;
        if (procedureCode != null){
            res1 = orderProcedureService.updateOrderProcedureByProcedureCode(procedureTemplate);
        }
        int res2 = procedureTemplateService.updateProcedureTemplate(procedureTemplate);
        if (res1 == 0 && res2 == 0){
            return 0;
        }else {
            return 1;
        }
    }


    @RequestMapping(value = "/getproceduretemplatehintbyinfo", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getProcedureTemplateHintByInfo(@RequestParam(value = "procedureNumber", required = false) Integer procedureNumber,
                                                              @RequestParam(value = "subProcedureName", required = false) String subProcedureName,
                                                              @RequestParam(value = "subProcedureDescription", required = false) String subProcedureDescription,
                                                              @RequestParam(value = "subRemark", required = false) String subRemark,
                                                              @RequestParam(value = "page", required = false)int page,
                                                              @RequestParam(value = "limit", required = false)int limit
                                                              ){
        Map<String, Object> map = new LinkedHashMap<>();
        List<ProcedureTemplate> procedureTemplateList = new ArrayList<>();
        if ((procedureNumber == null || procedureNumber.equals("")) && (subProcedureName == null || subProcedureName.equals("")) && (subProcedureDescription == null || subProcedureDescription.equals("")) && (subRemark == null || subRemark.equals(""))){
            map.put("data", procedureTemplateList);
            map.put("count",procedureTemplateList.size());
            map.put("code",0);
            return map;
        } else {
            List<ProcedureTemplate> lists = procedureTemplateService.getProcedureTemplateHintByInfo(procedureNumber, subProcedureName, subProcedureDescription, subRemark,null,null);
            procedureTemplateList = procedureTemplateService.getProcedureTemplateHintByInfo(procedureNumber, subProcedureName, subProcedureDescription, subRemark,(page-1)*limit,limit);
            map.put("data", procedureTemplateList);
            map.put("count",lists.size());
            map.put("code",0);
            return map;
        }
    }

}
