package com.example.erp01.action;

import com.example.erp01.model.ClothesPrice;
import com.example.erp01.service.ClothesPriceService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "erp")
public class ClothesPriceController {

    @Autowired
    private ClothesPriceService clothesPriceService;

    @RequestMapping(value = "/addclothespricebatch", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> addClothesPriceBatch(@RequestParam("clothesPriceJson")String clothesPriceJson){
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        List<ClothesPrice> clothesPriceList = gson.fromJson(clothesPriceJson,new TypeToken<List<ClothesPrice>>(){}.getType());
        int res = clothesPriceService.addClothesPriceBatch(clothesPriceList);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/getclothespricebyorder", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getManufactureAccessoryByOrder(@RequestParam("orderName")String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<ClothesPrice> clothesPriceList = clothesPriceService.getClothesPriceByInfo(orderName, null);
        map.put("data", clothesPriceList);
        map.put("count",clothesPriceList.size());
        map.put("code",0);
        return map;
    }

    @RequestMapping(value = "/deleteclothespricebatch", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> deleteClothesPriceBatch(@RequestParam("idList")List<Integer> idList){
        Map<String, Object> map = new LinkedHashMap<>();
        int res = clothesPriceService.deleteClothesPriceBatch(idList);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/updateclothespricebatch", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> updateClothesPriceBatch(@RequestParam("clothesPriceJson")String clothesPriceJson){
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        List<ClothesPrice> clothesPriceList = gson.fromJson(clothesPriceJson,new TypeToken<List<ClothesPrice>>(){}.getType());
        int res = clothesPriceService.updateClothesPriceBatch(clothesPriceList);
        map.put("result", res);
        return map;
    }

}
