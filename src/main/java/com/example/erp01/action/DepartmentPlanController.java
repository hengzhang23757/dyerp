package com.example.erp01.action;

import com.example.erp01.model.DepartmentPlan;
import com.example.erp01.service.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.SimpleDateFormat;
import java.util.*;
@Configuration      //1.主要用于标记配置类，兼备Component的效果。
@EnableScheduling   // 2.开启定时任务
@Controller
@RequestMapping(value = "erp")
public class DepartmentPlanController {

    @Autowired
    private DepartmentPlanService departmentPlanService;
    @Autowired
    private FabricDetailService fabricDetailService;
    @Autowired
    private FabricStorageService fabricStorageService;
    @Autowired
    private LooseFabricService looseFabricService;
    @Autowired
    private OrderClothesService orderClothesService;
    @Autowired
    private TailorService tailorService;

    @RequestMapping("/departmentPlanStart")
    public String departmentPlanStart(){
        return "plan/departmentPlan";
    }

    @RequestMapping(value = "/adddepartmentplan", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> addDepartmentPlan(@RequestParam("departmentPlanJson")String departmentPlanJson){
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        DepartmentPlan departmentPlan = gson.fromJson(departmentPlanJson,new TypeToken<DepartmentPlan>(){}.getType());
        String planType = departmentPlan.getPlanType();
        if (planType.equals("是")){
            departmentPlan.setPlanOrder(1);
            List<DepartmentPlan> departmentPlanList = departmentPlanService.getDepartmentPlanByFinishState("未完成", null, null);
            for (int i = 0; i < departmentPlanList.size(); i ++){
                departmentPlanList.get(i).setPlanOrder(i + 2);
            }
            departmentPlanService.updateDepartmentPlanBatch(departmentPlanList);
        } else {
            departmentPlan.setPlanOrder(departmentPlanService.getMaxPlanOrder() + 1);
        }
        if (departmentPlanService.addDepartmentPlan(departmentPlan) == 0){
            map.put("success", "添加成功！");
        }else {
            map.put("error", "添加失败！");
        }
        return map;
    }

    @RequestMapping(value = "/deletedepartmentplan", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> deleteDepartmentPlan(@RequestParam("departmentPlanID")Integer departmentPlanID){
        Map<String, Object> map = new LinkedHashMap<>();
        if (departmentPlanService.deleteDepartmentPlan(departmentPlanID) == 0){
            map.put("success", "删除成功！");
        }else {
            map.put("error", "删除失败！");
        }
        return map;
    }

    @RequestMapping(value = "/topdepartmentplan", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> topDepartmentPlan(@RequestParam("departmentPlanID")Integer departmentPlanID){
        Map<String, Object> map = new LinkedHashMap<>();
        List<DepartmentPlan> departmentPlanList = departmentPlanService.getDepartmentPlanByFinishState("未完成", null, null);
        int order = 2;
        for (DepartmentPlan departmentPlan : departmentPlanList){
            if (!departmentPlan.getDepartmentPlanID().equals(departmentPlanID)){
                departmentPlan.setPlanOrder(order++);
            } else {
                departmentPlan.setPlanOrder(1);
            }
        }
        int res = departmentPlanService.updateDepartmentPlanBatch(departmentPlanList);
        if (res == 0){
            map.put("success", "置顶成功！");
        }else {
            map.put("error", "置顶失败！");
        }
        return map;
    }

    @RequestMapping(value = "/updepartmentplan", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> upDepartmentPlan(@RequestParam("planOrder")Integer planOrder){
        Map<String, Object> map = new LinkedHashMap<>();
        List<DepartmentPlan> departmentPlanList = departmentPlanService.getDepartmentPlanByFinishState("未完成", null, null);
        List<DepartmentPlan> destDepartmentPlanList = new ArrayList<>();
        for (int i = 0; i < departmentPlanList.size(); i ++){
            int tmpOrder = departmentPlanList.get(i).getPlanOrder();
            if (tmpOrder == (planOrder - 1)){
                DepartmentPlan departmentPlan1 = departmentPlanList.get(i);
                departmentPlan1.setPlanOrder(planOrder);
                destDepartmentPlanList.add(departmentPlan1);
            } else if (tmpOrder == planOrder){
                DepartmentPlan departmentPlan2 = departmentPlanList.get(i);
                departmentPlan2.setPlanOrder(planOrder - 1);
                destDepartmentPlanList.add(departmentPlan2);
            }
        }
        int res = departmentPlanService.updateDepartmentPlanBatch(destDepartmentPlanList);
        if (res == 0){
            map.put("success", "上移成功！");
        }else {
            map.put("error", "上移失败！");
        }
        return map;
    }

    @RequestMapping(value = "/downdepartmentplan", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> downDepartmentPlan(@RequestParam("planOrder")Integer planOrder){
        Map<String, Object> map = new LinkedHashMap<>();
        List<DepartmentPlan> departmentPlanList = departmentPlanService.getDepartmentPlanByFinishState("未完成", null, null);
        boolean flag = false;
        for (DepartmentPlan departmentPlan : departmentPlanList){
            int tmpOrder = departmentPlan.getPlanOrder();
            if (tmpOrder > planOrder){
                flag = true;
            }
        }
        if (flag){
            List<DepartmentPlan> destDepartmentPlanList = new ArrayList<>();
            for (DepartmentPlan departmentPlan : departmentPlanList){
                int tmpOrder = departmentPlan.getPlanOrder();
                if (tmpOrder == planOrder){
                    departmentPlan.setPlanOrder(planOrder + 1);
                    destDepartmentPlanList.add(departmentPlan);
                } else if (tmpOrder == (planOrder + 1)){
                    departmentPlan.setPlanOrder(planOrder);
                    destDepartmentPlanList.add(departmentPlan);
                }
            }
            int res = departmentPlanService.updateDepartmentPlanBatch(destDepartmentPlanList);
            if (res == 0){
                map.put("success", "下移成功！");
            }else {
                map.put("error", "下移失败！");
            }
        } else {
            map.put("info", "到底了！");
        }
        return map;
    }

    @RequestMapping(value = "/finishdepartmentplan", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> finishDepartmentPlan(@RequestParam("departmentPlanID")Integer departmentPlanID){
        Map<String, Object> map = new LinkedHashMap<>();
        int res = departmentPlanService.setDepartmentPlanFinish(departmentPlanID);
        if (res == 0){
            map.put("success", "更新成功！");
        } else {
            map.put("error", "更新成功！");
        }
        return map;
    }

    @RequestMapping(value = "/updatedepartmentplan", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> updateDepartmentPlan(@RequestParam("departmentPlanJson")String departmentPlanJson){
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        DepartmentPlan departmentPlan = gson.fromJson(departmentPlanJson,new TypeToken<DepartmentPlan>(){}.getType());
        if (departmentPlanService.updateDepartmentPlan(departmentPlan) == 0){
            map.put("success", "更新成功！");
        }else {
            map.put("error", "更新失败！");
        }
        return map;
    }

    @RequestMapping(value = "/getdepartmentplanonemonth", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getDepartmentPlanOneMonth(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<DepartmentPlan> departmentPlanList = departmentPlanService.getDepartmentPlanOneMonth();
        map.put("data", departmentPlanList);
        map.put("code", 0);
        map.put("count", departmentPlanList.size());
        return map;
    }


    @RequestMapping(value = "/getplaninfobyordercolor", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getPlanInfoByOrderType(@RequestParam("orderName")String orderName,
                                                      @RequestParam("colorName")String colorName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> colorNameList = Arrays.asList(colorName.split(","));
        int inStoreBatch = fabricDetailService.getFabricBatchNumberByInfo(orderName, colorNameList);
        int orderCount = orderClothesService.getOrderCountByColorSizeList(orderName, colorNameList, null);
        int cutCount = tailorService.getWellCountByColorSizeList(orderName, colorNameList, null);
        int preCutBatch = looseFabricService.getLooseCountByOrder(orderName, colorNameList, 0);
        int cutBatch = looseFabricService.getLooseCountByOrder(orderName, colorNameList, 1);
        int storageBatch = fabricStorageService.getFabricBatchNumberByOrder(orderName, colorNameList);
        map.put("inStoreBatch", inStoreBatch);
        map.put("preCutBatch", preCutBatch);
        map.put("cutBatch", cutBatch);
        map.put("orderCount", orderCount);
        map.put("cutCount", cutCount);
        map.put("storageBatch", storageBatch);
        return map;
    }

    @RequestMapping(value = "/getplandetailbyordertype", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getPlanInfoByOrderType(@RequestParam("orderName")String orderName,
                                                      @RequestParam("beginDate")String beginDate,
                                                      @RequestParam("planType")String planType) throws Exception{
        Map<String, Object> map = new LinkedHashMap<>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date beginTime = sdf.parse(beginDate);
        List<DepartmentPlan> departmentPlanList;
        if (planType.equals("松布")){
            departmentPlanList = departmentPlanService.getLoosePlanDetail(orderName, beginTime);
            for (DepartmentPlan departmentPlan : departmentPlanList){
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(departmentPlan.getLooseTime());
                calendar.add(Calendar.HOUR, departmentPlan.getLooseHour());
                departmentPlan.setLooseTime(calendar.getTime());
            }
            map.put("data", departmentPlanList);
            map.put("code", 0);
            map.put("count", departmentPlanList.size());
        } else {
            departmentPlanList = departmentPlanService.getTailorPlanDetail(orderName, beginTime);
            map.put("data", departmentPlanList);
            map.put("code", 0);
            map.put("count", departmentPlanList.size());
        }
        return map;
    }



}
