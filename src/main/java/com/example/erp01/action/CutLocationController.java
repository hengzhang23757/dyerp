package com.example.erp01.action;

import com.example.erp01.model.CutLocation;
import com.example.erp01.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "erp")
public class CutLocationController {

    @Autowired
    private TailorService tailorService;

    @Autowired
    private CutInStoreService cutInStoreService;

    @Autowired
    private EmbInStoreService embInStoreService;

    @Autowired
    private EmbOutStoreService embOutStoreService;

    @Autowired
    private OrderClothesService orderClothesService;

    @Autowired
    private PieceWorkService pieceWorkService;


    @RequestMapping("/cutLocationStart")
    public String cutLocationStart(Model model){
        model.addAttribute("bigMenuTag",7);
        model.addAttribute("menuTag",76);
        return "report/cutLocation";
    }

    @RequestMapping(value = "/getdetailcutlocation",method = RequestMethod.GET)
    public String getDetailCutLocation(Model model,@RequestParam("orderName") String orderName,
                                       @RequestParam("bedNumber") String bedNumber,
                                       @RequestParam("colorName") String colorName,
                                       @RequestParam("sizeName") String sizeName){
        Integer bn;
        if ("".equals(colorName)){
            colorName = null;
        }
        if ("".equals(sizeName)){
            sizeName = null;
        }
        if ("".equals(bedNumber)){
            bn = null;
        }else {
            bn = Integer.parseInt(bedNumber);
        }
        List<CutLocation> cutLocationList1 = tailorService.getPackageInfo(orderName, bn, colorName, sizeName, null);
        List<CutLocation> cutLocationList2 = cutInStoreService.getCutInfo(orderName, bn, colorName, sizeName, null);
        List<CutLocation> cutLocationList3 = embInStoreService.getEmbInfo(orderName, bn, colorName, sizeName, null);
        List<CutLocation> cutLocationList4 = embOutStoreService.getEmbOutInfo(orderName, bn, colorName, sizeName, null);
        List<CutLocation> cutLocationList5 = pieceWorkService.getPieceWorkInfo(orderName, bn, colorName, sizeName);
        List<CutLocation> cutLocationList6 = pieceWorkService.getMatchInfo(orderName, bn, colorName, sizeName);
        int cutLayerCount = 0;
        int cutCount = cutLocationList1.size();
        int cutLocationCount = 0;
        int embLocationCount = 0;
        int workshopCount = 0;
        int pieceWorkCount = 0;
        int matchCount = 0;
        for (CutLocation cutLocation1 : cutLocationList1){
            if (cutLocation1.getLayerCount() != null){
                cutLayerCount += cutLocation1.getLayerCount();
            }
            cutLocation1.setClothesVersionNumber(orderClothesService.getVersionNumberByOrderName(cutLocation1.getOrderName()));
            for (CutLocation cutLocation2 : cutLocationList2){
                if (cutLocation1.getBedNumber().equals(cutLocation2.getBedNumber()) && cutLocation1.getPackageNumber().equals(cutLocation2.getPackageNumber())){
                    cutLocation1.setCutLocation(cutLocation2.getCutLocation());
                    cutLocationCount ++;
                }
            }
            for (CutLocation cutLocation3 : cutLocationList3){
                if (cutLocation1.getBedNumber().equals(cutLocation3.getBedNumber()) && cutLocation1.getPackageNumber().equals(cutLocation3.getPackageNumber())){
                    cutLocation1.setEmbLocation(cutLocation3.getEmbLocation());
                    embLocationCount ++;
                }

            }
            for (CutLocation cutLocation4 : cutLocationList4){
                if (cutLocation1.getBedNumber().equals(cutLocation4.getBedNumber()) && cutLocation1.getPackageNumber().equals(cutLocation4.getPackageNumber())){
                    cutLocation1.setWorkshop(cutLocation4.getWorkshop());
                    workshopCount ++;
                }

            }
            for (CutLocation cutLocation5 : cutLocationList5){
                if (cutLocation1.getBedNumber().equals(cutLocation5.getBedNumber()) && cutLocation1.getPackageNumber().equals(cutLocation5.getPackageNumber())){
                    cutLocation1.setPieceWork("1");
                    pieceWorkCount ++;
                }
            }
            for (CutLocation cutLocation6 : cutLocationList6){
                if (cutLocation1.getBedNumber().equals(cutLocation6.getBedNumber()) && cutLocation1.getPackageNumber().equals(cutLocation6.getPackageNumber())){
                    cutLocation1.setMatchWork("1");
                    matchCount ++;
                }
            }
        }
        model.addAttribute("cutLayerCount",cutLayerCount);
        model.addAttribute("cutLocationList",cutLocationList1);
        model.addAttribute("cutCount",cutCount);
        model.addAttribute("cutLocationCount",cutLocationCount);
        model.addAttribute("embLocationCount",embLocationCount);
        model.addAttribute("workshopCount",workshopCount);
        model.addAttribute("pieceWorkCount",pieceWorkCount);
        model.addAttribute("matchCount",matchCount);
        return "report/fb_cutLocationList";
    }


}
