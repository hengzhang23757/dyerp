package com.example.erp01.action;

import com.example.erp01.model.ProcessRequirement;
import com.example.erp01.service.ProcessRequirementService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "erp")
public class ProcessRequirementController {

    @Autowired
    private ProcessRequirementService processRequirementService;

    @RequestMapping(value = "/addprocessrequirementbatch", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> addProcessRequirementBatch(@RequestParam("processRequirementJson")String processRequirementJson){
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        List<ProcessRequirement> manufactureFabricList = gson.fromJson(processRequirementJson,new TypeToken<List<ProcessRequirement>>(){}.getType());
        int res = processRequirementService.addProcessRequirementBatch(manufactureFabricList);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/addprocessrequirement", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> addProcessRequirement(@RequestParam("orderName")String orderName,
                                                     @RequestParam("clothesVersionNumber")String clothesVersionNumber,
                                                     @RequestParam("processRequirementImg")String processRequirementImg){
        Map<String, Object> map = new LinkedHashMap<>();
        ProcessRequirement processRequirement = new ProcessRequirement();
        processRequirement.setOrderName(orderName);
        processRequirement.setClothesVersionNumber(clothesVersionNumber);
        processRequirement.setProcessRequirementImg(processRequirementImg);
        int res = processRequirementService.addProcessRequirement(processRequirement);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/getprocessrequirementbyorder", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getProcessRequirementByOrder(@RequestParam("orderName")String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<ProcessRequirement> processRequirementList = processRequirementService.getProcessRequirementByOrder(orderName);
        map.put("data", processRequirementList);
        map.put("size", processRequirementList.size());
        map.put("code", 0);
        return map;
    }

    @RequestMapping(value = "/deleteprocessrequirementbyorder", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> deleteProcessRequirementByName(@RequestParam("orderName")String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        int res = processRequirementService.deleteProcessRequirementByOrder(orderName);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/deleteprocessrequirementbyid", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> deleteProcessRequirementByID(@RequestParam("processRequirementID")Integer processRequirementID){
        Map<String, Object> map = new LinkedHashMap<>();
        int res = processRequirementService.deleteProcessRequirementByID(processRequirementID);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/updateprocessrequirement", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> updateProcessRequirement(ProcessRequirement processRequirement){
        Map<String, Object> map = new LinkedHashMap<>();
        int res = processRequirementService.updateProcessRequirement(processRequirement);
        map.put("result", res);
        return map;
    }


}
