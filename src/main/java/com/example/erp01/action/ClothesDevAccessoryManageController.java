package com.example.erp01.action;

import com.example.erp01.model.*;
import com.example.erp01.service.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.SimpleDateFormat;
import java.util.*;

@Controller
@RequestMapping(value = "erp")
public class ClothesDevAccessoryManageController {

    @Autowired
    private ClothesDevAccessoryManageService clothesDevAccessoryManageService;
    @Autowired
    private ManufactureAccessoryService manufactureAccessoryService;
    @Autowired
    private AccessoryInStoreService accessoryInStoreService;
    @Autowired
    private AccessoryStorageService accessoryStorageService;
    @Autowired
    private CheckMonthService checkMonthService;

    @RequestMapping("/clothesDevAccessoryManageStart")
    public String clothesDevAccessoryManageStart(){
        return "clothesDev/clothesDevAccessory";
    }

    @RequestMapping("/shareAccessoryStart")
    public String shareAccessoryStart(){
        return "share/shareAccessory";
    }

    @RequestMapping(value = "/getclothesdevaccessorymanagebyinfo",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getClothesDevAccessoryManageByInfo(@RequestParam(value = "accessoryName", required = false)String accessoryName,
                                                               @RequestParam(value = "accessoryNumber", required = false)String accessoryNumber,
                                                               @RequestParam(value = "accessoryID", required = false)Integer accessoryID,
                                                               @RequestParam(value = "storageType", required = false)String storageType){
        Map<String, Object> map = new LinkedHashMap<>();
        Map<String, Object> param = new HashMap<>();
        param.put("accessoryName", accessoryName);
        param.put("accessoryNumber", accessoryNumber);
        param.put("accessoryID", accessoryID);
        param.put("storageType", storageType);
        List<ClothesDevAccessoryManage> clothesDevAccessoryManageList = clothesDevAccessoryManageService.getClothesDevAccessoryManageByMap(param);
        map.put("data", clothesDevAccessoryManageList);
        map.put("count", clothesDevAccessoryManageList.size());
        map.put("code", 0);
        return map;
    }

    @RequestMapping(value = "/addclothesdevaccessorymanage",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> addClothesDevAccessoryManage(@RequestParam("clothesDevAccessoryManageJson")String clothesDevAccessoryManageJson){
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        ClothesDevAccessoryManage clothesDevAccessoryManage = gson.fromJson(clothesDevAccessoryManageJson, ClothesDevAccessoryManage.class);
        int res1 = clothesDevAccessoryManageService.addClothesDevAccessoryManage(clothesDevAccessoryManage);
        if (res1 == 0){
            map.put("result", 0);
            return map;
        } else {
            map.put("result", 1);
            return map;
        }
    }

    @RequestMapping(value = "/getclothesdevaccessorynamehint", method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getClothesDevAccessoryNameHint(@RequestParam("keywords")String keywords){
        Map<String,Object> map = new HashMap<>();
        List<String> accessoryNameList = clothesDevAccessoryManageService.getClothsDevAccessoryNameHint(keywords);
        map.put("content", accessoryNameList);
        map.put("code",0);
        map.put("type","success");
        return map;
    }

    @RequestMapping(value = "/getclothesdevaccessorynumberhint", method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getClothesDevAccessoryNumberHint(@RequestParam("keywords")String keywords){
        Map<String,Object> map = new HashMap<>();
        List<String> accessoryNumberList = clothesDevAccessoryManageService.getClothsDevAccessoryNumberHint(keywords);
        map.put("content", accessoryNumberList);
        map.put("code",0);
        map.put("type","success");
        return map;
    }

    @RequestMapping(value = "/getclothesdevaccessorymanagehint", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getManufactureAccessoryHintByFabricName(@RequestParam("subAccessoryName")String subAccessoryName
            ,@RequestParam("page")Integer page,@RequestParam("limit")Integer limit){
        Map<String, Object> map = new LinkedHashMap<>();
        List<ClothesDevAccessoryManage> countList = clothesDevAccessoryManageService.getClothesDevAccessoryManageHint(subAccessoryName,null,null);
        List<ClothesDevAccessoryManage> clothesDevAccessoryManageList = clothesDevAccessoryManageService.getClothesDevAccessoryManageHint(subAccessoryName,(page-1)*limit,limit);
        Collections.reverse(clothesDevAccessoryManageList);
        map.put("data",clothesDevAccessoryManageList);
        map.put("msg","");
        map.put("count",countList.size());
        map.put("code",0);
        return map;
    }

    @RequestMapping(value = "/getclothesdevaccessorymanagehintbynumber", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getClothesDevAccessoryManageHintByNumber(@RequestParam("subAccessoryNumber")String subAccessoryNumber
            ,@RequestParam("page")Integer page,@RequestParam("limit")Integer limit){
        Map<String, Object> map = new LinkedHashMap<>();
        List<ClothesDevAccessoryManage> countList = clothesDevAccessoryManageService.getClothesDevAccessoryManageHintByNumber(subAccessoryNumber,null,null);
        List<ClothesDevAccessoryManage> clothesDevAccessoryManageList = clothesDevAccessoryManageService.getClothesDevAccessoryManageHintByNumber(subAccessoryNumber,(page-1)*limit,limit);
        Collections.reverse(clothesDevAccessoryManageList);
        map.put("data",clothesDevAccessoryManageList);
        map.put("msg","");
        map.put("count",countList.size());
        map.put("code",0);
        return map;
    }

    @RequestMapping(value = "/deleteclothesdevaccessorymanagealltype", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> deleteClothesDevAccessoryManageAllType(@RequestParam("id")Integer id){
        Map<String, Object> map = new LinkedHashMap<>();
        int res1 = clothesDevAccessoryManageService.deleteClothesDevAccessory(id);
        int res2 = clothesDevAccessoryManageService.deleteClothesDevAccessoryRecordByAccessoryId(id);
        if (res1 == 0 && res2 == 0){
            map.put("result", 0);
        } else {
            map.put("result", 1);
        }
        return map;
    }

    @RequestMapping(value = "/clothesdevaccessorymanageinstore", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> clothesDevAccessoryManageInStore(@RequestParam("id")Integer id,
                                                                @RequestParam("inStoreCount")Float inStoreCount,
                                                                @RequestParam("price")Float price,
                                                                @RequestParam("payState")String payState,
                                                                @RequestParam("operateDate")String operateDate,
                                                                @RequestParam("location")String location) throws Exception{
        Map<String, Object> map = new LinkedHashMap<>();
        ClothesDevAccessoryManage clothesDevAccessoryManage = clothesDevAccessoryManageService.getClothesDevAccessoryManageById(id);
        ClothesDevAccessoryManage clothesDevFabricManageIn = new ClothesDevAccessoryManage(clothesDevAccessoryManage.getId(), clothesDevAccessoryManage.getAccessoryName(), clothesDevAccessoryManage.getAccessoryNumber(), clothesDevAccessoryManage.getSpecification(), clothesDevAccessoryManage.getAccessoryColor(), clothesDevAccessoryManage.getAccessoryColorNumber(), clothesDevAccessoryManage.getUnit(), clothesDevAccessoryManage.getPieceUsage(), price, inStoreCount, 0f, 0f, clothesDevAccessoryManage.getSupplier(),"in", clothesDevAccessoryManage.getRemark(), payState, location);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = simpleDateFormat.parse(operateDate);
        clothesDevFabricManageIn.setOperateDate(date);
        Map<String, Object> param = new HashMap<>();
        param.put("accessoryID", id);
        param.put("storageType", "storage");
        List<ClothesDevAccessoryManage> clothesDevAccessoryManageList = clothesDevAccessoryManageService.getClothesDevAccessoryManageByMap(param);
        int res1;
        if (clothesDevAccessoryManageList != null && !clothesDevAccessoryManageList.isEmpty()){
            ClothesDevAccessoryManage clothesDevAccessoryManage1 = clothesDevAccessoryManageList.get(0);
            clothesDevAccessoryManage1.setInStoreCount(clothesDevAccessoryManage1.getInStoreCount() + inStoreCount);
            clothesDevAccessoryManage1.setStorageCount(clothesDevAccessoryManage1.getStorageCount() + inStoreCount);
            res1 = clothesDevAccessoryManageService.updateClothesDevAccessoryManage(clothesDevAccessoryManage1);
        } else {
            ClothesDevAccessoryManage clothesDevAccessoryManage1 = new ClothesDevAccessoryManage(clothesDevAccessoryManage.getId(), clothesDevAccessoryManage.getAccessoryName(), clothesDevAccessoryManage.getAccessoryNumber(), clothesDevAccessoryManage.getSpecification(), clothesDevAccessoryManage.getAccessoryColor(), clothesDevAccessoryManage.getAccessoryColorNumber(), clothesDevAccessoryManage.getUnit(), clothesDevAccessoryManage.getPieceUsage(), price, inStoreCount, inStoreCount, 0f, clothesDevAccessoryManage.getSupplier(),"storage", clothesDevAccessoryManage.getRemark(), payState, location);
            clothesDevAccessoryManage1.setOperateDate(date);
            res1 = clothesDevAccessoryManageService.addClothesDevAccessoryManage(clothesDevAccessoryManage1);
        }
        int res2 = clothesDevAccessoryManageService.addClothesDevAccessoryManage(clothesDevFabricManageIn);
        if (res1 == 0 && res2 == 0){
            map.put("result", 0);
        } else {
            map.put("result", 1);
        }
        return map;
    }

    @RequestMapping(value = "/clothesdevaccessorymanageshareinstore", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> clothesDevAccessoryManageShareInStore(@RequestParam("id")Integer id,
                                                                     @RequestParam("inStoreCount")Float inStoreCount,
                                                                     @RequestParam("operateDate")String operateDate,
                                                                     @RequestParam("price")Float price,
                                                                     @RequestParam("payState")String payState,
                                                                     @RequestParam("location")String location,
                                                                     @RequestParam("remark")String remark) throws Exception{
        Map<String, Object> map = new LinkedHashMap<>();
        ClothesDevAccessoryManage clothesDevAccessoryManage = clothesDevAccessoryManageService.getClothesDevAccessoryManageById(id);
        ClothesDevAccessoryManage clothesDevFabricManageIn = new ClothesDevAccessoryManage(clothesDevAccessoryManage.getId(), clothesDevAccessoryManage.getAccessoryName(), clothesDevAccessoryManage.getAccessoryNumber(), clothesDevAccessoryManage.getSpecification(), clothesDevAccessoryManage.getAccessoryColor(), clothesDevAccessoryManage.getAccessoryColorNumber(), clothesDevAccessoryManage.getUnit(), clothesDevAccessoryManage.getPieceUsage(), price, inStoreCount, 0f, 0f, clothesDevAccessoryManage.getSupplier(),"shareIn", remark, payState, location);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = simpleDateFormat.parse(operateDate);
        clothesDevFabricManageIn.setOperateDate(date);
        Map<String, Object> param = new HashMap<>();
        param.put("accessoryID", id);
        param.put("storageType", "shareStorage");
        param.put("price", price);
        List<ClothesDevAccessoryManage> clothesDevAccessoryManageList = clothesDevAccessoryManageService.getClothesDevAccessoryManageByMap(param);
        int res1;
        if (clothesDevAccessoryManageList != null && !clothesDevAccessoryManageList.isEmpty()){
            ClothesDevAccessoryManage clothesDevAccessoryManage1 = clothesDevAccessoryManageList.get(0);
            clothesDevAccessoryManage1.setInStoreCount(clothesDevAccessoryManage1.getInStoreCount() + inStoreCount);
            clothesDevAccessoryManage1.setStorageCount(clothesDevAccessoryManage1.getStorageCount() + inStoreCount);
            res1 = clothesDevAccessoryManageService.updateClothesDevAccessoryManage(clothesDevAccessoryManage1);
        } else {
            ClothesDevAccessoryManage clothesDevAccessoryManage1 = new ClothesDevAccessoryManage(clothesDevAccessoryManage.getId(), clothesDevAccessoryManage.getAccessoryName(), clothesDevAccessoryManage.getAccessoryNumber(), clothesDevAccessoryManage.getSpecification(), clothesDevAccessoryManage.getAccessoryColor(), clothesDevAccessoryManage.getAccessoryColorNumber(), clothesDevAccessoryManage.getUnit(), clothesDevAccessoryManage.getPieceUsage(), price, inStoreCount, inStoreCount, 0f, clothesDevAccessoryManage.getSupplier(),"shareStorage", clothesDevAccessoryManage.getRemark(), payState, location);
            clothesDevAccessoryManage1.setOperateDate(date);
            res1 = clothesDevAccessoryManageService.addClothesDevAccessoryManage(clothesDevAccessoryManage1);
        }
        int res2 = clothesDevAccessoryManageService.addClothesDevAccessoryManage(clothesDevFabricManageIn);
        if (res1 == 0 && res2 == 0){
            map.put("result", 0);
        } else {
            map.put("result", 1);
        }
        return map;
    }

    @RequestMapping(value = "/clothesdevaccessorymanageoutstore", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> clothesDevAccessoryManageOutStore(@RequestParam("id")Integer id,
                                                                 @RequestParam("outStoreCount")Float outStoreCount){
        Map<String, Object> map = new LinkedHashMap<>();
        ClothesDevAccessoryManage clothesDevAccessoryManage = clothesDevAccessoryManageService.getClothesDevAccessoryManageById(id);
        ClothesDevAccessoryManage clothesDevAccessoryManageOut = new ClothesDevAccessoryManage(clothesDevAccessoryManage.getId(), clothesDevAccessoryManage.getAccessoryName(), clothesDevAccessoryManage.getAccessoryNumber(), clothesDevAccessoryManage.getSpecification(), clothesDevAccessoryManage.getAccessoryColor(), clothesDevAccessoryManage.getAccessoryColorNumber(), clothesDevAccessoryManage.getUnit(), clothesDevAccessoryManage.getPieceUsage(), clothesDevAccessoryManage.getPrice(), 0f, 0f, outStoreCount, clothesDevAccessoryManage.getSupplier(),"out", clothesDevAccessoryManage.getRemark(), clothesDevAccessoryManage.getPayState(), clothesDevAccessoryManage.getLocation());
        clothesDevAccessoryManageOut.setOperateDate(new Date());
        Map<String, Object> param = new HashMap<>();
        param.put("accessoryID", id);
        param.put("storageType", "storage");
        List<ClothesDevAccessoryManage> clothesDevAccessoryManageList = clothesDevAccessoryManageService.getClothesDevAccessoryManageByMap(param);
        int res1;
        if (clothesDevAccessoryManageList == null || clothesDevAccessoryManageList.isEmpty()){
            map.put("result", 4);
            return map;
        }
        ClothesDevAccessoryManage clothesDevAccessoryManageStorage = clothesDevAccessoryManageList.get(0);
        if (clothesDevAccessoryManageStorage.getStorageCount() < outStoreCount){
            map.put("result", 4);
            return map;
        }
        clothesDevAccessoryManageStorage.setOutStoreCount(clothesDevAccessoryManageStorage.getOutStoreCount() + outStoreCount);
        clothesDevAccessoryManageStorage.setStorageCount(clothesDevAccessoryManageStorage.getStorageCount() - outStoreCount);
        res1 = clothesDevAccessoryManageService.updateClothesDevAccessoryManage(clothesDevAccessoryManageStorage);
        int res2 = clothesDevAccessoryManageService.addClothesDevAccessoryManage(clothesDevAccessoryManageOut);
        if (res1 == 0 && res2 == 0){
            map.put("result", 0);
        } else {
            map.put("result", 1);
        }
        return map;
    }

    @RequestMapping(value = "/updateclothesdevaccessorymanagetotal", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> updateClothesDevAccessoryManageTotal(@RequestParam("id")Integer id,
                                                                    @RequestParam("clothesDevAccessoryManageJson")String clothesDevAccessoryManageJson){
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        ClothesDevAccessoryManage clothesDevAccessoryManage = gson.fromJson(clothesDevAccessoryManageJson, ClothesDevAccessoryManage.class);
        int res1 = clothesDevAccessoryManageService.updateClothesDevAccessoryManage(clothesDevAccessoryManage);
        Map<String, Object> param = new HashMap<>();
        param.put("accessoryID", id);
        List<ClothesDevAccessoryManage> clothesDevAccessoryManageList = clothesDevAccessoryManageService.getClothesDevAccessoryManageByMap(param);
        for (ClothesDevAccessoryManage clothesDevAccessoryManage1: clothesDevAccessoryManageList){
            clothesDevAccessoryManage1.setAccessoryName(clothesDevAccessoryManage.getAccessoryName());
            clothesDevAccessoryManage1.setAccessoryNumber(clothesDevAccessoryManage.getAccessoryNumber());
            clothesDevAccessoryManage1.setSpecification(clothesDevAccessoryManage.getSpecification());
            clothesDevAccessoryManage1.setAccessoryColor(clothesDevAccessoryManage.getAccessoryColor());
            clothesDevAccessoryManage1.setAccessoryColorNumber(clothesDevAccessoryManage.getAccessoryColorNumber());
            clothesDevAccessoryManage1.setUnit(clothesDevAccessoryManage.getUnit());
            clothesDevAccessoryManage1.setPieceUsage(clothesDevAccessoryManage.getPieceUsage());
//            clothesDevAccessoryManage1.setPrice(clothesDevAccessoryManage.getPrice());
            clothesDevAccessoryManage1.setSupplier(clothesDevAccessoryManage.getSupplier());
            clothesDevAccessoryManage1.setRemark(clothesDevAccessoryManage.getRemark());
        }
        int res2 = clothesDevAccessoryManageService.updateClothesDevAccessoryManageBatch(clothesDevAccessoryManageList);
        if (res1 == 0 && res2 == 0){
            map.put("result", 0);
        } else {
            map.put("result", 1);
        }
        return map;
    }


    @RequestMapping(value = "/deleteclothesdevaccessorymanageinbatch", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> deleteClothesDevAccessoryManageInBatch(@RequestParam("idList")List<Integer> idList,
                                                                      @RequestParam("id")Integer id,
                                                                      @RequestParam("inStoreCount")Float inStoreCount){
        Map<String, Object> map = new LinkedHashMap<>();
        Map<String, Object> param = new HashMap<>();
        param.put("accessoryID", id);
        param.put("storageType", "storage");
        List<ClothesDevAccessoryManage> clothesDevAccessoryManageList = clothesDevAccessoryManageService.getClothesDevAccessoryManageByMap(param);
        if (clothesDevAccessoryManageList == null || clothesDevAccessoryManageList.isEmpty()){
            map.put("result", 4);
            return map;
        }
        ClothesDevAccessoryManage clothesDevAccessoryManageStorage = clothesDevAccessoryManageList.get(0);
        if (clothesDevAccessoryManageStorage.getStorageCount() < inStoreCount){
            map.put("result", 4);
            return map;
        }
        clothesDevAccessoryManageStorage.setStorageCount(clothesDevAccessoryManageStorage.getStorageCount() - inStoreCount);
        int res1 = clothesDevAccessoryManageService.updateClothesDevAccessoryManage(clothesDevAccessoryManageStorage);
        int res2 = clothesDevAccessoryManageService.deleteClothesDevAccessoryManageBatch(idList);
        if (res1 == 0 && res2 == 0){
            map.put("result", 0);
        } else {
            map.put("result", 1);
        }
        return map;
    }

    @RequestMapping(value = "/deleteclothesdevaccessorymanageoutbatch", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> deleteClothesDevAccessoryManageOutBatch(@RequestParam("idList")List<Integer> idList,
                                                                       @RequestParam("id")Integer id,
                                                                       @RequestParam("outStoreCount")Float outStoreCount){
        Map<String, Object> map = new LinkedHashMap<>();
        Map<String, Object> param = new HashMap<>();
        param.put("accessoryID", id);
        param.put("storageType", "storage");
        List<ClothesDevAccessoryManage> clothesDevAccessoryManageList = clothesDevAccessoryManageService.getClothesDevAccessoryManageByMap(param);
        if (clothesDevAccessoryManageList == null || clothesDevAccessoryManageList.isEmpty()){
            map.put("result", 4);
            return map;
        }
        ClothesDevAccessoryManage clothesDevAccessoryManageStorage = clothesDevAccessoryManageList.get(0);
        clothesDevAccessoryManageStorage.setStorageCount(clothesDevAccessoryManageStorage.getStorageCount() + outStoreCount);
        int res1 = clothesDevAccessoryManageService.updateClothesDevAccessoryManage(clothesDevAccessoryManageStorage);
        int res2 = clothesDevAccessoryManageService.deleteClothesDevAccessoryManageBatch(idList);
        if (res1 == 0 && res2 == 0){
            map.put("result", 0);
        } else {
            map.put("result", 1);
        }
        return map;
    }

    //
    @RequestMapping(value = "/deleteclothesdevaccessorymanagesharein", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> deleteClothesDevAccessoryManageShareIn(@RequestParam("id")Integer id){
        Map<String, Object> map = new LinkedHashMap<>();
        ClothesDevAccessoryManage clothesDevAccessoryManageIn = clothesDevAccessoryManageService.getClothesDevAccessoryManageById(id);
        Map<String, Object> param = new HashMap<>();
        param.put("accessoryID", clothesDevAccessoryManageIn.getAccessoryID());
        param.put("storageType", "shareStorage");
        List<ClothesDevAccessoryManage> clothesDevAccessoryManageList = clothesDevAccessoryManageService.getClothesDevAccessoryManageByMap(param);
        if (clothesDevAccessoryManageList == null || clothesDevAccessoryManageList.isEmpty()){
            map.put("result", 4);
            return map;
        }
        ClothesDevAccessoryManage clothesDevAccessoryManageStorage = clothesDevAccessoryManageList.get(0);
        if (clothesDevAccessoryManageStorage.getStorageCount() < clothesDevAccessoryManageIn.getInStoreCount()){
            map.put("result", 4);
            return map;
        }
        clothesDevAccessoryManageStorage.setStorageCount(clothesDevAccessoryManageStorage.getStorageCount() - clothesDevAccessoryManageIn.getInStoreCount());
        int res1 = clothesDevAccessoryManageService.updateClothesDevAccessoryManage(clothesDevAccessoryManageStorage);
        int res2 = clothesDevAccessoryManageService.deleteClothesDevAccessory(id);
        if (res1 == 0 && res2 == 0){
            map.put("result", 0);
        } else {
            map.put("result", 1);
        }
        return map;
    }

//    @RequestMapping(value = "/useshareaccessorytoorder", method = RequestMethod.POST)
//    @ResponseBody
//    public Map<String, Object> useShareAccessoryToOrder(@RequestParam("id")Integer id,
//                                                        @RequestParam("accessoryID")Integer accessoryID,
//                                                        @RequestParam("location")String location,
//                                                        @RequestParam("useCount")Float useCount){
//        Map<String, Object> map = new LinkedHashMap<>();
//        ManufactureAccessory manufactureAccessory = manufactureAccessoryService.getManufactureAccessoryByID(accessoryID);
//        if (!manufactureAccessory.getCheckState().equals("通过")){
//            map.put("result", 3);
//            map.put("msg", manufactureAccessory.getAccessoryName() + "审核未通过,请先审核~~");
//            return map;
//        }
//        ClothesDevAccessoryManage clothesDevAccessoryManageStorage = clothesDevAccessoryManageService.getClothesDevAccessoryManageById(id);
//        clothesDevAccessoryManageStorage.setStorageCount(clothesDevAccessoryManageStorage.getStorageCount() - useCount);
//        int res1 = clothesDevAccessoryManageService.updateClothesDevAccessoryManage(clothesDevAccessoryManageStorage);
//        clothesDevAccessoryManageStorage.setOrderName(manufactureAccessory.getOrderName());
//        clothesDevAccessoryManageStorage.setClothesVersionNumber(manufactureAccessory.getClothesVersionNumber());
//        clothesDevAccessoryManageStorage.setStorageType("shareOut");
//        clothesDevAccessoryManageStorage.setOutStoreCount(useCount);
//
//        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
//        AccessoryInStore accessoryInStoreInit = new AccessoryInStore(manufactureAccessory.getOrderName(), manufactureAccessory.getClothesVersionNumber(), manufactureAccessory.getAccessoryName(), manufactureAccessory.getAccessoryNumber(), manufactureAccessory.getSpecification(), manufactureAccessory.getAccessoryUnit(), manufactureAccessory.getAccessoryColor(), manufactureAccessory.getPieceUsage(), manufactureAccessory.getSizeName(), manufactureAccessory.getColorName(), useCount, 0f, location, manufactureAccessory.getSupplier(), simpleDateFormat.format(new Date()), manufactureAccessory.getAccessoryID(), clothesDevAccessoryManageStorage.getPrice());
//        List<AccessoryInStore> accessoryInStoreList1 = accessoryInStoreService.getAccessoryInStoreByOrder(manufactureAccessory.getOrderName());
//        float accountCount = 0f;
//        float accessoryCount = manufactureAccessory.getAccessoryCount();
//        float inStoreCount = useCount;
//        for (AccessoryInStore accessoryInStore : accessoryInStoreList1){
//            if (accessoryInStore.getAccessoryID().equals(manufactureAccessory.getAccessoryID())){
//                accountCount += accessoryInStore.getAccountCount();
//            }
//        }
//        if (accountCount + inStoreCount <= accessoryCount){
//            accessoryInStoreInit.setAccountCount(inStoreCount);
//        } else {
//            accessoryInStoreInit.setAccountCount((accessoryCount - accountCount) <= 0 ? 0 : (accessoryCount - accountCount));
//        }
//        List<AccessoryInStore> accessoryInStoreList = new ArrayList<>();
//        accessoryInStoreList.add(accessoryInStoreInit);
//        AccessoryStorage accessoryStorageInit = new AccessoryStorage(manufactureAccessory.getOrderName(), manufactureAccessory.getClothesVersionNumber(), manufactureAccessory.getAccessoryName(), manufactureAccessory.getAccessoryNumber(), manufactureAccessory.getSpecification(), manufactureAccessory.getAccessoryUnit(), manufactureAccessory.getAccessoryColor(), manufactureAccessory.getPieceUsage(), manufactureAccessory.getSizeName(), manufactureAccessory.getColorName(), useCount, location, manufactureAccessory.getSupplier(), manufactureAccessory.getAccessoryID());
//        List<AccessoryStorage> accessoryStorageList = new ArrayList<>();
//        accessoryStorageList.add(accessoryStorageInit);
//        List<AccessoryStorage> accessoryStorageList1 = accessoryStorageService.getAccessoryStorageByInfo(manufactureAccessory.getOrderName(), null, null, null, null, null, null);
//        List<AccessoryStorage> accessoryStorageList2 = new ArrayList<>();
//        for (AccessoryStorage accessoryStorage : accessoryStorageList){
//            boolean flag = true;
//            for (AccessoryStorage accessoryStorage1 : accessoryStorageList1){
//                if (accessoryStorage.getAccessoryID().equals(accessoryStorage1.getAccessoryID()) && accessoryStorage.getAccessoryLocation().equals(accessoryStorage1.getAccessoryLocation())){
//                    accessoryStorage1.setStorageCount(accessoryStorage.getStorageCount() + accessoryStorage1.getStorageCount());
//                    flag = false;
//                }
//            }
//            if (flag){
//                accessoryStorageList2.add(accessoryStorage);
//            }
//        }
//        int res2 = accessoryInStoreService.addAccessoryInStoreBatch(accessoryInStoreList);
//        int res3 = accessoryStorageService.addAccessoryStorageBatch(accessoryStorageList2);
//        int res4 = accessoryStorageService.updateAccessoryStorageBatch(accessoryStorageList1);
//        clothesDevAccessoryManageService.addClothesDevAccessoryManage(clothesDevAccessoryManageStorage);
//        if (res1 == 0 && res2 == 0 && res3 == 0 && res4 == 0){
//            map.put("result", 0);
//        }else {
//            map.put("result", 1);
//        }
//        return map;
//    }


//    @RequestMapping(value = "/getaccessorynameorderbyprefix", method = RequestMethod.GET)
//    @ResponseBody
//    public Map<String, Object> getAccessoryNameOrderByPreFix(@RequestParam("accessoryNumberCode")String accessoryNumberCode){
//        Map<String, Object> map = new LinkedHashMap<>();
//        String maxOrder = clothesDevAccessoryManageService.getMaxClothesDevAccessoryOrderByPreFix(accessoryNumberCode);
//        int a = Integer.valueOf(maxOrder).intValue();
//        String orderStr = String.format("%05d", a + 1);
//        map.put("accessoryOrder", orderStr);
//        return map;
//    }

    @RequestMapping(value = "/getshareaccessorystorage", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getShareAccessoryStorage(@RequestParam("storageType")String storageType,
                                                        @RequestParam(value = "supplier",required = false)String supplier,
                                                        @RequestParam(value = "from", required = false)String from,
                                                        @RequestParam(value = "to", required = false)String to){
        Map<String, Object> map = new LinkedHashMap<>();
        Map<String, Object> param = new HashMap<>();
        param.put("storageType", storageType);
        param.put("supplier", supplier);
        param.put("from", from);
        param.put("to", to);
        List<ClothesDevAccessoryManage> clothesDevAccessoryManageList = clothesDevAccessoryManageService.getClothesDevAccessoryManageByMap(param);
        map.put("data", clothesDevAccessoryManageList);
        return map;
    }

    @RequestMapping(value = "/clothesdevaccessoryupdateincheck", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> getShareFabricStorage(@RequestParam("clothesDevAccessoryJson")String clothesDevAccessoryJson,
                                                     @RequestParam("checkMonth")String checkMonth){
        Map<String, Object> map = new LinkedHashMap<>();
        Map<String, Object> param = new HashMap<>();
        param.put("checkMonth", checkMonth);
        CheckMonth fixedMonth = checkMonthService.getCheckMonthByInfo(param);
        if (fixedMonth != null){
            map.put("result", 3);
            return map;
        }
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        List<ClothesDevAccessoryManage> clothesDevAccessoryManageList = gson.fromJson(clothesDevAccessoryJson,new TypeToken<List<ClothesDevAccessoryManage>>(){}.getType());
        int res = clothesDevAccessoryManageService.updateClothesDevAccessoryManageBatch(clothesDevAccessoryManageList);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/quitclothesdevaccessorybatch", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> quitClothesDevAccessoryBatch(@RequestParam("idList")List<Integer> idList){
        Map<String, Object> map = new LinkedHashMap<>();
        int res = clothesDevAccessoryManageService.quitClothesDevAccessoryManageCheck(idList);
        map.put("result", res);
        return map;
    }



}
