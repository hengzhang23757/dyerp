package com.example.erp01.action;

import com.example.erp01.model.*;
import com.example.erp01.service.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;


@Controller
@RequestMapping(value = "erp")
public class OrderProcedureController {
    private static final Logger log = LoggerFactory.getLogger(OrderProcedureController.class);
    @Autowired
    private OrderProcedureService orderProcedureService;
    @Autowired
    private OrderProcedureDetailService orderProcedureDetailService;
    @Autowired
    private ProcedureTemplateService procedureTemplateService;
    @Autowired
    private OrderClothesService orderClothesService;
    @Autowired
    private MessageService messageService;

    @RequestMapping("/orderProcedureStart")
    public String orderProcedureStart(Model model){
        model.addAttribute("bigMenuTag",9);
        model.addAttribute("menuTag",92);
        return "miniProgram/orderProcedure";
    }

    @RequestMapping("/orderProcedureNewStart")
    public String orderProcedureNewStart(){
        return "ieInfo/orderProcedureNew";
    }

    @RequestMapping("/addOrderProcedureStart")
    public String addOrderProcedureStart(Model model,String type,String orderName) throws Exception{
        if (orderName == null){
            model.addAttribute("type", type);
        }else {
//            orderName = java.net.URLDecoder.decode(orderName,"UTF-8");
            model.addAttribute("type", type);
            model.addAttribute("orderName", orderName);
        }
        return "miniProgram/fb_orderProcedureAdd";
    }

    @RequestMapping("/addOrderProcedureNewStart")
    public String addOrderProcedureNewStart(Model model,String type,String orderName) throws Exception{
        if (orderName == null){
            model.addAttribute("type", type);
        }else {
//            orderName = java.net.URLDecoder.decode(orderName,"UTF-8");
            model.addAttribute("type", type);
            model.addAttribute("orderName", orderName);
        }
        return "ieInfo/fb_orderProcedureAddNew";
    }

    @RequestMapping("/orderProcedureDetailStart")
    public String addOrderStart(Model model,String orderName,String type) throws Exception{
//        orderName = java.net.URLDecoder.decode(orderName,"UTF-8");
        if (type == null){
            model.addAttribute("type", "detail");
        }else{
            model.addAttribute("type","review");
        }
        model.addAttribute("orderName", orderName);
        return "miniProgram/fb_orderProcedureDetail";
    }

    @RequestMapping("/orderProcedureDetailNewStart")
    public String orderProcedureDetailNewStart(Model model,String orderName,String type) throws Exception{
//        orderName = java.net.URLDecoder.decode(orderName,"UTF-8");
        if (type == null){
            model.addAttribute("type", "detail");
        }else{
            model.addAttribute("type","review");
        }
        model.addAttribute("orderName", orderName);
        return "ieInfo/fb_orderProcedureDetailNew";
    }

    @RequestMapping(value = "/addorderprocedure", method = RequestMethod.POST)
    @ResponseBody
    public int addOrderProcedure(OrderProcedure orderProcedure){
        int res = orderProcedureService.addOrderProcedure(orderProcedure);
        return res;
    }

    @RequestMapping(value = "/addorderprocedurebatch", method = RequestMethod.POST)
    @ResponseBody
    public int addOrderProcedureBatch(@RequestParam("orderProcedureJson")String orderProcedureJson,
                                      @RequestParam("userName")String userName){
        log.warn("添加订单工序---内容："+orderProcedureJson+"---操作员工："+userName);
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        List<OrderProcedure> orderProcedureList = gson.fromJson(orderProcedureJson,new TypeToken<List<OrderProcedure>>(){}.getType());
        String orderName = orderProcedureList.get(0).getOrderName();
        List<OrderProcedure> orderProcedureList1 = orderProcedureService.getOrderProcedureByOrder(orderName);
        if (orderProcedureList1 != null && !orderProcedureList1.isEmpty()){
            return 4;
        }
        for (OrderProcedure orderProcedure : orderProcedureList){
            orderProcedure.setLastUpdateTime(new Date());
            orderProcedure.setLastUpdateUser(userName);
        }
        int res1 = orderProcedureService.deleteOrderProcedureByName(orderName);
        int res2 = orderProcedureDetailService.deleteOrderProcedureByOrderName(orderName);
        int res3 = orderProcedureService.addOrderProcedureBatch(orderProcedureList);
        List<OrderProcedureDetail> orderProcedureDetailList = new ArrayList<>();
        for (OrderProcedure orderProcedure : orderProcedureList){
            String uuid = UUID.randomUUID().toString();
            OrderProcedureDetail opd = new OrderProcedureDetail(orderName,orderProcedure.getProcedureNumber().toString(),orderProcedure.getProcedureCode(),orderProcedure.getProcedureName(),orderProcedure.getProcedureDescription(), uuid);
            orderProcedureDetailList.add(opd);
        }
        int res4 = orderProcedureDetailService.addOrderProcedureDetail(orderProcedureDetailList);
        updateMessageAfterDoSomething(orderName, "IE");
        if (res1 == 0 && res2 == 0 && res3 == 0 && res4 == 0){
            return 0;
        }else {
            return 1;
        }
    }

    @RequestMapping(value = "/updateorderprocedurebatch", method = RequestMethod.POST)
    @ResponseBody
    public int updateOrderProcedureBatch(@RequestParam("orderProcedureJson")String orderProcedureJson,
                                         @RequestParam("userName")String userName){
        log.warn("修改订单工序---内容："+orderProcedureJson+"---操作员工："+userName);
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        List<OrderProcedure> orderProcedureList = gson.fromJson(orderProcedureJson,new TypeToken<List<OrderProcedure>>(){}.getType());
        String orderName = orderProcedureList.get(0).getOrderName();
        List<OrderProcedure> initOrderProcedureList = orderProcedureService.getOrderProcedureByOrder(orderName);
        for (OrderProcedure orderProcedure0 : initOrderProcedureList){
            for (OrderProcedure orderProcedure1 : orderProcedureList){
                if (orderProcedure0.getProcedureNumber().equals(orderProcedure1.getProcedureNumber())){
                    orderProcedure1.setProcedureState(orderProcedure0.getProcedureState());
                }
            }
        }
        for (OrderProcedure orderProcedure1 : orderProcedureList){
            orderProcedure1.setLastUpdateTime(new Date());
            orderProcedure1.setLastUpdateUser(userName);
        }
        int res1 = orderProcedureService.deleteOrderProcedureByName(orderName);
        int res3 = orderProcedureService.addOrderProcedureBatch(orderProcedureList);
        List<OrderProcedureDetail> orderProcedureDetailList = orderProcedureDetailService.getOrderProcedureDetailByOrderName(orderName);
        List<OrderProcedureDetail> insertOrderProcedureDetailList = new ArrayList<>();
        List<OrderProcedureDetail> updateOrderProcedureDetailList = new ArrayList<>();
        List<Integer> deleteOrderProcedureIDList = new ArrayList<>();
        // 以工序号为基准 查看 工序代码 工序名 工序描述是否相同
        for (OrderProcedure orderProcedure : orderProcedureList){
            boolean flag = true;
            for (OrderProcedureDetail orderProcedureDetail : orderProcedureDetailList){
                if (orderProcedure.getProcedureNumber().toString().equals(orderProcedureDetail.getProcedureNumber())){
                    flag = false;
                    if (!orderProcedure.getProcedureCode().equals(orderProcedureDetail.getProcedureCode()) || !orderProcedure.getProcedureName().equals(orderProcedureDetail.getProcedureName()) || !orderProcedure.getProcedureDescription().equals(orderProcedureDetail.getProcedureDescription())){
                        OrderProcedureDetail opd = new OrderProcedureDetail(orderName,orderProcedure.getProcedureNumber().toString(),orderProcedure.getProcedureCode(),orderProcedure.getProcedureName(),orderProcedure.getProcedureDescription(),orderProcedureDetail.getGuid());
                        opd.setOrderProcedureDetailID(orderProcedureDetail.getOrderProcedureDetailID());
                        updateOrderProcedureDetailList.add(opd);
                    }
                }
            }
            if (flag){
                String uuid = UUID.randomUUID().toString();
                OrderProcedureDetail opd = new OrderProcedureDetail(orderName,orderProcedure.getProcedureNumber().toString(),orderProcedure.getProcedureCode(),orderProcedure.getProcedureName(),orderProcedure.getProcedureDescription(),uuid);
                insertOrderProcedureDetailList.add(opd);
            }
        }
        for (OrderProcedureDetail orderProcedureDetail : orderProcedureDetailList){
            boolean flag = true;
            for (OrderProcedure orderProcedure : orderProcedureList){
                if (orderProcedure.getProcedureNumber().toString().equals(orderProcedureDetail.getProcedureNumber())){
                    flag = false;
                }
            }
            if (flag){
                deleteOrderProcedureIDList.add(orderProcedureDetail.getOrderProcedureDetailID());
            }
        }
        int res4 = orderProcedureDetailService.addOrderProcedureDetail(insertOrderProcedureDetailList);
        int res5 = orderProcedureDetailService.updateOrderProcedureDetailBatch(updateOrderProcedureDetailList);
        int res6 = orderProcedureDetailService.deleteOrderProcedureDetailBatch(deleteOrderProcedureIDList);
        if (res1 == 0 && res3 == 0 && res4 == 0 && res5 == 0 && res6 == 0){
            return 0;
        }else {
            return 1;
        }
    }


    @RequestMapping(value = "/deleteorderprocedure", method = RequestMethod.POST)
    @ResponseBody
    public int deleteOrderProcedure(@RequestParam("orderProcedureID")Integer orderProcedureID){
        int res = orderProcedureService.deleteOrderProcedure(orderProcedureID);
        return res;
    }

    @RequestMapping(value = "/deleteorderprocedurebyname", method = RequestMethod.POST)
    @ResponseBody
    public int deleteOrderProcedureByName(@RequestParam("orderName")String orderName,
                                          @RequestParam("userName")String userName){
        log.warn("删除订单工序---订单名："+orderName+"---操作员工："+userName);
        int res1 = orderProcedureService.deleteOrderProcedureByName(orderName);
        int res2 = orderProcedureDetailService.deleteOrderProcedureByOrderName(orderName);
        int res = 0;
        if (res1 != 0 || res2 != 0){
            res = 1;
        }
        return res;
    }


    @RequestMapping(value = "/getuniqueorderprocedure", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getUniqueOrderProcedure(String clothesVersionNumber,String styleDescription,String orderName,
            String beginDate,String customerName,Integer procedureState,int page,int limit){
        Map<String, Object> map = new HashMap<>();
        Map<String, Object> paramMaps = new HashMap<>();
        paramMaps.put("clothesVersionNumber",clothesVersionNumber);
        paramMaps.put("styleDescription",styleDescription);
        paramMaps.put("orderName",orderName);
        paramMaps.put("beginDate",beginDate);
        paramMaps.put("customerName",customerName);
        paramMaps.put("procedureState",procedureState);
        List<OrderProcedure> countList = orderProcedureService.getUniqueOrderProcedure(paramMaps);
        map.put("count",countList.size());
        paramMaps.put("page",(page-1)*limit);
        paramMaps.put("limit",limit);
        List<OrderProcedure> orderProcedureList = orderProcedureService.getUniqueOrderProcedure(paramMaps);
        for (OrderProcedure orderProcedure : orderProcedureList){
            String tmpOrderName = orderProcedure.getOrderName();
            List<Integer> procedureStateList = orderProcedureService.getDistinctProcedureStateByOrder(tmpOrderName);
            orderProcedure.setReviewState(checkProcedureState(procedureStateList));
        }
        map.put("data",orderProcedureList);
        map.put("code",0);
        map.put("msg",null);
        return map;
    }

    @RequestMapping(value = "/minigetprocedureinfobyorder", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> miniGetProcedureInfoByOrder(@RequestParam("orderName")String orderName){
        Map<String, Object> map = new HashMap();
        List<ProcedureInfo> procedureInfoList = orderProcedureService.getProcedureInfoByOrderName(orderName);
        map.put("procedureInfoList",procedureInfoList);
        return map;
    }

    @RequestMapping(value = "/getprocedureinfobyorder", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getProcedureInfoByOrder(@RequestParam("orderName")String orderName){
        Map<String, Object> map = new HashMap();
        List<ProcedureInfo> procedureInfoList = orderProcedureService.getProcedureInfoByOrderName(orderName);
        map.put("procedureInfoList",procedureInfoList);
        return map;
    }

    @RequestMapping(value = "/getprocedurenumbersbyordername", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getProcedureNumbersByOrderName(@RequestParam("orderName")String orderName){
        Map<String, Object> map = new HashMap();
        List<Integer> procedureNumberList = orderProcedureService.getProcedureNumbersByOrderName(orderName);
        map.put("procedureNumberList",procedureNumberList);
        return map;
    }

    @RequestMapping(value = "/getprocedurecodenamebynumber", method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getProcedureCodeNameByNumber(@RequestParam("orderName")String orderName,
                                                           @RequestParam("procedureNumber")String procedureNumber){
        Map<String, Object> map = new HashMap();
        ProcedureInfo procedureInfo = orderProcedureService.getProcedureCodeNameByNumber(orderName,Integer.parseInt(procedureNumber));
        map.put("procedureInfo",procedureInfo);
        return map;
    }

    @RequestMapping(value = "/getproceudredetailbyorder",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getProcedureDetailByOrder(@RequestParam("orderName")String orderName){
        Map<String,Object> map = new HashMap<>();
        List<ProcedureDetail> orderProcedureDetailList = orderProcedureService.getProcedureDetailByOrder(orderName);
        map.put("orderProcedureDetailList",orderProcedureDetailList);
        return map;
    }

    @RequestMapping(value = "/getorderproceudrebyorder",method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> getOrderProcedureByOrder(@RequestParam("orderName")String orderName){
        Map<String,Object> map = new LinkedHashMap<>();
        List<OrderProcedure> orderProcedureList = orderProcedureService.getOrderProcedureByOrder(orderName);
        map.put("orderProcedureList",orderProcedureList);
        List<String> sizeNameList = orderClothesService.getOrderSizeNamesByOrder(orderName);
        map.put("sizeNameList",sizeNameList);
        List<String> colorNameList = orderClothesService.getOrderColorNamesByOrder(orderName);
        map.put("colorNameList",colorNameList);
        return map;
    }

    @RequestMapping(value = "/changeprocedurestate",method = RequestMethod.POST)
    @ResponseBody
    public int commitProcedureReview(@RequestParam("orderName")String orderName,
                                     @RequestParam("procedureState")Integer procedureState){
        return orderProcedureService.changeProcedureState(orderName, procedureState);
    }

    @RequestMapping(value = "/getspecialprocedurebyorder",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getSpecialProcedureByOrder(@RequestParam("orderName")String orderName){
        Map<String,Object> map = new LinkedHashMap<>();
        List<ProcedureInfo> procedureInfoList = orderProcedureService.getSpecialProcedureInfoByOrder(orderName);
        map.put("procedureInfoList",procedureInfoList);
        return map;
    }

    @RequestMapping(value = "/changeprocedurestatebatch",method = RequestMethod.POST)
    @ResponseBody
    public int changeProcedureStateBatch(@RequestParam("orderName")String orderName,
                                         @RequestParam("procedureNumberList")List<Integer> procedureNumberList,
                                         @RequestParam("procedureState")Integer procedureState){
        return orderProcedureService.changeProcedureStateBatch(orderName, procedureNumberList, procedureState);
    }

    @RequestMapping(value = "/getorderproceduretoreview",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getOrderProcedureToReview(@RequestParam("orderName")String orderName,
                                                         @RequestParam("procedureNumberList")List<Integer> procedureNumberList){
        Map<String, Object> map = new LinkedHashMap<>();
        List<OrderProcedure> orderProcedureList = orderProcedureService.getOrderProcedureToReview(orderName, procedureNumberList);
        map.put("orderProcedureList", orderProcedureList);
        return map;
    }

    @RequestMapping(value = "/getorderprocedurebystate",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> getOrderProcedureByState(@RequestParam("orderName")String orderName,
                                                        @RequestParam("procedureStateList")List<Integer> procedureStateList){
        Map<String, Object> map = new LinkedHashMap<>();
        List<OrderProcedure> orderProcedureList = orderProcedureService.getOrderProcedureByState(orderName, procedureStateList);
        map.put("orderProcedureList", orderProcedureList);
        return map;
    }

    @RequestMapping(value = "/orderProcedureAudit")
    @ResponseBody
    public int orderProcedureAudit(@RequestParam("orderName")String orderName,@RequestParam("procedureState")Integer procedureState) {
        List<Integer> procedureStateList = new ArrayList<Integer>();
        List<Integer> procedureNumberList = new ArrayList<Integer>();
        procedureStateList.add(1);
        List<OrderProcedure> orderProcedureList = orderProcedureService.getOrderProcedureByState(orderName,procedureStateList);
        for(OrderProcedure orderProcedure : orderProcedureList) {
            procedureNumberList.add(orderProcedure.getProcedureNumber());
        }
        return orderProcedureService.changeProcedureStateBatch(orderName, procedureNumberList, procedureState);
    }

    private int checkProcedureState(List<Integer> procedureStateList){
        if (procedureStateList == null || procedureStateList.size() == 0){
            return 0;
        }
        if (procedureStateList.size() == 1){
            if (procedureStateList.get(0) == 0){
                return 0;
            }
            if (procedureStateList.get(0) == 1){
                return 1;
            }
            if (procedureStateList.get(0) == 2){
                return 2;
            }
            if (procedureStateList.get(0) == 3){
                return 3;
            }
        }else if(procedureStateList.size() == 2){
            if (procedureStateList.contains(0) && procedureStateList.contains(3)){
                return 4;
            }
            if (procedureStateList.contains(0) && procedureStateList.contains(1)){
                return 5;
            }
            if (procedureStateList.contains(0) && procedureStateList.contains(2)){
                return 6;
            }
            if (procedureStateList.contains(1) && procedureStateList.contains(2)){
                return 7;
            }
            if (procedureStateList.contains(1) && procedureStateList.contains(3)){
                return 8;
            }
            if (procedureStateList.contains(2) && procedureStateList.contains(3)){
                return 9;
            }

        }else if (procedureStateList.size() == 3){
            if (procedureStateList.contains(0) && procedureStateList.contains(1) && procedureStateList.contains(2)){
                return 10;
            }
            if (procedureStateList.contains(0) && procedureStateList.contains(1) && procedureStateList.contains(3)){
                return 8;
            }
            if (procedureStateList.contains(0) && procedureStateList.contains(2) && procedureStateList.contains(3)){
                return 9;
            }
            if (procedureStateList.contains(1) && procedureStateList.contains(2) && procedureStateList.contains(3)){
                return 11;
            }

        }else if (procedureStateList.size() == 4){
            return 11;
        }else{
            return 0;
        }
        return 0;

    }

    @RequestMapping(value = "/getsectionsamvaluebyordername",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getSectionSamValueByValue(@RequestParam("orderName")String orderName,
                                                         @RequestParam("procedureSection") String procedureSection){
        Map<String, Object> map = new LinkedHashMap<>();
        Float sam = orderProcedureService.getSectionSamByOrder(orderName, procedureSection);
        map.put("sam", sam);
        return map;
    }


    @RequestMapping(value = "/minichangescanpartofprocedure",method = RequestMethod.POST)
    @ResponseBody
    public int miniChangeScanPartOfProcedure(@RequestParam("orderName")String orderName,
                                             @RequestParam("procedureNumber")Integer procedureNumber,
                                             @RequestParam("scanPart")String scanPart){
        return orderProcedureService.changeScanPartOfProcedure(orderName, procedureNumber, scanPart);
    }


    @RequestMapping(value = "/getpendingorderprocedurecount",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getPendingOrderProcedureCount(){
        Map<String, Object> map = new LinkedHashMap<>();
        int pendingCount = orderProcedureService.getPendingOrderProcedureCount(1);
        map.put("pendingCount", pendingCount);
        return map;
    }

    private void updateMessageAfterDoSomething(String orderName, String messageType){
        Map<String, Object> param = new HashMap<>();
        param.put("orderName", orderName);
        param.put("messageType", messageType);
        param.put("readType", "未完成");
        List<Message> messageList = messageService.getMessageContentByInfo(param);
        for (Message message : messageList){
            message.setReadType("已完成");
        }
        messageService.updateMessageBatch(messageList);
    }

}
