package com.example.erp01.action;

import com.example.erp01.model.*;
import com.example.erp01.service.OPABackService;
import com.example.erp01.service.TailorService;
import com.google.gson.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.security.PublicKey;
import java.util.*;

@Controller
@RequestMapping(value = "erp")
public class OPABackController {

    @Autowired
    private OPABackService opaBackService;

    @Autowired
    private TailorService tailorService;


    /**
     * 进入花片回场页面
     * @param model
     * @return
     */
    @RequestMapping("/opaBackStart")
    public String opaBackStart(Model model) {
        model.addAttribute ("bigMenuTag", 2);
        model.addAttribute ("menuTag", 26);
        return "cutMarket/opaBack";
    }

    @RequestMapping("/opaBackQueryStart")
    public String opaBackQueryStart(Model model) {
        model.addAttribute ("bigMenuTag", 1);
        model.addAttribute ("menuTag", 14);
        return "orderMsg/opaBackReport";
    }

    @RequestMapping(value = "/getallopaback",method = RequestMethod.GET)
    public List<OPABack> getAllOPABack(){
        List<OPABack> opaBackList = new ArrayList<>();
        opaBackList = opaBackService.getAllOPABack();
        return opaBackList;
    }

    @RequestMapping(value = "/opabackquery",method = RequestMethod.GET)
    @ResponseBody
    public List<String> opaBackQuery(@RequestParam("orderName")String orderName,
                                     @RequestParam("bedNumber")int bedNumber){
        List<String> tailorQcodeList = opaBackService.opaBackQuery(orderName,bedNumber);
        return tailorQcodeList;
    }

    @RequestMapping(value = "/getopabackinfobyorderbedpart",method = RequestMethod.GET)
    @ResponseBody
    public String getOPABackInfoByOrderBedPart(@RequestParam("orderName")String orderName,
                                               @RequestParam("bedNumber")int bedNumber,
                                               @RequestParam("partName")String partName){
        List<OpaQueryLeak> opaBackInfoList = opaBackService.getOPABackInfoByOrderBedPart(orderName,bedNumber,partName);
        Gson gson = new Gson();
        String res = gson.toJson(opaBackInfoList);
        return res;
    }


//    @RequestMapping(value = "/searchOpaQueryLeak",method = RequestMethod.GET)
//    @ResponseBody
//    public String searchOpaQueryLeak(@RequestParam("orderName")String orderName,
//                                               @RequestParam("bedNumber")int bedNumber,
//                                               @RequestParam("partName")String partName){
//        List<OpaQueryLeak> opaBackInfoList = opaBackService.getOPABackInfoByOrderBedPart(orderName,bedNumber,partName);
//        List<Tailor> tailorList = tailorService.getTailorByOrderNameBedNumPart(orderName, bedNumber, partName);
//        Map<String,Object> colorMap = new HashMap<>();
//        List<String> colorList = new ArrayList<>();
//        List<String> sizeList = new ArrayList<>();
//        Gson gson = new Gson();
//        if(tailorList!=null && tailorList.size()>0 && (opaBackInfoList==null || opaBackInfoList.size()==0)){
//            for(Tailor tailor : tailorList){
//                String tmpColor = tailor.getColorName();
//                if (!colorList.contains(tmpColor)){
//                    colorList.add(tmpColor);
//                }
//            }
//            for(Tailor tailor : tailorList){
//                String tmpSize = tailor.getSizeName();
//                if (!sizeList.contains(tmpSize)){
//                    sizeList.add(tmpSize);
//                }
//            }
//            for (int i=0; i<colorList.size(); i++){
//                String color = colorList.get(i);
//                Map<String,Object> tmpSizeMap = new HashMap<>();
//                for (int j=0; j<sizeList.size(); j++){
//                    String size = sizeList.get(j);
//                    Boolean flag = true;
//                    for(Tailor tailor : tailorList){
//                        if (tailor.getColorName().equals(color) && tailor.getSizeName().equals(size)){
//                            ReportCompare rc = new ReportCompare(tailor.getLayerCount(),0,0-tailor.getLayerCount());
//                            tmpSizeMap.put(size,rc);
//                            flag = false;
//                        }
//                    }
//                    if (flag){
//                        ReportCompare rc = new ReportCompare(0,0,0);
//                        tmpSizeMap.put(size,rc);
//                    }
//                }
//                colorMap.put(color,tmpSizeMap);
//            }
//            String res = gson.toJson(colorMap);
//            return res;
//        }else if ((tailorList==null || tailorList.size()==0) && opaBackInfoList!=null && opaBackInfoList.size()!=0){
//            for(OpaQueryLeak opaQueryLeak : opaBackInfoList){
//                String tmpColor = opaQueryLeak.getColorName();
//                if (!colorList.contains(tmpColor)){
//                    colorList.add(tmpColor);
//                }
//            }
//            for(OpaQueryLeak opaQueryLeak : opaBackInfoList){
//                String tmpSize = opaQueryLeak.getSizeName();
//                if (!sizeList.contains(tmpSize)){
//                    sizeList.add(tmpSize);
//                }
//            }
//            for (int i=0; i<colorList.size(); i++){
//                String color = colorList.get(i);
//                Map<String,Object> tmpSizeMap = new HashMap<>();
//                for (int j=0; j<sizeList.size(); j++){
//                    String size = sizeList.get(j);
//                    Boolean flag = true;
//                    for(OpaQueryLeak opaQueryLeak : opaBackInfoList){
//                        if (opaQueryLeak.getColorName().equals(color) && opaQueryLeak.getSizeName().equals(size)){
//                            ReportCompare rc = new ReportCompare(0,opaQueryLeak.getBackCount(),opaQueryLeak.getBackCount());
//                            tmpSizeMap.put(size,rc);
//                            flag = false;
//                        }
//                    }
//                    if (flag){
//                        ReportCompare rc = new ReportCompare(0,0,0);
//                        tmpSizeMap.put(size,rc);
//                    }
//                }
//                colorMap.put(color,tmpSizeMap);
//            }
//            String res = gson.toJson(colorMap);
//            return res;
//        }else if(tailorList!=null && tailorList.size()!=0 && opaBackInfoList!=null && opaBackInfoList.size()!=0){
//            for(Tailor tailor : tailorList){
//                String tmpColor = tailor.getColorName();
//                if (!colorList.contains(tmpColor)){
//                    colorList.add(tmpColor);
//                }
//                String tmpSize = tailor.getSizeName();
//                if (!sizeList.contains(tmpSize)){
//                    sizeList.add(tmpSize);
//                }
//            }
//            for(OpaQueryLeak opaQueryLeak : opaBackInfoList){
//                String tmpColor = opaQueryLeak.getColorName();
//                if (!colorList.contains(tmpColor)){
//                    colorList.add(tmpColor);
//                }
//                String tmpSize = opaQueryLeak.getSizeName();
//                if (!sizeList.contains(tmpSize)){
//                    sizeList.add(tmpSize);
//                }
//            }
//            for (int i=0; i<colorList.size(); i++){
//                String color = colorList.get(i);
//                Map<String,Object> tmpSizeMap = new HashMap<>();
//                for (int j=0; j<sizeList.size(); j++){
//                    String size = sizeList.get(j);
//                    int tmpPlanCount = 0;
//                    int tmpActualCount = 0;
//
//                    for(Tailor tailor : tailorList){
//                        if (tailor.getColorName().equals(color) && tailor.getSizeName().equals(size)){
//                            tmpPlanCount = tailor.getLayerCount();
//                        }
//                    }
//                    for(OpaQueryLeak opaQueryLeak : opaBackInfoList){
//                        if (opaQueryLeak.getColorName().equals(color) && opaQueryLeak.getSizeName().equals(size)){
//                            tmpActualCount = opaQueryLeak.getBackCount();
//                        }
//                    }
//                    ReportCompare rc = new ReportCompare(tmpPlanCount,tmpActualCount,tmpActualCount-tmpPlanCount);
//                    tmpSizeMap.put(size,rc);
//
//                }
//                colorMap.put(color,tmpSizeMap);
//            }
//            String res = gson.toJson(colorMap);
//            return res;
//        }else{
//            return null;
//        }
//    }


//    @RequestMapping(value = "/searchOpaQueryLeak",method = RequestMethod.GET)
//    @ResponseBody
//    public String searchOpaQueryLeak(@RequestParam("orderName")String orderName,
//                                     @RequestParam("bedNumber")int bedNumber,
//                                     @RequestParam("partName")String partName){
//        List<OpaQueryLeak> opaBackInfoList = opaBackService.getOPABackInfoByOrderBedPart(orderName,bedNumber,partName);
//        List<Tailor> tailorList = tailorService.getTailorByOrderNameBedNumPart(orderName, bedNumber, partName);
//        Gson gson = new Gson();
//        if(opaBackInfoList!=null && opaBackInfoList.size()>0 && (tailorList==null || tailorList.size()==0)) {
//            String res = gson.toJson(opaBackInfoList);
//            return res;
//        }else if ((opaBackInfoList==null || opaBackInfoList.size()==0) && tailorList!=null && tailorList.size()!=0) {
//            List<OpaQueryLeak> list = new LinkedList<>();
//            for(Tailor tailor : tailorList) {
//                OpaQueryLeak opaQueryLeak = new OpaQueryLeak();
//                opaQueryLeak.setColorName(tailor.getColorName());
//                opaQueryLeak.setSizeName(tailor.getSizeName());
//                opaQueryLeak.setOutCount(tailor.getLayerCount());
//                list.add(opaQueryLeak);
//            }
//            String res = gson.toJson(list);
//            return res;
//        }else if (opaBackInfoList!=null && opaBackInfoList.size()!=0 && tailorList!=null && tailorList.size()!=0) {
//            for (Tailor tailor : tailorList) {
//                Boolean flag = true;
//                for (OpaQueryLeak opaQueryLeak : opaBackInfoList) {
//                    if(opaQueryLeak.getColorName().equals(tailor.getColorName()) &&
//                            opaQueryLeak.getSizeName().equals(tailor.getSizeName())) {
//                        opaQueryLeak.setOutCount(tailor.getLayerCount());
//                        flag = false;
//                        break;
//                    }
//                }
//                if(flag) {
//                    OpaQueryLeak opaQueryLeak = new OpaQueryLeak();
//                    opaQueryLeak.setColorName(tailor.getColorName());
//                    opaQueryLeak.setSizeName(tailor.getSizeName());
//                    opaQueryLeak.setOutCount(tailor.getLayerCount());
//                    opaBackInfoList.add(opaQueryLeak);
//                }
//            }
//            String res = gson.toJson(opaBackInfoList);
//            return res;
//        }else {
//            return null;
//        }
//    }

}
