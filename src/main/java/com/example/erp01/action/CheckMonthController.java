package com.example.erp01.action;

import com.example.erp01.model.CheckMonth;
import com.example.erp01.service.CheckMonthService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "erp")
public class CheckMonthController {

    @Autowired
    private CheckMonthService checkMonthService;

    @RequestMapping("/checkMonthStart")
    public String checkMonthStart(){
        return "checkModule/checkMonth";
    }

    @RequestMapping(value = "/addcheckmonth",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> addCheckMonth(@RequestParam("monthStr")String monthStr){
        Map<String, Object> map = new LinkedHashMap<>();
        CheckMonth checkMonth = new CheckMonth();
        checkMonth.setCheckMonth(monthStr);
        int res = checkMonthService.addCheckMonth(checkMonth);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/deletecheckmonth",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> deleteCheckMonth(@RequestParam("id")Integer id){
        Map<String, Object> map = new LinkedHashMap<>();
        int res = checkMonthService.deleteCheckMonth(id);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/getallcheckmonth",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getAllCheckMonth(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<CheckMonth> checkMonthList = checkMonthService.getAllCheckMonth();
        map.put("data",checkMonthList);
        return map;
    }

    @RequestMapping(value = "/updatecheckmonth",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> updateCheckMonth(@RequestParam("checkMonthJson")String checkMonthJson){
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        CheckMonth checkMonth = gson.fromJson(checkMonthJson,new TypeToken<CheckMonth>(){}.getType());
        int res = checkMonthService.updateCheckMonth(checkMonth);
        map.put("result", res);
        return map;
    }

}
