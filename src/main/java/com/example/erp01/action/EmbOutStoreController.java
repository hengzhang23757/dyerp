package com.example.erp01.action;

import com.example.erp01.model.*;
import com.example.erp01.service.*;
import com.google.gson.*;
import org.omg.PortableInterceptor.INACTIVE;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
@RequestMapping(value = "erp")
public class EmbOutStoreController {

    @Autowired
    private EmbOutStoreService embOutStoreService;
    @Autowired
    private TailorService tailorService;
    @Autowired
    private EmbStorageService embStorageService;
    @Autowired
    private OrderClothesService orderClothesService;
    @Autowired
    private EmbPlanService embPlanService;
    @Autowired
    private EmbInStoreService embInStoreService;
    @Autowired
    private ManufactureOrderService manufactureOrderService;

    @RequestMapping("/embOutDetailStart")
    public String embOutDetailStart(Model model){
        model.addAttribute("bigMenuTag",7);
        model.addAttribute("menuTag",73);
        return "embMarket/embOutDetail";
    }

    @RequestMapping("/embOutPackageDetailStart")
    public String embOutPackageDetailStart(Model model){
        model.addAttribute("bigMenuTag",7);
        model.addAttribute("menuTag",74);
        return "embMarket/embOutPackageDetail";
    }

    @RequestMapping("/embOutAmongStart")
    public String embOutAmongStart(){
        return "embMarket/embOutAmong";
    }

    @RequestMapping("/embOutPrintStart")
    public String embOutPrintStart(){
        return "embMarket/embOutPrint";
    }

    @RequestMapping("/queryEmbLeakStart")
    public String queryEmbLeakStart(Model model){
        model.addAttribute("bigMenuTag",7);
        model.addAttribute("menuTag",75);
        return "embMarket/queryEmbLeak";
    }

    @RequestMapping("/embOutStoreSingleStart")
    public String embOutStoreSingleStart(Model model){
        model.addAttribute("bigMenuTag",6);
        model.addAttribute("menuTag",65);
        return "embMarket/embOutStoreSingle";
    }

    @RequestMapping(value = "/addemboutstore",method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> addEmbOutStore(@RequestParam("embOutStoreJson")String embOutStoreJson){
        JsonParser jsonParser = new JsonParser();
        Map<String,Object> map = new HashMap<>();
        try{
            JsonObject json = (JsonObject) jsonParser.parse(embOutStoreJson);
            String groupName = json.get("groupName").getAsString();
            JsonArray jsonArray = json.get("tailorQcode").getAsJsonArray();
            Iterator iterator = jsonArray.iterator();
            List<Integer> tailorQcodeList = new ArrayList<>();
            while(iterator.hasNext()){
                JsonPrimitive next = (JsonPrimitive) iterator.next();
                tailorQcodeList.add(Integer.parseInt(next.toString().replace("\"","")));
            }
            List<Tailor> tailorList = tailorService.getTailorsByTailorQcodeIDs(tailorQcodeList, 0);
            if (tailorList == null || tailorList.size() == 0){
                map.put("result",3);
                return map;
            }
            Tailor firstTailor = tailorList.get(0);
            List<EmbOutStore> embOutStoreList = new ArrayList<>();
            String embStoreLocation = embStorageService.getLocationByOrderColorSizePackage(firstTailor.getOrderName(),firstTailor.getBedNumber(),firstTailor.getColorName(),firstTailor.getSizeName(),firstTailor.getPackageNumber());
            if (embStoreLocation == null){
                map.put("result",1);
                return map;
            }
            List<EmbStorage> embStorageList = embStorageService.getEmbStorageByOrderColorSize(embStoreLocation,firstTailor.getOrderName(),firstTailor.getColorName(),firstTailor.getSizeName());
            Integer embOutCount = embStorageList.size();
            int res1 = embStorageService.embOutStoreBatch(embStoreLocation,firstTailor.getOrderName(),firstTailor.getColorName(),firstTailor.getSizeName());
            for (EmbStorage embStorage : embStorageList){
                EmbOutStore eos = new EmbOutStore(groupName,firstTailor.getOrderName(),embStorage.getBedNumber(),firstTailor.getColorName(),firstTailor.getSizeName(),embStorage.getLayerCount(),embStorage.getPackageNumber(),embStorage.getTailorQcode());
                embOutStoreList.add(eos);
            }
            int res2 = embOutStoreService.addEmbOutStore(embOutStoreList);
            if (0 == res1 && 0 == res2){
                map.put("result",0);
                map.put("embStoreLocation",embStoreLocation);
                map.put("embOutCount",embOutCount);
                return map;
            }
        }catch (JsonIOException e){
            e.printStackTrace();
        }catch (JsonSyntaxException e){
            e.printStackTrace();
        }
        map.put("result",2);
        return map;
    }


    @RequestMapping(value = "/miniemboutstoresingle",method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> addEmbOutStoreOne(@RequestParam("embOutStoreJson")String embOutStoreJson){
        JsonParser jsonParser = new JsonParser();
        Map<String,Object> map = new HashMap<>();
        try{
            JsonObject json = (JsonObject) jsonParser.parse(embOutStoreJson);
            String groupName = json.get("groupName").getAsString();
            JsonArray jsonTailorQcode = json.get("tailorQcode").getAsJsonArray();
            Iterator iterator = jsonTailorQcode.iterator();
            List<Integer> tailorQcodeList = new ArrayList<>();
            while(iterator.hasNext()){
                JsonPrimitive tailorQcode = (JsonPrimitive) iterator.next();
                tailorQcodeList.add(Integer.parseInt(tailorQcode.toString().replace("\"","")));
            }
            List<Tailor> tailorList = tailorService.getTailorsByTailorQcodeIDs(tailorQcodeList, 0);
            if (tailorList == null || tailorList.size() == 0){
                map.put("result",2);
                return map;
            }
            if ("异常".equals(groupName)){
                int res = 0;
                for (Tailor tailor : tailorList){
                    res += embStorageService.deleteEmbStorageByOrderBedPackage(tailor.getOrderName(), tailor.getBedNumber(), tailor.getPackageNumber());
                }
                int embOutCount = tailorList.size();
                if (res == 0){
                    map.put("result",0);
                    map.put("embOutCount",embOutCount);
                    return map;
                }else {
                    map.put("result",1);
                    return map;
                }
            }else{
                List<EmbOutStore> embOutStoreList = new ArrayList<>();
                int res1 = 0;
                for (Tailor tailor : tailorList){
                    EmbOutStore eos = new EmbOutStore(groupName,tailor.getOrderName(),tailor.getBedNumber(),tailor.getColorName(),tailor.getSizeName(),tailor.getLayerCount(),tailor.getPackageNumber(),tailor.getTailorQcode());
                    embOutStoreList.add(eos);
                    res1 += embStorageService.deleteEmbStorageByOrderBedPackage(tailor.getOrderName(), tailor.getBedNumber(), tailor.getPackageNumber());
                }
                int res2 = embOutStoreService.addEmbOutStore(embOutStoreList);
                int embOutCount = tailorList.size();
                if (0 == res1 && 0 == res2){
                    map.put("result",0);
                    map.put("embOutCount",embOutCount);
                    return map;
                }else {
                    map.put("result",1);
                    return map;
                }
            }

        }catch (JsonIOException e){
            e.printStackTrace();
        }catch (JsonSyntaxException e){
            e.printStackTrace();
        }
        map.put("result",1);
        return map;
    }


    @RequestMapping(value = "/miniaddemboutstore",method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> miniAddEmbOutStore(@RequestParam("embOutStoreJson")String embOutStoreJson){
        JsonParser jsonParser = new JsonParser();
        Map<String,Object> map = new HashMap<>();
        try{
            JsonObject json = (JsonObject) jsonParser.parse(embOutStoreJson);
            String groupName = json.get("groupName").getAsString();
            JsonArray jsonArray = json.get("tailorQcode").getAsJsonArray();
            Iterator iterator = jsonArray.iterator();
            List<Integer> tailorQcodeList = new ArrayList<>();
            while(iterator.hasNext()){
                JsonPrimitive next = (JsonPrimitive) iterator.next();
                tailorQcodeList.add(Integer.parseInt(next.toString().replace("\"","")));
            }
            List<Tailor> tailorList = tailorService.getTailorsByTailorQcodeIDs(tailorQcodeList, 0);
            if (tailorList == null || tailorList.size() == 0){
                map.put("result",3);
                return map;
            }
            Tailor firstTailor = tailorList.get(0);
            List<EmbOutStore> embOutStoreList = new ArrayList<>();
            String embStoreLocation = embStorageService.getLocationByOrderColorSizePackage(firstTailor.getOrderName(),firstTailor.getBedNumber(),firstTailor.getColorName(),firstTailor.getSizeName(),firstTailor.getPackageNumber());
            if (embStoreLocation == null){
                map.put("result",1);
                return map;
            }
            List<EmbStorage> embStorageList = embStorageService.getEmbStorageByOrderColorSize(embStoreLocation,firstTailor.getOrderName(),firstTailor.getColorName(),firstTailor.getSizeName());
            Integer embOutCount = embStorageList.size();
            int res1 = embStorageService.embOutStoreBatch(embStoreLocation,firstTailor.getOrderName(),firstTailor.getColorName(),firstTailor.getSizeName());
            for (EmbStorage embStorage : embStorageList){
                EmbOutStore eos = new EmbOutStore(groupName,firstTailor.getOrderName(),embStorage.getBedNumber(),firstTailor.getColorName(),firstTailor.getSizeName(),embStorage.getLayerCount(),embStorage.getPackageNumber(),embStorage.getTailorQcode());
                embOutStoreList.add(eos);
            }
            int res2 = embOutStoreService.addEmbOutStore(embOutStoreList);
            if (0 == res1 && 0 == res2){
                map.put("result",0);
                map.put("embStoreLocation",embStoreLocation);
                map.put("embOutCount",embOutCount);
                return map;
            }
        }catch (JsonIOException e){
            e.printStackTrace();
        }catch (JsonSyntaxException e){
            e.printStackTrace();
        }
        map.put("result",2);
        return map;
    }



    @RequestMapping(value = "/deleteemboutstore",method = RequestMethod.POST)
    @ResponseBody
    public int deleteEmbOutStore(@RequestParam("embOutStoreID")int embOutStoreID){
        int res = embOutStoreService.deleteEmbOutStore(embOutStoreID);
        return res;
    }

    @RequestMapping(value = "/getamongembout",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getEmbOutDetail(@RequestParam(value = "from", required = false)String from,
                                               @RequestParam(value = "to", required = false)String to,
                                               @RequestParam(value = "orderName", required = false)String orderName)throws Exception{
        Map<String, Object> map = new LinkedHashMap<>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date fromDate = null;
        Date toDate = null;
        if (from != null){
            fromDate = sdf.parse(from);
        }
        if (to != null){
            toDate = sdf.parse(to);
        }
        List<EmbOutDetail> embOutDetailList = embOutStoreService.getAmongEmbOut(fromDate, toDate, orderName);
        List<String> orderNameList = new ArrayList<>();
        for (EmbOutDetail embOutDetail : embOutDetailList){
            orderNameList.add(embOutDetail.getOrderName());
        }
        List<ManufactureOrder> manufactureOrderList = manufactureOrderService.getManufactureOrderByOrderNameList(orderNameList);
        for (EmbOutDetail embOutDetail : embOutDetailList){
            for (ManufactureOrder manufactureOrder : manufactureOrderList){
                if (embOutDetail.getOrderName().equals(manufactureOrder.getOrderName())){
                    embOutDetail.setClothesVersionNumber(manufactureOrder.getClothesVersionNumber());
                }
            }
        }
        map.put("embOutDetailList", embOutDetailList);
        return map;
    }

    @RequestMapping(value = "/getemboutdetail",method = RequestMethod.GET)
    public String getEmbOutDetail(Model model,@RequestParam("fromDate")String fromDate,
                                              @RequestParam("toDate")String toDate)throws Exception{
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date from = sdf.parse(fromDate);
        Date to = sdf.parse(toDate);
        List<EmbOutDetail> embOutDetailList = embOutStoreService.getEmbOutDetail(from, to);
        if (embOutDetailList != null && embOutDetailList.size()>0){
            for (EmbOutDetail embOutDetail : embOutDetailList){
                String orderName = embOutDetail.getOrderName();
                embOutDetail.setClothesVersionNumber(orderClothesService.getVersionNumberByOrderName(orderName));
            }
        }
        model.addAttribute("embOutDetailList",embOutDetailList);
        return "embMarket/fb_embOutDetailList";
    }

    @RequestMapping(value = "/minigetemboutdetail",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> miniGetEmbOutDetail(@RequestParam("fromDate")String fromDate,
                                  @RequestParam("toDate")String toDate)throws Exception{
        Map<String, Object> map = new LinkedHashMap<>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date from = sdf.parse(fromDate);
        Date to = sdf.parse(toDate);
        List<EmbOutDetail> embOutDetailList = embOutStoreService.getEmbOutDetail(from, to);
        if (embOutDetailList != null && embOutDetailList.size()>0){
            for (EmbOutDetail embOutDetail : embOutDetailList){
                String orderName = embOutDetail.getOrderName();
                embOutDetail.setClothesVersionNumber(orderClothesService.getVersionNumberByOrderName(orderName));
            }
        }
        map.put("embOutDetailList",embOutDetailList);
        return map;
    }

    @RequestMapping(value = "/getemboutpackagedetail",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getEmbOutPackageDetail(@RequestParam("orderName")String orderName,
                                                      @RequestParam(value = "from", required = false)String from,
                                                      @RequestParam(value = "to", required = false)String to,
                                                      @RequestParam(value = "groupName", required = false)String groupName){
        Map<String,Object> map = new LinkedHashMap<>();
        Map<String,Object> summaryMap = new LinkedHashMap<>();
        List<TailorReport> reportList = embOutStoreService.getEmbOutPackageDetail(orderName,from,to,groupName);
        List<TailorReport> packageList = embOutStoreService.getEmbOutPackageCountByInfo(orderName,from,to,groupName);
        List<String> colorList = new ArrayList<>();
        List<String> sizeList = new ArrayList<>();
        for (TailorReport tailorReport : reportList){
            if (!colorList.contains(tailorReport.getColorName())){
                colorList.add(tailorReport.getColorName());
            }
            if (!sizeList.contains(tailorReport.getSizeName())){
                sizeList.add(tailorReport.getSizeName());
            }
        }
        List<String> desiredOrder = Arrays.asList("XXXXXS","5XS","XXXXS","4XS","XXXS","3XS","XXS","2XS","XS","XS/25","xs","S","S/27","M","M/29","L","L/31","XL",
                "XL/33","XXL","2XL","XXL/36","XXXL","3XL","XXXL/38","4XL","XXXXL","5XL","XXXXXL","6XL","XXXXXXL","80","090","90","100","110","110/27",
                "120","120/29","130","130/31","140","140/33","150","150/36","155","160","165","170","175","180","185","190");
        Comparator<String> sizeOrder = Comparator.comparingInt(desiredOrder::indexOf);
        sizeList.sort(sizeOrder);
        for (String color : colorList){
            Map<String, Object> tmpSizeMap = new LinkedHashMap<>();
            for (String size : sizeList){
                int tmpCount = 0;
                for (TailorReport tailorReport : reportList){
                    if (color.equals(tailorReport.getColorName()) && size.equals(tailorReport.getSizeName())){
                        tmpCount += tailorReport.getLayerCount();
                    }
                }
                tmpSizeMap.put(size,tmpCount);
            }
            summaryMap.put(color, tmpSizeMap);
        }

        int oneLength = 0;
        int twoLength = 0;
        int threeLength = 0;
        if (reportList.size() == 0){
            oneLength = 0;
            twoLength = 0;
            threeLength = 0;
        } else if (reportList.size() == 1){
            oneLength = 1;
            twoLength = 0;
            threeLength = 0;
        } else if (reportList.size() == 2){
            oneLength = 1;
            twoLength = 1;
            threeLength = 0;
        } else if (reportList.size()%3 == 0){
            oneLength = reportList.size()/3;
            twoLength = reportList.size()/3;
            threeLength = reportList.size()/3;
        } else if (reportList.size()%3 == 1){
            oneLength = reportList.size()/3 + 1;
            twoLength = reportList.size()/3;
            threeLength = reportList.size()/3;
        }else if (reportList.size()%3 == 2){
            oneLength = reportList.size()/3 + 1;
            twoLength = reportList.size()/3 + 1;
            threeLength = reportList.size()/3;
        }
        map.put("oneLength",oneLength);
        map.put("twoLength",twoLength);
        map.put("threeLength",threeLength);
        map.put("detail",reportList);
        map.put("summary",summaryMap);
        map.put("colorList",colorList);
        map.put("sizeList",sizeList);
        map.put("packageCount",packageList);
        return map;
    }


    @RequestMapping(value = "/queryembleak",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> searchCutQueryLeak(@RequestParam("orderName")String orderName) {
        List<CutQueryLeak> tailorInfoList = tailorService.getTailorInfoByOrderPartType(orderName, null, 0, null, null);
        List<CutQueryLeak> embOutList = embOutStoreService.getEmbOutInfoByOrder(orderName);
        List<EmbOutDaily> embOutDailyList = embOutStoreService.getEmbOutDaily(orderName);
        Map<String, Object> map = new LinkedHashMap<>();
        Map<String, Object> colorMap = new LinkedHashMap<>();
        List<String> colorList = new ArrayList<>();
        List<String> sizeList = new ArrayList<>();
        if (tailorInfoList == null || tailorInfoList.size() == 0){
            return null;
        }
        for(CutQueryLeak cutQueryLeak : tailorInfoList){
            if (!colorList.contains(cutQueryLeak.getColorName())){
                colorList.add(cutQueryLeak.getColorName());
            }
            if (!sizeList.contains(cutQueryLeak.getSizeName())){
                sizeList.add(cutQueryLeak.getSizeName());
            }
        }
        List<String> desiredOrder = Arrays.asList("XXXXXS","5XS","XXXXS","4XS","XXXS","3XS","XXS","2XS","XS","XS/25","xs","S","S/27","M","M/29","L","L/31","XL",
                "XL/33","XXL","2XL","XXL/36","XXXL","3XL","XXXL/38","4XL","XXXXL","5XL","XXXXXL","6XL","XXXXXXL","80","090","90","100","110","110/27",
                "120","120/29","130","130/31","140","140/33","150","150/36","155","160","165","170","175","180","185","190");
        Comparator<String> sizeOrder = Comparator.comparingInt(desiredOrder::indexOf);
        sizeList.sort(sizeOrder);
        for (String color : colorList){
            Map<String,Object> tmpSizeMap = new LinkedHashMap<>();
            for (String size : sizeList){
                int tmpPlanCount = 0;
                int tmpActualCount = 0;
                for (CutQueryLeak cutQueryLeak1 : tailorInfoList){
                    if (cutQueryLeak1.getColorName().equals(color) && cutQueryLeak1.getSizeName().equals(size)){
                        tmpPlanCount = cutQueryLeak1.getWellCount();
                    }
                }
                for (CutQueryLeak cutQueryLeak2 : embOutList){
                    if (cutQueryLeak2.getColorName().equals(color) && cutQueryLeak2.getSizeName().equals(size)){
                        tmpActualCount = cutQueryLeak2.getLayerCount();
                    }
                }
                ReportCompare rc = new ReportCompare(tmpPlanCount,tmpActualCount,tmpActualCount-tmpPlanCount);
                tmpSizeMap.put(size,rc);
            }
            colorMap.put(color,tmpSizeMap);
        }
        map.put("total",colorMap);
        Map<String, Object> dailyMap = new LinkedHashMap<>();
        List<Date> dateList = new ArrayList<>();
        for (EmbOutDaily embOutDaily : embOutDailyList){
            if (!dateList.contains(embOutDaily.getOutDate())){
                dateList.add(embOutDaily.getOutDate());
            }
        }
        for (Date date : dateList){
            Map<String,Object> dailyColorMap = new LinkedHashMap<>();
            for (String color : colorList){
                Map<String,Object> dailySizeMap = new LinkedHashMap<>();
                int countSize = 0;
                for (String size : sizeList){
                    int tmpCount = 0;
                    for (EmbOutDaily embOutDaily : embOutDailyList){
                        if (date.equals(embOutDaily.getOutDate()) && color.equals(embOutDaily.getColorName()) && size.equals(embOutDaily.getSizeName())){
                            tmpCount += embOutDaily.getOutCount();
                            countSize ++;
                        }
                    }
                    dailySizeMap.put(size,tmpCount);
                }
                if (countSize > 0){
                    dailyColorMap.put(color,dailySizeMap);
                }
            }
            dailyMap.put(date.toString(),dailyColorMap);
        }
        map.put("daily",dailyMap);
        return map;
    }


    @RequestMapping(value = "/miniqueryembleak",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> miniSearchCutQueryLeak(@RequestParam("orderName")String orderName) {
        List<CutQueryLeak> tailorInfoList = tailorService.getTailorInfoByOrderPartType(orderName, null, 0, null, null);
        List<CutQueryLeak> embOutList = embOutStoreService.getEmbOutInfoByOrder(orderName);
        Map<String, Object> colorMap = new LinkedHashMap<>();
        List<String> colorList = new ArrayList<>();
        List<String> sizeList = new ArrayList<>();
        if (tailorInfoList == null || tailorInfoList.size() == 0){
            return null;
        }
        for(CutQueryLeak cutQueryLeak : tailorInfoList){
            if (!colorList.contains(cutQueryLeak.getColorName())){
                colorList.add(cutQueryLeak.getColorName());
            }
            if (!sizeList.contains(cutQueryLeak.getSizeName())){
                sizeList.add(cutQueryLeak.getSizeName());
            }
        }
        List<String> desiredOrder = Arrays.asList("XXXXXS","5XS","XXXXS","4XS","XXXS","3XS","XXS","2XS","XS","XS/25","xs","S","S/27","M","M/29","L","L/31","XL",
                "XL/33","XXL","2XL","XXL/36","XXXL","3XL","XXXL/38","4XL","XXXXL","5XL","XXXXXL","6XL","XXXXXXL","80","090","90","100","110","110/27",
                "120","120/29","130","130/31","140","140/33","150","150/36","155","160","165","170","175","180","185","190");
        Comparator<String> sizeOrder = Comparator.comparingInt(desiredOrder::indexOf);
        sizeList.sort(sizeOrder);
        for (String color : colorList){
            Map<String,Object> tmpSizeMap = new LinkedHashMap<>();
            for (String size : sizeList){
                int tmpPlanCount = 0;
                int tmpActualCount = 0;
                for (CutQueryLeak cutQueryLeak1 : tailorInfoList){
                    if (cutQueryLeak1.getColorName().equals(color) && cutQueryLeak1.getSizeName().equals(size)){
                        tmpPlanCount = cutQueryLeak1.getLayerCount();
                    }
                }
                for (CutQueryLeak cutQueryLeak2 : embOutList){
                    if (cutQueryLeak2.getColorName().equals(color) && cutQueryLeak2.getSizeName().equals(size)){
                        tmpActualCount = cutQueryLeak2.getLayerCount();
                    }
                }
                ReportCompare rc = new ReportCompare(tmpPlanCount,tmpActualCount,tmpActualCount-tmpPlanCount);
                tmpSizeMap.put(size,rc);
            }
            colorMap.put(color,tmpSizeMap);
        }
        return colorMap;
    }


    @RequestMapping(value = "/minisearchembleakbyorder",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> miniSearchEmbLeakByOrder(@RequestParam("orderName")String orderName,
                                                        @RequestParam(value = "colorName", required = false)String colorName,
                                                        @RequestParam(value = "sizeName", required = false)String sizeName) {
        Map<String, Object> map = new LinkedHashMap<>();
        if ("".equals(colorName) || "全部".equals(colorName)){
            colorName = null;
        }
        if ("".equals(sizeName) || "全部".equals(sizeName)){
            sizeName = null;
        }
        List<CutQueryLeak> tailorInfoList = tailorService.getTailorInfoGroupByColorSize(orderName, colorName, sizeName, null, 0);
        List<CutQueryLeak> embInList = embInStoreService.getEmbInStoreInfoGroupByColorSize(orderName, colorName, sizeName);
        List<CutQueryLeak> embOutList = embOutStoreService.getEmbOutStoreGroupByColorSize(orderName, colorName, sizeName);
        for (CutQueryLeak cutQueryLeak : tailorInfoList){
            for (CutQueryLeak embIn: embInList){
                if (embIn.getColorName().equals(cutQueryLeak.getColorName()) && embIn.getSizeName().equals(cutQueryLeak.getSizeName())){
                    cutQueryLeak.setInStoreCount(embIn.getInStoreCount());
                }
            }
            for (CutQueryLeak embOut: embOutList){
                if (embOut.getColorName().equals(cutQueryLeak.getColorName()) && embOut.getSizeName().equals(cutQueryLeak.getSizeName())){
                    cutQueryLeak.setOutStoreCount(embOut.getOutStoreCount());
                }
            }
            cutQueryLeak.setStorageCount(cutQueryLeak.getInStoreCount() - cutQueryLeak.getOutStoreCount());
        }
        map.put("embLeakList", tailorInfoList);
        return map;
    }



    @RequestMapping(value = "/miniemboutprecheck",method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> miniEmbOutPreCheck(@RequestParam("groupName")String groupName,
                                                 @RequestParam("tailorQcodeID")Integer tailorQcodeID){
        Map<String, Object> map = new LinkedHashMap<>();
        Tailor tailor = tailorService.getTailorByTailorQcodeID(tailorQcodeID, 0);
        if (tailor == null){
            return map;
        }
        map.put("tailor", tailor);
        String orderName = tailor.getOrderName();
        String colorName = tailor.getColorName();
        String sizeName = tailor.getSizeName();
        Integer bedNumber = tailor.getBedNumber();
        Integer packageNumber = tailor.getPackageNumber();
        String embStoreLocation = embStorageService.getLocationByOrderColorSizePackage(orderName,bedNumber,colorName,sizeName,packageNumber);
        EmbOutStore embOutStore = embOutStoreService.getEmbOutStoreByInfo(orderName, bedNumber, packageNumber);
        if (embStoreLocation == null){
            if (embOutStore != null){
                map.put("outFinish", "已出库");
            }
            return map;
        }
        map.put("embStoreLocation", embStoreLocation);
        List<EmbStorage> embStorageList = embStorageService.getEmbStorageByOrderColorSize(embStoreLocation,orderName,colorName,sizeName);
        Integer packageCount = 0;
        Integer layerSum = 0;
        for (EmbStorage embStorage : embStorageList){
            packageCount += 1;
            layerSum += embStorage.getLayerCount();
        }
        map.put("packageCount", packageCount);
        map.put("layerSum", layerSum);
        Date outTime = new Date();
        List<EmbPlan> embPlanList = embPlanService.getEmbPlansByOrderGroupDate(orderName, groupName, outTime);
        if (embPlanList == null || embPlanList.isEmpty()){
            map.put("isOut", 0);
            map.put("groupName", groupName);
            return map;
        }
        for (EmbPlan embPlan : embPlanList){
            boolean colorFlag = false;
            boolean sizeFlag = false;
            if (embPlan.getColorName() == null || embPlan.getColorName().equals("全部") || embPlan.getColorName().equals(colorName)){
                colorFlag = true;
            }
            if (embPlan.getSizeName() == null || embPlan.getSizeName().equals("全部") || embPlan.getSizeName().equals(sizeName)){
                sizeFlag = true;
            }
            if (colorFlag && sizeFlag){
                map.put("embPlan", embPlan);
                Integer actCount = embPlan.getActCount();
                Float driftPercent = embPlan.getDriftPercent();
                Integer planCount = embPlan.getPlanCount();
                int objCount =  (int)(planCount * (driftPercent/100 + 1));
                if (layerSum + actCount > objCount){
                    map.put("isOut", 1);
                }else {
                    map.put("isOut", 0);
                }
                map.put("groupName", groupName);
                return map;
            }

        }
        map.put("isOut", 1);
        map.put("groupName", groupName);
        return map;

    }


    @RequestMapping(value = "/miniembbatchoutprecheck",method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> miniEmbBatchOutPreCheck(@RequestParam("groupName")String groupName,
                                                      @RequestParam("orderName")String orderName,
                                                      @RequestParam("colorName")String colorName,
                                                      @RequestParam("sizeName")String sizeName,
                                                      @RequestParam("layerCount")Integer layerCount){
        Map<String, Object> map = new LinkedHashMap<>();
        Date outTime = new Date();
        List<EmbPlan> embPlanList = embPlanService.getEmbPlansByOrderGroupDate(orderName, groupName, outTime);
        if (embPlanList != null && !embPlanList.isEmpty()){
            for (EmbPlan embPlan : embPlanList){
                boolean colorFlag = false;
                boolean sizeFlag = false;
                if (embPlan.getColorName() == null || embPlan.getColorName().equals("全部") || embPlan.getColorName().equals(colorName)){
                    colorFlag = true;
                }
                if (embPlan.getSizeName() == null || embPlan.getSizeName().equals("全部") || embPlan.getSizeName().equals(sizeName)){
                    sizeFlag = true;
                }
                if (colorFlag && sizeFlag){
                    Integer actCount = embPlan.getActCount();
                    Float driftPercent = embPlan.getDriftPercent();
                    Integer planCount = embPlan.getPlanCount();
                    int objCount =  (int)(planCount * (driftPercent/100 + 1));
                    map.put("orderName", orderName);
                    map.put("planCount", objCount);
                    map.put("actCount", actCount);
                    map.put("remainCount", objCount - actCount);
                    if ((objCount - actCount) >= layerCount){
                        map.put("isOut", 0);
                    } else {
                        map.put("isOut", 1);
                    }
                }
            }
            return map;
        } else {
            map.put("orderName", orderName);
            map.put("planCount", 100000);
            map.put("actCount", 0);
            map.put("remainCount", 100000);
            map.put("isOut", 0);
            return map;
        }

    }


    @RequestMapping(value = "/miniemboutsingleprecheck",method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> miniEmbOutSinglePreCheck(@RequestParam("groupName")String groupName,
                                                       @RequestParam("tailorQcodeID")Integer tailorQcodeID){
        Map<String, Object> map = new LinkedHashMap<>();
        Tailor tailor = tailorService.getTailorByTailorQcodeID(tailorQcodeID, 0);
        if (tailor == null){
            return map;
        }
        map.put("tailor", tailor);
        String orderName = tailor.getOrderName();
        String colorName = tailor.getColorName();
        String sizeName = tailor.getSizeName();
        Integer bedNumber = tailor.getBedNumber();
        Integer packageNumber = tailor.getPackageNumber();
        String embStoreLocation = embStorageService.getLocationByOrderColorSizePackage(orderName,bedNumber,colorName,sizeName,packageNumber);
        EmbOutStore embOutStore = embOutStoreService.getEmbOutStoreByInfo(orderName, bedNumber, packageNumber);
        if (embStoreLocation == null){
            if (embOutStore != null){
                map.put("outFinish", "已出库");
            }
            return map;
        }
        map.put("layer", tailor.getLayerCount());
        map.put("embStoreLocation", embStoreLocation);
        List<EmbStorage> embStorageList = embStorageService.getEmbStorageByOrderColorSize(embStoreLocation,orderName,colorName,sizeName);
        Integer packageCount = 0;
        Integer layerSum = 0;
        for (EmbStorage embStorage : embStorageList){
            packageCount += 1;
            layerSum += embStorage.getLayerCount();
        }
        map.put("packageCount", packageCount);
        map.put("layerSum", layerSum);
        Date outTime = new Date();
        List<EmbPlan> embPlanList = embPlanService.getEmbPlansByOrderGroupDate(orderName, groupName, outTime);
        if (embPlanList == null || embPlanList.isEmpty()){
            EmbPlan ep = new EmbPlan();
            ep.setOrderName(orderName);
            ep.setActCount(0);
            ep.setPlanCount(1000000);
            ep.setGroupName(groupName);
            ep.setDriftPercent(100);
            map.put("embPlan", ep);
            map.put("groupName", groupName);
            return map;
        }

        for (EmbPlan embPlan : embPlanList){
            boolean colorFlag = false;
            boolean sizeFlag = false;
            if (embPlan.getColorName() == null || embPlan.getColorName().equals("全部") || embPlan.getColorName().equals(colorName)){
                colorFlag = true;
            }
            if (embPlan.getSizeName() == null || embPlan.getSizeName().equals("全部") || embPlan.getSizeName().equals(sizeName)){
                sizeFlag = true;
            }
            if (colorFlag && sizeFlag){
                map.put("embPlan", embPlan);
                map.put("groupName", groupName);
                return map;
            }

        }
        map.put("groupName", groupName);
        return map;
    }

    @RequestMapping(value = "/miniembsinglescan",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> minEmbSingleScan(@RequestParam("tailorQcodeID")Integer tailorQcodeID){
        Map<String, Object> map = new LinkedHashMap<>();
        Tailor tailor = tailorService.getTailorByTailorQcodeID(tailorQcodeID, 0);
        if (tailor == null){
            map.put("error", "二维码信息不存在");
        }
        String embStoreLocation = embStorageService.getLocationByOrderColorSizePackage(tailor.getOrderName(),tailor.getBedNumber(),tailor.getColorName(),tailor.getSizeName(),tailor.getPackageNumber());
        if (embStoreLocation == null){
            map.put("error", "未入库,请先入库");
        }
        EmbOutStore embOutStore = embOutStoreService.getEmbOutStoreByInfo(tailor.getOrderName(), tailor.getBedNumber(), tailor.getPackageNumber());
        if (embOutStore != null){
            map.put("error", "重复出库");
        }
        map.put("tailor", tailor);
        return map;
    }

    @RequestMapping(value = "/minidirectembsinglescan",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> minDirectEmbSingleScan(@RequestParam("tailorQcodeID")Integer tailorQcodeID){
        Map<String, Object> map = new LinkedHashMap<>();
        Tailor tailor = tailorService.getTailorByTailorQcodeID(tailorQcodeID, 0);
        if (tailor == null){
            map.put("error", "二维码信息不存在");
        }
        EmbOutStore embOutStore = embOutStoreService.getEmbOutStoreByInfo(tailor.getOrderName(), tailor.getBedNumber(), tailor.getPackageNumber());
        if (embOutStore != null){
            map.put("error", "重复出库");
        }
        map.put("tailor", tailor);
        return map;
    }



    @RequestMapping(value = "/miniaddemboutstorenew",method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> miniAddEmbOutStoreNew(@RequestParam("groupName")String groupName,
                                                    @RequestParam("tailorQcodeID")Integer tailorQcodeID,
                                                    @RequestParam("embStoreLocation")String embStoreLocation){
        Map<String, Object> map = new LinkedHashMap<>();
        Tailor tailor = tailorService.getTailorByTailorQcodeID(tailorQcodeID, 0);
        String orderName = tailor.getOrderName();
        String colorName = tailor.getColorName();
        String sizeName = tailor.getSizeName();
        List<EmbStorage> embStorageList = embStorageService.getEmbStorageByOrderColorSize(embStoreLocation,orderName,colorName,sizeName);
        Integer embOutCount = embStorageList.size();
        Integer layerSum = 0;
        int res1 = embStorageService.embOutStoreBatch(embStoreLocation,orderName,colorName,sizeName);
        List<EmbOutStore> embOutStoreList = new ArrayList<>();
        for (EmbStorage embStorage : embStorageList){
            layerSum += embStorage.getLayerCount();
            EmbOutStore eos = new EmbOutStore(groupName,orderName,embStorage.getBedNumber(),colorName,sizeName,embStorage.getLayerCount(),embStorage.getPackageNumber(),embStorage.getTailorQcode());
            embOutStoreList.add(eos);
        }
        int res2 = embOutStoreService.addEmbOutStore(embOutStoreList);
        Date outTime = new Date();
        List<EmbPlan> embPlanList = embPlanService.getEmbPlansByOrderGroupDate(orderName, groupName, outTime);
        if (embPlanList == null || embPlanList.isEmpty()){
            if (0 == res1 && 0 == res2){
                map.put("result",0);
                map.put("embStoreLocation",embStoreLocation);
                map.put("embOutCount",embOutCount);
                return map;
            }else {
                map.put("result",1);
            }
            return map;
        }
        for (EmbPlan embPlan : embPlanList){
            if ((embPlan.getColorName() == null || embPlan.getColorName().equals("全部") || embPlan.getColorName().equals(colorName)) && (embPlan.getSizeName() == null || embPlan.getSizeName().equals("全部") || embPlan.getSizeName().equals(sizeName))){
                Integer actCount = embPlan.getActCount() + layerSum;
                Float achievePercent = 0f;
                if (embPlan.getPlanCount() > 0){
                    achievePercent = (float) actCount/embPlan.getPlanCount();
                }
                int res3 = embPlanService.updateEmbPlanOnOut(embPlan.getEmbPlanID(), actCount, achievePercent);
                if (0 == res1 && 0 == res2 && 0 == res3){
                    map.put("result",0);
                    map.put("embStoreLocation",embStoreLocation);
                    map.put("embOutCount",embOutCount);
                    return map;
                } else {
                    map.put("result",1);
                }
                return map;
            }
        }
        map.put("result",1);
        return map;
    }


    @RequestMapping(value = "/miniemboutstorebatchnew",method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> miniEmbOutStoreBatchNew(@RequestParam("groupName")String groupName,
                                                      @RequestParam("orderName")String orderName,
                                                      @RequestParam("colorName")String colorName,
                                                      @RequestParam("sizeName")String sizeName,
                                                      @RequestParam("embStoreLocation")String embStoreLocation){
        Map<String, Object> map = new LinkedHashMap<>();
        List<EmbStorage> embStorageList = embStorageService.getEmbStorageByOrderColorSize(embStoreLocation,orderName,colorName,sizeName);
        Integer embOutCount = embStorageList.size();
        Integer layerSum = 0;
        int res1 = embStorageService.embOutStoreBatch(embStoreLocation,orderName,colorName,sizeName);
        List<EmbOutStore> embOutStoreList = new ArrayList<>();
        for (EmbStorage embStorage : embStorageList){
            layerSum += embStorage.getLayerCount();
            EmbOutStore eos = new EmbOutStore(groupName,orderName,embStorage.getBedNumber(),colorName,sizeName,embStorage.getLayerCount(),embStorage.getPackageNumber(),embStorage.getTailorQcode());
            embOutStoreList.add(eos);
        }
        int res2 = embOutStoreService.addEmbOutStore(embOutStoreList);
        Date outTime = new Date();
        List<EmbPlan> embPlanList = embPlanService.getEmbPlansByOrderGroupDate(orderName, groupName, outTime);
        if (embPlanList == null || embPlanList.isEmpty()){
            if (0 == res1 && 0 == res2){
                map.put("result",0);
                map.put("embStoreLocation",embStoreLocation);
                map.put("embOutCount",embOutCount);
                return map;
            }else {
                map.put("result",1);
            }
            return map;
        }
        for (EmbPlan embPlan : embPlanList){
            if ((embPlan.getColorName() == null || embPlan.getColorName().equals("全部") || embPlan.getColorName().equals(colorName)) && (embPlan.getSizeName() == null || embPlan.getSizeName().equals("全部") || embPlan.getSizeName().equals(sizeName))){
                Integer actCount = embPlan.getActCount() + layerSum;
                Float achievePercent = 0f;
                if (embPlan.getPlanCount() > 0){
                    achievePercent = (float) actCount/embPlan.getPlanCount();
                }
                int res3 = embPlanService.updateEmbPlanOnOut(embPlan.getEmbPlanID(), actCount, achievePercent);
                if (0 == res1 && 0 == res2 && 0 == res3){
                    map.put("result",0);
                    map.put("embStoreLocation",embStoreLocation);
                    map.put("embOutCount",embOutCount);
                    return map;
                } else {
                    map.put("result",1);
                }
                return map;
            }
        }
        map.put("result",1);
        return map;
    }


    @RequestMapping(value = "/miniaddemboutstoresinglenew",method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> miniAddEmbOutStoreSingleNew(@RequestParam("groupName")String groupName,
                                                          @RequestParam("tailorQcodeIDList") List<Integer> tailorQcodeIDList,
                                                          @RequestParam("embStoreLocation")String embStoreLocation){
        Map<String, Object> map = new LinkedHashMap<>();
        List<Tailor> tailorList = tailorService.getTailorsByTailorQcodeIDs(tailorQcodeIDList, 0);
        List<EmbOutStore> embOutStoreList = new ArrayList<>();
        List<EmbOutInfo> embOutInfoList = new ArrayList<>();
        String orderName = "";
        int layerSum = 0;
        String colorName = "";
        String sizeName = "";
        for (Tailor tailor : tailorList){
            orderName = tailor.getOrderName();
            colorName = tailor.getColorName();
            sizeName = tailor.getSizeName();
            int layerCount = tailor.getLayerCount();
            int bedNumber = tailor.getBedNumber();
            int packageNumber = tailor.getPackageNumber();
            layerSum += layerCount;
            EmbOutStore eos = new EmbOutStore(groupName,orderName,bedNumber,colorName,sizeName,layerCount,packageNumber,tailor.getTailorQcode());
            embOutStoreList.add(eos);
            EmbOutInfo embOutInfo = new EmbOutInfo(orderName, bedNumber, packageNumber);
            embOutInfoList.add(embOutInfo);
        }
        int res1 = embStorageService.embOutStoreSingle(embOutInfoList);
        if ("异常".equals(groupName)){
            if (res1 == 0){
                map.put("result", 0);
                map.put("embStoreLocation", embStoreLocation);
                map.put("embOutCount", embOutInfoList.size());
            }else {
                map.put("result", 1);
            }
            return map;

        }
        int res2 = embOutStoreService.addEmbOutStore(embOutStoreList);
        Date outTime = new Date();
        List<EmbPlan> embPlanList = embPlanService.getEmbPlansByOrderGroupDate(orderName, groupName, outTime);
        if (embPlanList == null || embPlanList.isEmpty()){
            if (0 == res1 && 0 == res2){
                map.put("result",0);
                map.put("embStoreLocation",embStoreLocation);
                map.put("embOutCount",embOutInfoList.size());
                return map;
            }else {
                map.put("result",1);
            }
            return map;
        }

        for (EmbPlan embPlan : embPlanList){
            if ((embPlan.getColorName() == null || embPlan.getColorName().equals("全部") || embPlan.getColorName().equals(colorName)) && (embPlan.getSizeName() == null || embPlan.getSizeName().equals("全部") || embPlan.getSizeName().equals(sizeName))){
                Integer actCount = embPlan.getActCount() + layerSum;
                Float achievePercent = 0f;
                if (embPlan.getPlanCount() > 0){
                    achievePercent = (float) actCount/embPlan.getPlanCount();
                }
                int res3 = embPlanService.updateEmbPlanOnOut(embPlan.getEmbPlanID(), actCount, achievePercent);
                if (0 == res1 && 0 == res2 && 0 == res3){
                    map.put("result",0);
                    map.put("embStoreLocation",embStoreLocation);
                    map.put("embOutCount",embOutInfoList.size());
                    return map;
                } else {
                    map.put("result",1);
                }
                return map;
            }
        }
        map.put("result",1);
        return map;
    }


    @RequestMapping(value = "/miniemboutstoresinglescan",method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> miniEmbOutStoreSingleScan(EmbOutDto embOutDto){
        Map<String, Object> map = new LinkedHashMap<>();
        List<Tailor> tailorList = tailorService.getTailorsByTailorQcodeIDs(embOutDto.getTailorQcodeIDList(), 0);
        List<EmbOutStore> embOutStoreList = new ArrayList<>();
        List<EmbOutInfo> embOutInfoList = new ArrayList<>();
        String orderName = "";
        int layerSum = 0;
        String colorName = "";
        String sizeName = "";
        for (Tailor tailor : tailorList){
            orderName = tailor.getOrderName();
            colorName = tailor.getColorName();
            sizeName = tailor.getSizeName();
            int layerCount = tailor.getLayerCount();
            int bedNumber = tailor.getBedNumber();
            int packageNumber = tailor.getPackageNumber();
            layerSum += layerCount;
            EmbOutStore eos = new EmbOutStore(embOutDto.getGroupName(),orderName,bedNumber,colorName,sizeName,layerCount,packageNumber,tailor.getTailorQcode());
            embOutStoreList.add(eos);
            EmbOutInfo embOutInfo = new EmbOutInfo(orderName, bedNumber, packageNumber);
            embOutInfoList.add(embOutInfo);
        }
        int res1 = embStorageService.embOutStoreSingle(embOutInfoList);
        if ("异常".equals(embOutDto.getGroupName())){
            if (res1 == 0){
                map.put("result", 0);
                map.put("embOutCount", embOutInfoList.size());
            }else {
                map.put("result", 1);
            }
            return map;

        }
        int res2 = embOutStoreService.addEmbOutStore(embOutStoreList);
        Date outTime = new Date();
        List<EmbPlan> embPlanList = embPlanService.getEmbPlansByOrderGroupDate(orderName, embOutDto.getGroupName(), outTime);
        if (embPlanList == null || embPlanList.isEmpty()){
            if (0 == res1 && 0 == res2){
                map.put("result",0);
                map.put("embOutCount",embOutInfoList.size());
                return map;
            }else {
                map.put("result",1);
            }
            return map;
        }

        for (EmbPlan embPlan : embPlanList){
            if ((embPlan.getColorName() == null || embPlan.getColorName().equals("全部") || embPlan.getColorName().equals(colorName)) && (embPlan.getSizeName() == null || embPlan.getSizeName().equals("全部") || embPlan.getSizeName().equals(sizeName))){
                Integer actCount = embPlan.getActCount() + layerSum;
                Float achievePercent = 0f;
                if (embPlan.getPlanCount() > 0){
                    achievePercent = (float) actCount/embPlan.getPlanCount();
                }
                int res3 = embPlanService.updateEmbPlanOnOut(embPlan.getEmbPlanID(), actCount, achievePercent);
                if (0 == res1 && 0 == res2 && 0 == res3){
                    map.put("result",0);
                    map.put("embOutCount",embOutInfoList.size());
                    return map;
                } else {
                    map.put("result",1);
                }
                return map;
            }
        }
        map.put("result",1);
        return map;
    }


    @RequestMapping(value = "/miniembdirectoutstoresinglescan",method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> miniEmbDirectOutStoreSingleScan(EmbOutDto embOutDto){
        Map<String, Object> map = new LinkedHashMap<>();
        List<Tailor> tailorList = tailorService.getTailorsByTailorQcodeIDs(embOutDto.getTailorQcodeIDList(), 0);
        List<EmbOutStore> embOutStoreList = new ArrayList<>();
        List<EmbOutInfo> embOutInfoList = new ArrayList<>();
        List<EmbStorage> embStorageList = new ArrayList<>();
        String orderName = "";
        int layerSum = 0;
        String colorName = "";
        String sizeName = "";
        for (Tailor tailor : tailorList){
            orderName = tailor.getOrderName();
            colorName = tailor.getColorName();
            sizeName = tailor.getSizeName();
            int layerCount = tailor.getLayerCount();
            int bedNumber = tailor.getBedNumber();
            int packageNumber = tailor.getPackageNumber();
            layerSum += layerCount;
            //这里添加衣胚入库记录
            EmbStorage embStorage = new EmbStorage("B11-1-1", orderName, bedNumber, colorName, sizeName, layerCount, packageNumber, tailor.getTailorQcode());
            EmbOutStore eos = new EmbOutStore(embOutDto.getGroupName(),orderName,bedNumber,colorName,sizeName,layerCount,packageNumber,tailor.getTailorQcode());
            embOutStoreList.add(eos);
            EmbOutInfo embOutInfo = new EmbOutInfo(orderName, bedNumber, packageNumber);
            embOutInfoList.add(embOutInfo);
            embStorageList.add(embStorage);
        }
        embInStoreService.addEmbInStore(embStorageList);
        int res1 = embStorageService.embOutStoreSingle(embOutInfoList);
        int res2 = embOutStoreService.addEmbOutStore(embOutStoreList);
        if (0 == res1 && 0 == res2){
            map.put("result",0);
            map.put("embOutCount",embOutInfoList.size());
            return map;
        }else {
            map.put("result",1);
        }
        return map;
    }

}
