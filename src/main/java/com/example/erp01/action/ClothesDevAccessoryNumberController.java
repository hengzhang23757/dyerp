package com.example.erp01.action;

import com.example.erp01.model.ClothesDevAccessoryNumber;
import com.example.erp01.service.ClothesDevAccessoryNumberService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "erp")
public class ClothesDevAccessoryNumberController {

    @Autowired
    private ClothesDevAccessoryNumberService clothesDevAccessoryNumberService;

    @RequestMapping("/clothesDevAccessoryNumberStart")
    public String clothesDevAccessoryNumberStart(){
        return "basicData/accessoryNumberManage";
    }

    @RequestMapping(value = "/addclothesdevaccessorynumber",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> addClothesDevAccessoryNumber(@RequestParam("clothesDevAccessoryNumberJson")String clothesDevAccessoryNumberJson){
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        ClothesDevAccessoryNumber clothesDevAccessoryNumber = gson.fromJson(clothesDevAccessoryNumberJson, ClothesDevAccessoryNumber.class);
        int res = clothesDevAccessoryNumberService.addClothesDevAccessoryNumber(clothesDevAccessoryNumber);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/deleteclothesdevaccessorynumber",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> deleteClothesDevAccessoryNumber(@RequestParam("id")Integer id){
        Map<String, Object> map = new LinkedHashMap<>();
        int res = clothesDevAccessoryNumberService.deleteClothesDevAccessoryNumber(id);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/getallclothesdevaccessorynumber",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getAllCustomer(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<ClothesDevAccessoryNumber> clothesDevAccessoryNumberList = clothesDevAccessoryNumberService.getAllClothesDevAccessoryNumber();
        map.put("data",clothesDevAccessoryNumberList);
        return map;
    }

    @RequestMapping(value = "/updateclothesdevaccessorynumber",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> updateClothesDevAccessoryNumber(@RequestParam("clothesDevAccessoryNumberJson")String clothesDevAccessoryNumberJson){
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        ClothesDevAccessoryNumber clothesDevAccessoryNumber = gson.fromJson(clothesDevAccessoryNumberJson, ClothesDevAccessoryNumber.class);
        int res = clothesDevAccessoryNumberService.updateClothesDevAccessoryNumber(clothesDevAccessoryNumber);
        map.put("result", res);
        return map;
    }


}
