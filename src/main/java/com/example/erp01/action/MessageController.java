package com.example.erp01.action;

import com.example.erp01.model.Message;
import com.example.erp01.service.MessageService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "erp")
public class MessageController {

    @Autowired
    private MessageService messageService;

    @RequestMapping(value ="/messageWrite")
    public String messageWrite() {
        return "/views/app/message/writeMessage";
    }

    @RequestMapping(value = "/addmessagebatch",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> addMessageBatch(@RequestParam("messageJson") String messageJson){
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        List<Message> messageList = gson.fromJson(messageJson,new TypeToken<List<Message>>(){}.getType());
        int res = messageService.addMessageBatch(messageList);
        map.put("data", res);
        return map;
    }

    @RequestMapping(value = "/minigetmessagecountbytype",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getMessageCountByType(@RequestParam("receiveUser") String receiveUser){
        Map<String, Object> map = new LinkedHashMap<>();
        List<Message> messageList = messageService.getMessageCountGroupByType(receiveUser);
        map.put("messageList", messageList);
        return map;
    }


    @RequestMapping(value = "/getmessagebyinfo",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getMessageByInfo(@RequestParam(value = "receiveUser", required = false) String receiveUser,
                                                @RequestParam(value = "messageType", required = false) String messageType){
        Map<String, Object> map = new LinkedHashMap<>();
        List<Message> messageList = messageService.getMessageByInfo(receiveUser, messageType);
        map.put("data", messageList);
        map.put("code", 0);
        map.put("count", messageList.size());
        return map;
    }
    @RequestMapping(value = "/getSendedMessage",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getSendedMessage(@RequestParam("sendUser") String sendUser,@RequestParam("page") Integer page,
                                                @RequestParam("limit") Integer limit){
        Map<String, Object> map = new LinkedHashMap<>();
        Integer count = messageService.countBySender(sendUser);
        List<Message> messageList = messageService.getSendedMessageByPage(sendUser,page,limit);
        map.put("data", messageList);
        map.put("code", 0);
        map.put("count", count);
        return map;
    }

    @RequestMapping(value = "/minigetmessagebyinfo",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> miniGetMessageByInfo(@RequestParam("receiveUser") String receiveUser,
                                                    @RequestParam(value = "messageType", required = false) String messageType){
        Map<String, Object> map = new LinkedHashMap<>();
        List<Message> messageList = messageService.getMessageByInfo(receiveUser, messageType);
        map.put("messageList", messageList);
        map.put("code", 0);
        map.put("count", messageList.size());
        return map;
    }


    @RequestMapping(value = "/updatemessagereadtype",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> updateMessageReadType(@RequestParam("idList") List<Integer> idList){
        Map<String, Object> map = new LinkedHashMap<>();
        int res = messageService.updateMessageReadType(idList);
        map.put("res", res);
        return map;
    }

    @RequestMapping(value = "/checkmessagedot",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> checkMessageDot(@RequestParam("receiveUser") String receiveUser){
        Map<String, Object> map = new LinkedHashMap<>();
        int messageCount = messageService.getReadStateCountByMessageType(receiveUser, null);
        map.put("messageCount", messageCount);
        return map;
    }

    @RequestMapping(value = "/getmessagecountinfobytype",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getMessageCountInfoByType(@RequestParam("receiveUser") String receiveUser){
        Map<String, Object> map = new LinkedHashMap<>();
        List<Message> messageList = messageService.getMessageCountGroupByType(receiveUser);
        map.put("messageList", messageList);
        return map;
    }

    @RequestMapping(value = "/getunfinishmessagecountinfobytype",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getUnFinishMessageCountInfoByType(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<Message> messageList = messageService.getUnFinishMessageByType(null);
        map.put("messageList", messageList);
        return map;
    }

    @RequestMapping(value = "/getunfinishmessagetotalcount",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getUnFinishMessageTotalCount(){
        Map<String, Object> map = new LinkedHashMap<>();
        int messageCount = messageService.getUnFinishCountTotalCount();
        map.put("messageCount", messageCount);
        return map;
    }

    @RequestMapping(value = "/updatemessagereadtypebyreceiveuser",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> updateMessageReadTypeByReceiveUser(@RequestParam("receiveUser") String receiveUser,
                                                                  @RequestParam(value = "messageType", required = false) String messageType){
        Map<String, Object> map = new LinkedHashMap<>();
        int res = messageService.updateMessageReadTypeByReceiveUser(receiveUser, messageType);
        map.put("res", res);
        return map;
    }

    @RequestMapping(value = "/miniupdatemessagereadtypebytypeanduser",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> miniUpdateMessageReadTypeByReceiveUser(@RequestParam("receiveUser") String receiveUser,
                                                                      @RequestParam(value = "messageType", required = false) String messageType){
        Map<String, Object> map = new LinkedHashMap<>();
        int res = messageService.updateMessageReadTypeByReceiveUser(receiveUser, messageType);
        map.put("res", res);
        return map;
    }
}
