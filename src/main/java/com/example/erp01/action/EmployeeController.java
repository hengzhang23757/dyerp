package com.example.erp01.action;

import com.alibaba.druid.util.StringUtils;
import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClient;
import com.aliyun.oss.OSSClientBuilder;
import com.example.erp01.model.CheckDepartment;
import com.example.erp01.model.DepartmentGroup;
import com.example.erp01.model.Employee;
import com.example.erp01.service.DepartmentGroupService;
import com.example.erp01.service.EmployeeService;
import com.example.erp01.utils.AuthService;
import com.example.erp01.utils.BankCardUtils;
import com.example.erp01.utils.IdcardUtils;
import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import java.awt.image.BufferedImage;
import java.io.*;
import java.math.BigInteger;
import java.net.URI;
import java.net.URL;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
@Configuration      //1.主要用于标记配置类，兼备Component的效果。
@EnableScheduling   // 2.开启定时任务
@Controller
@RequestMapping(value = "erp")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private DepartmentGroupService departmentGroupService;

    private static BASE64Decoder decoder = new BASE64Decoder();


    @RequestMapping(value = "/employeeStart")
    public String employeeStart(Model model){
        model.addAttribute("bigMenuTag",10);
        model.addAttribute("menuTag",101);
        return "miniProgram/employee";
    }

    @RequestMapping(value ="/miniemployeelogin", method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> userLogin(@RequestParam("employeeNumber")String employeeNumber,
                                        @RequestParam("passWord")String passWord,
                                        HttpServletRequest request){
        System.out.println("用户名："+employeeNumber+"密码："+passWord);
        Map<String,Object> result = new HashMap<String,Object>();
        if(StringUtils.isEmpty(employeeNumber) || StringUtils.isEmpty(passWord)){
            result.put("flag",false);
            result.put("msg","用户名或密码不能为空！");
        }
        Employee employee = employeeService.employeeLogin(employeeNumber,passWord);
        if (employee != null){
            result.put("flag","true");
            result.put("msg","登录成功！");
            result.put("employee",employee);
//            request.getSession().setAttribute("employeeNumber", employee.getEmployeeNumber());
//            request.getSession().setAttribute("role", employee.getRole());
//            request.getSession().setMaxInactiveInterval(1800);
        }else{
            result.put("msg","登录失败，用户名或密码错误！");
            result.put("flag",false);
        }
        return result;
    }

    @RequestMapping(value ="/minicutemblogin", method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> miniCutEmbLogin(@RequestParam("employeeNumber")String employeeNumber,
                                              @RequestParam("passWord")String passWord){
        System.out.println("用户名："+employeeNumber+"密码："+passWord);
        Map<String,Object> result = new HashMap<String,Object>();
        if(StringUtils.isEmpty(employeeNumber) || StringUtils.isEmpty(passWord)){
            result.put("flag",false);
            result.put("msg","用户名或密码不能为空！");
        }
        Employee employee = employeeService.employeeLogin(employeeNumber,passWord);
        if (employee != null){
            if (("role5").equals(employee.getRole()) || ("role6").equals(employee.getRole()) || ("role7").equals(employee.getRole())){
                result.put("flag",0);
                result.put("msg","登录成功！");
                result.put("employee",employee);
            }else {
                result.put("flag",1);
                result.put("msg","无登录权限！");
            }
        }else{
            result.put("msg","登录失败，用户名或密码错误！");
            result.put("flag",2);
        }
        return result;
    }

    @RequestMapping(value = "/staffStart")
    public String staffStart(Model model){
        return "factoryMsg/staff";
    }


    @RequestMapping(value = "/getStaffList")
    public String getStaffList(Model model){
        List<Employee> employeeList = employeeService.getAllEmployee();
        model.addAttribute("employeeList",employeeList);
        return "factoryMsg/fb_staffList";
    }

    @RequestMapping(value = "/getAllStaff",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> getAllStall(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<Employee> employeeList = employeeService.getAllEmployee();
        map.put("data", employeeList);
        map.put("code", 0);
        map.put("count", employeeList.size());
        return map;
    }

    @RequestMapping(value = "/getAllInStateStaff",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> getAllInStateStall(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<Employee> employeeList = employeeService.getAllInStateEmployee();
        map.put("data", employeeList);
        map.put("code", 0);
        map.put("count", employeeList.size());
        return map;
    }

    @RequestMapping(value = "/getAllOffStateStaff",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> getAllOffStateStall(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<Employee> employeeList = employeeService.getAllOffStateEmployee();
        map.put("data", employeeList);
        map.put("code", 0);
        map.put("count", employeeList.size());
        return map;
    }

    @RequestMapping(value = "/uploadPic")
    @ResponseBody
    public Map<String,Object> uploadPic(@RequestParam(name = "file",required= false) MultipartFile file, HttpServletRequest request){
        Map<String,Object> result = new HashMap<String, Object>();
        result.put("code",0);
        try{
            /***
             * 下面贴key  4行
             */
            String endPoint = "http://oss-cn-beijing.aliyuncs.com";
            String accessKeyId = "LTAI4GJH7FZWj86YK3MDNv4Z";
            String accessKeySecret = "vznlyo2fKbrT86mWJMxetwf7IJcVsi";
            String bucketName = "hengdaerp";
            if(file != null) {
                String fileName = file.getOriginalFilename();
                String uuid = UUID.randomUUID().toString().replaceAll("-", "");
                String objectName = "dy/" + uuid + fileName;
                OSS ossClient = new OSSClientBuilder().build(endPoint, accessKeyId, accessKeySecret);
                InputStream input = file.getInputStream();
                ossClient.putObject(bucketName, objectName, input);
                input.close();
                ossClient.shutdown();
                File path = new File(ResourceUtils.getURL("classpath:").getPath());
                File downDir = new File("/tmp/dev/uploads/");
                if(!downDir.exists()){
                    downDir.mkdirs();
                }
                File img = new File(downDir.getPath()+"/"+uuid + fileName);
                OutputStream os = new FileOutputStream(img);
                InputStream inputStream = file.getInputStream();
                int bytesRead = 0;
                byte[] buffer = new byte[8192];
                while ((bytesRead = inputStream.read(buffer, 0, 8192)) != -1) {
                    os.write(buffer, 0, bytesRead);
                }
                os.close();
                inputStream.close();
                result.put("success",true);
                result.put("picUrl",objectName);
                result.put("msg","修改成功");
                result.put("imageUrl","/uploads/"+uuid + fileName);
            }else {
                result.put("success",false);
            }
        }catch (IOException e){
            e.printStackTrace();
            result.put("success",false);
        }finally {

        }
        return result;
    }

    /**
     * 文件流生成base64
     * @return
     */
    public static String ImageToBase64ByStream(InputStream in) {
        // 将图片文件转化为字节数组字符串，并对其进行Base64编码处理
        byte[] data = null;
        // 读取图片字节数组
        try {
            data = new byte[in.available()];
            in.read(data);
            in.close();
            BASE64Encoder encoder = new BASE64Encoder();
            return encoder.encode(data);// 返回Base64编码过的字节数组字符串
        } catch (IOException e) {
            e.printStackTrace();
        }
        // 对字节数组Base64编码
        return null;
    }


    @RequestMapping(value = "/addemployee",method = RequestMethod.POST)
    @ResponseBody
    public int addEmployee(Employee employee){
        String employeeNumber = employee.getEmployeeNumber();
        List<Employee> employeeList = employeeService.getEmpByEmpNumber(employeeNumber);
        if (employeeList != null && employeeList.size() > 0){
            return 2;
        }
        String uuid = UUID.randomUUID().toString();
        employee.setPassWord(employeeNumber);
        employee.setGuid(uuid);
        List<DepartmentGroup> departmentGroupList = departmentGroupService.getAllDepartmentGroup();
//        String deptId = "0";
//        for (DepartmentGroup departmentGroup : departmentGroupList){
//            if (departmentGroup.getDepartmentName().equals(employee.getDepartment())){
//                deptId = departmentGroup.getDepartmentId();
//            }
//        }
//        String ccCount = syncEmployeeToCheck(employee.getEmployeeName(), employee.getPassWord(), employee.getTelephone(), deptId);
//        employee.setCcCount(ccCount);
        return employeeService.addEmployee(employee);

    }

    @RequestMapping(value = "/deleteemployee",method = RequestMethod.POST)
    @ResponseBody
    public int deleteEmployee(@RequestParam("employeeID") Integer employeeID){
        Employee employee = employeeService.getEmpByID(employeeID);
        int res2 = employeeService.deleteEmployee(employeeID);
        if (res2 == 0){
            return 0;
        } else {
            return 1;
        }

//        Employee employee = employeeService.getEmpByID(employeeID);
//        String res1 =  deleteEmployeeToCheck(employee.getCcCount());
//        int res2 = employeeService.deleteEmployee(employeeID);
//        if (res1.equals("1") && res2 == 0){
//            return 0;
//        } else if (!res1.equals("1") && res2 == 0){
//            return 1;
//        } else if (res1.equals("1") && res2 == 1){
//            return 2;
//        } else {
//            return 3;
//        }
    }

    @RequestMapping(value = "/getempbyid",method = RequestMethod.GET)
    @ResponseBody
    public Employee getEmpByID(@RequestParam("employeeID")Integer employeeID){
        Employee employee = employeeService.getEmpByID(employeeID);
        if(!StringUtils.isEmpty(employee.getImgUrl())) {
            if(employee.getImgUrl().startsWith("data:image/png;base64,")) {
                employee.setTmpImgUrl(employee.getImgUrl());
            }else {
                try {
                    String endPoint = "http://oss-cn-beijing.aliyuncs.com";
                    String accessKeyId = "LTAI4GJH7FZWj86YK3MDNv4Z";
                    String accessKeySecret = "vznlyo2fKbrT86mWJMxetwf7IJcVsi";
                    String bucketName = "hengdaerp";
                    OSS ossClient = new OSSClientBuilder().build(endPoint, accessKeyId, accessKeySecret);
                    Date expiration = new Date(new Date().getTime() + 3600 * 1000);
                    URL url = ossClient.generatePresignedUrl(bucketName, employee.getImgUrl(), expiration);
                    employee.setTmpImgUrl(url.toString());
                    ossClient.shutdown();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return employee;
    }

    @RequestMapping(value = "/updateemployee",method = RequestMethod.POST)
    @ResponseBody
    public int updateEmployee(Employee employee){
        String employeeNumber = employee.getEmployeeNumber();
        List<Employee> employeeList1 = employeeService.getEmpByEmpNumber(employeeNumber);
        if (employeeList1 != null && employeeList1.size() > 1){
            return 2;
        }
        Employee employee1 = employeeService.getEmpByID(employee.getEmployeeID());
        int res2 = employeeService.updateEmployee(employee);
        if (res2 == 0){
            return 0;
        } else{
            return 5;
        }

//        String res1;
//        if (employee.getPositionState().equals("离职")){
//            res1 = deleteEmployeeToCheck(employee1.getCcCount());
//        } else {
//            List<DepartmentGroup> departmentGroupList = departmentGroupService.getAllDepartmentGroup();
//            String deptId = "0";
//            for (DepartmentGroup departmentGroup : departmentGroupList){
//                if (departmentGroup.getDepartmentName().equals(employee.getDepartment())){
//                    deptId = departmentGroup.getDepartmentId();
//                }
//            }
//            res1 = updateEmployeeToCheck(employee.getEmployeeName(), employee.getEmployeeNumber(), employee.getTelephone(), deptId, employee1.getCcCount());
//        }
//        if (res1.equals("1") && res2 == 0){
//            return 0;
//        } else if (!res1.equals("1") && res2 == 0){
//            return 4;
//        } else if (res1.equals("1") && res2 == 1){
//            return 5;
//        } else {
//            return 6;
//        }
    }

    @RequestMapping(value = "/getallemployee",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getAllEmployee(){
        Map<String, Object> map = new HashMap<>();
        List<Employee> employeeList = new ArrayList<>();
        employeeList = employeeService.getAllEmployee();
        map.put("data",employeeList);
        return map;
    }

    @RequestMapping(value = "/getempnamebyempnum",method = RequestMethod.GET)
    @ResponseBody
    public String getEmpNameByEmpNum(@RequestParam("employeeNumber")String employeeNumber){
        String employeeName = employeeService.getEmpNameByEmpNum(employeeNumber);
        return employeeName;
    }

    @RequestMapping(value = "/getgroupnamebyempnum",method = RequestMethod.GET)
    @ResponseBody
    public String getGroupNameByEmpNum(@RequestParam("employeeNumber")String employeeNumber){
        String groupName = employeeService.getGroupNameByEmpNum(employeeNumber);
        return groupName;
    }

    @RequestMapping(value = "/minigetemployeebygroup",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getEmployeeByGroupName(@RequestParam("groupName")String groupName){
        Map<String, Object> map = new HashMap<>();
        List<Employee> employeeList = new ArrayList<>();
        employeeList = employeeService.getEmployeeByGroup(groupName);
//        for(int i=0;i<5;i++) {
//            Employee employee = new Employee();
//            employee.setEmployeeName("员工"+i);
//            employee.setEmployeeNumber("2000"+i);
//            employeeList.add(employee);
//        }
        map.put("groupEmployeeList",employeeList);
        return map;
    }

    @RequestMapping(value = "/getemployeebygroupnames",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getEmployeeByGroupNames(@RequestParam(value = "groupList[]")List<String> groupList){
        Map<String, Object> map = new LinkedHashMap<>();
        List<Employee> employeeList = employeeService.getEmployeeByGroups(groupList);
        map.put("groupEmployeeList",employeeList);
        return map;
    }


    @RequestMapping(value = "/getemphint",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getEmpHint(@RequestParam("keywords")String keywords){
        Map<String,Object> map = new HashMap<>();
        List<String> empNumberList = employeeService.getEmpHint(keywords);
        map.put("content",empNumberList);
        map.put("code",0);
        map.put("type","success");
        return map;
    }

    @RequestMapping(value = "/miniupdatepassword",method = RequestMethod.POST)
    @ResponseBody
    public int miniUpdatePassWord(@RequestParam("employeeNumber")String employeeNumber,
                                  @RequestParam("passWord")String passWord){
        int res = employeeService.updatePassWord(employeeNumber, passWord);
        return res;
    }

    @RequestMapping(value = "/getempnamehint",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getEmpNameHint(@RequestParam("keywords")String keywords){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> empNameList = employeeService.getEmpNameHint(keywords);
        map.put("content",empNameList);
        map.put("type","success");
        map.put("code",0);
        return map;
    }

    @RequestMapping(value = "/getempnumbersbyname",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getEmpNumbersByName(@RequestParam("employeeName")String employeeName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> employeeNumberList = employeeService.getEmpNumbersByName(employeeName);
        map.put("employeeNumberList",employeeNumberList);
        return map;
    }

    @RequestMapping(value = "/identifyIdcard")
    @ResponseBody
    public Map<String,Object> identifyIdcard(String imgUrl) {
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("flag",false);
        String accessToken = AuthService.getAuth("QvkcnSbQG7vKHeWCgoyrV2DU","wT1UonQzrg0HdgDURPrikTvvW38ne49P");
        imgUrl = imgUrl.replace("data:image/png;base64,","");
        String result = IdcardUtils.idcard(imgUrl,accessToken);
        JSONObject jsonObject = new JSONObject(result);
        String image_status = jsonObject.getString("image_status");
        if("normal".equals(image_status)) {
            map.put("flag",true);
            map.put("IDCard",jsonObject.getJSONObject("words_result").getJSONObject("公民身份号码").getString("words"));
            map.put("employeeName",jsonObject.getJSONObject("words_result").getJSONObject("姓名").getString("words"));
            map.put("gender",jsonObject.getJSONObject("words_result").getJSONObject("性别").getString("words"));
            map.put("birthday",jsonObject.getJSONObject("words_result").getJSONObject("出生").getString("words"));
            map.put("birthPlace",jsonObject.getJSONObject("words_result").getJSONObject("住址").getString("words"));
            map.put("nation",jsonObject.getJSONObject("words_result").getJSONObject("民族").getString("words")+"族");
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            try {
                Date date = simpleDateFormat.parse(jsonObject.getJSONObject("words_result").getJSONObject("出生").getString("words"));
                String dateString = sdf.format(date);
                map.put("birthday",dateString);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            String photo = "data:image/png;base64,"+jsonObject.getString("photo");
            map.put("photo",photo);
        }else if("blurred".equals(image_status)) {
            map.put("msg","身份证模糊");
        }else if("non_idcard".equals(image_status)) {
            map.put("msg","上传的图片中不包含身份证");
        }else if("reversed_side".equals(image_status)) {
            map.put("msg","身份证正反面颠倒");
        }else if("other_type_card".equals(image_status)) {
            map.put("msg","其他类型证照");
        }else if("over_exposure".equals(image_status)) {
            map.put("msg","身份证关键字段反光或过曝");
        }else if("over_dark".equals(image_status)) {
            map.put("msg","身份证欠曝（亮度过低）");
        }else {
            map.put("msg","识别失败");
        }
        return map;
    }

    @RequestMapping(value = "/identifyBankCard")
    @ResponseBody
    public Map<String,Object> identifyBankCard(String imgUrl) {
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("flag",false);
        String accessToken = AuthService.getAuth("QvkcnSbQG7vKHeWCgoyrV2DU","wT1UonQzrg0HdgDURPrikTvvW38ne49P");
        imgUrl = imgUrl.replace("data:image/png;base64,","");
        String result = BankCardUtils.bankCard(imgUrl,accessToken);
        JSONObject jsonObject = new JSONObject(result);
        JSONObject identifyResult = jsonObject.getJSONObject("result");
        if(identifyResult != null) {
            map.put("flag",true);
            map.put("bank_card_number",identifyResult.getString("bank_card_number"));
            map.put("bank_name",identifyResult.getString("bank_name"));
        }else {
            map.put("msg","识别失败");
        }
        return map;
    }

    @RequestMapping(value = "/getemployeebyemployeenumber",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getEmployeeByEmployeeNumber(@RequestParam("employeeNumber")String employeeNumber){
        Map<String, Object> map = new LinkedHashMap<>();
        List<Employee> employeeList = employeeService.getEmpByEmpNumber(employeeNumber);
        map.put("employeeList",employeeList);
        return map;
    }

    @RequestMapping(value = "/getEmployeeByPositions",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getEmployeeByPositions(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> positionList = new ArrayList<>();
        positionList.add("包装");
        List<Employee> employeeList = employeeService.getEmployeeByPositions(positionList);
        map.put("employeeList",employeeList);
        return map;
    }

    @RequestMapping(value = "/getemployeecountbygroup",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getEmployeeCountByGroup(@RequestParam("groupName")String groupName){
        Map<String, Object> map = new LinkedHashMap<>();
        int employeeCount = employeeService.getEmployeeCountByGroup(groupName);
        map.put("employeeCount", employeeCount);
        return map;
    }

    @RequestMapping(value = "/minigetallgroupnamelist",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> miniGetAllGroupNameList(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> groupList = employeeService.getAllGroupName();
        map.put("groupList", groupList);
        return map;
    }

    private String syncEmployeeToCheck(String realName, String passWord, String mobile, String deptId) {
        CloseableHttpClient httpclient = HttpClients.createDefault();
        String resultString = "";
        String ccCount = "0";
        passWord = encryption(passWord);
        CloseableHttpResponse response = null;
        try {
            // 创建uri
            String url = "http://yun.kqapi.com/Api/Api/addEmployee";
            URIBuilder builder = new URIBuilder(url);
            Long currentTime = new Date().getTime();
            Map<String, String> param = new LinkedHashMap<>();
            param.put("account", "b8fe2958b0aec8eb5c70b039832d989a");
            param.put("requesttime", currentTime.toString());
            param.put("realname", realName);
            param.put("password", passWord);
            param.put("mobile", mobile);
            param.put("deptid", deptId);
            param.put("sn", "K50194710151");
            String planText = param.get("account") + param.get("deptid") + param.get("mobile") + param.get("password") + param.get("realname") + param.get("requesttime") + param.get("sn") + "andy171fr";
            String md5code = encryption(planText);
            param.put("sign", md5code);
            if (param != null) {
                for (String key : param.keySet()) {
                    builder.addParameter(key, param.get(key));
                }
            }
            URI uri = builder.build();
            // 创建http GET请求
            HttpGet httpGet = new HttpGet(uri);
            // 执行请求
            response = httpclient.execute(httpGet);
            // 判断返回状态是否为200
            if (response.getStatusLine().getStatusCode() == 200) {
                resultString = EntityUtils.toString(response.getEntity(), "UTF-8");
            }
            System.out.println(resultString);
            JsonParser jsonParser = new JsonParser();
            JsonObject json = (JsonObject) jsonParser.parse(resultString);
            String status = json.get("status").getAsString();
            if (status.equals("1")){
                JsonObject dataJson = json.get("data").getAsJsonObject();
                ccCount = dataJson.get("account").getAsString();
            }
            return ccCount;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (response != null) {
                    response.close();
                }
                httpclient.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return ccCount;
    }

    private String updateEmployeeToCheck(String realName, String passWord, String mobile, String deptId, String useraccount) {
        CloseableHttpClient httpclient = HttpClients.createDefault();
        String resultString = "";
        String status = "2";
        passWord = encryption(passWord);
        CloseableHttpResponse response = null;
        try {
            // 创建uri
            String url = "http://yun.kqapi.com/Api/Api/updateEmployee";
            URIBuilder builder = new URIBuilder(url);
            Long currentTime = new Date().getTime();
            Map<String, String> param = new LinkedHashMap<>();
            param.put("account", "b8fe2958b0aec8eb5c70b039832d989a");
            param.put("requesttime", currentTime.toString());
            param.put("realname", realName);
            param.put("password", passWord);
            param.put("mobile", mobile);
            param.put("deptid", deptId);
            param.put("sn", "K50194710151");
            param.put("useraccount", useraccount);
            String planText = param.get("account") + param.get("deptid") + param.get("mobile") + param.get("password") + param.get("realname") + param.get("requesttime") + param.get("sn") + param.get("useraccount") + "andy171fr";
            String md5code = encryption(planText);
            param.put("sign", md5code);
            if (param != null) {
                for (String key : param.keySet()) {
                    builder.addParameter(key, param.get(key));
                }
            }
            URI uri = builder.build();
            // 创建http GET请求
            HttpGet httpGet = new HttpGet(uri);
            // 执行请求
            response = httpclient.execute(httpGet);
            // 判断返回状态是否为200
            if (response.getStatusLine().getStatusCode() == 200) {
                resultString = EntityUtils.toString(response.getEntity(), "UTF-8");
            }
            System.out.println(resultString);
            JsonParser jsonParser = new JsonParser();
            JsonObject json = (JsonObject) jsonParser.parse(resultString);
            return json.get("status").getAsString();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (response != null) {
                    response.close();
                }
                httpclient.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return status;
    }


//    @Scheduled(cron = "0 57 22 * * ? ")
    private String deleteEmployeeToCheck() {
        CloseableHttpClient httpclient = HttpClients.createDefault();
        String resultString = "";
        CloseableHttpResponse response = null;
        try {
            // 创建uri
            String url = "http://yun.kqapi.com/Api/Api/layoffEmployee";
            URIBuilder builder = new URIBuilder(url);
            Long currentTime = new Date().getTime();
            Map<String, String> param = new LinkedHashMap<>();
            param.put("account", "b8fe2958b0aec8eb5c70b039832d989a");
            param.put("requesttime", currentTime.toString());
            param.put("useraccount", "4386032");
            String planText = param.get("account") + param.get("requesttime") + param.get("useraccount") + "andy171fr";
            String md5code = encryption(planText);
            param.put("sign", md5code);
            if (param != null) {
                for (String key : param.keySet()) {
                    builder.addParameter(key, param.get(key));
                }
            }
            URI uri = builder.build();
            // 创建http GET请求
            HttpGet httpGet = new HttpGet(uri);
            // 执行请求
            response = httpclient.execute(httpGet);
            // 判断返回状态是否为200
            if (response.getStatusLine().getStatusCode() == 200) {
                resultString = EntityUtils.toString(response.getEntity(), "UTF-8");
            }
            System.out.println(resultString);
            JsonParser jsonParser = new JsonParser();
            JsonObject json = (JsonObject) jsonParser.parse(resultString);
            return json.get("status").getAsString();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (response != null) {
                    response.close();
                }
                httpclient.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return "2";
    }


    public String encryption(String plainText) {
        String hashtext = new String();
        try {
            MessageDigest m = MessageDigest.getInstance("MD5");
            m.reset();
            m.update(plainText.getBytes());
            byte[] digest = m.digest();
            BigInteger bigInt = new BigInteger(1,digest);
            hashtext = bigInt.toString(16);
            while(hashtext.length() < 32 ){
                hashtext = "0" + hashtext;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return hashtext;
    }


    private static String unicodeToCn(String unicode) {
        /** 以 \ u 分割，因为java注释也能识别unicode，因此中间加了一个空格*/
        String[] strs = unicode.split("\\\\u");
        String returnStr = "";
        // 由于unicode字符串以 \ u 开头，因此分割出的第一个字符是""。
        for (int i = 1; i < strs.length; i++) {
            returnStr += (char) Integer.valueOf(strs[i], 16).intValue();
        }
        return returnStr;
    }

    @RequestMapping(value = "/minigetempinfohint", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> miniGetEmpInfoHint(@RequestParam("keyWord") String keyWord){
        Map<String, Object> map = new LinkedHashMap<>();
        Integer count = employeeService.getEmployeeCountOfHint(keyWord);
        List<Employee> employeeList = employeeService.getEmployeeInfoHit(keyWord,null, null);
        map.put("data",employeeList);
        map.put("msg","");
        map.put("count",count==null?0:count);
        map.put("code",0);
        return map;
    }

}