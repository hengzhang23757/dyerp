//package com.example.erp01.action;
//
//import com.example.erp01.model.BasicFabric;
//import com.example.erp01.model.FabricPlan;
//import com.example.erp01.service.BasicFabricService;
//import com.example.erp01.service.FabricPlanService;
//import com.google.gson.Gson;
//import com.google.gson.GsonBuilder;
//import com.google.gson.reflect.TypeToken;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.ResponseBody;
//
//import java.text.SimpleDateFormat;
//import java.util.*;
//
//@Controller
//@RequestMapping(value = "erp")
//public class FabricPlanController {
//
//    @Autowired
//    private FabricPlanService fabricPlanService;
//
//    @Autowired
//    private BasicFabricService basicFabricService;
//
//    @RequestMapping("/fabricPlanStart")
//    public String fabricPlanStart(){
//        return "basicInfo/fabricPlan";
//    }
//
//    @RequestMapping("/fabricPlanAddStart")
//    public String fabricPlanAddStart(Model model, String type){
//        model.addAttribute("type", type);
//        return "basicInfo/fb_fabricPlanAdd";
//    }
//
//    @RequestMapping("/fabricPlanDetailStart")
//    public String fabricPlanDetailStart(Model model, String orderName, Integer orderCount, String season, String orderDate){
//        model.addAttribute("orderName", orderName);
//        model.addAttribute("orderCount", orderCount);
//        model.addAttribute("season", season);
//        model.addAttribute("orderDate", orderDate);
//        return "basicInfo/fb_fabricPlanDetail";
//    }
//
//    @RequestMapping(value = "/getuniquefabricplan", method = RequestMethod.GET)
//    @ResponseBody
//    public Map<String, Object> getUniqueFabricPlan(){
//        Map<String, Object> map = new LinkedHashMap<>();
//        List<FabricPlan> fabricPlanList = fabricPlanService.getUniqueFabricPlan();
//        map.put("fabricPlanList",fabricPlanList);
//        return map;
//    }
//
//    @RequestMapping(value = "/addfabricplanbatch", method = RequestMethod.POST)
//    @ResponseBody
//    public int addFabricPlanBatch(@RequestParam("fabricPlanJson")String fabricPlanJson){
//        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
//        List<FabricPlan> fabricPlanList = gson.fromJson(fabricPlanJson,new TypeToken<List<FabricPlan>>(){}.getType());
//        List<BasicFabric> basicFabricList = new ArrayList<>();
//        for (FabricPlan fabricPlan : fabricPlanList){
//            String basicFabricName = fabricPlan.getFabricName();
//            String basicFabricColor = fabricPlan.getFabricColor();
//            String basicFabricWidth = fabricPlan.getFabricWidth();
//            String basicFabricWeight = fabricPlan.getFabricWeight();
//            String basicFabricSupplier = fabricPlan.getSupplier();
//            BasicFabric basicFabric = new BasicFabric(basicFabricName, basicFabricColor, basicFabricWidth, basicFabricWeight, basicFabricSupplier);
//            basicFabricList.add(basicFabric);
//        }
//        int res1 = basicFabricService.addBasicFabricBatch(basicFabricList);
//        int res2 = fabricPlanService.addFabricPlanBatch(fabricPlanList);
//        if (res1 == 0 && res2 == 0){
//            return 0;
//        }else {
//            return 1;
//        }
//    }
//
//    @RequestMapping(value = "/getfabricplandetailbyorderseasondate", method = RequestMethod.GET)
//    @ResponseBody
//    public Map<String, Object> getFabricPlanDetailByOrder(@RequestParam("orderName")String orderName,
//                                                          @RequestParam("orderCount")Integer orderCount,
//                                                          @RequestParam("season")String season,
//                                                          @RequestParam("orderDate")String orderDate){
//        Map<String, Object> map = new LinkedHashMap<>();
//        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
//        try{
//            Date od = formatter.parse(orderDate);
//            List<FabricPlan> fabricPlanList = fabricPlanService.getFabricPlanDetailByOrderSeasonDate(orderName, orderCount, season, od);
//            map.put("fabricPlanList", fabricPlanList);
//            return map;
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//        return map;
//    }
//
//    @RequestMapping(value = "/deletefabricplanbyorderseasondate", method = RequestMethod.POST)
//    @ResponseBody
//    public int deleteFabricPlanByOrderSeasonDate(@RequestParam("orderName")String orderName,
//                                                 @RequestParam("orderCount")Integer orderCount,
//                                                 @RequestParam("season")String season,
//                                                 @RequestParam("orderDate")String orderDate){
//        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
//        try{
//            Date od = formatter.parse(orderDate);
//            return fabricPlanService.deleteFabricPlanByOrderSeasonDate(orderName, orderCount, season, od);
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//        return 1;
//    }
//
//    @RequestMapping(value = "/deletefabricplanbyid", method = RequestMethod.POST)
//    @ResponseBody
//    public int deleteFabricPlanByID(@RequestParam("fabricPlanID")Integer fabricPlanID){
//        return fabricPlanService.deleteFabricPlan(fabricPlanID);
//    }
//
//
//    @RequestMapping(value = "/getfabricnamesbyorder", method = RequestMethod.GET)
//    @ResponseBody
//    public Map<String, Object> getFabricNamesByOrder(@RequestParam("orderName")String orderName){
//        Map<String, Object> map = new LinkedHashMap<>();
//        List<String> fabricNameList = fabricPlanService.getFabricNamesByOrder(orderName);
//        map.put("fabricNameList", fabricNameList);
//        return map;
//    }
//
//    @RequestMapping(value = "/getfabriccolorsbyorderfabric", method = RequestMethod.GET)
//    @ResponseBody
//    public Map<String, Object> getFabricColorsByOrderFabric(@RequestParam("orderName")String orderName,
//                                                            @RequestParam("fabricName")String fabricName){
//        Map<String, Object> map = new LinkedHashMap<>();
//        List<String> fabricColorList = fabricPlanService.getFabricColorsByOrderFabric(orderName, fabricName);
//        map.put("fabricColorList", fabricColorList);
//        return map;
//    }
//
//    @RequestMapping(value = "/getfabricordercountbyorderfabriccolor", method = RequestMethod.GET)
//    @ResponseBody
//    public int getFabricOrderCountByOrderFabricColor(@RequestParam("orderName")String orderName,
//                                                     @RequestParam("fabricName")String fabricName,
//                                                     @RequestParam("fabricColor")String fabricColor){
//        return fabricPlanService.getFabricOrderCountByOrderFabricColor(orderName, fabricName, fabricColor);
//    }
//
//}
