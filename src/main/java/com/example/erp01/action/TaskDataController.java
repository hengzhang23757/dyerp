package com.example.erp01.action;

import com.example.erp01.model.*;
import com.example.erp01.service.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import javafx.concurrent.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

@Controller
@RequestMapping(value = "erp")
@Configuration      //1.主要用于标记配置类，兼备Component的效果。
@EnableScheduling   // 2.开启定时任务
public class TaskDataController {

    @Autowired
    private TaskDataService taskDataService;
    @Autowired
    private DailyActionService dailyActionService;
    @Autowired
    private OrderClothesService orderClothesService;
    @Autowired
    private OrderProcedureService orderProcedureService;
    @Autowired
    private EmployeeService employeeService;
    @Autowired
    private EmbStorageService embStorageService;
    @Autowired
    private ManufactureAccessoryService manufactureAccessoryService;
    @Autowired
    private AccessoryStorageService accessoryStorageService;
    @Autowired
    private AccessoryInStoreService accessoryInStoreService;


    @RequestMapping("/sewingPlanStart")
    public String sewingPlanStart(){
        return "plan/sewingPlan";
    }

    @RequestMapping(value = "/getcommittaskdata", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getCommitTaskData(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> taskTypeList = new ArrayList<>();
        List<String> groupNameList = new ArrayList<>();
        taskTypeList.add("2");
        groupNameList.add("车缝A组");
        groupNameList.add("车缝B组");
        groupNameList.add("车缝C组");
        groupNameList.add("车缝D组");
        groupNameList.add("车缝E组");
        groupNameList.add("车缝F组");
        groupNameList.add("车缝G组");
        groupNameList.add("车缝H组");
        List<TaskData> taskDataList = taskDataService.getTaskDataByTypeGroup(null, null, taskTypeList, groupNameList);
        map.put("taskDataList", taskDataList);
        return map;
    }
    //根据 组名  款号 获取hover的数据信息
    @RequestMapping(value = "/gethintinfobyorderparent", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getHintInfoByOrderParent(@RequestParam("id")Integer id){
        Map<String, Object> map = new LinkedHashMap<>();
        TaskData taskData = taskDataService.getTaskDataById(id);
        map.put("taskData", taskData);
        String orderName = taskData.getOrderName();
        String colorName = taskData.getColorName();
        String sizeName = taskData.getSizeName();
        String groupName = taskData.getGroupName();
        List<String> colorList = new ArrayList<>();
        List<String> sizeList = new ArrayList<>();
        if ("".equals(colorName) || colorName.contains("全部")){
            colorList = null;
        } else {
            colorList = new ArrayList<>(Arrays.asList(colorName.split(",")));
            colorList.add("通用");
        }
        if ("".equals(sizeName) || sizeName.contains("全部")){
            sizeList = null;
        } else {
            sizeList = new ArrayList<>(Arrays.asList(sizeName.split(",")));
            sizeList.add("通用");
        }
        List<EmbStorage> embStorageList = embStorageService.getEmbStorageByOrderColorSizeList(orderName, colorList, sizeList);
        map.put("embStorageList", embStorageList);
        List<ManufactureAccessory> manufactureAccessoryList = manufactureAccessoryService.getManufactureAccessoryByColorSizeList(orderName, colorList, sizeList);
        List<AccessoryStorage> accessoryStorageList = accessoryStorageService.getAccessoryStorageByColorSizeList(orderName, colorList, sizeList);
        List<AccessoryInStore> accessoryInStoreList = accessoryInStoreService.getAccessoryInStoreByColorSizeList(orderName, colorList, sizeList);
        for (ManufactureAccessory manufactureAccessory : manufactureAccessoryList){
            for (AccessoryInStore accessoryInStore : accessoryInStoreList){
                if (manufactureAccessory.getAccessoryName().equals(accessoryInStore.getAccessoryName()) && manufactureAccessory.getSpecification().equals(accessoryInStore.getSpecification()) && manufactureAccessory.getColorName().equals(accessoryInStore.getColorName()) && manufactureAccessory.getSizeName().equals(accessoryInStore.getSizeName())){
                    manufactureAccessory.setAccessoryReturnCount(manufactureAccessory.getAccessoryReturnCount() + accessoryInStore.getInStoreCount());
                }
            }
            for (AccessoryStorage accessoryStorage : accessoryStorageList){
                if (manufactureAccessory.getAccessoryName().equals(accessoryStorage.getAccessoryName()) && manufactureAccessory.getSpecification().equals(accessoryStorage.getSpecification()) && manufactureAccessory.getColorName().equals(accessoryStorage.getColorName()) && manufactureAccessory.getSizeName().equals(accessoryStorage.getSizeName())){
                    manufactureAccessory.setAccessoryStorageCount(manufactureAccessory.getAccessoryStorageCount() + accessoryStorage.getStorageCount());
                }
            }
        }
        map.put("manufactureAccessoryList", manufactureAccessoryList);
        List<GeneralSalary> generalSalaryList = taskDataService.getGroupDailyPieceWorkByInfo(groupName.substring(2), orderName, null);
        map.put("generalSalaryList", generalSalaryList);
        return map;
    }

    //根据 组名  款号 获取hover的数据信息
    @RequestMapping(value = "/getalltypetaskdatabyid", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getAllTypeTaskDataById(@RequestParam("id")Integer id){
        Map<String, Object> map = new LinkedHashMap<>();
        TaskData taskData = taskDataService.getTaskDataById(id);
        if (taskData == null){
            return map;
        }
        TaskData taskData1 = taskDataService.getCurrentTaskDataByGroupType("3", taskData.getGroupName());
        map.put("taskData", taskData);
        if (taskData1 != null){
            map.put("currentTask", taskData1);
        }
        return map;
    }

    @RequestMapping(value = "/getunfinishtaskdatabygrouptype", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getUnFinishTaskDataByGroupType(@RequestParam("groupName")String groupName,
                                                              @RequestParam("taskType")String taskType){
        Map<String, Object> map = new LinkedHashMap<>();
        List<TaskData> taskDataList = taskDataService.getUnFinishTaskDataByTaskTypeGroupName(taskType, groupName);
        map.put("taskDataList", taskDataList);
        return map;
    }


    @RequestMapping(value = "/taskdataunifiedprocess", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> taskDataUnifiedProcess(@RequestParam("groupName")String groupName,
                                                      @RequestParam("taskDataJson")String taskDataJson){
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        List<TaskData> taskDataList = gson.fromJson(taskDataJson,new TypeToken<List<TaskData>>(){}.getType());
        int res1 = taskDataService.clearTaskDataByGroupType("3", groupName);
        for (TaskData taskData : taskDataList){
            taskData.setTaskType("3");
        }
        int res2 = taskDataService.addTaskDataBatch(taskDataList);
        List<TaskData> taskDataList1 = taskDataService.getUnFinishTaskDataByTaskTypeGroupName("3", groupName);
        List<TaskLinks> taskLinksList = new ArrayList<>();
        for (int i = 0; i < taskDataList1.size() - 1; i++){
            TaskLinks taskLinks = new TaskLinks();
            taskLinks.setSource(taskDataList1.get(i).getId());
            taskLinks.setTarget(taskDataList1.get(i + 1).getId());
            taskLinks.setType(0);
            taskLinksList.add(taskLinks);
        }
        int res3 = taskDataService.addTaskLinksBatch(taskLinksList);
        if (res1 == 0 && res2 == 0 && res3 == 0){
            map.put("result", 0);
            return map;
        } else {
            map.put("result", 1);
            return map;
        }
    }

    @RequestMapping(value = "/getalltaskdata", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getAllTaskData(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<TaskData> taskDataList = taskDataService.getAllTaskData();
        List<TaskLinks> linkList = taskDataService.getAllTaskLinks();
        map.put("mainTaskList", taskDataList);
        map.put("linkList", linkList);
        return map;
    }

    @RequestMapping(value = "/getactidlistgroupbydeliverystate", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getActIdListGroupByDeliveryState(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<Integer> normalList = taskDataService.getActIdByDeliveryState("正常");
        List<Integer> unNormalList = taskDataService.getActIdByDeliveryState("逾期");
        map.put("normalList", normalList);
        map.put("unNormalList", unNormalList);
        return map;
    }


    public static int getHour(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.get(Calendar.HOUR_OF_DAY);
    }

    public int compareDays(Date date1, Date date2) {
        Calendar calendar1 = Calendar.getInstance();
        Calendar calendar2 = Calendar.getInstance();
        calendar1.setTime(date1);
        calendar2.setTime(date2);
        int day1 = calendar1.get(Calendar.DAY_OF_YEAR);
        int day2 = calendar2.get(Calendar.DAY_OF_YEAR);
        int year1 = calendar1.get(Calendar.YEAR);
        int year2 = calendar2.get(Calendar.YEAR);
        if(year1 > year2) {
            int tempYear = year1;
            int tempDay = day1;
            day1 = day2;
            day2 = tempDay;
            year1 = year2;
            year2 = tempYear;
        }
        if (year1 == year2) {
            return  (day2 - day1);
        } else {
            int DayCount = 0;
            for (int i = year1; i < year2; i++) {
                if (i % 4 == 0 && i % 100 != 0 || i % 400 == 0) {
                    DayCount += 366;
                }else {
                    DayCount += 365;
                }
            }
            return DayCount+(day2-day1);
        }
    }

    public static Date getBeforeDate(Date date, int day) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH,-day);
        return calendar.getTime();
    }

    public static Date getAfterDate(Date date, int day) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, day);
        return calendar.getTime();
    }

    public static int getDuration(int diffDay, int totalCount, int finishCount, int yesterdayCount) {
        if (diffDay == 1){
            if ((totalCount - finishCount - 2 * yesterdayCount) <= 0){
                return 2;
            } else if ((totalCount - finishCount - 5 * yesterdayCount) <= 0){
                return 3;
            } else {
                return ((totalCount - finishCount - 5 * yesterdayCount)/(yesterdayCount*3) + 3);
            }
        } else if (diffDay == 2){
            if ((totalCount - finishCount - yesterdayCount*3/2) <= 0){
                return 3;
            } else {
                return ((totalCount - finishCount - yesterdayCount*3/2)/(yesterdayCount*3/2) + 3);
            }
        } else if (diffDay >= 3) {
            return ((totalCount - finishCount - yesterdayCount)/(yesterdayCount) + diffDay);
        }
        return diffDay;
    }


    public static int getCurrentHour(){
        Calendar calendar = Calendar.getInstance();
        return calendar.get(calendar.HOUR_OF_DAY);
    }
    // 早八点计划更新
    // 获取今日和昨日数据   (昨日/前日数据一定要以时间排序)
    //     如果今日数据为空  不动
    //     如果昨日数据为空  获取前日的
    // 如果今日数据与昨日/前日不一致
    //     adjust计划  类型是today
    //     创建新的实际进度
    // 如果今日数据与昨日/前日一致
    //     更新今天是该款进行的第几天


    // 1. 一个任务最少是一天
    // 2. 任务可以叠加一天
    // 3.
//    @Scheduled(cron = "0 0/30 * * * *")
    public void taskDataHalfHour(){
        List<TaskData> taskDataList = taskDataService.getAllTaskDataByType("1");
        System.out.println("获取组别信息正常");
        for (TaskData taskData : taskDataList){
            System.out.println("组名=====" + taskData.getGroupName());
            int currHour = getCurrentHour();
            if (currHour == 0){
                System.out.println("新的一天");
                List<PieceWork> pieceWorkList = taskDataService.getSomeDayFinishCount(taskData.getGroupName().substring(2), getBeforeDate(new Date(), 1));
                if (pieceWorkList != null){
                    PieceWork pieceWork = pieceWorkList.get(0);
                    TaskData actTaskData = taskDataService.getTaskDataByBetweenDateType("4", taskData.getGroupName(), pieceWork.getOrderName(), getBeforeDate(new Date(), 1));
                    // dayorder = 0 不计算  >= 1 计算
                    if (actTaskData != null){
                        int taskCount = actTaskData.getTaskCount();
                        int dailyCount = pieceWork.getLayerCount();
                        int finishCount = taskDataService.getFinishCountByTaskBegin(taskData.getGroupName().substring(2), actTaskData.getOrderName(), actTaskData.getStartDate());
                        actTaskData.setFinishCount(finishCount);
                        actTaskData.setDailyCount(pieceWork.getLayerCount());
                        actTaskData.setDayOrder(actTaskData.getDayOrder() + 1);
                        actTaskData.setProgress((float)finishCount/taskCount);
                        int dOrder = actTaskData.getDayOrder();
                        int duration;
                        if (dOrder == 1){
                            duration = Math.round((float)taskCount/(dailyCount*3));
                        } else if (dOrder == 2){
                            duration = Math.round(taskCount/(dailyCount*1.5f));
                        } else {
                            duration = Math.round(taskCount/(dailyCount*1f));
                        }
                        duration = duration > 0 ? duration : 1;
                        actTaskData.setDuration(duration);
                        actTaskData.setEndDate(getAfterDate(actTaskData.getStartDate(), duration));
                        int res = taskDataService.updateTaskData(actTaskData);
                        if (res == 0){
                            System.out.println("新的一天更新成功");
                        } else {
                            System.out.println("新的一天更新失败");
                        }
                    }
                } else {
                    // 昨天为空  可以把当前任务往后延一天
                    TaskData actTaskData = taskDataService.getTaskDataByBetweenDateType("4", taskData.getGroupName(), null, getBeforeDate(new Date(), 1));
                    if (actTaskData != null){
                        actTaskData.setDuration(actTaskData.getDuration() + 1);
                        actTaskData.setEndDate(getAfterDate(actTaskData.getEndDate(), 1));
                        taskDataService.updateTaskData(actTaskData);
                    }
                    System.out.println("昨天休息, 后延一天");
                }
            } else {
                // 如果正常时间段 首先根据当前时间段的最新款号获取到当前的实际任务
                List<PieceWork> pieceWorkList = taskDataService.getSomeDayFinishCount(taskData.getGroupName().substring(2), new Date());
                if (pieceWorkList != null && !pieceWorkList.isEmpty()){
                    PieceWork pieceWork = pieceWorkList.get(0);
                    TaskData actTaskData = taskDataService.getTaskDataByBetweenDateType("4", taskData.getGroupName(), pieceWork.getOrderName(), new Date());
                    if (actTaskData != null){
                        // 更新
                        int taskCount = actTaskData.getTaskCount();
                        int finishCount = taskDataService.getFinishCountByTaskBegin(taskData.getGroupName().substring(2), actTaskData.getOrderName(), actTaskData.getStartDate());
                        actTaskData.setFinishCount(finishCount);
                        actTaskData.setProgress((float)finishCount/taskCount);
                        taskDataService.updateTaskData(actTaskData);
                        System.out.println("正常更新");
                    } else {
                        // 漏掉了一个  就是当前实际数据刚刚出现  当前或者后续已经存在计划  则需要把计划调整到位  调整到初始日期是今天然后数量和结束日期重新计算 并创建
                        // 该漏洞会导致新创建的计划  但是实际数据没有  就会直接截断创建新的
                        // 创建  1.获取计划数量
                        String orderName = pieceWork.getOrderName();
                        String clothesVersionNumber = orderClothesService.getVersionNumberByOrderName(pieceWork.getOrderName());
                        // 获取当前计划 把当前计划的结束日期调整为昨天
                        TaskData currentTaskData = taskDataService.getCurrentTaskDataByGroupType("3", taskData.getGroupName());
                        if (currentTaskData != null){
                            if (!currentTaskData.getOrderName().equals(orderName)){
                                currentTaskData.setEndDate(new Date());
                                currentTaskData.setDuration(compareDays(currentTaskData.getStartDate(), currentTaskData.getEndDate()));
                                int res = taskDataService.updateTaskData(currentTaskData);
                            }
                        }
                        // 获取上一刻的实际 并把实际的结束日期设置为昨天
                        TaskData currentActTask = taskDataService.getCurrentTaskDataByGroupType("4", taskData.getGroupName());
                        if (currentActTask != null){
                            currentActTask.setEndDate(new Date());
                            currentActTask.setDuration(compareDays(currentActTask.getStartDate(), currentActTask.getEndDate()));
                            int res = taskDataService.updateTaskData(currentActTask);
                        }
                        int res1 = adjustPlanByGroupOrderName(taskData.getGroupName(), orderName, clothesVersionNumber, new Date());
                        int res2 = createNewActTaskData(taskData.getGroupName(), orderName, clothesVersionNumber, pieceWork.getLayerCount());
                        System.out.println("创建并调整");
                    }
                }

            }

        }
    }



    // 比较当天所做的款号有几个
    public Map<String, Object> compareCurrentOrder(String groupName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<PieceWork> pieceWorkList = taskDataService.getSomeDayFinishCount(groupName.substring(2), new Date());
        if (pieceWorkList != null && !pieceWorkList.isEmpty()){
            if (pieceWorkList.size() == 1){
                map.put("flag", true);
            }
            if (pieceWorkList.size() == 2){
                map.put("flag", false);
            }
        }
        return map;
    }

    // 比较最近两天款号是否相等
    public Map<String, Object> compareRecentOrder(String groupName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<PieceWork> pieceWorkList1 = taskDataService.getSomeDayFinishCount(groupName.substring(2), new Date());
        if (pieceWorkList1 != null && !pieceWorkList1.isEmpty()){
            String orderName1 = pieceWorkList1.get(0).getOrderName();
//            if (pieceWorkList1.size() > 1){
//                orderName1 = pieceWorkList1.get(pieceWorkList1.size() - 1).getOrderName();
//                clothesVersionNumber1 = pieceWorkList1.get(pieceWorkList1.size() - 1).getClothesVersionNumber();
//            }
            List<PieceWork> pieceWorkList2 = taskDataService.getSomeDayFinishCount(groupName.substring(2), getBeforeDate(new Date(), 1));
            if (pieceWorkList2 != null && !pieceWorkList2.isEmpty()){
                String orderName2 = pieceWorkList2.get(0).getOrderName();
//                if (pieceWorkList1.size() > 1){
//                    orderName2 = pieceWorkList1.get(pieceWorkList1.size() - 1).getOrderName();
//                }
                if (orderName1.equals(orderName2)){
                    map.put("flag", true);
                    map.put("orderName", orderName1);
                } else {
                    map.put("flag", false);
                    map.put("orderName", orderName1);
                }
            } else {
                map.put("flag", false);
                map.put("orderName", orderName1);
            }
        }
        return map;
    }

    // 比较今天款号是否相等
    public Map<String, Object> compareTodayOrder(String groupName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<PieceWork> pieceWorkList1 = taskDataService.getSomeDayFinishCount(groupName.substring(2), new Date());
        if (pieceWorkList1 != null && !pieceWorkList1.isEmpty()){
            String orderName1 = pieceWorkList1.get(0).getOrderName();
            Integer pieceCount = pieceWorkList1.get(0).getLayerCount();
            boolean flag = true;
            if (pieceWorkList1.size() > 1){
                flag = false;
                orderName1 = pieceWorkList1.get(pieceWorkList1.size() - 1).getOrderName();
            }
            map.put("flag", flag);
            map.put("orderName", orderName1);
            map.put("pieceCount", pieceCount);
        }
        return map;
    }

    // 创建新的计划，并调整该组后续计划
    public int adjustPlanByGroupOrderName(String groupName, String orderName, String clothesVersionNumber, Date startDate){
        // 首先获取到该组后续的计划，看是否含有这个款
        TaskData parentTaskData = taskDataService.getParentTaskDataByGroupName(groupName, "1");
        List<TaskData> taskDataList1 = taskDataService.getUnFinishTaskDataByTaskTypeGroupName("3", groupName);
        List<TaskData> taskDataList2 = new ArrayList<>();
        Date globalStart = new Date();
        TaskData tmpTask = new TaskData();
        if (taskDataList1 != null && !taskDataList1.isEmpty()){
            // 看后续的计划中有没有当前做的款  如果有就把该款计划挑出来  把剩余的计划重新调整
            boolean flag = true;
            for (TaskData taskData1 : taskDataList1){
                if (taskData1.getOrderName().equals(orderName)){
                    flag = false;
                    tmpTask = taskData1;
                } else {
                    taskDataList2.add(taskData1);
                }
            }
            // 如果没有就创建新的计划
            if (flag){
                int orderCount =  orderClothesService.getOrderTotalCount(orderName);
                Float sam = orderProcedureService.getSectionSamByOrder(orderName, "车缝");
                int employeeCount = employeeService.getEmployeeCountByGroup(groupName);
                tmpTask.setTaskType("3");
                tmpTask.setText(orderName);
                tmpTask.setGroupName(groupName);
                tmpTask.setOrderName(orderName);
                tmpTask.setTaskCount(orderCount);
                tmpTask.setClothesVersionNumber(clothesVersionNumber);
                tmpTask.setParent(parentTaskData.getId());
                tmpTask.setColorName("全部");
                tmpTask.setSizeName("全部");
                tmpTask.setProgress(1f);
                tmpTask.setTaskOpen("true");
                tmpTask.setBackType("green");
                tmpTask.setSam(sam);
                tmpTask.setRemark("自动生成");
                tmpTask.setDeliveryState("正常");
                tmpTask.setStartDate(startDate);
                globalStart = startDate;
                int duration = Math.round((orderCount * sam) / (employeeCount * 11 * 60f));
                duration = duration > 1 ? duration : 1;
                tmpTask.setDuration(duration);
            }
            // 把当前计划放到第一位
            taskDataList2.add(0, tmpTask);
            for (TaskData  taskData2: taskDataList2){
                taskData2.setStartDate(globalStart);
                taskData2.setEndDate(getAfterDate(globalStart, taskData2.getDuration()));
                globalStart = getAfterDate(globalStart, taskData2.getDuration());
            }
            int res = dealWithAllTask(taskDataList2, groupName);
            if (res == 0){
                return 0;
            } else {
                return 1;
            }
        } else {
            int orderCount =  orderClothesService.getOrderTotalCount(orderName);
            Float sam = orderProcedureService.getSectionSamByOrder(orderName, "车缝");
            int employeeCount = employeeService.getEmployeeCountByGroup(groupName);
            tmpTask.setTaskType("3");
            tmpTask.setText(orderName);
            tmpTask.setOrderName(orderName);
            tmpTask.setGroupName(groupName);
            tmpTask.setTaskCount(orderCount);
            tmpTask.setClothesVersionNumber(clothesVersionNumber);
            tmpTask.setParent(parentTaskData.getId());
            tmpTask.setColorName("全部");
            tmpTask.setSizeName("全部");
            tmpTask.setProgress(1f);
            tmpTask.setTaskOpen("true");
            tmpTask.setBackType("green");
            tmpTask.setSam(sam);
            tmpTask.setRemark("自动生成");
            tmpTask.setDeliveryState("正常");
            tmpTask.setStartDate(startDate);
            globalStart = startDate;

            int duration = Math.round((orderCount * sam) / (employeeCount * 11 * 60f));
            duration = duration > 1 ? duration : 1;
            tmpTask.setDuration(duration);

            tmpTask.setEndDate(getAfterDate(globalStart, tmpTask.getDuration()));
            taskDataList2.add(tmpTask);
            int res = dealWithAllTask(taskDataList2, groupName);
            if (res == 0){
                return 0;
            } else {
                return 1;
            }
        }

    }

    // 空启动， 如果当前款号和昨天一致，但是目前没有计划，也没有实际
    public int coldStart(String groupName, String orderName, String clothesVersionNumber){
        Map<String, Object> map = checkOrderNameReturn(groupName, orderName);
        Date startDate = (Date)map.get("startDate");
        System.out.println("startDate------" + startDate);
        int pieceCount = Math.round((float)map.get("pieceCount"));
        System.out.println("pieceCount---------" + pieceCount);
        int dayOrder = (int)map.get("dayOrder");
        System.out.println("dayOrder---------" + dayOrder);
        int dailyCount = Math.round((float)map.get("dailyCount"));
        System.out.println("dailyCount---------" + dailyCount);
        int res1 = adjustPlanByGroupOrderName(groupName, orderName, clothesVersionNumber, startDate);
        TaskData parentTaskData = taskDataService.getParentTaskDataByGroupName(groupName, "2");
        TaskData planTask = taskDataService.getPlanTaskDataByOrder(groupName, orderName, startDate);
        TaskData actTask = new TaskData();
        actTask.setTaskType("4");
        actTask.setGroupName(groupName);
        actTask.setText(orderName);
        actTask.setOrderName(orderName);
        actTask.setTaskCount(planTask.getTaskCount());
        actTask.setClothesVersionNumber(clothesVersionNumber);
        actTask.setParent(parentTaskData.getId());
        actTask.setColorName(planTask.getColorName());
        actTask.setSizeName(planTask.getSizeName());
        actTask.setFinishCount(pieceCount);
        actTask.setProgress((float)pieceCount/planTask.getTaskCount());
        actTask.setTaskOpen("true");
        actTask.setBackType("green");
        actTask.setSam(planTask.getSam());
        actTask.setRemark("无");
        actTask.setStartDate(startDate);
        if (dayOrder == 0){
            actTask.setDuration(Math.round((float)planTask.getTaskCount()/(dailyCount*3)));
        } else if (dayOrder == 1) {
            actTask.setDuration(Math.round(planTask.getTaskCount()/(dailyCount*1.5f)));
        } else {
            actTask.setDuration(Math.round((float)planTask.getTaskCount()/dailyCount));
        }
        actTask.setEndDate(getAfterDate(startDate, actTask.getDuration()));
        actTask.setDailyCount(dailyCount);
        actTask.setFinishCount(pieceCount);
        if (actTask.getEndDate().after(planTask.getEndDate())){
            actTask.setDeliveryState("逾期");
        } else {
            actTask.setDeliveryState("正常");
        }
        int res2 = taskDataService.addTaskData(actTask);
        if (res1 == 0 && res2 == 0){
            return 0;
        } else {
            return 1;
        }
    }

    // 判断该组该款是否是二次上线
    public Map<String, Object> checkOrderNameReturn(String groupName, String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<GeneralSalary> generalSalaryList = taskDataService.getGroupDailyPieceWorkByInfo(groupName.substring(2), orderName, null);
        List<GeneralSalary> resultGeneralSalaryList = new ArrayList<>();
        float pieceCount = 0;
        for (int i = 0; i < generalSalaryList.size() - 1; i ++){
            Date generalDate = generalSalaryList.get(i).getGeneralDate();
            Date yesterday = generalSalaryList.get(i + 1).getGeneralDate();
            pieceCount += generalSalaryList.get(i).getPieceCount();
            resultGeneralSalaryList.add(generalSalaryList.get(i));
            if (yesterday.compareTo(getBeforeDate(generalDate, 1)) != 0){
                List<GeneralSalary> generalSalaryList1 = taskDataService.getGroupDailyPieceWorkByInfo(groupName.substring(2), null, getBeforeDate(generalDate, 1));
                if (generalSalaryList1 != null && !generalSalaryList1.isEmpty()){
                    map.put("startDate", generalDate);
                    map.put("pieceCount", pieceCount);
                    map.put("dayOrder", resultGeneralSalaryList.size() - 1);
                    map.put("dailyCount", generalSalaryList.get(1).getPieceCount());
                    return map;
                }
            }
        }
        map.put("startDate", generalSalaryList.get(generalSalaryList.size() - 1).getGeneralDate());
        map.put("pieceCount", pieceCount);
        map.put("dayOrder", resultGeneralSalaryList.size() - 1);
        map.put("dailyCount", generalSalaryList.get(1).getPieceCount());
        return map;
    }


    // 创建一个新的实际进度
    public int createNewActTaskData(String groupName, String orderName, String clothesVersionNumber, int pieceCount){
        TaskData parentTaskData = taskDataService.getParentTaskDataByGroupName(groupName, "2");
        TaskData planTask = taskDataService.getPlanTaskDataByOrder(groupName, orderName, new Date());
        TaskData actTask = new TaskData();
        Float sam = orderProcedureService.getSectionSamByOrder(orderName, "车缝");
        int employeeCount = employeeService.getEmployeeCountByGroup(groupName);
        actTask.setTaskType("4");
        actTask.setText(orderName);
        actTask.setOrderName(orderName);
        actTask.setGroupName(groupName);
        actTask.setTaskCount(planTask.getTaskCount());
        actTask.setClothesVersionNumber(clothesVersionNumber);
        actTask.setParent(parentTaskData.getId());
        actTask.setColorName(planTask.getColorName());
        actTask.setSizeName(planTask.getSizeName());
        actTask.setProgress((float)pieceCount/planTask.getTaskCount());
        actTask.setTaskOpen("true");
        actTask.setBackType("green");
        actTask.setSam(planTask.getSam());
        actTask.setRemark("无");
        actTask.setDayOrder(0);
        actTask.setDeliveryState("正常");
        actTask.setStartDate(new Date());
        actTask.setDuration(planTask.getDuration());
        actTask.setEndDate(planTask.getEndDate());
        actTask.setDailyCount(Math.round((employeeCount*11*60)/(sam*3f)));
        actTask.setFinishCount(pieceCount);
        return taskDataService.addTaskData(actTask);
    }

    // 把计划的数据重新梳理
    public int dealWithAllTask(List<TaskData> taskDataList, String groupName){
        int res1 = taskDataService.clearTaskDataByGroupType("3", groupName);
        for (TaskData taskData : taskDataList){
            taskData.setTaskType("3");
        }
        int res2 = taskDataService.addTaskDataBatch(taskDataList);
        List<TaskData> taskDataList3 = taskDataService.getUnFinishTaskDataByTaskTypeGroupName("3", groupName);
        List<TaskLinks> taskLinksList = new ArrayList<>();
        for (int i = 0; i < taskDataList3.size() - 1; i++){
            TaskLinks taskLinks = new TaskLinks();
            taskLinks.setSource(taskDataList3.get(i).getId());
            taskLinks.setTarget(taskDataList3.get(i + 1).getId());
            taskLinks.setType(0);
            taskLinksList.add(taskLinks);
        }
        int res3 = taskDataService.addTaskLinksBatch(taskLinksList);
        if (res1 == 0 && res2 == 0 && res3 == 0){
            return 0;
        } else {
            return 1;
        }
    }

}
