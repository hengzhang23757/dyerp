package com.example.erp01.action;

import com.example.erp01.model.*;
import com.example.erp01.service.*;
import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import java.text.SimpleDateFormat;
import java.util.*;

//裁片基本操作，包括生成裁片信息，保存裁片信息，查询裁片数据等
@Controller
@RequestMapping(value = "erp")
public class TailorController {
    private static final Logger log = LoggerFactory.getLogger(TailorController.class);
    @Autowired
    private TailorService tailorService;

    @Autowired
    private MaxTailorQcodeIDService maxTailorQcodeIDService;

    @Autowired
    private OtherTailorService otherTailorService;

    @Autowired
    private OrderClothesService orderClothesService;

    @Autowired
    private FinishTailorService finishTailorService;

    @Autowired
    private TailorBackService tailorBackService;

    @Autowired
    private ReportService reportService;

    @Autowired
    private MaxBedNumberService maxBedNumberService;
    @Autowired
    private OrderProcedureService orderProcedureService;

    private static int tmpPackageNumber;

    @RequestMapping("/tailorStart")
    public String tailorStart(){
        return "tailor/tailor";
    }


    @RequestMapping("/multiTailorStart")
    public String multiTailorStart(){
        return "opaMsg/multiTailor";
    }

    @RequestMapping("/bedTailorInfoStart")
    public String bedTailorInfoStart(Model model){
        model.addAttribute("bigMenuTag",2);
        model.addAttribute("menuTag",23);
        return "opaMsg/bedTailorInfo";
    }

    @RequestMapping("/oneTailorInfoStart")
    public String oneTailorInfoStart(Model model){
        model.addAttribute("bigMenuTag",2);
        model.addAttribute("menuTag",24);
        return "opaMsg/oneTailorInfo";
    }

    @RequestMapping("/tailorReprintStart")
    public String tailorReprintStart(Model model){
        model.addAttribute("bigMenuTag",2);
        model.addAttribute("menuTag",21);
        return "opaMsg/tailorReprint";
    }

    @RequestMapping("/tailorChangeCodeStart")
    public String tailorChangeCodeStart(Model model){
        return "opaMsg/tailorChangeCode";
    }

    @RequestMapping("/finishReprintStart")
    public String finishReprintStart(Model model){
        model.addAttribute("bigMenuTag",2);
        model.addAttribute("menuTag",22);
        return "opaMsg/finishReprint";
    }

//    @RequestMapping("/tailorSummaryStart")
//    public String tailorSummaryStart(Model model){
//        model.addAttribute("bigMenuTag",2);
//        model.addAttribute("menuTag",211);
//        return "opaMsg/tailorSummary";
//    }



    /**
     * 进入录入裁片信息页面
     * @param model
     * @return
     */
    @RequestMapping("/addSingleTailorStart")
    public String addSingleTailorStart(Model model,String orderName){
        if(orderName == null) {
            model.addAttribute("type", "add");
        }else {
            model.addAttribute("orderName", orderName);
            model.addAttribute("type", "detail");
        }
        return "opaMsg/fb_addSingleTailor";
    }

    @RequestMapping("/addMultiTailorStart")
    public String addMultiTailorStart(Model model,String orderName){
        if(orderName == null) {
            model.addAttribute("type", "add");
        }else {
            model.addAttribute("orderName", orderName);
            model.addAttribute("type", "detail");
        }
        return "opaMsg/fb_addMultiTailor";
    }

    @RequestMapping("/addSmallMultiTailorStart")
    public String addSmallMultiTailorStart(Model model,String orderName){
        if(orderName == null) {
            model.addAttribute("type", "add");
        }else {
            model.addAttribute("orderName", orderName);
            model.addAttribute("type", "detail");
        }
        return "opaMsg/fb_addSmallMultiTailor";
    }


    @RequestMapping(value = "/savetailordata")
    @ResponseBody
    public int saveTailorData(@RequestParam("tailorList")String tailorList,
                              @RequestParam("groupName")String groupName){
        Gson gson = new Gson();
        List<Tailor> tailorList1 = gson.fromJson(tailorList,new TypeToken<List<Tailor>>(){}.getType());
        List<TailorBack> tailorList2 = gson.fromJson(tailorList,new TypeToken<List<TailorBack>>(){}.getType());
        for (Tailor tailor : tailorList1){
            int initCount = tailor.getLayerCount();
            tailor.setGroupName(groupName);
            tailor.setInitCount(initCount);
        }
        for (TailorBack tailorBack : tailorList2){
            int initCount = tailorBack.getLayerCount();
            tailorBack.setGroupName(groupName);
            tailorBack.setInitCount(initCount);
        }
        int res1 = tailorService.saveTailorData(tailorList1);
        int res2 = tailorBackService.saveTailorBackData(tailorList2);
        if (res1 == 0 && res2 == 0){
            return 0;
        }else {
            return 1;
        }
    }


    @RequestMapping(value = "/getmaxbednumber", method = RequestMethod.GET)
    @ResponseBody
    public Integer getMaxBedNumber(@RequestParam("orderName")String orderName){
        return maxBedNumberService.getMaxBedNumberByOrder(orderName);
    }

    @RequestMapping(value = "/getmaxbednumberbyordertype", method = RequestMethod.GET)
    @ResponseBody
    public Integer getBedNumberByOrderName(@RequestParam("orderName")String orderName,
                                           @RequestParam("tailorType")Integer tailorType){
        int bedNumber = tailorService.getMaxBedNumberByOrderTailorType(orderName, tailorType);
        return  (bedNumber + 1);
    }


    @RequestMapping(value = "/getbednumberbyordername", method = RequestMethod.GET)
    @ResponseBody
    public Integer getBedNumberByOrderName(@RequestParam("orderName")String orderName){
        Integer bedNumber = tailorService.getMaxBedNumber(orderName);
        return bedNumber;
    }


    @RequestMapping(value = "/tailorreport", method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getTailorReport(@RequestParam("orderName")String orderName,
                                              @RequestParam("partName")String partName,
                                              @RequestParam(value = "bedNumber", required = false)Integer bedNumber){
        Map<String,Object> map = new LinkedHashMap<>();
        List<TailorReport> reportList = new ArrayList<>();
        if (partName.equals("主身布")){
            reportList = tailorService.tailorReport(orderName, bedNumber, 0);
        } else {
            reportList = tailorService.tailorReport(orderName, bedNumber, 1);
        }
        Date cutDate = tailorService.getLastCutDateByOrderBed(orderName, bedNumber);
        map.put("cutDate", cutDate);
        List<String> sizeList = new ArrayList<>();
        List<String> colorList = new ArrayList<>();
        List<String> jarList = new ArrayList<>();
        List<String> loList = new ArrayList<>();
        List<TailorReport> loTailorReportList1 = new ArrayList<>();
        List<TailorReport> loTailorReportList2 = new ArrayList<>();
        for (TailorReport tailorReport : reportList){
            String tmpSize = tailorReport.getSizeName();
            String tmpColor = tailorReport.getColorName();
            String tmpJar = tailorReport.getJarName();
            if (!jarList.contains(tmpJar)){
                jarList.add(tmpJar);
            }
            if (!sizeList.contains(tmpSize)){
                sizeList.add(tmpSize);
            }
            if (!colorList.contains(tmpColor)){
                colorList.add(tmpColor);
            }
            if (tmpJar.length() < 2){
                loTailorReportList2.add(tailorReport);
            } else if (tmpJar.substring(tmpJar.length()-2,tmpJar.length()-1).equals("#")){
                loList.add(tmpJar.substring(tmpJar.length()-2));
                loTailorReportList1.add(tailorReport);
            } else {
                loTailorReportList2.add(tailorReport);
            }
        }
        List<String> desiredOrder = Arrays.asList("XXXXXS","5XS","XXXXS","4XS","XXXS","3XS","XXS","2XS","XS","XS/25","xs","S","S/27","M","M/29","L","L/31","XL",
                "XL/33","XXL","2XL","XXL/36","XXXL","3XL","XXXL/38","4XL","XXXXL","5XL","XXXXXL","6XL","XXXXXXL","80","090","90","100","110","110/27",
                "120","120/29","130","130/31","140","140/33","150","150/36","155","160","165","170","175","180","185","190");
        Comparator<String> sizeOrder = Comparator.comparingInt(desiredOrder::indexOf);
        sizeList.sort(sizeOrder);
        Map<String, Object> colorMap = new LinkedHashMap<>();
        for (String color : colorList){
            Map<String, Object> sizeMap = new LinkedHashMap<>();
            for (String size : sizeList){
                int tmpSizeCount = 0;
                for (TailorReport tailorReport : reportList){
                    if (tailorReport.getColorName().equals(color) && tailorReport.getSizeName().equals(size)){
                        tmpSizeCount += tailorReport.getLayerCount();
                    }
                }
                sizeMap.put(size,tmpSizeCount);
            }
            colorMap.put(color,sizeMap);
        }
        Map<String, Object> loColorMap = new LinkedHashMap<>();
        for (String color : colorList){
            boolean colorFlag = false;
            Map<String, Object> loMap = new LinkedHashMap<>();
            for (String lo : loList){
                boolean loFlag = false;
                Map<String, Object> loJarMap = new LinkedHashMap<>();
                for (String jar : jarList){
                    boolean jarFlag = false;
                    Map<String, Object> loSizeMap = new LinkedHashMap<>();
                    for (String size : sizeList){
                        int thisSizeCount = 0;
                        for (TailorReport tailorReport : loTailorReportList1){
                            String thisColor = tailorReport.getColorName();
                            String thisSize = tailorReport.getSizeName();
                            String thisJar = tailorReport.getJarName();
                            if (thisColor.equals(color) && thisSize.equals(size) && lo.equals(thisJar.substring(thisJar.length()-2)) && jar.equals(thisJar)){
                                thisSizeCount += tailorReport.getLayerCount();
                                jarFlag = true;
                            }
                        }
                        loSizeMap.put(size, thisSizeCount);
                    }
                    if (jarFlag){
                        loJarMap.put(jar, loSizeMap);
                        loFlag = true;
                    }
                }
                if (loFlag){
                    loMap.put(lo, loJarMap);
                    colorFlag = true;
                }
            }
            boolean loNoFlag = false;
            Map<String, Object> loJarMap2 = new LinkedHashMap<>();
            for (String jar2 : jarList){
                boolean jarFlag2 = false;
                Map<String, Object> loSizeMap2 = new LinkedHashMap<>();
                for (String size2 : sizeList){
                    int thisSizeCount2 = 0;
                    for (TailorReport tailorReport2 : loTailorReportList2){
                        String thisColor = tailorReport2.getColorName();
                        String thisSize = tailorReport2.getSizeName();
                        String thisJar = tailorReport2.getJarName();
                        if (thisColor.equals(color) && thisSize.equals(size2) && jar2.equals(thisJar)){
                            thisSizeCount2 += tailorReport2.getLayerCount();
                            jarFlag2 = true;
                        }
                    }
                    loSizeMap2.put(size2, thisSizeCount2);
                }
                if (jarFlag2){
                    loJarMap2.put(jar2, loSizeMap2);
                    loNoFlag = true;
                }
            }
            if (loNoFlag){
                loMap.put("未分", loJarMap2);
                colorFlag = true;
            }
            if (colorFlag){
                loColorMap.put(color, loMap);
            }
        }
        int oneLength = 0;
        int twoLength = 0;
        int threeLength = 0;
        if (reportList.size() == 0){
            oneLength = 0;
            twoLength = 0;
            threeLength = 0;
        } else if (reportList.size() == 1){
            oneLength = 1;
            twoLength = 0;
            threeLength = 0;
        } else if (reportList.size() == 2){
            oneLength = 1;
            twoLength = 1;
            threeLength = 0;
        } else if (reportList.size()%3 == 0){
            oneLength = reportList.size()/3;
            twoLength = reportList.size()/3;
            threeLength = reportList.size()/3;
        } else if (reportList.size()%3 == 1){
            oneLength = reportList.size()/3 + 1;
            twoLength = reportList.size()/3;
            threeLength = reportList.size()/3;
        }else if (reportList.size()%3 == 2){
            oneLength = reportList.size()/3 + 1;
            twoLength = reportList.size()/3 + 1;
            threeLength = reportList.size()/3;
        }
        map.put("detail",reportList);
        map.put("color",colorMap);
        map.put("jar",loColorMap);
        map.put("size",sizeList);
        map.put("oneLength",oneLength);
        map.put("twoLength",twoLength);
        map.put("threeLength",threeLength);
        return map;
    }

    @RequestMapping(value = "/getbednumbersbyordername", method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getBedNumbersByOrderName(@RequestParam("orderName")String orderName){
        Map<String,Object> result = new HashMap<>();
        List<Integer> bedNumList = tailorService.getBedNumbersByOrderNamePartNameTailorType(orderName, null, null);
        result.put("bedNumList",bedNumList);
        return result;
    }

    @RequestMapping(value = "/gettailorcountbyordernamebednumpart", method = RequestMethod.GET)
    @ResponseBody
    public Integer getTailorCountByOrderNameBedNumPart(@RequestParam("orderName")String orderName,
                                                       @RequestParam("bedNumber")int bedNumber,
                                                       @RequestParam("partName")String partName){
        int tmpCount = tailorService.getTailorCountByInfo(orderName, null, null, partName,bedNumber, null, null);
        return tmpCount;
    }

    /**
     * 月度裁床报表
     */

    @RequestMapping(value = "/tailormonthreport", method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> tailorMonthReport(@RequestParam("from")String from,
                                                @RequestParam("to")String to,
                                                @RequestParam(value = "orderName", required = false)String orderName,
                                                @RequestParam(value = "groupName", required = false)String groupName)throws Exception{
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date fromTime = sdf.parse(from);
        Date toTime = sdf.parse(to);
        Map<String,Object> map = new HashMap<>();
        List<TailorMonthReport> tailorMonthReportList = tailorService.tailorMonthReport(fromTime, toTime, orderName,null,0, groupName);
        List<String> orderNameList = new ArrayList<>();
        for (TailorMonthReport tailorMonthReport : tailorMonthReportList){
            orderNameList.add(tailorMonthReport.getOrderName());
        }
        List<OrderClothes> orderClothesList = orderClothesService.getOrderCountByOrderNameList(orderNameList);
        List<OrderProcedure> orderProcedureList = orderProcedureService.getOrderProcedureByOrderNames(orderNameList, null);
        for (TailorMonthReport tailorMonthReport: tailorMonthReportList){
            for (OrderClothes orderClothes : orderClothesList){
                if (tailorMonthReport.getOrderName().equals(orderClothes.getOrderName())){
                    tailorMonthReport.setOrderCount(orderClothes.getOrderSum());
                }
            }
            for (OrderProcedure orderProcedure : orderProcedureList){
                if (orderProcedure.getProcedureNumber().equals(2000) && orderProcedure.getOrderName().equals(tailorMonthReport.getOrderName())){
                    tailorMonthReport.setPrice((float)orderProcedure.getPiecePrice());
                    tailorMonthReport.setPriceTwo((float)orderProcedure.getPiecePriceTwo());
                    tailorMonthReport.setSumMoney(tailorMonthReport.getPieceCount() * (tailorMonthReport.getPrice() + tailorMonthReport.getPriceTwo()));
                }
            }
            Integer cutCount = tailorService.getTailorCountByInfo(tailorMonthReport.getOrderName(), null, null,null,null,null, 0);
            if (cutCount != null){
                tailorMonthReport.setCutCount(cutCount);
            }
            tailorMonthReport.setDifferenceCount(tailorMonthReport.getCutCount() - tailorMonthReport.getOrderCount());
        }
        map.put("tailorMonthReportList", tailorMonthReportList);
        return map;
    }
    /**
     * 用作裁片查漏
     * @param orderName
     * @return
     */
    @RequestMapping(value = "/gettailorinfobyname",method = RequestMethod.GET)
    @ResponseBody
    public String getTailorInfoByName(@RequestParam("orderName")String orderName){
        List<CutQueryLeak> tailorInfoList = tailorService.getTailorInfoByOrderPartType(orderName, null, 0, null, null);
        Gson gson = new Gson();
        String res = gson.toJson(tailorInfoList);
        return res;
    }

    @RequestMapping(value = "/gettailorlistbytailorqcodeidlist",method = RequestMethod.GET)
    @ResponseBody
    public List<Tailor> getTailorQcodeByTailorQcodeID(@RequestParam("tailorQcodeIDList")List<Integer> tailorQcodeIDList){
        List<Tailor> tailorQcodeList = tailorService.getTailorsByTailorQcodeIDs(tailorQcodeIDList, 0);
        return tailorQcodeList;
    }

    @RequestMapping(value = "/minigetonetailorqcodebytailorqcodeid",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> miniGetOneTailorQcodeByTailorQcodeID(@RequestParam("tailorQcodeID")Integer tailorQcodeID){
        Map<String, Object> map = new LinkedHashMap<>();
        Tailor tailor = tailorService.getTailorByTailorQcodeID(tailorQcodeID, null);
        if (null == tailor){
            FinishTailor finishTailor = null;
            if (finishTailorService.getFinishTailorByTailorQcodeID(tailorQcodeID) != null){
                finishTailor = finishTailorService.getFinishTailorByTailorQcodeID(tailorQcodeID);
            }
            map.put("tailor", finishTailor);
        } else {
            map.put("tailor", tailor);
        }
        return map;
    }

    @RequestMapping(value = "/getpackageinfoindelete",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getPackageInfoInDelete(@RequestParam("orderName")String orderName,
                                                     @RequestParam("bedNumber")int bedNumber){
        Map<String,Object> map = new HashMap<>();
        List<Tailor> tailorList = tailorService.getPackageInfoInDelete(orderName, bedNumber);
        map.put("tailorList",tailorList);
        return map;
    }

    @RequestMapping(value = "/getbedinfoindelete",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getBedInfoInDelete(@RequestParam("orderName")String orderName){
        Map<String,Object> map = new HashMap<>();
        List<Tailor> tailorList = tailorService.getBedInfoInDelete(orderName);
        map.put("tailorList",tailorList);
        return map;
    }

    @RequestMapping(value = "/deletetailor",method = RequestMethod.POST)
    @ResponseBody
    public int deleteTailor(@RequestParam("tailorID")int tailorID){
        int res = tailorService.deleteTailor(tailorID);
        return res;
    }

    @RequestMapping(value = "/deletetailorbyorderbed",method = RequestMethod.POST)
    @ResponseBody
    public int deleteTailorByOrderBed(@RequestParam("orderName")String orderName,
                                      @RequestParam("bedNumber")Integer bedNumber,
                                      @RequestParam("userName")String userName){
        log.warn("裁片按床删除---"+"订单号："+orderName+"---床号："+bedNumber+"---操作用户："+userName);
        tailorService.preDeleteInDeleteBed(orderName, bedNumber);
        return tailorService.deleteTailorByOrderBed(orderName, bedNumber);
    }


    @RequestMapping(value = "/deletetailorbyorderbedpack",method = RequestMethod.POST)
    @ResponseBody
    public int deleteTailorByOrderBedPack(@RequestParam("orderName")String orderName,
                                          @RequestParam("bedNumber")Integer bedNumber,
                                          @RequestParam("userName")String userName,
                                          @RequestParam("packageNumberList")List<Integer> packageNumberList){
        log.warn("裁片按扎删除---"+"订单号："+orderName+"---床号："+bedNumber+"---操作用户："+userName+"---删除内容："+packageNumberList);
        tailorService.preDeleteInDeletePackage(orderName, bedNumber, packageNumberList);
        return tailorService.deleteTailorByOrderBedPack(orderName, bedNumber, packageNumberList);
    }

    @RequestMapping(value = "/getalltailorbyorderbed",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getAllTailorByOrderBed(@RequestParam("orderName")String orderName,
                                                     @RequestParam(value = "bedNumber", required = false)Integer bedNumber,
                                                     @RequestParam(value = "packageNumber", required = false)Integer packageNumber){
        Map<String,Object> map = new HashMap<>();
        List<Tailor> tailorList = tailorService.getTailorByInfo(orderName, null, null, null, bedNumber, packageNumber, null, 0);
        map.put("tailorList",tailorList);
        return map;
    }

    @RequestMapping(value = "/getpackagenumbersbyorderbedcolor",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getPackageNumbersByOrderBedColor(@RequestParam("orderName")String orderName,
                                                         @RequestParam("bedNumber")Integer bedNumber,
                                                               @RequestParam("colorName")String colorName){
        Map<String,Object> map = new HashMap<>();
        List<Integer> packageNumberList = tailorService.getPackageNumbersByOrderBedColor(orderName, bedNumber, colorName);
        map.put("packageNumberList",packageNumberList);
        return map;
    }

    @RequestMapping(value = "/getalltailorbyordercolorsizepart",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getAllTailorByOrderColorSizePart(@RequestParam("orderName")String orderName,
                                                               @RequestParam("colorName")String colorName,
                                                               @RequestParam("sizeName")String sizeName,
                                                               @RequestParam("partName")String partName){
        Map<String,Object> map = new HashMap<>();
        List<Tailor> tailorList = tailorService.getTailorByInfo(orderName,colorName,sizeName,partName,null,null,null,0);
        map.put("tailorList",tailorList);
        return map;

    }


    @RequestMapping(value = "/updatetailordata", method = RequestMethod.POST)
    @ResponseBody
    public int updateTailorData(@RequestParam("tailorList")String tailorList){
        Gson gson = new Gson();
        List<Tailor> tailorList1 = gson.fromJson(tailorList,new TypeToken<List<Tailor>>(){}.getType());
        List<Tailor> tailorList2 = new ArrayList<>();
        for (Tailor tailor : tailorList1){
            String orderName = tailor.getOrderName();
            String customerName = tailor.getCustomerName();
            String bedNumber = tailor.getBedNumber().toString();
            String jarName = tailor.getJarName();
            String colorName = tailor.getColorName();
            String sizeName = tailor.getSizeName();
            String layerCount = tailor.getLayerCount().toString();
            String packageNumber = tailor.getPackageNumber().toString();
            String partName = tailor.getPartName();
            String tailorQcode = orderName+"-"+customerName+"-"+bedNumber+"-"+jarName+"-"+colorName+"-"+sizeName+"-"+layerCount+"-"+packageNumber+"-"+partName;
            Tailor tt = new Tailor(tailor.getTailorID(),tailor.getLayerCount(),tailor.getInitCount(),tailorQcode,tailor.getGroupName(),tailor.getWeight());
            tailorList2.add(tt);
        }
        return tailorService.updateTailorData(tailorList2);
    }

    @RequestMapping(value = "/tailorsummarymonthreport", method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> tailorSummaryMonthReport(@RequestParam("from")String from,
                                                       @RequestParam("to")String to,
                                                       @RequestParam(value = "orderName", required = false)String orderName,
                                                       @RequestParam("type")String type,
                                                       @RequestParam(value = "groupName", required = false)String groupName)throws Exception{
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date fromTime = sdf.parse(from);
        Date toTime = sdf.parse(to);
        Map<String,Object> map = new HashMap<>();
        List<TailorMonthReport> tailorMonthReportList1 = tailorService.tailorMonthReport(fromTime, toTime,orderName,null, 0,groupName);
        List<TailorMonthReport> tailorMonthReportList2 = tailorService.otherTailorMonthReport(fromTime, toTime,orderName,null, 1,groupName);
        List<TailorMonthReport> summaryTailorMonthReportList = new ArrayList<>();
        for (TailorMonthReport tailorMonthReport: tailorMonthReportList1){
            Integer orderCount = orderClothesService.getOrderTotalCount(tailorMonthReport.getOrderName());
            Integer cutCount = tailorService.getTailorCountByInfo(tailorMonthReport.getOrderName(), null,null,null,null,null, 0);
            if (orderCount == null){
                orderCount = 0;
            }
            if (cutCount == null){
                cutCount = 0;
            }
            Integer differenceCount = cutCount - orderCount;
//            String versionNumber = orderClothesService.getVersionNumberByOrderName(tailorMonthReport.getOrderName());
            summaryTailorMonthReportList.add(new TailorMonthReport(tailorMonthReport.getOrderName(),tailorMonthReport.getClothesVersionNumber(), tailorMonthReport.getCustomerName(),tailorMonthReport.getBedNumberCount(),tailorMonthReport.getPieceCount(),orderCount,cutCount,differenceCount,"主身布",tailorMonthReport.getGroupName()));
        }
        for (TailorMonthReport otherTailorMonthReport: tailorMonthReportList2){
            Integer orderCount = orderClothesService.getOrderTotalCount(otherTailorMonthReport.getOrderName());
            Integer cutCount = tailorService.getTailorCountByInfo(otherTailorMonthReport.getOrderName(),null,null, otherTailorMonthReport.getPartName(), null,null,1);
            if (orderCount == null){
                orderCount = 0;
            }
            if (cutCount == null){
                cutCount = 0;
            }
            Integer differenceCount = cutCount - orderCount;
//            String versionNumber = orderClothesService.getVersionNumberByOrderName(otherTailorMonthReport.getOrderName());
            summaryTailorMonthReportList.add(new TailorMonthReport(otherTailorMonthReport.getOrderName(),otherTailorMonthReport.getClothesVersionNumber(), otherTailorMonthReport.getCustomerName(),otherTailorMonthReport.getBedNumberCount(),otherTailorMonthReport.getPieceCount(),orderCount,cutCount,differenceCount,otherTailorMonthReport.getPartName(), otherTailorMonthReport.getGroupName()));
        }
        map.put("summaryTailorMonthReportList",summaryTailorMonthReportList);
        return map;
    }

    @RequestMapping(value = "/tailorsummarymonthreportofeachday", method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> tailorSummaryMonthReportOfEachDay(@RequestParam("from")String from,
                                                                @RequestParam("to")String to,
                                                                @RequestParam(value = "orderName",required = false)String orderName,
                                                                @RequestParam(value = "groupName", required = false)String groupName)throws Exception{
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        if ("".equals(orderName)){
            orderName = null;
        }
        if ("".equals(groupName)){
            groupName = null;
        }
        Date fromTime = sdf.parse(from);
        Date toTime = sdf.parse(to);
        Map<String,Object> map = new HashMap<>();
        List<TailorMonthReport> tailorMonthReportList1 = reportService.getTailorMonthReportOfEachDay(fromTime, toTime,orderName,null,0,groupName);
        List<TailorMonthReport> tailorMonthReportList2 = reportService.getOtherTailorMonthReportOfEachDay(fromTime, toTime, orderName, null, 1,groupName);
        List<TailorMonthReport> summaryTailorMonthReportList = new ArrayList<>();
        for (TailorMonthReport tailorMonthReport: tailorMonthReportList1){
            Integer orderCount = orderClothesService.getOrderTotalCount(tailorMonthReport.getOrderName());
            Integer cutCount = tailorService.getTailorCountByInfo(tailorMonthReport.getOrderName(), null,null,null,null,null,0);
            if (orderCount == null){
                orderCount = 0;
            }
            if (cutCount == null){
                cutCount = 0;
            }
            Integer differenceCount = cutCount - orderCount;
            TailorMonthReport tailorMonthReport1 = new TailorMonthReport(tailorMonthReport.getOrderName(),tailorMonthReport.getClothesVersionNumber(),tailorMonthReport.getCustomerName(),tailorMonthReport.getBedNumberCount(),tailorMonthReport.getPieceCount(),orderCount,cutCount,differenceCount,tailorMonthReport.getCutDate(),"主身布", tailorMonthReport.getGroupName());
            summaryTailorMonthReportList.add(tailorMonthReport1);
        }
        for (TailorMonthReport otherTailorMonthReport: tailorMonthReportList2){
            Integer orderCount = orderClothesService.getOrderTotalCount(otherTailorMonthReport.getOrderName());
            Integer cutCount = tailorService.getTailorCountByInfo(otherTailorMonthReport.getOrderName(),null,null, otherTailorMonthReport.getPartName(),null,null, 1);
            if (orderCount == null){
                orderCount = 0;
            }
            if (cutCount == null){
                cutCount = 0;
            }
            Integer differenceCount = cutCount - orderCount;
            TailorMonthReport tailorMonthReport2 = new TailorMonthReport(otherTailorMonthReport.getOrderName(),otherTailorMonthReport.getClothesVersionNumber(),otherTailorMonthReport.getCustomerName(),otherTailorMonthReport.getBedNumberCount(),otherTailorMonthReport.getPieceCount(),orderCount,cutCount,differenceCount,otherTailorMonthReport.getCutDate(),otherTailorMonthReport.getPartName(), otherTailorMonthReport.getGroupName());
            summaryTailorMonthReportList.add(tailorMonthReport2);
        }
        map.put("summaryTailorMonthReportList",summaryTailorMonthReportList);
        return map;
    }


    @RequestMapping(value = "/tailormonthreportofeachday", method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> tailorMonthReportOfEachDay(@RequestParam("from")String from,
                                                         @RequestParam("to")String to,
                                                         @RequestParam(value = "orderName", required = false)String orderName,
                                                         @RequestParam(value = "groupName", required = false)String groupName)throws Exception{
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date fromTime = sdf.parse(from);
        Date toTime = sdf.parse(to);
        Map<String,Object> map = new HashMap<>();
        List<TailorMonthReport> tailorMonthReportList1 = reportService.getTailorMonthReportOfEachDay(fromTime, toTime,orderName,null,0,groupName);
        List<String> orderNameList = new ArrayList<>();
        for (TailorMonthReport tailorMonthReport : tailorMonthReportList1){
            orderNameList.add(tailorMonthReport.getOrderName());
        }
        List<OrderClothes> orderClothesList = orderClothesService.getOrderCountByOrderNameList(orderNameList);
        List<OrderProcedure> orderProcedureList = orderProcedureService.getOrderProcedureByOrderNames(orderNameList, null);
        for (TailorMonthReport tailorMonthReport: tailorMonthReportList1){
            for (OrderClothes orderClothes : orderClothesList){
                if (orderClothes.getOrderName().equals(tailorMonthReport.getOrderName())){
                    tailorMonthReport.setOrderCount(orderClothes.getOrderSum());
                }
            }
            for (OrderProcedure orderProcedure : orderProcedureList){
                if (orderProcedure.getProcedureNumber().equals(2000) && orderProcedure.getOrderName().equals(tailorMonthReport.getOrderName())){
                    tailorMonthReport.setPrice((float)orderProcedure.getPiecePrice());
                    tailorMonthReport.setSumMoney(tailorMonthReport.getPieceCount() * tailorMonthReport.getPrice());
                }
            }
            Integer cutCount = tailorService.getTailorCountByInfo(tailorMonthReport.getOrderName(),null,null,null,null, null, 0);
            if (cutCount != null){
                tailorMonthReport.setCutCount(cutCount);
            }
            tailorMonthReport.setDifferenceCount(tailorMonthReport.getCutCount() - tailorMonthReport.getOrderCount());
        }
        map.put("summaryTailorMonthReportList", tailorMonthReportList1);
        return map;
    }


    @RequestMapping(value = "/generatemaintailordata", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> generateMainTailorData(@RequestParam("pcTailorJson")String pcTailorJson,
                                                      @RequestParam(value = "ratioSplit", required = false)Integer ratioSplit){
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        Map<String, Object> map = new LinkedHashMap<>();
        PcTailor pcTailor = gson.fromJson(pcTailorJson,new TypeToken<PcTailor>(){}.getType());
        String orderName = pcTailor.getOrderName();
        Integer bedNumber = pcTailor.getBedNumber();
        String clothesVersionNumber = pcTailor.getClothesVersionNumber();
        String customerName = pcTailor.getCustomerName();
        List<MiniTailorLayerInfo> miniTailorLayerInfoList = pcTailor.getMiniTailorLayerInfoList();
        List<MiniMatchRatio> miniMatchRatioList = pcTailor.getMiniMatchRatioList();
        List<String> partNameList = pcTailor.getPartNameList();
        List<Tailor> tailorList = new ArrayList<>();
        tmpPackageNumber = 0;
        Integer tmpMaxPackageNumber = tailorService.getMaxPackageNumberByOrder(orderName, 0);
        if(tmpMaxPackageNumber != null){
            tmpPackageNumber = tmpMaxPackageNumber;
        }
        if (null == ratioSplit || ratioSplit.equals(0)){
            synchronized (this) {
                int tmpMaxTailorQcodeID = maxTailorQcodeIDService.getMaxTailorQcodeId();
                for (int i = 0; i < partNameList.size(); i++) {
                    int tmpNumber = tmpPackageNumber;
                    for (MiniMatchRatio miniMatchRatio : miniMatchRatioList){
                        for (MiniTailorLayerInfo miniTailorLayerInfo : miniTailorLayerInfoList){
                            tmpNumber += 1;
                            tmpMaxTailorQcodeID += 1;
                            String tmpTailorQcode = orderName + "-" + customerName + "-" + bedNumber + "-" + miniTailorLayerInfo.getJarName() + "-" + miniTailorLayerInfo.getColorName() + "-" + miniMatchRatio.getSizeName() + "-" + miniTailorLayerInfo.getLayerCount()*miniMatchRatio.getRatio() + "-" + tmpNumber + "-" + partNameList.get(i);
                            Tailor tailor = new Tailor(orderName, clothesVersionNumber, customerName, bedNumber, miniTailorLayerInfo.getJarName(), miniTailorLayerInfo.getColorName(), miniMatchRatio.getSizeName(), partNameList.get(i), miniTailorLayerInfo.getLayerCount()*miniMatchRatio.getRatio(), tmpNumber, tmpTailorQcode, tmpMaxTailorQcodeID, miniTailorLayerInfo.getWeight(), miniTailorLayerInfo.getBatch());
                            tailorList.add(tailor);
                        }
                    }
                }
                maxTailorQcodeIDService.updateMaxTailorQcodeId(tmpMaxTailorQcodeID);
            }
        } else {
            synchronized (this) {
                int tmpMaxTailorQcodeID = maxTailorQcodeIDService.getMaxTailorQcodeId();
                for (int i = 0; i < partNameList.size(); i++) {
                    int tmpNumber = tmpPackageNumber;
                    for (MiniMatchRatio miniMatchRatio : miniMatchRatioList){
                        for (int sizeIndex = 0; sizeIndex < miniMatchRatio.getRatio(); sizeIndex ++){
                            for (MiniTailorLayerInfo miniTailorLayerInfo : miniTailorLayerInfoList){
                                tmpNumber += 1;
                                tmpMaxTailorQcodeID += 1;
                                String tmpTailorQcode = orderName + "-" + customerName + "-" + bedNumber + "-" + miniTailorLayerInfo.getJarName() + "-" + miniTailorLayerInfo.getColorName() + "-" + miniMatchRatio.getSizeName() + "-" + miniTailorLayerInfo.getLayerCount()*ratioSplit + "-" + tmpNumber + "-" + partNameList.get(i);
                                Tailor tailor = new Tailor(orderName, clothesVersionNumber, customerName, bedNumber, miniTailorLayerInfo.getJarName(), miniTailorLayerInfo.getColorName(), miniMatchRatio.getSizeName(), partNameList.get(i), miniTailorLayerInfo.getLayerCount()*ratioSplit, tmpNumber, tmpTailorQcode, tmpMaxTailorQcodeID, miniTailorLayerInfo.getWeight(), miniTailorLayerInfo.getBatch());
                                tailorList.add(tailor);
                            }
                        }

                    }
                }
                maxTailorQcodeIDService.updateMaxTailorQcodeId(tmpMaxTailorQcodeID);
            }
        }
        map.put("tailorList", tailorList);
        return map;
    }

    @RequestMapping(value = "/getMaxTailorQcodeId")
    @ResponseBody
    public int getMaxTailorQcodeId(int number) {
        int maxTailorQcodeID = maxTailorQcodeIDService.getMaxTailorQcodeId();
        maxTailorQcodeIDService.updateMaxTailorQcodeId(maxTailorQcodeID+number);
        return maxTailorQcodeID;
    }

    @RequestMapping(value = "/getMaxPackageNumberByOrderType")
    @ResponseBody
    public int getMaxPackageNumberByOrder(@RequestParam("orderName")String orderName,
                                          @RequestParam("tailorType")Integer tailorType) {
        tmpPackageNumber = 0;
        if (tailorType.equals(1)){
            tmpPackageNumber = 10000;
        }
        Integer tmpMaxPackageNumber = tailorService.getMaxPackageNumberByOrder(orderName, tailorType);
        if(tmpMaxPackageNumber != null){
            tmpPackageNumber = tmpMaxPackageNumber;
        }
        return tmpPackageNumber;
    }

    @RequestMapping(value = "/minigeneratemaintailordata", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> miniGenerateMainTailorData(@RequestParam("mainTailorJson")String mainTailorJson){
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        Map<String, Object> map = new LinkedHashMap<>();
        MiniTailor miniTailor = gson.fromJson(mainTailorJson,new TypeToken<MiniTailor>(){}.getType());
        String orderName = miniTailor.getOrderName();
        String clothesVersionNumber = miniTailor.getClothesVersionNumber();
        String customerName = orderClothesService.getCustomerNameByOrderName(orderName);
        List<MiniTailorLayerInfo> miniTailorLayerInfoList = miniTailor.getMiniTailorLayerInfoList();
        List<MiniMatchRatio> miniMatchRatioList = miniTailor.getMiniMatchRatioList();
        List<String> partNameList = miniTailor.getPartNameList();
        List<Tailor> tailorList = new ArrayList<>();
        tmpPackageNumber = 0;
        Integer tmpMaxPackageNumber = tailorService.getMaxPackageNumberByOrder(orderName, 0);
        if(tmpMaxPackageNumber != null){
            tmpPackageNumber = tmpMaxPackageNumber;
        }
        synchronized (this) {
            int tmpMaxTailorQcodeID = maxTailorQcodeIDService.getMaxTailorQcodeId();
            int bedNumber = tailorService.getMaxBedNumberByOrderTailorType(orderName, 0) + 1;
            for (int i = 0; i < partNameList.size(); i++) {
                int tmpNumber = tmpPackageNumber;
                for (MiniMatchRatio miniMatchRatio : miniMatchRatioList){
                    for (MiniTailorLayerInfo miniTailorLayerInfo : miniTailorLayerInfoList){
                        tmpNumber += 1;
                        tmpMaxTailorQcodeID += 1;
                        String tmpTailorQcode = orderName + "-" + customerName + "-" + bedNumber + "-" + miniTailorLayerInfo.getJarName() + "-" + miniTailorLayerInfo.getColorName() + "-" + miniMatchRatio.getSizeName() + "-" + miniTailorLayerInfo.getLayerCount()*miniMatchRatio.getRatio() + "-" + tmpNumber + "-" + partNameList.get(i);
                        Tailor tailor = new Tailor(orderName, clothesVersionNumber, customerName, bedNumber, miniTailorLayerInfo.getJarName(), miniTailorLayerInfo.getColorName(), miniMatchRatio.getSizeName(), partNameList.get(i), miniTailorLayerInfo.getLayerCount()*miniMatchRatio.getRatio(), tmpNumber, tmpTailorQcode, tmpMaxTailorQcodeID, miniTailorLayerInfo.getWeight(), miniTailorLayerInfo.getBatch());
                        tailorList.add(tailor);
                    }
                }
            }
            maxTailorQcodeIDService.updateMaxTailorQcodeId(tmpMaxTailorQcodeID);
        }
        map.put("tailorList", tailorList);
        return map;
    }

    @RequestMapping(value = "/minisavemaintailordata", method = RequestMethod.POST)
    @ResponseBody
    public int miniSaveMainTailorData(@RequestParam("tailorList")String tailorList,
                                      @RequestParam("groupName")String groupName){
        Gson gson = new Gson();
        List<Tailor> tailorList1 = gson.fromJson(tailorList,new TypeToken<List<Tailor>>(){}.getType());
        for (Tailor tailor : tailorList1){
            int initCount = tailor.getLayerCount();
            tailor.setGroupName(groupName);
            tailor.setInitCount(initCount);
            tailor.setPrintTimes(0);
            tailor.setIsDelete(0);
            tailor.setTailorType(0);
        }
        int res1 = tailorService.saveTailorData(tailorList1);
        if (res1 == 0){
            return 0;
        }else {
            return 1;
        }
    }

    @RequestMapping(value = "/generatemainsmalltailor")
    @ResponseBody
    public Map<String, Object> generateMainSmallTailor(@RequestParam("tailorListJson")String tailorListJson,
                                                       @RequestParam("partNameListJson")String partNameListJson,
                                                       @RequestParam("orderName")String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new Gson();
        List<String> partNameList = gson.fromJson(partNameListJson,new TypeToken<List<String>>(){}.getType());
        List<Tailor> tailorList1 = gson.fromJson(tailorListJson,new TypeToken<List<Tailor>>(){}.getType());
        tmpPackageNumber = 0;
        Integer tmpMaxPackageNumber = tailorService.getMaxPackageNumberByOrder(orderName, 0);
        if(tmpMaxPackageNumber != null){
            tmpPackageNumber = tmpMaxPackageNumber;
        }
        List<Tailor> tailorList = new ArrayList<>();
        synchronized (this) {
            int tmpMaxTailorQcodeID = maxTailorQcodeIDService.getMaxTailorQcodeId();
            for (String partName : partNameList){
                int tmpNumber = tmpPackageNumber;
                for (Tailor tailor : tailorList1){
                    tmpNumber += 1;
                    tmpMaxTailorQcodeID += 1;
                    Tailor destTailor = new Tailor();
                    destTailor.setOrderName(tailor.getOrderName());
                    destTailor.setClothesVersionNumber(tailor.getClothesVersionNumber());
                    destTailor.setPartName(partName);
                    destTailor.setJarName(tailor.getJarName());
                    destTailor.setTailorQcodeID(tmpMaxTailorQcodeID);
                    destTailor.setPackageNumber(tmpNumber);
                    destTailor.setBedNumber(tailor.getBedNumber());
                    destTailor.setIsDelete(0);
                    destTailor.setPrintTimes(0);
                    destTailor.setWeight(1f);
                    destTailor.setBatch(1);
                    destTailor.setTailorType(0);
                    destTailor.setLayerCount(tailor.getLayerCount());
                    destTailor.setInitCount(tailor.getInitCount());
                    destTailor.setCustomerName(tailor.getCustomerName());
                    destTailor.setColorName(tailor.getColorName());
                    destTailor.setSizeName(tailor.getSizeName());
                    tailorList.add(destTailor);
                }
            }
            maxTailorQcodeIDService.updateMaxTailorQcodeId(tmpMaxTailorQcodeID);
        }
        map.put("tailorList", tailorList);
        return map;
    }

    @RequestMapping(value = "/minigetmainbednumbersbyordername", method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> miniGetBedNumbersByOrderName(@RequestParam("orderName")String orderName){
        Map<String,Object> result = new HashMap<>();
        List<Integer> bedNumList = tailorService.getBedNumbersByOrderNamePartNameTailorType(orderName, null, 0);
        result.put("bedNumList",bedNumList);
        return result;
    }


    @RequestMapping(value = "/minigettailordatabyordernamebednumber", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> miniGetTailorDataByOrderNameBedNumber(@RequestParam("orderName")String orderName,
                                                                     @RequestParam("bedNumber")int bedNumber){
        Map<String, Object> map = new LinkedHashMap<>();
        List<Tailor> tailorList = tailorService.getTailorByInfo(orderName,null,null,null,bedNumber,null,null,0);
        map.put("tailorList", tailorList);
        return map;
    }

    @RequestMapping(value = "/minigettailorbytailorqcodeid", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> miniGetTailorByTailorQcodeID(@RequestParam("tailorQcodeID") Integer tailorQcodeID){
        Map<String, Object> map = new LinkedHashMap<>();
        Tailor tailor = tailorService.getTailorByTailorQcodeID(tailorQcodeID, 0);
        map.put("tailor", tailor);
        return map;
    }

    @RequestMapping(value = "/minigetonetailorbytailorqcodeid", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> miniGetOneTailorByTailorQcodeID(@RequestParam("tailorQcodeID") Integer tailorQcodeID){
        Map<String, Object> map = new LinkedHashMap<>();
        if (tailorService.getTailorByTailorQcodeID(tailorQcodeID, null) != null){
            Tailor tailor = tailorService.getTailorByTailorQcodeID(tailorQcodeID, null);
            map.put("tailor", tailor);
            return map;
        } else if (finishTailorService.getFinishTailorByTailorQcodeID(tailorQcodeID) != null){
            FinishTailor finishTailor = finishTailorService.getFinishTailorByTailorQcodeID(tailorQcodeID);
//            int check = 0;
//            if (finishTailor.getShelfOut() != null){
//                check = finishTailor.getShelfOut();
//            }
            map.put("tailor", finishTailor);
            map.put("check", 1);
            return map;
        } else {
            return map;
        }
    }

    @RequestMapping(value = "/getbednumbersbyorderparttype", method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getBedNumbersByOrderPartType(@RequestParam("orderName")String orderName,
                                                           @RequestParam("partName")String partName){
        Map<String,Object> result = new HashMap<>();
        List<Integer> bedNumList = new ArrayList<>();
        if (partName.equals("主身布")){
            bedNumList = tailorService.getBedNumbersByOrderNamePartNameTailorType(orderName, null, 0);
        } else {
            bedNumList = tailorService.getBedNumbersByOrderNamePartNameTailorType(orderName, partName, 1);
        }
        result.put("bedNumList",bedNumList);
        return result;
    }


    @RequestMapping(value = "/getrecentweekcutinfo", method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getRecentWeekCutInfo(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<TailorMatch> tailorMatchList = tailorService.getRecentWeekCutInfo();
        Set<String> orderSet = new HashSet<>();
        for (TailorMatch tailorMatch : tailorMatchList){
            orderSet.add(tailorMatch.getOrderName());
        }
        List<String> orderNameList = new ArrayList<>(orderSet);
        // 订单数
        List<OrderClothes> orderClothesList = orderClothesService.getOrderListSummaryByColor(orderNameList);
        // 主身布
        List<TailorMatch> mainTailorList = tailorService.getMainTailorMatchDataByInfo(orderNameList);
        // 配料
        List<TailorMatch> otherTailorList = tailorService.getOtherTailorMatchDataByInfo(orderNameList);
        for (TailorMatch tailorMatch : tailorMatchList){
            for (OrderClothes orderClothes : orderClothesList){
                if (orderClothes.getOrderName().equals(tailorMatch.getOrderName()) && orderClothes.getColorName().equals(tailorMatch.getColorName())){
                    tailorMatch.setOrderCount(orderClothes.getOrderSum());
                }
            }
            for (TailorMatch mainTailor : mainTailorList){
                if (mainTailor.getOrderName().equals(tailorMatch.getOrderName()) && mainTailor.getColorName().equals(tailorMatch.getColorName())){
                    tailorMatch.setMainCount(mainTailor.getMatchCount());
                    tailorMatch.setMatchCount(mainTailor.getMatchCount());
                }
            }
            for (TailorMatch otherTailor : otherTailorList){
                if (otherTailor.getOrderName().equals(tailorMatch.getOrderName()) && otherTailor.getColorName().equals(tailorMatch.getColorName()) && !otherTailor.getPartName().startsWith("士啤")){
                    if (tailorMatch.getOtherCount().equals(0)){
                        tailorMatch.setOtherCount(otherTailor.getMatchCount());
                        tailorMatch.setMatchCount(tailorMatch.getMainCount() < tailorMatch.getOtherCount() ? tailorMatch.getMainCount() : tailorMatch.getOtherCount());
                    } else {
                        if (tailorMatch.getMatchCount() > tailorMatch.getOtherCount()){
                            tailorMatch.setOtherCount(otherTailor.getMatchCount());
                            tailorMatch.setMatchCount(otherTailor.getMatchCount());
                        }
                    }
                }
            }
        }
        map.put("tailorMatchList", tailorMatchList);
        return map;
    }

    @RequestMapping(value = "/monthtailorreportgroupbybednumber", method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> monthTailorReportGroupByBedNumber(@RequestParam("from")String from,
                                                                @RequestParam("to")String to,
                                                                @RequestParam(value = "orderName", required = false)String orderName,
                                                                @RequestParam(value = "groupName", required = false)String groupName){
        Map<String,Object> map = new HashMap<>();
        List<Tailor> tailorList = tailorService.monthTailorReportGroupByBedNumber(from, to, orderName, groupName);
        for (Tailor tailor : tailorList){
            tailor.setPartName("主身布");
        }
        List<Tailor> otherTailorList = tailorService.otherMonthTailorReportGroupByBedNumber(from, to, orderName, groupName);
        tailorList.addAll(otherTailorList);
        map.put("tailorList",tailorList);
        return map;
    }

    @RequestMapping(value = "/miniupdatetailorlayercountbyinfo", method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> miniUpdateTailorLayerCountByInfo(@RequestParam("orderName")String orderName,
                                                                @RequestParam("bedNumber")Integer bedNumber,
                                                                @RequestParam("packageNumber")Integer packageNumber,
                                                                @RequestParam("layerCount")Integer layerCount){
        Map<String,Object> map = new HashMap<>();
        int res1 = tailorService.updateTailorLayerCountByInfo(orderName, bedNumber, packageNumber, layerCount);
        int res2 = finishTailorService.updateFinishTailorLayerCountByInfo(orderName, bedNumber, packageNumber, layerCount);
        map.put("result", res1 + res2);
        return map;
    }

}
