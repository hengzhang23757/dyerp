package com.example.erp01.action;

import com.example.erp01.model.*;
import com.example.erp01.service.*;
import com.example.erp01.service.impl.ManufactureOrderServiceImpl;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;
import java.util.stream.Collectors;

@Controller
@RequestMapping(value = "erp")
public class FabricAccessoryMonthCheckController {

    @Autowired
    private OrderClothesService orderClothesService;
    @Autowired
    private FabricAccessoryMonthCheckService fabricAccessoryMonthCheckService;
    @Autowired
    private CheckMonthService checkMonthService;
    @Autowired
    private ManufactureFabricService manufactureFabricService;
    @Autowired
    private FabricDetailService fabricDetailService;
    @Autowired
    private ManufactureAccessoryService manufactureAccessoryService;
    @Autowired
    private AccessoryInStoreService accessoryInStoreService;
    @Autowired
    private AccessoryPreStoreService accessoryPreStoreService;
    @Autowired
    private FabricOutRecordService fabricOutRecordService;
    @Autowired
    private SupplierInfoService supplierInfoService;
    @Autowired
    private ManufactureOrderService manufactureOrderService;

    @RequestMapping("/fabricAccessoryMonthCheckStart")
    public String fabricAccessoryMonthCheckStart(){
        return "checkModule/fabricAccessoryMonthCheck";
    }


    @RequestMapping(value = "/getfabricaccessorymonthcheckbyinfo", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> searchFabricAccessoryLeakByInfo(@RequestParam(value = "from", required = false)String from,
                                                               @RequestParam(value = "to",required = false)String to,
                                                               @RequestParam(value = "customerName", required = false)String customerName,
                                                               @RequestParam(value = "season", required = false)String season,
                                                               @RequestParam(value = "orderName", required = false)String orderName,
                                                               @RequestParam(value = "type")String type,
                                                               @RequestParam(value = "checkState", required = false)Integer checkState,
                                                               @RequestParam(value = "supplier", required = false)String supplier){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> orderNameList = new ArrayList<>();
        if (orderName != null){
            orderNameList.add(orderName);
        }
        else {
            if ((season != null && !"".equals(season)) || (customerName != null && !"".equals(customerName))){
                orderNameList = manufactureOrderService.getOrderNameByCustomerSeason(season, customerName);
                if (CollectionUtils.isEmpty(orderNameList)){
                    map.put("data", new ArrayList<>());
                    return map;
                }
            }
        }
        Map<String, Object> searchParam = new HashMap<>();
        searchParam.put("orderNameList", orderNameList);
        searchParam.put("from", from);
        searchParam.put("to", to);
        searchParam.put("supplier", supplier);
        searchParam.put("checkState", checkState);
        if ("面料".equals(type)){
            List<Integer> fabricIdList = new ArrayList<>();
            List<FabricMonthCheck> fabricDetailList = fabricAccessoryMonthCheckService.getFabricDetailCheckByInfo(searchParam);
            for (FabricMonthCheck fabricMonthCheck : fabricDetailList){
                fabricIdList.add(fabricMonthCheck.getFabricID());
                fabricMonthCheck.setReturnMoney(fabricMonthCheck.getPrice() * fabricMonthCheck.getWeight());
            }
            List<ManufactureFabric> manufactureFabricList = manufactureFabricService.getManufactureFabricByFabricIdList(fabricIdList);
            List<FabricDetail> fabricDetailList1 = fabricDetailService.getFabricDetailByFabricIDList(fabricIdList);
            for (ManufactureFabric manufactureFabric : manufactureFabricList){
                for (FabricDetail fabricDetail : fabricDetailList1){
                    if (manufactureFabric.getFabricID().equals(fabricDetail.getFabricID())){
                        manufactureFabric.setFabricReturn(manufactureFabric.getFabricReturn() + fabricDetail.getWeight());
                        if (fabricDetail.getCheckNumber() != null){
                            manufactureFabric.setFabricReturnCheckCount(manufactureFabric.getFabricReturnCheckCount() + fabricDetail.getWeight());
                        } else {
                            manufactureFabric.setFabricReturnUnCheck(manufactureFabric.getFabricReturnUnCheck() + fabricDetail.getWeight());
                        }
                    }
                }
            }
            List<String> onList = manufactureFabricList.stream().map(ManufactureFabric::getOrderName).distinct().collect(Collectors.toList());
            List<ManufactureOrder> manufactureOrderList = manufactureOrderService.getManufactureOrderByOrderNameList(onList);
            for (ManufactureFabric manufactureFabric : manufactureFabricList){
                for (ManufactureOrder manufactureOrder : manufactureOrderList){
                    if (manufactureFabric.getOrderName().equals(manufactureOrder.getOrderName())){
                        manufactureFabric.setCustomerName(manufactureOrder.getCustomerName());
                        manufactureFabric.setSeason(manufactureOrder.getSeason());
                    }
                }
            }
            map.put("data", manufactureFabricList);
            return map;
        } else {
            List<Integer> accessoryIDList = new ArrayList<>();
            List<AccessoryInStore> accessoryInStoreList = fabricAccessoryMonthCheckService.getAccessoryInStoreByInfo(searchParam);
            for (AccessoryInStore accessoryInStore : accessoryInStoreList){
                accessoryIDList.add(accessoryInStore.getAccessoryID());
                accessoryInStore.setReturnMoney(accessoryInStore.getAccountCount() * accessoryInStore.getPrice());
            }
            List<ManufactureAccessory> manufactureAccessoryList = manufactureAccessoryService.getManufactureAccessoryByIdList(accessoryIDList);
            List<AccessoryInStore> accessoryInStoreList1 = accessoryInStoreService.getAccessoryInStoreByIdList(accessoryIDList);
            for (ManufactureAccessory manufactureAccessory: manufactureAccessoryList){
                for (AccessoryInStore accessoryInStore : accessoryInStoreList1){
                    if (manufactureAccessory.getAccessoryID().equals(accessoryInStore.getAccessoryID())){
                        manufactureAccessory.setAccessoryReturnCount(manufactureAccessory.getAccessoryReturnCount() + accessoryInStore.getInStoreCount());
                        manufactureAccessory.setAccessoryAccountCount(manufactureAccessory.getAccessoryAccountCount() + accessoryInStore.getAccountCount());
                        if (accessoryInStore.getCheckNumber() != null){
                            manufactureAccessory.setAccessoryReturnCheckCount(manufactureAccessory.getAccessoryReturnCheckCount() + accessoryInStore.getAccountCount());
                        } else {
                            manufactureAccessory.setAccessoryReturnUnCheck(manufactureAccessory.getAccessoryReturnUnCheck() + accessoryInStore.getAccountCount());
                        }
                    }
                }
            }
            List<String> onList = manufactureAccessoryList.stream().map(ManufactureAccessory::getOrderName).distinct().collect(Collectors.toList());
            List<ManufactureOrder> manufactureOrderList = manufactureOrderService.getManufactureOrderByOrderNameList(onList);
            for (ManufactureAccessory manufactureAccessory: manufactureAccessoryList){
                for (ManufactureOrder manufactureOrder : manufactureOrderList){
                    if (manufactureAccessory.getOrderName().equals(manufactureOrder.getOrderName())){
                        manufactureAccessory.setCustomerName(manufactureOrder.getCustomerName());
                        manufactureAccessory.setSeason(manufactureOrder.getSeason());
                    }
                }
            }
            map.put("data", manufactureAccessoryList);
            return map;
        }

    }

    @RequestMapping(value = "/getfabricaccessorymonthcheckrecordbyinfo", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> searchFabricAccessoryMonthCheckRecordByInfo(@RequestParam(value = "from", required = false)String from,
                                                                           @RequestParam(value = "to",required = false)String to,
                                                                           @RequestParam(value = "customerName", required = false)String customerName,
                                                                           @RequestParam(value = "season", required = false)String season,
                                                                           @RequestParam(value = "orderName", required = false)String orderName,
                                                                           @RequestParam(value = "type")String type,
                                                                           @RequestParam(value = "checkState", required = false)Integer checkState,
                                                                           @RequestParam(value = "supplier", required = false)String supplier){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> orderNameList = new ArrayList<>();
        if (orderName != null){
            orderNameList.add(orderName);
        }
        else {
            if ((season != null && !"".equals(season)) || (customerName != null && !"".equals(customerName))){
                orderNameList = manufactureOrderService.getOrderNameByCustomerSeason(season, customerName);
                if (CollectionUtils.isEmpty(orderNameList)){
                    map.put("data", new ArrayList<>());
                    return map;
                }
            }
        }
        List<Integer> idList = new ArrayList<>();
        Map<String, Object> searchParam = new HashMap<>();
        searchParam.put("orderNameList", orderNameList);
        searchParam.put("from", from);
        searchParam.put("to", to);
        searchParam.put("supplier", supplier);
        searchParam.put("checkState", checkState);
        searchParam.put("operateType", "退面料厂");
        if ("面料".equals(type)){
            List<FabricMonthCheck> fabricMonthCheckList = new ArrayList<>();
            List<FabricMonthCheck> fabricDetailList = fabricAccessoryMonthCheckService.getFabricDetailCheckByInfo(searchParam);
            for (FabricMonthCheck fabricMonthCheck : fabricDetailList){
                fabricMonthCheck.setReturnMoney(fabricMonthCheck.getPrice() * fabricMonthCheck.getWeight());
                fabricMonthCheck.setSourceType("detail");
                fabricMonthCheckList.add(fabricMonthCheck);
                idList.add(fabricMonthCheck.getFabricID());
            }
            if (!idList.isEmpty()){
                Map<String, Object> param0 = new HashMap<>();
                param0.put("fabricIDList", idList);
                List<AddFeeVo> fabricAddFeeList = manufactureFabricService.getFeeVoByInfo(param0);
                for (FabricMonthCheck fabricMonthCheck : fabricDetailList){
                    for (AddFeeVo addFeeVo : fabricAddFeeList){
                        if (fabricMonthCheck.getFabricID().equals(addFeeVo.getFabricID())
                                && addFeeVo.getFeeCount().equals(0)){
                            fabricMonthCheck.setAddFee(addFeeVo.getAddFee());
                            fabricMonthCheck.setAddRemark(addFeeVo.getAddRemark());
                            addFeeVo.setFeeCount(1);
                        }
                    }
                }
            }
            List<FabricMonthCheck> fabricOutRecordList = fabricAccessoryMonthCheckService.getFabricOutRecordCheckByInfo(searchParam);
            for (FabricMonthCheck fabricMonthCheck : fabricOutRecordList){
                fabricMonthCheck.setReturnMoney(- fabricMonthCheck.getPrice() * fabricMonthCheck.getWeight());
                fabricMonthCheck.setSourceType("out");
                fabricMonthCheckList.add(fabricMonthCheck);
            }
            List<String> onList = fabricMonthCheckList.stream().map(FabricMonthCheck::getOrderName).distinct().collect(Collectors.toList());
            List<ManufactureOrder> manufactureOrderList = manufactureOrderService.getManufactureOrderByOrderNameList(onList);
            for (FabricMonthCheck fabricMonthCheck : fabricMonthCheckList){
                for (ManufactureOrder manufactureOrder : manufactureOrderList){
                    if (fabricMonthCheck.getOrderName().equals(manufactureOrder.getOrderName())){
                        fabricMonthCheck.setCustomerName(manufactureOrder.getCustomerName());
                        fabricMonthCheck.setSeason(manufactureOrder.getSeason());
                    }
                }
            }
            map.put("data", fabricMonthCheckList);
            return map;
        }
        else {
            List<AccessoryInStore> accessoryInStoreList = fabricAccessoryMonthCheckService.getAccessoryInStoreByInfo(searchParam);
            List<String> onList = accessoryInStoreList.stream().map(AccessoryInStore::getOrderName).collect(Collectors.toList());
            List<ManufactureOrder> manufactureOrderList = manufactureOrderService.getManufactureOrderByOrderNameList(onList);
            for (AccessoryInStore accessoryInStore : accessoryInStoreList){
                if ("共用".equals(accessoryInStore.getInStoreType())){
                    accessoryInStore.setReturnMoney(accessoryInStore.getAccountCount() * accessoryInStore.getPrice());
                    accessoryInStore.setPayMoney(0f);
                } else {
                    idList.add(accessoryInStore.getAccessoryID());
                    accessoryInStore.setReturnMoney(accessoryInStore.getAccountCount() * accessoryInStore.getPrice());
                    accessoryInStore.setPayMoney(accessoryInStore.getAccountCount() * accessoryInStore.getPrice());
                }
            }
            if (!idList.isEmpty()){
                Map<String, Object> param0 = new HashMap<>();
                param0.put("accessoryIDList", idList);
                List<AddFeeVo> accessoryAddFeeList = manufactureAccessoryService.getFeeVoByInfo(param0);
                for (AccessoryInStore accessoryInStore : accessoryInStoreList){
                    for (AddFeeVo addFeeVo : accessoryAddFeeList){
                        if (accessoryInStore.getAccessoryID().equals(addFeeVo.getAccessoryID())
                                && addFeeVo.getFeeCount().equals(0)){
                            accessoryInStore.setAddFee(addFeeVo.getAddFee());
                            accessoryInStore.setAddRemark(addFeeVo.getAddRemark());
                            addFeeVo.setFeeCount(1);
                        }
                    }
                    for (ManufactureOrder manufactureOrder : manufactureOrderList){
                        if (accessoryInStore.getOrderName().equals(manufactureOrder.getOrderName())){
                            accessoryInStore.setCustomerName(manufactureOrder.getCustomerName());
                            accessoryInStore.setSeason(manufactureOrder.getSeason());
                        }
                    }
                }
            }
            map.put("data", accessoryInStoreList);
            return map;
        }
    }

    @RequestMapping(value = "/getfabricreadycheckdata", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getFabricReadyCheckData(@RequestParam("fabricID")Integer fabricID,
                                                       @RequestParam("checkState")Integer checkState){
        Map<String, Object> map = new LinkedHashMap<>();
        List<Integer> fabricIdList = new ArrayList<>();
        fabricIdList.add(fabricID);
        List<FabricDetail> fabricDetailList = fabricDetailService.getFabricDetailByFabricIDList(fabricIdList);
        ManufactureFabric manufactureFabric = manufactureFabricService.getManufactureFabricByID(fabricID);
        List<FabricDetail> fabricDetailList1 = new ArrayList<>();
        for (FabricDetail fabricDetail : fabricDetailList){
            fabricDetail.setPrice(manufactureFabric.getPrice() == null ? 0f : manufactureFabric.getPrice());
            fabricDetail.setReturnMoney(fabricDetail.getPrice() * fabricDetail.getWeight());
            if (checkState.equals(0) && fabricDetail.getCheckNumber() == null){
                fabricDetailList1.add(fabricDetail);
            }
        }
        if (checkState.equals(0)){
            map.put("fabricDetailList", fabricDetailList1);
        } else {
            map.put("fabricDetailList", fabricDetailList);
        }
        return map;
    }

    @RequestMapping(value = "/getaccessoryreadycheckdata", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getAccessoryReadyCheckData(@RequestParam("accessoryID")Integer accessoryID,
                                                          @RequestParam("checkState")Integer checkState){
        Map<String, Object> map = new LinkedHashMap<>();
        List<Integer> accessoryIDList = new ArrayList<>();
        accessoryIDList.add(accessoryID);
        List<AccessoryInStore> accessoryInStoreList = accessoryInStoreService.getAccessoryInStoreByIdList(accessoryIDList);
        ManufactureAccessory manufactureAccessory = manufactureAccessoryService.getManufactureAccessoryByID(accessoryID);
        List<AccessoryInStore> accessoryInStoreList1 = new ArrayList<>();
        for (AccessoryInStore accessoryInStore : accessoryInStoreList){
            accessoryInStore.setPrice(manufactureAccessory.getPrice() == null ? 0f : manufactureAccessory.getPrice());
            if (accessoryInStore.getInStoreType().equals("共用")){
                accessoryInStore.setReturnMoney(accessoryInStore.getPrice() * accessoryInStore.getAccountCount());
                accessoryInStore.setPayMoney(0f);
            } else {
                accessoryInStore.setReturnMoney(accessoryInStore.getPrice() * accessoryInStore.getAccountCount());
                accessoryInStore.setPayMoney(accessoryInStore.getPrice() * accessoryInStore.getAccountCount());
            }
            if (checkState.equals(0) && accessoryInStore.getCheckNumber() == null){
                accessoryInStoreList1.add(accessoryInStore);
            }
        }
        if (checkState.equals(0)){
            map.put("accessoryInStoreList", accessoryInStoreList1);
        } else {
            map.put("accessoryInStoreList", accessoryInStoreList);
        }
        return map;
    }

    @RequestMapping(value = "/getallfabricsupplier", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getAllFabricSupplier(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> supplierList = fabricAccessoryMonthCheckService.getAllFabricSupplier();
        List<String> fabricSupplierList = supplierInfoService.getAllSupplierNameByInfo("面料");
        supplierList.addAll(fabricSupplierList);
        supplierList = removeDuplicate(supplierList);
        map.put("supplierList", supplierList);
        return map;
    }

    @RequestMapping(value = "/getonlyfabricsupplier", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getOnlyFabricSupplier(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> fabricSupplierList = supplierInfoService.getAllSupplierNameByInfo("面料");
        map.put("supplierList", fabricSupplierList);
        return map;
    }

    @RequestMapping(value = "/getallaccessorysupplier", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getAllAccessorySupplier(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> supplierList = fabricAccessoryMonthCheckService.getAllAccessorySupplier();
        List<String> accessorySupplierList = supplierInfoService.getAllSupplierNameByInfo("辅料");
        supplierList.addAll(accessorySupplierList);
        supplierList = removeDuplicate(supplierList);
        map.put("supplierList", supplierList);
        return map;
    }

    @RequestMapping(value = "/getonlyaccessorysupplier", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getOnlyAccessorySupplier(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> accessorySupplierList = supplierInfoService.getAllSupplierNameByInfo("辅料");
        map.put("supplierList", accessorySupplierList);
        return map;
    }

    @RequestMapping(value = "/updatefabricdetailincheck", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> updateFabricDetailInCheck(@RequestParam("fabricDetailJson")String fabricDetailJson,
                                                         @RequestParam("fabricOutRecordJson")String fabricOutRecordJson,
                                                         @RequestParam("checkMonth")String checkMonth){
        Map<String, Object> map = new LinkedHashMap<>();
        Map<String, Object> param = new HashMap<>();
        param.put("checkMonth", checkMonth);
        CheckMonth fixedMonth = checkMonthService.getCheckMonthByInfo(param);
        if (fixedMonth != null){
            map.put("result", 3);
            return map;
        }
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        List<FabricDetail> fabricDetailList = gson.fromJson(fabricDetailJson,new TypeToken<List<FabricDetail>>(){}.getType());
        List<FabricOutRecord> fabricOutRecordList = gson.fromJson(fabricOutRecordJson,new TypeToken<List<FabricOutRecord>>(){}.getType());
        List<Integer> idList = new ArrayList<>();
        for (FabricDetail fabricDetail : fabricDetailList){
            if (fabricDetail.getAddFee() > 0){
                idList.add(fabricDetail.getFabricID());
            }
        }
        manufactureFabricService.changeFeeStateBatch(idList, 1);
        int res1 = fabricAccessoryMonthCheckService.updateCheckDetailInCheck(fabricDetailList);
        int res2 = fabricAccessoryMonthCheckService.updateCheckOutRecordInCheck(fabricOutRecordList);
        map.put("result", res1 + res2);
        return map;
    }

    @RequestMapping(value = "/updateaccessoryinstoreincheck", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> updateAccessoryInStoreIncheck(@RequestParam("accessoryInStoreJson")String accessoryInStoreJson,
                                                             @RequestParam("checkMonth")String checkMonth){
        Map<String, Object> map = new LinkedHashMap<>();
        Map<String, Object> param = new HashMap<>();
        param.put("checkMonth", checkMonth);
        CheckMonth fixedMonth = checkMonthService.getCheckMonthByInfo(param);
        if (fixedMonth != null){
            map.put("result", 3);
            return map;
        }
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        List<AccessoryInStore> accessoryInStoreList = gson.fromJson(accessoryInStoreJson,new TypeToken<List<AccessoryInStore>>(){}.getType());
        List<Integer> idList = new ArrayList<>();
        for (AccessoryInStore accessoryInStore : accessoryInStoreList){
            if (accessoryInStore.getAddFee() > 0){
                idList.add(accessoryInStore.getAccessoryID());
            }
        }
        manufactureAccessoryService.changeFeeStateBatch(idList, 1);
        int res = fabricAccessoryMonthCheckService.updateAccessoryInStoreInCheck(accessoryInStoreList);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/quitfabriccheckbatch", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> quitFabricCheckBatch(@RequestParam("fabricDetailIDList")List<Integer> fabricDetailIDList){
        Map<String, Object> map = new LinkedHashMap<>();
        List<Integer> fabricIDList = fabricDetailService.getFabricIdListByAddFee(fabricDetailIDList);
        manufactureFabricService.changeFeeStateBatch(fabricIDList, 0);
        int res = fabricAccessoryMonthCheckService.quitFabricDetailBatch(fabricDetailIDList);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/quitaccessoryinstorebatch", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> quitAccessoryInStoreBatch(@RequestParam("idList")List<Integer> idList){
        Map<String, Object> map = new LinkedHashMap<>();
        List<Integer> accessoryIdList = accessoryInStoreService.getAccessoryIdListByAddFee(idList);
        manufactureAccessoryService.changeFeeStateBatch(accessoryIdList, 0);
        int res = fabricAccessoryMonthCheckService.quitAccessoryInStoreBatch(idList);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/getaccessorypremonthcheckrecordbyinfo", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getFabricReadyCheckData(@RequestParam(value = "from", required = false)String from,
                                                       @RequestParam(value = "to",required = false)String to,
                                                       @RequestParam(value = "customerName", required = false)String customerName,
                                                       @RequestParam(value = "season", required = false)String season,
                                                       @RequestParam(value = "orderName", required = false)String orderName,
                                                       @RequestParam(value = "type")String type,
                                                       @RequestParam(value = "checkState", required = false)Integer checkState,
                                                       @RequestParam(value = "supplier", required = false)String supplier){
        Map<String, Object> map = new LinkedHashMap<>();
        Map<String, Object> param = new HashMap<>();
        param.put("from", from);
        param.put("to", to);
        param.put("supplier", supplier);
        param.put("operateType", "in");
        param.put("checkState", checkState);
        List<AccessoryPreStore> accessoryPreStoreList = accessoryPreStoreService.getAccessoryPreStoreByInfo(param);
        for (AccessoryPreStore accessoryPreStore : accessoryPreStoreList){
            accessoryPreStore.setReturnMoney(accessoryPreStore.getInStoreCount() * accessoryPreStore.getPrice());
        }
        map.put("data", accessoryPreStoreList);
        return map;
    }

    public static List removeDuplicate(List list) {
        HashSet h = new HashSet(list);
        list.clear();
        list.addAll(h);
        return list;
    }
}
