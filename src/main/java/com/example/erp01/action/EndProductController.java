package com.example.erp01.action;

import com.example.erp01.model.*;
import com.example.erp01.service.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Configuration      //1.主要用于标记配置类，兼备Component的效果。
@EnableScheduling   // 2.开启定时任务
@Controller
@RequestMapping(value = "erp")
public class EndProductController {

    @Autowired
    private EndProductService endProductService;
    @Autowired
    private TailorService tailorService;
    @Autowired
    private OrderClothesService orderClothesService;
    @Autowired
    private ManufactureOrderService manufactureOrderService;
    @Autowired
    private EndProductPayService endProductPayService;
    @Autowired
    private ClothesPriceService clothesPriceService;

    @RequestMapping("/endProductAccountStart")
    public String endProductAccountStart(){
        return "endProduct/endProductAccount";
    }

    @RequestMapping("/endProductStorageStart")
    public String endProductStorageStart(){
        return "endProduct/endProductStorage";
    }

    @RequestMapping(value = "/getendproductpreinstoredata", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getEndProductPreInStoreData(@RequestParam("orderName")String orderName,
                                                           @RequestParam("endType")String endType){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> colorNameList = orderClothesService.getOrderColorNamesByOrder(orderName);
        List<String> sizeNameList = orderClothesService.getOrderSizeNamesByOrder(orderName);
        List<CutQueryLeak> orderInfoList = orderClothesService.getOrderInfoByName(orderName);
        List<CutQueryLeak> tailorInfoList = tailorService.getTailorInfoByOrderPartType(orderName, null, 0, null, null);
        List<EndProduct> endProductList = endProductService.getEndProductInStoreInfo(orderName, endType);
        for (CutQueryLeak cutQueryLeak1 : orderInfoList){
            for (CutQueryLeak cutQueryLeak2 : tailorInfoList){
                if (cutQueryLeak2.getColorName().equals(cutQueryLeak1.getColorName()) && cutQueryLeak2.getSizeName().equals(cutQueryLeak1.getSizeName())){
                    cutQueryLeak1.setWellCount(cutQueryLeak2.getWellCount());
                }
            }
            for (EndProduct endProduct : endProductList){
                if (endProduct.getColorName().equals(cutQueryLeak1.getColorName()) && endProduct.getSizeName().equals(cutQueryLeak1.getSizeName())){
                    cutQueryLeak1.setFinishCount(cutQueryLeak1.getFinishCount() + endProduct.getInStoreCount());
                }
            }
        }
        map.put("colorNameList", colorNameList);
        map.put("sizeNameList", sizeNameList);
        map.put("orderInfoList", orderInfoList);
        return map;
    }


    @RequestMapping(value = "/getendproductalltyperecord", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getEndProductAllTypeRecord(@RequestParam(value = "orderName", required = false)String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<EndProduct> rightEndProductList = endProductService.getEndProductStorageSummary("正品", orderName);
        List<EndProduct> defectiveEndProductList = endProductService.getDefectiveEndProductSummary(orderName);
        List<EndProduct> endProductList = new ArrayList<>();
        for (EndProduct endProduct : rightEndProductList){
            for (EndProduct defectiveEndProduct : defectiveEndProductList){
                if (endProduct.getOrderName().equals(defectiveEndProduct.getOrderName())){
                    endProduct.setDefectiveInStore(defectiveEndProduct.getInStoreCount());
                    endProduct.setDefectiveStorage(defectiveEndProduct.getStorageCount());
                    endProduct.setDefectiveOutStore(defectiveEndProduct.getOutStoreCount());
                }
            }
            endProductList.add(endProduct);
        }
        for (EndProduct defectiveEndProduct : defectiveEndProductList){
            boolean flag = false;
            for (EndProduct endProduct : rightEndProductList){
                if (endProduct.getOrderName().equals(defectiveEndProduct.getOrderName())){
                    flag = true;
                    break;
                }
            }
            if (!flag){
                defectiveEndProduct.setDefectiveInStore(defectiveEndProduct.getInStoreCount());
                defectiveEndProduct.setDefectiveStorage(defectiveEndProduct.getStorageCount());
                defectiveEndProduct.setDefectiveOutStore(defectiveEndProduct.getOutStoreCount());
                defectiveEndProduct.setInStoreCount(0);
                defectiveEndProduct.setStorageCount(0);
                defectiveEndProduct.setOutStoreCount(0);
                endProductList.add(defectiveEndProduct);
            }
        }
        map.put("endProductList", endProductList);
        return map;
    }

    @RequestMapping(value = "/addendproductinbatch",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> addAccessoryBatch(@RequestParam("endProductInStoreJson")String endProductInStoreJson,
                                                 @RequestParam("orderName")String orderName,
                                                 @RequestParam("endType")String endType){
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        ManufactureOrder manufactureOrder = manufactureOrderService.getManufactureOrderByOrderName(orderName);
        List<EndProduct> endProductInList = gson.fromJson(endProductInStoreJson,new TypeToken<List<EndProduct>>(){}.getType());
        List<EndProduct> endProductStorageList = endProductService.getEndProductStorageInfo(orderName, endType);
        List<EndProduct> updateStorageList = new ArrayList<>();
        List<EndProduct> initStorageList = new ArrayList<>();
        for (EndProduct endProductIn : endProductInList){
            endProductIn.setCustomerName(manufactureOrder.getCustomerName());
            boolean flag = false;
            for (EndProduct endProductStorage : endProductStorageList){
                if (endProductIn.getColorName().equals(endProductStorage.getColorName()) && endProductIn.getSizeName().equals(endProductStorage.getSizeName())){
                    flag = true;
                    endProductStorage.setInStoreCount(endProductStorage.getInStoreCount() + endProductIn.getInStoreCount());
                    endProductStorage.setStorageCount(endProductStorage.getStorageCount() + endProductIn.getInStoreCount());
                    updateStorageList.add(endProductStorage);
                }
            }
            if (!flag){
                EndProduct endProduct = new EndProduct();
                endProduct.setOrderName(endProductIn.getOrderName());
                endProduct.setClothesVersionNumber(endProductIn.getClothesVersionNumber());
                endProduct.setColorName(endProductIn.getColorName());
                endProduct.setSizeName(endProductIn.getSizeName());
                endProduct.setInStoreCount(endProductIn.getInStoreCount());
                endProduct.setStorageCount(endProductIn.getInStoreCount());
                endProduct.setOperateType("storage");
                endProduct.setCustomerName(manufactureOrder.getCustomerName());
                endProduct.setEndType(endType);
                initStorageList.add(endProduct);
            }
        }
        endProductInList.addAll(initStorageList);
        // 该更新的更新 该添加的添加
        int res1 = endProductService.addEndProductBatch(endProductInList);
        int res2 = endProductService.updateEndProductBatch(updateStorageList);
        if (res1 == 0 && res2 == 0){
            map.put("result", 0);
        } else {
            map.put("result", 1);
        }
        return map;
    }

    @RequestMapping(value = "/adddefectiveendproductinbatch",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> addDefectiveEndProductInBatch(@RequestParam("endProductInStoreJson")String endProductInStoreJson){
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        List<EndProduct> endProductInList = gson.fromJson(endProductInStoreJson,new TypeToken<List<EndProduct>>(){}.getType());
        int res1 = endProductService.addEndProductBatch(endProductInList);
        map.put("result", res1);
        return map;

    }



    @RequestMapping(value = "/getendproductinstorebyorder", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getEndProductInStoreByOrder(@RequestParam("orderName")String orderName,
                                                           @RequestParam("endType")String endType){
        Map<String, Object> map = new LinkedHashMap<>();
        List<EndProduct> endProductList = endProductService.getEndProductInStoreInfo(orderName, endType);
        map.put("data", endProductList);
        map.put("count", endProductList.size());
        map.put("code",0);
        return map;
    }

    @RequestMapping(value = "/getendproductstoragebyorder", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getEndProductStorageByOrder(@RequestParam("orderName")String orderName,
                                                           @RequestParam("endType")String endType){
        Map<String, Object> map = new LinkedHashMap<>();
        List<EndProduct> endProductList = endProductService.getEndProductStorageInfo(orderName, endType);
        map.put("data", endProductList);
        map.put("count", endProductList.size());
        map.put("code",0);
        return map;
    }

    @RequestMapping(value = "/getendproductoutstorebyorder", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getEndProductOutStoreByOrder(@RequestParam("orderName")String orderName,
                                                            @RequestParam("endType")String endType){
        Map<String, Object> map = new LinkedHashMap<>();
        List<EndProduct> endProductList = endProductService.getEndProductOutStoreInfo(orderName, endType);
        map.put("data", endProductList);
        map.put("count", endProductList.size());
        map.put("code",0);
        return map;
    }

    @RequestMapping(value = "/getdefectiveendproductinstorebyorder", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getDefectiveEndProductInStoreByOrder(@RequestParam("orderName")String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<EndProduct> endProductList = endProductService.getDefectiveEndProductDetailInByOrder(orderName);
        map.put("data", endProductList);
        map.put("count", endProductList.size());
        map.put("code",0);
        return map;
    }

    @RequestMapping(value = "/getdefectiveendproductstoragebyorder", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getDefectiveEndProductStorageByOrder(@RequestParam("orderName")String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<EndProduct> endProductList = endProductService.getDefectiveEndProductDetailStorageByOrder(orderName);
        map.put("data", endProductList);
        map.put("count", endProductList.size());
        map.put("code",0);
        return map;
    }

    @RequestMapping(value = "/getdefectiveendproductoutstorebyorder", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getDefectiveEndProductOutStoreByOrder(@RequestParam("orderName")String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<EndProduct> endProductList = endProductService.getDefectiveEndProductDetailOutByOrder(orderName);
        map.put("data", endProductList);
        map.put("count", endProductList.size());
        map.put("code",0);
        return map;
    }

    @RequestMapping(value = "/deleteendproductinbatch", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> deleteEndProductInBatch(@RequestParam("idList")List<Integer> idList,
                                                       @RequestParam("orderName")String orderName,
                                                       @RequestParam("endType")String endType){
        Map<String, Object> map = new LinkedHashMap<>();
        List<EndProduct> endProductStorageList = endProductService.getEndProductStorageInfo(orderName, endType);
        List<EndProduct> endProductInList = endProductService.getEndProductByIdList(idList);
        for (EndProduct endProductStorage : endProductStorageList){
            for (EndProduct endProductIn : endProductInList){
                if (endProductIn.getColorName().equals(endProductStorage.getColorName()) && endProductIn.getSizeName().equals(endProductStorage.getSizeName())){
                    endProductStorage.setInStoreCount(endProductStorage.getInStoreCount() - endProductIn.getInStoreCount());
                    endProductStorage.setStorageCount(endProductStorage.getStorageCount() - endProductIn.getInStoreCount());
                    if (endProductStorage.getStorageCount() < 0){
                        map.put("msg", endProductIn.getColorName()+"-"+endProductIn.getSizeName() + "删除数量大于库存数量,不能删除");
                        return map;
                    }
                }
            }
        }
        int res1 = endProductService.updateEndProductBatch(endProductStorageList);
        int res2 = endProductService.deleteEndProductBatch(idList);
        if (res1 == 0 && res2 == 0){
            map.put("result", 0);
        } else {
            map.put("result", 1);
        }
        return map;
    }

    @RequestMapping(value = "/deleteendproductoutbatch", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> deleteEndProductOutBatch(@RequestParam("idList")List<Integer> idList,
                                                        @RequestParam("orderName")String orderName,
                                                        @RequestParam("endType")String endType){
        Map<String, Object> map = new LinkedHashMap<>();
        List<EndProduct> endProductStorageList = endProductService.getEndProductStorageInfo(orderName, endType);
        List<EndProduct> endProductOutList = endProductService.getEndProductByIdList(idList);
        for (EndProduct endProductStorage : endProductStorageList){
            for (EndProduct endProductOut : endProductOutList){
                if (endProductOut.getColorName().equals(endProductStorage.getColorName()) && endProductOut.getSizeName().equals(endProductStorage.getSizeName())){
                    endProductStorage.setOutStoreCount(endProductStorage.getOutStoreCount() - endProductOut.getOutStoreCount());
                    endProductStorage.setStorageCount(endProductStorage.getStorageCount() + endProductOut.getOutStoreCount());
                }
            }
        }
        int res1 = endProductService.updateEndProductBatch(endProductStorageList);
        int res2 = endProductService.deleteEndProductBatch(idList);
        if (res1 == 0 && res2 == 0){
            map.put("result", 0);
        } else {
            map.put("result", 1);
        }
        return map;
    }

    @RequestMapping(value = "/deletedefectiveendproductoutbatch", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> deleteDefectiveEndProductOutBatch(@RequestParam("idList")List<Integer> idList,
                                                                 @RequestParam("orderName")String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<EndProduct> endProductStorageList = endProductService.getDefectiveEndProductDetailInByOrder(orderName);
        List<EndProduct> endProductOutList = endProductService.getEndProductByIdList(idList);
        for (EndProduct endProductStorage : endProductStorageList){
            for (EndProduct endProductOut : endProductOutList){
                if (endProductOut.getStorageId().equals(endProductStorage.getId())){
                    endProductStorage.setOutStoreCount(endProductStorage.getOutStoreCount() - endProductOut.getOutStoreCount());
                    endProductStorage.setStorageCount(endProductStorage.getStorageCount() + endProductOut.getOutStoreCount());
                }
            }
        }
        int res1 = endProductService.updateEndProductBatch(endProductStorageList);
        int res2 = endProductService.deleteEndProductBatch(idList);
        if (res1 == 0 && res2 == 0){
            map.put("result", 0);
        } else {
            map.put("result", 1);
        }
        return map;
    }


    @RequestMapping(value = "/getendproductpreoutstoredata", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getEndProductPreOutStoreData(@RequestParam("orderName")String orderName,
                                                            @RequestParam("endType")String endType){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> colorNameList = endProductService.getEndProductStorageColorList(orderName, endType);
        List<String> sizeNameList = endProductService.getEndProductStorageSizeList(orderName, endType);
        List<EndProduct> endProductList = endProductService.getEndProductStorageInfo(orderName, endType);
        map.put("colorNameList", colorNameList);
        map.put("sizeNameList", sizeNameList);
        map.put("endProductList", endProductList);
        return map;
    }


    @RequestMapping(value = "/addendproductoutbatch",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> addEndProductOutBatch(@RequestParam("endProductOutStoreJson")String endProductOutStoreJson,
                                                     @RequestParam("orderName")String orderName,
                                                     @RequestParam("endType")String endType){
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        ManufactureOrder manufactureOrder = manufactureOrderService.getManufactureOrderByOrderName(orderName);
        List<EndProduct> endProductOutList = gson.fromJson(endProductOutStoreJson,new TypeToken<List<EndProduct>>(){}.getType());
        List<EndProduct> endProductStorageList = endProductService.getEndProductStorageInfo(orderName, endType);
        List<ClothesPrice> clothesPriceList = clothesPriceService.getClothesPriceByInfo(orderName, null);
        List<EndProduct> updateStorageList = new ArrayList<>();
        for (EndProduct endProductOut : endProductOutList){
            float price = 0f;
            for (ClothesPrice clothesPrice : clothesPriceList){
                if (clothesPrice.getColorName().equals(endProductOut.getColorName())){
                    price = clothesPrice.getClothesPrice();
                }
            }
            endProductOut.setPrice(price);
            endProductOut.setCustomerName(manufactureOrder.getCustomerName() == null ? "" : manufactureOrder.getCustomerName());
            endProductOut.setTotalMoney(endProductOut.getPrice() * endProductOut.getOutStoreCount());
            for (EndProduct endProductStorage : endProductStorageList){
                if (endProductOut.getColorName().equals(endProductStorage.getColorName()) && endProductOut.getSizeName().equals(endProductStorage.getSizeName())){
                    endProductStorage.setOutStoreCount(endProductStorage.getOutStoreCount() + endProductOut.getOutStoreCount());
                    endProductStorage.setStorageCount(endProductStorage.getStorageCount() - endProductOut.getOutStoreCount());
                    if (endProductStorage.getStorageCount() < 0){
                        map.put("result", 3);
                        map.put("msg", endProductStorage.getColorName() + "-" + endProductStorage.getSizeName() + "库存不足无法出库！");
                        return map;
                    }
                    updateStorageList.add(endProductStorage);
                }
            }
        }
        // 该更新的更新 该添加的添加
        int res1 = endProductService.addEndProductBatch(endProductOutList);
        int res2 = endProductService.updateEndProductBatch(updateStorageList);
        if (res1 == 0 && res2 == 0){
            map.put("result", 0);
        } else {
            map.put("result", 1);
        }
        return map;

    }

    @RequestMapping(value = "/adddefectiveendproductoutbatch",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> addDefectiveEndProductOutBatch(@RequestParam("endProductOutStoreJson")String endProductOutStoreJson,
                                                              @RequestParam("orderName")String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        ManufactureOrder manufactureOrder = manufactureOrderService.getManufactureOrderByOrderName(orderName);
        List<EndProduct> endProductOutList = gson.fromJson(endProductOutStoreJson,new TypeToken<List<EndProduct>>(){}.getType());
        List<EndProduct> endProductStorageList = endProductService.getDefectiveEndProductDetailInByOrder(orderName);
        List<EndProduct> updateStorageList = new ArrayList<>();
        for (EndProduct endProductOut : endProductOutList){
            endProductOut.setPrice(manufactureOrder.getPrice() == null ? 0f : manufactureOrder.getPrice());
            endProductOut.setCustomerName(manufactureOrder.getCustomerName() == null ? "" : manufactureOrder.getCustomerName());
            endProductOut.setTotalMoney(endProductOut.getPrice() * endProductOut.getOutStoreCount());
            for (EndProduct endProductStorage : endProductStorageList){
                if (endProductOut.getStorageId().equals(endProductStorage.getId())){
                    endProductStorage.setOutStoreCount(endProductStorage.getOutStoreCount() + endProductOut.getOutStoreCount());
                    endProductStorage.setStorageCount(endProductStorage.getStorageCount() - endProductOut.getOutStoreCount());
                    if (endProductStorage.getStorageCount() < 0){
                        map.put("result", 3);
                        map.put("msg", endProductStorage.getColorName() + "-" + endProductStorage.getSizeName() + "库存不足无法出库！");
                        return map;
                    }
                    updateStorageList.add(endProductStorage);
                }
            }
        }
        // 该更新的更新 该添加的添加
        int res1 = endProductService.addEndProductBatch(endProductOutList);
        int res2 = endProductService.updateEndProductBatch(updateStorageList);
        if (res1 == 0 && res2 == 0){
            map.put("result", 0);
        } else {
            map.put("result", 1);
        }
        return map;

    }

    @RequestMapping(value = "/getoutstorecountbyordername", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getOutStoreCountByOrderName(@RequestParam("orderName")String orderName,
                                                           @RequestParam("endType")String endType){
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("outStoreCount", endProductService.getOutStoreCountByOrderName(orderName, endType));
        return map;
    }


    @RequestMapping(value = "/updateinstorecount", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> updateInStoreCount(@RequestParam("orderName")String orderName,
                                                  @RequestParam("colorName")String colorName,
                                                  @RequestParam("sizeName")String sizeName,
                                                  @RequestParam("initCount")Integer initCount,
                                                  @RequestParam("id")Integer id,
                                                  @RequestParam("toCount")Integer toCount,
                                                  @RequestParam("endType")String endType){
        Map<String, Object> map = new LinkedHashMap<>();
        EndProduct endProductStorage = endProductService.getStorageByOrderColorSize(orderName, colorName, sizeName, endType);
        if (endProductStorage == null){
            map.put("result", 3);
            return map;
        }
        if (toCount >= initCount){
            EndProduct endProduct = new EndProduct();
            endProduct.setId(id);
            endProduct.setInStoreCount(toCount);
            int res1 = endProductService.updateEndProduct(endProduct);
            endProductStorage.setInStoreCount(endProductStorage.getInStoreCount() + (toCount - initCount));
            endProductStorage.setStorageCount(endProductStorage.getStorageCount() + (toCount - initCount));
            int res2 = endProductService.updateEndProduct(endProductStorage);
            if (res1 == 0 && res2 == 0){
                map.put("result", 0);
            } else {
                map.put("result", 1);
            }
            return map;
        } else {
            int diffCount = initCount - toCount;
            if (endProductStorage.getStorageCount() < diffCount){
                map.put("result", 4);
                return map;
            } else {
                EndProduct endProduct = new EndProduct();
                endProduct.setId(id);
                endProduct.setInStoreCount(toCount);
                int res1 = endProductService.updateEndProduct(endProduct);
                endProductStorage.setInStoreCount(endProductStorage.getInStoreCount() + (toCount - initCount));
                endProductStorage.setStorageCount(endProductStorage.getStorageCount() + (toCount - initCount));
                int res2 = endProductService.updateEndProduct(endProductStorage);
                if (res1 == 0 && res2 == 0){
                    map.put("result", 0);
                } else {
                    map.put("result", 1);
                }
                return map;
            }
        }
    }

    @RequestMapping(value = "/updatedefectiveinstorecount", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> updateDefectiveInStoreCount(@RequestParam("orderName")String orderName,
                                                           @RequestParam("colorName")String colorName,
                                                           @RequestParam("sizeName")String sizeName,
                                                           @RequestParam("initCount")Integer initCount,
                                                           @RequestParam("id")Integer id,
                                                           @RequestParam("toCount")Integer toCount,
                                                           @RequestParam("endType")String endType){
        Map<String, Object> map = new LinkedHashMap<>();
        EndProduct endProduct = endProductService.getEndProductById(id);
        if (endProduct == null){
            map.put("result", 3);
            return map;
        }
        if (toCount >= initCount){
            endProduct.setInStoreCount(endProduct.getInStoreCount() + (toCount - initCount));
            endProduct.setStorageCount(endProduct.getStorageCount() + (toCount - initCount));
            int res = endProductService.updateEndProduct(endProduct);
            map.put("result", res);
            return map;
        } else {
            int diffCount = initCount - toCount;
            if (endProduct.getStorageCount() < diffCount){
                map.put("result", 4);
                return map;
            } else {
                endProduct.setInStoreCount(endProduct.getInStoreCount() + (toCount - initCount));
                endProduct.setStorageCount(endProduct.getStorageCount() + (toCount - initCount));
                int res = endProductService.updateEndProduct(endProduct);
                map.put("result", res);
                return map;
            }
        }
    }


    @RequestMapping(value = "/getendproductdetailbyorder", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getEndProductDetailByOrder(@RequestParam("orderName")String orderName,
                                                          @RequestParam("endType")String endType){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> colorNameList = orderClothesService.getOrderColorNamesByOrder(orderName);
        List<String> sizeNameList = orderClothesService.getOrderSizeNamesByOrder(orderName);
        List<CutQueryLeak> orderInfoList = orderClothesService.getOrderInfoByName(orderName);
        List<CutQueryLeak> tailorInfoList = tailorService.getTailorInfoByOrderPartType(orderName, null, 0, null, null);
        List<EndProduct> endProductInList = endProductService.getEndProductInStoreInfo(orderName, endType);
        List<EndProduct> endProductStorageList = endProductService.getEndProductStorageInfo(orderName, endType);
        List<EndProduct> endProductOutList = endProductService.getEndProductOutStoreInfo(orderName, endType);
        for (CutQueryLeak cutQueryLeak1 : orderInfoList){
            for (CutQueryLeak cutQueryLeak2 : tailorInfoList){
                if (cutQueryLeak2.getColorName().equals(cutQueryLeak1.getColorName()) && cutQueryLeak2.getSizeName().equals(cutQueryLeak1.getSizeName())){
                    cutQueryLeak1.setWellCount(cutQueryLeak2.getWellCount());
                }
            }
            for (EndProduct in : endProductInList){
                if (in.getColorName().equals(cutQueryLeak1.getColorName()) && in.getSizeName().equals(cutQueryLeak1.getSizeName())){
                    cutQueryLeak1.setInStoreCount(cutQueryLeak1.getInStoreCount() + in.getInStoreCount());
                }
            }
            for (EndProduct storage : endProductStorageList){
                if (storage.getColorName().equals(cutQueryLeak1.getColorName()) && storage.getSizeName().equals(cutQueryLeak1.getSizeName())){
                    cutQueryLeak1.setStorageCount(cutQueryLeak1.getStorageCount() + storage.getStorageCount());
                }
            }
            for (EndProduct out : endProductOutList){
                if (out.getColorName().equals(cutQueryLeak1.getColorName()) && out.getSizeName().equals(cutQueryLeak1.getSizeName())){
                    cutQueryLeak1.setOutStoreCount(cutQueryLeak1.getOutStoreCount() + out.getOutStoreCount());
                }
            }
        }
        map.put("colorNameList", colorNameList);
        map.put("sizeNameList", sizeNameList);
        map.put("endProductList", orderInfoList);
        return map;
    }

    @RequestMapping(value = "/getdefectivesummary", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getDefectiveSummary(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<EndProduct> endProductList = endProductService.getDefectiveSummary();
        map.put("endProductList", endProductList);
        return map;
    }

    @RequestMapping(value = "/getpreaccountendproduct", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getPreAccountEndProduct(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<EndProduct> endProductList = endProductService.getPreAccountEndProduct();
        for (EndProduct endProduct : endProductList){
            endProduct.setTotalMoney(endProduct.getOutStoreCount() * endProduct.getPrice());
        }
        map.put("data", endProductList);
        map.put("count", endProductList.size());
        map.put("code",0);
        return map;
    }

    @RequestMapping(value = "/getendproductoutstorebyinfo", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getEndProductOutStoreByInfo(@RequestParam(value = "monthStr", required = false)String monthStr,
                                                           @RequestParam(value = "customerName", required = false)String customerName,
                                                           @RequestParam(value = "orderName", required = false)String orderName,
                                                           @RequestParam(value = "endType", required = false)String endType){
        Map<String, Object> map = new LinkedHashMap<>();
        List<EndProduct> endProductList = endProductService.getEndProductOutStoreByInfo(monthStr, customerName, orderName, endType);
        for (EndProduct endProduct : endProductList){
            endProduct.setTotalMoney(endProduct.getOutStoreCount() * endProduct.getPrice());
        }
        map.put("data", endProductList);
        map.put("count", endProductList.size());
        map.put("code",0);
        return map;
    }

    @RequestMapping(value = "/endproducttakeintoaccount", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> endProductTakeIntoAccount(@RequestParam("idList")List<Integer> idList,
                                                         @RequestParam("accountDate")String accountDate,
                                                         @RequestParam("payNumber")String payNumber){
        Map<String, Object> map = new LinkedHashMap<>();
        int res = endProductService.endProductTakeIntoAccount(idList, accountDate, payNumber);
        List<EndProductPay> endProductPayList = endProductService.endProductToEndProductPay(payNumber);
        for (EndProductPay endProductPay : endProductPayList){
            endProductPay.setBalanceMoney(endProductPay.getSumMoney());
            endProductPay.setPayType("对账");
        }
        endProductPayService.addEndProductPayBatch(endProductPayList);
        map.put("result", res);
        return map;
    }

//    @Scheduled(cron = "0 11 20 * * ?")
//    public void adjustEndProduct(){
//        List<EndProduct> endProductList = endProductService.getAllEndProduct();
//        List<ManufactureOrder> manufactureOrderList = manufactureOrderService.getAllManufactureOrder();
//        for (ManufactureOrder manufactureOrder : manufactureOrderList){
//            for (EndProduct endProduct : endProductList){
//                if (manufactureOrder.getOrderName().equals(endProduct.getOrderName())){
//                    endProduct.setCustomerName(manufactureOrder.getCustomerName());
//                    if (manufactureOrder.getPrice() != null){
//                        endProduct.setPrice(manufactureOrder.getPrice());
//                    } else {
//                        endProduct.setPrice(0f);
//                    }
//                }
//            }
//        }
//        int res = endProductService.updateEndProductBatch(endProductList);
//        System.out.println("更新结束======" + res);
//    }


    @RequestMapping(value = "/quitaccountbypaynumber", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> quitAccountByIdList(@RequestParam("payNumber")String payNumber){
        Map<String, Object> map = new LinkedHashMap<>();
        int res1 = endProductService.quitAccountByPayNumber(payNumber);
        int res2 = endProductPayService.deleteEndProductPayByPayNumber(payNumber);
        if (res1 == 0 && res2 == 0){
            map.put("result", 0);
        } else {
            map.put("result", 1);
        }
        return map;
    }

    @RequestMapping(value = "/addendproductquitbatch",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> addEndProductQuitBatch(@RequestParam("endProductOutStoreJson")String endProductOutStoreJson,
                                                      @RequestParam("orderName")String orderName,
                                                      @RequestParam("endType")String endType){
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        List<EndProduct> initEndProductOutList = endProductService.getEndProductOutStoreInfo(orderName, endType);
        List<EndProduct> endProductOutList = gson.fromJson(endProductOutStoreJson,new TypeToken<List<EndProduct>>(){}.getType());
        List<EndProduct> endProductStorageList = endProductService.getEndProductStorageInfo(orderName, endType);
        List<EndProduct> updateStorageList = new ArrayList<>();
        List<EndProduct> insertEndProductList = new ArrayList<>();
        for (EndProduct endProduct : endProductOutList){
            for (EndProduct endProduct1 : initEndProductOutList){
                if (endProduct.getColorName().equals(endProduct1.getColorName()) && endProduct.getSizeName().equals(endProduct1.getSizeName())){
                    endProduct1.setOutStoreCount(- endProduct.getOutStoreCount());
                    endProduct1.setAccountState("未入账");
                    endProduct1.setAccountDate(null);
                    insertEndProductList.add(endProduct1);
                }
            }
            for (EndProduct endProduct2 : endProductStorageList){
                if (endProduct.getColorName().equals(endProduct2.getColorName()) && endProduct.getSizeName().equals(endProduct2.getSizeName())){
                    endProduct2.setStorageCount(endProduct2.getStorageCount() + endProduct.getOutStoreCount());
                    endProduct2.setOutStoreCount(endProduct2.getOutStoreCount() - endProduct.getOutStoreCount());
                    updateStorageList.add(endProduct2);
                }
            }
        }
        // 该更新的更新 该添加的添加
        int res1 = endProductService.addEndProductBatch(insertEndProductList);
        int res2 = endProductService.updateEndProductBatch(updateStorageList);
        if (res1 == 0 && res2 == 0){
            map.put("result", 0);
        } else {
            map.put("result", 1);
        }
        return map;

    }

    @RequestMapping(value = "/updatepricebyordername", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> updatePriceByOrderName(@RequestParam("orderName")String orderName,
                                                      @RequestParam("colorName")String colorName,
                                                      @RequestParam("price")Float price){
        Map<String, Object> map = new LinkedHashMap<>();
        int res = endProductService.updatePriceByOrderName(orderName, colorName, price);
        List<String> payNumberList = endProductService.getPayNumberByOrderName(orderName);
        for (String payNumber : payNumberList){
            float sumMoney = endProductService.getSumMoneyByPayNumber(payNumber);
            EndProductPay endProductPay = endProductPayService.getEndProductPayByPayNumber(payNumber);
            endProductPay.setSumMoney(sumMoney);
            endProductPay.setBalanceMoney(sumMoney);
            endProductPayService.updateEndProductPay(endProductPay);
        }
        if (res == 0){
            map.put("result", 0);
        } else {
            map.put("result", 1);
        }
        return map;
    }


}
