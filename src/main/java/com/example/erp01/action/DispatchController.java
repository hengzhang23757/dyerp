package com.example.erp01.action;


import com.example.erp01.model.Dispatch;
import com.example.erp01.model.OrderProcedure;
import com.example.erp01.model.ProcedureInfo;
import com.example.erp01.model.ProcessRequirement;
import com.example.erp01.service.DispatchService;
import com.example.erp01.service.OrderProcedureService;
import com.google.gson.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

@Controller
@RequestMapping(value = "erp")
public class DispatchController {

    @Autowired
    private DispatchService dispatchService;
    @Autowired
    private OrderProcedureService orderProcedureService;

    @RequestMapping(value = "/dispatchStart")
    public String dispatchStart(Model model){
        model.addAttribute("bigMenuTag",10);
        model.addAttribute("menuTag",102);
        return "miniProgram/dispatch";
    }


    @RequestMapping(value = "/adddispatch",method = RequestMethod.POST)
    @ResponseBody
    public int addDispatch(Dispatch dispatch){
        int res = dispatchService.addDispatch(dispatch);
        return res;
    }

    @RequestMapping(value = "/miniadddispatchbatch",method = RequestMethod.POST)
    @ResponseBody
    public int miniAddDispatchBatch(@RequestParam("dispatchJson")String dispatchJson){
        JsonParser jsonParser = new JsonParser();
        try{
            JsonObject json = (JsonObject) jsonParser.parse(dispatchJson);
            String orderName = json.get("orderName").getAsString();
            String procedureInfo = json.get("procedureInfo").getAsString();
            String[] tmp = procedureInfo.split("-");
            int procedureCode = 0;
            int procedureNumber = Integer.parseInt(tmp[1]);
            String procedureName = tmp[2];
            String groupName = json.get("groupName").getAsString();
            JsonArray array = json.get("emp").getAsJsonArray();
            List<Dispatch> dispatchList = new ArrayList<>();
            for (int i = 0; i < array.size(); i++) {
                JsonObject subObject = array.get(i).getAsJsonObject();
                String tmpEmpNum = subObject.get("employeeNumber").getAsString();
                String tmpEmpName = subObject.get("employeeName").getAsString();
                Dispatch dispatch = new Dispatch(tmpEmpNum,tmpEmpName,groupName,orderName,procedureCode,procedureNumber,procedureName);
                dispatchList.add(dispatch);
            }
            int res = dispatchService.addDispatchBatch(dispatchList);
            return res;
        }catch (JsonIOException e){
            e.printStackTrace();
        }catch (JsonSyntaxException e){
            e.printStackTrace();
        }
        return 1;
    }

    @RequestMapping(value = "/miniadddispatchbatchnew",method = RequestMethod.POST)
    @ResponseBody
    public int miniAddDispatchBatchNew(@RequestParam("dispatchJson")String dispatchJson){
        JsonParser jsonParser = new JsonParser();
        try{
            JsonObject json = (JsonObject) jsonParser.parse(dispatchJson);
            String orderName = json.get("orderName").getAsString();
            JsonArray procedureInfoArray = json.get("procedureInfo").getAsJsonArray();
            String groupName = json.get("groupName").getAsString();
            JsonArray array = json.get("emp").getAsJsonArray();
            List<String> employeeNumberList = new ArrayList<>();
            List<Dispatch> dispatchList = new ArrayList<>();
            for (int i = 0; i < array.size(); i++) {
                JsonObject empObject = array.get(i).getAsJsonObject();
                String tmpEmpNum = empObject.get("employeeNumber").getAsString();
                String tmpEmpName = empObject.get("employeeName").getAsString();
                employeeNumberList.add(tmpEmpNum);
                for (int j = 0; j < procedureInfoArray.size(); j ++){
                    String procedureString = procedureInfoArray.get(j).getAsString();
                    String[] tmp = procedureString.split("-");
                    int procedureCode = 0;
                    int procedureNumber = Integer.parseInt(tmp[1]);
                    String procedureName = tmp[2];
                    Dispatch dispatch = new Dispatch(tmpEmpNum,tmpEmpName,groupName,orderName,procedureCode,procedureNumber,procedureName);
                    dispatchList.add(dispatch);
                }
            }
            dispatchService.deleteDispatchByOrderEmployeeList(orderName, employeeNumberList);
            return dispatchService.addDispatchBatch(dispatchList);
        }catch (JsonIOException e){
            e.printStackTrace();
        }catch (JsonSyntaxException e){
            e.printStackTrace();
        }
        return 1;
    }

    @RequestMapping(value = "/adddispatchbatch",method = RequestMethod.POST)
    @ResponseBody
    public int addDispatchBatch(@RequestParam("dispatchJson")String dispatchJson){
        JsonParser jsonParser = new JsonParser();
        try{
            JsonObject json = (JsonObject) jsonParser.parse(dispatchJson);
            String orderName = json.get("orderName").getAsString();
            String procedureInfo = json.get("procedureInfo").getAsString();
            String[] tmp = procedureInfo.split("-");
            int procedureCode = 0;
            int procedureNumber = Integer.parseInt(tmp[1]);
            String procedureName = tmp[2];
            String groupName = json.get("groupName").getAsString();
            JsonArray array = json.get("emp").getAsJsonArray();
            List<Dispatch> dispatchList = new ArrayList<>();
            for (int i = 0; i < array.size(); i++) {
                JsonObject subObject = array.get(i).getAsJsonObject();
                String tmpEmpNum = subObject.get("employeeNumber").getAsString();
                String tmpEmpName = subObject.get("employeeName").getAsString();
                Dispatch dispatch = new Dispatch(tmpEmpNum,tmpEmpName,groupName,orderName,procedureCode,procedureNumber,procedureName);
                dispatchList.add(dispatch);
            }
            int res = dispatchService.addDispatchBatch(dispatchList);
            return res;
        }catch (JsonIOException e){
            e.printStackTrace();
        }catch (JsonSyntaxException e){
            e.printStackTrace();
        }
        return 1;
    }

    @RequestMapping(value = "/minideletedispatch",method = RequestMethod.POST)
    @ResponseBody
    public int miniDeleteDispatch(@RequestParam("dispatchID")int dispatchID){
        int res = dispatchService.deleteDispatch(dispatchID);
        return res;
    }

    @RequestMapping(value = "/miniinitdispatch",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> miniInitDispatch(@RequestParam("initDispatchJson")String initDispatchJson){
        Map<String, Object> map = new LinkedHashMap<>();
        try {
            JsonParser jsonParser = new JsonParser();
            JsonObject json = (JsonObject) jsonParser.parse(initDispatchJson);
            String orderName = json.get("orderName").getAsString();
            JsonArray array = json.get("emp").getAsJsonArray();
            List<String> employeeNumberList = new ArrayList<>();
            for (int i = 0; i < array.size(); i++) {
                JsonObject subObject = array.get(i).getAsJsonObject();
                String tmpEmpNum = subObject.get("employeeNumber").getAsString();
                employeeNumberList.add(tmpEmpNum);
            }
            int res = dispatchService.deleteDispatchByOrderEmployeeList(orderName, employeeNumberList);
            map.put("result", res);
        } catch (JsonSyntaxException e) {
            map.put("result", 1);
            e.printStackTrace();
        }
        return map;
    }

    @RequestMapping(value = "/deletedispatch",method = RequestMethod.POST)
    @ResponseBody
    public int deleteDispatch(@RequestParam("dispatchID")int dispatchID){
        int res = dispatchService.deleteDispatch(dispatchID);
        return res;
    }


    @RequestMapping(value = "/getalldispatch",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getAllDispatch(){
        Map<String,Object> map = new HashMap<>();
        List<Dispatch> dispatchList = new ArrayList<>();
        dispatchList = dispatchService.getAllDispatch();
        Collections.sort(dispatchList);
        map.put("dispatchList",dispatchList);
        return map;
    }

    @RequestMapping(value = "/minigetdispatchbyorder",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> miniGetDispatchByOrder(@RequestParam("orderName")String orderName){
        Map<String,Object> map = new HashMap<>();
        List<Dispatch> dispatchList = new ArrayList<>();
        dispatchList = dispatchService.getDispatchByOrder(orderName);
        Collections.sort(dispatchList, new Comparator<Dispatch>() {
            @Override
            public int compare(Dispatch o1, Dispatch o2) {
                if(o1.getProcedureCode().compareTo(o2.getProcedureCode())>0) {
                    return 1;
                }else if(o1.getProcedureCode().compareTo(o2.getProcedureCode())<0) {
                    return -1;
                }else
                    return 0;
            }
        });
        map.put("dispatchListOrderName",dispatchList);
        return map;
    }

    @RequestMapping(value = "/getdispatchbyorder",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getDispatchByOrder(@RequestParam("orderName")String orderName){
        Map<String,Object> map = new HashMap<>();
        List<Dispatch> dispatchList = new ArrayList<>();
        dispatchList = dispatchService.getDispatchByOrder(orderName);
        map.put("dispatchListOrderName",dispatchList);
        return map;
    }

    @RequestMapping(value = "/getdispatchbygroup",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getDispatchByGroup(@RequestParam("groupName")String groupName){
        Map<String,Object> map = new HashMap<>();
        List<Dispatch> dispatchList = new ArrayList<>();
        dispatchList = dispatchService.getDispatchByGroup(groupName);
        map.put("dispatchListGroupName",dispatchList);
        return map;
    }

    @RequestMapping(value = "/minigetdispatchbyordergroup",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> miniGetDispatchByOrderGroup(@RequestParam("orderName")String orderName,
                                                      @RequestParam("groupName")String groupName){
        Map<String,Object> map = new HashMap<>();
        List<Dispatch> dispatchList = new ArrayList<>();
        dispatchList = dispatchService.getDispatchByOrderGroup(orderName, groupName);
        Collections.sort(dispatchList, new Comparator<Dispatch>() {
            @Override
            public int compare(Dispatch o1, Dispatch o2) {
                if(o1.getProcedureCode().compareTo(o2.getProcedureCode())>0) {
                    return 1;
                }else if(o1.getProcedureCode().compareTo(o2.getProcedureCode())<0) {
                    return -1;
                }else
                    return 0;
            }
        });
        map.put("dispatchListOrderGroup",dispatchList);
        return map;
    }

    @RequestMapping(value = "/getdispatchbyordergroup",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getDispatchByOrderGroup(@RequestParam("orderName")String orderName,
                                                      @RequestParam("groupName")String groupName){
        Map<String,Object> map = new HashMap<>();
        List<Dispatch> dispatchList = new ArrayList<>();
        dispatchList = dispatchService.getDispatchByOrderGroup(orderName, groupName);
        map.put("dispatchListOrderGroup",dispatchList);
        return map;
    }

    @RequestMapping(value = "/minigetprocedureinfobyorderemp",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> miniGetProcedureInfoByOrderEmp(@RequestParam("orderName")String orderName,
                                                             @RequestParam("employeeNumber")String employeeNumber,
                                                             @RequestParam(value = "partName")String partName){
        Map<String,Object> map = new LinkedHashMap<>();
        List<ProcedureInfo> procedureInfoEmpList = dispatchService.getProcedureInfoByOrderEmp(orderName, employeeNumber);
        List<OrderProcedure> orderProcedureList = orderProcedureService.getOrderProcedureByOrder(orderName);
        List<ProcedureInfo> procedureInfoList = new ArrayList<>();
        for (ProcedureInfo procedureInfo : procedureInfoEmpList){
            for (OrderProcedure orderProcedure : orderProcedureList){
                if (procedureInfo.getProcedureNumber().equals(orderProcedure.getProcedureNumber())){
                    if (partName.contains(orderProcedure.getScanPart())){
                        procedureInfoList.add(procedureInfo);
                    }
                }
            }
        }
        map.put("procedureInfoEmpList",procedureInfoList);
        return map;
    }



    @RequestMapping(value = "/getprocedureinfobyorderemp",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getProcedureInfoByOrderEmp(@RequestParam("orderName")String orderName,
                                                         @RequestParam("employeeNumber")String employeeNumber){
        Map<String,Object> map = new HashMap<>();
        List<ProcedureInfo> procedureInfoEmpList = dispatchService.getProcedureInfoByOrderEmp(orderName, employeeNumber);
        map.put("procedureInfoEmpList",procedureInfoEmpList);
        return map;
    }

    @RequestMapping(value = "/minigetdispatchemployeenames",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> miniGetDispatchEmployeeNames(@RequestParam("orderName")String orderName,
                                                           @RequestParam("groupName")String groupName){
        Map<String,Object> map = new LinkedHashMap<>();
        if ("".equals(groupName) || "全部".equals(groupName)){
            groupName = null;
        }
        List<String> employeeNameList = dispatchService.getDistinctEmployeeNameByOrder(orderName, groupName);
        map.put("employeeNameList",employeeNameList);
        return map;
    }

    @RequestMapping(value = "/minigetdispatchbyordergroupemp",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> miniGetDispatchByOrderGroupEmp(@RequestParam("orderName")String orderName,
                                                             @RequestParam("groupName")String groupName,
                                                             @RequestParam("employeeName")String employeeName){
        Map<String,Object> map = new LinkedHashMap<>();
        if ("".equals(groupName) || "全部".equals(groupName)){
            groupName = null;
        }
        if ("".equals(employeeName) || "全部".equals(employeeName)){
            employeeName = null;
        }
        List<Dispatch> dispatchList = dispatchService.getDispatchByOrderGroupEmp(orderName, groupName, employeeName);
        map.put("dispatchList",dispatchList);
        return map;
    }

    @RequestMapping(value = "/gettodaydispatch",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getTodayDispatch(){
        Map<String,Object> map = new LinkedHashMap<>();
        List<Dispatch> dispatchList = dispatchService.getTodayDispatch();
        Collections.sort(dispatchList);
        map.put("dispatchList",dispatchList);
        return map;
    }

    @RequestMapping(value = "/getonemonthdispatch",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getOneMonthDispatch(){
        Map<String,Object> map = new LinkedHashMap<>();
        List<Dispatch> dispatchList = dispatchService.getOneMonthDispatch();
        Collections.sort(dispatchList);
        map.put("dispatchList",dispatchList);
        return map;
    }

    @RequestMapping(value = "/getthreemonthsdispatch",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getThreeMonthsDispatch(){
        Map<String,Object> map = new LinkedHashMap<>();
        List<Dispatch> dispatchList = dispatchService.getThreeMonthsDispatch();
        Collections.sort(dispatchList);
        map.put("dispatchList",dispatchList);
        return map;
    }

    @RequestMapping(value = "/minigetdispatchbyorderemployeenumber",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> miniGetDispatchByOrderEmployeeNumber(@RequestParam("orderName")String orderName,
                                                                   @RequestParam("employeeNumber")String employeeNumber){
        Map<String,Object> map = new LinkedHashMap<>();
        List<ProcedureInfo> procedureInfoEmpList = dispatchService.getProcedureInfoByOrderEmp(orderName, employeeNumber);
        List<OrderProcedure> orderProcedureList = orderProcedureService.getOrderProcedureByOrder(orderName);
        for (ProcedureInfo procedureInfo : procedureInfoEmpList){
            for (OrderProcedure orderProcedure : orderProcedureList){
                if (procedureInfo.getProcedureNumber().equals(orderProcedure.getProcedureNumber())){
                    procedureInfo.setScanPart(orderProcedure.getScanPart());
                }
            }
        }
        map.put("dispatchList",procedureInfoEmpList);
        return map;
    }

}
