package com.example.erp01.action;

import com.example.erp01.model.PrintPart;
import com.example.erp01.model.Tailor;
import com.example.erp01.service.PrintPartService;
import com.example.erp01.service.SyncTailorService;
import com.example.erp01.service.TailorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "erp")
public class SyncTailorController {

    @Autowired
    private SyncTailorService syncTailorService;
    @Autowired
    private TailorService tailorService;
    @Autowired
    private PrintPartService printPartService;

    @RequestMapping(value = "/checktailorexist", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> checkTailorExist(@RequestParam("orderName")String orderName,
                                                @RequestParam(value = "bedNumber", required = false)Integer bedNumber,
                                                @RequestParam(value = "packageNumber", required = false)Integer packageNumber){
        Map<String, Object> map = new LinkedHashMap<>();
        List<Tailor> tailorList = syncTailorService.getTailorByInfo(orderName, bedNumber, packageNumber);
        if (tailorList != null && !tailorList.isEmpty()){
            map.put("result", 1);
        } else {
            map.put("result", 0);
        }
        return map;
    }

    @RequestMapping(value = "/synctailortoyc", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> syncTailorToYc(@RequestParam("orderName")String orderName,
                                              @RequestParam(value = "bedNumber", required = false)Integer bedNumber,
                                              @RequestParam(value = "packageNumber", required = false)Integer packageNumber){
        Map<String, Object> map = new LinkedHashMap<>();
        List<Tailor> tailorList = tailorService.getTailorByInfo(orderName, null, null, null, bedNumber, packageNumber, null, 0);
        int res = syncTailorService.addTailorBatch(tailorList);
        List<PrintPart> printPartList = printPartService.getAllPrintPartByOrder(orderName);
        syncTailorService.addPrintPartBatch(printPartList);
        map.put("result", res);
        return map;
    }

}
