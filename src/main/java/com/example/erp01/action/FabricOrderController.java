package com.example.erp01.action;

import com.example.erp01.model.FabricOrder;
import com.example.erp01.model.OpaBackInput;
import com.example.erp01.service.FabricOrderService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "erp")
public class FabricOrderController {

    @Autowired
    private FabricOrderService fabricOrderService;

    @RequestMapping("/fabricOrderStart")
    public String completionStart(){
        return "fabric/fabricOrder";
    }

    @RequestMapping(value = "/addfabricorderbatch",method = RequestMethod.POST)
    @ResponseBody
    public int addFabricOrderBatch(@RequestParam("fabricOrderJson") String fabricOrderJson){
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        List<FabricOrder> fabricOrderList = gson.fromJson(fabricOrderJson,new TypeToken<List<FabricOrder>>(){}.getType());
        return fabricOrderService.addFabricOrderBatch(fabricOrderList);
    }

    @RequestMapping(value = "/getallfabricorder",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getAllFabric(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<FabricOrder> fabricOrderList = fabricOrderService.getAllFabricOrder();
        map.put("fabricOrderList",fabricOrderList);
        return map;
    }

    @RequestMapping(value = "/getonemonthfabricorder",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getOneMonthFabric(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<FabricOrder> fabricOrderList = fabricOrderService.getOneMonthFabricOrder();
        map.put("fabricOrderList",fabricOrderList);
        return map;
    }

    @RequestMapping(value = "/gettodayfabricorder",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getOneYearFabricOrder(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<FabricOrder> fabricOrderList = fabricOrderService.getTodayFabricOrder();
        map.put("fabricOrderList",fabricOrderList);
        return map;
    }

    @RequestMapping(value = "/getthreemonthsfabricorder",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getThreeMonthsFabricOrder(Model model){
        Map<String, Object> map = new LinkedHashMap<>();
        List<FabricOrder> fabricOrderList = fabricOrderService.getTodayFabricOrder();
        map.put("fabricOrderList",fabricOrderList);
        return map;
    }

    @RequestMapping("/addFabricOrderStart")
    public String addOrderProcedureStart(Model model){
        model.addAttribute("type", "add");
        return "fabric/fb_fabricOrderAdd";
    }

    @RequestMapping(value = "/commitfabricorder",method = RequestMethod.POST)
    @ResponseBody
    public int commitFabricOrder(@RequestParam("fabricOrderIDJson") String fabricOrderIDJson){
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        List<Integer> fabricOrderIDList = gson.fromJson(fabricOrderIDJson,new TypeToken<List<Integer>>(){}.getType());
        return fabricOrderService.commitFabricOrder(fabricOrderIDList);
    }

    @RequestMapping(value = "/deletefabricorder",method = RequestMethod.POST)
    @ResponseBody
    public int deleteFabricOrder(@RequestParam("fabricOrderID") Integer fabricOrderID){
        return fabricOrderService.deleteFabricOrder(fabricOrderID);
    }


}
