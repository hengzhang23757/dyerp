package com.example.erp01.action;

import com.example.erp01.model.FabricHandOver;
import com.example.erp01.model.LooseFabric;
import com.example.erp01.service.FabricHandOverService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
@RequestMapping(value = "erp")
public class FabricHandOverController {

    @Autowired
    private FabricHandOverService fabricHandOverService;

    @RequestMapping(value = "/miniaddfabrichandoverbatch", method = RequestMethod.POST)
    @ResponseBody
    public int addFabricOrderBatch(@RequestParam("looseFabricJson") String looseFabricJson,
                                   @RequestParam("shelfName") String shelfName){
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        List<LooseFabric> looseFabricList = gson.fromJson(looseFabricJson,new TypeToken<List<LooseFabric>>(){}.getType());
        List<FabricHandOver> fabricHandOverList = new ArrayList<>();
        Date loadingTime = new Date();
        for (LooseFabric looseFabric:looseFabricList){
            FabricHandOver fabricHandOver = new FabricHandOver(looseFabric.getOrderName(), looseFabric.getClothesVersionNumber(), looseFabric.getColorName(), looseFabric.getJarNumber(), looseFabric.getBatchOrder(), looseFabric.getFabricName(), looseFabric.getFabricColor(), looseFabric.getqCodeID(), looseFabric.getFromLocation(), shelfName, loadingTime, null);
            fabricHandOverList.add(fabricHandOver);
        }
        return fabricHandOverService.addFabricHandOverBatch(fabricHandOverList);
    }

    @RequestMapping(value = "/minigetfabrichandoverbyqcode", method = RequestMethod.GET)
    @ResponseBody
    public int getFabricHandOverByQcode(@RequestParam("qCodeID") Integer qCodeID){
        List<FabricHandOver> fabricHandOverList = fabricHandOverService.getFabricHandOverByQcode(qCodeID);
        if (fabricHandOverList == null || fabricHandOverList.size() == 0){
            return 1;
        }else {
            return 0;
        }
    }

    @RequestMapping(value = "/minigetfabrichandoverbyshelf", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getFabricHandOverByShelf(@RequestParam("shelfName") String shelfName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<FabricHandOver> fabricHandOverList = fabricHandOverService.getFabricHandOverByShelf(shelfName);
        map.put("fabricHandOverList", fabricHandOverList);
        return map;
    }

    @RequestMapping(value = "/miniupdatefabrichandoverbatch", method = RequestMethod.POST)
    @ResponseBody
    public int updateFabricHandOverBatch(@RequestParam("looseFabricJson") String looseFabricJson){
        Gson gson = new GsonBuilder().setDateFormat("yyyy/MM/dd").create();
        List<FabricHandOver> fabricHandOverList = gson.fromJson(looseFabricJson,new TypeToken<List<FabricHandOver>>(){}.getType());
        Date unLoadTime = new Date();
        for (FabricHandOver fabricHandOver : fabricHandOverList){
            fabricHandOver.setUnLoadTime(unLoadTime);
        }
        return fabricHandOverService.updateFabricHandOverBatch(fabricHandOverList);
    }

    @RequestMapping(value = "/minideletefabrichandoverbyshelf", method = RequestMethod.POST)
    @ResponseBody
    public int deleteFabricHandOverByShelf(@RequestParam("shelfName") String shelfName){
        return fabricHandOverService.deleteFabricHandOverByShelf(shelfName);
    }

    @RequestMapping(value = "/minigetfabrichandovercolorhint", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getFabricHandOverColorHint(@RequestParam("orderName") String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> colorNameList = fabricHandOverService.getFabricHandOverColorHint(orderName);
        map.put("colorNameList", colorNameList);
        return map;
    }

    @RequestMapping(value = "/minigetfabrichandoverfabriccolorhint", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getFabricHandOverFabricColorHint(@RequestParam("orderName") String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> fabricColorList = fabricHandOverService.getFabricHandOverFabricColorHint(orderName);
        map.put("fabricColorList", fabricColorList);
        return map;
    }

    @RequestMapping(value = "/minigetfabrichandoverbyinfo", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getFabricHandOverByInfo(@RequestParam("from") String from,
                                                       @RequestParam("to") String to,
                                                       @RequestParam("orderName") String orderName,
                                                       @RequestParam("colorName") String colorName,
                                                       @RequestParam("fabricColor") String fabricColor){
        Map<String, Object> map = new LinkedHashMap<>();
        if("".equals(orderName) || "请选择款号".equals(orderName)){
            orderName = null;
        }
        if("".equals(from)){
            from = "1970-01-01";
        }
        if("".equals(to)){
            to = "2050-01-01";
        }
        if("".equals(colorName) || "全部".equals(colorName)){
            colorName = null;
        }
        if("".equals(fabricColor) || "全部".equals(fabricColor)){
            fabricColor = null;
        }
        try{
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date fromDate = sdf.parse(from);
            Date toDate = sdf.parse(to);
            List<FabricHandOver> fabricHandOverList = fabricHandOverService.getFabricHandOverByInfo(fromDate, toDate, orderName, colorName, fabricColor);
            map.put("fabricHandOverList", fabricHandOverList);
            return map;
        }catch (ParseException e){
            e.printStackTrace();
        }
        return map;
    }

}
