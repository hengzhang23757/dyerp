package com.example.erp01.action;

import com.example.erp01.model.Employee;
import com.example.erp01.model.Message;
import com.example.erp01.model.User;
import com.example.erp01.model.YesterdayReWork;
import com.example.erp01.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.*;

//用户基本操作，新增，删除，登录等操作

@Controller
@RequestMapping(value = "erp")
public class UserController {

    @Autowired
    private YesterdayReportService yesterdayReportService;


    @Autowired
    private UserService userService;

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private MessageService messageService;

    @RequestMapping(value ="/userlogin", method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> userLogin(@RequestParam("userName")String userName,
                                        @RequestParam("passWord")String passWord,
                                        HttpServletRequest request){
        Map<String,Object> result = new HashMap<String,Object>();
        if(StringUtils.isEmpty(userName) || StringUtils.isEmpty(passWord)){
            result.put("flag",false);
            result.put("msg","用户名或密码不能为空！");
        }
        User user = userService.userLogin(userName,passWord);
        if (user != null){
            result.put("flag",true);
            result.put("msg","登录成功！");
            Date now = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");//可以方便地修改日期格式
            String data = dateFormat.format( now );
            System.out.println("用户名："+userName+",密码："+passWord+",时间："+data);
            request.getSession().setAttribute("userName", user.getUserName());
            request.getSession().setAttribute("role", user.getRole());
            request.getSession().setMaxInactiveInterval(86400);
        }else if (employeeService.employeeLogin(userName,passWord) != null){
            Employee employee = employeeService.employeeLogin(userName,passWord);
            result.put("piece",true);
            result.put("flag",true);
            result.put("msg","登录成功！");
            Date now = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");//可以方便地修改日期格式
            String data = dateFormat.format( now );
            System.out.println("用户名："+userName+",密码："+passWord+",时间："+data);
            request.getSession().setAttribute("userName", employee.getEmployeeNumber());
            request.getSession().setAttribute("role", "role13");
            request.getSession().setAttribute("employeeName", employee.getEmployeeName());
            request.getSession().setAttribute("groupName", employee.getGroupName());
            request.getSession().setMaxInactiveInterval(86400);
        }else{
            result.put("msg","登录失败，用户名或密码错误！");
            result.put("flag",false);
        }
        return result;
    }

    @RequestMapping(value ="/homepage")
    public String getHomePageStart(Model model,HttpServletRequest request) {
        model.addAttribute("bigMenuTag",0);
        model.addAttribute("menuTag",0);
        List<YesterdayReWork> yesterdayReWorkList = yesterdayReportService.getYesterdayReWorkReport();
        List<String> orderNameList = new ArrayList<>();
        for (YesterdayReWork yesterdayReWork : yesterdayReWorkList){
            if (!orderNameList.contains(yesterdayReWork.getOrderName())){
                orderNameList.add(yesterdayReWork.getOrderName());
            }
        }
        model.addAttribute("orderNameList",orderNameList);
        return "homepage/index";
    }


    @RequestMapping(value ="/homepageNew")
    public String getHomePageNewStart(Model model,HttpServletRequest request) {
        return "homepage/homepageNew";
    }

    @RequestMapping(value ="/consoleStart")
    public String getConsolePage() {
        return "console/console";
    }


    @RequestMapping(value ="/userInfoStart")
    public String userInfoStart() {
        return "views/set/user/info";
    }

    @RequestMapping(value ="/passwordStart")
    public String passwordStart() {
        return "views/set/user/password";
    }

    @RequestMapping(value ="/logout")
    public String logoutStart() {
        return "views/user/login";
    }

    @RequestMapping(value ="/messageIndexStart")
    public String messageIndexStart() {
        return "views/app/message/index";
    }


    @RequestMapping(value ="/messageDetailStart")
    public String messageDetailStart(Model model, Integer id) {
        Message message = messageService.getMessageById(id);
        List<Integer> idList = new ArrayList<>();
        idList.add(id);
        messageService.updateMessageReadType(idList);
        model.addAttribute("title", message.getTitle());
        model.addAttribute("messageBodyOne", message.getMessageBodyOne());
        model.addAttribute("createTime", message.getCreateTime());
        model.addAttribute("createUser", message.getCreateUser());
        return "views/app/message/detail";
    }


    @RequestMapping(value ="/aboutStart")
    public String aboutStart() {
        return "about/about";
    }

    @RequestMapping("/userStart")
    public String userStart(){
        return "factoryMsg/user";
    }


    @RequestMapping(value ="/adduser", method = RequestMethod.POST)
    @ResponseBody
    public int addUser(User user){
        User user1 = getUserByName(user.getUserName());
        if(user1!=null && user1.getId()!=null) {
            return 3;
        }
        return userService.addUser(user);
    }



    @RequestMapping(value = "/getuserbyname", method = RequestMethod.GET)
    @ResponseBody
    public User getUserByName(@RequestParam("userName")String userName){
        User user = userService.getUserByName(userName);
        return user;
    }

    @RequestMapping(value = "/deleteuser", method = RequestMethod.POST)
    @ResponseBody
    public int deleteUser(@RequestParam("userID")Integer userID){
        int res = userService.deleteUser(userID);
        return res;
    }

    @RequestMapping(value = "/getuserbyid", method = RequestMethod.GET)
    @ResponseBody
    public User getUserByID(@RequestParam("userID")Integer userID){
        User user = userService.getUserByID(userID);
        return user;
    }

    @RequestMapping(value = "/getalluser", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getAllUser(){
        List<User> userList = userService.getAllUser();
        Map<String, Object> map = new LinkedHashMap<>();
        Set<String> departmentSet = new HashSet<>();
        for (User user : userList){
            departmentSet.add(user.getDepartment());
        }
        map.put("departmentSet", departmentSet);
        map.put("data", userList);
        map.put("code", 0);
        map.put("count", userList.size());
        return map;
    }

    @RequestMapping(value = "/updateUser", method = RequestMethod.GET)
    @ResponseBody
    public int updateUser(String userName,String originPassWord,String newPassWord){
        User user = userService.userLogin(userName,originPassWord);
        if (user != null) {
            user.setPassWord(newPassWord);
            return userService.updateUser(user);
        }else {
            return -1;
        }
    }

    @RequestMapping(value = "/getallusername", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getAllUserName(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> userNameList = userService.getAllUserName();
        map.put("userNameList", userNameList);
        return map;
    }


}
