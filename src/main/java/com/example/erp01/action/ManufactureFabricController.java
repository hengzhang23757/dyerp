package com.example.erp01.action;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.example.erp01.model.*;
import com.example.erp01.service.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
@RequestMapping(value = "erp")
public class ManufactureFabricController {

    private static final Logger log = LoggerFactory.getLogger(ManufactureFabricController.class);

    @Autowired
    private ManufactureFabricService manufactureFabricService;
    @Autowired
    private OrderClothesService orderClothesService;
    @Autowired
    private ManufactureOrderService manufactureOrderService;
    @Autowired
    private FabricDetailService fabricDetailService;
    @Autowired
    private MessageService messageService;
    @Autowired
    private ManufactureAccessoryService manufactureAccessoryService;

    @RequestMapping("/fabricAccessoryStart")
    public String fabricAccessoryStart(){
        return "basicInfo/fabricAccessory";
    }

    @RequestMapping("/fabricAccessoryCheckStart")
    public String fabricAccessoryCheckStart(){
        return "basicInfo/fabricAccessoryCheck";
    }

    @RequestMapping("/fabricAccessoryOnlyCheckStart")
    public String fabricAccessoryOnlyCheckStart(){
        return "basicInfo/fabricAccessoryOnlyCheck";
    }

    @RequestMapping("/fabricReturnStart")
    public String fabricReturnStart(){
        return "basicInfo/fabricReturn";
    }

    @RequestMapping(value = "/addmanufacturefabricbatch", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> addManufactureFabricBatch(@RequestParam(value = "manufactureFabricJson", required = false)String manufactureFabricJson,
                                                         @RequestParam("orderName")String orderName,
                                                         @RequestParam(value = "manufactureAccessoryJson", required = false)String manufactureAccessoryJson){
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        if (manufactureFabricJson != null){
            List<ManufactureFabric> manufactureFabricList = gson.fromJson(manufactureFabricJson,new TypeToken<List<ManufactureFabric>>(){}.getType());
            manufactureFabricService.addManufactureFabricBatch(manufactureFabricList);
        }
        if (manufactureAccessoryJson != null){
            List<ManufactureAccessory> manufactureAccessoryList = gson.fromJson(manufactureAccessoryJson,new TypeToken<List<ManufactureAccessory>>(){}.getType());
            manufactureAccessoryService.deleteManufactureAccessoryByAccessoryType(orderName, "扁机", 0, null);
            int maxAccessoryKey = manufactureAccessoryService.getMaxAccessoryKey(orderName, 0);
            for (ManufactureAccessory manufactureAccessory : manufactureAccessoryList){
                manufactureAccessory.setAccessoryKey(maxAccessoryKey + 1 + manufactureAccessory.getAccessoryKey());
                manufactureAccessory.setCheckState("未提交");
                manufactureAccessory.setPreCheck("未提交");
            }
            manufactureAccessoryService.addManufactureAccessoryBatch(manufactureAccessoryList);
        }
        map.put("result", 0);
        return map;
    }

    @RequestMapping(value = "/getmanufacturefabricbyorder", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getManufactureFabricByOrder(@RequestParam("orderName")String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<ManufactureFabric> manufactureFabricList = manufactureFabricService.getManufactureFabricByOrder(orderName, null);
        map.put("data", manufactureFabricList);
        map.put("count", manufactureFabricList.size());
        map.put("code", 0);
        return map;
    }

    @RequestMapping(value = "/deletemanufacturefabricbyorder", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> deleteManufactureFabricByName(@RequestParam("orderName")String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        int res = manufactureFabricService.deleteManufactureFabricByName(orderName);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/deletemanufacturefabricbyid", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> deleteManufactureFabricByID(@RequestParam("fabricID")Integer fabricID){
        Map<String, Object> map = new LinkedHashMap<>();
        int res = manufactureFabricService.deleteManufactureFabricByID(fabricID);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/deletemanufacturefabricbatch", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> deleteManufactureFabricBatch(@RequestParam("fabricIDList")List<Integer> fabricIDList){
        Map<String, Object> map = new LinkedHashMap<>();
        List<FabricDetail> fabricDetailList = fabricDetailService.getFabricDetailByFabricIDList(fabricIDList);
        if (fabricDetailList != null && !fabricDetailList.isEmpty()){
            map.put("result", 3);
            return map;
        }
        int res = manufactureFabricService.deleteManufactureFabricBatch(fabricIDList);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/updatemanufacturefabric", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> updateManufactureFabric(ManufactureFabric manufactureFabric){
        Map<String, Object> map = new LinkedHashMap<>();
        int res = manufactureFabricService.updateManufactureFabric(manufactureFabric);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/getmanufacturefabrichintbyfabricname", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getManufactureFabricHintByFabricName(@RequestParam("subFabricName")String subFabricName
            ,@RequestParam("page")Integer page,@RequestParam("limit")Integer limit){
        Map<String, Object> map = new LinkedHashMap<>();
        List<ManufactureFabric> countList = manufactureFabricService.getManufactureFabricHintByFabricName(subFabricName,null,null);
        List<ManufactureFabric> manufactureFabricList = manufactureFabricService.getManufactureFabricHintByFabricName(subFabricName,(page-1)*limit,limit);
        Collections.reverse(manufactureFabricList);
        map.put("data",manufactureFabricList);
        map.put("msg","");
        map.put("count",countList.size());
        map.put("code",0);
        return map;
    }

    @RequestMapping(value = "/getmanufacturefabricplaceorderinfo", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getManufactureFabricPlaceOrderInfo(@RequestParam("orderName")String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> supplierList = manufactureFabricService.getDistinctSupplier();
        List<ManufactureFabric> manufactureFabricList = manufactureFabricService.getManufactureFabricByOrderCheckState(orderName, "未提交");
        List<OrderClothes> orderClothesList = orderClothesService.getOrderSummaryByColor(orderName);
        map.put("manufactureFabricList", manufactureFabricList);
        map.put("orderClothesList", orderClothesList);
        map.put("supplierList", supplierList);
        return map;
    }

    @RequestMapping(value = "/getmanufacturefabricplaceorderinfobyorderlist", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getManufactureFabricPlaceOrderInfoByOrderList(@RequestParam("orderNameList")List<String> orderNameList){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> supplierList = manufactureFabricService.getDistinctSupplier();
        List<ManufactureFabric> manufactureFabricList = manufactureFabricService.getManufactureFabricByOrderListCheckState(orderNameList, "未提交");
        List<OrderClothes> orderClothesList = orderClothesService.getOrderListSummaryByColor(orderNameList);
        map.put("manufactureFabricList", manufactureFabricList);
        map.put("orderClothesList", orderClothesList);
        map.put("supplierList", supplierList);
        return map;
    }


    @RequestMapping(value = "/addmanufacturefabricplaceorderbatch", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> addManufactureFabricPlaceOrderBatch(@RequestParam("manufactureFabricJson")String manufactureFabricJson){
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        List<ManufactureFabric> manufactureFabricList = gson.fromJson(manufactureFabricJson,new TypeToken<List<ManufactureFabric>>(){}.getType());
        int res = manufactureFabricService.updateManufactureFabricOnPlaceOrder(manufactureFabricList);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/getoneyearmanufacturefabriclay", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> getOneYearManufactureFabricLay(@RequestParam(value = "orderName", required = false)String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<ManufactureFabric> manufactureFabricList = manufactureFabricService.getManufactureFabricOneYear(orderName);
        map.put("data", manufactureFabricList);
        map.put("code", 0);
        map.put("count", manufactureFabricList.size());
        return map;
    }

    @RequestMapping(value = "/getmanufacturefabricbyid", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getManufactureFabricByID(@RequestParam("fabricID")Integer fabricID){
        Map<String, Object> map = new LinkedHashMap<>();
        ManufactureFabric manufactureFabric =  manufactureFabricService.getManufactureFabricByID(fabricID);
        map.put("manufactureFabric", manufactureFabric);
        return map;
    }

    @RequestMapping(value = "/getdistinctfabricsupplier", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getDistinctSupplier(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> supplierList = manufactureFabricService.getDistinctSupplier();
        map.put("supplierList", supplierList);
        return map;
    }

    @RequestMapping(value = "/changefabricorderstateinplaceorder", method = RequestMethod.POST)
    @ResponseBody
    public int changeFabricOrderStateInPlaceOrder(@RequestParam("fabricIDList[]")List<Integer> fabricIDList){
        return manufactureFabricService.changeFabricOrderStateInPlaceOrder(fabricIDList);
    }

    @RequestMapping(value = "/updatemanufacturefabricbatch", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> updateManufactureFabricBatch(@RequestParam("manufactureFabricJson")String manufactureFabricJson){
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        List<ManufactureFabric> manufactureFabricList = gson.fromJson(manufactureFabricJson,new TypeToken<List<ManufactureFabric>>(){}.getType());
        int res = manufactureFabricService.updateManufactureFabricBatch(manufactureFabricList);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/changefabriccheckstateinplaceorderpre", method = RequestMethod.POST)
    @ResponseBody
    public int changeFabricOrderStateInPlaceOrderPre(@RequestParam("fabricIDList")List<Integer> fabricIDList,
                                                  @RequestParam(value = "preCheck", required = false)String preCheck,
                                                  @RequestParam(value = "userName", required = false)String userName,
                                                  @RequestParam(value = "checkState", required = false)String checkState){
        log.warn("面料预审核:"+fabricIDList+"---操作员工："+userName);
        return manufactureFabricService.changeFabricCheckStateInPlaceOrder(fabricIDList, preCheck, checkState, userName, null);
    }

    @RequestMapping(value = "/changefabriccheckstateinplaceordercheck", method = RequestMethod.POST)
    @ResponseBody
    public int changeFabricOrderStateInPlaceOrderCheck(@RequestParam("fabricIDList")List<Integer> fabricIDList,
                                                  @RequestParam(value = "preCheck", required = false)String preCheck,
                                                  @RequestParam(value = "userName", required = false)String userName,
                                                  @RequestParam(value = "checkState", required = false)String checkState){
        log.warn("面料审核:"+fabricIDList+"---操作员工："+userName);
        return manufactureFabricService.changeFabricCheckStateInPlaceOrder(fabricIDList, preCheck, checkState, null, userName);
    }


    @RequestMapping(value = "/getfabricnamehint", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getVersionHint(@RequestParam("keywords")String keywords){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> fabricNameList = manufactureFabricService.getFabricNameHint(keywords);
        map.put("content",fabricNameList);
        map.put("type","success");
        map.put("code",0);
        return map;
    }

    @RequestMapping(value = "/updatemanufacturefabricaftercommit", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> updateManufactureAccessoryAfterCommit(@RequestParam("manufactureFabricJson")String manufactureFabricJson){
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        List<ManufactureFabric> manufactureFabricList = gson.fromJson(manufactureFabricJson,new TypeToken<List<ManufactureFabric>>(){}.getType());
        int res = manufactureFabricService.updateManufactureFabricAfterCommit(manufactureFabricList);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/getcutfabricbyordername", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getCutFabricByOrderName(@RequestParam("orderName")String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<ManufactureFabric> manufactureFabricList = manufactureFabricService.getManufactureFabricByOrder(orderName, null);
        List<String> colorList = orderClothesService.getOrderColorNamesByOrder(orderName);
        List<String> fabricNameList = manufactureFabricService.getDistinctFabricNameByOrder(orderName);
        List<String> sizeList = orderClothesService.getOrderSizeNamesByOrder(orderName);
        List<OrderClothes> orderClothesList = orderClothesService.getOrderClothesByOrderName(orderName);
        ManufactureOrder manufactureOrder = manufactureOrderService.getManufactureOrderByOrderName(orderName);
        map.put("manufactureOrder", manufactureOrder);
        map.put("colorList", colorList);
        map.put("sizeList", sizeList);
        map.put("fabricNameList", fabricNameList);
        map.put("manufactureFabricList", manufactureFabricList);
        map.put("orderClothesList", orderClothesList);
        return map;
    }

    @RequestMapping(value = "/fabricreadd", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> fabricReAdd(@RequestParam("fabricID")Integer fabricID,
                                           @RequestParam("weight")Float weight,
                                           @RequestParam("remark")String remark){
        Map<String, Object> map = new LinkedHashMap<>();
        ManufactureFabric manufactureFabric = manufactureFabricService.getManufactureFabricByID(fabricID);
        manufactureFabric.setRemark(remark);
        manufactureFabric.setFabricActCount(weight);
        manufactureFabric.setFabricCount(weight);
        manufactureFabric.setFabricAdd(0f);
        manufactureFabric.setOrderCount(manufactureFabric.getOrderCount());
        manufactureFabric.setSumMoney(weight * manufactureFabric.getPrice());
        manufactureFabric.setPreCheck("已提交");
        manufactureFabric.setCheckState("未提交");
        manufactureFabric.setOrderState("未下单");
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
        String dateString = formatter.format(new Date());
        manufactureFabric.setCheckNumber(dateString);
        int res = manufactureFabricService.reAddManufactureFabric(manufactureFabric);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/getfabricchecknumberhint", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getFabricCheckNumberHint(@RequestParam("keywords")String keywords){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> checkNumberList = manufactureFabricService.getFabricCheckNumberHint(keywords);
        map.put("content",checkNumberList);
        map.put("type","success");
        map.put("code",0);
        return map;
    }


    @RequestMapping(value = "/getmanufacturefabricbychecknumber", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getManufactureFabricByCheckNumber(@RequestParam(value = "checkNumber")String checkNumber){
        Map<String, Object> map = new LinkedHashMap<>();
        List<ManufactureFabric> manufactureFabricList = manufactureFabricService.getManufactureFabricByCheckNumberState(checkNumber, null);
        map.put("data", manufactureFabricList);
        map.put("code", 0);
        map.put("count", manufactureFabricList.size());
        return map;
    }

    private void updateMessageAfterDoSomething(String orderName, String messageType){
        Map<String, Object> param = new HashMap<>();
        param.put("orderName", orderName);
        param.put("messageType", messageType);
        param.put("readType", "未完成");
        List<Message> messageList = messageService.getMessageContentByInfo(param);
        for (Message message : messageList){
            message.setReadType("已完成");
        }
        messageService.updateMessageBatch(messageList);
    }

}
