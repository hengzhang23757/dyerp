package com.example.erp01.action;

import com.example.erp01.model.SizeManage;
import com.example.erp01.service.GroupService;
import com.example.erp01.service.SizeManageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

@Controller
@RequestMapping(value = "erp")
public class SizeManageController {

    @Autowired
    private SizeManageService sizeManageService;


    @RequestMapping("/sizeManageStart")
    public String sizeManageStart(){
        return "factoryMsg/sizeManage";
    }

    @RequestMapping(value = "/getallsizenamebygroup",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getAllSize(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> sizeGroupList = new ArrayList<>();
        List<SizeManage> sizeList = sizeManageService.getAllSizeManage();
        for (SizeManage sizeManage : sizeList){
            if (!sizeGroupList.contains(sizeManage.getSizeGroup())){
                sizeGroupList.add(sizeManage.getSizeGroup());
            }
        }
        Map<String, Object> sizeGroupMap = new LinkedHashMap<>();
        for (String sizeGroup : sizeGroupList){
            List<String> sizeNameList = new ArrayList<>();
            for (SizeManage sizeManage : sizeList){
                if (sizeGroup.equals(sizeManage.getSizeGroup())){
                    sizeNameList.add(sizeManage.getSizeName());
                }
            }


            List<String> desiredOrder = Arrays.asList("XXXXXS","5XS","XXXXS","4XS","XXXS","3XS","XXS","2XS","XS","XS/25","xs","S","S/27","M","M/29","L","L/31","XL",
                    "XL/33","XXL","2XL","XXL/36","XXXL","3XL","XXXL/38","4XL","XXXXL","5XL","XXXXXL","6XL","XXXXXXL","070","075","80","085","090","095","90","100","105","110","115","110/27",
                    "120","125","120/29","130","135","130/31","140","145","140/33","150","150/36","155","160","165","170","175","180","185","190","195","200");
            Comparator<String> sizeOrder = Comparator.comparingInt(desiredOrder::indexOf);
            sizeNameList.sort(sizeOrder);
            sizeGroupMap.put(sizeGroup, sizeNameList);
        }
        map.put("size",sizeGroupMap);
        return map;
    }



    @RequestMapping(value = "/getallsizenamemanage",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> getAllSizeManage(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<SizeManage> sizeList = sizeManageService.getAllSizeManage();
        map.put("data", sizeList);
        map.put("code", 0);
        map.put("count", sizeList.size());
        return map;
    }

    @RequestMapping(value = "/addsizemanageone",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> addSizeManageOne(@RequestParam("sizeGroup")String sizeGroup,
                                                @RequestParam("sizeName")String sizeName){
        Map<String, Object> map = new LinkedHashMap<>();
        SizeManage sizeManage = new SizeManage(sizeGroup, sizeName);
        int res = sizeManageService.addSizeManageOne(sizeManage);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/deletesizemanagebyid",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> addSizeManageOne(@RequestParam("sizeManageID")Integer sizeManageID){
        Map<String, Object> map = new LinkedHashMap<>();
        int res = sizeManageService.deleteSizeManageById(sizeManageID);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/getallsizenamefromsizemanage",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getAllSizeName(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> sizeNameList = sizeManageService.getAllSizeName();
        map.put("sizeNameList", sizeNameList);
        return map;
    }

}
