package com.example.erp01.action;

import com.example.erp01.model.*;
import com.example.erp01.service.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
@RequestMapping(value = "erp")
public class AccessoryPreStoreController {

    @Autowired
    private AccessoryPreStoreService accessoryPreStoreService;
    @Autowired
    private ManufactureAccessoryService manufactureAccessoryService;
    @Autowired
    private OrderClothesService orderClothesService;
    @Autowired
    private TailorService tailorService;
    @Autowired
    private AccessoryInStoreService accessoryInStoreService;
    @Autowired
    private AccessoryOutRecordService accessoryOutRecordService;
    @Autowired
    private CheckMonthService checkMonthService;

    @RequestMapping("/accessoryPreStoreStart")
    public String accessoryPreStoreStart(){
        return "accessory/accessoryPreStore";
    }

    @RequestMapping(value = "/getpreaccessoryhintbyname", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getPreAccessoryHintByName(@RequestParam("subAccessoryName")String subAccessoryName
                                                         , @RequestParam("page")Integer page, @RequestParam("limit")Integer limit){
        Map<String, Object> map = new LinkedHashMap<>();
        List<AccessoryPreStore> countList = accessoryPreStoreService.getAccessoryPreStoreHint(subAccessoryName,null,null);
        List<AccessoryPreStore> preAccessoryList = accessoryPreStoreService.getAccessoryPreStoreHint(subAccessoryName,(page-1)*limit,limit);
        Collections.reverse(preAccessoryList);
        map.put("data",preAccessoryList);
        map.put("msg","");
        map.put("count",countList.size());
        map.put("code",0);
        return map;
    }

    @RequestMapping(value = "/addaccessoryprestorebatch", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> addManufactureAccessoryBatch(@RequestParam("accessoryPreStoreJson")String accessoryPreStoreJson){
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        List<AccessoryPreStore> accessoryPreStoreList = gson.fromJson(accessoryPreStoreJson,new TypeToken<List<AccessoryPreStore>>(){}.getType());
        int res = accessoryPreStoreService.addAccessoryPreStoreBatch(accessoryPreStoreList);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/getaccessoryprestorageindex", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getAccessoryPreStorageIndex(@RequestParam("operateType")String operateType,
                                                           @RequestParam(value = "accessoryName", required = false)String accessoryName,
                                                           @RequestParam(value = "accessoryNumber", required = false)String accessoryNumber){
        Map<String, Object> map = new LinkedHashMap<>();
        Map<String, Object> param = new HashMap<>();
        param.put("operateType", operateType);
        param.put("accessoryName", accessoryName);
        param.put("accessoryNumber", accessoryNumber);
        List<AccessoryPreStore> preAccessoryList = accessoryPreStoreService.getAccessoryPreStoreByInfo(param);
        map.put("data",preAccessoryList);
        map.put("count",preAccessoryList.size());
        map.put("code",0);
        return map;
    }


    @RequestMapping(value = "/deleteaccessoryprestorefromroot", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> deleteAccessoryPreStoreFromRoot(@RequestParam("id")Integer id){
        Map<String, Object> map = new LinkedHashMap<>();
        List<AccessoryPreStore> accessoryPreStoreList = accessoryPreStoreService.getAccessoryPreStoreByPreId(id);
        if (accessoryPreStoreList != null && !accessoryPreStoreList.isEmpty()){
            map.put("result", 4);
            return map;
        }
        int res1 = accessoryPreStoreService.deleteAccessoryPreStoreById(id);
        int res2 = accessoryPreStoreService.deleteAccessoryPreStoreByPreId(id);
        map.put("result",res1 + res2);
        return map;
    }


    @RequestMapping(value = "/addaccessoryprestoredirectorder", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> addAccessoryPreStoreDirectOrder(@RequestParam("id")Integer id,
                                                               @RequestParam("accessoryCount")Float accessoryCount,
                                                               @RequestParam("price")Float price,
                                                               @RequestParam("checkNumber")String checkNumber){
        Map<String, Object> map = new LinkedHashMap<>();
        AccessoryPreStore accessoryPreStore = accessoryPreStoreService.getAccessoryPreStoreById(id);
        accessoryPreStore.setOperateType("order");
        accessoryPreStore.setPrice(price);
        accessoryPreStore.setPreStoreId(id);
        accessoryPreStore.setAccessoryCount(accessoryCount);
        accessoryPreStore.setCheckNumber(checkNumber);
        accessoryPreStore.setPreCheck("已提交");
        accessoryPreStore.setCheckState("未提交");
        int res = accessoryPreStoreService.addAccessoryPreStore(accessoryPreStore);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/addaccessoryprestorepreorder", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> addAccessoryPreStorePreOrder(@RequestParam("id")Integer id,
                                                            @RequestParam("accessoryCount")Float accessoryCount,
                                                            @RequestParam("price")Float price){
        Map<String, Object> map = new LinkedHashMap<>();
        AccessoryPreStore accessoryPreStore = accessoryPreStoreService.getAccessoryPreStoreById(id);
        accessoryPreStore.setOperateType("order");
        accessoryPreStore.setPrice(price);
        accessoryPreStore.setPreStoreId(id);
        accessoryPreStore.setAccessoryCount(accessoryCount);
        accessoryPreStore.setPreCheck("未提交");
        accessoryPreStore.setCheckState("待提交");
        int res = accessoryPreStoreService.addAccessoryPreStore(accessoryPreStore);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/getaccessoryprestorecombine", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getAccessoryPreStoreCombine(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<AccessoryPreStore> accessoryPreStoreList = accessoryPreStoreService.getAccessoryPreStoreForCombine();
        map.put("accessoryPreStoreList",accessoryPreStoreList);
        return map;
    }

    @RequestMapping(value = "/accessoryprestorecombineorder", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> accessoryPreStoreCombineOrder(@RequestParam("accessoryPreStoreJson")String accessoryPreStoreJson){
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        List<AccessoryPreStore> accessoryPreStoreList = gson.fromJson(accessoryPreStoreJson,new TypeToken<List<AccessoryPreStore>>(){}.getType());
        int res = accessoryPreStoreService.updateAccessoryPreStoreBatchInCombineOrder(accessoryPreStoreList);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/changeaccessoryprestoreincheck", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> changeAccessoryPreStoreInCheck(@RequestParam("accessoryPreStoreJson")String accessoryPreStoreJson){
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        List<AccessoryPreStore> accessoryPreStoreList = gson.fromJson(accessoryPreStoreJson,new TypeToken<List<AccessoryPreStore>>(){}.getType());
        int res = accessoryPreStoreService.updateAccessoryPreStoreBatchInCombineOrder(accessoryPreStoreList);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/changeaccessoryprestoreorderstate", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> changeAccessoryPreStoreOrderState(@RequestParam("checkNumber")String checkNumber,
                                                                 @RequestParam("orderState")String orderState){
        Map<String, Object> map = new LinkedHashMap<>();
        int res = accessoryPreStoreService.updateAccessoryPreStoreOrderState(checkNumber, orderState);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/changeaccessoryprestorecheckstate", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> changeAccessoryPreStoreCheckState(@RequestParam("checkNumber")String checkNumber,
                                                                 @RequestParam("checkState")String checkState){
        Map<String, Object> map = new LinkedHashMap<>();
        int res = accessoryPreStoreService.updateAccessoryPreStoreCheckStateByCheckNumber(checkNumber, checkState);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/updateaccessoryprestorebyid", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> updateAccessoryPreStoreById(AccessoryPreStore accessoryPreStore){
        Map<String, Object> map = new LinkedHashMap<>();
        int res = accessoryPreStoreService.updateAccessoryPreStore(accessoryPreStore);
        map.put("result",res);
        return map;
    }

    @RequestMapping(value = "/deleteaccessoryprestorebyid", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> deleteAccessoryPreStoreById(@RequestParam("id")Integer id){
        Map<String, Object> map = new LinkedHashMap<>();
        int res = accessoryPreStoreService.deleteAccessoryPreStoreById(id);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/getaccessoryprestorebyid", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getAccessoryPreStoreById(@RequestParam("id")Integer id){
        Map<String, Object> map = new LinkedHashMap<>();
        AccessoryPreStore accessoryPreStore = accessoryPreStoreService.getAccessoryPreStoreById(id);
        map.put("accessoryPreStore",accessoryPreStore);
        return map;
    }


    @RequestMapping(value = "/getaccessoryprestorebytypepreid", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getAccessoryPreStoreById(@RequestParam("operateType")String operateType,
                                                        @RequestParam("preStoreId")Integer preStoreId){
        Map<String, Object> map = new LinkedHashMap<>();
        Map<String, Object> param = new HashMap<>();
        param.put("operateType", operateType);
        param.put("preStoreId", preStoreId);
        List<AccessoryPreStore> accessoryPreStoreList = accessoryPreStoreService.getAccessoryPreStoreByInfo(param);
        map.put("data", accessoryPreStoreList);
        map.put("count", accessoryPreStoreList.size());
        map.put("code",0);
        return map;
    }

    @RequestMapping(value = "/getaccessoryprestorebychecknumber", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getAccessoryPreStoreByCheckNumber(@RequestParam("checkNumber")String checkNumber){
        Map<String, Object> map = new LinkedHashMap<>();
        List<AccessoryPreStore> accessoryPreStoreList = accessoryPreStoreService.getAccessoryPreStoreByCheckNumber(checkNumber);
        for (AccessoryPreStore accessoryPreStore : accessoryPreStoreList){
            accessoryPreStore.setSumMoney(accessoryPreStore.getAccessoryCount() * accessoryPreStore.getPrice());
        }
        map.put("accessoryPreStoreList",accessoryPreStoreList);
        return map;
    }


    @RequestMapping(value = "/getaccessoryprestorebypreid", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getAccessoryPreStoreByPreId(@RequestParam("preStoreId")Integer preStoreId){
        Map<String, Object> map = new LinkedHashMap<>();
        List<AccessoryPreStore> accessoryPreStoreList = accessoryPreStoreService.getAccessoryPreStoreByPreId(preStoreId);
        map.put("data", accessoryPreStoreList);
        map.put("count", accessoryPreStoreList.size());
        map.put("code",0);
        return map;
    }

    @RequestMapping(value = "/getaccessoryprestorebyaccessorynumbertype", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getAccessoryPreStoreByAccessoryNumberType(@RequestParam("accessoryNumber")String accessoryNumber,
                                                                         @RequestParam("operateType")String operateType){
        Map<String, Object> map = new LinkedHashMap<>();
        List<AccessoryPreStore> accessoryPreStoreList = accessoryPreStoreService.getAccessoryPreStoreByAccessoryNumber(accessoryNumber, operateType);
        map.put("data", accessoryPreStoreList);
        map.put("count", accessoryPreStoreList.size());
        map.put("code",0);
        return map;
    }

    @RequestMapping(value = "/addaccessoryprestore", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> addAccessoryPreStore(@RequestParam("accessoryPreStoreJson")String accessoryPreStoreJson){
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        AccessoryPreStore accessoryPreStore = gson.fromJson(accessoryPreStoreJson, AccessoryPreStore.class);
        AccessoryPreStore accessoryPreStorage = gson.fromJson(accessoryPreStoreJson, AccessoryPreStore.class);
        List<AccessoryPreStore> accessoryPreStoreExitList = accessoryPreStoreService.getAccessoryPreStoreByAccessoryNumber(accessoryPreStore.getAccessoryNumber(), "storage");
        if (accessoryPreStoreExitList != null && !accessoryPreStoreExitList.isEmpty()){
            map.put("result", 4);
            return map;
        }
        accessoryPreStore.setOperateType("in");
        accessoryPreStorage.setStorageCount(accessoryPreStorage.getAccessoryCount());
        accessoryPreStorage.setOperateType("storage");
        int res1 = accessoryPreStoreService.addAccessoryPreStore(accessoryPreStorage);
        if (res1 == 0){
            List<AccessoryPreStore> accessoryPreStoreList = accessoryPreStoreService.getAccessoryPreStoreByAccessoryNumber(accessoryPreStore.getAccessoryNumber(), "storage");
            int preId = accessoryPreStoreList.get(0).getId();
            accessoryPreStore.setPreStoreId(preId);
            int res2 = accessoryPreStoreService.addAccessoryPreStore(accessoryPreStore);
            map.put("result", res2);
            return map;
        } else {
            map.put("result", 1);
            return map;
        }
    }

    @RequestMapping(value = "/addaccessoryprestoreindex", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> addAccessoryPreStoreIndex(@RequestParam("accessoryPreStoreJson")String accessoryPreStoreJson){
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        AccessoryPreStore accessoryPreStore = gson.fromJson(accessoryPreStoreJson, AccessoryPreStore.class);
        Map<String, Object> param = new HashMap<>();
        param.put("accessoryNumber", accessoryPreStore.getAccessoryNumber());
        param.put("accessoryColor", accessoryPreStore.getAccessoryColor());
        param.put("specification", accessoryPreStore.getSpecification());
        List<AccessoryPreStore> accessoryPreStoreList = accessoryPreStoreService.getHistoryIndexAccessoryPreStoreByInfo(param);
        if (accessoryPreStoreList != null && !accessoryPreStoreList.isEmpty()){
            map.put("result", 4);
            return map;
        }
        accessoryPreStore.setOperateType("index");
        int res = accessoryPreStoreService.addAccessoryPreStore(accessoryPreStore);
        map.put("result", res);
        return map;
    }


    @RequestMapping(value = "/addaccessoryprestoreinstore", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> addAccessoryPreStore(@RequestParam("accessoryCount")Float accessoryCount,
                                                    @RequestParam("accessoryLocation")String accessoryLocation,
                                                    @RequestParam("id")Integer id){
        Map<String, Object> map = new LinkedHashMap<>();
        AccessoryPreStore accessoryPreStore = accessoryPreStoreService.getAccessoryPreStoreById(id);
        accessoryPreStore.setAccessoryCount(accessoryPreStore.getAccessoryCount() + accessoryCount);
        accessoryPreStore.setStorageCount(accessoryPreStore.getStorageCount() + accessoryCount);
        AccessoryPreStore accessoryPreStore1 = new AccessoryPreStore(accessoryPreStore.getAccessoryName(), accessoryPreStore.getAccessoryNumber(), accessoryPreStore.getSpecification(), accessoryPreStore.getAccessoryColor(), accessoryPreStore.getAccessoryUnit(), accessoryPreStore.getSupplier(), accessoryPreStore.getPrice(), accessoryPreStore.getAccessoryCount(), accessoryPreStore.getOutStoreCount(), accessoryPreStore.getStorageCount(), accessoryLocation, id, "in");
        int res1 = accessoryPreStoreService.updateAccessoryPreStore(accessoryPreStore);
        int res2 = accessoryPreStoreService.addAccessoryPreStore(accessoryPreStore1);
        if (res1 == 0 && res2 == 0){
            map.put("result", 0);
            return map;
        } else {
            map.put("result", 1);
            return map;
        }
    }


    @RequestMapping(value = "/acessoryprestoreinstore", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> acessoryPreStoreInStore(@RequestParam("inStoreCount")Float inStoreCount,
                                                       @RequestParam("accessoryLocation")String accessoryLocation,
                                                       @RequestParam("checkNumber")String checkNumber,
                                                       @RequestParam("inStoreDate")String inStoreDate,
                                                       @RequestParam("preStoreId")Integer preStoreId) throws ParseException {
        Map<String, Object> map = new LinkedHashMap<>();
        SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");
        AccessoryPreStore accessoryPreStore = accessoryPreStoreService.getAccessoryPreStoreById(preStoreId);
        Map<String, Object> param = new HashMap<>();
        param.put("operateType", "storage");
        param.put("preStoreId", preStoreId);
        param.put("accessoryLocation", accessoryLocation);
        List<AccessoryPreStore> accessoryPreStoreList = accessoryPreStoreService.getAccessoryPreStoreByInfo(param);
        accessoryPreStore.setOperateType("in");
        accessoryPreStore.setPreStoreId(preStoreId);
        accessoryPreStore.setInStoreCount(inStoreCount);
        accessoryPreStore.setCheckNumber(checkNumber);
        accessoryPreStore.setAccessoryLocation(accessoryLocation);
        accessoryPreStore.setCreateTime(ft.parse(inStoreDate));
        int res1 = accessoryPreStoreService.addAccessoryPreStore(accessoryPreStore);
        int res2;
        if (accessoryPreStoreList == null || accessoryPreStoreList.isEmpty()){
            accessoryPreStore.setOperateType("storage");
            accessoryPreStore.setPreStoreId(preStoreId);
            accessoryPreStore.setStorageCount(inStoreCount);
            accessoryPreStore.setAccessoryLocation(accessoryLocation);
            res2 = accessoryPreStoreService.addAccessoryPreStore(accessoryPreStore);
        } else {
            AccessoryPreStore storage = accessoryPreStoreList.get(0);
            storage.setStorageCount(storage.getStorageCount() + inStoreCount);
            res2 = accessoryPreStoreService.updateAccessoryPreStore(storage);
        }
        map.put("result", res1 + res2);
        return map;
    }

    @RequestMapping(value = "/deleteaccessoryprestoreinbyid", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> acessoryPreStoreInStore(@RequestParam("id") Integer id,
                                                       @RequestParam("preStoreId")Integer preStoreId){
        Map<String, Object> map = new LinkedHashMap<>();
        AccessoryPreStore accessoryPreStore = accessoryPreStoreService.getAccessoryPreStoreById(id);
        String accessoryLocation = accessoryPreStore.getAccessoryLocation();
        Map<String, Object> param = new LinkedHashMap<>();
        param.put("operateType", "storage");
        param.put("preStoreId", preStoreId);
        param.put("accessoryLocation", accessoryLocation);
        List<AccessoryPreStore> accessoryPreStoreList = accessoryPreStoreService.getAccessoryPreStoreByInfo(param);
        if (accessoryPreStoreList == null || accessoryPreStoreList.isEmpty()){
            map.put("result", 4);
            return map;
        }
        AccessoryPreStore storage = accessoryPreStoreList.get(0);
        if (storage.getStorageCount() < accessoryPreStore.getInStoreCount()){
            map.put("result", 4);
            return map;
        }
        int res1 = accessoryPreStoreService.deleteAccessoryPreStoreById(id);
        storage.setStorageCount(storage.getStorageCount() - accessoryPreStore.getInStoreCount());
        int res2 = accessoryPreStoreService.updateAccessoryPreStore(storage);
        map.put("result", res1 + res2);
        return map;
    }

    @RequestMapping(value = "/getaccessoryprestoreforcheck", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getAccessoryPreStoreForCheck(@RequestParam(value = "orderName", required = false) String orderName,
                                                            @RequestParam(value = "preCheck", required = false) String preCheck,
                                                            @RequestParam(value = "checkState", required = false) String checkState){
        Map<String, Object> map = new LinkedHashMap<>();
        Map<String, Object> param = new HashMap<>();
        param.put("orderName", orderName);
        param.put("preCheck", preCheck);
        param.put("checkState", checkState);
        List<AccessoryPreStore> accessoryPreStoreList = accessoryPreStoreService.getAccessoryPreStoreForCheck(param);
        map.put("accessoryPreStoreList", accessoryPreStoreList);
        return map;
    }

    @RequestMapping(value = "/getaccessorynameorderbyprefix", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getAccessoryNameOrderByPreFix(@RequestParam("preFix")String preFix){
        Map<String, Object> map = new LinkedHashMap<>();
        String maxOrder = accessoryPreStoreService.getMaxAccessoryOrderByPreFix(preFix);
        int a = Integer.valueOf(maxOrder).intValue();
        String orderStr = String.format("%04d", a + 1);
        map.put("accessoryOrder", orderStr);
        return map;
    }

    @RequestMapping(value = "/updateaccessoryprestoreincheck", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> updateAccessoryPreStoreInCheck(@RequestParam("accessoryPreStoreJson")String accessoryPreStoreJson,
                                                              @RequestParam("checkMonth")String checkMonth){
        Map<String, Object> map = new LinkedHashMap<>();
        Map<String, Object> param = new HashMap<>();
        param.put("checkMonth", checkMonth);
        CheckMonth fixedMonth = checkMonthService.getCheckMonthByInfo(param);
        if (fixedMonth != null){
            map.put("result", 3);
            return map;
        }
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        List<AccessoryPreStore> accessoryPreStoreList = gson.fromJson(accessoryPreStoreJson,new TypeToken<List<AccessoryPreStore>>(){}.getType());
        int res = accessoryPreStoreService.updateAccessoryPreStoreInMonthCheck(accessoryPreStoreList);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/quitaccessoryprestorebatch", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> quitAccessoryPreStoreBatch(@RequestParam("idList")List<Integer> idList){
        Map<String, Object> map = new LinkedHashMap<>();
        int res = accessoryPreStoreService.quitCheckAccessoryPreStoreBatch(idList);
        map.put("result", res);
        return map;
    }


    @RequestMapping(value = "/updateaccessoryprestoreaftercommit", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> updateAccessoryPreStoreAfterCommit(@RequestParam("id")Integer id,
                                                                  @RequestParam("accessoryCount")Float accessoryCount,
                                                                  @RequestParam("price")Float price){
        Map<String, Object> map = new LinkedHashMap<>();
        int res = accessoryPreStoreService.updateAccessoryAfterCommit(id, accessoryCount, price);
        map.put("result", res);
        return map;
    }

}
