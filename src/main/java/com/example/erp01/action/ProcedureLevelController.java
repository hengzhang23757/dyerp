package com.example.erp01.action;

import com.example.erp01.model.ProcedureLevel;
import com.example.erp01.service.ProcedureLevelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "erp")
public class ProcedureLevelController {

    @Autowired
    private ProcedureLevelService procedureLevelService;

    @RequestMapping(value = "/procedureLevelStart")
    public String procedureLevelStart(Model model){
        model.addAttribute("bigMenuTag",9);
        model.addAttribute("menuTag",94);
        return "miniProgram/procedureLevel";
    }

    @RequestMapping(value = "/getallprocedurelevel",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getAllProcedureLevel(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<ProcedureLevel> procedureLevelList = procedureLevelService.getAllProcedureLevel();
        map.put("procedureLevelList",procedureLevelList);
        return map;
    }

    @RequestMapping(value = "/updateprocedurelevel",method = RequestMethod.POST)
    @ResponseBody
    public int addProcedureLevel(ProcedureLevel procedureLevel){
        int res = procedureLevelService.updateProcedureLevel(procedureLevel);
        return res;
    }

    @RequestMapping(value = "/getlevelvaluebyname",method = RequestMethod.GET)
    @ResponseBody
    public Float getLevelByName(@RequestParam("levelValue")String levelValue){
        Float res = procedureLevelService.getLevelValueByName(levelValue);
        if (res == null){
            res = 0f;
        }
        return res;
    }

}
