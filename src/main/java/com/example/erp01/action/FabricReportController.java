package com.example.erp01.action;

import com.example.erp01.model.*;
import com.example.erp01.service.FabricDetailService;
import com.example.erp01.service.FabricReportService;
import com.example.erp01.service.ManufactureFabricService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.SimpleDateFormat;
import java.util.*;

@Controller
@RequestMapping(value = "erp")
public class FabricReportController {

    @Autowired
    private FabricReportService fabricReportService;
    @Autowired
    private ManufactureFabricService manufactureFabricService;
    @Autowired
    private FabricDetailService fabricDetailService;

    @RequestMapping("/fabricLeakStart")
    public String fabricLeakStart(){
        return "fabric/fabricLeak";
    }

    @RequestMapping("/fabricAccessoryReportLeakStart")
    public String fabricAccessoryReportLeakStart(){
        return "fabricAccessoryLeak/fabricAccessoryLeak";
    }

    @RequestMapping("/looseFabricSearchStart")
    public String looseFabricSearchStart(){
        return "fabric/looseFabricSearch";
    }

    @RequestMapping(value = "/searchfabricleakbyorder", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> searchFabricLeakByOrder(@RequestParam("orderName")String orderName,
                                                       @RequestParam(value = "colorName", required = false)String colorName){
        Map<String, Object> map = new LinkedHashMap<>();
        // 制单的面料
        List<ManufactureFabric> manufactureFabricList = manufactureFabricService.getManufactureFabricByOrder(orderName, colorName);
        // 入库面料
        List<FabricDetail> fabricDetailList = fabricReportService.getFabricDetailByOrder(orderName, colorName);
        // 暂存面料
        List<FabricDetail> fabricDetailTmpList = fabricDetailService.getFabricDetailTmpByFabricId(null, orderName, colorName);
        List<FabricDetail> allTypeFabricDetailList = fabricReportService.getAllTypeFabricDetailByOrder(orderName, colorName);
        // 库存
        List<FabricStorage> fabricStorageList = fabricReportService.getFabricStorageByOrder(orderName, colorName);
        // 出库
        List<FabricOutRecord> fabricOutRecordList = fabricReportService.getFabricOutRecordByOrder(orderName, colorName);
        List<FabricDifference> fabricDifferenceList = new ArrayList<>();
        for (ManufactureFabric manufactureFabric : manufactureFabricList){
            String fabricName = manufactureFabric.getFabricName();
            String thisColorName = manufactureFabric.getColorName();
            String fabricColor = manufactureFabric.getFabricColor();
            String partName = manufactureFabric.getPartName();
            Float fabricCount = manufactureFabric.getFabricActCount();
            String supplier = manufactureFabric.getSupplier();
            String unit = manufactureFabric.getUnit();
            Float fabricActualCount = 0f;
            Integer batchNumber = 0;
            for (FabricDetail fabricDetail : fabricDetailList){
                if (manufactureFabric.getFabricID().equals(fabricDetail.getFabricID())){
                    fabricActualCount += fabricDetail.getWeight();
                    batchNumber += fabricDetail.getBatchNumber();
                }
            }
            for (FabricDetail fabricDetailTmp : fabricDetailTmpList){
                if (manufactureFabric.getFabricID().equals(fabricDetailTmp.getFabricID())){
                    fabricActualCount += fabricDetailTmp.getWeight();
                    batchNumber += fabricDetailTmp.getBatchNumber();
                }
            }
            for (FabricOutRecord fabricOutRecord : fabricOutRecordList){
                if (manufactureFabric.getFabricID().equals(fabricOutRecord.getFabricID()) && fabricOutRecord.getOperateType().equals("面料异常")){
                    fabricActualCount -= fabricOutRecord.getWeight();
                    batchNumber -= fabricOutRecord.getBatchNumber();
                }
            }
            FabricDifference fabricDifference = new FabricDifference(manufactureFabric.getClothesVersionNumber(), manufactureFabric.getOrderName(), fabricName, manufactureFabric.getFabricNumber(), supplier, fabricColor, partName, fabricCount, fabricActualCount,  fabricActualCount - fabricCount, batchNumber, thisColorName, unit);
            fabricDifference.setRemark(manufactureFabric.getRemark());
            fabricDifferenceList.add(fabricDifference);
        }
        map.put("fabricDifferenceList", fabricDifferenceList);
        map.put("fabricDetailList", allTypeFabricDetailList);
        map.put("fabricStorageList", fabricStorageList);
        map.put("fabricDetailTmpList", fabricDetailTmpList);
        map.put("fabricOutRecordList", fabricOutRecordList);
        return map;
    }


    @RequestMapping(value = "/searchfabricleaksummarybyorder", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> searchFabricLeakSummaryByOrder(@RequestParam("orderName")String orderName,
                                                              @RequestParam("colorName")String colorName) throws Exception{
        Map<String, Object> map = new LinkedHashMap<>();
        if ("".equals(colorName)){
            colorName = null;
        }
        List<ManufactureFabric> manufactureFabricList = manufactureFabricService.getManufactureFabricByOrder(orderName, colorName);
        List<FabricDetail> fabricDetailList = fabricReportService.getFabricDetailByOrder(orderName, colorName);
        List<FabricOutRecord> fabricOutRecordList = fabricReportService.getFabricOutRecordByOrder(orderName, colorName);
        List<FabricDifference> fabricDifferenceList = new ArrayList<>();
        for (ManufactureFabric manufactureFabric : manufactureFabricList){
            String fabricName = manufactureFabric.getFabricName();
            String thisColorName = manufactureFabric.getColorName();
            String fabricColor = manufactureFabric.getFabricColor();
            String partName = manufactureFabric.getPartName();
            Float fabricCount = manufactureFabric.getFabricActCount();
            String supplier = manufactureFabric.getSupplier();
            String unit = manufactureFabric.getUnit();
            Float fabricActualCount = 0f;
            Integer batchNumber = 0;
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");//注意月份是MM
            Date lastDate = simpleDateFormat.parse("1970-01-01");
            Float lastCount = 0f;
            for (FabricDetail fabricDetail : fabricDetailList){
                if (fabricName.equals(fabricDetail.getFabricName()) && thisColorName.equals(fabricDetail.getColorName()) && fabricColor.equals(fabricDetail.getFabricColor())){
                    fabricActualCount += fabricDetail.getWeight();
                    batchNumber += fabricDetail.getBatchNumber();
                    if (fabricDetail.getReturnTime().after(lastDate)){
                        lastDate = fabricDetail.getReturnTime();
                        lastCount = fabricDetail.getWeight();
                    }
                }
            }
            for (FabricOutRecord fabricOutRecord : fabricOutRecordList){
                if (fabricName.equals(fabricOutRecord.getFabricName()) && thisColorName.equals(fabricOutRecord.getColorName()) && fabricColor.equals(fabricOutRecord.getFabricColor()) && fabricOutRecord.getOperateType().equals("面料异常")){
                    fabricActualCount -= fabricOutRecord.getWeight();
                    batchNumber -= fabricOutRecord.getBatchNumber();
                }
            }
            FabricDifference fabricDifference = new FabricDifference(fabricName, supplier, fabricColor, partName, fabricCount, fabricActualCount,  fabricActualCount - fabricCount, batchNumber, thisColorName, unit, lastDate, lastCount);
            fabricDifferenceList.add(fabricDifference);
        }
        map.put("fabricDifferenceList", fabricDifferenceList);
        return map;
    }


    @RequestMapping(value = "/searchloosefabricbyinfo", method = RequestMethod.GET)
    public String searchLooseFabricByInfo(Model model,
                                          @RequestParam("fromDate") String fromDate,
                                          @RequestParam("toDate") String toDate,
                                          @RequestParam("orderName") String orderName){
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date from = sdf.parse(fromDate);
            Date to = sdf.parse(toDate);
            if ("".equals(orderName)){
                orderName = null;
            }
            List<LooseFabric> looseFabricList = fabricReportService.getLooseFabricByInfo(from, to, orderName);
            model.addAttribute("looseFabricList", looseFabricList);
        }catch (Exception e){
            e.printStackTrace();
        }
        return "fabric/fb_looseFabricSearchList";
    }




}
