package com.example.erp01.action;

import com.example.erp01.model.FabricDetail;
import com.example.erp01.model.FabricStorage;
import com.example.erp01.service.FabricDetailService;
import com.example.erp01.service.FabricStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "erp")
public class TmpFabricController {

    @Autowired
    private FabricDetailService fabricDetailService;
    @Autowired
    private FabricStorageService fabricStorageService;

    @RequestMapping("/tmpFabricStart")
    public String tmpFabricStart(){
        return "tmpFabric/tmpFabric";
    }

    @RequestMapping(value = "/getalltmpfabric", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getAllTmpFabric(@RequestParam(value = "orderName", required = false)String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<FabricDetail> tmpFabricList = fabricDetailService.getAllTmpFabricDetail(orderName);
        map.put("data", tmpFabricList);
        map.put("count", tmpFabricList.size());
        return map;
    }

    @RequestMapping(value = "/deletetmpfabricbyid", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> deleteTmpFabricById(@RequestParam("fabricDetailID")Integer fabricDetailID){
        Map<String, Object> map = new LinkedHashMap<>();
        int res = fabricDetailService.deleteFabricDetailTmpById(fabricDetailID);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/tmpfabrictrans", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> tmpFabricTrans(@RequestParam("fabricDetailID")Integer fabricDetailID,
                                              @RequestParam("batchNumber")Integer batchNumber,
                                              @RequestParam("weight")Float weight,
                                              @RequestParam("orderName")String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        FabricDetail tmpFabric = fabricDetailService.getTmpFabricById(fabricDetailID);
        int res1,res2;
        if (tmpFabric.getWeight().equals(weight)){
            res1 = fabricDetailService.deleteFabricDetailTmpById(fabricDetailID);
        } else {
            tmpFabric.setWeight(tmpFabric.getWeight() - weight);
            tmpFabric.setBatchNumber(tmpFabric.getBatchNumber() - batchNumber);
            res1 = fabricDetailService.updateTmpFabricDetail(tmpFabric);
        }
        tmpFabric.setBatchNumber(batchNumber);
        tmpFabric.setWeight(weight);
        tmpFabric.setRemark("暂存面料");
        res2 = fabricDetailService.addFabricDetail(tmpFabric);
        List<FabricStorage> fabricStorageList = fabricStorageService.getFabricStorageByInfo(null, null, null, null, tmpFabric.getFabricID());
        boolean flag = false;
        if (fabricStorageList != null && !fabricStorageList.isEmpty()){
            for (FabricStorage fabricStorage : fabricStorageList){
                if (!flag){
                    if (fabricStorage.getJarName().equals(tmpFabric.getJarName()) && fabricStorage.getLocation().equals(tmpFabric.getLocation())){
                        fabricStorage.setWeight(fabricStorage.getWeight() + weight);
                        fabricStorage.setBatchNumber(fabricStorage.getBatchNumber() + batchNumber);
                        fabricStorageService.updateFabricStorage(fabricStorage);
                        flag = true;
                    }
                }

            }
        }
        if (!flag){
            FabricStorage fabricStorage = new FabricStorage(tmpFabric.getLocation(), tmpFabric.getOrderName(), tmpFabric.getClothesVersionNumber(), tmpFabric.getFabricName(), tmpFabric.getFabricNumber(), tmpFabric.getFabricColor(), tmpFabric.getFabricColorNumber(), tmpFabric.getJarName(), weight, batchNumber, tmpFabric.getOperateType(), tmpFabric.getReturnTime(), tmpFabric.getColorName(), tmpFabric.getUnit(), tmpFabric.getIsHit(), tmpFabric.getSupplier(), tmpFabric.getFabricID());
            fabricStorageService.addFabricStorage(fabricStorage);
        }
        if (res1 == 0 && res2 == 0){
            map.put("result", 0);
        } else {
            map.put("result", 1);
        }
        return map;
    }

}
