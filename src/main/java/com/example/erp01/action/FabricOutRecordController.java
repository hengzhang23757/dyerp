package com.example.erp01.action;


import com.example.erp01.model.*;
import com.example.erp01.service.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

@Controller
@RequestMapping(value = "erp")
public class FabricOutRecordController {

    @Autowired
    private FabricOutRecordService fabricOutRecordService;
    @Autowired
    private FabricStorageService fabricStorageService;
    @Autowired
    private FabricService fabricService;
    @Autowired
    private FabricDetailService fabricDetailService;
    @Autowired
    private ManufactureFabricService manufactureFabricService;


    @RequestMapping("/fabricOutRecordStart")
    public String fabricOutRecordStart(){
        return "fabric/fabricOutRecord";
    }

    @RequestMapping("/fabricOutStoreStart")
    public String fabricOutStoreStart(){
        return "fabric/fabricOutStore";
    }

    @RequestMapping(value = "/addunnormalfabricoutrecord",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> addFabricOutRecord(@RequestParam("fabricOutRecordJson") String fabricOutRecordJson,
                                                  @RequestParam("initWeight") Float initWeight,
                                                  @RequestParam("initBatch") Integer initBatch){
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        Map<String, Object> map = new LinkedHashMap<>();
        FabricOutRecord fabricOutRecord = gson.fromJson(fabricOutRecordJson,new TypeToken<FabricOutRecord>(){}.getType());
        Integer fabricStorageID = fabricOutRecord.getFabricOutRecordID();
        Integer batchNumber = fabricOutRecord.getBatchNumber();
        Float weight = fabricOutRecord.getWeight();
        String jarName = fabricOutRecord.getJarName();
        String location = fabricOutRecord.getLocation();
        FabricStorage fabricStorage = fabricStorageService.getFabricStorageByOrderLocationJar(null, null, null, null, jarName, location, fabricOutRecord.getFabricID());
        //如果出库后卷数和数量为0，则删除这条记录
        if(initBatch.equals(batchNumber)){
            //首先添加库存记录
            int res7 = fabricOutRecordService.addFabricOutRecord(fabricOutRecord);
            int res8 = fabricStorageService.deleteFabricStorage(fabricStorageID);
            if (res7 == 0 && res8 == 0){
                map.put("code", 0);
                map.put("result", "出库成功");
            }else{
                map.put("code", 1);
                map.put("result", "出库失败");
            }
        }else{
            int res9 = fabricOutRecordService.addFabricOutRecord(fabricOutRecord);
            fabricStorage.setWeight(initWeight - weight);
            fabricStorage.setBatchNumber(initBatch - batchNumber);
            int res10 = fabricStorageService.updateFabricStorage(fabricStorage);
            if (res9 == 0 && res10 == 0){
                map.put("code", 0);
                map.put("result", "出库成功");
            }else{
                map.put("code", 1);
                map.put("result", "出库失败");
            }
        }
        return map;
    }

    // 有问题  要用请修改
//    @RequestMapping(value = "/fabricchangeorder",method = RequestMethod.POST)
//    @ResponseBody
//    public Map<String, Object> fabricChangeOrder(@RequestParam("fabricDetailJson") String fabricDetailJson,
//                                                 @RequestParam("toClothesVersionNumber") String toClothesVersionNumber,
//                                                 @RequestParam("toOrderName") String toOrderName,
//                                                 @RequestParam("toColorName") String toColorName,
//                                                 @RequestParam("initBatch") Integer initBatch,
//                                                 @RequestParam("initWeight") Float initWeight){
//        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
//        Map<String, Object> map = new LinkedHashMap<>();
//        FabricDetail fabricDetail = gson.fromJson(fabricDetailJson,new TypeToken<FabricDetail>(){}.getType());
//        String orderName = fabricDetail.getOrderName();
//        String colorName = fabricDetail.getColorName();
//        String fabricName = fabricDetail.getFabricName();
//        String fabricColor = fabricDetail.getFabricColor();
//        String jarName = fabricDetail.getJarName();
//        String location = fabricDetail.getLocation();
//        String operateType = fabricDetail.getOperateType();
//        Integer batchNumber = fabricDetail.getBatchNumber();
//        Float weight = fabricDetail.getWeight();
//        List<ManufactureFabric> manufactureFabricList = manufactureFabricService.getManufactureFabricByInfo(toOrderName, fabricName, toColorName, fabricColor);
//        FabricStorage fabricStorage = fabricStorageService.getFabricStorageByOrderLocationJar(null, null, null, null, jarName, location, fabricDetail.getFabricID());
//        if (fabricStorage == null){
//            map.put("error", "记录异常:库存记录为空，请先检查库存！");
//            return map;
//        }
//        if (manufactureFabricList == null || manufactureFabricList.size() == 0){
//            map.put("error", "记录异常:" + toOrderName + "款制单数据中没有该款面料，请先在制单模块添加");
//            return map;
//        }
//        if (fabricStorage.getWeight() < weight || fabricStorage.getBatchNumber() < batchNumber){
//            map.put("error", "数据异常:换款数量超过库存数量，请先检查库存！");
//            return map;
//        }
//        String info = "";
//        if ("".equals(toClothesVersionNumber) || "".equals(toOrderName) || "".equals(toColorName)){
//            map.put("error", "换款异常:换款信息为空！");
//            return map;
//        }
//        if (initBatch.equals(batchNumber)){
//            fabricDetail.setClothesVersionNumber(toClothesVersionNumber);
//            fabricDetail.setOrderName(toOrderName);
//            fabricDetail.setColorName(toColorName);
//            int res1 = fabricDetailService.updateFabricDetail(fabricDetail);
//            if (res1 == 0){
//                info += "入库记录已更换,";
//            }else{
//                info += "入库记录更换异常,";
//            }
//        }else {
//            fabricDetail.setWeight(initWeight - weight);
//            fabricDetail.setBatchNumber(initBatch - batchNumber);
//            int res2 = fabricDetailService.updateFabricDetail(fabricDetail);
//            FabricDetail fabricDetail1 = new FabricDetail(toClothesVersionNumber, toOrderName, fabricName, fabricColor, jarName, batchNumber, weight, location, "入库", fabricDetail.getReturnTime(), fabricDetail.getSupplier(), toColorName, fabricDetail.getIsHit(), fabricDetail.getUnit());
//            int res3 = fabricDetailService.addFabricDetail(fabricDetail1);
//            if (res2 == 0 && res3 == 0){
//                info += "入库已创建,";
//            }else{
//                info += "入库创建异常,";
//            }
//        }
//        if (fabricStorage.getBatchNumber().equals(batchNumber)){
//            fabricStorage.setClothesVersionNumber(toClothesVersionNumber);
//            fabricStorage.setOrderName(toOrderName);
//            fabricStorage.setColorName(toColorName);
//            int res4 = fabricStorageService.updateFabricStorage(fabricStorage);
//            if (res4 == 0){
//                info += "库存已更换,";
//            }else{
//                info += "库存更换异常,";
//            }
//
//        }else{
//            fabricStorage.setWeight(fabricStorage.getWeight() - weight);
//            fabricStorage.setBatchNumber(fabricStorage.getBatchNumber() - batchNumber);
//            int res5 = fabricStorageService.updateFabricStorage(fabricStorage);
//            FabricStorage fabricStorage1 = new FabricStorage(location, toOrderName, toClothesVersionNumber, fabricName, fabricStorage.getFabricNumber(), fabricColor, fabricStorage.getFabricColorNumber(), jarName, weight, batchNumber, "入库", fabricStorage.getReturnTime(), toColorName, fabricStorage.getUnit(), fabricStorage.getIsHit(), fabricStorage.getSupplier());
//            int res6 = fabricStorageService.addFabricStorage(fabricStorage1);
//            if (res5 == 0 && res6 == 0){
//                info += "库存已创建,";
//            }else{
//                info += "库存创建异常,";
//            }
//        }
//        map.put("trans", info);
//        return map;
//    }

    @RequestMapping(value = "/fabricbatchoutstore",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> fabricBatchOutStore(@RequestParam("fabricStorageIDList")List<Integer> fabricStorageIDList,
                                                   @RequestParam("receiver") String receiver,
                                                   @RequestParam("fabricID") Integer fabricID){
        Map<String, Object> map = new LinkedHashMap<>();
        List<FabricStorage> fabricStorageList = fabricStorageService.getFabricStorageByIdList(fabricStorageIDList);
        List<FabricOutRecord> fabricOutRecordList = new ArrayList<>();
        for (FabricStorage fabricStorage : fabricStorageList){
            FabricOutRecord fabricOutRecord = new FabricOutRecord();
            fabricOutRecord.setOrderName(fabricStorage.getOrderName());
            fabricOutRecord.setClothesVersionNumber(fabricStorage.getClothesVersionNumber());
            fabricOutRecord.setColorName(fabricStorage.getColorName());
            fabricOutRecord.setFabricName(fabricStorage.getFabricName());
            fabricOutRecord.setFabricColor(fabricStorage.getFabricColor());
            fabricOutRecord.setJarName(fabricStorage.getJarName());
            fabricOutRecord.setBatchNumber(fabricStorage.getBatchNumber());
            fabricOutRecord.setWeight(fabricStorage.getWeight());
            fabricOutRecord.setReturnTime(new Date());
            fabricOutRecord.setOperateType("出库");
            fabricOutRecord.setReceiver(receiver);
            fabricOutRecord.setLocation(fabricStorage.getLocation());
            fabricOutRecord.setUnit(fabricStorage.getUnit());
            fabricOutRecord.setIsHit(fabricStorage.getIsHit() == null ? fabricStorage.getIsHit() : "");
            fabricOutRecord.setSupplier(fabricStorage.getSupplier());
            fabricOutRecord.setFabricID(fabricID);
            fabricOutRecord.setFabricNumber(fabricStorage.getFabricNumber() == null ? fabricStorage.getFabricNumber() : "");
            fabricOutRecord.setFabricColorNumber(fabricStorage.getFabricColorNumber());
            fabricOutRecordList.add(fabricOutRecord);
        }
        int res1 = fabricOutRecordService.addFabricOutRecordBatch(fabricOutRecordList);
        int res2 = fabricStorageService.deleteFabricStorageBatch(fabricStorageIDList);
        if (res1 == 0 && res2 == 0){
            map.put("result", 0);
        } else {
            map.put("result", 1);
        }
        return map;
    }

    @RequestMapping(value = "/deletefabricoutrecord",method = RequestMethod.POST)
    @ResponseBody
    public int deleteFabricOutRecord(@RequestParam("fabricOutRecordID") Integer fabricOutRecordID){
        FabricOutRecord fabricOutRecord = fabricOutRecordService.getFabricOutRecordById(fabricOutRecordID);
        FabricStorage fabricStorage = fabricStorageService.getFabricStorageByOrderColorLocationJar(null, null, null, null, fabricOutRecord.getJarName(), fabricOutRecord.getLocation(), fabricOutRecord.getFabricID());
        ManufactureFabric manufactureFabric = manufactureFabricService.getManufactureFabricByID(fabricOutRecord.getFabricID());
        if (fabricStorage == null){
            FabricStorage fabricStorage1 = new FabricStorage(fabricOutRecord.getLocation(), fabricOutRecord.getOrderName(), fabricOutRecord.getClothesVersionNumber(), fabricOutRecord.getFabricName(), manufactureFabric.getFabricNumber() == null ? "": manufactureFabric.getFabricNumber(), manufactureFabric.getFabricColor(), manufactureFabric.getFabricColorNumber() == null ? "" : manufactureFabric.getFabricColorNumber(), fabricOutRecord.getJarName(), fabricOutRecord.getWeight(), fabricOutRecord.getBatchNumber(), "入库", new Date(), manufactureFabric.getColorName(), manufactureFabric.getUnit() == null ? "":manufactureFabric.getUnit(), manufactureFabric.getIsHit(), manufactureFabric.getSupplier() == null ? "" : manufactureFabric.getSupplier(), fabricOutRecord.getFabricID());
            fabricStorageService.addFabricStorage(fabricStorage1);
        } else {
            fabricStorage.setWeight(fabricOutRecord.getWeight() + fabricStorage.getWeight());
            fabricStorage.setBatchNumber(fabricOutRecord.getBatchNumber() + fabricStorage.getBatchNumber());
            fabricStorageService.updateFabricStorage(fabricStorage);
        }
        return fabricOutRecordService.deleteFabricOutRecord(fabricOutRecordID);
    }


    @RequestMapping(value = "/gettodayfabricoutrecord", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> getTodayFabricOutRecord(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<FabricOutRecord> fabricOutRecordList = fabricOutRecordService.getTodayFabricOutRecord();
        map.put("data", fabricOutRecordList);
        map.put("code", 0);
        map.put("count", fabricOutRecordList.size());
        return map;
    }

    @RequestMapping(value = "/getfabricoutrecordbyinfo", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getFabricOutRecordByInfo(@RequestParam("fabricID")Integer fabricID){
        Map<String, Object> map = new LinkedHashMap<>();
        List<FabricOutRecord> fabricOutRecordList = fabricOutRecordService.getFabricOutRecordByInfo(null, null, null, null, fabricID);
        map.put("data", fabricOutRecordList);
        map.put("count", fabricOutRecordList.size());
        map.put("code",0);
        return map;
    }

    @RequestMapping(value = "/getfabricoutrecordbyordertype", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getFabricOutRecordByInfo(@RequestParam("orderName")String orderName,
                                                        @RequestParam("operateType")String fabricName,
                                                        @RequestParam(value = "colorName", required = false)String colorName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<FabricOutRecord> fabricOutRecordList = fabricOutRecordService.getFabricOutRecordByOrderType(orderName, fabricName,colorName);
        map.put("fabricOutRecordList", fabricOutRecordList);
        return map;
    }

}
