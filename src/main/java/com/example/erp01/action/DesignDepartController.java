package com.example.erp01.action;

import com.example.erp01.model.DesignDepart;
import com.example.erp01.service.DesignDepartService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "erp")
public class DesignDepartController {

    @Autowired
    private DesignDepartService designDepartService;


    @RequestMapping("/designDepartStart")
    public String designDepartStart(){
        return "basicData/designDepart";
    }

    @RequestMapping(value = "/adddesigndepart",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> addDesignDepart(@RequestParam("designDepartJson")String designDepartJson){
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        DesignDepart designDepart = gson.fromJson(designDepartJson, DesignDepart.class);
        int res = designDepartService.addDesignDepart(designDepart);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/deletedesigndepart",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> deleteDesignDepart(@RequestParam("id")Integer id){
        Map<String, Object> map = new LinkedHashMap<>();
        int res = designDepartService.deleteColorManage(id);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/getalldesigndepart",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getAllDesignDepart(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<DesignDepart> designDepartList = designDepartService.getAllDesignDepart();
        map.put("data",designDepartList);
        return map;
    }

    @RequestMapping(value = "/updatedesigndepart",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> updateCustomer(@RequestParam("designDepartJson")String designDepartJson){
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        DesignDepart designDepart = gson.fromJson(designDepartJson, DesignDepart.class);
        int res = designDepartService.updateDesignDepart(designDepart);
        map.put("result", res);
        return map;
    }


}
