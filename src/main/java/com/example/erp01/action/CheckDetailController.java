package com.example.erp01.action;

import com.example.erp01.model.*;
import com.example.erp01.service.CheckDetailService;
import com.example.erp01.service.CheckRuleService;
import com.example.erp01.service.CheckService;
import com.example.erp01.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.SimpleDateFormat;
import java.util.*;

@Controller
@RequestMapping(value = "erp")
public class CheckDetailController {

    @Autowired
    private CheckDetailService checkDetailService;
    @Autowired
    private EmployeeService employeeService;
    @Autowired
    private CheckService checkService;
    @Autowired
    private CheckRuleService checkRuleService;

    @RequestMapping("/checkDetailStart")
    public String checkDetailStart(){
        return "factoryMsg/checkDetail";
    }

    @RequestMapping("/checkRuleStart")
    public String checkRuleStart(){
        return "factoryMsg/checkRule";
    }

    @RequestMapping(value = "/minigetcheckdetailbyemp",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> miniGetCheckDetailByEmp(@RequestParam("year")Integer year,
                                                       @RequestParam("month")Integer month,
                                                       @RequestParam("employeeNumber")String employeeNumber) throws Exception{
        Map<String, Object> map = new LinkedHashMap<>();
        Employee employee = employeeService.getEmpByEmpNumber(employeeNumber).get(0);
        String monthString;
        if (month < 10){
            monthString = year + "-0" + month;
        } else {
            monthString = year + "-" + month;
        }
        List<String> recordRuleList = new ArrayList<>();
        List<String> dateList = getAllDateOfMonth(year, month);
        List<CheckRule> checkRuleList = checkRuleService.getAllCheckRule();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss");
        Date in1Date = simpleDateFormat.parse("08:00:00");
        Date out1Date = simpleDateFormat.parse("12:00:00");
        Date in2Date = simpleDateFormat.parse("13:30:00");
        Date out2Date = simpleDateFormat.parse("18:00:00");
        Date in3Date = simpleDateFormat.parse("19:00:00");
        Date out3Date = simpleDateFormat.parse("22:00:00");
        for (CheckRule checkRule : checkRuleList){
            recordRuleList.add(checkRule.getCheckTime());
            if (checkRule.getWorkName().equals("in1")){
                in1Date = simpleDateFormat.parse(checkRule.getCheckTime());
            }
            if (checkRule.getWorkName().equals("out1")){
                out1Date = simpleDateFormat.parse(checkRule.getCheckTime());
            }
            if (checkRule.getWorkName().equals("in2")){
                in2Date = simpleDateFormat.parse(checkRule.getCheckTime());
            }
            if (checkRule.getWorkName().equals("out2")){
                out2Date = simpleDateFormat.parse(checkRule.getCheckTime());
            }
            if (checkRule.getWorkName().equals("in3")){
                in3Date = simpleDateFormat.parse(checkRule.getCheckTime());
            }
            if (checkRule.getWorkName().equals("out3")){
                out3Date = simpleDateFormat.parse(checkRule.getCheckTime());
            }
        }
        List<CheckDetail> checkDetailList = new ArrayList<>();
        if (employee.getCcCount() != null){
            List<AttendLog> attendLogList = checkService.getMonthAttendLogByEmp(monthString, employee.getCcCount());
            if (attendLogList != null && !attendLogList.isEmpty()){
                for (String date : dateList){
                    CheckDetail checkDetail = new CheckDetail();
                    checkDetail.setSignDate(date);
                    checkDetail.setEmployeeName(employee.getEmployeeName());
                    checkDetail.setEmployeeNumber(employee.getEmployeeNumber());
                    if (attendLogList != null && !attendLogList.isEmpty()){
                        for (AttendLog attendLog : attendLogList){
                            if (attendLog.getDateString().equals(date)){
                                Date date1 = simpleDateFormat.parse(attendLog.getDateTimeString().substring(11));
                                if (date1.before(in1Date) || (date1.before(out1Date) && checkDetail.getIn1().equals("未签到"))){
                                    checkDetail.setIn1(attendLog.getDateTimeString().substring(11));
                                }
                                if (!checkDetail.getIn1().equals("未签到")){
                                    if (date1.after(simpleDateFormat.parse(checkDetail.getIn1())) && date1.before(in3Date)){
                                        checkDetail.setOut1(attendLog.getDateTimeString().substring(11));
                                    }
                                } else {
                                    if (date1.after(in1Date) && date1.before(in3Date)){
                                        checkDetail.setOut1(attendLog.getDateTimeString().substring(11));
                                    }
                                }
                                if (!checkDetail.getOut1().equals("未签到")){
                                    if (date1.after(simpleDateFormat.parse(checkDetail.getOut1())) && date1.before(out2Date)){
                                        checkDetail.setIn2(attendLog.getDateTimeString().substring(11));
                                    }
                                } else {
                                    if (date1.after(out1Date) && date1.before(out2Date)){
                                        checkDetail.setIn2(attendLog.getDateTimeString().substring(11));
                                    }
                                }
                                if (!checkDetail.getIn2().equals("未签到")){
                                    if (date1.after(simpleDateFormat.parse(checkDetail.getIn2())) && date1.before(in3Date)){
                                        checkDetail.setOut2(attendLog.getDateTimeString().substring(11));
                                    }
                                } else {
                                    if (date1.after(in2Date) && date1.before(in3Date)){
                                        checkDetail.setOut2(attendLog.getDateTimeString().substring(11));
                                    }
                                }
                                if (!checkDetail.getOut2().equals("未签到")){
                                    if (date1.after(simpleDateFormat.parse(checkDetail.getOut2())) && date1.before(out3Date)){
                                        checkDetail.setIn3(attendLog.getDateTimeString().substring(11));
                                    }
                                } else {
                                    if (date1.after(out2Date) && date1.before(out3Date)){
                                        checkDetail.setIn3(attendLog.getDateTimeString().substring(11));
                                    }
                                }
                                if (!checkDetail.getIn3().equals("未签到")){
                                    if (date1.after(simpleDateFormat.parse(checkDetail.getIn3()))){
                                        checkDetail.setIn3(attendLog.getDateTimeString().substring(11));
                                    }
                                } else {
                                    if (date1.after(in3Date)){
                                        checkDetail.setIn3(attendLog.getDateTimeString().substring(11));
                                    }
                                }
                            }
                        }
                    }
                    checkDetailList.add(checkDetail);
                }
            }
        }
        map.put("checkDetailList", checkDetailList);
        map.put("recordRuleList", recordRuleList);
        return map;
    }

//    @RequestMapping(value = "/getchecksummarybymonth",method = RequestMethod.POST)
//    @ResponseBody
//    public Map<String, Object> getCheckSummaryByMonth(@RequestParam("month")String month,
//                                                      @RequestParam(value = "groupName", required = false)String groupName){
//        Map<String, Object> map = new LinkedHashMap<>();
//        List<DailyCheck> dailyCheckList = checkService.getDailyCheckByMonthGroup(month, groupName);
//        for (DailyCheck dailyCheck : dailyCheckList){
//            dailyCheck.setLateHour((float)dailyCheck.getLate()/3600);
//            dailyCheck.setLeaveearlyHour((float)dailyCheck.getLeaveearly()/3600);
//            dailyCheck.setWorktimeHour((float)dailyCheck.getWorktime()/3600);
//            dailyCheck.setRealtimeHour((float)dailyCheck.getRealtime()/3600);
//            dailyCheck.setAbsentHour((float)dailyCheck.getAbsent()/3600);
//        }
//        map.put("data", dailyCheckList);
//        map.put("code", 0);
//        map.put("count", dailyCheckList.size());
//        return map;
//    }


    @RequestMapping(value = "/getchecksummarybymonth",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> getCheckSummaryByMonth(@RequestParam("month")String month,
                                                      @RequestParam(value = "groupName", required = false)String groupName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<SummaryCheck> summaryCheckList = checkService.getSummaryCheckByMonthGroup(month, groupName);
        for (SummaryCheck summaryCheck : summaryCheckList){
            summaryCheck.setWorktimeHour((float)summaryCheck.getWorktime()/3600);
            summaryCheck.setRealtimeHour((float)summaryCheck.getRealtime()/3600);
            summaryCheck.setLatetimeHour((float)summaryCheck.getLatetime()/3600);
            summaryCheck.setEarlytimeHour((float)summaryCheck.getEarlytime()/3600);
            summaryCheck.setAbsenttimeHour((float)summaryCheck.getAbsenttime()/3600);
            summaryCheck.setApplytimeHour((float)summaryCheck.getApplytime()/3600);
            summaryCheck.setOvertimeHour((float)summaryCheck.getOvertime()/3600);
            summaryCheck.setHolidaytimeHour((float)summaryCheck.getHolidaytime()/3600);
        }
        map.put("data", summaryCheckList);
        map.put("code", 0);
        map.put("count", summaryCheckList.size());
        return map;
    }

    @RequestMapping(value = "/getallcheckrule",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> getAllCheckRule(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<CheckRule> checkRuleList = checkRuleService.getAllCheckRule();
        for (CheckRule checkRule : checkRuleList){
            if (checkRule.getWorkName().equals("in1")){
                checkRule.setRuleName("上午上班");
            }
            if (checkRule.getWorkName().equals("out1")){
                checkRule.setRuleName("上午下班");
            }
            if (checkRule.getWorkName().equals("in2")){
                checkRule.setRuleName("下午上班");
            }
            if (checkRule.getWorkName().equals("out2")){
                checkRule.setRuleName("下午下班");
            }
            if (checkRule.getWorkName().equals("in3")){
                checkRule.setRuleName("晚上上班");
            }
            if (checkRule.getWorkName().equals("out3")){
                checkRule.setRuleName("晚上下班");
            }
        }
        map.put("data", checkRuleList);
        map.put("code", 0);
        map.put("count", checkRuleList.size());
        return map;
    }


    @RequestMapping(value = "/updatecheckrule",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> updateCheckRule(CheckRule checkRule){
        Map<String, Object> map = new LinkedHashMap<>();
        int res = checkRuleService.updateCheckRule(checkRule);
        map.put("result", res);
        return map;
    }

//    @RequestMapping(value = "/getyesterdaycheckdetail",method = RequestMethod.GET)
//    @ResponseBody
//    public Map<String, Object> getYesterdayCheckDetail(){
//        Map<String, Object> map = new LinkedHashMap<>();
//        List<Employee> employeeList = employeeService.getAllWorkEmployee();
//        List<CheckDetail> checkDetailList = checkDetailService.getYesterdayCheckDetail();
//        List<CheckDetail> checkDetailList1 = new ArrayList<>();
//        for (CheckDetail checkDetail : checkDetailList){
//            for (Employee employee : employeeList){
//                if (employee.getEmployeeNumber() != null && checkDetail.getEmployeeNumber() != null){
//                    if (checkDetail.getEmployeeNumber().replaceAll("^(0+)", "").equals(employee.getEmployeeNumber())){
//                        checkDetail.setEmployeeName(employee.getEmployeeName());
//                        checkDetailList1.add(checkDetail);
//                    }
//                }
//
//            }
//        }
//        map.put("data", checkDetailList1);
//        map.put("code", 0);
//        map.put("count", checkDetailList1.size());
//        return map;
//    }
//
//
//    @RequestMapping(value = "/getcheckdetailbyinfo",method = RequestMethod.GET)
//    @ResponseBody
//    public Map<String, Object> getCheckDetailByInfo(@RequestParam("from")String from,
//                                                    @RequestParam("to")String to,
//                                                    @RequestParam("employeeNumber")String employeeNumber){
//        Map<String, Object> map = new LinkedHashMap<>();
//        try{
//            String en = String.format("%06d", Integer.parseInt(employeeNumber));
//            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//            Date fromDate;
//            Date toDate;
//            fromDate = sdf.parse(from);
//            toDate = sdf.parse(to);
//            List<Employee> employeeList = employeeService.getAllWorkEmployee();
//            List<CheckDetail> checkDetailList = checkDetailService.getCheckDetailByInfo(fromDate, toDate, en);
//            List<CheckDetail> checkDetailList1 = new ArrayList<>();
//            for (CheckDetail checkDetail : checkDetailList){
//                for (Employee employee : employeeList){
//                    if (employee.getEmployeeNumber() != null && checkDetail.getEmployeeNumber() != null){
//                        if (checkDetail.getEmployeeNumber().replaceAll("^(0+)", "").equals(employee.getEmployeeNumber())){
//                            checkDetail.setEmployeeName(employee.getEmployeeName());
//                            checkDetailList1.add(checkDetail);
//                        }
//                    }
//
//                }
//            }
//            map.put("data", checkDetailList1);
//            map.put("code", 0);
//            map.put("count", checkDetailList1.size());
//            return map;
//        }catch (ParseException e){
//            e.printStackTrace();
//        }
//        return map;
//    }
//
//
//    @RequestMapping(value = "/updatecheckdetail", method = RequestMethod.POST)
//    @ResponseBody
//    public Map<String, Object> updateCheckDetail(@RequestParam("checkDetailJson")String checkDetailJson){
//        Map<String, Object> map = new LinkedHashMap<>();
//        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
//        CheckDetail checkDetail = gson.fromJson(checkDetailJson,new TypeToken<CheckDetail>(){}.getType());
//        int res = checkDetailService.updateCheckDetail(checkDetail);
//        map.put("result", res);
//        return map;
//    }

    public static int differentDays(Date date1,Date date2) {
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);
        int day1= cal1.get(Calendar.DAY_OF_YEAR);
        int day2 = cal2.get(Calendar.DAY_OF_YEAR);
        int year1 = cal1.get(Calendar.YEAR);
        int year2 = cal2.get(Calendar.YEAR);
        if(year1 != year2) {
            int timeDistance = 0 ;
            for(int i = year1 ; i < year2 ; i ++) {
                if(i%4==0 && i%100!=0 || i%400==0) {
                    timeDistance += 366;
                } else {
                    timeDistance += 365;
                }
            }

            return timeDistance + (day2-day1) + 1;
        }
        else {
            return day2- day1 + 1;
        }
    }

    public static Date getSomeDay(Date date, int day){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, day);
        return calendar.getTime();
    }

    public static List<String> getAllDateOfMonth(int year, int month) throws Exception{
        Calendar cd = Calendar.getInstance();
        List<String> dateList = new ArrayList<>();
        int day = 0;
        int[] monDays = new int[] {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        if ( ( (year) % 4 == 0 && (year) % 100 != 0) ||(year) % 400 == 0) {
            day = monDays[month - 1]++;
        } else {
            day = monDays[month - 1];
        }
        for (int i = 1; i <= day; i++) {
            String dateStr;
            if (month < 10){
                dateStr = year + "-0" + month;
            } else {
                dateStr = year + "-" + month;
            }
            if (i < 10){
                dateStr += "-0" + i;
            } else {
                dateStr += "-" + i;
            }
            dateList.add(dateStr);
        }
        return dateList;
    }

}
