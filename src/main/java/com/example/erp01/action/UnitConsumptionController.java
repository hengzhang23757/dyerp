package com.example.erp01.action;

import com.example.erp01.model.*;
import com.example.erp01.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.SimpleDateFormat;
import java.util.*;

@Controller
@RequestMapping(value = "erp")
public class UnitConsumptionController {

    @Autowired
    private UnitConsumptionService unitConsumptionService;
    @Autowired
    private OrderClothesService orderClothesService;
    @Autowired
    private TailorService tailorService;
    @Autowired
    private PrintPartService printPartService;
    @Autowired
    private LooseFabricService looseFabricService;

    @RequestMapping("/unitConsumptionStart")
    public String unitConsumptionStart(){
        return "cutReport/unitConsumption";
    }

    @RequestMapping(value = "/unitconsumptionreport",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getUnitConsumptionByOrderColor(@RequestParam("orderName") String orderName,
                                                              @RequestParam(value = "colorName", required = false) String colorName){

        Map<String, Object> map = new LinkedHashMap<>();
        List<String> sizeNameList = orderClothesService.getOrderSizeNamesByOrder(orderName);
        List<String> colorNameList = new ArrayList<>();
        if (colorName != null){
            colorNameList.add(colorName);
        } else {
            colorNameList = orderClothesService.getOrderColorNamesByOrder(orderName);
        }
        List<UnitConsumption> unitConsumptionList1 = unitConsumptionService.getConsumptionSummaryOfOrder(orderName, colorName, null, 0);
        for (UnitConsumption unitConsumption1 : unitConsumptionList1){
            Float colorActConsumption = unitConsumptionService.getActConsumptionByColor(orderName, unitConsumption1.getColorName(), null, 0);
            unitConsumption1.setActTotalConsumption(colorActConsumption);
//            Integer layerCount = unitConsumptionService.getSumLayerCountOfByOrderColorName(orderName, unitConsumption1.getColorName(), null, 0);
            Integer layerCount = tailorService.getTailorCountByInfo(orderName, unitConsumption1.getColorName(), null,null,null,null, 0);
            if (layerCount == 0){
                unitConsumption1.setAverageConsumption(0f);
            } else{
                unitConsumption1.setAverageConsumption(colorActConsumption/layerCount);
            }
            String tmpJar = unitConsumption1.getJarName();
            int charLength  = tmpJar.length();
            if (unitConsumption1.getJarName().endsWith("#")){
                tmpJar = tmpJar.substring(0, tmpJar.length() -1);
            } else if (charLength >= 2 && tmpJar.charAt(charLength - 2) == '#'){
                tmpJar = tmpJar.substring(0, tmpJar.length() - 2);
            }
            Float colorTotalReturn = unitConsumptionService.getReturnFabricCountByOrderColorJar(orderName, unitConsumption1.getColorName(), tmpJar);
            Float colorPlanConsumption = unitConsumptionService.getPlanConsumptionByJar(orderName, unitConsumption1.getColorName(), tmpJar);
            unitConsumption1.setPlanConsumption(colorPlanConsumption);
            unitConsumption1.setTotalReturnCount(colorTotalReturn);
        }
        map.put("summary", unitConsumptionList1);
        List<UnitConsumption> unitConsumptionList2 = unitConsumptionService.getBasicInfoOfEachBed(orderName, colorName, null, 0);
        map.put("colorList", colorNameList);
        map.put("sizeList", sizeNameList);
        map.put("cutInfo", unitConsumptionList2);
        List<UnitConsumption> unitConsumptionList3 = unitConsumptionService.getBasicInfoOfEachBed(orderName, colorName, null, 0);
        for (String color : colorNameList){
            List<Integer> bedList = new ArrayList<>();
            boolean flag = false;
            for (UnitConsumption unitConsumption : unitConsumptionList3){
                if (color.equals(unitConsumption.getColorName())){
                    flag = true;
                    if (!bedList.contains(unitConsumption.getBedNumber())){
                        bedList.add(unitConsumption.getBedNumber());
                    }
                }
            }
            if (flag){
                map.put(color, bedList);
            }
        }
        List<UnitConsumption> unitConsumptionList4 = unitConsumptionService.getConsumptionSummaryOfEachBed(orderName, colorName, null, 0);
        for (UnitConsumption unitConsumption3 : unitConsumptionList3){
            String tmpColor = unitConsumption3.getColorName();
            Integer tmpBed = unitConsumption3.getBedNumber();
            for (UnitConsumption unitConsumption4 : unitConsumptionList4){
                if (tmpColor.equals(unitConsumption4.getColorName()) && tmpBed.equals(unitConsumption4.getBedNumber())){
                    unitConsumption3.setTotalBatch(unitConsumption4.getTotalBatch());
                    unitConsumption3.setTotalWeight(unitConsumption4.getTotalWeight());
                }
            }
        }
        map.put("bedConsumption", unitConsumptionList3);
        return map;

    }


    @RequestMapping(value = "/otherunitconsumptionreport",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getUnitConsumptionByOrderColor(@RequestParam("orderName") String orderName,
                                                              @RequestParam("partName") String partName,
                                                              @RequestParam(value = "colorName", required = false) String colorName){

        Map<String, Object> map = new LinkedHashMap<>();
        List<String> sizeNameList = orderClothesService.getOrderSizeNamesByOrder(orderName);
        List<String> colorNameList = new ArrayList<>();
        if (colorName != null){
            colorNameList.add(colorName);
        } else {
            colorNameList = orderClothesService.getOrderColorNamesByOrder(orderName);
        }
        List<UnitConsumption> unitConsumptionList1 = unitConsumptionService.getConsumptionSummaryOfOrder(orderName, colorName, partName, 1);
        for (UnitConsumption unitConsumption1 : unitConsumptionList1){
            Float colorActConsumption = unitConsumptionService.getActConsumptionByColor(orderName, unitConsumption1.getColorName(), partName, 1);
            unitConsumption1.setActTotalConsumption(colorActConsumption);
            Integer layerCount = unitConsumptionService.getSumLayerCountOfByOrderColorName(orderName, unitConsumption1.getColorName(), partName, 1);
            if (layerCount == 0){
                unitConsumption1.setAverageConsumption(0f);
            } else{
                unitConsumption1.setAverageConsumption(colorActConsumption/layerCount);
            }
            String tmpJar = unitConsumption1.getJarName();
            if (unitConsumption1.getJarName().endsWith("#")){
                tmpJar = tmpJar.substring(0, tmpJar.length() -1);
            }
            Float colorTotalReturn = unitConsumptionService.getReturnFabricCountByOrderColorJar(orderName, unitConsumption1.getColorName(), tmpJar);
            Float colorPlanConsumption = unitConsumptionService.getPlanConsumptionByJar(orderName, unitConsumption1.getColorName(), tmpJar);
            unitConsumption1.setPlanConsumption(colorPlanConsumption);
            unitConsumption1.setTotalReturnCount(colorTotalReturn);
        }
        map.put("summary", unitConsumptionList1);
        List<UnitConsumption> unitConsumptionList2 = unitConsumptionService.getBasicInfoOfEachBed(orderName, colorName, partName, 1);
        map.put("colorList", colorNameList);
        map.put("sizeList", sizeNameList);
        map.put("cutInfo", unitConsumptionList2);
        List<UnitConsumption> unitConsumptionList3 = unitConsumptionService.getBasicInfoOfEachBed(orderName, colorName, partName, 1);
        for (String color : colorNameList){
            List<Integer> bedList = new ArrayList<>();
            boolean flag = false;
            for (UnitConsumption unitConsumption : unitConsumptionList3){
                if (color.equals(unitConsumption.getColorName())){
                    flag = true;
                    if (!bedList.contains(unitConsumption.getBedNumber())){
                        bedList.add(unitConsumption.getBedNumber());
                    }
                }
            }
            if (flag){
                map.put(color, bedList);
            }
        }
        List<UnitConsumption> unitConsumptionList4 = unitConsumptionService.getConsumptionSummaryOfEachBed(orderName, colorName, partName, 1);
        for (UnitConsumption unitConsumption3 : unitConsumptionList3){
            String tmpColor = unitConsumption3.getColorName();
            Integer tmpBed = unitConsumption3.getBedNumber();
            for (UnitConsumption unitConsumption4 : unitConsumptionList4){
                if (tmpColor.equals(unitConsumption4.getColorName()) && tmpBed.equals(unitConsumption4.getBedNumber())){
                    unitConsumption3.setTotalBatch(unitConsumption4.getTotalBatch());
                    unitConsumption3.setTotalWeight(unitConsumption4.getTotalWeight());
                }
            }
        }
        map.put("bedConsumption", unitConsumptionList3);
        return map;

    }


    @RequestMapping(value = "/historyunitconsumptioninfo",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> historyUnitConsumptionInfo(@RequestParam("orderName") String orderName,
                                                          @RequestParam("colorName") String colorName,
                                                          @RequestParam("partName") String partName){

        Map<String, Object> map = new LinkedHashMap<>();
        //判断有没有裁过
        Integer colorOrderCount = orderClothesService.getColorOrderCount(orderName, colorName);
        System.out.println("colorOrderCount" + colorOrderCount);
        map.put("colorOrderCount", colorOrderCount);
        if (partName.equals("主身")){
            Integer layerCount = tailorService.getTailorCountByInfo(orderName, colorName, null,null,null,null, 0);
            Integer unFinishCount = colorOrderCount - layerCount;
            map.put("layerCount", layerCount);
            map.put("unFinishCount", unFinishCount);
            Float historyUnit = 0f;
            String jarNumber = unitConsumptionService.getMaxJarNumberByOrderColorPartType(orderName, colorName,null, 0);
            Float preCutCount = 0f;
            if (jarNumber.length() >= 2){
                if (jarNumber.endsWith("#") || jarNumber.charAt(jarNumber.length() - 2) == '#'){
                    preCutCount = looseFabricService.getPreCutFabricWeightByOrderColor(orderName, colorName, jarNumber);
                } else {
                    jarNumber = jarNumber + "#";
                    preCutCount = looseFabricService.getPreCutFabricWeightByOrderColor(orderName, colorName, jarNumber);
                }
            } else {
                preCutCount = looseFabricService.getPreCutFabricWeightByOrderColor(orderName, colorName, jarNumber);
            }
            if (layerCount.equals(0)){
                map.put("historyUnit", historyUnit);
                map.put("needFabric", 0);
                map.put("preCut", preCutCount);
                map.put("actNeed", 0);
                return map;
            } else {
                Float colorActConsumption = unitConsumptionService.getActConsumptionByColor(orderName, colorName, null, 0);
                historyUnit = colorActConsumption/layerCount;
                map.put("historyUnit", historyUnit);
                map.put("needFabric", historyUnit * unFinishCount);
                map.put("preCut", preCutCount);
                map.put("actNeed", historyUnit * unFinishCount - preCutCount);
                return map;
            }
        } else {
            Integer layerCount = tailorService.getTailorCountByInfo(orderName, colorName, null, partName,null,null, 1);
            Integer unFinishCount = colorOrderCount - layerCount;
            map.put("layerCount", layerCount);
            map.put("unFinishCount", unFinishCount);
            Float historyUnit = 0f;
            String jarNumber = unitConsumptionService.getMaxJarNumberByOrderColorPartType(orderName, colorName, partName, 1);
            Float preCutCount = looseFabricService.getPreCutFabricWeightByOrderColor(orderName, colorName, jarNumber);
            if (layerCount.equals(0)){
                map.put("historyUnit", historyUnit);
                map.put("needFabric", 0);
                map.put("preCut", preCutCount);
                map.put("actNeed", 0);
                return map;
            } else {
                Float colorActConsumption = unitConsumptionService.getActConsumptionByColor(orderName, colorName, partName, 1);
                historyUnit = colorActConsumption/layerCount;
                map.put("historyUnit", historyUnit);
                map.put("needFabric", historyUnit * unFinishCount);
                map.put("preCut", preCutCount);
                map.put("actNeed", historyUnit * unFinishCount - preCutCount);
                return map;
            }
        }
    }


}
