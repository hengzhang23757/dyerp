package com.example.erp01.action;

import com.example.erp01.model.*;
import com.example.erp01.service.InvoicingAccessoryService;
import com.example.erp01.service.InvoicingFabricService;
import com.example.erp01.service.ManufactureAccessoryService;
import com.example.erp01.service.ManufactureFabricService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

@Controller
@RequestMapping(value = "erp")
public class InvoicingController {

    @Autowired
    private InvoicingFabricService invoicingFabricService;
    @Autowired
    private ManufactureFabricService manufactureFabricService;
    @Autowired
    private InvoicingAccessoryService invoicingAccessoryService;
    @Autowired
    private ManufactureAccessoryService manufactureAccessoryService;

    @RequestMapping("/invoicingStart")
    public String invoicingStart(){
        return "invoicing/invoicing";
    }




    @RequestMapping(value = "/getinvoicingfabricbyinfo", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getInvoicingFabricByInfo(@RequestParam(value = "from", required = false)String from,
                                                        @RequestParam(value = "to", required = false)String to,
                                                        @RequestParam(value = "orderName", required = false)String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        Map<String, Object> param = new HashMap<>();
        param.put("from", from);
        param.put("to", to);
        param.put("orderName", orderName);
        List<Integer> fabricIDList = invoicingFabricService.getFabricOperateIdList(param);
        List<FabricDetail> fabricDetailListTotal = invoicingFabricService.getFabricDetailByIdList(fabricIDList);
        List<FabricDetail> fabricDetailList = invoicingFabricService.getFabricDetailByMap(param);
        List<FabricStorage> fabricStorageListTotal = invoicingFabricService.getFabricStorageByIdList(fabricIDList);
        List<FabricOutRecord> fabricOutRecordListTotal = invoicingFabricService.getFabricOutRecordByIdList(fabricIDList);
        List<FabricOutRecord> fabricOutRecordList = invoicingFabricService.getFabricOutRecordByMap(param);
        List<ManufactureFabric> manufactureFabricList = manufactureFabricService.getManufactureFabricByFabricIdList(fabricIDList);
        List<InvoicingFabric> invoicingFabricList = new ArrayList<>();
        for (ManufactureFabric manufactureFabric : manufactureFabricList){
            InvoicingFabric invoicingFabric = new InvoicingFabric(manufactureFabric.getFabricID(), manufactureFabric.getOrderName(), manufactureFabric.getClothesVersionNumber(), manufactureFabric.getFabricName(), manufactureFabric.getFabricNumber(), manufactureFabric.getFabricColor(), manufactureFabric.getFabricColorNumber(), manufactureFabric.getSupplier(), manufactureFabric.getColorName(), manufactureFabric.getUnit());
            // 区间入库
            for (FabricDetail fabricDetail : fabricDetailList){
                if (manufactureFabric.getFabricID().equals(fabricDetail.getFabricID())){
                    invoicingFabric.setInStoreBatch(invoicingFabric.getInStoreBatch() + fabricDetail.getBatchNumber());
                    invoicingFabric.setInStoreWeight(invoicingFabric.getInStoreWeight() + fabricDetail.getWeight());
                }
            }
            // 区间出库
            for (FabricOutRecord fabricOutRecord : fabricOutRecordList){
                if (manufactureFabric.getFabricID().equals(fabricOutRecord.getFabricID())){
                    invoicingFabric.setOutStoreBatch(invoicingFabric.getOutStoreBatch() + fabricOutRecord.getBatchNumber());
                    invoicingFabric.setOutStoreWeight(invoicingFabric.getOutStoreWeight() + fabricOutRecord.getWeight());
                }
            }
            // 总入库
            for (FabricDetail fabricDetailTotal : fabricDetailListTotal){
                if (manufactureFabric.getFabricID().equals(fabricDetailTotal.getFabricID())){
                    invoicingFabric.setTotalInStore(invoicingFabric.getTotalInStore() + fabricDetailTotal.getWeight());
                    invoicingFabric.setTotalInBatch(invoicingFabric.getTotalInBatch() + fabricDetailTotal.getBatchNumber());
                }
            }
            // 总库存
            for (FabricStorage fabricStorage : fabricStorageListTotal){
                if (manufactureFabric.getFabricID().equals(fabricStorage.getFabricID())){
                    invoicingFabric.setTotalStorage(invoicingFabric.getTotalStorage() + fabricStorage.getWeight());
                    invoicingFabric.setTotalStorageBatch(invoicingFabric.getTotalStorageBatch() + fabricStorage.getBatchNumber());
                }
            }
            // 总出库
            for (FabricOutRecord fabricOutRecord : fabricOutRecordListTotal){
                if (manufactureFabric.getFabricID().equals(fabricOutRecord.getFabricID())){
                    invoicingFabric.setTotalOutStore(invoicingFabric.getTotalOutStore() + fabricOutRecord.getWeight());
                    invoicingFabric.setTotalOutBatch(invoicingFabric.getTotalOutBatch() + fabricOutRecord.getBatchNumber());
                }
            }
            invoicingFabricList.add(invoicingFabric);
        }
        map.put("data", invoicingFabricList);
        map.put("code", 0);
        map.put("count", invoicingFabricList.size());
        return map;
    }

    @RequestMapping(value = "/getinvoicingaccessorybyinfo", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getInvoicingAccessoryByInfo(@RequestParam(value = "from", required = false)String from,
                                                           @RequestParam(value = "to", required = false)String to,
                                                           @RequestParam(value = "orderName", required = false)String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        Map<String, Object> param = new HashMap<>();
        param.put("from", from);
        param.put("to", to);
        param.put("orderName", orderName);
        List<Integer> accessoryIDList = invoicingAccessoryService.getAccessoryOperateIdList(param);
        List<AccessoryInStore> accessoryInStoreListTotal = invoicingAccessoryService.getAccessoryInStoreByIdList(accessoryIDList);
        List<AccessoryStorage> accessoryStorageListTotal = invoicingAccessoryService.getAccessoryStorageByIdList(accessoryIDList);
        List<AccessoryOutRecord> accessoryOutRecordListTotal = invoicingAccessoryService.getAccessoryOutRecordByIdList(accessoryIDList);
        List<AccessoryInStore> accessoryInStoreList = invoicingAccessoryService.getAccessoryInStoreByMap(param);
        List<AccessoryStorage> accessoryStorageList = invoicingAccessoryService.getAccessoryStorageByMap(param);
        List<AccessoryOutRecord> accessoryOutRecordList = invoicingAccessoryService.getAccessoryOutRecordByMap(param);
        List<ManufactureAccessory> manufactureAccessoryList = manufactureAccessoryService.getManufactureAccessoryByIdList(accessoryIDList);
        List<InvoicingAccessory> invoicingAccessoryList = new ArrayList<>();
        for (ManufactureAccessory manufactureAccessory : manufactureAccessoryList){
            InvoicingAccessory invoicingAccessory = new InvoicingAccessory(manufactureAccessory.getAccessoryID(), manufactureAccessory.getOrderName(), manufactureAccessory.getClothesVersionNumber(), manufactureAccessory.getAccessoryName(), manufactureAccessory.getAccessoryNumber(), manufactureAccessory.getSpecification(), manufactureAccessory.getAccessoryUnit(), manufactureAccessory.getAccessoryColor(), manufactureAccessory.getSizeName(), manufactureAccessory.getColorName(), manufactureAccessory.getSupplier());
            // 区间入库
            for (AccessoryInStore accessoryInStore : accessoryInStoreList){
                if (manufactureAccessory.getAccessoryID().equals(accessoryInStore.getAccessoryID())){
                    invoicingAccessory.setInStoreCount(invoicingAccessory.getInStoreCount() + accessoryInStore.getInStoreCount());
                }
            }
            // 区间出库
            for (AccessoryOutRecord accessoryOutRecord : accessoryOutRecordList){
                if (manufactureAccessory.getAccessoryID().equals(accessoryOutRecord.getAccessoryID())){
                    invoicingAccessory.setOutStoreCount(invoicingAccessory.getOutStoreCount() + accessoryOutRecord.getOutStoreCount());
                }
            }
            // 总入库
            for (AccessoryInStore accessoryInStore : accessoryInStoreListTotal){
                if (manufactureAccessory.getAccessoryID().equals(accessoryInStore.getAccessoryID())){
                    invoicingAccessory.setTotalInStore(invoicingAccessory.getTotalInStore() + accessoryInStore.getInStoreCount());
                }
            }
            // 总库存
            for (AccessoryStorage accessoryStorage : accessoryStorageListTotal){
                if (manufactureAccessory.getAccessoryID().equals(accessoryStorage.getAccessoryID())){
                    invoicingAccessory.setTotalStorage(invoicingAccessory.getTotalStorage() + accessoryStorage.getStorageCount());
                }
            }
            // 总出库
            for (AccessoryOutRecord accessoryOutRecord : accessoryOutRecordListTotal){
                if (manufactureAccessory.getAccessoryID().equals(accessoryOutRecord.getAccessoryID())){
                    invoicingAccessory.setTotalOutStore(invoicingAccessory.getTotalOutStore() + accessoryOutRecord.getOutStoreCount());
                }
            }
            invoicingAccessoryList.add(invoicingAccessory);
        }
        map.put("data", invoicingAccessoryList);
        map.put("code", 0);
        map.put("count", invoicingAccessoryList.size());
        return map;
    }


}
