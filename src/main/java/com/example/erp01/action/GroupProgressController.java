package com.example.erp01.action;

import com.example.erp01.model.GroupProgress;
import com.example.erp01.model.OrderClothes;
import com.example.erp01.service.GroupProgressService;
import com.example.erp01.service.OrderClothesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
@RequestMapping(value = "erp")
public class GroupProgressController {

    @Autowired
    private GroupProgressService groupProgressService;
    @Autowired
    private OrderClothesService orderClothesService;

    @RequestMapping(value = "/groupProgressStart")
    public String groupProgressStart(){
        return "finance/groupWorkProgress";
    }


    @RequestMapping(value = "/getgroupprogressbyinfo",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getGroupProgressByInfo(@RequestParam("from") String from,
                                                      @RequestParam("to") String to,
                                                      @RequestParam("type") Integer type,
                                                      @RequestParam("procedureName") String procedureName,
                                                      @RequestParam(value = "groupList[]", required = false) List<String> groupList,
                                                      @RequestParam(value = "orderName", required = false) String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<GroupProgress> groupProgressList1 = new ArrayList<>();
        List<Integer> procedureNumberList1 = new ArrayList<>();
        List<Integer> procedureNumberList2 = new ArrayList<>();
        List<Integer> procedureNumberList3 = new ArrayList<>();
        procedureNumberList1.add(2001);
        procedureNumberList2.add(25);
        procedureNumberList2.add(2025);
        procedureNumberList3.add(555);
        procedureNumberList3.add(6555);
        if (type == 0){
            if (procedureName.equals("查片")){
                groupProgressList1 = groupProgressService.getGroupProgressByTimeProcedure(from, to, orderName, groupList, procedureNumberList1);
            } else if (procedureName.equals("挂片")){
                groupProgressList1 = groupProgressService.getGroupProgressByTimeProcedure(from, to, orderName, groupList, procedureNumberList2);
            } else {
                groupProgressList1 = groupProgressService.getGroupProgressByTimeProcedure(from, to, orderName, groupList, procedureNumberList3);
            }
        } else {
            if (procedureName.equals("查片")){
                groupProgressList1 = groupProgressService.getGroupProgressByTimeProcedureGroupByDay(from, to, orderName, groupList, procedureNumberList1);
            } else if (procedureName.equals("挂片")){
                groupProgressList1 = groupProgressService.getGroupProgressByTimeProcedureGroupByDay(from, to, orderName, groupList, procedureNumberList2);
            } else {
                groupProgressList1 = groupProgressService.getGroupProgressByTimeProcedureGroupByDay(from, to, orderName, groupList, procedureNumberList3);
            }
            for (GroupProgress groupProgress1 : groupProgressList1) {
                groupProgress1.setProcedureName(procedureName);
            }
        }
        for (GroupProgress groupProgress1 : groupProgressList1){
            groupProgress1.setProcedureName(procedureName);
        }
        map.put("groupProgressList", groupProgressList1);
        return map;
    }




}
