package com.example.erp01.action;

import com.example.erp01.model.*;
import com.example.erp01.service.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
@RequestMapping(value = "erp")
public class ClothesDevController {

    @Autowired
    private ClothesDevService clothesDevService;
    @Autowired
    private ManufactureOrderService manufactureOrderService;
    @Autowired
    private OrderClothesService orderClothesService;
    @Autowired
    private StyleImageService styleImageService;
    @Autowired
    private OrderMeasureService orderMeasureService;
    @Autowired
    private ProcessRequirementService processRequirementService;
    @Autowired
    private ManufactureFabricService manufactureFabricService;

    @RequestMapping("/clothesDevStart")
    public String clothesDevStart(){
        return "clothesDev/clothesDev";
    }

    @RequestMapping(value = "/getclothesdevinfobymap", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getClothesDevInfoByMap(@RequestParam(value = "clothesDevNumber", required = false)String clothesDevNumber,
                                                      @RequestParam(value = "orderName", required = false)String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        Map<String, Object> param = new HashMap<>();
        param.put("clothesDevNumber", clothesDevNumber);
        param.put("orderName", orderName);
        List<ClothesDevInfo> clothesDevInfoList = clothesDevService.getClothesDevInfoByMap(param);
        map.put("data", clothesDevInfoList);
        return map;
    }



    @RequestMapping(value = "/getclothesdevinfodata", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getClothesDevInfoData(@RequestParam(value = "orderName", required = false)String orderName,
                                                          @RequestParam(value = "season", required = false)String season){
        Map<String, Object> map = new LinkedHashMap<>();
        Map<String, Object> param = new HashMap<>();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");//设置日期格式
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.YEAR, -1);
        param.put("from", df.format(calendar.getTime()));
        param.put("season", season);
        param.put("orderName", orderName);
        List<ClothesDevInfo> clothesDevInfoList = clothesDevService.getClothesDevInfoByMap(param);
        map.put("data", clothesDevInfoList);
        return map;
    }

    @RequestMapping(value = "/getpreclothesversionnumber", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getClothesDevInfoData(){
        Map<String, Object> map = new LinkedHashMap<>();
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH )+1;
        String monthStr = month < 10 ? "0" + month : String.valueOf(month);
        String preFix = "ban" + String.valueOf(year).substring(2) + monthStr + "-";
        String maxOrder = clothesDevService.getMaxClothesVersionOrder(preFix);
        int a = Integer.valueOf(maxOrder).intValue();
        String orderStr = String.format("%03d", a + 1);
        map.put("clothesDevNumber", preFix + orderStr);
        return map;
    }

    @RequestMapping(value = "/getclothesdevaccessoryhint", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getClothesDevAccessoryHint(@RequestParam("subAccessoryName")String subAccessoryName
            ,@RequestParam("page")Integer page,@RequestParam("limit")Integer limit){
        Map<String, Object> map = new LinkedHashMap<>();
        List<ClothesDevAccessory> countList = clothesDevService.getClothesDevAccessoryHint(subAccessoryName,null,null);
        List<ClothesDevAccessory> clothesDevAccessoryHint = clothesDevService.getClothesDevAccessoryHint(subAccessoryName,(page-1)*limit,limit);
        Collections.reverse(clothesDevAccessoryHint);
        map.put("data",clothesDevAccessoryHint);
        map.put("msg","");
        map.put("count",countList.size());
        map.put("code",0);
        return map;
    }


    @RequestMapping(value = "/getclothesdevordernamebyprefix", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getClothesDevOrderNameByPrefix(@RequestParam("preFix")String preFix){
        Map<String, Object> map = new LinkedHashMap<>();
        String maxOrder = clothesDevService.getClothesDevOrderNameByPreFix(preFix);
        int a = Integer.valueOf(maxOrder).intValue();
        String orderStr = String.format("%04d", a + 1);
        map.put("orderName", preFix + orderStr);
        return map;
    }

    @RequestMapping(value = "/addclothesdevtotal",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> addClothesDevTotal(@RequestParam("clothesDevInfoJson")String clothesDevInfoJson,
                                                  @RequestParam("clothesDevImageJson")String clothesDevImageJson,
                                                  @RequestParam("clothesDevProcessJson")String clothesDevProcessJson,
                                                  @RequestParam("clothesDevMeasureJson")String clothesDevMeasureJson,
                                                  @RequestParam("clothesDevColorListJson")String clothesDevColorListJson,
                                                  @RequestParam("clothesDevSizeListJson")String clothesDevSizeListJson,
                                                  @RequestParam("clothesDevAccessoryListJson")String clothesDevAccessoryListJson,
                                                  @RequestParam("clothesDevFabricListJson")String clothesDevFabricListJson){
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        ClothesDevInfo clothesDevInfo = gson.fromJson(clothesDevInfoJson, ClothesDevInfo.class);
        ClothesDevImage clothesDevImage = gson.fromJson(clothesDevImageJson, ClothesDevImage.class);
        ClothesDevProcess clothesDevProcess = gson.fromJson(clothesDevProcessJson, ClothesDevProcess.class);
        ClothesDevMeasure clothesDevMeasure = gson.fromJson(clothesDevMeasureJson, ClothesDevMeasure.class);
        List<ClothesDevColor> clothesDevColorList = gson.fromJson(clothesDevColorListJson,new TypeToken<List<ClothesDevColor>>(){}.getType());
        List<ClothesDevSize> clothesDevSizeList = gson.fromJson(clothesDevSizeListJson,new TypeToken<List<ClothesDevSize>>(){}.getType());
        List<ClothesDevAccessory> clothesDevAccessoryList = gson.fromJson(clothesDevAccessoryListJson,new TypeToken<List<ClothesDevAccessory>>(){}.getType());
        List<ClothesDevFabric> clothesDevFabricList = gson.fromJson(clothesDevFabricListJson,new TypeToken<List<ClothesDevFabric>>(){}.getType());
        int res = 0;
        res += clothesDevService.addClothesDevInfo(clothesDevInfo);
        res += clothesDevService.addClothesDevImage(clothesDevImage);
        res += clothesDevService.addClothesDevProcess(clothesDevProcess);
        res += clothesDevService.addClothesDevMeasure(clothesDevMeasure);
        res += clothesDevService.addClothesDevColorBatch(clothesDevColorList);
        res += clothesDevService.addClothesDevSizeBatch(clothesDevSizeList);
        res += clothesDevService.addClothesDevAccessoryBatch(clothesDevAccessoryList);
        res += clothesDevService.addClothesDevFabricBatch(clothesDevFabricList);
        if (res == 0){
            map.put("result", 0);
        } else {
            map.put("result", 1);
        }
        return map;
    }

    @RequestMapping(value = "/deleteclothesdevtotal",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> deleteClothesDevTotal(@RequestParam("clothesDevNumber")String clothesDevNumber){
        Map<String, Object> map = new LinkedHashMap<>();
        int res = 0;
        res += clothesDevService.deleteClothesDevInfoByDevNumber(clothesDevNumber);
        res += clothesDevService.deleteClothesDevImageByDevNumber(clothesDevNumber);
        res += clothesDevService.deleteClothesDevFabricByDevNumber(clothesDevNumber);
        res += clothesDevService.deleteClothesDevAccessoryByDevNumber(clothesDevNumber);
        res += clothesDevService.deleteClothesDevSizeByDevNumber(clothesDevNumber);
        res += clothesDevService.deleteClothesDevColorByDevNumber(clothesDevNumber);
        res += clothesDevService.deleteClothesDevMeasureByDevNumber(clothesDevNumber);
        res += clothesDevService.deleteClothesDevProcessByDevNumber(clothesDevNumber);
        if (res == 0){
            map.put("result", 0);
        } else {
            map.put("result", 1);
        }
        return map;
    }


    @RequestMapping(value = "/updateclothesdevtotal",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> updateClothesDevTotal(@RequestParam("clothesDevNumber")String clothesDevNumber,
                                                     @RequestParam("clothesDevInfoJson")String clothesDevInfoJson,
                                                     @RequestParam("clothesDevImageJson")String clothesDevImageJson,
                                                     @RequestParam("clothesDevProcessJson")String clothesDevProcessJson,
                                                     @RequestParam("clothesDevMeasureJson")String clothesDevMeasureJson,
                                                     @RequestParam("clothesDevColorListJson")String clothesDevColorListJson,
                                                     @RequestParam("clothesDevSizeListJson")String clothesDevSizeListJson,
                                                     @RequestParam("clothesDevAccessoryListJson")String clothesDevAccessoryListJson,
                                                     @RequestParam("clothesDevFabricListJson")String clothesDevFabricListJson){
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        ClothesDevInfo clothesDevInfo = gson.fromJson(clothesDevInfoJson, ClothesDevInfo.class);
        ClothesDevImage clothesDevImage = gson.fromJson(clothesDevImageJson, ClothesDevImage.class);
        ClothesDevProcess clothesDevProcess = gson.fromJson(clothesDevProcessJson, ClothesDevProcess.class);
        ClothesDevMeasure clothesDevMeasure = gson.fromJson(clothesDevMeasureJson, ClothesDevMeasure.class);
        List<ClothesDevColor> clothesDevColorList = gson.fromJson(clothesDevColorListJson,new TypeToken<List<ClothesDevColor>>(){}.getType());
        List<ClothesDevSize> clothesDevSizeList = gson.fromJson(clothesDevSizeListJson,new TypeToken<List<ClothesDevSize>>(){}.getType());
        List<ClothesDevAccessory> clothesDevAccessoryList = gson.fromJson(clothesDevAccessoryListJson,new TypeToken<List<ClothesDevAccessory>>(){}.getType());
        List<ClothesDevFabric> clothesDevFabricList = gson.fromJson(clothesDevFabricListJson,new TypeToken<List<ClothesDevFabric>>(){}.getType());
        clothesDevService.deleteClothesDevAccessoryByDevNumber(clothesDevNumber);
        clothesDevService.deleteClothesDevFabricByDevNumber(clothesDevNumber);
        clothesDevService.deleteClothesDevColorByDevNumber(clothesDevNumber);
        clothesDevService.deleteClothesDevSizeByDevNumber(clothesDevNumber);
        int res = 0;
        res += clothesDevService.updateClothesDevInfoByDevNumber(clothesDevInfo);
        res += clothesDevService.updateClothesDevImageByDevNumber(clothesDevImage);
        res += clothesDevService.updateClothesDevProcessByDevNumber(clothesDevProcess);
        res += clothesDevService.updateClothesDevMeasureByDevNumber(clothesDevMeasure);
        res += clothesDevService.addClothesDevColorBatch(clothesDevColorList);
        res += clothesDevService.addClothesDevSizeBatch(clothesDevSizeList);
        res += clothesDevService.addClothesDevAccessoryBatch(clothesDevAccessoryList);
        res += clothesDevService.addClothesDevFabricBatch(clothesDevFabricList);
        if (res == 0){
            map.put("result", 0);
        } else {
            map.put("result", 1);
        }
        return map;
    }

    @RequestMapping(value = "/getclothesdevtotalbyclothesdevnumber", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getClothesDevTotalByClothesDevNumber(@RequestParam("clothesDevNumber")String clothesDevNumber){
        Map<String, Object> map = new LinkedHashMap<>();
        Map<String, Object> param = new HashMap<>();
        param.put("clothesDevNumber", clothesDevNumber);
        ClothesDevInfo clothesDevInfo = clothesDevService.getOneClothesDevInfoByMap(param);
        ClothesDevImage clothesDevImage = clothesDevService.getClothesDevImageByMap(param);
        ClothesDevMeasure clothesDevMeasure = clothesDevService.getClothesDevMeasureByMap(param);
        ClothesDevProcess clothesDevProcess = clothesDevService.getClothesDevProcessByMap(param);
        List<ClothesDevColor> clothesDevColorList = clothesDevService.getClothesDevColorByMap(param);
        List<ClothesDevSize> clothesDevSizeList = clothesDevService.getClothesDevSizeByMap(param);
        List<ClothesDevFabric> clothesDevFabricList = clothesDevService.getClothesDevFabricByMap(param);
        List<ClothesDevFabric> uniqueClothesDevFabricList = clothesDevService.getUniqueClothesDevFabricByDevNumber(clothesDevNumber);
        List<ClothesDevAccessory> clothesDevAccessoryList = clothesDevService.getClothesDevAccessoryByMap(param);
        List<String> colorList = new ArrayList<>();
        for (ClothesDevColor clothesDevColor : clothesDevColorList){
            colorList.add(clothesDevColor.getColorName());
        }
        List<String> sizeList = new ArrayList<>();
        for (ClothesDevSize clothesDevSize : clothesDevSizeList){
            sizeList.add(clothesDevSize.getSizeName());
        }
        map.put("clothesDevInfo", clothesDevInfo);
        map.put("clothesDevImage", clothesDevImage);
        map.put("clothesDevMeasure", clothesDevMeasure);
        map.put("clothesDevProcess", clothesDevProcess);
        map.put("clothesDevColorList", clothesDevColorList);
        map.put("clothesDevSizeList", clothesDevSizeList);
        map.put("clothesDevFabricList", clothesDevFabricList);
        map.put("uniqueClothesDevFabricList", uniqueClothesDevFabricList);
        map.put("clothesDevAccessoryList", clothesDevAccessoryList);
        map.put("colorInfo", colorList.toString());
        map.put("sizeInfo", sizeList.toString());
        return map;
    }

    @RequestMapping(value = "/changeclothesdevtoorder", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> changeClothesDevToOrder(@RequestParam("clothesDevNumber")String clothesDevNumber,
                                                       @RequestParam("orderName")String orderName,
                                                       @RequestParam("serialNumber")String serialNumber,
                                                       @RequestParam("brandName")String brandName,
                                                       @RequestParam("initClothesDevNumber")String initClothesDevNumber,
                                                       @RequestParam("customerName")String customerName,
                                                       @RequestParam("orderClothesJson")String orderClothesJson,
                                                       @RequestParam("colorJson")String colorJson,
                                                       @RequestParam("sizeJson")String sizeJson,
                                                       @RequestParam("colorInfo")String colorInfo,
                                                       @RequestParam("deliveryDate")String deliveryDate,
                                                       @RequestParam("buyer")String buyer,
                                                       @RequestParam("sizeInfo")String sizeInfo) throws ParseException {
        Map<String, Object> map = new LinkedHashMap<>();
        int res = 0;
        Map<String, Object> param = new HashMap<>();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");//注意月份是MM
        Date date = simpleDateFormat.parse(deliveryDate);
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        List<OrderClothes> orderClothesList = gson.fromJson(orderClothesJson,new TypeToken<List<OrderClothes>>(){}.getType());
        param.put("clothesDevNumber", initClothesDevNumber);
        ClothesDevInfo clothesDevInfo = clothesDevService.getOneClothesDevInfoByMap(param);
        ClothesDevImage clothesDevImage = clothesDevService.getClothesDevImageByMap(param);
        ClothesDevMeasure clothesDevMeasure = clothesDevService.getClothesDevMeasureByMap(param);
        ClothesDevProcess clothesDevProcess = clothesDevService.getClothesDevProcessByMap(param);
        List<ClothesDevFabric> clothesDevFabricList = clothesDevService.getClothesDevFabricByMap(param);
        ManufactureOrder manufactureOrder = new ManufactureOrder(clothesDevInfo.getStyleName(), customerName, "ODM", orderName, clothesDevInfo.getStyleName(), colorInfo, sizeInfo, clothesDevNumber, buyer, clothesDevInfo.getSeason(), new Date(), date, 0f);
        res += manufactureOrderService.addManufactureOrder(manufactureOrder);
        List<String> colorList = gson.fromJson(colorJson,new TypeToken<List<String>>(){}.getType());
        StyleImage styleImage = new StyleImage(orderName, clothesDevNumber, clothesDevImage.getDevImageOne());
        OrderMeasure orderMeasure = new OrderMeasure(orderName, clothesDevNumber, clothesDevMeasure.getMeasureImg());
        ProcessRequirement processRequirement = new ProcessRequirement(orderName, clothesDevNumber, clothesDevProcess.getProcessImg());
        List<ManufactureFabric> manufactureFabricList = new ArrayList<>();
        for (String colorName : colorList){
            for (ClothesDevFabric clothesDevFabric : clothesDevFabricList){
                if (colorName.equals(clothesDevFabric.getColorName())){
                    ManufactureFabric manufactureFabric = new ManufactureFabric(orderName, clothesDevNumber, clothesDevFabric.getFabricNumber(), clothesDevFabric.getFabricName(), clothesDevFabric.getFabricWidth(), clothesDevFabric.getFabricWeight(), clothesDevFabric.getUnit(), clothesDevFabric.getPartName(), clothesDevFabric.getPieceUsage(), colorName, clothesDevFabric.getFabricColor(), clothesDevFabric.getFabricColorNumber(), clothesDevFabric.getSupplier());
                    manufactureFabric.setCheckState("未提交");
                    manufactureFabricList.add(manufactureFabric);
                }
            }
        }
        res += processRequirementService.addProcessRequirement(processRequirement);
        res += orderMeasureService.addOrderMeasure(orderMeasure);
        res += styleImageService.addStyleImage(styleImage);
        res += manufactureFabricService.addManufactureFabricBatch(manufactureFabricList);
        res += orderClothesService.addOrderClothes(orderClothesList);
        if (res == 0){
            map.put("result", 0);
        } else {
            map.put("result", 1);
        }
        return map;
    }

}
