package com.example.erp01.action;

import com.example.erp01.model.EmpRoom;
import com.example.erp01.model.Room;
import com.example.erp01.service.EmpRoomService;
import com.example.erp01.service.RoomService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "erp")
public class RoomController {

    @Autowired
    private RoomService roomService;

    @Autowired
    private EmpRoomService empRoomService;

    @RequestMapping("/roomStart")
    public String roomStart(){
        return "logistic/room";
    }

    @RequestMapping(value = "/addroombatch", method = RequestMethod.POST)
    @ResponseBody
    public int addRoomBatch(@RequestParam("roomJson")String roomJson){
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        List<Room> roomList = gson.fromJson(roomJson,new TypeToken<List<Room>>(){}.getType());
        return roomService.addRoomBatch(roomList);
    }

    @RequestMapping(value = "/deleteroom", method = RequestMethod.POST)
    @ResponseBody
    public int deleteRoom(@RequestParam("roomID")Integer roomID){
        Room room = roomService.getRoomByID(roomID);
        List<EmpRoom> empRoomList = empRoomService.getEmpRoomByRoom(room.getRoomNumber());
        if (!empRoomList.isEmpty() && empRoomList.size() > 0){
            return 2;
        }
        return roomService.deleteRoom(roomID);
    }

    @RequestMapping(value = "/updateroom", method = RequestMethod.POST)
    @ResponseBody
    public int updateRoom(@RequestParam("room")String room){
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        Room rm = gson.fromJson(room, Room.class);
        return roomService.updateRoom(rm);
    }

    @RequestMapping(value = "/getallroom", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getAllRoom(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<Room> roomList = roomService.getAllRoom();
        map.put("room", roomList);
        return map;
    }

    @RequestMapping(value = "/getallfloor", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getAllFloor(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> floorList = roomService.getAllFloor();
        map.put("floorList", floorList);
        return map;
    }

}
