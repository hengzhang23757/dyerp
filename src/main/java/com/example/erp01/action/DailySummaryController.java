package com.example.erp01.action;

import com.example.erp01.model.*;
import com.example.erp01.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.apache.commons.collections4.ListUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Configuration      //1.主要用于标记配置类，兼备Component的效果。
@EnableScheduling   // 2.开启定时任务
public class DailySummaryController {

    @Autowired
    private DepartmentPlanService departmentPlanService;

    @Autowired
    private DailySummaryService dailySummaryService;

    @Autowired
    private DailyFabricService dailyFabricService;

    @Autowired
    private YesterdayReportService yesterdayReportService;

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private OrderClothesService orderClothesService;
    @Autowired
    private OrderProcedureService orderProcedureService;

//    @Scheduled(cron = "*/50 * * * * ?")
//    public void dailySummary(){
//        /***
//         * 1.部门计划汇总并更新
//         * 2.详情汇总到一张单独的表中
//         */
//        /***
//         * 1.裁床查裁数更新裁床计划
//         * 2.面料查松布匹数更新面料计划
//         * 3.车间查中查数
//         * 4.后整查大烫数
//         */
//        Calendar calendar= Calendar.getInstance();
//        calendar.set(Calendar.HOUR_OF_DAY,-24);
//        Date yesterdayDate = calendar.getTime();
//        Integer yesterdayTailorCount = 0;
//        Integer yesterdayWorkShopCount = 0;
//        Integer yesterdayFinishCount = 0;
//        Integer yesterdayFabricCount = 0;
//        List<YesterdayTailor> yesterdayTailorList = yesterdayReportService.getYesterdayTailorReport();
//        List<YesterdayWorkShop> yesterdayWorkShopList1 = yesterdayReportService.getYesterdayHangProduction();
//        List<YesterdayWorkShop> yesterdayWorkShopList2 = yesterdayReportService.getYesterdayMiniProduction();
//        List<YesterdayFinish> yesterdayFinishList1 = yesterdayReportService.getYesterdayFinishReport();
//        List<YesterdayFinish> yesterdayFinishList2 = yesterdayReportService.getYesterdayHangFinishReport();
//        List<DailyFabric> yesterdayFabricList = yesterdayReportService.getYesterdayFabricReport();
//        List<DailySummary> dailySummaryList = new ArrayList<>();
//        List<DepartmentPlan> departmentPlanList = departmentPlanService.getRecentDepartmentPlan();
//        for (YesterdayTailor yesterdayTailor : yesterdayTailorList){
//            DailySummary dailySummary = new DailySummary("裁床", "裁剪", yesterdayDate, yesterdayTailor.getClothesVersionNumber(), yesterdayTailor.getOrderName(), yesterdayTailor.getYesterdayCount());
//            dailySummaryList.add(dailySummary);
//            yesterdayTailorCount += yesterdayTailor.getYesterdayCount();
//        }
//        for (YesterdayWorkShop yesterdayWorkShop : yesterdayWorkShopList1){
//            DailySummary dailySummary = new DailySummary(yesterdayWorkShop.getGroupName(), "吊挂中查", yesterdayDate, yesterdayWorkShop.getClothesVersionNumber(), yesterdayWorkShop.getOrderName(), yesterdayWorkShop.getYesterdayCount());
//            dailySummaryList.add(dailySummary);
//            yesterdayWorkShopCount += yesterdayWorkShop.getYesterdayCount();
//        }
//        for (YesterdayWorkShop yesterdayWorkShop : yesterdayWorkShopList2){
//            DailySummary dailySummary = new DailySummary(yesterdayWorkShop.getGroupName(), "手机中查", yesterdayDate, yesterdayWorkShop.getClothesVersionNumber(), yesterdayWorkShop.getOrderName(), yesterdayWorkShop.getYesterdayCount());
//            dailySummaryList.add(dailySummary);
//            yesterdayWorkShopCount += yesterdayWorkShop.getYesterdayCount();
//        }
//        for (YesterdayFinish yesterdayFinish : yesterdayFinishList1){
//            DailySummary dailySummary = new DailySummary("后整", "吊挂大烫", yesterdayDate, yesterdayFinish.getClothesVersionNumber(), yesterdayFinish.getOrderName(), yesterdayFinish.getYesterdayFinishCount());
//            dailySummaryList.add(dailySummary);
//            yesterdayFinishCount += yesterdayFinish.getYesterdayFinishCount();
//        }
//        for (YesterdayFinish yesterdayFinish : yesterdayFinishList2){
//            DailySummary dailySummary = new DailySummary("后整", "手机大烫", yesterdayDate, yesterdayFinish.getClothesVersionNumber(), yesterdayFinish.getOrderName(), yesterdayFinish.getYesterdayFinishCount());
//            dailySummaryList.add(dailySummary);
//            yesterdayFinishCount += yesterdayFinish.getYesterdayFinishCount();
//        }
//        for (DailyFabric dailyFabric : yesterdayFabricList){
//            dailyFabric.setFabricDate(yesterdayDate);
//            yesterdayFabricCount += dailyFabric.getBatchCount();
//        }
//        for (DepartmentPlan departmentPlan : departmentPlanList){
//            if ("裁床".equals(departmentPlan.getDepartmentName())){
//                departmentPlan.setActCount(departmentPlan.getActCount() + yesterdayTailorCount);
//                departmentPlan.setYesterdayCount(yesterdayTailorCount);
//                if (departmentPlan.getPlanCount().equals(0)){
//                    departmentPlan.setCompletionRate(0f);
//                }else {
//                    departmentPlan.setCompletionRate((float)departmentPlan.getActCount()/departmentPlan.getPlanCount());
//                }
//            }
//            if ("车间".equals(departmentPlan.getDepartmentName())){
//                departmentPlan.setActCount(departmentPlan.getActCount() + yesterdayWorkShopCount);
//                departmentPlan.setYesterdayCount(yesterdayWorkShopCount);
//                if (departmentPlan.getPlanCount().equals(0)){
//                    departmentPlan.setCompletionRate(0f);
//                }else {
//                    departmentPlan.setCompletionRate((float)departmentPlan.getActCount()/departmentPlan.getPlanCount());
//                }
//
//            }
//            if ("后整".equals(departmentPlan.getDepartmentName())){
//                departmentPlan.setActCount(departmentPlan.getActCount() + yesterdayFinishCount);
//                departmentPlan.setYesterdayCount(yesterdayFinishCount);
//                if (departmentPlan.getPlanCount().equals(0)){
//                    departmentPlan.setCompletionRate(0f);
//                }else {
//                    departmentPlan.setCompletionRate((float)departmentPlan.getActCount()/departmentPlan.getPlanCount());
//                }
//
//            }
//            if ("面料".equals(departmentPlan.getDepartmentName())){
//                departmentPlan.setActCount(departmentPlan.getActCount() + yesterdayFabricCount);
//                departmentPlan.setYesterdayCount(yesterdayFabricCount);
//                if (departmentPlan.getPlanCount().equals(0)){
//                    departmentPlan.setCompletionRate(0f);
//                }else {
//                    departmentPlan.setCompletionRate((float)departmentPlan.getActCount()/departmentPlan.getPlanCount());
//                }
//
//            }
//        }
//        dailySummaryService.addDailySummaryBatch(dailySummaryList);
//        dailyFabricService.addDailyFabricBatch(yesterdayFabricList);
//        departmentPlanService.updateDepartmentPlanBatch(departmentPlanList);
//        System.out.println("汇总执行完毕--------");
//    }

//    @Scheduled(cron = "0 28 12 * * ? ")
//    public void updatePieceWork(){
//        System.out.println("开始更新");
//        List<String> employeeNumberList = dailySummaryService.getDistinctEmployeeNumberHang(null, null);
//        for (String employeeNumber : employeeNumberList){
//            List<Employee> employeeList = employeeService.getEmpByEmpNumber(employeeNumber);
//            String groupName = "车缝A组";
//            if (employeeList != null && !employeeList.isEmpty()){
//                groupName = employeeList.get(0).getGroupName();
//            }
//            int res = dailySummaryService.updateHangEmployeeInfo(employeeNumber, groupName);
//            if (res != 0){
//                System.out.println("更新有误！");
//            }
//        }
//    }

//    @Scheduled(cron = "0 32 14 * * ? ")
//    public void updatePieceWork(){
//        System.out.println("开始更新");
//        List<String> orderNameList = dailySummaryService.getDistinctOrderNameHang(null, null);
//        for (String orderName : orderNameList){
//            String clothesVersionNumber = orderName;
//            if (orderClothesService.getVersionNumberByOrderName(orderName) != null){
//                clothesVersionNumber = orderClothesService.getVersionNumberByOrderName(orderName);
//            }
//            int res = dailySummaryService.updateHangOrderInfo(orderName, clothesVersionNumber);
//            if (res != 0){
//                System.out.println("更新有误！");
//            }
//        }
//        System.out.println("更新结束");
//    }

//    @Scheduled(cron = "0 47 14 * * ? ")
//    public void updatePieceWork(){
//        System.out.println("开始更新");
//        List<OrderProcedure> orderProcedureList = dailySummaryService.getDistinctOrderProcedureHang(null, null);
//        for (OrderProcedure orderProcedure : orderProcedureList){
//            String procedureName = "通用";
//            if (orderProcedureService.getProcedureInfoByOrderProcedureNumber(orderProcedure.getOrderName(), orderProcedure.getProcedureNumber()) != null){
//                procedureName = orderProcedureService.getProcedureInfoByOrderProcedureNumber(orderProcedure.getOrderName(), orderProcedure.getProcedureNumber()).getProcedureName();
//            }
//            int res = dailySummaryService.updateHangProcedureInfo(orderProcedure.getOrderName(), orderProcedure.getProcedureNumber(), procedureName);
//            if (res != 0){
//                System.out.println("更新有误！");
//            }
//        }
//        System.out.println("更新结束");
//    }

//    @Scheduled(cron = "0 35 15 * * ? ")
//    public void updatePieceWork(){
//        System.out.println("开始更新");
//        Calendar cal=Calendar.getInstance();
//        cal.add(Calendar.DATE,-1);
//        Date d=cal.getTime();
//        List<String> employeeNumberList = dailySummaryService.getDistinctEmployeeNumberMini(null, d);
//        for (String employeeNumber : employeeNumberList){
//            List<Employee> employeeList = employeeService.getEmpByEmpNumber(employeeNumber);
//            String groupName = "车缝A组";
//            String employeeName = "离职";
//            if (employeeList != null && !employeeList.isEmpty()){
//                groupName = employeeList.get(0).getGroupName();
//                employeeName = employeeList.get(0).getEmployeeName();
//            }
//            int res = dailySummaryService.updateMiniEmployeeInfo(employeeNumber, employeeName, groupName);
//            if (res != 0){
//                System.out.println("更新有误！");
//            }
//        }
//        System.out.println("更新结束");
//    }

//    @Scheduled(cron = "0 44 15 * * ? ")
////    public void updatePieceWork(){
////        System.out.println("开始更新");
////        Calendar cal=Calendar.getInstance();
////        cal.add(Calendar.DATE,-1);
////        Date d=cal.getTime();
////        List<String> orderNameList = dailySummaryService.getDistinctOrderNameMini(null, d);
////        for (String orderName : orderNameList){
////            String clothesVersionNumber = orderName;
////            if (orderClothesService.getVersionNumberByOrderName(orderName) != null){
////                clothesVersionNumber = orderClothesService.getVersionNumberByOrderName(orderName);
////            }
////            int res = dailySummaryService.updateMiniOrderInfo(orderName, clothesVersionNumber);
////            if (res != 0){
////                System.out.println("更新有误！");
////            }
////        }
////        System.out.println("更新结束");
////    }

//    @Scheduled(cron = "0 49 15 * * ? ")
//    public void updatePieceWork(){
//        System.out.println("开始更新");
//        Calendar cal=Calendar.getInstance();
//        cal.add(Calendar.DATE,-1);
//        Date d=cal.getTime();
//        List<OrderProcedure> orderProcedureList = dailySummaryService.getDistinctOrderProcedureMini(null, d);
//        for (OrderProcedure orderProcedure : orderProcedureList){
//            String procedureName = "通用";
//            if (orderProcedureService.getProcedureInfoByOrderProcedureNumber(orderProcedure.getOrderName(), orderProcedure.getProcedureNumber()) != null){
//                procedureName = orderProcedureService.getProcedureInfoByOrderProcedureNumber(orderProcedure.getOrderName(), orderProcedure.getProcedureNumber()).getProcedureName();
//            }
//            int res = dailySummaryService.updateMiniProcedureInfo(orderProcedure.getOrderName(), orderProcedure.getProcedureNumber(), procedureName);
//            if (res != 0){
//                System.out.println("更新有误！");
//            }
//        }
//        System.out.println("更新结束");
//    }

//    @Scheduled(cron = "0 57 21 * * ? ")
//    public void copyPieceWork() throws Exception{
//        System.out.println("开始更新");
//        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
//        Date fromDate = simpleDateFormat.parse("2020-05-31");
//        Date toDate = simpleDateFormat.parse("2020-06-30");
//        List<PieceWork> pieceWorkList = dailySummaryService.getPieceWorkFromHang(fromDate, toDate);
//        int res = dailySummaryService.copyPieceWorkFromHangToMini(pieceWorkList);
//        if (res == 0){
//            System.out.println("更新成功");
//        } else {
//            System.out.println("更新失败");
//        }
//        System.out.println("更新结束");
//    }

    @Scheduled(cron = "0 0/30 * * * *")
    public void empSkillTest(){
        Integer maxNid = dailySummaryService.getMaxNidFromPieceWork();
        List<PieceWork> pieceWorkList = dailySummaryService.getPieceWorkFromHangByNid(maxNid);
        if (pieceWorkList != null && !pieceWorkList.isEmpty()){
            List<List<PieceWork>> subList = ListUtils.partition(pieceWorkList, 500);
            subList.forEach(subPieceWorkList -> {
                for (PieceWork pieceWork : subPieceWorkList){
                    String employeeNumber = pieceWork.getEmployeeNumber();
                    List<Employee> employeeList = employeeService.getEmpByEmpNumber(employeeNumber);
                    String groupName = "离职";
                    if (employeeList != null && !employeeList.isEmpty()){
                        groupName = employeeList.get(0).getGroupName();
                    }
                    pieceWork.setGroupName(groupName);
                    String orderName = pieceWork.getOrderName();
                    pieceWork.setClothesVersionNumber(orderClothesService.getVersionNumberByOrderName(orderName));
                    Integer procedureNumber = pieceWork.getProcedureNumber();
                    String procedureName = "通用";
                    if (orderProcedureService.getProcedureInfoByOrderProcedureNumber(orderName, procedureNumber) != null){
                        procedureName = orderProcedureService.getProcedureInfoByOrderProcedureNumber(orderName, procedureNumber).getProcedureName();
                    }
                    pieceWork.setProcedureName(procedureName);
                }
                int res = dailySummaryService.copyPieceWorkFromHangToMini(subPieceWorkList);
                if (res == 0){
                    System.out.println("更新成功");
                } else {
                    System.out.println("更新失败");
                }
            });
        }
        System.out.println("更新结束");
    }

}
