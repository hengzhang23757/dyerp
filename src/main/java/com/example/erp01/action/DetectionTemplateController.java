package com.example.erp01.action;

import com.example.erp01.model.DetectionTemplate;
import com.example.erp01.service.DetectionTemplateService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "erp")
public class DetectionTemplateController {

    @Autowired
    private DetectionTemplateService detectionTemplateService;

    @RequestMapping("/detectionTemplateStart")
    public String detectionTemplateStart(){
        return "IQC/detectionTemplate";
    }

    @RequestMapping(value = "/adddetectiontemplate",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> addDetectionTemplate(@RequestParam("detectionTemplateJson")String detectionTemplateJson){
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        DetectionTemplate detectionTemplate = gson.fromJson(detectionTemplateJson, DetectionTemplate.class);
        int res = detectionTemplateService.addDetectionTemplate(detectionTemplate);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/updatedetectiontemplate",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> updateDetectionTemplate(@RequestParam("detectionTemplateJson")String detectionTemplateJson){
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        DetectionTemplate detectionTemplate = gson.fromJson(detectionTemplateJson, DetectionTemplate.class);
        int res = detectionTemplateService.updateDetectionTemplate(detectionTemplate);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/deletedetectiontemplate",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> deleteDetectionTemplate(@RequestParam("id")Integer id){
        Map<String, Object> map = new LinkedHashMap<>();
        int res = detectionTemplateService.deleteDetectionTemplate(id);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/getdetectiontemplatebyinfo",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getDetectionTemplateByInfo(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<DetectionTemplate> detectionTemplateList = detectionTemplateService.getDetectionTemplateByInfo(null);
        map.put("detectionTemplateList", detectionTemplateList);
        return map;
    }

    @RequestMapping(value = "/getdetectiontemplatebyid",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getDetectionTemplateById(@RequestParam("id")Integer id){
        Map<String, Object> map = new LinkedHashMap<>();
        DetectionTemplate detectionTemplate = detectionTemplateService.getDetectionTemplateById(id);
        map.put("detectionTemplate", detectionTemplate);
        return map;
    }

    @RequestMapping(value = "/getdetectionnamebydetectiontype",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getDetectionNameByDetectionType(@RequestParam("detectionType")String detectionType){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> detectionNameList = detectionTemplateService.getDetectionNameByDetectionType(detectionType);
        map.put("detectionNameList", detectionNameList);
        return map;
    }

    @RequestMapping(value = "/getdetectiontemplatebydetectionname",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getDetectionTemplateByDetectionName(@RequestParam("detectionName")String detectionName){
        Map<String, Object> map = new LinkedHashMap<>();
        Map<String, Object> param = new HashMap<>();
        param.put("detectionName", detectionName);
        List<DetectionTemplate> detectionTemplateList = detectionTemplateService.getDetectionTemplateByInfo(param);
        if (detectionTemplateList != null && !detectionTemplateList.isEmpty()){
            map.put("detectionTemplate", detectionTemplateList.get(0));
        }
        return map;
    }

}
