package com.example.erp01.action;

import com.example.erp01.model.*;
import com.example.erp01.service.FabricAccessoryLeakService;
import com.example.erp01.service.FabricOutRecordService;
import com.example.erp01.service.ManufactureOrderService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

@Controller
@RequestMapping(value = "erp")
public class FabricAccessoryLeakController {

    @Autowired
    private ManufactureOrderService manufactureOrderService;
    @Autowired
    private FabricAccessoryLeakService fabricAccessoryLeakService;
    @Autowired
    private FabricOutRecordService fabricOutRecordService;

    @RequestMapping("/fabricAccessoryLeakStart")
    public String fabricAccessoryLeakStart(){
        return "basicInfo/fabricAccessoryLeak";
    }

    @RequestMapping(value = "/searchfabricaccessoryleakbyinfo", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> searchFabricAccessoryLeakByInfo(@RequestParam("fabricAccessoryDtoJson")String fabricAccessoryDtoJson){
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        FabricAccessoryDto fabricAccessoryDto = gson.fromJson(fabricAccessoryDtoJson, FabricAccessoryDto.class);
        Map<String, Object> map = new LinkedHashMap<>();
        String type = fabricAccessoryDto.getType();
        List<String> orderNameList = new ArrayList<>();
        List<ManufactureOrder> manufactureOrderList = new ArrayList<>();
        if (fabricAccessoryDto.getOrderName() != null){
            orderNameList.add(fabricAccessoryDto.getOrderName());
            ManufactureOrder manufactureOrder = manufactureOrderService.getManufactureOrderByOrderName(fabricAccessoryDto.getOrderName());
            manufactureOrderList.add(manufactureOrder);
        } else {
            manufactureOrderList = manufactureOrderService.getManufactureOrderListByModelSeason(fabricAccessoryDto.getModelType(), fabricAccessoryDto.getSeasonList(), fabricAccessoryDto.getCustomerList());
            for (ManufactureOrder manufactureOrder : manufactureOrderList){
                orderNameList.add(manufactureOrder.getOrderName());
            }
        }
        List<FabricAccessoryLeak> fabricAccessoryLeakList1 = fabricAccessoryLeakService.getOrderInfoByOrderList(orderNameList);
        List<FabricAccessoryLeak> fabricAccessoryLeakList2 = fabricAccessoryLeakService.getCutInfoByOrderList(orderNameList);
        List<FabricAccessoryLeak> fabricAccessoryLeakList3 = fabricAccessoryLeakService.getFabricAccessoryLeakByOrderList(orderNameList, type);
        List<FabricAccessoryLeak> fabricAccessoryLeakList4 = fabricAccessoryLeakService.getFabricAccessoryReturnByOrderList(orderNameList, type);
        List<FabricAccessoryLeak> fabricAccessoryLeakList = new ArrayList<>();
        for (ManufactureOrder manufactureOrder : manufactureOrderList){
            FabricAccessoryLeak fabricAccessoryLeak = new FabricAccessoryLeak();
            fabricAccessoryLeak.setOrderName(manufactureOrder.getOrderName());
            fabricAccessoryLeak.setClothesVersionNumber(manufactureOrder.getClothesVersionNumber());
            fabricAccessoryLeak.setSeason(manufactureOrder.getSeason());
            fabricAccessoryLeak.setModelType(manufactureOrder.getModelType());
            fabricAccessoryLeak.setCustomerName(manufactureOrder.getCustomerName());
            for (FabricAccessoryLeak fabricAccessoryLeak1 : fabricAccessoryLeakList1){
                if (manufactureOrder.getOrderName().equals(fabricAccessoryLeak1.getOrderName())){
                    fabricAccessoryLeak.setOrderCount(fabricAccessoryLeak1.getOrderCount());
                }
            }
            for (FabricAccessoryLeak fabricAccessoryLeak2 : fabricAccessoryLeakList2){
                if (manufactureOrder.getOrderName().equals(fabricAccessoryLeak2.getOrderName())){
                    fabricAccessoryLeak.setWellCount(fabricAccessoryLeak2.getWellCount());
                }
            }
            for (FabricAccessoryLeak fabricAccessoryLeak3 : fabricAccessoryLeakList3){
                if (manufactureOrder.getOrderName().equals(fabricAccessoryLeak3.getOrderName())){
                    fabricAccessoryLeak.setPurchaseCount(fabricAccessoryLeak3.getPurchaseCount());
                }
            }
            for (FabricAccessoryLeak fabricAccessoryLeak4 : fabricAccessoryLeakList4){
                if (manufactureOrder.getOrderName().equals(fabricAccessoryLeak4.getOrderName())){
                    fabricAccessoryLeak.setInStoreCount(fabricAccessoryLeak4.getInStoreCount());
                    if (fabricAccessoryLeak.getPurchaseCount() == 0){
                        fabricAccessoryLeak.setReadyPercent(0f);
                    } else {
                        fabricAccessoryLeak.setReadyPercent(fabricAccessoryLeak.getInStoreCount()/fabricAccessoryLeak.getPurchaseCount());
                    }
                }
            }
            fabricAccessoryLeakList.add(fabricAccessoryLeak);
        }
        map.put("fabricAccessoryLeakList", fabricAccessoryLeakList);
        return map;
    }

    @RequestMapping(value = "/searchfabricaccessorysummoneybyinfo", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> searchFabricAccessorySumMoneyByInfo(@RequestParam("fabricAccessoryDtoJson")String fabricAccessoryDtoJson){
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        FabricAccessoryDto fabricAccessoryDto = gson.fromJson(fabricAccessoryDtoJson, FabricAccessoryDto.class);
        Map<String, Object> map = new LinkedHashMap<>();
        String type = fabricAccessoryDto.getType();
        List<String> orderNameList = new ArrayList<>();
        List<ManufactureOrder> manufactureOrderList = new ArrayList<>();
        if (fabricAccessoryDto.getOrderName() != null){
            orderNameList.add(fabricAccessoryDto.getOrderName());
            ManufactureOrder manufactureOrder = manufactureOrderService.getManufactureOrderByOrderName(fabricAccessoryDto.getOrderName());
            manufactureOrderList.add(manufactureOrder);
        } else {
            manufactureOrderList = manufactureOrderService.getManufactureOrderListByModelSeason(fabricAccessoryDto.getModelType(), fabricAccessoryDto.getSeasonList(), fabricAccessoryDto.getCustomerList());
            for (ManufactureOrder manufactureOrder : manufactureOrderList){
                orderNameList.add(manufactureOrder.getOrderName());
            }
        }
        if (type.equals("面料")){
            List<ManufactureFabric> manufactureFabricList = fabricAccessoryLeakService.getManufactureFabricByOrderList(orderNameList);
            List<FabricDetail> fabricDetailList = fabricAccessoryLeakService.getFabricDetailByOrderList(orderNameList);
            Map<String, Object> param = new HashMap<>();
            param.put("orderNameList", orderNameList);
            param.put("operateType", "退面料厂");
            List<FabricOutRecord> fabricOutRecordList = fabricOutRecordService.getFabricOutRecordByMap(param);
            for (ManufactureFabric manufactureFabric : manufactureFabricList){
                for (FabricDetail fabricDetail : fabricDetailList){
                    if (manufactureFabric.getFabricID().equals(fabricDetail.getFabricID())){
                        manufactureFabric.setFabricReturn(manufactureFabric.getFabricReturn() + fabricDetail.getWeight());
                    }
                }
                manufactureFabric.setSumMoney(manufactureFabric.getFabricActCount() * manufactureFabric.getPrice());
                manufactureFabric.setReturnSumMoney(manufactureFabric.getFabricReturn() * manufactureFabric.getPrice());
                for (FabricOutRecord fabricOutRecord : fabricOutRecordList){
                    if (manufactureFabric.getFabricID().equals(fabricOutRecord.getFabricID())){
                        manufactureFabric.setFabricReturn(manufactureFabric.getFabricReturn() - fabricOutRecord.getWeight());
                    }
                }
                manufactureFabric.setSumMoney(manufactureFabric.getFabricActCount() * manufactureFabric.getPrice());
                manufactureFabric.setReturnSumMoney(manufactureFabric.getFabricReturn() * manufactureFabric.getPrice());
            }
            map.put("manufactureFabricList", manufactureFabricList);
            return map;
        } else {
            List<ManufactureAccessory> manufactureAccessoryList = fabricAccessoryLeakService.getManufactureAccessoryByOrderList(orderNameList);
            List<AccessoryInStore> accessoryInStoreList = fabricAccessoryLeakService.getAccessoryInStoreByOrderList(orderNameList);
            for (ManufactureAccessory manufactureAccessory : manufactureAccessoryList){
                for (AccessoryInStore accessoryInStore : accessoryInStoreList){
                    if (manufactureAccessory.getAccessoryID().equals(accessoryInStore.getAccessoryID())){
                        manufactureAccessory.setReturnCount(manufactureAccessory.getReturnCount() + accessoryInStore.getInStoreCount());
                        manufactureAccessory.setAccessoryAccountCount(manufactureAccessory.getAccessoryAccountCount() + accessoryInStore.getAccountCount());
                    }
                }
                manufactureAccessory.setSumMoney(manufactureAccessory.getAccessoryCount() * manufactureAccessory.getPrice());
                manufactureAccessory.setReturnSumMoney(manufactureAccessory.getAccessoryAccountCount() * manufactureAccessory.getPrice());
            }
            map.put("manufactureAccessoryList", manufactureAccessoryList);
            return map;
        }
    }
}
