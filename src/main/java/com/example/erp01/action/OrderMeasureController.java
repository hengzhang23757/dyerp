package com.example.erp01.action;

import com.example.erp01.model.OrderMeasure;
import com.example.erp01.model.OrderMeasureItem;
import com.example.erp01.service.OrderClothesService;
import com.example.erp01.service.OrderMeasureItemService;
import com.example.erp01.service.OrderMeasureService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "erp")
public class OrderMeasureController {

    @Autowired
    private OrderMeasureService orderMeasureService;
    @Autowired
    private OrderClothesService orderClothesService;
    @Autowired
    private OrderMeasureItemService orderMeasureItemService;

    @RequestMapping("/orderMeasureStart")
    public String orderMeasureStart(){
        return "ieInfo/orderMeasure";
    }

    @RequestMapping(value = "/addordermeasurebatch", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> addOrderMeasureBatch(@RequestParam("orderMeasureJson")String orderMeasureJson){
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        List<OrderMeasure> orderMeasureList = gson.fromJson(orderMeasureJson,new TypeToken<List<OrderMeasure>>(){}.getType());
        int res = orderMeasureService.addOrderMeasureBatch(orderMeasureList);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/addordermeasuretext", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> addOrderMeasure(@RequestParam("orderName")String orderName,
                                               @RequestParam("clothesVersionNumber")String clothesVersionNumber,
                                               @RequestParam("orderMeasureImg")String orderMeasureImg){
        Map<String, Object> map = new LinkedHashMap<>();
        OrderMeasure orderMeasure = new OrderMeasure();
        orderMeasure.setOrderName(orderName);
        orderMeasure.setClothesVersionNumber(clothesVersionNumber);
        orderMeasure.setOrderMeasureImg(orderMeasureImg);
        int res = orderMeasureService.addOrderMeasure(orderMeasure);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/getordermeasurebyorder", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getOrderMeasureByOrder(@RequestParam("orderName") String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        OrderMeasure orderMeasure = orderMeasureService.getOneOrderMeasureByOrder(orderName);
        if (orderMeasure != null){
            map.put("orderMeasure", orderMeasure);
        }
        return map;
    }

    @RequestMapping(value = "/updateordermeasure", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> updateOrderMeasure(OrderMeasure orderMeasure){
        Map<String, Object> map = new LinkedHashMap<>();
        int res = orderMeasureService.updateOrderMeasure(orderMeasure);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/deleteordermeasurebyid", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> deleteOrderMeasureByID(@RequestParam("orderMeasureID")Integer orderMeasureID){
        Map<String, Object> map = new LinkedHashMap<>();
        int res = orderMeasureService.deleteOrderMeasureByID(orderMeasureID);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/getordermeasureitemlistbyorder", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getOrderMeasureItemListByOrder(@RequestParam("orderName") String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> sizeNameList = orderClothesService.getOrderSizeNamesByOrder(orderName);
        map.put("sizeNameList",sizeNameList);
        List<OrderMeasureItem> orderMeasureItemList = orderMeasureItemService.getOrderMeasureItemByOrder(orderName);
        List<String> partCodeList = orderMeasureItemService.getUniquePartCodeListByOrder(orderName);
        if (orderMeasureItemList != null && !orderMeasureItemList.isEmpty()){
            map.put("orderMeasureItemList", orderMeasureItemList);
            map.put("partCodeList", partCodeList);
        }
        return map;
    }

    @RequestMapping(value = "/getordermeasurebyinfo", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getOrderMeasureByInfo(@RequestParam(value = "orderName",required = false) String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<OrderMeasure> orderMeasureList = orderMeasureService.getOrderMeasureByOrder(orderName);
        map.put("orderMeasureList", orderMeasureList);
        return map;
    }

}
