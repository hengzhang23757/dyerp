package com.example.erp01.action;

import com.example.erp01.model.FixedProcedure;
import com.example.erp01.service.FixedProcedureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "erp")
public class FixedProcedureController {

    @Autowired
    private FixedProcedureService fixedProcedureService;

    @RequestMapping("/fixedProcedureStart")
    public String fixedProcedureStart(){
        return "ieInfo/fixedProcedure";
    }

    @RequestMapping(value = "/getallfixedprocedure",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getAllFixedProcedure(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<FixedProcedure> fixedProcedureList = fixedProcedureService.getAllFixedProcedure();
        map.put("data", fixedProcedureList);
        map.put("code", 0);
        map.put("count", fixedProcedureList.size());
        return map;
    }

    @RequestMapping(value = "/minigetallfixedprocedure",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> miniGetAllFixedProcedure(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<FixedProcedure> fixedProcedureList = fixedProcedureService.getAllFixedProcedure();
        map.put("fixedProcedureList", fixedProcedureList);
        return map;
    }

    @RequestMapping(value = "/addfixedprocedure",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> addFixedProcedure(@RequestParam("procedureNumber")Integer procedureNumber,
                                                 @RequestParam("procedureName")String procedureName,
                                                 @RequestParam("procedureSection")String procedureSection){
        Map<String, Object> map = new LinkedHashMap<>();
        FixedProcedure fixedProcedure = new FixedProcedure();
        fixedProcedure.setProcedureNumber(procedureNumber);
        fixedProcedure.setProcedureName(procedureName);
        fixedProcedure.setProcedureSection(procedureSection);
        int res = fixedProcedureService.addFixedProcedure(fixedProcedure);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/deletefixedprocedure",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> deleteFixedProcedure(@RequestParam("id")Integer id){
        Map<String, Object> map = new LinkedHashMap<>();
        int res = fixedProcedureService.deleteFixedProcedure(id);
        map.put("result", res);
        return map;
    }

}
