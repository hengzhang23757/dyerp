package com.example.erp01.action;

import com.example.erp01.model.*;
import com.example.erp01.service.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.SimpleDateFormat;
import java.util.*;

@Controller
@RequestMapping(value = "erp")
public class ClothesDevFabricManageController {

    @Autowired
    private ClothesDevFabricManageService clothesDevFabricManageService;
    @Autowired
    private ManufactureFabricService manufactureFabricService;
    @Autowired
    private FabricStorageService fabricStorageService;
    @Autowired
    private FabricDetailService fabricDetailService;
    @Autowired
    private CheckMonthService checkMonthService;

    @RequestMapping("/clothesDevFabricManageStart")
    public String clothesDevFabricManageStart(){
        return "clothesDev/clothesDevFabric";
    }

    @RequestMapping("/shareFabricStart")
    public String shareFabricStart(){
        return "share/shareFabric";
    }

    @RequestMapping(value = "/getclothesdevfabricmanagebyinfo",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getClothesDevFabricManageByInfo(@RequestParam(value = "fabricName", required = false)String fabricName,
                                                               @RequestParam(value = "fabricNumber", required = false)String fabricNumber,
                                                               @RequestParam(value = "fabricID", required = false)Integer fabricID,
                                                               @RequestParam(value = "storageType", required = false)String storageType){
        Map<String, Object> map = new LinkedHashMap<>();
        Map<String, Object> param = new HashMap<>();
        param.put("fabricName", fabricName);
        param.put("fabricNumber", fabricNumber);
        param.put("fabricID", fabricID);
        param.put("storageType", storageType);
        List<ClothesDevFabricManage> clothesDevFabricManageList = clothesDevFabricManageService.getClothesDevFabricManageByMap(param);
        map.put("data", clothesDevFabricManageList);
        map.put("count", clothesDevFabricManageList.size());
        map.put("code", 0);
        return map;
    }

    @RequestMapping(value = "/addclothesdevfabricmanage",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> addClothesDevFabricManage(@RequestParam("clothesDevFabricManageJson")String clothesDevFabricManageJson){
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        ClothesDevFabricManage clothesDevFabricManage = gson.fromJson(clothesDevFabricManageJson, ClothesDevFabricManage.class);
        int res1 = clothesDevFabricManageService.addClothesDevFabricManage(clothesDevFabricManage);
        if (res1 == 0){
            map.put("result", 0);
            return map;
        } else {
            map.put("result", 1);
            return map;
        }
    }

    @RequestMapping(value = "/getclothesdevfabricnamehint", method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getClothesDevFabricNameHint(@RequestParam("keywords")String keywords){
        Map<String,Object> map = new HashMap<>();
        List<String> fabricNameList = clothesDevFabricManageService.getClothsDevFabricNameHint(keywords);
        map.put("content", fabricNameList);
        map.put("code",0);
        map.put("type","success");
        return map;
    }

    @RequestMapping(value = "/getclothesdevfabricnumberhint", method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getClothesDevFabricNumberHint(@RequestParam("keywords")String keywords){
        Map<String,Object> map = new HashMap<>();
        List<String> fabricNumberList = clothesDevFabricManageService.getClothsDevFabricNumberHint(keywords);
        map.put("content", fabricNumberList);
        map.put("code",0);
        map.put("type","success");
        return map;
    }

    @RequestMapping(value = "/getclothesdevfabricmanagehint", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getManufactureFabricHintByFabricName(@RequestParam("subFabricName")String subFabricName
            ,@RequestParam("page")Integer page,@RequestParam("limit")Integer limit){
        Map<String, Object> map = new LinkedHashMap<>();
        List<ClothesDevFabricManage> countList = clothesDevFabricManageService.getClothesDevFabricManageHint(subFabricName,null,null);
        List<ClothesDevFabricManage> clothesDevFabricManageList = clothesDevFabricManageService.getClothesDevFabricManageHint(subFabricName,(page-1)*limit,limit);
        Collections.reverse(clothesDevFabricManageList);
        map.put("data",clothesDevFabricManageList);
        map.put("msg","");
        map.put("count",countList.size());
        map.put("code",0);
        return map;
    }

    @RequestMapping(value = "/deleteclothesdevfabricmanagealltype", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> deleteClothesDevFabricManageAllType(@RequestParam("id")Integer id){
        Map<String, Object> map = new LinkedHashMap<>();
        int res1 = clothesDevFabricManageService.deleteClothesDevFabricManage(id);
        int res2 = clothesDevFabricManageService.deleteClothesDevFabricRecordByFabricId(id);
        if (res1 == 0 && res2 == 0){
            map.put("result", 0);
        } else {
            map.put("result", 1);
        }
        return map;
    }

    @RequestMapping(value = "/clothesdevfabricmanageinstore", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> clothesDevFabricManageInStore(@RequestParam("id")Integer id,
                                                             @RequestParam("fabricInCount")Float fabricInCount,
                                                             @RequestParam("fabricInCountTwo")Float fabricInCountTwo,
                                                             @RequestParam("batchNumberIn")Integer batchNumberIn,
                                                             @RequestParam("price")Float price,
                                                             @RequestParam("priceTwo")Float priceTwo,
                                                             @RequestParam("location")String location,
                                                             @RequestParam("operateDate")String operateDate,
                                                             @RequestParam("remark")String remark,
                                                             @RequestParam("payState")String payState) throws Exception{
        Map<String, Object> map = new LinkedHashMap<>();
        ClothesDevFabricManage clothesDevFabricManage = clothesDevFabricManageService.getClothesDevFabricManageById(id);
        ClothesDevFabricManage clothesDevFabricManageIn = new ClothesDevFabricManage(clothesDevFabricManage.getId(), clothesDevFabricManage.getFabricNumber(), clothesDevFabricManage.getFabricName(), clothesDevFabricManage.getFabricWidth(), clothesDevFabricManage.getFabricWeight(), clothesDevFabricManage.getFabricContent(), clothesDevFabricManage.getUnit(), clothesDevFabricManage.getUnitTwo(), fabricInCount, 0f, 0f, fabricInCountTwo, 0f, 0f, clothesDevFabricManage.getRatio(), clothesDevFabricManage.getFabricColor(), clothesDevFabricManage.getFabricColorNumber(), clothesDevFabricManage.getSupplier(), price, priceTwo, batchNumberIn, 0, 0, "in", remark, payState);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = simpleDateFormat.parse(operateDate);
        clothesDevFabricManageIn.setOperateDate(date);
        clothesDevFabricManageIn.setLocation(location);
        Map<String, Object> param = new HashMap<>();
        param.put("fabricID", id);
        param.put("storageType", "storage");
        List<ClothesDevFabricManage> clothesDevFabricManageList = clothesDevFabricManageService.getClothesDevFabricManageByMap(param);
        int res1;
        if (clothesDevFabricManageList != null && !clothesDevFabricManageList.isEmpty()){
            ClothesDevFabricManage clothesDevFabricManageStorage = clothesDevFabricManageList.get(0);
            clothesDevFabricManageStorage.setFabricInCount(clothesDevFabricManageStorage.getFabricInCount() + fabricInCount);
            clothesDevFabricManageStorage.setFabricInCountTwo(clothesDevFabricManageStorage.getFabricInCountTwo() + fabricInCountTwo);
            clothesDevFabricManageStorage.setFabricStorageCount(clothesDevFabricManageStorage.getFabricStorageCount() + fabricInCount);
            clothesDevFabricManageStorage.setFabricStorageCountTwo(clothesDevFabricManageStorage.getFabricStorageCountTwo() + fabricInCountTwo);
            clothesDevFabricManageStorage.setBatchNumberIn(clothesDevFabricManageStorage.getBatchNumberIn() + batchNumberIn);
            clothesDevFabricManageStorage.setBatchNumberStorage(clothesDevFabricManageStorage.getBatchNumberStorage() + batchNumberIn);
            clothesDevFabricManageStorage.setOperateDate(date);
            res1 = clothesDevFabricManageService.updateClothesDevFabricManage(clothesDevFabricManageStorage);
        } else {
            ClothesDevFabricManage clothesDevFabricManageStorage = new ClothesDevFabricManage(clothesDevFabricManage.getId(), clothesDevFabricManage.getFabricNumber(), clothesDevFabricManage.getFabricName(), clothesDevFabricManage.getFabricWidth(), clothesDevFabricManage.getFabricWeight(), clothesDevFabricManage.getFabricContent(), clothesDevFabricManage.getUnit(), clothesDevFabricManage.getUnitTwo(), fabricInCount, fabricInCount, 0f, fabricInCountTwo, fabricInCountTwo, 0f, clothesDevFabricManage.getRatio(), clothesDevFabricManage.getFabricColor(), clothesDevFabricManage.getFabricColorNumber(), clothesDevFabricManage.getSupplier(), price, priceTwo, batchNumberIn, batchNumberIn, 0, "storage", clothesDevFabricManage.getRemark(), payState);
            clothesDevFabricManageStorage.setOperateDate(date);
            clothesDevFabricManageStorage.setLocation(location);
            res1 = clothesDevFabricManageService.addClothesDevFabricManage(clothesDevFabricManageStorage);
        }
        int res2 = clothesDevFabricManageService.addClothesDevFabricManage(clothesDevFabricManageIn);
        if (res1 == 0 && res2 == 0){
            map.put("result", 0);
        } else {
            map.put("result", 1);
        }
        return map;
    }

    @RequestMapping(value = "/clothesdevfabricmanageoutstore", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> clothesDevFabricManageOutStore(@RequestParam("id")Integer id,
                                                             @RequestParam("fabricOutCount")Float fabricOutCount,
                                                             @RequestParam("fabricOutCountTwo")Float fabricOutCountTwo,
                                                             @RequestParam("batchNumberOut")Integer batchNumberOut){
        Map<String, Object> map = new LinkedHashMap<>();
        ClothesDevFabricManage clothesDevFabricManage = clothesDevFabricManageService.getClothesDevFabricManageById(id);
        ClothesDevFabricManage clothesDevFabricManageOut = new ClothesDevFabricManage(clothesDevFabricManage.getFabricID(), clothesDevFabricManage.getFabricNumber(), clothesDevFabricManage.getFabricName(), clothesDevFabricManage.getFabricWidth(), clothesDevFabricManage.getFabricWeight(), clothesDevFabricManage.getFabricContent(), clothesDevFabricManage.getUnit(), clothesDevFabricManage.getUnitTwo(), 0f, 0f, fabricOutCount, 0f, 0f, fabricOutCountTwo, clothesDevFabricManage.getRatio(), clothesDevFabricManage.getFabricColor(), clothesDevFabricManage.getFabricColorNumber(), clothesDevFabricManage.getSupplier(), clothesDevFabricManage.getPrice(), clothesDevFabricManage.getPriceTwo(), 0, 0, batchNumberOut, "out", clothesDevFabricManage.getRemark(), null);
        clothesDevFabricManage.setFabricOutCount(clothesDevFabricManage.getFabricOutCount() + fabricOutCount);
        clothesDevFabricManage.setFabricOutCountTwo(clothesDevFabricManage.getFabricOutCountTwo() + fabricOutCountTwo);
        clothesDevFabricManage.setFabricStorageCount(clothesDevFabricManage.getFabricStorageCount() - fabricOutCount);
        clothesDevFabricManage.setFabricStorageCountTwo(clothesDevFabricManage.getFabricStorageCountTwo() - fabricOutCountTwo);
        clothesDevFabricManage.setBatchNumberStorage(clothesDevFabricManage.getBatchNumberStorage() - batchNumberOut);
        clothesDevFabricManage.setBatchNumberOut(clothesDevFabricManage.getBatchNumberOut() + batchNumberOut);
        int res1 = clothesDevFabricManageService.updateClothesDevFabricManage(clothesDevFabricManage);
        int res2 = clothesDevFabricManageService.addClothesDevFabricManage(clothesDevFabricManageOut);
        if (res1 == 0 && res2 == 0){
            map.put("result", 0);
        } else {
            map.put("result", 1);
        }
        return map;
    }

    @RequestMapping(value = "/updateclothesdevfabricmanagetotal", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> updateClothesDevFabricManageTotal(@RequestParam("id")Integer id,
                                                                 @RequestParam("clothesDevFabricManageJson")String clothesDevFabricManageJson){
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        ClothesDevFabricManage clothesDevFabricManage = gson.fromJson(clothesDevFabricManageJson, ClothesDevFabricManage.class);
        int res1 = clothesDevFabricManageService.updateClothesDevFabricManage(clothesDevFabricManage);
        Map<String, Object> param = new HashMap<>();
        param.put("fabricID", id);
        List<ClothesDevFabricManage> clothesDevFabricManageList = clothesDevFabricManageService.getClothesDevFabricManageByMap(param);
        for (ClothesDevFabricManage clothesDevFabricManage1: clothesDevFabricManageList){
            clothesDevFabricManage1.setFabricName(clothesDevFabricManage.getFabricName());
            clothesDevFabricManage1.setFabricNumber(clothesDevFabricManage.getFabricNumber());
            clothesDevFabricManage1.setFabricWidth(clothesDevFabricManage.getFabricWidth());
            clothesDevFabricManage1.setFabricWeight(clothesDevFabricManage.getFabricWeight());
            clothesDevFabricManage1.setFabricContent(clothesDevFabricManage.getFabricContent());
            clothesDevFabricManage1.setUnit(clothesDevFabricManage.getUnit());
            clothesDevFabricManage1.setUnitTwo(clothesDevFabricManage.getUnitTwo());
            clothesDevFabricManage1.setRatio(clothesDevFabricManage.getRatio());
            clothesDevFabricManage1.setPriceTwo(clothesDevFabricManage1.getPrice() * clothesDevFabricManage.getRatio());
            clothesDevFabricManage1.setFabricColor(clothesDevFabricManage.getFabricColor());
            clothesDevFabricManage1.setFabricColorNumber(clothesDevFabricManage.getFabricColorNumber());
            clothesDevFabricManage1.setSupplier(clothesDevFabricManage.getSupplier());
            clothesDevFabricManage1.setRemark(clothesDevFabricManage.getRemark());
        }
        int res2 = clothesDevFabricManageService.updateClothesDevFabricManageBatch(clothesDevFabricManageList);
        if (res1 == 0 && res2 == 0){
            map.put("result", 0);
        } else {
            map.put("result", 1);
        }
        return map;
    }

    @RequestMapping(value = "/deleteclothesdevfabricmanageinbatch", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> deleteClothesDevFabricManageInBatch(@RequestParam("id")Integer id,
                                                                   @RequestParam("idList")List<Integer> idList,
                                                                   @RequestParam("fabricInCount")Float fabricInCount,
                                                                   @RequestParam("batchNumberIn")Integer batchNumberIn){
        Map<String, Object> map = new LinkedHashMap<>();
        Map<String, Object> param = new HashMap<>();
        param.put("fabricID", id);
        param.put("storageType", "storage");
        List<ClothesDevFabricManage> clothesDevFabricManageList = clothesDevFabricManageService.getClothesDevFabricManageByMap(param);
        if (clothesDevFabricManageList == null || clothesDevFabricManageList.isEmpty()){
            map.put("result", 4);
            return map;
        }
        ClothesDevFabricManage clothesDevFabricManageStorage = clothesDevFabricManageList.get(0);
        if (clothesDevFabricManageStorage.getFabricStorageCount() < fabricInCount){
            map.put("result", 4);
            return map;
        }
        clothesDevFabricManageStorage.setFabricInCount(clothesDevFabricManageStorage.getFabricInCount() - fabricInCount);
        clothesDevFabricManageStorage.setFabricStorageCount(clothesDevFabricManageStorage.getFabricStorageCount() - fabricInCount);
        clothesDevFabricManageStorage.setBatchNumberStorage(clothesDevFabricManageStorage.getBatchNumberStorage() - batchNumberIn);
        clothesDevFabricManageStorage.setBatchNumberIn(clothesDevFabricManageStorage.getBatchNumberIn() - batchNumberIn);
        int res1 = clothesDevFabricManageService.updateClothesDevFabricManage(clothesDevFabricManageStorage);
        int res2 = clothesDevFabricManageService.deleteClothesDevFabricManageBatch(idList);
        if (res1 == 0 && res2 == 0){
            map.put("result", 0);
        } else {
            map.put("result", 1);
        }
        return map;
    }


    @RequestMapping(value = "/deleteclothesdevfabricmanageoutbatch", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> deleteClothesDevFabricManageOutBatch(@RequestParam("id")Integer id,
                                                                    @RequestParam("idList")List<Integer> idList,
                                                                    @RequestParam("fabricOutCount")Float fabricOutCount,
                                                                    @RequestParam("batchNumberOut")Integer batchNumberOut){
        Map<String, Object> map = new LinkedHashMap<>();
        Map<String, Object> param = new HashMap<>();
        param.put("fabricID", id);
        param.put("storageType", "storage");
        List<ClothesDevFabricManage> clothesDevFabricManageList = clothesDevFabricManageService.getClothesDevFabricManageByMap(param);
        if (clothesDevFabricManageList == null || clothesDevFabricManageList.isEmpty()){
            map.put("result", 4);
            return map;
        }
        ClothesDevFabricManage clothesDevFabricManageStorage = clothesDevFabricManageList.get(0);
        clothesDevFabricManageStorage.setFabricOutCount(clothesDevFabricManageStorage.getFabricOutCount() - fabricOutCount);
        clothesDevFabricManageStorage.setFabricStorageCount(clothesDevFabricManageStorage.getFabricStorageCount() + fabricOutCount);
        clothesDevFabricManageStorage.setBatchNumberStorage(clothesDevFabricManageStorage.getBatchNumberStorage() + batchNumberOut);
        clothesDevFabricManageStorage.setBatchNumberOut(clothesDevFabricManageStorage.getBatchNumberOut() - batchNumberOut);
        int res1 = clothesDevFabricManageService.updateClothesDevFabricManage(clothesDevFabricManageStorage);
        int res2 = clothesDevFabricManageService.deleteClothesDevFabricManageBatch(idList);
        if (res1 == 0 && res2 == 0){
            map.put("result", 0);
        } else {
            map.put("result", 1);
        }
        return map;
    }



    // 共用面料
    @RequestMapping(value = "/sharefabricinstore", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> shareFabricInStore(@RequestParam("id")Integer id,
                                                  @RequestParam("fabricInCount")Float fabricInCount,
                                                  @RequestParam("fabricInCountTwo")Float fabricInCountTwo,
                                                  @RequestParam("batchNumberIn")Integer batchNumberIn,
                                                  @RequestParam("operateDate")String operateDate,
                                                  @RequestParam("jarName")String jarName,
                                                  @RequestParam("location")String location,
                                                  @RequestParam("price")Float price,
                                                  @RequestParam("priceTwo")Float priceTwo,
                                                  @RequestParam("payState")String payState,
                                                  @RequestParam("remark")String remark) throws Exception{
        Map<String, Object> map = new LinkedHashMap<>();
        ClothesDevFabricManage clothesDevFabricManage = clothesDevFabricManageService.getClothesDevFabricManageById(id);
        ClothesDevFabricManage clothesDevFabricManageIn = new ClothesDevFabricManage(clothesDevFabricManage.getId(), clothesDevFabricManage.getFabricNumber(), clothesDevFabricManage.getFabricName(), clothesDevFabricManage.getFabricWidth(), clothesDevFabricManage.getFabricWeight(), clothesDevFabricManage.getFabricContent(), clothesDevFabricManage.getUnit(), clothesDevFabricManage.getUnitTwo(), fabricInCount, 0f, 0f, fabricInCountTwo, 0f, 0f, clothesDevFabricManage.getRatio(), clothesDevFabricManage.getFabricColor(), clothesDevFabricManage.getFabricColorNumber(), clothesDevFabricManage.getSupplier(), price, priceTwo, batchNumberIn, 0, 0, "shareIn", remark, payState);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = simpleDateFormat.parse(operateDate);
        clothesDevFabricManageIn.setOperateDate(date);
        clothesDevFabricManageIn.setLocation(location);
        clothesDevFabricManageIn.setJarName(jarName);
        clothesDevFabricManageIn.setRemark(remark);
        ClothesDevFabricManage clothesDevFabricManageStorage = new ClothesDevFabricManage(clothesDevFabricManage.getId(), clothesDevFabricManage.getFabricNumber(), clothesDevFabricManage.getFabricName(), clothesDevFabricManage.getFabricWidth(), clothesDevFabricManage.getFabricWeight(), clothesDevFabricManage.getFabricContent(), clothesDevFabricManage.getUnit(), clothesDevFabricManage.getUnitTwo(), fabricInCount, fabricInCount, 0f, fabricInCountTwo, fabricInCountTwo, 0f, clothesDevFabricManage.getRatio(), clothesDevFabricManage.getFabricColor(), clothesDevFabricManage.getFabricColorNumber(), clothesDevFabricManage.getSupplier(), price, priceTwo, batchNumberIn, batchNumberIn, 0, "shareStorage", remark, payState);
        clothesDevFabricManageStorage.setOperateDate(date);
        clothesDevFabricManageStorage.setLocation(location);
        clothesDevFabricManageStorage.setJarName(jarName);
        clothesDevFabricManageStorage.setRemark(remark);
        int res1 = clothesDevFabricManageService.addClothesDevFabricManage(clothesDevFabricManageStorage);
        int res2 = clothesDevFabricManageService.addClothesDevFabricManage(clothesDevFabricManageIn);
        if (res1 == 0 && res2 == 0){
            map.put("result", 0);
        } else {
            map.put("result", 1);
        }
        return map;
    }

    @RequestMapping(value = "/deletesharefabricinstore", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> deleteShareFabricInStore(@RequestParam("id")Integer id) {
        Map<String, Object> map = new LinkedHashMap<>();
        ClothesDevFabricManage clothesDevFabricManage = clothesDevFabricManageService.getClothesDevFabricManageById(id);
        Map<String, Object> param = new HashMap<>();
        param.put("fabricID", clothesDevFabricManage.getFabricID());
        param.put("storageType", "shareStorage");
        param.put("jarName", clothesDevFabricManage.getJarName());
        param.put("location", clothesDevFabricManage.getLocation());
        List<ClothesDevFabricManage> clothesDevFabricManageList = clothesDevFabricManageService.getClothesDevFabricManageByMap(param);
        if (clothesDevFabricManageList == null || clothesDevFabricManageList.isEmpty()){
            map.put("result", 4);
            return map;
        }
        ClothesDevFabricManage clothesDevFabricManageStorage = clothesDevFabricManageList.get(0);
        if (!clothesDevFabricManageStorage.getFabricStorageCountTwo().equals(clothesDevFabricManage.getFabricInCountTwo())){
            map.put("result", 4);
            return map;
        }
        int res1 = clothesDevFabricManageService.deleteClothesDevFabricManage(clothesDevFabricManageStorage.getId());
        int res2 = clothesDevFabricManageService.deleteClothesDevFabricManage(id);
        if (res1 == 0 && res2 == 0){
            map.put("result", 0);
        } else {
            map.put("result", 1);
        }
        return map;
    }

    @RequestMapping(value = "/deletesharefabricoutstore", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> deleteShareFabricOutStore(@RequestParam("id")Integer id) {
        Map<String, Object> map = new LinkedHashMap<>();
        ClothesDevFabricManage clothesDevFabricManage = clothesDevFabricManageService.getClothesDevFabricManageById(id);
        Map<String, Object> param = new HashMap<>();
        param.put("fabricID", clothesDevFabricManage.getFabricID());
        param.put("storageType", "shareStorage");
        param.put("jarName", clothesDevFabricManage.getJarName());
        param.put("location", clothesDevFabricManage.getLocation());
        List<ClothesDevFabricManage> clothesDevFabricManageList = clothesDevFabricManageService.getClothesDevFabricManageByMap(param);
        if (clothesDevFabricManageList == null || clothesDevFabricManageList.isEmpty()){
            map.put("result", 4);
            return map;
        }
        ClothesDevFabricManage clothesDevFabricManageStorage = clothesDevFabricManageList.get(0);
        clothesDevFabricManageStorage.setBatchNumberStorage(clothesDevFabricManageStorage.getBatchNumberStorage() + clothesDevFabricManage.getBatchNumberOut());
        clothesDevFabricManageStorage.setFabricStorageCountTwo(clothesDevFabricManageStorage.getFabricStorageCountTwo() + clothesDevFabricManage.getFabricOutCountTwo());
        clothesDevFabricManageStorage.setFabricStorageCount(clothesDevFabricManageStorage.getFabricStorageCount() + clothesDevFabricManage.getFabricOutCount());
        int res1 = clothesDevFabricManageService.updateClothesDevFabricManage(clothesDevFabricManageStorage);
        int res2 = clothesDevFabricManageService.deleteClothesDevFabricManage(id);
        if (res1 == 0 && res2 == 0){
            map.put("result", 0);
        } else {
            map.put("result", 1);
        }
        return map;
    }

    // 调用
//    @RequestMapping(value = "/usesharefabrictoorder", method = RequestMethod.POST)
//    @ResponseBody
//    public Map<String, Object> changeFabricOrder(@RequestParam("id")Integer id,
//                                                 @RequestParam("fabricID")Integer fabricID,
//                                                 @RequestParam("useWeight")Float useWeight,
//                                                 @RequestParam("useBatch")Integer useBatch){
//        Map<String, Object> map = new LinkedHashMap<>();
//        ClothesDevFabricManage clothesDevFabricManage = clothesDevFabricManageService.getClothesDevFabricManageById(id);
//        // 添加调到的面料信息
//        ManufactureFabric manufactureFabric = manufactureFabricService.getManufactureFabricByID(fabricID);
//        FabricDetail fabricDetail = new FabricDetail();
//        fabricDetail.setOrderName(manufactureFabric.getOrderName());
//        fabricDetail.setClothesVersionNumber(manufactureFabric.getClothesVersionNumber());
//        fabricDetail.setFabricName(manufactureFabric.getFabricName());
//        fabricDetail.setFabricNumber(manufactureFabric.getFabricNumber());
//        fabricDetail.setFabricColor(manufactureFabric.getFabricColor());
//        fabricDetail.setFabricColorNumber(clothesDevFabricManage.getFabricColorNumber());
//        fabricDetail.setColorName(manufactureFabric.getColorName());
//        fabricDetail.setJarName(clothesDevFabricManage.getJarName());
//        fabricDetail.setBatchNumber(useBatch);
//        fabricDetail.setWeight(useWeight * clothesDevFabricManage.getRatio());
//        fabricDetail.setWeightTwo(useWeight);
//        fabricDetail.setLocation(clothesDevFabricManage.getLocation());
//        fabricDetail.setSupplier(clothesDevFabricManage.getSupplier());
//        fabricDetail.setUnit(clothesDevFabricManage.getUnit());
//        fabricDetail.setUnitTwo(clothesDevFabricManage.getUnitTwo());
//        fabricDetail.setRatio(clothesDevFabricManage.getRatio());
//        fabricDetail.setFabricID(manufactureFabric.getFabricID());
//        fabricDetail.setIsHit(manufactureFabric.getIsHit());
//        fabricDetail.setReturnTime(clothesDevFabricManage.getOperateDate());
//        fabricDetail.setOperateType("共用面料调用");
//        fabricDetail.setRemark("由共用面料调用");
//        List<FabricDetail> addFabricDetailList = new ArrayList<>();
//        addFabricDetailList.add(fabricDetail);
//        if (!manufactureFabric.getCheckState().equals("通过")){
//            map.put("result", 3);
//            return map;
//        }
//        // 共用面料处理
//        clothesDevFabricManage.setFabricStorageCountTwo(clothesDevFabricManage.getFabricStorageCountTwo() - useWeight);
//        clothesDevFabricManage.setFabricStorageCount(clothesDevFabricManage.getFabricStorageCount() - useWeight * clothesDevFabricManage.getRatio());
//        clothesDevFabricManage.setBatchNumberStorage(clothesDevFabricManage.getBatchNumberStorage() - useBatch);
//        if (clothesDevFabricManage.getBatchNumberStorage().equals(0)){
//            clothesDevFabricManage.setFabricStorageCount(0f);
//            clothesDevFabricManage.setFabricStorageCountTwo(0f);
//        }
//        clothesDevFabricManageService.updateClothesDevFabricManage(clothesDevFabricManage);
//        clothesDevFabricManage.setOrderName(manufactureFabric.getOrderName());
//        clothesDevFabricManage.setClothesVersionNumber(manufactureFabric.getClothesVersionNumber());
//        clothesDevFabricManage.setFabricStorageCount(0f);
//        clothesDevFabricManage.setFabricOutCountTwo(useWeight);
//        clothesDevFabricManage.setFabricOutCount(useWeight * clothesDevFabricManage.getRatio());
//        clothesDevFabricManage.setBatchNumberStorage(0);
//        clothesDevFabricManage.setBatchNumberOut(useBatch);
//        clothesDevFabricManage.setStorageType("shareOut");
//        int res1 = clothesDevFabricManageService.addClothesDevFabricManage(clothesDevFabricManage);
//        int res3 = 0;
//        FabricStorage newFabricStorage = fabricStorageService.getFabricStorageByOrderColorLocationJar(null, null, null, null, clothesDevFabricManage.getJarName(), clothesDevFabricManage.getLocation(), fabricID);
//        if (newFabricStorage == null){
//            FabricStorage fs = new FabricStorage(clothesDevFabricManage.getLocation(), manufactureFabric.getOrderName(), manufactureFabric.getClothesVersionNumber(), manufactureFabric.getFabricName(), manufactureFabric.getFabricNumber(), manufactureFabric.getFabricColor(), manufactureFabric.getFabricColorNumber(), clothesDevFabricManage.getJarName(), useWeight * clothesDevFabricManage.getRatio(), useWeight, useBatch, "入库", clothesDevFabricManage.getOperateDate(), manufactureFabric.getColorName(), clothesDevFabricManage.getUnit(), clothesDevFabricManage.getUnitTwo(), clothesDevFabricManage.getRatio(), manufactureFabric.getIsHit(), manufactureFabric.getSupplier(), fabricID);
//            fs.setFabricColorNumber(clothesDevFabricManage.getFabricColorNumber());
//            res3 += fabricStorageService.addFabricStorage(fs);
//        }else{
//            newFabricStorage.setWeight(newFabricStorage.getWeight() + useWeight * clothesDevFabricManage.getRatio());
//            newFabricStorage.setWeightTwo(newFabricStorage.getWeight() + useWeight);
//            newFabricStorage.setBatchNumber(newFabricStorage.getBatchNumber() + useBatch);
//            res3 += fabricStorageService.updateFabricStorage(newFabricStorage);
//        }
//        int res2 = fabricDetailService.addFabricDetailBatch(addFabricDetailList);
//        if (res1 == 0 && res2 == 0 && res3 == 0){
//            map.put("result", 0);
//        } else {
//            map.put("result", 1);
//        }
//        return map;
//    }

    @RequestMapping(value = "/getfabricnameorderbyprefix", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getFabricNameOrderByPreFix(@RequestParam("fabricNumberCode")String fabricNumberCode){
        Map<String, Object> map = new LinkedHashMap<>();
        String maxOrder = clothesDevFabricManageService.getMaxClothesDevFabricOrderByPreFix(fabricNumberCode);
        int a = Integer.valueOf(maxOrder).intValue();
        String orderStr = String.format("%05d", a + 1);
        map.put("fabricOrder", orderStr);
        return map;
    }

    @RequestMapping(value = "/getsharefabricstorage", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getShareFabricStorage(@RequestParam("storageType")String storageType,
                                                     @RequestParam(value = "supplier",required = false)String supplier,
                                                     @RequestParam(value = "from", required = false)String from,
                                                     @RequestParam(value = "to", required = false)String to){
        Map<String, Object> map = new LinkedHashMap<>();
        Map<String, Object> param = new HashMap<>();
        param.put("storageType", storageType);
        param.put("supplier", supplier);
        param.put("from", from);
        param.put("to", to);
        List<ClothesDevFabricManage> clothesDevFabricManageList = clothesDevFabricManageService.getClothesDevFabricManageByMap(param);
        map.put("data", clothesDevFabricManageList);
        return map;
    }

    @RequestMapping(value = "/clothesdevfabricupdateincheck", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> getShareFabricStorage(@RequestParam("clothesDevFabricJson")String clothesDevFabricJson,
                                                     @RequestParam("checkMonth")String checkMonth){
        Map<String, Object> map = new LinkedHashMap<>();
        Map<String, Object> param = new HashMap<>();
        param.put("checkMonth", checkMonth);
        CheckMonth fixedMonth = checkMonthService.getCheckMonthByInfo(param);
        if (fixedMonth != null){
            map.put("result", 3);
            return map;
        }
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        List<ClothesDevFabricManage> clothesDevFabricManageList = gson.fromJson(clothesDevFabricJson,new TypeToken<List<ClothesDevFabricManage>>(){}.getType());
        int res = clothesDevFabricManageService.updateClothesDevFabricManageBatch(clothesDevFabricManageList);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/quitclothesdevfabricbatch", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> quitClothesDevFabricBatch(@RequestParam("idList")List<Integer> idList){
        Map<String, Object> map = new LinkedHashMap<>();
        int res = clothesDevFabricManageService.quitClothesDevFabricManageCheck(idList);
        map.put("result", res);
        return map;
    }

}
