package com.example.erp01.action;

import com.example.erp01.model.OtherStore;
import com.example.erp01.service.OtherStoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "erp")
public class OtherStoreController {

    @Autowired
    private OtherStoreService otherStoreService;

    @RequestMapping(value = "/minigetotherstorebylocation", method = RequestMethod.GET)
    @ResponseBody
    public int miniGetOtherStoreByLocation(@RequestParam("otherStoreLocation")String otherStoreLocation){
        OtherStore otherStore = otherStoreService.getOtherStoreByLocation(otherStoreLocation);
        if (otherStore == null){
            return 1;
        }else{
            return 0;
        }
    }

}
