package com.example.erp01.action;

import com.example.erp01.model.*;
import com.example.erp01.service.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.sun.corba.se.impl.ior.OldJIDLObjectKeyTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.sql.Timestamp;
import java.util.*;

@Controller
@RequestMapping(value = "erp")
public class OrderSalaryController {

    @Autowired
    private OrderSalaryService orderSalaryService;
    @Autowired
    private OrderProcedureService orderProcedureService;
    @Autowired
    private OrderClothesService orderClothesService;
    @Autowired
    private TailorService tailorService;
    @Autowired
    private FixedSalaryService fixedSalaryService;
    @Autowired
    private PieceWorkService pieceWorkService;

    @RequestMapping("/orderSalaryStart")
    public String orderSalaryStart(){
        return "finance/orderSalary";
    }

    @RequestMapping("/excessReportStart")
    public String excessReportStart(){
        return "finance/excessReport";
    }

    @RequestMapping(value = "/getordersalarybyorderprocedure", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getOrderSalaryByOrderProcedure(@RequestParam("orderName")String orderName,
                                                              @RequestParam(value = "type", required = false)Integer type,
                                                              @RequestParam(value = "viewType", required = false)Integer viewType,
                                                              @RequestParam(value = "procedureNumber", required = false)Integer procedureNumber){
        Map<String, Object> map = new LinkedHashMap<>();
        List<OrderProcedure> orderProcedureList = orderProcedureService.getOrderProcedureByOrder(orderName);
        Integer orderCount = orderClothesService.getOrderTotalCount(orderName);
        Integer cutCount = tailorService.getTailorCountByInfo(orderName, null, null, null, null, null, 0);
        Integer wellCount = tailorService.getWellCountByInfo(orderName, null, null, null, null, null, 0);
        List<OrderSalary> orderSalaryTotalList = orderSalaryService.getOrderSalarySummaryByOrderProcedure(orderName, procedureNumber);
        List<OrderSalary> orderSalaryMonthList = orderSalaryService.getOrderSalarySummaryByOrderProcedureMonth(orderName, procedureNumber);
        List<OrderSalary> cutSalaryList = orderSalaryService.getOrderCutInfoByMonth(orderName);
        List<OrderSalary> destOrderSalaryList = new ArrayList<>();
        if (type.equals(0)){
            for (OrderSalary orderMonthSalary : orderSalaryTotalList){
                for (OrderProcedure orderProcedure : orderProcedureList){
                    if (orderMonthSalary.getProcedureNumber().equals(orderProcedure.getProcedureNumber())){
                        OrderSalary tmpSalary = new OrderSalary(orderProcedure.getOrderName(), orderProcedure.getClothesVersionNumber(), orderProcedure.getProcedureNumber(), orderProcedure.getProcedureName());
                        tmpSalary.setMonthString(orderMonthSalary.getMonthString());
                        tmpSalary.setOrderCount(orderCount);
                        tmpSalary.setWellCount(wellCount);
                        tmpSalary.setPieceCount(orderMonthSalary.getPieceCount());
                        tmpSalary.setAmongPieceCount(orderMonthSalary.getPieceCount());
                        tmpSalary.setPriceSalary(orderProcedure.getPiecePrice());
                        tmpSalary.setPieceSalaryTwo(orderProcedure.getPiecePriceTwo());
                        tmpSalary.setAddition(orderProcedure.getSubsidy());
                        tmpSalary.setSumPrice((orderProcedure.getPiecePrice() + orderProcedure.getPiecePriceTwo())*(1+orderProcedure.getSubsidy()));
                        tmpSalary.setSumSalary(tmpSalary.getPieceCount() * tmpSalary.getSumPrice());
                        destOrderSalaryList.add(tmpSalary);
                    }
                }
            }
        }
        else {
            for (OrderSalary orderMonthSalary : orderSalaryTotalList){
                for (OrderProcedure orderProcedure : orderProcedureList){
                    if (orderMonthSalary.getProcedureNumber().equals(orderProcedure.getProcedureNumber())
                    && orderProcedure.getReviewState().equals(2)){
                        OrderSalary tmpSalary = new OrderSalary(orderProcedure.getOrderName(), orderProcedure.getClothesVersionNumber(), orderProcedure.getProcedureNumber(), orderProcedure.getProcedureName());
                        tmpSalary.setMonthString(orderMonthSalary.getMonthString());
                        tmpSalary.setOrderCount(orderCount);
                        tmpSalary.setWellCount(wellCount);
                        tmpSalary.setPieceCount(orderMonthSalary.getPieceCount());
                        tmpSalary.setAmongPieceCount(orderMonthSalary.getPieceCount());
                        tmpSalary.setPriceSalary(orderProcedure.getPiecePrice());
                        tmpSalary.setPieceSalaryTwo(orderProcedure.getPiecePriceTwo());
                        tmpSalary.setAddition(orderProcedure.getSubsidy());
                        tmpSalary.setSumPrice((orderProcedure.getPiecePrice() + orderProcedure.getPiecePriceTwo())*(1+orderProcedure.getSubsidy()));
                        tmpSalary.setSumSalary(tmpSalary.getPieceCount() * tmpSalary.getSumPrice());
                        destOrderSalaryList.add(tmpSalary);
                    }
                }
            }
        }
        map.put("orderSalaryList", destOrderSalaryList);
        return map;
    }


    @RequestMapping(value = "/getmonthordersalary", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> getMonthOrderSalary(@RequestParam("from")String from,
                                                   @RequestParam("to")String to,
                                                   @RequestParam(value = "type", required = false)Integer type,
                                                   @RequestParam(value = "viewType", required = false)Integer viewType,
                                                   @RequestParam(value = "orderName", required = false)String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> orderNameList1 = new ArrayList<>();
        if (orderName != null && !orderName.equals("")){
            orderNameList1.add(orderName);
        } else {
            orderNameList1 = orderSalaryService.getCutOrderNameListByMonth(from, to);
            List<String> orderNameList2 = orderSalaryService.getPieceWorkOrderNameListByMonth(from ,to);
            orderNameList1.removeAll(orderNameList2);
            orderNameList1.addAll(orderNameList2);
        }
        List<OrderSalary> orderSalaryList1 = orderSalaryService.getOrderCountByOrderNameList(orderNameList1);
        List<OrderSalary> orderSalaryList2 = orderSalaryService.getMonthCutCountByMonthOrderNameList(from, to, orderNameList1);
        List<OrderSalary> orderSalaryList3 = orderSalaryService.getMonthCutCountByMonthOrderNameList(from, to, orderNameList1);
        List<OrderSalary> orderSalaryList4 = orderSalaryService.getMonthPieceSalary(from, to, orderNameList1);
        List<OrderSalary> orderSalaryList5 = orderSalaryService.getMonthPieceSalary(from, to, orderNameList1);
        List<OrderSalary> orderSalaryList6 = orderSalaryService.getMonthHourSalary(from, to);
        List<OrderProcedure> orderProcedureList = orderProcedureService.getOrderProcedureByOrderNames(orderNameList1, null);
        List<OrderSalary> orderSalaryList = new ArrayList<>();
        for (OrderProcedure orderProcedure : orderProcedureList){
            OrderSalary orderSalary = new OrderSalary(orderProcedure.getOrderName(), orderProcedure.getClothesVersionNumber(), orderProcedure.getProcedureNumber(), orderProcedure.getProcedureName());
            orderSalary.setMonthString(from);
            for (OrderSalary orderSalary1 : orderSalaryList1){
                if (orderSalary1.getOrderName().equals(orderProcedure.getOrderName())){
                    orderSalary.setOrderCount(orderSalary1.getOrderCount());
                }
            }
            for (OrderSalary orderSalary2 : orderSalaryList2){
                if (orderSalary2.getOrderName().equals(orderProcedure.getOrderName())){
                    orderSalary.setCutCount(orderSalary2.getCutCount());
                    orderSalary.setWellCount(orderSalary2.getWellCount());
                }
            }
            if (orderProcedure.getProcedureNumber().equals(1)){
                for (OrderSalary orderSalary2 : orderSalaryList2){
                    if (orderSalary2.getOrderName().equals(orderProcedure.getOrderName())){
                        orderSalary.setPieceCount((float)orderSalary2.getCutCount());
                    }
                }
                for (OrderSalary orderSalary3 : orderSalaryList3){
                    if (orderSalary3.getOrderName().equals(orderProcedure.getOrderName())){
                        orderSalary.setAmongPieceCount((float)orderSalary3.getCutCount());
                        if (type.equals(0) || orderProcedure.getReviewState().equals(2)){
                            orderSalary.setPriceSalary(orderProcedure.getPiecePrice());
                            orderSalary.setPieceSalaryTwo(orderProcedure.getPiecePriceTwo());
                            orderSalary.setAddition(orderProcedure.getSubsidy());
                            orderSalary.setSumPrice((1+orderProcedure.getSubsidy()) * orderProcedure.getPiecePrice() + orderProcedure.getPiecePriceTwo());
                            orderSalary.setSumSalary(orderSalary3.getCutCount()*orderSalary.getSumPrice());
                        } else {
                            orderSalary.setPriceSalary(0d);
                            orderSalary.setPieceSalaryTwo(0d);
                            orderSalary.setAddition(0f);
                            orderSalary.setSumPrice(0d);
                            orderSalary.setSumSalary(0d);
                        }
                    }
                }
            } else {
                for (OrderSalary orderSalary4 : orderSalaryList4){
                    if (orderSalary4.getOrderName().equals(orderProcedure.getOrderName()) && orderSalary4.getProcedureNumber().equals(orderProcedure.getProcedureNumber())){
                        orderSalary.setPieceCount(orderSalary4.getPieceCount());
                    }
                }
                for (OrderSalary orderSalary5 : orderSalaryList5){
                    if (orderSalary5.getOrderName().equals(orderProcedure.getOrderName()) && orderSalary5.getProcedureNumber().equals(orderProcedure.getProcedureNumber())){
                        orderSalary.setAmongPieceCount(orderSalary5.getPieceCount());
                        if (type.equals(0) || orderProcedure.getReviewState().equals(2)){
                            orderSalary.setPriceSalary(orderProcedure.getPiecePrice());
                            orderSalary.setPieceSalaryTwo(orderProcedure.getPiecePriceTwo());
                            orderSalary.setAddition(orderProcedure.getSubsidy());
                            orderSalary.setSumPrice((1+orderProcedure.getSubsidy()) * orderProcedure.getPiecePrice() + orderProcedure.getPiecePriceTwo());
                            orderSalary.setSumSalary(orderSalary5.getPieceCount()*orderSalary.getSumPrice());
                        } else {
                            orderSalary.setPriceSalary(0d);
                            orderSalary.setPieceSalaryTwo(0d);
                            orderSalary.setAddition(0f);
                            orderSalary.setSumPrice(0d);
                            orderSalary.setSumSalary(0d);
                        }
                    }
                }
            }
            if (viewType.equals(0)){
                if (orderSalary.getAmongPieceCount() > 0){
                    orderSalaryList.add(orderSalary);
                }
            } else {
                orderSalaryList.add(orderSalary);
            }
        }
        orderSalaryList.addAll(orderSalaryList6);
        map.put("orderSalaryList", orderSalaryList);
        map.put("data",orderSalaryList);
        map.put("code",0);
        map.put("msg",null);
        return map;
    }


    @RequestMapping(value = "/getexcessreport", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getExcessReport(@RequestParam(value = "from", required = false)String from,
                                               @RequestParam(value = "to", required = false)String to,
                                               @RequestParam(value = "orderName", required = false)String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> orderNameList1 = new ArrayList<>();
        if (orderName != null && !"".equals(orderName)){
            orderNameList1.add(orderName);
        } else {
            List<String> orderNameList2 = orderSalaryService.getPieceWorkOrderNameListByMonth(from, to);
            orderNameList1.addAll(orderNameList2);
        }
        List<OrderSalary> timeOrderSalaryList = orderSalaryService.getMonthPieceSalary(from, to, orderNameList1);
        List<OrderSalary> orderSalaryList1 = orderSalaryService.getOrderCountByOrderNameList(orderNameList1);
        List<OrderSalary> orderSalaryList2 = orderSalaryService.getMonthCutCountByMonthOrderNameList(null, null, orderNameList1);
        List<OrderSalary> orderSalaryList4 = orderSalaryService.getMonthPieceSalary(null, null, orderNameList1);
        List<OrderProcedure> orderProcedureList = orderProcedureService.getOrderProcedureByOrderNames(orderNameList1, null);
        List<OrderSalary> orderSalaryList = new ArrayList<>();
        List<OrderProcedure> orderProcedureList1 = new ArrayList<>();
        List<OrderProcedure> orderProcedureList2 = new ArrayList<>();
        for (OrderProcedure orderProcedure : orderProcedureList){
            if ("全部".equals(orderProcedure.getColorName()) && "全部".equals(orderProcedure.getSizeName())){
                orderProcedureList1.add(orderProcedure);
            }
            else {
                orderProcedureList2.add(orderProcedure);
            }
        }
        for (OrderProcedure orderProcedure : orderProcedureList1){
            boolean flag = false;
            for (OrderSalary orderSalaryTmp : timeOrderSalaryList){
                if (orderProcedure.getOrderName().equals(orderSalaryTmp.getOrderName())
                && orderProcedure.getProcedureNumber().equals(orderSalaryTmp.getProcedureNumber())){
                    flag = true;
                    break;
                }
            }
            if (flag){
                OrderSalary orderSalary = new OrderSalary(orderProcedure.getOrderName(), orderProcedure.getClothesVersionNumber(), orderProcedure.getProcedureNumber(), orderProcedure.getProcedureName());
                for (OrderSalary orderSalary1 : orderSalaryList1){
                    if (orderSalary1.getOrderName().equals(orderProcedure.getOrderName())){
                        orderSalary.setOrderCount(orderSalary1.getOrderCount());
                    }
                }
                for (OrderSalary orderSalary2 : orderSalaryList2){
                    if (orderSalary2.getOrderName().equals(orderProcedure.getOrderName())){
                        orderSalary.setWellCount(orderSalary2.getWellCount());
                    }
                }
                for (OrderSalary orderSalary4 : orderSalaryList4){
                    if (orderSalary4.getOrderName().equals(orderProcedure.getOrderName()) && orderSalary4.getProcedureNumber().equals(orderProcedure.getProcedureNumber())){
                        orderSalary.setPieceCount(orderSalary4.getPieceCount());
                    }
                }
                if (orderSalary.getPieceCount() > orderSalary.getWellCount()){
                    orderSalary.setCutCount(Math.round(orderSalary.getPieceCount()) - orderSalary.getWellCount());
                    orderSalaryList.add(orderSalary);
                }
            }

        }
        for (OrderProcedure orderProcedure : orderProcedureList2){
            boolean flag = false;
            for (OrderSalary orderSalaryTmp : timeOrderSalaryList){
                if (orderProcedure.getOrderName().equals(orderSalaryTmp.getOrderName())
                        && orderProcedure.getProcedureNumber().equals(orderSalaryTmp.getProcedureNumber())){
                    flag = true;
                    break;
                }
            }
            if (flag){
                OrderSalary orderSalary = new OrderSalary(orderProcedure.getOrderName(), orderProcedure.getClothesVersionNumber(), orderProcedure.getProcedureNumber(), orderProcedure.getProcedureName());

                // 获取制单数量
                List<String> colorList;
                List<String> sizeList;
                if ("全部".equals(orderProcedure.getColorName())){
                    colorList = null;
                } else {
                    colorList = Arrays.asList(orderProcedure.getColorName().split(","));
                }
                if ("全部".equals(orderProcedure.getSizeName())){
                    sizeList = null;
                } else {
                    sizeList = Arrays.asList(orderProcedure.getSizeName().split(","));
                }
                int tmpOrderCount = orderClothesService.getOrderCountByColorSizeList(orderProcedure.getOrderName(), colorList, sizeList);
                orderSalary.setOrderCount(tmpOrderCount);
                int tmpWellCount = tailorService.getWellCountByColorSizeList(orderProcedure.getOrderName(), colorList, sizeList);
                orderSalary.setWellCount(tmpWellCount);
                for (OrderSalary orderSalary4 : orderSalaryList4){
                    if (orderSalary4.getOrderName().equals(orderProcedure.getOrderName()) && orderSalary4.getProcedureNumber().equals(orderProcedure.getProcedureNumber())){
                        orderSalary.setPieceCount(orderSalary4.getPieceCount());
                    }
                }
                if (orderSalary.getPieceCount() > orderSalary.getWellCount()){
                    orderSalary.setCutCount(Math.round(orderSalary.getPieceCount()) - orderSalary.getWellCount());
                    orderSalaryList.add(orderSalary);
                }
            }

        }
        map.put("data",orderSalaryList);
        map.put("code",0);
        map.put("msg",null);
        return map;
    }


    @RequestMapping(value = "/excessdecrease", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> excessDecrease(@RequestParam("orderName")String orderName,
                                              @RequestParam("procedureNumber")Integer procedureNumber,
                                              @RequestParam("wellCount")Integer wellCount,
                                              @RequestParam("pieceCount")Integer pieceCount,
                                              @RequestParam("createTime")String createTime){
        Map<String, Object> map = new LinkedHashMap<>();
        List<String> fixedSalaryList = fixedSalaryService.getAllFixedSalaryValue();
        String createMonth = createTime.substring(0, 7);
        for (String fixedValue : fixedSalaryList){
            if (createMonth.equals(fixedValue)){
                map.put("result", 4);
                return map;
            }
        }
        List<GeneralSalary> generalSalaryList = orderSalaryService.getOrderProcedureCountSummary(orderName, procedureNumber);
        int decreaseCount = pieceCount - wellCount;
        List<PieceWork> pieceWorkList = new ArrayList<>();
        Timestamp ts = new Timestamp(System.currentTimeMillis());
        ts = Timestamp.valueOf(createTime.concat(" 12:00:00"));
        int i;
        int fc = 0;
        for (i = 0; i < generalSalaryList.size() - 1; i ++){
            GeneralSalary generalSalary = generalSalaryList.get(i);
            int dc = Math.round(generalSalary.getPieceCount() / pieceCount * decreaseCount);
            if (dc > 0){
                fc += dc;
                PieceWork pw = new PieceWork(generalSalary.getEmployeeNumber(), generalSalary.getEmployeeName(), generalSalary.getGroupName(), orderName, generalSalary.getClothesVersionNumber(), "0", "0",-dc,procedureNumber,generalSalary.getProcedureName(),"admin",ts);
                pw.setWorkLine(generalSalary.getGroupName());
                pieceWorkList.add(pw);
            }
        }
        GeneralSalary generalSalary = generalSalaryList.get(i);
        int dc = decreaseCount - fc;
        if (dc > 0){
            PieceWork pw = new PieceWork(generalSalary.getEmployeeNumber(), generalSalary.getEmployeeName(), generalSalary.getGroupName(), orderName, generalSalary.getClothesVersionNumber(), "0", "0",-dc,procedureNumber,generalSalary.getProcedureName(),"admin",ts);
            pw.setWorkLine(generalSalary.getGroupName());
            pieceWorkList.add(pw);
        }
        int res = pieceWorkService.addPieceWorkTotalBatch(pieceWorkList);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/getexcessdetaildata", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getExcessDetailData(@RequestParam("orderName")String orderName,
                                                   @RequestParam("procedureNumber")Integer procedureNumber){
        Map<String, Object> map = new LinkedHashMap<>();
        List<GeneralSalary> generalSalaryList = orderSalaryService.getOrderProcedureCountSummary(orderName, procedureNumber);
        map.put("data", generalSalaryList);
        return map;
    }

    @RequestMapping(value = "/getexcessdetailbyinfo", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getExcessDetailByInfo(@RequestParam("procedureJson")String procedureJson){
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        List<ProcedureDto> procedureDtoList = gson.fromJson(procedureJson,new TypeToken<List<ProcedureDto>>(){}.getType());
        List<String> orderNameList = new ArrayList<>();
        for (ProcedureDto procedureDto : procedureDtoList){
            if (!orderNameList.contains(procedureDto.getOrderName())){
                orderNameList.add(procedureDto.getOrderName());
            }
        }
        List<OrderProcedure> orderProcedureList = orderProcedureService.getOrderProcedureByOrderNames(orderNameList, null);
        List<ProcedureDto> resultList = new ArrayList<>();
        for (ProcedureDto procedureDto : procedureDtoList){
            String orderName = procedureDto.getOrderName();
            String procedureName = procedureDto.getProcedureName();
            Integer procedureNumber = procedureDto.getProcedureNumber();
            Integer orderCount = procedureDto.getOrderCount();
            Integer wellCount = procedureDto.getWellCount();
            Integer pieceCount = procedureDto.getPieceCount();
            List<GeneralSalary> generalSalaryList = orderSalaryService.getOrderProcedureCountSummary(orderName, procedureNumber);
            for (GeneralSalary generalSalary : generalSalaryList){
                float price = 0;
                for (OrderProcedure orderProcedure : orderProcedureList){
                    if (generalSalary.getOrderName().equals(orderProcedure.getOrderName())
                            && generalSalary.getProcedureNumber().equals(orderProcedure.getProcedureNumber())){
                        price = (float) orderProcedure.getPiecePrice();
                    }
                }
                ProcedureDto procedureDtoNew = new ProcedureDto();
                procedureDtoNew.setOrderName(orderName);
                procedureDtoNew.setProcedureName(procedureName);
                procedureDtoNew.setProcedureNumber(procedureNumber);
                procedureDtoNew.setEmployeeNumber(generalSalary.getEmployeeNumber());
                procedureDtoNew.setEmployeeName(generalSalary.getEmployeeName());
                procedureDtoNew.setGroupName(generalSalary.getGroupName());
                procedureDtoNew.setOrderCount(orderCount);
                procedureDtoNew.setWellCount(wellCount);
                procedureDtoNew.setPieceCount(pieceCount);
                procedureDtoNew.setOverCount(pieceCount - wellCount);
                procedureDtoNew.setEmployeeCount((int)generalSalary.getPieceCount().floatValue());
                procedureDtoNew.setDecreaseCount(Math.round((pieceCount - wellCount) * (float)procedureDtoNew.getEmployeeCount()/pieceCount));
                procedureDtoNew.setPrice(price);
                procedureDtoNew.setDecreaseMoney(procedureDtoNew.getDecreaseCount() * price);
                resultList.add(procedureDtoNew);
            }
        }
        map.put("data", resultList);
        return map;
    }


}
