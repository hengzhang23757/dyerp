package com.example.erp01.action;

import com.example.erp01.model.*;
import com.example.erp01.service.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.SimpleDateFormat;
import java.util.*;

@Controller
@RequestMapping(value = "erp")
public class AccessoryStorageController {

    @Autowired
    private AccessoryInStoreService accessoryInStoreService;
    @Autowired
    private AccessoryStorageService accessoryStorageService;
    @Autowired
    private ManufactureAccessoryService manufactureAccessoryService;
    @Autowired
    private AccessoryOutRecordService accessoryOutRecordService;
    @Autowired
    private OrderClothesService orderClothesService;
    @Autowired
    private TailorService tailorService;
    @Autowired
    private FabricDetailService fabricDetailService;
    @Autowired
    private FabricOutRecordService fabricOutRecordService;
    @Autowired
    private ManufactureFabricService manufactureFabricService;
    @Autowired
    private AccessoryPreStoreService accessoryPreStoreService;

    @RequestMapping("/accessoryInStoreStart")
    public String accessoryInStoreStart(){
        return "accessory/accessoryReturn";
    }

    @RequestMapping("/accessoryStorageStart")
    public String accessoryStorageStart(){
        return "accessory/accessoryStorage";
    }

    @RequestMapping("/fabricAccessoryPrintStart")
    public String fabricAccessoryPrintStart(){
        return "basicInfo/fabricAccessoryPrint";
    }

    @RequestMapping(value = "/accessoryinstorebatch",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> addAccessoryBatch(@RequestParam("accessoryInStoreJson")String accessoryInStoreJson){
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        List<AccessoryInStore> accessoryInStoreList = gson.fromJson(accessoryInStoreJson,new TypeToken<List<AccessoryInStore>>(){}.getType());
        List<AccessoryStorage> accessoryStorageList = gson.fromJson(accessoryInStoreJson,new TypeToken<List<AccessoryStorage>>(){}.getType());
        String orderName = accessoryInStoreList.get(0).getOrderName();
        List<Integer> accessoryIDList = new ArrayList<>();
        for (AccessoryInStore accessoryInStore : accessoryInStoreList){
            accessoryIDList.add(accessoryInStore.getAccessoryID());
        }
        List<ManufactureAccessory> manufactureAccessoryList = manufactureAccessoryService.getManufactureAccessoryByIdList(accessoryIDList);
        for (ManufactureAccessory manufactureAccessory : manufactureAccessoryList){
            if (!manufactureAccessory.getCheckState().equals("通过")){
                map.put("result", 3);
                map.put("msg", manufactureAccessory.getAccessoryName() + "审核未通过,请先审核~~");
                return map;
            }
        }
        List<AccessoryPreStore> accessoryPreStoreListUpdate = new ArrayList<>();
        List<AccessoryPreStore> accessoryPreStoreListInsert = new ArrayList<>();
        // 判断入账数量
        List<AccessoryInStore> accessoryInStoreList1 = accessoryInStoreService.getAccessoryInStoreByOrder(orderName);
        for (AccessoryInStore accessoryInStore : accessoryInStoreList){
            // 调用
            if (accessoryInStore.getUseCount() > 0){
                accessoryInStore.setInStoreCount(accessoryInStore.getUseCount());
                accessoryInStore.setAccountCount(accessoryInStore.getUseCount());
                AccessoryPreStore thisAccessoryPreStore = accessoryPreStoreService.getMaxStorageAccessoryPreStoreByAccessoryNumber(accessoryInStore.getAccessoryNumber(), null, null);
                // 填充单价
                accessoryInStore.setPrice(thisAccessoryPreStore.getPrice());
                thisAccessoryPreStore.setStorageCount(thisAccessoryPreStore.getStorageCount() - accessoryInStore.getUseCount());
                accessoryPreStoreListUpdate.add(thisAccessoryPreStore);
                Integer preStoreId = thisAccessoryPreStore.getPreStoreId();
                AccessoryPreStore accessoryPreStoreIndex = accessoryPreStoreService.getAccessoryPreStoreById(preStoreId);
                accessoryPreStoreIndex.setOrderName(accessoryInStore.getOrderName());
                accessoryPreStoreIndex.setClothesVersionNumber(accessoryInStore.getClothesVersionNumber());
                accessoryPreStoreIndex.setOperateType("out");
                accessoryPreStoreIndex.setOutStoreCount(accessoryInStore.getUseCount());
                accessoryPreStoreIndex.setPreStoreId(preStoreId);
                accessoryPreStoreListInsert.add(accessoryPreStoreIndex);
            } else {
                // 入库
                float accessoryCount = 0f;
                float accountCount = 0f;
                float inStoreCount = accessoryInStore.getInStoreCount();
                for (ManufactureAccessory manufactureAccessory : manufactureAccessoryList){
                    if (manufactureAccessory.getAccessoryID().equals(accessoryInStore.getAccessoryID())){
                        accessoryCount = manufactureAccessory.getAccessoryCount();
                    }
                }
                for (AccessoryInStore accessoryInStore1 : accessoryInStoreList1){
                    if (accessoryInStore1.getAccessoryID().equals(accessoryInStore.getAccessoryID())){
                        accountCount += accessoryInStore1.getAccountCount();
                    }
                }
                if (accountCount + inStoreCount <= accessoryCount){
                    accessoryInStore.setAccountCount(inStoreCount);
                } else {
                    accessoryInStore.setAccountCount((accessoryCount - accountCount) <= 0 ? 0 : (accessoryCount - accountCount));
                }
            }
        }
        int res1 = accessoryInStoreService.addAccessoryInStoreBatch(accessoryInStoreList);
        List<AccessoryStorage> accessoryStorageList1 = accessoryStorageService.getAccessoryStorageByInfo(orderName, null, null, null, null, null, null);
        List<AccessoryStorage> accessoryStorageList2 = new ArrayList<>();
        for (AccessoryStorage accessoryStorage : accessoryStorageList){
            boolean flag = true;
            for (AccessoryStorage accessoryStorage1 : accessoryStorageList1){
                if (accessoryStorage.getAccessoryID().equals(accessoryStorage1.getAccessoryID()) && accessoryStorage.getAccessoryLocation().equals(accessoryStorage1.getAccessoryLocation())){
                    accessoryStorage1.setStorageCount(accessoryStorage.getStorageCount() + accessoryStorage1.getStorageCount());
                    flag = false;
                }
            }
            if (flag){
                accessoryStorageList2.add(accessoryStorage);
            }
        }
        int res2 = accessoryStorageService.addAccessoryStorageBatch(accessoryStorageList2);
        int res3 = accessoryStorageService.updateAccessoryStorageBatch(accessoryStorageList1);
        int res4 = accessoryPreStoreService.updateAccessoryPreStoreBatch(accessoryPreStoreListUpdate);
        int res5 = accessoryPreStoreService.addAccessoryPreStoreBatch(accessoryPreStoreListInsert);
        if (res1 == 0 && res2 == 0 && res3 == 0){
            map.put("result", 0);
        }else {
            map.put("result", 1);
        }
        return map;
    }

    @RequestMapping(value = "/accessoryinstorebatchchecknumber",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> accessoryInStoreBatchCheckNumber(@RequestParam("accessoryInStoreJson")String accessoryInStoreJson,
                                                                @RequestParam("checkNumber")String checkNumber){
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        List<AccessoryInStore> accessoryInStoreList = gson.fromJson(accessoryInStoreJson,new TypeToken<List<AccessoryInStore>>(){}.getType());
        List<AccessoryStorage> accessoryStorageList = gson.fromJson(accessoryInStoreJson,new TypeToken<List<AccessoryStorage>>(){}.getType());
        List<ManufactureAccessory> manufactureAccessoryList = manufactureAccessoryService.getManufactureAccessoryByCheckNumberState(checkNumber, null);
        List<Integer> accessoryIdList = new ArrayList<>();
        for (ManufactureAccessory manufactureAccessory : manufactureAccessoryList){
            accessoryIdList.add(manufactureAccessory.getAccessoryID());
            if (!manufactureAccessory.getCheckState().equals("通过")){
                map.put("result", 3);
                map.put("msg", manufactureAccessory.getAccessoryName() + "审核未通过,请先审核~~");
                return map;
            }
        }
        // 判断入账数量
        List<AccessoryInStore> accessoryInStoreList1 = accessoryInStoreService.getAccessoryInStoreByIdList(accessoryIdList);
        for (AccessoryInStore accessoryInStore : accessoryInStoreList){
            float accessoryCount = 0f;
            float accountCount = 0f;
            float inStoreCount = accessoryInStore.getInStoreCount();
            for (ManufactureAccessory manufactureAccessory : manufactureAccessoryList){
                if (manufactureAccessory.getAccessoryID().equals(accessoryInStore.getAccessoryID())){
                    accessoryCount = manufactureAccessory.getAccessoryCount();
                }
            }
            for (AccessoryInStore accessoryInStore1 : accessoryInStoreList1){
                if (accessoryInStore1.getAccessoryID().equals(accessoryInStore.getAccessoryID())){
                    accountCount += accessoryInStore1.getAccountCount();
                }
            }
            if (accountCount + inStoreCount <= accessoryCount){
                accessoryInStore.setAccountCount(inStoreCount);
            } else {
                accessoryInStore.setAccountCount((accessoryCount - accountCount) <= 0 ? 0 : (accessoryCount - accountCount));
            }
        }
        int res1 = accessoryInStoreService.addAccessoryInStoreBatch(accessoryInStoreList);
        List<AccessoryStorage> accessoryStorageList1 = accessoryStorageService.getAccessoryStorageByAccessoryIdList(accessoryIdList);
        List<AccessoryStorage> accessoryStorageList2 = new ArrayList<>();
        for (AccessoryStorage accessoryStorage : accessoryStorageList){
            boolean flag = true;
            for (AccessoryStorage accessoryStorage1 : accessoryStorageList1){
                if (accessoryStorage.getAccessoryID().equals(accessoryStorage1.getAccessoryID()) && accessoryStorage.getAccessoryLocation().equals(accessoryStorage1.getAccessoryLocation())){
                    accessoryStorage1.setStorageCount(accessoryStorage.getStorageCount() + accessoryStorage1.getStorageCount());
                    flag = false;
                }
            }
            if (flag){
                accessoryStorageList2.add(accessoryStorage);
            }
        }
        int res2 = accessoryStorageService.addAccessoryStorageBatch(accessoryStorageList2);
        int res3 = accessoryStorageService.updateAccessoryStorageBatch(accessoryStorageList1);
        if (res1 == 0 && res2 == 0 && res3 == 0){
            map.put("result", 0);
        }else {
            map.put("result", 1);
        }
        return map;
    }

    @RequestMapping(value = "/getaccessoryinstorerecordbyinfo",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getAccessoryInStoreRecord(@RequestParam("orderName")String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<AccessoryInStore> accessoryInStoreList = accessoryInStoreService.getAccessoryInStoreByInfo(orderName, null, null, null, null, null, null);
        map.put("data", accessoryInStoreList);
        map.put("count",accessoryInStoreList.size());
        map.put("code",0);
        return map;
    }

    @RequestMapping(value = "/getbjinstorerecordbyinfo",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getBjInStoreRecord(@RequestParam("orderName")String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<AccessoryInStore> accessoryInStoreList = accessoryInStoreService.getBjInStoreByOrder(orderName);
        map.put("data", accessoryInStoreList);
        map.put("count",accessoryInStoreList.size());
        map.put("code",0);
        return map;
    }

    @RequestMapping(value = "/getaccessorystoragebyinfo",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getAccessoryStorageRecord(@RequestParam("orderName")String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<AccessoryStorage> accessoryStorageList = accessoryStorageService.getAccessoryStorageByInfo(orderName, null, null, null, null, null, null);
        map.put("data", accessoryStorageList);
        map.put("count",accessoryStorageList.size());
        map.put("code",0);
        return map;
    }

    @RequestMapping(value = "/getbjstoragebyinfo",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getBjStorageRecord(@RequestParam("orderName")String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<AccessoryStorage> accessoryStorageList = accessoryStorageService.getBjStorageByInfo(orderName);
        map.put("data", accessoryStorageList);
        map.put("count",accessoryStorageList.size());
        map.put("code",0);
        return map;
    }

    @RequestMapping(value = "/getaccessoryoutrecordbyinfo",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getAccessoryOutRecordByInfo(@RequestParam("orderName")String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<AccessoryOutRecord> accessoryOutRecordList = accessoryOutRecordService.getAccessoryOutRecordByInfo(orderName, null, null, null, null, null, null);
        map.put("data", accessoryOutRecordList);
        map.put("count",accessoryOutRecordList.size());
        map.put("code",0);
        return map;
    }

    @RequestMapping(value = "/getbjoutrecordbyinfo",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getBjOutRecordByInfo(@RequestParam("orderName")String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<AccessoryOutRecord> accessoryOutRecordList = accessoryOutRecordService.getBjOutRecordByInfo(orderName);
        map.put("data", accessoryOutRecordList);
        map.put("count",accessoryOutRecordList.size());
        map.put("code",0);
        return map;
    }


    @RequestMapping(value = "/accessoryoutstore",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> addAccessoryOutStore(@RequestParam("accessoryOutRecordJson") String accessoryOutRecordJson){
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        AccessoryOutRecord accessoryOutRecord = gson.fromJson(accessoryOutRecordJson,new TypeToken<AccessoryOutRecord>(){}.getType());
        Integer storageID = accessoryOutRecord.getId();
        AccessoryStorage accessoryStorage = accessoryStorageService.getAccessoryById(storageID);
        accessoryOutRecord.setId(null);
        float storageCount = accessoryStorage.getStorageCount();
        float outStoreCount = accessoryOutRecord.getOutStoreCount();
        accessoryStorage.setStorageCount(storageCount - outStoreCount);
        if (storageCount == outStoreCount){
            int res1 = accessoryStorageService.deleteAccessoryByID(storageID);
            int res2 = accessoryOutRecordService.addAccessoryOutRecord(accessoryOutRecord);
            if (res1 == 0 && res2 == 0){
                map.put("result", 0);
            }else{
                map.put("result", 1);
            }
        }else{
            int res3 = accessoryStorageService.updateAccessoryStorage(accessoryStorage);
            int res4 = accessoryOutRecordService.addAccessoryOutRecord(accessoryOutRecord);
            if (res3 == 0 && res4 == 0){
                map.put("result", 0);
            }else{
                map.put("result", 1);
            }
        }
        return map;
    }

    @RequestMapping(value = "/getallaccessorystorage", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> getAllAccessoryStorage(){
        Map<String, Object> map = new LinkedHashMap<>();
        List<AccessoryStorage> accessoryStorageList = accessoryStorageService.getAllAccessoryStorage();
        map.put("data", accessoryStorageList);
        map.put("code", 0);
        map.put("count", accessoryStorageList.size());
        return map;
    }

    @RequestMapping(value = "/searchaccessoryleakbyorder", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getAccessoryLeakByOrder(@RequestParam("orderName") String orderName){
        Map<String, Object> map = new LinkedHashMap<>();
        List<ManufactureAccessory> manufactureAccessoryList = manufactureAccessoryService.getManufactureAccessoryByOrder(orderName, null);
        List<AccessoryStorage> accessoryStorageList = accessoryStorageService.getAccessoryStorageByOrder(orderName);
        List<AccessoryOutRecord> accessoryOutRecordList = accessoryOutRecordService.getAccessoryOutRecordByOrder(orderName);
        List<AccessoryInStore> accessoryInStoreList = accessoryInStoreService.getAccessoryInStoreByOrder(orderName);
        for(ManufactureAccessory manufactureAccessory : manufactureAccessoryList){
            Float accessoryReturnCount = 0f;
            int wellCount = 0;
            int orderCount = 0;
            if (manufactureAccessory.getColorName().equals("通用") && manufactureAccessory.getSizeName().equals("通用")){
                wellCount = tailorService.getWellCountByColorSizeList(manufactureAccessory.getOrderName(), null, null);
                orderCount = orderClothesService.getOrderTotalCount(manufactureAccessory.getOrderName());
                manufactureAccessory.setWellCount(wellCount);
                manufactureAccessory.setOrderCount(orderCount);
                manufactureAccessory.setAccessoryPlanCount(orderCount * ((manufactureAccessory.getPieceUsage() == null) ? 0 : manufactureAccessory.getPieceUsage()));
            } else if (manufactureAccessory.getColorName().equals("通用")){
                List<String> sizeList = Arrays.asList(manufactureAccessory.getSizeName().split(","));
                wellCount = tailorService.getWellCountByColorSizeList(manufactureAccessory.getOrderName(), null, sizeList);
                orderCount = orderClothesService.getOrderCountByColorSizeList(manufactureAccessory.getOrderName(), null, sizeList);
                manufactureAccessory.setWellCount(wellCount);
                manufactureAccessory.setOrderCount(orderCount);
                manufactureAccessory.setAccessoryPlanCount(orderCount * ((manufactureAccessory.getPieceUsage() == null) ? 0 : manufactureAccessory.getPieceUsage()));
            } else if (manufactureAccessory.getSizeName().equals("通用")){
                List<String> colorList = Arrays.asList(manufactureAccessory.getColorName().split(","));
                wellCount = tailorService.getWellCountByColorSizeList(manufactureAccessory.getOrderName(), colorList, null);
                orderCount = orderClothesService.getOrderCountByColorSizeList(manufactureAccessory.getOrderName(), colorList, null);
                manufactureAccessory.setWellCount(wellCount);
                manufactureAccessory.setOrderCount(orderCount);
                manufactureAccessory.setAccessoryPlanCount(orderCount * ((manufactureAccessory.getPieceUsage() == null) ? 0 : manufactureAccessory.getPieceUsage()));
            } else {
                List<String> colorList = Arrays.asList(manufactureAccessory.getColorName().split(","));
                List<String> sizeList = Arrays.asList(manufactureAccessory.getSizeName().split(","));
                wellCount = tailorService.getWellCountByColorSizeList(manufactureAccessory.getOrderName(), colorList, sizeList);
                orderCount = orderClothesService.getOrderCountByColorSizeList(manufactureAccessory.getOrderName(), colorList, sizeList);
                manufactureAccessory.setWellCount(wellCount);
                manufactureAccessory.setOrderCount(orderCount);
                manufactureAccessory.setAccessoryPlanCount(orderCount * ((manufactureAccessory.getPieceUsage() == null) ? 0 : manufactureAccessory.getPieceUsage()));
            }
            for (AccessoryInStore accessoryInStore : accessoryInStoreList){
                if (accessoryInStore.getAccessoryID().equals(manufactureAccessory.getAccessoryID())){
                    accessoryReturnCount += accessoryInStore.getInStoreCount();
                }
            }
            manufactureAccessory.setAccessoryReturnCount(accessoryReturnCount);
            for (AccessoryInStore accessoryInStore : accessoryInStoreList){
                if (accessoryInStore.getColorName().equals(manufactureAccessory.getColorName()) && accessoryInStore.getSizeName().equals(manufactureAccessory.getSizeName())){
                    accessoryInStore.setOrderCount(manufactureAccessory.getOrderCount());
                    accessoryInStore.setWellCount(wellCount);
                    accessoryInStore.setOrderCount(orderCount);
                }
            }
            for (AccessoryStorage accessoryStorage : accessoryStorageList){
                if (accessoryStorage.getColorName().equals(manufactureAccessory.getColorName()) && accessoryStorage.getSizeName().equals(manufactureAccessory.getSizeName())){
                    accessoryStorage.setOrderCount(manufactureAccessory.getOrderCount());
                    accessoryStorage.setWellCount(wellCount);
                    accessoryStorage.setOrderCount(orderCount);
                }
            }
            for (AccessoryOutRecord accessoryOutRecord : accessoryOutRecordList){
                if (accessoryOutRecord.getColorName().equals(manufactureAccessory.getColorName()) && accessoryOutRecord.getSizeName().equals(manufactureAccessory.getSizeName())){
                    accessoryOutRecord.setOrderCount(manufactureAccessory.getOrderCount());
                    accessoryOutRecord.setWellCount(wellCount);
                    accessoryOutRecord.setOrderCount(orderCount);
                }
            }
        }
        map.put("plan", manufactureAccessoryList);
        map.put("inStore", accessoryInStoreList);
        map.put("storage", accessoryStorageList);
        map.put("out", accessoryOutRecordList);
        return map;
    }


    @RequestMapping(value = "/searchaccessoryleaksummarybyorder", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getAccessoryLeakSummaryByOrder(@RequestParam("orderName") String orderName) throws Exception{
        Map<String, Object> map = new LinkedHashMap<>();
        List<ManufactureAccessory> manufactureAccessoryList = manufactureAccessoryService.getManufactureAccessoryByOrder(orderName, null);
        List<AccessoryInStore> accessoryInStoreList = accessoryInStoreService.getAccessoryInStoreByOrder(orderName);
        List<ManufactureAccessory> destAccessoryList = new ArrayList<>();
        for(ManufactureAccessory manufactureAccessory : manufactureAccessoryList){
            Float accessoryReturnCount = 0f;
            int wellCount = 0;
            int orderCount = 0;
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");//注意月份是MM
            Date lastDate = simpleDateFormat.parse("1970-01-01");
            Float lastCount = 0f;
            for (AccessoryInStore accessoryInStore : accessoryInStoreList){
                String thisAccessoryName = accessoryInStore.getAccessoryName();
                String manufactureAccessoryName = manufactureAccessory.getAccessoryName();
                if (manufactureAccessoryName.equals(thisAccessoryName) && manufactureAccessory.getSpecification().equals(accessoryInStore.getSpecification()) && manufactureAccessory.getAccessoryColor().equals(accessoryInStore.getAccessoryColor()) && manufactureAccessory.getColorName().equals(accessoryInStore.getColorName()) && manufactureAccessory.getSizeName().equals(accessoryInStore.getSizeName())){
                    accessoryReturnCount += accessoryInStore.getInStoreCount();
                    Date inStoreDate = simpleDateFormat.parse(accessoryInStore.getInStoreTime());
                    if (inStoreDate.after(lastDate)){
                        lastDate = inStoreDate;
                        lastCount = accessoryInStore.getInStoreCount();
                    }
                }
            }
            manufactureAccessory.setAccessoryReturnCount(accessoryReturnCount);
            manufactureAccessory.setLastDate(lastDate);
            manufactureAccessory.setLastCount(lastCount);
            if (manufactureAccessory.getAccessoryReturnCount() < manufactureAccessory.getAccessoryCount()){
                if (manufactureAccessory.getColorName().equals("通用") && manufactureAccessory.getSizeName().equals("通用")){
                    wellCount = tailorService.getWellCountByColorSizeList(manufactureAccessory.getOrderName(), null, null);
                    orderCount = orderClothesService.getOrderTotalCount(manufactureAccessory.getOrderName());
                    manufactureAccessory.setWellCount(wellCount);
                    manufactureAccessory.setOrderCount(orderCount);
                    manufactureAccessory.setAccessoryPlanCount(orderCount * ((manufactureAccessory.getPieceUsage() == null) ? 0 : manufactureAccessory.getPieceUsage()));
                } else if (manufactureAccessory.getColorName().equals("通用")){
                    List<String> sizeList = Arrays.asList(manufactureAccessory.getSizeName().split(","));
                    wellCount = tailorService.getWellCountByColorSizeList(manufactureAccessory.getOrderName(), null, sizeList);
                    orderCount = orderClothesService.getOrderCountByColorSizeList(manufactureAccessory.getOrderName(), null, sizeList);
                    manufactureAccessory.setWellCount(wellCount);
                    manufactureAccessory.setOrderCount(orderCount);
                    manufactureAccessory.setAccessoryPlanCount(orderCount * ((manufactureAccessory.getPieceUsage() == null) ? 0 : manufactureAccessory.getPieceUsage()));
                } else if (manufactureAccessory.getSizeName().equals("通用")){
                    List<String> colorList = Arrays.asList(manufactureAccessory.getColorName().split(","));
                    wellCount = tailorService.getWellCountByColorSizeList(manufactureAccessory.getOrderName(), colorList, null);
                    orderCount = orderClothesService.getOrderCountByColorSizeList(manufactureAccessory.getOrderName(), colorList, null);
                    manufactureAccessory.setWellCount(wellCount);
                    manufactureAccessory.setOrderCount(orderCount);
                    manufactureAccessory.setAccessoryPlanCount(orderCount * ((manufactureAccessory.getPieceUsage() == null) ? 0 : manufactureAccessory.getPieceUsage()));
                } else {
                    List<String> colorList = Arrays.asList(manufactureAccessory.getColorName().split(","));
                    List<String> sizeList = Arrays.asList(manufactureAccessory.getSizeName().split(","));
                    wellCount = tailorService.getWellCountByColorSizeList(manufactureAccessory.getOrderName(), colorList, sizeList);
                    orderCount = orderClothesService.getOrderCountByColorSizeList(manufactureAccessory.getOrderName(), colorList, sizeList);
                    manufactureAccessory.setWellCount(wellCount);
                    manufactureAccessory.setOrderCount(orderCount);
                    manufactureAccessory.setAccessoryPlanCount(orderCount * ((manufactureAccessory.getPieceUsage() == null) ? 0 : manufactureAccessory.getPieceUsage()));
                }
                destAccessoryList.add(manufactureAccessory);
            }
        }
        map.put("plan", destAccessoryList);
        return map;
    }


    @RequestMapping(value = "/accessoryoutstorebatch",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> addAccessoryOutStoreBatch(@RequestParam("accessoryOutStoreJson")String accessoryOutStoreJson){
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        List<AccessoryStorage> accessoryStorageList1 = gson.fromJson(accessoryOutStoreJson,new TypeToken<List<AccessoryStorage>>(){}.getType());
        List<AccessoryOutRecord> accessoryOutRecordList = gson.fromJson(accessoryOutStoreJson,new TypeToken<List<AccessoryOutRecord>>(){}.getType());
        int res1 = accessoryOutRecordService.addAccessoryOutRecordBatch(accessoryOutRecordList);
        List<AccessoryStorage> accessoryStorageList2 = new ArrayList<>();
        List<Integer> storageIDList = new ArrayList<>();
        for (AccessoryStorage accessoryStorage : accessoryStorageList1){
            for (AccessoryOutRecord accessoryOutRecord : accessoryOutRecordList){
                if (accessoryStorage.getId().equals(accessoryOutRecord.getId())){
                    if (accessoryOutRecord.getOutStoreCount().equals(accessoryStorage.getStorageCount())){
                        storageIDList.add(accessoryStorage.getId());
                    }else {
                        accessoryStorage.setStorageCount(accessoryStorage.getStorageCount() - accessoryOutRecord.getOutStoreCount());
                        accessoryStorageList2.add(accessoryStorage);
                    }
                }
            }
        }
        int res2 = accessoryStorageService.updateAccessoryStorageBatch(accessoryStorageList2);
        int res3 = accessoryStorageService.deleteAccessoryStorageBatch(storageIDList);
        if (res1 == 0 && res2 == 0 && res3 == 0){
            map.put("result", 0);
        }else {
            map.put("result", 1);
        }
        return map;
    }

    @RequestMapping("/placeOrderStart")
    public String placeOrderStart(){
        return "fabric/placeOrder";
    }

    @RequestMapping("/orderCheckStart")
    public String orderCheckStart(){
        return "fabric/orderCheck";
    }

    @RequestMapping(value = "/deleteaccessoryinstoredata",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> deleteAccessoryInStoreById(@RequestParam("id") Integer id,
                                                          @RequestParam("orderName") String orderName,
                                                          @RequestParam("accessoryName") String accessoryName,
                                                          @RequestParam("colorName") String colorName,
                                                          @RequestParam("sizeName") String sizeName,
                                                          @RequestParam("inStoreCount") Float inStoreCount,
                                                          @RequestParam("accessoryLocation") String accessoryLocation){
        Map<String, Object> map = new LinkedHashMap<>();
        List<AccessoryStorage> accessoryStorageList = accessoryStorageService.getAccessoryStorageByInfo(orderName, accessoryName, null, null, colorName, sizeName, null);
        for (AccessoryStorage accessoryStorage : accessoryStorageList){
            if (accessoryStorage.getAccessoryLocation().equals(accessoryLocation)){
                if (accessoryStorage.getStorageCount() < inStoreCount){
                    map.put("result", 3);
                    return map;
                } else if (accessoryStorage.getStorageCount() - inStoreCount < 1){
                    accessoryStorageService.deleteAccessoryByID(accessoryStorage.getId());
                } else {
                    accessoryStorage.setStorageCount(accessoryStorage.getStorageCount() - inStoreCount);
                    accessoryStorageService.updateAccessoryStorage(accessoryStorage);
                }
            }
        }
        int res1 = accessoryInStoreService.deleteAccessoryInStoreByID(id);
        if (res1 == 0){
            map.put("result", 0);
        } else {
            map.put("result", 1);
        }
        return map;
    }

    @RequestMapping(value = "/fabricaccessoryprintinit",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> fabricAccessoryPrintInit(@RequestParam("printType")String printType){
        Map<String, Object> map = new LinkedHashMap<>();
        if (printType.equals("面料入库")){
            List<FabricDetail> fabricDetailList = fabricDetailService.getTodayFabricDetail();
            map.put("fabricIn", fabricDetailList);
        } else if (printType.equals("面料出库")){
            List<FabricOutRecord> fabricOutRecordList = fabricOutRecordService.getTodayFabricOutRecord();
            map.put("fabricOut", fabricOutRecordList);
        } else if (printType.equals("辅料入库")){
            List<AccessoryInStore> accessoryInStoreList = accessoryInStoreService.getTodayAccessoryInStore();
            map.put("accessoryIn", accessoryInStoreList);
        } else if (printType.equals("辅料出库")){
            List<AccessoryOutRecord> accessoryOutRecordList = accessoryOutRecordService.getTodayAccessoryOutRecord();
            map.put("accessoryOut", accessoryOutRecordList);
        }
        return map;
    }


    @RequestMapping(value = "/fabricaccessoryprintbytime",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> fabricAccessoryPrintByTime(@RequestParam("from")String from,
                                                          @RequestParam("to")String to,
                                                          @RequestParam("searchType")String searchType){
        Map<String, Object> map = new LinkedHashMap<>();
        if (searchType.equals("面料入库")){
            List<FabricDetail> fabricDetailList = fabricDetailService.getFabricDetailByTime(from, to);
            map.put("fabricIn", fabricDetailList);
        } else if (searchType.equals("面料出库")){
            List<FabricOutRecord> fabricOutRecordList = fabricOutRecordService.getFabricOutRecordByTime(from, to);
            map.put("fabricOut", fabricOutRecordList);
        } else if (searchType.equals("辅料入库")){
            List<AccessoryInStore> accessoryInStoreList = accessoryInStoreService.getAccessoryInStoreByTime(from, to);
            map.put("accessoryIn", accessoryInStoreList);
        } else if (searchType.equals("辅料出库")){
            List<AccessoryOutRecord> accessoryOutRecordList = accessoryOutRecordService.getAccessoryOutRecordByTime(from, to);
            List<String> orderNameList = new ArrayList<>();
            for (AccessoryOutRecord accessoryOutRecord : accessoryOutRecordList){
                if (!orderNameList.contains(accessoryOutRecord.getOrderName())){
                    orderNameList.add(accessoryOutRecord.getOrderName());
                }
            }
            for (AccessoryOutRecord accessoryOutRecord : accessoryOutRecordList){
                String orderName = accessoryOutRecord.getOrderName();
                if (accessoryOutRecord.getColorName().equals("通用") && accessoryOutRecord.getSizeName().equals("通用")){
                    int orderCount = orderClothesService.getOrderTotalCount(orderName);
                    accessoryOutRecord.setWellCount(orderCount);
                } else if (accessoryOutRecord.getColorName().equals("通用")){
                    List<String> sizeList = Arrays.asList(accessoryOutRecord.getSizeName().split(","));
                    int orderCount = orderClothesService.getOrderCountByColorSizeList(orderName, null, sizeList);
                    accessoryOutRecord.setWellCount(orderCount);
                } else if (accessoryOutRecord.getSizeName().equals("通用")){
                    List<String> colorList = Arrays.asList(accessoryOutRecord.getColorName().split(","));
                    int orderCount = orderClothesService.getOrderCountByColorSizeList(orderName, colorList, null);
                    accessoryOutRecord.setWellCount(orderCount);
                } else {
                    List<String> colorList = Arrays.asList(accessoryOutRecord.getColorName().split(","));
                    List<String> sizeList = Arrays.asList(accessoryOutRecord.getSizeName().split(","));
                    int orderCount = orderClothesService.getOrderCountByColorSizeList(orderName, colorList, sizeList);
                    accessoryOutRecord.setWellCount(orderCount);
                }
            }
            List<AccessoryOutRecord> accessoryOutRecordList1 = accessoryOutRecordService.getAccessoryOutRecordByOrderNameList(orderNameList);
            for (AccessoryOutRecord accessoryOutRecord : accessoryOutRecordList){
                for (AccessoryOutRecord accessoryOutRecord1 : accessoryOutRecordList1){
                    if (accessoryOutRecord.getAccessoryID().equals(accessoryOutRecord1.getAccessoryID())){
                        accessoryOutRecord.setFinishCount(accessoryOutRecord.getFinishCount() + accessoryOutRecord1.getOutStoreCount());
                    }
                }
//                accessoryOutRecord.setFinishCount(accessoryOutRecord.getOutStoreCount() + accessoryOutRecord.getFinishCount());
                accessoryOutRecord.setNeedCount(accessoryOutRecord.getWellCount() * accessoryOutRecord.getPieceUsage());
                accessoryOutRecord.setDiffCount(accessoryOutRecord.getNeedCount() - accessoryOutRecord.getFinishCount());
            }
            map.put("accessoryOut", accessoryOutRecordList);
        }
        return map;
    }



    @RequestMapping(value = "/deleteaccessorystoragebyid",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> deleteAccessoryInStoreById(@RequestParam("id") Integer id){
        Map<String, Object> map = new LinkedHashMap<>();
        int res = accessoryStorageService.deleteAccessoryByID(id);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/getmaterialprogressbychecknumber", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getReviewDataByTypeCheckNumber(@RequestParam("checkNumber")String checkNumber,
                                                              @RequestParam("reviewType")String reviewType){
        Map<String, Object> map = new LinkedHashMap<>();
        if (reviewType.equals("面料")){
            List<ManufactureFabric> manufactureFabricList = manufactureFabricService.getManufactureFabricByCheckNumberState(checkNumber, null);
            for (ManufactureFabric manufactureFabric : manufactureFabricList){
                List<FabricDetail> fabricDetailList = fabricDetailService.getFabricDetailByInfo(null, null, null, null, manufactureFabricList.get(0).getFabricID());
                for (FabricDetail fabricDetail : fabricDetailList){
                    manufactureFabric.setFabricReturn(manufactureFabric.getFabricReturn() + fabricDetail.getWeight());
                }
            }
            map.put("data", manufactureFabricList);
            map.put("code", 0);
            map.put("count", manufactureFabricList.size());
        } else {
            List<ManufactureAccessory> manufactureAccessoryList = manufactureAccessoryService.getManufactureAccessoryByCheckNumberState(checkNumber, null);
            for (ManufactureAccessory manufactureAccessory : manufactureAccessoryList){
                List<AccessoryInStore> accessoryInStoreList = accessoryInStoreService.getAccessoryInStoreByInfo(manufactureAccessory.getOrderName(), manufactureAccessory.getAccessoryName(), manufactureAccessory.getSpecification(), manufactureAccessory.getAccessoryColor(), manufactureAccessory.getColorName(), manufactureAccessory.getSizeName(), null);
                for (AccessoryInStore accessoryInStore: accessoryInStoreList){
                    manufactureAccessory.setReturnCount(manufactureAccessory.getReturnCount() + accessoryInStore.getInStoreCount());
                }
            }
            map.put("data", manufactureAccessoryList);
            map.put("code", 0);
            map.put("count", manufactureAccessoryList.size());
        }
        return map;
    }


    @RequestMapping(value = "/getaccessoryinstorebychecknumber", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getAccessoryInStoreByCheckNumber(@RequestParam("checkNumber")String checkNumber){
        Map<String, Object> map = new LinkedHashMap<>();
        List<ManufactureAccessory> manufactureAccessoryList = manufactureAccessoryService.getManufactureAccessoryByCheckNumberState(checkNumber, null);
        List<Integer> accessoryIDList = new ArrayList<>();
        for (ManufactureAccessory manufactureAccessory : manufactureAccessoryList){
            accessoryIDList.add(manufactureAccessory.getAccessoryID());
        }
        List<AccessoryInStore> accessoryInStoreList = accessoryInStoreService.getAccessoryInStoreByIdList(accessoryIDList);
        map.put("data", accessoryInStoreList);
        map.put("count",accessoryInStoreList.size());
        map.put("code",0);
        return map;
    }

    @RequestMapping(value = "/changeaccessoryinstoredatatoanother", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> changeAccessoryInStoreDataToAnother(@RequestParam("id")Integer id,
                                                                   @RequestParam("accessoryID")Integer accessoryID){
        Map<String, Object> map = new LinkedHashMap<>();
        AccessoryInStore accessoryInStore = accessoryInStoreService.getAccessoryInStoreById(id);
        ManufactureAccessory manufactureAccessory = manufactureAccessoryService.getManufactureAccessoryByID(accessoryID);
        if (!manufactureAccessory.getCheckState().equals("通过")){
            map.put("result", 3);
            map.put("msg", "审核未通过,请先审核~~");
            return map;
        }
        if (accessoryInStore.getCheckNumber() != null && !accessoryInStore.getCheckNumber().equals("")){
            map.put("result", 4);
            map.put("msg", "财务已对账,不能转了~~");
            return map;
        }
        float accountCount = 0f;
        float inStoreCount = accessoryInStore.getInStoreCount();
        float accessoryCount = manufactureAccessory.getAccessoryCount();
        List<AccessoryInStore> accessoryInStoreList = accessoryInStoreService.getAccessoryInStoreByInfo(null, null, null, null, null, null, accessoryID);
        for (AccessoryInStore accessoryInStore1 : accessoryInStoreList){
            accountCount += accessoryInStore1.getAccountCount();
        }
        if (accountCount + inStoreCount <= accessoryCount){
            accessoryInStore.setAccountCount(inStoreCount);
        } else {
            accessoryInStore.setAccountCount((accessoryCount - accountCount) <= 0 ? 0 : (accessoryCount - accountCount));
        }
        accessoryInStore.setAccessoryID(accessoryID);
        int res = accessoryInStoreService.updateAccessoryInStoreInChange(accessoryInStore);
        map.put("result", res);
        return map;
    }


}
