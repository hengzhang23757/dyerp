package com.example.erp01.action;

import com.alibaba.dubbo.config.annotation.Reference;
import com.example.erp01.model.*;
import com.example.erp01.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
@Controller
@RequestMapping(value = "erp")
public class PrenatalProgressController {

    @Reference(check=false,url = "dubbo://115.28.184.180:20883",version = "2.0.0")
    private RpcService xhlRpcServiceRef;
    @Autowired
    private TailorService tailorService;
    @Autowired
    private OrderClothesService orderClothesService;
    @Autowired
    private EmbInStoreService embInStoreService;
    @Autowired
    private EmbStorageService embStorageService;
    @Autowired
    private EmbOutStoreService embOutStoreService;
    @Autowired
    private PieceWorkService pieceWorkService;
    @Autowired
    private PrenatalProgressService prenatalProgressService;

    @RequestMapping("/prenatalProgressSummaryStart")
    public String prenatalProgressSummaryStart(){
        return "plan/prenatalProgress";
    }

    @RequestMapping(value = "/getprenatalprogresssummary", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getPrenatalProgressSummary(@RequestParam(value = "from", required = false)String from,
                                                          @RequestParam(value = "to", required = false)String to,
                                                          @RequestParam(value = "orderName", required = false)String orderName,
                                                          @RequestParam(value = "clothesVersionNumber", required = false)String clothesVersionNumber) {
        Map<String, Object> map = new LinkedHashMap<>();
        List<OrderClothes> orderClothesList = new ArrayList<>();
        if (orderName != null){
            OrderClothes orderClothes = new OrderClothes();
            orderClothes.setOrderName(orderName);
            orderClothes.setClothesVersionNumber(clothesVersionNumber);
            orderClothesList.add(orderClothes);
        } else {
            orderClothesList = prenatalProgressService.getWorkOrderByTime(from, to);
        }
        List<PrenatalProgress> prenatalProgressList = new ArrayList<>();
        for (OrderClothes orderClothes : orderClothesList){
            String tmpName = orderClothes.getOrderName();
            String tmpVersion = orderClothes.getClothesVersionNumber();
            List<OrderClothes> orderClothesList1 = orderClothesService.getOrderSummaryByColor(tmpName);
            List<Tailor> tailorList = tailorService.getTailorInfoGroupByColorPartType(tmpName, null, 0);
            List<XhlTailorReport> xhlTailorReportList = xhlRpcServiceRef.getInStoreSummary("德悦", tmpName);
            List<OutBoundReport> outBoundReportList = xhlRpcServiceRef.getOutBoundSummary("德悦", tmpName, null, null);
            List<GeneralSalary> generalSalaryList = pieceWorkService.getPieceInfoGroupByOrderColor(tmpName, 2005);
            List<GeneralSalary> generalSalaryList1 = pieceWorkService.getPieceInfoGroupByOrderColor(tmpName, 2025);
            List<EmbStorage> embInStoreList = embInStoreService.getEmbInStoreGroupByColor(tmpName);
            List<EmbStorage> embStorageList = embStorageService.getEmbStorageGroupByColor(tmpName);
            List<EmbStorage> embOutStoreList = embOutStoreService.getEmbOutStoreGroupByColor(tmpName);
            List<PrenatalProgress> looseInfoList = prenatalProgressService.getPreCutCountByOrder(tmpName);
            if (tailorList != null && !tailorList.isEmpty()){
                for (Tailor tailor : tailorList){
                    PrenatalProgress prenatalProgress = new PrenatalProgress();
                    prenatalProgress.setOrderName(tmpName);
                    prenatalProgress.setColorName(tailor.getColorName());
                    prenatalProgress.setPartName(tailor.getPartName());
                    prenatalProgress.setClothesVersionNumber(tmpVersion);
                    prenatalProgress.setWellCount(tailor.getTailorReportCount());
                    for (OrderClothes orderClothes1 : orderClothesList1){
                        if (tailor.getColorName().equals(orderClothes1.getColorName())){
                            prenatalProgress.setOrderCount(orderClothes1.getOrderSum());
                            prenatalProgress.setCutDiff(prenatalProgress.getWellCount() - prenatalProgress.getOrderCount());
                        }
                    }
                    for (XhlTailorReport xhlTailorReport : xhlTailorReportList){
                        if (xhlTailorReport.getColorName().equals(tailor.getColorName()) && xhlTailorReport.getPrintingPart().equals(tailor.getPartName())){
                            prenatalProgress.setOpaCount(prenatalProgress.getOpaCount() + xhlTailorReport.getTotalLayer());
                        }
                    }
                    for (OutBoundReport outBoundReport : outBoundReportList){
                        if (outBoundReport.getColorName().equals(tailor.getColorName()) && outBoundReport.getPrintingPart().equals(tailor.getPartName())){
                            prenatalProgress.setOpaBackCount(prenatalProgress.getOpaBackCount() + outBoundReport.getLayerCount());
                        }
                    }
                    prenatalProgress.setOpaDiff(prenatalProgress.getOpaBackCount() - prenatalProgress.getOpaCount());
                    for (EmbStorage embInStore : embInStoreList){
                        if (embInStore.getColorName().equals(tailor.getColorName())){
                            prenatalProgress.setEmbInStore(embInStore.getLayerCount());
                        }
                    }
                    for (GeneralSalary generalSalary : generalSalaryList){
                        if (generalSalary.getColorName().equals(tailor.getColorName())){
                            prenatalProgress.setMatchCount(Math.round(generalSalary.getPieceCount()));
                        }
                    }
                    for (GeneralSalary generalSalary1 : generalSalaryList1){
                        if (generalSalary1.getColorName().equals(tailor.getColorName())){
                            prenatalProgress.setHangCount(Math.round(generalSalary1.getPieceCount()));
                        }
                    }
                    for (EmbStorage embStorage : embStorageList){
                        if (embStorage.getColorName().equals(tailor.getColorName())){
                            prenatalProgress.setEmbDiff(embStorage.getLayerCount());
                        }
                    }
                    for (EmbStorage embStorage : embOutStoreList){
                        if (embStorage.getColorName().equals(tailor.getColorName())){
                            prenatalProgress.setUnloadCount(embStorage.getLayerCount());
                        }
                    }
                    for (PrenatalProgress prenatalProgress1 : looseInfoList){
                        if (prenatalProgress1.getColorName().equals(tailor.getColorName())){
                            prenatalProgress.setLooseCount(prenatalProgress1.getLooseCount());
                        }
                    }
                    prenatalProgressList.add(prenatalProgress);
                }
            }
        }
        map.put("prenatalProgressList", prenatalProgressList);
        return map;
    }

























}
