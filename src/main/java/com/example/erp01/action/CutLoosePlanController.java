package com.example.erp01.action;

import com.example.erp01.model.CutLoosePlan;
import com.example.erp01.service.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
@Configuration      //1.主要用于标记配置类，兼备Component的效果。
@EnableScheduling   // 2.开启定时任务
@Controller
@RequestMapping(value = "erp")
public class CutLoosePlanController {

    @Autowired
    private CutLoosePlanService cutLoosePlanService;
    @Autowired
    private FabricDetailService fabricDetailService;
    @Autowired
    private OrderClothesService orderClothesService;
    @Autowired
    private TailorService tailorService;
    @Autowired
    private LooseFabricService looseFabricService;
    @Autowired
    private FabricStorageService fabricStorageService;

    @RequestMapping("/cutLoosePlanStart")
    public String cutLoosePlanStart(){
        return "plan/cutLoosePlan";
    }

    @RequestMapping(value = "/addorupdatecutlooseplan", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> addOrUpdateCutLoosePlan(@RequestParam("cutLoosePlanJson")String cutLoosePlanJson) {
        Map<String, Object> map = new LinkedHashMap<>();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        CutLoosePlan cutLoosePlan = gson.fromJson(cutLoosePlanJson, CutLoosePlan.class);
        int res = cutLoosePlanService.addOrUpdateCutLoosePlan(cutLoosePlan);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/deletecutlooseplan", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> deleteCutLoosePlan(@RequestParam("id")Integer id) {
        Map<String, Object> map = new LinkedHashMap<>();
        int res = cutLoosePlanService.deleteCutLoosePlan(id);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/updatefinishstate", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> updateFinishState(@RequestParam("id")Integer id) {
        Map<String, Object> map = new LinkedHashMap<>();
        int res = cutLoosePlanService.updateFinishState(id);
        map.put("result", res);
        return map;
    }

    @RequestMapping(value = "/getunfinishedcutlooseplan", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getUnfinishedCutLoosePlan() {
        Map<String, Object> map = new LinkedHashMap<>();
        List<CutLoosePlan> cutLoosePlanList = cutLoosePlanService.getCutLoosePlanByFinishState("未完成");
        map.put("data", cutLoosePlanList);
        map.put("count", cutLoosePlanList.size());
        map.put("code",0);
        return map;
    }


    @RequestMapping(value = "/getfinishedcutlooseplan", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getFinishedCutLoosePlan() {
        Map<String, Object> map = new LinkedHashMap<>();
        List<CutLoosePlan> cutLoosePlanList = cutLoosePlanService.getCutLoosePlanByFinishState("完成");
        map.put("data", cutLoosePlanList);
        map.put("count", cutLoosePlanList.size());
        map.put("code",0);
        return map;
    }


//    @Scheduled(cron = "0 30 * * * ? ")
    public void syncCutLoosePlan (){
        List<CutLoosePlan> cutLoosePlanList = cutLoosePlanService.getCutLoosePlanByFinishState("未完成");
        if (cutLoosePlanList != null && !cutLoosePlanList.isEmpty()){
            for (CutLoosePlan cutLoosePlan: cutLoosePlanList){
                List<String> colorNameList = Arrays.asList(cutLoosePlan.getColorName().split(","));
                int inStoreBatch = fabricDetailService.getFabricBatchNumberByInfo(cutLoosePlan.getOrderName(), colorNameList);
                int orderCount = orderClothesService.getOrderCountByColorSizeList(cutLoosePlan.getOrderName(), colorNameList, null);
                int cutCount = tailorService.getWellCountByColorSizeList(cutLoosePlan.getOrderName(), colorNameList, null);
                int preCutBatch = looseFabricService.getLooseCountByOrder(cutLoosePlan.getOrderName(), colorNameList, 0);
                int cutBatch = looseFabricService.getLooseCountByOrder(cutLoosePlan.getOrderName(), colorNameList, 1);
                int storageBatch = fabricStorageService.getFabricBatchNumberByOrder(cutLoosePlan.getOrderName(), colorNameList);
                cutLoosePlan.setInStoreBatch(inStoreBatch);
                cutLoosePlan.setOrderCount(orderCount);
                cutLoosePlan.setCutCount(cutCount);
                cutLoosePlan.setPreCutBatch(preCutBatch);
                cutLoosePlan.setStorageBatch(storageBatch);
            }
            int res = cutLoosePlanService.updateCutLoosePlanBatch(cutLoosePlanList);
            if (res == 0){
                System.out.println("松布裁剪同步完成！");
            } else {
                System.out.println("松布裁剪同步失败！");
            }
        }
    }


}
