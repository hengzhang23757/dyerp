var form;
var userName = $("#userName").val();
var userRole = $("#userRole").val();
layui.use('form', function(){
    form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
    form.render();
});
layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.21'
}).extend({                         // 模块别名
    soulTable: 'soulTable'
});
var reportTable,checkTable;
var supplierDemo;
$(document).ready(function () {
    $Date = layui.laydate;
    layui.laydate.render({
        elem: '#from',
        trigger: 'click',
        value: getFormatDate()
    });
    layui.laydate.render({
        elem: '#to',
        trigger: 'click',
        value: getFormatDate()
    });
    $.ajax({
        url: "/erp/gethistoryseason",
        data: {},
        success:function(data){
            if (data.seasonList){
                $("#seasonName").empty();
                if (data.seasonList) {
                    $("#seasonName").append("<option value=''>请选择季度</option>");
                    $.each(data.seasonList, function(index,element){
                        $("#seasonName").append("<option value="+element+">"+element+"</option>");
                    });
                }
                form.render();
            }
        }, error:function(){
        }
    });
    $.ajax({
        url: "/erp/getallfabricsupplier",
        data: {},
        async:false,
        success:function(data){
            if (data.supplierList){
                var resultData = [];
                $.each(data.supplierList, function(index,element){
                    if (element != null && element != ''){
                        resultData.push({name:element, value:element});
                    }
                });
                supplierDemo = xmSelect.render({
                    el: '#supplier',
                    radio: true,
                    filterable: true,
                    data: resultData
                });
            }
        }, error:function(){
        }
    });
    $.ajax({
        url: "/erp/gethistorycustomer",
        data: {},
        success:function(data){
            if (data.customerList){
                $("#customerName").empty();
                if (data.customerList) {
                    $("#customerName").append("<option value=''>请选择客户</option>");
                    $.each(data.customerList, function(index,element){
                        $("#customerName").append("<option value="+element+">"+element+"</option>");
                    });
                }
                form.render();
            }
        }, error:function(){
        }
    });
    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumber", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });
});
$(document).on('keydown', '.layui-table-edit', function (e) {
    var td = $(this).parent('td')
        ,tr = td.parent('tr')
        ,trs = tr.parent().parent().find('tr')
        ,tr_index = tr.index()
        ,td_index = td.index()
        ,td_last_index = tr.find('[data-edit="text"]:last').index()
        ,td_first_index = tr.find('[data-edit="text"]:first').index();
    switch (e.keyCode) {
        case 13:
            tr.next().find('td').eq(td_index).click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            return false;

        case 39:
            td.nextAll('[data-edit="text"]:first').click();
            if (td_index == td_last_index){
                tr.next().find('td').eq(td_first_index).click();
                if (tr_index == trs.length - 1){
                    trs.eq(0).find('td').eq(td_first_index).click();
                }
            }
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
        case 37:
            td.prevAll('[data-edit="text"]:first').click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
        case 38:
            tr.prev().find('td').eq(td_index).click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
        case 40:
            tr.next().find('td').eq(td_index).click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
    }

});

layui.use(['form', 'soulTable', 'table'], function () {

    var table = layui.table,
        soulTable = layui.soulTable,
        form = layui.form,
        $ = layui.$;

    reportTable = table.render({
        id:'reportTable'
        ,elem: '#reportTable'
        ,cols:[[]]
        ,data:[]
        ,loading:false
        ,height: 'full-130'
        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
        ,page: true
        ,title: '面辅料对账表'
        ,totalRow: true
        ,even: true
        ,limit:500
        ,limits:[500, 1000, 2000]
        ,done: function (res, curr, count) {
            soulTable.render(this);
        }
    });
    var filterOrderName = $("#orderName").val();
    var filterCustomerName = $("#customerName").val();
    var filterSeason = $("#seasonName").val();
    var filterType = $("#type").val();
    var filterFrom = $("#from").val();
    var filterTo = $("#to").val();
    var filterCheckState = $("#checkState").val();
    var showType = $("#showType").val();
    var filterSupplier = supplierDemo.getValue('nameStr');
    initTable(filterFrom, filterTo, '', '', '', '', "面料", '', '流水');
    function initTable(from, to, customerName, season, orderName, supplier, type, checkState, showType){
        var load = layer.load();
        var param = {};
        if (from != null && from != ''){
            param.from = from;
        }
        if (to != null && to != ''){
            param.to = to;
        }
        if (customerName != null && customerName != ''){
            param.customerName = customerName;
        }
        if (season != null && season != ''){
            param.season = season;
        }
        if (orderName != null && orderName != ''){
            param.orderName = orderName;
        }
        if (supplier != null && supplier != ''){
            param.supplier = supplier;
        }
        if (type != null && type != ''){
            param.type = type;
        }
        if (checkState != null && checkState != ''){
            param.checkState = checkState;
        }
        if (type === "面料"){
            if (showType === "流水"){
                $.ajax({
                    url: "/erp/getfabricaccessorymonthcheckrecordbyinfo",
                    type: 'GET',
                    data: param,
                    success: function (res) {
                        if (res.data) {
                            var reportData = res.data;
                            reportTable = table.render({
                                elem: '#reportTable'
                                ,cols:[[
                                    {type:'checkbox', fixed: 'left'}
                                    ,{type: 'numbers', align:'center', title:'序号', minWidth:80}
                                    ,{field: 'clothesVersionNumber', title:'单号', align:'center', minWidth:150, sort: true, filter: true}
                                    ,{field: 'orderName', title:'款号', align:'center', minWidth:150, sort: true, filter: true, totalRowText: '合计'}
                                    ,{field: 'customerName', title:'客户', align:'center', minWidth:120, sort: true, filter: true, totalRowText: '合计'}
                                    ,{field: 'season', title:'季度', align:'center', minWidth:120, sort: true, filter: true, totalRowText: '合计'}
                                    ,{field: 'supplier', title: '供应商',align:'center', minWidth: 90, sort: true, filter: true}
                                    ,{field: 'fabricName', title: '面料名称',align:'center', minWidth: 165, sort: true, filter: true, totalRowText: '合计'}
                                    ,{field: 'fabricNumber', title: '面料号',align:'center', minWidth: 90, sort: true, filter: true}
                                    ,{field: 'colorName', title: '色组',align:'center', minWidth: 100, sort: true, filter: true}
                                    ,{field: 'fabricColor', title: '面料颜色',align:'center', minWidth: 120, sort: true, filter: true}
                                    ,{field: 'orderCount', title: '订单量',align:'center', minWidth: 90, sort: true, filter: true,excel: {cellType: 'n'}, totalRow:true}
                                    ,{field: 'fabricActCount', title: '总订布',align:'center', minWidth: 90, sort: true, filter: true,excel: {cellType: 'n'}, totalRow:true}
                                    ,{field: 'unit', title: '单位',align:'center', minWidth: 80, sort: true, filter: true}
                                    ,{field: 'weight', title: '回布量',align:'center', minWidth: 90, sort: true, filter: true,excel: {cellType: 'n'}, fixed:'right', totalRow:true}
                                    ,{field: 'price', title: '单价',align:'center', minWidth: 100, fixed:'right', sort: true, filter: true,excel: {cellType: 'n'}, templet:function (d) {
                                            return toDecimal(d.price);
                                        }}
                                    ,{field: 'returnMoney', title: '金额',align:'center', minWidth: 110, fixed:'right', sort: true, filter: true,excel: {cellType: 'n'}, totalRow:true, templet:function (d) {
                                            return toDecimal(d.returnMoney);
                                        }}
                                    ,{field: 'addFee', title: '附加费',align:'center', minWidth: 90, fixed:'right', sort: true, filter: true,excel: {cellType: 'n'}, totalRow:true, templet:function (d) {
                                            return toDecimal(d.addFee);
                                        }}
                                    // ,{field: 'addRemark', title: '备注',align:'center', minWidth: 110, fixed:'right', sort: true, filter: true}
                                    ,{field: 'returnTime', title: '回布日期',align:'center', minWidth: 120, fixed:'right', sort: true, filter: true,  templet:function (d) {
                                            return moment(d.returnTime).format("YYYY-MM-DD");
                                        }}
                                    ,{field: 'checkMonth', title: '入账月',align:'center', minWidth: 90, sort: true, filter: true, fixed:'right'}
                                    ,{field: 'checkNumber', title: '入账号',align:'center', minWidth: 120, sort: true, filter: true, fixed:'right'}
                                    ,{field: 'reviewType',hide:true}
                                    ,{field: 'fabricID',hide:true}
                                    ,{field: 'id',hide:true}
                                    ,{field: 'fabricDetailID',hide:true}
                                    ,{field: 'sourceType',hide:true}
                                ]]
                                ,loading:true
                                ,data:reportData
                                ,height: 'full-130'
                                ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                                ,title: '面辅料表'
                                ,totalRow: true
                                ,even: true
                                ,page: true
                                ,overflow: 'tips'
                                ,limit:500
                                ,limits:[500, 1000, 2000]
                                ,done: function (res, curr, count) {
                                    soulTable.render(this);
                                    layer.close(load);
                                }
                            });
                        }

                    }
                })
            }
            else {
                $.ajax({
                    url: "/erp/getfabricaccessorymonthcheckbyinfo",
                    type: 'GET',
                    data: param,
                    success: function (res) {
                        if (res.data) {
                            var reportData = res.data;
                            reportTable = table.render({
                                elem: '#reportTable'
                                ,cols:[[
                                    {type: 'numbers', align:'center', title:'序号', minWidth:80}
                                    ,{field: 'clothesVersionNumber', title:'单号', align:'center', minWidth:150, sort: true, filter: true}
                                    ,{field: 'orderName', title:'款号', align:'center', minWidth:150, sort: true, filter: true}
                                    ,{field: 'customerName', title:'客户', align:'center', minWidth:120, sort: true, filter: true, totalRowText: '合计'}
                                    ,{field: 'season', title:'季度', align:'center', minWidth:120, sort: true, filter: true, totalRowText: '合计'}
                                    ,{field: 'supplier', title: '供应商',align:'center', minWidth: 90, sort: true, filter: true}
                                    ,{field: 'fabricName', title: '面料名称',align:'center', minWidth: 165, sort: true, filter: true, totalRowText: '合计'}
                                    ,{field: 'fabricNumber', title: '面料号',align:'center', minWidth: 90, sort: true, filter: true}
                                    ,{field: 'colorName', title: '色组',align:'center', minWidth: 100, sort: true, filter: true}
                                    ,{field: 'fabricColor', title: '面料颜色',align:'center', minWidth: 120, sort: true, filter: true}
                                    ,{field: 'orderCount', title: '订单量',align:'center', minWidth: 90, sort: true, filter: true,excel: {cellType: 'n'}, totalRow:true}
                                    ,{field: 'fabricActCount', title: '总订布',align:'center', minWidth: 90, sort: true, filter: true,excel: {cellType: 'n'}, totalRow:true,  templet:function (d) {
                                            return toDecimal(d.fabricActCount);
                                        }}
                                    ,{field: 'unit', title: '单位',align:'center', minWidth: 80, sort: true, filter: true}
                                    ,{field: 'fabricReturn', title: '总回布',align:'center', minWidth: 120, sort: true, filter: true,excel: {cellType: 'n'}, fixed:'right', totalRow:true,  templet:function (d) {
                                            return toDecimal(d.fabricReturn);
                                        }}
                                    ,{field: 'fabricReturnCheckCount', title: '已入账',align:'center', minWidth: 120, sort: true, filter: true,excel: {cellType: 'n'}, fixed:'right', totalRow:true,  templet:function (d) {
                                            return toDecimal(d.fabricReturnCheckCount);
                                        }}
                                    ,{field: 'fabricReturnUnCheck', title: '需入账',align:'center', minWidth: 130, sort: true, filter: true,excel: {cellType: 'n'}, fixed:'right', totalRow:true,  templet:function (d) {
                                            return toDecimal(d.fabricReturnUnCheck);
                                        }}
                                    ,{field: 'fabricID',fixed: 'right', title:'操作', align:'center', toolbar: '#barTop', width:120}
                                ]]
                                ,loading:true
                                ,data:reportData
                                ,height: 'full-130'
                                ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                                ,title: '面辅料表'
                                ,totalRow: true
                                ,even: true
                                ,page: true
                                ,overflow: 'tips'
                                ,limit:500
                                ,limits:[500, 1000, 2000]
                                ,done: function (res, curr, count) {
                                    soulTable.render(this);
                                    layer.close(load);
                                }
                            });
                        }

                    }
                })
            }
        }
        else if (type === "辅料"){
            if (showType === '流水'){
                $.ajax({
                    url: "/erp/getfabricaccessorymonthcheckrecordbyinfo",
                    type: 'GET',
                    data: param,
                    success: function (res) {
                        if (res.data) {
                            var reportData = res.data;
                            reportTable = table.render({
                                elem: '#reportTable'
                                ,cols:[[
                                    {type:'checkbox', fixed: 'left'}
                                    ,{type: 'numbers', align:'center', title:'序号', minWidth:80}
                                    ,{field: 'clothesVersionNumber', title:'单号', align:'center', minWidth:150, sort: true, filter: true}
                                    ,{field: 'orderName', title:'款号', align:'center', minWidth:150, sort: true, filter: true, totalRowText: '合计'}
                                    ,{field: 'customerName', title:'客户', align:'center', minWidth:120, sort: true, filter: true, totalRowText: '合计'}
                                    ,{field: 'season', title:'季度', align:'center', minWidth:120, sort: true, filter: true, totalRowText: '合计'}
                                    ,{field: 'supplier', title: '供应商',align:'center', minWidth: 90, sort: true, filter: true}
                                    ,{field: 'accessoryName', title: '辅料名称',align:'center', minWidth: 165, sort: true, filter: true}
                                    ,{field: 'accessoryNumber', title: '辅料号',align:'center', minWidth: 90, sort: true, filter: true}
                                    ,{field: 'colorName', title: '颜色',align:'center', minWidth: 100, sort: true, filter: true}
                                    ,{field: 'sizeName', title: '尺码',align:'center', minWidth: 100, sort: true, filter: true}
                                    ,{field: 'accessoryColor', title: '辅料颜色',align:'center', minWidth: 120, sort: true, filter: true}
                                    ,{field: 'specification', title: '规格',align:'center', minWidth: 120, sort: true, filter: true}
                                    ,{field: 'orderCount', title: '订单量',align:'center', minWidth: 90, totalRow:true, sort: true, filter: true,excel: {cellType: 'n'}}
                                    ,{field: 'pieceUsage', title: '单耗',align:'center', minWidth: 90, sort: true, filter: true,excel: {cellType: 'n'}}
                                    ,{field: 'accessoryUnit', title: '单位',align:'center', minWidth: 80, sort: true, filter: true}
                                    ,{field: 'accessoryCount', title: '订购量',align:'center', minWidth: 90, sort: true, filter: true, totalRow:true,excel: {cellType: 'n'}}
                                    ,{field: 'inStoreType', title: '类别',align:'center', minWidth: 130, sort: true, filter: true}
                                    ,{field: 'inStoreCount', title: '本次入库',align:'center', minWidth: 100, sort: true, filter: true, fixed:'right', totalRow:true,excel: {cellType: 'n'}}
                                    ,{field: 'accountCount', title: '对账数',align:'center', minWidth: 100, sort: true, filter: true, fixed:'right', totalRow:true,excel: {cellType: 'n'}}
                                    ,{field: 'price', title: '单价',align:'center', minWidth: 90, sort: true, filter: true, fixed:'right',excel: {cellType: 'n'}}
                                    ,{field: 'returnMoney', title: '金额',align:'center', minWidth: 90, fixed:'right', sort: true, filter: true,excel: {cellType: 'n'}, totalRow:true, templet:function (d) {
                                            return toDecimal(d.returnMoney);
                                        }}
                                    ,{field: 'payMoney', title: '应付',align:'center', minWidth: 90, fixed:'right', sort: true, filter: true,excel: {cellType: 'n'}, totalRow:true, templet:function (d) {
                                            return toDecimal(d.payMoney);
                                        }}
                                    ,{field: 'addFee', title: '附加费',align:'center', minWidth: 90, totalRow:true, fixed:'right', sort: true, filter: true,excel: {cellType: 'n'}, templet:function (d) {
                                            return toDecimal(d.addFee);
                                        }}
                                    // ,{field: 'addRemark', title: '备注',align:'center', minWidth: 110, fixed:'right', sort: true, filter: true}
                                    ,{field: 'inStoreTime', title: '入库日期',align:'center', minWidth: 120, fixed:'right', sort: true, filter: true,  templet:function (d) {
                                            return moment(d.inStoreTime).format("YYYY-MM-DD");
                                        }}
                                    ,{field: 'checkMonth', title: '入账月',align:'center', minWidth: 90, sort: true, filter: true, fixed:'right'}
                                    ,{field: 'checkNumber', title: '入账号',align:'center', minWidth: 120, sort: true, filter: true, fixed:'right'}
                                    ,{field: 'reviewType',hide:true}
                                    ,{field: 'accessoryID',hide:true}
                                    ,{field: 'id',hide:true}
                                ]]
                                ,loading:true
                                ,data:reportData
                                ,height: 'full-130'
                                ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                                ,title: '面辅料表'
                                ,totalRow: true
                                ,even: true
                                ,page: true
                                ,overflow: 'tips'
                                ,limit:500
                                ,limits:[500, 1000, 2000]
                                ,done: function (res, curr, count) {
                                    soulTable.render(this);
                                    layer.close(load);
                                }
                            });
                        }
                    }
                })
            } else {
                $.ajax({
                    url: "/erp/getfabricaccessorymonthcheckbyinfo",
                    type: 'GET',
                    data: param,
                    success: function (res) {
                        if (res.data) {
                            var reportData = res.data;
                            console.log(reportData);
                            reportTable = table.render({
                                elem: '#reportTable'
                                ,cols:[[
                                    {type: 'numbers', align:'center', title:'序号', minWidth:80}
                                    ,{field: 'clothesVersionNumber', title:'单号', align:'center', minWidth:150, sort: true, filter: true}
                                    ,{field: 'orderName', title:'款号', align:'center', minWidth:150, sort: true, filter: true, totalRowText: '合计'}
                                    ,{field: 'customerName', title:'客户', align:'center', minWidth:120, sort: true, filter: true, totalRowText: '合计'}
                                    ,{field: 'season', title:'季度', align:'center', minWidth:120, sort: true, filter: true, totalRowText: '合计'}
                                    ,{field: 'supplier', title: '供应商',align:'center', minWidth: 90, sort: true, filter: true}
                                    ,{field: 'accessoryName', title: '辅料名称',align:'center', minWidth: 165, sort: true, filter: true}
                                    ,{field: 'accessoryNumber', title: '辅料号',align:'center', minWidth: 90, sort: true, filter: true}
                                    ,{field: 'colorName', title: '颜色',align:'center', minWidth: 100, sort: true, filter: true}
                                    ,{field: 'sizeName', title: '尺码',align:'center', minWidth: 100, sort: true, filter: true}
                                    ,{field: 'accessoryColor', title: '辅料颜色',align:'center', minWidth: 120, sort: true, filter: true}
                                    ,{field: 'specification', title: '规格',align:'center', minWidth: 120, sort: true, filter: true}
                                    ,{field: 'orderCount', title: '订单量',align:'center', minWidth: 90, sort: true, filter: true,excel: {cellType: 'n'}, totalRow:true}
                                    ,{field: 'pieceUsage', title: '单耗',align:'center', minWidth: 90, sort: true, filter: true,excel: {cellType: 'n'}}
                                    ,{field: 'accessoryUnit', title: '单位',align:'center', minWidth: 80, sort: true, filter: true}
                                    ,{field: 'accessoryCount', title: '订购量',align:'center', minWidth: 90, sort: true, filter: true,excel: {cellType: 'n'}, totalRow:true,  templet:function (d) {
                                            return toDecimal(d.accessoryCount);
                                        }}
                                    ,{field: 'accessoryReturnCount', title: '总入库',align:'center', minWidth: 100, fixed:'right', sort: true,excel: {cellType: 'n'}, filter: true, totalRow:true,  templet:function (d) {
                                            return toDecimal(d.accessoryReturnCount);
                                        }}
                                    ,{field: 'accessoryAccountCount', title: '应对账',align:'center', minWidth: 100, fixed:'right', sort: true, filter: true,excel: {cellType: 'n'}, totalRow:true,  templet:function (d) {
                                            return toDecimal(d.accessoryAccountCount);
                                        }}
                                    ,{field: 'accessoryReturnCheckCount', title: '已对账',align:'center', minWidth: 100, fixed:'right', sort: true, filter: true,excel: {cellType: 'n'}, totalRow:true,  templet:function (d) {
                                            return toDecimal(d.accessoryReturnCheckCount);
                                        }}
                                    ,{field: 'accessoryReturnUnCheck', title: '未对账',align:'center', minWidth: 100, fixed:'right', sort: true, filter: true,excel: {cellType: 'n'}, totalRow:true,  templet:function (d) {
                                            return toDecimal(d.accessoryReturnUnCheck);
                                        }}
                                    ,{field: 'accessoryID',fixed: 'right', title:'操作', align:'center', toolbar: '#barTop2', width:120}
                                ]]
                                ,loading:true
                                ,data:reportData
                                ,height: 'full-130'
                                ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                                ,title: '面辅料表'
                                ,totalRow: true
                                ,even: true
                                ,page: true
                                ,overflow: 'tips'
                                ,limit:500
                                ,limits:[500, 1000, 2000]
                                ,done: function (res, curr, count) {
                                    soulTable.render(this);
                                    layer.close(load);
                                }
                            });
                        }

                    }
                })
            }
        }
        else if (type === "共用辅料"){
            $.ajax({
                url: "/erp/getaccessorypremonthcheckrecordbyinfo",
                type: 'GET',
                data: param,
                success: function (res) {
                    if (res.data) {
                        var reportData = res.data;
                        reportTable = table.render({
                            elem: '#reportTable'
                            ,cols:[[
                                {type:'checkbox', fixed: 'left'}
                                ,{type: 'numbers', align:'center', title:'序号', minWidth:80}
                                ,{field: 'accessoryName', title: '辅料名称',align:'center', minWidth: 165, sort: true, filter: true}
                                ,{field: 'accessoryNumber', title: '辅料号',align:'center', minWidth: 90, sort: true, filter: true}
                                ,{field: 'supplier', title: '供应商',align:'center', minWidth: 90, sort: true, filter: true}
                                ,{field: 'specification', title: '规格',align:'center', minWidth: 120, sort: true, filter: true}
                                ,{field: 'accessoryColor', title: '颜色',align:'center', minWidth: 120, sort: true, filter: true}
                                ,{field: 'pieceUsage', title: '单耗',align:'center', minWidth: 90, sort: true, filter: true,excel: {cellType: 'n'}}
                                ,{field: 'accessoryUnit', title: '单位',align:'center', minWidth: 80, sort: true, filter: true}
                                ,{field: 'inStoreCount', title: '入库量',align:'center', minWidth: 90, sort: true, filter: true,excel: {cellType: 'n'}, totalRow:true}
                                ,{field: 'price', title: '单价',align:'center', minWidth: 120, sort: true, filter: true,excel: {cellType: 'n'}, fixed:'right'}
                                ,{field: 'returnMoney', title: '金额',align:'center', minWidth: 130, fixed:'right', sort: true, filter: true,excel: {cellType: 'n'}, totalRow:true,  templet:function (d) {
                                        return toDecimal(d.returnMoney);
                                    }}
                                ,{field: 'createTime', title: '入库日期',align:'center', minWidth: 120, fixed:'right', sort: true, filter: true,  templet:function (d) {
                                        return moment(d.createTime).format("YYYY-MM-DD");
                                    }}
                                ,{field: 'checkMonth', title: '入账月',align:'center', minWidth: 90, sort: true, filter: true, fixed:'right'}
                                ,{field: 'checkNumber', title: '入账号',align:'center', minWidth: 120, sort: true, filter: true, fixed:'right'}
                                ,{field: 'reviewType',hide:true}
                                ,{field: 'id',hide:true}
                            ]]
                            ,loading:true
                            ,data:reportData
                            ,height: 'full-130'
                            ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                            ,title: '共用辅料表'
                            ,totalRow: true
                            ,even: true
                            ,page: true
                            ,overflow: 'tips'
                            ,limit:500
                            ,limits:[500, 1000, 2000]
                            ,done: function (res, curr, count) {
                                soulTable.render(this);
                                layer.close(load);
                            }
                        });
                    }

                }
            })
        }
    }

    form.on('submit(searchBeat)', function(data){
        filterOrderName = $("#orderName").val();
        filterCustomerName = $("#customerName").val();
        filterSeason = $("#seasonName").val();
        filterType = $("#type").val();
        filterFrom = $("#from").val();
        filterTo = $("#to").val();
        filterCheckState = $("#checkState").val();
        showType = $("#showType").val();
        filterSupplier = supplierDemo.getValue('nameStr');
        initTable(filterFrom, filterTo, filterCustomerName, filterSeason, filterOrderName, filterSupplier, filterType, filterCheckState, showType);
        return false;
    });

    form.on('select(demo)', function(data){
        if(data.value == "面料"){
            $.ajax({
                url: "/erp/getallfabricsupplier",
                data: {},
                success:function(data){
                    if (data.supplierList){
                        var resultData = [];
                        $.each(data.supplierList, function(index,element){
                            if (element != null && element != ''){
                                resultData.push({name:element, value:element});
                            }
                        });
                        supplierDemo = xmSelect.render({
                            el: '#supplier',
                            radio: true,
                            filterable: true,
                            data: resultData
                        });
                    }
                }, error:function(){
                }
            });
            form.render('select');
        }else{
            $.ajax({
                url: "/erp/getallaccessorysupplier",
                data: {},
                success:function(data){
                    if (data.supplierList){
                        var resultData = [];
                        $.each(data.supplierList, function(index,element){
                            if (element != null && element != ''){
                                resultData.push({name:element, value:element});
                            }
                        });
                        supplierDemo = xmSelect.render({
                            el: '#supplier',
                            radio: true,
                            filterable: true,
                            data: resultData
                        });
                    }
                }, error:function(){
                }
            });
            form.render('select');
        }
    });

    table.on('toolbar(reportTable)', function(obj){
        if (obj.event === 'clearFilter') {
            // 清除所有筛选条件并重载表格
            // 参数: tableId
            soulTable.clearFilter('reportTable')
        } else if (obj.event === 'exportExcel') {
            soulTable.export('reportTable');
        }else if (obj.event === "check") {   //面料下单
            if(userRole !== 'role10' && userRole !== 'root') {
                layer.msg('财务/管理员才能操作~~~');
                return false;
            }
            var checkStatus = table.checkStatus(obj.config.id); //获取选中行状态
            if(checkStatus.data.length == 0) {
                layer.msg('请先选择要入账的数据');
            }else {
                var nowDate = dateFormat("YYYY-mm-dd HH:MM:SS", new Date()).replace(" ", "").replace(/\:/g,'').replace(/\-/g,'');
                var reviewType;
                reviewType = checkStatus.data[0].reviewType;
                if (reviewType === '面料'){
                    var fabricDetailList = [];
                    var fabricOutRecordList = [];
                    var index = layer.open({
                        type: 1 //Page层类型
                        , title: '添加'
                        , btn: ['入账','取消']
                        , shade: 0.6 //遮罩透明度
                        , maxmin: false //允许全屏最小化
                        , anim: 0 //0-6的动画形式，-1不开启
                        , area: ['500px', '300px']
                        , content: $("#operateDiv")
                        , yes: function (index, layero) {
                            // 通过form批量取值
                            var param = form.val("formBaseInfo");
                            if (param.checkMonth == null || param.checkMonth == ""){
                                layer.msg("请填写完整", { icon: 2 });
                                return false;
                            }
                            var checkMonth = $("#checkMonth").val();
                            $.each(checkStatus.data, function (index, item) {
                                if (item.sourceType === 'detail'){
                                    fabricDetailList.push({fabricDetailID: item.id, checkNumber: nowDate, checkMonth: checkMonth, fabricID: item.fabricID, addFee: item.addFee});
                                } else {
                                    fabricOutRecordList.push({fabricOutRecordID: item.id, checkNumber: nowDate, checkMonth: checkMonth});
                                }
                            });
                            $.ajax({
                                url: "/erp/updatefabricdetailincheck",
                                type: 'POST',
                                data: {
                                    fabricDetailJson: JSON.stringify(fabricDetailList),
                                    fabricOutRecordJson: JSON.stringify(fabricOutRecordList),
                                    checkMonth: checkMonth
                                },
                                success: function (res) {
                                    if (res.result == 0) {
                                        initTable(filterFrom, filterTo, filterCustomerName, filterSeason, filterOrderName, filterSupplier, filterType, filterCheckState);
                                        layer.close(index);
                                        layer.msg("提交成功！", {icon: 1});
                                        $("#checkMonth").val("");
                                    } else if (res.result == 3) {
                                        layer.msg("本月对账已锁定,无法继续对账！", {icon: 2});
                                    } else {
                                        layer.msg("提交失败！", {icon: 2});
                                    }
                                },
                                error: function () {
                                    layer.msg("提交失败！", {icon: 2});
                                }
                            })
                        }
                        , cancel: function (i, layero) {
                            $("#checkMonth").val("");
                            layer.closeAll();
                        }
                    });
                    form.render('select');
                    layui.laydate.render({
                        elem: '#checkMonth'
                        ,type: 'month'
                    });
                }
                else if (reviewType === '辅料'){
                    var accessoryInStoreList = [];
                    var index = layer.open({
                        type: 1 //Page层类型
                        , title: '添加'
                        , btn: ['入账','取消']
                        , shade: 0.6 //遮罩透明度
                        , maxmin: false //允许全屏最小化
                        , anim: 0 //0-6的动画形式，-1不开启
                        , area: ['500px', '300px']
                        , content: $("#operateDiv")
                        , yes: function (index, layero) {
                            // 通过form批量取值
                            var param = form.val("formBaseInfo");
                            if (param.checkMonth == null || param.checkMonth == ""){
                                layer.msg("请填写完整", { icon: 2 });
                                return false;
                            }
                            var checkMonth = $("#checkMonth").val();
                            $.each(checkStatus.data, function (index, item) {
                                accessoryInStoreList.push({id: item.id, checkNumber: nowDate, checkMonth: checkMonth, accessoryID: item.accessoryID, addFee: item.addFee});
                            });
                            $.ajax({
                                url: "/erp/updateaccessoryinstoreincheck",
                                type: 'POST',
                                data: {
                                    accessoryInStoreJson: JSON.stringify(accessoryInStoreList),
                                    checkMonth: checkMonth
                                },
                                success: function (res) {
                                    if (res.result == 0) {
                                        initTable(filterFrom, filterTo, filterCustomerName, filterSeason, filterOrderName, filterSupplier, filterType, filterCheckState);
                                        layer.close(index);
                                        layer.msg("提交成功！", {icon: 1});
                                        $("#checkMonth").val("");
                                    } else if (res.result == 3) {
                                        layer.msg("本月对账已锁定,无法继续对账！", {icon: 2});
                                    } else {
                                        layer.msg("提交失败！", {icon: 2});
                                    }
                                },
                                error: function () {
                                    layer.msg("提交失败！", {icon: 2});
                                }
                            })
                        }
                        , cancel: function (i, layero) {
                            $("#checkMonth").val("");
                            layer.closeAll();
                        }
                    });
                    form.render('select');
                    layui.laydate.render({
                        elem: '#checkMonth'
                        ,type: 'month'
                    });
                }
                else if (reviewType === '共用辅料'){
                    var accessoryInStoreList = [];
                    var index = layer.open({
                        type: 1 //Page层类型
                        , title: '添加'
                        , btn: ['入账','取消']
                        , shade: 0.6 //遮罩透明度
                        , maxmin: false //允许全屏最小化
                        , anim: 0 //0-6的动画形式，-1不开启
                        , area: ['500px', '300px']
                        , content: $("#operateDiv")
                        , yes: function (index, layero) {
                            // 通过form批量取值
                            var param = form.val("formBaseInfo");
                            if (param.checkMonth == null || param.checkMonth == ""){
                                layer.msg("请填写完整", { icon: 2 });
                                return false;
                            }
                            var checkMonth = $("#checkMonth").val();
                            $.each(checkStatus.data, function (index, item) {
                                accessoryInStoreList.push({id: item.id, checkNumber: nowDate, checkMonth: checkMonth});
                            });
                            $.ajax({
                                url: "/erp/updateaccessoryprestoreincheck",
                                type: 'POST',
                                data: {
                                    accessoryPreStoreJson: JSON.stringify(accessoryInStoreList),
                                    checkMonth: checkMonth
                                },
                                success: function (res) {
                                    if (res.result == 0) {
                                        initTable(filterFrom, filterTo, filterCustomerName, filterSeason, filterOrderName, filterSupplier, filterType, filterCheckState);
                                        layer.close(index);
                                        layer.msg("提交成功！", {icon: 1});
                                        $("#checkMonth").val("");
                                    } else if (res.result == 3) {
                                        layer.msg("本月对账已锁定,无法继续对账！", {icon: 2});
                                    } else {
                                        layer.msg("提交失败！", {icon: 2});
                                    }
                                },
                                error: function () {
                                    layer.msg("提交失败！", {icon: 2});
                                }
                            })
                        }
                        , cancel: function (i, layero) {
                            $("#checkMonth").val("");
                            layer.closeAll();
                        }
                    });
                    form.render('select');
                    layui.laydate.render({
                        elem: '#checkMonth'
                        ,type: 'month'
                    });
                }
            }
        } else if (obj.event === "unCheck") {   //面料下单
            var checkStatus = table.checkStatus(obj.config.id); //获取选中行状态
            if(checkStatus.data.length == 0) {
                layer.msg('请先选择要取消的数据');
            }else {
                var reviewType;
                reviewType = checkStatus.data[0].reviewType;
                if (reviewType === '面料'){
                    var fabricDetailIDList = [];
                    $.each(checkStatus.data, function (index, item) {
                        fabricDetailIDList.push(item.id);
                    });
                    layer.confirm('确认取消入账吗?', function(index){
                        $.ajax({
                            url: "/erp/quitfabriccheckbatch",
                            type: 'POST',
                            data: {
                                fabricDetailIDList: fabricDetailIDList
                            },
                            traditional: true,
                            success: function (res) {
                                if (res.result == 0) {
                                    initTable(filterFrom, filterTo, filterCustomerName, filterSeason, filterOrderName, filterSupplier, filterType, filterCheckState);
                                    layer.close(index);
                                    layer.msg("提交成功！", {icon: 1});
                                } else {
                                    layer.msg("提交失败！", {icon: 2});
                                }
                            },
                            error: function () {
                                layer.msg("提交失败！", {icon: 2});
                            }
                        })

                    });
                }
                else if (reviewType === '辅料'){
                    var idList = [];
                    $.each(checkStatus.data, function (index, item) {
                        idList.push(item.id);
                    });
                    layer.confirm('确认取消入账吗?', function(index){
                        $.ajax({
                            url: "/erp/quitaccessoryinstorebatch",
                            type: 'POST',
                            data: {
                                idList: idList
                            },
                            traditional: true,
                            success: function (res) {
                                if (res.result == 0) {
                                    initTable(filterFrom, filterTo, filterCustomerName, filterSeason, filterOrderName, filterSupplier, filterType, filterCheckState);
                                    layer.close(index);
                                    layer.msg("提交成功！", {icon: 1});
                                } else {
                                    layer.msg("提交失败！", {icon: 2});
                                }
                            },
                            error: function () {
                                layer.msg("提交失败！", {icon: 2});
                            }
                        })

                    });
                }
                else if (reviewType === '共用辅料'){
                    var idList = [];
                    $.each(checkStatus.data, function (index, item) {
                        idList.push(item.id);
                    });
                    layer.confirm('确认取消入账吗?', function(index){
                        $.ajax({
                            url: "/erp/quitaccessoryprestorebatch",
                            type: 'POST',
                            data: {
                                idList: idList
                            },
                            traditional: true,
                            success: function (res) {
                                if (res.result == 0) {
                                    initTable(filterFrom, filterTo, filterCustomerName, filterSeason, filterOrderName, filterSupplier, filterType, filterCheckState);
                                    layer.close(index);
                                    layer.msg("提交成功！", {icon: 1});
                                } else {
                                    layer.msg("提交失败！", {icon: 2});
                                }
                            },
                            error: function () {
                                layer.msg("提交失败！", {icon: 2});
                            }
                        })

                    });
                }
            }
        }
    });

    table.on('tool(reportTable)', function(obj){
        var data = obj.data;
        if(obj.event === 'detail'){
            var index = layer.open({
                type: 1 //Page层类型
                , title: '详情'
                , btn: ['取消入账']
                , shade: 0.6 //遮罩透明度
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , area: ['500px', '300px']
                , content: $("#detailDiv")
                , yes: function (index, layero) {
                    var fabricCheckData = table.cache.detailTable;
                    var idList = [];
                    $.each(fabricCheckData,function(i,item){
                        if (item.LAY_CHECKED){
                            idList.push(item.fabricDetailID);
                        }
                    });
                    if (idList.length == 0){
                        layer.msg("请选择要取消入账的数据~~", { icon: 2 });
                        return false;
                    }
                    layer.confirm('确认取消入账吗?', function(index){
                        $.ajax({
                            url: "/erp/quitfabriccheckbatch",
                            type: 'POST',
                            data: {
                                fabricDetailIDList: idList
                            },
                            traditional: true,
                            success: function (res) {
                                if (res.result == 0) {
                                    layer.closeAll();
                                    initTable(filterFrom, filterTo, filterCustomerName, filterSeason, filterOrderName, filterSupplier, filterType, filterCheckState);
                                    layer.msg("提交成功！", {icon: 1});
                                } else {
                                    layer.msg("提交失败！", {icon: 2});
                                }
                            },
                            error: function () {
                                layer.msg("提交失败！", {icon: 2});
                            }
                        })

                    });
                }
                , cancel: function (i, layero) {
                    layer.closeAll();
                }
            });
            layer.full(index);
            setTimeout(function () {
                $.ajax({
                    url: "/erp/getfabricreadycheckdata",
                    type: 'GET',
                    data: {
                        fabricID: data.fabricID,
                        checkState: 1
                    },
                    success: function (res) {
                        if (res.fabricDetailList) {
                            var reportData = res.fabricDetailList;
                            table.render({
                                elem: '#detailTable'
                                ,cols:[[
                                    {type:'checkbox'}
                                    ,{type: 'numbers', align:'center', title:'序号', minWidth:80}
                                    ,{field: 'clothesVersionNumber', title:'单号', align:'center', minWidth:150, sort: true, filter: true}
                                    ,{field: 'orderName', title:'款号', align:'center', minWidth:150, sort: true, filter: true, totalRowText: '合计'}
                                    ,{field: 'supplier', title: '供应商',align:'center', minWidth: 90, sort: true, filter: true}
                                    ,{field: 'fabricName', title: '面料名称',align:'center', minWidth: 165, sort: true, filter: true}
                                    ,{field: 'fabricNumber', title: '面料号',align:'center', minWidth: 90, sort: true, filter: true}
                                    ,{field: 'colorName', title: '色组',align:'center', minWidth: 100, sort: true, filter: true}
                                    ,{field: 'fabricColor', title: '面料颜色',align:'center', minWidth: 120, sort: true, filter: true}
                                    ,{field: 'unit', title: '单位',align:'center', minWidth: 80, sort: true, filter: true}
                                    ,{field: 'jarName', title: '缸号',align:'center', minWidth: 120, sort: true, filter: true}
                                    ,{field: 'weight', title: '回布',align:'center', minWidth: 120, sort: true, filter: true,excel: {cellType: 'n'}, fixed:'right', totalRow:true,  templet:function (d) {
                                            return toDecimal(d.weight);
                                        }}
                                    ,{field: 'price', title: '单价',align:'center', minWidth: 120, sort: true, filter: true,excel: {cellType: 'n'}, fixed:'right', totalRow:true,  templet:function (d) {
                                            return toDecimal(d.price);
                                        }}
                                    ,{field: 'returnMoney', title: '总价',align:'center', minWidth: 120, sort: true, filter: true,excel: {cellType: 'n'}, fixed:'right', totalRow:true,  templet:function (d) {
                                            return toDecimal(d.returnMoney);
                                        }}
                                    ,{field: 'returnTime', title: '回布日期',align:'center', minWidth: 120, sort: true, filter: true, fixed:'right', totalRow:true,  templet:function (d) {
                                            return moment(d.inStoreTime).format("YYYY-MM-DD");
                                        }}
                                    ,{field: 'checkMonth', title: '入账月',align:'center', minWidth: 90, sort: true, filter: true, fixed:'right'}
                                    ,{field: 'checkNumber', title: '入账号',align:'center', minWidth: 120, sort: true, filter: true, fixed:'right'}
                                    ,{field: 'fabricDetailID', hide: true}
                                ]]
                                ,loading:true
                                ,data:reportData
                                ,height: 'full-130'
                                ,title: '面辅料表'
                                ,totalRow: true
                                ,even: true
                                ,page: true
                                ,overflow: 'tips'
                                ,limit:500
                                ,limits:[500, 1000, 2000]
                                ,done: function (res, curr, count) {
                                    soulTable.render(this);
                                }
                            });
                        }
                    }
                })
            }, 100);
        } else if(obj.event === 'check'){
            var index = layer.open({
                type: 1 //Page层类型
                , title: '入账'
                , btn: ['入账','关闭']
                , shade: 0.6 //遮罩透明度
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , area: ['500px', '300px']
                , content: $("#operateDiv")
                , yes: function (index, layero) {
                    var fabricCheckData = table.cache.checkTable;
                    var fabricDetailList = [];
                    var checkMonth = $("#checkMonth").val();
                    var nowDate = dateFormat("YYYY-mm-dd HH:MM:SS", new Date()).replace(" ", "").replace(/\:/g,'').replace(/\-/g,'');
                    if (checkMonth == null || checkMonth == ""){
                        layer.msg("请填写完整", { icon: 2 });
                        return false;
                    }
                    $.each(fabricCheckData,function(i,item){
                        if (item.LAY_CHECKED){
                            fabricDetailList.push({fabricDetailID: item.fabricDetailID, checkNumber: nowDate, checkMonth: checkMonth});
                        }
                    });
                    if (fabricDetailList.length == 0){
                        layer.msg("请选择要入账的数据~~", { icon: 2 });
                        return false;
                    }
                    $.ajax({
                        url: "/erp/updatefabricdetailincheck",
                        type: 'POST',
                        data: {
                            fabricDetailJson: JSON.stringify(fabricDetailList),
                            checkMonth: checkMonth
                        },
                        success: function (res) {
                            if (res.result == 0) {
                                initTable(filterFrom, filterTo, filterCustomerName, filterSeason, filterOrderName, filterSupplier, filterType, filterCheckState);
                                layer.close(index);
                                layer.msg("提交成功！", {icon: 1});
                                $("#checkMonth").val("");
                            } else if (res.result == 3) {
                                layer.msg("本月对账已锁定,无法继续对账！", {icon: 2});
                            } else {
                                layer.msg("提交失败！", {icon: 2});
                            }
                        },
                        error: function () {
                            layer.msg("提交失败！", {icon: 2});
                        }
                    })

                }
                , cancel: function (i, layero) {
                    $("#checkMonth").val("");
                    layer.closeAll();
                }
            });
            layer.full(index);
            form.render('select');
            layui.laydate.render({
                elem: '#checkMonth'
                ,type: 'month'
            });
            setTimeout(function () {
                $.ajax({
                    url: "/erp/getfabricreadycheckdata",
                    type: 'GET',
                    data: {
                        fabricID: data.fabricID,
                        checkState: 0
                    },
                    success: function (res) {
                        if (res.fabricDetailList) {
                            var reportData = res.fabricDetailList;
                            table.render({
                                elem: '#checkTable'
                                ,cols:[[
                                    {type:'checkbox'}
                                    ,{type: 'numbers', align:'center', title:'序号', minWidth:80}
                                    ,{field: 'clothesVersionNumber', title:'单号', align:'center', minWidth:150, sort: true, filter: true}
                                    ,{field: 'orderName', title:'款号', align:'center', minWidth:150, sort: true, filter: true, totalRowText: '合计'}
                                    ,{field: 'supplier', title: '供应商',align:'center', minWidth: 90, sort: true, filter: true}
                                    ,{field: 'fabricName', title: '面料名称',align:'center', minWidth: 165, sort: true, filter: true}
                                    ,{field: 'fabricNumber', title: '面料号',align:'center', minWidth: 90, sort: true, filter: true}
                                    ,{field: 'colorName', title: '色组',align:'center', minWidth: 100, sort: true, filter: true}
                                    ,{field: 'fabricColor', title: '面料颜色',align:'center', minWidth: 120, sort: true, filter: true}
                                    ,{field: 'unit', title: '单位',align:'center', minWidth: 80, sort: true, filter: true}
                                    ,{field: 'weight', title: '回布',align:'center', minWidth: 120, sort: true, filter: true,excel: {cellType: 'n'}, fixed:'right', totalRow:true,  templet:function (d) {
                                            return toDecimal(d.weight);
                                        }}
                                    ,{field: 'price', title: '单价',align:'center', minWidth: 120, sort: true, filter: true,excel: {cellType: 'n'}, fixed:'right', totalRow:true,  templet:function (d) {
                                            return toDecimal(d.price);
                                        }}
                                    ,{field: 'returnMoney', title: '总价',align:'center', minWidth: 120, sort: true, filter: true,excel: {cellType: 'n'}, fixed:'right', totalRow:true,  templet:function (d) {
                                            return toDecimal(d.returnMoney);
                                        }}
                                    ,{field: 'returnTime', title: '回布日期',align:'center', minWidth: 120, sort: true, filter: true, fixed:'right', totalRow:true,  templet:function (d) {
                                            return moment(d.inStoreTime).format("YYYY-MM-DD");
                                        }}
                                    ,{field: 'fabricDetailID', hide: true}
                                ]]
                                ,loading:true
                                ,data:reportData
                                ,height: 'full-170'
                                ,title: '面辅料表'
                                ,totalRow: true
                                ,even: true
                                ,page: true
                                ,overflow: 'tips'
                                ,limit:500
                                ,limits:[500, 1000, 2000]
                                ,done: function (res, curr, count) {
                                    soulTable.render(this);
                                }
                            });
                        }

                    }
                })
            }, 100);
        } else if(obj.event === 'detail2'){
            var index = layer.open({
                type: 1 //Page层类型
                , title: '详情'
                , btn: ['取消入账']
                , shade: 0.6 //遮罩透明度
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , area: ['500px', '300px']
                , content: $("#detailDiv")
                , yes: function (index, layero) {
                    var accessoryCheckData = table.cache.detailTable;
                    var idList = [];
                    $.each(accessoryCheckData,function(i,item){
                        if (item.LAY_CHECKED){
                            idList.push(item.id);
                        }
                    });
                    if (idList.length == 0){
                        layer.msg("请选择要取消入账的数据~~", { icon: 2 });
                        return false;
                    }
                    layer.confirm('确认取消入账吗?', function(index){
                        $.ajax({
                            url: "/erp/quitaccessoryinstorebatch",
                            type: 'POST',
                            data: {
                                idList: idList
                            },
                            traditional: true,
                            success: function (res) {
                                if (res.result == 0) {
                                    initTable(filterFrom, filterTo, filterCustomerName, filterSeason, filterOrderName, filterSupplier, filterType, filterCheckState);
                                    layer.closeAll();
                                    layer.msg("提交成功！", {icon: 1});
                                } else {
                                    layer.msg("提交失败！", {icon: 2});
                                }
                            },
                            error: function () {
                                layer.msg("提交失败！", {icon: 2});
                            }
                        })

                    });
                }
                , cancel: function (i, layero) {
                    layer.closeAll();
                }
            });
            layer.full(index);
            setTimeout(function () {
                $.ajax({
                    url: "/erp/getaccessoryreadycheckdata",
                    type: 'GET',
                    data: {
                        accessoryID: data.accessoryID,
                        checkState: 1
                    },
                    success: function (res) {
                        if (res.accessoryInStoreList) {
                            var reportData = res.accessoryInStoreList;
                            table.render({
                                elem: '#detailTable'
                                ,cols:[[
                                    {type:'checkbox'}
                                    ,{type: 'numbers', align:'center', title:'序号', minWidth:80}
                                    ,{field: 'clothesVersionNumber', title:'单号', align:'center', minWidth:150, sort: true, filter: true}
                                    ,{field: 'orderName', title:'款号', align:'center', minWidth:150, sort: true, filter: true, totalRowText: '合计'}
                                    ,{field: 'supplier', title: '供应商',align:'center', minWidth: 90, sort: true, filter: true}
                                    ,{field: 'accessoryName', title: '辅料名称',align:'center', minWidth: 165, sort: true, filter: true}
                                    ,{field: 'accessoryNumber', title: '辅料号',align:'center', minWidth: 90, sort: true, filter: true}
                                    ,{field: 'colorName', title: '颜色',align:'center', minWidth: 100, sort: true, filter: true}
                                    ,{field: 'sizeName', title: '尺码',align:'center', minWidth: 100, sort: true, filter: true}
                                    ,{field: 'specification', title: '规格',align:'center', minWidth: 120, sort: true, filter: true}
                                    ,{field: 'orderCount', title: '订单量',align:'center', minWidth: 90, sort: true, filter: true,excel: {cellType: 'n'}, totalRow:true}
                                    ,{field: 'pieceUsage', title: '单耗',align:'center', minWidth: 90, sort: true, filter: true,excel: {cellType: 'n'}}
                                    ,{field: 'accessoryUnit', title: '单位',align:'center', minWidth: 80, sort: true, filter: true}
                                    ,{field: 'inStoreType', title: '类别',align:'center', minWidth: 120, sort: true, filter: true}
                                    ,{field: 'inStoreCount', title: '入库',align:'center', minWidth: 120, sort: true, filter: true,excel: {cellType: 'n'}, fixed:'right', totalRow:true,  templet:function (d) {
                                            return toDecimal(d.inStoreCount);
                                        }}
                                    ,{field: 'accountCount', title: '对账数',align:'center', minWidth: 120, sort: true, filter: true,excel: {cellType: 'n'}, fixed:'right', totalRow:true,  templet:function (d) {
                                            return toDecimal(d.accountCount);
                                        }}
                                    ,{field: 'price', title: '单价',align:'center', minWidth: 120, sort: true, filter: true,excel: {cellType: 'n'}, fixed:'right', totalRow:true,  templet:function (d) {
                                            return toDecimal(d.price);
                                        }}
                                    ,{field: 'returnMoney', title: '总价',align:'center', minWidth: 120, sort: true, filter: true,excel: {cellType: 'n'}, fixed:'right', totalRow:true,  templet:function (d) {
                                            return toDecimal(d.returnMoney);
                                        }}
                                    ,{field: 'payMoney', title: '应付',align:'center', minWidth: 120, sort: true, filter: true,excel: {cellType: 'n'}, fixed:'right', totalRow:true,  templet:function (d) {
                                            return toDecimal(d.payMoney);
                                        }}
                                    ,{field: 'createTime', title: '入库日期',align:'center', minWidth: 120, sort: true, filter: true, fixed:'right', totalRow:true,  templet:function (d) {
                                            return moment(d.createTime).format("YYYY-MM-DD");
                                        }}
                                    ,{field: 'checkMonth', title: '入账月',align:'center', minWidth: 90, sort: true, filter: true, fixed:'right'}
                                    ,{field: 'checkNumber', title: '入账号',align:'center', minWidth: 120, sort: true, filter: true, fixed:'right'}
                                    ,{field: 'id', hide: true}
                                ]]
                                ,loading:true
                                ,data:reportData
                                ,height: 'full-130'
                                ,title: '面辅料表'
                                ,totalRow: true
                                ,even: true
                                ,page: true
                                ,overflow: 'tips'
                                ,limit:500
                                ,limits:[500, 1000, 2000]
                                ,done: function (res, curr, count) {
                                    soulTable.render(this);
                                }
                            });
                        }
                    }
                })
            }, 100);
        } else if(obj.event === 'check2'){
            var index = layer.open({
                type: 1 //Page层类型
                , title: '入账'
                , btn: ['入账','关闭']
                , shade: 0.6 //遮罩透明度
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , area: ['500px', '300px']
                , content: $("#operateDiv")
                , yes: function (index, layero) {
                    var accessoryCheckData = table.cache.checkTable;
                    var accessoryInStoreList = [];
                    var checkMonth = $("#checkMonth").val();
                    var nowDate = dateFormat("YYYY-mm-dd HH:MM:SS", new Date()).replace(" ", "").replace(/\:/g,'').replace(/\-/g,'');
                    if (checkMonth == null || checkMonth == ""){
                        layer.msg("请填写完整", { icon: 2 });
                        return false;
                    }
                    $.each(accessoryCheckData,function(i,item){
                        if (item.LAY_CHECKED){
                            accessoryInStoreList.push({id: item.id, checkNumber: nowDate, checkMonth: checkMonth});
                        }
                    });
                    if (accessoryInStoreList.length == 0){
                        layer.msg("请选择要入账的数据~~", { icon: 2 });
                        return false;
                    }
                    $.ajax({
                        url: "/erp/updateaccessoryinstoreincheck",
                        type: 'POST',
                        data: {
                            accessoryInStoreJson: JSON.stringify(accessoryInStoreList),
                            checkMonth: checkMonth
                        },
                        success: function (res) {
                            if (res.result == 0) {
                                initTable(filterFrom, filterTo, filterCustomerName, filterSeason, filterOrderName, filterSupplier, filterType, filterCheckState);
                                layer.close(index);
                                layer.msg("提交成功！", {icon: 1});
                                $("#checkMonth").val("");
                            } else if (res.result == 3) {
                                layer.msg("本月对账已锁定,无法继续对账！", {icon: 2});
                            } else {
                                layer.msg("提交失败！", {icon: 2});
                            }
                        },
                        error: function () {
                            layer.msg("提交失败！", {icon: 2});
                        }
                    })

                }
                , cancel: function (i, layero) {
                    $("#checkMonth").val("");
                    layer.closeAll();
                }
            });
            layer.full(index);
            form.render('select');
            layui.laydate.render({
                elem: '#checkMonth'
                ,type: 'month'
            });
            setTimeout(function () {
                $.ajax({
                    url: "/erp/getaccessoryreadycheckdata",
                    type: 'GET',
                    data: {
                        accessoryID: data.accessoryID,
                        checkState: 0
                    },
                    success: function (res) {
                        if (res.accessoryInStoreList) {
                            var reportData = res.accessoryInStoreList;
                            table.render({
                                elem: '#checkTable'
                                ,cols:[[
                                    {type:'checkbox'}
                                    ,{type: 'numbers', align:'center', title:'序号', minWidth:80}
                                    ,{field: 'clothesVersionNumber', title:'单号', align:'center', minWidth:150, sort: true, filter: true}
                                    ,{field: 'orderName', title:'款号', align:'center', minWidth:150, sort: true, filter: true, totalRowText: '合计'}
                                    ,{field: 'supplier', title: '供应商',align:'center', minWidth: 90, sort: true, filter: true}
                                    ,{field: 'accessoryName', title: '辅料名称',align:'center', minWidth: 165, sort: true, filter: true}
                                    ,{field: 'accessoryNumber', title: '辅料号',align:'center', minWidth: 90, sort: true, filter: true}
                                    ,{field: 'colorName', title: '颜色',align:'center', minWidth: 100, sort: true, filter: true}
                                    ,{field: 'sizeName', title: '尺码',align:'center', minWidth: 100, sort: true, filter: true}
                                    ,{field: 'specification', title: '规格',align:'center', minWidth: 120, sort: true, filter: true}
                                    ,{field: 'orderCount', title: '订单量',align:'center', minWidth: 90, sort: true, filter: true,excel: {cellType: 'n'}, totalRow:true}
                                    ,{field: 'pieceUsage', title: '单耗',align:'center', minWidth: 90, sort: true, filter: true,excel: {cellType: 'n'}}
                                    ,{field: 'accessoryUnit', title: '单位',align:'center', minWidth: 80, sort: true, filter: true}
                                    ,{field: 'inStoreType', title: '类别',align:'center', minWidth: 120, sort: true, filter: true}
                                    ,{field: 'inStoreCount', title: '入库',align:'center', minWidth: 120, sort: true, filter: true,excel: {cellType: 'n'}, fixed:'right', totalRow:true,  templet:function (d) {
                                            return toDecimal(d.inStoreCount);
                                        }}
                                    ,{field: 'accountCount', title: '对账',align:'center', minWidth: 120, sort: true, filter: true,excel: {cellType: 'n'}, fixed:'right', totalRow:true,  templet:function (d) {
                                            return toDecimal(d.accountCount);
                                        }}
                                    ,{field: 'price', title: '单价',align:'center', minWidth: 120, sort: true, filter: true,excel: {cellType: 'n'}, fixed:'right', totalRow:true,  templet:function (d) {
                                            return toDecimal(d.price);
                                        }}
                                    ,{field: 'returnMoney', title: '总价',align:'center', minWidth: 120, sort: true, filter: true,excel: {cellType: 'n'}, fixed:'right', totalRow:true,  templet:function (d) {
                                            return toDecimal(d.returnMoney);
                                        }}
                                    ,{field: 'payMoney', title: '应付',align:'center', minWidth: 120, sort: true, filter: true,excel: {cellType: 'n'}, fixed:'right', totalRow:true,  templet:function (d) {
                                            return toDecimal(d.payMoney);
                                        }}
                                    ,{field: 'createTime', title: '入库日期',align:'center', minWidth: 120, sort: true, filter: true, fixed:'right', totalRow:true,  templet:function (d) {
                                            return moment(d.createTime).format("YYYY-MM-DD");
                                        }}
                                    ,{field: 'id', hide: true}
                                ]]
                                ,loading:true
                                ,data:reportData
                                ,height: 'full-130'
                                ,title: '面辅料表'
                                ,totalRow: true
                                ,even: true
                                ,page: true
                                ,overflow: 'tips'
                                ,limit:500
                                ,limits:[500, 1000, 2000]
                                ,done: function (res, curr, count) {
                                    soulTable.render(this);
                                }
                            });
                        }

                    }
                })
            }, 100);
        }
    });

});
function toDecimal(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*100)/100;
    return f;
}
function toDecimalFour(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*10000)/10000;
    return f;
}
function toDecimalSix(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*1000000)/1000000;
    return f;
}
function dateFormat(fmt, date) {
    var ret;
    var opt = {
        "Y+": date.getFullYear().toString(),        // 年
        "m+": (date.getMonth() + 1).toString(),     // 月
        "d+": date.getDate().toString(),            // 日
        "H+": date.getHours().toString(),           // 时
        "M+": date.getMinutes().toString(),         // 分
        "S+": date.getSeconds().toString()          // 秒
        // 有其他格式化字符需求可以继续添加，必须转化成字符串
    };
    for (var k in opt) {
        ret = new RegExp("(" + k + ")").exec(fmt);
        if (ret) {
            fmt = fmt.replace(ret[1], (ret[1].length == 1) ? (opt[k]) : (opt[k].padStart(ret[1].length, "0")))
        }
    }
    return fmt;
}
function printDeal(){
    $("#printf").empty();
    var printBoxs = document.getElementsByName('printTable');
    var newContent = '';
    for(var i=0;i<printBoxs.length;i++) {
        newContent += printBoxs[i].innerHTML;
    }
    var f = document.getElementById('printf');
    // f.contentDocument.write('<link rel="stylesheet" type="text/css" href="/css/app.v2.css">');
    f.contentDocument.write('<style>' +
        'table {' +
        'border-collapse:collapse;' +
        '}' +
        'td {\n' +
        '        border: 1px solid #000000;\n' +
        '    }'+
        '</style>');
    f.contentDocument.write(newContent);
    f.contentDocument.close();
    window.frames['printf'].focus();
    try{
        window.frames['printf'].print();
    }catch(err){
        f.contentWindow.print();
    }
}
function getFormatDate() {
    var date = new Date();
    var month = date.getMonth() + 1;
    var strDate = date.getDate();
    if (month >= 1 && month <= 9) {
        month = "0" + month;
    }
    if (strDate >= 0 && strDate <= 9) {
        strDate = "0" + strDate;
    }
    var currentDate = date.getFullYear() + "-" + month + "-" + strDate;
    return currentDate;
}
