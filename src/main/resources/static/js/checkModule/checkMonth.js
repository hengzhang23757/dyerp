layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.6'
}).extend({                         // 模块别名
    soulTable: 'soulTable'
});
var dataTable;
var userRole = $("#userRole").val();
layui.use(['form', 'soulTable', 'table', 'upload','yutons_sug'], function () {
    var table = layui.table,
        form = layui.form,
        soulTable = layui.soulTable;
    dataTable = table.render({
        elem: '#dataTable'
        ,cols: [[]]
        ,loading:true
        ,data:[]
        ,height: 'full-80'
        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
        ,defaultToolbar: ['filter', 'print']
        ,title: '锁定月份'
        ,totalRow: true
        ,page: true
        ,even: true
        ,overflow: 'tips'
        ,limits: [50, 100, 200]
        ,limit: 100 //每页默认显示的数量
        ,done: function (res, curr, count) {
            soulTable.render(this);
        }
    });

    initTable();

    function initTable(){
        var load = layer.load();
        $.ajax({
            url: "/erp/getallcheckmonth",
            type: 'GET',
            data: {},
            success: function (res) {
                if (res.data) {
                    var reportData = res.data;
                    table.render({
                        elem: '#dataTable'
                        ,cols:[[
                            {type:'numbers', align:'center', title:'序号', width:60}
                            ,{field:'checkMonth', title:'已锁定月份', align:'center', width:270, sort: true, filter: true}
                            ,{field:'id', fixed: 'right', title:'操作', align:'center', toolbar: '#barTop', width:120}
                        ]]
                        ,data:reportData
                        ,height: 'full-80'
                        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                        ,title: '月份锁定'
                        ,totalRow: true
                        ,page: true
                        ,even: true
                        ,overflow: 'tips'
                        ,limits: [50, 100, 200]
                        ,limit: 100 //每页默认显示的数量
                        ,done: function (res, curr, count) {
                            soulTable.render(this);
                            layer.close(load);
                        }
                    });
                }
            }
        })
    }

    table.on('toolbar(dataTable)', function(obj) {
        if (obj.event === 'refresh') {
            initTable();
        } else if (obj.event === 'add') {
            var index = layer.open({
                type: 1 //Page层类型
                , title: '添加'
                , btn: ['添加','取消']
                , shade: 0.6 //遮罩透明度
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , area: ['500px', '300px']
                , content: $("#operateDiv")
                , yes: function (index, layero) {
                    // 通过form批量取值
                    var param = form.val("formBaseInfo");
                    if (param.checkMonth == null || param.checkMonth == ""){
                        layer.msg("请填写完整", { icon: 2 });
                        return false;
                    }
                    $.ajax({
                        url: "/erp/addcheckmonth",
                        type: 'POST',
                        data: {
                            monthStr: $("#checkMonth").val()
                        },
                        success: function (res) {
                            if (res.result == 0){
                                layer.closeAll();
                                $("#operateDiv").find("input").val("");
                                initTable();
                                layer.msg("录入成功！", {icon: 1});
                            }
                        }, error: function () {
                            layer.msg("添加失败！", {icon: 2});
                        }
                    })
                }
                , cancel: function (i, layero) {
                    $("#operateDiv").find("input").val("");
                    layer.closeAll();
                }
            });
            form.render('select');
            layui.laydate.render({
                elem: '#checkMonth'
                ,type: 'month'
            });
            return false;
        }
    });

    //监听行工具事件
    table.on('tool(dataTable)', function(obj){
        var data = obj.data;
        if(obj.event === 'del'){
            layer.confirm('真的删除吗', function(index){
                $.ajax({
                    url: "/erp/deletecheckmonth",
                    type: 'POST',
                    data: {
                        id: data.id
                    },
                    success: function (res) {
                        if (res.result == 0) {
                            initTable();
                            layer.close(index);
                            layer.msg("删除成功！", {icon: 1});
                        } else {
                            layer.msg("删除失败！", {icon: 2});
                        }
                    },
                    error: function () {
                        layer.msg("删除失败！", {icon: 2});
                    }
                })

            });
        }
    })

});