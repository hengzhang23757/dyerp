var form;
var userName = $("#userName").val();
var userRole = $("#userRole").val();
layui.use('form', function(){
    form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
    form.render();
});
layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.21'
}).extend({                         // 模块别名
    soulTable: 'soulTable',
});
var fabricAccessoryTable;

$(document).ready(function () {
    // $Date = layui.laydate;
    // layui.laydate.render({
    //     elem: '#orderDate',
    //     trigger: 'click',
    //     value: getFormatDate()
    // });
    // layui.laydate.render({
    //     elem: '#deliveryDate',
    //     trigger: 'click'
    // });
    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumber", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });
    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "supplierName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",
            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'supplierName',
                    title: '供应商',
                    align: 'left'
                }, {
                    field: 'supplierFullName',
                    title: '供应商全称',
                    align: 'left'
                }, {
                    field: 'supplierAddress',
                    title: '供应商地址',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'supplierName',
                    field: 'supplierName'
                }, {
                    name: 'supplierFullName',
                    field: 'supplierFullName'
                }, {
                    name: 'supplierAddress',
                    field: 'supplierAddress'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getsupplierinfohint?subSupplierName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });
});
$(document).on('keydown', '.layui-table-edit', function (e) {
    var td = $(this).parent('td')
        ,tr = td.parent('tr')
        ,trs = tr.parent().parent().find('tr')
        ,tr_index = tr.index()
        ,td_index = td.index()
        ,td_last_index = tr.find('[data-edit="text"]:last').index()
        ,td_first_index = tr.find('[data-edit="text"]:first').index();
    switch (e.keyCode) {
        case 13:
            tr.next().find('td').eq(td_index).click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            return false;

        case 39:
            td.nextAll('[data-edit="text"]:first').click();
            if (td_index == td_last_index){
                tr.next().find('td').eq(td_first_index).click();
                if (tr_index == trs.length - 1){
                    trs.eq(0).find('td').eq(td_first_index).click();
                }
            }
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
        case 37:
            td.prevAll('[data-edit="text"]:first').click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
        case 38:
            tr.prev().find('td').eq(td_index).click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
        case 40:
            tr.next().find('td').eq(td_index).click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
    }

})
layui.use(['form', 'soulTable', 'table'], function () {
    var table = layui.table,
        soulTable = layui.soulTable,
        form = layui.form,
        $ = layui.$;

    fabricAccessoryTable = table.render({
        id:'fabricAccessoryTable'
        ,elem: '#fabricAccessoryTable'
        ,cols:[[]]
        ,data:[]
        ,loading:false
        ,height: 'full-130'
        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
        ,page: true
        ,title: '面辅料审核数据'
        ,totalRow: true
        ,even: true
        ,limit:50
        ,limits:[50, 100, 200]
        ,done: function (res, curr, count) {
            soulTable.render(this);
        }
    });
    initTable('','面料', '已提交');
    function initTable(orderName, type, preCheck){
        var load = layer.load();
        var param = {};
        if (orderName != null && orderName !== ''){
            param.orderName = orderName;
        }
        if (preCheck != null && preCheck !== ''){
            param.preCheck = preCheck;
        }
        if (type === '面料'){
            $.ajax({
                url: "/erp/getfabricreviewdata",
                type: 'GET',
                data: param,
                success: function (res) {
                    if (res.data) {
                        var reportData = res.data;
                        console.log(reportData)
                        fabricAccessoryTable = table.render({
                            elem: '#fabricAccessoryTable'
                            ,cols:[[
                                {type:'numbers', align:'center', title:'序号', minWidth:60}
                                ,{field: 'reviewType', title: '类别',align:'center', minWidth: 90, sort: true, filter: true,hide:true}
                                ,{field: 'orderName', title: '款号',align:'center', minWidth: 200, sort: true, filter: true}
                                ,{field: 'clothesVersionNumber', title: '单号',align:'center', minWidth: 200, sort: true, filter: true, hide:true}
                                ,{field: 'materialName', title: '名称',align:'center', minWidth: 200, sort: true, filter: true}
                                ,{field: 'supplier', title: '供应商',align:'center', minWidth: 100, sort: true, filter: true}
                                ,{field: 'checkNumber', title: '合同号',align:'center', minWidth: 150, sort: true, filter: true}
                                ,{field: 'checkDate', title: '日期',align:'center', minWidth: 120, sort: true, filter: true}
                                ,{field: 'preCheck', title: '预审核',align:'center', minWidth: 100, sort: true, filter: true}
                                ,{field: 'preCheckEmp', title: '预审人',align:'center', minWidth: 100, sort: true, filter: true}
                                ,{field: 'preCheckTime', title: '预审时间',align:'center', minWidth: 100, sort: true, filter: true,templet:function (d) {
                                        if (d.preCheckTime == null){
                                            return ''
                                        }
                                        else {
                                            return moment(d.preCheckTime).format("YY-MM-DD");
                                        }
                                    }}
                                ,{field: 'checkState', title: '审核',align:'center', minWidth: 100, sort: true, filter: true}
                                ,{field: 'checkEmp', title: '审核人',align:'center', minWidth: 100, sort: true, filter: true}
                                ,{field: 'checkTime', title: '审核时间',align:'center', minWidth: 120, sort: true, filter: true,templet:function (d) {
                                        if (d.checkTime == null){
                                            return ''
                                        }
                                        else {
                                            return moment(d.checkTime).format("YY-MM-DD");
                                        }
                                    }}
                                ,{title:'操作', align:'center', toolbar: '#barTopFabric', minWidth:100, fixed:'right'}
                            ]]
                            ,loading:true
                            ,data:reportData
                            ,height: 'full-130'
                            ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                            ,title: '面辅料表'
                            ,totalRow: true
                            ,even: true
                            ,page: true
                            ,overflow: 'tips'
                            ,initSort:{field:'checkNumber', type:'desc'}
                            ,limit:50
                            ,limits:[50, 100, 200]
                            ,done: function (res, curr, count) {
                                soulTable.render(this);
                                layer.close(load);
                            }
                        });
                    }
                }
            });
        }
        else if (type === '辅料'){
            $.ajax({
                url: "/erp/getaccessoryreviewdata",
                type: 'GET',
                data: param,
                success: function (res) {
                    if (res.data) {
                        var reportData = res.data;
                        fabricAccessoryTable = table.render({
                            elem: '#fabricAccessoryTable'
                            ,cols:[[
                                {type:'numbers', align:'center', title:'序号', minWidth:60}
                                ,{field: 'reviewType', title: '类别',align:'center', minWidth: 90, sort: true, filter: true,hide:true}
                                ,{field: 'orderName', title: '款号',align:'center', minWidth: 200, sort: true, filter: true}
                                ,{field: 'clothesVersionNumber', title: '单号',align:'center', minWidth: 200, sort: true, filter: true, hide:true}
                                ,{field: 'customerName', title: '客户',align:'center', minWidth: 120, sort: true, filter: true}
                                ,{field: 'materialName', title: '名称',align:'center', minWidth: 200, sort: true, filter: true}
                                ,{field: 'supplier', title: '供应商',align:'center', minWidth: 100, sort: true, filter: true}
                                ,{field: 'checkNumber', title: '合同号',align:'center', minWidth: 150, sort: true, filter: true}
                                ,{field: 'checkDate', title: '日期',align:'center', minWidth: 120, sort: true, filter: true}
                                ,{field: 'preCheck', title: '预审核',align:'center', minWidth: 100, sort: true, filter: true}
                                ,{field: 'preCheckEmp', title: '预审人',align:'center', minWidth: 100, sort: true, filter: true}
                                ,{field: 'preCheckTime', title: '预审时间',align:'center', minWidth: 100, sort: true, filter: true,templet:function (d) {
                                        if (d.preCheckTime == null){
                                            return ''
                                        }
                                        else {
                                            return moment(d.preCheckTime).format("YY-MM-DD");
                                        }
                                    }}
                                ,{field: 'checkState', title: '审核',align:'center', minWidth: 100, sort: true, filter: true}
                                ,{field: 'checkEmp', title: '审核人',align:'center', minWidth: 100, sort: true, filter: true}
                                ,{field: 'checkTime', title: '审核时间',align:'center', minWidth: 120, sort: true, filter: true,templet:function (d) {
                                        if (d.checkTime == null){
                                            return ''
                                        }
                                        else {
                                            return moment(d.checkTime).format("YY-MM-DD");
                                        }
                                    }}
                                ,{title:'操作', align:'center', toolbar: '#barTopAccessory', minWidth:100, fixed:'right'}
                            ]]
                            ,loading:true
                            ,data:reportData
                            ,height: 'full-130'
                            ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                            ,title: '辅料审核表'
                            ,totalRow: true
                            ,even: true
                            ,page: true
                            ,overflow: 'tips'
                            ,limit:50
                            ,limits:[50, 100, 200]
                            ,done: function (res, curr, count) {
                                soulTable.render(this);
                                layer.close(load);
                            }
                        });
                    }
                }
            });
        }
        else if (type === '扁机'){
            $.ajax({
                url: "/erp/getbjreviewdata",
                type: 'GET',
                data: param,
                success: function (res) {
                    if (res.data) {
                        var reportData = res.data;
                        fabricAccessoryTable = table.render({
                            elem: '#fabricAccessoryTable'
                            ,cols:[[
                                {type:'numbers', align:'center', title:'序号', minWidth:60}
                                ,{field: 'reviewType', title: '类别',align:'center', minWidth: 90, sort: true, filter: true,hide:true}
                                ,{field: 'orderName', title: '款号',align:'center', minWidth: 200, sort: true, filter: true}
                                ,{field: 'clothesVersionNumber', title: '单号',align:'center', minWidth: 200, sort: true, filter: true, hide:true}
                                ,{field: 'customerName', title: '客户',align:'center', minWidth: 120, sort: true, filter: true}
                                ,{field: 'materialName', title: '名称',align:'center', minWidth: 200, sort: true, filter: true}
                                ,{field: 'supplier', title: '供应商',align:'center', minWidth: 100, sort: true, filter: true}
                                ,{field: 'checkNumber', title: '合同号',align:'center', minWidth: 150, sort: true, filter: true}
                                ,{field: 'checkDate', title: '日期',align:'center', minWidth: 120, sort: true, filter: true}
                                ,{field: 'preCheck', title: '预审核',align:'center', minWidth: 100, sort: true, filter: true}
                                ,{field: 'preCheckEmp', title: '预审人',align:'center', minWidth: 100, sort: true, filter: true}
                                ,{field: 'preCheckTime', title: '预审时间',align:'center', minWidth: 100, sort: true, filter: true,templet:function (d) {
                                        if (d.preCheckTime == null){
                                            return ''
                                        }
                                        else {
                                            return moment(d.preCheckTime).format("YY-MM-DD");
                                        }
                                    }}
                                ,{field: 'checkState', title: '审核',align:'center', minWidth: 100, sort: true, filter: true}
                                ,{field: 'checkEmp', title: '审核人',align:'center', minWidth: 100, sort: true, filter: true}
                                ,{field: 'checkTime', title: '审核时间',align:'center', minWidth: 120, sort: true, filter: true,templet:function (d) {
                                        if (d.checkTime == null){
                                            return ''
                                        }
                                        else {
                                            return moment(d.checkTime).format("YY-MM-DD");
                                        }
                                    }}
                                ,{title:'操作', align:'center', toolbar: '#barTopBj', minWidth:100, fixed:'right'}
                            ]]
                            ,loading:true
                            ,data:reportData
                            ,height: 'full-130'
                            ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                            ,title: '辅料审核表'
                            ,totalRow: true
                            ,even: true
                            ,page: true
                            ,overflow: 'tips'
                            ,limit:50
                            ,limits:[50, 100, 200]
                            ,done: function (res, curr, count) {
                                soulTable.render(this);
                                layer.close(load);
                            }
                        });
                    }
                }
            });
        }
        else if (type === '共用辅料'){
            $.ajax({
                url: "/erp/getaccessoryprestoreforcheck",
                type: 'GET',
                data: param,
                success: function (res) {
                    if (res.accessoryPreStoreList) {
                        var reportData = res.accessoryPreStoreList;
                        fabricAccessoryTable = table.render({
                            elem: '#fabricAccessoryTable'
                            ,cols:[[
                                {type:'numbers', align:'center', title:'序号', minWidth:60}
                                ,{field: 'reviewType', title: '类别',align:'center', minWidth: 90, sort: true, filter: true,hide:true}
                                ,{field: 'accessoryName', title: '名称',align:'center', minWidth: 120, sort: true, filter: true}
                                ,{field: 'accessoryNumber', title: '编号',align:'center', minWidth: 120, sort: true, filter: true}
                                ,{field: 'specification', title: '规格',align:'center', minWidth: 200, sort: true, filter: true}
                                ,{field: 'accessoryColor', title: '辅料颜色',align:'center', minWidth: 200, sort: true, filter: true, hide:true}
                                ,{field: 'supplier', title: '供应商',align:'center', minWidth: 200, sort: true, filter: true}
                                ,{field: 'checkNumber', title: '合同号',align:'center', minWidth: 150, sort: true, filter: true}
                                ,{field: 'preCheck', title: '预审核',align:'center', minWidth: 150, sort: true, filter: true}
                                ,{field: 'checkState', title: '审核',align:'center', minWidth: 100, sort: true, filter: true}
                                ,{title:'操作', align:'center', toolbar: '#barTopShareAccessory', minWidth:100, fixed:'right'}
                            ]]
                            ,loading:true
                            ,data:reportData
                            ,height: 'full-130'
                            ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                            ,title: '共用辅料审核表'
                            ,totalRow: true
                            ,even: true
                            ,page: true
                            ,overflow: 'tips'
                            ,limit:50
                            ,limits:[50, 100, 200]
                            ,done: function (res, curr, count) {
                                soulTable.render(this);
                                layer.close(load);
                            }
                        });
                    }
                }
            });
        }
    }
    function reloadTable(){
        var filterOrderName = $("#orderName").val();
        var preCheck = $("#preCheck").val();
        var type = $("#type").val();
        initTable(filterOrderName, type, preCheck);
        return false;
    }
    form.on('submit(searchBeat)', function(data){
        reloadTable();
        return false;
    });
    table.on('toolbar(fabricAccessoryTable)', function(obj){
        if (obj.event === 'clearFilter') {
            // 清除所有筛选条件并重载表格
            // 参数: tableId
            soulTable.clearFilter('fabricAccessoryTable')
        }
        else if (obj.event === 'exportExcel') {
            soulTable.export('fabricAccessoryTable');
        }
    });
    table.on('tool(fabricAccessoryTable)', function(obj){
        var data = obj.data;
        if (obj.event === 'reviewFabric') {
            if (userRole != "role10" && userRole != "root") {
                layer.msg("您没有权限！", {icon: 2});
                return false;
            }
            else {
                if (data.preCheck.indexOf("已提交") < 0) {
                    layer.msg('没有待审核的内容');
                }
                else {
                    var label = "物料审核, 类别:" + data.reviewType + "&emsp;&emsp;&emsp;&emsp; 合同号:" + data.checkNumber;
                    var index = layer.open({
                        type: 1 //Page层类型
                        , title: label
                        , btn: ['通过', '不通过']
                        , shade: 0.6 //遮罩透明度
                        , area: '1000px'
                        , maxmin: false //允许全屏最小化
                        , anim: 0 //0-6的动画形式，-1不开启
                        , content: "<div id=\"materialReview\">\n" +
                            "        <form class=\"layui-form\" style=\"margin: 0 auto;max-width:100%;padding-top: 20px;\">\n" +
                            "            <table class=\"layui-hide\" id=\"materialReviewTable\" lay-filter=\"materialReviewTable\"></table>\n" +
                            "        </form>\n" +
                            "    </div>"
                        , yes: function (i, layero) {
                            var materialData = table.cache.materialReviewTable;
                            var fabricIDList = [];
                            $.each(materialData, function (i, item) {
                                if (item.LAY_CHECKED) {
                                    fabricIDList.push(item.fabricID)
                                }
                            });
                            if (fabricIDList.length === 0){
                                layer.msg("请选择数据~~~");
                                return false;
                            }
                            $.ajax({
                                url: "/erp/changefabriccheckstateinplaceorderpre",
                                type: 'POST',
                                data: {
                                    fabricIDList: fabricIDList,
                                    preCheck: "通过",
                                    checkState: "已提交",
                                    userName: userName
                                },
                                traditional: true,
                                success: function (res) {
                                    if (res == 0) {
                                        reloadTable();
                                        layer.close(index);
                                        layer.msg("提交成功！", {icon: 1});
                                    } else {
                                        layer.msg("提交失败！", {icon: 2});
                                    }
                                }, error: function () {
                                    layer.msg("提交失败！", {icon: 2});
                                }
                            })
                        }
                        , btn2: function (i, layero) {
                            var materialData = table.cache.materialReviewTable;
                            var fabricIDList = [];
                            $.each(materialData, function (i, item) {
                                if (item.LAY_CHECKED) {
                                    fabricIDList.push(item.fabricID)
                                }
                            });
                            if (fabricIDList.length == 0) {
                                layer.msg("请先选中,后提交~~");
                                return false;
                            }

                            $.ajax({
                                url: "/erp/changefabriccheckstateinplaceorderpre",
                                type: 'POST',
                                data: {
                                    fabricIDList: fabricIDList,
                                    preCheck: "不通过",
                                    checkState: "未提交",
                                    userName: userName
                                },
                                traditional: true,
                                success: function (res) {
                                    if (res == 0) {
                                        reloadTable();
                                        layer.close(index);
                                        layer.msg("提交成功！", {icon: 1});
                                    } else {
                                        layer.msg("提交失败！", {icon: 2});
                                    }
                                }, error: function () {
                                    layer.msg("提交失败！", {icon: 2});
                                }
                            })
                        }
                    });
                    layer.full(index);
                    setTimeout(function () {
                        $.ajax({
                            url: "/erp/getreviewdatabytypechecknumber",
                            type: 'GET',
                            data: {
                                checkNumber: data.checkNumber,
                                reviewType: '面料'
                            },
                            success: function (res) {
                                var resultData = [];
                                var title = [
                                    {type: 'checkbox'},
                                    {
                                        field: 'orderName',
                                        title: '款号',
                                        align: 'center',
                                        minWidth: 140,
                                        sort: true,
                                        filter: true
                                    },
                                    {
                                        field: 'clothesVersionNumber',
                                        title: '单号',
                                        align: 'center',
                                        minWidth: 140,
                                        sort: true,
                                        filter: true
                                    },
                                    {
                                        field: 'fabricName',
                                        title: '名称',
                                        align: 'center',
                                        minWidth: 200,
                                        sort: true,
                                        filter: true
                                    },
                                    {
                                        field: 'unit',
                                        title: '单位',
                                        align: 'center',
                                        minWidth: 80,
                                        sort: true,
                                        filter: true
                                    },
                                    {
                                        field: 'partName',
                                        title: '部位',
                                        align: 'center',
                                        minWidth: 80,
                                        sort: true,
                                        filter: true
                                    },
                                    {
                                        field: 'colorName',
                                        title: '订单颜色',
                                        align: 'center',
                                        minWidth: 100,
                                        sort: true,
                                        filter: true
                                    },
                                    {
                                        field: 'fabricColor',
                                        title: '面料颜色',
                                        align: 'center',
                                        minWidth: 100,
                                        sort: true,
                                        filter: true
                                    },
                                    {field: 'supplier', title: '供应商', align: 'center', minWidth: 100},
                                    {field: 'orderPieceUsage', title: '客供用量', align: 'center', minWidth: 80},
                                    {field: 'pieceUsage', title: '单件用量', align: 'center', minWidth: 80},
                                    {
                                        field: 'orderCount',
                                        title: '订单量',
                                        align: 'center',
                                        minWidth: 130,
                                        fixed: 'right',
                                        totalRow: true
                                    },
                                    {
                                        field: 'fabricAdd',
                                        title: '上浮(%)',
                                        align: 'center',
                                        minWidth: 100,
                                        fixed: 'right'
                                    },
                                    {
                                        field: 'fabricCount',
                                        title: '订布量',
                                        align: 'center',
                                        minWidth: 130,
                                        fixed: 'right',
                                        totalRow: true
                                    },
                                    {
                                        field: 'fabricActCount',
                                        title: '实际订布',
                                        align: 'center',
                                        minWidth: 130,
                                        fixed: 'right',
                                        totalRow: true
                                    },
                                    {field: 'price', title: '单价', align: 'center', minWidth: 130, fixed: 'right'},
                                    {
                                        field: 'sumMoney',
                                        title: '总价',
                                        align: 'center',
                                        minWidth: 130,
                                        fixed: 'right',
                                        totalRow: true
                                    },
                                    {field: 'fabricID', title: 'fabricID', hide: true}
                                ];
                                if (res.data) {
                                    $.each(res.data, function (index1, value1) {
                                        var thisFabricData = {};
                                        thisFabricData.fabricID = value1.fabricID;
                                        thisFabricData.orderName = value1.orderName;
                                        thisFabricData.clothesVersionNumber = value1.clothesVersionNumber;
                                        thisFabricData.fabricName = value1.fabricName;
                                        thisFabricData.unit = value1.unit;
                                        thisFabricData.partName = value1.partName;
                                        thisFabricData.colorName = value1.colorName;
                                        thisFabricData.isHit = value1.isHit;
                                        thisFabricData.fabricColor = value1.fabricColor;
                                        thisFabricData.supplier = value1.supplier;
                                        thisFabricData.orderPieceUsage = value1.orderPieceUsage;
                                        thisFabricData.pieceUsage = value1.pieceUsage;
                                        thisFabricData.orderCount = value1.orderCount;
                                        thisFabricData.fabricAdd = value1.fabricAdd;
                                        thisFabricData.fabricCount = value1.fabricCount;
                                        thisFabricData.fabricActCount = value1.fabricActCount;
                                        thisFabricData.price = value1.price;
                                        thisFabricData.sumMoney = value1.sumMoney;
                                        resultData.push(thisFabricData);
                                    });
                                } else {
                                    layer.msg("面料信息未录入或者全部已经提交审核");
                                    layer.close(index);
                                }
                                table.render({
                                    elem: '#materialReviewTable'
                                    , cols: [title]
                                    , data: resultData
                                    , height: 'full-150'
                                    , limit: 1000
                                    , totalRow: true
                                    , limits: [1000, 2000, 3000]
                                    , filter: {
                                        cache: false
                                    }
                                    , done: function (res) {

                                        soulTable.render(this);
                                    }
                                });
                            },
                            error: function () {
                                layer.msg("未录入面料信息！", {icon: 2});
                            }
                        })
                    }, 100);
                }
            }
        }
        else if (obj.event === 'reviewAccessory'){
            if (userRole != "role10" && userRole != "root") {
                layer.msg("您没有权限！", {icon: 2});
                return false;
            }
            else {
                if (data.preCheck.indexOf("已提交") < 0) {
                    layer.msg('没有待审核的内容');
                }
                else {
                    var label = "物料审核, 类别:" + data.reviewType + "&emsp;&emsp;&emsp;&emsp; 合同号:" + data.checkNumber;
                    var index = layer.open({
                        type: 1 //Page层类型
                        , title: label
                        , btn: ['通过','不通过']
                        , shade: 0.6 //遮罩透明度
                        , area: '1000px'
                        , maxmin: false //允许全屏最小化
                        , anim: 0 //0-6的动画形式，-1不开启
                        , content: "<div id=\"materialReview\">\n" +
                            "        <form class=\"layui-form\" style=\"margin: 0 auto;max-width:100%;padding-top: 20px;\">\n" +
                            "            <table class=\"layui-hide\" id=\"materialReviewTable\" lay-filter=\"materialReviewTable\"></table>\n" +
                            "        </form>\n" +
                            "    </div>"
                        ,yes: function(i, layero){
                            var materialData = table.cache.materialReviewTable;
                            var accessoryIDList = [];
                            $.each(materialData,function(i,item){
                                if (item.LAY_CHECKED){
                                    accessoryIDList.push(item.accessoryID)
                                }
                            });
                            if (accessoryIDList.length === 0){
                                layer.msg("请选择数据~~~");
                                return false;
                            }
                            $.ajax({
                                url: "/erp/changeaccessorycheckstateinplaceorderpre",
                                type: 'POST',
                                data: {
                                    accessoryIDList:accessoryIDList,
                                    preCheck:"通过",
                                    checkState:"已提交",
                                    userName: userName
                                },
                                traditional: true,
                                success: function (res) {
                                    if (res == 0) {
                                        reloadTable();
                                        layer.close(index);
                                        layer.msg("提交成功！", {icon: 1});
                                    } else {
                                        layer.msg("提交失败！", {icon: 2});
                                    }
                                }, error: function () {
                                    layer.msg("提交失败！", {icon: 2});
                                }
                            })
                        }
                        ,btn2: function(i, layero){
                            var materialData = table.cache.materialReviewTable;
                            var accessoryIDList = [];
                            $.each(materialData,function(i,item){
                                if (item.LAY_CHECKED){
                                    accessoryIDList.push(item.accessoryID)
                                }
                            });

                            if (accessoryIDList.length == 0){
                                layer.msg("请先选中,后提交~~");
                                return false;
                            }
                            $.ajax({
                                url: "/erp/changeaccessorycheckstateinplaceorderpre",
                                type: 'POST',
                                data: {
                                    accessoryIDList:accessoryIDList,
                                    preCheck:"不通过",
                                    checkState:"未提交",
                                    userName: userName
                                },
                                traditional: true,
                                success: function (res) {
                                    if (res.result == 0) {
                                        reloadTable();
                                        layer.close(index);
                                        layer.msg("提交成功！", {icon: 1});
                                    } else {
                                        layer.msg("提交失败！", {icon: 2});
                                    }
                                }, error: function () {
                                    layer.msg("提交失败！", {icon: 2});
                                }
                            })
                        }
                    });
                    layer.full(index);
                    setTimeout(function () {
                        $.ajax({
                            url: "/erp/getreviewdatabytypechecknumber",
                            type: 'GET',
                            data: {
                                checkNumber: data.checkNumber,
                                reviewType: '辅料'
                            },
                            success: function (res) {
                                var resultData = [];
                                var title = [
                                    {type:'checkbox'},
                                    {field: 'orderName', title: '款号',align:'center',minWidth:120, sort: true, filter: true},
                                    {field: 'clothesVersionNumber', title: '单号',align:'center',minWidth:120, sort: true, filter: true},
                                    {field: 'accessoryName', title: '辅料名称',align:'center',minWidth:100, sort: true, filter: true},
                                    {field: 'accessoryNumber', title: '编号',align:'center',minWidth:80, sort: true, filter: true},
                                    {field: 'specification', title: '辅料规格',align:'center',minWidth:80, sort: true, filter: true},
                                    {field: 'accessoryUnit', title: '单位',align:'center',minWidth:80, sort: true, filter: true},
                                    {field: 'accessoryColor', title: '辅料颜色',align:'center',minWidth:80, sort: true, filter: true},
                                    {field: 'orderPieceUsage', title: '客供用量',align:'center',minWidth:80},
                                    {field: 'pieceUsage', title: '单件用量',align:'center',minWidth:80},
                                    {field: 'sizeName', title: '尺码',align:'center',minWidth:100},
                                    {field: 'colorName', title: '颜色',align:'center',minWidth:120},
                                    {field: 'supplier', title: '供应商',align:'center',minWidth:100, sort: true, filter: true},
                                    {field: 'orderCount', title: '订单量',align:'center',minWidth:80, fixed: 'right', totalRow:true},
                                    {field: 'accessoryAdd', title: '上浮(%)',align:'center',minWidth:80, fixed: 'right'},
                                    {field: 'orderAccessoryCount', title: '客供总量',align:'center',minWidth:90, fixed: 'right', totalRow:true},
                                    {field: 'accessoryCount', title: '订购量',align:'center',minWidth:90, fixed: 'right', totalRow:true},
                                    {field: 'orderPrice', title: '客供单价',align:'center',minWidth:100, fixed: 'right'},
                                    {field: 'price', title: '单价',align:'center',minWidth:80, fixed: 'right'},
                                    {field: 'orderSumMoney', title: '客供总价',align:'center',minWidth:100, fixed: 'right'},
                                    {field: 'sumMoney', title: '总价',align:'center',minWidth:80, fixed: 'right', totalRow:true},
                                    {field: 'addFee', title: '附加费',align:'center',minWidth:80, fixed: 'right', totalRow:true},
                                    {field: 'addRemark', title: '备注',align:'center',minWidth:100, fixed: 'right'},
                                    {field: 'accessoryID', title: 'accessoryID',hide:true}
                                ];
                                if (res.data){
                                    $.each(res.data,function(index1,value1){
                                        var thisAccessoryData = {};
                                        thisAccessoryData.accessoryID = value1.accessoryID;
                                        thisAccessoryData.orderName = value1.orderName;
                                        thisAccessoryData.clothesVersionNumber = value1.clothesVersionNumber;
                                        thisAccessoryData.accessoryName = value1.accessoryName;
                                        thisAccessoryData.accessoryNumber = value1.accessoryNumber;
                                        thisAccessoryData.specification = value1.specification;
                                        thisAccessoryData.accessoryUnit = value1.accessoryUnit;
                                        thisAccessoryData.accessoryColor = value1.accessoryColor;
                                        thisAccessoryData.orderPieceUsage = value1.orderPieceUsage;
                                        thisAccessoryData.pieceUsage = value1.pieceUsage;
                                        thisAccessoryData.sizeName = value1.sizeName;
                                        thisAccessoryData.colorName = value1.colorName;
                                        thisAccessoryData.supplier = value1.supplier;
                                        thisAccessoryData.orderCount = value1.orderCount;
                                        thisAccessoryData.accessoryAdd = value1.accessoryAdd;
                                        thisAccessoryData.accessoryCount = value1.accessoryCount;
                                        thisAccessoryData.orderAccessoryCount = toDecimal(value1.orderCount * value1.orderPieceUsage);
                                        thisAccessoryData.price = value1.price;
                                        thisAccessoryData.orderPrice = value1.orderPrice;
                                        thisAccessoryData.sumMoney = toDecimal(value1.price * value1.accessoryCount);
                                        thisAccessoryData.addFee = value1.addFee;
                                        thisAccessoryData.addRemark = value1.addRemark;
                                        thisAccessoryData.orderSumMoney = toDecimal(value1.orderPrice * thisAccessoryData.orderAccessoryCount);
                                        resultData.push(thisAccessoryData);
                                    });
                                } else{
                                    layer.msg("面料信息未录入或者全部已经提交审核");
                                    layer.close(index);
                                }
                                table.render({
                                    elem: '#materialReviewTable'
                                    ,cols: [title]
                                    ,data: resultData
                                    ,height: 'full-150'
                                    ,limit: 1000
                                    , totalRow:true
                                    ,limits: [1000, 2000, 3000]
                                    ,filter: {
                                        cache: false
                                    }
                                    ,done:function (res) {

                                        soulTable.render(this);
                                    }
                                });
                            },
                            error: function () {
                                layer.msg("未录入面料信息！", {icon: 2});
                            }
                        })
                    },100);
                }
            }
        }
        else if (obj.event === 'reviewBj'){
            if (userRole != "role10" && userRole != "root") {
                layer.msg("您没有权限！", {icon: 2});
                return false;
            }
            else {
                if (data.preCheck.indexOf("已提交") < 0) {
                    layer.msg('没有待审核的内容');
                }
                else {
                    var label = "物料审核, 类别:" + data.reviewType + "&emsp;&emsp;&emsp;&emsp; 合同号:" + data.checkNumber;
                    var index = layer.open({
                        type: 1 //Page层类型
                        , title: label
                        , btn: ['通过','不通过']
                        , shade: 0.6 //遮罩透明度
                        , area: '1000px'
                        , maxmin: false //允许全屏最小化
                        , anim: 0 //0-6的动画形式，-1不开启
                        , content: "<div id=\"materialReview\">\n" +
                            "        <form class=\"layui-form\" style=\"margin: 0 auto;max-width:100%;padding-top: 20px;\">\n" +
                            "            <table class=\"layui-hide\" id=\"materialReviewTable\" lay-filter=\"materialReviewTable\"></table>\n" +
                            "        </form>\n" +
                            "    </div>"
                        ,yes: function(i, layero){
                            var materialData = table.cache.materialReviewTable;
                            var accessoryIDList = [];
                            $.each(materialData,function(i,item){
                                if (item.LAY_CHECKED){
                                    accessoryIDList.push(item.accessoryID)
                                }
                            });
                            if (accessoryIDList.length === 0){
                                layer.msg("请选择数据~~~");
                                return false;
                            }
                            $.ajax({
                                url: "/erp/changeaccessorycheckstateinplaceorderpre",
                                type: 'POST',
                                data: {
                                    accessoryIDList:accessoryIDList,
                                    preCheck:"通过",
                                    checkState:"已提交",
                                    userName: userName
                                },
                                traditional: true,
                                success: function (res) {
                                    if (res == 0) {
                                        reloadTable();
                                        layer.close(index);
                                        layer.msg("提交成功！", {icon: 1});
                                    } else {
                                        layer.msg("提交失败！", {icon: 2});
                                    }
                                }, error: function () {
                                    layer.msg("提交失败！", {icon: 2});
                                }
                            })
                        }
                        ,btn2: function(i, layero){
                            var materialData = table.cache.materialReviewTable;
                            var accessoryIDList = [];
                            $.each(materialData,function(i,item){
                                if (item.LAY_CHECKED){
                                    accessoryIDList.push(item.accessoryID)
                                }
                            });

                            if (accessoryIDList.length == 0){
                                layer.msg("请先选中,后提交~~");
                                return false;
                            }
                            $.ajax({
                                url: "/erp/changeaccessorycheckstateinplaceorderpre",
                                type: 'POST',
                                data: {
                                    accessoryIDList:accessoryIDList,
                                    preCheck:"不通过",
                                    checkState:"未提交",
                                    userName: userName
                                },
                                traditional: true,
                                success: function (res) {
                                    if (res.result == 0) {
                                        reloadTable();
                                        layer.close(index);
                                        layer.msg("提交成功！", {icon: 1});
                                    } else {
                                        layer.msg("提交失败！", {icon: 2});
                                    }
                                }, error: function () {
                                    layer.msg("提交失败！", {icon: 2});
                                }
                            })
                        }
                    });
                    layer.full(index);
                    setTimeout(function () {
                        $.ajax({
                            url: "/erp/getreviewdatabytypechecknumber",
                            type: 'GET',
                            data: {
                                checkNumber: data.checkNumber,
                                reviewType: '辅料'
                            },
                            success: function (res) {
                                var resultData = [];
                                var title = [
                                    {type:'checkbox'},
                                    {field: 'orderName', title: '款号',align:'center',minWidth:120, sort: true, filter: true},
                                    {field: 'clothesVersionNumber', title: '单号',align:'center',minWidth:120, sort: true, filter: true},
                                    {field: 'accessoryName', title: '辅料名称',align:'center',minWidth:100, sort: true, filter: true},
                                    {field: 'accessoryNumber', title: '编号',align:'center',minWidth:80, sort: true, filter: true},
                                    {field: 'specification', title: '辅料规格',align:'center',minWidth:80, sort: true, filter: true},
                                    {field: 'accessoryUnit', title: '单位',align:'center',minWidth:80, sort: true, filter: true},
                                    {field: 'accessoryColor', title: '辅料颜色',align:'center',minWidth:80, sort: true, filter: true},
                                    {field: 'orderPieceUsage', title: '客供用量',align:'center',minWidth:80},
                                    {field: 'pieceUsage', title: '单件用量',align:'center',minWidth:80},
                                    {field: 'sizeName', title: '尺码',align:'center',minWidth:100},
                                    {field: 'colorName', title: '颜色',align:'center',minWidth:120},
                                    {field: 'supplier', title: '供应商',align:'center',minWidth:100, sort: true, filter: true},
                                    {field: 'orderCount', title: '订单量',align:'center',minWidth:80, fixed: 'right', totalRow:true},
                                    {field: 'accessoryAdd', title: '上浮(%)',align:'center',minWidth:80, fixed: 'right'},
                                    {field: 'accessoryCount', title: '订购量',align:'center',minWidth:90, fixed: 'right', totalRow:true},
                                    {field: 'orderPrice', title: '客供单价',align:'center',minWidth:100, fixed: 'right'},
                                    {field: 'orderSumMoney', title: '客供总价',align:'center',minWidth:100, fixed: 'right'},
                                    {field: 'price', title: '单价',align:'center',minWidth:80, fixed: 'right'},
                                    {field: 'sumMoney', title: '总价',align:'center',minWidth:80, fixed: 'right', totalRow:true},
                                    {field: 'addFee', title: '附加费',align:'center',minWidth:80, fixed: 'right', totalRow:true},
                                    {field: 'addRemark', title: '备注',align:'center',minWidth:100, fixed: 'right'},
                                    {field: 'accessoryID', title: 'accessoryID',hide:true}
                                ];
                                if (res.data){
                                    $.each(res.data,function(index1,value1){
                                        var thisAccessoryData = {};
                                        thisAccessoryData.accessoryID = value1.accessoryID;
                                        thisAccessoryData.orderName = value1.orderName;
                                        thisAccessoryData.clothesVersionNumber = value1.clothesVersionNumber;
                                        thisAccessoryData.accessoryName = value1.accessoryName;
                                        thisAccessoryData.accessoryNumber = value1.accessoryNumber;
                                        thisAccessoryData.specification = value1.specification;
                                        thisAccessoryData.accessoryUnit = value1.accessoryUnit;
                                        thisAccessoryData.accessoryColor = value1.accessoryColor;
                                        thisAccessoryData.orderPieceUsage = value1.orderPieceUsage;
                                        thisAccessoryData.pieceUsage = value1.pieceUsage;
                                        thisAccessoryData.sizeName = value1.sizeName;
                                        thisAccessoryData.colorName = value1.colorName;
                                        thisAccessoryData.supplier = value1.supplier;
                                        thisAccessoryData.orderCount = value1.orderCount;
                                        thisAccessoryData.accessoryAdd = value1.accessoryAdd;
                                        thisAccessoryData.accessoryCount = value1.accessoryCount;
                                        thisAccessoryData.price = value1.price;
                                        thisAccessoryData.orderPrice = value1.orderPrice;
                                        thisAccessoryData.sumMoney = value1.sumMoney;
                                        thisAccessoryData.addFee = value1.addFee;
                                        thisAccessoryData.addRemark = value1.addRemark;
                                        thisAccessoryData.orderSumMoney = toDecimal(value1.orderPrice * value1.orderCount);
                                        resultData.push(thisAccessoryData);
                                    });
                                } else{
                                    layer.msg("面料信息未录入或者全部已经提交审核");
                                    layer.close(index);
                                }
                                table.render({
                                    elem: '#materialReviewTable'
                                    ,cols: [title]
                                    ,data: resultData
                                    ,height: 'full-150'
                                    ,limit: 1000
                                    , totalRow:true
                                    ,limits: [1000, 2000, 3000]
                                    ,filter: {
                                        cache: false
                                    }
                                    ,done:function (res) {

                                        soulTable.render(this);
                                    }
                                });
                            },
                            error: function () {
                                layer.msg("未录入面料信息！", {icon: 2});
                            }
                        })
                    },100);
                }
            }
        }
        else if (obj.event === 'reviewShareAccessory'){
            if (userRole != "role10" && userRole != "root"){
                layer.msg("您没有权限！", {icon: 2});
                return false;
            } else {
                if(data.preCheck.indexOf("已提交") < 0) {
                    layer.msg('没有待审核的内容');
                }
                else {
                    var label = "物料审核, 类别:" + data.reviewType + "&emsp;&emsp;&emsp;&emsp; 合同号:" + data.checkNumber;
                    var index = layer.open({
                        type: 1 //Page层类型
                        , title: label
                        , btn: ['通过','不通过']
                        , shade: 0.6 //遮罩透明度
                        , area: '1000px'
                        , maxmin: false //允许全屏最小化
                        , anim: 0 //0-6的动画形式，-1不开启
                        , content: "<div id=\"materialReview\">\n" +
                            "        <form class=\"layui-form\" style=\"margin: 0 auto;max-width:100%;padding-top: 20px;\">\n" +
                            "            <table class=\"layui-hide\" id=\"materialReviewTable\" lay-filter=\"materialReviewTable\"></table>\n" +
                            "        </form>\n" +
                            "    </div>"
                        ,yes: function(i, layero){
                            var materialData = table.cache.materialReviewTable;
                            var accessoryPreStoreList = [];
                            $.each(materialData,function(i,item){
                                accessoryPreStoreList.push({id: item.id, preCheck: "通过", checkState: "已提交"});
                            });
                            if (accessoryPreStoreList.length === 0){
                                layer.msg("请选择数据~~~");
                                return false;
                            }
                            $.ajax({
                                url: "/erp/changeaccessoryprestoreincheck",
                                type: 'POST',
                                data: {
                                    accessoryPreStoreJson: JSON.stringify(accessoryPreStoreList)
                                },
                                success: function (res) {
                                    if (res.result == 0) {
                                        reloadTable();
                                        layer.close(index);
                                        layer.msg("提交成功！", {icon: 1});
                                    } else {
                                        layer.msg("提交失败！", {icon: 2});
                                    }
                                }, error: function () {
                                    layer.msg("提交失败！", {icon: 2});
                                }
                            });
                        }
                        ,btn2: function(i, layero){
                            var materialData = table.cache.materialReviewTable;
                            var accessoryPreStoreList = [];
                            $.each(materialData,function(i,item){
                                accessoryPreStoreList.push({id: item.id, preCheck: "不通过", checkState: "未提交"});
                            });
                            $.ajax({
                                url: "/erp/changeaccessoryprestoreincheck",
                                type: 'POST',
                                data: {
                                    accessoryPreStoreJson: JSON.stringify(accessoryPreStoreList)
                                },
                                success: function (res) {
                                    if (res.result == 0) {
                                        reloadTable();
                                        layer.msg("提交成功！", {icon: 1});
                                    } else {
                                        layer.msg("提交失败！", {icon: 2});
                                    }
                                }, error: function () {
                                    layer.msg("提交失败！", {icon: 2});
                                }
                            });
                        }
                    });
                    layer.full(index);
                    setTimeout(function () {
                        $.ajax({
                            url: "/erp/getaccessoryprestorebychecknumber",
                            type: 'GET',
                            data: {
                                checkNumber: data.checkNumber
                            },
                            success: function (res) {
                                var resultData = res.accessoryPreStoreList;
                                var title = [
                                    {field: 'accessoryName', title: '名称',align:'center',minWidth:140, sort: true, filter: true},
                                    {field: 'accessoryNumber', title: '编号',align:'center',minWidth:140, sort: true, filter: true},
                                    {field: 'specification', title: '规格',align:'center',minWidth:200, sort: true, filter: true},
                                    {field: 'accessoryColor', title: '颜色',align:'center',minWidth:80, sort: true, filter: true},
                                    {field: 'accessoryUnit', title: '单位',align:'center',minWidth:80, sort: true, filter: true},
                                    {field: 'supplier', title: '供应商',align:'center',minWidth:100, sort: true, filter: true},
                                    {field: 'price', title: '单价',align:'center',minWidth:100, sort: true, filter: true},
                                    {field: 'accessoryCount', title: '数量',align:'center',minWidth:100,totalRow:true},
                                    {field: 'sumMoney', title: '金额',align:'center',minWidth:80,totalRow:true},
                                    {field: 'id',hide:true}
                                ];
                                table.render({
                                    elem: '#materialReviewTable'
                                    ,cols: [title]
                                    ,data: resultData
                                    ,height: 'full-150'
                                    ,limit: 1000
                                    ,totalRow:true
                                    ,limits: [1000, 2000, 3000]
                                    ,done:function (res) {
                                        soulTable.render(this);
                                    }
                                });
                            },
                            error: function () {
                                layer.msg("未录入面料信息！", {icon: 2});
                            }
                        })
                    },100);
                }
            }
        }
        // if(obj.event === 'reviewCommit'){
        //     if (userRole != "role10" && userRole != "root"){
        //         layer.msg("您没有权限！", {icon: 2});
        //         return false;
        //     } else {
        //         if(data.checkState.indexOf("已提交") < 0) {
        //             layer.msg('没有待审核的内容');
        //         } else {
        //             var label = "物料审核, 类别:" + data.reviewType + "&emsp;&emsp;&emsp;&emsp; 合同号:" + data.checkNumber;
        //             var index = layer.open({
        //                 type: 1 //Page层类型
        //                 , title: label
        //                 , btn: ['通过','不通过']
        //                 , shade: 0.6 //遮罩透明度
        //                 , area: '1000px'
        //                 , maxmin: false //允许全屏最小化
        //                 , anim: 0 //0-6的动画形式，-1不开启
        //                 , content: "<div id=\"materialReview\">\n" +
        //                     "        <form class=\"layui-form\" style=\"margin: 0 auto;max-width:100%;padding-top: 20px;\">\n" +
        //                     "            <table class=\"layui-hide\" id=\"materialReviewTable\" lay-filter=\"materialReviewTable\"></table>\n" +
        //                     "        </form>\n" +
        //                     "    </div>"
        //                 ,yes: function(i, layero){
        //                     if (data.reviewType === "面料"){
        //                         var materialData = table.cache.materialReviewTable;
        //                         var fabricIDList = [];
        //                         $.each(materialData,function(i,item){
        //                             if (item.LAY_CHECKED){
        //                                 fabricIDList.push(item.fabricID)
        //                             }
        //                         });
        //                         $.ajax({
        //                             url: "/erp/changefabriccheckstateinplaceorder",
        //                             type: 'POST',
        //                             data: {
        //                                 fabricIDList:fabricIDList,
        //                                 checkState:"通过"
        //                             },
        //                             traditional: true,
        //                             success: function (res) {
        //                                 if (res == 0) {
        //                                     initTable('','','',1);
        //                                     reloadInfo();
        //                                     layer.close(index);
        //                                     layer.msg("提交成功！", {icon: 1});
        //                                 } else {
        //                                     layer.msg("提交失败！", {icon: 2});
        //                                 }
        //                             }, error: function () {
        //                                 layer.msg("提交失败！", {icon: 2});
        //                             }
        //                         })
        //                     }
        //                     else if (data.reviewType === "辅料"){
        //                         var materialData = table.cache.materialReviewTable;
        //                         var accessoryIDList = [];
        //                         $.each(materialData,function(i,item){
        //                             if (item.LAY_CHECKED){
        //                                 accessoryIDList.push(item.accessoryID)
        //                             }
        //                         });
        //                         $.ajax({
        //                             url: "/erp/changeaccessorycheckstateinplaceorder",
        //                             type: 'POST',
        //                             data: {
        //                                 accessoryIDList:accessoryIDList,
        //                                 checkState:"通过"
        //                             },
        //                             traditional: true,
        //                             success: function (res) {
        //                                 if (res == 0) {
        //                                     initTable('','','',2);
        //                                     reloadInfo();
        //                                     layer.close(index);
        //                                     layer.msg("提交成功！", {icon: 1});
        //                                 } else {
        //                                     layer.msg("提交失败！", {icon: 2});
        //                                 }
        //                             }, error: function () {
        //                                 layer.msg("提交失败！", {icon: 2});
        //                             }
        //                         })
        //                     }
        //                 }
        //                 ,btn2: function(i, layero){
        //                     if (data.reviewType === "面料"){
        //                         var materialData = table.cache.materialReviewTable;
        //                         var fabricIDList = [];
        //                         $.each(materialData,function(i,item){
        //                             if (item.LAY_CHECKED){
        //                                 fabricIDList.push(item.fabricID)
        //                             }
        //                         });
        //                         if (fabricIDList.length == 0){
        //                             layer.msg("请先选中,后提交~~");
        //                             return false;
        //                         }
        //
        //                         $.ajax({
        //                             url: "/erp/changefabriccheckstateinplaceorder",
        //                             type: 'POST',
        //                             data: {
        //                                 fabricIDList:fabricIDList,
        //                                 checkState:"不通过"
        //                             },
        //                             traditional: true,
        //                             success: function (res) {
        //                                 if (res == 0) {
        //                                     fabricAccessoryTable.reload();
        //                                     reloadInfo();
        //                                     layer.close(index);
        //                                     layer.msg("提交成功！", {icon: 1});
        //                                 } else {
        //                                     layer.msg("提交失败！", {icon: 2});
        //                                 }
        //                             }, error: function () {
        //                                 layer.msg("提交失败！", {icon: 2});
        //                             }
        //                         })
        //                     } else if (data.reviewType === "辅料"){
        //                         var materialData = table.cache.materialReviewTable;
        //                         var accessoryIDList = [];
        //                         $.each(materialData,function(i,item){
        //                             if (item.LAY_CHECKED){
        //                                 accessoryIDList.push(item.accessoryID)
        //                             }
        //                         });
        //
        //                         if (accessoryIDList.length == 0){
        //                             layer.msg("请先选中,后提交~~");
        //                             return false;
        //                         }
        //                         $.ajax({
        //                             url: "/erp/changeaccessorycheckstateinplaceorder",
        //                             type: 'POST',
        //                             data: {
        //                                 accessoryIDList:accessoryIDList,
        //                                 checkState:"不通过"
        //                             },
        //                             traditional: true,
        //                             success: function (res) {
        //                                 if (res.result == 0) {
        //                                     fabricAccessoryTable.reload();
        //                                     reloadInfo();
        //                                     layer.close(index);
        //                                     layer.msg("提交成功！", {icon: 1});
        //                                 } else {
        //                                     layer.msg("提交失败！", {icon: 2});
        //                                 }
        //                             }, error: function () {
        //                                 layer.msg("提交失败！", {icon: 2});
        //                             }
        //                         })
        //                     }
        //                 }
        //             });
        //             layer.full(index);
        //             setTimeout(function () {
        //                 $.ajax({
        //                     url: "/erp/getreviewdatabytypechecknumber",
        //                     type: 'GET',
        //                     data: {
        //                         checkNumber: data.checkNumber,
        //                         reviewType: data.reviewType
        //                     },
        //                     success: function (res) {
        //                         var resultData = [];
        //                         if (data.reviewType === "面料"){
        //                             var title = [
        //                                 {type:'checkbox'},
        //                                 {field: 'orderName', title: '款号',align:'center',minWidth:140, sort: true, filter: true},
        //                                 {field: 'clothesVersionNumber', title: '单号',align:'center',minWidth:140, sort: true, filter: true},
        //                                 {field: 'fabricName', title: '名称',align:'center',minWidth:200, sort: true, filter: true},
        //                                 {field: 'unit', title: '单位',align:'center',minWidth:80, sort: true, filter: true},
        //                                 {field: 'partName', title: '部位',align:'center',minWidth:80, sort: true, filter: true},
        //                                 {field: 'colorName', title: '订单颜色',align:'center',minWidth:100, sort: true, filter: true},
        //                                 {field: 'fabricColor', title: '面料颜色',align:'center',minWidth:100, sort: true, filter: true},
        //                                 {field: 'supplier', title: '供应商',align:'center',minWidth:100},
        //                                 {field: 'orderPieceUsage', title: '客供用量',align:'center',minWidth:80},
        //                                 {field: 'pieceUsage', title: '单件用量',align:'center',minWidth:80},
        //                                 {field: 'orderCount', title: '订单量',align:'center',minWidth:130, fixed: 'right', totalRow:true},
        //                                 {field: 'fabricAdd', title: '上浮(%)',align:'center',minWidth:100, fixed: 'right'},
        //                                 {field: 'fabricCount', title: '订布量',align:'center',minWidth:130, fixed: 'right', totalRow:true},
        //                                 {field: 'fabricActCount', title: '实际订布',align:'center',minWidth:130, fixed: 'right', totalRow:true},
        //                                 {field: 'price', title: '单价',align:'center',minWidth:130, fixed: 'right'},
        //                                 {field: 'sumMoney', title: '总价',align:'center',minWidth:130, fixed: 'right', totalRow:true},
        //                                 {field: 'fabricID', title: 'fabricID',hide:true}
        //                             ];
        //
        //                             if (res.data){
        //                                 $.each(res.data,function(index1,value1){
        //                                     var thisFabricData = {};
        //                                     thisFabricData.fabricID = value1.fabricID;
        //                                     thisFabricData.orderName = value1.orderName;
        //                                     thisFabricData.clothesVersionNumber = value1.clothesVersionNumber;
        //                                     thisFabricData.fabricName = value1.fabricName;
        //                                     thisFabricData.unit = value1.unit;
        //                                     thisFabricData.partName = value1.partName;
        //                                     thisFabricData.colorName = value1.colorName;
        //                                     thisFabricData.isHit = value1.isHit;
        //                                     thisFabricData.fabricColor = value1.fabricColor;
        //                                     thisFabricData.supplier = value1.supplier;
        //                                     thisFabricData.orderPieceUsage = value1.orderPieceUsage;
        //                                     thisFabricData.pieceUsage = value1.pieceUsage;
        //                                     thisFabricData.orderCount = value1.orderCount;
        //                                     thisFabricData.fabricAdd = value1.fabricAdd;
        //                                     thisFabricData.fabricCount = value1.fabricCount;
        //                                     thisFabricData.fabricActCount = value1.fabricActCount;
        //                                     thisFabricData.price = value1.price;
        //                                     thisFabricData.sumMoney = value1.sumMoney;
        //                                     resultData.push(thisFabricData);
        //                                 });
        //                             } else{
        //                                 layer.msg("面料信息未录入或者全部已经提交审核");
        //                                 layer.close(index);
        //                             }
        //                         }
        //                         else if (data.reviewType === "辅料"){
        //                             var title = [
        //                                 {type:'checkbox'},
        //                                 {field: 'orderName', title: '款号',align:'center',minWidth:120, sort: true, filter: true},
        //                                 {field: 'clothesVersionNumber', title: '单号',align:'center',minWidth:120, sort: true, filter: true},
        //                                 {field: 'accessoryName', title: '辅料名称',align:'center',minWidth:100, sort: true, filter: true},
        //                                 {field: 'accessoryNumber', title: '编号',align:'center',minWidth:80, sort: true, filter: true},
        //                                 {field: 'specification', title: '辅料规格',align:'center',minWidth:80, sort: true, filter: true},
        //                                 {field: 'accessoryUnit', title: '单位',align:'center',minWidth:80, sort: true, filter: true},
        //                                 {field: 'accessoryColor', title: '辅料颜色',align:'center',minWidth:80, sort: true, filter: true},
        //                                 {field: 'orderPieceUsage', title: '客供用量',align:'center',minWidth:80},
        //                                 {field: 'pieceUsage', title: '单件用量',align:'center',minWidth:80},
        //                                 {field: 'sizeName', title: '尺码',align:'center',minWidth:100},
        //                                 {field: 'colorName', title: '颜色',align:'center',minWidth:120},
        //                                 {field: 'supplier', title: '供应商',align:'center',minWidth:100, sort: true, filter: true},
        //                                 {field: 'orderCount', title: '订单量',align:'center',minWidth:80, fixed: 'right', totalRow:true},
        //                                 {field: 'accessoryAdd', title: '上浮(%)',align:'center',minWidth:80, fixed: 'right'},
        //                                 {field: 'accessoryCount', title: '订购量',align:'center',minWidth:90, fixed: 'right', totalRow:true},
        //                                 {field: 'orderPrice', title: '客供单价',align:'center',minWidth:100, fixed: 'right'},
        //                                 {field: 'orderSumMoney', title: '客供总价',align:'center',minWidth:100, fixed: 'right'},
        //                                 {field: 'price', title: '单价',align:'center',minWidth:80, fixed: 'right'},
        //                                 {field: 'sumMoney', title: '总价',align:'center',minWidth:80, fixed: 'right', totalRow:true},
        //                                 {field: 'addFee', title: '附加费',align:'center',minWidth:80, fixed: 'right', totalRow:true},
        //                                 {field: 'addRemark', title: '备注',align:'center',minWidth:100, fixed: 'right'},
        //                                 {field: 'accessoryID', title: 'accessoryID',hide:true}
        //                             ];
        //                             if (res.data){
        //                                 $.each(res.data,function(index1,value1){
        //                                     var thisAccessoryData = {};
        //                                     thisAccessoryData.accessoryID = value1.accessoryID;
        //                                     thisAccessoryData.orderName = value1.orderName;
        //                                     thisAccessoryData.clothesVersionNumber = value1.clothesVersionNumber;
        //                                     thisAccessoryData.accessoryName = value1.accessoryName;
        //                                     thisAccessoryData.accessoryNumber = value1.accessoryNumber;
        //                                     thisAccessoryData.specification = value1.specification;
        //                                     thisAccessoryData.accessoryUnit = value1.accessoryUnit;
        //                                     thisAccessoryData.accessoryColor = value1.accessoryColor;
        //                                     thisAccessoryData.orderPieceUsage = value1.orderPieceUsage;
        //                                     thisAccessoryData.pieceUsage = value1.pieceUsage;
        //                                     thisAccessoryData.sizeName = value1.sizeName;
        //                                     thisAccessoryData.colorName = value1.colorName;
        //                                     thisAccessoryData.supplier = value1.supplier;
        //                                     thisAccessoryData.orderCount = value1.orderCount;
        //                                     thisAccessoryData.accessoryAdd = value1.accessoryAdd;
        //                                     thisAccessoryData.accessoryCount = value1.accessoryCount;
        //                                     thisAccessoryData.price = value1.price;
        //                                     thisAccessoryData.orderPrice = value1.orderPrice;
        //                                     thisAccessoryData.sumMoney = value1.sumMoney;
        //                                     thisAccessoryData.addFee = value1.addFee;
        //                                     thisAccessoryData.addRemark = value1.addRemark;
        //                                     thisAccessoryData.orderSumMoney = toDecimal(value1.orderPrice * value1.orderCount);
        //                                     resultData.push(thisAccessoryData);
        //                                 });
        //                             } else{
        //                                 layer.msg("面料信息未录入或者全部已经提交审核");
        //                                 layer.close(index);
        //                             }
        //                         }
        //                         table.render({
        //                             elem: '#materialReviewTable'
        //                             ,cols: [title]
        //                             ,data: resultData
        //                             ,height: 'full-150'
        //                             ,limit: 1000
        //                             , totalRow:true
        //                             ,limits: [1000, 2000, 3000]
        //                             ,filter: {
        //                                 cache: false
        //                             }
        //                             ,done:function (res) {
        //
        //                                 soulTable.render(this);
        //                             }
        //                         });
        //                     },
        //                     error: function () {
        //                         layer.msg("未录入面料信息！", {icon: 2});
        //                     }
        //                 })
        //             },100);
        //         }
        //     }
        // }
        // else if (obj.event === 'exportCheck'){
        //     if(data.checkState === "通过") {
        //         var label = "物料审核, 类别:" + data.reviewType + "&emsp;&emsp;&emsp;&emsp; 合同号:" + data.checkNumber;
        //         var tableHtml = "";
        //         var orderDate = $("#orderDate").val();
        //         var deliveryDate = $("#deliveryDate").val();
        //         var partBName = $("#supplierFullName").val();
        //         var partBAddress = $("#supplierAddress").val();
        //         var supplierName = $("#supplierName").val();
        //         var index = layer.open({
        //             type: 1 //Page层类型
        //             , title: label
        //             , btn: ['导出/打印']
        //             , shade: 0.6 //遮罩透明度
        //             , area: '1000px'
        //             , maxmin: false //允许全屏最小化
        //             , anim: 0 //0-6的动画形式，-1不开启
        //             , content: "<div>\n" +
        //                 "        <div class=\"row\">\n" +
        //                 "            <div id=\"materialCheck\">\n" +
        //                 "            </div>\n" +
        //                 "        </div>\n" +
        //                 "        <iframe id=\"printf\" src=\"\" width=\"0\" height=\"0\" frameborder=\"0\"></iframe>\n" +
        //                 "    </div>"
        //             ,yes: function(i, layero){
        //                 printDeal();
        //                 $.ajax({
        //                     url: "/erp/addsupplierinfo",
        //                     type:'POST',
        //                     data: {
        //                         "supplierName": supplierName,
        //                         "supplierFullName": partBName,
        //                         "supplierAddress": partBAddress
        //                     },
        //                     success:function(data){
        //                     }, error:function(){
        //                     }
        //                 });
        //             }
        //         });
        //         layer.full(index);
        //         setTimeout(function () {
        //             $.ajax({
        //                 url: "/erp/getprintdatabytypechecknumber",
        //                 type: 'GET',
        //                 data: {
        //                     checkNumber: data.checkNumber,
        //                     reviewType: data.reviewType
        //                 },
        //                 success: function (res) {
        //                     if (data.reviewType === "面料"){
        //                         $("#materialCheck").empty();
        //                         var manufactureFabricList = res.data;
        //                         var tax = manufactureFabricList[0].tax;
        //                         tableHtml = "<div class=\"col-md-12\" name='printTable'>\n" +
        //                             "               <section class=\"panel panel-default\" style='page-break-after:always;'>\n" +
        //                             "                   <header class=\"panel-heading font-bold\" style=\"text-align: center\">\n" +
        //                             "                       <div style=\"font-size: 20px;font-weight: 700\">中山市德悦服饰有限公司</div><br>\n" +
        //                             "                       <div style=\"font-size: 16px;font-weight: 700\">购销合同&nbsp;&nbsp;&nbsp;&nbsp;合同编号: " + data.checkNumber + "&nbsp;&nbsp;&nbsp;&nbsp;含税:" + tax +"</div><br>\n" +
        //                             "                   </header>\n" +
        //                             "                   <table class=\"table table-bordered\" style='width: 100%'>\n" +
        //                             "                       <tbody>";
        //
        //                         tableHtml += "<tr><td colspan='7' style='text-align: left; font-weight: 700; color: black'>甲方(需方):中山市德悦服饰有限公司</td><td colspan='7' style='text-align: left; font-weight: 700; color: black'>签约地点:广东省中山市</td></tr>";
        //                         tableHtml += "<tr><td colspan='7' style='text-align: left; font-weight: 700; color: black'>乙方(供方):"+ partBName +"</td><td colspan='7' style='text-align: left; font-weight: 700; color: black'>签约时间:"+ orderDate +"</td></tr>";
        //                         tableHtml += "<tr><td colspan='14' style='text-align: center; font-weight: 700; color: black'>甲乙双方协商一致,签订以下协议,双方共同遵守</td></tr>";
        //                         tableHtml += "<tr><td colspan='14' style='text-align: left; font-weight: 700; color: black'>一、产品基本信息</td></tr>";
        //                         tableHtml += "<tr><td>单号</td><td>款号</td><td>面料名称</td><td>面料号</td><td>布封</td><td>克重</td><td>部位</td><td>色组</td><td>单件用量</td><td>单位</td><td>件数</td><td>订购量</td><td>单价</td><td>金额</td></tr>";
        //                         var pieceSum = 0;
        //                         var moneySum = 0;
        //                         for (var i=0;i<manufactureFabricList.length;i++){
        //                             tableHtml += "<tr><td>"+manufactureFabricList[i].clothesVersionNumber+"</td><td>"+manufactureFabricList[i].orderName+"</td><td>"+manufactureFabricList[i].fabricName+"</td><td>"+manufactureFabricList[i].fabricNumber+"</td><td>"+manufactureFabricList[i].fabricWidth+"</td><td>"+manufactureFabricList[i].fabricWeight+"</td><td>"+manufactureFabricList[i].partName+"</td><td>"+manufactureFabricList[i].colorName+"</td><td>"+manufactureFabricList[i].pieceUsage+"</td><td>"+manufactureFabricList[i].unit+"</td><td>"+manufactureFabricList[i].orderCount+"</td><td>"+manufactureFabricList[i].fabricActCount+"</td><td>"+toDecimal(manufactureFabricList[i].price)+"</td><td>"+toDecimal(manufactureFabricList[i].sumMoney)+"</td></tr>";
        //                             pieceSum += manufactureFabricList[i].fabricActCount;
        //                             moneySum += manufactureFabricList[i].sumMoney;
        //                         }
        //                         tableHtml += "<tr><td colspan='11' style='text-align:center'>小计：</td><td>"+toDecimal(pieceSum)+"</td><td></td><td>"+toDecimal(moneySum)+"</td></tr>";
        //                         tableHtml += "<tr><td colspan='3' style='text-align:center'>交货日期：</td><td colspan='11' style='text-align:center'>"+ deliveryDate +"</td></tr>";
        //                         tableHtml += "<tr><td colspan='3' style='text-align:center'>交货地址：</td><td colspan='11' style='text-align:center'>中山市沙溪镇康乐北路42号德悦服饰1楼面料仓</td></tr>";
        //                         tableHtml += "<tr><td colspan='2' rowspan='5' style='text-align:center'>备注</td><td colspan='12'>1.质量要求:质量要求：用料要环保，不能含致癌物质，品质跟样办。</td></tr>";
        //                         tableHtml += "<tr><td colspan='12'>2.包装要求：请务必在每袋或每捆上注明款号、规格、数量。</td></tr>";
        //                         tableHtml += "<tr><td colspan='12'>3.送货单：需注明采购单号及款号并注明产品名称、颜色、规格、数量。</td></tr>";
        //                         tableHtml += "<tr><td colspan='12'>4.供应商收到订单应第一时间回复本司货期，否则作默认同意，供应商必须保证产品的质量和货期，若产品质量不对办，我司将要求无偿更换或退货处理，如不能如期交货，请提前回复我司。</td></tr>";
        //                         tableHtml += "<tr><td colspan='12'>5.本合同需签字回传订单方可生效。。</td></tr>";
        //                         tableHtml += "<tr><td colspan='14' style='text-align: left; font-weight: 700; color: black'>二、其他条款</td></tr>";
        //                         tableHtml += "<tr><td colspan='14' style='text-align: left;'>1、乙方应严格按照《针织T恤衫》国家（一等品）标准，以及甲方订单要求进行加工生产，乙方交至甲方的产品，必须符合甲方的要求。</td></tr>";
        //                         tableHtml += "<tr><td colspan='14' style='text-align: left;'>2、乙方应于订单规定的时间内把甲方采购的（物料/布料）交至甲方指定地点，乙方承担运费及运输过程中的一切风险和法律责任。</td></tr>";
        //                         tableHtml += "<tr><td colspan='14' style='text-align: left;'>3、甲方对货物的验收仅是形式检验，其验收不及于物品的内在质量，乙方仍应对物品质量承担保证责任。如在生产或销售过程中，发现内在质量不合格，造成退货或其他损失，由乙方负全责。</td></tr>";
        //                         tableHtml += "<tr><td colspan='14' style='text-align: left;'>4、如乙方货物出现短缺、规格不符或质量问题等不符合合同约定情形的，甲方有权拒收并视为未交货，乙方应负责于规定时间内，免费更换合格的产品并运送至甲方，如出现特殊情况，需双方协商一致后再另行处理。</td></tr>";
        //                         tableHtml += "<tr><td colspan='14' style='text-align: left;'>5、乙方因生产进度、质量问题等导致交货延期或双方另行协商的货期内仍不能完成的,视为乙方违约,乙方应承担逾期交货违约责任，并按逾期交货金额1% 每日向甲方支付违约金，因此给甲方造成经济损失的，乙方还应予以赔偿。</td></tr>";
        //                         tableHtml += "<tr><td colspan='14' style='text-align: left;'>6、结算方式：月结 _____ 天。（乙方每月需将采购货物签收单原件交付甲方，如乙方代发货至第三方的，同样需将签收单原件交付甲方。）付款期限内，乙方开具13%的增值税发票或相关票据后，甲方付给乙方货款。</td></tr>";
        //                         tableHtml += "<tr><td colspan='14' style='text-align: left;'>7、在合同履行过程中，债权及债务不可转让给第三方。</td></tr>";
        //                         tableHtml += "<tr><td colspan='14' style='text-align: left;'>8、本合同未尽事宜，或在合同执行过程中出现的争执和异议，双方本着友好协商的态度共同解决，协商不成的，任何一方可向甲方所在地人民法院起诉。</td></tr>";
        //                         tableHtml += "<tr><td colspan='14' style='text-align: left;'>9、本合同一式两份，甲乙双方各执一份，具有同等法律效力。</td></tr>";
        //                         tableHtml += "<tr><td colspan='6' style='text-align: left; font-weight: 700; color: black'>甲方:中山市徳悦服饰有限公司</td><td colspan='8' style='text-align: left; font-weight: 700; color: black'>乙方:"+ partBName +"</td></tr>";
        //                         tableHtml += "<tr><td colspan='6' style='text-align: left; font-weight: 700; color: black'>单位地址:中山市沙溪镇康乐北路42号之1二楼</td><td colspan='8' style='text-align: left; font-weight: 700; color: black'>单位地址:"+ partBAddress +"</td></tr>";
        //                         tableHtml += "<tr><td colspan='6' style='text-align: left; font-weight: 700; color: black'>授权代表:</td><td colspan='8' style='text-align: left; font-weight: 700; color: black'>授权代表:</td></tr>";
        //                         tableHtml += "<tr><td colspan='6' style='text-align: left; font-weight: 700; color: black'>电话:</td><td colspan='8' style='text-align: left; font-weight: 700; color: black'>电话:</td></tr>";
        //                         tableHtml +=  "                       </tbody>" +
        //                             "                   </table>" +
        //                             "               </section>" +
        //                             "              </div>";
        //                         $("#materialCheck").append(tableHtml);
        //                     }
        //                     else if (data.reviewType === "辅料"){
        //                         if (res.data){
        //                             $("#materialCheck").empty();
        //                             var manufactureAccessoryList = res.data;
        //                             var tax = manufactureAccessoryList[0].tax;
        //                             tableHtml = "<div class=\"col-md-12\" name='printTable'>\n" +
        //                                 "               <section class=\"panel panel-default\" style='page-break-after:always;'>\n" +
        //                                 "                   <header class=\"panel-heading font-bold\" style=\"text-align: center\">\n" +
        //                                 "                       <div style=\"font-size: 20px;font-weight: 700\">中山市德悦服饰有限公司</div><br>\n" +
        //                                 "                       <div style=\"font-size: 16px;font-weight: 700\">购销合同&nbsp;&nbsp;&nbsp;&nbsp;合同编号: " + data.checkNumber +"&nbsp;&nbsp;&nbsp;&nbsp;含税:"+ tax +"</div><br>\n" +
        //                                 "                   </header>\n" +
        //                                 "                   <table class=\"table table-bordered\" style='width: 100%'>\n" +
        //                                 "                       <tbody>";
        //
        //                             tableHtml += "<tr><td colspan='6' style='text-align: left; font-weight: 700; color: black'>甲方(需方):中山市德悦服饰有限公司</td><td colspan='6' style='text-align: left; font-weight: 700; color: black'>签约地点:广东省中山市</td></tr>";
        //                             tableHtml += "<tr><td colspan='6' style='text-align: left; font-weight: 700; color: black'>乙方(供方):"+ partBName +"</td><td colspan='6' style='text-align: left; font-weight: 700; color: black'>签约时间:"+ orderDate +"</td></tr>";
        //                             tableHtml += "<tr><td colspan='12' style='text-align: center; font-weight: 700; color: black'>甲乙双方协商一致,签订以下协议,双方共同遵守</td></tr>";
        //                             tableHtml += "<tr><td colspan='12' style='text-align: left; font-weight: 700; color: black'>一、产品基本信息</td></tr>";
        //                             tableHtml += "<tr><td width='12%'>款号</td><td width='20%'>名称</td><td width='6%'>编号</td><td width='7%'>规格</td><td width='8%'>颜色</td><td width='7%'>尺码</td><td width='5%'>单位</td><td width='7%'>数量</td><td width='7%'>单价</td><td width='7%'>金额</td><td width='7%'>附加费</td><td width='7%'>备注</td></tr>";
        //                             var pieceSum = 0;
        //                             var moneySum = 0;
        //                             var feeSum = 0;
        //
        //                             for (var i=0;i<manufactureAccessoryList.length;i++){
        //                                 tableHtml += "<tr><td>"+manufactureAccessoryList[i].orderName+"</td><td>"+manufactureAccessoryList[i].accessoryName+"</td>";
        //                                 if (manufactureAccessoryList[i].accessoryNumber && manufactureAccessoryList[i].accessoryNumber != "null"){
        //                                     tableHtml += "<td>"+manufactureAccessoryList[i].accessoryNumber+"</td>";
        //                                 } else {
        //                                     tableHtml += "<td></td>";
        //                                 }
        //                                 if (manufactureAccessoryList[i].specification && manufactureAccessoryList[i].specification != "null"){
        //                                     tableHtml += "<td>"+manufactureAccessoryList[i].specification+"</td>";
        //                                 }else {
        //                                     tableHtml += "<td></td>";
        //                                 }
        //                                 if (manufactureAccessoryList[i].accessoryColor && manufactureAccessoryList[i].accessoryColor != "null"){
        //                                     tableHtml += "<td>"+manufactureAccessoryList[i].accessoryColor+"</td>";
        //                                 }else {
        //                                     tableHtml += "<td></td>";
        //                                 }
        //                                 if (manufactureAccessoryList[i].sizeName && manufactureAccessoryList[i].sizeName != "null"){
        //                                     tableHtml += "<td>"+manufactureAccessoryList[i].sizeName+"</td>";
        //                                 }else {
        //                                     tableHtml += "<td></td>";
        //                                 }
        //                                 if (manufactureAccessoryList[i].accessoryUnit && manufactureAccessoryList[i].accessoryUnit != "null"){
        //                                     tableHtml += "<td>"+manufactureAccessoryList[i].accessoryUnit+"</td>";
        //                                 }else {
        //                                     tableHtml += "<td></td>";
        //                                 }
        //                                 if (manufactureAccessoryList[i].accessoryCount && manufactureAccessoryList[i].accessoryCount != "null"){
        //                                     tableHtml += "<td>"+manufactureAccessoryList[i].accessoryCount+"</td>";
        //                                 }else {
        //                                     tableHtml += "<td></td>";
        //                                 }
        //                                 tableHtml += "<td>"+toDecimalFour(manufactureAccessoryList[i].price)+"</td><td>"+toDecimal(manufactureAccessoryList[i].sumMoney)+"</td><td>"+toDecimal(manufactureAccessoryList[i].addFee)+"</td><td>"+manufactureAccessoryList[i].addRemark+"</td></tr>";
        //                                 pieceSum += manufactureAccessoryList[i].accessoryCount;
        //                                 moneySum += manufactureAccessoryList[i].sumMoney;
        //                                 feeSum += manufactureAccessoryList[i].addFee;
        //                             }
        //                             tableHtml += "<tr><td colspan='7' style='text-align:center'>小计：</td><td>"+toDecimal(pieceSum)+"</td><td></td><td>"+toDecimal(moneySum)+"</td><td>"+toDecimal(feeSum)+"</td><td></td></tr>";
        //                             tableHtml += "<tr><td colspan='5' style='text-align:center'>交货日期：</td><td colspan='7' style='text-align:center'>"+ deliveryDate +"</td></tr>";
        //                             tableHtml += "<tr><td colspan='5' style='text-align:center'>交货地址：</td><td colspan='7' style='text-align:center'>中山市沙溪镇康乐北路42号德悦服饰2楼辅料仓</td></tr>";
        //                             tableHtml += "<tr><td colspan='2' rowspan='5' style='text-align:center'>备注</td><td colspan='10'>1.质量要求:质量要求：用料要环保，不能含致癌物质，品质跟样办。</td></tr>";
        //                             tableHtml += "<tr><td colspan='10'>2.包装要求：请务必在每袋或每捆上注明款号、规格、数量。</td></tr>";
        //                             tableHtml += "<tr><td colspan='10'>3.送货单：需注明采购单号及款号并注明产品名称、颜色、规格、数量。</td></tr>";
        //                             tableHtml += "<tr><td colspan='10'>4.供应商收到订单应第一时间回复本司货期，否则作默认同意，供应商必须保证产品的质量和货期，若产品质量不对办，我司将要求无偿更换或退货处理，如不能如期交货，请提前回复我司。</td></tr>";
        //                             tableHtml += "<tr><td colspan='10'>5.本合同需签字回传订单方可生效。。</td></tr>";
        //                             tableHtml += "<tr><td colspan='12' style='text-align: left; font-weight: 700; color: black'>二、其他条款</td></tr>";
        //                             tableHtml += "<tr><td colspan='12' style='text-align: left;'>1、乙方应严格按照《针织T恤衫》国家（一等品）标准，以及甲方订单要求进行加工生产，乙方交至甲方的产品，必须符合甲方的要求。</td></tr>";
        //                             tableHtml += "<tr><td colspan='12' style='text-align: left;'>2、乙方应于订单规定的时间内把甲方采购的（物料/布料）交至甲方指定地点，乙方承担运费及运输过程中的一切风险和法律责任。</td></tr>";
        //                             tableHtml += "<tr><td colspan='12' style='text-align: left;'>3、甲方对货物的验收仅是形式检验，其验收不及于物品的内在质量，乙方仍应对物品质量承担保证责任。如在生产或销售过程中，发现内在质量不合格，造成退货或其他损失，由乙方负全责。</td></tr>";
        //                             tableHtml += "<tr><td colspan='12' style='text-align: left;'>4、如乙方货物出现短缺、规格不符或质量问题等不符合合同约定情形的，甲方有权拒收并视为未交货，乙方应负责于规定时间内，免费更换合格的产品并运送至甲方，如出现特殊情况，需双方协商一致后再另行处理。</td></tr>";
        //                             tableHtml += "<tr><td colspan='12' style='text-align: left;'>5、乙方因生产进度、质量问题等导致交货延期或双方另行协商的货期内仍不能完成的,视为乙方违约,乙方应承担逾期交货违约责任，并按逾期交货金额1% 每日向甲方支付违约金，因此给甲方造成经济损失的，乙方还应予以赔偿。</td></tr>";
        //                             tableHtml += "<tr><td colspan='12' style='text-align: left;'>6、结算方式：月结 _____ 天。（乙方每月需将采购货物签收单原件交付甲方，如乙方代发货至第三方的，同样需将签收单原件交付甲方。）付款期限内，乙方开具13%的增值税发票或相关票据后，甲方付给乙方货款。</td></tr>";
        //                             tableHtml += "<tr><td colspan='12' style='text-align: left;'>7、在合同履行过程中，债权及债务不可转让给第三方。</td></tr>";
        //                             tableHtml += "<tr><td colspan='12' style='text-align: left;'>8、本合同未尽事宜，或在合同执行过程中出现的争执和异议，双方本着友好协商的态度共同解决，协商不成的，任何一方可向甲方所在地人民法院起诉。</td></tr>";
        //                             tableHtml += "<tr><td colspan='12' style='text-align: left;'>9、本合同一式两份，甲乙双方各执一份，具有同等法律效力。</td></tr>";
        //                             tableHtml += "<tr><td colspan='4' style='text-align: left; font-weight: 700; color: black'>甲方:中山市徳悦服饰有限公司</td><td colspan='8' style='text-align: left; font-weight: 700; color: black'>乙方:"+ partBName +"</td></tr>";
        //                             tableHtml += "<tr><td colspan='4' style='text-align: left; font-weight: 700; color: black'>单位地址:中山市沙溪镇康乐北路42号之1二楼</td><td colspan='8' style='text-align: left; font-weight: 700; color: black'>单位地址:"+ partBAddress +"</td></tr>";
        //                             tableHtml += "<tr><td colspan='4' style='text-align: left; font-weight: 700; color: black'>授权代表:</td><td colspan='8' style='text-align: left; font-weight: 700; color: black'>授权代表:</td></tr>";
        //                             tableHtml += "<tr><td colspan='4' style='text-align: left; font-weight: 700; color: black'>电话:</td><td colspan='8' style='text-align: left; font-weight: 700; color: black'>电话:</td></tr>";
        //                             tableHtml +=  "                       </tbody>" +
        //                                 "                   </table>" +
        //                                 "               </section>" +
        //                                 "              </div>";
        //                             $("#materialCheck").append(tableHtml);
        //                         } else{
        //                             layer.msg("面料信息未录入或者全部已经提交审核");
        //                             layer.close(index);
        //                         }
        //                     }
        //                 }, error: function () {
        //                     layer.msg("未录入面料信息！", {icon: 2});
        //                 }
        //             })
        //         },100);
        //     } else {
        //         layer.msg('该合同中的内容没有全部通过,全部通过后才能打印/导出~~');
        //     }
        // }
        // else if (obj.event === 'destroyCheck'){
        //     if (userRole != "role10" && userRole != "root"){
        //         layer.msg("您没有权限！", {icon: 2});
        //         return false;
        //     } else {
        //         layer.confirm("确认反审吗?", function () {
        //             $.ajax({
        //                 url: "/erp/destroycheckbychecknumber",
        //                 type: 'POST',
        //                 data: {
        //                     reviewType: data.reviewType,
        //                     checkNumber: data.checkNumber
        //                 },
        //                 success: function (res) {
        //                     if (res.data == 0) {
        //                         if (data.reviewType === '面料'){
        //                             initTable('','','',1);
        //                         } else {
        //                             initTable('','','',2);
        //                         }
        //                         reloadInfo();
        //                         layer.close(index);
        //                         layer.msg("提交成功！", {icon: 1});
        //                     } else {
        //                         layer.msg("提交失败！", {icon: 2});
        //                     }
        //                 }, error: function () {
        //                     layer.msg("提交失败！", {icon: 2});
        //                 }
        //             })
        //         });
        //     }
        // }
        // else if (obj.event === 'exportExcel'){
        //     $.ajax({
        //         url: "/erp/getreviewdatabytypechecknumber",
        //         type: 'GET',
        //         data: {
        //             checkNumber: data.checkNumber,
        //             reviewType: data.reviewType
        //         },
        //         async:false,
        //         success: function (res) {
        //             var resultData = [];
        //             var fileName = data.reviewType + data.checkNumber + ".xlsx";
        //             if (data.reviewType === "面料"){
        //                 var title = [
        //                     {field: 'orderName', title: '款号',align:'center',minWidth:140, sort: true, filter: true},
        //                     {field: 'clothesVersionNumber', title: '单号',align:'center',minWidth:140, sort: true, filter: true},
        //                     {field: 'fabricName', title: '名称',align:'center',minWidth:200, sort: true, filter: true,edit:'text'},
        //                     {field: 'unit', title: '单位',align:'center',minWidth:80, sort: true, filter: true,edit:'text'},
        //                     {field: 'partName', title: '部位',align:'center',minWidth:80, sort: true, filter: true,edit:'text'},
        //                     {field: 'colorName', title: '订单颜色',align:'center',minWidth:100, sort: true, filter: true,edit:'text'},
        //                     {field: 'fabricColor', title: '面料颜色',align:'center',minWidth:100, sort: true, filter: true,edit:'text'},
        //                     {field: 'supplier', title: '供应商',edit:'text',align:'center',minWidth:100},
        //                     {field: 'pieceUsage', title: '单件用量',edit:'text',align:'center',minWidth:130, fixed: 'right'},
        //                     {field: 'orderCount', title: '订单量',edit:'text',align:'center',minWidth:130, fixed: 'right', totalRow:true},
        //                     {field: 'fabricAdd', title: '损耗(%)',edit:'text',align:'center',minWidth:100, fixed: 'right'},
        //                     {field: 'fabricCount', title: '理论订布',edit:'text',align:'center',minWidth:130, fixed: 'right', totalRow:true},
        //                     {field: 'fabricActCount', title: '实际订布',edit:'text',align:'center',minWidth:130, fixed: 'right', totalRow:true},
        //                     {field: 'price', title: '单价',edit:'text',align:'center',minWidth:130, fixed: 'right'},
        //                     {field: 'sumMoney', title: '总价',edit:'text',align:'center',minWidth:130, fixed: 'right', totalRow:true},
        //                     {field: 'fabricID', title: 'fabricID',hide:true}
        //                 ];
        //                 if (res.data){
        //                     $.each(res.data,function(index1,value1){
        //                         var thisFabricData = {};
        //                         thisFabricData.fabricID = value1.fabricID;
        //                         thisFabricData.orderName = value1.orderName;
        //                         thisFabricData.clothesVersionNumber = value1.clothesVersionNumber;
        //                         thisFabricData.fabricName = value1.fabricName;
        //                         thisFabricData.unit = value1.unit;
        //                         thisFabricData.partName = value1.partName;
        //                         thisFabricData.colorName = value1.colorName;
        //                         thisFabricData.isHit = value1.isHit;
        //                         thisFabricData.fabricColor = value1.fabricColor;
        //                         thisFabricData.supplier = value1.supplier;
        //                         thisFabricData.pieceUsage = value1.pieceUsage;
        //                         thisFabricData.orderCount = value1.orderCount;
        //                         thisFabricData.fabricAdd = value1.fabricAdd;
        //                         thisFabricData.fabricCount = value1.fabricCount;
        //                         thisFabricData.fabricActCount = value1.fabricActCount;
        //                         thisFabricData.price = value1.price;
        //                         thisFabricData.sumMoney = value1.sumMoney;
        //                         resultData.push(thisFabricData);
        //                     });
        //                 } else{
        //                     layer.msg("面料信息未录入");
        //                     layer.close(index);
        //                 }
        //             } else if (data.reviewType === "辅料"){
        //                 var title = [
        //                     {field: 'orderName', title: '款号',align:'center',minWidth:120, sort: true, filter: true},
        //                     {field: 'clothesVersionNumber', title: '单号',align:'center',minWidth:120, sort: true, filter: true},
        //                     {field: 'accessoryName', title: '辅料名称',align:'center',minWidth:100, sort: true, filter: true,edit:'text'},
        //                     {field: 'accessoryNumber', title: '编号',align:'center',minWidth:80, sort: true, filter: true,edit:'text'},
        //                     {field: 'specification', title: '辅料规格',align:'center',minWidth:80, sort: true, filter: true,edit:'text'},
        //                     {field: 'accessoryUnit', title: '单位',align:'center',minWidth:80, sort: true, filter: true,edit:'text'},
        //                     {field: 'accessoryColor', title: '辅料颜色',align:'center',minWidth:80, sort: true, filter: true,edit:'text'},
        //                     {field: 'pieceUsage', title: '单件用量',align:'center',edit:'text',minWidth:80},
        //                     {field: 'sizeName', title: '尺码',align:'center',minWidth:100},
        //                     {field: 'colorName', title: '颜色',align:'center',minWidth:120},
        //                     {field: 'supplier', title: '供应商',align:'center',edit:'text',minWidth:100, sort: true, filter: true},
        //                     {field: 'orderCount', title: '订单量',align:'center',edit:'text',minWidth:130, fixed: 'right', totalRow:true},
        //                     {field: 'accessoryAdd', title: '上浮(%)',align:'center',edit:'text',minWidth:130, fixed: 'right'},
        //                     {field: 'accessoryCount', title: '订购量',align:'center',edit:'text',minWidth:130, fixed: 'right', totalRow:true},
        //                     {field: 'price', title: '单价',align:'center',edit:'text',minWidth:130, fixed: 'right'},
        //                     {field: 'sumMoney', title: '总价',align:'center',edit:'text',minWidth:130, fixed: 'right', totalRow:true},
        //                     {field: 'accessoryID', title: 'accessoryID',hide:true}
        //                 ];
        //                 if (res.data){
        //                     $.each(res.data,function(index1,value1){
        //                         var thisAccessoryData = {};
        //                         thisAccessoryData.accessoryID = value1.accessoryID;
        //                         thisAccessoryData.orderName = value1.orderName;
        //                         thisAccessoryData.clothesVersionNumber = value1.clothesVersionNumber;
        //                         thisAccessoryData.accessoryName = value1.accessoryName;
        //                         thisAccessoryData.accessoryNumber = value1.accessoryNumber;
        //                         thisAccessoryData.specification = value1.specification;
        //                         thisAccessoryData.accessoryUnit = value1.accessoryUnit;
        //                         thisAccessoryData.accessoryColor = value1.accessoryColor;
        //                         thisAccessoryData.pieceUsage = value1.pieceUsage;
        //                         thisAccessoryData.sizeName = value1.sizeName;
        //                         thisAccessoryData.colorName = value1.colorName;
        //                         thisAccessoryData.supplier = value1.supplier;
        //                         thisAccessoryData.orderCount = value1.orderCount;
        //                         thisAccessoryData.accessoryAdd = value1.accessoryAdd;
        //                         thisAccessoryData.accessoryCount = value1.accessoryCount;
        //                         thisAccessoryData.price = value1.price;
        //                         thisAccessoryData.sumMoney = value1.sumMoney;
        //                         resultData.push(thisAccessoryData);
        //                     });
        //                 } else{
        //                     layer.msg("辅料信息未录入");
        //                     layer.close(index);
        //                 }
        //             }
        //             fabricAccessoryExcelTable = table.render({
        //                 elem: '#fabricAccessoryExcelTable'
        //                 ,cols: [title]
        //                 ,data: resultData
        //                 ,height: 'full-150'
        //                 ,toolbar: true
        //                 ,limit: 1000
        //                 ,limits: [1000, 2000, 3000]
        //                 ,totalRow:true
        //                 ,excel:{ // 导出excel配置, （以下值均为默认值）
        //                     filename: fileName
        //                 }
        //                 ,done:function (res) {
        //                     soulTable.render(this);
        //                 }
        //             });
        //         },
        //         error: function () {
        //             layer.msg("未录入面料信息！", {icon: 2});
        //         }
        //     });
        //     soulTable.export(fabricAccessoryExcelTable);
        // }
        // else if (obj.event === 'checkDetailUpdate'){
        //     var label = "合同详情, 类别:" + data.reviewType + "&emsp;&emsp;&emsp;&emsp; 合同号:" + data.checkNumber + "&emsp;&emsp;&emsp;&emsp; 删除要选中,修改不用选中";
        //     if (data.checkState === '通过'){
        //         layer.msg("已经审核通过,无法修改!");
        //         return false;
        //     }
        //     var index = layer.open({
        //         type: 1 //Page层类型
        //         , title: label
        //         , shade: 0.6 //遮罩透明度
        //         , area: '1000px'
        //         , btn: ['保存','删除','计算用量','计算总价']
        //         , maxmin: false //允许全屏最小化
        //         , anim: 0 //0-6的动画形式，-1不开启
        //         , content: "<div id=\"materialReview\">\n" +
        //             "        <form class=\"layui-form\" style=\"margin: 0 auto;max-width:100%;padding-top: 20px;\">\n" +
        //             "            <table class=\"layui-hide\" id=\"materialReviewTable\" lay-filter=\"materialReviewTable\"></table>\n" +
        //             "        </form>\n" +
        //             "    </div>"
        //         ,yes: function(i, layero){
        //             if (data.reviewType === "面料"){
        //                 var materialData = table.cache.materialReviewTable;
        //                 $.each(materialData,function(index1,value1){
        //                     value1.orderState = '未下单';
        //                     value1.checkState = '已提交';
        //                 });
        //                 $.ajax({
        //                     url: "/erp/updatemanufacturefabricaftercommit",
        //                     type: 'POST',
        //                     data: {
        //                         manufactureFabricJson:JSON.stringify(materialData)
        //                     },
        //                     success: function (res) {
        //                         if (res.result == 0) {
        //                             initTable('','','',1);
        //                             reloadInfo();
        //                             layer.close(index);
        //                             layer.msg("提交成功！", {icon: 1});
        //                         } else {
        //                             layer.msg("提交失败！", {icon: 2});
        //                         }
        //                     }, error: function () {
        //                         layer.msg("提交失败！", {icon: 2});
        //                     }
        //                 })
        //             }
        //             else if (data.reviewType === "辅料"){
        //                 var materialData = table.cache.materialReviewTable;
        //                 $.each(materialData,function(index1,value1){
        //                     value1.orderState = '未下单';
        //                     value1.checkState = '已提交';
        //                 });
        //                 $.ajax({
        //                     url: "/erp/updatemanufactureaccessoryaftercommit",
        //                     type: 'POST',
        //                     data: {
        //                         manufactureAccessoryJson:JSON.stringify(materialData)
        //                     },
        //                     success: function (res) {
        //                         if (res.result == 0) {
        //                             initTable('','','',2);
        //                             reloadInfo();
        //                             layer.close(index);
        //                             layer.msg("提交成功！", {icon: 1});
        //                         } else {
        //                             layer.msg("提交失败！", {icon: 2});
        //                         }
        //                     }, error: function () {
        //                         layer.msg("提交失败！", {icon: 2});
        //                     }
        //                 })
        //             }
        //
        //         }
        //         ,btn2: function(index, layero){
        //             if (data.reviewType === "面料"){
        //                 var materialData = table.cache.materialReviewTable;
        //                 var idList = [];
        //                 $.each(materialData,function(index1,value1){
        //                     if (value1.LAY_CHECKED){
        //                         idList.push(value1.fabricID);
        //                     }
        //                 });
        //                 if (idList.length === 0){
        //                     layer.msg("请先选中后删除");
        //                     return false;
        //                 }
        //                 $.ajax({
        //                     url: "/erp/deletemanufacturefabricbatch",
        //                     type: 'POST',
        //                     data: {
        //                         fabricIDList: idList
        //                     },
        //                     traditional: true,
        //                     success: function (res) {
        //                         if (res.result == 0) {
        //                             layer.close(index);
        //                             layer.msg("删除成功！", {icon: 1});
        //                         } else if (res.result == 3) {
        //                             layer.msg("所选面料已入库,无法删除！", {icon: 2});
        //                         } else {
        //                             layer.msg("删除失败！", {icon: 2});
        //                         }
        //                     }, error: function () {
        //                         layer.msg("删除失败！", {icon: 2});
        //                     }
        //                 })
        //             } else {
        //                 var materialData = table.cache.materialReviewTable;
        //                 var idList = [];
        //                 $.each(materialData,function(index1,value1){
        //                     if (value1.LAY_CHECKED){
        //                         idList.push(value1.accessoryID);
        //                     }
        //                 });
        //                 if (idList.length === 0){
        //                     layer.msg("请先选中后删除");
        //                     return false;
        //                 }
        //                 $.ajax({
        //                     url: "/erp/deletemanufactureaccessorybatch",
        //                     type: 'POST',
        //                     data: {
        //                         accessoryIDList: idList
        //                     },
        //                     traditional: true,
        //                     success: function (res) {
        //                         if (res.result == 0) {
        //                             layer.close(index);
        //                             layer.msg("删除成功！", {icon: 1});
        //                         } else {
        //                             layer.msg("删除失败！", {icon: 2});
        //                         }
        //                     }, error: function () {
        //                         layer.msg("删除失败！", {icon: 2});
        //                     }
        //                 })
        //             }
        //         }
        //         ,btn3: function(index, layero){
        //             if (data.reviewType === "面料"){
        //                 var materialData = table.cache.materialReviewTable;
        //                 $.each(materialData,function(index1,value1){
        //                     if (value1.pieceUsage == 0 || value1.orderCount == 0){
        //                         value1.fabricActCount = 0;
        //                         value1.fabricCount = 0;
        //                     } else {
        //                         value1.fabricCount = Math.round(value1.orderPieceUsage * value1.orderCount);
        //                         value1.fabricActCount = Math.round((1 + value1.fabricAdd/100) * value1.pieceUsage * value1.orderCount);
        //                     }
        //                 });
        //                 table.reload("materialReviewTable",{
        //                     data:materialData   // 将新数据重新载入表格
        //                 });
        //             } else if (data.reviewType === "辅料"){
        //                 var materialData = table.cache.materialReviewTable;
        //                 $.each(materialData,function(index1,value1){
        //                     if (value1.pieceUsage == 0 || value1.orderCount == 0){
        //                         value1.accessoryCount = 0;
        //                     } else {
        //                         value1.accessoryCount = Math.round((1 + value1.accessoryAdd/100) * value1.pieceUsage * value1.orderCount);
        //                     }
        //                 });
        //                 table.reload("materialReviewTable",{
        //                     data:materialData   // 将新数据重新载入表格
        //                 });
        //             }
        //             layui.form.render("select");
        //             return false  //开启该代码可禁止点击该按钮关闭
        //         }
        //         ,btn4: function(index, layero){
        //             if (data.reviewType === "面料"){
        //                 var materialData = table.cache.materialReviewTable;
        //                 $.each(materialData,function(index1,value1){
        //                     if (value1.fabricActCount == 0 || value1.price == 0){
        //                         value1.sumMoney = 0;
        //                     } else {
        //                         value1.sumMoney = toDecimal(value1.fabricActCount * value1.price);
        //                     }
        //                 });
        //                 table.reload("materialReviewTable",{
        //                     data:materialData   // 将新数据重新载入表格
        //                 });
        //             } else if (data.reviewType === "辅料"){
        //                 var materialData = table.cache.materialReviewTable;
        //                 $.each(materialData,function(index1,value1){
        //                     if (value1.accessoryCount == 0 || value1.price == 0){
        //                         value1.sumMoney = 0;
        //                     } else {
        //                         value1.sumMoney = toDecimal(value1.accessoryCount * value1.price);
        //                     }
        //                 });
        //                 table.reload("materialReviewTable",{
        //                     data:materialData   // 将新数据重新载入表格
        //                 });
        //             }
        //             layui.form.render("select");
        //             return false  //开启该代码可禁止点击该按钮关闭
        //         }
        //     });
        //     layer.full(index);
        //     setTimeout(function () {
        //         $.ajax({
        //             url: "/erp/getreviewdatabytypechecknumber",
        //             type: 'GET',
        //             data: {
        //                 checkNumber: data.checkNumber,
        //                 reviewType: data.reviewType
        //             },
        //             success: function (res) {
        //                 var resultData = [];
        //                 if (data.reviewType === "面料"){
        //                     var title = [
        //                         {type:'checkbox'},
        //                         {field: 'orderName', title: '款号',align:'center',minWidth:140, sort: true, filter: true},
        //                         {field: 'clothesVersionNumber', title: '单号',align:'center',minWidth:140, sort: true, filter: true},
        //                         {field: 'fabricName', title: '名称',align:'center',minWidth:200, sort: true, filter: true,edit:'text'},
        //                         {field: 'unit', title: '单位',align:'center',minWidth:80, sort: true, filter: true,edit:'text'},
        //                         {field: 'partName', title: '部位',align:'center',minWidth:80, sort: true, filter: true,edit:'text'},
        //                         {field: 'colorName', title: '订单颜色',align:'center',minWidth:100, sort: true, filter: true,edit:'text'},
        //                         {field: 'fabricColor', title: '面料颜色',align:'center',minWidth:100, sort: true, filter: true,edit:'text'},
        //                         {field: 'supplier', title: '供应商',edit:'text',align:'center',minWidth:100},
        //                         {field: 'orderPieceUsage', title: '客供用量',edit:'text',align:'center',minWidth:130, fixed: 'right'},
        //                         {field: 'pieceUsage', title: '单件用量',edit:'text',align:'center',minWidth:130, fixed: 'right'},
        //                         {field: 'orderCount', title: '订单量',edit:'text',align:'center',minWidth:130, fixed: 'right', totalRow:true},
        //                         {field: 'fabricAdd', title: '损耗(%)',edit:'text',align:'center',minWidth:100, fixed: 'right'},
        //                         {field: 'fabricCount', title: '理论订布',edit:'text',align:'center',minWidth:130, fixed: 'right', totalRow:true},
        //                         {field: 'fabricActCount', title: '实际订布',edit:'text',align:'center',minWidth:130, fixed: 'right', totalRow:true},
        //                         {field: 'price', title: '单价',edit:'text',align:'center',minWidth:130, fixed: 'right'},
        //                         {field: 'sumMoney', title: '总价',edit:'text',align:'center',minWidth:130, fixed: 'right', totalRow:true},
        //                         {field: 'fabricID', title: 'fabricID',hide:true}
        //                     ];
        //                     if (res.data){
        //                         $.each(res.data,function(index1,value1){
        //                             var thisFabricData = {};
        //                             thisFabricData.fabricID = value1.fabricID;
        //                             thisFabricData.orderName = value1.orderName;
        //                             thisFabricData.clothesVersionNumber = value1.clothesVersionNumber;
        //                             thisFabricData.fabricName = value1.fabricName;
        //                             thisFabricData.unit = value1.unit;
        //                             thisFabricData.partName = value1.partName;
        //                             thisFabricData.colorName = value1.colorName;
        //                             thisFabricData.isHit = value1.isHit;
        //                             thisFabricData.fabricColor = value1.fabricColor;
        //                             thisFabricData.supplier = value1.supplier;
        //                             thisFabricData.pieceUsage = value1.pieceUsage;
        //                             thisFabricData.orderPieceUsage = value1.orderPieceUsage;
        //                             thisFabricData.orderCount = value1.orderCount;
        //                             thisFabricData.fabricAdd = value1.fabricAdd;
        //                             thisFabricData.fabricCount = value1.fabricCount;
        //                             thisFabricData.fabricActCount = value1.fabricActCount;
        //                             thisFabricData.price = value1.price;
        //                             thisFabricData.sumMoney = value1.sumMoney;
        //                             resultData.push(thisFabricData);
        //                         });
        //                     } else{
        //                         layer.msg("面料信息未录入");
        //                         layer.close(index);
        //                     }
        //                 }
        //                 else if (data.reviewType === "辅料"){
        //                     var title = [
        //                         {type:'checkbox'},
        //                         {field: 'orderName', title: '款号',align:'center',minWidth:120, sort: true, filter: true},
        //                         {field: 'clothesVersionNumber', title: '单号',align:'center',minWidth:120, sort: true, filter: true},
        //                         {field: 'accessoryName', title: '辅料名称',align:'center',minWidth:100, sort: true, filter: true,edit:'text'},
        //                         {field: 'accessoryNumber', title: '编号',align:'center',minWidth:80, sort: true, filter: true,edit:'text'},
        //                         {field: 'specification', title: '辅料规格',align:'center',minWidth:80, sort: true, filter: true,edit:'text'},
        //                         {field: 'accessoryUnit', title: '单位',align:'center',minWidth:80, sort: true, filter: true,edit:'text'},
        //                         {field: 'accessoryColor', title: '辅料颜色',align:'center',minWidth:80, sort: true, filter: true,edit:'text'},
        //                         {field: 'sizeName', title: '尺码',align:'center',minWidth:100},
        //                         {field: 'colorName', title: '颜色',align:'center',minWidth:120},
        //                         {field: 'orderCount', title: '订单量',align:'center',edit:'text',minWidth:130, totalRow:true},
        //                         {field: 'pieceUsage', title: '单件用量',align:'center',edit:'text',minWidth:80},
        //                         {field: 'orderPieceUsage', title: '客供用量',align:'center',edit:'text',minWidth:80},
        //                         {field: 'supplier', title: '供应商',align:'center',edit:'text',minWidth:100, sort: true, filter: true},
        //                         {field: 'accessoryAdd', title: '上浮(%)',align:'center',edit:'text',minWidth:130, fixed: 'right'},
        //                         {field: 'accessoryCount', title: '订购量',align:'center',edit:'text',minWidth:130, fixed: 'right', totalRow:true},
        //                         {field: 'orderPrice', title: '客供单价',align:'center',edit:'text',minWidth:130, fixed: 'right'},
        //                         {field: 'price', title: '单价',align:'center',edit:'text',minWidth:130, fixed: 'right'},
        //                         {field: 'sumMoney', title: '总价',align:'center',edit:'text',minWidth:130, fixed: 'right', totalRow:true},
        //                         {field: 'addFee', title: '附加费',align:'center',edit:'text',minWidth:130, fixed: 'right', totalRow:true},
        //                         {field: 'addRemark', title: '备注',align:'center',edit:'text',minWidth:130, fixed: 'right'},
        //                         {field: 'accessoryID', title: 'accessoryID',hide:true}
        //                     ];
        //                     if (res.data){
        //                         $.each(res.data,function(index1,value1){
        //                             var thisAccessoryData = {};
        //                             thisAccessoryData.accessoryID = value1.accessoryID;
        //                             thisAccessoryData.orderName = value1.orderName;
        //                             thisAccessoryData.clothesVersionNumber = value1.clothesVersionNumber;
        //                             thisAccessoryData.accessoryName = value1.accessoryName;
        //                             thisAccessoryData.accessoryNumber = value1.accessoryNumber;
        //                             thisAccessoryData.specification = value1.specification;
        //                             thisAccessoryData.accessoryUnit = value1.accessoryUnit;
        //                             thisAccessoryData.accessoryColor = value1.accessoryColor;
        //                             thisAccessoryData.pieceUsage = value1.pieceUsage;
        //                             thisAccessoryData.orderPieceUsage = value1.orderPieceUsage;
        //                             thisAccessoryData.sizeName = value1.sizeName;
        //                             thisAccessoryData.colorName = value1.colorName;
        //                             thisAccessoryData.supplier = value1.supplier;
        //                             thisAccessoryData.orderCount = value1.orderCount;
        //                             thisAccessoryData.accessoryAdd = value1.accessoryAdd;
        //                             thisAccessoryData.accessoryCount = value1.accessoryCount;
        //                             thisAccessoryData.price = value1.price;
        //                             thisAccessoryData.orderPrice = value1.orderPrice;
        //                             thisAccessoryData.sumMoney = value1.sumMoney;
        //                             thisAccessoryData.addFee = value1.addFee;
        //                             thisAccessoryData.addRemark = value1.addRemark;
        //                             resultData.push(thisAccessoryData);
        //                         });
        //                     } else{
        //                         layer.msg("辅料信息未录入");
        //                         layer.close(index);
        //                     }
        //                 }
        //                 table.render({
        //                     elem: '#materialReviewTable'
        //                     ,cols: [title]
        //                     ,data: resultData
        //                     ,height: 'full-150'
        //                     ,toolbar: true
        //                     ,limit: 1000
        //                     ,limits: [1000, 2000, 3000]
        //                     ,totalRow:true
        //                     ,filter: {
        //                         cache: false
        //                     }
        //                     ,done:function (res) {
        //                         soulTable.render(this);
        //                     }
        //                 });
        //             },
        //             error: function () {
        //                 layer.msg("未录入面料信息！", {icon: 2});
        //             }
        //         })
        //     },100);
        // }
        // else if (obj.event === 'checkDetail'){
        //     var label = "合同详情, 类别:" + data.reviewType + "&emsp;&emsp;&emsp;&emsp; 合同号:" + data.checkNumber;
        //     var index = layer.open({
        //         type: 1 //Page层类型
        //         , title: label
        //         , shade: 0.6 //遮罩透明度
        //         , area: '1000px'
        //         , maxmin: false //允许全屏最小化
        //         , anim: 0 //0-6的动画形式，-1不开启
        //         , content: "<div id=\"materialReview\">\n" +
        //             "        <form class=\"layui-form\" style=\"margin: 0 auto;max-width:100%;padding-top: 20px;\">\n" +
        //             "            <table class=\"layui-hide\" id=\"materialReviewTable\" lay-filter=\"materialReviewTable\"></table>\n" +
        //             "        </form>\n" +
        //             "    </div>"
        //     });
        //     layer.full(index);
        //     setTimeout(function () {
        //         $.ajax({
        //             url: "/erp/getreviewdatabytypechecknumber",
        //             type: 'GET',
        //             data: {
        //                 checkNumber: data.checkNumber,
        //                 reviewType: data.reviewType
        //             },
        //             success: function (res) {
        //                 var resultData = [];
        //                 if (data.reviewType === "面料"){
        //                     var title = [
        //                         {field: 'orderName', title: '款号',align:'center',minWidth:140, sort: true, filter: true},
        //                         {field: 'clothesVersionNumber', title: '单号',align:'center',minWidth:140, sort: true, filter: true},
        //                         {field: 'fabricName', title: '名称',align:'center',minWidth:200, sort: true, filter: true},
        //                         {field: 'unit', title: '单位',align:'center',minWidth:80, sort: true, filter: true},
        //                         {field: 'partName', title: '部位',align:'center',minWidth:80, sort: true, filter: true},
        //                         {field: 'colorName', title: '订单颜色',align:'center',minWidth:100, sort: true, filter: true},
        //                         {field: 'fabricColor', title: '面料颜色',align:'center',minWidth:100, sort: true, filter: true},
        //                         {field: 'supplier', title: '供应商',align:'center',minWidth:100},
        //                         {field: 'pieceUsage', title: '单件用量',align:'center',minWidth:130, fixed: 'right'},
        //                         {field: 'orderCount', title: '订单量',align:'center',minWidth:130, fixed: 'right',totalRow: true},
        //                         {field: 'fabricAdd', title: '上浮(%)',align:'center',minWidth:130, fixed: 'right'},
        //                         {field: 'fabricCount', title: '订布量',align:'center',minWidth:130, fixed: 'right',totalRow: true},
        //                         {field: 'fabricActCount', title: '实际订布',align:'center',minWidth:130, fixed: 'right',totalRow: true},
        //                         {field: 'price', title: '单价',align:'center',minWidth:130, fixed: 'right'},
        //                         {field: 'sumMoney', title: '总价',align:'center',minWidth:130, fixed: 'right',totalRow: true},
        //                         {field: 'fabricID', title: 'fabricID',hide:true}
        //                     ];
        //
        //                     if (res.data){
        //                         $.each(res.data,function(index1,value1){
        //                             var thisFabricData = {};
        //                             thisFabricData.fabricID = value1.fabricID;
        //                             thisFabricData.orderName = value1.orderName;
        //                             thisFabricData.clothesVersionNumber = value1.clothesVersionNumber;
        //                             thisFabricData.fabricName = value1.fabricName;
        //                             thisFabricData.unit = value1.unit;
        //                             thisFabricData.partName = value1.partName;
        //                             thisFabricData.colorName = value1.colorName;
        //                             thisFabricData.isHit = value1.isHit;
        //                             thisFabricData.fabricColor = value1.fabricColor;
        //                             thisFabricData.supplier = value1.supplier;
        //                             thisFabricData.pieceUsage = value1.pieceUsage;
        //                             thisFabricData.orderCount = value1.orderCount;
        //                             thisFabricData.fabricAdd = value1.fabricAdd;
        //                             thisFabricData.fabricCount = value1.fabricCount;
        //                             thisFabricData.fabricActCount = value1.fabricActCount;
        //                             thisFabricData.price = value1.price;
        //                             thisFabricData.sumMoney = value1.sumMoney;
        //                             resultData.push(thisFabricData);
        //                         });
        //                     } else{
        //                         layer.msg("面料信息未录入或者全部已经提交审核");
        //                         layer.close(index);
        //                     }
        //                 } else if (data.reviewType === "辅料"){
        //                     var title = [
        //                         {field: 'orderName', title: '款号',align:'center',minWidth:120, sort: true, filter: true},
        //                         {field: 'clothesVersionNumber', title: '单号',align:'center',minWidth:120, sort: true, filter: true},
        //                         {field: 'accessoryName', title: '辅料名称',align:'center',minWidth:100, sort: true, filter: true},
        //                         {field: 'accessoryNumber', title: '编号',align:'center',minWidth:80, sort: true, filter: true},
        //                         {field: 'specification', title: '辅料规格',align:'center',minWidth:80, sort: true, filter: true},
        //                         {field: 'accessoryUnit', title: '单位',align:'center',minWidth:80, sort: true, filter: true},
        //                         {field: 'accessoryColor', title: '辅料颜色',align:'center',minWidth:80, sort: true, filter: true},
        //                         {field: 'pieceUsage', title: '单件用量',align:'center',minWidth:80},
        //                         {field: 'sizeName', title: '尺码',align:'center',minWidth:100},
        //                         {field: 'colorName', title: '颜色',align:'center',minWidth:120},
        //                         {field: 'supplier', title: '供应商',align:'center',minWidth:100, sort: true, filter: true},
        //                         {field: 'orderCount', title: '订单量',align:'center',minWidth:130, fixed: 'right',totalRow: true},
        //                         {field: 'accessoryAdd', title: '上浮(%)',align:'center',minWidth:130, fixed: 'right'},
        //                         {field: 'accessoryCount', title: '订购量',align:'center',minWidth:130, fixed: 'right',totalRow: true},
        //                         {field: 'price', title: '单价',align:'center',minWidth:130, fixed: 'right'},
        //                         {field: 'sumMoney', title: '总价',align:'center',minWidth:130, fixed: 'right',totalRow: true},
        //                         {field: 'accessoryID', title: 'accessoryID',hide:true}
        //                     ];
        //                     if (res.data){
        //                         $.each(res.data,function(index1,value1){
        //                             var thisAccessoryData = {};
        //                             thisAccessoryData.accessoryID = value1.accessoryID;
        //                             thisAccessoryData.orderName = value1.orderName;
        //                             thisAccessoryData.clothesVersionNumber = value1.clothesVersionNumber;
        //                             thisAccessoryData.accessoryName = value1.accessoryName;
        //                             thisAccessoryData.accessoryNumber = value1.accessoryNumber;
        //                             thisAccessoryData.specification = value1.specification;
        //                             thisAccessoryData.accessoryUnit = value1.accessoryUnit;
        //                             thisAccessoryData.accessoryColor = value1.accessoryColor;
        //                             thisAccessoryData.pieceUsage = value1.pieceUsage;
        //                             thisAccessoryData.sizeName = value1.sizeName;
        //                             thisAccessoryData.colorName = value1.colorName;
        //                             thisAccessoryData.supplier = value1.supplier;
        //                             thisAccessoryData.orderCount = value1.orderCount;
        //                             thisAccessoryData.accessoryAdd = value1.accessoryAdd;
        //                             thisAccessoryData.accessoryCount = value1.accessoryCount;
        //                             thisAccessoryData.price = value1.price;
        //                             thisAccessoryData.sumMoney = value1.sumMoney;
        //                             resultData.push(thisAccessoryData);
        //                         });
        //                     } else{
        //                         layer.msg("面料信息未录入或者全部已经提交审核");
        //                         layer.close(index);
        //                     }
        //                 }
        //                 table.render({
        //                     elem: '#materialReviewTable'
        //                     ,cols: [title]
        //                     ,data: resultData
        //                     ,height: 'full-150'
        //                     ,toolbar: true
        //                     ,limit: 1000
        //                     ,limits: [1000, 2000, 3000]
        //                     ,totalRow: true
        //                     ,filter: {
        //                         cache: false
        //                     }
        //                     ,done:function (res) {
        //                         soulTable.render(this);
        //                     }
        //                 });
        //             },
        //             error: function () {
        //                 layer.msg("未录入面料信息！", {icon: 2});
        //             }
        //         })
        //     },100);
        // }
        // else if (obj.event === 'changeOrderState'){
        //     if (data.checkState === "通过"){
        //         layer.confirm('确认下单了吗?', function(index){
        //             $.ajax({
        //                 url: "/erp/updateorderstatebychecknumber",
        //                 type: 'POST',
        //                 data: {
        //                     checkNumber: data.checkNumber,
        //                     reviewType: data.reviewType
        //                 },
        //                 success: function (res) {
        //                     if (res.data == 0) {
        //                         if (data.reviewType === '面料'){
        //                             initTable('','','',1);
        //                         } else {
        //                             initTable('','','',2);
        //                         }
        //                         layer.close(index);
        //                         layer.msg("确认成功！", {icon: 1});
        //                     } else {
        //                         layer.msg("确认失败！", {icon: 2});
        //                     }
        //                 }, error: function () {
        //                     layer.msg("确认失败！", {icon: 2});
        //                 }
        //             })
        //         });
        //
        //     } else {
        //         layer.msg("该合同还未通过不能确认下单！", {icon: 2});
        //         return false;
        //     }
        // }
        // else if (obj.event === 'progress'){
        //     var label = "回料进度, 类别:" + data.reviewType + "&emsp;&emsp;&emsp;&emsp; 合同号:" + data.checkNumber;
        //     var index = layer.open({
        //         type: 1 //Page层类型
        //         , title: label
        //         , shade: 0.6 //遮罩透明度
        //         , area: '1000px'
        //         , maxmin: false //允许全屏最小化
        //         , anim: 0 //0-6的动画形式，-1不开启
        //         , content: "<div id=\"materialProgress\">\n" +
        //             "        <form class=\"layui-form\" style=\"margin: 0 auto;max-width:100%;padding-top: 20px;\">\n" +
        //             "            <table class=\"layui-hide\" id=\"materialProgressTable\" lay-filter=\"materialProgressTable\"></table>\n" +
        //             "        </form>\n" +
        //             "    </div>"
        //     });
        //     layer.full(index);
        //     setTimeout(function () {
        //         $.ajax({
        //             url: "/erp/getmaterialprogressbychecknumber",
        //             type: 'GET',
        //             data: {
        //                 checkNumber: data.checkNumber,
        //                 reviewType: data.reviewType
        //             },
        //             success: function (res) {
        //                 var resultData = res.data;
        //                 if (data.reviewType === "面料"){
        //                     var title = [
        //                         {field: 'orderName', title: '款号',align:'center',minWidth:140, sort: true, filter: true},
        //                         {field: 'clothesVersionNumber', title: '单号',align:'center',minWidth:140, sort: true, filter: true},
        //                         {field: 'fabricName', title: '名称',align:'center',minWidth:200, sort: true, filter: true},
        //                         {field: 'unit', title: '单位',align:'center',minWidth:80, sort: true, filter: true},
        //                         {field: 'partName', title: '部位',align:'center',minWidth:80, sort: true, filter: true},
        //                         {field: 'colorName', title: '订单颜色',align:'center',minWidth:100, sort: true, filter: true},
        //                         {field: 'fabricColor', title: '面料颜色',align:'center',minWidth:100, sort: true, filter: true},
        //                         {field: 'supplier', title: '供应商',align:'center',minWidth:100},
        //                         {field: 'pieceUsage', title: '单件用量',align:'center',minWidth:80},
        //                         {field: 'orderCount', title: '订单量',align:'center',minWidth:70},
        //                         {field: 'fabricActCount', title: '实际订布',align:'center',minWidth:100},
        //                         {field: 'fabricReturn', title: '回布量',align:'center',minWidth:100}
        //                     ];
        //                 } else if (data.reviewType === "辅料"){
        //                     var title = [
        //                         {field: 'orderName', title: '款号',align:'center',minWidth:120, sort: true, filter: true},
        //                         {field: 'clothesVersionNumber', title: '单号',align:'center',minWidth:120, sort: true, filter: true},
        //                         {field: 'accessoryName', title: '辅料名称',align:'center',minWidth:100, sort: true, filter: true},
        //                         {field: 'accessoryNumber', title: '编号',align:'center',minWidth:80, sort: true, filter: true},
        //                         {field: 'specification', title: '辅料规格',align:'center',minWidth:80, sort: true, filter: true},
        //                         {field: 'accessoryUnit', title: '单位',align:'center',minWidth:80, sort: true, filter: true},
        //                         {field: 'accessoryColor', title: '辅料颜色',align:'center',minWidth:80, sort: true, filter: true},
        //                         {field: 'pieceUsage', title: '单件用量',align:'center',minWidth:80},
        //                         {field: 'sizeName', title: '尺码',align:'center',minWidth:100},
        //                         {field: 'colorName', title: '颜色',align:'center',minWidth:120},
        //                         {field: 'supplier', title: '供应商',align:'center',minWidth:100, sort: true, filter: true},
        //                         {field: 'orderCount', title: '订单量',align:'center',minWidth:70,fixed: 'right'},
        //                         {field: 'accessoryCount', title: '订购量',align:'center',minWidth:70,fixed: 'right'},
        //                         {field: 'returnCount', title: '回料',align:'center',minWidth:60,fixed: 'right'}
        //                     ];
        //                 }
        //                 table.render({
        //                     elem: '#materialProgressTable'
        //                     ,cols: [title]
        //                     ,data: resultData
        //                     ,height: 'full-150'
        //                     ,toolbar: true
        //                     ,limit: 1000
        //                     ,limits: [1000, 2000, 3000]
        //                     ,filter: {
        //                         cache: false
        //                     }
        //                     ,done:function (res) {
        //                         soulTable.render(this);
        //                     }
        //                 });
        //             }, error: function () {
        //                 layer.msg("获取数据失败！", {icon: 2});
        //             }
        //         })
        //     },100);
        // }
        // else if (obj.event === 'reviewCommitShare'){
        //     if (userRole != "role10" && userRole != "root"){
        //         layer.msg("您没有权限！", {icon: 2});
        //         return false;
        //     } else {
        //         if(data.checkState.indexOf("已提交") < 0) {
        //             layer.msg('没有待审核的内容');
        //         } else {
        //             var label = "物料审核, 类别:" + data.reviewType + "&emsp;&emsp;&emsp;&emsp; 合同号:" + data.checkNumber;
        //             var index = layer.open({
        //                 type: 1 //Page层类型
        //                 , title: label
        //                 , btn: ['通过','不通过']
        //                 , shade: 0.6 //遮罩透明度
        //                 , area: '1000px'
        //                 , maxmin: false //允许全屏最小化
        //                 , anim: 0 //0-6的动画形式，-1不开启
        //                 , content: "<div id=\"materialReview\">\n" +
        //                     "        <form class=\"layui-form\" style=\"margin: 0 auto;max-width:100%;padding-top: 20px;\">\n" +
        //                     "            <table class=\"layui-hide\" id=\"materialReviewTable\" lay-filter=\"materialReviewTable\"></table>\n" +
        //                     "        </form>\n" +
        //                     "    </div>"
        //                 ,yes: function(i, layero){
        //                     var materialData = table.cache.materialReviewTable;
        //                     var accessoryPreStoreList = [];
        //                     $.each(materialData,function(i,item){
        //                         accessoryPreStoreList.push({id: item.id, checkState: "通过"});
        //                     });
        //                     $.ajax({
        //                         url: "/erp/changeaccessoryprestoreincheck",
        //                         type: 'POST',
        //                         data: {
        //                             accessoryPreStoreJson: JSON.stringify(accessoryPreStoreList)
        //                         },
        //                         success: function (res) {
        //                             if (res.result == 0) {
        //                                 initTable('','','',3);
        //                                 layer.close(index);
        //                                 layer.msg("提交成功！", {icon: 1});
        //                             } else {
        //                                 layer.msg("提交失败！", {icon: 2});
        //                             }
        //                         }, error: function () {
        //                             layer.msg("提交失败！", {icon: 2});
        //                         }
        //                     });
        //                 }
        //                 ,btn2: function(i, layero){
        //                     var materialData = table.cache.materialReviewTable;
        //                     var accessoryPreStoreList = [];
        //                     $.each(materialData,function(i,item){
        //                         accessoryPreStoreList.push({id: item.id, checkState: "不通过"});
        //                     });
        //                     $.ajax({
        //                         url: "/erp/changeaccessoryprestoreincheck",
        //                         type: 'POST',
        //                         data: {
        //                             accessoryPreStoreJson: JSON.stringify(accessoryPreStoreList)
        //                         },
        //                         success: function (res) {
        //                             if (res.result == 0) {
        //                                 initTable('','','',3);
        //                                 layer.close(index);
        //                                 layer.msg("提交成功！", {icon: 1});
        //                             } else {
        //                                 layer.msg("提交失败！", {icon: 2});
        //                             }
        //                         }, error: function () {
        //                             layer.msg("提交失败！", {icon: 2});
        //                         }
        //                     });
        //                 }
        //             });
        //             layer.full(index);
        //             setTimeout(function () {
        //                 $.ajax({
        //                     url: "/erp/getaccessoryprestorebychecknumber",
        //                     type: 'GET',
        //                     data: {
        //                         checkNumber: data.checkNumber
        //                     },
        //                     success: function (res) {
        //                         var resultData = res.accessoryPreStoreList;
        //                         var title = [
        //                             {field: 'accessoryName', title: '名称',align:'center',minWidth:140, sort: true, filter: true},
        //                             {field: 'accessoryNumber', title: '编号',align:'center',minWidth:140, sort: true, filter: true},
        //                             {field: 'specification', title: '规格',align:'center',minWidth:200, sort: true, filter: true},
        //                             {field: 'accessoryColor', title: '颜色',align:'center',minWidth:80, sort: true, filter: true},
        //                             {field: 'accessoryUnit', title: '单位',align:'center',minWidth:80, sort: true, filter: true},
        //                             {field: 'supplier', title: '供应商',align:'center',minWidth:100, sort: true, filter: true},
        //                             {field: 'price', title: '单价',align:'center',minWidth:100, sort: true, filter: true},
        //                             {field: 'accessoryCount', title: '数量',align:'center',minWidth:100,totalRow:true},
        //                             {field: 'sumMoney', title: '金额',align:'center',minWidth:80,totalRow:true},
        //                             {field: 'id',hide:true}
        //                         ];
        //                         table.render({
        //                             elem: '#materialReviewTable'
        //                             ,cols: [title]
        //                             ,data: resultData
        //                             ,height: 'full-150'
        //                             ,limit: 1000
        //                             ,totalRow:true
        //                             ,limits: [1000, 2000, 3000]
        //                             ,done:function (res) {
        //                                 soulTable.render(this);
        //                             }
        //                         });
        //                     },
        //                     error: function () {
        //                         layer.msg("未录入面料信息！", {icon: 2});
        //                     }
        //                 })
        //             },100);
        //         }
        //     }
        // }
        // else if (obj.event === 'exportExcelShare'){
        //     $.ajax({
        //         url: "/erp/getaccessoryprestorebychecknumber",
        //         type: 'GET',
        //         data: {
        //             checkNumber: data.checkNumber
        //         },
        //         async:false,
        //         success: function (res) {
        //             var resultData = res.accessoryPreStoreList;
        //             var title = [
        //                 {field: 'accessoryName', title: '名称',align:'center',minWidth:140, sort: true, filter: true},
        //                 {field: 'accessoryNumber', title: '编号',align:'center',minWidth:140, sort: true, filter: true},
        //                 {field: 'specification', title: '规格',align:'center',minWidth:200, sort: true, filter: true},
        //                 {field: 'accessoryColor', title: '颜色',align:'center',minWidth:80, sort: true, filter: true},
        //                 {field: 'accessoryUnit', title: '单位',align:'center',minWidth:80, sort: true, filter: true},
        //                 {field: 'supplier', title: '供应商',align:'center',minWidth:100, sort: true, filter: true},
        //                 {field: 'price', title: '单价',align:'center',minWidth:100, sort: true, filter: true},
        //                 {field: 'accessoryCount', title: '数量',align:'center',minWidth:100,totalRow:true},
        //                 {field: 'sumMoney', title: '金额',align:'center',minWidth:80,totalRow:true},
        //                 {field: 'id',hide:true}
        //             ];
        //             var fileName = data.reviewType + data.checkNumber + ".xlsx";
        //             fabricAccessoryExcelTable = table.render({
        //                 elem: '#fabricAccessoryExcelTable'
        //                 ,cols: [title]
        //                 ,data: resultData
        //                 ,height: 'full-150'
        //                 ,limit: 1000
        //                 ,totalRow:true
        //                 ,excel:{ // 导出excel配置, （以下值均为默认值）
        //                     filename: fileName
        //                 }
        //                 ,limits: [1000, 2000, 3000]
        //                 ,done:function (res) {
        //                     soulTable.render(this);
        //                 }
        //             });
        //         },
        //         error: function () {
        //             layer.msg("未录入面料信息！", {icon: 2});
        //         }
        //     });
        //     soulTable.export(fabricAccessoryExcelTable);
        // }
        // else if (obj.event === 'exportCheckShare'){
        //     if(data.checkState === "通过") {
        //         var label = "物料审核, 类别:" + data.reviewType + "&emsp;&emsp;&emsp;&emsp; 合同号:" + data.checkNumber;
        //         var tableHtml = "";
        //         var orderDate = $("#orderDate").val();
        //         var deliveryDate = $("#deliveryDate").val();
        //         var partBName = $("#supplierFullName").val();
        //         var partBAddress = $("#supplierAddress").val();
        //         var supplierName = $("#supplierName").val();
        //         var index = layer.open({
        //             type: 1 //Page层类型
        //             , title: label
        //             , btn: ['导出/打印']
        //             , shade: 0.6 //遮罩透明度
        //             , area: '1000px'
        //             , maxmin: false //允许全屏最小化
        //             , anim: 0 //0-6的动画形式，-1不开启
        //             , content: "<div>\n" +
        //                 "        <div class=\"row\">\n" +
        //                 "            <div id=\"materialCheck\">\n" +
        //                 "            </div>\n" +
        //                 "        </div>\n" +
        //                 "        <iframe id=\"printf\" src=\"\" width=\"0\" height=\"0\" frameborder=\"0\"></iframe>\n" +
        //                 "    </div>"
        //             ,yes: function(i, layero){
        //                 printDeal();
        //                 $.ajax({
        //                     url: "/erp/addsupplierinfo",
        //                     type:'POST',
        //                     data: {
        //                         "supplierName": supplierName,
        //                         "supplierFullName": partBName,
        //                         "supplierAddress": partBAddress
        //                     },
        //                     success:function(data){
        //                     }, error:function(){
        //                     }
        //                 });
        //             }
        //         });
        //         layer.full(index);
        //         setTimeout(function () {
        //             $.ajax({
        //                 url: "/erp/getaccessoryprestorebychecknumber",
        //                 type: 'GET',
        //                 data: {
        //                     checkNumber: data.checkNumber
        //                 },
        //                 success: function (res) {
        //                     if (res.accessoryPreStoreList){
        //                         $("#materialCheck").empty();
        //                         tableHtml = "<div class=\"col-md-12\" name='printTable'>\n" +
        //                             "               <section class=\"panel panel-default\" style='page-break-after:always;'>\n" +
        //                             "                   <header class=\"panel-heading font-bold\" style=\"text-align: center\">\n" +
        //                             "                       <div style=\"font-size: 20px;font-weight: 700\">中山市德悦服饰有限公司</div><br>\n" +
        //                             "                       <div style=\"font-size: 16px;font-weight: 700\">购销合同  合同编号: " + data.checkNumber +"</div><br>\n" +
        //                             "                   </header>\n" +
        //                             "                   <table class=\"table table-bordered\" style='width: 100%'>\n" +
        //                             "                       <tbody>";
        //
        //                         tableHtml += "<tr><td colspan='5' style='text-align: left; font-weight: 700; color: black'>甲方(需方):中山市德悦服饰有限公司</td><td colspan='6' style='text-align: left; font-weight: 700; color: black'>签约地点:广东省中山市</td></tr>";
        //                         tableHtml += "<tr><td colspan='5' style='text-align: left; font-weight: 700; color: black'>乙方(供方):"+ partBName +"</td><td colspan='6' style='text-align: left; font-weight: 700; color: black'>签约时间:"+ orderDate +"</td></tr>";
        //                         tableHtml += "<tr><td colspan='10' style='text-align: center; font-weight: 700; color: black'>甲乙双方协商一致,签订以下协议,双方共同遵守</td></tr>";
        //                         tableHtml += "<tr><td colspan='10' style='text-align: left; font-weight: 700; color: black'>一、产品基本信息</td></tr>";
        //                         tableHtml += "<tr><td width='20%'>名称</td><td width='20%'>编号</td><td width='10%'>颜色</td><td width='10%'>规格</td><td width='10%'>单位</td><td width='10%'>数量</td><td width='10%'>单价</td><td width='10%'>金额</td></tr>";
        //                         var pieceSum = 0;
        //                         var moneySum = 0;
        //                         var accessoryList = res.accessoryPreStoreList;
        //                         for (var i=0;i<accessoryList.length;i++){
        //                             tableHtml += "<tr><td>"+accessoryList[i].accessoryName+"</td><td>"+accessoryList[i].accessoryNumber+"</td>";
        //                             if (accessoryList[i].accessoryColor && accessoryList[i].accessoryColor != "null"){
        //                                 tableHtml += "<td>"+accessoryList[i].accessoryColor+"</td>";
        //                             }else {
        //                                 tableHtml += "<td></td>";
        //                             }
        //                             if (accessoryList[i].specification && accessoryList[i].specification != "null"){
        //                                 tableHtml += "<td>"+accessoryList[i].specification+"</td>";
        //                             }else {
        //                                 tableHtml += "<td></td>";
        //                             }
        //                             if (accessoryList[i].accessoryUnit && accessoryList[i].accessoryUnit != "null"){
        //                                 tableHtml += "<td>"+accessoryList[i].accessoryUnit+"</td>";
        //                             }else {
        //                                 tableHtml += "<td></td>";
        //                             }
        //                             if (accessoryList[i].accessoryCount && accessoryList[i].accessoryCount != "null"){
        //                                 tableHtml += "<td>"+accessoryList[i].accessoryCount+"</td>";
        //                             }else {
        //                                 tableHtml += "<td></td>";
        //                             }
        //                             tableHtml += "<td>"+toDecimalFour(accessoryList[i].price)+"</td><td>"+toDecimal(accessoryList[i].sumMoney)+"</td></tr>";
        //                             pieceSum += accessoryList[i].accessoryCount;
        //                             moneySum += accessoryList[i].sumMoney;
        //                         }
        //                         tableHtml += "<tr><td colspan='5' style='text-align:center'>小计：</td><td>"+toDecimal(pieceSum)+"</td><td></td><td>"+toDecimal(moneySum)+"</td></tr>";
        //                         tableHtml += "<tr><td colspan='3' style='text-align:center'>交货日期：</td><td colspan='7' style='text-align:center'>"+ deliveryDate +"</td></tr>";
        //                         tableHtml += "<tr><td colspan='3' style='text-align:center'>交货地址：</td><td colspan='7' style='text-align:center'>中山市沙溪镇康乐北路42号德悦服饰2楼辅料仓</td></tr>";
        //                         tableHtml += "<tr><td colspan='2' rowspan='5' style='text-align:center'>备注</td><td colspan='8'>1.质量要求:质量要求：用料要环保，不能含致癌物质，品质跟样办。</td></tr>";
        //                         tableHtml += "<tr><td colspan='8'>2.包装要求：请务必在每袋或每捆上注明款号、规格、数量。</td></tr>";
        //                         tableHtml += "<tr><td colspan='8'>3.送货单：需注明采购单号及款号并注明产品名称、颜色、规格、数量。</td></tr>";
        //                         tableHtml += "<tr><td colspan='8'>4.供应商收到订单应第一时间回复本司货期，否则作默认同意，供应商必须保证产品的质量和货期，若产品质量不对办，我司将要求无偿更换或退货处理，如不能如期交货，请提前回复我司。</td></tr>";
        //                         tableHtml += "<tr><td colspan='8'>5.本合同需签字回传订单方可生效。。</td></tr>";
        //                         tableHtml += "<tr><td colspan='10' style='text-align: left; font-weight: 700; color: black'>二、其他条款</td></tr>";
        //                         tableHtml += "<tr><td colspan='10' style='text-align: left;'>1、乙方应严格按照《针织T恤衫》国家（一等品）标准，以及甲方订单要求进行加工生产，乙方交至甲方的产品，必须符合甲方的要求。</td></tr>";
        //                         tableHtml += "<tr><td colspan='10' style='text-align: left;'>2、乙方应于订单规定的时间内把甲方采购的（物料/布料）交至甲方指定地点，乙方承担运费及运输过程中的一切风险和法律责任。</td></tr>";
        //                         tableHtml += "<tr><td colspan='10' style='text-align: left;'>3、甲方对货物的验收仅是形式检验，其验收不及于物品的内在质量，乙方仍应对物品质量承担保证责任。如在生产或销售过程中，发现内在质量不合格，造成退货或其他损失，由乙方负全责。</td></tr>";
        //                         tableHtml += "<tr><td colspan='10' style='text-align: left;'>4、如乙方货物出现短缺、规格不符或质量问题等不符合合同约定情形的，甲方有权拒收并视为未交货，乙方应负责于规定时间内，免费更换合格的产品并运送至甲方，如出现特殊情况，需双方协商一致后再另行处理。</td></tr>";
        //                         tableHtml += "<tr><td colspan='10' style='text-align: left;'>5、乙方因生产进度、质量问题等导致交货延期或双方另行协商的货期内仍不能完成的,视为乙方违约,乙方应承担逾期交货违约责任，并按逾期交货金额1% 每日向甲方支付违约金，因此给甲方造成经济损失的，乙方还应予以赔偿。</td></tr>";
        //                         tableHtml += "<tr><td colspan='10' style='text-align: left;'>6、结算方式：月结 _____ 天。（乙方每月需将采购货物签收单原件交付甲方，如乙方代发货至第三方的，同样需将签收单原件交付甲方。）付款期限内，乙方开具13%的增值税发票或相关票据后，甲方付给乙方货款。</td></tr>";
        //                         tableHtml += "<tr><td colspan='10' style='text-align: left;'>7、在合同履行过程中，债权及债务不可转让给第三方。</td></tr>";
        //                         tableHtml += "<tr><td colspan='10' style='text-align: left;'>8、本合同未尽事宜，或在合同执行过程中出现的争执和异议，双方本着友好协商的态度共同解决，协商不成的，任何一方可向甲方所在地人民法院起诉。</td></tr>";
        //                         tableHtml += "<tr><td colspan='10' style='text-align: left;'>9、本合同一式两份，甲乙双方各执一份，具有同等法律效力。</td></tr>";
        //                         tableHtml += "<tr><td colspan='4' style='text-align: left; font-weight: 700; color: black'>甲方:中山市徳悦服饰有限公司</td><td colspan='6' style='text-align: left; font-weight: 700; color: black'>乙方:"+ partBName +"</td></tr>";
        //                         tableHtml += "<tr><td colspan='4' style='text-align: left; font-weight: 700; color: black'>单位地址:中山市沙溪镇康乐北路42号之1二楼</td><td colspan='6' style='text-align: left; font-weight: 700; color: black'>单位地址:"+ partBAddress +"</td></tr>";
        //                         tableHtml += "<tr><td colspan='4' style='text-align: left; font-weight: 700; color: black'>授权代表:</td><td colspan='6' style='text-align: left; font-weight: 700; color: black'>授权代表:</td></tr>";
        //                         tableHtml += "<tr><td colspan='4' style='text-align: left; font-weight: 700; color: black'>电话:</td><td colspan='6' style='text-align: left; font-weight: 700; color: black'>电话:</td></tr>";
        //                         tableHtml +=  "                       </tbody>" +
        //                             "                   </table>" +
        //                             "               </section>" +
        //                             "              </div>";
        //                         $("#materialCheck").append(tableHtml);
        //                     } else{
        //                         layer.msg("辅料信息未录入或者全部已经提交审核");
        //                         layer.close(index);
        //                     }
        //                 }, error: function () {
        //                     layer.msg("未录入面料信息！", {icon: 2});
        //                 }
        //             })
        //         },100);
        //     } else {
        //         layer.msg('该合同中的内容没有全部通过,全部通过后才能打印/导出~~');
        //     }
        // }
        // else if (obj.event === 'changeOrderStateShare'){
        //     if (data.checkState === "通过"){
        //         layer.confirm('确认下单了吗?', function(index){
        //             $.ajax({
        //                 url: "/erp/changeaccessoryprestoreorderstate",
        //                 type: 'POST',
        //                 data: {
        //                     checkNumber: data.checkNumber,
        //                     orderState: "已下单"
        //                 },
        //                 success: function (res) {
        //                     if (res.result == 0) {
        //                         initTable('','','',3);
        //                         layer.close(index);
        //                         layer.msg("确认成功！", {icon: 1});
        //                     } else {
        //                         layer.msg("确认失败！", {icon: 2});
        //                     }
        //                 }, error: function () {
        //                     layer.msg("确认失败！", {icon: 2});
        //                 }
        //             })
        //         });
        //
        //     } else {
        //         layer.msg("该合同还未通过不能确认下单！", {icon: 2});
        //         return false;
        //     }
        // }
        // else if (obj.event === 'progressShare'){
        //
        // }
        // else if (obj.event === 'destroyCheckShare'){
        //     layer.confirm('确认撤回吗?', function(index){
        //         $.ajax({
        //             url: "/erp/changeaccessoryprestorecheckstate",
        //             type: 'POST',
        //             data: {
        //                 checkNumber: data.checkNumber,
        //                 checkState: "待提交"
        //             },
        //             success: function (res) {
        //                 if (res.result == 0) {
        //                     initTable('','','',3);
        //                     layer.close(index);
        //                     layer.msg("提交成功！", {icon: 1});
        //                 } else {
        //                     layer.msg("提交失败！", {icon: 2});
        //                 }
        //             }, error: function () {
        //                 layer.msg("提交失败！", {icon: 2});
        //             }
        //         })
        //     });
        // }
    });
});
function toDecimal(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*100)/100;
    return f;
}
function toDecimalFour(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*10000)/10000;
    return f;
}
function toDecimalSix(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*1000000)/1000000;
    return f;
}
function dateFormat(fmt, date) {
    var ret;
    var opt = {
        "Y+": date.getFullYear().toString(),        // 年
        "m+": (date.getMonth() + 1).toString(),     // 月
        "d+": date.getDate().toString(),            // 日
        "H+": date.getHours().toString(),           // 时
        "M+": date.getMinutes().toString(),         // 分
        "S+": date.getSeconds().toString()          // 秒
        // 有其他格式化字符需求可以继续添加，必须转化成字符串
    };
    for (var k in opt) {
        ret = new RegExp("(" + k + ")").exec(fmt);
        if (ret) {
            fmt = fmt.replace(ret[1], (ret[1].length == 1) ? (opt[k]) : (opt[k].padStart(ret[1].length, "0")))
        }
    }
    return fmt;
}
function printDeal(){
    $("#printf").empty();
    var printBoxs = document.getElementsByName('printTable');
    var newContent = '';
    for(var i=0;i<printBoxs.length;i++) {
        newContent += printBoxs[i].innerHTML;
    }
    var f = document.getElementById('printf');
    // f.contentDocument.write('<link rel="stylesheet" type="text/css" href="/css/app.v2.css">');
    f.contentDocument.write('<style>' +
        'table {' +
        'border-collapse:collapse;' +
        '}' +
        'td {\n' +
        '        border: 1px solid #000000;\n' +
        '    }'+
        '</style>');
    f.contentDocument.write(newContent);
    f.contentDocument.close();
    window.frames['printf'].focus();
    try{
        window.frames['printf'].print();
    }catch(err){
        f.contentWindow.print();
    }
}
function getFormatDate() {
    var date = new Date();
    var month = date.getMonth() + 1;
    var strDate = date.getDate();
    if (month >= 1 && month <= 9) {
        month = "0" + month;
    }
    if (strDate >= 0 && strDate <= 9) {
        strDate = "0" + strDate;
    }
    var currentDate = date.getFullYear() + "-" + month + "-" + strDate;
    return currentDate;
}
function reloadInfo() {
    $.ajax({
        url: "/erp/getchecknumbercount",
        type: 'GET',
        data: {},
        success: function (res) {
            if (res.checkNumberCount != null) {
                $("#checkNumberCount").text(res.checkNumberCount);
            } else {
                layer.msg("获取失败！", {icon: 2});
            }
        }, error: function () {
            layer.msg("获取失败！", {icon: 2});
        }
    });
}