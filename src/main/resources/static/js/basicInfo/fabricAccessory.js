layui.use('form', function(){
    var form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
    form.render();
});
var userName = $("#userName").val();
var userRole = $("#userRole").val();
layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.21'
}).extend({                         // 模块别名
    soulTable: 'soulTable',
});

var fabricAccessoryTable;

$(document).on('keydown', '.layui-table-edit', function (e) {
    var td = $(this).parent('td')
        ,tr = td.parent('tr')
        ,trs = tr.parent().parent().find('tr')
        ,tr_index = tr.index()
        ,td_index = td.index()
        ,td_last_index = tr.find('[data-edit="text"]:last').index()
        ,td_first_index = tr.find('[data-edit="text"]:first').index();
    switch (e.keyCode) {
        case 13:
            tr.next().find('td').eq(td_index).click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            return false;
        case 39:
            td.nextAll('[data-edit="text"]:first').click();
            if (td_index == td_last_index){
                tr.next().find('td').eq(td_first_index).click();
                if (tr_index == trs.length - 1){
                    trs.eq(0).find('td').eq(td_first_index).click();
                }
            }
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
        case 37:
            td.prevAll('[data-edit="text"]:first').click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
        case 38:
            tr.prev().find('td').eq(td_index).click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
        case 40:
            tr.next().find('td').eq(td_index).click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
    }

})

layui.use(['form', 'soulTable', 'table'], function () {

    var table = layui.table,
        soulTable = layui.soulTable,
        $ = layui.$;
    var load = layer.load();

    fabricAccessoryTable = table.render({
        elem: '#fabricAccessoryTable'
        ,url:'getoneyearordersummarylay'
        ,method:'post'
        ,cols: [[]]
        ,loading:true
        ,done: function (res, curr, count) {
            table.init('fabricAccessoryTable',{//转换成静态表格
                cols:[[
                    {title: '#', width: 50, collapse: true,lazy: true, children:[
                            {
                                title: '面料信息'
                                ,url: 'getmanufacturefabricbyorder'
                                ,where: function(row){
                                    return {orderName: row.orderName}
                                }
                                ,loading:true
                                ,totalRow: true
                                ,page: false
                                ,cols: [[
                                    {field: 'orderName', title: '订单',align:'center', minWidth: 120},
                                    {field: 'clothesVersionNumber', title: '版单',align:'center', minWidth: 120},
                                    {field: 'fabricName', title: '面料名称',align:'center', minWidth: 165, totalRowText: '合计'},
                                    {field: 'fabricNumber', title: '面料号',align:'center', minWidth: 90},
                                    {field: 'fabricWidth', title: '布封',align:'center', minWidth: 90},
                                    {field: 'fabricWeight', title: '克重',align:'center', minWidth: 90},
                                    {field: 'unit', title: '单位',align:'center', minWidth: 80},
                                    {field: 'fabricDeflation', title: '面料直缩',align:'center', minWidth: 120},
                                    {field: 'fabricShrink', title: '面料横缩',align:'center', minWidth: 120},
                                    {field: 'clothesDeflation', title: '成衣直缩',align:'center', minWidth: 120},
                                    {field: 'clothesShrink', title: '成衣横缩',align:'center', minWidth: 120},
                                    {field: 'partName', title: '部位',align:'center', minWidth: 90},
                                    {field: 'orderPieceUsage', title: '接单用量',align:'center', minWidth: 90},
                                    {field: 'pieceUsage', title: '单件用量',align:'center', minWidth: 80},
                                    {field: 'colorName', title: '订单颜色',align:'center', minWidth: 100},
                                    {field: 'isHit', title: '是否撞色',align:'center', minWidth: 90},
                                    {field: 'fabricColor', title: '面料颜色',align:'center', minWidth: 120},
                                    {field: 'fabricColorNumber', title: '面料色号',align:'center', minWidth: 120},
                                    {field: 'fabricLoss', title: '损耗(%)',align:'center', minWidth: 100},
                                    {field: 'supplier', title: '供应商',align:'center', minWidth: 120},
                                    {field: 'orderCount', title: '订单量',align:'center', minWidth: 90},
                                    {field: 'fabricAdd', title: '上浮',align:'center', minWidth: 90},
                                    {field: 'fabricCount', title: '订购量',align:'center', minWidth: 120, totalRow: true},
                                    {field: 'fabricActCount', title: '实际订购',align:'center', minWidth: 120, totalRow: true},
                                    {field: 'price', title: '单价',align:'center', minWidth: 120},
                                    {field: 'sumMoney', title: '总价',align:'center', minWidth: 120, totalRow: true},
                                    {field: 'checkState', title: '审核状态',align:'center', minWidth: 120, totalRow: true},
                                    {field: 'fabricID', title: 'fabricID',hide:true}
                                ]]
                                ,done: function () {
                                    soulTable.render(this);
                                }
                                ,filter: {
                                    bottom: false,
                                    items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                                }
                            }, {
                                title: '辅料信息'
                                ,url: 'getmanufactureaccessorybyorder'
                                ,where: function(row){
                                    return {orderName: row.orderName}
                                }
                                ,page: false
                                ,totalRow: true
                                ,cols: [[
                                    {field: 'orderName', title: '订单',align:'center', minWidth: 120},
                                    {field: 'clothesVersionNumber', title: '版单',align:'center', minWidth: 120},
                                    {field: 'accessoryName', title: '辅料名称',align:'center', minWidth: 165, totalRowText: '合计'},
                                    {field: 'accessoryNumber', title: '辅料编号',align:'center', minWidth: 165},
                                    {field: 'specification', title: '辅料规格',align:'center', minWidth: 90},
                                    {field: 'accessoryUnit', title: '辅料单位',align:'center', minWidth: 90},
                                    {field: 'accessoryColor', title: '辅料颜色',align:'center', minWidth: 80},
                                    {field: 'orderPieceUsage', title: '接单用量',align:'center', minWidth: 120},
                                    {field: 'pieceUsage', title: '单件用量',align:'center', minWidth: 120},
                                    {field: 'sizeName', title: '尺码',align:'center', minWidth: 120},
                                    {field: 'colorName', title: '颜色',align:'center', minWidth: 120},
                                    {field: 'supplier', title: '供应商',align:'center', minWidth: 120},
                                    {field: 'orderCount', title: '订单数量',align:'center', minWidth: 90},
                                    {field: 'accessoryAdd', title: '上浮',align:'center', minWidth: 90},
                                    {field: 'accessoryCount', title: '订购量',align:'center', minWidth: 90, totalRow: true},
                                    {field: 'price', title: '单价',align:'center', minWidth: 90},
                                    {field: 'sumMoney', title: '总价',align:'center', minWidth: 90, totalRow: true},
                                    {field: 'checkState', title: '审核状态',align:'center', minWidth: 120, totalRow: true},
                                    {field: 'accessoryID', title: 'accessoryID',hide:true}
                                ]]
                                ,done: function () {
                                    soulTable.render(this);
                                }
                                ,filter: {
                                    bottom: false,
                                    items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                                }
                            }
                        ]}
                    ,{type:'checkbox'}
                    ,{type:'numbers', align:'center', title:'序号', width:80}
                    ,{field:'orderName', title:'款号', align:'center', width:200, sort: true, filter: true, totalRowText: '合计'}
                    ,{field:'clothesVersionNumber', title:'版单号', align:'center', width:200, sort: true, filter: true}
                    ,{field:'customerName', title:'品牌', align:'center', width:150, sort: true, filter: true}
                    ,{field:'orderSum', title:'订单量', align:'center', width:120, sort: true, filter: true, totalRow: true}
                    ,{field:'styleDescription', title:'款式描述', align:'center',width:240, sort: true, filter: true}
                    ,{field:'season', title:'季度', align:'center', width:120, sort: true, filter: true}
                    ,{field:'deadLine', title:'收单日期', align:'center', width:120, sort: true, filter: true, templet:function (d) {
                            return moment(d.deadLine).format("YYYY-MM-DD");
                        }}
                    ,{field:'userName', title:'跟单', align:'center', width:120, sort: true, filter: true}
                    // ,{fixed: 'right', title:'操作', align:'center', toolbar: '#barTop', width:120}
                ]]
                ,data:res.data
                ,height: 'full-50'
                ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                ,defaultToolbar: ['filter', 'print']
                ,title: '用户数据表'
                ,totalRow: true
                ,even: true
                ,page: true
                ,limits: [50, 100, 200]
                ,limit: 100 //每页默认显示的数量
                ,done: function (res, curr, count) {
                    soulTable.render(this);
                    layer.close(load);
                }
                ,filter: {
                    bottom: true,
                    items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                }
            });
        }
    });

    table.on('toolbar(fabricAccessoryTable)', function(obj){
        if (obj.event === 'clearFilter') {
            // 清除所有筛选条件并重载表格
            // 参数: tableId
            soulTable.clearFilter('fabricAccessoryTable')
        } else if (obj.event === 'exportExcel') {
            soulTable.export('fabricAccessoryTable');
        }else if (obj.event === "addFabricPlaceOrder") {   //面料下单
            var checkStatus = table.checkStatus(obj.config.id); //获取选中行状态
            if(checkStatus.data.length == 0) {
                layer.msg('请先选择订单');
            }else {
                var orderNameList = [];
                var nowDate = dateFormat("YYYY-mm-dd HH:MM", new Date()).replace(" ", "").replace(":","").replace(/\-/g,'');
                $.each(checkStatus.data, function (index, item) {
                    orderNameList.push(item.orderName);
                });
                var label = "面料下单" + "&emsp;&emsp;&emsp;&emsp;(该页显示的是未提交审核的面料信息，如果已提交请先在反审中撤回后再修改)";
                var index = layer.open({
                    type: 1 //Page层类型
                    , title: label
                    , btn: ['保存合同并审核','只保存']
                    , shade: 0.6 //遮罩透明度
                    , area: '1000px'
                    , maxmin: false //允许全屏最小化
                    , anim: 0 //0-6的动画形式，-1不开启
                    , content: $("#fabricOrderAdd")
                    ,yes: function(i, layero){
                        var fabricOrderData = table.cache.fabricOrderTable;
                        var manufactureFabrics = [];
                        $.each(fabricOrderData,function(i,item){
                            if(item.supplier.getValue()[0] != undefined) {
                                manufactureFabrics.push({
                                    supplier:item.supplier.getValue()[0].value,
                                    price:item.price,
                                    orderCount:item.orderCount,
                                    fabricCount:item.fabricCount,
                                    fabricActCount:item.fabricActCount,
                                    fabricAdd:item.fabricAdd,
                                    sumMoney:item.sumMoney,
                                    fabricID:item.fabricID,
                                    pieceUsage:item.pieceUsage,
                                    checkState:'已提交',
                                    checkNumber:nowDate
                                })
                            }
                        });

                        $.ajax({
                            url: "/erp/addmanufacturefabricplaceorderbatch",
                            type: 'POST',
                            data: {
                                manufactureFabricJson:JSON.stringify(manufactureFabrics)
                            },
                            success: function (res) {
                                if (res.result == 0) {
                                    fabricAccessoryTable.reload();
                                    layer.close(index);
                                    layer.msg("录入成功！", {icon: 1});
                                } else {
                                    layer.msg("录入失败！", {icon: 2});
                                }
                            },
                            error: function () {
                                layer.msg("录入失败！", {icon: 2});
                            }
                        })
                    }
                    ,btn2: function(i, layero){
                        var fabricOrderData = table.cache.fabricOrderTable;
                        var manufactureFabrics = [];
                        $.each(fabricOrderData,function(i,item){
                            if(item.supplier.getValue()[0] != undefined) {
                                manufactureFabrics.push({
                                    supplier:item.supplier.getValue()[0].value,
                                    price:item.price,
                                    orderCount:item.orderCount,
                                    fabricCount:item.fabricCount,
                                    fabricActCount:item.fabricActCount,
                                    fabricAdd:item.fabricAdd,
                                    sumMoney:item.sumMoney,
                                    fabricID:item.fabricID,
                                    pieceUsage:item.pieceUsage,
                                    checkState:'未提交'
                                })
                            }
                        });

                        $.ajax({
                            url: "/erp/addmanufacturefabricplaceorderbatch",
                            type: 'POST',
                            data: {
                                manufactureFabricJson:JSON.stringify(manufactureFabrics)
                            },
                            success: function (res) {
                                if (res.result == 0) {
                                    fabricAccessoryTable.reload();
                                    layer.close(index);
                                    layer.msg("录入成功！", {icon: 1});
                                } else {
                                    layer.msg("录入失败！", {icon: 2});
                                }
                            },
                            error: function () {
                                layer.msg("录入失败！", {icon: 2});
                            }
                        })
                    }
                });
                layer.full(index);
                setTimeout(function () {
                    $.ajax({
                        url: "/erp/getmanufacturefabricplaceorderinfobyorderlist",
                        type: 'GET',
                        data: {
                            orderNameList:orderNameList
                        },
                        traditional: true,
                        success: function (res) {
                            var title = [
                                {field: 'orderName', title: '款号',align:'center',minWidth:140, sort: true, filter: true},
                                {field: 'fabricName', title: '面料名称',align:'center',minWidth:200, sort: true, filter: true},
                                {field: 'unit', title: '单位',align:'center',minWidth:80, sort: true, filter: true},
                                {field: 'partName', title: '部位',align:'center',minWidth:80, sort: true, filter: true},
                                {field: 'colorName', title: '订单颜色',align:'center',minWidth:100, sort: true, filter: true},
                                {field: 'fabricColor', title: '面料颜色',align:'center',minWidth:100, sort: true, filter: true},
                                {field: 'supplier', title: '供应商',align:'center',minWidth:200,templet: function(d){
                                        return '<div id="supplier-'+d.LAY_TABLE_INDEX+'" class="xm-select-demo"></div>'
                                    }},
                                {field: 'pieceUsage', title: '单件用量',align:'center',minWidth:80,edit:'text'},
                                {field: 'orderCount', title: '订单量',align:'center',minWidth:70},
                                {field: 'fabricAdd', title: '上浮(%)',align:'center',minWidth:100,edit:'text'},
                                {field: 'fabricCount', title: '订布量',align:'center',minWidth:90},
                                {field: 'fabricActCount', title: '实际订布',align:'center',minWidth:100,edit:'text'},
                                {field: 'price', title: '单价',align:'center',minWidth:70,edit:'text'},
                                {field: 'sumMoney', title: '总价',align:'center',minWidth:70},
                                {field: 'fabricID', title: 'fabricID',hide:true}
                            ];
                            var fabricData = [];
                            var selectedSupplier = [];
                            if (res.manufactureFabricList && res.orderClothesList){
                                if (res.manufactureFabricList.length == 0){
                                    layer.msg("面料信息未录入或者全部已经提交审核");
                                    layer.close(index);
                                }
                                $.each(res.manufactureFabricList,function(index1,value1){
                                    var thisFabricData = {};
                                    thisFabricData.fabricID = value1.fabricID;
                                    thisFabricData.orderName = value1.orderName;
                                    thisFabricData.fabricName = value1.fabricName;
                                    thisFabricData.unit = value1.unit;
                                    thisFabricData.partName = value1.partName;
                                    thisFabricData.colorName = value1.colorName;
                                    thisFabricData.isHit = value1.isHit;
                                    thisFabricData.fabricColor = value1.fabricColor;

                                    if (value1.supplier != null){
                                        var tmpSupp = {};
                                        tmpSupp.name = value1.supplier;
                                        tmpSupp.value = value1.supplier;
                                        selectedSupplier.push(tmpSupp)
                                    } else {
                                        var tmpSupp = {};
                                        tmpSupp.name = '';
                                        tmpSupp.value = '';
                                        selectedSupplier.push(tmpSupp)
                                    }
                                    thisFabricData.pieceUsage = value1.pieceUsage;
                                    thisFabricData.orderCount = 0;
                                    $.each(res.orderClothesList,function(index2,value2){
                                        if (thisFabricData.colorName === value2.colorName && value1.orderName === value2.orderName){
                                            thisFabricData.orderCount = value2.orderSum;
                                        }
                                    });
                                    thisFabricData.fabricAdd = value1.fabricAdd;
                                    thisFabricData.fabricCount = toDecimal((1 + value1.fabricAdd/100)*thisFabricData.pieceUsage * thisFabricData.orderCount);
                                    if (value1.fabricActCount != 0){
                                        thisFabricData.fabricActCount = toDecimal(value1.fabricActCount);
                                    } else {
                                        thisFabricData.fabricActCount = toDecimal(thisFabricData.fabricCount);
                                    }
                                    thisFabricData.price = value1.price;
                                    thisFabricData.sumMoney = toDecimal(thisFabricData.fabricCount * thisFabricData.price);
                                    fabricData.push(thisFabricData);
                                });
                            } else{
                                layer.msg("面料信息未录入或者全部已经提交审核");
                                layer.close(index);
                            }
                            var fabricSupplierList = [];
                            if (res.supplierList){
                                $.each(res.supplierList,function (index, item) {
                                    fabricSupplierList.push(
                                        {name: item, value: item}
                                    )
                                })
                            }
                            table.render({
                                elem: '#fabricOrderTable'
                                ,cols: [title]
                                ,data: fabricData
                                ,height: 'full-150'
                                ,limit: Number.MAX_VALUE //每页默认显示的数量
                                ,filter: {
                                    // items:['column','data','condition','editCondition','clearCache'],
                                    cache: false
                                }
                                ,done:function (res) {
                                    //渲染多选
                                    $.each(res.data,function (index, item) {

                                        var changeSupplier = fabricSupplierList;
                                        for (var i = 0; i < changeSupplier.length; i++){
                                            changeSupplier[i]['selected'] = false;
                                        }
                                        $("#supplier-"+item.LAY_TABLE_INDEX).parent().css("overflow","unset");
                                        $("#supplier-"+item.LAY_TABLE_INDEX).parent().css("height","auto");

                                        if (selectedSupplier[index]['name'] != ''){
                                            if (selectedSupplier[index]['name'].indexOf('创建') != -1){
                                                var tmpBed = {};
                                                tmpBed.name = selectedSupplier[index]['name'];
                                                tmpBed.value = selectedSupplier[index]['value'];
                                                tmpBed.selected = true;
                                                changeSupplier.push(tmpBed);
                                            } else {
                                                for (var j = 0; j < changeSupplier.length; j ++){
                                                    if (changeSupplier[j]['name'] == selectedSupplier[index]['name']){
                                                        changeSupplier[j]['selected'] = true;
                                                    }
                                                }
                                            }
                                        }

                                        var sxm = xmSelect.render({
                                            el: '#supplier-' + item.LAY_TABLE_INDEX,
                                            autoRow: true,
                                            radio: true,
                                            clickClose: true,
                                            filterable: true,
                                            create: function(val, arr){
                                                if(arr.length === 0){
                                                    return {
                                                        name: '创建-' + val,
                                                        value: val
                                                    }
                                                }
                                            },
                                            model: {
                                                icon: 'hidden',
                                                label: {
                                                    type: 'text'
                                                }
                                            },
                                            data: changeSupplier
                                        });
                                        item.supplier = sxm;
                                    });
                                    $(".layui-table-main  tr").each(function (index ,val) {
                                        $($(".layui-table-fixed .layui-table-body tbody tr")[index]).height($(val).height());
                                    });
                                    soulTable.render(this);
                                }
                            });
                            //监听单元格编辑
                            table.on('edit(fabricOrderTable)', function(obj){
                                var tmpData = table.cache.fabricOrderTable;
                                var selectedSupplier = [];
                                $.each(tmpData,function (index,item) {
                                    var tmpSupplier = {};
                                    if (item.supplier.getValue()[0] != undefined){
                                        var xxx = item.supplier.getValue()[0];
                                        for (var i in xxx){
                                            tmpSupplier[i] = xxx[i];
                                        }
                                    }else {
                                        tmpSupplier.name = '';
                                        tmpSupplier.value = '';
                                    }
                                    selectedSupplier.push(tmpSupplier);
                                });
                                var value = obj.value //得到修改后的值
                                    ,data = obj.data //得到所在行所有键值
                                    ,field = obj.field; //得到字段
                                if (field == 'fabricAdd'){
                                    $.each(fabricData,function(index1,value1){
                                        if (value1.fabricID == data.fabricID){
                                            fabricData[index1].fabricAdd = value;
                                            fabricData[index1].fabricCount = toDecimal((1 + value/100) * data.pieceUsage * data.orderCount);
                                            if (fabricData[index1].fabricActCount == 0){
                                                fabricData[index1].fabricActCount = toDecimal((1 + value/100) * data.pieceUsage * data.orderCount);
                                            }
                                            fabricData[index1].sumMoney = toDecimal((1 + value/100) * data.pieceUsage * data.orderCount * data.price);
                                        }
                                    })
                                } else if (field == 'price'){
                                    $.each(fabricData,function(index1,value1){
                                        if (value1.fabricID == data.fabricID){
                                            fabricData[index1].price = value;
                                            fabricData[index1].sumMoney = toDecimal(data.fabricActCount * value);
                                        }
                                    })
                                } else if (field == 'fabricActCount'){
                                    $.each(fabricData,function(index1,value1){
                                        if (value1.fabricID == data.fabricID){
                                            fabricData[index1].fabricActCount = value;
                                            fabricData[index1].sumMoney = toDecimal(data.price * value);
                                        }
                                    })
                                }else if (field == 'pieceUsage'){
                                    $.each(fabricData,function(index1,value1){
                                        if (value1.fabricID == data.fabricID){
                                            fabricData[index1].pieceUsage = value;
                                            fabricData[index1].fabricCount = toDecimal((1 + data.fabricAdd/100) * value * data.orderCount);
                                            if (fabricData[index1].fabricActCount == 0){
                                                fabricData[index1].fabricActCount = toDecimal((1 + data.fabricAdd/100) * value * data.orderCount);
                                            }
                                            fabricData[index1].sumMoney = toDecimal((1 + data.fabricAdd/100) * value * data.orderCount * data.price);
                                        }
                                    });
                                }
                                table.reload("fabricOrderTable",{
                                    data:fabricData   // 将新数据重新载入表格
                                    ,done:function (res) {
                                        //渲染多选
                                        $.each(res.data,function (index, item) {

                                            var changeSupplier = fabricSupplierList;
                                            for (var i = 0; i < changeSupplier.length; i++){
                                                changeSupplier[i]['selected'] = false;
                                            }
                                            $("#supplier-"+item.LAY_TABLE_INDEX).parent().css("overflow","unset");
                                            $("#supplier-"+item.LAY_TABLE_INDEX).parent().css("height","auto");
                                            if (selectedSupplier[index]['name'] != ''){
                                                if (selectedSupplier[index]['name'].indexOf('创建') != -1){
                                                    var tmpBed = {};
                                                    tmpBed.name = selectedSupplier[index]['name'];
                                                    tmpBed.value = selectedSupplier[index]['value'];
                                                    tmpBed.selected = true;
                                                    changeSupplier.push(tmpBed);
                                                } else {
                                                    for (var j = 0; j < changeSupplier.length; j ++){
                                                        if (changeSupplier[j]['name'] == selectedSupplier[index]['name']){
                                                            changeSupplier[j]['selected'] = true;
                                                        }
                                                    }
                                                }
                                            }
                                            var sxm = xmSelect.render({
                                                el: '#supplier-' + item.LAY_TABLE_INDEX,
                                                autoRow: true,
                                                radio: true,
                                                clickClose: true,
                                                filterable: true,
                                                create: function(val, arr){
                                                    if(arr.length === 0){
                                                        return {
                                                            name: '创建-' + val,
                                                            value: val
                                                        }
                                                    }
                                                },
                                                model: {
                                                    icon: 'hidden',
                                                    label: {
                                                        type: 'text'
                                                    }
                                                },
                                                data: changeSupplier
                                            });
                                            item.supplier = sxm;
                                        });
                                        $(".layui-table-main  tr").each(function (index ,val) {
                                            $($(".layui-table-fixed .layui-table-body tbody tr")[index]).height($(val).height());
                                        });
                                    }
                                });
                            });

                        },
                        error: function () {
                            layer.msg("未录入面料信息！", {icon: 2});
                        }
                    })
                },100);

            }
        }else if (obj.event === "addAccessoryPlaceOrder") {   //面料下单
            var checkStatus = table.checkStatus(obj.config.id); //获取选中行状态
            if(checkStatus.data.length == 0) {
                layer.msg('请先选择订单');
            }else {
                var orderNameList = [];
                $.each(checkStatus.data, function (index, item) {
                    orderNameList.push(item.orderName);
                });
                var accessoryData = [];
                var label = "辅料下单" + "&emsp;&emsp;&emsp;&emsp;(该页显示的是未提交审核的面料信息，如果已提交请先在反审中撤回后再修改)";
                var index = layer.open({
                    type: 1 //Page层类型
                    , title: label
                    , btn: ['保存并提交审核','只保存','计算']
                    , shade: 0.6 //遮罩透明度
                    , area: '1000px'
                    , maxmin: false //允许全屏最小化
                    , anim: 0 //0-6的动画形式，-1不开启
                    , content: $("#accessoryOrderAdd")
                    ,yes: function(i, layero){
                        var accessoryOrderData = table.cache.accessoryOrderTable;
                        var manufactureAccessories = [];
                        $.each(accessoryOrderData,function(i,item){
                            if (item.LAY_CHECKED){
                                manufactureAccessories.push({
                                    supplier:item.supplier,
                                    price:item.price,
                                    orderCount:item.orderCount,
                                    accessoryCount:item.accessoryCount,
                                    accessoryAdd:item.accessoryAdd,
                                    sumMoney:item.sumMoney,
                                    accessoryUnit:item.accessoryUnit,
                                    accessoryName:item.accessoryName,
                                    accessoryNumber:item.accessoryNumber,
                                    specification:item.specification,
                                    accessoryID:item.accessoryID,
                                    pieceUsage:item.pieceUsage,
                                    checkState:'已提交'
                                })
                            }
                        });

                        $.ajax({
                            url: "/erp/addmanufactureaccessoryplaceorderbatch",
                            type: 'POST',
                            data: {
                                manufactureAccessoryJson:JSON.stringify(manufactureAccessories),
                                userName: userName
                            },
                            success: function (res) {
                                if (res.result == 0) {
                                    fabricAccessoryTable.reload();
                                    layer.close(index);
                                    layer.msg("录入成功", {icon: 1});
                                } else {
                                    layer.msg("录入失败！", {icon: 2});
                                }
                            },
                            error: function () {
                                layer.msg("录入失败！", {icon: 2});
                            }
                        })
                    }
                    ,btn2: function(i, layero){
                        var accessoryOrderData = table.cache.accessoryOrderTable;
                        var manufactureAccessories = [];
                        $.each(accessoryOrderData,function(i,item){
                            if (item.LAY_CHECKED){
                                manufactureAccessories.push({
                                    supplier:item.supplier,
                                    price:item.price,
                                    orderCount:item.orderCount,
                                    accessoryUnit:item.accessoryUnit,
                                    accessoryName:item.accessoryName,
                                    accessoryNumber:item.accessoryNumber,
                                    specification:item.specification,
                                    accessoryCount:item.accessoryCount,
                                    accessoryAdd:item.accessoryAdd,
                                    sumMoney:item.sumMoney,
                                    accessoryID:item.accessoryID,
                                    pieceUsage:item.pieceUsage,
                                    checkState:'未提交'
                                })
                            }
                        });

                        $.ajax({
                            url: "/erp/addmanufactureaccessoryplaceorderbatch",
                            type: 'POST',
                            data: {
                                manufactureAccessoryJson:JSON.stringify(manufactureAccessories),
                                userName: userName
                            },
                            success: function (res) {
                                if (res.result == 0) {
                                    fabricAccessoryTable.reload();
                                    layer.close(index);
                                    layer.msg("录入成功！", {icon: 1});
                                } else {
                                    layer.msg("录入失败！", {icon: 2});
                                }
                            },
                            error: function () {
                                layer.msg("录入失败！", {icon: 2});
                            }
                        })
                    }
                    ,btn3: function(index, layero){
                        var tmpData = table.cache.accessoryOrderTable;
                        $.each(tmpData,function(index1,value1){
                            if (value1.pieceUsage == 0 || value1.orderCount == 0){
                                value1.accessoryCount = 0;
                            } else {
                                value1.accessoryCount = toDecimal((1 + value1.accessoryAdd/100) * value1.pieceUsage * value1.orderCount);
                            }
                            if (value1.price == 0 || value1.accessoryCount == 0){
                                value1.sumMoney = 0;
                            } else {
                                value1.sumMoney = toDecimal(value1.accessoryCount * value1.price);
                            }
                        });
                        table.reload("accessoryOrderTable",{
                            data:tmpData   // 将新数据重新载入表格
                        });
                        layui.form.render("select");
                        return false  //开启该代码可禁止点击该按钮关闭
                    }
                    ,cancel: function(){
                        accessoryData = [];
                    }
                });
                layer.full(index);
                setTimeout(function () {
                    $.ajax({
                        url: "/erp/getmanufactureaccessoryplaceorderinfoorderlist",
                        type: 'GET',
                        data: {
                            orderNameList:orderNameList
                        },
                        traditional: true,
                        success: function (res) {
                            var title = [
                                {type:'checkbox'},
                                {field: 'orderName', title: '款号',align:'center',minWidth:120, sort: true, filter: true},
                                {field: 'accessoryName', title: '辅料名称',align:'center',minWidth:100,edit:'text', sort: true, filter: true},
                                {field: 'accessoryNumber', title: '编号',align:'center',minWidth:80,edit:'text', sort: true, filter: true},
                                {field: 'specification', title: '辅料规格',align:'center',minWidth:80,edit:'text', sort: true, filter: true},
                                {field: 'accessoryUnit', title: '单位',align:'center',minWidth:80,edit:'text', sort: true, filter: true},
                                {field: 'accessoryColor', title: '辅料颜色',align:'center',minWidth:80, sort: true, filter: true},
                                {field: 'pieceUsage', title: '单件用量',align:'center',minWidth:80,edit:'text'},
                                {field: 'sizeName', title: '尺码',align:'center',minWidth:100},
                                {field: 'colorName', title: '颜色',align:'center',minWidth:120},
                                {field: 'supplier', title: '供应商',align:'center',minWidth:100,edit:'text', sort: true, filter: true},
                                {field: 'orderCount', title: '订单量',align:'center',minWidth:70,fixed: 'right'},
                                {field: 'accessoryAdd', title: '上浮(%)',align:'center',minWidth:70,edit:'text',fixed: 'right'},
                                {field: 'accessoryCount', title: '订购量',align:'center',minWidth:70,edit:'text',fixed: 'right'},
                                {field: 'price', title: '单价',align:'center',minWidth:60,edit:'text',fixed: 'right'},
                                {field: 'sumMoney', title: '总价',align:'center',minWidth:60,edit:'text',fixed: 'right'},
                                {field: 'accessoryID', title: 'accessoryID',hide:true}
                            ];

                            if (res.manufactureAccessoryList){
                                if (res.manufactureAccessoryList.length == 0){
                                    layer.msg("辅料信息未录入或者全部已经提交审核");
                                    layer.close(index);
                                }
                                $.each(res.manufactureAccessoryList,function(index1,value1){
                                    var thisAccessoryData = {};
                                    thisAccessoryData.accessoryID = value1.accessoryID;
                                    thisAccessoryData.orderName = value1.orderName;
                                    thisAccessoryData.accessoryName = value1.accessoryName;
                                    thisAccessoryData.accessoryNumber = value1.accessoryNumber;
                                    thisAccessoryData.specification = value1.specification;
                                    thisAccessoryData.accessoryUnit = value1.accessoryUnit;
                                    thisAccessoryData.accessoryColor = value1.accessoryColor;
                                    thisAccessoryData.pieceUsage = value1.pieceUsage;
                                    thisAccessoryData.sizeName = value1.sizeName;
                                    thisAccessoryData.colorName = value1.colorName;
                                    thisAccessoryData.supplier = value1.supplier;
                                    thisAccessoryData.orderCount = value1.orderCount;
                                    if (value1.accessoryAdd != null){
                                        thisAccessoryData.accessoryAdd = value1.accessoryAdd;
                                    } else {
                                        thisAccessoryData.accessoryAdd = 0;
                                    }
                                    if (value1.accessoryCount != null){
                                        thisAccessoryData.accessoryCount = value1.accessoryCount;
                                    } else {
                                        thisAccessoryData.accessoryCount = thisAccessoryData.pieceUsage * thisAccessoryData.orderCount;
                                    }
                                    if (value1.price != null){
                                        thisAccessoryData.price = value1.price;
                                    } else {
                                        thisAccessoryData.price = 0;
                                    }
                                    if (value1.sumMoney != null){
                                        thisAccessoryData.sumMoney = value1.sumMoney;
                                    } else {
                                        thisAccessoryData.sumMoney = thisAccessoryData.price * thisAccessoryData.orderCount;
                                    }
                                    accessoryData.push(thisAccessoryData);
                                });
                            } else{
                                layer.msg("辅料信息未录入或者全部已经提交审核");
                                layer.close(index);
                            }
                            table.render({
                                elem: '#accessoryOrderTable'
                                ,cols: [title]
                                ,data: accessoryData
                                ,height: 'full-150'
                                ,loading: true
                                ,limit: Number.MAX_VALUE //每页默认显示的数量
                                ,filter: {
                                    // items:['column','data','condition','editCondition','clearCache'],
                                    cache: false
                                }
                                ,done:function (res) {
                                    soulTable.render(this);
                                }
                            });
                        },
                        error: function () {
                            layer.msg("未录入辅料信息！", {icon: 2});
                        }
                    })
                },100);

            }
        }
    });

});


function toDecimal(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*100)/100;
    return f;
}
