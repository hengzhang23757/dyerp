var form;
var userName = $("#userName").val();
var userRole = $("#userRole").val();
layui.use('form', function(){
    form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
    form.render();
});
layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.21'
}).extend({                         // 模块别名
    soulTable: 'soulTable',
});
var fabricTable,accessoryTable,shareAccessoryTable,bjTable;

layui.use(['form', 'soulTable', 'table'], function () {
    var table = layui.table,
        soulTable = layui.soulTable,
        $ = layui.$;

    fabricTable = table.render({
        elem: '#fabricTable'
        ,cols:[[]]
        ,data:[]
        ,loading:false
        ,height: 'full-100'
        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
        ,page: true
        ,title: '面料审核表'
        ,totalRow: true
        ,even: true
        ,overflow: 'tips'
        ,limit:50
        ,limits:[50, 100, 200]
        ,done: function (res, curr, count) {
            soulTable.render(this);
        }
    });
    accessoryTable = table.render({
        elem: '#accessoryTable'
        ,cols:[[]]
        ,data:[]
        ,loading:false
        ,height: 'full-100'
        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
        ,page: true
        ,title: '辅料审核表'
        ,totalRow: true
        ,even: true
        ,overflow: 'tips'
        ,limit:50
        ,limits:[50, 100, 200]
        ,done: function (res, curr, count) {
            soulTable.render(this);
        }
    });
    bjTable = table.render({
        elem: '#bjTable'
        ,cols:[[]]
        ,data:[]
        ,loading:false
        ,height: 'full-100'
        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
        ,page: true
        ,title: '扁机审核表'
        ,totalRow: true
        ,even: true
        ,overflow: 'tips'
        ,limit:50
        ,limits:[50, 100, 200]
        ,done: function (res, curr, count) {
            soulTable.render(this);
        }
    });
    shareAccessoryTable = table.render({
        elem: '#shareAccessoryTable'
        ,cols:[[]]
        ,data:[]
        ,loading:false
        ,height: 'full-100'
        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
        ,page: true
        ,title: '共用辅料审核表'
        ,totalRow: true
        ,even: true
        ,overflow: 'tips'
        ,limit:50
        ,limits:[50, 100, 200]
        ,done: function (res, curr, count) {
            soulTable.render(this);
        }
    });
    initTable();
    function initTable(){
        var load = layer.load();
        $.ajax({
            url: "/erp/getfabricaccessoryonlyreviewdata",
            type: 'GET',
            data: {},
            success: function (res) {
                if (res.code === 0) {
                    layer.close(load);
                    var fabricData = res.fabricReviewList;
                    var accessoryData = res.accessoryReviewList;
                    var bjData = res.bjReviewList;
                    var preAccessoryData = res.accessoryPreStoreList;
                    fabricTable = table.render({
                        elem: '#fabricTable'
                        ,cols:[[
                            {type:'numbers', align:'center', title:'序号', minWidth:60}
                            ,{field: 'reviewType', title: '类别',align:'center', minWidth: 90, sort: true, filter: true}
                            // ,{field: 'season', title: '季度',align:'center', minWidth: 120, sort: true, filter: true}
                            ,{field: 'customerName', title: '品牌',align:'center', minWidth: 120, sort: true, filter: true}
                            ,{field: 'orderName', title: '款号',align:'center', minWidth: 200, sort: true, filter: true}
                            ,{field: 'clothesVersionNumber', title: '单号',align:'center', minWidth: 200, sort: true, filter: true}
                            ,{field: 'materialName', title: '名称',align:'center', minWidth: 200, sort: true, filter: true}
                            ,{field: 'supplier', title: '供应商',align:'center', minWidth: 100, sort: true, filter: true}
                            ,{field: 'checkNumber', title: '合同号',align:'center', minWidth: 150, sort: true, filter: true}
                            ,{field: 'checkDate', title: '日期',align:'center', minWidth: 120, sort: true, filter: true}
                            ,{field: 'preCheckEmp', title: '预审人',align:'center', minWidth: 100, sort: true, filter: true}
                            ,{field: 'preCheckTime', title: '预审时间',align:'center', minWidth: 100, sort: true, filter: true,templet:function (d) {
                                    if (d.preCheckTime == null){
                                        return ''
                                    }
                                    else {
                                        return moment(d.preCheckTime).format("YY-MM-DD");
                                    }
                                }}
                            ,{field: 'checkState', title: '审核状态',align:'center', minWidth: 100, sort: true, filter: true}
                            ,{field: 'checkEmp', title: '审核人',align:'center', minWidth: 100, sort: true, filter: true}
                            ,{field: 'checkTime', title: '审核时间',align:'center', minWidth: 100, sort: true, filter: true,templet:function (d) {
                                    if (d.checkTime == null){
                                        return ''
                                    }
                                    else {
                                        return moment(d.checkTime).format("YY-MM-DD");
                                    }
                                }}
                            ,{field: 'orderState', title: '下单状态',align:'center', minWidth: 100, sort: true, filter: true}
                            ,{title:'操作', align:'center', toolbar: '#barTopReview', minWidth:120}
                        ]]
                        ,loading:true
                        ,data:fabricData
                        ,height: 'full-100'
                        ,title: '面料审核表'
                        ,totalRow: true
                        ,even: true
                        ,page: true
                        ,overflow: 'tips'
                        ,limit:50
                        ,limits:[50, 100, 200]
                        ,done: function (res, curr, count) {
                            soulTable.render(this);
                        }
                    });
                    accessoryTable = table.render({
                        elem: '#accessoryTable'
                        ,cols:[[
                            {type:'numbers', align:'center', title:'序号', minWidth:60}
                            ,{field: 'reviewType', title: '类别',align:'center', minWidth: 90, sort: true, filter: true}
                            // ,{field: 'season', title: '季度',align:'center', minWidth: 120, sort: true, filter: true}
                            ,{field: 'customerName', title: '品牌',align:'center', minWidth: 120, sort: true, filter: true}
                            ,{field: 'orderName', title: '款号',align:'center', minWidth: 200, sort: true, filter: true}
                            ,{field: 'clothesVersionNumber', title: '单号',align:'center', minWidth: 200, sort: true, filter: true}
                            ,{field: 'materialName', title: '名称',align:'center', minWidth: 200, sort: true, filter: true}
                            ,{field: 'supplier', title: '供应商',align:'center', minWidth: 100, sort: true, filter: true}
                            ,{field: 'checkNumber', title: '合同号',align:'center', minWidth: 150, sort: true, filter: true}
                            ,{field: 'checkDate', title: '日期',align:'center', minWidth: 120, sort: true, filter: true}
                            ,{field: 'preCheckEmp', title: '预审人',align:'center', minWidth: 100, sort: true, filter: true}
                            ,{field: 'preCheckTime', title: '预审时间',align:'center', minWidth: 100, sort: true, filter: true,templet:function (d) {
                                    if (d.preCheckTime == null){
                                        return ''
                                    }
                                    else {
                                        return moment(d.preCheckTime).format("YY-MM-DD");
                                    }
                                }}
                            ,{field: 'checkState', title: '审核状态',align:'center', minWidth: 100, sort: true, filter: true}
                            ,{field: 'checkEmp', title: '审核人',align:'center', minWidth: 100, sort: true, filter: true}
                            ,{field: 'checkTime', title: '审核时间',align:'center', minWidth: 100, sort: true, filter: true,templet:function (d) {
                                    if (d.checkTime == null){
                                        return ''
                                    }
                                    else {
                                        return moment(d.checkTime).format("YY-MM-DD");
                                    }
                                }}
                            ,{title:'操作', align:'center', toolbar: '#barTopReview', minWidth:120}
                        ]]
                        ,loading:true
                        ,data:accessoryData
                        ,height: 'full-100'
                        ,title: '辅料审核表'
                        ,totalRow: true
                        ,even: true
                        ,page: true
                        ,overflow: 'tips'
                        ,limit:50
                        ,limits:[50, 100, 200]
                        ,done: function (res, curr, count) {
                            soulTable.render(this);
                        }
                    });
                    bjTable = table.render({
                        elem: '#bjTable'
                        ,cols:[[
                            {type:'numbers', align:'center', title:'序号', minWidth:60}
                            ,{field: 'reviewType', title: '类别',align:'center', minWidth: 90, sort: true, filter: true}
                            ,{field: 'customerName', title: '品牌',align:'center', minWidth: 120, sort: true, filter: true}
                            ,{field: 'orderName', title: '款号',align:'center', minWidth: 200, sort: true, filter: true}
                            ,{field: 'clothesVersionNumber', title: '单号',align:'center', minWidth: 200, sort: true, filter: true}
                            ,{field: 'materialName', title: '名称',align:'center', minWidth: 200, sort: true, filter: true}
                            ,{field: 'supplier', title: '供应商',align:'center', minWidth: 100, sort: true, filter: true}
                            ,{field: 'checkNumber', title: '合同号',align:'center', minWidth: 150, sort: true, filter: true}
                            ,{field: 'checkDate', title: '日期',align:'center', minWidth: 120, sort: true, filter: true}
                            ,{field: 'preCheckEmp', title: '预审人',align:'center', minWidth: 100, sort: true, filter: true}
                            ,{field: 'preCheckTime', title: '预审时间',align:'center', minWidth: 100, sort: true, filter: true,templet:function (d) {
                                    if (d.preCheckTime == null){
                                        return ''
                                    }
                                    else {
                                        return moment(d.preCheckTime).format("YY-MM-DD");
                                    }
                                }}
                            ,{field: 'checkState', title: '审核状态',align:'center', minWidth: 100, sort: true, filter: true}
                            ,{field: 'checkEmp', title: '审核人',align:'center', minWidth: 100, sort: true, filter: true}
                            ,{field: 'checkTime', title: '审核时间',align:'center', minWidth: 100, sort: true, filter: true,templet:function (d) {
                                    if (d.checkTime == null){
                                        return ''
                                    }
                                    else {
                                        return moment(d.checkTime).format("YY-MM-DD");
                                    }
                                }}
                            ,{title:'操作', align:'center', toolbar: '#barTopReview', minWidth:120}
                        ]]
                        ,loading:true
                        ,data:bjData
                        ,height: 'full-100'
                        ,title: '扁机审核表'
                        ,totalRow: true
                        ,even: true
                        ,page: true
                        ,overflow: 'tips'
                        ,limit:50
                        ,limits:[50, 100, 200]
                        ,done: function (res, curr, count) {
                            soulTable.render(this);
                        }
                    });
                    shareAccessoryTable = table.render({
                        elem: '#shareAccessoryTable'
                        ,cols:[[
                            {type:'numbers', align:'center', title:'序号', minWidth:60}
                            ,{field: 'reviewType', title: '类别',align:'center', minWidth: 90, sort: true, filter: true,hide:true}
                            ,{field: 'accessoryName', title: '名称',align:'center', minWidth: 120, sort: true, filter: true}
                            ,{field: 'accessoryNumber', title: '编号',align:'center', minWidth: 120, sort: true, filter: true}
                            ,{field: 'specification', title: '规格',align:'center', minWidth: 200, sort: true, filter: true}
                            ,{field: 'accessoryColor', title: '辅料颜色',align:'center', minWidth: 200, sort: true, filter: true, hide:true}
                            ,{field: 'supplier', title: '供应商',align:'center', minWidth: 200, sort: true, filter: true}
                            ,{field: 'checkState', title: '审核状态',align:'center', minWidth: 100, sort: true, filter: true}
                            ,{field: 'checkNumber', title: '合同号',align:'center', minWidth: 150, sort: true, filter: true}
                            ,{field: 'orderState', title: '下单状态',align:'center', minWidth: 150, sort: true, filter: true}
                            ,{title:'操作', align:'center', toolbar: '#barTopReview', minWidth:120}
                        ]]
                        ,loading:true
                        ,data:preAccessoryData
                        ,height: 'full-100'
                        ,title: '共有辅料审核表'
                        ,totalRow: true
                        ,even: true
                        ,page: true
                        ,overflow: 'tips'
                        ,limit:50
                        ,limits:[50, 100, 200]
                        ,done: function (res, curr, count) {
                            soulTable.render(this);
                        }
                    });
                } else {
                    layer.close(load);
                    layer.msg("获取失败~~~");
                }
            }
        })
    }
    $.ajax({
        url: "/erp/getchecknumbercount",
        type: 'GET',
        data: {},
        success: function (res) {
            if (res.checkNumberCount != null) {
                $("#checkNumberCount").text(res.checkNumberCount);
            } else {
                layer.msg("获取失败！", {icon: 2});
            }
        }, error: function () {
            layer.msg("获取失败！", {icon: 2});
        }
    });
    table.on('tool(fabricTable)', function(obj){
        var data = obj.data;
        if(obj.event === 'reviewCommit'){
            if (userRole != "role10" && userRole != "root"){
                layer.msg("您没有权限！", {icon: 2});
                return false;
            } else {
                var label = "物料审核, 类别:" + data.reviewType + "&emsp;&emsp;&emsp;&emsp; 合同号:" + data.checkNumber;
                var index = layer.open({
                    type: 1 //Page层类型
                    , title: label
                    , btn: ['通过','不通过']
                    , shade: 0.6 //遮罩透明度
                    , area: '1000px'
                    , maxmin: false //允许全屏最小化
                    , anim: 0 //0-6的动画形式，-1不开启
                    , content: $("#materialReview")
                    ,yes: function(i, layero){
                        var materialData = table.cache.materialReviewTable;
                        var fabricIDList = [];
                        $.each(materialData,function(i,item){
                            if (item.LAY_CHECKED){
                                fabricIDList.push(item.fabricID)
                            }
                        });
                        if (fabricIDList.length == 0){
                            layer.msg("未选中内容, 请先选中");
                            return false;
                        }

                        $.ajax({
                            url: "/erp/changefabriccheckstateinplaceordercheck",
                            type: 'POST',
                            data: {
                                fabricIDList:fabricIDList,
                                preCheck:"通过",
                                checkState:"通过"
                            },
                            traditional: true,
                            success: function (res) {
                                if (res == 0) {
                                    initTable();
                                    reloadInfo();
                                    layer.close(index);
                                    layer.msg("提交成功！", {icon: 1});
                                } else {
                                    layer.msg("提交失败！", {icon: 2});
                                }
                            }, error: function () {
                                layer.msg("提交失败！", {icon: 2});
                            }
                        })
                    }
                    ,btn2: function(i, layero){
                        var materialData = table.cache.materialReviewTable;
                        var fabricIDList = [];
                        $.each(materialData,function(i,item){
                            if (item.LAY_CHECKED){
                                fabricIDList.push(item.fabricID)
                            }
                        });
                        if (fabricIDList.length == 0){
                            layer.msg("未选中内容, 请先选中");
                            return false;
                        }
                        $.ajax({
                            url: "/erp/changefabriccheckstateinplaceordercheck",
                            type: 'POST',
                            data: {
                                fabricIDList:fabricIDList,
                                preCheck:"未提交",
                                checkState:"不通过"
                            },
                            traditional: true,
                            success: function (res) {
                                if (res == 0) {
                                    initTable();
                                    reloadInfo();
                                    layer.close(index);
                                    layer.msg("提交成功！", {icon: 1});
                                } else {
                                    layer.msg("提交失败！", {icon: 2});
                                }
                            }, error: function () {
                                layer.msg("提交失败！", {icon: 2});
                            }
                        })
                    }
                });
                layer.full(index);
                setTimeout(function () {
                    $.ajax({
                        url: "/erp/getreviewdatabytypechecknumber",
                        type: 'GET',
                        data: {
                            checkNumber: data.checkNumber,
                            reviewType: data.reviewType
                        },
                        success: function (res) {
                            var resultData = [];
                            if (data.reviewType === "面料"){
                                var title = [
                                    {type:'checkbox'},
                                    {field: 'orderName', title: '款号',align:'center',minWidth:120, sort: true, filter: true},
                                    {field: 'clothesVersionNumber', title: '单号',align:'center',minWidth:120, sort: true, filter: true},
                                    {field: 'fabricName', title: '名称',align:'center',minWidth:120, sort: true, filter: true},
                                    {field: 'unit', title: '单位',align:'center',minWidth:80, sort: true, filter: true},
                                    {field: 'partName', title: '部位',align:'center',minWidth:80, sort: true, filter: true},
                                    {field: 'colorName', title: '色组',align:'center',minWidth:90, sort: true, filter: true},
                                    {field: 'fabricColor', title: '面色',align:'center',minWidth:90, sort: true, filter: true},
                                    {field: 'supplier', title: '供应商',align:'center',minWidth:90},
                                    {field: 'orderPieceUsage', title: '客供单耗',align:'center',minWidth:120,totalRow: true},
                                    {field: 'pieceUsage', title: '单耗',align:'center',minWidth:120,totalRow: true},
                                    {field: 'orderCount', title: '订单量',align:'center',minWidth:100,totalRow: true},
                                    {field: 'fabricAdd', title: '损耗(%)',align:'center',minWidth:100,totalRow: true},
                                    {field: 'price', title: '单价',align:'center',minWidth:140, fixed: 'right',totalRow: true},
                                    {field: 'fabricCount', title: '客供总量',align:'center',minWidth:140, fixed: 'right',totalRow: true},
                                    {field: 'fabricMoney', title: '客供总价',align:'center',minWidth:140, fixed: 'right',totalRow: true},
                                    {field: 'fabricActCount', title: '实际订布',align:'center',minWidth:140, fixed: 'right',totalRow: true},
                                    {field: 'sumMoney', title: '实际总价',align:'center',minWidth:140, fixed: 'right',totalRow: true},
                                    {field: 'fabricID', title: 'fabricID',hide:true}
                                ];

                                if (res.data){
                                    $.each(res.data,function(index1,value1){
                                        var thisFabricData = {};
                                        thisFabricData.fabricID = value1.fabricID;
                                        thisFabricData.orderName = value1.orderName;
                                        thisFabricData.clothesVersionNumber = value1.clothesVersionNumber;
                                        thisFabricData.fabricName = value1.fabricName;
                                        thisFabricData.unit = value1.unit;
                                        thisFabricData.partName = value1.partName;
                                        thisFabricData.colorName = value1.colorName;
                                        thisFabricData.isHit = value1.isHit;
                                        thisFabricData.fabricColor = value1.fabricColor;
                                        thisFabricData.supplier = value1.supplier;
                                        thisFabricData.orderPieceUsage = value1.orderPieceUsage;
                                        thisFabricData.pieceUsage = value1.pieceUsage;
                                        thisFabricData.orderCount = value1.orderCount;
                                        thisFabricData.fabricAdd = value1.fabricAdd;
                                        thisFabricData.fabricCount = toDecimal(value1.orderPieceUsage * value1.orderCount);
                                        thisFabricData.fabricActCount = value1.fabricActCount;
                                        thisFabricData.price = value1.price;
                                        thisFabricData.fabricMoney = toDecimal(value1.orderPieceUsage * value1.price * value1.orderCount);
                                        thisFabricData.sumMoney = value1.sumMoney;
                                        resultData.push(thisFabricData);
                                    });
                                } else{
                                    layer.msg("面料信息未录入或者全部已经提交审核");
                                    layer.close(index);
                                }
                            }
                            else if (data.reviewType === "辅料"){
                                var title = [
                                    {type:'checkbox'},
                                    {field: 'orderName', title: '款号',align:'center',minWidth:120, sort: true, filter: true},
                                    {field: 'clothesVersionNumber', title: '单号',align:'center',minWidth:120, sort: true, filter: true},
                                    {field: 'accessoryName', title: '辅料名称',align:'center',minWidth:100, sort: true, filter: true},
                                    {field: 'accessoryNumber', title: '编号',align:'center',minWidth:80, sort: true, filter: true},
                                    {field: 'specification', title: '辅料规格',align:'center',minWidth:80, sort: true, filter: true},
                                    {field: 'accessoryUnit', title: '单位',align:'center',minWidth:80, sort: true, filter: true},
                                    {field: 'accessoryColor', title: '辅料颜色',align:'center',minWidth:80, sort: true, filter: true},
                                    {field: 'orderPieceUsage', title: '客供用量',align:'center',minWidth:80},
                                    {field: 'pieceUsage', title: '单件用量',align:'center',minWidth:80},
                                    {field: 'sizeName', title: '尺码',align:'center',minWidth:100},
                                    {field: 'colorName', title: '颜色',align:'center',minWidth:120},
                                    {field: 'supplier', title: '供应商',align:'center',minWidth:100, sort: true, filter: true},
                                    {field: 'orderCount', title: '订单量',align:'center',minWidth:120,totalRow: true},
                                    {field: 'accessoryAdd', title: '上浮(%)',align:'center',minWidth:120},
                                    {field: 'orderAccessoryCount', title: '客供总量',align:'center',minWidth:120,totalRow: true},
                                    {field: 'accessoryCount', title: '订购量',align:'center',minWidth:120,totalRow: true},
                                    {field: 'orderPrice', title: '客供单价',align:'center',minWidth:120,fixed: 'right',totalRow: true},
                                    {field: 'price', title: '单价',align:'center',minWidth:120,fixed: 'right',totalRow: true},
                                    {field: 'orderSumMoney', title: '客供总价',align:'center',minWidth:120,fixed: 'right',totalRow: true},
                                    {field: 'sumMoney', title: '总价',align:'center',minWidth:120,fixed: 'right',totalRow: true},
                                    {field: 'addFee', title: '附加费',align:'center',minWidth:120,fixed: 'right',totalRow: true},
                                    {field: 'addRemark', title: '备注',align:'center',minWidth:120,fixed: 'right',totalRow: true},
                                    {field: 'accessoryID', title: 'accessoryID',hide:true}
                                ];
                                if (res.data){
                                    $.each(res.data,function(index1,value1){
                                        var thisAccessoryData = {};
                                        thisAccessoryData.accessoryID = value1.accessoryID;
                                        thisAccessoryData.orderName = value1.orderName;
                                        thisAccessoryData.clothesVersionNumber = value1.clothesVersionNumber;
                                        thisAccessoryData.accessoryName = value1.accessoryName;
                                        thisAccessoryData.accessoryNumber = value1.accessoryNumber;
                                        thisAccessoryData.specification = value1.specification;
                                        thisAccessoryData.accessoryUnit = value1.accessoryUnit;
                                        thisAccessoryData.accessoryColor = value1.accessoryColor;
                                        thisAccessoryData.orderPieceUsage = value1.orderPieceUsage;
                                        thisAccessoryData.pieceUsage = value1.pieceUsage;
                                        thisAccessoryData.sizeName = value1.sizeName;
                                        thisAccessoryData.colorName = value1.colorName;
                                        thisAccessoryData.supplier = value1.supplier;
                                        thisAccessoryData.orderCount = value1.orderCount;
                                        thisAccessoryData.accessoryAdd = value1.accessoryAdd;
                                        thisAccessoryData.accessoryCount = value1.accessoryCount;
                                        thisAccessoryData.orderAccessoryCount = toDecimal(value1.orderPieceUsage * value1.orderCount);
                                        thisAccessoryData.price = value1.price;
                                        thisAccessoryData.orderPrice = toDecimal(value1.orderPrice);
                                        thisAccessoryData.addFee = toDecimal(value1.addFee);
                                        thisAccessoryData.addRemark = value1.addRemark;
                                        thisAccessoryData.sumMoney = toDecimal(value1.accessoryCount * value1.price * (1 + value1.accessoryAdd/100));
                                        thisAccessoryData.orderSumMoney = toDecimal(thisAccessoryData.orderAccessoryCount * thisAccessoryData.orderPrice);
                                        resultData.push(thisAccessoryData);
                                    });
                                } else{
                                    layer.msg("面料信息未录入或者全部已经提交审核");
                                    layer.close(index);
                                }
                            }
                            table.render({
                                elem: '#materialReviewTable'
                                ,cols: [title]
                                ,data: resultData
                                ,height: 'full-150'
                                ,overflow: 'tips'
                                ,totalRow: true
                                ,limit: Number.MAX_VALUE //每页默认显示的数量
                                ,done:function (res) {
                                    soulTable.render(this);
                                }
                            });
                        },
                        error: function () {
                            layer.msg("未录入面料信息！", {icon: 2});
                        }
                    })
                },100);
            }
        }
    });
    table.on('tool(accessoryTable)', function(obj){
        var data = obj.data;
        if(obj.event === 'reviewCommit'){
            if (userRole != "role10" && userRole != "root"){
                layer.msg("您没有权限！", {icon: 2});
                return false;
            } else {
                var label = "物料审核, 类别:" + data.reviewType + "&emsp;&emsp;&emsp;&emsp; 合同号:" + data.checkNumber;
                var index = layer.open({
                    type: 1 //Page层类型
                    , title: label
                    , btn: ['通过','不通过']
                    , shade: 0.6 //遮罩透明度
                    , area: '1000px'
                    , maxmin: false //允许全屏最小化
                    , anim: 0 //0-6的动画形式，-1不开启
                    , content: $("#materialReview")
                    ,yes: function(i, layero){
                        var materialData = table.cache.materialReviewTable;
                        var accessoryIDList = [];
                        $.each(materialData,function(i,item){
                            if (item.LAY_CHECKED){
                                accessoryIDList.push(item.accessoryID)
                            }
                        });
                        if (accessoryIDList.length == 0){
                            layer.msg("未选中内容, 请先选中");
                            return false;
                        }
                        $.ajax({
                            url: "/erp/changeaccessorycheckstateinplaceordercheck",
                            type: 'POST',
                            data: {
                                accessoryIDList:accessoryIDList,
                                preCheck:"通过",
                                checkState:"通过"
                            },
                            traditional: true,
                            success: function (res) {
                                if (res == 0) {
                                    initTable();
                                    reloadInfo();
                                    layer.close(index);
                                    layer.msg("提交成功！", {icon: 1});
                                } else {
                                    layer.msg("提交失败！", {icon: 2});
                                }
                            }, error: function () {
                                layer.msg("提交失败！", {icon: 2});
                            }
                        })
                    }
                    ,btn2: function(i, layero){
                        var materialData = table.cache.materialReviewTable;
                        var accessoryIDList = [];
                        $.each(materialData,function(i,item){
                            if (item.LAY_CHECKED){
                                accessoryIDList.push(item.accessoryID)
                            }
                        });
                        if (accessoryIDList.length == 0){
                            layer.msg("未选中内容, 请先选中");
                            return false;
                        }
                        $.ajax({
                            url: "/erp/changeaccessorycheckstateinplaceordercheck",
                            type: 'POST',
                            data: {
                                accessoryIDList:accessoryIDList,
                                preCheck:"未提交",
                                checkState:"不通过"
                            },
                            traditional: true,
                            success: function (res) {
                                if (res == 0) {
                                    initTable();
                                    reloadInfo();
                                    layer.close(index);
                                    layer.msg("提交成功！", {icon: 1});
                                } else {
                                    layer.msg("提交失败！", {icon: 2});
                                }
                            }, error: function () {
                                layer.msg("提交失败！", {icon: 2});
                            }
                        })
                    }
                });
                layer.full(index);
                setTimeout(function () {
                    $.ajax({
                        url: "/erp/getreviewdatabytypechecknumber",
                        type: 'GET',
                        data: {
                            checkNumber: data.checkNumber,
                            reviewType: data.reviewType
                        },
                        success: function (res) {
                            var resultData = [];
                            if (data.reviewType === "面料"){
                                var title = [
                                    {type:'checkbox'},
                                    {field: 'orderName', title: '款号',align:'center',minWidth:120, sort: true, filter: true},
                                    {field: 'clothesVersionNumber', title: '单号',align:'center',minWidth:120, sort: true, filter: true},
                                    {field: 'fabricName', title: '名称',align:'center',minWidth:120, sort: true, filter: true},
                                    {field: 'unit', title: '单位',align:'center',minWidth:80, sort: true, filter: true},
                                    {field: 'partName', title: '部位',align:'center',minWidth:80, sort: true, filter: true},
                                    {field: 'colorName', title: '色组',align:'center',minWidth:90, sort: true, filter: true},
                                    {field: 'fabricColor', title: '面色',align:'center',minWidth:90, sort: true, filter: true},
                                    {field: 'supplier', title: '供应商',align:'center',minWidth:90},
                                    {field: 'orderPieceUsage', title: '客供单耗',align:'center',minWidth:120,totalRow: true},
                                    {field: 'pieceUsage', title: '单耗',align:'center',minWidth:120,totalRow: true},
                                    {field: 'orderCount', title: '订单量',align:'center',minWidth:100,totalRow: true},
                                    {field: 'fabricAdd', title: '损耗(%)',align:'center',minWidth:100,totalRow: true},
                                    {field: 'price', title: '单价',align:'center',minWidth:140, fixed: 'right',totalRow: true},
                                    {field: 'fabricCount', title: '理论订布',align:'center',minWidth:140, fixed: 'right',totalRow: true},
                                    {field: 'fabricMoney', title: '理论总价',align:'center',minWidth:140, fixed: 'right',totalRow: true},
                                    {field: 'fabricActCount', title: '实际订布',align:'center',minWidth:140, fixed: 'right',totalRow: true},
                                    {field: 'sumMoney', title: '实际总价',align:'center',minWidth:140, fixed: 'right',totalRow: true},
                                    {field: 'fabricID', title: 'fabricID',hide:true}
                                ];

                                if (res.data){
                                    $.each(res.data,function(index1,value1){
                                        var thisFabricData = {};
                                        thisFabricData.fabricID = value1.fabricID;
                                        thisFabricData.orderName = value1.orderName;
                                        thisFabricData.clothesVersionNumber = value1.clothesVersionNumber;
                                        thisFabricData.fabricName = value1.fabricName;
                                        thisFabricData.unit = value1.unit;
                                        thisFabricData.partName = value1.partName;
                                        thisFabricData.colorName = value1.colorName;
                                        thisFabricData.isHit = value1.isHit;
                                        thisFabricData.fabricColor = value1.fabricColor;
                                        thisFabricData.supplier = value1.supplier;
                                        thisFabricData.orderPieceUsage = value1.orderPieceUsage;
                                        thisFabricData.pieceUsage = value1.pieceUsage;
                                        thisFabricData.orderCount = value1.orderCount;
                                        thisFabricData.fabricAdd = value1.fabricAdd;
                                        thisFabricData.fabricCount = value1.fabricCount;
                                        thisFabricData.fabricActCount = value1.fabricActCount;
                                        thisFabricData.price = value1.price;
                                        thisFabricData.fabricMoney = toDecimal(value1.fabricCount * value1.price);
                                        thisFabricData.sumMoney = value1.sumMoney;
                                        resultData.push(thisFabricData);
                                    });
                                } else{
                                    layer.msg("面料信息未录入或者全部已经提交审核");
                                    layer.close(index);
                                }
                            }
                            else if (data.reviewType === "辅料"){
                                var title = [
                                    {type:'checkbox'},
                                    {field: 'orderName', title: '款号',align:'center',minWidth:120, sort: true, filter: true},
                                    {field: 'clothesVersionNumber', title: '单号',align:'center',minWidth:120, sort: true, filter: true},
                                    {field: 'accessoryName', title: '辅料名称',align:'center',minWidth:100, sort: true, filter: true},
                                    {field: 'accessoryNumber', title: '编号',align:'center',minWidth:80, sort: true, filter: true},
                                    {field: 'specification', title: '辅料规格',align:'center',minWidth:80, sort: true, filter: true},
                                    {field: 'accessoryUnit', title: '单位',align:'center',minWidth:80, sort: true, filter: true},
                                    {field: 'accessoryColor', title: '辅料颜色',align:'center',minWidth:80, sort: true, filter: true},
                                    {field: 'orderPieceUsage', title: '客供单耗',align:'center',minWidth:80},
                                    {field: 'pieceUsage', title: '单件用量',align:'center',minWidth:80},
                                    {field: 'sizeName', title: '尺码',align:'center',minWidth:100},
                                    {field: 'colorName', title: '颜色',align:'center',minWidth:120},
                                    {field: 'supplier', title: '供应商',align:'center',minWidth:100, sort: true, filter: true},
                                    {field: 'orderCount', title: '订单量',align:'center',minWidth:120,totalRow: true},
                                    {field: 'accessoryAdd', title: '上浮(%)',align:'center',minWidth:120},
                                    {field: 'orderAccessoryCount', title: '客供总量',align:'center',minWidth:120,totalRow: true},
                                    {field: 'accessoryCount', title: '订购量',align:'center',minWidth:120,totalRow: true},
                                    {field: 'orderPrice', title: '客供单价',align:'center',minWidth:120,totalRow: true},
                                    {field: 'price', title: '单价',align:'center',minWidth:120,fixed: 'right',totalRow: true},
                                    {field: 'orderSumMoney', title: '客供总价',align:'center',minWidth:120,fixed: 'right',totalRow: true},
                                    {field: 'sumMoney', title: '总价',align:'center',minWidth:120,fixed: 'right',totalRow: true},
                                    {field: 'addFee', title: '附加费',align:'center',minWidth:120,fixed: 'right',totalRow: true},
                                    {field: 'addRemark', title: '备注',align:'center',minWidth:120,fixed: 'right'},
                                    {field: 'accessoryID', title: 'accessoryID',hide:true}
                                ];
                                if (res.data){
                                    $.each(res.data,function(index1,value1){
                                        var thisAccessoryData = {};
                                        thisAccessoryData.accessoryID = value1.accessoryID;
                                        thisAccessoryData.orderName = value1.orderName;
                                        thisAccessoryData.clothesVersionNumber = value1.clothesVersionNumber;
                                        thisAccessoryData.accessoryName = value1.accessoryName;
                                        thisAccessoryData.accessoryNumber = value1.accessoryNumber;
                                        thisAccessoryData.specification = value1.specification;
                                        thisAccessoryData.accessoryUnit = value1.accessoryUnit;
                                        thisAccessoryData.accessoryColor = value1.accessoryColor;
                                        thisAccessoryData.pieceUsage = value1.pieceUsage;
                                        thisAccessoryData.orderPieceUsage = value1.orderPieceUsage;
                                        thisAccessoryData.sizeName = value1.sizeName;
                                        thisAccessoryData.colorName = value1.colorName;
                                        thisAccessoryData.supplier = value1.supplier;
                                        thisAccessoryData.orderCount = value1.orderCount;
                                        thisAccessoryData.accessoryAdd = value1.accessoryAdd;
                                        thisAccessoryData.accessoryCount = value1.accessoryCount;
                                        thisAccessoryData.orderAccessoryCount = toDecimal(value1.orderCount * value1.orderPieceUsage);
                                        thisAccessoryData.price = value1.price;
                                        thisAccessoryData.addFee = toDecimal(value1.addFee);
                                        thisAccessoryData.addRemark = value1.addRemark;
                                        thisAccessoryData.orderPrice = value1.orderPrice;
                                        thisAccessoryData.sumMoney = toDecimal(value1.price * value1.accessoryCount);
                                        thisAccessoryData.orderSumMoney = toDecimal(thisAccessoryData.orderPrice * thisAccessoryData.orderAccessoryCount);
                                        resultData.push(thisAccessoryData);
                                    });
                                } else{
                                    layer.msg("面料信息未录入或者全部已经提交审核");
                                    layer.close(index);
                                }
                            }
                            table.render({
                                elem: '#materialReviewTable'
                                ,cols: [title]
                                ,data: resultData
                                ,height: 'full-150'
                                ,overflow: 'tips'
                                ,totalRow: true
                                ,limit: Number.MAX_VALUE //每页默认显示的数量
                                ,done:function (res) {
                                    soulTable.render(this);
                                }
                            });
                        },
                        error: function () {
                            layer.msg("未录入面料信息！", {icon: 2});
                        }
                    })
                },100);
            }
        }
    });
    table.on('tool(bjTable)', function(obj){
        var data = obj.data;
        if(obj.event === 'reviewCommit'){
            if (userRole != "role10" && userRole != "root"){
                layer.msg("您没有权限！", {icon: 2});
                return false;
            } else {
                var label = "物料审核, 类别:" + data.reviewType + "&emsp;&emsp;&emsp;&emsp; 合同号:" + data.checkNumber;
                var index = layer.open({
                    type: 1 //Page层类型
                    , title: label
                    , btn: ['通过','不通过']
                    , shade: 0.6 //遮罩透明度
                    , area: '1000px'
                    , maxmin: false //允许全屏最小化
                    , anim: 0 //0-6的动画形式，-1不开启
                    , content: $("#materialReview")
                    ,yes: function(i, layero){
                        var materialData = table.cache.materialReviewTable;
                        var accessoryIDList = [];
                        $.each(materialData,function(i,item){
                            if (item.LAY_CHECKED){
                                accessoryIDList.push(item.accessoryID)
                            }
                        });
                        if (accessoryIDList.length == 0){
                            layer.msg("未选中内容, 请先选中");
                            return false;
                        }
                        $.ajax({
                            url: "/erp/changeaccessorycheckstateinplaceordercheck",
                            type: 'POST',
                            data: {
                                accessoryIDList:accessoryIDList,
                                preCheck:"通过",
                                checkState:"通过"
                            },
                            traditional: true,
                            success: function (res) {
                                if (res == 0) {
                                    initTable();
                                    reloadInfo();
                                    layer.close(index);
                                    layer.msg("提交成功！", {icon: 1});
                                } else {
                                    layer.msg("提交失败！", {icon: 2});
                                }
                            }, error: function () {
                                layer.msg("提交失败！", {icon: 2});
                            }
                        })
                    }
                    ,btn2: function(i, layero){
                        var materialData = table.cache.materialReviewTable;
                        var accessoryIDList = [];
                        $.each(materialData,function(i,item){
                            if (item.LAY_CHECKED){
                                accessoryIDList.push(item.accessoryID)
                            }
                        });
                        if (accessoryIDList.length == 0){
                            layer.msg("未选中内容, 请先选中");
                            return false;
                        }
                        $.ajax({
                            url: "/erp/changeaccessorycheckstateinplaceordercheck",
                            type: 'POST',
                            data: {
                                accessoryIDList:accessoryIDList,
                                preCheck:"未提交",
                                checkState:"不通过"
                            },
                            traditional: true,
                            success: function (res) {
                                if (res == 0) {
                                    initTable();
                                    reloadInfo();
                                    layer.close(index);
                                    layer.msg("提交成功！", {icon: 1});
                                } else {
                                    layer.msg("提交失败！", {icon: 2});
                                }
                            }, error: function () {
                                layer.msg("提交失败！", {icon: 2});
                            }
                        })
                    }
                });
                layer.full(index);
                setTimeout(function () {
                    $.ajax({
                        url: "/erp/getreviewdatabytypechecknumber",
                        type: 'GET',
                        data: {
                            checkNumber: data.checkNumber,
                            reviewType: data.reviewType
                        },
                        success: function (res) {
                            var resultData = [];
                            if (data.reviewType === "面料"){
                                var title = [
                                    {type:'checkbox'},
                                    {field: 'orderName', title: '款号',align:'center',minWidth:120, sort: true, filter: true},
                                    {field: 'clothesVersionNumber', title: '单号',align:'center',minWidth:120, sort: true, filter: true},
                                    {field: 'fabricName', title: '名称',align:'center',minWidth:120, sort: true, filter: true},
                                    {field: 'unit', title: '单位',align:'center',minWidth:80, sort: true, filter: true},
                                    {field: 'partName', title: '部位',align:'center',minWidth:80, sort: true, filter: true},
                                    {field: 'colorName', title: '色组',align:'center',minWidth:90, sort: true, filter: true},
                                    {field: 'fabricColor', title: '面色',align:'center',minWidth:90, sort: true, filter: true},
                                    {field: 'supplier', title: '供应商',align:'center',minWidth:90},
                                    {field: 'orderPieceUsage', title: '客供单耗',align:'center',minWidth:120,totalRow: true},
                                    {field: 'pieceUsage', title: '单耗',align:'center',minWidth:120,totalRow: true},
                                    {field: 'orderCount', title: '订单量',align:'center',minWidth:100,totalRow: true},
                                    {field: 'fabricAdd', title: '损耗(%)',align:'center',minWidth:100,totalRow: true},
                                    {field: 'price', title: '单价',align:'center',minWidth:140, fixed: 'right',totalRow: true},
                                    {field: 'fabricCount', title: '理论订布',align:'center',minWidth:140, fixed: 'right',totalRow: true},
                                    {field: 'fabricMoney', title: '理论总价',align:'center',minWidth:140, fixed: 'right',totalRow: true},
                                    {field: 'fabricActCount', title: '实际订布',align:'center',minWidth:140, fixed: 'right',totalRow: true},
                                    {field: 'sumMoney', title: '实际总价',align:'center',minWidth:140, fixed: 'right',totalRow: true},
                                    {field: 'fabricID', title: 'fabricID',hide:true}
                                ];

                                if (res.data){
                                    $.each(res.data,function(index1,value1){
                                        var thisFabricData = {};
                                        thisFabricData.fabricID = value1.fabricID;
                                        thisFabricData.orderName = value1.orderName;
                                        thisFabricData.clothesVersionNumber = value1.clothesVersionNumber;
                                        thisFabricData.fabricName = value1.fabricName;
                                        thisFabricData.unit = value1.unit;
                                        thisFabricData.partName = value1.partName;
                                        thisFabricData.colorName = value1.colorName;
                                        thisFabricData.isHit = value1.isHit;
                                        thisFabricData.fabricColor = value1.fabricColor;
                                        thisFabricData.supplier = value1.supplier;
                                        thisFabricData.orderPieceUsage = value1.orderPieceUsage;
                                        thisFabricData.pieceUsage = value1.pieceUsage;
                                        thisFabricData.orderCount = value1.orderCount;
                                        thisFabricData.fabricAdd = value1.fabricAdd;
                                        thisFabricData.fabricCount = value1.fabricCount;
                                        thisFabricData.fabricActCount = value1.fabricActCount;
                                        thisFabricData.price = value1.price;
                                        thisFabricData.fabricMoney = toDecimal(value1.fabricCount * value1.price);
                                        thisFabricData.sumMoney = value1.sumMoney;
                                        resultData.push(thisFabricData);
                                    });
                                } else{
                                    layer.msg("面料信息未录入或者全部已经提交审核");
                                    layer.close(index);
                                }
                            }
                            else if (data.reviewType === "辅料" || data.reviewType === "扁机"){
                                var title = [
                                    {type:'checkbox'},
                                    {field: 'orderName', title: '款号',align:'center',minWidth:120, sort: true, filter: true},
                                    {field: 'clothesVersionNumber', title: '单号',align:'center',minWidth:120, sort: true, filter: true},
                                    {field: 'accessoryName', title: '辅料名称',align:'center',minWidth:100, sort: true, filter: true},
                                    {field: 'accessoryNumber', title: '编号',align:'center',minWidth:80, sort: true, filter: true},
                                    {field: 'specification', title: '辅料规格',align:'center',minWidth:80, sort: true, filter: true},
                                    {field: 'accessoryUnit', title: '单位',align:'center',minWidth:80, sort: true, filter: true},
                                    {field: 'accessoryColor', title: '辅料颜色',align:'center',minWidth:80, sort: true, filter: true},
                                    {field: 'orderPieceUsage', title: '客供单耗',align:'center',minWidth:80},
                                    {field: 'pieceUsage', title: '单件用量',align:'center',minWidth:80},
                                    {field: 'sizeName', title: '尺码',align:'center',minWidth:100},
                                    {field: 'colorName', title: '颜色',align:'center',minWidth:120},
                                    {field: 'supplier', title: '供应商',align:'center',minWidth:100, sort: true, filter: true},
                                    {field: 'orderCount', title: '订单量',align:'center',minWidth:120,totalRow: true},
                                    {field: 'accessoryAdd', title: '上浮(%)',align:'center',minWidth:120},
                                    {field: 'orderAccessoryCount', title: '客供总量',align:'center',minWidth:120,totalRow: true},
                                    {field: 'accessoryCount', title: '订购量',align:'center',minWidth:120,totalRow: true},
                                    {field: 'orderPrice', title: '客供单价',align:'center',minWidth:120,totalRow: true},
                                    {field: 'price', title: '单价',align:'center',minWidth:120,fixed: 'right',totalRow: true},
                                    {field: 'orderSumMoney', title: '客供总价',align:'center',minWidth:120,fixed: 'right',totalRow: true},
                                    {field: 'sumMoney', title: '总价',align:'center',minWidth:120,fixed: 'right',totalRow: true},
                                    {field: 'addFee', title: '附加费',align:'center',minWidth:120,fixed: 'right',totalRow: true},
                                    {field: 'addRemark', title: '备注',align:'center',minWidth:120,fixed: 'right'},
                                    {field: 'accessoryID', title: 'accessoryID',hide:true}
                                ];
                                if (res.data){
                                    $.each(res.data,function(index1,value1){
                                        var thisAccessoryData = {};
                                        thisAccessoryData.accessoryID = value1.accessoryID;
                                        thisAccessoryData.orderName = value1.orderName;
                                        thisAccessoryData.clothesVersionNumber = value1.clothesVersionNumber;
                                        thisAccessoryData.accessoryName = value1.accessoryName;
                                        thisAccessoryData.accessoryNumber = value1.accessoryNumber;
                                        thisAccessoryData.specification = value1.specification;
                                        thisAccessoryData.accessoryUnit = value1.accessoryUnit;
                                        thisAccessoryData.accessoryColor = value1.accessoryColor;
                                        thisAccessoryData.pieceUsage = value1.pieceUsage;
                                        thisAccessoryData.orderPieceUsage = value1.pieceUsage;
                                        thisAccessoryData.sizeName = value1.sizeName;
                                        thisAccessoryData.colorName = value1.colorName;
                                        thisAccessoryData.supplier = value1.supplier;
                                        thisAccessoryData.orderCount = value1.orderCount;
                                        thisAccessoryData.accessoryAdd = value1.accessoryAdd;
                                        thisAccessoryData.accessoryCount = value1.accessoryCount;
                                        thisAccessoryData.orderAccessoryCount = toDecimal(value1.orderCount * value1.pieceUsage);
                                        thisAccessoryData.price = value1.price;
                                        thisAccessoryData.addFee = toDecimal(value1.addFee);
                                        thisAccessoryData.addRemark = value1.addRemark;
                                        thisAccessoryData.orderPrice = value1.orderPrice;
                                        thisAccessoryData.sumMoney = toDecimal(value1.price * value1.accessoryCount);
                                        thisAccessoryData.orderSumMoney = toDecimal(thisAccessoryData.orderPrice * thisAccessoryData.orderAccessoryCount);
                                        resultData.push(thisAccessoryData);
                                        console.log(thisAccessoryData);
                                    });
                                } else{
                                    layer.msg("面料信息未录入或者全部已经提交审核");
                                    layer.close(index);
                                }
                            }
                            table.render({
                                elem: '#materialReviewTable'
                                ,cols: [title]
                                ,data: resultData
                                ,height: 'full-150'
                                ,overflow: 'tips'
                                ,totalRow: true
                                ,limit: Number.MAX_VALUE //每页默认显示的数量
                                ,done:function (res) {
                                    soulTable.render(this);
                                }
                            });
                        },
                        error: function () {
                            layer.msg("未录入面料信息！", {icon: 2});
                        }
                    })
                },100);
            }
        }
    });
    table.on('tool(shareAccessoryTable)', function(obj){
        var data = obj.data;
        if(obj.event === 'reviewCommit'){
            if (userRole != "role10" && userRole != "root"){
                layer.msg("您没有权限！", {icon: 2});
                return false;
            } else {
                var label = "物料审核, 类别:" + data.reviewType + "&emsp;&emsp;&emsp;&emsp; 合同号:" + data.checkNumber;
                var index = layer.open({
                    type: 1 //Page层类型
                    , title: label
                    , btn: ['通过','不通过']
                    , shade: 0.6 //遮罩透明度
                    , area: '1000px'
                    , maxmin: false //允许全屏最小化
                    , anim: 0 //0-6的动画形式，-1不开启
                    , content: $("#materialReview")
                    ,yes: function(i, layero){
                        var materialData = table.cache.materialReviewTable;
                        var accessoryPreStoreList = [];
                        $.each(materialData,function(i,item){
                            accessoryPreStoreList.push({id: item.id, preCheck: "通过", checkState: "通过"});
                        });
                        $.ajax({
                            url: "/erp/changeaccessoryprestoreincheck",
                            type: 'POST',
                            data: {
                                accessoryPreStoreJson: JSON.stringify(accessoryPreStoreList)
                            },
                            success: function (res) {
                                if (res.result == 0) {
                                    initTable();
                                    layer.close(index);
                                    layer.msg("提交成功！", {icon: 1});
                                } else {
                                    layer.msg("提交失败！", {icon: 2});
                                }
                            }, error: function () {
                                layer.msg("提交失败！", {icon: 2});
                            }
                        });
                    }
                    ,btn2: function(i, layero){
                        var materialData = table.cache.materialReviewTable;
                        var accessoryPreStoreList = [];
                        $.each(materialData,function(i,item){
                            accessoryPreStoreList.push({id: item.id, preCheck: "未提交", checkState: "不通过"});
                        });
                        $.ajax({
                            url: "/erp/changeaccessoryprestoreincheck",
                            type: 'POST',
                            data: {
                                accessoryPreStoreJson: JSON.stringify(accessoryPreStoreList)
                            },
                            success: function (res) {
                                if (res.result == 0) {
                                    initTable('','','',3);
                                    layer.close(index);
                                    layer.msg("提交成功！", {icon: 1});
                                } else {
                                    layer.msg("提交失败！", {icon: 2});
                                }
                            }, error: function () {
                                layer.msg("提交失败！", {icon: 2});
                            }
                        });
                    }
                });
                layer.full(index);
                setTimeout(function () {
                    $.ajax({
                        url: "/erp/getaccessoryprestorebychecknumber",
                        type: 'GET',
                        data: {
                            checkNumber: data.checkNumber
                        },
                        success: function (res) {
                            var resultData = res.accessoryPreStoreList;
                            var title = [
                                {field: 'accessoryName', title: '名称',align:'center',minWidth:140, sort: true, filter: true},
                                {field: 'accessoryNumber', title: '编号',align:'center',minWidth:140, sort: true, filter: true},
                                {field: 'specification', title: '规格',align:'center',minWidth:200, sort: true, filter: true},
                                {field: 'accessoryColor', title: '颜色',align:'center',minWidth:80, sort: true, filter: true},
                                {field: 'accessoryUnit', title: '单位',align:'center',minWidth:80, sort: true, filter: true},
                                {field: 'supplier', title: '供应商',align:'center',minWidth:100, sort: true, filter: true},
                                {field: 'price', title: '单价',align:'center',minWidth:100, sort: true, filter: true},
                                {field: 'accessoryCount', title: '数量',align:'center',minWidth:100,totalRow:true},
                                {field: 'sumMoney', title: '金额',align:'center',minWidth:80,totalRow:true},
                                {field: 'id',hide:true}
                            ];
                            table.render({
                                elem: '#materialReviewTable'
                                ,cols: [title]
                                ,data: resultData
                                ,height: 'full-150'
                                ,limit: 1000
                                ,totalRow:true
                                ,limits: [1000, 2000, 3000]
                                ,done:function (res) {
                                    soulTable.render(this);
                                }
                            });
                        },
                        error: function () {
                            layer.msg("未录入面料信息！", {icon: 2});
                        }
                    })
                },100);
            }
        }
    });
});


function toDecimal(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*100)/100;
    return f;
}


function toDecimalFour(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*10000)/10000;
    return f;
}

function toDecimalSix(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*1000000)/1000000;
    return f;
}

function dateFormat(fmt, date) {
    var ret;
    var opt = {
        "Y+": date.getFullYear().toString(),        // 年
        "m+": (date.getMonth() + 1).toString(),     // 月
        "d+": date.getDate().toString(),            // 日
        "H+": date.getHours().toString(),           // 时
        "M+": date.getMinutes().toString(),         // 分
        "S+": date.getSeconds().toString()          // 秒
        // 有其他格式化字符需求可以继续添加，必须转化成字符串
    };
    for (var k in opt) {
        ret = new RegExp("(" + k + ")").exec(fmt);
        if (ret) {
            fmt = fmt.replace(ret[1], (ret[1].length == 1) ? (opt[k]) : (opt[k].padStart(ret[1].length, "0")))
        }
    }
    return fmt;
}

function getFormatDate() {
    var date = new Date();
    var month = date.getMonth() + 1;
    var strDate = date.getDate();
    if (month >= 1 && month <= 9) {
        month = "0" + month;
    }
    if (strDate >= 0 && strDate <= 9) {
        strDate = "0" + strDate;
    }
    var currentDate = date.getFullYear() + "-" + month + "-" + strDate;
    return currentDate;
}


function reloadInfo() {
    $.ajax({
        url: "/erp/getchecknumbercount",
        type: 'GET',
        data: {},
        success: function (res) {
            if (res.checkNumberCount != null) {
                $("#checkNumberCount").text(res.checkNumberCount);
            } else {
                layer.msg("获取失败！", {icon: 2});
            }
        }, error: function () {
            layer.msg("获取失败！", {icon: 2});
        }
    });
}