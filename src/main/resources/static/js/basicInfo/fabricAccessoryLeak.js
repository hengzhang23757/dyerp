var form;
layui.use('form', function(){
    form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
    form.render();
});
layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.21'
}).extend({                         // 模块别名
    soulTable: 'soulTable',
});
var reportTable;
var cxm,sxm;
$(document).ready(function () {
    $.ajax({
        url: "/erp/gethistorycustomer",
        data: {},
        success:function(data){
            var customerData = [];
            if (data.customerList){
                $.each(data.customerList, function (index, item) {
                    customerData.push({name: item, value: item, selected: false});
                })
            }
            cxm = xmSelect.render({
                el: '#xmCustomer',
                filterable: true,
                data: customerData
            });
        }, error:function(){
        }
    });

    $.ajax({
        url: "/erp/gethistoryseason",
        data: {},
        success:function(data){
            var seasonData = [];
            if (data.seasonList){
                $.each(data.seasonList, function (index, item) {
                    seasonData.push({name: item, value: item, selected: false});
                })
            }
            sxm = xmSelect.render({
                el: '#xmSeason',
                filterable: true,
                data: seasonData
            });
        }, error:function(){
        }
    });

    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumber", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });
});

layui.use(['form', 'soulTable', 'table', 'element'], function () {

    var table = layui.table,
        soulTable = layui.soulTable,
        element = layui.element,
        $ = layui.$;
    var load = layer.load();

    reportTable = table.render({
        elem: '#reportTable'
        ,cols:[[]]
        ,loading:true
        ,data:[]
        ,height: 'full-130'
        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
        ,title: '面辅料汇总查询报表'
        ,totalRow: true
        ,page: true
        ,overflow: 'tips'
        ,limits: [500, 1000, 2000]
        ,limit: 1000 //每页默认显示的数量
        ,done: function () {
            soulTable.render(this);
            layer.close(load);
        }
    });


    table.on('toolbar(reportTable)', function(obj){
        if (obj.event === 'clearFilter') {
            // 清除所有筛选条件并重载表格
            // 参数: tableId
            soulTable.clearFilter('reportTable')
        } else if (obj.event === 'exportExcel') {
            soulTable.export('reportTable');
        }
    });

    //监听提交
    form.on('submit(searchBeat)', function(data){
        var data = {};
        var seasonSelect = sxm.getValue('value');
        var customerSelect = cxm.getValue('value');
        var orderName = $("#orderName").val();
        var type = $("#type").val();
        var modelType = $("#modelType").val();
        if ((seasonSelect.length == 0 || customerSelect.length == 0) && (orderName == null || orderName == '')){
            layer.msg("品牌-季度或者款号二选一", { icon: 2 });
            return false;
        }
        if (modelType != null && modelType != ""){
            data.modelType = modelType;
        }
        if (orderName != null && orderName != ''){
            data.orderName = orderName;
        } else {
            data.seasonList = seasonSelect;
            data.customerList = customerSelect;
        }
        data.type = type;
        var load = layer.load(2);
        if (type === '面料' || type === '辅料'){
            $.ajax({
                url: "/erp/searchfabricaccessoryleakbyinfo",
                type: 'GET',
                data: {
                    fabricAccessoryDtoJson:JSON.stringify(data)
                },
                success: function (res) {
                    if (res.fabricAccessoryLeakList) {
                        var reportData = res.fabricAccessoryLeakList;
                        table.render({
                            elem: '#reportTable'
                            ,cols:[[
                                {type:'numbers', align:'center', title:'序号', width:90}
                                ,{field:'customerName', title:'客户', align:'center', width:100, sort: true, filter: true, totalRowText: '合计'}
                                ,{field:'season', title:'季度', align:'center', width:120, sort: true, filter: true}
                                ,{field:'modelType', title:'组别系列', align:'center', width:120, sort: true, filter: true}
                                ,{field:'clothesVersionNumber', title:'单号', align:'center', width:160, sort: true, filter: true}
                                ,{field:'orderName', title:'款号', align:'center', width:160, sort: true, filter: true}
                                ,{field:'orderCount', title:'订单数', align:'center', width:120, sort: true, filter: true, totalRow: true,excel: {cellType: 'n'}}
                                ,{field:'wellCount', title:'好片数', align:'center', width:120, sort: true, filter: true, totalRow: true,excel: {cellType: 'n'}}
                                ,{field:'purchaseCount', title:'订购量', align:'center', width:120, sort: true, filter: true, totalRow: true,excel: {cellType: 'n'}}
                                ,{field:'inStoreCount', title:'入库量', align:'center', width:120, sort: true, filter: true, totalRow: true,excel: {cellType: 'n'}}
                                ,{field:'readyPercent', title:'回料比例', align:'center', width:140, sort: true, filter: true,templet: function(d){
                                        //***重点***：判断颜色
                                        var colorCount = toDecimal(Number(d.readyPercent)*100);
                                        var color = '';
                                        if(30<colorCount && colorCount<90){
                                            color='layui-bg-orange'
                                        }else if(0<colorCount && colorCount<=30){
                                            color='layui-bg-red'
                                        }
                                        //***重点***：拼接进度条
                                        return '<div class="layui-progress layui-progress-big" lay-showpercent="true"><div class="layui-progress-bar '+color+'" lay-percent="'+colorCount+'%"></div></div><br>'
                                    }}
                                ,{field:'', title:'详情', align:'center',width:120, toolbar: '#barTopOne', fixed: 'right'}
                                ,{field:'', title:'未齐查看', align:'center', width:120, toolbar: '#barTopTwo', fixed: 'right'}
                            ]]
                            ,loading:true
                            ,data: reportData
                            ,height: 'full-130'
                            ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                            ,title: '面辅料汇总查询报表'
                            ,totalRow: true
                            ,page: true
                            ,overflow: 'tips'
                            ,limits: [500, 1000, 2000]
                            ,limit: 1000 //每页默认显示的数量
                            ,done: function () {
                                soulTable.render(this);
                                layer.close(load);
                                element.render();
                            }
                        });
                    } else {
                        layer.msg("获取失败！", {icon: 2});
                    }
                }, error: function () {
                    layer.msg("获取失败！", {icon: 2});
                }
            });
        } else if (type === '面料金额') {
            data.type = '面料';
            $.ajax({
                url: "/erp/searchfabricaccessorysummoneybyinfo",
                type: 'GET',
                data: {
                    fabricAccessoryDtoJson:JSON.stringify(data)
                },
                success: function (res) {
                    if (res.manufactureFabricList) {
                        var reportData = res.manufactureFabricList;
                        table.render({
                            elem: '#reportTable'
                            ,cols:[[
                                {type:'numbers', align:'center', title:'序号', width:90}
                                ,{field:'clothesVersionNumber', title:'单号', align:'center', width:120, sort: true, filter: true}
                                ,{field:'orderName', title:'款号', align:'center', width:120, sort: true, filter: true}
                                ,{field:'fabricName', title:'面料名称', align:'center', width:120, sort: true, filter: true}
                                ,{field:'fabricNumber', title:'面料号', align:'center', width:120, sort: true, filter: true}
                                ,{field:'fabricWidth', title:'布封', align:'center', width:100, sort: true, filter: true}
                                ,{field:'fabricWeight', title:'克重', align:'center', width:100, sort: true, filter: true}
                                ,{field:'unit', title:'单位', align:'center', width:90, sort: true, filter: true}
                                ,{field:'partName', title:'部位', align:'center', width:100, sort: true, filter: true}
                                ,{field:'colorName', title:'色组', align:'center', width:100, sort: true, filter: true}
                                ,{field:'fabricColor', title:'面料颜色', align:'center', width:120, sort: true, filter: true}
                                ,{field:'fabricActCount', title:'订购量', align:'center', width:120, sort: true, filter: true, totalRow: true,excel: {cellType: 'n'}}
                                ,{field:'price', title:'单价', align:'center', width:100, sort: true, filter: true,excel: {cellType: 'n'}}
                                ,{field:'sumMoney', title:'订购金额', align:'center', width:120, sort: true, filter: true, totalRow: true,excel: {cellType: 'n'}}
                                ,{field:'addFee', title:'附加费', align:'center', width:120, sort: true, filter: true, totalRow: true,excel: {cellType: 'n'}}
                                ,{field:'fabricReturn', title:'回布量', align:'center', width:100, sort: true, filter: true, totalRow: true,excel: {cellType: 'n'}}
                                ,{field:'returnSumMoney', title:'回布金额', align:'center', width:120, sort: true, filter: true, totalRow: true,excel: {cellType: 'n'}}
                            ]]
                            ,loading:true
                            ,data: reportData
                            ,height: 'full-130'
                            ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                            ,title: '面辅料汇总查询报表'
                            ,totalRow: true
                            ,page: true
                            ,overflow: 'tips'
                            ,limits: [500, 1000, 2000]
                            ,limit: 1000 //每页默认显示的数量
                            ,done: function () {
                                soulTable.render(this);
                                layer.close(load);
                                element.render();
                            }
                        });
                    } else {
                        layer.msg("获取失败！", {icon: 2});
                    }
                }, error: function () {
                    layer.msg("获取失败！", {icon: 2});
                }
            });
        } else if (type === '辅料金额') {
            data.type = '辅料';
            $.ajax({
                url: "/erp/searchfabricaccessorysummoneybyinfo",
                type: 'GET',
                data: {
                    fabricAccessoryDtoJson:JSON.stringify(data)
                },
                success: function (res) {
                    if (res.manufactureAccessoryList) {
                        var reportData = res.manufactureAccessoryList;
                        $.each(reportData, function (r_index, r_item){
                            r_item.orderAccessoryCount = toDecimal(r_item.orderPieceUsage * r_item.orderCount);
                            r_item.orderSumMoney = toDecimal(r_item.orderAccessoryCount * r_item.orderPrice);
                        });
                        table.render({
                            elem: '#reportTable'
                            ,cols:[[
                                {type:'numbers', align:'center', title:'序号', width:90}
                                ,{field:'clothesVersionNumber', title:'单号', align:'center', width:120, sort: true, filter: true}
                                ,{field:'orderName', title:'款号', align:'center', width:120, sort: true, filter: true}
                                ,{field:'accessoryName', title:'面料名称', align:'center', width:120, sort: true, filter: true}
                                ,{field:'specification', title:'规格', align:'center', width:120, sort: true, filter: true}
                                ,{field:'accessoryColor', title:'辅料颜色', align:'center', width:100, sort: true, filter: true}
                                ,{field:'accessoryUnit', title:'单位', align:'center', width:100, sort: true, filter: true}
                                ,{field:'colorName', title:'色组', align:'center', width:100, sort: true, filter: true}
                                ,{field:'sizeName', title:'尺码', align:'center', width:100, sort: true, filter: true}
                                ,{field:'orderPieceUsage', title:'客供单耗', align:'center', width:120, sort: true, filter: true,excel: {cellType: 'n'}}
                                ,{field:'pieceUsage', title:'单耗', align:'center', width:90, sort: true, filter: true,excel: {cellType: 'n'}}
                                ,{field:'orderPrice', title:'客供单价', align:'center', width:120, sort: true, filter: true,excel: {cellType: 'n'}}
                                ,{field:'price', title:'单价', align:'center', width:90, sort: true, filter: true,excel: {cellType: 'n'}}
                                ,{field:'orderAccessoryCount', title:'客供总量', align:'center', width:100, sort: true, filter: true, totalRow: true,excel: {cellType: 'n'}}
                                ,{field:'accessoryCount', title:'订购量', align:'center', width:100, sort: true, filter: true, totalRow: true,excel: {cellType: 'n'}}
                                ,{field:'orderSumMoney', title:'客供金额', align:'center', width:100, sort: true, filter: true, totalRow: true,excel: {cellType: 'n'}}
                                ,{field:'sumMoney', title:'订购金额', align:'center', width:100, sort: true, filter: true, totalRow: true,excel: {cellType: 'n'}}
                                ,{field:'addFee', title:'附加费', align:'center', width:100, sort: true, filter: true, totalRow: true,excel: {cellType: 'n'}}
                                ,{field:'returnCount', title:'回料', align:'center', width:120, sort: true, filter: true, totalRow: true,excel: {cellType: 'n'}}
                                ,{field:'accessoryAccountCount', title:'对账', align:'center', width:120, sort: true, filter: true, totalRow: true,excel: {cellType: 'n'}}
                                ,{field:'returnSumMoney', title:'对账金额', align:'center', width:120, sort: true, filter: true, totalRow: true,excel: {cellType: 'n'}}
                            ]]
                            ,loading:true
                            ,data: reportData
                            ,height: 'full-130'
                            ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                            ,title: '面辅料汇总查询报表'
                            ,totalRow: true
                            ,page: true
                            ,overflow: 'tips'
                            ,limits: [500, 1000, 2000]
                            ,limit: 1000 //每页默认显示的数量
                            ,done: function () {
                                soulTable.render(this);
                                layer.close(load);
                                element.render();
                            }
                        });
                    } else {
                        layer.msg("获取失败！", {icon: 2});
                    }
                }, error: function () {
                    layer.msg("获取失败！", {icon: 2});
                }
            });
        }
        return false;
    });

    //监听行工具事件
    table.on('tool(reportTable)', function(obj){
        var data = obj.data;
        var thisOrderName = data.orderName;
        var thisClothesVersionNumber = data.clothesVersionNumber;
        var thisCustomer = data.customerName;
        if(obj.event === 'detail'){
            var index = layer.open({
                type: 1 //Page层类型
                , title: '物料详情'
                // , btn: ['下载/打印']
                , shade: 0.6 //遮罩透明度
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , content: $("#fabricAccessoryTab")
                , cancel : function (i,layero) {
                    table.render({
                        elem: '#summaryTable'
                        ,height: 'full-130'
                        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                        ,cols: [[]]
                        ,loading:false
                        ,totalRow: true
                        ,page: true
                        ,even: true
                        ,limits: [50, 100, 200]
                        ,limit: 100 //每页默认显示的数量
                        ,done: function (res, curr, count) {
                            soulTable.render(this);
                        }
                    });
                    table.render({
                        elem: '#inStoreTable'
                        ,height: 'full-130'
                        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                        ,cols: [[]]
                        ,loading:false
                        ,totalRow: true
                        ,page: true
                        ,even: true
                        ,limits: [50, 100, 200]
                        ,limit: 100 //每页默认显示的数量
                        ,done: function (res, curr, count) {
                            soulTable.render(this);
                        }
                    });
                    table.render({
                        elem: '#storageTable'
                        ,height: 'full-130'
                        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                        ,cols: [[]]
                        ,loading:false
                        ,totalRow: true
                        ,page: true
                        ,even: true
                        ,limits: [50, 100, 200]
                        ,limit: 100 //每页默认显示的数量
                        ,done: function (res, curr, count) {
                            soulTable.render(this);
                        }
                    });
                    table.render({
                        elem: '#tmpStorageTable'
                        ,height: 'full-130'
                        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                        ,cols: [[]]
                        ,loading:false
                        ,totalRow: true
                        ,page: true
                        ,even: true
                        ,limits: [50, 100, 200]
                        ,limit: 100 //每页默认显示的数量
                        ,done: function (res, curr, count) {
                            soulTable.render(this);
                        }
                    });
                    table.render({
                        elem: '#outStoreTable'
                        ,height: 'full-130'
                        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                        ,cols: [[]]
                        ,loading:false
                        ,totalRow: true
                        ,page: true
                        ,even: true
                        ,limits: [50, 100, 200]
                        ,limit: 100 //每页默认显示的数量
                        ,done: function (res, curr, count) {
                            soulTable.render(this);
                        }
                    });
                }
            });
            layer.full(index);
            var load = layer.load(2);
            setTimeout(function () {
                if ($("#type").val() === "面料"){
                    $.ajax({
                        url: "/erp/searchfabricleakbyorder",
                        type:'GET',
                        data: {
                            orderName: data.orderName
                        },
                        success: function (data) {
                            layer.close(load);
                            var fabricDifferenceList = data.fabricDifferenceList;
                            var fabricDetailList = data.fabricDetailList;
                            var fabricStorageList = data.fabricStorageList;
                            var fabricDetailTmpList = data.fabricDetailTmpList;
                            var fabricOutRecordList = data.fabricOutRecordList;
                            table.render({
                                elem: '#summaryTable'
                                ,height: 'full-150'
                                ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                                ,cols: [[
                                    {type: 'numbers', align:'center', title:'序号', minWidth:80}
                                    ,{field: 'clothesVersionNumber', title:'单号', align:'center', minWidth:120, sort: true, filter: true, totalRowText: '合计'}
                                    ,{field: 'orderName', title:'款号', align:'center', minWidth:120, sort: true, filter: true}
                                    ,{field: 'fabricName', title:'面料名', align:'center', minWidth:120, sort: true, filter: true}
                                    ,{field: 'fabricNumber', title:'面料号', align:'center', minWidth:120, sort: true, filter: true}
                                    ,{field: 'supplier', title:'供应商', align:'center', minWidth:100, sort: true, filter: true}
                                    ,{field: 'colorName', title:'色组', align:'center', minWidth:100, sort: true, filter: true}
                                    ,{field: 'fabricColor', title:'面料颜色', align:'center', minWidth:120, sort: true, filter: true}
                                    ,{field: 'fabricUse', title:'用途', align:'center', minWidth:120, sort: true, filter: true}
                                    ,{field: 'unit', title:'单位', align:'center', minWidth:90, sort: true, filter: true}
                                    ,{field: 'orderCount', title:'订购量', align:'center', minWidth:100, sort: true, filter: true,excel: {cellType: 'n'}, totalRow: true}
                                    ,{field: 'actualCount', title:'回布量', align:'center', minWidth:100, sort: true, filter: true,excel: {cellType: 'n'}, totalRow: true}
                                    ,{field: 'differenceCount', title:'差异', align:'center', minWidth:100, sort: true, filter: true,excel: {cellType: 'n'}, totalRow: true}
                                    ,{field: 'batchNumber', title:'回布卷数', align:'center', minWidth:100, sort: true, filter: true,excel: {cellType: 'n'}, totalRow: true}

                                ]]
                                ,data: fabricDifferenceList
                                ,loading:false
                                ,totalRow: true
                                ,page: true
                                ,even: true
                                ,overflow: "tips"
                                ,limits: [50, 100, 200]
                                ,limit: 100 //每页默认显示的数量
                                ,done: function (res, curr, count) {
                                    soulTable.render(this);
                                }
                            });
                            table.render({
                                elem: '#inStoreTable'
                                ,height: 'full-150'
                                ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                                ,cols: [[
                                    {type: 'numbers', align:'center', title:'序号', minWidth:80}
                                    ,{field: 'clothesVersionNumber', title:'单号', align:'center', minWidth:120, sort: true, filter: true, totalRowText: '合计'}
                                    ,{field: 'orderName', title:'款号', align:'center', minWidth:120, sort: true, filter: true}
                                    ,{field: 'fabricName', title:'面料名', align:'center', minWidth:120, sort: true, filter: true}
                                    ,{field: 'fabricNumber', title:'面料号', align:'center', minWidth:120, sort: true, filter: true}
                                    ,{field: 'supplier', title:'供应商', align:'center', minWidth:100, sort: true, filter: true}
                                    ,{field: 'colorName', title:'色组', align:'center', minWidth:100, sort: true, filter: true}
                                    ,{field: 'fabricColor', title:'面料颜色', align:'center', minWidth:120, sort: true, filter: true}
                                    ,{field: 'unit', title:'单位', align:'center', minWidth:90, sort: true, filter: true}
                                    ,{field: 'weight', title:'数量', align:'center', minWidth:100, sort: true, filter: true,excel: {cellType: 'n'}, totalRow: true}
                                    ,{field: 'batchNumber', title:'卷数', align:'center', minWidth:100, sort: true, filter: true,excel: {cellType: 'n'}, totalRow: true}
                                    ,{field: 'jarName', title:'缸号', align:'center', minWidth:100, sort: true, filter: true}
                                    ,{field: 'location', title:'位置', align:'center', minWidth:100, sort: true, filter: true}
                                    ,{field: 'returnTime', title:'日期', align:'center', minWidth:100, sort: true, filter: true, templet: function (d) {
                                            return moment(d.returnTime).format("YYYY-MM-DD");
                                        }}
                                    ,{field: 'operateType', title:'操作类型', align:'center', minWidth:100, sort: true, filter: true}
                                    ,{field: 'remark', title:'备注', align:'center', minWidth:100, sort: true, filter: true}

                                ]]
                                ,data: fabricDetailList
                                ,loading:false
                                ,totalRow: true
                                ,page: true
                                ,even: true
                                ,overflow: "tips"
                                ,limits: [50, 100, 200]
                                ,limit: 100 //每页默认显示的数量
                                ,done: function (res, curr, count) {
                                    soulTable.render(this);
                                }
                            });
                            table.render({
                                elem: '#storageTable'
                                ,height: 'full-150'
                                ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                                ,cols: [[
                                    {type: 'numbers', align:'center', title:'序号', minWidth:80}
                                    ,{field: 'clothesVersionNumber', title:'单号', align:'center', minWidth:120, sort: true, filter: true, totalRowText: '合计'}
                                    ,{field: 'orderName', title:'款号', align:'center', minWidth:120, sort: true, filter: true}
                                    ,{field: 'fabricName', title:'面料名', align:'center', minWidth:120, sort: true, filter: true}
                                    ,{field: 'fabricNumber', title:'面料号', align:'center', minWidth:120, sort: true, filter: true}
                                    ,{field: 'supplier', title:'供应商', align:'center', minWidth:100, sort: true, filter: true}
                                    ,{field: 'colorName', title:'色组', align:'center', minWidth:100, sort: true, filter: true}
                                    ,{field: 'fabricColor', title:'面料颜色', align:'center', minWidth:120, sort: true, filter: true}
                                    ,{field: 'unit', title:'单位', align:'center', minWidth:90, sort: true, filter: true}
                                    ,{field: 'weight', title:'数量', align:'center', minWidth:100, sort: true, filter: true,excel: {cellType: 'n'}, totalRow: true}
                                    ,{field: 'batchNumber', title:'卷数', align:'center', minWidth:100, sort: true, filter: true,excel: {cellType: 'n'}, totalRow: true}
                                    ,{field: 'jarName', title:'缸号', align:'center', minWidth:100, sort: true, filter: true}
                                    ,{field: 'location', title:'位置', align:'center', minWidth:100, sort: true, filter: true}
                                    ,{field: 'returnTime', title:'日期', align:'center', minWidth:100, sort: true, filter: true, templet: function (d) {
                                            return moment(d.returnTime).format("YYYY-MM-DD");
                                        }}
                                    ,{field: 'operateType', title:'操作类型', align:'center', minWidth:100, sort: true, filter: true}
                                    ,{field: 'remark', title:'备注', align:'center', minWidth:100, sort: true, filter: true}

                                ]]
                                ,data: fabricStorageList
                                ,loading:false
                                ,totalRow: true
                                ,page: true
                                ,even: true
                                ,overflow: "tips"
                                ,limits: [50, 100, 200]
                                ,limit: 100 //每页默认显示的数量
                                ,done: function (res, curr, count) {
                                    soulTable.render(this);
                                }
                            });
                            table.render({
                                elem: '#tmpStorageTable'
                                ,height: 'full-150'
                                ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                                ,cols: [[
                                    {type: 'numbers', align:'center', title:'序号', minWidth:80}
                                    ,{field: 'clothesVersionNumber', title:'单号', align:'center', minWidth:120, sort: true, filter: true, totalRowText: '合计'}
                                    ,{field: 'orderName', title:'款号', align:'center', minWidth:120, sort: true, filter: true}
                                    ,{field: 'fabricName', title:'面料名', align:'center', minWidth:120, sort: true, filter: true}
                                    ,{field: 'fabricNumber', title:'面料号', align:'center', minWidth:120, sort: true, filter: true}
                                    ,{field: 'supplier', title:'供应商', align:'center', minWidth:100, sort: true, filter: true}
                                    ,{field: 'colorName', title:'色组', align:'center', minWidth:100, sort: true, filter: true}
                                    ,{field: 'fabricColor', title:'面料颜色', align:'center', minWidth:120, sort: true, filter: true}
                                    ,{field: 'unit', title:'单位', align:'center', minWidth:90, sort: true, filter: true}
                                    ,{field: 'weight', title:'数量', align:'center', minWidth:100, sort: true, filter: true,excel: {cellType: 'n'}, totalRow: true}
                                    ,{field: 'batchNumber', title:'卷数', align:'center', minWidth:100, sort: true, filter: true,excel: {cellType: 'n'}, totalRow: true}
                                    ,{field: 'jarName', title:'缸号', align:'center', minWidth:100, sort: true, filter: true}
                                    ,{field: 'location', title:'位置', align:'center', minWidth:100, sort: true, filter: true}
                                    ,{field: 'returnTime', title:'日期', align:'center', minWidth:100, sort: true, filter: true, templet: function (d) {
                                            return moment(d.returnTime).format("YYYY-MM-DD");
                                        }}
                                    ,{field: 'operateType', title:'操作类型', align:'center', minWidth:100, sort: true, filter: true}
                                    ,{field: 'remark', title:'备注', align:'center', minWidth:100, sort: true, filter: true}

                                ]]
                                ,data: fabricDetailTmpList
                                ,loading:false
                                ,totalRow: true
                                ,page: true
                                ,even: true
                                ,overflow: "tips"
                                ,limits: [50, 100, 200]
                                ,limit: 100 //每页默认显示的数量
                                ,done: function (res, curr, count) {
                                    soulTable.render(this);
                                }
                            });
                            table.render({
                                elem: '#outStoreTable'
                                ,height: 'full-150'
                                ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                                ,cols: [[
                                    {type: 'numbers', align:'center', title:'序号', minWidth:80}
                                    ,{field: 'clothesVersionNumber', title:'单号', align:'center', minWidth:120, sort: true, filter: true, totalRowText: '合计'}
                                    ,{field: 'orderName', title:'款号', align:'center', minWidth:120, sort: true, filter: true}
                                    ,{field: 'fabricName', title:'面料名', align:'center', minWidth:120, sort: true, filter: true}
                                    ,{field: 'fabricNumber', title:'面料号', align:'center', minWidth:120, sort: true, filter: true}
                                    ,{field: 'supplier', title:'供应商', align:'center', minWidth:100, sort: true, filter: true}
                                    ,{field: 'colorName', title:'色组', align:'center', minWidth:100, sort: true, filter: true}
                                    ,{field: 'fabricColor', title:'面料颜色', align:'center', minWidth:120, sort: true, filter: true}
                                    ,{field: 'unit', title:'单位', align:'center', minWidth:90, sort: true, filter: true}
                                    ,{field: 'weight', title:'数量', align:'center', minWidth:100, sort: true, filter: true,excel: {cellType: 'n'}, totalRow: true}
                                    ,{field: 'batchNumber', title:'卷数', align:'center', minWidth:100, sort: true, filter: true,excel: {cellType: 'n'}, totalRow: true}
                                    ,{field: 'jarName', title:'缸号', align:'center', minWidth:100, sort: true, filter: true}
                                    ,{field: 'location', title:'位置', align:'center', minWidth:100, sort: true, filter: true}
                                    ,{field: 'returnTime', title:'日期', align:'center', minWidth:100, sort: true, filter: true, templet: function (d) {
                                            return moment(d.returnTime).format("YYYY-MM-DD");
                                        }}
                                    ,{field: 'operateType', title:'操作类型', align:'center', minWidth:100, sort: true, filter: true}
                                    ,{field: 'receiver', title:'接收', align:'center', minWidth:100, sort: true, filter: true}
                                    ,{field: 'remark', title:'备注', align:'center', minWidth:100, sort: true, filter: true}

                                ]]
                                ,data: fabricOutRecordList
                                ,loading:false
                                ,totalRow: true
                                ,page: true
                                ,even: true
                                ,overflow: "tips"
                                ,limits: [50, 100, 200]
                                ,limit: 100 //每页默认显示的数量
                                ,done: function (res, curr, count) {
                                    soulTable.render(this);
                                }
                            });
                        }, error: function () {
                            layer.msg("出错了~~~");
                        }
                    })
                } else if ($("#type").val() === "辅料"){
                    $.ajax({
                        url: "/erp/searchaccessoryleakbyorder",
                        type:'GET',
                        data: {
                            orderName: data.orderName
                        },
                        success: function (data) {
                            layer.close(load);
                            var summaryData = data.plan;
                            $.each(summaryData, function(index,element){
                                element.diffCount = element.accessoryReturnCount - element.accessoryPlanCount;
                            });
                            var inStoreData = data.inStore;
                            var outData = data.out;
                            var storageData = data.storage;
                            table.render({
                                elem: '#summaryTable'
                                ,height: 'full-150'
                                ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                                ,cols: [[
                                    {type: 'numbers', align:'center', title:'序号', minWidth:80}
                                    ,{field: 'clothesVersionNumber', title:'单号', align:'center', minWidth:120, sort: true, filter: true, totalRowText: '合计'}
                                    ,{field: 'orderName', title:'款号', align:'center', minWidth:120, sort: true, filter: true}
                                    ,{field: 'accessoryName', title:'名称', align:'center', minWidth:120, sort: true, filter: true}
                                    ,{field: 'accessoryNumber', title:'编号', align:'center', minWidth:120, sort: true, filter: true}
                                    ,{field: 'specification', title:'规格', align:'center', minWidth:100, sort: true, filter: true}
                                    ,{field: 'accessoryColor', title:'颜色', align:'center', minWidth:100, sort: true, filter: true}
                                    ,{field: 'accessoryUnit', title:'单位', align:'center', minWidth:90, sort: true, filter: true}
                                    ,{field: 'supplier', title:'供应商', align:'center', minWidth:100, sort: true, filter: true}
                                    ,{field: 'colorName', title:'色组', align:'center', minWidth:100, sort: true, filter: true}
                                    ,{field: 'sizeName', title:'尺码', align:'center', minWidth:100, sort: true, filter: true}
                                    ,{field: 'accessoryCount', title:'订购量', align:'center', minWidth:90, sort: true, filter: true,excel: {cellType: 'n'}, totalRow: true, templet: function (d) {
                                            return toDecimalFour(d.accessoryCount);
                                        }}
                                    ,{field: 'accessoryReturnCount', title:'入库量', align:'center', minWidth:90, sort: true, filter: true,excel: {cellType: 'n'}, totalRow: true, templet: function (d) {
                                            return toDecimalFour(d.accessoryReturnCount);
                                        }}
                                    ,{field: 'orderCount', title:'订单量', align:'center', minWidth:90, sort: true, filter: true,excel: {cellType: 'n'}}
                                    ,{field: 'pieceUsage', title:'单耗', align:'center', minWidth:90, sort: true, filter: true,excel: {cellType: 'n'}, templet: function (d) {
                                            return toDecimalFour(d.pieceUsage);
                                        }}
                                    ,{field: 'accessoryPlanCount', title:'理论', align:'center', minWidth:90, sort: true, filter: true,excel: {cellType: 'n'}, totalRow: true, templet: function (d) {
                                            return toDecimalFour(d.accessoryPlanCount);
                                        }}
                                    ,{field: 'diffCount', title:'差异', align:'center', minWidth:90, sort: true, filter: true,excel: {cellType: 'n'}, totalRow: true, templet: function (d) {
                                            if (d.diffCount < 0){
                                                return "<p style='color: red'>" + toDecimalFour(d.diffCount) + "</p>";
                                            } else {
                                                return "<p style='color: green'>" + toDecimalFour(d.diffCount) + "</p>";
                                            }
                                        }}
                                    ,{field: 'wellCount', title:'好片', align:'center', minWidth:90, sort: true, filter: true,excel: {cellType: 'n'}, totalRow: true}
                                ]]
                                ,data: summaryData
                                ,loading:false
                                ,totalRow: true
                                ,page: true
                                ,even: true
                                ,overflow: "tips"
                                ,limits: [50, 100, 200]
                                ,limit: 100 //每页默认显示的数量
                                ,done: function (res, curr, count) {
                                    soulTable.render(this);
                                }
                            });
                            table.render({
                                elem: '#inStoreTable'
                                ,height: 'full-150'
                                ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                                ,cols: [[
                                    {type: 'numbers', align:'center', title:'序号', minWidth:80}
                                    ,{field: 'clothesVersionNumber', title:'单号', align:'center', minWidth:120, sort: true, filter: true, totalRowText: '合计'}
                                    ,{field: 'orderName', title:'款号', align:'center', minWidth:120, sort: true, filter: true}
                                    ,{field: 'accessoryName', title:'名称', align:'center', minWidth:120, sort: true, filter: true}
                                    ,{field: 'accessoryNumber', title:'编号', align:'center', minWidth:120, sort: true, filter: true}
                                    ,{field: 'specification', title:'规格', align:'center', minWidth:100, sort: true, filter: true}
                                    ,{field: 'accessoryColor', title:'颜色', align:'center', minWidth:100, sort: true, filter: true}
                                    ,{field: 'accessoryUnit', title:'单位', align:'center', minWidth:90, sort: true, filter: true}
                                    ,{field: 'supplier', title:'供应商', align:'center', minWidth:100, sort: true, filter: true}
                                    ,{field: 'colorName', title:'色组', align:'center', minWidth:100, sort: true, filter: true}
                                    ,{field: 'sizeName', title:'尺码', align:'center', minWidth:100, sort: true, filter: true}
                                    ,{field: 'orderCount', title:'订单量', align:'center', minWidth:90, sort: true, filter: true,excel: {cellType: 'n'}}
                                    ,{field: 'wellCount', title:'好片数', align:'center', minWidth:90, sort: true, filter: true,excel: {cellType: 'n'}}
                                    ,{field: 'inStoreCount', title:'入库量', align:'center', minWidth:90, sort: true, filter: true,excel: {cellType: 'n'}, totalRow: true, templet: function (d) {
                                            return toDecimalFour(d.inStoreCount);
                                        }}
                                    ,{field: 'createTime', title:'日期', align:'center', minWidth:90, sort: true, filter: true, templet: function (d) {
                                            return moment(d.createTime).format("YYYY-MM-DD");
                                        }}
                                    ,{field: 'accessoryLocation', title:'位置', align:'center', minWidth:90, sort: true, filter: true}
                                    ,{field: 'colorNumber', title:'色号', align:'center', minWidth:90, sort: true, filter: true}
                                ]]
                                ,data: inStoreData
                                ,loading:false
                                ,totalRow: true
                                ,page: true
                                ,even: true
                                ,overflow: "tips"
                                ,limits: [50, 100, 200]
                                ,limit: 100 //每页默认显示的数量
                                ,done: function (res, curr, count) {
                                    soulTable.render(this);
                                }
                            });
                            table.render({
                                elem: '#storageTable'
                                ,height: 'full-150'
                                ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                                ,cols: [[
                                    {type: 'numbers', align:'center', title:'序号', minWidth:80}
                                    ,{field: 'clothesVersionNumber', title:'单号', align:'center', minWidth:120, sort: true, filter: true, totalRowText: '合计'}
                                    ,{field: 'orderName', title:'款号', align:'center', minWidth:120, sort: true, filter: true}
                                    ,{field: 'accessoryName', title:'名称', align:'center', minWidth:120, sort: true, filter: true}
                                    ,{field: 'accessoryNumber', title:'编号', align:'center', minWidth:120, sort: true, filter: true}
                                    ,{field: 'specification', title:'规格', align:'center', minWidth:100, sort: true, filter: true}
                                    ,{field: 'accessoryColor', title:'颜色', align:'center', minWidth:100, sort: true, filter: true}
                                    ,{field: 'accessoryUnit', title:'单位', align:'center', minWidth:90, sort: true, filter: true}
                                    ,{field: 'supplier', title:'供应商', align:'center', minWidth:100, sort: true, filter: true}
                                    ,{field: 'colorName', title:'色组', align:'center', minWidth:100, sort: true, filter: true}
                                    ,{field: 'sizeName', title:'尺码', align:'center', minWidth:100, sort: true, filter: true}
                                    ,{field: 'orderCount', title:'订单量', align:'center', minWidth:90, sort: true, filter: true,excel: {cellType: 'n'}}
                                    ,{field: 'wellCount', title:'好片数', align:'center', minWidth:90, sort: true, filter: true,excel: {cellType: 'n'}}
                                    ,{field: 'storageCount', title:'库存量', align:'center', minWidth:90, sort: true, filter: true,excel: {cellType: 'n'}, totalRow: true, templet: function (d) {
                                            return toDecimalFour(d.storageCount);
                                        }}
                                    ,{field: 'updateTime', title:'最后更新', align:'center', minWidth:120, sort: true, filter: true, templet: function (d) {
                                            return moment(d.updateTime).format("YYYY-MM-DD");
                                        }}
                                    ,{field: 'accessoryLocation', title:'位置', align:'center', minWidth:90, sort: true, filter: true}
                                ]]
                                ,data: storageData
                                ,loading:false
                                ,totalRow: true
                                ,page: true
                                ,even: true
                                ,overflow: "tips"
                                ,limits: [50, 100, 200]
                                ,limit: 100 //每页默认显示的数量
                                ,done: function (res, curr, count) {
                                    soulTable.render(this);
                                }
                            });
                            table.render({
                                elem: '#tmpStorageTable'
                                ,height: 'full-150'
                                ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                                ,cols: [[]]
                                ,data: []
                                ,loading:false
                                ,totalRow: true
                                ,page: true
                                ,even: true
                                ,overflow: "tips"
                                ,limits: [50, 100, 200]
                                ,limit: 100 //每页默认显示的数量
                                ,done: function (res, curr, count) {
                                    soulTable.render(this);
                                }
                            });
                            table.render({
                                elem: '#outStoreTable'
                                ,height: 'full-150'
                                ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                                ,cols: [[
                                    {type: 'numbers', align:'center', title:'序号', minWidth:80}
                                    ,{field: 'clothesVersionNumber', title:'单号', align:'center', minWidth:120, sort: true, filter: true, totalRowText: '合计'}
                                    ,{field: 'orderName', title:'款号', align:'center', minWidth:120, sort: true, filter: true}
                                    ,{field: 'accessoryName', title:'名称', align:'center', minWidth:120, sort: true, filter: true}
                                    ,{field: 'accessoryNumber', title:'编号', align:'center', minWidth:120, sort: true, filter: true}
                                    ,{field: 'specification', title:'规格', align:'center', minWidth:100, sort: true, filter: true}
                                    ,{field: 'accessoryColor', title:'颜色', align:'center', minWidth:100, sort: true, filter: true}
                                    ,{field: 'accessoryUnit', title:'单位', align:'center', minWidth:90, sort: true, filter: true}
                                    ,{field: 'supplier', title:'供应商', align:'center', minWidth:100, sort: true, filter: true}
                                    ,{field: 'colorName', title:'色组', align:'center', minWidth:100, sort: true, filter: true}
                                    ,{field: 'sizeName', title:'尺码', align:'center', minWidth:100, sort: true, filter: true}
                                    ,{field: 'orderCount', title:'订单量', align:'center', minWidth:90, sort: true, filter: true,excel: {cellType: 'n'}}
                                    ,{field: 'wellCount', title:'好片数', align:'center', minWidth:90, sort: true, filter: true,excel: {cellType: 'n'}}
                                    ,{field: 'outStoreCount', title:'出库量', align:'center', minWidth:90, sort: true, filter: true,excel: {cellType: 'n'}, totalRow: true, templet: function (d) {
                                            return toDecimalFour(d.outStoreCount);
                                        }}
                                    ,{field: 'createTime', title:'日期', align:'center', minWidth:120, sort: true, filter: true, templet: function (d) {
                                            return moment(d.createTime).format("YYYY-MM-DD");
                                        }}
                                    ,{field: 'accessoryLocation', title:'位置', align:'center', minWidth:90, sort: true, filter: true}
                                ]]
                                ,data: outData
                                ,loading:false
                                ,totalRow: true
                                ,page: true
                                ,even: true
                                ,overflow: "tips"
                                ,limits: [50, 100, 200]
                                ,limit: 100 //每页默认显示的数量
                                ,done: function (res, curr, count) {
                                    soulTable.render(this);
                                }
                            });
                        }, error: function () {
                            layer.msg("出错了~~~");
                        }
                    });
                }
            },500)

        } else if(obj.event === 'leakDetail') {
            var index = layer.open({
                type: 1 //Page层类型
                , title: '未回齐物料详情'
                , btn: ['下载/打印']
                , shade: 0.6 //遮罩透明度
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , content: "<div id='materialDetail'></div><iframe id='printf' src='' width='0' height='0' frameborder='0'></iframe>"
                , yes: function () {
                    $("#printf").empty();
                    var printBoxs = document.getElementsByName('printTable');
                    var newContent = '';
                    for(var i=0;i<printBoxs.length;i++) {
                        newContent += printBoxs[i].innerHTML;
                    }
                    var f = document.getElementById('printf');
                    // f.contentDocument.write('<link rel="stylesheet" type="text/css" href="/css/app.v2.css">');
                    f.contentDocument.write('<style>' +
                        'table {' +
                        'border-collapse:collapse;' +
                        '}' +
                        'td {\n' +
                        '        border: 1px solid #000000;\n' +
                        '    }'+
                        '</style>');
                    f.contentDocument.write(newContent);
                    f.contentDocument.close();
                    window.frames['printf'].focus();
                    try{
                        window.frames['printf'].print();
                    }catch(err){
                        f.contentWindow.print();
                    }
                }
                , cancel : function (i,layero) {
                    $("#materialDetail").empty();
                }
            });
            layer.full(index);
            var load = layer.load(2);
            setTimeout(function () {
                if ($("#type").val() === "面料"){
                    $.ajax({
                        url: "searchfabricleaksummarybyorder",
                        type:'GET',
                        data: {
                            orderName: thisOrderName,
                            colorName:""
                        },
                        success: function (data) {
                            var tableHtml = "";
                            $("#materialDetail").empty();
                            if(data) {
                                tableHtml = "<div class=\"col-md-12\" name='printTable'>\n" +
                                    "               <section class=\"panel panel-default\" style='page-break-after:always;'>\n" +
                                    "                   <header class=\"panel-heading font-bold\" style=\"text-align: center\">\n" +
                                    "                       <div style=\"font-size: xx-large;font-weight: bolder\">德悦服饰面料未回齐报表</div><br>\n" +
                                    "                       <div style=\"font-size: larger;font-weight: bolder; height: 30px\">客户:" + thisCustomer +  "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;版单:"+thisClothesVersionNumber+"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;订单:"+thisOrderName+"</div>\n"+
                                    "                   </header>\n" +
                                    "                   <table class=\"table table-bordered\" style='width: 100%'>\n" +
                                    "                       <tbody>";

                                tableHtml += "<tr style='font-size: larger; font-weight: bolder color: black'><td style='width: 8%'>供应商</td><td style='width: 8%'>订单颜色</td><td style='width: 15%'>面料名称</td><td style='width: 8%'>面料颜色</td><td style='width: 8%'>面料用途</td><td style='width: 8%'>订布数量</td><td style='width: 5%'>单位</td><td style='width: 8%'>回布数量</td><td style='width: 8%'>差异</td><td style='width: 8%'>回布卷数</td><td style='width: 10%'>最后回布</td><td style='width: 8%'>最后数量</td></tr>";
                                var orderSum = 0;
                                var actualSum = 0;
                                var differenceSum = 0;
                                var batchSum = 0;
                                var detailFlag = false;
                                if (data["fabricDifferenceList"]){
                                    var differenceData = data["fabricDifferenceList"];
                                    for (var i=0;i<differenceData.length;i++){
                                        if (differenceData[i].differenceCount < 0){
                                            detailFlag = true;
                                            tableHtml += "<tr style='font-size: larger; font-weight: bolder color: black'><td>"+differenceData[i].supplier+"</td><td>"+differenceData[i].colorName+"</td><td>"+differenceData[i].fabricName+"</td><td>"+differenceData[i].fabricColor+"</td><td>"+differenceData[i].fabricUse+"</td><td>"+toDecimal(differenceData[i].orderCount)+"</td><td>"+differenceData[i].unit+"</td><td>"+toDecimal(differenceData[i].actualCount)+"</td>";
                                            if (differenceData[i].differenceCount < 0){
                                                if (moment(differenceData[i].lastDate).format("YYYY-MM-DD") === '1970-01-01'){
                                                    tableHtml += "<td style='background-color: #FF5722'>"+toDecimal(differenceData[i].differenceCount)+"</td><td>"+differenceData[i].batchNumber+"</td><td></td><td>"+toDecimal(differenceData[i].lastCount)+"</td></tr>";
                                                } else {
                                                    tableHtml += "<td style='background-color: #FF5722'>"+toDecimal(differenceData[i].differenceCount)+"</td><td>"+differenceData[i].batchNumber+"</td><td>"+ moment(differenceData[i].lastDate).format("YYYY-MM-DD") +"<sup id='supText' style='font-weight: bolder; font-style:italic; font-size: large; color: red'>new</sup></td><td>"+toDecimal(differenceData[i].lastCount)+"</td></tr>";
                                                }
                                            }
                                        }
                                        var differenceRow = [];
                                        differenceRow.push(differenceData[i].fabricName);
                                        differenceRow.push(differenceData[i].supplier);
                                        differenceRow.push(differenceData[i].colorName);
                                        differenceRow.push(differenceData[i].fabricColor);
                                        differenceRow.push(differenceData[i].fabricUse);
                                        differenceRow.push(toDecimal(differenceData[i].orderCount));
                                        differenceRow.push(differenceData[i].unit);
                                        differenceRow.push(toDecimal(differenceData[i].actualCount));
                                        differenceRow.push(toDecimal(differenceData[i].differenceCount));
                                        differenceRow.push(differenceData[i].batchNumber);
                                        orderSum += differenceData[i].orderCount;
                                        actualSum += differenceData[i].actualCount;
                                        differenceSum += differenceData[i].differenceCount;
                                        batchSum += differenceData[i].batchNumber;
                                    }
                                }
                                if (detailFlag){
                                    tableHtml += "<tr style='font-size: larger; font-weight: bolder color: black'><td colspan='5' style='text-align:center'>小计：</td><td>"+toDecimal(orderSum)+"</td><td></td><td>"+toDecimal(actualSum)+"</td><td>"+toDecimal(differenceSum)+"</td><td>"+batchSum+"</td><td></td><td></td></tr>";
                                }
                                tableHtml +=  "                       </tbody>" +
                                    "                   </table>" +
                                    "               </section>" +
                                    "              </div>";
                                $("#materialDetail").append(tableHtml);
                                layer.close(load);
                            }
                        }, error: function () {
                            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
                        }
                    })
                } else if ($("#type").val() === "辅料"){
                    $.ajax({
                        url: "searchaccessoryleaksummarybyorder",
                        type:'GET',
                        data: {
                            orderName:thisOrderName
                        },
                        success: function (data) {
                            var tableHtml = "";
                            $("#materialDetail").empty();
                            if(data) {
                                tableHtml = "<div class=\"col-md-12\" name='printTable'>\n" +
                                    "               <section class=\"panel panel-default\" style='page-break-after:always;'>\n" +
                                    "                   <header class=\"panel-heading font-bold\" style=\"text-align: center\">\n" +
                                    "                       <div style=\"font-size: xx-large;font-weight: bolder\">德悦服饰辅料未齐报表</div><br>\n" +
                                    "                       <div style=\"font-size: larger;font-weight: bolder; height: 30px\">客户:" + thisCustomer +  "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;版单:"+thisClothesVersionNumber+"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;订单:"+thisOrderName+"</div>\n"+
                                    "                   </header>\n" +
                                    "                   <table class=\"table table-bordered\" style='width: 100%; font-size: medium; color: black'>" +
                                    "                       <tbody>";
                                tableHtml += "<tr><th style='width: 12%'>辅料名称</th><th style='width: 6%'>辅料规格</th><th style='width: 5%'>单位</th><th style='width: 8%'>辅料颜色</th><th style='width: 6%'>色组</th><th style='width: 5%'>尺码</th><th style='width: 6%'>供应商</th><th style='width: 6%'>订购量</th><th style='width: 6%'>入库数量</th><th style='width: 5%'>订单数</th><th style='width: 5%'>单耗</th><th style='width: 5%'>理论</th><th style='width: 5%'>差异</th><th style='width: 5%'>好片</th><th style='width: 10%'>最后日期</th><th style='width: 5%'>最后数量</th></tr>";
                                var orderSum = 0;
                                var actualSum = 0;
                                var differenceSum = 0;
                                var planSum = 0;

                                if (data["plan"]){
                                    var differenceData = data["plan"];
                                    for (var i=0;i<differenceData.length;i++){
                                        tableHtml += "<tr><td>"+differenceData[i].accessoryName+"</td><td>"+differenceData[i].specification+"</td><td>"+differenceData[i].accessoryUnit+"</td><td>"+differenceData[i].accessoryColor+"</td><td>"+differenceData[i].colorName+"</td><td>"+differenceData[i].sizeName+"</td><td>"+differenceData[i].supplier+"</td><td>"+toDecimal(differenceData[i].accessoryCount)+"</td><td>"+toDecimal(differenceData[i].accessoryReturnCount)+"</td><td>"+differenceData[i].orderCount+"</td><td>"+differenceData[i].pieceUsage+"</td><td>"+differenceData[i].accessoryPlanCount+"</td>";
                                        if (differenceData[i].accessoryReturnCount - differenceData[i].accessoryPlanCount < 0){
                                            if (moment(differenceData[i].lastDate).format("YYYY-MM-DD") == '1970-01-01'){
                                                tableHtml += "<td style='color: red; font-weight: bolder'>"+toDecimal(differenceData[i].accessoryReturnCount - differenceData[i].accessoryPlanCount)+"</td><td>"+differenceData[i].wellCount+"</td><td></td><td>"+toDecimal(differenceData[i].lastCount)+"</td></tr>";
                                            } else {
                                                tableHtml += "<td style='color: red; font-weight: bolder'>"+toDecimal(differenceData[i].accessoryReturnCount - differenceData[i].accessoryPlanCount)+"</td><td>"+differenceData[i].wellCount+"</td><td>"+moment(differenceData[i].lastDate).format("YYYY-MM-DD")+"<sup id='supText' style='font-weight: bolder; font-style:italic; font-size: large; color: red'>new</sup></td><td>"+toDecimal(differenceData[i].lastCount)+"</td></tr>";
                                            }

                                        } else {
                                            tableHtml += "<td style='color: green; font-weight: bolder'>"+toDecimal(differenceData[i].accessoryReturnCount - differenceData[i].accessoryPlanCount)+"</td><td>"+differenceData[i].wellCount+"</td><td>"+moment(differenceData[i].lastDate).format("YYYY-MM-DD")+"<sup id='supText' style='font-weight: bolder; font-style:italic; font-size: large; color: red'>new</sup></td><td>"+toDecimal(differenceData[i].lastCount)+"</td></tr>";
                                        }
                                        var differenceRow = [];
                                        differenceRow.push(differenceData[i].accessoryName);
                                        differenceRow.push(differenceData[i].specification);
                                        differenceRow.push(differenceData[i].accessoryUnit);
                                        differenceRow.push(differenceData[i].accessoryColor);
                                        differenceRow.push(differenceData[i].colorName);
                                        differenceRow.push(differenceData[i].sizeName);
                                        differenceRow.push(differenceData[i].supplier);
                                        differenceRow.push(toDecimal(differenceData[i].accessoryCount));
                                        differenceRow.push(toDecimal(differenceData[i].accessoryReturnCount));
                                        differenceRow.push(differenceData[i].orderCount);
                                        differenceRow.push(differenceData[i].pieceUsage);
                                        differenceRow.push(differenceData[i].accessoryPlanCount);
                                        differenceRow.push(toDecimal(differenceData[i].accessoryReturnCount - differenceData[i].accessoryPlanCount));
                                        differenceRow.push(differenceData[i].wellCount);
                                        orderSum += differenceData[i].accessoryCount;
                                        actualSum += differenceData[i].accessoryReturnCount;
                                        planSum += differenceData[i].accessoryPlanCount;
                                        differenceSum += toDecimal(differenceData[i].accessoryReturnCount - differenceData[i].accessoryPlanCount);
                                    }
                                }
                                tableHtml += "<tr><td colspan='7' style='text-align:center'>小计：</td><td>"+toDecimal(orderSum)+"</td><td>"+toDecimal(actualSum)+"</td><td></td><td></td><td>"+ toDecimal(planSum) +"</td><td>"+toDecimal(differenceSum)+"</td><td></td><td></td><td></td></tr>";
                                tableHtml +=  "                       </tbody>" +
                                    "                   </table>" +
                                    "               </section>" +
                                    "              </div>";
                                $("#materialDetail").append(tableHtml);
                                layer.close(load);
                            }
                        }, error: function () {
                            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
                        }
                    })
                }
            },500)
        }
    });

});


function toDecimal(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*100)/100;
    return f;
}

function toDecimalFour(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*10000)/10000;
    return f;
}

function toDecimalSix(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*1000000)/1000000;
    return f;
}

function dateFormat(fmt, date) {
    var ret;
    var opt = {
        "Y+": date.getFullYear().toString(),        // 年
        "m+": (date.getMonth() + 1).toString(),     // 月
        "d+": date.getDate().toString(),            // 日
        "H+": date.getHours().toString(),           // 时
        "M+": date.getMinutes().toString(),         // 分
        "S+": date.getSeconds().toString()          // 秒
        // 有其他格式化字符需求可以继续添加，必须转化成字符串
    };
    for (var k in opt) {
        ret = new RegExp("(" + k + ")").exec(fmt);
        if (ret) {
            fmt = fmt.replace(ret[1], (ret[1].length == 1) ? (opt[k]) : (opt[k].padStart(ret[1].length, "0")))
        }
    }
    return fmt;
}

function printDeal(){
    $("#printf").empty();
    var printBoxs = document.getElementsByName('printTable');
    var newContent = '';
    for(var i=0;i<printBoxs.length;i++) {
        newContent += printBoxs[i].innerHTML;
    }
    var f = document.getElementById('printf');
    // f.contentDocument.write('<link rel="stylesheet" type="text/css" href="/css/app.v2.css">');
    f.contentDocument.write('<style>' +
        'table {' +
        'border-collapse:collapse;' +
        '}' +
        'td {\n' +
        '        border: 1px solid #000000;\n' +
        '    }'+
        '</style>');
    f.contentDocument.write(newContent);
    f.contentDocument.close();
    window.frames['printf'].focus();
    try{
        window.frames['printf'].print();
    }catch(err){
        f.contentWindow.print();
    }
}

function getFormatDate() {
    var date = new Date();
    var month = date.getMonth() + 1;
    var strDate = date.getDate();
    if (month >= 1 && month <= 9) {
        month = "0" + month;
    }
    if (strDate >= 0 && strDate <= 9) {
        strDate = "0" + strDate;
    }
    var currentDate = date.getFullYear() + "-" + month + "-" + strDate;
    return currentDate;
}

function compare(property) {
    return function (a, b) {
        var value1 = a[property];
        var value2 = b[property];
        return value1 - value2;
    }
}

function reloadInfo() {
    $.ajax({
        url: "/erp/getchecknumbercount",
        type: 'GET',
        data: {},
        success: function (res) {
            if (res.checkNumberCount != null) {
                $("#checkNumberCount").text(res.checkNumberCount);
            } else {
                layer.msg("获取失败！", {icon: 2});
            }
        }, error: function () {
            layer.msg("获取失败！", {icon: 2});
        }
    });
}