var form;
layui.use('form', function(){
    form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
    form.render();
});

var userRole = $("#userRole").val();
var userName = $("#userName").val();
layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.21'
}).extend({                         // 模块别名
    soulTable: 'soulTable'
});
var LODOP; //声明为全局变量

var typeFlag = "面料入库";
$(document).ready(function () {

    form.render('select');

    layui.laydate.render({
        elem: '#from',
        trigger: 'click',
        value: new Date()
    });
    layui.laydate.render({
        elem: '#to',
        trigger: 'click',
        value: new Date()
    });

});

var reportTable;
layui.use(['form', 'table', 'soulTable'], function () {
    var table = layui.table,
        soulTable = layui.soulTable,
        form = layui.form,
        $ = layui.$;
    var load = layer.load();

    reportTable = table.render({
        elem: '#reportTable'
        , cols: [[]]
        , loading: true
        , data: []
        , height: 'full-130'
        , toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
        , defaultToolbar: ['filter', 'print']
        , title: '报表'
        , totalRow: true
        , page: true
        , limits: [500, 1000, 2000]
        , limit: 1000 //每页默认显示的数量
        , done: function (res, curr, count) {
            soulTable.render(this);
            layer.close(load);
        }
        , filter: {
            bottom: true,
            items: ['column', 'data', 'condition'] // 只显示表格列和导出excel两个菜单项
        }
    });

    var initFrom = $("#from").val();
    var initTo = $("#to").val();

    initTable(initFrom, initTo, '面料入库');
    function initTable(from, to, searchType){
        var  param = {};
        param.from = from;
        param.to = to;
        param.searchType = searchType;
        typeFlag = searchType;
        var load = layer.load(2);
        $.ajax({
            url: "fabricaccessoryprintbytime",
            type: 'GET',
            data: param,
            success:function(data){
                if (data.fabricIn) {
                    var reportData = data.fabricIn;
                    table.render({
                        elem: '#reportTable'
                        , cols:[[
                            {type:'checkbox'}
                            ,{type:'numbers', align:'center', title:'序号', width:60}
                            ,{field:'clothesVersionNumber', title:'单号', align:'center', width:120, sort: true, filter: true, totalRowText: '合计'}
                            ,{field:'orderName', title:'款号', align:'center', width:120, sort: true, filter: true}
                            ,{field:'fabricName', title:'名称', align:'center', width:120, sort: true, filter: true}
                            ,{field:'fabricNumber', title:'编号', align:'center', width:90, sort: true, filter: true}
                            ,{field:'colorName', title:'订单颜色', align:'center', width:120, sort: true, filter: true}
                            ,{field:'fabricColor', title:'面料颜色', align:'center', width:120, sort: true, filter: true}
                            ,{field:'fabricColorNumber', title:'色号', align:'center', width:120, sort: true, filter: true}
                            ,{field:'jarName', title:'缸号', align:'center', width:120, sort: true, filter: true}
                            ,{field:'unit', title:'单位', align:'center', width:90, sort: true, filter: true}
                            ,{field:'batchNumber', title:'卷数', align:'center', width:90, sort: true, filter: true}
                            ,{field:'weight', title:'数量', align:'center', width:90, sort: true, filter: true}
                            ,{field:'location', title:'位置', align:'center', width:90, sort: true, filter: true}
                            ,{field:'supplier', title:'供应商', align:'center', width:90, sort: true, filter: true}
                            ,{field:'returnTime', title:'时间', align:'center', width:120, sort: true, filter: true,templet: function (d) {
                                    return moment(d.returnTime).format("YYYY-MM-DD");
                                }}
                            ,{field:'operateType', title:'操作', align:'center', width:120, sort: true, filter: true}
                        ]]
                        , loading: true
                        , data: reportData   // 将新数据重新载入表格
                        , height: 'full-130'
                        , toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                        , defaultToolbar: ['filter', 'print']
                        , title: '报表'
                        , totalRow: true
                        , page: true
                        ,overflow: 'tips'
                        , limits: [500, 1000, 2000]
                        , limit: 1000 //每页默认显示的数量
                        , done: function (res, curr, count) {
                            soulTable.render(this);
                            layer.close(load);
                        }
                    });
                }
                else if (data.fabricOut) {
                    var reportData = data.fabricOut;
                    table.render({
                        elem: '#reportTable'
                        ,cols:[[
                            {type:'checkbox'}
                            ,{type:'numbers', align:'center', title:'序号', width:60}
                            ,{field:'clothesVersionNumber', title:'单号', align:'center', width:120, sort: true, filter: true, totalRowText: '合计'}
                            ,{field:'orderName', title:'款号', align:'center', width:120, sort: true, filter: true}
                            ,{field:'fabricName', title:'名称', align:'center', width:120, sort: true, filter: true}
                            ,{field:'fabricNumber', title:'编号', align:'center', width:90, sort: true, filter: true}
                            ,{field:'colorName', title:'订单颜色', align:'center', width:120, sort: true, filter: true}
                            ,{field:'fabricColor', title:'面料颜色', align:'center', width:120, sort: true, filter: true}
                            ,{field:'fabricColorNumber', title:'色号', align:'center', width:90, sort: true, filter: true}
                            ,{field:'jarName', title:'缸号', align:'center', width:90, sort: true, filter: true}
                            ,{field:'unit', title:'单位', align:'center', width:90, sort: true, filter: true}
                            ,{field:'batchNumber', title:'卷数', align:'center', width:90, sort: true, filter: true}
                            ,{field:'weight', title:'数量', align:'center', width:90, sort: true, filter: true}
                            ,{field:'location', title:'位置', align:'center', width:90, sort: true, filter: true}
                            ,{field:'supplier', title:'供应商', align:'center', width:90, sort: true, filter: true}
                            ,{field:'returnTime', title:'时间', align:'center', width:120, sort: true, filter: true,templet: function (d) {
                                    return moment(d.returnTime).format("YYYY-MM-DD");
                                }}
                            ,{field:'receiver', title:'接收', align:'center', width:90, sort: true, filter: true}
                            ,{field:'operateType', title:'操作', align:'center', width:80, sort: true, filter: true}
                        ]]
                        ,data: reportData   // 将新数据重新载入表格
                        , loading: true
                        , height: 'full-130'
                        , toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                        , defaultToolbar: ['filter', 'print']
                        , title: '报表'
                        , totalRow: true
                        , page: true
                        , limits: [500, 1000, 2000]
                        , limit: 1000 //每页默认显示的数量
                        ,done: function (res, curr, count) {
                            soulTable.render(this);
                            layer.close(load);
                        }
                    });
                }
                else if (data.accessoryIn) {
                    var reportData = data.accessoryIn;
                    table.render({
                        elem: '#reportTable'
                        ,cols:[[
                            {type:'checkbox'}
                            ,{type:'numbers', align:'center', title:'序号', width:60}
                            ,{field:'clothesVersionNumber', title:'单号', align:'center', width:120, sort: true, filter: true, totalRowText: '合计'}
                            ,{field:'orderName', title:'款号', align:'center', width:120, sort: true, filter: true}
                            ,{field:'accessoryName', title:'名称', align:'center', width:120, sort: true, filter: true}
                            ,{field:'accessoryNumber', title:'编号', align:'center', width:120, sort: true, filter: true}
                            ,{field:'specification', title:'规格', align:'center', width:90, sort: true, filter: true}
                            ,{field:'accessoryUnit', title:'单位', align:'center', width:90, sort: true, filter: true}
                            ,{field:'accessoryColor', title:'颜色', align:'center', width:120, sort: true, filter: true}
                            ,{field:'colorName', title:'色组', align:'center', width:90, sort: true, filter: true}
                            ,{field:'sizeName', title:'尺码', align:'center', width:90, sort: true, filter: true}
                            ,{field:'pieceUsage', title:'单耗', align:'center', width:90, sort: true, filter: true}
                            ,{field:'inStoreCount', title:'数量', align:'center', width:90, sort: true, filter: true}
                            ,{field:'accessoryLocation', title:'位置', align:'center', width:90, sort: true, filter: true}
                            ,{field:'supplier', title:'供应商', align:'center', width:90, sort: true, filter: true}
                            ,{field:'inStoreTime', title:'时间', align:'center', width:120, sort: true, filter: true,templet: function (d) {
                                    return moment(d.returnTime).format("YYYY-MM-DD");
                                }}
                        ]]
                        ,data: reportData   // 将新数据重新载入表格
                        , loading: true
                        , height: 'full-130'
                        , toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                        , defaultToolbar: ['filter', 'print']
                        , title: '报表'
                        , totalRow: true
                        , page: true
                        , limits: [500, 1000, 2000]
                        , limit: 1000 //每页默认显示的数量
                        ,done: function (res, curr, count) {
                            soulTable.render(this);
                            layer.close(load);
                        }
                    });
                }
                else if (data.accessoryOut) {
                    var reportData = data.accessoryOut;
                    table.render({
                        elem: '#reportTable'
                        ,cols:[[
                            {type:'checkbox'}
                            ,{type:'numbers', align:'center', title:'序号', width:60}
                            ,{field:'clothesVersionNumber', title:'单号', align:'center', width:120, sort: true, filter: true, totalRowText: '合计'}
                            ,{field:'orderName', title:'款号', align:'center', width:120, sort: true, filter: true}
                            ,{field:'accessoryName', title:'名称', align:'center', width:120, sort: true, filter: true}
                            ,{field:'accessoryNumber', title:'编号', align:'center', width:90, sort: true, filter: true}
                            ,{field:'specification', title:'规格', align:'center', width:90, sort: true, filter: true}
                            ,{field:'accessoryUnit', title:'单位', align:'center', width:90, sort: true, filter: true}
                            ,{field:'accessoryColor', title:'颜色', align:'center', width:90, sort: true, filter: true}
                            ,{field:'colorName', title:'色组', align:'center', width:90, sort: true, filter: true}
                            ,{field:'sizeName', title:'尺码', align:'center', width:120, sort: true, filter: true}
                            ,{field:'pieceUsage', title:'单耗', align:'center', width:90, sort: true, filter: true}
                            ,{field:'outStoreCount', title:'数量', align:'center', width:90, sort: true, filter: true}
                            ,{field:'finishCount', title:'累计', align:'center', width:90, sort: true, filter: true}
                            ,{field:'wellCount', title:'好片', align:'center', width:90, sort: true, filter: true}
                            ,{field:'needCount', title:'需求', align:'center', width:90, sort: true, filter: true}
                            ,{field:'diffCount', title:'差异', align:'center', width:90, sort: true, filter: true}
                            ,{field:'accessoryLocation', title:'位置', align:'center', width:90, sort: true, filter: true}
                            ,{field:'supplier', title:'供应商', align:'center', width:90, sort: true, filter: true}
                            ,{field:'createTime', title:'时间', align:'center', width:120, sort: true, filter: true, templet: function (d) {
                                    return moment(d.createTime).format("YYYY-MM-DD");
                                }}
                            ,{field:'receiver', title:'接收', align:'center', width:90, sort: true, filter: true}
                        ]]
                        ,data: reportData   // 将新数据重新载入表格
                        , loading: true
                        , height: 'full-130'
                        , toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                        , defaultToolbar: ['filter', 'print']
                        , title: '报表'
                        , totalRow: true
                        , page: true
                        , limits: [500, 1000, 2000]
                        , limit: 1000 //每页默认显示的数量
                        ,done: function (res, curr, count) {
                            soulTable.render(this);
                            layer.close(load);
                        }
                    });
                }
            }, error: function () {
                layer.msg("获取数据失败", { icon: 2 });
            }
        });
        return false;
    }

    form.on('submit(search)', function (data) {
        var from = $("#from").val();
        var to = $("#to").val();
        var searchType = $("#searchType").val();
        if (from == '' || from == null || to == '' || to == null){
            layer.msg("开始和结束日期不能为空！");
            return false;
        }
        initTable(from,  to, searchType);
        return false;
    });

    table.on('toolbar(reportTable)', function(obj) {
        if (obj.event === 'clearFilter') {
            // 清除所有筛选条件并重载表格
            // 参数: tableId
            soulTable.clearFilter('reportTable')
        }
        else if (obj.event === 'exportExcel') {
            var exportData = table.cache.reportTable;
            soulTable.export('reportTable');
        }
        else if (obj.event === 'print') {
            var data = table.cache.reportTable;
            if (typeFlag === '面料入库'){
                var printDiv ="";
                var printFlag = true;
                var printData = [];
                $.each(data,function (index,item) {
                    if (item.LAY_CHECKED){
                        printData.push(item);
                        if (printFlag){
                            printDiv += "<div><table border=1 cellspacing='0' cellpadding='0' style='border-collapse:collapse; font-size: medium'><h2 align='center'>德悦服饰面料入库单</h2>\n" +
                                "<div class='row'><div align='left'>收货单位:中山德悦</div><div align='right'>供应商:"+ item.supplier +"日期:"+ moment(item.returnTime).format("YYYY-MM-DD") +"</div></div>\n"+
                                "<tr><td align='center' width='5%'>序号</td><td align='center' width='10%'>单号</td><td align='center' width='10%'>款号</td><td align='center' width='20%'>名称</td><td align='center' width='8%'>色组</td><td align='center' width='8%'>面色</td><td align='center' width='9%'>缸号</td><td align='center' width='5%'>单位</td><td align='center' width='7%'>卷数</td><td align='center' width='7%'>数量</td><td align='center' width='10%'>位置</td></tr>\n";
                            printDiv += "<tr><td align='center'>"+ (index + 1) +"</td><td align='center'>"+ item.clothesVersionNumber +"</td><td align='center'>"+ item.orderName +"</td><td align='center'>"+ item.fabricName +"</td><td align='center'>"+ item.colorName +"</td><td align='center'>"+ item.fabricColor +"</td><td align='center'>"+ item.jarName +"</td><td align='center'>"+ item.unit +"</td><td align='center'>"+ item.batchNumber +"</td><td align='center'>"+ item.weight +"</td><td align='center'>"+ item.location +"</td></tr>\n";
                            printFlag = false;
                        } else {
                            printDiv += "<tr><td align='center'>"+ (index + 1) +"</td><td align='center'>"+ item.clothesVersionNumber +"</td><td align='center'>"+ item.orderName +"</td><td align='center'>"+ item.fabricName +"</td><td align='center'>"+ item.colorName +"</td><td align='center'>"+ item.fabricColor +"</td><td align='center'>"+ item.jarName +"</td><td align='center'>"+ item.unit +"</td><td align='center'>"+ item.batchNumber +"</td><td align='center'>"+ item.weight +"</td><td align='center'>"+ item.location +"</td></tr>\n";
                        }
                    }
                });
                printDiv += "<tr><td colspan='7' align='center'>仓库负责人:"+ userName +"</td><td colspan='7' align='center'>送货人签字:</td></tr>\n";
                printDiv += "</table></div>\n";
                if (printData.length === 0){
                    layer.msg("打印数据不能为空！", {icon: 2});
                    return false;
                } else {
                    layer.msg("打印启动！", {icon: 1});
                    LODOP=getLodop();
                    // LODOP.SET_PRINT_PAGESIZE(2,'210mm','297mm');
                    LODOP.PRINT_INIT("面料入库");
                    LODOP.ADD_PRINT_HTM("2%","0%","100%","96%", printDiv);
                    LODOP.PREVIEW();
                }
            }
            else if (typeFlag === '面料出库'){
                var printDiv ="";
                var printFlag = true;
                var printData = [];
                var destination = "";
                $.each(data,function (index,item) {
                    if (item.LAY_CHECKED){
                        printData.push(item);
                        destination = item.receiver;
                        if (printFlag){
                            printDiv += "<div><table border=1 cellspacing='0' cellpadding='0' style='border-collapse:collapse; font-size: medium'><h2 align='center'>德悦服饰面料出库单</h2></tr>\n" +
                                "<div class='row'><div align='left'>收货单位:"+ destination +"</div><div align='right'>供应商:"+ item.supplier +"日期:"+ moment(item.returnTime).format("YYYY-MM-DD") +"</div></div>\n"+
                                "<tr><td align='center' width='5%'>序号</td><td align='center' width='10%'>单号</td><td align='center' width='10%'>款号</td><td align='center' width='20%'>名称</td><td align='center' width='8%'>色组</td><td align='center' width='8%'>面色</td><td align='center' width='9%'>缸号</td><td align='center' width='5%'>单位</td><td align='center' width='7%'>卷数</td><td align='center' width='7%'>数量</td><td align='center' width='10%'>位置</td></tr>\n";
                            printDiv += "<tr><td align='center'>"+ (index + 1) +"</td><td align='center'>"+ item.clothesVersionNumber +"</td><td align='center'>"+ item.orderName +"</td><td align='center'>"+ item.fabricName +"</td><td align='center'>"+ item.colorName +"</td><td align='center'>"+ item.fabricColor +"</td><td align='center'>"+ item.jarName +"</td><td align='center'>"+ item.unit +"</td><td align='center'>"+ item.batchNumber +"</td><td align='center'>"+ item.weight +"</td><td align='center'>"+ item.location +"</td></tr>\n";
                            printFlag = false;
                        } else {
                            printDiv += "<tr><td align='center'>"+ (index + 1) +"</td><td align='center'>"+ item.clothesVersionNumber +"</td><td align='center'>"+ item.orderName +"</td><td align='center'>"+ item.fabricName +"</td><td align='center'>"+ item.colorName +"</td><td align='center'>"+ item.fabricColor +"</td><td align='center'>"+ item.jarName +"</td><td align='center'>"+ item.unit +"</td><td align='center'>"+ item.batchNumber +"</td><td align='center'>"+ item.weight +"</td><td align='center'>"+ item.location +"</td></tr>\n";
                        }
                    }
                });
                printDiv += "<tr><td colspan='4' align='center'>仓库负责人:"+ userName +"</td><td colspan='7' align='center'>领料人签字:</td></tr>\n";
                printDiv += "</table></div>\n";
                if (printData.length === 0){
                    layer.msg("打印数据不能为空！", {icon: 2});
                    return false;
                } else {
                    layer.msg("打印启动！", {icon: 1});
                    LODOP=getLodop();
                    // LODOP.SET_PRINT_PAGESIZE(2,'210mm','297mm');
                    LODOP.PRINT_INIT("面料出库");
                    LODOP.ADD_PRINT_HTM("2%","0%","100%","96%", printDiv);
                    LODOP.PREVIEW();
                }
            }
            else if (typeFlag === '辅料入库'){
                var printDiv ="";
                var printFlag = true;
                var printData = [];
                $.each(data,function (index,item) {
                    if (item.LAY_CHECKED){
                        printData.push(item);
                        if (printFlag){
                            printDiv += "<div><table border=1 cellspacing='0' cellpadding='0' style='border-collapse:collapse; font-size: medium'><h2 align='center'>德悦服饰辅料入库单</h2></tr>\n" +
                                "<div class='row'><div align='left'>收货单位:中山德悦</div><div align='right'>日期:"+ moment(item.inStoreTime).format("YYYY-MM-DD") +"</div></div>\n"+
                                "<tr><td align='center' width='5%'>序号</td><td align='center' width='10%'>单号</td><td align='center' width='10%'>款号</td><td align='center' width='15%'>名称</td><td align='center' width='10%'>规格</td><td align='center' width='10%'>颜色</td><td align='center' width='10%'>色组</td><td align='center' width='10%'>尺码</td><td align='center' width='7%'>单位</td><td align='center' width='6%'>数量</td><td align='center' width='7%'>单耗</td></tr>\n";
                            printDiv += "<tr><td align='center'>"+ (index + 1) +"</td><td align='center'>"+ item.clothesVersionNumber +"</td><td align='center'>"+ item.orderName +"</td><td align='center'>"+ item.accessoryName +"</td><td align='center'>"+ item.specification +"</td><td align='center'>"+ item.accessoryColor +"</td><td align='center'>"+ item.colorName +"</td><td align='center'>"+ item.sizeName +"</td><td align='center'>"+ item.accessoryUnit +"</td><td align='center'>"+ item.inStoreCount +"</td><td align='center'>"+ item.pieceUsage +"</td></tr>\n";
                            printFlag = false;
                        } else {
                            printDiv += "<tr><td align='center'>"+ (index + 1) +"</td><td align='center'>"+ item.clothesVersionNumber +"</td><td align='center'>"+ item.orderName +"</td><td align='center'>"+ item.accessoryName +"</td><td align='center'>"+ item.specification +"</td><td align='center'>"+ item.accessoryColor +"</td><td align='center'>"+ item.colorName +"</td><td align='center'>"+ item.sizeName +"</td><td align='center'>"+ item.accessoryUnit +"</td><td align='center'>"+ item.inStoreCount +"</td><td align='center'>"+ item.pieceUsage +"</td></tr>\n";
                        }
                    }
                });
                printDiv += "<tr><td colspan='5' align='center'>仓库负责人:"+ userName +"</td><td colspan='6' align='center'>送货人签字:</td></tr>\n";
                printDiv += "</table></div>\n";
                if (printData.length === 0){
                    layer.msg("打印数据不能为空！", {icon: 2});
                    return false;
                } else {
                    layer.msg("打印启动！", {icon: 1});
                    LODOP=getLodop();
                    // LODOP.SET_PRINT_PAGESIZE(2,'210mm','297mm');
                    LODOP.PRINT_INIT("辅料入库");
                    LODOP.ADD_PRINT_HTM("2%","0%","100%","96%", printDiv);
                    LODOP.PREVIEW();
                }
            }
            else if (typeFlag === '辅料出库'){
                var printDiv ="";
                var printFlag = true;
                var printData = [];
                var destination = "";
                $.each(data,function (index,item) {
                    if (item.LAY_CHECKED){
                        printData.push(item);
                        destination = item.receiver;
                        if (printFlag){
                            printDiv += "<div><table border=1 cellspacing='0' cellpadding='0' style='border-collapse:collapse; font-size: medium'><h2 align='center'>德悦服饰辅料出库单</h2></tr>\n" +
                                "<div class='row'><div align='left'>收货单位:" + destination + "</div><div align='right'>日期:"+ moment(item.createTime).format("YYYY-MM-DD") +"</div></div>\n"+
                                "<tr><td align='center' width='8%'>款号</td><td align='center' width='10%'>名称</td><td align='center' width='10%'>规格</td><td align='center' width='6%'>色组</td><td align='center' width='6%'>尺码</td><td align='center' width='6%'>单位</td><td align='center' width='6%'>数量</td><td align='center' width='6%'>单耗</td><td align='center' width='6%'>累计出</td><td align='center' width='6%'>需求</td><td align='center' width='6%'>差异</td><td align='center' width='6%'>位置</td></tr>\n";
                            printDiv += "<tr><td align='center'>"+ item.orderName +"</td><td align='center'>"+ item.accessoryName +"</td><td align='center'>"+ item.specification + item.accessoryColor +"</td><td align='center'>"+ item.colorName +"</td><td align='center'>"+ item.sizeName +"</td><td align='center'>"+ item.accessoryUnit +"</td><td align='center'>"+ item.outStoreCount +"</td><td align='center'>"+ item.pieceUsage +"</td><td align='center'>"+ item.finishCount +"</td><td align='center'>"+ item.needCount +"</td><td align='center'>"+ -item.diffCount +"</td><td align='center'>"+ item.accessoryLocation +"</td></tr>\n";
                            printFlag = false;
                        } else {
                            printDiv += "<tr><td align='center'>"+ item.orderName +"</td><td align='center'>"+ item.accessoryName +"</td><td align='center'>"+ item.specification + item.accessoryColor +"</td><td align='center'>"+ item.colorName +"</td><td align='center'>"+ item.sizeName +"</td><td align='center'>"+ item.accessoryUnit +"</td><td align='center'>"+ item.outStoreCount +"</td><td align='center'>"+ item.pieceUsage +"</td><td align='center'>"+ item.finishCount +"</td><td align='center'>"+ item.needCount +"</td><td align='center'>"+ -item.diffCount +"</td><td align='center'>"+ item.accessoryLocation +"</td></tr>\n";
                        }
                    }
                });
                printDiv += "<tr><td colspan='5' align='center'>仓库负责人:"+ userName +"</td><td colspan='7' align='center'>领料人签字:</td></tr>\n";
                printDiv += "<tr><td colspan='12' align='left'>收到辅料后请三天内确认，有异常请与仓库沟通，没有反馈或造成辅料补数由收货部门承担</td></tr>\n";
                printDiv += "</table></div>\n";
                if (printData.length === 0){
                    layer.msg("打印数据不能为空！", {icon: 2});
                    return false;
                } else {
                    layer.msg("打印启动！", {icon: 1});
                    LODOP=getLodop();
                    // LODOP.SET_PRINT_PAGESIZE(2,'210mm','297mm');
                    LODOP.PRINT_INIT("辅料出库");
                    LODOP.ADD_PRINT_HTM("2%","0%","100%","96%", printDiv);
                    LODOP.PREVIEW();
                }
            }
        }
    });


});
