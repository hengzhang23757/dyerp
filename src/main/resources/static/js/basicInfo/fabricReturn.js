var form;
layui.use('form', function(){
    form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
    form.render();
});
layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.24'
}).extend({                         // 模块别名
    soulTable: 'soulTable'
});
$(document).on('keydown', '.layui-table-edit', function (e) {
    var td = $(this).parent('td')
        ,tr = td.parent('tr')
        ,trs = tr.parent().parent().find('tr')
        ,tr_index = tr.index()
        ,td_index = td.index()
        ,td_last_index = tr.find('[data-edit="text"]:last').index()
        ,td_first_index = tr.find('[data-edit="text"]:first').index();
    switch (e.keyCode) {
        case 13:
            tr.next().find('td').eq(td_index).click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            return false;
        case 39:
            td.nextAll('[data-edit="text"]:first').click();
            if (td_index == td_last_index){
                tr.next().find('td').eq(td_first_index).click();
                if (tr_index == trs.length - 1){
                    trs.eq(0).find('td').eq(td_first_index).click();
                }
            }
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
        case 37:
            td.prevAll('[data-edit="text"]:first').click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
        case 38:
            tr.prev().find('td').eq(td_index).click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
        case 40:
            tr.next().find('td').eq(td_index).click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
    }

})
$(document).ready(function () {
    layui.laydate.render({
        elem: '#updateInStoreTime',
        trigger: 'click'
    });
    layui.laydate.render({
        elem: '#returnInStoreTime',
        trigger: 'click'
    });
    layui.laydate.render({
        elem: '#updateStorageTime',
        trigger: 'click'
    });

    layui.use(['autocomplete'], function () {
        layui.autocomplete.render({
            elem: $('#checkNumber')[0],
            url: 'getfabricchecknumberhint',
            template_val: '{{d}}',
            template_txt: '{{d}}',
            onselect: function (resp) {
            }
        })
    });

    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumber", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });

    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumberTwo", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumberTwo',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderNameTwo',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumberTwo',
                    field: 'clothesVersionNumberTwo'
                }, {
                    name: 'orderNameTwo',
                    field: 'orderNameTwo'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderNameTwo", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumberTwo',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderNameTwo',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumberTwo',
                    field: 'clothesVersionNumberTwo'
                }, {
                    name: 'orderNameTwo',
                    field: 'orderNameTwo'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });

    layui.use(['autocomplete'], function () {
        layui.autocomplete.render({
            elem: $('#toClothesVersionNumber')[0],
            url: 'getversionhint',
            template_val: '{{d}}',
            template_txt: '{{d}}',
            onselect: function (resp) {
                autoComplete(resp)
            }
        })
    });
    form.on('select(test)', function(data){
        var toOrderName = $("#toOrderName").val();
        $.ajax({
            url: "/erp/getordercolorhint",
            data: {"orderName": toOrderName},
            success:function(data){
                $("#toColorName").empty();
                if (data.colorList) {
                    $("#toColorName").append("<option value=''>选择色组</option>");
                    $.each(data.colorList, function(index,element){
                        $("#toColorName").append("<option value='"+element+"'>"+element+"</option>");
                    });
                    form.render('select');
                }
            }, error:function(){
            }
        });
    });
    form.render('select');

});
function autoComplete(keywords) {
    $("#toOrderName").empty();
    $.ajax({
        url: "/erp/getorderbyversion",
        data: {"clothesVersionNumber": keywords},
        success:function(data){
            $("#toOrderName").empty();
            if (data.orderList) {
                $("#toOrderName").append("<option value=''>选择订单号</option>");
                $.each(data.orderList, function(index,element){
                    $("#toOrderName").append("<option value='"+element+"'>"+element+"</option>");
                });
                form.render('select');
            }
        }, error:function(){
        }
    });
}
var fabricReturnTable,changeOrderTable;
layui.use(['form', 'soulTable', 'table', 'laydate'], function () {
    var table = layui.table,
        soulTable = layui.soulTable,
        laydate = layui.laydate,
        form = layui.form,
        $ = layui.$;
    fabricReturnTable = table.render({
        elem: '#fabricReturnTable'
        ,excel: {
            filename: '面料总表.xlsx'
        }
        ,height: 'full-100'
        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
        ,cols: [[]]
        ,loading:true
        ,totalRow: true
        ,page: true
        ,even: true
        ,limits: [50, 100, 200]
        ,limit: 100 //每页默认显示的数量
        ,done: function (res, curr, count) {
            soulTable.render(this);
        }
    });
    initTable('', 0);
    function initTable(orderName, type){
        var param = {};
        if (orderName != null && orderName != ''){
            param.orderName = orderName;
        }
        var loading = layer.load();
        if (type == 0) {
            $.ajax({
                url: "/erp/getoneyearmanufacturefabriclay",
                type: 'POST',
                data: param,
                success: function (res) {
                    layer.close(loading);
                    if (res.data) {
                        var reportData = res.data;
                        table.render({
                            elem: '#fabricReturnTable'
                            ,cols:[[
                                {title: '#', width: 50, collapse: true,lazy: true, children:[
                                        {
                                            title: '入库记录'
                                            ,url: '/erp/getfabricdetailbyinfo'
                                            ,where: function(row){
                                                return {
                                                    fabricID: row.fabricID
                                                }
                                            }
                                            ,loading:true
                                            ,page: false
                                            ,totalRow: true
                                            ,cols: [[
                                                {field: 'fabricName', title: '面料名称',align:'center', minWidth: 200, totalRowText: '合计'},
                                                {field: 'fabricNumber', title: '面料号',align:'center', minWidth: 100 },
                                                {field: 'colorName', title: '订单颜色',align:'center', minWidth: 100 },
                                                {field: 'isHit', title: '是否撞色',align:'center', minWidth: 100 },
                                                {field: 'fabricColor', title: '面料颜色',align:'center', minWidth: 100},
                                                {field: 'fabricColorNumber', title: '面料色号',align:'center', minWidth: 100},
                                                {field: 'jarName', title: '缸号',align:'center', minWidth: 100},
                                                {field: 'unit', title: '单位',align:'center', minWidth: 80},
                                                {field: 'batchNumber', title: '卷数',align:'center', minWidth: 100, totalRow: true},
                                                {field: 'weight', title: '数量',align:'center', minWidth: 100, totalRow: true},
                                                {field: 'location', title: '位置',align:'center', minWidth: 100},
                                                {field: 'returnTime', title: '入库时间',align:'center', minWidth: 120,templet:function (d) {
                                                        return moment(d.returnTime).format("YYYY-MM-DD");
                                                    }},
                                                {field: 'supplier', title: '供应商',align:'center', minWidth: 100},
                                                {field: 'fabricDetailID', align:'center', hide:true},
                                                {field: 'orderName', align:'center', hide:true},
                                                {field: 'clothesVersionNumber', align:'center', hide:true},
                                                {field: 'remark', title: '备注', align:'center', minWidth: 120},
                                                {field: 'fabricID', align:'center', hide:true},
                                                {title: '操作', minWidth: 250,align:'center', templet: function(row) {
                                                        return '<a class="layui-btn layui-btn-xs layui-btn-normal" lay-event="updateFabricReturnData">修改</a><a class="layui-btn layui-btn-xs layui-btn-danger" lay-event="deleteFabricByJar">删除本缸</a>';
                                                    }}
                                            ]]
                                            ,toolEvent: function (obj, pobj) {
                                                // obj 子表当前行对象
                                                // pobj 父表当前行对象
                                                var rowData = obj.data;
                                                var childId = this.id; // 通过 this 对象获取当前子表的id
                                                if (obj.event === 'updateFabricReturnData') {
                                                    var index = layer.open({
                                                        type: 1 //Page层类型
                                                        , title: '修改面料入库信息'
                                                        , btn: ['保存']
                                                        , shade: 0.6 //遮罩透明度
                                                        , maxmin: false //允许全屏最小化
                                                        , anim: 0 //0-6的动画形式，-1不开启
                                                        , content: "<div id=\"fabricInStoreUpdate\" style=\"cursor:default;\">\n" +
                                                            "        <form class=\"layui-form\" style=\"margin: 5px;padding-top: 40px;\" lay-filter=\"inStoreUpdate\">\n" +
                                                            "            <div class=\"layui-row layui-col-space10\">\n" +
                                                            "                <div class=\"layui-col-md3\">\n" +
                                                            "                    <!-- 填充内容 -->\n" +
                                                            "                    <label class=\"layui-form-label\">面料名称</label>\n" +
                                                            "                    <div class=\"layui-input-inline\">\n" +
                                                            "                        <input type=\"text\" name=\"fabricName\" autocomplete=\"off\" class=\"layui-input\" readonly=\"true\">\n" +
                                                            "                    </div>\n" +
                                                            "                </div>\n" +
                                                            "                <div class=\"layui-col-md3\">\n" +
                                                            "                    <!-- 填充内容 -->\n" +
                                                            "                    <label class=\"layui-form-label\">面料号</label>\n" +
                                                            "                    <div class=\"layui-input-inline\">\n" +
                                                            "                        <input type=\"text\" name=\"fabricNumber\" autocomplete=\"off\" class=\"layui-input\" readonly=\"true\">\n" +
                                                            "                    </div>\n" +
                                                            "                </div>\n" +
                                                            "                <div class=\"layui-col-md3\">\n" +
                                                            "                    <!-- 填充内容 -->\n" +
                                                            "                    <label class=\"layui-form-label\">色组</label>\n" +
                                                            "                    <div class=\"layui-input-inline\">\n" +
                                                            "                        <input type=\"text\" name=\"colorName\" autocomplete=\"off\" class=\"layui-input\" readonly=\"true\">\n" +
                                                            "                    </div>\n" +
                                                            "                </div>\n" +
                                                            "                <div class=\"layui-col-md3\">\n" +
                                                            "                    <!-- 填充内容 -->\n" +
                                                            "                    <label class=\"layui-form-label\">是否撞色</label>\n" +
                                                            "                    <div class=\"layui-input-inline\">\n" +
                                                            "                        <input type=\"text\" name=\"isHit\" autocomplete=\"off\" class=\"layui-input\" readonly=\"true\">\n" +
                                                            "                    </div>\n" +
                                                            "                </div>\n" +
                                                            "            </div>\n" +
                                                            "            <div class=\"layui-row layui-col-space10\">\n" +
                                                            "                <div class=\"layui-col-md3\">\n" +
                                                            "                    <!-- 填充内容 -->\n" +
                                                            "                    <label class=\"layui-form-label\">面料颜色</label>\n" +
                                                            "                    <div class=\"layui-input-inline\">\n" +
                                                            "                        <input type=\"text\" name=\"fabricColor\" autocomplete=\"off\" class=\"layui-input\" readonly=\"true\">\n" +
                                                            "                    </div>\n" +
                                                            "                </div>\n" +
                                                            "                <div class=\"layui-col-md3\">\n" +
                                                            "                    <!-- 填充内容 -->\n" +
                                                            "                    <label class=\"layui-form-label\">面料色号</label>\n" +
                                                            "                    <div class=\"layui-input-inline\">\n" +
                                                            "                        <input type=\"text\" name=\"fabricColorNumber\" autocomplete=\"off\" class=\"layui-input\" readonly=\"true\">\n" +
                                                            "                    </div>\n" +
                                                            "                </div>\n" +
                                                            "                <div class=\"layui-col-md3\">\n" +
                                                            "                    <!-- 填充内容 -->\n" +
                                                            "                    <label class=\"layui-form-label\">供应商</label>\n" +
                                                            "                    <div class=\"layui-input-inline\">\n" +
                                                            "                        <input type=\"text\" name=\"supplier\" autocomplete=\"off\" class=\"layui-input\" readonly=\"true\">\n" +
                                                            "                    </div>\n" +
                                                            "                </div>\n" +
                                                            "                <div class=\"layui-col-md3\">\n" +
                                                            "                    <!-- 填充内容 -->\n" +
                                                            "                    <label class=\"layui-form-label\">单位</label>\n" +
                                                            "                    <div class=\"layui-input-inline\">\n" +
                                                            "                        <input type=\"text\" name=\"unit\" autocomplete=\"off\" class=\"layui-input\" readonly=\"true\">\n" +
                                                            "                    </div>\n" +
                                                            "                </div>\n" +
                                                            "\n" +
                                                            "            </div>\n" +
                                                            "            <div class=\"layui-row layui-col-space10\">\n" +
                                                            "                <div class=\"layui-col-md3\">\n" +
                                                            "                    <!-- 填充内容 -->\n" +
                                                            "                    <label class=\"layui-form-label\">缸号</label>\n" +
                                                            "                    <div class=\"layui-input-inline\">\n" +
                                                            "                        <input type=\"text\" name=\"jarName\" autocomplete=\"off\" class=\"layui-input\">\n" +
                                                            "                    </div>\n" +
                                                            "                </div>\n" +
                                                            "                <div class=\"layui-col-md3\">\n" +
                                                            "                    <!-- 填充内容 -->\n" +
                                                            "                    <label class=\"layui-form-label\">卷数</label>\n" +
                                                            "                    <div class=\"layui-input-inline\">\n" +
                                                            "                        <input type=\"text\" name=\"batchNumber\" autocomplete=\"off\" class=\"layui-input\">\n" +
                                                            "                    </div>\n" +
                                                            "                </div>\n" +
                                                            "                <div class=\"layui-col-md3\">\n" +
                                                            "                    <!-- 填充内容 -->\n" +
                                                            "                    <label class=\"layui-form-label\">重量</label>\n" +
                                                            "                    <div class=\"layui-input-inline\">\n" +
                                                            "                        <input type=\"text\" name=\"weight\" autocomplete=\"off\" class=\"layui-input\">\n" +
                                                            "                    </div>\n" +
                                                            "                </div>\n" +
                                                            "                <div class=\"layui-col-md3\">\n" +
                                                            "                    <!-- 填充内容 -->\n" +
                                                            "                    <label class=\"layui-form-label\">入库时间</label>\n" +
                                                            "                    <div class=\"layui-input-inline\">\n" +
                                                            "                        <input type=\"text\" name=\"returnTime\" id=\"updateInStoreTime\" autocomplete=\"off\" class=\"layui-input\" readonly>\n" +
                                                            "                    </div>\n" +
                                                            "                </div>\n" +
                                                            "\n" +
                                                            "            </div>\n" +
                                                            "            <div class=\"layui-row layui-col-space10\">\n" +
                                                            "                <div class=\"layui-col-md3\">\n" +
                                                            "                    <!-- 填充内容 -->\n" +
                                                            "                    <label class=\"layui-form-label\">位置</label>\n" +
                                                            "                    <div class=\"layui-input-inline\">\n" +
                                                            "                        <input type=\"text\" name=\"location\" id=\"updateInStoreLocation\" autocomplete=\"off\" class=\"layui-input\">\n" +
                                                            "                    </div>\n" +
                                                            "                </div>\n" +
                                                            "\n" +
                                                            "            </div>\n" +
                                                            "        </form>\n" +
                                                            "    </div>"
                                                        ,yes: function(index, layero){
                                                            var params = layui.form.val("inStoreUpdate");
                                                            params.fabricDetailID = rowData.fabricDetailID;
                                                            params.fabricID = rowData.fabricID;
                                                            params.orderName = rowData.orderName;
                                                            params.clothesVersionNumber = rowData.clothesVersionNumber;
                                                            $.ajax({
                                                                url: "/erp/updatefabricdetail",
                                                                type: 'POST',
                                                                data: {
                                                                    fabricDetail: JSON.stringify(params)
                                                                },
                                                                success: function (res) {
                                                                    if (res == 0) {
                                                                        layer.close(index);
                                                                        layer.msg("保存成功！", {icon: 1});
                                                                        $("#fabricInStoreUpdate").find("input").val("");
                                                                        reloadTable();
                                                                    } else if (res == 3) {
                                                                        layer.msg("此布已松，无法修改！");
                                                                    } else if (res == 4) {
                                                                        layer.msg("库存不足，无法修改！");
                                                                    } else {
                                                                        layer.msg("保存失败！", {icon: 2});
                                                                    }
                                                                },
                                                                error: function () {
                                                                    layer.msg("保存失败！", {icon: 2});
                                                                }
                                                            })
                                                        }
                                                        , cancel : function (i,layero) {
                                                            $("#fabricInStoreUpdate").find("input").val("");
                                                        }
                                                    });
                                                    layer.full(index);
                                                    $("#fabricInStoreUpdate input[name='fabricName']").val(rowData.fabricName);
                                                    $("#fabricInStoreUpdate input[name='fabricNumber']").val(rowData.fabricNumber);
                                                    $("#fabricInStoreUpdate input[name='colorName']").val(rowData.colorName);
                                                    $("#fabricInStoreUpdate input[name='isHit']").val(rowData.isHit);
                                                    $("#fabricInStoreUpdate input[name='fabricColor']").val(rowData.fabricColor);
                                                    $("#fabricInStoreUpdate input[name='fabricColorNumber']").val(rowData.fabricColorNumber);
                                                    $("#fabricInStoreUpdate input[name='jarName']").val(rowData.jarName);
                                                    $("#fabricInStoreUpdate input[name='unit']").val(rowData.unit);
                                                    $("#fabricInStoreUpdate input[name='batchNumber']").val(rowData.batchNumber);
                                                    $("#fabricInStoreUpdate input[name='weight']").val(rowData.weight);
                                                    $("#fabricInStoreUpdate input[name='location']").val(rowData.location);
                                                    $("#fabricInStoreUpdate input[name='returnTime']").val(moment(rowData.returnTime).format("YYYY-MM-DD"));
                                                    $("#fabricInStoreUpdate input[name='supplier']").val(rowData.supplier);
                                                    form.render('select');
                                                    $("#initCount").val(rowData.inStoreCount);
                                                    return false;
                                                } else if (obj.event === 'deleteFabricByJar'){
                                                    layer.confirm('真的删除吗', function(index){
                                                        $.ajax({
                                                            url: "/erp/deletefabricalldatabyjar",
                                                            type: 'POST',
                                                            data: {
                                                                orderName: rowData.orderName,
                                                                jarName: rowData.jarName
                                                            },
                                                            success: function (res) {
                                                                if (res.result == 0) {
                                                                    layer.close(index);
                                                                    layer.msg("删除成功！", {icon: 1});
                                                                    reloadTable();
                                                                }else if (res == 3){
                                                                    layer.msg("此布已松,不能删除！");
                                                                } else if (res == 4){
                                                                    layer.msg("库存不足,不能删除！");
                                                                } else {
                                                                    layer.msg("删除失败！", {icon: 2});
                                                                }
                                                            },
                                                            error: function () {
                                                                layer.msg("删除失败！", {icon: 2});
                                                            }
                                                        })
                                                    });
                                                } else if (obj.event === 'fabricReturnDataChangeOrder'){
                                                    var index = layer.open({
                                                        type: 1 //Page层类型
                                                        , title: '面料换款'
                                                        , btn: ['保存']
                                                        , shade: 0.6 //遮罩透明度
                                                        , maxmin: false //允许全屏最小化
                                                        , anim: 0 //0-6的动画形式，-1不开启
                                                        , content: "<div id=\"fabricReturnDataChangeOrder\" style=\"cursor:default;\" xmlns=\"http://www.w3.org/1999/html\">\n" +
                                                            "        <form class=\"layui-form\" style=\"margin: 5px;padding-top: 40px;\" lay-filter=\"returnChangeOrder\">\n" +
                                                            "            <div class=\"layui-row layui-col-space10\">\n" +
                                                            "                <div class=\"layui-col-md3\">\n" +
                                                            "                    <!-- 填充内容 -->\n" +
                                                            "                    <label class=\"layui-form-label\">面料名称</label>\n" +
                                                            "                    <div class=\"layui-input-inline\">\n" +
                                                            "                        <input type=\"text\" name=\"fabricName\" autocomplete=\"off\" class=\"layui-input\" readonly=\"true\">\n" +
                                                            "                    </div>\n" +
                                                            "                </div>\n" +
                                                            "                <div class=\"layui-col-md3\">\n" +
                                                            "                    <!-- 填充内容 -->\n" +
                                                            "                    <label class=\"layui-form-label\">面料号</label>\n" +
                                                            "                    <div class=\"layui-input-inline\">\n" +
                                                            "                        <input type=\"text\" name=\"fabricNumber\" autocomplete=\"off\" class=\"layui-input\" readonly=\"true\">\n" +
                                                            "                    </div>\n" +
                                                            "                </div>\n" +
                                                            "                <div class=\"layui-col-md3\">\n" +
                                                            "                    <!-- 填充内容 -->\n" +
                                                            "                    <label class=\"layui-form-label\">色组</label>\n" +
                                                            "                    <div class=\"layui-input-inline\">\n" +
                                                            "                        <input type=\"text\" name=\"colorName\" autocomplete=\"off\" class=\"layui-input\" readonly=\"true\">\n" +
                                                            "                    </div>\n" +
                                                            "                </div>\n" +
                                                            "                <div class=\"layui-col-md3\">\n" +
                                                            "                    <!-- 填充内容 -->\n" +
                                                            "                    <label class=\"layui-form-label\">是否撞色</label>\n" +
                                                            "                    <div class=\"layui-input-inline\">\n" +
                                                            "                        <input type=\"text\" name=\"isHit\" autocomplete=\"off\" class=\"layui-input\" readonly=\"true\">\n" +
                                                            "                    </div>\n" +
                                                            "                </div>\n" +
                                                            "            </div>\n" +
                                                            "            <div class=\"layui-row layui-col-space10\">\n" +
                                                            "                <div class=\"layui-col-md3\">\n" +
                                                            "                    <!-- 填充内容 -->\n" +
                                                            "                    <label class=\"layui-form-label\">面料颜色</label>\n" +
                                                            "                    <div class=\"layui-input-inline\">\n" +
                                                            "                        <input type=\"text\" name=\"fabricColor\" autocomplete=\"off\" class=\"layui-input\" readonly=\"true\">\n" +
                                                            "                    </div>\n" +
                                                            "                </div>\n" +
                                                            "                <div class=\"layui-col-md3\">\n" +
                                                            "                    <!-- 填充内容 -->\n" +
                                                            "                    <label class=\"layui-form-label\">面料色号</label>\n" +
                                                            "                    <div class=\"layui-input-inline\">\n" +
                                                            "                        <input type=\"text\" name=\"fabricColorNumber\" autocomplete=\"off\" class=\"layui-input\" readonly=\"true\">\n" +
                                                            "                    </div>\n" +
                                                            "                </div>\n" +
                                                            "                <div class=\"layui-col-md3\">\n" +
                                                            "                    <!-- 填充内容 -->\n" +
                                                            "                    <label class=\"layui-form-label\">供应商</label>\n" +
                                                            "                    <div class=\"layui-input-inline\">\n" +
                                                            "                        <input type=\"text\" name=\"supplier\" autocomplete=\"off\" class=\"layui-input\" readonly=\"true\">\n" +
                                                            "                    </div>\n" +
                                                            "                </div>\n" +
                                                            "                <div class=\"layui-col-md3\">\n" +
                                                            "                    <!-- 填充内容 -->\n" +
                                                            "                    <label class=\"layui-form-label\">单位</label>\n" +
                                                            "                    <div class=\"layui-input-inline\">\n" +
                                                            "                        <input type=\"text\" name=\"unit\" autocomplete=\"off\" class=\"layui-input\" readonly=\"true\">\n" +
                                                            "                    </div>\n" +
                                                            "                </div>\n" +
                                                            "\n" +
                                                            "            </div>\n" +
                                                            "            <div class=\"layui-row layui-col-space10\">\n" +
                                                            "                <div class=\"layui-col-md3\">\n" +
                                                            "                    <!-- 填充内容 -->\n" +
                                                            "                    <label class=\"layui-form-label\">缸号</label>\n" +
                                                            "                    <div class=\"layui-input-inline\">\n" +
                                                            "                        <input type=\"text\" name=\"jarName\" autocomplete=\"off\" class=\"layui-input\" readonly=\"true\">\n" +
                                                            "                    </div>\n" +
                                                            "                </div>\n" +
                                                            "                <div class=\"layui-col-md3\">\n" +
                                                            "                    <!-- 填充内容 -->\n" +
                                                            "                    <label class=\"layui-form-label\">卷数</label>\n" +
                                                            "                    <div class=\"layui-input-inline\">\n" +
                                                            "                        <input type=\"text\" name=\"batchNumber\" autocomplete=\"off\" class=\"layui-input\">\n" +
                                                            "                    </div>\n" +
                                                            "                </div>\n" +
                                                            "                <div class=\"layui-col-md3\">\n" +
                                                            "                    <!-- 填充内容 -->\n" +
                                                            "                    <label class=\"layui-form-label\">重量</label>\n" +
                                                            "                    <div class=\"layui-input-inline\">\n" +
                                                            "                        <input type=\"text\" name=\"weight\" autocomplete=\"off\" class=\"layui-input\">\n" +
                                                            "                    </div>\n" +
                                                            "                </div>\n" +
                                                            "                <div class=\"layui-col-md3\">\n" +
                                                            "                    <!-- 填充内容 -->\n" +
                                                            "                    <label class=\"layui-form-label\">入库时间</label>\n" +
                                                            "                    <div class=\"layui-input-inline\">\n" +
                                                            "                        <input type=\"text\" name=\"returnTime\" id=\"updateStorageTime\" autocomplete=\"off\" class=\"layui-input\" readonly=\"true\">\n" +
                                                            "                    </div>\n" +
                                                            "                </div>\n" +
                                                            "\n" +
                                                            "            </div>\n" +
                                                            "            <div class=\"layui-row layui-col-space10\">\n" +
                                                            "                <div class=\"layui-col-md3\">\n" +
                                                            "                    <!-- 填充内容 -->\n" +
                                                            "                    <label class=\"layui-form-label\">位置</label>\n" +
                                                            "                    <div class=\"layui-input-inline\">\n" +
                                                            "                        <input type=\"text\" name=\"location\" autocomplete=\"off\" class=\"layui-input\" readonly=\"true\">\n" +
                                                            "                    </div>\n" +
                                                            "                </div>\n" +
                                                            "                <div class=\"layui-col-md3\"></div>\n" +
                                                            "                <div class=\"layui-col-md3\"></div>\n" +
                                                            "                <div class=\"layui-col-md3\"></div>\n" +
                                                            "            </div>\n" +
                                                            "            <div class=\"layui-row layui-col-space10\" style=\"padding-top: 30px\">\n" +
                                                            "                <h3 style=\"text-align: center\">调换到:</h3><br>\n" +
                                                            "            </div>\n" +
                                                            "            <div class=\"layui-row layui-col-space10\">\n" +
                                                            "                <div class=\"layui-col-md3\">\n" +
                                                            "                    <!-- 填充内容 -->\n" +
                                                            "                    <label class=\"layui-form-label\">版单</label>\n" +
                                                            "                    <div class=\"layui-input-inline\">\n" +
                                                            "                        <input type=\"text\" name=\"toClothesVersionNumber\" id=\"toClothesVersionNumber\" autocomplete=\"off\" class=\"layui-input\">\n" +
                                                            "                    </div>\n" +
                                                            "                </div>\n" +
                                                            "                <div class=\"layui-col-md3\">\n" +
                                                            "                    <!-- 填充内容 -->\n" +
                                                            "                    <label class=\"layui-form-label\">订单</label>\n" +
                                                            "                    <div class=\"layui-input-inline\">\n" +
                                                            "                        <select type=\"text\" name=\"toOrderName\" id=\"toOrderName\" autocomplete=\"off\" class=\"layui-input\" lay-filter=\"test\"></select>\n" +
                                                            "                    </div>\n" +
                                                            "                </div>\n" +
                                                            "                <div class=\"layui-col-md3\">\n" +
                                                            "                    <!-- 填充内容 -->\n" +
                                                            "                    <label class=\"layui-form-label\">色组</label>\n" +
                                                            "                    <div class=\"layui-input-inline\">\n" +
                                                            "                        <select type=\"text\" name=\"toColorName\" id=\"toColorName\" autocomplete=\"off\" class=\"layui-input\"></select>\n" +
                                                            "                    </div>\n" +
                                                            "                </div>\n" +
                                                            "            </div>\n" +
                                                            "        </form>\n" +
                                                            "    </div>"
                                                        ,yes: function(index, layero){
                                                            var params = layui.form.val("returnChangeOrder");
                                                            params.fabricDetailID = rowData.fabricDetailID;
                                                            params.fabricID = rowData.fabricID;
                                                            params.orderName = rowData.orderName;
                                                            params.clothesVersionNumber = rowData.clothesVersionNumber;
                                                            var initWeight = rowData.weight;
                                                            var initBatch = rowData.batchNumber;
                                                            if (Number(params.batchNumber) > Number(initBatch) || Number(params.weight) > Number(initWeight)){
                                                                layer.msg("换款数量超数", {icon: 2});
                                                            }
                                                            $.ajax({
                                                                url: "/erp/fabricchangeorder",
                                                                type: 'POST',
                                                                data: {
                                                                    fabricDetailJson: JSON.stringify(params),
                                                                    toClothesVersionNumber: params.toClothesVersionNumber,
                                                                    toOrderName: params.toOrderName,
                                                                    toColorName: params.toColorName,
                                                                    initWeight: initWeight,
                                                                    initBatch: initBatch
                                                                },
                                                                success: function (data) {
                                                                    if (data.error) {
                                                                        layer.msg(data.error, {icon: 2});
                                                                    } else if(data.trans){
                                                                        layer.close(index);
                                                                        layer.msg(data.trans, {icon: 1});
                                                                        reloadTable();
                                                                        $("#fabricReturnDataChangeOrder").find("input").val("");
                                                                    }else if(data.out){
                                                                        layer.close(index);
                                                                        layer.msg(data.out, {icon: 1});
                                                                        reloadTable();
                                                                        $("#fabricReturnDataChangeOrder").find("input").val("");
                                                                    }else {
                                                                        layer.msg(data.error, {icon: 2});
                                                                    }
                                                                }, error: function () {
                                                                    layer.msg("调款失败！", {icon: 2});
                                                                }
                                                            })
                                                        }
                                                        , cancel : function (i,layero) {
                                                            $("#fabricReturnDataChangeOrder").find("input").val("");
                                                        }
                                                    });
                                                    layer.full(index);
                                                    $("#fabricReturnDataChangeOrder input[name='fabricName']").val(rowData.fabricName);
                                                    $("#fabricReturnDataChangeOrder input[name='fabricNumber']").val(rowData.fabricNumber);
                                                    $("#fabricReturnDataChangeOrder input[name='colorName']").val(rowData.colorName);
                                                    $("#fabricReturnDataChangeOrder input[name='isHit']").val(rowData.isHit);
                                                    $("#fabricReturnDataChangeOrder input[name='fabricColor']").val(rowData.fabricColor);
                                                    $("#fabricReturnDataChangeOrder input[name='fabricColorNumber']").val(rowData.fabricColorNumber);
                                                    $("#fabricReturnDataChangeOrder input[name='jarName']").val(rowData.jarName);
                                                    $("#fabricReturnDataChangeOrder input[name='unit']").val(rowData.unit);
                                                    $("#fabricReturnDataChangeOrder input[name='weight']").val(rowData.weight);
                                                    $("#fabricReturnDataChangeOrder input[name='batchNumber']").val(rowData.batchNumber);
                                                    $("#fabricReturnDataChangeOrder input[name='location']").val(rowData.location);
                                                    $("#fabricReturnDataChangeOrder input[name='returnTime']").val(moment(rowData.returnTime).format("YYYY-MM-DD"));
                                                    $("#fabricReturnDataChangeOrder input[name='supplier']").val(rowData.supplier);
                                                    form.render('select');
                                                }
                                            }
                                            ,done: function () {
                                                soulTable.render(this);
                                            }
                                            ,filter: {
                                                bottom: false,
                                                items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                                            }
                                        },
                                        {
                                            title: '库存记录'
                                            ,url: '/erp/getfabricstoragebyinfo'
                                            ,where: function(row){
                                                return {
                                                    fabricID: row.fabricID
                                                }
                                            }
                                            ,loading:true
                                            ,page: false
                                            ,totalRow: true
                                            ,toolbar: '<div><a class="layui-btn layui-btn-normal layui-btn-sm" lay-event="batchDelete">批量出库</a></div>'
                                            ,cols: [[
                                                {type: 'checkbox', fixed: 'left'},
                                                {field: 'fabricName', title: '面料名称',align:'center', minWidth: 200, totalRowText: '合计'},
                                                {field: 'fabricNumber', title: '面料号',align:'center', minWidth: 100 },
                                                {field: 'supplier', title: '供应商',align:'center', minWidth: 100 },
                                                {field: 'colorName', title: '订单颜色',align:'center', minWidth: 100 },
                                                {field: 'fabricColor', title: '面料颜色',align:'center', minWidth: 100},
                                                {field: 'isHit', title: '是否撞色',align:'center', minWidth: 100},
                                                {field: 'fabricColorNumber', title: '面料色号',align:'center', minWidth: 100},
                                                {field: 'jarName', title: '缸号',align:'center', minWidth: 100},
                                                {field: 'unit', title: '单位',align:'center', minWidth: 80},
                                                {field: 'batchNumber', title: '卷数',align:'center', minWidth: 100, totalRow: true},
                                                {field: 'weight', title: '数量',align:'center', minWidth: 100, totalRow: true},
                                                {field: 'location', title: '位置',align:'center', minWidth: 100},
                                                {field: 'returnTime', title: '入库时间',align:'center', minWidth: 120,templet:function (d) {
                                                        return moment(d.returnTime).format("YYYY-MM-DD");
                                                    }},
                                                {field: 'operateType', title: '入库类型',align:'center', minWidth: 100},
                                                {field: 'fabricStorageID', align:'center', hide:true},
                                                {field: 'orderName', align:'center', hide:true},
                                                {field: 'clothesVersionNumber', align:'center', hide:true},
                                                {field: 'fabricID',align:'center', hide: true},
                                                {title: '操作', minWidth: 250,align:'center', templet: function(row) {
                                                        return '<a class="layui-btn layui-btn-xs" lay-event="fabricStorageNormalOutStore">出库</a><a class="layui-btn layui-btn-xs layui-btn-warm" lay-event="fabricStorageUnNormalOutStore">异常出库</a><a class="layui-btn layui-btn-xs" lay-event="changeFabricStorage">调库</a><a class="layui-btn layui-btn-normal layui-btn-xs" lay-event="changeFabricOrder">换款</a>';
                                                    }}
                                            ]]
                                            ,toolEvent: function (obj, pobj) {
                                                // obj 子表当前行对象
                                                // pobj 父表当前行对象
                                                var rowData = obj.data;
                                                var childId = this.id; // 通过 this 对象获取当前子表的id
                                                if (obj.event === 'fabricStorageNormalOutStore'){
                                                    var param = {};
                                                    param.fabricName = rowData.fabricName;
                                                    param.fabricNumber = rowData.fabricNumber;
                                                    param.supplier = rowData.supplier;
                                                    param.colorName = rowData.colorName;
                                                    param.fabricColor = rowData.fabricColor;
                                                    param.isHit = rowData.isHit;
                                                    param.fabricColorNumber = rowData.fabricColorNumber;
                                                    param.jarName = rowData.jarName;
                                                    param.unit = rowData.unit;
                                                    param.batchNumber = rowData.batchNumber;
                                                    param.weight = rowData.weight;
                                                    param.location = rowData.location;
                                                    param.returnTime = moment(rowData.returnTime).format("YYYY-MM-DD");
                                                    param.fabricStorageID = rowData.fabricStorageID;
                                                    param.orderName = rowData.orderName;
                                                    param.clothesVersionNumber = rowData.clothesVersionNumber;
                                                    var initWeight = rowData.weight;
                                                    var initBatch = rowData.batchNumber;
                                                    param.fabricID = rowData.fabricID;
                                                    param.fabricOutRecordID = param.fabricStorageID;
                                                    param.operateType = "出库";
                                                    var index = layer.open({
                                                        type: 1 //Page层类型
                                                        , title: '面料出库'
                                                        , btn: ['保存']
                                                        , shade: 0.6 //遮罩透明度
                                                        , maxmin: false //允许全屏最小化
                                                        , anim: 0 //0-6的动画形式，-1不开启
                                                        , area: ['300px', '240px']
                                                        , content: "<div><table><tr><td><label class='layui-form-label'>卷数</label></td><td><input type='text' id='normalBatchNumber' autocomplete='off' class='layui-input'></td></tr><tr><td><label class='layui-form-label'>数量</label></td><td><input type='text' id='normalWeight' autocomplete='off' class='layui-input'></td></tr><tr><td><label class='layui-form-label'>接收</label></td><td><input type='text' id='receiver1' autocomplete='off' class='layui-input'></td></tr></table></div>"
                                                        ,yes: function(index, layero){
                                                            var normalBatchNumber = $("#normalBatchNumber").val();
                                                            var normalWeight = $("#normalWeight").val();
                                                            var receiver = $("#receiver1").val();
                                                            param.batchNumber = normalBatchNumber;
                                                            param.weight = normalWeight;
                                                            param.receiver = receiver;
                                                            if (Number(normalBatchNumber) > Number(initBatch) || Number(normalWeight) > Number(initWeight)){
                                                                layer.msg("出库数量超过库存", {icon: 2});
                                                                return false;
                                                            }
                                                            if (receiver == null || receiver == ''){
                                                                layer.msg("接收不能为空", {icon: 2});
                                                                return false;
                                                            }
                                                            $.ajax({
                                                                url: "/erp/addunnormalfabricoutrecord",
                                                                type: 'POST',
                                                                data: {
                                                                    fabricOutRecordJson: JSON.stringify(param),
                                                                    initWeight: initWeight,
                                                                    initBatch: initBatch
                                                                },
                                                                success: function (res) {
                                                                    if (res.code == 0) {
                                                                        layer.close(index);
                                                                        layer.msg("出库成功！", {icon: 1});
                                                                        reloadTable();
                                                                        $("#normalBatchNumber").val("");
                                                                        $("#normalWeight").val("");
                                                                        $("#receiver").val("");
                                                                    } else {
                                                                        layer.msg("出库失败！", {icon: 2});
                                                                    }
                                                                },
                                                                error: function () {
                                                                    layer.msg("保存失败！", {icon: 2});
                                                                }
                                                            })
                                                        }
                                                        , cancel : function (i,layero) {
                                                            $("#normalBatchNumber").val('');
                                                            $("#normalWeight").val('');
                                                            $("#receiver").val("");
                                                        }
                                                    });
                                                    $("#normalBatchNumber").val(rowData.batchNumber);
                                                    $("#normalWeight").val(rowData.weight);
                                                } else if (obj.event === 'fabricStorageUnNormalOutStore'){
                                                    var index = layer.open({
                                                        type: 1 //Page层类型
                                                        , title: '面料异常出库'
                                                        , btn: ['保存']
                                                        , shade: 0.6 //遮罩透明度
                                                        , maxmin: false //允许全屏最小化
                                                        , anim: 0 //0-6的动画形式，-1不开启
                                                        , content: "<div id=\"fabricUnNormalOutStore\" style=\"cursor:default;\" xmlns=\"http://www.w3.org/1999/html\">\n" +
                                                            "        <form class=\"layui-form\" style=\"margin: 5px;padding-top: 40px;\" lay-filter=\"unNormalOutStore\">\n" +
                                                            "            <div class=\"layui-row layui-col-space10\">\n" +
                                                            "                <div class=\"layui-col-md3\">\n" +
                                                            "                    <!-- 填充内容 -->\n" +
                                                            "                    <label class=\"layui-form-label\">面料名称</label>\n" +
                                                            "                    <div class=\"layui-input-inline\">\n" +
                                                            "                        <input type=\"text\" name=\"fabricName\" autocomplete=\"off\" class=\"layui-input\" readonly=\"true\">\n" +
                                                            "                    </div>\n" +
                                                            "                </div>\n" +
                                                            "                <div class=\"layui-col-md3\">\n" +
                                                            "                    <!-- 填充内容 -->\n" +
                                                            "                    <label class=\"layui-form-label\">面料号</label>\n" +
                                                            "                    <div class=\"layui-input-inline\">\n" +
                                                            "                        <input type=\"text\" name=\"fabricNumber\" autocomplete=\"off\" class=\"layui-input\" readonly=\"true\">\n" +
                                                            "                    </div>\n" +
                                                            "                </div>\n" +
                                                            "                <div class=\"layui-col-md3\">\n" +
                                                            "                    <!-- 填充内容 -->\n" +
                                                            "                    <label class=\"layui-form-label\">色组</label>\n" +
                                                            "                    <div class=\"layui-input-inline\">\n" +
                                                            "                        <input type=\"text\" name=\"colorName\" autocomplete=\"off\" class=\"layui-input\" readonly=\"true\">\n" +
                                                            "                    </div>\n" +
                                                            "                </div>\n" +
                                                            "                <div class=\"layui-col-md3\">\n" +
                                                            "                    <!-- 填充内容 -->\n" +
                                                            "                    <label class=\"layui-form-label\">是否撞色</label>\n" +
                                                            "                    <div class=\"layui-input-inline\">\n" +
                                                            "                        <input type=\"text\" name=\"isHit\" autocomplete=\"off\" class=\"layui-input\" readonly=\"true\">\n" +
                                                            "                    </div>\n" +
                                                            "                </div>\n" +
                                                            "            </div>\n" +
                                                            "            <div class=\"layui-row layui-col-space10\">\n" +
                                                            "                <div class=\"layui-col-md3\">\n" +
                                                            "                    <!-- 填充内容 -->\n" +
                                                            "                    <label class=\"layui-form-label\">面料颜色</label>\n" +
                                                            "                    <div class=\"layui-input-inline\">\n" +
                                                            "                        <input type=\"text\" name=\"fabricColor\" autocomplete=\"off\" class=\"layui-input\" readonly=\"true\">\n" +
                                                            "                    </div>\n" +
                                                            "                </div>\n" +
                                                            "                <div class=\"layui-col-md3\">\n" +
                                                            "                    <!-- 填充内容 -->\n" +
                                                            "                    <label class=\"layui-form-label\">面料色号</label>\n" +
                                                            "                    <div class=\"layui-input-inline\">\n" +
                                                            "                        <input type=\"text\" name=\"fabricColorNumber\" autocomplete=\"off\" class=\"layui-input\" readonly=\"true\">\n" +
                                                            "                    </div>\n" +
                                                            "                </div>\n" +
                                                            "                <div class=\"layui-col-md3\">\n" +
                                                            "                    <!-- 填充内容 -->\n" +
                                                            "                    <label class=\"layui-form-label\">供应商</label>\n" +
                                                            "                    <div class=\"layui-input-inline\">\n" +
                                                            "                        <input type=\"text\" name=\"supplier\" autocomplete=\"off\" class=\"layui-input\" readonly=\"true\">\n" +
                                                            "                    </div>\n" +
                                                            "                </div>\n" +
                                                            "                <div class=\"layui-col-md3\">\n" +
                                                            "                    <!-- 填充内容 -->\n" +
                                                            "                    <label class=\"layui-form-label\">单位</label>\n" +
                                                            "                    <div class=\"layui-input-inline\">\n" +
                                                            "                        <input type=\"text\" name=\"unit\" autocomplete=\"off\" class=\"layui-input\" readonly=\"true\">\n" +
                                                            "                    </div>\n" +
                                                            "                </div>\n" +
                                                            "\n" +
                                                            "            </div>\n" +
                                                            "            <div class=\"layui-row layui-col-space10\">\n" +
                                                            "                <div class=\"layui-col-md3\">\n" +
                                                            "                    <!-- 填充内容 -->\n" +
                                                            "                    <label class=\"layui-form-label\">缸号</label>\n" +
                                                            "                    <div class=\"layui-input-inline\">\n" +
                                                            "                        <input type=\"text\" name=\"jarName\" autocomplete=\"off\" class=\"layui-input\" readonly=\"true\">\n" +
                                                            "                    </div>\n" +
                                                            "                </div>\n" +
                                                            "                <div class=\"layui-col-md3\">\n" +
                                                            "                    <!-- 填充内容 -->\n" +
                                                            "                    <label class=\"layui-form-label\">卷数</label>\n" +
                                                            "                    <div class=\"layui-input-inline\">\n" +
                                                            "                        <input type=\"text\" name=\"batchNumber\" autocomplete=\"off\" class=\"layui-input\">\n" +
                                                            "                    </div>\n" +
                                                            "                </div>\n" +
                                                            "                <div class=\"layui-col-md3\">\n" +
                                                            "                    <!-- 填充内容 -->\n" +
                                                            "                    <label class=\"layui-form-label\">重量</label>\n" +
                                                            "                    <div class=\"layui-input-inline\">\n" +
                                                            "                        <input type=\"text\" name=\"weight\" autocomplete=\"off\" class=\"layui-input\">\n" +
                                                            "                    </div>\n" +
                                                            "                </div>\n" +
                                                            "                <div class=\"layui-col-md3\">\n" +
                                                            "                    <!-- 填充内容 -->\n" +
                                                            "                    <label class=\"layui-form-label\">入库时间</label>\n" +
                                                            "                    <div class=\"layui-input-inline\">\n" +
                                                            "                        <input type=\"text\" name=\"returnTime\" id=\"updateStorageTime\" autocomplete=\"off\" class=\"layui-input\" readonly=\"true\">\n" +
                                                            "                    </div>\n" +
                                                            "                </div>\n" +
                                                            "\n" +
                                                            "            </div>\n" +
                                                            "            <div class=\"layui-row layui-col-space10\">\n" +
                                                            "                <div class=\"layui-col-md3\">\n" +
                                                            "                    <!-- 填充内容 -->\n" +
                                                            "                    <label class=\"layui-form-label\">位置</label>\n" +
                                                            "                    <div class=\"layui-input-inline\">\n" +
                                                            "                        <input type=\"text\" name=\"location\" autocomplete=\"off\" class=\"layui-input\" readonly=\"true\">\n" +
                                                            "                    </div>\n" +
                                                            "                </div>\n" +
                                                            "                <div class=\"layui-col-md3\">\n" +
                                                            "                    <!-- 填充内容 -->\n" +
                                                            "                    <label class=\"layui-form-label\">类型</label>\n" +
                                                            "                    <div class=\"layui-input-inline\">\n" +
                                                            "                        <select type=\"text\" name=\"operateType\" autocomplete=\"off\" class=\"layui-input\">\n" +
                                                            "                            <option value=\"\">出库类型</option>\n" +
                                                            "                            <option value=\"外发加工\">外发加工</option>\n" +
                                                            "                            <option value=\"退面料厂\">退面料厂</option>\n" +
                                                            "                            <option value=\"余布出售\">余布出售</option>\n" +
                                                            "                            <option value=\"面料异常\">面料异常</option>\n" +
                                                            "                        </select>\n" +
                                                            "                    </div>\n" +
                                                            "                </div>\n" +
                                                            "                <div class=\"layui-col-md3\">\n" +
                                                            "                    <!-- 填充内容 -->\n" +
                                                            "                    <label class=\"layui-form-label\">接收</label>\n" +
                                                            "                    <div class=\"layui-input-inline\">\n" +
                                                            "                        <input type=\"text\" name=\"receiver\" autocomplete=\"off\" class=\"layui-input\">\n" +
                                                            "                    </div>\n" +
                                                            "                </div>\n" +
                                                            "                <div class=\"layui-col-md3\"></div>\n" +
                                                            "            </div>\n" +
                                                            "        </form>\n" +
                                                            "    </div>"
                                                        ,yes: function(index, layero){
                                                            var params = layui.form.val("unNormalOutStore");
                                                            params.fabricStorageID = rowData.fabricStorageID;
                                                            params.orderName = rowData.orderName;
                                                            params.fabricID = rowData.fabricID;
                                                            params.clothesVersionNumber = rowData.clothesVersionNumber;
                                                            var initWeight = rowData.weight;
                                                            var initBatch = rowData.batchNumber;
                                                            params.fabricOutRecordID = params.fabricStorageID;
                                                            if (Number(params.batchNumber) > Number(initBatch) || Number(params.weight) > Number(initWeight)){
                                                                layer.msg("出库数量超过库存", {icon: 2});
                                                            }
                                                            if (params.operateType == null || params.operateType == ""){
                                                                layer.msg("请选择出库类型", {icon: 2});
                                                                return false;
                                                            }
                                                            if (params.receiver == null || params.receiver == ""){
                                                                layer.msg("请输入接收", {icon: 2});
                                                            }
                                                            $.ajax({
                                                                url: "/erp/addunnormalfabricoutrecord",
                                                                type: 'POST',
                                                                data: {
                                                                    fabricOutRecordJson: JSON.stringify(params),
                                                                    initWeight: initWeight,
                                                                    initBatch: initBatch
                                                                },
                                                                success: function (res) {
                                                                    if (res.code == 0) {
                                                                        layer.close(index);
                                                                        layer.msg("出库成功！", {icon: 1});
                                                                        reloadTable();
                                                                        $("#fabricUnNormalOutStore").find("input").val("");
                                                                    } else {
                                                                        layer.msg("出库失败！", {icon: 2});
                                                                    }
                                                                },
                                                                error: function () {
                                                                    layer.msg("保存失败！", {icon: 2});
                                                                }
                                                            })
                                                        }
                                                        , cancel : function (i,layero) {
                                                            $("#fabricUnNormalOutStore").find("input").val("");
                                                        }
                                                    });
                                                    layer.full(index);
                                                    $("#fabricUnNormalOutStore input[name='fabricName']").val(rowData.fabricName);
                                                    $("#fabricUnNormalOutStore input[name='fabricNumber']").val(rowData.fabricNumber);
                                                    $("#fabricUnNormalOutStore input[name='supplier']").val(rowData.supplier);
                                                    $("#fabricUnNormalOutStore input[name='colorName']").val(rowData.colorName);
                                                    $("#fabricUnNormalOutStore input[name='fabricColor']").val(rowData.fabricColor);
                                                    $("#fabricUnNormalOutStore input[name='isHit']").val(rowData.isHit);
                                                    $("#fabricUnNormalOutStore input[name='fabricColorNumber']").val(rowData.fabricColorNumber);
                                                    $("#fabricUnNormalOutStore input[name='jarName']").val(rowData.jarName);
                                                    $("#fabricUnNormalOutStore input[name='unit']").val(rowData.unit);
                                                    $("#fabricUnNormalOutStore input[name='batchNumber']").val(rowData.batchNumber);
                                                    $("#fabricUnNormalOutStore input[name='weight']").val(rowData.weight);
                                                    $("#fabricUnNormalOutStore input[name='location']").val(rowData.location);
                                                    $("#fabricUnNormalOutStore input[name='returnTime']").val(moment(rowData.returnTime).format("YYYY-MM-DD"));
                                                    form.render('select');
                                                } else if (obj.event === 'changeFabricStorage'){
                                                    var index = layer.open({
                                                        type: 1 //Page层类型
                                                        , title: '面料调库'
                                                        , btn: ['保存']
                                                        , shade: 0.6 //遮罩透明度
                                                        ,area: ['320px', '180px'] //宽高
                                                        , anim: 0 //0-6的动画形式，-1不开启
                                                        , content: '<div class="layui-col-md3" style="width: 300px; padding-top: 30px">\n' +
                                                            '                    <!-- 填充内容 -->\n' +
                                                            '                    <label class="layui-form-label">调换到</label>\n' +
                                                            '                    <div class="layui-input-inline">\n' +
                                                            '                        <input id="destinationLocation" style="width: 200px;" class="layui-input">' +
                                                            '                    </div>\n' +
                                                            '                </div>'
                                                        ,success: function(layero, index){
                                                            $("#destinationLocation").val('');
                                                        }
                                                        ,yes: function(index, layero){
                                                            var destLocation = $("#destinationLocation").val();
                                                            $.ajax({
                                                                url: "/erp/updatefabricstorage",
                                                                type: 'POST',
                                                                data: {
                                                                    fabricStorageID: rowData.fabricStorageID,
                                                                    location: destLocation
                                                                },
                                                                success: function (res) {
                                                                    if (res == 0) {
                                                                        layer.close(index);
                                                                        layer.msg("保存成功！", {icon: 1});
                                                                        reloadTable();
                                                                    } else {
                                                                        layer.msg("保存失败！", {icon: 2});
                                                                    }
                                                                },
                                                                error: function () {
                                                                    layer.msg("保存失败！", {icon: 2});
                                                                }
                                                            })
                                                        }, cancel : function (i,layero) {

                                                        }
                                                    });
                                                    form.render('select');
                                                } else if (obj.event === 'changeFabricOrder'){
                                                    var index = layer.open({
                                                        type: 1 //Page层类型
                                                        , title: '面料换款'
                                                        , btn: ['保存']
                                                        , shade: 0.6 //遮罩透明度
                                                        , maxmin: false //允许全屏最小化
                                                        , anim: 0 //0-6的动画形式，-1不开启
                                                        , content: $("#operateDiv")
                                                        ,yes: function(index, layero){
                                                            var checkStatus = table.checkStatus('fabricChangeTable'); //获取选中行状态
                                                            if(checkStatus.data.length == 0) {
                                                                layer.msg('请先选择订单');
                                                            }else {
                                                                var changeBatch = $("#changeBatch").val();
                                                                var changeWeight = $("#changeWeight").val();
                                                                if (Number(changeWeight) > rowData.weight || Number(changeBatch) > rowData.batchNumber){
                                                                    layer.msg('数量有误~');
                                                                    return false;
                                                                }
                                                                var toData = checkStatus.data[0];
                                                                console.log(toData);
                                                                console.log(toData.fabricName);
                                                                console.log(rowData.fabricName);
                                                                console.log(toData.fabricColor);
                                                                console.log(rowData.fabricColor);
                                                                if (toData.fabricName != rowData.fabricName || toData.fabricColor != rowData.fabricColor){
                                                                    layer.msg('选择的面料名称和颜色不对应,请确认~~~');
                                                                    return false;
                                                                }
                                                                $.ajax({
                                                                    url: "/erp/changefabricorder",
                                                                    type: 'POST',
                                                                    data: {
                                                                        fabricStorageID: rowData.fabricStorageID,
                                                                        initID: rowData.fabricID,
                                                                        fabricID: toData.fabricID,
                                                                        changeWeight:changeWeight,
                                                                        changeBatch:changeBatch
                                                                    },
                                                                    success: function (res) {
                                                                        if (res.over === 1){
                                                                            var tipHtml = "本次录入超过了允许接收的上限:<br><div style='font-weight: bolder;font-size: larger;color: orange;margin-bottom: 10px'>订布量:" + toDecimal(res.fabricActCount) + "<br>已入库:" + toDecimal(res.historyWeight) + "<br>允许超数:" + toDecimal(res.overWeight) +  "<br>最多还可入:" + toDecimal(res.availableWeight) + "<br>";
                                                                            layer.confirm(tipHtml, {
                                                                                title: '超数提醒'
                                                                                ,btn: ['返回修改'] //按钮
                                                                            }, function(){

                                                                            });
                                                                        } else {
                                                                            if (res.result == 0) {
                                                                                layer.close(index);
                                                                                layer.msg("保存成功！", {icon: 1});
                                                                                reloadTable();
                                                                            } else if (res.result == 3) {
                                                                                layer.msg("该面料未审核,请联系审核人员审核~~");
                                                                            } else if (res.result == 4) {
                                                                                layer.msg("面料库存异常,请联系IT~~");
                                                                            } else {
                                                                                layer.msg("提交失败！", {icon: 2});
                                                                            }
                                                                        }
                                                                    },
                                                                    error: function () {
                                                                        layer.msg("保存失败！", {icon: 2});
                                                                    }
                                                                })
                                                            }
                                                        }
                                                        , cancel : function (i,layero) {
                                                            $("#operateDiv").find("input").val("");
                                                        }
                                                    });
                                                    layer.full(index);
                                                    setTimeout(function () {
                                                        $("#operateDiv input[name='clothesVersionNumber']").val(rowData.clothesVersionNumber);
                                                        $("#operateDiv input[name='orderName']").val(rowData.orderName);
                                                        $("#operateDiv input[name='fabricName']").val(rowData.fabricName);
                                                        $("#operateDiv input[name='fabricNumber']").val(rowData.fabricNumber);
                                                        $("#operateDiv input[name='supplier']").val(rowData.supplier);
                                                        $("#operateDiv input[name='colorName']").val(rowData.colorName);
                                                        $("#operateDiv input[name='fabricColor']").val(rowData.fabricColor);
                                                        $("#operateDiv input[name='fabricColorNumber']").val(rowData.fabricColorNumber);
                                                        $("#operateDiv input[name='jarName']").val(rowData.jarName);
                                                        $("#operateDiv input[name='unit']").val(rowData.unit);
                                                        $("#operateDiv input[name='batchNumber']").val(rowData.batchNumber);
                                                        $("#operateDiv input[name='changeBatch']").val(rowData.batchNumber);
                                                        $("#operateDiv input[name='weight']").val(rowData.weight);
                                                        $("#operateDiv input[name='changeWeight']").val(rowData.weight);
                                                        $("#operateDiv input[name='location']").val(rowData.location);
                                                        $("#operateDiv input[name='returnTime']").val(moment(rowData.returnTime).format("YYYY-MM-DD"));
                                                        form.on('submit(searchChangeBeat)', function(data){
                                                            var toOrderName = $("#orderNameTwo").val();
                                                            if (toOrderName == null || toOrderName == ''){
                                                                layer.msg("款号必须~~");
                                                                return false;
                                                            }
                                                            $.ajax({
                                                                url: "/erp/getmanufacturefabricbyorder",
                                                                type: 'GET',
                                                                data: {
                                                                    orderName: toOrderName
                                                                },
                                                                success: function (res) {
                                                                    if (res.data) {
                                                                        var reportData = res.data;
                                                                        table.render({
                                                                            elem: '#fabricChangeTable'
                                                                            ,cols:[[
                                                                                {type:'radio'},
                                                                                {field: 'orderName', title: '款号', minWidth: 130, sort: true, filter: true},
                                                                                {field: 'clothesVersionNumber', title: '单号', minWidth: 130, sort: true, filter: true},
                                                                                {field: 'fabricName', title: '面料名称', minWidth: 130, sort: true, filter: true},
                                                                                {field: 'fabricNumber', title: '面料号', minWidth: 130, sort: true, filter: true},
                                                                                {field: 'supplier', title: '供应商', minWidth: 100, sort: true, filter: true},
                                                                                {field: 'unit', title: '单位', minWidth: 100, sort: true, filter: true},
                                                                                {field: 'fabricWidth', title: '布封', minWidth: 165 , filter: true},
                                                                                {field: 'fabricWeight', title: '克重', minWidth: 123, filter: true},
                                                                                {field: 'fabricColor', title: '面料颜色', minWidth: 123, filter: true},
                                                                                {field: 'fabricColorNumber', title: '面料色号', minWidth: 123, filter: true},
                                                                                {field: 'fabricID', hide: true}
                                                                            ]]
                                                                            ,excel: {
                                                                                filename: '面料表.xlsx'
                                                                            }
                                                                            ,data: reportData
                                                                            ,height: '400'
                                                                            ,title: '辅料操作表'
                                                                            ,totalRow: true
                                                                            ,loading: false
                                                                            ,page: true
                                                                            ,even: true
                                                                            ,overflow: 'tips'
                                                                            ,limits: [50, 100, 200]
                                                                            ,limit: 100 //每页默认显示的数量
                                                                            ,done: function () {
                                                                                soulTable.render(this);
                                                                            }
                                                                        });
                                                                    } else {
                                                                        layer.msg("获取失败！", {icon: 2});
                                                                    }
                                                                }, error: function () {
                                                                    layer.msg("获取失败！", {icon: 2});
                                                                }
                                                            });
                                                            return false;
                                                        });
                                                    });
                                                }
                                            }
                                            ,toolbarEvent: function (obj, pobj) {
                                                // obj 子表当前行对象
                                                // pobj 父表当前行对象
                                                var objData = obj.data;
                                                var childId = this.id;
                                                var fabricID;
                                                if (obj.event === 'batchDelete') {
                                                    var checkStatus = table.checkStatus(obj.config.id);
                                                    if(checkStatus.data.length == 0) {
                                                        layer.msg('请选择数据');
                                                    }else {
                                                        var fabricStorageIDList = [];
                                                        $.each(checkStatus.data, function (index, item) {
                                                            fabricStorageIDList.push(item.fabricStorageID);
                                                            fabricID = item.fabricID;
                                                        });
                                                        var index = layer.open({
                                                            type: 1 //Page层类型
                                                            , title: '面料出库'
                                                            , btn: ['保存']
                                                            , shade: 0.6 //遮罩透明度
                                                            , maxmin: false //允许全屏最小化
                                                            , anim: 0 //0-6的动画形式，-1不开启
                                                            , area: ['300px', '150px']
                                                            , content: "<div><table><tr><td><label class='layui-form-label'>接收:</label></td><td><input type='text' id='receiver2' autocomplete='off' class='layui-input'></td></tr></table></div>"
                                                            ,yes: function(index, layero){
                                                                var receiver = $("#receiver2").val();
                                                                $.ajax({
                                                                    url: "/erp/fabricbatchoutstore",
                                                                    type: 'POST',
                                                                    data: {
                                                                        fabricStorageIDList: fabricStorageIDList,
                                                                        receiver: receiver,
                                                                        fabricID: fabricID
                                                                    },
                                                                    traditional: true,
                                                                    success: function (res) {
                                                                        if (res.result == 0) {
                                                                            layer.close(index);
                                                                            layer.msg("出库成功！", {icon: 1});
                                                                            table.reload(childId);
                                                                            $("#receiver").val('');
                                                                        } else {
                                                                            layer.msg("出库失败！", {icon: 2});
                                                                        }
                                                                    }, error: function () {
                                                                        layer.msg("出库失败！", {icon: 2});
                                                                    }
                                                                })
                                                            }
                                                            , cancel : function (i,layero) {
                                                                $("#receiver").val('');
                                                            }
                                                        });
                                                    }
                                                }
                                            }
                                            ,done: function () {
                                                soulTable.render(this);
                                            }
                                            ,filter: {
                                                bottom: false,
                                                items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                                            }
                                        },
                                        {
                                            title: '出库记录'
                                            ,url: '/erp/getfabricoutrecordbyinfo'
                                            ,where: function(row){
                                                return {
                                                    fabricID: row.fabricID
                                                }
                                            }
                                            ,loading:true
                                            ,page: false
                                            ,totalRow: true
                                            ,cols: [[
                                                {field: 'fabricName', title: '面料名称',align:'center', minWidth: 200, totalRowText: '合计'},
                                                {field: 'colorName', title: '订单颜色',align:'center', minWidth: 100 },
                                                {field: 'fabricColor', title: '面料颜色',align:'center', minWidth: 100},
                                                {field: 'jarName', title: '缸号',align:'center', minWidth: 100},
                                                {field: 'unit', title: '单位',align:'center', minWidth: 80},
                                                {field: 'batchNumber', title: '卷数',align:'center', minWidth: 100, totalRow: true},
                                                {field: 'weight', title: '数量',align:'center', minWidth: 100, totalRow: true},
                                                {field: 'location', title: '位置',align:'center', minWidth: 100},
                                                {field: 'createTime', title: '出库时间',align:'center', minWidth: 120,templet:function (d) {
                                                        return moment(d.createTime).format("YYYY-MM-DD");
                                                    }},
                                                {field: 'operateType', title: '出库类型',align:'center', minWidth: 150},
                                                {field: 'receiver', title: '接收',align:'center', minWidth: 100},
                                                {field: 'fabricID',align:'center', hide: true},
                                                {field: 'fabricOutRecordID', title: '操作', minWidth: 180,align:'center', templet: function(row) {
                                                        return '<a class="layui-btn layui-btn-xs layui-btn-danger" lay-event="deleteFabricOutRecord">删除</a><a class="layui-btn layui-btn-xs" lay-event="returnFabricReturnData">退库</a>';
                                                    }}
                                            ]]
                                            ,toolEvent: function (obj, pobj) {
                                                // obj 子表当前行对象
                                                // pobj 父表当前行对象
                                                var rowData = obj.data;
                                                var childId = this.id; // 通过 this 对象获取当前子表的id
                                                if (obj.event === 'deleteFabricOutRecord'){
                                                    layer.confirm('真的删除吗', function(index){
                                                        $.ajax({
                                                            url: "/erp/deletefabricoutrecord",
                                                            type: 'POST',
                                                            data: {
                                                                fabricOutRecordID: rowData.fabricOutRecordID
                                                            },
                                                            success: function (res) {
                                                                if (res == 0) {
                                                                    layer.close(index);
                                                                    layer.msg("删除成功！", {icon: 1});
                                                                    reloadTable();
                                                                } else {
                                                                    layer.msg("删除失败！", {icon: 2});
                                                                }
                                                            },
                                                            error: function () {
                                                                layer.msg("删除失败！", {icon: 2});
                                                            }
                                                        })
                                                    });
                                                }
                                                else if (obj.event === 'returnFabricReturnData'){
                                                    var index = layer.open({
                                                        type: 1 //Page层类型
                                                        , title: '余布退库'
                                                        , btn: ['保存']
                                                        , shade: 0.6 //遮罩透明度
                                                        , maxmin: false //允许全屏最小化
                                                        , anim: 0 //0-6的动画形式，-1不开启
                                                        , content: "<div id=\"fabricInStoreReturn\" style=\"cursor:default;\">\n" +
                                                            "        <form class=\"layui-form\" style=\"margin: 5px;padding-top: 40px;\" lay-filter=\"inStoreReturn\">\n" +
                                                            "            <div class=\"layui-row layui-col-space10\">\n" +
                                                            "                <div class=\"layui-col-md3\">\n" +
                                                            "                    <!-- 填充内容 -->\n" +
                                                            "                    <label class=\"layui-form-label\">面料名称</label>\n" +
                                                            "                    <div class=\"layui-input-inline\">\n" +
                                                            "                        <input type=\"text\" name=\"fabricName\" autocomplete=\"off\" class=\"layui-input\" readonly=\"true\">\n" +
                                                            "                    </div>\n" +
                                                            "                </div>\n" +
                                                            "                <div class=\"layui-col-md3\">\n" +
                                                            "                    <!-- 填充内容 -->\n" +
                                                            "                    <label class=\"layui-form-label\">色组</label>\n" +
                                                            "                    <div class=\"layui-input-inline\">\n" +
                                                            "                        <input type=\"text\" name=\"colorName\" autocomplete=\"off\" class=\"layui-input\" readonly=\"true\">\n" +
                                                            "                    </div>\n" +
                                                            "                </div>\n" +
                                                            "                <div class=\"layui-col-md3\">\n" +
                                                            "                    <!-- 填充内容 -->\n" +
                                                            "                    <label class=\"layui-form-label\">面料颜色</label>\n" +
                                                            "                    <div class=\"layui-input-inline\">\n" +
                                                            "                        <input type=\"text\" name=\"fabricColor\" autocomplete=\"off\" class=\"layui-input\" readonly=\"true\">\n" +
                                                            "                    </div>\n" +
                                                            "                </div>\n" +
                                                            "                <div class=\"layui-col-md3\">\n" +
                                                            "                    <!-- 填充内容 -->\n" +
                                                            "                    <label class=\"layui-form-label\">单位</label>\n" +
                                                            "                    <div class=\"layui-input-inline\">\n" +
                                                            "                        <input type=\"text\" name=\"unit\" autocomplete=\"off\" class=\"layui-input\" readonly=\"true\">\n" +
                                                            "                    </div>\n" +
                                                            "                </div>\n" +
                                                            "            </div>\n" +
                                                            "            <div class=\"layui-row layui-col-space10\">\n" +
                                                            "                <div class=\"layui-col-md3\">\n" +
                                                            "                    <!-- 填充内容 -->\n" +
                                                            "                    <label class=\"layui-form-label\">缸号</label>\n" +
                                                            "                    <div class=\"layui-input-inline\">\n" +
                                                            "                        <input type=\"text\" name=\"jarName\" autocomplete=\"off\" class=\"layui-input\">\n" +
                                                            "                    </div>\n" +
                                                            "                </div>\n" +
                                                            "                <div class=\"layui-col-md3\">\n" +
                                                            "                    <!-- 填充内容 -->\n" +
                                                            "                    <label class=\"layui-form-label\">卷数</label>\n" +
                                                            "                    <div class=\"layui-input-inline\">\n" +
                                                            "                        <input type=\"text\" name=\"batchNumber\" autocomplete=\"off\" class=\"layui-input\">\n" +
                                                            "                    </div>\n" +
                                                            "                </div>\n" +
                                                            "                <div class=\"layui-col-md3\">\n" +
                                                            "                    <!-- 填充内容 -->\n" +
                                                            "                    <label class=\"layui-form-label\">重量</label>\n" +
                                                            "                    <div class=\"layui-input-inline\">\n" +
                                                            "                        <input type=\"text\" name=\"weight\" autocomplete=\"off\" class=\"layui-input\">\n" +
                                                            "                    </div>\n" +
                                                            "                </div>\n" +
                                                            "                <div class=\"layui-col-md3\">\n" +
                                                            "                    <!-- 填充内容 -->\n" +
                                                            "                    <label class=\"layui-form-label\">收货</label>\n" +
                                                            "                    <div class=\"layui-input-inline\">\n" +
                                                            "                        <input type=\"text\" name=\"receiver\" id=\"receiver\" autocomplete=\"off\" class=\"layui-input\">\n" +
                                                            "                    </div>\n" +
                                                            "                </div>\n" +
                                                            "            </div>\n" +
                                                            "            <div class=\"layui-row layui-col-space10\">\n" +
                                                            "                <div class=\"layui-col-md3\">\n" +
                                                            "                    <!-- 填充内容 -->\n" +
                                                            "                    <label class=\"layui-form-label\">位置</label>\n" +
                                                            "                    <div class=\"layui-input-inline\">\n" +
                                                            "                        <input type=\"text\" name=\"newLocation\" autocomplete=\"off\" class=\"layui-input\">\n" +
                                                            "                    </div>\n" +
                                                            "                </div>\n" +
                                                            "            </div>\n" +
                                                            "        </form>\n" +
                                                            "    </div>"
                                                        ,yes: function(index, layero){
                                                            var params = layui.form.val("inStoreReturn");
                                                            params.operateType = '余布入仓';
                                                            params.location = rowData.location;
                                                            params.orderName = rowData.orderName;
                                                            params.fabricID = rowData.fabricID;
                                                            params.fabricOutRecordID = rowData.fabricOutRecordID;
                                                            params.clothesVersionNumber = rowData.clothesVersionNumber;
                                                            var newLocation = rowData.location
                                                            if (params.newLocation == null || params.newLocation === ''){
                                                                newLocation = params.newLocation
                                                            }
                                                            $.ajax({
                                                                url: "/erp/returnfabricreturn",
                                                                type: 'POST',
                                                                data: {
                                                                    fabricOutRecordJson: JSON.stringify(params),
                                                                    newLocation: newLocation
                                                                },
                                                                success: function (res) {
                                                                    if (res == 0) {
                                                                        layer.close(index);
                                                                        layer.msg("保存成功！", {icon: 1});
                                                                        reloadTable();
                                                                        $("#fabricInStoreReturn").find("input").val("");
                                                                    } else {
                                                                        layer.msg("保存失败！", {icon: 2});
                                                                    }
                                                                },
                                                                error: function () {
                                                                    layer.msg("保存失败！", {icon: 2});
                                                                }
                                                            })
                                                        }
                                                        , cancel : function (i,layero) {
                                                            $("#fabricInStoreReturn").find("input").val("");
                                                        }
                                                    });
                                                    layer.full(index);
                                                    $("#fabricInStoreReturn input[name='fabricName']").val(rowData.fabricName);
                                                    $("#fabricInStoreReturn input[name='colorName']").val(rowData.colorName);
                                                    $("#fabricInStoreReturn input[name='fabricColor']").val(rowData.fabricColor);
                                                    $("#fabricInStoreReturn input[name='jarName']").val(rowData.jarName);
                                                    $("#fabricInStoreReturn input[name='unit']").val(rowData.unit);
                                                    $("#fabricInStoreReturn input[name='weight']").val('');
                                                    $("#fabricInStoreReturn input[name='batchNumber']").val('');
                                                    $("#fabricInStoreReturn input[name='location']").val(rowData.location);
                                                    $("#fabricInStoreReturn input[name='newLocation']").val(rowData.location);
                                                    $("#fabricInStoreReturn input[name='receiver']").val("布仓");
                                                    form.render('select');
                                                }
                                            }
                                            ,done: function () {
                                                soulTable.render(this);
                                            }
                                            ,filter: {
                                                bottom: false,
                                                items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                                            }
                                        },
                                        {
                                            title: '暂存记录'
                                            ,url: '/erp/getfabricdetailtmpbyid'
                                            ,where: function(row){
                                                return {
                                                    fabricID: row.fabricID
                                                }
                                            }
                                            ,loading:true
                                            ,page: false
                                            ,totalRow: true
                                            ,cols: [[
                                                {field: 'fabricName', title: '面料名称',align:'center', minWidth: 200, totalRowText: '合计'},
                                                {field: 'fabricNumber', title: '面料号',align:'center', minWidth: 100 },
                                                {field: 'colorName', title: '订单颜色',align:'center', minWidth: 100 },
                                                {field: 'isHit', title: '是否撞色',align:'center', minWidth: 100 },
                                                {field: 'fabricColor', title: '面料颜色',align:'center', minWidth: 100},
                                                {field: 'fabricColorNumber', title: '面料色号',align:'center', minWidth: 100},
                                                {field: 'jarName', title: '缸号',align:'center', minWidth: 100},
                                                {field: 'unit', title: '单位',align:'center', minWidth: 80},
                                                {field: 'batchNumber', title: '卷数',align:'center', minWidth: 100, totalRow: true},
                                                {field: 'weight', title: '数量',align:'center', minWidth: 100, totalRow: true},
                                                {field: 'location', title: '位置',align:'center', minWidth: 100},
                                                {field: 'returnTime', title: '入库时间',align:'center', minWidth: 120,templet:function (d) {
                                                        return moment(d.returnTime).format("YYYY-MM-DD");
                                                    }},
                                                {field: 'supplier', title: '供应商',align:'center', minWidth: 100},
                                                {field: 'fabricDetailID', align:'center', hide:true},
                                                {field: 'orderName', align:'center', hide:true},
                                                {field: 'clothesVersionNumber', align:'center', hide:true},
                                                {field: 'fabricID', align:'center', hide:true},
                                                {title: '操作', minWidth: 250,align:'center', templet: function(row) {
                                                        return '<a class="layui-btn layui-btn-xs layui-btn-danger" lay-event="deleteFabricReturnData">删除</a>';
                                                    }}
                                            ]]
                                            ,toolEvent: function (obj, pobj) {
                                                // obj 子表当前行对象
                                                // pobj 父表当前行对象
                                                var rowData = obj.data;
                                                var childId = this.id; // 通过 this 对象获取当前子表的id
                                                if (obj.event === 'deleteFabricReturnData'){
                                                    layer.confirm('真的删除吗', function(index){
                                                        $.ajax({
                                                            url: "/erp/deletefabricdetailtmpbyid",
                                                            type: 'POST',
                                                            data: {
                                                                fabricDetailID: rowData.fabricDetailID
                                                            },
                                                            success: function (res) {
                                                                if (res.result == 0) {
                                                                    layer.close(index);
                                                                    layer.msg("删除成功！", {icon: 1});
                                                                    reloadTable();
                                                                } else {
                                                                    layer.msg("删除失败！", {icon: 2});
                                                                }
                                                            },
                                                            error: function () {
                                                                layer.msg("删除失败！", {icon: 2});
                                                            }
                                                        })
                                                    });
                                                }
                                            }
                                            ,done: function () {
                                                soulTable.render(this);
                                            }
                                            ,filter: {
                                                bottom: false,
                                                items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                                            }
                                        }]}
                                ,{type:'radio'}
                                ,{type:'numbers', align:'center', title:'序号'}
                                ,{field:'orderName', title:'款号', align:'center', width:150, sort: true, filter: true}
                                ,{field:'clothesVersionNumber', title:'版单号', align:'center', width:150, sort: true, filter: true, totalRowText: '合计'}
                                ,{field:'fabricName', title:'面料名称', align:'center', width:200, sort: true, filter: true}
                                ,{field:'fabricNumber', title:'面料号', align:'center', width:200, sort: true, filter: true}
                                ,{field:'fabricWidth', title:'门幅', align:'center', width:80, sort: true, filter: true}
                                ,{field:'fabricWeight', title:'克重', align:'center',width:80, sort: true, filter: true}
                                ,{field:'unit', title:'单位', align:'center', width:80, sort: true, filter: true}
                                ,{field:'partName', title:'部位', align:'center', width:90, sort: true, filter: true}
                                ,{field:'colorName', title:'色组', align:'center', width:90, sort: true, filter: true}
                                ,{field:'isHit', title:'是否撞色', align:'center', width:100, sort: true, filter: true}
                                ,{field:'fabricColor', title:'面料颜色', align:'center', width:100, sort: true, filter: true}
                                ,{field:'fabricColorNumber', title:'面料色号', align:'center', width:100, sort: true, filter: true}
                                ,{field:'supplier', title:'供应商', align:'center', width:80, sort: true, filter: true}
                                ,{field:'orderCount', title:'订单量', align:'center', width:100, sort: true, filter: true}
                                ,{field:'orderPieceUsage', title:'接单用量', align:'center', width:100, sort: true, filter: true}
                                ,{field:'pieceUsage', title:'单件用量', align:'center', width:100, sort: true, filter: true}
                                ,{field:'fabricAdd', title:'上浮', align:'center', width:100, sort: true, filter: true}
                                ,{field:'fabricActCount', title:'订布量', align:'center', width:120, sort: true, filter: true, totalRow: true}
                                ,{field:'price', hide: true}
                                ,{field:'sumMoney', hide: true}
                                ,{field:'remark', title:'备注', align:'center', width:120, sort: true, filter: true, totalRow: true}
                                ,{field:'fabricID',hide:true}
                            ]]
                            ,excel: {
                                filename: '面料总表.xlsx'
                            }
                            ,data: reportData
                            ,height: 'full-100'
                            ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                            ,title: '面料总表'
                            ,totalRow: true
                            ,loading: false
                            ,page: true
                            ,even: true
                            ,limits: [50, 100, 200]
                            ,limit: 100 //每页默认显示的数量
                            ,done: function () {
                                soulTable.render(this);
                            }
                        });

                    } else {
                        layer.msg("获取失败！", {icon: 2});
                    }
                }, error: function () {
                    layer.close(loading);
                    layer.msg("获取失败！", {icon: 2});
                }
            });
        }
        else {
            $.ajax({
                url: "/erp/getoneyearmanufactureaccessorylay",
                type: 'GET',
                data: param,
                success: function (res) {
                    layer.close(loading);
                    if (res.data) {
                        var reportData = res.data;
                        table.render({
                            elem: '#fabricReturnTable'
                            ,cols:[[
                                {title: '#', width:50 , show: 2, layerOption: {title: '子表', area: ['1300px','650px']}, children:[
                                        {
                                            title: '入库记录'
                                            ,url: 'getbjinstorerecordbyinfo'
                                            ,where: function(row){
                                                return {
                                                    orderName: row.orderName
                                                }
                                            }
                                            ,loading:true
                                            ,page: false
                                            ,limit: Number.MAX_VALUE //每页默认显示的数量
                                            ,totalRow: true
                                            ,height:500
                                            ,cols: [[
                                                {field: 'id',align:'center', hide:true},
                                                {field: 'orderName', title: '款号',align:'center', hide:true},
                                                {field: 'clothesVersionNumber', title: '版单',align:'center', hide:true},
                                                {field: 'accessoryName', title: '辅料名称',align:'center', minWidth: 100, totalRowText: '合计', filter: true},
                                                {field: 'accessoryNumber', title: '辅料编号',align:'center', minWidth: 100, filter: true},
                                                {field: 'supplier', title: '供应商',align:'center', filter: true},
                                                {field: 'specification', title: '辅料规格',align:'center', minWidth: 100 , filter: true},
                                                {field: 'accessoryUnit', title: '辅料单位',align:'center', minWidth: 100 , filter: true},
                                                {field: 'accessoryColor', title: '辅料颜色',align:'center', minWidth: 100 , filter: true},
                                                {field: 'colorName', title: '色组',align:'center', minWidth: 200, filter: true },
                                                {field: 'sizeName', title: '尺码',align:'center', minWidth: 200, filter: true},
                                                {field: 'inStoreCount', title: '入库数量',align:'center', minWidth: 100, totalRow: true, filter: true},
                                                {field: 'accountCount', title: '对账数量',align:'center', minWidth: 100, totalRow: true, filter: true},
                                                {field: 'colorNumber', title: '色号',align:'center', minWidth: 100, filter: true},
                                                {field: 'inStoreTime', title: '入库时间',align:'center', minWidth: 120,templet:function (d) {
                                                        return moment(d.inStoreTime).format("YYYY-MM-DD");
                                                    }, filter: true},
                                                {field: 'accessoryLocation', title: '位置',align:'center', minWidth: 100, filter: true},
                                                {field: 'accessoryID',title: '操作', minWidth: 180,align:'center', templet: '#accessoryChildBar'}
                                            ]]
                                            ,done: function () {
                                                soulTable.render(this);
                                            }
                                            ,toolEvent: function (obj, pobj) {
                                                // obj 子表当前行对象
                                                // pobj 父表当前行对象
                                                var objData = obj.data;
                                                var childId = this.id; // 通过 this 对象获取当前子表的id
                                                if (obj.event === 'deleteAccessoryInStore'){
                                                    layer.confirm('真的删除吗', function(index){
                                                        var id = objData.id;
                                                        var orderName = objData.orderName;
                                                        var accessoryID = objData.accessoryID;
                                                        var accessoryName = objData.accessoryName;
                                                        var colorName = objData.colorName;
                                                        var sizeName = objData.sizeName;
                                                        var inStoreCount = objData.inStoreCount;
                                                        var accessoryLocation = objData.accessoryLocation;
                                                        $.ajax({
                                                            url: "/erp/deleteaccessoryinstoredata",
                                                            type: 'POST',
                                                            data: {
                                                                id:id,
                                                                orderName:orderName,
                                                                accessoryName:accessoryName,
                                                                colorName:colorName,
                                                                sizeName:sizeName,
                                                                inStoreCount:inStoreCount,
                                                                accessoryLocation:accessoryLocation
                                                            },
                                                            success: function (res) {
                                                                if (res.result == 0) {
                                                                    layer.close(index);
                                                                    layer.msg("删除成功！", {icon: 1});
                                                                    obj.del();
                                                                    table.reload(childId);
                                                                } else if (res.result == 3) {
                                                                    layer.msg("库存不足,无法删除！", {icon: 2});
                                                                } else {
                                                                    layer.msg("删除失败！", {icon: 2});
                                                                }
                                                            },
                                                            error: function () {
                                                                layer.msg("删除失败！", {icon: 2});
                                                            }
                                                        })
                                                    });
                                                } else if (obj.event === 'change') {
                                                    var index = layer.open({
                                                        type: 1 //Page层类型
                                                        , title: '入库记录转补料单'
                                                        , btn: ['保存']
                                                        , shade: 0.6 //遮罩透明度
                                                        , anim: 0 //0-6的动画形式，-1不开启
                                                        , content: "<div id='changeDiv'>" +
                                                            "            <form class=\"layui-form\" style=\"margin: 0 auto;max-width:100%; padding-top: 0px;\">\n" +
                                                            "                <table class=\"layui-hide\" id=\"accessoryChangeTable\" lay-filter=\"accessoryChangeTable\"></table>\n" +
                                                            "            </form>\n" +
                                                            "      </div>"
                                                        ,yes: function(index, layero){
                                                            var accessoryChangeTableData = table.cache.accessoryChangeTable;
                                                            var param = {};
                                                            param.id = objData.id;
                                                            var flag = true;
                                                            $.each(accessoryChangeTableData, function (index, item){
                                                                if (item.LAY_CHECKED){
                                                                    param.accessoryName = item.accessoryName;
                                                                    param.accessoryID = item.accessoryID;
                                                                    flag = false;
                                                                }
                                                            });
                                                            if (flag){
                                                                layer.msg("请选择要转的行~~~");
                                                                return false;
                                                            }
                                                            if (objData.accessoryName != param.accessoryName){
                                                                layer.msg("辅料不对应,不能乱转~~~");
                                                                return false;
                                                            }
                                                            $.ajax({
                                                                url: "/erp/changeaccessoryinstoredatatoanother",
                                                                type: 'POST',
                                                                data: param,
                                                                success: function (res) {
                                                                    if (res.result == 0) {
                                                                        table.reload(childId);
                                                                        layer.close(index);
                                                                        layer.msg("出库成功！", {icon: 1});
                                                                    } else if (res.result == 3) {
                                                                        layer.msg("还未审核通过！", {icon: 2});
                                                                    } else if (res.result == 4) {
                                                                        layer.msg("财务已经对账,不能转了！", {icon: 2});
                                                                    } else {
                                                                        layer.msg("出库失败！", {icon: 2});
                                                                    }
                                                                }, error: function () {
                                                                    layer.msg("出库失败！", {icon: 2});
                                                                }
                                                            })
                                                        }, cancel : function (i,layero) {

                                                        }
                                                    });
                                                    layer.full(index);
                                                    setTimeout(function () {
                                                        $.ajax({
                                                            url: "/erp/getmanufactureaccessorybyorder",
                                                            type: 'GET',
                                                            data: {
                                                                orderName: objData.orderName
                                                            },
                                                            success: function (res) {
                                                                if (res.data) {
                                                                    var title = [
                                                                        {type: 'radio'},
                                                                        {field: 'clothesVersionNumber', title: '单号', align: 'center', minWidth: 100, sort: true, filter: true},
                                                                        {field: 'orderName', title: '款号', align: 'center', minWidth: 100, sort: true, filter: true},
                                                                        {field: 'accessoryName', title: '辅料名称', align: 'center', minWidth: 120, sort: true, filter: true},
                                                                        {field: 'accessoryNumber', title: '编号', align: 'center', minWidth: 80, sort: true, filter: true},
                                                                        {field: 'specification', title: '规格', align: 'center', minWidth: 80, sort: true, filter: true},
                                                                        {field: 'accessoryUnit', title: '单位', align: 'center', minWidth: 80, sort: true, filter: true},
                                                                        {field: 'accessoryColor', title: '辅料颜色', align: 'center', minWidth: 100, sort: true, filter: true},
                                                                        {field: 'sizeName', title: '尺码', align: 'center', minWidth: 100, sort: true, filter: true},
                                                                        {field: 'colorName', title: '颜色', align: 'center', minWidth: 100, sort: true, filter: true},
                                                                        {field: 'supplier', title: '供应商', align: 'center', minWidth: 80, sort: true, filter: true},
                                                                        {field: 'pieceUsage', title: '单件用量', align: 'center', minWidth: 90, sort: true, filter: true},
                                                                        {field: 'orderCount', title: '订单量', align: 'center', minWidth: 90, sort: true, filter: true, totalRow: true},
                                                                        {field: 'accessoryCount', title: '订购量', align: 'center', minWidth: 90, sort: true, filter: true, totalRow: true},
                                                                        {field: 'remark', title: '备注', align: 'center', minWidth: 60, sort: true, filter: true},
                                                                        {field: 'accessoryID', title: '', hide:true}
                                                                    ];
                                                                    var accessoryChangeData = [];
                                                                    $.each(res.data,function(index1,value1){
                                                                        var thisAccessoryData = {};
                                                                        thisAccessoryData.accessoryID = value1.accessoryID;
                                                                        thisAccessoryData.orderName = value1.orderName;
                                                                        thisAccessoryData.clothesVersionNumber = value1.clothesVersionNumber;
                                                                        thisAccessoryData.accessoryName = value1.accessoryName;
                                                                        thisAccessoryData.accessoryNumber = value1.accessoryNumber;
                                                                        thisAccessoryData.specification = value1.specification;
                                                                        thisAccessoryData.accessoryUnit = value1.accessoryUnit;
                                                                        thisAccessoryData.accessoryColor = value1.accessoryColor;
                                                                        thisAccessoryData.pieceUsage = value1.pieceUsage;
                                                                        thisAccessoryData.orderCount = value1.orderCount;
                                                                        thisAccessoryData.sizeName = value1.sizeName;
                                                                        thisAccessoryData.colorName = value1.colorName;
                                                                        thisAccessoryData.accessoryCount = value1.accessoryCount;
                                                                        thisAccessoryData.supplier = value1.supplier;
                                                                        thisAccessoryData.remark = value1.remark;
                                                                        accessoryChangeData.push(thisAccessoryData);
                                                                    });
                                                                    table.render({
                                                                        elem: '#accessoryChangeTable'
                                                                        , cols: [title]
                                                                        , data: accessoryChangeData
                                                                        , height: 'full-180'
                                                                        , even: true
                                                                        , overflow: 'tips'
                                                                        , totalRow: true
                                                                        , limit: Number.MAX_VALUE //每页默认显示的数量
                                                                        , done: function (res) {
                                                                            soulTable.render(this);
                                                                        }
                                                                    });
                                                                    //监听单元格编辑
                                                                    table.on('tool(accessoryReturnAddTable)', function (obj) {
                                                                        var data = obj.data;
                                                                        var tableData = table.cache.accessoryReturnAddTable;
                                                                        var that = this;
                                                                        var field = $(that).data('field');
                                                                        if(obj.event === 'date'){
                                                                            laydate.render({
                                                                                elem: $(this).find('.layui-table-edit').get(0)//
                                                                                ,show: true //直接显示
                                                                                ,done: function(value, date){
                                                                                    obj.update({
                                                                                        [field] : value
                                                                                    });
                                                                                }
                                                                            });
                                                                        }
                                                                    });
                                                                } else {
                                                                    layer.msg("获取失败！", {icon: 2});
                                                                }
                                                            }, error: function () {
                                                                layer.msg("获取失败！", {icon: 2});
                                                            }
                                                        })
                                                    }, 100);
                                                }
                                            }
                                        },
                                        {
                                            title: '库存记录'
                                            ,url: 'getbjstoragebyinfo'
                                            ,where: function(row){
                                                return {
                                                    orderName: row.orderName
                                                }
                                            }
                                            ,loading:true
                                            ,page: false
                                            ,height:500
                                            ,limit: Number.MAX_VALUE //每页默认显示的数量
                                            ,totalRow: true
                                            ,cols: [[
                                                {field: 'id',align:'center', hide:true},
                                                {field: 'accessoryName', title: '辅料名称',align:'center', minWidth: 100, totalRowText: '合计', filter: true},
                                                {field: 'accessoryNumber', title: '编号',align:'center', minWidth: 100, filter: true},
                                                {field: 'orderName', title: '款号',align:'center', hide:true},
                                                {field: 'clothesVersionNumber', title: '版单',align:'center', hide:true},
                                                {field: 'supplier', title: '供应商',align:'center', hide:true},
                                                {field: 'specification', title: '辅料规格',align:'center', minWidth: 100 , filter: true},
                                                {field: 'accessoryUnit', title: '辅料单位',align:'center', minWidth: 100 , filter: true},
                                                {field: 'accessoryColor', title: '辅料颜色',align:'center', minWidth: 100 , filter: true},
                                                {field: 'colorName', title: '色组',align:'center', minWidth: 100, filter: true },
                                                {field: 'sizeName', title: '尺码',align:'center', minWidth: 100, filter: true},
                                                {field: 'storageCount', title: '库存数量',align:'center', minWidth: 100, totalRow: true, filter: true},
                                                {field: 'updateTime', title: '记录时间',align:'center', filter: true, minWidth: 120,templet:function (d) {
                                                        return moment(d.updateTime).format("YYYY-MM-DD");
                                                    }},
                                                {field: 'accessoryLocation', title: '位置',align:'center', minWidth: 100, filter: true},
                                                {field: 'accessoryID',title: '操作', minWidth: 120,align:'center', templet: '#accessoryStorageChildBar'},
                                                {field: 'pieceUsage',align:'center', hide:true}
                                            ]]
                                            ,done: function () {
                                                soulTable.render(this);
                                            }
                                            ,toolEvent: function (obj, pobj) {
                                                // obj 子表当前行对象
                                                // pobj 父表当前行对象
                                                var objData = obj.data;
                                                var childId = this.id; // 通过 this 对象获取当前子表的id
                                                if (obj.event === 'outStoreSingle') {
                                                    var index = layer.open({
                                                        type: 1 //Page层类型
                                                        , title: '辅料出库'
                                                        , btn: ['保存']
                                                        , shade: 0.6 //遮罩透明度
                                                        ,area: ['420px', '300px'] //宽高
                                                        , anim: 0 //0-6的动画形式，-1不开启
                                                        , content: '<div style="padding-top: 30px">\n' +
                                                            '                    <table>' +
                                                            '                        <tr>' +
                                                            '                            <td style="text-align: right;margin-bottom: 15px;width: 100px">\n' +
                                                            '                                 <label class="layui-form-label" style="100px">库存</label>\n' +
                                                            '                            </td>\n' +
                                                            '                            <td style="margin-bottom: 15px;">\n' +
                                                            '                                 <input type="text" style="width: 260px;" id="storageCount" readonly class="layui-input">\n' +
                                                            '                            </td>' +
                                                            '                        </tr>' +
                                                            '                        <tr>' +
                                                            '                            <td style="text-align: right;margin-bottom: 15px;width: 100px">\n' +
                                                            '                                 <label class="layui-form-label" style="100px">出库</label>\n' +
                                                            '                            </td>\n' +
                                                            '                            <td style="margin-bottom: 15px;">\n' +
                                                            '                                 <input type="text" style="width: 260px;" id="outStoreCount" class="layui-input">\n' +
                                                            '                            </td>' +
                                                            '                        </tr>' +
                                                            '                        <tr>' +
                                                            '                            <td style="text-align: right;margin-bottom: 15px;width: 100px">\n' +
                                                            '                                 <label class="layui-form-label" style="100px">备注</label>\n' +
                                                            '                            </td>\n' +
                                                            '                            <td style="margin-bottom: 15px;">\n' +
                                                            '                                 <input type="text" name="remark" style="width: 260px;" id="remark" value="无" class="layui-input">\n' +
                                                            '                            </td>' +
                                                            '                        </tr>' +
                                                            '                    </table>' +
                                                            '                </div>'
                                                        ,yes: function(index, layero){
                                                            var storageCount = $("#storageCount").val();
                                                            var outStoreCount = $("#outStoreCount").val();
                                                            var remark = $("#remark").val();
                                                            if (outStoreCount == 0 || outStoreCount == "" || (Number(outStoreCount) > Number(storageCount))){
                                                                layer.msg("数量有误！", {icon: 2});
                                                                return false;
                                                            }
                                                            var accessoryOutStoreJson = [];
                                                            accessoryOutStoreJson.push({
                                                                orderName:objData.orderName,
                                                                clothesVersionNumber:objData.clothesVersionNumber,
                                                                id:objData.id,
                                                                accessoryID:objData.accessoryID,
                                                                outStoreCount:outStoreCount,
                                                                accessoryName : objData.accessoryName,
                                                                accessoryNumber : objData.accessoryNumber,
                                                                specification : objData.specification,
                                                                accessoryUnit : objData.accessoryUnit,
                                                                accessoryColor : objData.accessoryColor,
                                                                supplier : objData.supplier,
                                                                pieceUsage : objData.pieceUsage,
                                                                sizeName : objData.sizeName,
                                                                colorName : objData.colorName,
                                                                storageCount : objData.storageCount,
                                                                accessoryLocation : objData.accessoryLocation,
                                                                remark: remark
                                                            });
                                                            $.ajax({
                                                                url: "/erp/accessoryoutstorebatch",
                                                                type: 'POST',
                                                                data: {
                                                                    accessoryOutStoreJson: JSON.stringify(accessoryOutStoreJson)
                                                                },
                                                                success: function (res) {
                                                                    if (res.result == 0) {
                                                                        table.reload(childId);
                                                                        layer.close(index);
                                                                        layer.msg("出库成功！", {icon: 1});
                                                                    } else {
                                                                        layer.msg("出库失败！", {icon: 2});
                                                                    }
                                                                }, error: function () {
                                                                    layer.msg("出库失败！", {icon: 2});
                                                                }
                                                            })
                                                        }, cancel : function (i,layero) {
                                                            $("#storageCount").val('');
                                                            $("#outStoreCount").val('');
                                                            $("#remark").val('');
                                                        }
                                                    });
                                                    setTimeout(function () {
                                                        $("#storageCount").val(objData.storageCount);
                                                    }, 100);
                                                }
                                            }
                                        },
                                        {
                                            title: '出库记录'
                                            ,url: 'getbjoutrecordbyinfo'
                                            ,where: function(row){
                                                return {
                                                    orderName: row.orderName
                                                }
                                            }
                                            ,loading:true
                                            ,page: false
                                            ,limit: Number.MAX_VALUE //每页默认显示的数量
                                            ,totalRow: true
                                            ,height:500
                                            ,cols: [[
                                                {field: 'id',align:'center', hide:true},
                                                {field: 'orderName', title: '款号',align:'center', hide:true},
                                                {field: 'clothesVersionNumber', title: '版单',align:'center', hide:true},
                                                {field: 'accessoryName', title: '辅料名称',align:'center', minWidth: 200, totalRowText: '合计', filter: true},
                                                {field: 'accessoryNumber', title: '辅料编号',align:'center', minWidth: 200, filter: true},
                                                {field: 'supplier', title: '供应商',align:'center', filter: true},
                                                {field: 'specification', title: '辅料规格',align:'center', minWidth: 100, filter: true },
                                                {field: 'accessoryUnit', title: '辅料单位',align:'center', minWidth: 100, filter: true },
                                                {field: 'accessoryColor', title: '辅料颜色',align:'center', minWidth: 100, filter: true },
                                                {field: 'colorName', title: '色组',align:'center', minWidth: 100, filter: true },
                                                {field: 'sizeName', title: '尺码',align:'center', minWidth: 100, filter: true},
                                                {field: 'outStoreCount', title: '出库数量',align:'center', minWidth: 100, totalRow: true, filter: true},
                                                {field: 'createTime', title: '出库时间',align:'center', filter: true, minWidth: 180,templet:function (d) {
                                                        return moment(d.createTime).format("YYYY-MM-DD HH:MM:SS");
                                                    }},
                                                {field: 'accessoryLocation', title: '位置', filter: true,align:'center', minWidth: 100},
                                                {field: 'receiver', title: '接收', filter: true,align:'center', minWidth: 100},
                                                {field: 'remark', title: '备注',align:'center', filter: true, minWidth: 100}
                                            ]]
                                            ,done: function () {
                                                soulTable.render(this);
                                            }
                                        }]}
                                ,{type:'radio'}
                                ,{type:'numbers', align:'center', title:'序号'}
                                ,{field: 'orderName', title: '订单',align:'center', minWidth: 120, filter: true}
                                ,{field: 'clothesVersionNumber', title: '版单',align:'center', minWidth: 120, filter: true}
                                ,{field: 'orderCount', title: '订单数量',align:'center', minWidth: 90, filter: true}
                                ,{field: 'wellCount', title: '好片数',align:'center', minWidth: 120, filter: true}
                                ,{fixed: 'right', title:'操作', align:'center', toolbar: '#toolbar', width:180}
                            ]]
                            ,excel: {
                                filename: '辅料操作表.xlsx'
                            }
                            ,data: reportData
                            ,height: 'full-100'
                            ,toolbar: '#toolbarTopTwo' //开启头部工具栏，并为其绑定左侧模板
                            ,title: '辅料操作表'
                            ,totalRow: true
                            ,loading: false
                            ,page: true
                            ,even: true
                            ,limits: [50, 100, 200]
                            ,limit: 100 //每页默认显示的数量
                            ,done: function () {
                                soulTable.render(this);
                            }
                        });
                    } else {
                        layer.msg("获取失败！", {icon: 2});
                    }
                },
                error: function () {
                    layer.close(loading);
                    layer.msg("获取失败！", {icon: 2});
                }
            });
        }
    }
    function initCheckTable(checkNumber){
        var param = {};
        if (checkNumber != null && checkNumber != ''){
            param.checkNumber = checkNumber;
        }
        $.ajax({
            url: "/erp/getmanufacturefabricbychecknumber",
            type: 'GET',
            data: param,
            success: function (res) {
                if (res.data) {
                    var reportData = res.data;
                    table.render({
                        elem: '#fabricReturnTable'
                        ,cols:[[
                            {title: '#', width: 50, collapse: true,lazy: true, children:[
                                    {
                                        title: '入库记录'
                                        ,url: '/erp/getfabricdetailbyinfo'
                                        ,where: function(row){
                                            return {
                                                fabricID: row.fabricID
                                            }
                                        }
                                        ,loading:true
                                        ,page: false
                                        ,totalRow: true
                                        ,cols: [[
                                            {field: 'fabricName', title: '面料名称',align:'center', minWidth: 200, totalRowText: '合计'},
                                            {field: 'fabricNumber', title: '面料号',align:'center', minWidth: 100 },
                                            {field: 'colorName', title: '订单颜色',align:'center', minWidth: 100 },
                                            {field: 'isHit', title: '是否撞色',align:'center', minWidth: 100 },
                                            {field: 'fabricColor', title: '面料颜色',align:'center', minWidth: 100},
                                            {field: 'fabricColorNumber', title: '面料色号',align:'center', minWidth: 100},
                                            {field: 'jarName', title: '缸号',align:'center', minWidth: 100},
                                            {field: 'unit', title: '单位',align:'center', minWidth: 80},
                                            {field: 'batchNumber', title: '卷数',align:'center', minWidth: 100, totalRow: true},
                                            {field: 'weight', title: '数量',align:'center', minWidth: 100, totalRow: true},
                                            {field: 'location', title: '位置',align:'center', minWidth: 100},
                                            {field: 'returnTime', title: '入库时间',align:'center', minWidth: 120,templet:function (d) {
                                                    return moment(d.returnTime).format("YYYY-MM-DD");
                                                }},
                                            {field: 'supplier', title: '供应商',align:'center', minWidth: 100},
                                            {field: 'fabricDetailID', align:'center', hide:true},
                                            {field: 'orderName', align:'center', hide:true},
                                            {field: 'clothesVersionNumber', align:'center', hide:true},
                                            {field: 'remark', title: '备注', align:'center', minWidth: 120},
                                            {field: 'fabricID', align:'center', hide:true},
                                            {title: '操作', minWidth: 250,align:'center', templet: function(row) {
                                                    return '<a class="layui-btn layui-btn-xs layui-btn-normal" lay-event="updateFabricReturnData">修改</a><a class="layui-btn layui-btn-xs layui-btn-danger" lay-event="deleteFabricReturnData">删除</a>';
                                                }}
                                        ]]
                                        ,toolEvent: function (obj, pobj) {
                                            // obj 子表当前行对象
                                            // pobj 父表当前行对象
                                            var rowData = obj.data;
                                            var childId = this.id; // 通过 this 对象获取当前子表的id
                                            if (obj.event === 'updateFabricReturnData') {
                                                var index = layer.open({
                                                    type: 1 //Page层类型
                                                    , title: '修改面料入库信息'
                                                    , btn: ['保存']
                                                    , shade: 0.6 //遮罩透明度
                                                    , maxmin: false //允许全屏最小化
                                                    , anim: 0 //0-6的动画形式，-1不开启
                                                    , content: "<div id=\"fabricInStoreUpdate\" style=\"cursor:default;\">\n" +
                                                        "        <form class=\"layui-form\" style=\"margin: 5px;padding-top: 40px;\" lay-filter=\"inStoreUpdate\">\n" +
                                                        "            <div class=\"layui-row layui-col-space10\">\n" +
                                                        "                <div class=\"layui-col-md3\">\n" +
                                                        "                    <label class=\"layui-form-label\">面料名称</label>\n" +
                                                        "                    <div class=\"layui-input-inline\">\n" +
                                                        "                        <input type=\"text\" name=\"fabricName\" autocomplete=\"off\" class=\"layui-input\" readonly=\"true\">\n" +
                                                        "                    </div>\n" +
                                                        "                </div>\n" +
                                                        "                <div class=\"layui-col-md3\">\n" +
                                                        "                    <label class=\"layui-form-label\">面料号</label>\n" +
                                                        "                    <div class=\"layui-input-inline\">\n" +
                                                        "                        <input type=\"text\" name=\"fabricNumber\" autocomplete=\"off\" class=\"layui-input\" readonly=\"true\">\n" +
                                                        "                    </div>\n" +
                                                        "                </div>\n" +
                                                        "                <div class=\"layui-col-md3\">\n" +
                                                        "                    <label class=\"layui-form-label\">色组</label>\n" +
                                                        "                    <div class=\"layui-input-inline\">\n" +
                                                        "                        <input type=\"text\" name=\"colorName\" autocomplete=\"off\" class=\"layui-input\" readonly=\"true\">\n" +
                                                        "                    </div>\n" +
                                                        "                </div>\n" +
                                                        "                <div class=\"layui-col-md3\">\n" +
                                                        "                    <label class=\"layui-form-label\">是否撞色</label>\n" +
                                                        "                    <div class=\"layui-input-inline\">\n" +
                                                        "                        <input type=\"text\" name=\"isHit\" autocomplete=\"off\" class=\"layui-input\" readonly=\"true\">\n" +
                                                        "                    </div>\n" +
                                                        "                </div>\n" +
                                                        "            </div>\n" +
                                                        "            <div class=\"layui-row layui-col-space10\">\n" +
                                                        "                <div class=\"layui-col-md3\">\n" +
                                                        "                    <label class=\"layui-form-label\">面料颜色</label>\n" +
                                                        "                    <div class=\"layui-input-inline\">\n" +
                                                        "                        <input type=\"text\" name=\"fabricColor\" autocomplete=\"off\" class=\"layui-input\" readonly=\"true\">\n" +
                                                        "                    </div>\n" +
                                                        "                </div>\n" +
                                                        "                <div class=\"layui-col-md3\">\n" +
                                                        "                    <label class=\"layui-form-label\">面料色号</label>\n" +
                                                        "                    <div class=\"layui-input-inline\">\n" +
                                                        "                        <input type=\"text\" name=\"fabricColorNumber\" autocomplete=\"off\" class=\"layui-input\" readonly=\"true\">\n" +
                                                        "                    </div>\n" +
                                                        "                </div>\n" +
                                                        "                <div class=\"layui-col-md3\">\n" +
                                                        "                    <label class=\"layui-form-label\">供应商</label>\n" +
                                                        "                    <div class=\"layui-input-inline\">\n" +
                                                        "                        <input type=\"text\" name=\"supplier\" autocomplete=\"off\" class=\"layui-input\" readonly=\"true\">\n" +
                                                        "                    </div>\n" +
                                                        "                </div>\n" +
                                                        "                <div class=\"layui-col-md3\">\n" +
                                                        "                    <label class=\"layui-form-label\">单位</label>\n" +
                                                        "                    <div class=\"layui-input-inline\">\n" +
                                                        "                        <input type=\"text\" name=\"unit\" autocomplete=\"off\" class=\"layui-input\" readonly=\"true\">\n" +
                                                        "                    </div>\n" +
                                                        "                </div>\n" +
                                                        "            </div>\n" +
                                                        "            <div class=\"layui-row layui-col-space10\">\n" +
                                                        "                <div class=\"layui-col-md3\">\n" +
                                                        "                    <label class=\"layui-form-label\">缸号</label>\n" +
                                                        "                    <div class=\"layui-input-inline\">\n" +
                                                        "                        <input type=\"text\" name=\"jarName\" autocomplete=\"off\" class=\"layui-input\">\n" +
                                                        "                    </div>\n" +
                                                        "                </div>\n" +
                                                        "                <div class=\"layui-col-md3\">\n" +
                                                        "                    <label class=\"layui-form-label\">卷数</label>\n" +
                                                        "                    <div class=\"layui-input-inline\">\n" +
                                                        "                        <input type=\"text\" name=\"batchNumber\" autocomplete=\"off\" class=\"layui-input\">\n" +
                                                        "                    </div>\n" +
                                                        "                </div>\n" +
                                                        "                <div class=\"layui-col-md3\">\n" +
                                                        "                    <label class=\"layui-form-label\">重量</label>\n" +
                                                        "                    <div class=\"layui-input-inline\">\n" +
                                                        "                        <input type=\"text\" name=\"weight\" autocomplete=\"off\" class=\"layui-input\">\n" +
                                                        "                    </div>\n" +
                                                        "                </div>\n" +
                                                        "                <div class=\"layui-col-md3\">\n" +
                                                        "                    <label class=\"layui-form-label\">入库时间</label>\n" +
                                                        "                    <div class=\"layui-input-inline\">\n" +
                                                        "                        <input type=\"text\" name=\"returnTime\" id=\"updateInStoreTime\" autocomplete=\"off\" class=\"layui-input\" readonly>\n" +
                                                        "                    </div>\n" +
                                                        "                </div>\n" +
                                                        "            </div>\n" +
                                                        "            <div class=\"layui-row layui-col-space10\">\n" +
                                                        "                <div class=\"layui-col-md3\">\n" +
                                                        "                    <label class=\"layui-form-label\">位置</label>\n" +
                                                        "                    <div class=\"layui-input-inline\">\n" +
                                                        "                        <input type=\"text\" name=\"location\" id=\"updateInStoreLocation\" autocomplete=\"off\" class=\"layui-input\">\n" +
                                                        "                    </div>\n" +
                                                        "                </div>\n" +
                                                        "            </div>\n" +
                                                        "        </form>\n" +
                                                        "    </div>"
                                                    ,yes: function(index, layero){
                                                        var params = layui.form.val("inStoreUpdate");
                                                        params.fabricDetailID = rowData.fabricDetailID;
                                                        params.fabricID = rowData.fabricID;
                                                        params.orderName = rowData.orderName;
                                                        params.clothesVersionNumber = rowData.clothesVersionNumber;
                                                        $.ajax({
                                                            url: "/erp/updatefabricdetail",
                                                            type: 'POST',
                                                            data: {
                                                                fabricDetail: JSON.stringify(params)
                                                            },
                                                            success: function (res) {
                                                                if (res == 0) {
                                                                    layer.close(index);
                                                                    layer.msg("保存成功！", {icon: 1});
                                                                    $("#fabricInStoreUpdate").find("input").val("");
                                                                    reloadTable();
                                                                } else if (res == 3) {
                                                                    layer.msg("此布已松，无法修改！");
                                                                } else if (res == 4) {
                                                                    layer.msg("库存不足，无法修改！");
                                                                } else {
                                                                    layer.msg("保存失败！", {icon: 2});
                                                                }
                                                            },
                                                            error: function () {
                                                                layer.msg("保存失败！", {icon: 2});
                                                            }
                                                        })
                                                    }
                                                    , cancel : function (i,layero) {
                                                        $("#fabricInStoreUpdate").find("input").val("");
                                                    }
                                                });
                                                layer.full(index);
                                                $("#fabricInStoreUpdate input[name='fabricName']").val(rowData.fabricName);
                                                $("#fabricInStoreUpdate input[name='fabricNumber']").val(rowData.fabricNumber);
                                                $("#fabricInStoreUpdate input[name='colorName']").val(rowData.colorName);
                                                $("#fabricInStoreUpdate input[name='isHit']").val(rowData.isHit);
                                                $("#fabricInStoreUpdate input[name='fabricColor']").val(rowData.fabricColor);
                                                $("#fabricInStoreUpdate input[name='fabricColorNumber']").val(rowData.fabricColorNumber);
                                                $("#fabricInStoreUpdate input[name='jarName']").val(rowData.jarName);
                                                $("#fabricInStoreUpdate input[name='unit']").val(rowData.unit);
                                                $("#fabricInStoreUpdate input[name='batchNumber']").val(rowData.batchNumber);
                                                $("#fabricInStoreUpdate input[name='weight']").val(rowData.weight);
                                                $("#fabricInStoreUpdate input[name='location']").val(rowData.location);
                                                $("#fabricInStoreUpdate input[name='returnTime']").val(moment(rowData.returnTime).format("YYYY-MM-DD"));
                                                $("#fabricInStoreUpdate input[name='supplier']").val(rowData.supplier);
                                                form.render('select');
                                                $("#initCount").val(rowData.inStoreCount);
                                                return false;
                                            } else if (obj.event === 'deleteFabricReturnData'){
                                                layer.confirm('真的删除吗', function(index){
                                                    $.ajax({
                                                        url: "/erp/deletefabricdetailbyid",
                                                        type: 'POST',
                                                        data: {
                                                            fabricDetailID: rowData.fabricDetailID
                                                        },
                                                        success: function (res) {
                                                            if (res == 0) {
                                                                layer.close(index);
                                                                layer.msg("删除成功！", {icon: 1});
                                                                reloadTable();
                                                            }else if (res == 3){
                                                                layer.msg("此布已松,不能删除！");
                                                            } else if (res == 4){
                                                                layer.msg("库存不足,不能删除！");
                                                            } else {
                                                                layer.msg("删除失败！", {icon: 2});
                                                            }
                                                        },
                                                        error: function () {
                                                            layer.msg("删除失败！", {icon: 2});
                                                        }
                                                    })
                                                });
                                            } else if (obj.event === 'fabricReturnDataChangeOrder'){
                                                var index = layer.open({
                                                    type: 1 //Page层类型
                                                    , title: '面料换款'
                                                    , btn: ['保存']
                                                    , shade: 0.6 //遮罩透明度
                                                    , maxmin: false //允许全屏最小化
                                                    , anim: 0 //0-6的动画形式，-1不开启
                                                    , content: "<div id=\"fabricReturnDataChangeOrder\" style=\"cursor:default;\" xmlns=\"http://www.w3.org/1999/html\">\n" +
                                                        "        <form class=\"layui-form\" style=\"margin: 5px;padding-top: 40px;\" lay-filter=\"returnChangeOrder\">\n" +
                                                        "            <div class=\"layui-row layui-col-space10\">\n" +
                                                        "                <div class=\"layui-col-md3\">\n" +
                                                        "                    <!-- 填充内容 -->\n" +
                                                        "                    <label class=\"layui-form-label\">面料名称</label>\n" +
                                                        "                    <div class=\"layui-input-inline\">\n" +
                                                        "                        <input type=\"text\" name=\"fabricName\" autocomplete=\"off\" class=\"layui-input\" readonly=\"true\">\n" +
                                                        "                    </div>\n" +
                                                        "                </div>\n" +
                                                        "                <div class=\"layui-col-md3\">\n" +
                                                        "                    <!-- 填充内容 -->\n" +
                                                        "                    <label class=\"layui-form-label\">面料号</label>\n" +
                                                        "                    <div class=\"layui-input-inline\">\n" +
                                                        "                        <input type=\"text\" name=\"fabricNumber\" autocomplete=\"off\" class=\"layui-input\" readonly=\"true\">\n" +
                                                        "                    </div>\n" +
                                                        "                </div>\n" +
                                                        "                <div class=\"layui-col-md3\">\n" +
                                                        "                    <!-- 填充内容 -->\n" +
                                                        "                    <label class=\"layui-form-label\">色组</label>\n" +
                                                        "                    <div class=\"layui-input-inline\">\n" +
                                                        "                        <input type=\"text\" name=\"colorName\" autocomplete=\"off\" class=\"layui-input\" readonly=\"true\">\n" +
                                                        "                    </div>\n" +
                                                        "                </div>\n" +
                                                        "                <div class=\"layui-col-md3\">\n" +
                                                        "                    <!-- 填充内容 -->\n" +
                                                        "                    <label class=\"layui-form-label\">是否撞色</label>\n" +
                                                        "                    <div class=\"layui-input-inline\">\n" +
                                                        "                        <input type=\"text\" name=\"isHit\" autocomplete=\"off\" class=\"layui-input\" readonly=\"true\">\n" +
                                                        "                    </div>\n" +
                                                        "                </div>\n" +
                                                        "            </div>\n" +
                                                        "            <div class=\"layui-row layui-col-space10\">\n" +
                                                        "                <div class=\"layui-col-md3\">\n" +
                                                        "                    <!-- 填充内容 -->\n" +
                                                        "                    <label class=\"layui-form-label\">面料颜色</label>\n" +
                                                        "                    <div class=\"layui-input-inline\">\n" +
                                                        "                        <input type=\"text\" name=\"fabricColor\" autocomplete=\"off\" class=\"layui-input\" readonly=\"true\">\n" +
                                                        "                    </div>\n" +
                                                        "                </div>\n" +
                                                        "                <div class=\"layui-col-md3\">\n" +
                                                        "                    <!-- 填充内容 -->\n" +
                                                        "                    <label class=\"layui-form-label\">面料色号</label>\n" +
                                                        "                    <div class=\"layui-input-inline\">\n" +
                                                        "                        <input type=\"text\" name=\"fabricColorNumber\" autocomplete=\"off\" class=\"layui-input\" readonly=\"true\">\n" +
                                                        "                    </div>\n" +
                                                        "                </div>\n" +
                                                        "                <div class=\"layui-col-md3\">\n" +
                                                        "                    <!-- 填充内容 -->\n" +
                                                        "                    <label class=\"layui-form-label\">供应商</label>\n" +
                                                        "                    <div class=\"layui-input-inline\">\n" +
                                                        "                        <input type=\"text\" name=\"supplier\" autocomplete=\"off\" class=\"layui-input\" readonly=\"true\">\n" +
                                                        "                    </div>\n" +
                                                        "                </div>\n" +
                                                        "                <div class=\"layui-col-md3\">\n" +
                                                        "                    <!-- 填充内容 -->\n" +
                                                        "                    <label class=\"layui-form-label\">单位</label>\n" +
                                                        "                    <div class=\"layui-input-inline\">\n" +
                                                        "                        <input type=\"text\" name=\"unit\" autocomplete=\"off\" class=\"layui-input\" readonly=\"true\">\n" +
                                                        "                    </div>\n" +
                                                        "                </div>\n" +
                                                        "            </div>\n" +
                                                        "            <div class=\"layui-row layui-col-space10\">\n" +
                                                        "                <div class=\"layui-col-md3\">\n" +
                                                        "                    <label class=\"layui-form-label\">缸号</label>\n" +
                                                        "                    <div class=\"layui-input-inline\">\n" +
                                                        "                        <input type=\"text\" name=\"jarName\" autocomplete=\"off\" class=\"layui-input\" readonly=\"true\">\n" +
                                                        "                    </div>\n" +
                                                        "                </div>\n" +
                                                        "                <div class=\"layui-col-md3\">\n" +
                                                        "                    <label class=\"layui-form-label\">卷数</label>\n" +
                                                        "                    <div class=\"layui-input-inline\">\n" +
                                                        "                        <input type=\"text\" name=\"batchNumber\" autocomplete=\"off\" class=\"layui-input\">\n" +
                                                        "                    </div>\n" +
                                                        "                </div>\n" +
                                                        "                <div class=\"layui-col-md3\">\n" +
                                                        "                    <label class=\"layui-form-label\">重量</label>\n" +
                                                        "                    <div class=\"layui-input-inline\">\n" +
                                                        "                        <input type=\"text\" name=\"weight\" autocomplete=\"off\" class=\"layui-input\">\n" +
                                                        "                    </div>\n" +
                                                        "                </div>\n" +
                                                        "                <div class=\"layui-col-md3\">\n" +
                                                        "                    <label class=\"layui-form-label\">入库时间</label>\n" +
                                                        "                    <div class=\"layui-input-inline\">\n" +
                                                        "                        <input type=\"text\" name=\"returnTime\" id=\"updateStorageTime\" autocomplete=\"off\" class=\"layui-input\" readonly=\"true\">\n" +
                                                        "                    </div>\n" +
                                                        "                </div>\n" +
                                                        "\n" +
                                                        "            </div>\n" +
                                                        "            <div class=\"layui-row layui-col-space10\">\n" +
                                                        "                <div class=\"layui-col-md3\">\n" +
                                                        "                    <label class=\"layui-form-label\">位置</label>\n" +
                                                        "                    <div class=\"layui-input-inline\">\n" +
                                                        "                        <input type=\"text\" name=\"location\" autocomplete=\"off\" class=\"layui-input\" readonly=\"true\">\n" +
                                                        "                    </div>\n" +
                                                        "                </div>\n" +
                                                        "                <div class=\"layui-col-md3\"></div>\n" +
                                                        "                <div class=\"layui-col-md3\"></div>\n" +
                                                        "                <div class=\"layui-col-md3\"></div>\n" +
                                                        "            </div>\n" +
                                                        "            <div class=\"layui-row layui-col-space10\" style=\"padding-top: 30px\">\n" +
                                                        "                <h3 style=\"text-align: center\">调换到:</h3><br>\n" +
                                                        "            </div>\n" +
                                                        "            <div class=\"layui-row layui-col-space10\">\n" +
                                                        "                <div class=\"layui-col-md3\">\n" +
                                                        "                    <label class=\"layui-form-label\">版单</label>\n" +
                                                        "                    <div class=\"layui-input-inline\">\n" +
                                                        "                        <input type=\"text\" name=\"toClothesVersionNumber\" id=\"toClothesVersionNumber\" autocomplete=\"off\" class=\"layui-input\">\n" +
                                                        "                    </div>\n" +
                                                        "                </div>\n" +
                                                        "                <div class=\"layui-col-md3\">\n" +
                                                        "                    <label class=\"layui-form-label\">订单</label>\n" +
                                                        "                    <div class=\"layui-input-inline\">\n" +
                                                        "                        <select type=\"text\" name=\"toOrderName\" id=\"toOrderName\" autocomplete=\"off\" class=\"layui-input\" lay-filter=\"test\"></select>\n" +
                                                        "                    </div>\n" +
                                                        "                </div>\n" +
                                                        "                <div class=\"layui-col-md3\">\n" +
                                                        "                    <label class=\"layui-form-label\">色组</label>\n" +
                                                        "                    <div class=\"layui-input-inline\">\n" +
                                                        "                        <select type=\"text\" name=\"toColorName\" id=\"toColorName\" autocomplete=\"off\" class=\"layui-input\"></select>\n" +
                                                        "                    </div>\n" +
                                                        "                </div>\n" +
                                                        "            </div>\n" +
                                                        "        </form>\n" +
                                                        "    </div>"
                                                    ,yes: function(index, layero){
                                                        var params = layui.form.val("returnChangeOrder");
                                                        params.fabricDetailID = rowData.fabricDetailID;
                                                        params.fabricID = rowData.fabricID;
                                                        params.orderName = rowData.orderName;
                                                        params.clothesVersionNumber = rowData.clothesVersionNumber;
                                                        var initWeight = rowData.weight;
                                                        var initBatch = rowData.batchNumber;
                                                        if (Number(params.batchNumber) > Number(initBatch) || Number(params.weight) > Number(initWeight)){
                                                            layer.msg("换款数量超数", {icon: 2});
                                                        }
                                                        $.ajax({
                                                            url: "/erp/fabricchangeorder",
                                                            type: 'POST',
                                                            data: {
                                                                fabricDetailJson: JSON.stringify(params),
                                                                toClothesVersionNumber: params.toClothesVersionNumber,
                                                                toOrderName: params.toOrderName,
                                                                toColorName: params.toColorName,
                                                                initWeight: initWeight,
                                                                initBatch: initBatch
                                                            },
                                                            success: function (data) {
                                                                if (data.error) {
                                                                    layer.msg(data.error, {icon: 2});
                                                                } else if(data.trans){
                                                                    layer.close(index);
                                                                    layer.msg(data.trans, {icon: 1});
                                                                    reloadTable();
                                                                    $("#fabricReturnDataChangeOrder").find("input").val("");
                                                                }else if(data.out){
                                                                    layer.close(index);
                                                                    layer.msg(data.out, {icon: 1});
                                                                    reloadTable();
                                                                    $("#fabricReturnDataChangeOrder").find("input").val("");
                                                                }else {
                                                                    layer.msg(data.error, {icon: 2});
                                                                }
                                                            }, error: function () {
                                                                layer.msg("调款失败！", {icon: 2});
                                                            }
                                                        })
                                                    }
                                                    , cancel : function (i,layero) {
                                                        $("#fabricReturnDataChangeOrder").find("input").val("");
                                                    }
                                                });
                                                layer.full(index);
                                                $("#fabricReturnDataChangeOrder input[name='fabricName']").val(rowData.fabricName);
                                                $("#fabricReturnDataChangeOrder input[name='fabricNumber']").val(rowData.fabricNumber);
                                                $("#fabricReturnDataChangeOrder input[name='colorName']").val(rowData.colorName);
                                                $("#fabricReturnDataChangeOrder input[name='isHit']").val(rowData.isHit);
                                                $("#fabricReturnDataChangeOrder input[name='fabricColor']").val(rowData.fabricColor);
                                                $("#fabricReturnDataChangeOrder input[name='fabricColorNumber']").val(rowData.fabricColorNumber);
                                                $("#fabricReturnDataChangeOrder input[name='jarName']").val(rowData.jarName);
                                                $("#fabricReturnDataChangeOrder input[name='unit']").val(rowData.unit);
                                                $("#fabricReturnDataChangeOrder input[name='weight']").val(rowData.weight);
                                                $("#fabricReturnDataChangeOrder input[name='batchNumber']").val(rowData.batchNumber);
                                                $("#fabricReturnDataChangeOrder input[name='location']").val(rowData.location);
                                                $("#fabricReturnDataChangeOrder input[name='returnTime']").val(moment(rowData.returnTime).format("YYYY-MM-DD"));
                                                $("#fabricReturnDataChangeOrder input[name='supplier']").val(rowData.supplier);
                                                form.render('select');
                                            }
                                        }
                                        ,done: function () {
                                            soulTable.render(this);
                                        }
                                        ,filter: {
                                            bottom: false,
                                            items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                                        }
                                    },
                                    {
                                        title: '库存记录'
                                        ,url: '/erp/getfabricstoragebyinfo'
                                        ,where: function(row){
                                            return {
                                                fabricID: row.fabricID
                                            }
                                        }
                                        ,loading:true
                                        ,page: false
                                        ,totalRow: true
                                        ,toolbar: '<div><a class="layui-btn layui-btn-normal layui-btn-sm" lay-event="batchDelete">批量出库</a></div>'
                                        ,cols: [[
                                            {type: 'checkbox', fixed: 'left'},
                                            {field: 'fabricName', title: '面料名称',align:'center', minWidth: 200, totalRowText: '合计'},
                                            {field: 'fabricNumber', title: '面料号',align:'center', minWidth: 100 },
                                            {field: 'supplier', title: '供应商',align:'center', minWidth: 100 },
                                            {field: 'colorName', title: '订单颜色',align:'center', minWidth: 100 },
                                            {field: 'fabricColor', title: '面料颜色',align:'center', minWidth: 100},
                                            {field: 'isHit', title: '是否撞色',align:'center', minWidth: 100},
                                            {field: 'fabricColorNumber', title: '面料色号',align:'center', minWidth: 100},
                                            {field: 'jarName', title: '缸号',align:'center', minWidth: 100},
                                            {field: 'unit', title: '单位',align:'center', minWidth: 80},
                                            {field: 'batchNumber', title: '卷数',align:'center', minWidth: 100, totalRow: true},
                                            {field: 'weight', title: '数量',align:'center', minWidth: 100, totalRow: true},
                                            {field: 'location', title: '位置',align:'center', minWidth: 100},
                                            {field: 'returnTime', title: '入库时间',align:'center', minWidth: 120,templet:function (d) {
                                                    return moment(d.returnTime).format("YYYY-MM-DD");
                                                }},
                                            {field: 'operateType', title: '入库类型',align:'center', minWidth: 100},
                                            {field: 'fabricStorageID', align:'center', hide:true},
                                            {field: 'orderName', align:'center', hide:true},
                                            {field: 'clothesVersionNumber', align:'center', hide:true},
                                            {field: 'fabricID',align:'center', hide: true},
                                            {title: '操作', minWidth: 250,align:'center', templet: function(row) {
                                                    return '<a class="layui-btn layui-btn-xs" lay-event="fabricStorageNormalOutStore">出库</a><a class="layui-btn layui-btn-xs layui-btn-warm" lay-event="fabricStorageUnNormalOutStore">异常出库</a><a class="layui-btn layui-btn-xs" lay-event="changeFabricStorage">调库</a>';
                                                }}
                                        ]]
                                        ,toolEvent: function (obj, pobj) {
                                            // obj 子表当前行对象
                                            // pobj 父表当前行对象
                                            var rowData = obj.data;
                                            var childId = this.id; // 通过 this 对象获取当前子表的id
                                            if (obj.event === 'fabricStorageNormalOutStore'){
                                                var param = {};
                                                param.fabricName = rowData.fabricName;
                                                param.fabricNumber = rowData.fabricNumber;
                                                param.supplier = rowData.supplier;
                                                param.colorName = rowData.colorName;
                                                param.fabricColor = rowData.fabricColor;
                                                param.isHit = rowData.isHit;
                                                param.fabricColorNumber = rowData.fabricColorNumber;
                                                param.jarName = rowData.jarName;
                                                param.unit = rowData.unit;
                                                param.batchNumber = rowData.batchNumber;
                                                param.weight = rowData.weight;
                                                param.location = rowData.location;
                                                param.returnTime = moment(rowData.returnTime).format("YYYY-MM-DD");
                                                param.fabricStorageID = rowData.fabricStorageID;
                                                param.orderName = rowData.orderName;
                                                param.clothesVersionNumber = rowData.clothesVersionNumber;
                                                var initWeight = rowData.weight;
                                                var initBatch = rowData.batchNumber;
                                                param.fabricID = rowData.fabricID;
                                                param.fabricOutRecordID = param.fabricStorageID;
                                                param.operateType = "出库";
                                                var index = layer.open({
                                                    type: 1 //Page层类型
                                                    , title: '面料出库'
                                                    , btn: ['保存']
                                                    , shade: 0.6 //遮罩透明度
                                                    , maxmin: false //允许全屏最小化
                                                    , anim: 0 //0-6的动画形式，-1不开启
                                                    , area: ['300px', '240px']
                                                    , content: "<div><table><tr><td><label class='layui-form-label'>卷数</label></td><td><input type='text' id='normalBatchNumber' autocomplete='off' class='layui-input'></td></tr><tr><td><label class='layui-form-label'>数量</label></td><td><input type='text' id='normalWeight' autocomplete='off' class='layui-input'></td></tr><tr><td><label class='layui-form-label'>接收</label></td><td><input type='text' id='receiver1' autocomplete='off' class='layui-input'></td></tr></table></div>"
                                                    ,yes: function(index, layero){
                                                        var normalBatchNumber = $("#normalBatchNumber").val();
                                                        var normalWeight = $("#normalWeight").val();
                                                        var receiver = $("#receiver1").val();
                                                        param.batchNumber = normalBatchNumber;
                                                        param.weight = normalWeight;
                                                        param.receiver = receiver;
                                                        if (Number(normalBatchNumber) > Number(initBatch) || Number(normalWeight) > Number(initWeight)){
                                                            layer.msg("出库数量超过库存", {icon: 2});
                                                            return false;
                                                        }
                                                        if (receiver == null || receiver == ''){
                                                            layer.msg("接收不能为空", {icon: 2});
                                                            return false;
                                                        }
                                                        $.ajax({
                                                            url: "/erp/addunnormalfabricoutrecord",
                                                            type: 'POST',
                                                            data: {
                                                                fabricOutRecordJson: JSON.stringify(param),
                                                                initWeight: initWeight,
                                                                initBatch: initBatch
                                                            },
                                                            success: function (res) {
                                                                if (res.code == 0) {
                                                                    layer.close(index);
                                                                    layer.msg("出库成功！", {icon: 1});
                                                                    reloadTable();
                                                                    $("#normalBatchNumber").val("");
                                                                    $("#normalWeight").val("");
                                                                    $("#receiver").val("");
                                                                } else {
                                                                    layer.msg("出库失败！", {icon: 2});
                                                                }
                                                            },
                                                            error: function () {
                                                                layer.msg("保存失败！", {icon: 2});
                                                            }
                                                        })
                                                    }
                                                    , cancel : function (i,layero) {
                                                        $("#normalBatchNumber").val('');
                                                        $("#normalWeight").val('');
                                                        $("#receiver").val("");
                                                    }
                                                });
                                                $("#normalBatchNumber").val(rowData.batchNumber);
                                                $("#normalWeight").val(rowData.weight);
                                            }
                                            else if (obj.event === 'fabricStorageUnNormalOutStore'){
                                                var index = layer.open({
                                                    type: 1 //Page层类型
                                                    , title: '面料异常出库'
                                                    , btn: ['保存']
                                                    , shade: 0.6 //遮罩透明度
                                                    , maxmin: false //允许全屏最小化
                                                    , anim: 0 //0-6的动画形式，-1不开启
                                                    , content: "<div id=\"fabricUnNormalOutStore\" style=\"cursor:default;\" xmlns=\"http://www.w3.org/1999/html\">\n" +
                                                        "        <form class=\"layui-form\" style=\"margin: 5px;padding-top: 40px;\" lay-filter=\"unNormalOutStore\">\n" +
                                                        "            <div class=\"layui-row layui-col-space10\">\n" +
                                                        "                <div class=\"layui-col-md3\">\n" +
                                                        "                    <label class=\"layui-form-label\">面料名称</label>\n" +
                                                        "                    <div class=\"layui-input-inline\">\n" +
                                                        "                        <input type=\"text\" name=\"fabricName\" autocomplete=\"off\" class=\"layui-input\" readonly=\"true\">\n" +
                                                        "                    </div>\n" +
                                                        "                </div>\n" +
                                                        "                <div class=\"layui-col-md3\">\n" +
                                                        "                    <label class=\"layui-form-label\">面料号</label>\n" +
                                                        "                    <div class=\"layui-input-inline\">\n" +
                                                        "                        <input type=\"text\" name=\"fabricNumber\" autocomplete=\"off\" class=\"layui-input\" readonly=\"true\">\n" +
                                                        "                    </div>\n" +
                                                        "                </div>\n" +
                                                        "                <div class=\"layui-col-md3\">\n" +
                                                        "                    <label class=\"layui-form-label\">色组</label>\n" +
                                                        "                    <div class=\"layui-input-inline\">\n" +
                                                        "                        <input type=\"text\" name=\"colorName\" autocomplete=\"off\" class=\"layui-input\" readonly=\"true\">\n" +
                                                        "                    </div>\n" +
                                                        "                </div>\n" +
                                                        "                <div class=\"layui-col-md3\">\n" +
                                                        "                    <label class=\"layui-form-label\">是否撞色</label>\n" +
                                                        "                    <div class=\"layui-input-inline\">\n" +
                                                        "                        <input type=\"text\" name=\"isHit\" autocomplete=\"off\" class=\"layui-input\" readonly=\"true\">\n" +
                                                        "                    </div>\n" +
                                                        "                </div>\n" +
                                                        "            </div>\n" +
                                                        "            <div class=\"layui-row layui-col-space10\">\n" +
                                                        "                <div class=\"layui-col-md3\">\n" +
                                                        "                    <label class=\"layui-form-label\">面料颜色</label>\n" +
                                                        "                    <div class=\"layui-input-inline\">\n" +
                                                        "                        <input type=\"text\" name=\"fabricColor\" autocomplete=\"off\" class=\"layui-input\" readonly=\"true\">\n" +
                                                        "                    </div>\n" +
                                                        "                </div>\n" +
                                                        "                <div class=\"layui-col-md3\">\n" +
                                                        "                    <label class=\"layui-form-label\">面料色号</label>\n" +
                                                        "                    <div class=\"layui-input-inline\">\n" +
                                                        "                        <input type=\"text\" name=\"fabricColorNumber\" autocomplete=\"off\" class=\"layui-input\" readonly=\"true\">\n" +
                                                        "                    </div>\n" +
                                                        "                </div>\n" +
                                                        "                <div class=\"layui-col-md3\">\n" +
                                                        "                    <label class=\"layui-form-label\">供应商</label>\n" +
                                                        "                    <div class=\"layui-input-inline\">\n" +
                                                        "                        <input type=\"text\" name=\"supplier\" autocomplete=\"off\" class=\"layui-input\" readonly=\"true\">\n" +
                                                        "                    </div>\n" +
                                                        "                </div>\n" +
                                                        "                <div class=\"layui-col-md3\">\n" +
                                                        "                    <label class=\"layui-form-label\">单位</label>\n" +
                                                        "                    <div class=\"layui-input-inline\">\n" +
                                                        "                        <input type=\"text\" name=\"unit\" autocomplete=\"off\" class=\"layui-input\" readonly=\"true\">\n" +
                                                        "                    </div>\n" +
                                                        "                </div>\n" +
                                                        "            </div>\n" +
                                                        "            <div class=\"layui-row layui-col-space10\">\n" +
                                                        "                <div class=\"layui-col-md3\">\n" +
                                                        "                    <label class=\"layui-form-label\">缸号</label>\n" +
                                                        "                    <div class=\"layui-input-inline\">\n" +
                                                        "                        <input type=\"text\" name=\"jarName\" autocomplete=\"off\" class=\"layui-input\" readonly=\"true\">\n" +
                                                        "                    </div>\n" +
                                                        "                </div>\n" +
                                                        "                <div class=\"layui-col-md3\">\n" +
                                                        "                    <label class=\"layui-form-label\">卷数</label>\n" +
                                                        "                    <div class=\"layui-input-inline\">\n" +
                                                        "                        <input type=\"text\" name=\"batchNumber\" autocomplete=\"off\" class=\"layui-input\">\n" +
                                                        "                    </div>\n" +
                                                        "                </div>\n" +
                                                        "                <div class=\"layui-col-md3\">\n" +
                                                        "                    <label class=\"layui-form-label\">重量</label>\n" +
                                                        "                    <div class=\"layui-input-inline\">\n" +
                                                        "                        <input type=\"text\" name=\"weight\" autocomplete=\"off\" class=\"layui-input\">\n" +
                                                        "                    </div>\n" +
                                                        "                </div>\n" +
                                                        "                <div class=\"layui-col-md3\">\n" +
                                                        "                    <label class=\"layui-form-label\">入库时间</label>\n" +
                                                        "                    <div class=\"layui-input-inline\">\n" +
                                                        "                        <input type=\"text\" name=\"returnTime\" id=\"updateStorageTime\" autocomplete=\"off\" class=\"layui-input\" readonly=\"true\">\n" +
                                                        "                    </div>\n" +
                                                        "                </div>\n" +
                                                        "            </div>\n" +
                                                        "            <div class=\"layui-row layui-col-space10\">\n" +
                                                        "                <div class=\"layui-col-md3\">\n" +
                                                        "                    <label class=\"layui-form-label\">位置</label>\n" +
                                                        "                    <div class=\"layui-input-inline\">\n" +
                                                        "                        <input type=\"text\" name=\"location\" autocomplete=\"off\" class=\"layui-input\" readonly=\"true\">\n" +
                                                        "                    </div>\n" +
                                                        "                </div>\n" +
                                                        "                <div class=\"layui-col-md3\">\n" +
                                                        "                    <label class=\"layui-form-label\">类型</label>\n" +
                                                        "                    <div class=\"layui-input-inline\">\n" +
                                                        "                        <select type=\"text\" name=\"operateType\" autocomplete=\"off\" class=\"layui-input\">\n" +
                                                        "                            <option value=\"\">出库类型</option>\n" +
                                                        "                            <option value=\"外发加工\">外发加工</option>\n" +
                                                        "                            <option value=\"退面料厂\">退面料厂</option>\n" +
                                                        "                            <option value=\"余布出售\">余布出售</option>\n" +
                                                        "                        </select>\n" +
                                                        "                    </div>\n" +
                                                        "                </div>\n" +
                                                        "                <div class=\"layui-col-md3\">\n" +
                                                        "                    <label class=\"layui-form-label\">接收</label>\n" +
                                                        "                    <div class=\"layui-input-inline\">\n" +
                                                        "                        <input type=\"text\" name=\"receiver\" autocomplete=\"off\" class=\"layui-input\">\n" +
                                                        "                    </div>\n" +
                                                        "                </div>\n" +
                                                        "                <div class=\"layui-col-md3\"></div>\n" +
                                                        "            </div>\n" +
                                                        "        </form>\n" +
                                                        "    </div>"
                                                    ,yes: function(index, layero){
                                                        var params = layui.form.val("unNormalOutStore");
                                                        params.fabricStorageID = rowData.fabricStorageID;
                                                        params.orderName = rowData.orderName;
                                                        params.fabricID = rowData.fabricID;
                                                        params.clothesVersionNumber = rowData.clothesVersionNumber;
                                                        var initWeight = rowData.weight;
                                                        var initBatch = rowData.batchNumber;
                                                        params.fabricOutRecordID = params.fabricStorageID;
                                                        if (Number(params.batchNumber) > Number(initBatch) || Number(params.weight) > Number(initWeight)){
                                                            layer.msg("出库数量超过库存", {icon: 2});
                                                        }
                                                        if (params.operateType == null || params.operateType == ""){
                                                            layer.msg("请选择出库类型", {icon: 2});
                                                            return false;
                                                        }
                                                        if (params.receiver == null || params.receiver == ""){
                                                            layer.msg("请输入接收", {icon: 2});
                                                        }
                                                        $.ajax({
                                                            url: "/erp/addunnormalfabricoutrecord",
                                                            type: 'POST',
                                                            data: {
                                                                fabricOutRecordJson: JSON.stringify(params),
                                                                initWeight: initWeight,
                                                                initBatch: initBatch
                                                            },
                                                            success: function (res) {
                                                                if (res.code == 0) {
                                                                    layer.close(index);
                                                                    layer.msg("出库成功！", {icon: 1});
                                                                    reloadTable();
                                                                    $("#fabricUnNormalOutStore").find("input").val("");
                                                                } else {
                                                                    layer.msg("出库失败！", {icon: 2});
                                                                }
                                                            },
                                                            error: function () {
                                                                layer.msg("保存失败！", {icon: 2});
                                                            }
                                                        })
                                                    }
                                                    , cancel : function (i,layero) {
                                                        $("#fabricUnNormalOutStore").find("input").val("");
                                                    }
                                                });
                                                layer.full(index);
                                                $("#fabricUnNormalOutStore input[name='fabricName']").val(rowData.fabricName);
                                                $("#fabricUnNormalOutStore input[name='fabricNumber']").val(rowData.fabricNumber);
                                                $("#fabricUnNormalOutStore input[name='supplier']").val(rowData.supplier);
                                                $("#fabricUnNormalOutStore input[name='colorName']").val(rowData.colorName);
                                                $("#fabricUnNormalOutStore input[name='fabricColor']").val(rowData.fabricColor);
                                                $("#fabricUnNormalOutStore input[name='isHit']").val(rowData.isHit);
                                                $("#fabricUnNormalOutStore input[name='fabricColorNumber']").val(rowData.fabricColorNumber);
                                                $("#fabricUnNormalOutStore input[name='jarName']").val(rowData.jarName);
                                                $("#fabricUnNormalOutStore input[name='unit']").val(rowData.unit);
                                                $("#fabricUnNormalOutStore input[name='batchNumber']").val(rowData.batchNumber);
                                                $("#fabricUnNormalOutStore input[name='weight']").val(rowData.weight);
                                                $("#fabricUnNormalOutStore input[name='location']").val(rowData.location);
                                                $("#fabricUnNormalOutStore input[name='returnTime']").val(moment(rowData.returnTime).format("YYYY-MM-DD"));
                                                form.render('select');
                                            }
                                            else if (obj.event === 'changeFabricStorage'){
                                                var index = layer.open({
                                                    type: 1 //Page层类型
                                                    , title: '面料调库'
                                                    , btn: ['保存']
                                                    , shade: 0.6 //遮罩透明度
                                                    ,area: ['320px', '180px'] //宽高
                                                    , anim: 0 //0-6的动画形式，-1不开启
                                                    , content: '<div class="layui-col-md3" style="width: 300px; padding-top: 30px">\n' +
                                                        '                    <!-- 填充内容 -->\n' +
                                                        '                    <label class="layui-form-label">调换到</label>\n' +
                                                        '                    <div class="layui-input-inline">\n' +
                                                        '                        <input id="destinationLocation" style="width: 200px;" class="layui-input">' +
                                                        '                    </div>\n' +
                                                        '                </div>'
                                                    ,success: function(layero, index){
                                                        $("#destinationLocation").val('');
                                                    }
                                                    ,yes: function(index, layero){
                                                        var destLocation = $("#destinationLocation").val();
                                                        $.ajax({
                                                            url: "/erp/updatefabricstorage",
                                                            type: 'POST',
                                                            data: {
                                                                fabricStorageID: rowData.fabricStorageID,
                                                                location: destLocation
                                                            },
                                                            success: function (res) {
                                                                if (res == 0) {
                                                                    layer.close(index);
                                                                    layer.msg("保存成功！", {icon: 1});
                                                                    reloadTable();
                                                                } else {
                                                                    layer.msg("保存失败！", {icon: 2});
                                                                }
                                                            },
                                                            error: function () {
                                                                layer.msg("保存失败！", {icon: 2});
                                                            }
                                                        })
                                                    }, cancel : function (i,layero) {

                                                    }
                                                });
                                                form.render('select');
                                            }
                                        }
                                        ,toolbarEvent: function (obj, pobj) {
                                            // obj 子表当前行对象
                                            // pobj 父表当前行对象
                                            var objData = obj.data;
                                            var childId = this.id;
                                            var fabricID;
                                            if (obj.event === 'batchDelete') {
                                                var checkStatus = table.checkStatus(obj.config.id);
                                                if(checkStatus.data.length == 0) {
                                                    layer.msg('请选择数据');
                                                }else {
                                                    var fabricStorageIDList = [];
                                                    $.each(checkStatus.data, function (index, item) {
                                                        fabricStorageIDList.push(item.fabricStorageID);
                                                        fabricID = item.fabricID;
                                                    });
                                                    var index = layer.open({
                                                        type: 1 //Page层类型
                                                        , title: '面料出库'
                                                        , btn: ['保存']
                                                        , shade: 0.6 //遮罩透明度
                                                        , maxmin: false //允许全屏最小化
                                                        , anim: 0 //0-6的动画形式，-1不开启
                                                        , area: ['300px', '150px']
                                                        , content: "<div><table><tr><td><label class='layui-form-label'>接收:</label></td><td><input type='text' id='receiver2' autocomplete='off' class='layui-input'></td></tr></table></div>"
                                                        ,yes: function(index, layero){
                                                            var receiver = $("#receiver2").val();
                                                            $.ajax({
                                                                url: "/erp/fabricbatchoutstore",
                                                                type: 'POST',
                                                                data: {
                                                                    fabricStorageIDList: fabricStorageIDList,
                                                                    receiver: receiver,
                                                                    fabricID: fabricID
                                                                },
                                                                traditional: true,
                                                                success: function (res) {
                                                                    if (res.result == 0) {
                                                                        layer.close(index);
                                                                        layer.msg("出库成功！", {icon: 1});
                                                                        table.reload(childId);
                                                                        $("#receiver").val('');
                                                                    } else {
                                                                        layer.msg("出库失败！", {icon: 2});
                                                                    }
                                                                }, error: function () {
                                                                    layer.msg("出库失败！", {icon: 2});
                                                                }
                                                            })
                                                        }
                                                        , cancel : function (i,layero) {
                                                            $("#receiver").val('');
                                                        }
                                                    });
                                                }
                                            }
                                        }
                                        ,done: function () {
                                            soulTable.render(this);
                                        }
                                        ,filter: {
                                            bottom: false,
                                            items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                                        }
                                    },
                                    {
                                        title: '出库记录'
                                        ,url: '/erp/getfabricoutrecordbyinfo'
                                        ,where: function(row){
                                            return {
                                                fabricID: row.fabricID
                                            }
                                        }
                                        ,loading:true
                                        ,page: false
                                        ,totalRow: true
                                        ,cols: [[
                                            {field: 'fabricName', title: '面料名称',align:'center', minWidth: 200, totalRowText: '合计'},
                                            {field: 'colorName', title: '订单颜色',align:'center', minWidth: 100 },
                                            {field: 'fabricColor', title: '面料颜色',align:'center', minWidth: 100},
                                            {field: 'jarName', title: '缸号',align:'center', minWidth: 100},
                                            {field: 'unit', title: '单位',align:'center', minWidth: 80},
                                            {field: 'batchNumber', title: '卷数',align:'center', minWidth: 100, totalRow: true},
                                            {field: 'weight', title: '数量',align:'center', minWidth: 100, totalRow: true},
                                            {field: 'location', title: '位置',align:'center', minWidth: 100},
                                            {field: 'createTime', title: '出库时间',align:'center', minWidth: 120,templet:function (d) {
                                                    return moment(d.createTime).format("YYYY-MM-DD");
                                                }},
                                            {field: 'operateType', title: '出库类型',align:'center', minWidth: 150},
                                            {field: 'receiver', title: '接收',align:'center', minWidth: 100},
                                            {field: 'fabricID',align:'center', hide: true},
                                            {field: 'fabricOutRecordID', title: '操作', minWidth: 180,align:'center', templet: function(row) {
                                                    return '<a class="layui-btn layui-btn-xs layui-btn-danger" lay-event="deleteFabricOutRecord">删除</a><a class="layui-btn layui-btn-xs" lay-event="returnFabricReturnData">退库</a>';
                                                }}
                                        ]]
                                        ,toolEvent: function (obj, pobj) {
                                            // obj 子表当前行对象
                                            // pobj 父表当前行对象
                                            var rowData = obj.data;
                                            var childId = this.id; // 通过 this 对象获取当前子表的id
                                            if (obj.event === 'deleteFabricOutRecord'){
                                                layer.confirm('真的删除吗', function(index){
                                                    $.ajax({
                                                        url: "/erp/deletefabricoutrecord",
                                                        type: 'POST',
                                                        data: {
                                                            fabricOutRecordID: rowData.fabricOutRecordID
                                                        },
                                                        success: function (res) {
                                                            if (res == 0) {
                                                                layer.close(index);
                                                                layer.msg("删除成功！", {icon: 1});
                                                                reloadTable();
                                                            } else {
                                                                layer.msg("删除失败！", {icon: 2});
                                                            }
                                                        },
                                                        error: function () {
                                                            layer.msg("删除失败！", {icon: 2});
                                                        }
                                                    })
                                                });
                                            }
                                            else if (obj.event === 'returnFabricReturnData'){
                                                var index = layer.open({
                                                    type: 1 //Page层类型
                                                    , title: '余布退库'
                                                    , btn: ['保存']
                                                    , shade: 0.6 //遮罩透明度
                                                    , maxmin: false //允许全屏最小化
                                                    , anim: 0 //0-6的动画形式，-1不开启
                                                    , content: "<div id=\"fabricInStoreReturn\" style=\"cursor:default;\">\n" +
                                                        "        <form class=\"layui-form\" style=\"margin: 5px;padding-top: 40px;\" lay-filter=\"inStoreReturn\">\n" +
                                                        "            <div class=\"layui-row layui-col-space10\">\n" +
                                                        "                <div class=\"layui-col-md3\">\n" +
                                                        "                    <label class=\"layui-form-label\">面料名称</label>\n" +
                                                        "                    <div class=\"layui-input-inline\">\n" +
                                                        "                        <input type=\"text\" name=\"fabricName\" autocomplete=\"off\" class=\"layui-input\" readonly=\"true\">\n" +
                                                        "                    </div>\n" +
                                                        "                </div>\n" +
                                                        "                <div class=\"layui-col-md3\">\n" +
                                                        "                    <label class=\"layui-form-label\">色组</label>\n" +
                                                        "                    <div class=\"layui-input-inline\">\n" +
                                                        "                        <input type=\"text\" name=\"colorName\" autocomplete=\"off\" class=\"layui-input\" readonly=\"true\">\n" +
                                                        "                    </div>\n" +
                                                        "                </div>\n" +
                                                        "                <div class=\"layui-col-md3\">\n" +
                                                        "                    <label class=\"layui-form-label\">面料颜色</label>\n" +
                                                        "                    <div class=\"layui-input-inline\">\n" +
                                                        "                        <input type=\"text\" name=\"fabricColor\" autocomplete=\"off\" class=\"layui-input\" readonly=\"true\">\n" +
                                                        "                    </div>\n" +
                                                        "                </div>\n" +
                                                        "                <div class=\"layui-col-md3\">\n" +
                                                        "                    <label class=\"layui-form-label\">单位</label>\n" +
                                                        "                    <div class=\"layui-input-inline\">\n" +
                                                        "                        <input type=\"text\" name=\"unit\" autocomplete=\"off\" class=\"layui-input\" readonly=\"true\">\n" +
                                                        "                    </div>\n" +
                                                        "                </div>\n" +
                                                        "            </div>\n" +
                                                        "            <div class=\"layui-row layui-col-space10\">\n" +
                                                        "                <div class=\"layui-col-md3\">\n" +
                                                        "                    <label class=\"layui-form-label\">缸号</label>\n" +
                                                        "                    <div class=\"layui-input-inline\">\n" +
                                                        "                        <input type=\"text\" name=\"jarName\" autocomplete=\"off\" class=\"layui-input\">\n" +
                                                        "                    </div>\n" +
                                                        "                </div>\n" +
                                                        "                <div class=\"layui-col-md3\">\n" +
                                                        "                    <label class=\"layui-form-label\">卷数</label>\n" +
                                                        "                    <div class=\"layui-input-inline\">\n" +
                                                        "                        <input type=\"text\" name=\"batchNumber\" autocomplete=\"off\" class=\"layui-input\">\n" +
                                                        "                    </div>\n" +
                                                        "                </div>\n" +
                                                        "                <div class=\"layui-col-md3\">\n" +
                                                        "                    <label class=\"layui-form-label\">重量</label>\n" +
                                                        "                    <div class=\"layui-input-inline\">\n" +
                                                        "                        <input type=\"text\" name=\"weight\" autocomplete=\"off\" class=\"layui-input\">\n" +
                                                        "                    </div>\n" +
                                                        "                </div>\n" +
                                                        "                <div class=\"layui-col-md3\">\n" +
                                                        "                    <label class=\"layui-form-label\">收货</label>\n" +
                                                        "                    <div class=\"layui-input-inline\">\n" +
                                                        "                        <input type=\"text\" name=\"receiver\" id=\"receiver\" autocomplete=\"off\" class=\"layui-input\">\n" +
                                                        "                    </div>\n" +
                                                        "                </div>\n" +
                                                        "            </div>\n" +
                                                        "            <div class=\"layui-row layui-col-space10\">\n" +
                                                        "                <div class=\"layui-col-md3\">\n" +
                                                        "                    <!-- 填充内容 -->\n" +
                                                        "                    <label class=\"layui-form-label\">位置</label>\n" +
                                                        "                    <div class=\"layui-input-inline\">\n" +
                                                        "                        <input type=\"text\" name=\"newLocation\" autocomplete=\"off\" class=\"layui-input\">\n" +
                                                        "                    </div>\n" +
                                                        "                </div>\n" +
                                                        "            </div>\n" +
                                                        "        </form>\n" +
                                                        "    </div>"
                                                    ,yes: function(index, layero){
                                                        var params = layui.form.val("inStoreReturn");
                                                        params.operateType = '余布入仓';
                                                        params.location = rowData.location;
                                                        params.orderName = rowData.orderName;
                                                        params.fabricID = rowData.fabricID;
                                                        params.fabricOutRecordID = rowData.fabricOutRecordID;
                                                        params.clothesVersionNumber = rowData.clothesVersionNumber;
                                                        var newLocation = rowData.location
                                                        if (params.newLocation == null || params.newLocation === ''){
                                                            newLocation = params.newLocation
                                                        }
                                                        $.ajax({
                                                            url: "/erp/returnfabricreturn",
                                                            type: 'POST',
                                                            data: {
                                                                fabricOutRecordJson: JSON.stringify(params),
                                                                newLocation: newLocation
                                                            },
                                                            success: function (res) {
                                                                if (res == 0) {
                                                                    layer.close(index);
                                                                    layer.msg("保存成功！", {icon: 1});
                                                                    reloadTable();
                                                                    $("#fabricInStoreReturn").find("input").val("");
                                                                } else {
                                                                    layer.msg("保存失败！", {icon: 2});
                                                                }
                                                            },
                                                            error: function () {
                                                                layer.msg("保存失败！", {icon: 2});
                                                            }
                                                        })
                                                    }
                                                    , cancel : function (i,layero) {
                                                        $("#fabricInStoreReturn").find("input").val("");
                                                    }
                                                });
                                                layer.full(index);
                                                $("#fabricInStoreReturn input[name='fabricName']").val(rowData.fabricName);
                                                $("#fabricInStoreReturn input[name='colorName']").val(rowData.colorName);
                                                $("#fabricInStoreReturn input[name='fabricColor']").val(rowData.fabricColor);
                                                $("#fabricInStoreReturn input[name='jarName']").val(rowData.jarName);
                                                $("#fabricInStoreReturn input[name='unit']").val(rowData.unit);
                                                $("#fabricInStoreReturn input[name='weight']").val('');
                                                $("#fabricInStoreReturn input[name='batchNumber']").val('');
                                                $("#fabricInStoreReturn input[name='location']").val(rowData.location);
                                                $("#fabricInStoreReturn input[name='newLocation']").val(rowData.location);
                                                $("#fabricInStoreReturn input[name='receiver']").val("布仓");
                                                form.render('select');
                                            }
                                        }
                                        ,done: function () {
                                            soulTable.render(this);
                                        }
                                        ,filter: {
                                            bottom: false,
                                            items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                                        }
                                    },
                                    {
                                        title: '暂存记录'
                                        ,url: '/erp/getfabricdetailtmpbyid'
                                        ,where: function(row){
                                            return {
                                                fabricID: row.fabricID
                                            }
                                        }
                                        ,loading:true
                                        ,page: false
                                        ,totalRow: true
                                        ,cols: [[
                                            {field: 'fabricName', title: '面料名称',align:'center', minWidth: 200, totalRowText: '合计'},
                                            {field: 'fabricNumber', title: '面料号',align:'center', minWidth: 100 },
                                            {field: 'colorName', title: '订单颜色',align:'center', minWidth: 100 },
                                            {field: 'isHit', title: '是否撞色',align:'center', minWidth: 100 },
                                            {field: 'fabricColor', title: '面料颜色',align:'center', minWidth: 100},
                                            {field: 'fabricColorNumber', title: '面料色号',align:'center', minWidth: 100},
                                            {field: 'jarName', title: '缸号',align:'center', minWidth: 100},
                                            {field: 'unit', title: '单位',align:'center', minWidth: 80},
                                            {field: 'batchNumber', title: '卷数',align:'center', minWidth: 100, totalRow: true},
                                            {field: 'weight', title: '数量',align:'center', minWidth: 100, totalRow: true},
                                            {field: 'location', title: '位置',align:'center', minWidth: 100},
                                            {field: 'returnTime', title: '入库时间',align:'center', minWidth: 120,templet:function (d) {
                                                    return moment(d.returnTime).format("YYYY-MM-DD");
                                                }},
                                            {field: 'supplier', title: '供应商',align:'center', minWidth: 100},
                                            {field: 'fabricDetailID', align:'center', hide:true},
                                            {field: 'orderName', align:'center', hide:true},
                                            {field: 'clothesVersionNumber', align:'center', hide:true},
                                            {field: 'fabricID', align:'center', hide:true},
                                            {title: '操作', minWidth: 250,align:'center', templet: function(row) {
                                                    return '<a class="layui-btn layui-btn-xs layui-btn-danger" lay-event="deleteFabricReturnData">删除</a>';
                                                }}
                                        ]]
                                        ,toolEvent: function (obj, pobj) {
                                            // obj 子表当前行对象
                                            // pobj 父表当前行对象
                                            var rowData = obj.data;
                                            var childId = this.id; // 通过 this 对象获取当前子表的id
                                            if (obj.event === 'deleteFabricReturnData'){
                                                layer.confirm('真的删除吗', function(index){
                                                    $.ajax({
                                                        url: "/erp/deletefabricdetailtmpbyid",
                                                        type: 'POST',
                                                        data: {
                                                            fabricDetailID: rowData.fabricDetailID
                                                        },
                                                        success: function (res) {
                                                            if (res.result == 0) {
                                                                layer.close(index);
                                                                layer.msg("删除成功！", {icon: 1});
                                                                reloadTable();
                                                            } else {
                                                                layer.msg("删除失败！", {icon: 2});
                                                            }
                                                        },
                                                        error: function () {
                                                            layer.msg("删除失败！", {icon: 2});
                                                        }
                                                    })
                                                });
                                            }
                                        }
                                        ,done: function () {
                                            soulTable.render(this);
                                        }
                                        ,filter: {
                                            bottom: false,
                                            items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                                        }
                                    }]}
                            ,{type:'radio'}
                            ,{type:'numbers', align:'center', title:'序号'}
                            ,{field:'checkNumber', title:'合同号', align:'center', width:120, sort: true, filter: true}
                            ,{field:'orderName', title:'款号', align:'center', width:150, sort: true, filter: true}
                            ,{field:'clothesVersionNumber', title:'版单号', align:'center', width:150, sort: true, filter: true, totalRowText: '合计'}
                            ,{field:'fabricName', title:'面料名称', align:'center', width:200, sort: true, filter: true}
                            ,{field:'fabricNumber', title:'面料号', align:'center', width:200, sort: true, filter: true}
                            ,{field:'fabricWidth', title:'门幅', align:'center', width:80, sort: true, filter: true}
                            ,{field:'fabricWeight', title:'克重', align:'center',width:80, sort: true, filter: true}
                            ,{field:'unit', title:'单位', align:'center', width:80, sort: true, filter: true}
                            ,{field:'partName', title:'部位', align:'center', width:90, sort: true, filter: true}
                            ,{field:'colorName', title:'色组', align:'center', width:90, sort: true, filter: true}
                            ,{field:'isHit', title:'是否撞色', align:'center', width:100, sort: true, filter: true}
                            ,{field:'fabricColor', title:'面料颜色', align:'center', width:100, sort: true, filter: true}
                            ,{field:'fabricColorNumber', title:'面料色号', align:'center', width:100, sort: true, filter: true}
                            ,{field:'supplier', title:'供应商', align:'center', width:80, sort: true, filter: true}
                            ,{field:'orderCount', title:'订单量', align:'center', width:100, sort: true, filter: true}
                            ,{field:'orderPieceUsage', title:'接单用量', align:'center', width:100, sort: true, filter: true}
                            ,{field:'pieceUsage', title:'单件用量', align:'center', width:100, sort: true, filter: true}
                            ,{field:'fabricAdd', title:'上浮', align:'center', width:100, sort: true, filter: true}
                            ,{field:'fabricActCount', title:'订布量', align:'center', width:120, sort: true, filter: true, totalRow: true}
                            ,{field:'price', title:'单价', align:'center', width:120, sort: true, filter: true}
                            ,{field:'sumMoney', title:'总价', align:'center', width:120, sort: true, filter: true, totalRow: true}
                            ,{field:'remark', title:'备注', align:'center', width:120, sort: true, filter: true, totalRow: true}
                            ,{field:'fabricID',hide:true}
                        ]]
                        ,excel: {
                            filename: '面料总表.xlsx'
                        }
                        ,data: reportData
                        ,height: 'full-100'
                        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                        ,title: '面料总表'
                        ,totalRow: true
                        ,loading: false
                        ,page: true
                        ,even: true
                        ,limits: [50, 100, 200]
                        ,limit: 100 //每页默认显示的数量
                        ,done: function () {
                            soulTable.render(this);
                        }
                    });

                } else {
                    layer.msg("获取失败！", {icon: 2});
                }
            }, error: function () {
                layer.msg("获取失败！", {icon: 2});
            }
        });
    }

    function reloadTable(){
        var orderName = $("#orderName").val();
        var type = $("#fabricType").val();
        initTable(orderName, type);
        return false;
    }

    form.on('submit(searchBeat)', function(data){
        reloadTable();
        return false;
    });
    form.on('submit(searchNumberBeat)', function(data){
        var checkNumber = $("#checkNumber").val();
        if (checkNumber == null || checkNumber == ''){
            layer.msg("合同号不能为空~~~");
            return false;
        }
        initCheckTable(checkNumber);
        return false;
    });
    table.on('tool(fabricReturnTable)', function(obj) {
        var data = obj.data;
        var orderName = data.orderName;
        var wellCount = data.wellCount;
        var clothesVersionNumber = data.clothesVersionNumber;
        if (obj.event === "addBjReturn") {   //面料下单
            var label = "扁机入库&emsp;&emsp;&emsp;&emsp;" + "单号：" + clothesVersionNumber + "&emsp;&emsp;&emsp;&emsp;款号：" + orderName + "&emsp;&emsp;&emsp;&emsp;好片数：" + wellCount;
            var index = layer.open({
                type: 1 //Page层类型
                , title: label
                , btn: ['保存','填充时间','填充位置']
                , shade: 0.6 //遮罩透明度
                , area: '1000px'
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , content: "<div id=\"accessoryReturnAdd\">\n" +
                    "        <form class=\"layui-form\" style=\"margin: 0 auto;max-width:100%; min-height:400px; padding-top: 0px;\">\n" +
                    "            <table class=\"layui-hide\" id=\"accessoryReturnAddTable\" lay-filter=\"accessoryReturnAddTable\"></table>\n" +
                    "        </form>\n" +
                    "    </div>"
                , yes: function (i, layero) {
                    var accessoryReturnData = table.cache.accessoryReturnAddTable;
                    var accessoryDetails = [];
                    $.each(accessoryReturnData, function (i, item) {
                        if (item.inStoreCount != null && item.inStoreCount !== ''){
                            accessoryDetails.push({
                                orderName:orderName,
                                clothesVersionNumber:clothesVersionNumber,
                                accessoryName:item.accessoryName,
                                accessoryNumber:item.accessoryNumber,
                                specification:item.specification,
                                accessoryColor:item.accessoryColor,
                                sizeName:item.sizeName,
                                colorName:item.colorName,
                                supplier:item.supplier,
                                pieceUsage:item.pieceUsage,
                                accessoryUnit:item.accessoryUnit,
                                inStoreCount: item.inStoreCount === '' ? 0 : item.inStoreCount,
                                storageCount: item.inStoreCount === '' ? 0 : item.inStoreCount,
                                inStoreTime:item.inStoreTime,
                                accessoryID:item.accessoryID,
                                colorNumber:item.colorNumber,
                                accessoryLocation:item.accessoryLocation,
                                price:item.price,
                                inStoreType: "分款",
                                accessoryType: "扁机"
                            })
                        }
                    });
                    if (accessoryDetails.length == 0) {
                        layer.msg("请填写完整信息", {icon: 2});
                    }else {
                        $.ajax({
                            url: "/erp/accessoryinstorebatch",
                            type: 'POST',
                            data: {
                                accessoryInStoreJson: JSON.stringify(accessoryDetails)
                            },
                            success: function (res) {
                                if (res.result == 0) {
                                    reloadTable();
                                    layer.close(index);
                                    layer.msg("录入成功！", {icon: 1});
                                } else if (res.result == 3) {
                                    layer.msg(res.msg);
                                } else {
                                    layer.msg("录入失败！", {icon: 2});
                                }
                            },
                            error: function () {
                                layer.msg("录入失败！", {icon: 2});
                            }
                        })
                    }
                }
                , btn2: function () {
                    var tmpData = table.cache.accessoryReturnAddTable;
                    var tmpTime;
                    $.each(tmpData,function(index1,value1){
                        if (value1.inStoreTime != null && value1.inStoreTime != ''){
                            tmpTime = value1.inStoreTime;
                        }
                    });
                    $.each(tmpData,function(index1,value1){
                        if (value1.inStoreCount != null && value1.inStoreCount != ''){
                            value1.inStoreTime = tmpTime;
                        }
                    });
                    table.reload("accessoryReturnAddTable",{
                        data:tmpData   // 将新数据重新载入表格
                    });
                    return false  //开启该代码可禁止点击该按钮关闭
                }
                , btn3: function () {
                    var tmpData = table.cache.accessoryReturnAddTable;
                    var tmpLocation;
                    $.each(tmpData,function(index1,value1){
                        if (value1.accessoryLocation != null && value1.accessoryLocation != ''){
                            tmpLocation = value1.accessoryLocation;
                        }
                    });
                    $.each(tmpData,function(index1,value1){
                        if (value1.inStoreCount != null && value1.inStoreCount != ''){
                            value1.accessoryLocation = tmpLocation;
                        }
                    });
                    table.reload("accessoryReturnAddTable",{
                        data:tmpData   // 将新数据重新载入表格
                    });
                    return false  //开启该代码可禁止点击该按钮关闭
                }
            });
            layer.full(index);
            setTimeout(function () {
                $.ajax({
                    url: "/erp/getbjbyorderforinstore",
                    type: 'GET',
                    data: {
                        orderName: orderName
                    },
                    success: function (res) {
                        var title = [
                            {field: 'accessoryName', title: '辅料名称', align: 'center', minWidth: 150},
                            {field: 'accessoryNumber',hide: true},
                            {field: 'specification', title: '规格', align: 'center', minWidth: 80},
                            {field: 'accessoryUnit', title: '单位', align: 'center', minWidth: 80},
                            {field: 'accessoryColor', title: '辅料颜色', align: 'center', minWidth: 100},
                            {field: 'sizeName', title: '尺码', align: 'center', minWidth: 100},
                            {field: 'colorName', title: '颜色', align: 'center', minWidth: 100},
                            {field: 'supplier', title: '供应商', align: 'center', minWidth: 80},
                            {field: 'pieceUsage', title: '单件用量', align: 'center', minWidth: 90},
                            {field: 'orderCount', title: '订单量', align: 'center', minWidth: 90},
                            {field: 'accessoryCount', title: '订购量', align: 'center', minWidth: 90, totalRow: true},
                            {field: 'checkState', title: '审核', align: 'center', minWidth: 90},
                            {field: 'remark', title: '备注', align: 'center', minWidth: 60},
                            {field: 'inStoreCount', title: '入库数量', align: 'center', minWidth: 90,edit:'text'},
                            {field: 'inStoreTime', title: '时间', align: 'center', minWidth: 100,edit:'text',event:'date'},
                            {field: 'accessoryLocation', title: '仓位',align:'center',minWidth:100,edit:'text'},
                            {field: 'colorNumber', title: '色号',align:'center',minWidth:100,edit:'text'},
                            {field: 'accessoryID', hide:true},
                            {field: 'price', hide:true}
                        ];
                        var accessoryReturnData = [];
                        if (res.data){
                            $.each(res.data,function(index1,value1){
                                var thisAccessoryData = {};
                                thisAccessoryData.accessoryID = value1.accessoryID;
                                thisAccessoryData.accessoryName = value1.accessoryName;
                                thisAccessoryData.accessoryNumber = value1.accessoryNumber;
                                thisAccessoryData.specification = value1.specification;
                                thisAccessoryData.accessoryUnit = value1.accessoryUnit;
                                thisAccessoryData.accessoryColor = value1.accessoryColor;
                                thisAccessoryData.pieceUsage = value1.pieceUsage;
                                thisAccessoryData.orderCount = value1.orderCount;
                                thisAccessoryData.sizeName = value1.sizeName;
                                thisAccessoryData.colorName = value1.colorName;
                                thisAccessoryData.accessoryCount = value1.accessoryCount;
                                thisAccessoryData.supplier = value1.supplier;
                                thisAccessoryData.remark = value1.remark;
                                thisAccessoryData.checkState = value1.checkState;
                                thisAccessoryData.shareStorage = value1.shareStorage;
                                thisAccessoryData.price = value1.price;
                                thisAccessoryData.inStoreCount = '';
                                thisAccessoryData.inStoreTime = '';
                                thisAccessoryData.accessoryLocation = '';
                                thisAccessoryData.colorNumber = '';
                                accessoryReturnData.push(thisAccessoryData);
                            });
                        }
                        else{
                            layer.msg("未找到数据");
                        }
                        table.render({
                            elem: '#accessoryReturnAddTable'
                            , cols: [title]
                            , data: accessoryReturnData
                            , height: 'full-180'
                            , even: true
                            , overflow: 'tips'
                            , totalRow: true
                            , limit: Number.MAX_VALUE //每页默认显示的数量
                            , done: function (res) {
                                $(".layui-table-main  tr").each(function (index, val) {
                                    $($(".layui-table-fixed .layui-table-body tbody tr")[index]).height($(val).height());
                                });
                                soulTable.render(this);
                            }
                        });
                        //监听单元格编辑
                        table.on('tool(accessoryReturnAddTable)', function (obj) {
                            var data = obj.data;
                            var tableData = table.cache.accessoryReturnAddTable;
                            var that = this;
                            var field = $(that).data('field');
                            if(obj.event === 'date'){
                                laydate.render({
                                    elem: $(this).find('.layui-table-edit').get(0)//
                                    ,show: true //直接显示
                                    ,done: function(value, date){
                                        obj.update({
                                            [field] : value
                                        });
                                    }
                                });
                            }else if(obj.event === 'del'){
                                obj.del();
                                $.each(tableData,function (index,item) {
                                    if(item instanceof Array) {
                                        tableData.splice(index,1)
                                    }
                                });
                                table.reload("accessoryReturnAddTable",{
                                    data:tableData   // 将新数据重新载入表格
                                })
                            } else if(obj.event === 'add'){
                                tableData.push({
                                    "accessoryName": tableData[0].accessoryName
                                    ,"specification": tableData[0].specification
                                    ,"accessoryUnit": tableData[0].accessoryUnit
                                    ,"accessoryColor": tableData[0].accessoryColor
                                    ,"sizeName": tableData[0].sizeName
                                    ,"colorName": tableData[0].colorName
                                    ,"accessoryType": tableData[0].accessoryType
                                    ,"supplier": tableData[0].supplier
                                    ,"accessoryCount": tableData[0].accessoryCount
                                    ,"inStoreCount": ''
                                    ,"inStoreTime": ''
                                    ,"accessoryLocation": ''
                                    ,"colorNumber": ''

                                });
                                table.reload("accessoryReturnAddTable",{
                                    data:tableData   // 将新数据重新载入表格
                                })
                            }
                            layui.form.render("select");
                        });
                    },
                    error: function () {
                        layer.msg("获取辅料信息失败！", {icon: 2});
                    }
                })
            }, 100);
        }
    })
    table.on('toolbar(fabricReturnTable)', function(obj) {
        var filterOrder = $("#orderName").val();
        if (obj.event === 'clearFilter') {
            // 清除所有筛选条件并重载表格
            // 参数: tableId
            soulTable.clearFilter('fabricReturnTable')
        }
        else if (obj.event === 'exportExcel') {
            soulTable.export('fabricReturnTable');
        }
        else if (obj.event === "addFabricReturn") {   //面料下单
            var checkStatus = table.checkStatus(obj.config.id); //获取选中行状态
            if (checkStatus.data.length == 0) {
                layer.msg('请先选择面料');
            } else {
                var orderName = checkStatus.data[0].orderName;
                var fabricID = checkStatus.data[0].fabricID;
                var clothesVersionNumber = checkStatus.data[0].clothesVersionNumber;
                var label = "面料下单&emsp;&emsp;&emsp;&emsp;" + "单号：" + clothesVersionNumber + "&emsp;&emsp;&emsp;&emsp;款号：" + orderName
                var index = layer.open({
                    type: 1 //Page层类型
                    , title: label
                    , btn: ['保存','磅转公斤','公斤转磅']
                    , shade: 0.6 //遮罩透明度
                    , area: '1000px'
                    , maxmin: false //允许全屏最小化
                    , anim: 0 //0-6的动画形式，-1不开启
                    , content: "<div id=\"fabricReturnAdd\">\n" +
                        "        <form class=\"layui-form\" style=\"margin: 0 auto;max-width:100%; min-height:400px; padding-top: 40px;\">\n" +
                        "            <table class=\"layui-hide\" id=\"fabricReturnAddTable\" lay-filter=\"fabricReturnAddTable\"></table>\n" +
                        "        </form>\n" +
                        "    </div>"
                    , yes: function (i, layero) {
                        var fabricReturnData = table.cache.fabricReturnAddTable;
                        var isInput = true;
                        var weightFlag = true;
                        var batchFlag = true;
                        var fabricDetails = [];
                        var thisTimeWeight = 0;
                        var fabricActCount = fabricReturnData[0].fabricActCount;
                        $.each(fabricReturnData, function (i, item) {
                            if (item.returnTime == '' || item.jarName == '' || item.weight == '' || item.batchNumber == '' || item.location == '') {
                                isInput = false;
                                return false;
                            }
                            if (!isNumber(item.weight)){
                                weightFlag = false;
                                return false;
                            }
                            if (!isNumber(item.batchNumber)){
                                batchFlag = false;
                                return false;
                            }
                            thisTimeWeight += Number(item.weight);
                            fabricDetails.push({
                                orderName:orderName,
                                clothesVersionNumber:clothesVersionNumber,
                                fabricName:item.fabricName,
                                fabricNumber:item.fabricNumber,
                                colorName:item.colorName,
                                isHit:item.isHit,
                                fabricColor:item.fabricColor,
                                fabricColorNumber:item.fabricColorNumber,
                                unit:item.unit,
                                supplier:item.supplier,
                                fabricCount:item.fabricCount,
                                returnTime:item.returnTime,
                                jarName:item.jarName.replace('-',''),
                                weight:item.weight,
                                batchNumber:item.batchNumber,
                                location:item.location,
                                fabricID:item.fabricID,
                                operateType:'入库'
                            })
                        });

                        if (!isInput) {
                            layer.msg("请填写全部信息", {icon: 2});
                        } else if (!weightFlag) {
                            layer.msg("重量填写有误,只能是数字", {icon: 2});
                        } else if (!batchFlag) {
                            layer.msg("卷数填写有误,只能是数字", {icon: 2});
                        } else {
                            $.ajax({
                                url: "/erp/addfabricreturndetailbatch",
                                type: 'POST',
                                data: {
                                    fabricDetailJson: JSON.stringify(fabricDetails),
                                    fabricID: fabricID,
                                    fabricActCount: fabricActCount,
                                    thisTimeWeight: thisTimeWeight
                                },
                                success: function (res) {
                                    if (res.over === 1){
                                        var tipHtml = "本次录入超过了允许接收的上限:<br><div style='font-weight: bolder;font-size: larger;color: orange;margin-bottom: 10px'>订布量:" + toDecimal(res.fabricActCount) + "<br>已入库:" + toDecimal(res.historyWeight) + "<br>允许超数:" + toDecimal(res.overWeight) +  "<br>最多还可入:" + toDecimal(res.availableWeight) + "<br>";
                                        layer.confirm(tipHtml, {
                                            title: '超数提醒'
                                            ,btn: ['暂存','返回修改'] //按钮
                                        }, function(){
                                            $.each(fabricDetails, function (i, item) {
                                                item.operateType = "暂存";
                                            });
                                            $.ajax({
                                                url: "/erp/addfabricdetailbatchtotmpstorage",
                                                type: 'POST',
                                                data: {
                                                    fabricDetailJson: JSON.stringify(fabricDetails),
                                                    fabricID: fabricID
                                                },
                                                success: function (res) {
                                                    if (res.result == 0) {
                                                        layer.close(index);
                                                        layer.msg("提交成功！", {icon: 1});
                                                        reloadTable();
                                                    } else if (res.result == 3) {
                                                        layer.msg("该面料未审核,请联系审核人员审核~~");
                                                    } else {
                                                        layer.msg("提交失败！", {icon: 2});
                                                    }
                                                },
                                                error: function () {
                                                    layer.msg("提交失败！", {icon: 2});
                                                }
                                            })
                                        }, function(){

                                        });
                                    }
                                    else {
                                        if (res.result == 0) {
                                            layer.close(index);
                                            layer.msg("录入成功！", {icon: 1});
                                            reloadTable();
                                        } else if (res.result == 3) {
                                            layer.msg("该面料未审核,请联系审核人员审核~~");
                                        } else {
                                            layer.msg("录入失败！", {icon: 2});
                                        }
                                    }
                                },
                                error: function () {
                                    layer.msg("录入失败！", {icon: 2});
                                }
                            })
                        }
                    }
                    , btn2: function(index, layero){
                        var tmpData = table.cache.fabricReturnAddTable;
                        $.each(tmpData,function(index1,value1){
                            value1.weight = toDecimal(Number(value1.weight)/2.2046);
                        });
                        table.reload("fabricReturnAddTable",{
                            data:tmpData   // 将新数据重新载入表格
                        });
                        layui.form.render("select");
                        return false;  //开启该代码可禁止点击该按钮关闭
                    }
                    , btn3: function(index, layero){
                        var tmpData = table.cache.fabricReturnAddTable;
                        $.each(tmpData,function(index1,value1){
                            value1.weight = toDecimal(Number(value1.weight) * 2.2046);
                        });
                        table.reload("fabricReturnAddTable",{
                            data:tmpData   // 将新数据重新载入表格
                        });
                        layui.form.render("select");
                        return false  //开启该代码可禁止点击该按钮关闭
                    }
                });
                layer.full(index);
                setTimeout(function () {
                    $.ajax({
                        url: "/erp/getmanufacturefabricbyid",
                        type: 'GET',
                        data: {
                            fabricID: fabricID
                        },
                        success: function (res) {
                            var title = [
                                {field: 'fabricName', title: '面料名称', align: 'center', minWidth: 200},
                                {field: 'colorName', title: '订单颜色', align: 'center', minWidth: 100},
                                {field: 'isHit', title: '是否撞色', align: 'center', minWidth: 100},
                                {field: 'fabricColor', title: '面料颜色', align: 'center', minWidth: 100},
                                {field: 'unit', title: '单位', align: 'center', minWidth: 70},
                                {field: 'supplier', title: '供应商', align: 'center', minWidth: 80},
                                {field: 'fabricActCount', title: '订布量', align: 'center', minWidth: 80},
                                {field: 'returnTime', title: '回布日期', align: 'center', minWidth: 120,edit:'text',event:'date'},
                                {field: 'jarName', title: '缸号', align: 'center', minWidth: 120,edit:'text'},
                                {field: 'batchNumber', title: '卷数', align: 'center', minWidth: 90,edit:'text'},
                                {field: 'weight', title: '重量', align: 'center', minWidth: 90,edit:'text'},
                                {field: 'location', title: '仓位',align:'center',minWidth:100,edit:'text'},
                                {field: 'operation', title: '操作',minWidth:70,align:'center' ,templet: function(d){
                                        if(d.LAY_INDEX == 1)
                                            return '<a class="layui-btn layui-btn-xs" lay-event="add"><i class="layui-icon layui-icon-addition" style="margin-right:0"></i></a>'
                                        else
                                            return '<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del"><i class="layui-icon layui-icon-subtraction" style="margin-right:0"></i></a>'
                                    }},
                                {field: 'fabricID', title: 'fabricID', hide: true},
                                {field: 'fabricNumber', title: 'fabricNumber', hide: true},
                                {field: 'fabricColorNumber', title: 'fabricColorNumber', hide: true}
                            ];
                            var fabricReturnData = [];

                            if (res.manufactureFabric){
                                var fabricData = {};
                                fabricData.fabricID = res.manufactureFabric.fabricID;
                                fabricData.fabricNumber = res.manufactureFabric.fabricNumber;
                                fabricData.fabricColorNumber = res.manufactureFabric.fabricColorNumber;
                                fabricData.fabricName = res.manufactureFabric.fabricName;
                                fabricData.unit = res.manufactureFabric.unit;
                                fabricData.colorName = res.manufactureFabric.colorName;
                                fabricData.isHit = res.manufactureFabric.isHit;
                                fabricData.fabricColor = res.manufactureFabric.fabricColor;
                                fabricData.supplier = res.manufactureFabric.supplier;
                                fabricData.fabricActCount = res.manufactureFabric.fabricActCount;
                                fabricData.returnTime = '';
                                fabricData.jarName = '';
                                fabricData.weight = '';
                                fabricData.batchNumber = '';
                                fabricData.location = '';
                                fabricReturnData.push(fabricData);
                            } else{
                                layer.msg("未找到数据");
                            }

                            table.render({
                                elem: '#fabricReturnAddTable'
                                , cols: [title]
                                , data: fabricReturnData
                                , height: 'full-300'
                                , done: function (res) {
                                    $(".layui-table-main  tr").each(function (index, val) {
                                        $($(".layui-table-fixed .layui-table-body tbody tr")[index]).height($(val).height());
                                    });
                                }
                            });
                            //监听单元格编辑
                            table.on('tool(fabricReturnAddTable)', function (obj) {
                                var data = obj.data;
                                var tableData = table.cache.fabricReturnAddTable;
                                var that = this;
                                var field = $(that).data('field');
                                if(obj.event === 'date'){
                                    laydate.render({
                                        elem: $(this).find('.layui-table-edit').get(0)//
                                        ,show: true //直接显示
                                        ,done: function(value, date){
                                            obj.update({
                                                [field] : value
                                            });
                                        }
                                    });
                                }else if(obj.event === 'del'){
                                    obj.del();
                                    $.each(tableData,function (index,item) {
                                        if(item instanceof Array) {
                                            tableData.splice(index,1)
                                        }
                                    });
                                    table.reload("fabricReturnAddTable",{
                                        data:tableData   // 将新数据重新载入表格
                                    })
                                } else if(obj.event === 'add'){
                                    tableData.push({
                                        "fabricName": tableData[0].fabricName
                                        ,"fabricID": tableData[0].fabricID
                                        ,"fabricNumber": tableData[0].fabricNumber
                                        ,"fabricColorNumber": tableData[0].fabricColorNumber
                                        ,"unit": tableData[0].unit
                                        ,"colorName": tableData[0].colorName
                                        ,"isHit": tableData[0].isHit
                                        ,"fabricColor": tableData[0].fabricColor
                                        ,"supplier": tableData[0].supplier
                                        ,"fabricActCount": tableData[0].fabricActCount
                                        ,"returnTime": ''
                                        ,"jarName": ''
                                        ,"weight": ''
                                        ,"batchNumber": ''
                                        ,"location": ''

                                    });
                                    table.reload("fabricReturnAddTable",{
                                        data:tableData   // 将新数据重新载入表格
                                    })
                                }
                                layui.form.render("select");
                            });
                        },
                        error: function () {
                            layer.msg("获取尺码失败！", {icon: 2});
                        }
                    })
                }, 100);
            }
        }
    })
});
function toDecimal(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*100)/100;
    return f;
}
function isNumber(val){

    var regPos = /^\d+(\.\d+)?$/; //非负浮点数
    var regNeg = /^(-(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*)))$/; //负浮点数
    if (regPos.test(val) || regNeg.test(val)){
        return true;
    }else{
        return false;
    }

}
