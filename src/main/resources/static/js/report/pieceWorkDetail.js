layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.21'
}).extend();
$(document).ready(function () {
    layui.laydate.render({
        elem: '#from',
        trigger: 'click'
    });
    layui.laydate.render({
        elem: '#to',
        trigger: 'click'
    });

    layui.use(['autocomplete'], function () {
        layui.autocomplete.render({
            elem: $('#employeeNumber')[0],
            url: 'getemphint',
            template_val: '{{d.employeeNumber}}',
            template_txt: '{{d.employeeNumber}}',
            onselect: function (resp) {
                autoCompleteOther(resp.employeeNumber);
            }
        })
    });

    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumber", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });
});

function autoCompleteOther(employeeNumber) {
    $.ajax({
        url: "/erp/getempnamebyempnum",
        data: {"employeeNumber": employeeNumber},
        success:function(data){
            $("#employeeName").val(data);
            $("#employeeName").attr("disabled",true);
        },
        error:function(){
        }
    });
}

function search() {
    if($("#from").val().trim()=="") {
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入完整信息！</span>",html: true});
        return false;
    }
    if($("#to").val().trim()=="") {
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入完整信息！</span>",html: true});
        return false;
    }
    $.blockUI({ css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        },
        message: "<h3>稍等一下... 奋力搜索中</h3>"
    });
    $.ajax({
        url: "/erp/getpieceworkdetail",
        type:'GET',
        data: {
            from:$("#from").val(),
            to:$("#to").val(),
            employeeNumber:$("#employeeNumber").val(),
            orderName:$("#orderName").val()
        },
        success: function (html) {
            $("#entities").html(html);
            $('html, body').scrollTop($("#entities").offset().top);

            $('#pieceWorkDetailTable').DataTable({
                language : {
                    processing : "载入中",//处理页面数据的时候的显示
                    paginate : {//分页的样式文本内容。
                        previous : "上一页",
                        next : "下一页",
                        first : "第一页",
                        last : "最后一页"
                    },
                    search:"搜索：",
                    zeroRecords : "没有内容",//table tbody内容为空时，tbody的内容。
                    //下面三者构成了总体的左下角的内容。
                    info : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//左下角的信息显示，大写的词为关键字。
                    infoEmpty : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//筛选为空时左下角的显示。
                    infoFiltered : ""//筛选之后的左下角筛选提示(另一个是分页信息显示，在上面的info中已经设置，所以可以不显示)，
                },
                searching:true,
                lengthChange:false,
                // pageLength : 100,// 每页显示10条数据
                pagingType : "full_numbers",
                "paging" : false,
                "info": true,
                scrollX: 1200,
                scrollY: $(document.body).height() - 350,
                scrollCollapse: true,
                scroller:       true,
                dom: 'Bfrtip',
                "buttons": [{
                    'extend': 'excel',
                    'text': '导出',//定义导出excel按钮的文字
                    'className': 'btn btn-success', //按钮的class样式
                    'title': "产能详情",
                    'exportOptions': {
                        'modifier': {
                            'page': 'all'
                        }
                    }
                }],
                "footerCallback": function ( row, data, start, end, display ) {
                    var api = this.api(), data;

                    // Remove the formatting to get integer data for summation
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };

                    // Total over all pages
                    total = api
                        .column( 12 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Total over all pages
                    totalTwo = api
                        .column( 14 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Total over all pages
                    totalThree = api
                        .column( 10 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Total over this page
                    pageTotal = api
                        .column( 12, { page: 'current'} )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Total over this page
                    pageTotalTwo = api
                        .column( 14, { page: 'current'} )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Total over this page
                    pageTotalThree = api
                        .column( 10, { page: 'current'} )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Update footer
                    $( api.column( 4 ).footer() ).html(
                        // "总计数量："+ total,
                        "当前件数："+pageTotalThree +"，总件数："+ totalThree+
                        "当前页金额1："+pageTotal.toFixed(2) +"，总金额1："+ total.toFixed(2)+
                        "当前页金额2："+pageTotalTwo.toFixed(2) +"，总金额2："+ totalTwo.toFixed(2)
                    );
                }
            });
            $.unblockUI();
        },
        error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }
    })
}