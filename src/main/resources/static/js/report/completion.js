var basePath=$("#basePath").val();
var windowHeight;
$(document).ready(function () {
    windowHeight = $(document.body).height();
    $.blockUI({ css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        },
        message: "<h3>稍等一下... 奋力搜索中</h3>"
    });
    $.ajax({
        url: basePath + "erp/getcompletionreport",
        type:'GET',
        data: {},
        success: function (data) {
            if(data) {
                createFinishTable(data.completionList);
                $.unblockUI();
            }else {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，没有查到相关信息！</span>",html: true});
            }
        },
        error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }
    });

});

var finishTable;
var myDate = new Date();
function createFinishTable(data){
    if (finishTable != undefined){
        finishTable.clear();
        finishTable.destroy();
    }
    finishTable = $('#finishTable').DataTable({
        "data":data,
        language : {
            processing : "载入中",//处理页面数据的时候的显示
            paginate : {//分页的样式文本内容。
                previous : "上一页",
                next : "下一页",
                first : "第一页",
                last : "最后一页"
            },
            search:"搜索：",
            zeroRecords : "没有内容",//table tbody内容为空时，tbody的内容。
            //下面三者构成了总体的左下角的内容。
            info : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//左下角的信息显示，大写的词为关键字。
            infoEmpty : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//筛选为空时左下角的显示。
            infoFiltered : ""//筛选之后的左下角筛选提示(另一个是分页信息显示，在上面的info中已经设置，所以可以不显示)，
        },
        dom: 'Bfrtip',
        "buttons": [{
            'extend': 'excel',
            'text': '导出',//定义导出excel按钮的文字
            'className': 'btn', //按钮的class样式
            'title': '完工结算表'+myDate.toLocaleString( ),
            'exportOptions': {
                'modifier': {
                    'page': 'all'
                }
            }
        }],
        pageLength : 20,// 每页显示10条数据
        pagingType : "full_numbers", // 分页样式：simple,simple_numbers,full,full_numbers，
        "paging": true,
        // "ordering" : false,
        "info": true,
        searching:true,
        lengthChange:false,
        scrollX: 2000,
        scrollY: windowHeight - 100,
        fixedHeader: true,
        scrollCollapse: true,
        scroller:       true,
        "order": [[ 12, "desc" ]],
        "columns": [
            {
                "data": "clothesVersionNumber",
                "title":"版单号",
                "width":"150px",
                "defaultContent": "",
                "sClass": "text-center",
            },{
                "data": "orderName",
                "title":"订单号",
                "width":"150px",
                "defaultContent": "",
                "sClass": "text-center",
            },{
                "data": "season",
                "title":"季度",
                "width":"90px",
                "defaultContent": "",
                "sClass": "text-center",
            },{
                "data": "orderCount",
                "title":"订单量",
                "width":"60px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "tailorCount",
                "title":"裁数",
                "width":"60px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "wellTailorCount",
                "title":"好片数",
                "width":"60px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "wellPercent",
                "title":"好片率",
                "width":"80px",
                "defaultContent": "",
                "sClass": "text-center",
                render:function(data){
                    var tmp = ((100*data).toFixed(2)).toString();
                    var num =tmp+"%";
                    var result="style='width: "+tmp+"%'" +" 'color: #0f0f0f'";
                    var color="class='progress-bar'";
                    if(data>=0.9){
                        color="class='progress-bar bg-success'";
                    }
                    else if(data>=0.7){
                        color="class='progress-bar bg-warning'";
                    }
                    else {
                        color="class='progress-bar bg-danger'";
                    }
                    return "<div class='progress'>"+
                        "<div class='progress-bar' role='progressbar' aria-valuenow='50' aria-valuemin='0' aria-valuemax='100'" + result + color +">"+"<font color='black'>"+num+"</font>"+"</div>" +
                        "</div>";
                }
            }, {
                "data": "embCount",
                "title":"衣胚",
                "width":"45px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "upCount",
                "title":"吊挂",
                "width":"45px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "inspectCount",
                "title":"成品",
                "width":"45px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "pressCount",
                "title":"大烫",
                "width":"45px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "packageCount",
                "title":"装箱",
                "width":"45px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "completePercent",
                "title":"达成率",
                "width":"80px",
                "defaultContent": "",
                "sClass": "text-center",
                render:function(data){
                    var tmp = ((100*data).toFixed(2)).toString();
                    var num =tmp+"%";
                    var result="style='width: "+tmp+"%'" +" 'color: #0f0f0f'";
                    var color="class='progress-bar'";
                    if(data>=0.9){
                        color="class='progress-bar bg-success'";
                    }
                    else if(data>=0.7){
                        color="class='progress-bar bg-warning'";
                    }
                    else {
                        color="class='progress-bar bg-danger'";
                    }
                    return "<div class='progress'>"+
                        "<div class='progress-bar' role='progressbar' aria-valuenow='50' aria-valuemin='0' aria-valuemax='100'" + result + color +">"+"<font color='black'>"+num+"</font>"+"</div>" +
                        "</div>";
                }
            }
        ]
    });
}
