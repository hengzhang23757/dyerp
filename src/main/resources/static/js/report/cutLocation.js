layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.21'
}).extend();
$(document).ready(function () {
    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumber", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });

    $("#orderName").bind('blur change',function(){
        var orderName = $("#orderName").val();
        $("#bedNumber").empty();
        $.ajax({
            url: "/erp/getbednumbersbyordername",
            data: {"orderName": orderName},
            success:function(data){
                $("#bedNumber").empty();
                if (data.bedNumList) {
                    $("#bedNumber").append("<option value=''>选择床号</option>");
                    $.each(data.bedNumList, function(index,element){
                        $("#bedNumber").append("<option value="+element+">"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });

        $("#colorName").empty();
        $.ajax({
            url: "/erp/getcolorhint",
            data: {"orderName": orderName},
            success:function(data){
                $("#colorName").empty();
                if (data.colorList) {
                    $("#colorName").empty();
                    $("#colorName").append("<option value=''>颜色</option>");
                    $.each(data.colorList, function(index,element){
                        $("#colorName").append("<option value='"+element.colorName+"'>"+element.colorName+"</option>");
                    })
                }
            },
            error:function(){
            }
        });
        $("#sizeName").empty();
        $.ajax({
            url: "/erp/getordersizenamesbyorder",
            data: {"orderName": orderName},
            success:function(data){
                $("#sizeName").empty();
                if (data.sizeNameList) {
                    $("#sizeName").empty();
                    $("#sizeName").append("<option value=''>尺码</option>");
                    $.each(data.sizeNameList, function(index,element){
                        $("#sizeName").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });
    });

    $("#clothesVersionNumber").bind('blur change',function(){
        var orderName = $("#orderName").val();
        $("#bedNumber").empty();
        $.ajax({
            url: "/erp/getbednumbersbyordername",
            data: {"orderName": orderName},
            success:function(data){
                $("#bedNumber").empty();
                if (data.bedNumList) {
                    $("#bedNumber").append("<option value=''>选择床号</option>");
                    $.each(data.bedNumList, function(index,element){
                        $("#bedNumber").append("<option value="+element+">"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });

        $("#colorName").empty();
        $.ajax({
            url: "/erp/getcolorhint",
            data: {"orderName": orderName},
            success:function(data){
                $("#colorName").empty();
                if (data.colorList) {
                    $("#colorName").empty();
                    $("#colorName").append("<option value=''>颜色</option>");
                    $.each(data.colorList, function(index,element){
                        $("#colorName").append("<option value='"+element.colorName+"'>"+element.colorName+"</option>");
                    })
                }
            },
            error:function(){
            }
        });
        $("#sizeName").empty();
        $.ajax({
            url: "/erp/getordersizenamesbyorder",
            data: {"orderName": orderName},
            success:function(data){
                $("#sizeName").empty();
                if (data.sizeNameList) {
                    $("#sizeName").empty();
                    $("#sizeName").append("<option value=''>尺码</option>");
                    $.each(data.sizeNameList, function(index,element){
                        $("#sizeName").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });
    });
});

function search() {
    if($("#orderName").val().trim()=="") {
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入订单信息！</span>",html: true});
        return false;
    }

    $.blockUI({ css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        },
        message: "<h3>稍等一下... 奋力搜索中</h3>"
    });

    var orderName = $("#orderName").val();
    var bedNumber = $("#bedNumber").val();
    var colorName = $("#colorName").val();
    var sizeName = $("#sizeName").val();
    $.ajax({
        url: "/erp/getdetailcutlocation",
        type:'GET',
        data: {
            orderName:orderName,
            bedNumber:bedNumber,
            colorName:colorName,
            sizeName:sizeName,
        },
        success: function (html) {
            $("#entities").html(html);
            $('html, body').scrollTop($("#entities").offset().top);

            $('#cutLocationTable').DataTable({
                language : {
                    processing : "载入中",//处理页面数据的时候的显示
                    paginate : {//分页的样式文本内容。
                        previous : "上一页",
                        next : "下一页",
                        first : "第一页",
                        last : "最后一页"
                    },
                    search:"搜索：",
                    zeroRecords : "没有内容",//table tbody内容为空时，tbody的内容。
                    //下面三者构成了总体的左下角的内容。
                    info : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//左下角的信息显示，大写的词为关键字。
                    infoEmpty : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//筛选为空时左下角的显示。
                    infoFiltered : ""//筛选之后的左下角筛选提示(另一个是分页信息显示，在上面的info中已经设置，所以可以不显示)，
                },
                searching:true,
                lengthChange:false,
                // pageLength : 15,// 每页显示10条数据
                pagingType : "full_numbers",
                "paging" : false,
                "info": true,
                scrollX: 2000,
                scrollY: $(document.body).height() - 370,
                fixedHeader: true,
                scrollCollapse: true,
                scroller:       true,
                dom: 'Bfrtip',
                "buttons": [{
                    'extend': 'excel',
                    'text': '导出',//定义导出excel按钮的文字
                    'className': 'btn btn-success', //按钮的class样式
                    'title': orderName+"裁片实时位置",
                    'exportOptions': {
                        'modifier': {
                            'page': 'all'
                        }
                    }
                }]
            });
            $.unblockUI();
        },
        error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }
    })
}