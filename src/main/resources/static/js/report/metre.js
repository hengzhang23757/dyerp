layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.21'
}).extend();
$(document).ready(function () {
    layui.laydate.render({
        elem: '#from',
        trigger: 'click'
    });
    layui.laydate.render({
        elem: '#to',
        trigger: 'click'
    });

    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumber", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });

    $("#orderName").bind('blur change',function(){
        var orderName = $(this).val();
        var procedureNumber = $("#procedureNumber").val().trim();
        if (procedureNumber != "") {
            if (!(/(^[1-9]\d*$)/.test(procedureNumber))){
                swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">工序号有误！</span>",html: true});;
                return false;
            }
        }else {
            swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">工序号不能为空！</span>",html: true});;
            return false;
        }
        $("#employeeName").empty();
        $.ajax({
            url: "/erp/getemployeenamesbyorderprocedure",
            data: {
                "orderName": orderName,
                "procedureNumber": procedureNumber
            },
            success:function(data){
                $("#employeeName").empty();
                if (data.employeeNameList) {
                    $("#employeeName").append("<option value=''>姓名</option>");
                    $.each(data.employeeNameList, function(index,element){
                        $("#employeeName").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });
    });

    $("#clothesVersionNumber").bind('blur change',function(){
        var orderName = $(this).val();
        var procedureNumber = $("#procedureNumber").val().trim();
        if (procedureNumber != "") {
            if (!(/(^[1-9]\d*$)/.test(procedureNumber))){
                swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">工序号有误！</span>",html: true});;
                return false;
            }
        }else {
            swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">工序号不能为空！</span>",html: true});;
            return false;
        }
        $("#employeeName").empty();
        $.ajax({
            url: "/erp/getemployeenamesbyorderprocedure",
            data: {
                "orderName": orderName,
                "procedureNumber": procedureNumber
            },
            success:function(data){
                $("#employeeName").empty();
                if (data.employeeNameList) {
                    $("#employeeName").append("<option value=''>姓名</option>");
                    $.each(data.employeeNameList, function(index,element){
                        $("#employeeName").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });
    });

});

var basePath=$("#basePath").val();
var orderName,metreTime,employeeNumber;

function search() {
    if($("#procedureNumber").val()=="" || $("#procedureNumber").val() == null) {
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入工序号！</span>",html: true});
        return false;
    }
    if($("#orderName").val()=="" || $("#orderName").val()==null) {
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">订单号不能为空！</span>",html: true});
        return false;
    }
    $.blockUI({ css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        },
        message: "<h3>稍等一下... 奋力搜索中</h3>"
    });
    procedureNumber = $("#procedureNumber").val().trim();
    orderName = $("#orderName").val();
    from = $("#from").val();
    to = $("#to").val();
    employeeName = $("#employeeName").val();
    $.ajax({
        url: basePath + "erp/getmetrebyordertimeemp",
        type:'GET',
        data: {
            procedureNumber:procedureNumber,
            orderName:orderName,
            employeeName:employeeName,
            from:from,
            to:to,
        },
        success: function (data) {
            if(data) {
                createMetreTable(data.metreList);
            }else {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，没有查到相关信息！</span>",html: true});
            }
            $.unblockUI();
        },
        error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }
    })
}

var metreTable;
var myDate = new Date();
var clothesVersionNumber = $("#clothesVersionNumber").val();
function createMetreTable(data) {
    if (metreTable != undefined) {
        metreTable.clear(); //清空一下table
        metreTable.destroy(); //还原初始化了的datatable
    }
    metreTable = $('#metreTable').DataTable({
        "data":data,
        language : {
            processing : "载入中",//处理页面数据的时候的显示
            paginate : {//分页的样式文本内容。
                previous : "上一页",
                next : "下一页",
                first : "第一页",
                last : "最后一页"
            },
            search:"搜索：",
            zeroRecords : "没有内容",//table tbody内容为空时，tbody的内容。
            //下面三者构成了总体的左下角的内容。
            info : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//左下角的信息显示，大写的词为关键字。
            infoEmpty : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//筛选为空时左下角的显示。
            infoFiltered : ""//筛选之后的左下角筛选提示(另一个是分页信息显示，在上面的info中已经设置，所以可以不显示)，
        },
        dom: 'Bfrtip',
        "buttons": [{
            'extend': 'excel',
            'text': '导出',//定义导出excel按钮的文字
            'className': 'btn', //按钮的class样式
            'title': '节拍时间表'+myDate.toLocaleString( ),
            'exportOptions': {
                'modifier': {
                    'page': 'all'
                }
            }
        }],
        // pageLength : 100,// 每页显示10条数据
        pagingType : "full_numbers",
        "paging": false,
        "info": true,
        searching:true,
        lengthChange:true,
        scrollX: 2000,
        scrollY: $(document.body).height() - 280,
        fixedHeader: true,
        scrollCollapse: true,
        scroller:       true,
        "columns": [
            {
                "data": null,
                "title":"序号",
                "width":"30px",
                "defaultContent": "",
                "searchable":false,
                "sClass": "text-center",
                render: function (data, type, row, meta) {
                    var no = meta.settings._iDisplayStart + meta.row + 1;
                    return no;
                }
            }, {
                "data": "employeeName",
                "title":"姓名",
                "width":"60px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "employeeNumber",
                "title":"工号",
                "width":"60px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "beginTime",
                "title":"开始时间",
                "width":"160px",
                "defaultContent": "",
                "sClass": "text-center",
                "render": function(data, type, full, meta) {
                    return  moment(data).format("YYYY-MM-DD HH:mm:ss");
                },
            }, {
                "data": "endTime",
                "title":"结束时间",
                "width":"160px",
                "defaultContent": "",
                "sClass": "text-center",
                "render": function(data, type, full, meta) {
                    return  moment(data).format("YYYY-MM-DD HH:mm:ss");
                },
            }, {
                "data": "timeCount",
                "title":"计时",
                "width":"60px",
                "defaultContent": "",
                "sClass": "text-center",
            },{
                "data": "clothesVersionNumber",
                "title":"版单",
                "width":"100px",
                "defaultContent": "",
                "sClass": "text-center",
            },{
                "data": "orderName",
                "title":"订单",
                "width":"100px",
                "defaultContent": "",
                "sClass": "text-center",
            },{
                "data": "colorName",
                "title":"颜色",
                "width":"80px",
                "defaultContent": "",
                "sClass": "text-center",
            },{
                "data": "sizeName",
                "title":"尺码",
                "width":"60px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "procedureNumber",
                "title":"工序号",
                "width":"45px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "procedureName",
                "title":"工序名",
                "width":"90px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "layerCount",
                "title":"数量",
                "width":"30px",
                "defaultContent": "",
                "sClass": "text-center",
            }
        ]
    });
}


function transDate(data) {return  moment(data).format("YYYY-MM-DD HH:mm:ss");};

