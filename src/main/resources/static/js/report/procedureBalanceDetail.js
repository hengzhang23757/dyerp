var hot;
var basePath=$("#basePath").val();

$(document).ready(function () {
    var container = document.getElementById('detailExcel');
    hot = new Handsontable(container, {
        // data: data,
        rowHeaders: true,
        colHeaders: true,
        autoColumnSize:true,
        dropdownMenu: true,
        contextMenu:true,
        renderAllRows: true,
        minRows:1,
        minCols:5,
        // colWidths:70,
        language:'zh-CN',
        licenseKey: 'non-commercial-and-evaluation'
    });
    var orderName=$("#orderName").val();
    var from=$("#from").val();
    var to=$("#to").val();
    var groupName=$("#groupName").val();
    var procedureNumber=$("#procedureNumber").val();
    $.ajax({
        url: "/erp/getproductionprogressdetailbyordertimegroup",
        type:'GET',
        data: {
            orderName:orderName,
            from:from,
            to:to,
            groupName:groupName,
            procedureNumber:procedureNumber
        },
        success: function (data) {
            $("#exportDiv").show();
            var hotData = [];
            if(data) {
                if (procedureNumber == 1 || procedureNumber == 3){
                    var colorData = data["color"];
                    var personData = data["person"];
                    var personLength = personData.length;
                    var partIndex = 0;
                    var sizeSummary = [];
                    sizeSummary[0] = [""];
                    sizeSummary[1] = ["尺码生产合计"];
                    for(var color in colorData) {
                        var colorRow1 = [];
                        var colorRow2 = [];
                        var colorRow3 = [];
                        var colorRow4 = [];
                        colorRow2[0] = [];
                        colorRow3[0] = [];
                        colorRow4[0] = [];
                        colorRow1.push(color);
                        colorRow1.push("订单量");
                        colorRow2.push("好片数");
                        colorRow3.push("生产量");
                        colorRow4.push("未生成量");
                        var sum1 = 0;
                        var sum2 = 0;
                        var sum3 = 0;
                        var sum4 = 0;
                        var sizeData = colorData[color];
                        var sizeRow = [];
                        var sizeIndex = 2;
                        for (var sizeKey in sizeData){
                            if (partIndex == 0){
                                sizeRow[0] = [];
                                sizeRow[1] = [];
                                sizeRow.push(sizeKey);
                                sizeSummary[sizeIndex] = 0;
                            }
                            colorRow1.push(sizeData[sizeKey].orderCount);
                            colorRow2.push(sizeData[sizeKey].cutCount);
                            colorRow3.push(sizeData[sizeKey].cutCount);
                            colorRow4.push(0);
                            sum1 += sizeData[sizeKey].orderCount;
                            sum2 += sizeData[sizeKey].cutCount;
                            sum3 += sizeData[sizeKey].cutCount;
                            sum4 += 0;
                            sizeSummary[sizeIndex] += sizeData[sizeKey].cutCount;
                            sizeIndex++;
                        }
                        if (partIndex == 0) {
                            sizeRow.push("合计");
                            for (var personi=0;personi<personLength;personi++){
                                sizeRow.push([[""]]);
                            }
                            hotData.push(sizeRow);
                        }
                        colorRow1.push(sum1);
                        colorRow2.push(sum2);
                        colorRow3.push(sum3);
                        colorRow4.push(sum4);
                        hotData.push(colorRow1);
                        hotData.push(colorRow2);
                        hotData.push(colorRow3);
                        hotData.push(colorRow4);
                        partIndex ++;
                    }
                    hotData.push(sizeSummary);
                    hotData.push([]);
                    // hotData.push([["个人明细"]]);
                    // var groupRow = [];
                    // var employeeNumberRow = [];
                    // var employeeNameRow = [];
                    // var productionCountRow = [];
                    // groupRow.push("组名");
                    // employeeNumberRow.push("工号");
                    // employeeNameRow.push("姓名");
                    // productionCountRow.push("产量");
                    // for (var i=0;i<personData.length;i++){
                    //     groupRow.push(personData[i].groupName);
                    //     employeeNumberRow.push(personData[i].employeeNumber);
                    //     employeeNameRow.push(personData[i].employeeName);
                    //     productionCountRow.push(personData[i].productionCount);
                    // }
                    // hotData.push(groupRow);
                    // hotData.push(employeeNumberRow);
                    // hotData.push(employeeNameRow);
                    // hotData.push(productionCountRow);
                }else{
                    var colorData = data["color"];
                    var personData = data["person"];
                    var personLength = personData.length;
                    var partIndex = 0;
                    var sizeSummary = [];
                    sizeSummary[0] = [""];
                    sizeSummary[1] = ["尺码生产合计"];
                    for(var color in colorData) {
                        var colorRow1 = [];
                        var colorRow2 = [];
                        var colorRow3 = [];
                        var colorRow4 = [];
                        colorRow2[0] = [];
                        colorRow3[0] = [];
                        colorRow4[0] = [];
                        colorRow1.push(color);
                        colorRow1.push("订单量");
                        colorRow2.push("好片数");
                        colorRow3.push("生产量");
                        colorRow4.push("未生成量");
                        var sum1 = 0;
                        var sum2 = 0;
                        var sum3 = 0;
                        var sum4 = 0;
                        var sizeData = colorData[color];
                        var sizeRow = [];
                        var sizeIndex = 2;
                        for (var sizeKey in sizeData){
                            if (partIndex == 0){
                                sizeRow[0] = [];
                                sizeRow[1] = [];
                                sizeRow.push(sizeKey);
                                sizeSummary[sizeIndex] = 0;
                            }
                            colorRow1.push(sizeData[sizeKey].orderCount);
                            colorRow2.push(sizeData[sizeKey].cutCount);
                            colorRow3.push(sizeData[sizeKey].productionCount);
                            colorRow4.push(sizeData[sizeKey].leakCount);
                            sum1 += sizeData[sizeKey].orderCount;
                            sum2 += sizeData[sizeKey].cutCount;
                            sum3 += sizeData[sizeKey].productionCount;
                            sum4 += sizeData[sizeKey].leakCount;
                            sizeSummary[sizeIndex] += sizeData[sizeKey].productionCount;
                            sizeIndex++;
                        }
                        if (partIndex == 0) {
                            sizeRow.push("合计");
                            for (var personi=0;personi<personLength;personi++){
                                sizeRow.push([[""]]);
                            }
                            hotData.push(sizeRow);
                        }
                        colorRow1.push(sum1);
                        colorRow2.push(sum2);
                        colorRow3.push(sum3);
                        colorRow4.push(sum4);
                        hotData.push(colorRow1);
                        hotData.push(colorRow2);
                        hotData.push(colorRow3);
                        hotData.push(colorRow4);
                        partIndex ++;
                    }
                    hotData.push(sizeSummary);
                    hotData.push([]);
                    hotData.push([["个人明细"]]);
                    var groupRow = [];
                    var employeeNumberRow = [];
                    var employeeNameRow = [];
                    var productionCountRow = [];
                    groupRow.push("组名");
                    employeeNumberRow.push("工号");
                    employeeNameRow.push("姓名");
                    productionCountRow.push("产量");
                    for (var i=0;i<personData.length;i++){
                        groupRow.push(personData[i].groupName);
                        employeeNumberRow.push(personData[i].employeeNumber);
                        employeeNameRow.push(personData[i].employeeName);
                        productionCountRow.push(personData[i].productionCount);
                    }
                    hotData.push(groupRow);
                    hotData.push(employeeNumberRow);
                    hotData.push(employeeNameRow);
                    hotData.push(productionCountRow);
                }

            }
            if(hotData.length>0)
                hot.loadData(hotData);
        },
        error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }
    })
});



function exportData() {
    var data = hot.getData();
    var result = [];
    var col = 0;
    $.each(data,function (index, item) {
        for(var i=0;i<item.length;i++) {
            if(item[i]==null) {
                item[i] = '';
            }else if(index==1) {
                col++;
            }
        }
        result.push(item);
    });
    var myDate = new Date();
    export2Excel(['工序详情表-'+myDate.toLocaleString()],col, result, '工序详情表-'+myDate.toLocaleString()+".xls")
}