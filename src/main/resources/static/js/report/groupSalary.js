var form;
layui.use('form', function(){
    form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
    form.render();
});


$(document).ready(function () {

    form.render('select');
    // $("#groupName").next().find("select")[0].style.width = '150px';
    $(".layui-form-select,.layui-input").css("padding-right",0);

    $.ajax({
        url: "getallcheckgroupname",
        type: 'GET',
        data: {},
        success:function(res){
            if (res) {
                $("#groupName").empty();
                $("#groupName").append("<option value=''>全部</option>");
                $.each(res.data, function (index, item) {
                    $("#groupName").append("<option value='"+item+"'>"+item+"</option>");
                });
                form.render('select');
            }
        }, error: function () {
            layer.msg("获取数据失败", { icon: 2 });
        }
    });
    layui.laydate.render({
        elem: '#from',
        trigger: 'click'
    });
    layui.laydate.render({
        elem: '#to',
        trigger: 'click'
    });

});

layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.21'
}).extend({                         // 模块别名
    soulTable: 'soulTable'
});

var groupSalaryTable;

layui.use(['form', 'table', 'soulTable'], function () {
    var table = layui.table,
        soulTable = layui.soulTable,
        $ = layui.$;
    var load = layer.load();

    $(".layui-form-select,.layui-input").css("padding-right",0);

    groupSalaryTable = table.render({
        elem: '#groupSalaryTable'
        ,cols:[[]]
        ,loading:true
        ,data:[]
        ,height: 'full-70'
        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
        ,defaultToolbar: ['filter', 'print']
        ,title: '分组产能表'
        ,totalRow: true
        ,page: true
        ,limits: [500, 1000, 2000]
        ,limit: 1000 //每页默认显示的数量
        ,done: function () {
            soulTable.render(this);
            layer.close(load);
        }
        ,filter: {
            bottom: true,
            items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
        }
    });

    //监听提交
    form.on('submit(searchBeat)', function(data){
        var data = {};
        var from = $("#from").val();
        var to = $("#to").val();
        var groupName = $("#groupName").val();
        var salaryType = $("#salaryType").val();
        if ((from == null || from === "") && (to == null || to === "")){
            layer.msg("时间段必填", { icon: 2 });
            return false;
        }
        data.from = from;
        data.to = to;
        data.groupName = groupName;
        data.salaryType = salaryType;
        var load = layer.load(2);
        $.ajax({
            url: "/erp/getgroupsalarybytime",
            type: 'GET',
            data: data,
            success: function (res) {
                if (res.emp) {
                    var salaryData = res.emp;
                    if (res.type == "计件"){
                        table.render({
                            elem: '#groupSalaryTable'
                            ,totalRow: true
                            ,cols:[[
                                {type:'numbers', align:'center', title:'序号', width:60}
                                ,{field:'employeeName', title:'姓名', align:'center', width:100, sort: true, filter: true}
                                ,{field:'employeeNumber', title:'工号', align:'center', width:90, sort: true, filter: true}
                                ,{field:'groupName', title:'组名', align:'center', width:120, sort: true, filter: true}
                                ,{field:'orderName', title:'款号', align:'center', width:120, sort: true, filter: true}
                                ,{field:'clothesVersionNumber', title:'单号', align:'center', width:120, sort: true, filter: true}
                                ,{field:'procedureNumber', title:'工序号', align:'center', width:90, sort: true, filter: true}
                                ,{field:'procedureName', title:'工序名', align:'center',width:120, sort: true, filter: true}
                                ,{field:'pieceCount', title:'数量', align:'center', width:90, sort: true, filter: true, totalRow: true,excel:{cellType: 'n'}}
                                ,{field:'price', title:'单价1', align:'center', width:90, sort: true, filter: true,templet: function (d) {
                                        return toDecimalFour(d.price);
                                    },excel: {cellType: 'n'}}
                                ,{field:'salary', title:'金额1', align:'center', width:90, sort: true, filter: true, totalRow: true,templet: function (d) {
                                        return toDecimalFour(d.salary);
                                    },excel: {cellType: 'n'}}
                                ,{field:'priceTwo', title:'单价2', align:'center', width:90, sort: true, filter: true,templet: function (d) {
                                        return toDecimalFour(d.priceTwo);
                                    },excel: {cellType: 'n'}}
                                ,{field:'salaryTwo', title:'金额2', align:'center', width:90, sort: true, filter: true, totalRow: true,templet: function (d) {
                                        return toDecimalFour(d.salaryTwo);
                                    },excel: {cellType: 'n'}}
                            ]]
                            ,page: true
                            ,height: 'full-100'
                            ,limits: [500, 1000, 2000]
                            ,limit: 1000 //每页默认显示的数量
                            ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                            ,defaultToolbar: ['filter', 'print']
                            ,excel: {
                                filename: '员工计件工资分组详情.xlsx',
                                add: {
                                    top: {
                                        data: [
                                            ['员工计件工资分组详情'], //头部第一行数据，由于我设置了后面的数据merge了，就只写一个
                                            ["从"+from + "到"+to]
                                        ],
                                        heights: [30,15],
                                        merge: [['1,1','1,15'],['2,1','2,3']]
                                    }
                                }
                            }
                            ,data: salaryData   // 将新数据重新载入表格
                            ,overflow: 'tips'
                            ,done: function () {
                                soulTable.render(this);
                                layer.close(load);
                            }
                        });
                    }else if (res.type == "汇总"){
                        table.render({
                            elem: '#groupSalaryTable'
                            ,totalRow: true
                            ,cols:[[
                                {type:'numbers', align:'center', title:'序号', width:80},
                                {field: 'groupName', title: '组名', width: 120},
                                {field: 'employeeNumber', title: '工号', width: 120, sort: true, filter: true, totalRowText: '合计'},
                                {field: 'employeeName', title: '姓名', width: 120, sort: true, filter: true},
                                {field: 'position', title: '职位', width: 100, sort: true, filter: true},
                                {field: 'pieceSalary', title: '计件工资', width: 180 , filter: true, totalRow: true,templet: function (d) {
                                        return toDecimalFour(d.pieceSalary);
                                    },excel: {cellType: 'n'}},
                                {field: 'pieceSalaryTwo', title: '计件补贴', width: 180, filter: true, totalRow: true,templet: function (d) {
                                        return toDecimalFour(d.pieceSalaryTwo);
                                    },excel: {cellType: 'n'}},
                                {field: 'hourSalary', title: '时工工资', width: 180, filter: true, totalRow: true,templet: function (d) {
                                        return toDecimalFour(d.hourSalary);
                                    },excel: {cellType: 'n'}},
                                {field: 'bonus', title: '产值奖', width: 180, filter: true, totalRow: true,templet: function (d) {
                                        return toDecimalFour(d.bonus);
                                    },excel: {cellType: 'n'}},
                                {field: 'sumSalary', title: '总工资', width: 150, filter: true, totalRow: true,templet: function (d) {
                                        return toDecimalFour(d.sumSalary);
                                    },excel: {cellType: 'n'}}
                            ]]
                            ,page: true
                            ,height: 'full-100'
                            ,limits: [500, 1000, 2000]
                            ,limit: 1000 //每页默认显示的数量
                            ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                            ,defaultToolbar: ['filter', 'print']
                            ,excel: {
                                filename: '员工工资分组汇总.xlsx',
                                add: {
                                    top: {
                                        data: [
                                            ['德悦员工工资条'], //头部第一行数据，由于我设置了后面的数据merge了，就只写一个
                                            ["从"+from + "到"+to]
                                        ],
                                        heights: [30,15],
                                        merge: [['1,1','1,10'],['2,1','2,3']]
                                    }
                                }
                            }
                            ,data: salaryData   // 将新数据重新载入表格
                            ,overflow: 'tips'
                            ,done: function () {
                                soulTable.render(this);
                                layer.close(load);
                            }
                            ,filter: {
                                bottom: true,
                                clearFilter: false,
                                items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                            }
                        });



                        // layui.table.reload("groupSalaryTable", {
                        //     cols:[[
                        //         {type:'numbers', align:'center', title:'序号', width:80},
                        //         {field: 'groupName', title: '组名', width: 120},
                        //         {field: 'employeeNumber', title: '工号', width: 120, sort: true, filter: true, totalRowText: '合计'},
                        //         {field: 'employeeName', title: '姓名', width: 120, sort: true, filter: true},
                        //         {field: 'position', title: '职位', width: 100, sort: true, filter: true},
                        //         {field: 'pieceSalary', title: '计件工资', width: 180 , filter: true, totalRow: true,templet: function (d) {
                        //                 return toDecimalFour(d.pieceSalary);
                        //             },excel: {cellType: 'n'}},
                        //         {field: 'pieceSalaryTwo', title: '计件补贴', width: 180, filter: true, totalRow: true,templet: function (d) {
                        //                 return toDecimalFour(d.pieceSalaryTwo);
                        //             },excel: {cellType: 'n'}},
                        //         {field: 'hourSalary', title: '时工工资', width: 180, filter: true, totalRow: true,templet: function (d) {
                        //                 return toDecimalFour(d.hourSalary);
                        //             },excel: {cellType: 'n'}},
                        //         {field: 'bonus', title: '产值奖', width: 180, filter: true, totalRow: true,templet: function (d) {
                        //                 return toDecimalFour(d.bonus);
                        //             },excel: {cellType: 'n'}},
                        //         {field: 'sumSalary', title: '总工资', width: 150, filter: true, totalRow: true,templet: function (d) {
                        //                 return toDecimalFour(d.sumSalary);
                        //             },excel: {cellType: 'n'}}
                        //     ]]
                        //     ,excel: {
                        //         filename: '员工工资分组汇总.xlsx',
                        //         add: {
                        //             top: {
                        //                 data: [
                        //                     ['德悦员工工资条'], //头部第一行数据，由于我设置了后面的数据merge了，就只写一个
                        //                     ["从"+from + "到"+to]
                        //                 ],
                        //                 heights: [30,15],
                        //                 merge: [['1,1','1,10'],['2,1','2,3']]
                        //             }
                        //         }
                        //     }
                        //     ,data: salaryData   // 将新数据重新载入表格
                        //     ,overflow: 'tips'
                        //     ,done: function () {
                        //         soulTable.render(this);
                        //         layer.close(load);
                        //     }
                        //     ,filter: {
                        //         bottom: true,
                        //         clearFilter: false,
                        //         items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                        //     }
                        // });
                    }else if (res.type == "计时"){
                        table.render({
                            elem: '#groupSalaryTable'
                            ,totalRow: true
                            ,cols:[[
                                {type:'numbers', align:'center', title:'序号', width:80},
                                {field:'employeeNumber', title: '工号', width: 120, sort: true, filter: true, totalRowText: '合计'},
                                {field:'employeeName', title: '姓名', width: 120, sort: true, filter: true},
                                {field:'groupName', title: '组名', width: 120},
                                {field:'orderName', title: '款号', width: 120, sort: true, filter: true},
                                {field:'clothesVersionNumber', title: '单号', width: 120, sort: true, filter: true},
                                {field:'procedureNumber', title:'工序号', align:'center', width:90, sort: true, filter: true},
                                {field:'procedureName', title:'工序名', align:'center',width:120, sort: true, filter: true},
                                {field:'pieceCount', title:'数量', align:'center', width:90, sort: true, filter: true, totalRow: true,excel:{cellType: 'n'}},
                                {field:'price', title:'单价', align:'center', width:90, sort: true, filter: true,templet: function (d) {
                                        return toDecimalFour(d.price);
                                    },excel: {cellType: 'n'}},
                                {field:'salary', title:'金额', align:'center', width:90, sort: true, filter: true, totalRow: true,templet: function (d) {
                                        return toDecimalFour(d.salary);
                                    },excel: {cellType: 'n'}}
                            ]]
                            ,page: true
                            ,height: 'full-100'
                            ,limits: [500, 1000, 2000]
                            ,limit: 1000 //每页默认显示的数量
                            ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                            ,defaultToolbar: ['filter', 'print']
                            ,excel: {
                                filename: '时工分组汇总.xlsx',
                                add: {
                                    top: {
                                        data: [
                                            ['德悦员工时工汇总'], //头部第一行数据，由于我设置了后面的数据merge了，就只写一个
                                            ["从"+from + "到"+to]
                                        ],
                                        heights: [30,15],
                                        merge: [['1,1','1,10'],['2,1','2,3']]
                                    }
                                }
                            }
                            ,data: salaryData   // 将新数据重新载入表格
                            ,overflow: 'tips'
                            ,done: function () {
                                soulTable.render(this);
                                layer.close(load);
                            }
                            ,filter: {
                                bottom: true,
                                clearFilter: false,
                                items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                            }
                        });
                        // layui.table.reload("groupSalaryTable", {
                        //     cols:[[
                        //         {type:'numbers', align:'center', title:'序号', width:80},
                        //         {field:'employeeNumber', title: '工号', width: 120, sort: true, filter: true, totalRowText: '合计'},
                        //         {field:'employeeName', title: '姓名', width: 120, sort: true, filter: true},
                        //         {field:'groupName', title: '组名', width: 120},
                        //         {field:'orderName', title: '款号', width: 120, sort: true, filter: true},
                        //         {field:'clothesVersionNumber', title: '单号', width: 120, sort: true, filter: true},
                        //         {field:'procedureNumber', title:'工序号', align:'center', width:90, sort: true, filter: true},
                        //         {field:'procedureName', title:'工序名', align:'center',width:120, sort: true, filter: true},
                        //         {field:'pieceCount', title:'数量', align:'center', width:90, sort: true, filter: true, totalRow: true,excel:{cellType: 'n'}},
                        //         {field:'price', title:'单价', align:'center', width:90, sort: true, filter: true,templet: function (d) {
                        //                 return toDecimalFour(d.price);
                        //             },excel: {cellType: 'n'}},
                        //         {field:'salary', title:'金额', align:'center', width:90, sort: true, filter: true, totalRow: true,templet: function (d) {
                        //                 return toDecimalFour(d.salary);
                        //             },excel: {cellType: 'n'}}
                        //     ]]
                        //     ,excel: {
                        //         filename: '时工分组汇总.xlsx',
                        //         add: {
                        //             top: {
                        //                 data: [
                        //                     ['德悦员工时工汇总'], //头部第一行数据，由于我设置了后面的数据merge了，就只写一个
                        //                     ["从"+from + "到"+to]
                        //                 ],
                        //                 heights: [30,15],
                        //                 merge: [['1,1','1,10'],['2,1','2,3']]
                        //             }
                        //         }
                        //     }
                        //     ,data: salaryData   // 将新数据重新载入表格
                        //     ,overflow: 'tips'
                        //     ,done: function () {
                        //         soulTable.render(this);
                        //         layer.close(load);
                        //     }
                        //     ,filter: {
                        //         bottom: true,
                        //         clearFilter: false,
                        //         items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                        //     }
                        // });
                    }
                } else {
                    layer.msg("获取失败！", {icon: 2});
                }
            }, error: function () {
                layer.msg("获取失败！", {icon: 2});
            }
        });
        return false;
    });

    table.on('toolbar(groupSalaryTable)', function(obj) {
        if (obj.event === 'clearFilter') {
            // 清除所有筛选条件并重载表格
            // 参数: tableId
            soulTable.clearFilter('groupSalaryTable')
        } else if (obj.event === 'exportExcel') {
            soulTable.export('groupSalaryTable');
        }
    });

});


// $(document).ready(function () {
//     layui.laydate.render({
//         elem: '#from',
//         trigger: 'click'
//     });
//     layui.laydate.render({
//         elem: '#to',
//         trigger: 'click'
//     });
//
// });
//
// var hot;
//
// var basePath=$("#basePath").val();
// function search() {
//
//     var container = document.getElementById('addOrderExcel');
//     if(hot==undefined) {
//         hot = new Handsontable(container, {
//             rowHeaders: true,
//             colHeaders: true,
//             autoColumnSize: true,
//             dropdownMenu: true,
//             contextMenu: true,
//             // filters: true,
//             minCols: 20,
//             colWidths:[150,70,70,120,120,60,100,100,60,100],
//             language: 'zh-CN',
//             licenseKey: 'non-commercial-and-evaluation'
//         });
//     }
//
//     if($("#from").val()=="" || $("#from").val() == null) {
//         swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入开始日期！</span>",html: true});
//         return false;
//     }
//     if($("#to").val()=="" || $("#to").val() == null) {
//         swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入结束日期！</span>",html: true});
//         return false;
//     }
//     var from = $("#from").val();
//     var to = $("#to").val();
//     var salaryType = $("#salaryType").val();
//     $("#entities").empty();
//     $.blockUI({ css: {
//             border: 'none',
//             padding: '15px',
//             backgroundColor: '#000',
//             '-webkit-border-radius': '10px',
//             '-moz-border-radius': '10px',
//             opacity: .5,
//             color: '#fff'
//         },
//         message: "<h3>稍等一下... 奋力搜索中</h3>"
//     });
//     $.ajax({
//         url: basePath+"erp/getgroupsalarybytime",
//         type:'GET',
//         data: {
//             from:$("#from").val(),
//             to:$("#to").val(),
//             groupName:$("#groupName").val(),
//             salaryType:salaryType
//         },
//         success: function (data) {
//             $("#exportDiv").show();
//             var hotData = [];
//             var i = 0;
//             if (data){
//                 if (salaryType == "计时"){
//                     var empData = data["emp"];
//                     var tableHtml = "";
//                     var groupName = $("#groupName").val();
//                     var sumPieceCount = 0;
//                     var sumSalaryEmp  = 0;
//                     hotData[i++] = [["德悦时工产量汇总"]];
//                     hotData[i++] = [];
//                     hotData[i++] = [["组名"],groupName,[],[],[],[],["从"],from,["到"],to];
//                     hotData[i++] = [["工号"],["姓名"],["组名"],["订单号"],["版单号"],["工序号"],["工序名"],["数量"],["单价"],["金额"]];
//
//                     tableHtml = "<div class=\"col-md-12\" name='printTable'>\n" +
//                         "               <section class=\"panel panel-default\" style='page-break-after:always;'>\n" +
//                         "                   <header class=\"panel-heading font-bold\" style=\"text-align: center\">\n" +
//                         "                       <div style=\"font-size: 14px;font-weight: 700\">德悦时工产量汇总</div><br>\n" +
//                         "                       <div style=\"font-size: 14px;float: left;font-weight: 700\">组名："+groupName+"</div>\n"+
//                         "                       <div style=\"font-size: 14px;float: right;font-weight: 700\">从 "+from+" 到 "+to+"</div><br>\n"+
//                         "                   </header>\n" +
//                         "                   <table class=\"table table-bordered\" style='width: 100%'>\n" +
//                         "                       <tbody>";
//                     tableHtml += "<tr><td style='width: 12%'>工号</td><td style='width: 18%'>姓名</td><td style='width: 10%'>组名</td><td style='width: 5%'>订单</td><td style='width: 5%'>版单</td><td style='width: 10%'>工序号</td><td style='width: 10%'>工序名</td><td style='width: 10%'>数量</td><td style='width: 10%'>单价</td><td style='width: 10%'>金额</td></tr>";
//                     for (var empKey in empData){
//                         var empDetail = empData[empKey];
//                         var empIndex = 0;
//                         $.each(empDetail,function (index,item) {
//                             var tmp = [];
//                             tmp[0] = item.employeeNumber;
//                             tmp[1] = item.employeeName;
//                             tmp[2] = item.groupName;
//                             tmp[3] = [];
//                             tmp[4] = [];
//                             tmp[5] = item.procedureNumber;
//                             tmp[6] = item.procedureName;
//                             tmp[7] = item.pieceCount;
//                             tmp[8] = toDecimalFour(item.price);
//                             tmp[9] = toDecimal(item.salary);
//
//                             tableHtml += "<tr>\n" +
//                                 "             <td>"+item.employeeNumber+"</td>\n" +
//                                 "             <td>"+item.employeeName+"</td>\n" +
//                                 "             <td>"+item.groupName+"</td>\n" +
//                                 "             <td>"+" "+"</td>\n" +
//                                 "             <td>"+" "+"</td>\n" +
//                                 "             <td>"+item.procedureNumber+"</td>\n" +
//                                 "             <td>"+item.procedureName+"</td>\n" +
//                                 "             <td>"+item.pieceCount+"</td>\n" +
//                                 "             <td>"+toDecimalFour(item.price)+"</td>\n" +
//                                 "             <td>"+toDecimal(item.salary)+"</td>\n" +
//                                 "         </tr>";
//
//                             sumPieceCount += tmp[7];
//                             sumSalaryEmp += tmp[9];
//                             hotData[i++] = tmp;
//                             empIndex ++;
//                         });
//
//                     }
//                     tableHtml += "<tr><td colspan='7' style='text-align:center'>小计：</td><td>"+toDecimal(sumPieceCount)+"</td><td></td><td>"+toDecimal(sumSalaryEmp)+"</td></tr>" +
//                         "<tr><td colspan='7' style='text-align:center'>签字确认：</td><td colspan='3'></td></tr>";
//                     tableHtml +=  "                       </tbody>" +
//                         "                   </table>" +
//                         "               </section>" +
//                         "              </div>";
//                     $("#entities").append(tableHtml);
//
//                     hotData[i++] = [[],[],[],[],[],[],["小计"],sumPieceCount,[],toDecimal(sumSalaryEmp)];
//                     hotData[i++] = [[],[],[],[],[],[],["确认签字："]];
//                     hotData[i++] = [];
//                     hotData[i++] = [];
//                 }else if (salaryType == "计件"){
//                     var empData = data["emp"];
//                     var empCount = 0;
//                     var totalSalarySum = 0;
//                     var totalSalarySumTwo = 0;
//                     var groupName = $("#groupName").val();
//                     hotData[i++] = [[],[],[],[],[],[],["从"],from,["到"],to];
//                     hotData[i++] = [["工号"],["姓名"],["组名"],["订单号"],["版单号"],["工序号"],["工序名"],["数量"],["单价1"],["金额1"],["单价2"],["金额2"]];
//                     for (var empKey in empData){
//                         //每个员工做过的订单号
//                         var empOrderName = "";
//                         //每个员工做过的订单工序
//                         var empOrderProcedure = "";
//                         //该订单工序下的件数累计
//                         var sumEmpOrderProcedureCount = 0;
//                         //该订单工序下的工资1累计
//                         var sumEmpOrderProcedureSalary1 = 0;
//                         //该订单工序下的工资2累计
//                         var sumEmpOrderProcedureSalary2 = 0;
//                         empCount ++;
//                         var empDetail = empData[empKey];
//                         //统计总件数
//                         var sumPieceCount = 0;
//                         //统计总工资1
//                         var sumSalaryEmp = 0;
//                         //统计总工资2
//                         var sumSalaryEmpTwo = 0;
//                         //统计员工数量
//                         var empIndex = 0;
//                         var tableHtml = "";
//                         var tmp = [];
//                         var isBegin = true;
//                         if (empDetail[0].groupName.indexOf("合顺") != -1){
//                             $.each(empDetail,function (index,item) {
//
//                                 if (empIndex == 0){
//                                     tableHtml = "<div class=\"col-md-12\" name='printTable'>\n" +
//                                         "               <section class=\"panel panel-default\" style='page-break-after:always;'>\n" +
//                                         "                   <header class=\"panel-heading font-bold\" style=\"text-align: center\">\n" +
//                                         "                       <div style=\"font-size: 14px;font-weight: 700\">德悦员工产量汇总</div><br>\n" +
//                                         "                       <div style=\"font-size: 14px;float: left;font-weight: 700\">姓名："+item.employeeName+" 工号："+item.employeeNumber+"组名："+item.groupName+"</div>\n"+
//                                         "                       <div style=\"font-size: 14px;float: right;font-weight: 700\">从 "+from+" 到 "+to+"</div><br>\n"+
//                                         "                   </header>\n" +
//                                         "                   <table class=\"table table-bordered\" style='width: 100%'>\n" +
//                                         "                       <tbody>";
//                                     tableHtml += "<tr><td style='width: 12%'>订单号</td><td style='width: 12%'>版单号</td><td style='width: 7%'>工序号</td><td style='width: 10%'>工序名</td><td style='width: 7%'>数量</td><td style='width: 7%'>单价1</td><td style='width: 7%'>金额1</td><td style='width: 7%'>单价2</td><td style='width: 7%'>金额2</td></tr>";
//                                 }
//                                 empIndex ++;
//
//
//                                 if(isBegin){
//                                     empOrderName = item.orderName;
//                                     empOrderProcedure = item.procedureNumber;
//                                     sumEmpOrderProcedureCount += item.pieceCount;
//                                     sumEmpOrderProcedureSalary1 += 0;
//                                     sumEmpOrderProcedureSalary2 += 0;
//                                     tmp[0] = item.employeeNumber;
//                                     tmp[1] = item.employeeName;
//                                     tmp[2] = item.groupName;
//                                     tmp[3] = item.orderName;
//                                     tmp[4] = item.clothesVersionNumber;
//                                     tmp[5] = item.procedureNumber;
//                                     tmp[6] = item.procedureName;
//                                     tmp[7] = sumEmpOrderProcedureCount;
//                                     tmp[8] = toDecimalFour(0);
//                                     tmp[9] = toDecimal(0);
//                                     tmp[10] = toDecimalFour(0);
//                                     tmp[11] = toDecimal(0);
//                                     isBegin = false;
//                                 }else if (item.orderName == empOrderName && item.procedureNumber == empOrderProcedure){
//                                     sumEmpOrderProcedureCount += item.pieceCount;
//                                     sumEmpOrderProcedureSalary1 += 0;
//                                     sumEmpOrderProcedureSalary2 += 0;
//                                     tmp[0] = item.employeeNumber;
//                                     tmp[1] = item.employeeName;
//                                     tmp[2] = item.groupName;
//                                     tmp[3] = item.orderName;
//                                     tmp[4] = item.clothesVersionNumber;
//                                     tmp[5] = item.procedureNumber;
//                                     tmp[6] = item.procedureName;
//                                     tmp[7] = sumEmpOrderProcedureCount;
//                                     tmp[8] = toDecimalFour(0);
//                                     tmp[9] = toDecimal(0);
//                                     tmp[10] = toDecimalFour(0);
//                                     tmp[11] = toDecimal(0);
//                                 }else{
//                                     tableHtml += "<tr>\n" +
//                                         "             <td>"+tmp[3]+"</td>\n" +
//                                         "             <td>"+tmp[4]+"</td>\n" +
//                                         "             <td>"+tmp[5]+"</td>\n" +
//                                         "             <td>"+tmp[6]+"</td>\n" +
//                                         "             <td>"+tmp[7]+"</td>\n" +
//                                         "             <td>"+tmp[8]+"</td>\n" +
//                                         "             <td>"+tmp[9]+"</td>\n" +
//                                         "             <td>"+tmp[10]+"</td>\n" +
//                                         "             <td>"+tmp[11]+"</td>\n" +
//                                         "         </tr>";
//                                     sumPieceCount += tmp[7];
//                                     sumSalaryEmp += 0;
//                                     sumSalaryEmpTwo += 0;
//                                     hotData[i++] = [[tmp[0]],[tmp[1]],[tmp[2]],[tmp[3]],[tmp[4]],[tmp[5]],[tmp[6]],[tmp[7]],[0],[0],[0],[0]];
//                                     totalSalarySum += 0;
//                                     totalSalarySumTwo += 0;
//
//
//                                     empOrderName = item.orderName;
//                                     empOrderProcedure = item.procedureNumber;
//                                     sumEmpOrderProcedureCount = item.pieceCount;
//                                     sumEmpOrderProcedureSalary1 = 0;
//                                     sumEmpOrderProcedureSalary2 = 0;
//                                     tmp[0] = item.employeeNumber;
//                                     tmp[1] = item.employeeName;
//                                     tmp[2] = item.groupName;
//                                     tmp[3] = item.orderName;
//                                     tmp[4] = item.clothesVersionNumber;
//                                     tmp[5] = item.procedureNumber;
//                                     tmp[6] = item.procedureName;
//                                     tmp[7] = sumEmpOrderProcedureCount;
//                                     tmp[8] = toDecimalFour(0);
//                                     tmp[9] = toDecimal(0);
//                                     tmp[10] = toDecimalFour(0);
//                                     tmp[11] = toDecimal(0);
//                                 }
//
//
//                             });
//                             tableHtml += "<tr>\n" +
//                                 "             <td>"+tmp[3]+"</td>\n" +
//                                 "             <td>"+tmp[4]+"</td>\n" +
//                                 "             <td>"+tmp[5]+"</td>\n" +
//                                 "             <td>"+tmp[6]+"</td>\n" +
//                                 "             <td>"+tmp[7]+"</td>\n" +
//                                 "             <td>"+tmp[8]+"</td>\n" +
//                                 "             <td>"+tmp[9]+"</td>\n" +
//                                 "             <td>"+tmp[10]+"</td>\n" +
//                                 "             <td>"+tmp[11]+"</td>\n" +
//                                 "         </tr>";
//                             isBegin = true;
//                             sumPieceCount += tmp[7];
//                             sumSalaryEmp += 0;
//                             sumSalaryEmpTwo += 0;
//                             hotData[i++] = [[tmp[0]],[tmp[1]],[tmp[2]],[tmp[3]],[tmp[4]],[tmp[5]],[tmp[6]],[tmp[7]],[0],[0],[0],[0]];
//                             totalSalarySum += 0;
//                             totalSalarySumTwo += 0;
//
//                             sumEmpOrderProcedureCount = 0;
//                             //该订单工序下的工资1累计
//                             sumEmpOrderProcedureSalary1 = 0;
//                             //该订单工序下的工资2累计
//                             sumEmpOrderProcedureSalary2 = 0;
//                             tableHtml += "<tr><td colspan='4' style='text-align:center'>小计：</td><td>"+toDecimal(sumPieceCount)+"</td><td></td><td>"+toDecimal(sumSalaryEmp)+"</td><td></td><td>"+toDecimal(sumSalaryEmpTwo)+"</td></tr>" +
//                                 "<tr><td colspan='6' style='text-align:center'>签字确认：</td><td colspan='3'></td></tr>";
//                             tableHtml +=  "                       </tbody>" +
//                                 "                   </table>" +
//                                 "               </section>" +
//                                 "              </div>";
//                             $("#entities").append(tableHtml);
//                             hotData[i++] = [[],[],[],[],[],[],["小计"],sumPieceCount,[],toDecimal(sumSalaryEmp),[],toDecimal(sumSalaryEmpTwo)];
//                         }else {
//                             $.each(empDetail,function (index,item) {
//
//                                 if (empIndex == 0){
//                                     tableHtml = "<div class=\"col-md-12\" name='printTable'>\n" +
//                                         "               <section class=\"panel panel-default\" style='page-break-after:always;'>\n" +
//                                         "                   <header class=\"panel-heading font-bold\" style=\"text-align: center\">\n" +
//                                         "                       <div style=\"font-size: 14px;font-weight: 700\">德悦员工产量汇总</div><br>\n" +
//                                         "                       <div style=\"font-size: 14px;float: left;font-weight: 700\">姓名："+item.employeeName+" 工号："+item.employeeNumber+"组名："+item.groupName+"</div>\n"+
//                                         "                       <div style=\"font-size: 14px;float: right;font-weight: 700\">从 "+from+" 到 "+to+"</div><br>\n"+
//                                         "                   </header>\n" +
//                                         "                   <table class=\"table table-bordered\" style='width: 100%'>\n" +
//                                         "                       <tbody>";
//                                     tableHtml += "<tr><td style='width: 12%'>订单号</td><td style='width: 12%'>版单号</td><td style='width: 7%'>工序号</td><td style='width: 10%'>工序名</td><td style='width: 7%'>数量</td><td style='width: 7%'>单价1</td><td style='width: 7%'>金额1</td><td style='width: 7%'>单价2</td><td style='width: 7%'>金额2</td></tr>";
//                                 }
//                                 empIndex ++;
//
//
//                                 if(isBegin){
//                                     empOrderName = item.orderName;
//                                     empOrderProcedure = item.procedureNumber;
//                                     sumEmpOrderProcedureCount += item.pieceCount;
//                                     sumEmpOrderProcedureSalary1 += item.salary;
//                                     sumEmpOrderProcedureSalary2 += item.salaryTwo;
//                                     tmp[0] = item.employeeNumber;
//                                     tmp[1] = item.employeeName;
//                                     tmp[2] = item.groupName;
//                                     tmp[3] = item.orderName;
//                                     tmp[4] = item.clothesVersionNumber;
//                                     tmp[5] = item.procedureNumber;
//                                     tmp[6] = item.procedureName;
//                                     tmp[7] = sumEmpOrderProcedureCount;
//                                     tmp[8] = toDecimalFour(item.price);
//                                     tmp[9] = toDecimal(sumEmpOrderProcedureSalary1);
//                                     tmp[10] = toDecimalFour(item.priceTwo);
//                                     tmp[11] = toDecimal(sumEmpOrderProcedureSalary2);
//                                     isBegin = false;
//                                 }else if (item.orderName == empOrderName && item.procedureNumber == empOrderProcedure){
//                                     sumEmpOrderProcedureCount += item.pieceCount;
//                                     sumEmpOrderProcedureSalary1 += item.salary;
//                                     sumEmpOrderProcedureSalary2 += item.salaryTwo;
//                                     tmp[0] = item.employeeNumber;
//                                     tmp[1] = item.employeeName;
//                                     tmp[2] = item.groupName;
//                                     tmp[3] = item.orderName;
//                                     tmp[4] = item.clothesVersionNumber;
//                                     tmp[5] = item.procedureNumber;
//                                     tmp[6] = item.procedureName;
//                                     tmp[7] = sumEmpOrderProcedureCount;
//                                     tmp[8] = toDecimalFour(item.price);
//                                     tmp[9] = toDecimal(sumEmpOrderProcedureSalary1);
//                                     tmp[10] = toDecimalFour(item.priceTwo);
//                                     tmp[11] = toDecimal(sumEmpOrderProcedureSalary2);
//                                 }else{
//                                     tableHtml += "<tr>\n" +
//                                         "             <td>"+tmp[3]+"</td>\n" +
//                                         "             <td>"+tmp[4]+"</td>\n" +
//                                         "             <td>"+tmp[5]+"</td>\n" +
//                                         "             <td>"+tmp[6]+"</td>\n" +
//                                         "             <td>"+tmp[7]+"</td>\n" +
//                                         "             <td>"+tmp[8]+"</td>\n" +
//                                         "             <td>"+tmp[9]+"</td>\n" +
//                                         "             <td>"+tmp[10]+"</td>\n" +
//                                         "             <td>"+tmp[11]+"</td>\n" +
//                                         "         </tr>";
//                                     sumPieceCount += tmp[7];
//                                     sumSalaryEmp += tmp[9];
//                                     sumSalaryEmpTwo += tmp[11];
//                                     hotData[i++] = [[tmp[0]],[tmp[1]],[tmp[2]],[tmp[3]],[tmp[4]],[tmp[5]],[tmp[6]],[tmp[7]],[tmp[8]],[tmp[9]],[tmp[10]],[tmp[11]]];
//                                     totalSalarySum += tmp[9];
//                                     totalSalarySumTwo += tmp[11];
//
//
//                                     empOrderName = item.orderName;
//                                     empOrderProcedure = item.procedureNumber;
//                                     sumEmpOrderProcedureCount = item.pieceCount;
//                                     sumEmpOrderProcedureSalary1 = item.salary;
//                                     sumEmpOrderProcedureSalary2 = item.salaryTwo;
//                                     tmp[0] = item.employeeNumber;
//                                     tmp[1] = item.employeeName;
//                                     tmp[2] = item.groupName;
//                                     tmp[3] = item.orderName;
//                                     tmp[4] = item.clothesVersionNumber;
//                                     tmp[5] = item.procedureNumber;
//                                     tmp[6] = item.procedureName;
//                                     tmp[7] = sumEmpOrderProcedureCount;
//                                     tmp[8] = toDecimalFour(item.price);
//                                     tmp[9] = toDecimal(sumEmpOrderProcedureSalary1);
//                                     tmp[10] = toDecimalFour(item.priceTwo);
//                                     tmp[11] = toDecimal(sumEmpOrderProcedureSalary2);
//                                 }
//
//
//                             });
//                             tableHtml += "<tr>\n" +
//                                 "             <td>"+tmp[3]+"</td>\n" +
//                                 "             <td>"+tmp[4]+"</td>\n" +
//                                 "             <td>"+tmp[5]+"</td>\n" +
//                                 "             <td>"+tmp[6]+"</td>\n" +
//                                 "             <td>"+tmp[7]+"</td>\n" +
//                                 "             <td>"+tmp[8]+"</td>\n" +
//                                 "             <td>"+tmp[9]+"</td>\n" +
//                                 "             <td>"+tmp[10]+"</td>\n" +
//                                 "             <td>"+tmp[11]+"</td>\n" +
//                                 "         </tr>";
//                             isBegin = true;
//                             sumPieceCount += tmp[7];
//                             sumSalaryEmp += tmp[9];
//                             sumSalaryEmpTwo += tmp[11];
//                             hotData[i++] = [[tmp[0]],[tmp[1]],[tmp[2]],[tmp[3]],[tmp[4]],[tmp[5]],[tmp[6]],[tmp[7]],[tmp[8]],[tmp[9]],[tmp[10]],[tmp[11]]];
//                             totalSalarySum += tmp[9];
//                             totalSalarySumTwo += tmp[11];
//
//                             sumEmpOrderProcedureCount = 0;
//                             //该订单工序下的工资1累计
//                             sumEmpOrderProcedureSalary1 = 0;
//                             //该订单工序下的工资2累计
//                             sumEmpOrderProcedureSalary2 = 0;
//                             tableHtml += "<tr><td colspan='4' style='text-align:center'>小计：</td><td>"+toDecimal(sumPieceCount)+"</td><td></td><td>"+toDecimal(sumSalaryEmp)+"</td><td></td><td>"+toDecimal(sumSalaryEmpTwo)+"</td></tr>" +
//                                 "<tr><td colspan='6' style='text-align:center'>签字确认：</td><td colspan='3'></td></tr>";
//                             tableHtml +=  "                       </tbody>" +
//                                 "                   </table>" +
//                                 "               </section>" +
//                                 "              </div>";
//                             $("#entities").append(tableHtml);
//                             hotData[i++] = [[],[],[],[],[],[],["小计"],sumPieceCount,[],toDecimal(sumSalaryEmp),[],toDecimal(sumSalaryEmpTwo)];
//                         }
//                     }
//                     var summaryHtml = "";
//                     summaryHtml += "<div class=\"col-md-12\" name='printTable'>\n" +
//                         "               <section class=\"panel panel-default\" style='page-break-after:always;'>\n" +
//                         "                   <header class=\"panel-heading font-bold\" style=\"text-align: center\">\n" +
//                         "                       <div style=\"font-size: 14px;font-weight: 700\">员工产量分组汇总</div><br>\n" +
//                         "                       <div style=\"font-size: 14px;float: left;font-weight: 700\">组名："+groupName+"</div>\n"+
//                         "                       <div style=\"font-size: 14px;float: right;font-weight: 700\">从 "+from+" 到 "+to+"</div><br>\n"+
//                         "                   </header>\n" +
//                         "                   <table class=\"table table-bordered\" style='width: 100%'>\n" +
//                         "                       <tbody>";
//                     summaryHtml += "<tr><td style='width: 20%'>汇总</td><td style='width: 20%'>组名</td><td style='width: 20%'>员工总计</td><td style='width: 20%'>金额总计</td><td style='width: 20%'>补贴总计</td></tr>";
//                     summaryHtml += "<tr>\n" +
//                         "             <td>"+"分组汇总"+"</td>\n" +
//                         "             <td>"+groupName+"</td>\n" +
//                         "             <td>"+empCount+"</td>\n" +
//                         "             <td>"+toDecimal(totalSalarySum)+"</td>\n" +
//                         "             <td>"+toDecimal(totalSalarySumTwo)+"</td>\n" +
//                         "         </tr>";
//                     summaryHtml += "<tr><td colspan='2' style='text-align:center'>小计：</td><td>"+""+"</td><td>"+toDecimal(totalSalarySum)+"</td><td>"+toDecimal(totalSalarySumTwo)+"</td></tr>";
//                     summaryHtml +=  "                       </tbody>" +
//                         "                   </table>" +
//                         "               </section>" +
//                         "              </div>";
//                     $("#entities").append(summaryHtml);
//                 }else if (salaryType == "汇总"){
//                     var empData = data["emp"];
//                     var empCount = 0;
//                     var totalSalarySum = 0;
//                     var totalSalarySumTwo = 0;
//                     var totalHourSalarySum = 0;
//                     var totalSalary = 0;
//                     var bonusSum = 0;
//                     var groupName = $("#groupName").val();
//                     hotData[i++] = [[],[],[],[],[],[],["从"],from,["到"],to];
//                     hotData[i++] = [["序号"],["组名"],["工号"],["姓名"],["职位"],["计件工资"],["计件补贴"],["时工工资"],["产值奖"],["合计"]];
//                     var tableHtml = "<div class=\"col-md-12\" name='printTable'>\n" +
//                         "               <section class=\"panel panel-default\" style='page-break-after:always;'>\n" +
//                         "                   <header class=\"panel-heading font-bold\" style=\"text-align: center\">\n" +
//                         "                       <div style=\"font-size: 14px;font-weight: 700\">德悦员工工资条</div><br>\n" +
//                         "                       <div style=\"font-size: 14px;float: right;font-weight: 700\">从 "+from+" 到 "+to+"</div><br>\n"+
//                         "                   </header>\n" +
//                         "                   <table class=\"table table-bordered\" style='width: 100%'>\n" +
//                         "                       <tbody>";
//                     tableHtml += "<tr><td>序号</td><td>组名</td><td>工号</td><td>姓名</td><td>职位</td><td>计件工资</td><td>计件补贴</td><td>时工工资</td><td>产值奖</td><td>合计</td></tr>";
//                     for (var m=0; m<empData.length; m++){
//                         totalSalarySum += empData[m].pieceSalary;
//                         totalSalarySumTwo += empData[m].pieceSalaryTwo;
//                         totalHourSalarySum += empData[m].hourSalary;
//                         var personSumSalary = empData[m].pieceSalary + empData[m].pieceSalaryTwo + empData[m].hourSalary;
//
//                         var bonus = 0;
//                         if (personSumSalary < 2000){
//                             bonus = personSumSalary*0.08;
//                         }else{
//                             bonus = personSumSalary*0.12;
//                         }
//                         bonusSum += bonus;
//                         totalSalary += (personSumSalary + bonus);
//                         var tmpSalary = [[m+1],[empData[m].groupName],[empData[m].employeeNumber],[empData[m].employeeName],[empData[m].position],[empData[m].pieceSalary],[empData[m].pieceSalaryTwo],[empData[m].hourSalary],[bonus],[personSumSalary+bonus]];
//                         hotData[i++] = tmpSalary;
//
//                         tableHtml += "<tr>\n" +
//                             "             <td>"+(m+1)+"</td>\n" +
//                             "             <td>"+empData[m].groupName+"</td>\n" +
//                             "             <td>"+empData[m].employeeNumber+"</td>\n" +
//                             "             <td>"+empData[m].employeeName+"</td>\n" +
//                             "             <td>"+empData[m].position+"</td>\n" +
//                             "             <td>"+toDecimal(empData[m].pieceSalary)+"</td>\n" +
//                             "             <td>"+toDecimal(empData[m].pieceSalaryTwo)+"</td>\n" +
//                             "             <td>"+toDecimal(empData[m].hourSalary)+"</td>\n" +
//                             "             <td>"+toDecimal(bonus)+"</td>\n" +
//                             "             <td>"+toDecimal(bonus+personSumSalary)+"</td>\n" +
//                             "         </tr>";
//
//
//                     }
//                     hotData[i++] = [[],[],[],["合计"],[totalSalarySum],[totalSalarySumTwo],[totalHourSalarySum],[bonusSum],[totalSalary]];
//                     tableHtml += "<tr><td colspan='5' style='text-align:center'>合计：</td><td>"+toDecimal(totalSalarySum)+"</td><td>"+toDecimal(totalSalarySumTwo)+"</td><td>"+toDecimal(totalHourSalarySum)+"</td><td>"+toDecimal(bonusSum)+"</td><td>"+toDecimal(totalSalary)+"</td></tr>";
//                     tableHtml +=  "                       </tbody>" +
//                         "                   </table>" +
//                         "               </section>" +
//                         "              </div>";
//                     $("#entities").append(tableHtml);
//                 }
//
//             }
//             if(hotData.length > 0) {
//                 hot.loadData(hotData);
//             }
//
//             $.unblockUI();
//
//         },
//         error: function () {
//             swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
//         }
//     })
// }
//
// function exportData() {
//     var groupName = $("#groupName").val();
//     var from = $("#from").val();
//     var to = $("#to").val();
//     var title = groupName+'-'+from+'-'+to+'-分组产能';
//     var data = utl.Object.copyJson(hot.getData());//<=====读取handsontable的数据
//     var result = [];
//     var firstRow = new Array(data[0].length);
//     firstRow[0] = title;
//     for(var i=1;i<data[0].length;i++) {
//         firstRow[i] = "";
//     }
//     result.push(firstRow);
//     var col = 0;
//     $.each(data,function (index, item) {
//         for(var i=0;i<item.length;i++) {
//             if(item[i]==null) {
//                 item[i] = '';
//             }else if(index==1) {
//                 col++;
//             }
//         }
//         result.push(item);
//     });
//     utl.XLSX.onExport(result,"Sheet1","xlsx",title+".xlsx");//<====导出
// }
//
function toDecimal(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*100)/100;
    return f;
}

function toDecimalFour(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*10000)/10000;
    return f;
}

// function doPrint() {
//     var bdhtml=window.document.body.innerHTML;
//     var sprnstr="<!--startprint-->";
//     var eprnstr="<!--endprint-->";
//     var prnhtml=bdhtml.substr(bdhtml.indexOf(sprnstr)+17);
//     var prnhtml=prnhtml.substring(0,prnhtml.indexOf(eprnstr));
//     window.document.body.innerHTML=prnhtml;
//     window.print();
// }
//
// function printDeal(){
//     $("#printf").empty();
//     var printBoxs = document.getElementsByName('printTable');
//     var newContent = '';
//     for(var i=0;i<printBoxs.length;i++) {
//         newContent += printBoxs[i].innerHTML;
//     }
//     var f = document.getElementById('printf');
//     // f.contentDocument.write('<link rel="stylesheet" type="text/css" href="/css/app.v2.css">');
//     f.contentDocument.write('<style>' +
//         'table {' +
//         'border-collapse:collapse;' +
//         '}' +
//         'td {\n' +
//         '        border: 1px solid #000000;\n' +
//         '    }'+
//         '</style>');
//     f.contentDocument.write(newContent);
//     f.contentDocument.close();
//     window.frames['printf'].focus();
//     try{
//         window.frames['printf'].print();
//     }catch(err){
//         f.contentWindow.print();
//     }
// }
//
