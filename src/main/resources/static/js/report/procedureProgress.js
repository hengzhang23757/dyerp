layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.21'
}).extend();
var groupNameDemo;
$(document).ready(function () {
    layui.laydate.render({
        elem: '#from',
        trigger: 'click'
    });
    layui.laydate.render({
        elem: '#to',
        trigger: 'click'
    });
    groupNameDemo = xmSelect.render({
        el: '#groupName',
        filterable: true,
        toolbar: {
            show: true
        },
        data: []
    });
    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumber", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });
    $.ajax({
        url: "/erp/getallgroupnamelist",
        data: {},
        success:function(data){
            if (data.code == 0) {
                var groupData = [];
                if (data.data) {
                    $.each(data.data, function(index,element){
                        var tmpGroup = {};
                        tmpGroup.name = element;
                        tmpGroup.value = element;
                        groupData.push(tmpGroup);
                    })
                }
                groupNameDemo.update({
                    data: groupData
                });
            }
        },
        error:function(){}
    });
});
var hot;
var basePath=$("#basePath").val();
function search() {
    if($("#from").val().trim()=="") {
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入开始日期！</span>",html: true});
        return false;
    }
    if($("#to").val().trim()=="") {
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入结束日期！</span>",html: true});
        return false;
    }
    var from = $("#from").val();
    var to = $("#to").val();
    var data = {};
    data.from = from;
    data.to = to;
    if ($("#procedureFrom").val() != null && $("#procedureFrom").val() != ""){
        data.procedureFrom = $("#procedureFrom").val();
    }
    if ($("#procedureTo").val() != null && $("#procedureTo").val() != ""){
        data.procedureTo = $("#procedureTo").val();
    }
    if ($("#orderName").val() != null && $("#orderName").val() != ""){
        data.orderName = $("#orderName").val();
    }
    if ($("#employeeNumber").val() != null && $("#employeeNumber").val() != ""){
        data.employeeNumber = $("#employeeNumber").val();
    }
    var groupNameList = [];
    $.each(groupNameDemo.getValue(), function(index,element){
        groupNameList.push(element.value);
    });
    if (groupNameList.length > 0){
        data.groupList = groupNameList
    }

    $.blockUI({ css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        },
        message: "<h3>稍等一下... 奋力搜索中</h3>"
    });
    $("#entities").empty();

    $.ajax({
        url: basePath+"erp/getprocedureprogressmultigroup",
        type:'GET',
        data: data,
        success: function (data) {
            $("#exportDiv").show();
            var totalSumPiece = 0;
            var totalSumSalary = 0;
            $("#entities").empty();
            if (data){
                var tableHtml = "<div class=\"col-md-12\" name='printTable'>\n" +
                    "               <section class=\"panel panel-default\" style='page-break-after:always;'>\n" +
                    "                   <header class=\"panel-heading font-bold\" style=\"text-align: center\">\n" +
                    "                       <div style=\"font-size: 16px;float: left;font-weight: 700\">订单工序进度详情</div><div style=\"font-size: 16px;float: right;font-weight: 700\">从 "+from+" 到 "+to+"</div><br>\n"+
                    "                   </header>\n" +
                    "                   <table class=\"table table-bordered\" style='width: 100%' id='detailProcedureTable'>\n" +
                    "                       <tbody>\n" +
                    "<tr><td>工号</td><td>姓名</td><td>组别</td><td>日期</td><td>订单</td><td>版单</td><td>工序号</td><td>工序名</td><td>数量</td><td>单价</td><td>工资</td></tr>";

                for (var empKey in data){
                    var empDetail = data[empKey];
                    var sumPieceCount = 0;
                    var sumSalaryEmp  = 0;
                    for (var j=0; j < empDetail.length; j++){
                        var item = empDetail[j];
                        tableHtml += "<tr><td>" + item.employeeNumber + "</td><td>" + item.employeeName + "</td><td>" + item.groupName + "</td><td>"+ transDate(item.generalDate) +"</td><td>"+item.orderName+ "</td><td>"+item.clothesVersionNumber+"</td><td>"+item.procedureNumber+"</td><td>"+item.procedureName+"</td><td>"+item.pieceCount+"</td><td>"+toDecimalFour(item.price)+"</td><td>"+toDecimal(item.salary)+"</td></tr>";
                        sumPieceCount += item.pieceCount;
                        sumSalaryEmp += toDecimal(item.salary);
                    }
                    totalSumPiece += sumPieceCount;
                    totalSumSalary += sumSalaryEmp;
                    tableHtml += "<tr><td colspan='8' style='text-align:center'>小计：</td><td>"+sumPieceCount+"件</td><td></td><td>"+toDecimal(sumSalaryEmp)+"元</td></tr>";

                }
                tableHtml += "<tr><td colspan='11' style='text-align:center'>汇总</td></tr>"+
                    "<tr><td colspan='8' style='text-align:center'>总计：</td><td>"+totalSumPiece+"件</td><td></td><td>"+toDecimal(totalSumSalary)+"元</td></tr>"+
                    "                       </tbody>" +
                    "                   </table>" +
                    "               </section>" +
                    "              </div>";
                $("#entities").append(tableHtml);
                $("#exportButton").click(function () {
                    ExportToExcel("进度详情");
                })

            }
            $.unblockUI();
        },
        error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }
    });

}


function ExportToExcel(fileName) {
    $("#detailProcedureTable").table2excel({
        exclude : ".noExl", //过滤位置的 css 类名
        filename : fileName + new Date().getTime(), //文件名称
        name: fileName+".xlsx",
        exclude_img: false,//是否导出图片 false导出
        exclude_links: true,//是否导出链接 false导出
        exclude_inputs: true//是否导出输入框的值 true导出
    });
}



function toDecimal(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*100)/100;
    return f;
}

function toDecimalFour(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*10000)/10000;
    return f;
}


function transDate(data) {return  moment(data).format("YYYY-MM-DD");}


function printDeal(){
    $("#printf").empty();
    var printBoxs = document.getElementsByName('printTable');
    var newContent = '';
    for(var i=0;i<printBoxs.length;i++) {
        newContent += printBoxs[i].innerHTML;
    }
    var f = document.getElementById('printf');
    // f.contentDocument.write('<link rel="stylesheet" type="text/css" href="/css/app.v2.css">');
    f.contentDocument.write('<style>' +
        'table {' +
        'border-collapse:collapse;' +
        '}' +
        'td {\n' +
        '        border: 1px solid #000000;\n' +
        '    }'+
        '</style>');
    f.contentDocument.write(newContent);
    f.contentDocument.close();
    window.frames['printf'].focus();
    try{
        window.frames['printf'].print();
    }catch(err){
        f.contentWindow.print();
    }
}
