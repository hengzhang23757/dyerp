var hot;
var basePath=$("#basePath").val();

$(document).ready(function () {
    layui.use(['autocomplete'], function () {
        layui.autocomplete.render({
            elem: $('#clothesVersionNumber')[0],
            url: 'getversionhint',
            template_val: '{{d}}',
            template_txt: '{{d}}',
            onselect: function (resp) {
                autoComplete(resp)
            }
        })
    });
});

function autoComplete(keywords) {
    $("#orderName").empty();
    $.ajax({
        url: "/erp/getorderbyversion",
        data: {"clothesVersionNumber": keywords},
        success:function(data){
            if (data.orderList) {
                $("#orderName").empty();
                $.each(data.orderList, function(index,element){
                    $("#orderName").append("<option value='"+element+"'>"+element+"</option>");
                })
            }
        },
        error:function(){
        }
    });
}

function search() {
    var container = document.getElementById('addOrderExcel');
    if(hot==undefined) {
        hot = new Handsontable(container, {
            // data: data,
            rowHeaders: true,
            colHeaders: true,
            autoColumnSize: true,
            dropdownMenu: true,
            contextMenu: true,
            filters:true,
            // minRows: 20,
            minCols: 18,
            stretchH: 'all',
            autoWrapRow: true,
            height: $(document.body).height() - 180,
            manualRowResize: true,
            manualColumnResize: true,
            colWidths:[100,100,50,100,50,50,50,50,50,50,50,50,50,50,50,50,50,50],
            language: 'zh-CN',
            licenseKey: 'non-commercial-and-evaluation'
        });
    }


    if($("#orderName").val().trim()=="") {
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入要查询的订单号！</span>",html: true});
        return false;
    }
    $.ajax({
        url: basePath+"erp/getcutbedcolorbyorder",
        type:'GET',
        data: {
            orderName:$("#orderName").val(),
        },
        success: function (data) {
            $("#exportDiv").show();
            var hotData = [];
            if(data) {
                console.log(data);
                var sizeRow = [];
                var totalIndex = 0;
                var orderTotalSum = 0;
                var orderSummary = [];
                orderSummary[0] = [];
                orderSummary[1] = [];
                orderSummary[2] = [];
                orderSummary[3] = ["total"];
                var totalSum = 0;
                var totalSummary = [];
                for (var orderColorKey in data["orderInfo"]){
                    var orderData = data["orderInfo"];
                    var orderSizeData = orderData[orderColorKey];
                    var orderColorRow = [];
                    var orderColorSum = 0;
                    orderColorRow[0] = [];
                    orderColorRow[1] = [orderColorKey];
                    orderColorRow[2] = [];
                    orderColorRow[3] = [];
                    var orderSizeIndex = 4;
                    for (var orderSizeKey in orderSizeData){
                        if (totalIndex == 0){
                            sizeRow[0] = ["制单"];
                            sizeRow[1] = ["颜色"];
                            sizeRow[2] = ["床号"];
                            sizeRow[3] = ["日期"];
                            sizeRow.push(orderSizeKey);
                            orderSummary[orderSizeIndex] = 0;
                        }
                        orderSummary[orderSizeIndex] += orderSizeData[orderSizeKey];
                        orderColorRow.push(orderSizeData[orderSizeKey]);
                        orderColorSum += orderSizeData[orderSizeKey];
                        orderSizeIndex ++;
                    }
                    if (totalIndex == 0){
                        sizeRow.push("合计");
                        sizeRow.push("备注");
                        hotData.push(sizeRow);
                    }
                    orderColorRow.push(orderColorSum);
                    orderTotalSum += orderColorSum;
                    totalIndex ++;
                    hotData.push(orderColorRow);
                }
                orderSummary.push(orderTotalSum);
                hotData.push(orderSummary);
                hotData.push([]);
                for(var colorKey in data["cutInfo"]) {
                    var cutData = data["cutInfo"];
                    var bedData = cutData[colorKey];
                    var bedIndex = 0;
                    var cutColorIndex = 0;
                    var cutColorSum = 0;
                    var cutColorSummary = [];
                    cutColorSummary[0] = [];
                    cutColorSummary[1] = [];
                    cutColorSummary[2] = [];
                    cutColorSummary[3] = ["total"];
                    for (var bedKey in bedData){
                        var bedRow = [];
                        bedRow[0] = [];
                        var bedTime = bedKey.split("#");
                        bed = bedTime[0];
                        time = bedTime[1];
                        groupName = bedTime[2];
                        var bedSum = 0;
                        if (bedIndex == 0){
                            bedRow[0] = ["裁数"];
                            bedRow[1] = [colorKey];
                        }else{
                            bedRow[1] = [];
                        }
                        bedRow.push(bed);
                        bedRow.push(time);
                        var sizeData = bedData[bedKey];
                        var cutSizeIndex = 4;
                        for (var sizeKey in sizeData){
                            if (cutColorIndex == 0){
                                cutColorSummary[cutSizeIndex] = 0;
                            }
                            bedRow.push(sizeData[sizeKey]);
                            cutColorSummary[cutSizeIndex] += sizeData[sizeKey];
                            bedSum += sizeData[sizeKey];
                            cutSizeIndex ++;
                        }
                        cutColorSum += bedSum;
                        bedRow.push(bedSum);
                        bedRow.push(groupName);
                        hotData.push(bedRow);

                        totalSum += bedSum;
                        totalIndex ++;
                        bedIndex ++;
                        cutColorIndex ++;
                    }
                    cutColorSummary.push(cutColorSum);
                    hotData.push(cutColorSummary);
                    hotData.push([]);

                }
                totalSummary.push("总计");
                totalSummary.push(totalSum);
                hotData.push(totalSummary);
                hotData.push([]);
                hotData.push([["裁数汇总"]]);
                var tailorData = data["tailorInfo"];
                var tailorSummary = [];
                tailorSummary[0] = [];
                tailorSummary[1] = [];
                tailorSummary[2] = [];
                tailorSummary[3] = ["total"];
                var tailorColorIndex = 0;
                var tailorSum = 0;
                for(var tailorColorKey in tailorData) {
                    var tailorSizeData = tailorData[tailorColorKey];
                    var colorRow = [];
                    colorRow[0] = ["裁数"];
                    colorRow[1] = [tailorColorKey];
                    colorRow[2] = [];
                    colorRow[3] = [];
                    var colorSum = 0;
                    var tailorSizeIndex = 4;
                    for (var tailorSizeKey in tailorSizeData){
                        colorRow.push(tailorSizeData[tailorSizeKey]);
                        colorSum += tailorSizeData[tailorSizeKey];
                        if (tailorColorIndex == 0){
                            tailorSummary[tailorSizeIndex] = tailorSizeData[tailorSizeKey];
                        }else{
                            tailorSummary[tailorSizeIndex] += tailorSizeData[tailorSizeKey];
                        }
                        tailorSizeIndex ++;
                    }
                    colorRow.push(colorSum);
                    hotData.push(colorRow);
                    tailorSum += colorSum;
                    tailorColorIndex ++;
                }
                tailorSummary.push(tailorSum);
                hotData.push(tailorSummary);

                hotData.push([]);
                hotData.push([["裁数对比"]]);
                var diffData = data["diffInfo"];
                var diffSummary = [];
                diffSummary[0] = [];
                diffSummary[1] = [];
                diffSummary[2] = [];
                diffSummary[3] = ["total"];
                var diffColorIndex = 0;
                var diffSum = 0;
                for(var diffColorKey in diffData) {
                    var diffSizeData = diffData[diffColorKey];
                    var diffColorRow = [];
                    diffColorRow[0] = ["裁数对比"];
                    diffColorRow[1] = [diffColorKey];
                    diffColorRow[2] = [];
                    diffColorRow[3] = [];
                    var diffColorSum = 0;
                    var diffSizeIndex = 4;
                    for (var diffSizeKey in diffSizeData){
                        diffColorRow.push(diffSizeData[diffSizeKey]);
                        diffColorSum += diffSizeData[diffSizeKey];
                        if (diffColorIndex == 0){
                            diffSummary[diffSizeIndex] = diffSizeData[diffSizeKey];
                        }else{
                            diffSummary[diffSizeIndex] += diffSizeData[diffSizeKey];
                        }
                        diffSizeIndex ++;
                    }
                    diffColorRow.push(diffColorSum);
                    hotData.push(diffColorRow);
                    diffSum += diffColorSum;
                    diffColorIndex ++;
                }
                diffSummary.push(diffSum);
                hotData.push(diffSummary);

                hotData.push([]);
                hotData.push([["对比百分数"]]);
                var percentData = data["percentInfo"];
                var percentSummary = [];
                percentSummary[0] = [];
                percentSummary[1] = [];
                percentSummary[2] = [];
                percentSummary[3] = ["total"];
                var percentColorIndex = 0;
                var percentSum = 0;
                for(var percentColorKey in percentData) {
                    var percentSizeData = percentData[percentColorKey];
                    var percentColorRow = [];
                    percentColorRow[0] = ["百分数"];
                    percentColorRow[1] = [percentColorKey];
                    percentColorRow[2] = [];
                    percentColorRow[3] = [];
                    var percentColorSum = 0;
                    var percentSizeIndex = 4;
                    for (var percentSizeKey in percentSizeData){
                        percentColorRow.push(toPercent(percentSizeData[percentSizeKey]));
                        percentColorSum += percentSizeData[percentSizeKey];
                        if (percentColorIndex == 0){
                            percentSummary[percentSizeIndex] = percentSizeData[percentSizeKey];
                        }else{
                            percentSummary[percentSizeIndex] += percentSizeData[percentSizeKey];
                        }
                        percentSizeIndex ++;
                    }
                    percentColorRow.push(toPercent(percentColorSum));
                    hotData.push(percentColorRow);
                    percentSum += percentColorSum;
                    percentColorIndex ++;
                }
                for (var i = 4; i<percentSummary.length; i++){
                    percentSummary[i] = toPercent(percentSummary[i]);
                }
                percentSummary.push(toPercent(percentSum));
                hotData.push(percentSummary);
            }
            hot.loadData(hotData);
        },
        error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }
    })
}

function toPercent(point){
    var str=Number(point*100).toFixed(1);
    str+="%";
    return str;
}


function exportData() {
    var data = hot.getData();
    var result = [];
    var col = 0;
    $.each(data,function (index, item) {
        // if(item[0]!=null) {
        for(var i=0;i<item.length;i++) {
            if(item[i]==null) {
                item[i] = '';
            }else if(index==1) {
                col++;
            }
        }
        result.push(item);
        // }else {
        //     return false;
        // }
    });
    var orderName = $("#orderName").val();
    var clothesVersionNumber = $("#clothesVersionNumber").val();
    var myDate = new Date();
    export2Excel([orderName+"-"+clothesVersionNumber+'-裁床分组明细-'+myDate.toLocaleString()],col, result, orderName+"-"+clothesVersionNumber+'-裁床分组明细-'+myDate.toLocaleString()+".xls")
}