var hot;
var dom = document.getElementById("container");
var myChart = echarts.init(dom);
layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.21'
}).extend();
$(document).ready(function () {

    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumber", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });

});

var basePath=$("#basePath").val();
var myDate = new Date();

function search() {
    if($("#orderName").val().trim()=="") {
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入订单！</span>",html: true});
        return false;
    }
    if($("#type").val().trim()=="") {
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请选择类型！</span>",html: true});
        return false;
    }

    $.blockUI({ css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        },
        message: "<h3>稍等一下... 奋力搜索中</h3>"
    });
    orderName = $("#orderName").val();
    var typeValue = $("#type").val();
    if (typeValue == "detail"){
        $.ajax({
            url: basePath + "erp/getreworkreport",
            type:'GET',
            data: {
                orderName:orderName
            },
            success: function (data) {
                if(data) {
                    createReWorkTable(data.reWorkList);
                    $.unblockUI();
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，没有查到相关信息！</span>",html: true});
                }
            },
            error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        })
    }
    if (typeValue == "summary"){
        $.ajax({
            url: basePath + "erp/getreworksummaryreport",
            type:'GET',
            data: {
                orderName:orderName
            },
            success: function (data) {
                if(data) {
                    createReWorkSummaryTable(data.reWorkList);
                    $.unblockUI();
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，没有查到相关信息！</span>",html: true});
                }
            },
            error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        })
    }

    if (typeValue == "figure"){
        $.ajax({
            url: basePath + "erp/getreworksummaryreport",
            type:'GET',
            data: {
                orderName:orderName
            },
            success: function (data) {
                if(data) {
                    createReWorkFigure(data.reWorkList);
                    $.unblockUI();
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，没有查到相关信息！</span>",html: true});
                }
            },
            error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        })
    }

}

var reWorkTable;
var myDate = new Date();
function createReWorkTable(data) {
    $("#resultTable").show();
    $("#resultSummaryTable").hide();
    $("#reWorkFigure").hide();
    if (reWorkTable != undefined) {
        reWorkTable.clear(); //清空一下table
        reWorkTable.destroy(); //还原初始化了的datatable
    }
    reWorkTable = $('#reWorkTable').DataTable({
        "data":data,
        language : {
            processing : "载入中",//处理页面数据的时候的显示
            paginate : {//分页的样式文本内容。
                previous : "上一页",
                next : "下一页",
                first : "第一页",
                last : "最后一页"
            },
            search:"搜索：",
            zeroRecords : "没有内容",//table tbody内容为空时，tbody的内容。
            //下面三者构成了总体的左下角的内容。
            info : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//左下角的信息显示，大写的词为关键字。
            infoEmpty : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//筛选为空时左下角的显示。
            infoFiltered : ""//筛选之后的左下角筛选提示(另一个是分页信息显示，在上面的info中已经设置，所以可以不显示)，
        },
        dom: 'Bfrtip',
        "buttons": [{
            'extend': 'excel',
            'text': '导出',//定义导出excel按钮的文字
            'className': 'btn', //按钮的class样式
            'title': '返工查询'+myDate.toLocaleString( ),
            'exportOptions': {
                'modifier': {
                    'page': 'all'
                }
            }
        }],
        pageLength : 25,// 每页显示10条数据
        pagingType : "full_numbers",
        "paging": true,
        "info": true,
        searching:true,
        lengthChange:true,
        scrollX: 2000,
        scrollCollapse: true,
        scroller:       true,
        "columns": [
            {
                "data": null,
                "title":"序号",
                "width":"45px",
                "defaultContent": "",
                "sClass": "text-center",
                "searchable":false,
                render: function (data, type, row, meta) {
                    var no = meta.settings._iDisplayStart + meta.row + 1;
                    return no;
                }
            },{
                "data": "clothesVersionNumber",
                "title":"版单号",
                "width":"120px",
                "defaultContent": "",
                "sClass": "text-center",
            },{
                "data": "orderName",
                "title":"订单号",
                "width":"120px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "customer",
                "title":"客户名",
                "width":"90px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "description",
                "title":"款式描述",
                "width":"120px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "reWorkDate",
                "title":"返工日期",
                "width":"90px",
                "defaultContent": "",
                "sClass": "text-center",
                "render": function(data, type, full, meta) {
                    return  moment(data).format("YYYY-MM-DD");
                }
            }, {
                "data": "groupName",
                "title":"组名",
                "width":"60px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "procedureNumber",
                "title":"工序号",
                "width":"60px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "procedureName",
                "title":"工序名",
                "width":"60px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "reWorkCount",
                "title":"返工量",
                "width":"60px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "reWorkRate",
                "title":"返工率",
                "width":"60px",
                "defaultContent": "",
                "sClass": "text-center",
                "render": function(data, type, full, meta) {
                    var str=Number(data*100).toFixed(1);
                    str+="%";
                    return str;
                }
            }, {
                "data": "wellRate",
                "title":"合格率",
                "width":"60px",
                "defaultContent": "",
                "sClass": "text-center",
                "render": function(data, type, full, meta) {
                    var str=Number(data*100).toFixed(1);
                    str+="%";
                    return str;
                }
            }
        ]
    });
}

var reWorkSummaryTable;
function createReWorkSummaryTable(data) {
    $("#resultSummaryTable").show();
    $("#resultTable").hide();
    $("#reWorkFigure").hide();
    if (reWorkSummaryTable != undefined) {
        reWorkSummaryTable.clear(); //清空一下table
        reWorkSummaryTable.destroy(); //还原初始化了的datatable
    }
    reWorkSummaryTable = $('#reWorkSummaryTable').DataTable({
        "data":data,
        language : {
            processing : "载入中",//处理页面数据的时候的显示
            paginate : {//分页的样式文本内容。
                previous : "上一页",
                next : "下一页",
                first : "第一页",
                last : "最后一页"
            },
            search:"搜索：",
            zeroRecords : "没有内容",//table tbody内容为空时，tbody的内容。
            //下面三者构成了总体的左下角的内容。
            info : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//左下角的信息显示，大写的词为关键字。
            infoEmpty : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//筛选为空时左下角的显示。
            infoFiltered : ""//筛选之后的左下角筛选提示(另一个是分页信息显示，在上面的info中已经设置，所以可以不显示)，
        },
        dom: 'Bfrtip',
        "buttons": [{
            'extend': 'excel',
            'text': '导出',//定义导出excel按钮的文字
            'className': 'btn', //按钮的class样式
            'title': '返工汇总'+myDate.toLocaleString( ),
            'exportOptions': {
                'modifier': {
                    'page': 'all'
                }
            }
        }],
        pageLength : 25,// 每页显示10条数据
        pagingType : "full_numbers",
        "paging": true,
        "info": true,
        searching:true,
        lengthChange:true,
        scrollX: 2000,
        scrollCollapse: true,
        scroller:       true,
        "columns": [
            {
                "data": null,
                "title":"序号",
                "width":"45px",
                "defaultContent": "",
                "sClass": "text-center",
                "searchable":false,
                render: function (data, type, row, meta) {
                    var no = meta.settings._iDisplayStart + meta.row + 1;
                    return no;
                }
            },{
                "data": "clothesVersionNumber",
                "title":"版单号",
                "width":"110px",
                "defaultContent": "",
                "sClass": "text-center",
            },{
                "data": "orderName",
                "title":"订单号",
                "width":"120px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "customer",
                "title":"客户名",
                "width":"90px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "description",
                "title":"款式描述",
                "width":"120px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "procedureNumber",
                "title":"工序号",
                "width":"60px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "procedureName",
                "title":"工序名",
                "width":"60px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "reWorkCount",
                "title":"返工量",
                "width":"60px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "reWorkRate",
                "title":"返工率",
                "width":"60px",
                "defaultContent": "",
                "sClass": "text-center",
                "render": function(data, type, full, meta) {
                    var str=Number(data*100).toFixed(1);
                    str+="%";
                    return str;
                }
            }, {
                "data": "wellRate",
                "title":"合格率",
                "width":"60px",
                "defaultContent": "",
                "sClass": "text-center",
                "render": function(data, type, full, meta) {
                    var str=Number(data*100).toFixed(1);
                    str+="%";
                    return str;
                }
            }
        ]
    });
}

function createReWorkFigure(data){
    $("#reWorkFigure").show();
    $("#resultTable").hide();
    $("#resultSummaryTable").hide();
    var app = {};
    option = null;
    app.title = '环形图';
    var nameList = [];
    var reworkNameDataList = [];
    for (var i = 0; i < data.length; i++){
        var reWorkData = {};
        nameList.push(data[i]["procedureNumber"]+"-"+data[i]["procedureName"]);
        reWorkData.name = data[i]["procedureNumber"]+"-"+data[i]["procedureName"];
        reWorkData.value = data[i]["reWorkCount"];
        reworkNameDataList.push(reWorkData);
    }
    console.log(nameList);
    console.log(reworkNameDataList);
    option = {
        tooltip: {
            trigger: 'item',
            formatter: "{a} <br/>{b}: {c}件 (占比{d}%)"
        },
        legend: {
            orient: 'vertical',
            x: 'left',
            data:nameList
        },
        series: [
            {
                name:'返工统计',
                type:'pie',
                radius: ['50%', '70%'],
                avoidLabelOverlap: false,
                label: {
                    normal: {
                        show: false,
                        position: 'center'
                    },
                    emphasis: {
                        show: true,
                        textStyle: {
                            fontSize: '30',
                            fontWeight: 'bold'
                        }
                    }
                },
                labelLine: {
                    normal: {
                        show: false
                    }
                },
                data:reworkNameDataList
            }
        ]
    };
    if (option && typeof option === "object") {
        myChart.setOption(option, true);
    }
    tools.loopShowTooltip(myChart, option, {loopSeries: true});
    window.onresize = myChart.resize;
}