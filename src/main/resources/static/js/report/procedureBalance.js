var dom = document.getElementById("container");
var myChart = echarts.init(dom);
layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.21'
}).extend();
$(document).ready(function () {
    layui.laydate.render({
        elem: '#from',
        trigger: 'click'
    });
    layui.laydate.render({
        elem: '#to',
        trigger: 'click'
    });

    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumber", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });
});

var basePath=$("#basePath").val();
var orderName,from,to,groupName;

function search() {
    if($("#orderName").val()=="" || $("#orderName").val()==null) {
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入订单！</span>",html: true});
        return false;
    }

    $("#procedureDetail").empty();
    $("#procedureDetail").hide();
    $("#personProcedureDetail").hide();
    $("#personProcedureDetail").empty();
    $("#procedureInformation").show();
    orderName = $("#orderName").val();
    from = $("#from").val();
    to = $("#to").val();
    groupName = $("#groupName").val();
    $.blockUI({ css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        },
        message: "<h3>稍等一下... 奋力搜索中</h3>"
    });
    $.ajax({
        url: basePath + "erp/getproductionprogressbyordertimegroup",
        type:'GET',
        data: {
            orderName:orderName,
            from:from,
            to:to,
            groupName:groupName
        },
        success: function (data) {
            if(data) {
                createProcedureBalanceTable(data.productionProgressList);
            }else {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，没有查到相关信息！</span>",html: true});
            }
        },
        error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }
    });
    $.ajax({
        url: basePath + "erp/geteachpersonproduction",
        type:'GET',
        data: {
            orderName:orderName,
            from:from,
            to:to,
            groupName:groupName
        },
        success: function (data) {
            if(data) {
                createProcedureBalanceFigure(data);
            }else {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，没有查到相关信息！</span>",html: true});
            }
            $.unblockUI();
        },
        error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }
    });


}
var hot;
var basePath=$("#basePath").val();
var procedureBalanceTable;
var myDate = new Date();
function createProcedureBalanceTable(data) {
    if (procedureBalanceTable != undefined) {
        procedureBalanceTable.clear(); //清空一下table
        procedureBalanceTable.destroy(); //还原初始化了的datatable
    }
    procedureBalanceTable = $('#procedureBalanceTable').DataTable({
        "data":data,
        language : {
            processing : "载入中",//处理页面数据的时候的显示
            paginate : {//分页的样式文本内容。
                previous : "上一页",
                next : "下一页",
                first : "第一页",
                last : "最后一页"
            },
            search:"搜索：",
            zeroRecords : "没有内容",//table tbody内容为空时，tbody的内容。
            //下面三者构成了总体的左下角的内容。
            info : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//左下角的信息显示，大写的词为关键字。
            infoEmpty : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//筛选为空时左下角的显示。
            infoFiltered : ""//筛选之后的左下角筛选提示(另一个是分页信息显示，在上面的info中已经设置，所以可以不显示)，
        },
        pageLength : 200,
        // pagingType : "full_numbers",
        "paging": true,
        "info": true,
        searching:false,
        lengthChange:false,
        scrollX: 2000,
        scrollCollapse: true,
        scroller:       true,
        "columns": [
            {
                "data": "procedureNumber",
                "title":"工序号",
                "width":"45px",
                "defaultContent": "",
                "sClass": "text-center"
            }, {
                "data": "procedureName",
                "title":"工序名",
                "width":"80px",
                "defaultContent": "",
                "sClass": "text-center"
            }, {
                "data": "orderCount",
                "title":"订单量",
                "width":"45px",
                "defaultContent": "",
                "sClass": "text-center"
            }, {
                "data": "cutCount",
                "title":"好片数",
                "width":"45px",
                "defaultContent": "",
                "sClass": "text-center"
            }, {
                "data": "productionCount",
                "title":"生产量",
                "width":"45px",
                "defaultContent": "",
                "sClass": "text-center"
            }, {
                "data": "differenceCount",
                "title":"差异",
                "width":"30px",
                "defaultContent": "",
                "sClass": "text-center"
            }
        ]
    });

    $('#procedureBalanceTable tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        } else {
            procedureBalanceTable.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    } );

    $('#button').click( function () {
        table.row('.selected').remove().draw( false );
    } );

    $('#procedureBalanceTable').on('click', 'tr',function() {
        var data = procedureBalanceTable.row(this).data(); //获取单击那一行的数据
        var procedureName = data.procedureName;
        var procedureNumber = data.procedureNumber;
        var wellCount = data.cutCount;
        var workCount = data.productionCount;
        var achievePercent = 0;
        if (workCount > 0){
            achievePercent = toDecimal(workCount/wellCount * 100);
        }
        var procedureDescription = data.procedureDescription;
        var tableHtml = "";
        if (data.procedureNumber == 0){
            $.ajax({
                url: "/erp/getotherproductionprogressdetailbyordertime",
                type:'GET',
                data: {
                    orderName:orderName,
                    from:from,
                    to:to,
                    procedureName:procedureName
                },
                success: function (data) {
                    if (data){
                        $("#procedureDetail").show();
                        $("#procedureDetail").empty();
                        $("#personProcedureDetail").hide();
                        $("#personProcedureDetail").empty();
                        var colorData = data["color"];
                        var colorIndex = 0;
                        var sizeSum = [];
                        var sizeSumIndex = 2;
                        for (var color in colorData){
                            var row1 = "<tr><td style='width: 60px'>"+color+"</td><td style='width: 60px'>"+"订单量"+"</td>";
                            var row2 = "<tr><td style='width: 60px'></td><td style='width: 60px'>"+"好片数"+"</td>";
                            var row3 = "<tr><td style='width: 60px'></td><td style='width: 60px'>"+"生产量"+"</td>";
                            var row4 = "<tr><td style='width: 60px'></td><td style='width: 60px'>"+"未生产"+"</td>";
                            if (colorIndex == 0){
                                tableHtml = "<div name='printTable'>\n" +
                                    "               <section class=\"panel panel-default\" style='page-break-after:always;'>\n" +
                                    "                   <header class=\"panel-heading font-bold\" style=\"text-align: center\">\n" +
                                    "                       <div style=\"font-size: 14px; align-items: center;margin-top:10px;font-weight: 700\">工序号："+procedureNumber+" 工序："+procedureName+"描述："+procedureDescription+"</div>\n"+
                                    "                   </header>\n" +
                                    "                   <table class='table table-bordered' style='margin-top:10px;border: 2px;font-weight: 700;white-space: nowrap;'>\n" +
                                    "                       <tbody>";
                                tableHtml += "<tr style='color: red;'><td style='width: 60px'></td><td style='width: 60px'></td>";
                            }
                            var colorSizeSum1 = 0;
                            var colorSizeSum2 = 0;
                            var colorSizeSum3 = 0;
                            var colorSizeSum4 = 0;
                            var sizeData = colorData[color];
                            var thisSizeSumIndex = 2;
                            var sizeDataList = [];
                            for (var i in sizeData){
                                var sizeDataItem = {};
                                sizeDataItem.sizeName = i;
                                sizeDataItem.sizeDataDetail = sizeData[i];
                                sizeDataList.push(sizeDataItem);
                            }
                            sizeDataList = globalSizeDataSort(sizeDataList);
                            if (procedureNumber == 1){
                                for (var i in sizeDataList){
                                    if (colorIndex == 0){
                                        tableHtml += "<td style='width: 40px'>"+sizeDataList[i].sizeName+"</td>";
                                        sizeSum[thisSizeSumIndex] = sizeDataList[i].sizeDataDetail.cutCount;
                                        sizeSumIndex ++;
                                    }else{
                                        sizeSum[thisSizeSumIndex] += sizeDataList[i].sizeDataDetail.cutCount;
                                    }
                                    row1 += "<td style='width: 40px'>"+sizeDataList[i].sizeDataDetail.orderCount+"</td>";
                                    row2 += "<td style='width: 40px'>"+sizeDataList[i].sizeDataDetail.cutCount+"</td>";
                                    row3 += "<td style='width: 40px'>"+sizeDataList[i].sizeDataDetail.cutCount+"</td>";
                                    row4 += "<td style='width: 40px'>"+0+"</td>";
                                    colorSizeSum1 += sizeDataList[i].sizeDataDetail.orderCount;
                                    colorSizeSum2 += sizeDataList[i].sizeDataDetail.cutCount;
                                    colorSizeSum3 += sizeDataList[i].sizeDataDetail.cutCount;
                                    colorSizeSum4 += 0;
                                    thisSizeSumIndex ++;
                                }
                                if (colorIndex == 0){
                                    tableHtml += "<td style='width: 40px'>"+"合计"+"</td>";
                                }
                            }else{
                                for (var i in sizeDataList){
                                    if (colorIndex == 0){
                                        tableHtml += "<td style='width: 40px'>"+sizeDataList[i].sizeName+"</td>";
                                        sizeSum[thisSizeSumIndex] = sizeDataList[i].sizeDataDetail.productionCount;
                                        sizeSumIndex ++;
                                    }else{
                                        sizeSum[thisSizeSumIndex] += sizeDataList[i].sizeDataDetail.productionCount;
                                    }
                                    row1 += "<td style='width: 40px'>"+sizeDataList[i].sizeDataDetail.orderCount+"</td>";
                                    row2 += "<td style='width: 40px'>"+sizeDataList[i].sizeDataDetail.cutCount+"</td>";
                                    row3 += "<td style='width: 40px'>"+sizeDataList[i].sizeDataDetail.productionCount+"</td>";
                                    row4 += "<td style='width: 40px'>"+sizeDataList[i].sizeDataDetail.leakCount+"</td>";
                                    colorSizeSum1 += sizeDataList[i].sizeDataDetail.orderCount;
                                    colorSizeSum2 += sizeDataList[i].sizeDataDetail.cutCount;
                                    colorSizeSum3 += sizeDataList[i].sizeDataDetail.productionCount;
                                    colorSizeSum4 += sizeDataList[i].sizeDataDetail.leakCount;
                                    thisSizeSumIndex ++;
                                }
                                if (colorIndex == 0){
                                    tableHtml += "<td style='width: 40px'>"+"合计"+"</td>";
                                }
                            }

                            row1 += "<td style='width: 40px'>"+colorSizeSum1+"</td></tr>";
                            row2 += "<td style='width: 40px'>"+colorSizeSum2+"</td></tr>";
                            row3 += "<td style='width: 40px'>"+colorSizeSum3+"</td></tr>";
                            row4 += "<td style='width: 40px'>"+colorSizeSum4+"</td></tr>";
                            tableHtml += row1;
                            tableHtml += row2;
                            tableHtml += row3;
                            tableHtml += row4;
                            colorIndex ++;
                        }
                        tableHtml += "<tr style='color: red;'><td style='width: 60px'>"+""+"</td><td style='width: 40px'>"+"尺码生产量"+"</td>";
                        var totalSum = 0;
                        for (var i = 2; i< sizeSumIndex; i++){
                            totalSum += sizeSum[i];
                            tableHtml += "<td style='width: 40px'>"+sizeSum[i]+"</td>";
                        }
                        tableHtml += "<td style='width: 40px'>"+totalSum+"</td></tr>";
                        tableHtml +=  "                       </tbody>" +
                            "                   </table>" +
                            "               </section>" +
                            "              </div>";
                        $("#procedureDetail").append(tableHtml);
                    }
                },
                error: function () {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
                }
            });
        }else{
            $.ajax({
                url: "/erp/getproductionprogressdetailbyordertimegroup",
                type:'GET',
                data: {
                    orderName:orderName,
                    from:from,
                    to:to,
                    groupName:groupName,
                    procedureNumber:procedureNumber
                },
                success: function (data) {
                    if (data){
                        $("#procedureDetail").show();
                        $("#procedureDetail").empty();
                        $("#personProcedureDetail").show();
                        $("#personProcedureDetail").empty();
                        var colorData = data["color"];
                        var colorIndex = 0;
                        var sizeSum = [];
                        var sizeSumIndex = 2;
                        for (var color in colorData){
                            var row1 = "<tr><td style='width: 60px'>"+color+"</td><td style='width: 60px'>"+"订单量"+"</td>";
                            var row2 = "<tr><td style='width: 60px'></td><td style='width: 60px'>"+"好片数"+"</td>";
                            var row3 = "<tr><td style='width: 60px'></td><td style='width: 60px'>"+"生产量"+"</td>";
                            var row4 = "<tr><td style='width: 60px'></td><td style='width: 60px'>"+"未生产"+"</td>";
                            if (colorIndex == 0){
                                tableHtml = "<div name='printTable'>\n" +
                                    "               <section class=\"panel panel-default\" style='page-break-after:always;'>\n" +
                                    "                   <header class=\"panel-heading font-bold\" style=\"text-align: center\">\n" +
                                    "                       <div style=\"font-size: larger; font-weight: bolder; color: #1E9FFF; align-items: center;margin-top:10px;\">工序号:"+procedureNumber+"&nbsp;&nbsp;&nbsp;工序:"+procedureName+"&nbsp;&nbsp;&nbsp;描述:"+procedureDescription +"&nbsp;&nbsp;&nbsp;完成比例:"+achievePercent +"%</div>\n"+
                                    "                   </header>\n" +
                                    "                   <table class='table table-bordered' style='margin-top:10px;border: 2px;font-weight: 700;white-space: nowrap;'>\n" +
                                    "                       <tbody>";
                                tableHtml += "<tr style='color: red;'><td style='width: 60px'></td><td style='width: 60px'></td>";
                            }
                            var colorSizeSum1 = 0;
                            var colorSizeSum2 = 0;
                            var colorSizeSum3 = 0;
                            var colorSizeSum4 = 0;
                            var sizeData = colorData[color];
                            var thisSizeSumIndex = 2;
                            var sizeDataList = [];
                            for (var i in sizeData){
                                var sizeDataItem = {};
                                sizeDataItem.sizeName = i;
                                sizeDataItem.sizeDataDetail = sizeData[i];
                                sizeDataList.push(sizeDataItem);
                            }
                            sizeDataList = globalSizeDataSort(sizeDataList);
                            if (procedureNumber == 1 || procedureNumber == 3){

                                for(var i in sizeDataList){
                                    if (colorIndex == 0){
                                        tableHtml += "<td style='width: 40px'>"+sizeDataList[i].sizeName+"</td>";
                                        sizeSum[thisSizeSumIndex] = sizeDataList[i].sizeDataDetail.cutCount;
                                        sizeSumIndex ++;
                                    }else{
                                        sizeSum[thisSizeSumIndex] += sizeDataList[i].sizeDataDetail.cutCount;
                                    }
                                    row1 += "<td style='width: 40px'>"+sizeDataList[i].sizeDataDetail.orderCount+"</td>";
                                    row2 += "<td style='width: 40px'>"+sizeDataList[i].sizeDataDetail.cutCount+"</td>";
                                    row3 += "<td style='width: 40px'>"+sizeDataList[i].sizeDataDetail.cutCount+"</td>";
                                    row4 += "<td style='width: 40px'>"+0+"</td>";
                                    colorSizeSum1 += sizeDataList[i].sizeDataDetail.orderCount;
                                    colorSizeSum2 += sizeDataList[i].sizeDataDetail.cutCount;
                                    colorSizeSum3 += sizeDataList[i].sizeDataDetail.cutCount;
                                    colorSizeSum4 += 0;
                                    thisSizeSumIndex ++;
                                }
                                if (colorIndex == 0){
                                    tableHtml += "<td style='width: 40px'>"+"合计"+"</td>";
                                }
                            }else{
                                for (var i in sizeDataList){
                                    if (colorIndex == 0){
                                        tableHtml += "<td style='width: 40px'>"+sizeDataList[i].sizeName+"</td>";
                                        sizeSum[thisSizeSumIndex] = sizeDataList[i].sizeDataDetail.productionCount;
                                        sizeSumIndex ++;
                                    }else{
                                        sizeSum[thisSizeSumIndex] += sizeDataList[i].sizeDataDetail.productionCount;
                                    }
                                    row1 += "<td style='width: 40px'>"+sizeDataList[i].sizeDataDetail.orderCount+"</td>";
                                    row2 += "<td style='width: 40px'>"+sizeDataList[i].sizeDataDetail.cutCount+"</td>";
                                    row3 += "<td style='width: 40px'>"+sizeDataList[i].sizeDataDetail.productionCount+"</td>";
                                    row4 += "<td style='width: 40px'>"+sizeDataList[i].sizeDataDetail.leakCount+"</td>";
                                    colorSizeSum1 += sizeDataList[i].sizeDataDetail.orderCount;
                                    colorSizeSum2 += sizeDataList[i].sizeDataDetail.cutCount;
                                    colorSizeSum3 += sizeDataList[i].sizeDataDetail.productionCount;
                                    colorSizeSum4 += sizeDataList[i].sizeDataDetail.leakCount;
                                    thisSizeSumIndex ++;
                                }
                                if (colorIndex == 0){
                                    tableHtml += "<td style='width: 40px'>"+"合计"+"</td>";
                                }
                            }

                            row1 += "<td style='width: 40px'>"+colorSizeSum1+"</td></tr>";
                            row2 += "<td style='width: 40px'>"+colorSizeSum2+"</td></tr>";
                            row3 += "<td style='width: 40px'>"+colorSizeSum3+"</td></tr>";
                            row4 += "<td style='width: 40px'>"+colorSizeSum4+"</td></tr>";
                            tableHtml += row1;
                            tableHtml += row2;
                            tableHtml += row3;
                            tableHtml += row4;
                            colorIndex ++;
                        }
                        tableHtml += "<tr style='color: red;'><td style='width: 60px'>"+""+"</td><td style='width: 40px'>"+"尺码生产量"+"</td>";
                        var totalSum = 0;
                        for (var i = 2; i< sizeSumIndex; i++){
                            totalSum += sizeSum[i];
                            tableHtml += "<td style='width: 40px'>"+sizeSum[i]+"</td>";
                        }
                        tableHtml += "<td style='width: 40px'>"+totalSum+"</td></tr>";
                        tableHtml +=  "                       </tbody>" +
                            "                   </table>" +
                            "               </section>" +
                            "              </div>";
                        $("#procedureDetail").append(tableHtml);


                        var personTableHtml = "<div name='printTable'>\n" +
                            "               <section class=\"panel panel-default\" style='page-break-after:always;'>\n" +
                            "                   <header class=\"panel-heading font-bold\" style=\"text-align: center\">\n" +
                            "                       <div style=\"font-size: larger; font-weight: bolder; color: #1E9FFF; align-items: center;margin-top:10px;\">工序号："+procedureNumber+" 工序："+procedureName+"描述："+procedureDescription+"</div>\n"+
                            "                   </header>\n" +
                            "                   <table class='table table-bordered' style='margin-top:10px;border: 2px;font-weight: 700;white-space: nowrap;'>\n" +
                            "                       <tbody>";

                        //员工信息
                        var personData = data["person"];
                        personTableHtml += "<tr><td>组名</td><td>工号</td><td>姓名</td><td>数量</td>"
                        for (var k=0; k<personData.length; k++){
                            personTableHtml += "<tr><td>"+personData[k]["groupName"]+"</td>";
                            personTableHtml += "<td>"+personData[k]["employeeNumber"]+"</td>";
                            personTableHtml += "<td>"+personData[k]["employeeName"]+"</td>";
                            personTableHtml += "<td>"+personData[k]["productionCount"]+"</td></tr>";
                        }
                        personTableHtml +=  "                       </tbody>" +
                            "                   </table>" +
                            "               </section>" +
                            "              </div>";
                        $("#personProcedureDetail").append(personTableHtml);

                        // $("#personProcedureDetail").append(personHtml);
                    }
                },
                error: function () {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
                }
            });
        }

    } );

}

//
// function detail(procedureNumber) {
//     var urlName = encodeURIComponent(orderName);
//     var tabName = orderName.replace(/[&\|\\\*^%$#@\-+~`！!……——=「」{};:：；‘’\"\"/<>.,，。《》]/g,"");
//     $('#mainFrameTabs').bTabsAdd("tabId" + tabName, "详情信息", "/erp/procedureBalanceDetail?orderName="+urlName+"&from="+from+"&to="+to+"&groupName="+groupName+"&procedureNumber="+procedureNumber);
// }

function createProcedureBalanceFigure(data){
    var xSeries = [];
    var personSeries = [];
    for (var procedureKey in data){
        xSeries.push(procedureKey);
        var personData = data[procedureKey];
        for (var personKey in personData){
            if (!personSeries.includes(personKey)){
                personSeries.push(personKey);
            }
        }
    }
    var series = [];
    $.each(personSeries, function(index,element){
        var seriesObject={};
        seriesObject.name=element;
        seriesObject.type='bar';
        seriesObject.barWidth='20';
        seriesObject.stack='汇总';
        var seriesObjectData = [];
        for (var procedureKey in data){
            if (data[procedureKey][element] != null){
                seriesObjectData.push(data[procedureKey][element]);
            }else{
                seriesObjectData.push(0);
            }
        }
        seriesObject.data=seriesObjectData;
        series.push(seriesObject);
    });

    var app = {};
    option = null;
    app.title = '堆叠柱状图';

    option = {
        toolbox:{
            feature : {
                saveAsImage : {}
            }
        },
        tooltip : {
            trigger: 'axis',
            formatter: function(params){
                var resultText = '';
                resultText += params[0].name + "<br/>";
                for(var x in params){
                    if (params[x].value > 0){
                        resultText += params[x].seriesName +":"+params[x].value + "<br/>";
                    }else{
                        resultText += '';
                    }
                }
                return resultText;

            },
            axisPointer : {            // 坐标轴指示器，坐标轴触发有效
                type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
            }

        },
        grid: {
            left: '3%',
            right: '4%',
            bottom: '3%',
            containLabel: true
        },
        xAxis : [
            {
                type : 'category',
                data : xSeries,
                axisLabel:{
                    interval:0,
                    rotate:40
                }
            }
        ],
        yAxis : [
            {
                type : 'value'
            }
        ],
        series : series
    };
    if (series.length > 0){
        tools.loopShowTooltip(myChart, option, {loopSeries: true});
        if (option && typeof option === "object") {
            myChart.setOption(option, true);
        }
    }
    window.onresize = myChart.resize;
}



function toDecimal(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*100)/100;
    return f;
}