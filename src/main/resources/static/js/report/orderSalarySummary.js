var hot;
var basePath=$("#basePath").val();
layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.21'
}).extend();
$(document).ready(function () {

    layui.laydate.render({
        elem: '#from',
        trigger: 'click'
    });
    layui.laydate.render({
        elem: '#to',
        trigger: 'click'
    });

    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumber", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });
});

function search() {
    $.blockUI({ css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        },
        message: "<h3>稍等一下... 奋力搜索中</h3>"
    });
    var container = document.getElementById('addOrderExcel');
    if(hot==undefined) {
        hot = new Handsontable(container, {
            rowHeaders: true,
            colHeaders: true,
            autoColumnSize: true,
            dropdownMenu: true,
            contextMenu: true,
            // filters: true,
            minCols: 11,
            colWidths:[150,70,70,120,120,60,100,100,60,100],
            language: 'zh-CN',
            licenseKey: 'non-commercial-and-evaluation'
        });
    }

    if($("#orderName").val().trim()=="") {
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入订单！</span>",html: true});
        return false;
    }
    $("#entities").empty();
    $.ajax({
        url: basePath+"erp/getordersalarysummary",
        type:'GET',
        data: {
            orderName:$("#orderName").val(),
            from:$("#from").val(),
            to:$("#to").val(),
        },
        success: function (data) {
            $("#exportDiv").show();
            var hotData = [];
            var i = 0;
            if (data.orderSalarySummary){
                var generalSalary = data.orderSalarySummary;
                var totalSalarySum = 0;
                var totalSalarySumTwo = 0;
                hotData[i++] = [["德悦订单工资汇总"]];
                hotData[i++] = [];
                hotData[i++] = [["订单号"],["版单号"],["日期"],["类型"],["工序号"],["工序名"],["工段"],["数量"],["单价1"],["金额1"],["单价2"],["金额2"]];
                tableHtml = "<div class=\"col-md-12\" name='printTable'>\n" +
                    "               <section class=\"panel panel-default\" style='page-break-after:always;'>\n" +
                    "                   <header class=\"panel-heading font-bold\" style=\"text-align: center\">\n" +
                    "                       <div style=\"font-size: 14px;font-weight: 700\">德悦订单工资汇总</div><br>\n" +
                    "                   </header>\n" +
                    "                   <table class=\"table table-bordered\" style='width: 100%'>\n" +
                    "                       <tbody>";
                tableHtml += "<tr><td style='width: 10%'>订单号</td><td style='width: 10%'>版单号</td><td style='width: 10%'>日期</td><td style='width: 10%'>类型</td><td style='width: 7%'>工序号</td><td style='width: 10%'>工序名</td><td style='width: 7%'>工段</td><td style='width: 7%'>数量</td><td style='width: 7%'>单价1</td><td style='width: 7%'>金额1</td><td style='width: 7%'>单价2</td><td style='width: 7%'>金额2</td></tr>";
                $.each(generalSalary,function (index,item) {
                    var tmp = [];
                    tmp[0] = item.orderName;
                    tmp[1] = item.clothesVersionNumber;
                    tmp[2] = transDate(item.generalDate);
                    tmp[3] = item.salaryType;
                    tmp[4] = item.procedureNumber;
                    tmp[5] = item.procedureName;
                    tmp[6] = item.procedureSection;
                    tmp[7] = item.pieceCount;
                    tmp[8] = toDecimalFour(item.price);
                    tmp[9] = toDecimal(item.salary);
                    tmp[10] = toDecimalFour(item.priceTwo);
                    tmp[11] = toDecimal(item.salaryTwo);

                    tableHtml += "<tr>\n" +
                        "             <td>"+item.orderName+"</td>\n" +
                        "             <td>"+item.clothesVersionNumber+"</td>\n" +
                        "             <td>"+transDate(item.generalDate)+"</td>\n" +
                        "             <td>"+item.salaryType+"</td>\n" +
                        "             <td>"+item.procedureNumber+"</td>\n" +
                        "             <td>"+item.procedureName+"</td>\n" +
                        "             <td>"+item.procedureSection+"</td>\n" +
                        "             <td>"+item.pieceCount+"</td>\n" +
                        "             <td>"+toDecimalFour(item.price)+"</td>\n" +
                        "             <td>"+toDecimal(item.salary)+"</td>\n" +
                        "             <td>"+toDecimalFour(item.priceTwo)+"</td>\n" +
                        "             <td>"+toDecimal(item.salaryTwo)+"</td>\n" +
                        "         </tr>";

                    totalSalarySum += tmp[9];
                    totalSalarySumTwo += tmp[11];
                    hotData[i++] = tmp;
                });
                tableHtml += "<tr><td colspan='9' style='text-align:center'>小计：</td><td>"+toDecimal(totalSalarySum)+"</td><td></td><td>"+toDecimal(totalSalarySumTwo)+"</td></tr>"+
                    "<tr><td colspan='9' style='text-align:center'>总计：</td><td colspan='3'>"+toDecimal(totalSalarySum+totalSalarySumTwo)+"</td></tr>";
                tableHtml +=  "                       </tbody>" +
                    "                   </table>" +
                    "               </section>" +
                    "              </div>";
                $("#entities").append(tableHtml);

                hotData[i++] = [[],[],[],[],[],[],[],[],["小计"],toDecimal(totalSalarySum),[],toDecimal(totalSalarySumTwo)];
                hotData[i++] = [["总计"],toDecimal(totalSalarySum+totalSalarySumTwo)];

            }
            if(hotData.length > 0) {
                hot.loadData(hotData);
            }

            $.unblockUI();

        },
        error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }
    })
}

function exportData() {
    var groupName = $("#groupName").val();
    var from = $("#from").val();
    var to = $("#to").val();
    var title = groupName+'-'+from+'-'+to+'-分组产能';
    var data = utl.Object.copyJson(hot.getData());//<=====读取handsontable的数据
    var result = [];
    var firstRow = new Array(data[0].length);
    firstRow[0] = title;
    for(var i=1;i<data[0].length;i++) {
        firstRow[i] = "";
    }
    result.push(firstRow);
    var col = 0;
    $.each(data,function (index, item) {
        for(var i=0;i<item.length;i++) {
            if(item[i]==null) {
                item[i] = '';
            }else if(index==1) {
                col++;
            }
        }
        result.push(item);
    });
    utl.XLSX.onExport(result,"Sheet1","xlsx",title+".xlsx");//<====导出
}

function toDecimal(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*100)/100;
    return f;
}

function toDecimalFour(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*10000)/10000;
    return f;
}

function printDeal(){
    $("#printf").empty();
    var printBoxs = document.getElementsByName('printTable');
    var newContent = '';
    for(var i=0;i<printBoxs.length;i++) {
        newContent += printBoxs[i].innerHTML;
    }
    var f = document.getElementById('printf');
    // f.contentDocument.write('<link rel="stylesheet" type="text/css" href="/css/app.v2.css">');
    f.contentDocument.write('<style>' +
        'table {' +
        'border-collapse:collapse;' +
        '}' +
        'td {\n' +
        '        border: 1px solid #000000;\n' +
        '    }'+
        '</style>');
    f.contentDocument.write(newContent);
    f.contentDocument.close();
    window.frames['printf'].focus();
    try{
        window.frames['printf'].print();
    }catch(err){
        f.contentWindow.print();
    }
}

function transDate(data) {return  moment(data).format("YYYY-MM-DD");};