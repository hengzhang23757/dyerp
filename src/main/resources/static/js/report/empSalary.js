var form;
layui.use('form', function(){
    form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
    form.render();
});


$(document).ready(function () {
    layui.laydate.render({
        elem: '#from',
        trigger: 'click'
    });
    layui.laydate.render({
        elem: '#to',
        trigger: 'click'
    });

    layui.use(['autocomplete'], function () {
        layui.autocomplete.render({
            elem: $('#employeeNumber')[0],
            url: 'getemphint',
            template_val: '{{d.employeeNumber}}',
            template_txt: '{{d.employeeNumber}}',
            onselect: function (resp) {
                autoComplete(resp.employeeNumber);
            }
        })
    });

});

function autoComplete(employeeNumber) {
    $("#employeeName").empty();
    $.ajax({
        url: "/erp/getemployeebyemployeenumber",
        data: {"employeeNumber": employeeNumber},
        success:function(data){
            $("#employeeName").empty();
            if (data.employeeList) {
                $("#employeeName").val(data.employeeList[0].employeeName);
                $("#groupName").val(data.employeeList[0].groupName);
            }
        }, error:function(){}
    });
}

layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.21'
}).extend({                         // 模块别名
    soulTable: 'soulTable'
});

var empSalaryTable;

layui.use(['form', 'soulTable', 'table'], function () {
    var table = layui.table,
        soulTable = layui.soulTable,
        $ = layui.$;

    $(".layui-form-select,.layui-input").css("padding-right",0);

    empSalaryTable = table.render({
        elem: '#empSalaryTable'
        ,cols:[[
            {type:'numbers', align:'center', title:'序号', width:80}
            ,{field:'groupName', title:'组名', align:'center', width:100, sort: true, filter: true}
            ,{field:'employeeNumber', title:'工号', align:'center', width:100, sort: true, filter: true}
            ,{field:'employeeName', title:'姓名', align:'center', width:100, sort: true, filter: true}
            ,{field:'orderName', title:'款号', align:'center', width:150, sort: true, filter: true}
            ,{field:'clothesVersionNumber', title:'单号', align:'center', width:150, sort: true, filter: true}
            ,{field:'procedureNumber', title:'工序号', align:'center', width:90, sort: true, filter: true}
            ,{field:'procedureName', title:'工序名', align:'center',width:180, sort: true, filter: true}
            ,{field:'pieceCount', title:'数量', align:'center', width:120, sort: true, filter: true, totalRow: true,excel:{cellType: 'n'}}
            ,{field:'price', title:'单价1', align:'center', width:120, sort: true, filter: true,templet: function (d) {
                    return toDecimalFour(d.price);
                },excel: {cellType: 'n'}}
            ,{field:'salary', title:'金额1', align:'center', width:120, sort: true, filter: true, totalRow: true,templet: function (d) {
                    return toDecimalFour(d.salary);
                },excel: {cellType: 'n'}}
            ,{field:'priceTwo', title:'单价2', align:'center', width:120, sort: true, filter: true,templet: function (d) {
                    return toDecimalFour(d.priceTwo);
                },excel: {cellType: 'n'}}
            ,{field:'salaryTwo', title:'金额2', align:'center', width:120, sort: true, filter: true, totalRow: true,templet: function (d) {
                    return toDecimalFour(d.salaryTwo);
                },excel: {cellType: 'n'}}
        ]]
        ,loading:true
        ,data:[]
        ,height: 'full-80'
        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
        ,defaultToolbar: ['filter', 'print']
        ,title: '节拍数据表'
        ,totalRow: true
        ,page: true
        ,overflow: 'tips'
        ,limits: [200, 500, 1000]
        ,limit: 1000 //每页默认显示的数量
        ,done: function () {
            soulTable.render(this);
        }
        ,filter: {
            bottom: true,
            clearFilter: false,
            items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
        }
    });

    //监听提交
    form.on('submit(searchBeat)', function(data){
        var data = {};
        var isInput = true;
        var from = $("#from").val();
        var to = $("#to").val();
        var employeeNumber = $("#employeeNumber").val();
        if (from == null || from == '' || to == '' || to == null || employeeNumber == null || employeeNumber == ''){
            isInput = false;
        }

        if (!isInput){
            layer.msg("请输入完整信息", { icon: 2 });
        }else {
            data.from = from;
            data.to = to;
            data.employeeNumber = employeeNumber;
            var load = layer.load(2);
            $.ajax({
                url: "/erp/getemployeedetailproductionbyinfo",
                type: 'GET',
                data: data,
                success: function (res) {
                    if (res.salaryList) {
                        layui.table.reload("empSalaryTable", {
                            data: res.salaryList   // 将新数据重新载入表格
                        });
                        layer.close(load);
                    } else {
                        layer.msg("该员工无计件信息！");
                        layer.close(load);
                    }
                }, error: function () {
                    layer.msg("获取失败！", {icon: 2});
                    layer.close(load);
                }
            });
        }
        return false;
    });

    //监听提交
    form.on('submit(uniformGroup)', function(data){
        var data = {};
        var isInput = true;
        var from = $("#from").val();
        var to = $("#to").val();
        var employeeNumber = $("#employeeNumber").val();
        var groupName = $("#groupName").val();
        if (from == null || from == '' || to == '' || to == null || employeeNumber == null || employeeNumber == '' || groupName == null || groupName == ''){
            isInput = false;
        }
        if (!isInput){
            layer.msg("请输入完整信息", { icon: 2 });
        }else {
            data.from = from;
            data.to = to;
            data.employeeNumber = employeeNumber;
            data.groupName = groupName;
            layer.confirm('组名统一成'+ groupName + '吗?', function(index){
                $.ajax({
                    url: "/erp/updatesalarygroupnamebyemployeenumber",
                    type: 'POST',
                    data: data,
                    success: function (res) {
                        if (res.result == 0) {
                            layer.msg("修改成功！", {icon: 1});
                            var load = layer.load(2);
                            $.ajax({
                                url: "/erp/getemployeedetailproductionbyinfo",
                                type: 'GET',
                                data: data,
                                success: function (res) {
                                    if (res.salaryList) {
                                        layui.table.reload("empSalaryTable", {
                                            data: res.salaryList   // 将新数据重新载入表格
                                        });
                                        layer.close(load);
                                    } else {
                                        layer.msg("该员工无计件信息！");
                                        layer.close(load);
                                    }
                                }, error: function () {
                                    layer.msg("修改失败！", {icon: 2});
                                    layer.close(load);
                                }
                            });
                        } else {
                            layer.msg("该员工无计件信息！");
                            layer.close(load);
                        }
                    }, error: function () {
                        layer.msg("获取失败！", {icon: 2});
                        layer.close(load);
                    }
                });
            })
        }
        return false;
    });

    table.on('toolbar(empSalaryTable)', function(obj) {
        if (obj.event === 'clearFilter') {
            // 清除所有筛选条件并重载表格
            // 参数: tableId
            soulTable.clearFilter('empSalaryTable')
        } else if (obj.event === 'exportExcel') {
            soulTable.export('empSalaryTable');
        }
    })

});



function toDecimal(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*100)/100;
    return f;
}

function toDecimalFour(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*10000)/10000;
    return f;
}
function transDate(data) {return  moment(data).format("YYYY-MM-DD");};