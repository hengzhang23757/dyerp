$(document).ready(function () {
    layui.laydate.render({
        elem: '#from',
        trigger: 'click'
    });
    layui.laydate.render({
        elem: '#to',
        trigger: 'click'
    });

    layui.use(['autocomplete'], function () {
        layui.autocomplete.render({
            elem: $('#clothesVersionNumber')[0],
            url: 'getversionhint',
            template_val: '{{d}}',
            template_txt: '{{d}}',
            onselect: function (resp) {
                autoComplete(resp);
            }
        })
    });
});


function autoComplete(keywords) {
    $("#orderName").empty();
    $.ajax({
        url: "/erp/getorderbyversion",
        data: {"clothesVersionNumber": keywords},
        success:function(data){
            $("#orderName").empty();
            if (data.orderList) {
                $("#orderName").append("<option value=''>订单号</option>");
                $.each(data.orderList, function(index,element){
                    $("#orderName").append("<option value='"+element+"'>"+element+"</option>");
                })
            }
        },
        error:function(){
        }
    });
}


var hot;
var basePath=$("#basePath").val();

function search() {
    var container = document.getElementById('addOrderExcel');
    if(hot==undefined) {
        hot = new Handsontable(container, {
            // data: data,
            rowHeaders: true,
            colHeaders: true,
            autoColumnSize: true,
            dropdownMenu: true,
            contextMenu: true,
            filters:true,
            stretchH: 'all',
            autoWrapRow: true,
            height: $(document.body).height() - 180,
            manualRowResize: true,
            manualColumnResize: true,
            // minRows: 20,
            minCols: 18,
            colWidths:[100,60,60,60,60,60,60,60,60,60,60,60,60,60,60],
            language: 'zh-CN',
            licenseKey: 'non-commercial-and-evaluation'
        });
    }


    if($("#orderName").val().trim()=="") {
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入要查询的订单号！</span>",html: true});
        return false;
    }
    $.ajax({
        url: basePath+"erp/getcutdailydetailbyordertime",
        type:'GET',
        data: {
            from:$("#from").val(),
            to:$("#to").val(),
            orderName:$("#orderName").val(),
        },
        success: function (data) {
            $("#exportDiv").show();
            var hotData = [];
            if(data) {
                var totalSum = 0;
                var totalSummary = [];
                for(var dateKey in data) {
                    var dateIndex = 0;
                    var tmpDate = transDate(dateKey);
                    var dateRow = [tmpDate];
                    hotData.push(dateRow);
                    var dateColor = data[dateKey];
                    var colorSum = 0;
                    var summary = [];
                    for (var colorKey in dateColor){
                        var colorIndex = 0;
                        var colorRow = [];
                        colorRow.push(colorKey);
                        var dateColorSize = dateColor[colorKey];
                        var sizeRow = [];
                        sizeRow[0] = [];
                        var sizeSum = 0;
                        for (var sizeKey in dateColorSize){
                            if (dateIndex == 0){
                                sizeRow.push(sizeKey);
                                summary.push([]);
                            }
                            colorRow.push(dateColorSize[sizeKey]);
                            sizeSum += dateColorSize[sizeKey];
                        }
                        colorRow.push(sizeSum);
                        colorSum += sizeSum;
                        if (colorIndex == 0){
                            sizeRow.push("合计");
                        }
                        colorIndex ++;
                        if (dateIndex == 0){
                            hotData.push(sizeRow);
                        }
                        hotData.push(colorRow);
                        dateIndex ++;
                    }
                    totalSum += colorSum;
                    summary.push("小计");
                    summary.push(colorSum);
                    hotData.push(summary);
                }
                totalSummary.push("总计");
                totalSummary.push(totalSum);
                hotData.push(totalSummary);
            }
            hot.loadData(hotData);
        },
        error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }
    })
}

function exportData() {
    var orderName = $("#orderName").val();
    var versionNumber = $("#clothesVersionNumber").val();
    var myDate = new Date();
    var title = orderName+'-'+versionNumber+'-裁床每日明细-'+myDate.toLocaleString();
    var data = utl.Object.copyJson(hot.getData());//<=====读取handsontable的数据
    var result = [];
    var firstRow = new Array(data[0].length);
    firstRow[0] = title;
    for(var i=1;i<data[0].length;i++) {
        firstRow[i] = "";
    }
    result.push(firstRow);
    var col = 0;
    $.each(data,function (index, item) {
        for(var i=0;i<item.length;i++) {
            if(item[i]==null) {
                item[i] = '';
            }else if(index==1) {
                col++;
            }
        }
        result.push(item);
    });
    utl.XLSX.onExport(result,"Sheet1","xlsx",title+".xlsx");//<====导出
}


function transDate(data) {return  moment(data).format("YYYY-MM-DD");};

