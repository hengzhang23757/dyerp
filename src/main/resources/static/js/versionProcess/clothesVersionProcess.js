$(document).ready(function () {
    layui.laydate.render({
        elem: '#beginDate',
        trigger: 'click'
    });

    layui.use(['autocomplete'], function () {
        layui.autocomplete.render({
            elem: $('#clothesVersionNumber')[0],
            url: 'getversionhint',
            template_val: '{{d}}',
            template_txt: '{{d}}',
            onselect: function (resp) {
                autoComplete(resp)
            }
        })
    });


    $.ajax({
        url: basePath + "erp/getuniqueclothesversionprocess",
        type:'GET',
        data: {},
        success: function (data) {
            if(data) {
                createClothesVersionProcessTable(data.clothesVersionProcessList);
                $.unblockUI();
            }else {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，没有查到相关信息！</span>",html: true});
            }
        },
        error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }
    });

    $('#mainFrameTabs').bTabs();

    $('#orderListTab').click(function(){
        $.ajax({
            url: basePath + "erp/getuniqueclothesversionprocess",
            type:'GET',
            data: {},
            success: function (data) {
                if(data) {
                    createClothesVersionProcessTable(data.clothesVersionProcessList);
                    $.unblockUI();
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，没有查到相关信息！</span>",html: true});
                }
            },
            error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        });
    });


    $("#orderName").change(function () {
        var orderName = $("#orderName").val();
        $("#customerName").empty();
        $.ajax({
            url: "/erp/getcustomernamebyordername",
            data: {"orderName": orderName},
            success:function(data){
                $("#customerName").val(data);
            },
            error:function(){
            }
        });
        $("#styleDescription").empty();
        $.ajax({
            url: "/erp/getstyledescriptionbyorder",
            data: {"orderName": orderName},
            success:function(data){
                $("#styleDescription").val(data);
            },
            error:function(){
            }
        });
    });


});


function autoComplete(keywords) {
    $("#orderName").empty();
    $.ajax({
        url: "/erp/getorderbyversion",
        data: {"clothesVersionNumber": keywords},
        success:function(data){
            $("#orderName").empty();
            if (data.orderList) {
                $("#orderName").append("<option value=''>选择订单号</option>");
                $.each(data.orderList, function(index,element){
                    $("#orderName").append("<option value="+element+">"+element+"</option>");
                })
            }
        },
        error:function(){
        }
    });
}


var basePath=$("#basePath").val();
var userRole=$("#userRole").val();

var clothesVersionProcessTable;
function createClothesVersionProcessTable(data) {
    if (clothesVersionProcessTable != undefined) {
        clothesVersionProcessTable.clear(); //清空一下table
        clothesVersionProcessTable.destroy(); //还原初始化了的datatable
    }
    $.blockUI({ css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        },
        message: "<h3>稍等一下... 奋力搜索中</h3>"
    });
    clothesVersionProcessTable = $('#clothesVersionProcessTable').DataTable({
        "data":data,
        language : {
            processing : "载入中",//处理页面数据的时候的显示
            paginate : {//分页的样式文本内容。
                previous : "上一页",
                next : "下一页",
                first : "第一页",
                last : "最后一页"
            },
            search:"搜索：",
            zeroRecords : "没有内容",//table tbody内容为空时，tbody的内容。
            //下面三者构成了总体的左下角的内容。
            info : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//左下角的信息显示，大写的词为关键字。
            infoEmpty : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//筛选为空时左下角的显示。
            infoFiltered : ""//筛选之后的左下角筛选提示(另一个是分页信息显示，在上面的info中已经设置，所以可以不显示)，
        },
        dom: 'Bfrtip',
        "buttons": [{
            'extend': 'excel',
            'text': '导出',//定义导出excel按钮的文字
            'className': 'btn btn-success', //按钮的class样式
            'title': '版单进度表',
            'exportOptions': {
                'modifier': {
                    'page': 'all'
                }
            }
        }],
        pageLength : 15,// 每页显示10条数据
        pagingType : "full_numbers", // 分页样式：simple,simple_numbers,full,full_numbers，
        "paging": true,
        "info": true,
        searching:true,
        lengthChange:false,
        scrollX: 2000,
        scrollCollapse: true,
        scroller:       true,
        "columns": [
            {
                "data": null,
                "title":"序号",
                "width":"8%",
                "defaultContent": "",
                "sClass": "text-center",
                "searchable":false,
                render: function (data, type, row, meta) {
                    var no = meta.settings._iDisplayStart + meta.row + 1;
                    return no;
                }
            },{
                "data": "clothesVersionNumber",
                "title":"版单号",
                "width":"20%",
                "defaultContent": "",
                "sClass": "text-center"
            },{
                "data": "orderName",
                "title":"订单号",
                "width":"20%",
                "defaultContent": "",
                "sClass": "text-center"
            }, {
                "data": "styleDescription",
                "title":"款式",
                "width":"20%",
                "defaultContent": "",
                "sClass": "text-center"
            }, {
                "data": "beginDate",
                "title":"开版日期",
                "width":"16%",
                "defaultContent": "",
                "sClass": "text-center",
                "render": function(data, type, full, meta) {
                    return  moment(data).format("YYYY-MM-DD");
                }
            }, {
                "data": "orderName",
                "title": "操作",
                "width":"16%",
                "defaultContent": "",
                "sClass": "text-center"
            }
        ],
        "columnDefs" :
            [{
                "orderable" : false, // 禁用排序
                "targets" : [5], // 指定的列
                "data" : "orderName",
                "render" : function(data, type, full, meta) {
                    return "<a href='#' style='color:#ff0000' onclick=deleteClothesVersionProcess('"+data+"')>删除</a>&nbsp;&nbsp;&nbsp;<a href='#' style='color:#3e8eea' onclick=detailClothesVersionProcess('"+data+"')>详情</a>";
                }
            }]
    });
    $('#clothesVersionProcessTable_wrapper .dt-buttons').append("<button class=\"btn btn-s-lg  btn-success\" style=\"outline:none;margin-left: 10px\" onclick=\"createClothesVersionProcess()\">创建版单</button>");
    $.unblockUI();
}



function deleteClothesVersionProcess(orderName) {
    if(userRole!='root' && userRole!='role2'){
        swal("SORRY!", "对不起，您没有操作权限！", "warning");
        return;
    }
    swal({
        title: "",
        text: "<span style=\"font-weight:bolder;font-size: 20px\">确定要删除吗？</span>",
        type: "warning",
        html:true,
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: "确定",
        cancelButtonText:"我再想想",
        confirmButtonColor: "#ec6c62",
        showLoaderOnConfirm: true
    }, function() {
        $.ajax({
            url: basePath + "erp/deleteclothesversionprocessbyorder",
            type:'POST',
            data: {
                orderName:orderName,
                userName:$("#userName").val()
            },
            success: function (data) {
                if(data == 0) {
                    $.unblockUI();
                    $("input").val("");
                    swal({
                            type:"success",
                            title:"",
                            text: "<span style=\"font-weight:bolder;font-size: 20px\">恭喜你，删除成功！</span>",
                            html: true
                        },
                        function(){
                            // location.href=basePath+"erp/opaBackInputStart";
                            clothesVersionProcessTable.row($(obj).parents('tr')).remove().draw(false);
                        });
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，删除失败！</span>",html: true});
                }
            },
            error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        })
    });
}

var tabId = 1;
function detailClothesVersionProcess(orderName) {
    var urlName = encodeURIComponent(orderName);
    var tabName = orderName.replace(/[&\|\\\*^%$#@\-+~`！!……——=「」{};:：；‘’\"\"/<>.,，。《》]/g,"");
    $('#mainFrameTabs').bTabsAdd("tabId" + tabName, "版单进度", "/erp/clothesVersionProcessDetailStart?orderName="+urlName);
    tabId++;
}

function createClothesVersionProcess() {
    $.blockUI({
        css: {
            width: '70%',
            top: '15%',
            left: '15%',
            border: 'none',
            padding: '15px',
            backgroundColor: '#fff',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: 1,
            color: '#000'
        },
        message: $('#editPro')
    });
    var url = basePath + "erp/addclothesversionprocessbatch";
    $("#editYes").unbind("click").bind("click", function () {
        var flag = false;
        $("#editPro input").each(function (index, item) {
            if($(this).val() == "") {
                flag = true;
                return false;
            }
        });
        if(flag) {
            swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入完全部信息！</span>",html: true});
            return false;
        }
        var clothesVersionProcessJson = [];
        var clothesVersionProcess = {};
        clothesVersionProcess.clothesVersionNumber = $("#clothesVersionNumber").val();
        clothesVersionProcess.orderName = $("#orderName").val();
        clothesVersionProcess.styleDescription = $("#styleDescription").val();
        clothesVersionProcess.beginDate = $("#beginDate").val();
        clothesVersionProcess.processType = $("#processType").val();
        clothesVersionProcess.processDescription = $("#processDescription").val();
        clothesVersionProcess.processDate = $("#beginDate").val();
        clothesVersionProcessJson.push(clothesVersionProcess);
        $.ajax({
            url: url,
            type: 'POST',
            data: {
                clothesVersionProcessJson:JSON.stringify(clothesVersionProcessJson)
            },
            success: function (data) {
                if(data == 0) {
                    $.unblockUI();
                    $("input").val("");
                    swal({
                            type:"success",
                            title:"",
                            text: "<span style=\"font-weight:bolder;font-size: 20px\">恭喜你，保存成功！</span>",
                            html: true
                        },
                        function(){
                            location.href=basePath+"erp/clothesVersionProcessStart";
                        });
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，保存失败！</span>",html: true});
                }
            },
            error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        });
    });
    $("#editNo").unbind("click").bind("click", function () {
        $.unblockUI();
        $("input").val("");
        $("select").val("");
    });
}
