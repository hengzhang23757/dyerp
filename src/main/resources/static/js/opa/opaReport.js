var hot;
var otherHot;
var basePath=$("#basePath").val();
var form;
layui.use('form', function(){
    form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
    form.render();
});
layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.21'
}).extend({
    soulTable: 'soulTable'
});
$(document).ready(function () {

    layui.laydate.render({
        elem: '#from',
        trigger: 'click'
    });
    layui.laydate.render({
        elem: '#to',
        trigger: 'click'
    });

    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumber", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });

    $("#orderName").bind('blur change',function(){
        var orderName = $("#orderName").val();
        $.ajax({
            url: "/erp/getotherprintpartnamesbyorder",
            data: {"orderName": orderName},
            success:function(data){
                $("#partName").empty();
                if (data.printPartNameList) {
                    $("#partName").append("<option value='主身布'>主身布</option>");
                    $.each(data.printPartNameList, function(index,element){
                        $("#partName").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
                form.render('select');
            },
            error:function(){
            }
        });
    });

    $("#clothesVersionNumber").bind('blur change',function(){
        var orderName = $("#orderName").val();
        $.ajax({
            url: "/erp/getotherprintpartnamesbyorder",
            data: {"orderName": orderName},
            success:function(data){
                $("#partName").empty();
                if (data.printPartNameList) {
                    $("#partName").append("<option value='主身布'>主身布</option>");
                    $.each(data.printPartNameList, function(index,element){
                        $("#partName").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
                form.render('select');
            },
            error:function(){
            }
        });
    });

});
var opaTable;
layui.use(['form', 'table', 'soulTable'], function () {
    var table = layui.table,
        soulTable = layui.soulTable,
        form = layui.form,
        $ = layui.$;
    form.render('select');
    opaTable = table.render({
        elem: '#opaTable'
        ,cols:[[]]
        ,loading:true
        ,data:[]
        ,height: 'full-150'
        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
        ,defaultToolbar: ['filter', 'print']
        ,title: '花片报表'
        ,totalRow: true
        ,page: true
        ,limits: [500, 1000, 2000]
        ,limit: 1000 //每页默认显示的数量
        ,done: function () {
            soulTable.render(this);
        }
        ,filter: {
            bottom: true,
            items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
        }
    });

    //监听提交
    form.on('submit(searchBeat)', function(data){
        var data = {};
        var partName = $("#partName").val();
        var orderName = $("#orderName").val();
        var clothesVersionNumber = $("#clothesVersionNumber").val();
        var from = $("#from").val();
        var to = $("#to").val();
        var type = $("#type").val();
        data.partName = partName;
        data.orderName = orderName;
        data.clothesVersionNumber = clothesVersionNumber;
        if (from != null && from != ''){
            data.from = from;
        }
        if (to != null && to != ''){
            data.to = to;
        }
        if ((orderName == null || orderName === "")){
            layer.msg("款号必填", { icon: 2 });
            return false;
        }
        if (type === '汇总'){
            $.ajax({
                url: "/erp/getopareportsummary",
                type: 'GET',
                data: data,
                success: function (res) {
                    if (res.xhl) {
                        var reportData = res.xhl;
                        table.render({
                            elem: '#opaTable'
                            ,cols:[[
                                {type:'numbers', align:'center', title:'序号', width:90}
                                ,{field:'clothesVersionNumber', title:'单号', align:'center', width:150, sort: true, filter: true, totalRowText: '合计'}
                                ,{field:'orderName', title:'款号', align:'center', width:150, sort: true, filter: true}
                                ,{field:'partName', title:'部位', align:'center', width:120, sort: true, filter: true}
                                ,{field:'colorName', title:'颜色', align:'center', width:120, sort: true, filter: true}
                                ,{field:'sizeName', title:'尺码', align:'center', width:120, sort: true, filter: true}
                                ,{field:'wellCount', title:'好片数', align:'center', width:150, sort: true, filter: true, totalRow: true,excel: {cellType: 'n'}}
                                ,{field:'opaCount', title:'外发数量', align:'center',width:120, sort: true, filter: true, totalRow: true,excel: {cellType: 'n'}}
                                ,{field:'opaBackCount', title:'总回厂', align:'center',width:120, sort: true, filter: true, totalRow: true,excel: {cellType: 'n'}}
                                ,{field:'diffCount', title:'差异', align:'center',width:120, sort: true, filter: true, totalRow: true,excel: {cellType: 'n'}}
                                ,{field:'regionBack', title:'区间回厂', align:'center',width:120, sort: true, filter: true, totalRow: true,excel: {cellType: 'n'}}
                                ,{field:'defectiveCount', title:'次片', align:'center',width:120, sort: true, filter: true, totalRow: true,excel: {cellType: 'n'}}
                                ,{field:'rottenCount', title:'烂片', align:'center',width:120, sort: true, filter: true, totalRow: true,excel: {cellType: 'n'}}
                                ,{field:'lackCount', title:'少片', align:'center',width:120, sort: true, filter: true, totalRow: true,excel: {cellType: 'n'}}
                                ,{title:'类型', align:'center',width:120, sort: true, filter: true, templet: function (d) {
                                        return "新华隆";
                                    }}
                                ,{title:'操作', align:'center', width:150, toolbar: '#barTop'}
                            ]]
                            ,excel: {
                                filename: '裁床主身布月度报表.xlsx'
                            }
                            ,data: reportData   // 将新数据重新载入表格
                            ,overflow: 'tips'
                            ,loading:true
                            ,height: 'full-150'
                            ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                            ,defaultToolbar: ['filter', 'print']
                            ,title: '花片报表'
                            ,totalRow: true
                            ,page: true
                            ,limits: [500, 1000, 2000]
                            ,limit: 1000 //每页默认显示的数量
                            ,done: function () {
                                soulTable.render(this);
                            }
                            ,filter: {
                                bottom: true,
                                items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                            }
                        });

                    } else {
                        layer.msg("没有查到印绣花数据,请联系印绣花厂核实！");
                    }
                }, error: function () {
                    layer.msg("获取失败！", {icon: 2});
                }
            });
            return false;
        } else {
            $.ajax({
                url: "/erp/getopareportdailysummary",
                type: 'GET',
                data: data,
                success: function (res) {
                    if (res.xhl) {
                        var reportData = res.xhl;
                        table.render({
                            elem: '#opaTable'
                            ,cols:[[
                                {type:'numbers', align:'center', title:'序号', width:90}
                                ,{field:'clothesVersionNumber', title:'单号', align:'center', width:150, sort: true, filter: true, totalRowText: '合计'}
                                ,{field:'orderName', title:'款号', align:'center', width:150, sort: true, filter: true}
                                ,{field:'printingPart', title:'部位', align:'center', width:120, sort: true, filter: true}
                                ,{field:'colorName', title:'颜色', align:'center', width:120, sort: true, filter: true}
                                ,{field:'sizeName', title:'尺码', align:'center', width:120, sort: true, filter: true}
                                ,{field:'layerCount', title:'回厂数量', align:'center',width:120, sort: true, filter: true, totalRow: true,excel: {cellType: 'n'}}
                                ,{field:'outDate', title:'日期', align:'center',width:120, sort: true, filter: true, templet:function (d) {
                                        return moment(d.outDate).format("YYYY-MM-DD");
                                    }}
                            ]]
                            ,excel: {
                                filename: '花片回厂按天汇总.xlsx'
                            }
                            ,data: reportData   // 将新数据重新载入表格
                            ,overflow: 'tips'
                            ,loading:true
                            ,height: 'full-150'
                            ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                            ,defaultToolbar: ['filter', 'print']
                            ,title: '花片报表'
                            ,totalRow: true
                            ,page: true
                            ,limits: [500, 1000, 2000]
                            ,limit: 1000 //每页默认显示的数量
                            ,done: function () {
                                soulTable.render(this);
                            }
                            ,filter: {
                                bottom: true,
                                items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                            }
                        });

                    } else {
                        layer.msg("没有查到印绣花数据,请联系印绣花厂核实！");
                    }
                }, error: function () {
                    layer.msg("获取失败！", {icon: 2});
                }
            });
            return false;
        }
    });

    table.on('toolbar(opaTable)', function(obj) {
        if (obj.event === 'clearFilter') {
            // 清除所有筛选条件并重载表格
            // 参数: tableId
            soulTable.clearFilter('opaTable')
        } else if (obj.event === 'exportExcel') {
            soulTable.export('opaTable');
        }
    });

    //监听行工具事件
    table.on('tool(opaTable)', function(obj){
        var data = obj.data;
        if(obj.event === 'detail'){
            var orderName = data.orderName;
            var printingPart = data.partName;
            var colorName = data.colorName;
            var sizeName = data.sizeName;
            var label = "花片对照详情&emsp;&emsp;&emsp;&emsp;款号:"+ orderName + "&emsp;&emsp;&emsp;&emsp;部位:"+printingPart+"&emsp;&emsp;&emsp;&emsp;颜色:"+colorName+"&emsp;&emsp;&emsp;&emsp;尺码:"+sizeName;
            var index = layer.open({
                type: 1 //Page层类型
                , title: label
                , shade: 0.6 //遮罩透明度
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , content: $("#opaReportDetail")
            });
            layer.full(index);
            setTimeout(function () {
                $.ajax({
                    url: "/erp/getopareportdetail",
                    type: 'GET',
                    data: {
                        orderName:orderName,
                        printingPart:printingPart,
                        sizeName:sizeName,
                        colorName:colorName
                    },
                    success: function (res) {
                        var finishCount = res.finishCount;
                        var title = [
                            {field: 'orderName', title: '款号',align:'center',minWidth:150, totalRowText: '合计'},
                            {field: 'bedNumber', title: '床号',align:'center',  sort: true, filter: true,minWidth:100},
                            {field: 'printingPart', title: '部位',align:'center',  sort: true, filter: true,minWidth:150},
                            {field: 'colorName', title: '颜色',align:'center',  sort: true, filter: true,minWidth:120},
                            {field: 'sizeName', title: '尺码',align:'center',  sort: true, filter: true, minWidth:120},
                            {field: 'packageNumber', title: '扎号',align:'center',  sort: true, filter: true,minWidth:100},
                            {field: 'layerCount', title: '数量',align:'center',  sort: true, filter: true,minWidth:100 , totalRow: true},
                            {field: 'defectiveCount', title: '次片',align:'center',minWidth:100,  sort: true, filter: true , totalRow: true},
                            {field: 'rottenCount', title: '烂片',align:'center',minWidth:100,  sort: true, filter: true , totalRow: true},
                            {field: 'lackCount', title: '少片',align:'center',minWidth:100,  sort: true, filter: true , totalRow: true},
                            {field: 'inspectionOrderName', title: '是否完成',align:'center', sort: true, filter: true, minWidth:150, totalRowText: finishCount, templet: function (d) {
                                if (d.inspectionOrderName == "已做") {
                                    return "<span style='color: green; font-weight: bolder'>"+ d.inspectionOrderName +"</span>";
                                } else {
                                    return "<span style='color: red; font-weight: bolder'>"+ d.inspectionOrderName +"</span>";
                                }
                            }}
                        ];
                        table.render({
                            elem: '#opaReportDetail'
                            ,totalRow: true
                            ,cols: [title]
                            ,page: false
                            ,height: 'full-130'
                            ,limit: Number.MAX_VALUE //每页默认显示的数量
                            ,data: res.xhlTailorList
                            ,done: function () {
                                soulTable.render(this);
                            }
                        });
                    },
                    error: function () {
                        layer.msg("获取数据失败！", {icon: 2});
                    }
                })
            },100);

        }
    });

});

function exportData() {
    var data = hot.getData();
    var result = [];
    $.each(data,function (index, item) {
        for(var i=0;i<item.length;i++) {
            if(item[i]==null) {
                item[i] = '';
            }
        }
        result.push(item);
    });
    export2Excel([fromDate+"到"+toDate+'入仓表'], 11, result, fromDate+"到"+toDate+'入仓表'+".xls")
}

function toDecimal(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*100)/100;
    return f;
}

function toDecimalFour(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*10000)/10000;
    return f;
}

function printDeal(){
    $("#printf").empty();
    var printBoxs = document.getElementsByName('printTable');
    var newContent = '';
    for(var i=0;i<printBoxs.length;i++) {
        newContent += printBoxs[i].innerHTML;
    }
    var f = document.getElementById('printf');
    // f.contentDocument.write('<link rel="stylesheet" type="text/css" href="/css/app.v2.css">');
    f.contentDocument.write('<style>' +
        'table {' +
        'border-collapse:collapse;' +
        '}' +
        'td {\n' +
        '        border: 1px solid #000000;\n' +
        '    }'+
        '</style>');
    f.contentDocument.write(newContent);
    f.contentDocument.close();
    window.frames['printf'].focus();
    try{
        window.frames['printf'].print();
    }catch(err){
        f.contentWindow.print();
    }
}


function exportData() {
    var data = hot.getData();
    var result = [];
    var col = 0;
    $.each(data,function (index, item) {
        // if(item[0]!=null) {
        for(var i=0;i<item.length;i++) {
            if(item[i]==null) {
                item[i] = '';
            }else if(index==1) {
                col++;
            }
        }
        result.push(item);
        // }else {
        //     return false;
        // }
    });
    var myDate = new Date();
    export2Excel(['花片详情'+myDate.toLocaleString()],col, result, '花片详情'+myDate.toLocaleString()+".xls")
}

function exportOtherData() {
    var data = otherHot.getData();
    var result = [];
    var col = 0;
    $.each(data,function (index, item) {
        // if(item[0]!=null) {
        for(var i=0;i<item.length;i++) {
            if(item[i]==null) {
                item[i] = '';
            }else if(index==1) {
                col++;
            }
        }
        result.push(item);
        // }else {
        //     return false;
        // }
    });
    var myDate = new Date();
    export2Excel(['花片汇总'+myDate.toLocaleString()],col, result, '花片汇总'+myDate.toLocaleString()+".xls")
}