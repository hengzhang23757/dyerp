var opaBackInputTable;
$(document).ready(function () {

    layui.laydate.render({
        elem: '#opaBackDate', //指定元素
        trigger: 'click'
    });

    layui.laydate.render({
        elem: '#opaBackDateOther', //指定元素
        trigger: 'click'
    });

    layui.laydate.render({
        elem: '#opaBackDate1', //指定元素
        trigger: 'click'
    });

    opaBackInputTable = $('#opaBackInputTable').DataTable( {
        language : {
            processing : "载入中",//处理页面数据的时候的显示
            paginate : {//分页的样式文本内容。
                previous : "上一页",
                next : "下一页",
                first : "第一页",
                last : "最后一页"
            },
            search:"搜索：",
            zeroRecords : "没有内容",//table tbody内容为空时，tbody的内容。
            //下面三者构成了总体的左下角的内容。
            info : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//左下角的信息显示，大写的词为关键字。
            infoEmpty : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//筛选为空时左下角的显示。
            infoFiltered : ""//筛选之后的左下角筛选提示(另一个是分页信息显示，在上面的info中已经设置，所以可以不显示)，
        },
        pageLength : 10,// 每页显示10条数据
        pagingType : "full_numbers", // 分页样式：simple,simple_numbers,full,full_numbers，
        "paging": true,
        // "ordering" : false,
        "info": true,
        searching:true,
        lengthChange:false,
        dom: 'Bfrtip',
        "buttons": [{
            'extend': 'excel',
            'text': '导出',
            'className': 'btn btn-success', //按钮的class样式
            'title': '花片回厂详情',
            'exportOptions': {
                'modifier': {
                    'page': 'all'
                },
                columns: ':visible:not(.noExport)'
            }
        }],
        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            // Total over all pages
            total = api
                .column( 7 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );


            // Total over this page
            pageTotal = api
                .column( 7, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            // Update footer
            $( api.column( 9 ).footer() ).html(
                "当前页数量："+pageTotal +"，总计："+ total
            );
        }
    } );

    createTodayOpaBackInputTable();

    layui.use(['autocomplete'], function () {
        layui.autocomplete.render({
            elem: $('#clothesVersionNumber')[0],
            url: 'getversionhint',
            template_val: '{{d}}',
            template_txt: '{{d}}',
            onselect: function (resp) {
                autoComplete(resp)
            }
        })
    });

    layui.use(['autocomplete'], function () {
        layui.autocomplete.render({
            elem: $('#clothesVersionNumberOther')[0],
            url: 'getversionhint',
            template_val: '{{d}}',
            template_txt: '{{d}}',
            onselect: function (resp) {
                autoCompleteOther(resp)
            }
        })
    });

    $("#orderName").change(function () {
        var orderName = $("#orderName").val();
        $("select[name='bedNumber']").empty();
        $.ajax({
            url: "/erp/getopabednumbers",
            data: {"orderName": orderName},
            success:function(data){
                if (data.bedNumList) {
                    $.each(data.bedNumList, function(index,element){
                        $("select[name='bedNumber']").append("<option value="+element+">"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });
        $("select[name='partName']").empty();
        $.ajax({
            url: "/erp/getopapartnamesbyordername",
            data: {"orderName": orderName},
            success:function(data){
                if (data.partNameList) {
                    $.each(data.partNameList, function(index,element){
                        $("select[name='partName']").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });
        $("select[name='colorName']").empty();
        $.ajax({
            url: "/erp/getcolorhint",
            data: {"orderName": orderName},
            success:function(data){
                if (data.colorList) {
                    $.each(data.colorList, function(index,element){
                        $("select[name='colorName']").append("<option value='"+element.colorName+"'>"+element.colorName+"</option>");
                    })
                }
            },
            error:function(){
            }
        });
        $("select[name='sizeName']").empty();
        $.ajax({
            url: "/erp/getordersizenamesbyorder",
            data: {"orderName": orderName},
            success:function(data){
                if (data.sizeNameList) {
                    $.each(data.sizeNameList, function(index,element){
                        $("select[name='sizeName']").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });

    });


    $("#orderNameOther").change(function () {
        var orderNameOther = $("#orderNameOther").val();
        $("select[name='colorNameOther']").empty();
        $.ajax({
            url: "/erp/getcolorhint",
            data: {"orderName": orderNameOther},
            success:function(data){
                if (data.colorList) {
                    $.each(data.colorList, function(index,element){
                        $("select[name='colorNameOther']").append("<option value='"+element.colorName+"'>"+element.colorName+"</option>");
                    })
                }
            },
            error:function(){
            }
        });

        $("select[name='sizeNameOther']").empty();
        $.ajax({
            url: "/erp/getordersizenamesbyorder",
            data: {"orderName": orderNameOther},
            success:function(data){
                if (data.sizeNameList) {
                    $.each(data.sizeNameList, function(index,element){
                        $("select[name='sizeNameOther']").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });
        $("select[name='partNameOther']").empty();
        $.ajax({
            url: "/erp/getopapartnamesbyordername",
            data: {"orderName": orderNameOther},
            success:function(data){
                if (data.partNameList) {
                    $.each(data.partNameList, function(index,element){
                        $("select[name='partNameOther']").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });
    });

});

function autoComplete(keywords) {
    $("#orderName").empty();
    $.ajax({
        url: "/erp/getorderbyversion",
        data: {"clothesVersionNumber": keywords},
        success:function(data){
            $("#orderName").empty();
            if (data.orderList) {
                $("#orderName").append("<option value=''>选择订单</option>");
                $.each(data.orderList, function(index,element){
                    $("#orderName").append("<option value='"+element+"'>"+element+"</option>");
                })
            }
        },
        error:function(){
        }
    });
}


function autoCompleteOther(keywords) {
    $("#orderNameOther").empty();
    $.ajax({
        url: "/erp/getorderbyversion",
        data: {"clothesVersionNumber": keywords},
        success:function(data){
            $("#orderNameOther").empty();
            if (data.orderList) {
                $("#orderNameOther").append("<option value=''>选择订单</option>");
                $.each(data.orderList, function(index,element){
                    $("#orderNameOther").append("<option value='"+element+"'>"+element+"</option>");
                })
            }
        },
        error:function(){
        }
    });
}


var basePath=$("#basePath").val();

function addOpaBack() {
    $.blockUI({
        css: {
            width: '90%',
            top: '5%',
            left: '5%',
            border: 'none',
            padding: '15px',
            backgroundColor: '#fff',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: 1,
            color: '#000'
        },
        message: $('#editPro')
    });
    var url = basePath + "erp/addopabackinputbatch";
    $("#editYes").unbind("click").bind("click", function () {
        var flag = false;
        $("#editPro input").each(function (index, item) {
            if($(this).val() == "") {
                flag = true;
                return false;
            }
        });
        if(flag) {
            swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入完全部信息！</span>",html: true});
            return false;
        }
        var opaBackJson = [];
        $("select[name='bedNumber']").each(function (index,item) {
            var opaBack = {};
            opaBack.orderName = $("#orderName").val();
            opaBack.clothesVersionNumber = $("#clothesVersionNumber").val();
            opaBack.opaBackDate = $("#opaBackDate").val();
            opaBack.bedNumber = $(item).val();
            opaBack.partName = $("select[name='partName']").eq(index).val();
            opaBack.colorName = $("select[name='colorName']").eq(index).val();
            opaBack.sizeName = $("select[name='sizeName']").eq(index).val();
            opaBack.opaBackCount = $("input[name='opaBackCount']").eq(index).val();
            opaBackJson.push(opaBack);
        });
        $.ajax({
            url: url,
            type: 'POST',
            data: {
                opaBackJson:JSON.stringify(opaBackJson)
            },
            success: function (data) {
                if(data == 0) {
                    $.unblockUI();
                    $("input").val("");
                    swal({
                        type:"success",
                        title:"",
                        text: "<span style=\"font-weight:bolder;font-size: 20px\">恭喜你，保存成功！</span>",
                        html: true
                    },
                    function(){
                        location.href=basePath+"erp/opaBackInputStart";
                    });
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，保存失败！</span>",html: true});
                }
            },
            error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        });
    });
    $("#editNo").unbind("click").bind("click", function () {
        $.unblockUI();
        $("input").val("");
        $("select").val("");

    });
}

function addOtherOpaBack() {
    $.blockUI({
        css: {
            width: '90%',
            top: '5%',
            left: '5%',
            border: 'none',
            padding: '15px',
            backgroundColor: '#fff',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: 1,
            color: '#000'
        },
        message: $('#editOtherPro')
    });
    var url = basePath + "erp/addopabackinputbatch";
    $("#editOtherYes").unbind("click").bind("click", function () {
        var flag = false;
        $("#editOtherPro input").each(function (index, item) {
            if($(this).val() == "") {
                flag = true;
                return false;
            }
        });
        if(flag) {
            swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入完全部信息！</span>",html: true});
            return false;
        }
        var opaBackJson = [];
        $("select[name='partNameOther']").each(function (index,item) {
            var opaBack = {};
            opaBack.orderName = $("#orderNameOther").val();
            opaBack.bedNumber = $("#bedNumberOther").val();
            opaBack.opaBackDate = $("#opaBackDateOther").val();
            opaBack.clothesVersionNumber = $("#clothesVersionNumberOther").val();
            opaBack.partName = $(item).val();
            opaBack.colorName = $("select[name='colorNameOther']").eq(index).val();
            opaBack.sizeName = $("select[name='sizeNameOther']").eq(index).val();
            opaBack.opaBackCount = $("input[name='opaBackCountOther']").eq(index).val();
            opaBackJson.push(opaBack);
        });
        $.ajax({
            url: url,
            type: 'POST',
            data: {
                opaBackJson:JSON.stringify(opaBackJson)
            },
            success: function (data) {
                if(data == 0) {
                    $.unblockUI();
                    $("input").val("");
                    swal({
                            type:"success",
                            title:"",
                            text: "<span style=\"font-weight:bolder;font-size: 20px\">恭喜你，保存成功！</span>",
                            html: true
                        },
                        function(){
                            location.href=basePath+"erp/opaBackInputStart";
                        });
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，保存失败！</span>",html: true});
                }
            },
            error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        });
    });
    $("#editOtherNo").unbind("click").bind("click", function () {
        $.unblockUI();
        $("input").val("");
        $("select").val("");
    });
}


function deleteOpaBackInput(opaBackInputID,obj) {
    swal({
        title: "",
        text: "<span style=\"font-weight:bolder;font-size: 20px\">您确定要删除该条信息吗？</span>",
        type: "warning",
        html:true,
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: "确定",
        cancelButtonText:"我再想想",
        confirmButtonColor: "#ec6c62",
        showLoaderOnConfirm: true
    }, function() {
        $.ajax({
            url: basePath + "erp/deleteopabackinput",
            type:'POST',
            data: {
                opaBackInputID:opaBackInputID
            },
            success: function (data) {
                if(data == 0) {
                    $.unblockUI();
                    $("input").val("");
                    swal({
                            type:"success",
                            title:"",
                            text: "<span style=\"font-weight:bolder;font-size: 20px\">恭喜你，删除成功！</span>",
                            html: true
                        },
                        function(){
                            // location.href=basePath+"erp/opaBackInputStart";
                            opaBackInputTable.row($(obj).parents('tr')).remove().draw(false);
                        });
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，删除失败！</span>",html: true});
                }
            },
            error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        })
    });
}

function addSize(obj) {
    $("#addSizeDiv").append($("tr[name='sizeDiv']:last").clone());
    $("input[name='opaBackCount']:last").val("");
    $("select[name='bedNumber']:last").val($("select[name='bedNumber']:first").val());
    $("select[name='partName']:last").val($("select[name='partName']:first").val());
    $("select[name='colorName']:last").val($("select[name='colorName']:first").val());
    $("select[name='sizeName']:last").val($("select[name='sizeName']:first").val());
    $("button[name='addSizeBtn']").remove();
    $("button[name='delSizeBtn']:first").before('<button name="addSizeBtn" class="btn" style="margin-bottom: 15px;outline:none;border-radius: 5px;color: white;" onclick="addSize(this)"><i class="fa fa-plus"></i></button>');
    $("button[name='delSizeBtn']:last").show();
}

function delSize(obj) {
    $(obj).parent().parent().parent().remove();
}

function addSizeOther(obj) {
    $("#addSizeOtherDiv").append($("tr[name='sizeDivOther']:last").clone());
    $("input[name='opaBackCountOther']:last").val("");
    $("select[name='partNameOther']:last").val($("select[name='partNameOther']:first").val());
    $("select[name='colorNameOther']:last").val($("select[name='colorNameOther']:first").val());
    $("select[name='sizeNameOther']:last").val($("select[name='sizeNameOther']:first").val());
    $("button[name='addSizeOtherBtn']").remove();
    $("button[name='delSizeOtherBtn']:first").before('<button name="addSizeOtherBtn" class="btn" style="margin-bottom: 15px;outline:none;border-radius: 5px;color: white;" onclick="addSizeOther(this)"><i class="fa fa-plus"></i></button>');
    $("button[name='delSizeOtherBtn']:last").show();
}

function delSizeOther(obj) {
    $(obj).parent().parent().parent().remove();
}



function updateOpaBackInput(obj,opaBackInputID){
    $("#clothesVersionNumber1").val($(obj).parent().parent().find("td").eq(2).text());
    $("#orderName1").val($(obj).parent().parent().find("td").eq(1).text());
    $("#bedNumber1").val($(obj).parent().parent().find("td").eq(3).text());
    $("#colorName1").val($(obj).parent().parent().find("td").eq(4).text());
    $("#sizeName1").val($(obj).parent().parent().find("td").eq(5).text());
    $("#partName1").val($(obj).parent().parent().find("td").eq(6).text());
    $("#opaBackCount1").val($(obj).parent().parent().find("td").eq(7).text());
    $("#opaBackDate1").val($(obj).parent().parent().find("td").eq(8).text());
    $.blockUI({
        css: {
            width: '75%',
            top: '10%',
            left:'15%',
            border: 'none',
            padding: '15px',
            backgroundColor: '#fff',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: 1,
            color: '#000'
        },
        message: $('#editOpaBackInput')
    });



    $("#editOpaBackInputYes").unbind("click").bind("click", function () {
        var flag = false;
        if ($("#clothesVersionNumber1").val().trim()==""){
            flag = true;
        }
        if ($("#orderName1").val().trim() == ""){
            flag = true;
        }
        if ($("#bedNumber1").val().trim() == ""){
            flag = true;
        }
        if ($("#colorName1").val().trim() == ""){
            flag = true;
        }
        if ($("#sizeName1").val().trim() == ""){
            flag = true;
        }
        if ($("#partName1").val().trim() == ""){
            flag = true;
        }
        if ($("#opaBackCount1").val().trim() == ""){
            flag = true;
        }
        if ($("#opaBackDate1").val().trim() == ""){
            flag = true;
        }
        if(flag) {
            swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请填写完所有字段信息！</span>",html: true});
            return false;
        }
        var opaBackInput = {};
        opaBackInput.opaBackInputID = opaBackInputID;
        opaBackInput.clothesVersionNumber = $("#clothesVersionNumber1").val();
        opaBackInput.orderName = $("#orderName1").val();
        opaBackInput.bedNumber = $("#bedNumber1").val();
        opaBackInput.colorName = $("#colorName1").val();
        opaBackInput.sizeName = $("#sizeName1").val();
        opaBackInput.partName = $("#partName1").val();
        opaBackInput.opaBackCount = $("#opaBackCount1").val();
        opaBackInput.opaBackDate = $("#opaBackDate1").val();

        $.ajax({
            url: basePath + "erp/updateopabackinput",
            type:'POST',
            data: {
                opaBackInput:JSON.stringify(opaBackInput)
            },
            success: function (data) {
                if(data == 0) {
                    $.unblockUI();
                    $("#editOpaBackInput input").val("");
                    swal({
                            type:"success",
                            title:"",
                            text: "<span style=\"font-weight:bolder;font-size: 20px\">恭喜你，保存成功！</span>",
                            html: true
                        },
                        function(){
                            location.href=basePath+"erp/opaBackInputStart";
                        });
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，保存失败！</span>",html: true});
                }
            },
            error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        })
    });
    $("#editOpaBackInputNo").unbind("click").bind("click", function () {
        $.unblockUI();
        $("#editOpaBackInput input").val("");
    });
}
var opaBackInputTable;
function createTodayOpaBackInputTable() {
    if (opaBackInputTable != undefined) {
        opaBackInputTable.clear(); //清空一下table
        opaBackInputTable.destroy(); //还原初始化了的datatable
    }
    $.blockUI({ css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        },
        message: "<h3>稍等一下... 奋力搜索中</h3>"
    });
    $.ajax({
        url: "/erp/gettodayopabackinput",
        type:'GET',
        data: {
        },
        success: function (html) {
            $("#entities").html(html);
            $('html, body').scrollTop($("#entities").offset().top);
            opaBackInputTable = $('#opaBackInputTable').DataTable({
                language : {
                    processing : "载入中",//处理页面数据的时候的显示
                    sLengthMenu: "显示 _MENU_ 项结果",
                    paginate : {//分页的样式文本内容。
                        previous : "上一页",
                        next : "下一页",
                        first : "第一页",
                        last : "最后一页"
                    },
                    search:"搜索：",
                    zeroRecords : "没有内容",//table tbody内容为空时，tbody的内容。
                    //下面三者构成了总体的左下角的内容。
                    info : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//左下角的信息显示，大写的词为关键字。
                    infoEmpty : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//筛选为空时左下角的显示。
                    infoFiltered : ""//筛选之后的左下角筛选提示(另一个是分页信息显示，在上面的info中已经设置，所以可以不显示)，
                },
                searching:true,
                lengthChange:true,
                pageLength : 25,// 每页显示10条数据
                pagingType : "full_numbers", // 分页样式：simple,simple_numbers,full,full_numbers，
                paging : true,
                "info": true,
                // scrollY: 600,
                scrollCollapse: true,
                scroller:       true,
                "footerCallback": function ( row, data, start, end, display ) {
                    var api = this.api(), data;

                    // Remove the formatting to get integer data for summation
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };

                    // Total over all pages
                    total = api
                        .column( 7 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Total over this page
                    pageTotal = api
                        .column( 7, { page: 'current'} )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Update footer
                    $( api.column( 4 ).footer() ).html(
                        // "总计数量："+ total,
                        "当前页数量："+pageTotal +"，总计："+ total
                    );
                }
            });
        },
        error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }
    });
    $.unblockUI();
}



function createOneMonthOpaBackInputTable() {
    if (opaBackInputTable != undefined) {
        opaBackInputTable.clear(); //清空一下table
        opaBackInputTable.destroy(); //还原初始化了的datatable
    }
    $.blockUI({ css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        },
        message: "<h3>稍等一下... 奋力搜索中</h3>"
    });
    $.ajax({
        url: "/erp/getonemonthopabackinput",
        type:'GET',
        data: {
        },
        success: function (html) {
            $("#entities").html(html);
            $('html, body').scrollTop($("#entities").offset().top);

            opaBackInputTable = $('#opaBackInputTable').DataTable({
                language : {
                    processing : "载入中",//处理页面数据的时候的显示
                    sLengthMenu: "显示 _MENU_ 项结果",
                    paginate : {//分页的样式文本内容。
                        previous : "上一页",
                        next : "下一页",
                        first : "第一页",
                        last : "最后一页"
                    },
                    search:"搜索：",
                    zeroRecords : "没有内容",//table tbody内容为空时，tbody的内容。
                    //下面三者构成了总体的左下角的内容。
                    info : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//左下角的信息显示，大写的词为关键字。
                    infoEmpty : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//筛选为空时左下角的显示。
                    infoFiltered : ""//筛选之后的左下角筛选提示(另一个是分页信息显示，在上面的info中已经设置，所以可以不显示)，
                },
                searching:true,
                lengthChange:true,
                pageLength : 25,// 每页显示10条数据
                pagingType : "full_numbers", // 分页样式：simple,simple_numbers,full,full_numbers，
                "paging" : true,
                "info": true,
                // scrollY: 600,
                scrollCollapse: true,
                scroller:       true,
                "footerCallback": function ( row, data, start, end, display ) {
                    var api = this.api(), data;

                    // Remove the formatting to get integer data for summation
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };

                    // Total over all pages
                    total = api
                        .column( 7 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Total over this page
                    pageTotal = api
                        .column( 7, { page: 'current'} )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Update footer
                    $( api.column( 4 ).footer() ).html(
                        // "总计数量："+ total,
                        "当前页数量："+pageTotal +"，总计："+ total
                    );
                }
            });
        },
        error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }
    });
    $.unblockUI();
}


function createThreeMonthOpaBackInputTable() {
    if (opaBackInputTable != undefined) {
        opaBackInputTable.clear(); //清空一下table
        opaBackInputTable.destroy(); //还原初始化了的datatable
    }
    $.blockUI({ css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        },
        message: "<h3>稍等一下... 奋力搜索中</h3>"
    });
    $.ajax({
        url: "/erp/getthreemonthopabackinput",
        type:'GET',
        data: {
        },
        success: function (html) {
            $("#entities").html(html);
            $('html, body').scrollTop($("#entities").offset().top);

            opaBackInputTable = $('#opaBackInputTable').DataTable({
                language : {
                    processing : "载入中",//处理页面数据的时候的显示
                    sLengthMenu: "显示 _MENU_ 项结果",
                    paginate : {//分页的样式文本内容。
                        previous : "上一页",
                        next : "下一页",
                        first : "第一页",
                        last : "最后一页"
                    },
                    search:"搜索：",
                    zeroRecords : "没有内容",//table tbody内容为空时，tbody的内容。
                    //下面三者构成了总体的左下角的内容。
                    info : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//左下角的信息显示，大写的词为关键字。
                    infoEmpty : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//筛选为空时左下角的显示。
                    infoFiltered : ""//筛选之后的左下角筛选提示(另一个是分页信息显示，在上面的info中已经设置，所以可以不显示)，
                },
                searching:true,
                lengthChange:true,
                pageLength : 25,// 每页显示10条数据
                pagingType : "full_numbers", // 分页样式：simple,simple_numbers,full,full_numbers，
                "paging" : true,
                "info": true,
                // scrollY: 600,
                scrollCollapse: true,
                scroller:       true,
                "footerCallback": function ( row, data, start, end, display ) {
                    var api = this.api(), data;

                    // Remove the formatting to get integer data for summation
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };

                    // Total over all pages
                    total = api
                        .column( 7 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Total over this page
                    pageTotal = api
                        .column( 7, { page: 'current'} )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Update footer
                    $( api.column( 4 ).footer() ).html(
                        // "总计数量："+ total,
                        "当前页数量："+pageTotal +"，总计："+ total
                    );
                }
            });
        },
        error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }
    });
    $.unblockUI();
}

function changeTable(obj){
    var opt = obj.options[obj.selectedIndex];
    if (opt.value == "today"){
        $.blockUI({ css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            },
            message: "<h3>稍等一下... 奋力搜索中</h3>"
        });
        createTodayOpaBackInputTable();
        $.unblockUI();

    }
    if (opt.value == "oneMonth"){
        $.blockUI({ css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            },
            message: "<h3>稍等一下... 奋力搜索中</h3>"
        });
        createOneMonthOpaBackInputTable()
        $.unblockUI();

    }
    if (opt.value == "threeMonths"){
        $.blockUI({ css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            },
            message: "<h3>稍等一下... 奋力搜索中</h3>"
        });
        createThreeMonthOpaBackInputTable();
        $.unblockUI();
    }

}