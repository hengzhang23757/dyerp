var basePath=$("#basePath").val();
var $bedNumberSel;
$(document).ready(function () {
    layui.laydate.render({
        elem: '#opaDate', //指定元素
        trigger: 'click'
    });
    layui.laydate.render({
        elem: '#opaDateOther', //指定元素
        trigger: 'click'
    });

    createTodayOPATable();
    $('#mainFrameTabs').bTabs();

    $('#orderListTab').click(function(){
        createTodayOPATable();
    });

    $bedNumberSel = $('#bedNumber').selectize({
        plugins: ['remove_button'],
        maxItems: null,
        persist: true,
        create: true,
        valueField: 'id',
        labelField: 'title',
        searchField: ['title'],
    });

    $('#mainFrameTabs').bTabs();

    layui.use(['autocomplete'], function () {
        layui.autocomplete.render({
            elem: $('#clothesVersionNumber')[0],
            url: 'getversionhint',
            template_val: '{{d}}',
            template_txt: '{{d}}',
            onselect: function (resp) {
                autoComplete(resp)
            }
        })
    });

    layui.use(['autocomplete'], function () {
        layui.autocomplete.render({
            elem: $('#clothesVersionNumberOther')[0],
            url: 'getversionhint',
            template_val: '{{d}}',
            template_txt: '{{d}}',
            onselect: function (resp) {
                autoCompleteOther(resp)
            }
        })
    });

    $("#orderName").change(function () {
        var orderName = $("#orderName").val();
        $("#customerName").empty();
        $.ajax({
            url: "/erp/getcustomernamebyordername",
            data: {"orderName": orderName},
            success:function(data){
                $("#customerName").val(data);
            },
            error:function(){
            }
        });
        // $("#bedNumber").empty();
        // $.ajax({
        //     url: "/erp/getbednumbersbyordername",
        //     data: {"orderName": orderName},
        //     success:function(data){
        //         if (data.bedNumList) {
        //             data.bedNumList.sort(function(a,b){return b-a});
        //             $.each(data.bedNumList, function(index,element){
        //                 $("#bedNumber").append("<option value="+element+">"+element+"</option>");
        //             })
        //         }
        //     },
        //     error:function(){
        //     }
        // });

        var control = $bedNumberSel[0].selectize;
        $.ajax({
            url: "/erp/getopabednumbers",
            data: {"orderName": orderName},
            success: function (data) {
                control.clear();
                control.clearOptions();
                if(data.bedNumList) {
                    control.clear();
                    control.clearOptions();
                    data.bedNumList.sort(function(a,b){return b-a});
                    $.each(data.bedNumList,function (index,item) {
                        control.addOption({
                            id : item,
                            title : item
                        });
                    })
                }
            },
            error: function () {
                swal("OMG!", "发生了未知的错误！", "error");
            }
        });

        $("#partName").empty();
        $.ajax({
            url: "/erp/getopapartnamesbyordername",
            data: {"orderName": orderName},
            success:function(data){
                if (data.partNameList) {
                    $.each(data.partNameList, function(index,element){
                        $("#partName").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });
    });


    $("#orderNameOther").change(function () {
        var orderNameOther = $("#orderNameOther").val();
        $.ajax({
            url: "/erp/getcustomernamebyordername",
            data: {"orderName": orderNameOther},
            success:function(data){
                $("#customerNameOther").val(data);
            },
            error:function(){
            }
        });

        $("#partNameOther").empty();
        $.ajax({
            url: "/erp/getopapartnamesbyordername",
            data: {"orderName": orderNameOther},
            success:function(data){
                if (data.partNameList) {
                    $.each(data.partNameList, function(index,element){
                        $("#partNameOther").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });
    });



    $('#opaTable').DataTable( {
        language : {
            processing : "载入中",//处理页面数据的时候的显示
            paginate : {//分页的样式文本内容。
                previous : "上一页",
                next : "下一页",
                first : "第一页",
                last : "最后一页"
            },
            search:"搜索：",
            zeroRecords : "没有内容",//table tbody内容为空时，tbody的内容。
            //下面三者构成了总体的左下角的内容。
            info : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//左下角的信息显示，大写的词为关键字。
            infoEmpty : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//筛选为空时左下角的显示。
            infoFiltered : ""//筛选之后的左下角筛选提示(另一个是分页信息显示，在上面的info中已经设置，所以可以不显示)，
        },
        pageLength : 10,// 每页显示10条数据
        pagingType : "full_numbers", // 分页样式：simple,simple_numbers,full,full_numbers，
        "paging": true,
        // "ordering" : false,
        "info": true,
        searching:true,
        lengthChange:true,
        "scrollCollapse": true,  //表格高度随内容自适应
        dom: 'Bfrtip',
        "buttons": [{
            'extend': 'excel',
            'text': '导出',
            'className': 'btn btn-success', //按钮的class样式
            'title': '花片外发详情',
            'exportOptions': {
                'modifier': {
                    'page': 'all'
                },
                columns: ':visible:not(.noExport)'
            }
        }],
        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            // Total over all pages
            total = api
                .column( 7 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );


            // Total over this page
            pageTotal = api
                .column( 7, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            // Update footer
            $( api.column( 9 ).footer() ).html(
                "当前页数量："+pageTotal +"，总计："+ total
            );
        }
    } );

});

function autoComplete(keywords) {
    $("#orderName").empty();
    $.ajax({
        url: "/erp/getorderbyversion",
        data: {"clothesVersionNumber": keywords},
        success:function(data){
            $("#orderName").empty();
            if (data.orderList) {
                $("#orderName").append("<option value=''>选择订单</option>");
                $.each(data.orderList, function(index,element){
                    $("#orderName").append("<option value='"+element+"'>"+element+"</option>");
                })
            }
        },
        error:function(){
        }
    });
}


function autoCompleteOther(keywords) {
    $("#orderNameOther").empty();
    $.ajax({
        url: "/erp/getorderbyversion",
        data: {"clothesVersionNumber": keywords},
        success:function(data){
            $("#orderNameOther").empty();
            if (data.orderList) {
                $("#orderNameOther").append("<option value=''>选择订单</option>");
                $.each(data.orderList, function(index,element){
                    $("#orderNameOther").append("<option value='"+element+"'>"+element+"</option>");
                })
            }
        },
        error:function(){
        }
    });
}


var basePath=$("#basePath").val();

function addOPA() {
    $.blockUI({
        css: {
            width: '60%',
            top: '10%',
            left: '20%',
            border: 'none',
            padding: '15px',
            backgroundColor: '#fff',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: 1,
            color: '#000'
        },
        message: $('#editPro')
    });
    layui.use(['autocomplete'], function () {
        layui.autocomplete.render({
            elem: $('#clothesVersionNumber')[0],
            url: 'getversionhint',
            template_val: '{{d}}',
            template_txt: '{{d}}',
            onselect: function (resp) {
                autoComplete(resp)
            }
        })
    });
    var url = basePath + "erp/addopa";
    $("#editYes").unbind("click").bind("click", function () {
        if ($("clothesVersionNumber").val() == ""){
            swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入完整信息！</span>",html: true});
            return false;
        }
        if ($("orderName").val() == ""){
            swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入完整信息！</span>",html: true});
            return false;
        }
        if ($("customerName").val() == ""){
            swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入完整信息！</span>",html: true});
            return false;
        }
        if ($("destination").val() == ""){
            swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入完整信息！</span>",html: true});
            return false;
        }
        if ($("partName").val() == ""){
            swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入完整信息！</span>",html: true});
            return false;
        }
        if ($("opaDate").val() == ""){
            swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入完整信息！</span>",html: true});
            return false;
        }
        var opaJson = {};
        var bedNumbers = {};
        var selBedNumber = $("#bedNumber").val();
        if(selBedNumber == null) {
            swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请选择床号！</span>",html: true});
            return false;
        }
        $.each(selBedNumber,function (index,item) {
            bedNumbers[index] = item;
        });
        opaJson.clothesVersionNumber = $("#clothesVersionNumber").val();
        opaJson.orderName = $("#orderName").val();
        opaJson.customerName = $("#customerName").val();
        opaJson.destination = $("#destination").val();
        opaJson.bedNumbers = bedNumbers;
        opaJson.partName = $("#partName").val();
        opaJson.opaDate = $("#opaDate").val();
        $.ajax({
            url: url,
            type: 'POST',
            data: {
                opaJson:JSON.stringify(opaJson)
            },
            success: function (data) {
                if(data == 0) {
                    $.unblockUI();
                    $("input").val("");
                    swal({
                        type:"success",
                        title:"",
                        text: "<span style=\"font-weight:bolder;font-size: 20px\">恭喜你，保存成功！</span>",
                        html: true
                    },
                    function(){
                        location.href=basePath+"erp/opaStart";
                    });
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，保存失败！</span>",html: true});
                }
            },
            error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        });
    });
    $("#editNo").unbind("click").bind("click", function () {
        $.unblockUI();
        $("input").val("");
    });
}

function addOtherOPA() {
    $.blockUI({
        css: {
            width: '50%',
            top: '10%',
            border: 'none',
            padding: '15px',
            backgroundColor: '#fff',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: 1,
            color: '#000'
        },
        message: $('#editOtherPro')
    });
    var url = basePath + "erp/addotheropa";
    $("#editOtherYes").unbind("click").bind("click", function () {
        var flag = false;
        $("#editOtherPro input").each(function (index, item) {
            if($(this).val() == "") {
                flag = true;
                return false;
            }
        });
        if(flag) {
            swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入完全部信息！</span>",html: true});
            return false;
        }
        $.ajax({
            url: url,
            type: 'POST',
            data: {
                orderName:$("#orderNameOther").val(),
                customerName:$("#customerNameOther").val(),
                destination:$("#destinationOther").val(),
                bedNumber:$("#bedNumberOther").val(),
                partName:$("#partNameOther").val(),
                opaCount:$("#opaCountOther").val(),
                opaDate:$("#opaDateOther").val(),
            },
            success: function (data) {
                if(data == 0) {
                    $.unblockUI();
                    $("input").val("");
                    swal({
                            type:"success",
                            title:"",
                            text: "<span style=\"font-weight:bolder;font-size: 20px\">恭喜你，保存成功！</span>",
                            html: true
                        },
                        function(){
                            location.href=basePath+"erp/opaStart";
                        });
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，保存失败！</span>",html: true});
                }
            },
            error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        });
    });
    $("#editOtherNo").unbind("click").bind("click", function () {
        $.unblockUI();
        $("input").val("");
    });
}


function deleteOPA(opaID) {
    swal({
        title: "",
        text: "<span style=\"font-weight:bolder;font-size: 20px\">您确定要删除该条信息吗？</span>",
        type: "warning",
        html:true,
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: "确定",
        cancelButtonText:"我再想想",
        confirmButtonColor: "#ec6c62",
        showLoaderOnConfirm: true
    }, function() {
        $.ajax({
            url: basePath + "erp/deleteopa",
            type:'POST',
            data: {
                opaID:opaID
            },
            success: function (data) {
                if(data == 0) {
                    $.unblockUI();
                    $("input").val("");
                    swal({
                            type:"success",
                            title:"",
                            text: "<span style=\"font-weight:bolder;font-size: 20px\">恭喜你，删除成功！</span>",
                            html: true
                        },
                        function(){
                            location.href=basePath+"erp/opaStart";
                        });
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，删除失败！</span>",html: true});
                }
            },
            error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        })
    });
}

function detail(orderName, bedNumber, partName) {
    // partName = encodeURI(encodeURI(partName));
    var urlName = encodeURIComponent(orderName);
    var urlPartName = encodeURIComponent(partName);
    var tabOrderName = orderName.replace(/[&\|\\\*^%$#@\-+~`！!……——=「」{};:：；‘’\"\"/<>.,，。《》]/g,"");
    var tabPartName = orderName.replace(/[&\|\\\*^%$#@\-+~`！!……——=「」{};:：；‘’\"\"/<>.,，。《》]/g,"");
    var tabName = tabOrderName+bedNumber+tabPartName;
    $('#mainFrameTabs').bTabsAdd("tabId" + tabName, "详情", "/erp/detailOpaStart?orderName="+urlName+"&bedNumber="+bedNumber+"&partName="+urlPartName);
}

var opaTable;
function createTodayOPATable() {
    if (opaTable != undefined) {
        opaTable.clear(); //清空一下table
        opaTable.destroy(); //还原初始化了的datatable
    }
    $.blockUI({ css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        },
        message: "<h3>稍等一下... 奋力搜索中</h3>"
    });
    $.ajax({
        url: "/erp/gettodayopa",
        type:'GET',
        data: {
        },
        success: function (html) {
            $("#entities").html(html);
            $('html, body').scrollTop($("#entities").offset().top);

            $('#opaTable').DataTable({
                language : {
                    processing : "载入中",//处理页面数据的时候的显示
                    sLengthMenu: "显示 _MENU_ 项结果",
                    paginate : {//分页的样式文本内容。
                        previous : "上一页",
                        next : "下一页",
                        first : "第一页",
                        last : "最后一页"
                    },
                    search:"搜索：",
                    zeroRecords : "没有内容",//table tbody内容为空时，tbody的内容。
                    //下面三者构成了总体的左下角的内容。
                    info : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//左下角的信息显示，大写的词为关键字。
                    infoEmpty : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//筛选为空时左下角的显示。
                    infoFiltered : ""//筛选之后的左下角筛选提示(另一个是分页信息显示，在上面的info中已经设置，所以可以不显示)，
                },
                searching:true,
                lengthChange:true,
                pageLength : 25,// 每页显示10条数据
                pagingType : "full_numbers", // 分页样式：simple,simple_numbers,full,full_numbers，
                "paging" : true,
                "info": true,
                // scrollY: 600,
                scrollCollapse: true,
                scroller:       true,
                "footerCallback": function ( row, data, start, end, display ) {
                    var api = this.api(), data;

                    // Remove the formatting to get integer data for summation
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };

                    // Total over all pages
                    total = api
                        .column( 7 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Total over this page
                    pageTotal = api
                        .column( 7, { page: 'current'} )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Update footer
                    $( api.column( 4 ).footer() ).html(
                        // "总计数量："+ total,
                        "当前页数量："+pageTotal +"，总计："+ total
                    );
                }
            });
        },
        error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }
    });
    $.unblockUI();
}



function createOneMonthOPATable() {
    if (opaTable != undefined) {
        opaTable.clear(); //清空一下table
        opaTable.destroy(); //还原初始化了的datatable
    }
    $.blockUI({ css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        },
        message: "<h3>稍等一下... 奋力搜索中</h3>"
    });
    $.ajax({
        url: "/erp/getonemonthopa",
        type:'GET',
        data: {
        },
        success: function (html) {
            $("#entities").html(html);
            $('html, body').scrollTop($("#entities").offset().top);

            $('#opaTable').DataTable({
                language : {
                    processing : "载入中",//处理页面数据的时候的显示
                    sLengthMenu: "显示 _MENU_ 项结果",
                    paginate : {//分页的样式文本内容。
                        previous : "上一页",
                        next : "下一页",
                        first : "第一页",
                        last : "最后一页"
                    },
                    search:"搜索：",
                    zeroRecords : "没有内容",//table tbody内容为空时，tbody的内容。
                    //下面三者构成了总体的左下角的内容。
                    info : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//左下角的信息显示，大写的词为关键字。
                    infoEmpty : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//筛选为空时左下角的显示。
                    infoFiltered : ""//筛选之后的左下角筛选提示(另一个是分页信息显示，在上面的info中已经设置，所以可以不显示)，
                },
                searching:true,
                lengthChange:true,
                pageLength : 25,// 每页显示10条数据
                pagingType : "full_numbers", // 分页样式：simple,simple_numbers,full,full_numbers，
                "paging" : true,
                "info": true,
                // scrollY: 600,
                scrollCollapse: true,
                scroller:       true,
                "footerCallback": function ( row, data, start, end, display ) {
                    var api = this.api(), data;

                    // Remove the formatting to get integer data for summation
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };

                    // Total over all pages
                    total = api
                        .column( 7 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Total over this page
                    pageTotal = api
                        .column( 7, { page: 'current'} )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Update footer
                    $( api.column( 4 ).footer() ).html(
                        // "总计数量："+ total,
                        "当前页数量："+pageTotal +"，总计："+ total
                    );
                }
            });
        },
        error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }
    });
    $.unblockUI();
}


function createThreeMonthOPATable() {
    if (opaTable != undefined) {
        opaTable.clear(); //清空一下table
        opaTable.destroy(); //还原初始化了的datatable
    }
    $.blockUI({ css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        },
        message: "<h3>稍等一下... 奋力搜索中</h3>"
    });
    $.ajax({
        url: "/erp/getthreemonthopa",
        type:'GET',
        data: {
        },
        success: function (html) {
            $("#entities").html(html);
            $('html, body').scrollTop($("#entities").offset().top);

            $('#opaTable').DataTable({
                language : {
                    processing : "载入中",//处理页面数据的时候的显示
                    sLengthMenu: "显示 _MENU_ 项结果",
                    paginate : {//分页的样式文本内容。
                        previous : "上一页",
                        next : "下一页",
                        first : "第一页",
                        last : "最后一页"
                    },
                    search:"搜索：",
                    zeroRecords : "没有内容",//table tbody内容为空时，tbody的内容。
                    //下面三者构成了总体的左下角的内容。
                    info : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//左下角的信息显示，大写的词为关键字。
                    infoEmpty : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//筛选为空时左下角的显示。
                    infoFiltered : ""//筛选之后的左下角筛选提示(另一个是分页信息显示，在上面的info中已经设置，所以可以不显示)，
                },
                searching:true,
                lengthChange:true,
                pageLength : 25,// 每页显示10条数据
                pagingType : "full_numbers", // 分页样式：simple,simple_numbers,full,full_numbers，
                "paging" : true,
                "info": true,
                // scrollY: 600,
                scrollCollapse: true,
                scroller:       true,
                "footerCallback": function ( row, data, start, end, display ) {
                    var api = this.api(), data;

                    // Remove the formatting to get integer data for summation
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };

                    // Total over all pages
                    total = api
                        .column( 7 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Total over this page
                    pageTotal = api
                        .column( 7, { page: 'current'} )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Update footer
                    $( api.column( 4 ).footer() ).html(
                        // "总计数量："+ total,
                        "当前页数量："+pageTotal +"，总计："+ total
                    );
                }
            });
        },
        error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }
    });
    $.unblockUI();
}

function changeTable(obj){
    var opt = obj.options[obj.selectedIndex];
    if (opt.value == "today"){
        $.blockUI({ css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            },
            message: "<h3>稍等一下... 奋力搜索中</h3>"
        });
        createTodayOPATable();
        $.unblockUI();

    }
    if (opt.value == "oneMonth"){
        $.blockUI({ css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            },
            message: "<h3>稍等一下... 奋力搜索中</h3>"
        });
        createOneMonthOPATable()
        $.unblockUI();

    }
    if (opt.value == "threeMonths"){
        $.blockUI({ css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            },
            message: "<h3>稍等一下... 奋力搜索中</h3>"
        });
        createThreeMonthOPATable();
        $.unblockUI();
    }

}