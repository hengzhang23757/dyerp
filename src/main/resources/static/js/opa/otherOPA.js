var basePath=$("#basePath").val();
$(document).ready(function () {
    layui.laydate.render({
        elem: '#opaDate', //指定元素
        trigger: 'click'
    });

    createTodayOtherOPATable();
    layui.use(['autocomplete'], function () {
        layui.autocomplete.render({
            elem: $('#clothesVersionNumber')[0],
            url: 'getversionhint',
            template_val: '{{d}}',
            template_txt: '{{d}}',
            onselect: function (resp) {
                autoComplete(resp)
            }
        })
    });

    $("#orderName").change(function () {
        var orderName = $("#orderName").val();

        $("select[name='partName']").empty();
        $.ajax({
            url: "/erp/getopapartnamesbyordername",
            data: {"orderName": orderName},
            success:function(data){
                if (data.partNameList) {
                    $.each(data.partNameList, function(index,element){
                        $("select[name='partName']").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });

        $("select[name='colorName']").empty();
        $.ajax({
            url: "/erp/getcolorhint",
            data: {"orderName": orderName},
            success:function(data){
                if (data.colorList) {
                    $.each(data.colorList, function(index,element){
                        $("select[name='colorName']").append("<option value='"+element.colorName+"'>"+element.colorName+"</option>");
                    })
                }
            },
            error:function(){
            }
        });

        $("select[name='sizeName']").empty();
        $.ajax({
            url: "/erp/getordersizenamesbyorder",
            data: {"orderName": orderName},
            success:function(data){
                if (data.sizeNameList) {
                    $.each(data.sizeNameList, function(index,element){
                        $("select[name='sizeName']").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });
    });

    $('#otherOPATable').DataTable( {
        language : {
            processing : "载入中",//处理页面数据的时候的显示
            paginate : {//分页的样式文本内容。
                previous : "上一页",
                next : "下一页",
                first : "第一页",
                last : "最后一页"
            },
            search:"搜索：",
            zeroRecords : "没有内容",//table tbody内容为空时，tbody的内容。
            //下面三者构成了总体的左下角的内容。
            info : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//左下角的信息显示，大写的词为关键字。
            infoEmpty : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//筛选为空时左下角的显示。
            infoFiltered : ""//筛选之后的左下角筛选提示(另一个是分页信息显示，在上面的info中已经设置，所以可以不显示)，
        },
        pageLength : 25,// 每页显示10条数据
        pagingType : "full_numbers", // 分页样式：simple,simple_numbers,full,full_numbers，
        "paging": true,
        // "ordering" : false,
        "info": true,
        searching:true,
        lengthChange:true,
        "scrollCollapse": true,  //表格高度随内容自适应
        dom: 'Bfrtip',
        "buttons": [{
            'extend': 'excel',
            'text': '导出',
            'className': 'btn btn-success', //按钮的class样式
            'title': '士啤外发详情',
            'exportOptions': {
                'modifier': {
                    'page': 'all'
                },
                columns: ':visible:not(.noExport)'
            }
        }],
        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            // Total over all pages
            total = api
                .column( 8 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );


            // Total over this page
            pageTotal = api
                .column( 8, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            // Update footer
            $( api.column( 9 ).footer() ).html(
                "当前页数量："+pageTotal +"，总计："+ total
            );
        }
    } );


});

function autoComplete(keywords) {
    $("#orderName").empty();
    $.ajax({
        url: "/erp/getorderbyversion",
        data: {"clothesVersionNumber": keywords},
        success:function(data){
            $("#orderName").empty();
            if (data.orderList) {
                $("#orderName").append("<option value=''>选择订单</option>");
                $.each(data.orderList, function(index,element){
                    $("#orderName").append("<option value='"+element+"'>"+element+"</option>");
                })
            }
        },
        error:function(){
        }
    });
}

function addOtherOPA() {
    $.blockUI({
        css: {
            width: '90%',
            top: '5%',
            left: '5%',
            border: 'none',
            padding: '15px',
            backgroundColor: '#fff',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: 1,
            color: '#000'
        },
        message: $('#editPro')
    });
    layui.use(['autocomplete'], function () {
        layui.autocomplete.render({
            elem: $('#clothesVersionNumber')[0],
            url: 'getversionhint',
            template_val: '{{d}}',
            template_txt: '{{d}}',
            onselect: function (resp) {
                autoComplete(resp)
            }
        })
    });
    var url = basePath + "erp/addotheropabatch";
    $("#editYes").unbind("click").bind("click", function () {
        var flag = false;
        $("#editPro input").each(function (index, item) {
            if($(this).val() == "") {
                flag = true;
                return false;
            }
        });
        if(flag) {
            swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入完全部信息！</span>",html: true});
            return false;
        }
        var otherOpaJson = [];
        $("select[name='partName']").each(function (index,item) {
            var otherOpa = {};
            otherOpa.orderName = $("#orderName").val();
            otherOpa.clothesVersionNumber = $("#clothesVersionNumber").val();
            otherOpa.bedNumber = 0;
            otherOpa.customerName = "通用";
            otherOpa.destination = $("#destination").val();
            otherOpa.opaDate = $("#opaDate").val();
            otherOpa.partName = $(item).val();
            otherOpa.sizeName = $("select[name='sizeName']").eq(index).val();
            otherOpa.colorName = $("select[name='colorName']").eq(index).val();
            otherOpa.opaCount = $("input[name='opaCount']").eq(index).val();
            otherOpaJson.push(otherOpa);
        });
        $.ajax({
            url: url,
            type: 'POST',
            data: {
                otherOpaJson:JSON.stringify(otherOpaJson)
            },
            success: function (data) {
                if(data == 0) {
                    $.unblockUI();
                    $("input").val("");
                    swal({
                            type:"success",
                            title:"",
                            text: "<span style=\"font-weight:bolder;font-size: 20px\">恭喜你，保存成功！</span>",
                            html: true
                        },
                        function(){
                            location.href=basePath+"erp/otherOpaStart";
                        });
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，保存失败！</span>",html: true});
                }
            },
            error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        });
    });
    $("#editNo").unbind("click").bind("click", function () {
        $.unblockUI();
        $("input").val("");
        // $("input,select").val("");
        // $("select").empty();
    });
}



function deleteOtherOPA(otherOpaID) {
    swal({
        title: "",
        text: "<span style=\"font-weight:bolder;font-size: 20px\">您确定要删除该条信息吗？</span>",
        type: "warning",
        html:true,
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: "确定",
        cancelButtonText:"我再想想",
        confirmButtonColor: "#ec6c62",
        showLoaderOnConfirm: true
    }, function() {
        $.ajax({
            url: basePath + "erp/deleteotheropa",
            type:'POST',
            data: {
                otherOpaID:otherOpaID
            },
            success: function (data) {
                if(data == 0) {
                    $.unblockUI();
                    $("input").val("");
                    swal({
                            type:"success",
                            title:"",
                            text: "<span style=\"font-weight:bolder;font-size: 20px\">恭喜你，删除成功！</span>",
                            html: true
                        },
                        function(){
                            location.href=basePath+"erp/otherOpaStart";
                        });
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，删除失败！</span>",html: true});
                }
            },
            error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        })
    });
}

function addSize(obj) {
    $("#addSizeDiv").append($("tr[name='sizeDiv']:last").clone());
    $("input[name='opaCount']:last").val("");
    $("button[name='addSizeBtn']").remove();
    $("button[name='delSizeBtn']:first").before('<button name="addSizeBtn" class="btn" style="margin-bottom: 15px;outline:none;border-radius: 5px;color: white;" onclick="addSize(this)"><i class="fa fa-plus"></i></button>');
    $("button[name='delSizeBtn']:last").show();

}

function delSize(obj) {
    $(obj).parent().parent().parent().remove();
}


var otherOPATable;
function createTodayOtherOPATable() {
    if (otherOPATable != undefined) {
        otherOPATable.clear(); //清空一下table
        otherOPATable.destroy(); //还原初始化了的datatable
    }
    $.blockUI({ css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        },
        message: "<h3>稍等一下... 奋力搜索中</h3>"
    });
    $.ajax({
        url: "/erp/gettodayotheropa",
        type:'GET',
        data: {
        },
        success: function (html) {
            $("#entities").html(html);
            $('html, body').scrollTop($("#entities").offset().top);

            $('#otherOPATable').DataTable({
                language : {
                    processing : "载入中",//处理页面数据的时候的显示
                    sLengthMenu: "显示 _MENU_ 项结果",
                    paginate : {//分页的样式文本内容。
                        previous : "上一页",
                        next : "下一页",
                        first : "第一页",
                        last : "最后一页"
                    },
                    search:"搜索：",
                    zeroRecords : "没有内容",//table tbody内容为空时，tbody的内容。
                    //下面三者构成了总体的左下角的内容。
                    info : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//左下角的信息显示，大写的词为关键字。
                    infoEmpty : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//筛选为空时左下角的显示。
                    infoFiltered : ""//筛选之后的左下角筛选提示(另一个是分页信息显示，在上面的info中已经设置，所以可以不显示)，
                },
                searching:true,
                lengthChange:true,
                pageLength : 25,// 每页显示10条数据
                pagingType : "full_numbers", // 分页样式：simple,simple_numbers,full,full_numbers，
                "paging" : true,
                "info": true,
                // scrollY: 600,
                scrollCollapse: true,
                scroller:       true,
                "footerCallback": function ( row, data, start, end, display ) {
                    var api = this.api(), data;

                    // Remove the formatting to get integer data for summation
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };

                    // Total over all pages
                    total = api
                        .column( 8 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Total over this page
                    pageTotal = api
                        .column( 8, { page: 'current'} )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Update footer
                    $( api.column( 9 ).footer() ).html(
                        // "总计数量："+ total,
                        "当前页数量："+pageTotal +"，总计："+ total
                    );
                }
            });
        },
        error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }
    });
    $.unblockUI();
}



function createOneMonthOtherOPATable() {
    if (otherOPATable != undefined) {
        otherOPATable.clear(); //清空一下table
        otherOPATable.destroy(); //还原初始化了的datatable
    }
    $.blockUI({ css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        },
        message: "<h3>稍等一下... 奋力搜索中</h3>"
    });
    $.ajax({
        url: "/erp/getonemonthotheropa",
        type:'GET',
        data: {
        },
        success: function (html) {
            $("#entities").html(html);
            $('html, body').scrollTop($("#entities").offset().top);

            $('#otherOPATable').DataTable({
                language : {
                    processing : "载入中",//处理页面数据的时候的显示
                    sLengthMenu: "显示 _MENU_ 项结果",
                    paginate : {//分页的样式文本内容。
                        previous : "上一页",
                        next : "下一页",
                        first : "第一页",
                        last : "最后一页"
                    },
                    search:"搜索：",
                    zeroRecords : "没有内容",//table tbody内容为空时，tbody的内容。
                    //下面三者构成了总体的左下角的内容。
                    info : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//左下角的信息显示，大写的词为关键字。
                    infoEmpty : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//筛选为空时左下角的显示。
                    infoFiltered : ""//筛选之后的左下角筛选提示(另一个是分页信息显示，在上面的info中已经设置，所以可以不显示)，
                },
                searching:true,
                lengthChange:true,
                pageLength : 25,// 每页显示10条数据
                pagingType : "full_numbers", // 分页样式：simple,simple_numbers,full,full_numbers，
                "paging" : true,
                "info": true,
                // scrollY: 600,
                scrollCollapse: true,
                scroller:       true,
                "footerCallback": function ( row, data, start, end, display ) {
                    var api = this.api(), data;

                    // Remove the formatting to get integer data for summation
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };

                    // Total over all pages
                    total = api
                        .column( 8 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Total over this page
                    pageTotal = api
                        .column( 8, { page: 'current'} )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Update footer
                    $( api.column( 9 ).footer() ).html(
                        // "总计数量："+ total,
                        "当前页数量："+pageTotal +"，总计："+ total
                    );
                }
            });
        },
        error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }
    });
    $.unblockUI();
}


function createThreeMonthOtherOPATable() {
    if (otherOPATable != undefined) {
        otherOPATable.clear(); //清空一下table
        otherOPATable.destroy(); //还原初始化了的datatable
    }
    $.blockUI({ css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        },
        message: "<h3>稍等一下... 奋力搜索中</h3>"
    });
    $.ajax({
        url: "/erp/getthreemonthotheropa",
        type:'GET',
        data: {
        },
        success: function (html) {
            $("#entities").html(html);
            $('html, body').scrollTop($("#entities").offset().top);

            $('#otherOPATable').DataTable({
                language : {
                    processing : "载入中",//处理页面数据的时候的显示
                    sLengthMenu: "显示 _MENU_ 项结果",
                    paginate : {//分页的样式文本内容。
                        previous : "上一页",
                        next : "下一页",
                        first : "第一页",
                        last : "最后一页"
                    },
                    search:"搜索：",
                    zeroRecords : "没有内容",//table tbody内容为空时，tbody的内容。
                    //下面三者构成了总体的左下角的内容。
                    info : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//左下角的信息显示，大写的词为关键字。
                    infoEmpty : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//筛选为空时左下角的显示。
                    infoFiltered : ""//筛选之后的左下角筛选提示(另一个是分页信息显示，在上面的info中已经设置，所以可以不显示)，
                },
                searching:true,
                lengthChange:true,
                pageLength : 25,// 每页显示10条数据
                pagingType : "full_numbers", // 分页样式：simple,simple_numbers,full,full_numbers，
                "paging" : true,
                "info": true,
                // scrollY: 600,
                scrollCollapse: true,
                scroller:       true,
                "footerCallback": function ( row, data, start, end, display ) {
                    var api = this.api(), data;

                    // Remove the formatting to get integer data for summation
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };

                    // Total over all pages
                    total = api
                        .column( 8 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Total over this page
                    pageTotal = api
                        .column( 8, { page: 'current'} )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Update footer
                    $( api.column( 4 ).footer() ).html(
                        // "总计数量："+ total,
                        "当前页数量："+pageTotal +"，总计："+ total
                    );
                }
            });
        },
        error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }
    });
    $.unblockUI();
}

function changeTable(obj){
    var opt = obj.options[obj.selectedIndex];
    if (opt.value == "today"){
        $.blockUI({ css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            },
            message: "<h3>稍等一下... 奋力搜索中</h3>"
        });
        createTodayOtherOPATable();
        $.unblockUI();

    }
    if (opt.value == "oneMonth"){
        $.blockUI({ css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            },
            message: "<h3>稍等一下... 奋力搜索中</h3>"
        });
        createOneMonthOtherOPATable()
        $.unblockUI();

    }
    if (opt.value == "threeMonths"){
        $.blockUI({ css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            },
            message: "<h3>稍等一下... 奋力搜索中</h3>"
        });
        createThreeMonthOtherOPATable();
        $.unblockUI();
    }

}