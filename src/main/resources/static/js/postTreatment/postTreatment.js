var form;
layui.use('form', function(){
    form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
    form.render();
});
layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.21'
}).extend({
    soulTable: 'soulTable'
});
$(document).ready(function () {
    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumber", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });

    layui.laydate.render({
        elem: '#from',
        trigger: 'click'
    });
    layui.laydate.render({
        elem: '#to',
        trigger: 'click'
    });

    $.ajax({
        url: "/erp/getallcustomername",
        data: {},
        success:function(data){
            $("#customerName").empty();
            if (data.customerNameList) {
                $("#customerName").append("<option value=''>选择品牌</option>");
                $.each(data.customerNameList, function(index,element){
                    $("#customerName").append("<option value='"+element+"'>"+element+"</option>");
                });
                form.render('select');
            }
        }, error:function(){
        }
    });

    $.ajax({
        url: "/erp/gethistoryseason",
        data: {},
        success:function(data){
            $("#season").empty();
            if (data.seasonList) {
                $("#season").append("<option value=''>选择季度</option>");
                $.each(data.seasonList, function(index,element){
                    $("#season").append("<option value='"+element+"'>"+element+"</option>");
                });
                form.render('select');
            }
        }, error:function(){
        }
    });

});
$(document).on('keydown', '.layui-table-edit', function (e) {
    var td = $(this).parent('td')
        ,tr = td.parent('tr')
        ,trs = tr.parent().parent().find('tr')
        ,tr_index = tr.index()
        ,td_index = td.index()
        ,td_last_index = tr.find('[data-edit="text"]:last').index()
        ,td_first_index = tr.find('[data-edit="text"]:first').index();
    switch (e.keyCode) {
        case 13:
            tr.next().find('td').eq(td_index).click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            return false;

        case 39:
            td.nextAll('[data-edit="text"]:first').click();
            if (td_index == td_last_index){
                tr.next().find('td').eq(td_first_index).click();
                if (tr_index == trs.length - 1){
                    trs.eq(0).find('td').eq(td_first_index).click();
                }
            }
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
        case 37:
            td.prevAll('[data-edit="text"]:first').click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
        case 38:
            tr.prev().find('td').eq(td_index).click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
        case 40:
            tr.next().find('td').eq(td_index).click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
    }

});
var reportTable;

layui.use(['form', 'table', 'soulTable'], function () {
    var table = layui.table,
        soulTable = layui.soulTable,
        $ = layui.$;
    reportTable = table.render({
        elem: '#reportTable'
        ,cols:[[]]
        ,excel: {
            filename: '尾数报表.xlsx'
        }
        ,height: 'full-130'
        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
        ,title: '尾数管理'
        ,totalRow: true
        ,loading: false
        ,page: true
        ,even: true
        ,limits: [500, 1000, 2000]
        ,limit: 1000 //每页默认显示的数量
        ,done: function () {
            soulTable.render(this);
        }
    });

    //监听提交
    form.on('submit(searchBeat)', function(data){
        var param = {};
        var orderName = $("#orderName").val();
        var season = $("#season").val();
        var customerName = $("#customerName").val();
        var from = $("#from").val();
        var to = $("#to").val();
        if ((season == null || season === "") && (customerName == null || customerName === "") && (orderName == null || orderName === "")){
            layer.msg("季度-品牌-款号不能同时为空", { icon: 2 });
            return false;
        }
        if (season != null && season != ""){
            param.season = season;
        }
        if (customerName != null && customerName != ""){
            param.customerName = customerName;
        }
        if (orderName != null && orderName != ""){
            param.orderName = orderName;
        }
        var load= layer.load();
        $.ajax({
            url: "/erp/searchposttreatmentbyinfo",
            type: 'GET',
            data: param,
            success: function (res) {
                if (res.postTreatmentList) {
                    var reportData = res.postTreatmentList;
                    layui.table.reload("reportTable", {
                        cols:[[
                            {title: '#', width: 50, collapse: true,lazy: true, icon: ['layui-icon layui-icon-triangle-r', 'layui-icon layui-icon-triangle-d'], children:[
                                    {
                                        title: '洗水记录'
                                        ,url: 'getposttreatmentbyordertype'
                                        ,where: function(row){
                                            return {
                                                orderName: row.orderName,
                                                washCount: 1
                                            }
                                        }
                                        ,height: 300
                                        ,loading:true
                                        ,page: false
                                        ,totalRow: true
                                        ,cols: [[
                                            {field: 'clothesVersionNumber', title: '单号',align:'center', minWidth: 120},
                                            {field: 'orderName', title: '款号',align:'center', minWidth: 120},
                                            {field: 'colorName', title: '颜色',align:'center', minWidth: 120},
                                            {field: 'sizeName', title: '尺码',align:'center', minWidth: 120 },
                                            {field: 'washCount', title: '数量',align:'center', minWidth: 120, totalRow: true},
                                            {field: 'createTime', title: '日期',align:'center', minWidth: 120},
                                            {field: 'id', title: '操作', minWidth: 70,align:'center', templet: '#washCountChildBar'}
                                        ]]
                                        ,done: function () {
                                            soulTable.render(this);
                                        }
                                        ,toolEvent: function (obj, pobj) {
                                            // obj 子表当前行对象
                                            // pobj 父表当前行对象
                                            var objData = obj.data;
                                            var childId = this.id; // 通过 this 对象获取当前子表的id
                                            if (obj.event === 'deleteWashCount'){
                                                layer.confirm('真的删除吗', function(index){
                                                    $.ajax({
                                                        url: "/erp/deleteposttreatmentbyid",
                                                        type: 'POST',
                                                        data: {
                                                            id: objData.id
                                                        },
                                                        success: function (res) {
                                                            if (res.data > 0) {
                                                                layer.close(index);
                                                                layer.msg("删除成功！", {icon: 1});
                                                                table.reload(childId);
                                                            } else {
                                                                layer.msg("删除失败！", {icon: 2});
                                                            }
                                                        }, error: function () {
                                                            layer.msg("删除失败！", {icon: 2});
                                                        }
                                                    })
                                                });
                                            } else if (obj.event === 'updateWashCount'){
                                                var index = layer.open({
                                                    type: 1 //Page层类型
                                                    , title: '修改'
                                                    , btn: ['保存']
                                                    , shade: 0.6 //遮罩透明度
                                                    , area: ['300px','200px']
                                                    , maxmin: false //允许全屏最小化
                                                    , anim: 0 //0-6的动画形式，-1不开启
                                                    , offset: '100px'
                                                    , content: "<div><table><tr>" +
                                                        "                                            <td style=\"text-align: right;margin-bottom: 15px;\">\n" +
                                                        "                                                <label class=\"layui-form-label\">数量</label>\n" +
                                                        "                                            </td>\n" +
                                                        "                                            <td style=\"margin-bottom: 15px;\">\n" +
                                                        "                                                <input type=\"text\" name=\"updateWashCount\" id=\"updateWashCount\" autocomplete=\"off\" class=\"layui-input\">\n" +
                                                        "                                            </td></tr><tr>" +
                                                        "                                            <td style=\"text-align: right;margin-bottom: 15px;\">\n" +
                                                        "                                                <label class=\"layui-form-label\">日期</label>\n" +
                                                        "                                            </td>\n" +
                                                        "                                            <td style=\"margin-bottom: 15px;\">\n" +
                                                        "                                                <input type=\"text\" name=\"updateWashDate\" style='width: 200px' id=\"updateWashDate\" autocomplete=\"off\" class=\"layui-input\">\n" +
                                                        "                                            </td>" +
                                                        "</tr></table></div>"
                                                    ,yes: function(i, layero){
                                                        var updateWashCount = $("#updateWashCount").val();
                                                        var updateWashDate = $("#updateWashDate").val();
                                                        var postTreatmentData = {"id": objData.id, "washCount": updateWashCount, "createTime": updateWashDate};
                                                        $.ajax({
                                                            url: "/erp/updateposttreatment",
                                                            type:'POST',
                                                            data: {
                                                                postTreatmentJson: JSON.stringify(postTreatmentData)
                                                            },
                                                            success:function(res){
                                                                if(res.data == 0) {
                                                                    table.reload(childId);
                                                                    layer.close(index);
                                                                    layer.msg("修改成功！", {icon: 1});
                                                                }else {
                                                                    layer.msg("修改失败！", {icon: 2});
                                                                }
                                                            }, error:function(){
                                                                layer.msg("修改失败！", {icon: 1});
                                                            }
                                                        });
                                                    }
                                                });
                                                setTimeout(function () {
                                                    $("#updateWashCount").val(objData.washCount);
                                                    $("#updateWashDate").val(objData.createTime);
                                                    layui.laydate.render({
                                                        elem: '#updateWashDate',
                                                        trigger: 'click'
                                                    });
                                                },100);
                                            }

                                        }
                                    }, {
                                        title: '抽办'
                                        ,url: 'getposttreatmentbyordertype'
                                        ,where: function(row){
                                            return {
                                                orderName: row.orderName,
                                                sampleCount: 1
                                            }
                                        }
                                        ,height: 300
                                        ,loading:true
                                        ,page: false
                                        ,totalRow: true
                                        ,cols: [[
                                            {field: 'clothesVersionNumber', title: '单号',align:'center', minWidth: 120},
                                            {field: 'orderName', title: '款号',align:'center', minWidth: 120},
                                            {field: 'colorName', title: '颜色',align:'center', minWidth: 120},
                                            {field: 'sizeName', title: '尺码',align:'center', minWidth: 120 },
                                            {field: 'sampleCount', title: '数量',align:'center', minWidth: 120, totalRow: true},
                                            {field: 'sampleLocation', title: '事由',align:'center', minWidth: 120, totalRow: true},
                                            {field: 'createTime', title: '日期',align:'center', minWidth: 120},
                                            {field: 'id', title: '操作', minWidth: 70,align:'center', templet: '#sampleCountChildBar'}
                                        ]]
                                        ,done: function () {
                                            soulTable.render(this);
                                        }
                                        ,toolEvent: function (obj, pobj) {
                                            // obj 子表当前行对象
                                            // pobj 父表当前行对象
                                            var objData = obj.data;
                                            var childId = this.id; // 通过 this 对象获取当前子表的id
                                            if (obj.event === 'deleteSampleCount'){
                                                layer.confirm('真的删除吗', function(index){
                                                    $.ajax({
                                                        url: "/erp/deleteposttreatmentbyid",
                                                        type: 'POST',
                                                        data: {
                                                            id: objData.id
                                                        },
                                                        success: function (res) {
                                                            if (res.data > 0) {
                                                                layer.close(index);
                                                                layer.msg("删除成功！", {icon: 1});
                                                                table.reload(childId);
                                                            } else {
                                                                layer.msg("删除失败！", {icon: 2});
                                                            }
                                                        }, error: function () {
                                                            layer.msg("删除失败！", {icon: 2});
                                                        }
                                                    })
                                                });
                                            } else if (obj.event === 'updateSampleCount'){
                                                var index = layer.open({
                                                    type: 1 //Page层类型
                                                    , title: '修改'
                                                    , btn: ['保存']
                                                    , shade: 0.6 //遮罩透明度
                                                    , area: ['400px','400px']
                                                    , maxmin: false //允许全屏最小化
                                                    , anim: 0 //0-6的动画形式，-1不开启
                                                    , offset: '100px'
                                                    , content: "<div><table>" +
                                                        "                                            <tr>" +
                                                        "                                            <td style=\"text-align: right;margin-bottom: 15px;\">\n" +
                                                        "                                                <label class=\"layui-form-label\">数量</label>\n" +
                                                        "                                            </td>\n" +
                                                        "                                            <td style=\"margin-bottom: 15px;\">\n" +
                                                        "                                                <input type=\"text\" name=\"updateSampleCount\" id=\"updateSampleCount\" autocomplete=\"off\" class=\"layui-input\">\n" +
                                                        "                                            </td></tr>" +
                                                        "                                            <tr>" +
                                                        "                                            <td style=\"text-align: right;margin-bottom: 15px;\">\n" +
                                                        "                                                <label class=\"layui-form-label\">事由</label>\n" +
                                                        "                                            </td>\n" +
                                                        "                                            <td style=\"margin-bottom: 15px;\">\n" +
                                                        "                                                <input type=\"text\" name=\"updateSampleLocation\" id=\"updateSampleLocation\" autocomplete=\"off\" class=\"layui-input\">\n" +
                                                        "                                            </td></tr>" +
                                                        "                                            <tr>" +
                                                        "                                            <td style=\"text-align: right;margin-bottom: 15px;\">\n" +
                                                        "                                                <label class=\"layui-form-label\">日期</label>\n" +
                                                        "                                            </td>\n" +
                                                        "                                            <td style=\"margin-bottom: 15px;\">\n" +
                                                        "                                                <input type=\"text\" name=\"updateSampleDate\" style='width: 200px' id=\"updateDiffDate\" autocomplete=\"off\" class=\"layui-input\">\n" +
                                                        "                                            </td>" +
                                                        "</tr></table></div>"
                                                    ,yes: function(i, layero){
                                                        var updateSampleCount = $("#updateSampleCount").val();
                                                        var updateSampleDate = $("#updateSampleDate").val();
                                                        var updateSampleLocation = $("#updateSampleLocation").val();
                                                        var postTreatmentData = {"id": objData.id, "sampleCount": updateSampleCount, "sampleLocation": updateSampleLocation, "createTime": updateSampleDate};
                                                        $.ajax({
                                                            url: "/erp/updateposttreatment",
                                                            type:'POST',
                                                            data: {
                                                                postTreatmentJson: JSON.stringify(postTreatmentData)
                                                            },
                                                            success:function(res){
                                                                if(res.data == 0) {
                                                                    table.reload(childId);
                                                                    layer.close(index);
                                                                    layer.msg("修改成功！", {icon: 1});
                                                                }else {
                                                                    layer.msg("修改失败！", {icon: 2});
                                                                }
                                                            }, error:function(){
                                                                layer.msg("修改失败！", {icon: 1});
                                                            }
                                                        });
                                                    }
                                                });
                                                setTimeout(function () {
                                                    $("#updateSampleCount").val(objData.sampleCount);
                                                    $("#updateSampleLocation").val(objData.sampleLocation);
                                                    $("#updateDiffDate").val(objData.createTime);
                                                    layui.laydate.render({
                                                        elem: '#updateSampleDate',
                                                        trigger: 'click'
                                                    });
                                                },100);
                                            }

                                        }
                                    },{
                                        title: '出货记录'
                                        ,url: 'getposttreatmentbyordertype'
                                        ,where: function(row){
                                            return {
                                                orderName: row.orderName,
                                                shipCount: 1
                                            }
                                        }
                                        ,height: 300
                                        ,loading:true
                                        ,page: false
                                        ,totalRow: true
                                        ,cols: [[
                                            {field: 'clothesVersionNumber', title: '单号',align:'center', minWidth: 120},
                                            {field: 'orderName', title: '款号',align:'center', minWidth: 120},
                                            {field: 'colorName', title: '颜色',align:'center', minWidth: 120},
                                            {field: 'sizeName', title: '尺码',align:'center', minWidth: 120 },
                                            {field: 'shipCount', title: '数量',align:'center', minWidth: 120, totalRow: true},
                                            {field: 'createTime', title: '日期',align:'center', minWidth: 120},
                                            {field: 'id', title: '操作', minWidth: 70,align:'center', templet: '#shipCountChildBar'}
                                        ]]
                                        ,done: function () {
                                            soulTable.render(this);
                                        }
                                        ,toolEvent: function (obj, pobj) {
                                            // obj 子表当前行对象
                                            // pobj 父表当前行对象
                                            var objData = obj.data;
                                            var childId = this.id; // 通过 this 对象获取当前子表的id
                                            if (obj.event === 'deleteShipCount'){
                                                layer.confirm('真的删除吗', function(index){
                                                    $.ajax({
                                                        url: "/erp/deleteposttreatmentbyid",
                                                        type: 'POST',
                                                        data: {
                                                            id: objData.id
                                                        },
                                                        success: function (res) {
                                                            if (res.data > 0) {
                                                                layer.close(index);
                                                                layer.msg("删除成功！", {icon: 1});
                                                                table.reload(childId);
                                                            } else {
                                                                layer.msg("删除失败！", {icon: 2});
                                                            }
                                                        }, error: function () {
                                                            layer.msg("删除失败！", {icon: 2});
                                                        }
                                                    })
                                                });
                                            } else if (obj.event === 'updateShipCount'){
                                                var index = layer.open({
                                                    type: 1 //Page层类型
                                                    , title: '修改'
                                                    , btn: ['保存']
                                                    , shade: 0.6 //遮罩透明度
                                                    , area: ['300px','200px']
                                                    , maxmin: false //允许全屏最小化
                                                    , anim: 0 //0-6的动画形式，-1不开启
                                                    , offset: '100px'
                                                    , content: "<div><table><tr>" +
                                                        "                                            <td style=\"text-align: right;margin-bottom: 15px;\">\n" +
                                                        "                                                <label class=\"layui-form-label\">数量</label>\n" +
                                                        "                                            </td>\n" +
                                                        "                                            <td style=\"margin-bottom: 15px;\">\n" +
                                                        "                                                <input type=\"text\" name=\"updateShipCount\" id=\"updateShipCount\" autocomplete=\"off\" class=\"layui-input\">\n" +
                                                        "                                            </td></tr><tr>" +
                                                        "                                            <td style=\"text-align: right;margin-bottom: 15px;\">\n" +
                                                        "                                                <label class=\"layui-form-label\">日期</label>\n" +
                                                        "                                            </td>\n" +
                                                        "                                            <td style=\"margin-bottom: 15px;\">\n" +
                                                        "                                                <input type=\"text\" name=\"updateShipDate\" style='width: 200px' id=\"updateShipDate\" autocomplete=\"off\" class=\"layui-input\">\n" +
                                                        "                                            </td>" +
                                                        "</tr></table></div>"
                                                    ,yes: function(i, layero){
                                                        var updateShipCount = $("#updateShipCount").val();
                                                        var updateShipDate = $("#updateShipDate").val();
                                                        var postTreatmentData = {"id": objData.id, "shipCount": updateShipCount, "createTime": updateShipDate};
                                                        $.ajax({
                                                            url: "/erp/updateposttreatment",
                                                            type:'POST',
                                                            data: {
                                                                postTreatmentJson: JSON.stringify(postTreatmentData)
                                                            },
                                                            success:function(res){
                                                                if(res.data == 0) {
                                                                    table.reload(childId);
                                                                    layer.close(index);
                                                                    layer.msg("修改成功！", {icon: 1});
                                                                }else {
                                                                    layer.msg("修改失败！", {icon: 2});
                                                                }
                                                            }, error:function(){
                                                                layer.msg("修改失败！", {icon: 1});
                                                            }
                                                        });
                                                    }
                                                });
                                                setTimeout(function () {
                                                    $("#updateShipCount").val(objData.shipCount);
                                                    $("#updateShipDate").val(objData.createTime);
                                                    layui.laydate.render({
                                                        elem: '#updateShipDate',
                                                        trigger: 'click'
                                                    });
                                                },100);
                                            }

                                        }
                                    }, {
                                        title: '正品余数'
                                        ,url: 'getposttreatmentbyordertype'
                                        ,where: function(row){
                                            return {
                                                orderName: row.orderName,
                                                surplusCount: 1
                                            }
                                        }
                                        ,height: 300
                                        ,loading:true
                                        ,page: false
                                        ,totalRow: true
                                        ,cols: [[
                                            {field: 'clothesVersionNumber', title: '单号',align:'center', minWidth: 120},
                                            {field: 'orderName', title: '款号',align:'center', minWidth: 120},
                                            {field: 'colorName', title: '颜色',align:'center', minWidth: 120},
                                            {field: 'sizeName', title: '尺码',align:'center', minWidth: 120 },
                                            {field: 'surplusCount', title: '数量',align:'center', minWidth: 120, totalRow: true},
                                            {field: 'surplusLocation', title: '位置',align:'center', minWidth: 120},
                                            {field: 'createTime', title: '日期',align:'center', minWidth: 120},
                                            {field: 'id', title: '操作', minWidth: 70,align:'center', templet: '#surplusCountChildBar'}
                                        ]]
                                        ,done: function () {
                                            soulTable.render(this);
                                        }
                                        ,toolEvent: function (obj, pobj) {
                                            // obj 子表当前行对象
                                            // pobj 父表当前行对象
                                            var objData = obj.data;
                                            var childId = this.id; // 通过 this 对象获取当前子表的id
                                            if (obj.event === 'deleteSurplusCount'){
                                                layer.confirm('真的删除吗', function(index){
                                                    $.ajax({
                                                        url: "/erp/deleteposttreatmentbyid",
                                                        type: 'POST',
                                                        data: {
                                                            id: objData.id
                                                        },
                                                        success: function (res) {
                                                            if (res.data > 0) {
                                                                layer.close(index);
                                                                layer.msg("删除成功！", {icon: 1});
                                                                table.reload(childId);
                                                            } else {
                                                                layer.msg("删除失败！", {icon: 2});
                                                            }
                                                        }, error: function () {
                                                            layer.msg("删除失败！", {icon: 2});
                                                        }
                                                    })
                                                });
                                            } else if (obj.event === 'updateSurplusCount'){
                                                var index = layer.open({
                                                    type: 1 //Page层类型
                                                    , title: '修改'
                                                    , btn: ['保存']
                                                    , shade: 0.6 //遮罩透明度
                                                    , area: ['300px','250px']
                                                    , maxmin: false //允许全屏最小化
                                                    , anim: 0 //0-6的动画形式，-1不开启
                                                    , offset: '100px'
                                                    , content: "<div><table><tr>" +
                                                        "                                            <td style=\"text-align: right;margin-bottom: 15px;\">\n" +
                                                        "                                                <label class=\"layui-form-label\">数量</label>\n" +
                                                        "                                            </td>\n" +
                                                        "                                            <td style=\"margin-bottom: 15px;\">\n" +
                                                        "                                                <input type=\"text\" name=\"updateSurplusCount\" id=\"updateSurplusCount\" autocomplete=\"off\" class=\"layui-input\">\n" +
                                                        "                                            </td></tr><tr>" +
                                                        "                                            <td style=\"text-align: right;margin-bottom: 15px;\">\n" +
                                                        "                                                <label class=\"layui-form-label\">位置</label>\n" +
                                                        "                                            </td>\n" +
                                                        "                                            <td style=\"margin-bottom: 15px;\">\n" +
                                                        "                                                <input type=\"text\" name=\"updateSurplusLocation\" id=\"updateSurplusLocation\" autocomplete=\"off\" class=\"layui-input\">\n" +
                                                        "                                            </td></tr><tr>" +
                                                        "                                            <td style=\"text-align: right;margin-bottom: 15px;\">\n" +
                                                        "                                                <label class=\"layui-form-label\">日期</label>\n" +
                                                        "                                            </td>\n" +
                                                        "                                            <td style=\"margin-bottom: 15px;\">\n" +
                                                        "                                                <input type=\"text\" name=\"updateSurplusDate\" style='width: 200px' id=\"updateSurplusDate\" autocomplete=\"off\" class=\"layui-input\">\n" +
                                                        "                                            </td>" +
                                                        "</tr></table></div>"
                                                    ,yes: function(i, layero){
                                                        var updateSurplusCount = $("#updateSurplusCount").val();
                                                        var updateSurplusDate = $("#updateSurplusDate").val();
                                                        var updateSurplusLocation = $("#updateSurplusLocation").val();
                                                        var postTreatmentData = {"id": objData.id, "surplusLocation": updateSurplusLocation, "surplusCount": updateSurplusCount, "createTime": updateSurplusDate};
                                                        $.ajax({
                                                            url: "/erp/updateposttreatment",
                                                            type:'POST',
                                                            data: {
                                                                postTreatmentJson: JSON.stringify(postTreatmentData)
                                                            },
                                                            success:function(res){
                                                                if(res.data == 0) {
                                                                    table.reload(childId);
                                                                    layer.close(index);
                                                                    layer.msg("修改成功！", {icon: 1});
                                                                }else {
                                                                    layer.msg("修改失败！", {icon: 2});
                                                                }
                                                            }, error:function(){
                                                                layer.msg("修改失败！", {icon: 1});
                                                            }
                                                        });
                                                    }
                                                });
                                                setTimeout(function () {
                                                    $("#updateSurplusCount").val(objData.surplusCount);
                                                    $("#updateSurplusDate").val(objData.createTime);
                                                    $("#updateSurplusLocation").val(objData.surplusLocation);
                                                    layui.laydate.render({
                                                        elem: '#updateSurplusDate',
                                                        trigger: 'click'
                                                    });
                                                },100);
                                            }

                                        }
                                    }, {
                                        title: '次品数量'
                                        ,url: 'getposttreatmentbyordertype'
                                        ,where: function(row){
                                            return {
                                                orderName: row.orderName,
                                                defectiveCount: 1
                                            }
                                        }
                                        ,height: 300
                                        ,loading:true
                                        ,page: false
                                        ,totalRow: true
                                        ,cols: [[
                                            {field: 'clothesVersionNumber', title: '单号',align:'center', minWidth: 120},
                                            {field: 'orderName', title: '款号',align:'center', minWidth: 120},
                                            {field: 'colorName', title: '颜色',align:'center', minWidth: 120},
                                            {field: 'sizeName', title: '尺码',align:'center', minWidth: 120 },
                                            {field: 'defectiveCount', title: '数量',align:'center', minWidth: 120, totalRow: true},
                                            {field: 'defectiveLocation', title: '位置',align:'center', minWidth: 120},
                                            {field: 'createTime', title: '日期',align:'center', minWidth: 120},
                                            {field: 'id', title: '操作', minWidth: 70,align:'center', templet: '#defectiveCountChildBar'}
                                        ]]
                                        ,done: function () {
                                            soulTable.render(this);
                                        }
                                        ,toolEvent: function (obj, pobj) {
                                            // obj 子表当前行对象
                                            // pobj 父表当前行对象
                                            var objData = obj.data;
                                            var childId = this.id; // 通过 this 对象获取当前子表的id
                                            if (obj.event === 'deleteDefectiveCount'){
                                                layer.confirm('真的删除吗', function(index){
                                                    $.ajax({
                                                        url: "/erp/deleteposttreatmentbyid",
                                                        type: 'POST',
                                                        data: {
                                                            id: objData.id
                                                        },
                                                        success: function (res) {
                                                            if (res.data > 0) {
                                                                layer.close(index);
                                                                layer.msg("删除成功！", {icon: 1});
                                                                table.reload(childId);
                                                            } else {
                                                                layer.msg("删除失败！", {icon: 2});
                                                            }
                                                        }, error: function () {
                                                            layer.msg("删除失败！", {icon: 2});
                                                        }
                                                    })
                                                });
                                            } else if (obj.event === 'updateDefectiveCount'){
                                                var index = layer.open({
                                                    type: 1 //Page层类型
                                                    , title: '修改'
                                                    , btn: ['保存']
                                                    , shade: 0.6 //遮罩透明度
                                                    , area: ['300px','250px']
                                                    , maxmin: false //允许全屏最小化
                                                    , anim: 0 //0-6的动画形式，-1不开启
                                                    , offset: '100px'
                                                    , content: "<div><table><tr>" +
                                                        "                                            <td style=\"text-align: right;margin-bottom: 15px;\">\n" +
                                                        "                                                <label class=\"layui-form-label\">数量</label>\n" +
                                                        "                                            </td>\n" +
                                                        "                                            <td style=\"margin-bottom: 15px;\">\n" +
                                                        "                                                <input type=\"text\" name=\"updateDefectiveCount\" id=\"updateDefectiveCount\" autocomplete=\"off\" class=\"layui-input\">\n" +
                                                        "                                            </td></tr><tr>" +
                                                        "                                            <td style=\"text-align: right;margin-bottom: 15px;\">\n" +
                                                        "                                                <label class=\"layui-form-label\">位置</label>\n" +
                                                        "                                            </td>\n" +
                                                        "                                            <td style=\"margin-bottom: 15px;\">\n" +
                                                        "                                                <input type=\"text\" name=\"updateDefectiveLocation\" id=\"updateDefectiveLocation\" autocomplete=\"off\" class=\"layui-input\">\n" +
                                                        "                                            </td></tr><tr>" +
                                                        "                                            <td style=\"text-align: right;margin-bottom: 15px;\">\n" +
                                                        "                                                <label class=\"layui-form-label\">日期</label>\n" +
                                                        "                                            </td>\n" +
                                                        "                                            <td style=\"margin-bottom: 15px;\">\n" +
                                                        "                                                <input type=\"text\" name=\"updateDefectiveDate\" style='width: 200px' id=\"updateDefectiveDate\" autocomplete=\"off\" class=\"layui-input\">\n" +
                                                        "                                            </td>" +
                                                        "</tr></table></div>"
                                                    ,yes: function(i, layero){
                                                        var updateDefectiveCount = $("#updateDefectiveCount").val();
                                                        var updateDefectiveDate = $("#updateDefectiveDate").val();
                                                        var updateDefectiveLocation = $("#updateDefectiveLocation").val();
                                                        var postTreatmentData = {"id": objData.id, "defectiveLocation": updateDefectiveLocation, "defectiveCount": updateDefectiveCount, "createTime": updateDefectiveDate};
                                                        $.ajax({
                                                            url: "/erp/updateposttreatment",
                                                            type:'POST',
                                                            data: {
                                                                postTreatmentJson: JSON.stringify(postTreatmentData)
                                                            },
                                                            success:function(res){
                                                                if(res.data == 0) {
                                                                    table.reload(childId);
                                                                    layer.close(index);
                                                                    layer.msg("修改成功！", {icon: 1});
                                                                }else {
                                                                    layer.msg("修改失败！", {icon: 2});
                                                                }
                                                            }, error:function(){
                                                                layer.msg("修改失败！", {icon: 1});
                                                            }
                                                        });
                                                    }
                                                });
                                                setTimeout(function () {
                                                    $("#updateDefectiveCount").val(objData.defectiveCount);
                                                    $("#updateDefectiveDate").val(objData.createTime);
                                                    $("#updateDefectiveLocation").val(objData.defectiveLocation);
                                                    layui.laydate.render({
                                                        elem: '#updateDefectiveDate',
                                                        trigger: 'click'
                                                    });
                                                },100);
                                            }

                                        }
                                    }, {
                                        title: '次片数量'
                                        ,url: 'getposttreatmentbyordertype'
                                        ,where: function(row){
                                            return {
                                                orderName: row.orderName,
                                                chickenCount: 1
                                            }
                                        }
                                        ,height: 300
                                        ,loading:true
                                        ,page: false
                                        ,totalRow: true
                                        ,cols: [[
                                            {field: 'clothesVersionNumber', title: '单号',align:'center', minWidth: 120},
                                            {field: 'orderName', title: '款号',align:'center', minWidth: 120},
                                            {field: 'colorName', title: '颜色',align:'center', minWidth: 120},
                                            {field: 'sizeName', title: '尺码',align:'center', minWidth: 120 },
                                            {field: 'chickenCount', title: '数量',align:'center', minWidth: 120, totalRow: true},
                                            {field: 'chickenLocation', title: '位置',align:'center', minWidth: 120},
                                            {field: 'createTime', title: '日期',align:'center', minWidth: 120},
                                            {field: 'id', title: '操作', minWidth: 70,align:'center', templet: '#chickenCountChildBar'}
                                        ]]
                                        ,done: function () {
                                            soulTable.render(this);
                                        }
                                        ,toolEvent: function (obj, pobj) {
                                            // obj 子表当前行对象
                                            // pobj 父表当前行对象
                                            var objData = obj.data;
                                            var childId = this.id; // 通过 this 对象获取当前子表的id
                                            if (obj.event === 'deleteChickenCount'){
                                                layer.confirm('真的删除吗', function(index){
                                                    $.ajax({
                                                        url: "/erp/deleteposttreatmentbyid",
                                                        type: 'POST',
                                                        data: {
                                                            id: objData.id
                                                        },
                                                        success: function (res) {
                                                            if (res.data > 0) {
                                                                layer.close(index);
                                                                layer.msg("删除成功！", {icon: 1});
                                                                table.reload(childId);
                                                            } else {
                                                                layer.msg("删除失败！", {icon: 2});
                                                            }
                                                        }, error: function () {
                                                            layer.msg("删除失败！", {icon: 2});
                                                        }
                                                    })
                                                });
                                            } else if (obj.event === 'updateChickenCount'){
                                                var index = layer.open({
                                                    type: 1 //Page层类型
                                                    , title: '修改'
                                                    , btn: ['保存']
                                                    , shade: 0.6 //遮罩透明度
                                                    , area: ['300px','250px']
                                                    , maxmin: false //允许全屏最小化
                                                    , anim: 0 //0-6的动画形式，-1不开启
                                                    , offset: '100px'
                                                    , content: "<div><table><tr>" +
                                                        "                                            <td style=\"text-align: right;margin-bottom: 15px;\">\n" +
                                                        "                                                <label class=\"layui-form-label\">数量</label>\n" +
                                                        "                                            </td>\n" +
                                                        "                                            <td style=\"margin-bottom: 15px;\">\n" +
                                                        "                                                <input type=\"text\" name=\"updateChickenCount\" id=\"updateChickenCount\" autocomplete=\"off\" class=\"layui-input\">\n" +
                                                        "                                            </td></tr><tr>" +
                                                        "                                            <td style=\"text-align: right;margin-bottom: 15px;\">\n" +
                                                        "                                                <label class=\"layui-form-label\">位置</label>\n" +
                                                        "                                            </td>\n" +
                                                        "                                            <td style=\"margin-bottom: 15px;\">\n" +
                                                        "                                                <input type=\"text\" name=\"updateChickenLocation\" id=\"updateChickenLocation\" autocomplete=\"off\" class=\"layui-input\">\n" +
                                                        "                                            </td></tr><tr>" +
                                                        "                                            <td style=\"text-align: right;margin-bottom: 15px;\">\n" +
                                                        "                                                <label class=\"layui-form-label\">日期</label>\n" +
                                                        "                                            </td>\n" +
                                                        "                                            <td style=\"margin-bottom: 15px;\">\n" +
                                                        "                                                <input type=\"text\" name=\"updateChickenDate\" style='width: 200px' id=\"updateChickenDate\" autocomplete=\"off\" class=\"layui-input\">\n" +
                                                        "                                            </td>" +
                                                        "</tr></table></div>"
                                                    ,yes: function(i, layero){
                                                        var updateChickenCount = $("#updateChickenCount").val();
                                                        var updateChickenDate = $("#updateChickenDate").val();
                                                        var updateChickenLocation = $("#updateChickenLocation").val();
                                                        var postTreatmentData = {"id": objData.id, "chickenLocation": updateChickenLocation, "chickenCount": updateChickenCount, "createTime": updateChickenDate};
                                                        $.ajax({
                                                            url: "/erp/updateposttreatment",
                                                            type:'POST',
                                                            data: {
                                                                postTreatmentJson: JSON.stringify(postTreatmentData)
                                                            },
                                                            success:function(res){
                                                                if(res.data == 0) {
                                                                    table.reload(childId);
                                                                    layer.close(index);
                                                                    layer.msg("修改成功！", {icon: 1});
                                                                }else {
                                                                    layer.msg("修改失败！", {icon: 2});
                                                                }
                                                            }, error:function(){
                                                                layer.msg("修改失败！", {icon: 1});
                                                            }
                                                        });
                                                    }
                                                });
                                                setTimeout(function () {
                                                    $("#updateChickenCount").val(objData.chickenCount);
                                                    $("#updateChickenDate").val(objData.createTime);
                                                    $("#updateChickenLocation").val(objData.chickenLocation);
                                                    layui.laydate.render({
                                                        elem: '#updateChickenDate',
                                                        trigger: 'click'
                                                    });
                                                },100);
                                            }

                                        }
                                    }
                                ]}
                            ,{type:'radio'}
                            ,{type:'numbers', align:'center', title:'序号', width:70}
                            ,{field:'clothesVersionNumber', title:'单号', align:'center', width:150, sort: true, filter: true, totalRowText: '合计'}
                            ,{field:'orderName', title:'款号', align:'center', width:150, sort: true, filter: true}
                            ,{field:'orderCount', title:'订单', align:'center', minWidth:80, sort: true, filter: true, totalRow: true}
                            ,{field:'wellCount', title:'好片', align:'center', minWidth:80, sort: true, filter: true, totalRow: true}
                            ,{field:'checkCount', title:'查片', align:'center', minWidth:80, sort: true, filter: true, totalRow: true,templet:function (d) {
                                    if (d.checkCount > d.wellCount){
                                        return "<p style='color: red'>"+ d.checkCount +"</p>";
                                    } else {
                                        return "<p style='color: green'>"+ d.checkCount +"</p>";
                                    }
                                }}
                            ,{field:'hangCount', title:'挂片', align:'center', minWidth:80, sort: true, filter: true, totalRow: true,templet:function (d) {
                                    if (d.hangCount > d.wellCount){
                                        return "<p style='color: red'>"+ d.hangCount +"</p>";
                                    } else {
                                        return "<p style='color: green'>"+ d.hangCount +"</p>";
                                    }
                                }}
                            ,{field:'inspectionCount', title:'中查', align:'center', minWidth:80, sort: true, filter: true, totalRow: true,templet:function (d) {
                                    if (d.inspectionCount > d.wellCount){
                                        return "<p style='color: red'>"+ d.inspectionCount +"</p>";
                                    } else {
                                        return "<p style='color: green'>"+ d.inspectionCount +"</p>";
                                    }
                                }}
                            ,{field:'sampleCount', title:'抽办', align:'center', minWidth:80, sort: true, filter: true, totalRow: true,templet:function (d) {
                                    if (d.sampleCount > d.wellCount){
                                        return "<p style='color: red'>"+ d.sampleCount +"</p>";
                                    } else {
                                        return "<p style='color: green'>"+ d.sampleCount +"</p>";
                                    }
                                }}
                            ,{field:'washCount', title:'洗水', align:'center', minWidth:80, sort: true, filter: true, totalRow: true,templet:function (d) {
                                    if (d.washCount > d.wellCount){
                                        return "<p style='color: red'>"+ d.washCount +"</p>";
                                    } else {
                                        return "<p style='color: green'>"+ d.washCount +"</p>";
                                    }
                                }}
                            ,{field:'ironCount', title:'大烫', align:'center', minWidth:80, sort: true, filter: true, totalRow: true,templet:function (d) {
                                    if (d.ironCount > d.wellCount){
                                        return "<p style='color: red'>"+ d.ironCount +"</p>";
                                    } else {
                                        return "<p style='color: green'>"+ d.ironCount +"</p>";
                                    }
                                }}
                            ,{field:'tailCheckCount', title:'尾查', align:'center', minWidth:80, sort: true, filter: true, totalRow: true,templet:function (d) {
                                    if (d.tailCheckCount > d.wellCount){
                                        return "<p style='color: red'>"+ d.tailCheckCount +"</p>";
                                    } else {
                                        return "<p style='color: green'>"+ d.tailCheckCount +"</p>";
                                    }
                                }}
                            ,{field:'packageCount', title:'装箱', align:'center', minWidth:80, sort: true, filter: true, totalRow: true,templet:function (d) {
                                    if (d.packageCount > d.wellCount){
                                        return "<p style='color: red'>"+ d.packageCount +"</p>";
                                    } else {
                                        return "<p style='color: green'>"+ d.packageCount +"</p>";
                                    }
                                }}
                            ,{field:'shipCount', title:'出货', align:'center', minWidth:80, sort: true, filter: true, totalRow: true,templet:function (d) {
                                    if (d.shipCount > d.wellCount){
                                        return "<p style='color: red'>"+ d.shipCount +"</p>";
                                    } else {
                                        return "<p style='color: green'>"+ d.shipCount +"</p>";
                                    }
                                }}
                            ,{field:'surplusCount', title:'正品余数', align:'center', width:110, sort: true, filter: true, totalRow: true,templet:function (d) {
                                    if (d.surplusCount > d.wellCount){
                                        return "<p style='color: red'>"+ d.surplusCount +"</p>";
                                    } else {
                                        return "<p style='color: green'>"+ d.surplusCount +"</p>";
                                    }
                                }}
                            ,{field:'defectiveCount', title:'次品', align:'center', minWidth:80, sort: true, filter: true, totalRow: true,templet:function (d) {
                                    if (d.defectiveCount > d.wellCount){
                                        return "<p style='color: red'>"+ d.defectiveCount +"</p>";
                                    } else {
                                        return "<p style='color: green'>"+ d.defectiveCount +"</p>";
                                    }
                                }}
                            ,{field:'chickenCount', title:'次片', align:'center', minWidth:80, sort: true, filter: true, totalRow: true,templet:function (d) {
                                    if (d.chickenCount > d.wellCount){
                                        return "<p style='color: red'>"+ d.chickenCount +"</p>";
                                    } else {
                                        return "<p style='color: green'>"+ d.chickenCount +"</p>";
                                    }
                                }}
                            ,{field:'diffCount', title:'差数', align:'center', minWidth:80, sort: true, filter: true, totalRow: true,templet:function (d) {
                                    if (d.diffCount > d.wellCount){
                                        return "<p style='color: red'>"+ d.diffCount +"</p>";
                                    } else {
                                        return "<p style='color: green'>"+ d.diffCount +"</p>";
                                    }
                                }}
                        ]]
                        ,excel: {
                            filename: '完工报告.xlsx'
                        }
                        ,data: reportData   // 将新数据重新载入表格
                        ,overflow: 'tips'
                        ,even: true
                        ,done: function () {
                            soulTable.render(this);
                            var divArr = $(".layui-table-total div.layui-table-cell");
                            $.each(divArr,function (index,item) {
                                var _div = $(item);
                                var content = _div.html();
                                content = content.replace(".00","");
                                _div.html(content);
                            });
                            layer.close(load);
                        }
                        ,filter: {
                            bottom: true,
                            clearFilter: false,
                            items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                        }
                    });

                } else {
                    layer.msg("获取失败！", {icon: 2});
                    layer.close(load);
                }
            }, error: function () {
                layer.msg("获取失败！", {icon: 2});
                layer.close(load);
            }
        });
        return false;
    });

    table.on('toolbar(reportTable)', function(obj) {
        if (obj.event === 'clearFilter') {
            // 清除所有筛选条件并重载表格
            // 参数: tableId
            soulTable.clearFilter('reportTable')
        } else if (obj.event === 'exportExcel') {
            soulTable.export('reportTable');
        } else if (obj.event === 'addWash'){
            var checkStatus = table.checkStatus(obj.config.id); //获取选中行状态
            if(checkStatus.data.length == 0) {
                layer.msg('请先选择数据');
                return false;
            }else{
                var orderName = checkStatus.data[0].orderName;
                var clothesVersionNumber = checkStatus.data[0].clothesVersionNumber;
                var label = "添加洗水记录&emsp;&emsp;&emsp;&emsp;" + "单号："+clothesVersionNumber+"&emsp;&emsp;&emsp;&emsp;款号："+orderName;
                var index = layer.open({
                    type: 1 //Page层类型
                    , title: label
                    , btn: ['保存']
                    , shade: 0.6 //遮罩透明度
                    , area: '1000px'
                    , maxmin: false //允许全屏最小化
                    , anim: 0 //0-6的动画形式，-1不开启
                    , offset: '100px'
                    , content: "<div><table>" +
                        "                                <td style=\"text-align: right;margin-bottom: 15px;padding-top: 15px\">\n" +
                        "                                    <label class=\"layui-form-label\">颜色</label>\n" +
                        "                                </td>\n" +
                        "                                <td style=\"margin-bottom: 15px;padding-top: 15px\">\n" +
                        "                                    <select type=\"text\" style='width: 200px' name=\"colorName\" id=\"colorName\" autocomplete=\"off\" class=\"layui-input\"></select>\n" +
                        "                                </td>" +
                        "                                <td style=\"text-align: right;margin-bottom: 15px;padding-top: 15px\">\n" +
                        "                                    <label class=\"layui-form-label\">尺码</label>\n" +
                        "                                </td>\n" +
                        "                                <td style=\"margin-bottom: 15px;padding-top: 15px\">\n" +
                        "                                    <select type=\"text\" style='width: 200px' name=\"sizeName\" id=\"sizeName\" autocomplete=\"off\" class=\"layui-input\"></select>\n" +
                        "                                </td>" +
                        "                                <td style=\"text-align: right;margin-bottom: 15px;padding-top: 15px\">\n" +
                        "                                    <label class=\"layui-form-label\">数量</label>\n" +
                        "                                </td>\n" +
                        "                                <td style=\"margin-bottom: 15px;padding-top: 15px\">\n" +
                        "                                    <input type=\"text\" name=\"washCount\" id=\"washCount\" autocomplete=\"off\" class=\"layui-input\">\n" +
                        "                                </td>" +
                        "</table></div>"
                    ,yes: function(i, layero){
                        var colorName = $("#colorName").val();
                        var sizeName = $("#sizeName").val();
                        var washCount = $("#washCount").val();
                        if (washCount == '' || washCount == null || washCount == 0){
                            layer.msg("数量有误！");
                            return false;
                        }
                        var postTreatmentList = [{orderName: orderName, clothesVersionNumber: clothesVersionNumber, colorName: colorName, sizeName: sizeName, washCount: washCount}]
                        $.ajax({
                            url: "/erp/saveposttreatmentdatabatch",
                            type: 'POST',
                            data: {
                                postTreatmentJson:JSON.stringify(postTreatmentList)
                            },
                            success: function (res) {
                                if (res.data == 0) {
                                    layer.msg("录入成功！", {icon: 1});
                                } else {
                                    layer.msg("录入失败！", {icon: 2});
                                }
                            },
                            error: function () {
                                layer.msg("录入失败！", {icon: 2});
                            }
                        })
                    }
                    ,cancel: function () {
                        layer.close(index);
                        refreshTable();
                    }
                });
                setTimeout(function () {
                    $.ajax({
                        url: "/erp/getordercolorandsizenamesbyorder",
                        type: 'GET',
                        data: {
                            orderName:orderName
                        },
                        success: function (res) {
                            var tmpSizeList = res.sizeNameList;
                            tmpSizeList = globalSizeSort(tmpSizeList);
                            $("#colorName").empty();
                            $.each(res.colorNameList,function(index1,value1) {
                                $("#colorName").append("<option value='"+value1+"'>"+value1+"</option>");
                            });
                            $("#sizeName").empty();
                            $.each(tmpSizeList,function(index1,value1) {
                                $("#sizeName").append("<option value='"+value1+"'>"+value1+"</option>");
                            });
                            form.render('select');
                        }, error: function () {
                            layer.msg("获取尺码失败！", {icon: 2});
                        }
                    })
                },100);
            }
        } else if (obj.event === 'addShip'){
            var checkStatus = table.checkStatus(obj.config.id); //获取选中行状态
            if(checkStatus.data.length == 0) {
                layer.msg('请先选择数据');
                return false;
            }else{
                var orderName = checkStatus.data[0].orderName;
                var clothesVersionNumber = checkStatus.data[0].clothesVersionNumber;
                var label = "添加出货记录&emsp;&emsp;&emsp;&emsp;" + "单号："+clothesVersionNumber+"&emsp;&emsp;&emsp;&emsp;款号："+orderName;
                var index = layer.open({
                    type: 1 //Page层类型
                    , title: label
                    , btn: ['保存']
                    , shade: 0.6 //遮罩透明度
                    , area: '1000px'
                    , maxmin: false //允许全屏最小化
                    , anim: 0 //0-6的动画形式，-1不开启
                    , offset: '100px'
                    , content: "<div><table>" +
                        "                                <td style=\"text-align: right;margin-bottom: 15px;padding-top: 15px\">\n" +
                        "                                    <label class=\"layui-form-label\">颜色</label>\n" +
                        "                                </td>\n" +
                        "                                <td style=\"margin-bottom: 15px;padding-top: 15px\">\n" +
                        "                                    <select type=\"text\" style='width: 200px' name=\"colorName\" id=\"colorName\" autocomplete=\"off\" class=\"layui-input\"></select>\n" +
                        "                                </td>" +
                        "                                <td style=\"text-align: right;margin-bottom: 15px;padding-top: 15px\">\n" +
                        "                                    <label class=\"layui-form-label\">尺码</label>\n" +
                        "                                </td>\n" +
                        "                                <td style=\"margin-bottom: 15px;padding-top: 15px\">\n" +
                        "                                    <select type=\"text\" style='width: 200px' name=\"sizeName\" id=\"sizeName\" autocomplete=\"off\" class=\"layui-input\"></select>\n" +
                        "                                </td>" +
                        "                                <td style=\"text-align: right;margin-bottom: 15px;padding-top: 15px\">\n" +
                        "                                    <label class=\"layui-form-label\">数量</label>\n" +
                        "                                </td>\n" +
                        "                                <td style=\"margin-bottom: 15px;padding-top: 15px\">\n" +
                        "                                    <input type=\"text\" name=\"shipCount\" id=\"shipCount\" autocomplete=\"off\" class=\"layui-input\">\n" +
                        "                                </td>" +
                        "</table></div>"
                    ,yes: function(i, layero){
                        var colorName = $("#colorName").val();
                        var sizeName = $("#sizeName").val();
                        var shipCount = $("#shipCount").val();
                        if (shipCount == '' || shipCount == null || shipCount == 0){
                            layer.msg("数量有误！");
                            return false;
                        }
                        var postTreatmentList = [{orderName: orderName, clothesVersionNumber: clothesVersionNumber, colorName: colorName, sizeName: sizeName, shipCount: shipCount}]
                        $.ajax({
                            url: "/erp/saveposttreatmentdatabatch",
                            type: 'POST',
                            data: {
                                postTreatmentJson:JSON.stringify(postTreatmentList)
                            },
                            success: function (res) {
                                if (res.data == 0) {
                                    layer.msg("录入成功！", {icon: 1});
                                } else {
                                    layer.msg("录入失败！", {icon: 2});
                                }
                            },
                            error: function () {
                                layer.msg("录入失败！", {icon: 2});
                            }
                        })
                    }
                    ,cancel: function () {
                        layer.close(index);
                        refreshTable();
                    }
                });
                setTimeout(function () {
                    $.ajax({
                        url: "/erp/getordercolorandsizenamesbyorder",
                        type: 'GET',
                        data: {
                            orderName:orderName
                        },
                        success: function (res) {
                            var tmpSizeList = res.sizeNameList;
                            tmpSizeList = globalSizeSort(tmpSizeList);
                            $("#colorName").empty();
                            $.each(res.colorNameList,function(index1,value1) {
                                $("#colorName").append("<option value='"+value1+"'>"+value1+"</option>");
                            });
                            $("#sizeName").empty();
                            $.each(tmpSizeList,function(index1,value1) {
                                $("#sizeName").append("<option value='"+value1+"'>"+value1+"</option>");
                            });
                            form.render('select');
                        }, error: function () {
                            layer.msg("获取尺码失败！", {icon: 2});
                        }
                    })
                },100);
            }
        } else if (obj.event === 'addSurplus'){
            var checkStatus = table.checkStatus(obj.config.id); //获取选中行状态
            if(checkStatus.data.length == 0) {
                layer.msg('请先选择数据');
                return false;
            }else{
                var orderName = checkStatus.data[0].orderName;
                var clothesVersionNumber = checkStatus.data[0].clothesVersionNumber;
                var label = "添加正品余数&emsp;&emsp;&emsp;&emsp;" + "单号："+clothesVersionNumber+"&emsp;&emsp;&emsp;&emsp;款号："+orderName;
                var index = layer.open({
                    type: 1 //Page层类型
                    , title: label
                    , btn: ['保存']
                    , shade: 0.6 //遮罩透明度
                    , area: '1000px'
                    , maxmin: false //允许全屏最小化
                    , anim: 0 //0-6的动画形式，-1不开启
                    , offset: '100px'
                    , content: "<div><table>" +
                        "                                <td style=\"text-align: right;margin-bottom: 15px;padding-top: 15px\">\n" +
                        "                                    <label class=\"layui-form-label\">颜色</label>\n" +
                        "                                </td>\n" +
                        "                                <td style=\"margin-bottom: 15px;padding-top: 15px\">\n" +
                        "                                    <select type=\"text\" style='width: 200px' name=\"colorName\" id=\"colorName\" autocomplete=\"off\" class=\"layui-input\"></select>\n" +
                        "                                </td>" +
                        "                                <td style=\"text-align: right;margin-bottom: 15px;padding-top: 15px\">\n" +
                        "                                    <label class=\"layui-form-label\">尺码</label>\n" +
                        "                                </td>\n" +
                        "                                <td style=\"margin-bottom: 15px;padding-top: 15px\">\n" +
                        "                                    <select type=\"text\" style='width: 200px' name=\"sizeName\" id=\"sizeName\" autocomplete=\"off\" class=\"layui-input\"></select>\n" +
                        "                                </td>" +
                        "                                <td style=\"text-align: right;margin-bottom: 15px;padding-top: 15px\">\n" +
                        "                                    <label class=\"layui-form-label\">数量</label>\n" +
                        "                                </td>\n" +
                        "                                <td style=\"margin-bottom: 15px;padding-top: 15px\">\n" +
                        "                                    <input type=\"text\" name=\"surplusCount\" id=\"surplusCount\" autocomplete=\"off\" class=\"layui-input\">\n" +
                        "                                </td>" +
                        "</table></div>"
                    ,yes: function(i, layero){
                        var colorName = $("#colorName").val();
                        var sizeName = $("#sizeName").val();
                        var surplusCount = $("#surplusCount").val();
                        if (surplusCount == '' || surplusCount == null || surplusCount == 0){
                            layer.msg("数量有误！");
                            return false;
                        }
                        var postTreatmentList = [{orderName: orderName, clothesVersionNumber: clothesVersionNumber, colorName: colorName, sizeName: sizeName, surplusCount: surplusCount}]
                        $.ajax({
                            url: "/erp/saveposttreatmentdatabatch",
                            type: 'POST',
                            data: {
                                postTreatmentJson:JSON.stringify(postTreatmentList)
                            },
                            success: function (res) {
                                if (res.data == 0) {
                                    layer.msg("录入成功！", {icon: 1});
                                } else {
                                    layer.msg("录入失败！", {icon: 2});
                                }
                            },
                            error: function () {
                                layer.msg("录入失败！", {icon: 2});
                            }
                        })
                    }
                    ,cancel: function () {
                        layer.close(index);
                        refreshTable();
                    }
                });
                setTimeout(function () {
                    $.ajax({
                        url: "/erp/getordercolorandsizenamesbyorder",
                        type: 'GET',
                        data: {
                            orderName:orderName
                        },
                        success: function (res) {
                            var tmpSizeList = res.sizeNameList;
                            tmpSizeList = globalSizeSort(tmpSizeList);
                            $("#colorName").empty();
                            $.each(res.colorNameList,function(index1,value1) {
                                $("#colorName").append("<option value='"+value1+"'>"+value1+"</option>");
                            });
                            $("#sizeName").empty();
                            $.each(tmpSizeList,function(index1,value1) {
                                $("#sizeName").append("<option value='"+value1+"'>"+value1+"</option>");
                            });
                            form.render('select');
                        }, error: function () {
                            layer.msg("获取尺码失败！", {icon: 2});
                        }
                    })
                },100);
            }
        } else if (obj.event === 'addDefective'){
            var checkStatus = table.checkStatus(obj.config.id); //获取选中行状态
            if(checkStatus.data.length == 0) {
                layer.msg('请先选择数据');
                return false;
            }else{
                var orderName = checkStatus.data[0].orderName;
                var clothesVersionNumber = checkStatus.data[0].clothesVersionNumber;
                var label = "添加次品数量&emsp;&emsp;&emsp;&emsp;" + "单号："+clothesVersionNumber+"&emsp;&emsp;&emsp;&emsp;款号："+orderName;
                var index = layer.open({
                    type: 1 //Page层类型
                    , title: label
                    , btn: ['保存']
                    , shade: 0.6 //遮罩透明度
                    , area: '1000px'
                    , maxmin: false //允许全屏最小化
                    , anim: 0 //0-6的动画形式，-1不开启
                    , offset: '100px'
                    , content: "<div><table>" +
                        "                                <td style=\"text-align: right;margin-bottom: 15px;padding-top: 15px\">\n" +
                        "                                    <label class=\"layui-form-label\">颜色</label>\n" +
                        "                                </td>\n" +
                        "                                <td style=\"margin-bottom: 15px;padding-top: 15px\">\n" +
                        "                                    <select type=\"text\" style='width: 200px' name=\"colorName\" id=\"colorName\" autocomplete=\"off\" class=\"layui-input\"></select>\n" +
                        "                                </td>" +
                        "                                <td style=\"text-align: right;margin-bottom: 15px;padding-top: 15px\">\n" +
                        "                                    <label class=\"layui-form-label\">尺码</label>\n" +
                        "                                </td>\n" +
                        "                                <td style=\"margin-bottom: 15px;padding-top: 15px\">\n" +
                        "                                    <select type=\"text\" style='width: 200px' name=\"sizeName\" id=\"sizeName\" autocomplete=\"off\" class=\"layui-input\"></select>\n" +
                        "                                </td>" +
                        "                                <td style=\"text-align: right;margin-bottom: 15px;padding-top: 15px\">\n" +
                        "                                    <label class=\"layui-form-label\">数量</label>\n" +
                        "                                </td>\n" +
                        "                                <td style=\"margin-bottom: 15px;padding-top: 15px\">\n" +
                        "                                    <input type=\"text\" name=\"defectiveCount\" id=\"defectiveCount\" autocomplete=\"off\" class=\"layui-input\">\n" +
                        "                                </td>" +
                        "</table></div>"
                    ,yes: function(i, layero){
                        var colorName = $("#colorName").val();
                        var sizeName = $("#sizeName").val();
                        var defectiveCount = $("#defectiveCount").val();
                        if (defectiveCount == '' || defectiveCount == null || defectiveCount == 0){
                            layer.msg("数量有误！");
                            return false;
                        }
                        var postTreatmentList = [{orderName: orderName, clothesVersionNumber: clothesVersionNumber, colorName: colorName, sizeName: sizeName, defectiveCount: defectiveCount}]
                        $.ajax({
                            url: "/erp/saveposttreatmentdatabatch",
                            type: 'POST',
                            data: {
                                postTreatmentJson:JSON.stringify(postTreatmentList)
                            },
                            success: function (res) {
                                if (res.data == 0) {
                                    layer.msg("录入成功！", {icon: 1});
                                } else {
                                    layer.msg("录入失败！", {icon: 2});
                                }
                            },
                            error: function () {
                                layer.msg("录入失败！", {icon: 2});
                            }
                        })
                    }
                    ,cancel: function () {
                        layer.close(index);
                        refreshTable();
                    }
                });
                setTimeout(function () {
                    $.ajax({
                        url: "/erp/getordercolorandsizenamesbyorder",
                        type: 'GET',
                        data: {
                            orderName:orderName
                        },
                        success: function (res) {
                            var tmpSizeList = res.sizeNameList;
                            tmpSizeList = globalSizeSort(tmpSizeList);
                            $("#colorName").empty();
                            $.each(res.colorNameList,function(index1,value1) {
                                $("#colorName").append("<option value='"+value1+"'>"+value1+"</option>");
                            });
                            $("#sizeName").empty();
                            $.each(tmpSizeList,function(index1,value1) {
                                $("#sizeName").append("<option value='"+value1+"'>"+value1+"</option>");
                            });
                            form.render('select');
                        }, error: function () {
                            layer.msg("获取尺码失败！", {icon: 2});
                        }
                    })
                },100);
            }
        } else if (obj.event === 'addChicken'){
            var checkStatus = table.checkStatus(obj.config.id); //获取选中行状态
            if(checkStatus.data.length == 0) {
                layer.msg('请先选择数据');
                return false;
            }else{
                var orderName = checkStatus.data[0].orderName;
                var clothesVersionNumber = checkStatus.data[0].clothesVersionNumber;
                var label = "添加次片数量&emsp;&emsp;&emsp;&emsp;" + "单号："+clothesVersionNumber+"&emsp;&emsp;&emsp;&emsp;款号："+orderName;
                var index = layer.open({
                    type: 1 //Page层类型
                    , title: label
                    , btn: ['保存']
                    , shade: 0.6 //遮罩透明度
                    , area: '1000px'
                    , maxmin: false //允许全屏最小化
                    , anim: 0 //0-6的动画形式，-1不开启
                    , offset: '100px'
                    , content: "<div><table>" +
                        "                                <td style=\"text-align: right;margin-bottom: 15px;padding-top: 15px\">\n" +
                        "                                    <label class=\"layui-form-label\">颜色</label>\n" +
                        "                                </td>\n" +
                        "                                <td style=\"margin-bottom: 15px;padding-top: 15px\">\n" +
                        "                                    <select type=\"text\" style='width: 200px' name=\"colorName\" id=\"colorName\" autocomplete=\"off\" class=\"layui-input\"></select>\n" +
                        "                                </td>" +
                        "                                <td style=\"text-align: right;margin-bottom: 15px;padding-top: 15px\">\n" +
                        "                                    <label class=\"layui-form-label\">尺码</label>\n" +
                        "                                </td>\n" +
                        "                                <td style=\"margin-bottom: 15px;padding-top: 15px\">\n" +
                        "                                    <select type=\"text\" style='width: 200px' name=\"sizeName\" id=\"sizeName\" autocomplete=\"off\" class=\"layui-input\"></select>\n" +
                        "                                </td>" +
                        "                                <td style=\"text-align: right;margin-bottom: 15px;padding-top: 15px\">\n" +
                        "                                    <label class=\"layui-form-label\">数量</label>\n" +
                        "                                </td>\n" +
                        "                                <td style=\"margin-bottom: 15px;padding-top: 15px\">\n" +
                        "                                    <input type=\"text\" name=\"chickenCount\" id=\"chickenCount\" autocomplete=\"off\" class=\"layui-input\">\n" +
                        "                                </td>" +
                        "</table></div>"
                    ,yes: function(i, layero){
                        var colorName = $("#colorName").val();
                        var sizeName = $("#sizeName").val();
                        var chickenCount = $("#chickenCount").val();
                        if (chickenCount == '' || chickenCount == null || chickenCount == 0){
                            layer.msg("数量有误！");
                            return false;
                        }
                        var postTreatmentList = [{orderName: orderName, clothesVersionNumber: clothesVersionNumber, colorName: colorName, sizeName: sizeName, chickenCount: chickenCount}]
                        $.ajax({
                            url: "/erp/saveposttreatmentdatabatch",
                            type: 'POST',
                            data: {
                                postTreatmentJson:JSON.stringify(postTreatmentList)
                            },
                            success: function (res) {
                                if (res.data == 0) {
                                    layer.msg("录入成功！", {icon: 1});
                                } else {
                                    layer.msg("录入失败！", {icon: 2});
                                }
                            },
                            error: function () {
                                layer.msg("录入失败！", {icon: 2});
                            }
                        })
                    }
                    ,cancel: function () {
                        layer.close(index);
                        refreshTable();
                    }
                });
                setTimeout(function () {
                    $.ajax({
                        url: "/erp/getordercolorandsizenamesbyorder",
                        type: 'GET',
                        data: {
                            orderName:orderName
                        },
                        success: function (res) {
                            var tmpSizeList = res.sizeNameList;
                            tmpSizeList = globalSizeSort(tmpSizeList);
                            $("#colorName").empty();
                            $.each(res.colorNameList,function(index1,value1) {
                                $("#colorName").append("<option value='"+value1+"'>"+value1+"</option>");
                            });
                            $("#sizeName").empty();
                            $.each(tmpSizeList,function(index1,value1) {
                                $("#sizeName").append("<option value='"+value1+"'>"+value1+"</option>");
                            });
                            form.render('select');
                        }, error: function () {
                            layer.msg("获取尺码失败！", {icon: 2});
                        }
                    })
                },100);
            }
        } else if (obj.event === 'addSample'){
            var checkStatus = table.checkStatus(obj.config.id); //获取选中行状态
            if(checkStatus.data.length == 0) {
                layer.msg('请先选择数据');
                return false;
            }else{
                var orderName = checkStatus.data[0].orderName;
                var clothesVersionNumber = checkStatus.data[0].clothesVersionNumber;
                var label = "添加抽办&emsp;&emsp;&emsp;&emsp;" + "单号："+clothesVersionNumber+"&emsp;&emsp;&emsp;&emsp;款号："+orderName;
                var index = layer.open({
                    type: 1 //Page层类型
                    , title: label
                    , btn: ['保存']
                    , shade: 0.6 //遮罩透明度
                    , area: ['1200px','200px']
                    , maxmin: false //允许全屏最小化
                    , anim: 0 //0-6的动画形式，-1不开启
                    , offset: '100px'
                    , content: "<div><table>" +
                        "                                <td style=\"text-align: right;margin-bottom: 15px;padding-top: 15px\">\n" +
                        "                                    <label class=\"layui-form-label\">颜色</label>\n" +
                        "                                </td>\n" +
                        "                                <td style=\"margin-bottom: 15px;padding-top: 15px\">\n" +
                        "                                    <select type=\"text\" style='width: 200px' name=\"colorName\" id=\"colorName\" autocomplete=\"off\" class=\"layui-input\"></select>\n" +
                        "                                </td>" +
                        "                                <td style=\"text-align: right;margin-bottom: 15px;padding-top: 15px\">\n" +
                        "                                    <label class=\"layui-form-label\">尺码</label>\n" +
                        "                                </td>\n" +
                        "                                <td style=\"margin-bottom: 15px;padding-top: 15px\">\n" +
                        "                                    <select type=\"text\" style='width: 200px' name=\"sizeName\" id=\"sizeName\" autocomplete=\"off\" class=\"layui-input\"></select>\n" +
                        "                                </td>" +
                        "                                <td style=\"text-align: right;margin-bottom: 15px;padding-top: 15px\">\n" +
                        "                                    <label class=\"layui-form-label\">数量</label>\n" +
                        "                                </td>\n" +
                        "                                <td style=\"margin-bottom: 15px;padding-top: 15px\">\n" +
                        "                                    <input type=\"text\" name=\"sampleCount\" id=\"sampleCount\" autocomplete=\"off\" class=\"layui-input\">\n" +
                        "                                </td>" +
                        "                                <td style=\"text-align: right;margin-bottom: 15px;padding-top: 15px\">\n" +
                        "                                    <label class=\"layui-form-label\">事由</label>\n" +
                        "                                </td>\n" +
                        "                                <td style=\"margin-bottom: 15px;padding-top: 15px\">\n" +
                        "                                    <input type=\"text\" name=\"sampleLocation\" id=\"sampleLocation\" autocomplete=\"off\" class=\"layui-input\">\n" +
                        "                                </td>" +
                        "</table></div>"
                    ,yes: function(i, layero){
                        var colorName = $("#colorName").val();
                        var sizeName = $("#sizeName").val();
                        var sampleCount = $("#sampleCount").val();
                        var sampleLocation = $("#sampleLocation").val();
                        if (sampleCount == '' || sampleCount == null || sampleCount == 0){
                            layer.msg("数量有误！");
                            return false;
                        }
                        var postTreatmentList = [{orderName: orderName, clothesVersionNumber: clothesVersionNumber, colorName: colorName, sizeName: sizeName, sampleCount: sampleCount, sampleLocation: sampleLocation}];
                        $.ajax({
                            url: "/erp/saveposttreatmentdatabatch",
                            type: 'POST',
                            data: {
                                postTreatmentJson:JSON.stringify(postTreatmentList)
                            },
                            success: function (res) {
                                if (res.data == 0) {
                                    layer.msg("录入成功！", {icon: 1});
                                } else {
                                    layer.msg("录入失败！", {icon: 2});
                                }
                            },
                            error: function () {
                                layer.msg("录入失败！", {icon: 2});
                            }
                        })
                    }
                    ,cancel: function () {
                        layer.close(index);
                        refreshTable();
                    }
                });
                setTimeout(function () {
                    $.ajax({
                        url: "/erp/getordercolorandsizenamesbyorder",
                        type: 'GET',
                        data: {
                            orderName:orderName
                        },
                        success: function (res) {
                            var tmpSizeList = res.sizeNameList;
                            tmpSizeList = globalSizeSort(tmpSizeList);
                            $("#colorName").empty();
                            $.each(res.colorNameList,function(index1,value1) {
                                $("#colorName").append("<option value='"+value1+"'>"+value1+"</option>");
                            });
                            $("#sizeName").empty();
                            $.each(tmpSizeList,function(index1,value1) {
                                $("#sizeName").append("<option value='"+value1+"'>"+value1+"</option>");
                            });
                            form.render('select');
                        }, error: function () {
                            layer.msg("获取尺码失败！", {icon: 2});
                        }
                    })
                },100);
            }
        }
    });

    function refreshTable() {
        var param = {};
        var orderName = $("#orderName").val();
        var season = $("#season").val();
        var customerName = $("#customerName").val();
        var from = $("#from").val();
        var to = $("#to").val();
        if ((season == null || season === "") && (customerName == null || customerName === "") && (orderName == null || orderName === "")){
            layer.msg("季度-品牌-款号不能同时为空", { icon: 2 });
            return false;
        }
        if (season != null && season != ""){
            param.season = season;
        }
        if (customerName != null && customerName != ""){
            param.customerName = customerName;
        }
        if (orderName != null && orderName != ""){
            param.orderName = orderName;
        }
        var load = layer.load();
        $.ajax({
            url: "/erp/searchposttreatmentbyinfo",
            type: 'GET',
            data: param,
            success: function (res) {
                if (res.postTreatmentList) {
                    var reportData = res.postTreatmentList;

                    layui.table.reload("reportTable", {
                        cols:[[
                            {title: '#', width: 50, collapse: true,lazy: true, icon: ['layui-icon layui-icon-triangle-r', 'layui-icon layui-icon-triangle-d'], children:[
                                    {
                                        title: '洗水记录'
                                        ,url: 'getposttreatmentbyordertype'
                                        ,where: function(row){
                                            return {
                                                orderName: row.orderName,
                                                washCount: 1
                                            }
                                        }
                                        ,height: 300
                                        ,loading:true
                                        ,page: false
                                        ,totalRow: true
                                        ,cols: [[
                                            {field: 'clothesVersionNumber', title: '单号',align:'center', minWidth: 120},
                                            {field: 'orderName', title: '款号',align:'center', minWidth: 120},
                                            {field: 'colorName', title: '颜色',align:'center', minWidth: 120},
                                            {field: 'sizeName', title: '尺码',align:'center', minWidth: 120 },
                                            {field: 'washCount', title: '数量',align:'center', minWidth: 120, totalRow: true},
                                            {field: 'createTime', title: '日期',align:'center', minWidth: 120},
                                            {field: 'id', title: '操作', minWidth: 70,align:'center', templet: '#washCountChildBar'}
                                        ]]
                                        ,done: function () {
                                            soulTable.render(this);
                                        }
                                        ,toolEvent: function (obj, pobj) {
                                            // obj 子表当前行对象
                                            // pobj 父表当前行对象
                                            var objData = obj.data;
                                            var childId = this.id; // 通过 this 对象获取当前子表的id
                                            if (obj.event === 'deleteWashCount'){
                                                layer.confirm('真的删除吗', function(index){
                                                    $.ajax({
                                                        url: "/erp/deleteposttreatmentbyid",
                                                        type: 'POST',
                                                        data: {
                                                            id: objData.id
                                                        },
                                                        success: function (res) {
                                                            if (res.data > 0) {
                                                                layer.close(index);
                                                                layer.msg("删除成功！", {icon: 1});
                                                                table.reload(childId);
                                                            } else {
                                                                layer.msg("删除失败！", {icon: 2});
                                                            }
                                                        }, error: function () {
                                                            layer.msg("删除失败！", {icon: 2});
                                                        }
                                                    })
                                                });
                                            } else if (obj.event === 'updateWashCount'){
                                                var index = layer.open({
                                                    type: 1 //Page层类型
                                                    , title: '修改'
                                                    , btn: ['保存']
                                                    , shade: 0.6 //遮罩透明度
                                                    , area: ['300px','200px']
                                                    , maxmin: false //允许全屏最小化
                                                    , anim: 0 //0-6的动画形式，-1不开启
                                                    , offset: '100px'
                                                    , content: "<div><table><tr>" +
                                                        "                                            <td style=\"text-align: right;margin-bottom: 15px;\">\n" +
                                                        "                                                <label class=\"layui-form-label\">数量</label>\n" +
                                                        "                                            </td>\n" +
                                                        "                                            <td style=\"margin-bottom: 15px;\">\n" +
                                                        "                                                <input type=\"text\" name=\"updateWashCount\" id=\"updateWashCount\" autocomplete=\"off\" class=\"layui-input\">\n" +
                                                        "                                            </td></tr><tr>" +
                                                        "                                            <td style=\"text-align: right;margin-bottom: 15px;\">\n" +
                                                        "                                                <label class=\"layui-form-label\">日期</label>\n" +
                                                        "                                            </td>\n" +
                                                        "                                            <td style=\"margin-bottom: 15px;\">\n" +
                                                        "                                                <input type=\"text\" name=\"updateWashDate\" style='width: 200px' id=\"updateWashDate\" autocomplete=\"off\" class=\"layui-input\">\n" +
                                                        "                                            </td>" +
                                                        "</tr></table></div>"
                                                    ,yes: function(i, layero){
                                                        var updateWashCount = $("#updateWashCount").val();
                                                        var updateWashDate = $("#updateWashDate").val();
                                                        var postTreatmentData = {"id": objData.id, "washCount": updateWashCount, "createTime": updateWashDate};
                                                        $.ajax({
                                                            url: "/erp/updateposttreatment",
                                                            type:'POST',
                                                            data: {
                                                                postTreatmentJson: JSON.stringify(postTreatmentData)
                                                            },
                                                            success:function(res){
                                                                if(res.data == 0) {
                                                                    table.reload(childId);
                                                                    layer.close(index);
                                                                    layer.msg("修改成功！", {icon: 1});
                                                                }else {
                                                                    layer.msg("修改失败！", {icon: 2});
                                                                }
                                                            }, error:function(){
                                                                layer.msg("修改失败！", {icon: 1});
                                                            }
                                                        });
                                                    }
                                                });
                                                setTimeout(function () {
                                                    $("#updateWashCount").val(objData.washCount);
                                                    $("#updateWashDate").val(objData.createTime);
                                                    layui.laydate.render({
                                                        elem: '#updateWashDate',
                                                        trigger: 'click'
                                                    });
                                                },100);
                                            }

                                        }
                                    }, {
                                        title: '抽办'
                                        ,url: 'getposttreatmentbyordertype'
                                        ,where: function(row){
                                            return {
                                                orderName: row.orderName,
                                                sampleCount: 1
                                            }
                                        }
                                        ,height: 300
                                        ,loading:true
                                        ,page: false
                                        ,totalRow: true
                                        ,cols: [[
                                            {field: 'clothesVersionNumber', title: '单号',align:'center', minWidth: 120},
                                            {field: 'orderName', title: '款号',align:'center', minWidth: 120},
                                            {field: 'colorName', title: '颜色',align:'center', minWidth: 120},
                                            {field: 'sizeName', title: '尺码',align:'center', minWidth: 120 },
                                            {field: 'sampleCount', title: '数量',align:'center', minWidth: 120, totalRow: true},
                                            {field: 'sampleLocation', title: '事由',align:'center', minWidth: 120, totalRow: true},
                                            {field: 'createTime', title: '日期',align:'center', minWidth: 120},
                                            {field: 'id', title: '操作', minWidth: 70,align:'center', templet: '#sampleCountChildBar'}
                                        ]]
                                        ,done: function () {
                                            soulTable.render(this);
                                        }
                                        ,toolEvent: function (obj, pobj) {
                                            // obj 子表当前行对象
                                            // pobj 父表当前行对象
                                            var objData = obj.data;
                                            var childId = this.id; // 通过 this 对象获取当前子表的id
                                            if (obj.event === 'deleteSampleCount'){
                                                layer.confirm('真的删除吗', function(index){
                                                    $.ajax({
                                                        url: "/erp/deleteposttreatmentbyid",
                                                        type: 'POST',
                                                        data: {
                                                            id: objData.id
                                                        },
                                                        success: function (res) {
                                                            if (res.data > 0) {
                                                                layer.close(index);
                                                                layer.msg("删除成功！", {icon: 1});
                                                                table.reload(childId);
                                                            } else {
                                                                layer.msg("删除失败！", {icon: 2});
                                                            }
                                                        }, error: function () {
                                                            layer.msg("删除失败！", {icon: 2});
                                                        }
                                                    })
                                                });
                                            } else if (obj.event === 'updateSampleCount'){
                                                var index = layer.open({
                                                    type: 1 //Page层类型
                                                    , title: '修改'
                                                    , btn: ['保存']
                                                    , shade: 0.6 //遮罩透明度
                                                    , area: ['400px','400px']
                                                    , maxmin: false //允许全屏最小化
                                                    , anim: 0 //0-6的动画形式，-1不开启
                                                    , offset: '100px'
                                                    , content: "<div><table>" +
                                                        "                                            <tr>" +
                                                        "                                            <td style=\"text-align: right;margin-bottom: 15px;\">\n" +
                                                        "                                                <label class=\"layui-form-label\">数量</label>\n" +
                                                        "                                            </td>\n" +
                                                        "                                            <td style=\"margin-bottom: 15px;\">\n" +
                                                        "                                                <input type=\"text\" name=\"updateSampleCount\" id=\"updateSampleCount\" autocomplete=\"off\" class=\"layui-input\">\n" +
                                                        "                                            </td></tr>" +
                                                        "                                            <tr>" +
                                                        "                                            <td style=\"text-align: right;margin-bottom: 15px;\">\n" +
                                                        "                                                <label class=\"layui-form-label\">事由</label>\n" +
                                                        "                                            </td>\n" +
                                                        "                                            <td style=\"margin-bottom: 15px;\">\n" +
                                                        "                                                <input type=\"text\" name=\"updateSampleLocation\" id=\"updateSampleLocation\" autocomplete=\"off\" class=\"layui-input\">\n" +
                                                        "                                            </td></tr>" +
                                                        "                                            <tr>" +
                                                        "                                            <td style=\"text-align: right;margin-bottom: 15px;\">\n" +
                                                        "                                                <label class=\"layui-form-label\">日期</label>\n" +
                                                        "                                            </td>\n" +
                                                        "                                            <td style=\"margin-bottom: 15px;\">\n" +
                                                        "                                                <input type=\"text\" name=\"updateSampleDate\" style='width: 200px' id=\"updateDiffDate\" autocomplete=\"off\" class=\"layui-input\">\n" +
                                                        "                                            </td>" +
                                                        "</tr></table></div>"
                                                    ,yes: function(i, layero){
                                                        var updateSampleCount = $("#updateSampleCount").val();
                                                        var updateSampleDate = $("#updateSampleDate").val();
                                                        var updateSampleLocation = $("#updateSampleLocation").val();
                                                        var postTreatmentData = {"id": objData.id, "sampleCount": updateSampleCount, "sampleLocation": updateSampleLocation, "createTime": updateSampleDate};
                                                        $.ajax({
                                                            url: "/erp/updateposttreatment",
                                                            type:'POST',
                                                            data: {
                                                                postTreatmentJson: JSON.stringify(postTreatmentData)
                                                            },
                                                            success:function(res){
                                                                if(res.data == 0) {
                                                                    table.reload(childId);
                                                                    layer.close(index);
                                                                    layer.msg("修改成功！", {icon: 1});
                                                                }else {
                                                                    layer.msg("修改失败！", {icon: 2});
                                                                }
                                                            }, error:function(){
                                                                layer.msg("修改失败！", {icon: 1});
                                                            }
                                                        });
                                                    }
                                                });
                                                setTimeout(function () {
                                                    $("#updateSampleCount").val(objData.sampleCount);
                                                    $("#updateSampleLocation").val(objData.sampleLocation);
                                                    $("#updateDiffDate").val(objData.createTime);
                                                    layui.laydate.render({
                                                        elem: '#updateSampleDate',
                                                        trigger: 'click'
                                                    });
                                                },100);
                                            }

                                        }
                                    },{
                                        title: '出货记录'
                                        ,url: 'getposttreatmentbyordertype'
                                        ,where: function(row){
                                            return {
                                                orderName: row.orderName,
                                                shipCount: 1
                                            }
                                        }
                                        ,height: 300
                                        ,loading:true
                                        ,page: false
                                        ,totalRow: true
                                        ,cols: [[
                                            {field: 'clothesVersionNumber', title: '单号',align:'center', minWidth: 120},
                                            {field: 'orderName', title: '款号',align:'center', minWidth: 120},
                                            {field: 'colorName', title: '颜色',align:'center', minWidth: 120},
                                            {field: 'sizeName', title: '尺码',align:'center', minWidth: 120 },
                                            {field: 'shipCount', title: '数量',align:'center', minWidth: 120, totalRow: true},
                                            {field: 'createTime', title: '日期',align:'center', minWidth: 120},
                                            {field: 'id', title: '操作', minWidth: 70,align:'center', templet: '#shipCountChildBar'}
                                        ]]
                                        ,done: function () {
                                            soulTable.render(this);
                                        }
                                        ,toolEvent: function (obj, pobj) {
                                            // obj 子表当前行对象
                                            // pobj 父表当前行对象
                                            var objData = obj.data;
                                            var childId = this.id; // 通过 this 对象获取当前子表的id
                                            if (obj.event === 'deleteShipCount'){
                                                layer.confirm('真的删除吗', function(index){
                                                    $.ajax({
                                                        url: "/erp/deleteposttreatmentbyid",
                                                        type: 'POST',
                                                        data: {
                                                            id: objData.id
                                                        },
                                                        success: function (res) {
                                                            if (res.data > 0) {
                                                                layer.close(index);
                                                                layer.msg("删除成功！", {icon: 1});
                                                                table.reload(childId);
                                                            } else {
                                                                layer.msg("删除失败！", {icon: 2});
                                                            }
                                                        }, error: function () {
                                                            layer.msg("删除失败！", {icon: 2});
                                                        }
                                                    })
                                                });
                                            } else if (obj.event === 'updateShipCount'){
                                                var index = layer.open({
                                                    type: 1 //Page层类型
                                                    , title: '修改'
                                                    , btn: ['保存']
                                                    , shade: 0.6 //遮罩透明度
                                                    , area: ['300px','200px']
                                                    , maxmin: false //允许全屏最小化
                                                    , anim: 0 //0-6的动画形式，-1不开启
                                                    , offset: '100px'
                                                    , content: "<div><table><tr>" +
                                                        "                                            <td style=\"text-align: right;margin-bottom: 15px;\">\n" +
                                                        "                                                <label class=\"layui-form-label\">数量</label>\n" +
                                                        "                                            </td>\n" +
                                                        "                                            <td style=\"margin-bottom: 15px;\">\n" +
                                                        "                                                <input type=\"text\" name=\"updateShipCount\" id=\"updateShipCount\" autocomplete=\"off\" class=\"layui-input\">\n" +
                                                        "                                            </td></tr><tr>" +
                                                        "                                            <td style=\"text-align: right;margin-bottom: 15px;\">\n" +
                                                        "                                                <label class=\"layui-form-label\">日期</label>\n" +
                                                        "                                            </td>\n" +
                                                        "                                            <td style=\"margin-bottom: 15px;\">\n" +
                                                        "                                                <input type=\"text\" name=\"updateShipDate\" style='width: 200px' id=\"updateShipDate\" autocomplete=\"off\" class=\"layui-input\">\n" +
                                                        "                                            </td>" +
                                                        "</tr></table></div>"
                                                    ,yes: function(i, layero){
                                                        var updateShipCount = $("#updateShipCount").val();
                                                        var updateShipDate = $("#updateShipDate").val();
                                                        var postTreatmentData = {"id": objData.id, "shipCount": updateShipCount, "createTime": updateShipDate};
                                                        $.ajax({
                                                            url: "/erp/updateposttreatment",
                                                            type:'POST',
                                                            data: {
                                                                postTreatmentJson: JSON.stringify(postTreatmentData)
                                                            },
                                                            success:function(res){
                                                                if(res.data == 0) {
                                                                    table.reload(childId);
                                                                    layer.close(index);
                                                                    layer.msg("修改成功！", {icon: 1});
                                                                }else {
                                                                    layer.msg("修改失败！", {icon: 2});
                                                                }
                                                            }, error:function(){
                                                                layer.msg("修改失败！", {icon: 1});
                                                            }
                                                        });
                                                    }
                                                });
                                                setTimeout(function () {
                                                    $("#updateShipCount").val(objData.shipCount);
                                                    $("#updateShipDate").val(objData.createTime);
                                                    layui.laydate.render({
                                                        elem: '#updateShipDate',
                                                        trigger: 'click'
                                                    });
                                                },100);
                                            }

                                        }
                                    }, {
                                        title: '正品余数'
                                        ,url: 'getposttreatmentbyordertype'
                                        ,where: function(row){
                                            return {
                                                orderName: row.orderName,
                                                surplusCount: 1
                                            }
                                        }
                                        ,height: 300
                                        ,loading:true
                                        ,page: false
                                        ,totalRow: true
                                        ,cols: [[
                                            {field: 'clothesVersionNumber', title: '单号',align:'center', minWidth: 120},
                                            {field: 'orderName', title: '款号',align:'center', minWidth: 120},
                                            {field: 'colorName', title: '颜色',align:'center', minWidth: 120},
                                            {field: 'sizeName', title: '尺码',align:'center', minWidth: 120 },
                                            {field: 'surplusCount', title: '数量',align:'center', minWidth: 120, totalRow: true},
                                            {field: 'surplusLocation', title: '位置',align:'center', minWidth: 120},
                                            {field: 'createTime', title: '日期',align:'center', minWidth: 120},
                                            {field: 'id', title: '操作', minWidth: 70,align:'center', templet: '#surplusCountChildBar'}
                                        ]]
                                        ,done: function () {
                                            soulTable.render(this);
                                        }
                                        ,toolEvent: function (obj, pobj) {
                                            // obj 子表当前行对象
                                            // pobj 父表当前行对象
                                            var objData = obj.data;
                                            var childId = this.id; // 通过 this 对象获取当前子表的id
                                            if (obj.event === 'deleteSurplusCount'){
                                                layer.confirm('真的删除吗', function(index){
                                                    $.ajax({
                                                        url: "/erp/deleteposttreatmentbyid",
                                                        type: 'POST',
                                                        data: {
                                                            id: objData.id
                                                        },
                                                        success: function (res) {
                                                            if (res.data > 0) {
                                                                layer.close(index);
                                                                layer.msg("删除成功！", {icon: 1});
                                                                table.reload(childId);
                                                            } else {
                                                                layer.msg("删除失败！", {icon: 2});
                                                            }
                                                        }, error: function () {
                                                            layer.msg("删除失败！", {icon: 2});
                                                        }
                                                    })
                                                });
                                            } else if (obj.event === 'updateSurplusCount'){
                                                var index = layer.open({
                                                    type: 1 //Page层类型
                                                    , title: '修改'
                                                    , btn: ['保存']
                                                    , shade: 0.6 //遮罩透明度
                                                    , area: ['300px','250px']
                                                    , maxmin: false //允许全屏最小化
                                                    , anim: 0 //0-6的动画形式，-1不开启
                                                    , offset: '100px'
                                                    , content: "<div><table><tr>" +
                                                        "                                            <td style=\"text-align: right;margin-bottom: 15px;\">\n" +
                                                        "                                                <label class=\"layui-form-label\">数量</label>\n" +
                                                        "                                            </td>\n" +
                                                        "                                            <td style=\"margin-bottom: 15px;\">\n" +
                                                        "                                                <input type=\"text\" name=\"updateSurplusCount\" id=\"updateSurplusCount\" autocomplete=\"off\" class=\"layui-input\">\n" +
                                                        "                                            </td></tr><tr>" +
                                                        "                                            <td style=\"text-align: right;margin-bottom: 15px;\">\n" +
                                                        "                                                <label class=\"layui-form-label\">位置</label>\n" +
                                                        "                                            </td>\n" +
                                                        "                                            <td style=\"margin-bottom: 15px;\">\n" +
                                                        "                                                <input type=\"text\" name=\"updateSurplusLocation\" id=\"updateSurplusLocation\" autocomplete=\"off\" class=\"layui-input\">\n" +
                                                        "                                            </td></tr><tr>" +
                                                        "                                            <td style=\"text-align: right;margin-bottom: 15px;\">\n" +
                                                        "                                                <label class=\"layui-form-label\">日期</label>\n" +
                                                        "                                            </td>\n" +
                                                        "                                            <td style=\"margin-bottom: 15px;\">\n" +
                                                        "                                                <input type=\"text\" name=\"updateSurplusDate\" style='width: 200px' id=\"updateSurplusDate\" autocomplete=\"off\" class=\"layui-input\">\n" +
                                                        "                                            </td>" +
                                                        "</tr></table></div>"
                                                    ,yes: function(i, layero){
                                                        var updateSurplusCount = $("#updateSurplusCount").val();
                                                        var updateSurplusDate = $("#updateSurplusDate").val();
                                                        var updateSurplusLocation = $("#updateSurplusLocation").val();
                                                        var postTreatmentData = {"id": objData.id, "surplusLocation": updateSurplusLocation, "surplusCount": updateSurplusCount, "createTime": updateSurplusDate};
                                                        $.ajax({
                                                            url: "/erp/updateposttreatment",
                                                            type:'POST',
                                                            data: {
                                                                postTreatmentJson: JSON.stringify(postTreatmentData)
                                                            },
                                                            success:function(res){
                                                                if(res.data == 0) {
                                                                    table.reload(childId);
                                                                    layer.close(index);
                                                                    layer.msg("修改成功！", {icon: 1});
                                                                }else {
                                                                    layer.msg("修改失败！", {icon: 2});
                                                                }
                                                            }, error:function(){
                                                                layer.msg("修改失败！", {icon: 1});
                                                            }
                                                        });
                                                    }
                                                });
                                                setTimeout(function () {
                                                    $("#updateSurplusCount").val(objData.surplusCount);
                                                    $("#updateSurplusDate").val(objData.createTime);
                                                    $("#updateSurplusLocation").val(objData.surplusLocation);
                                                    layui.laydate.render({
                                                        elem: '#updateSurplusDate',
                                                        trigger: 'click'
                                                    });
                                                },100);
                                            }

                                        }
                                    }, {
                                        title: '次品数量'
                                        ,url: 'getposttreatmentbyordertype'
                                        ,where: function(row){
                                            return {
                                                orderName: row.orderName,
                                                defectiveCount: 1
                                            }
                                        }
                                        ,height: 300
                                        ,loading:true
                                        ,page: false
                                        ,totalRow: true
                                        ,cols: [[
                                            {field: 'clothesVersionNumber', title: '单号',align:'center', minWidth: 120},
                                            {field: 'orderName', title: '款号',align:'center', minWidth: 120},
                                            {field: 'colorName', title: '颜色',align:'center', minWidth: 120},
                                            {field: 'sizeName', title: '尺码',align:'center', minWidth: 120 },
                                            {field: 'defectiveCount', title: '数量',align:'center', minWidth: 120, totalRow: true},
                                            {field: 'defectiveLocation', title: '位置',align:'center', minWidth: 120},
                                            {field: 'createTime', title: '日期',align:'center', minWidth: 120},
                                            {field: 'id', title: '操作', minWidth: 70,align:'center', templet: '#defectiveCountChildBar'}
                                        ]]
                                        ,done: function () {
                                            soulTable.render(this);
                                        }
                                        ,toolEvent: function (obj, pobj) {
                                            // obj 子表当前行对象
                                            // pobj 父表当前行对象
                                            var objData = obj.data;
                                            var childId = this.id; // 通过 this 对象获取当前子表的id
                                            if (obj.event === 'deleteDefectiveCount'){
                                                layer.confirm('真的删除吗', function(index){
                                                    $.ajax({
                                                        url: "/erp/deleteposttreatmentbyid",
                                                        type: 'POST',
                                                        data: {
                                                            id: objData.id
                                                        },
                                                        success: function (res) {
                                                            if (res.data > 0) {
                                                                layer.close(index);
                                                                layer.msg("删除成功！", {icon: 1});
                                                                table.reload(childId);
                                                            } else {
                                                                layer.msg("删除失败！", {icon: 2});
                                                            }
                                                        }, error: function () {
                                                            layer.msg("删除失败！", {icon: 2});
                                                        }
                                                    })
                                                });
                                            } else if (obj.event === 'updateDefectiveCount'){
                                                var index = layer.open({
                                                    type: 1 //Page层类型
                                                    , title: '修改'
                                                    , btn: ['保存']
                                                    , shade: 0.6 //遮罩透明度
                                                    , area: ['300px','250px']
                                                    , maxmin: false //允许全屏最小化
                                                    , anim: 0 //0-6的动画形式，-1不开启
                                                    , offset: '100px'
                                                    , content: "<div><table><tr>" +
                                                        "                                            <td style=\"text-align: right;margin-bottom: 15px;\">\n" +
                                                        "                                                <label class=\"layui-form-label\">数量</label>\n" +
                                                        "                                            </td>\n" +
                                                        "                                            <td style=\"margin-bottom: 15px;\">\n" +
                                                        "                                                <input type=\"text\" name=\"updateDefectiveCount\" id=\"updateDefectiveCount\" autocomplete=\"off\" class=\"layui-input\">\n" +
                                                        "                                            </td></tr><tr>" +
                                                        "                                            <td style=\"text-align: right;margin-bottom: 15px;\">\n" +
                                                        "                                                <label class=\"layui-form-label\">位置</label>\n" +
                                                        "                                            </td>\n" +
                                                        "                                            <td style=\"margin-bottom: 15px;\">\n" +
                                                        "                                                <input type=\"text\" name=\"updateDefectiveLocation\" id=\"updateDefectiveLocation\" autocomplete=\"off\" class=\"layui-input\">\n" +
                                                        "                                            </td></tr><tr>" +
                                                        "                                            <td style=\"text-align: right;margin-bottom: 15px;\">\n" +
                                                        "                                                <label class=\"layui-form-label\">日期</label>\n" +
                                                        "                                            </td>\n" +
                                                        "                                            <td style=\"margin-bottom: 15px;\">\n" +
                                                        "                                                <input type=\"text\" name=\"updateDefectiveDate\" style='width: 200px' id=\"updateDefectiveDate\" autocomplete=\"off\" class=\"layui-input\">\n" +
                                                        "                                            </td>" +
                                                        "</tr></table></div>"
                                                    ,yes: function(i, layero){
                                                        var updateDefectiveCount = $("#updateDefectiveCount").val();
                                                        var updateDefectiveDate = $("#updateDefectiveDate").val();
                                                        var updateDefectiveLocation = $("#updateDefectiveLocation").val();
                                                        var postTreatmentData = {"id": objData.id, "defectiveLocation": updateDefectiveLocation, "defectiveCount": updateDefectiveCount, "createTime": updateDefectiveDate};
                                                        $.ajax({
                                                            url: "/erp/updateposttreatment",
                                                            type:'POST',
                                                            data: {
                                                                postTreatmentJson: JSON.stringify(postTreatmentData)
                                                            },
                                                            success:function(res){
                                                                if(res.data == 0) {
                                                                    table.reload(childId);
                                                                    layer.close(index);
                                                                    layer.msg("修改成功！", {icon: 1});
                                                                }else {
                                                                    layer.msg("修改失败！", {icon: 2});
                                                                }
                                                            }, error:function(){
                                                                layer.msg("修改失败！", {icon: 1});
                                                            }
                                                        });
                                                    }
                                                });
                                                setTimeout(function () {
                                                    $("#updateDefectiveCount").val(objData.defectiveCount);
                                                    $("#updateDefectiveDate").val(objData.createTime);
                                                    $("#updateDefectiveLocation").val(objData.defectiveLocation);
                                                    layui.laydate.render({
                                                        elem: '#updateDefectiveDate',
                                                        trigger: 'click'
                                                    });
                                                },100);
                                            }

                                        }
                                    }, {
                                        title: '次片数量'
                                        ,url: 'getposttreatmentbyordertype'
                                        ,where: function(row){
                                            return {
                                                orderName: row.orderName,
                                                chickenCount: 1
                                            }
                                        }
                                        ,height: 300
                                        ,loading:true
                                        ,page: false
                                        ,totalRow: true
                                        ,cols: [[
                                            {field: 'clothesVersionNumber', title: '单号',align:'center', minWidth: 120},
                                            {field: 'orderName', title: '款号',align:'center', minWidth: 120},
                                            {field: 'colorName', title: '颜色',align:'center', minWidth: 120},
                                            {field: 'sizeName', title: '尺码',align:'center', minWidth: 120 },
                                            {field: 'chickenCount', title: '数量',align:'center', minWidth: 120, totalRow: true},
                                            {field: 'chickenLocation', title: '位置',align:'center', minWidth: 120},
                                            {field: 'createTime', title: '日期',align:'center', minWidth: 120},
                                            {field: 'id', title: '操作', minWidth: 70,align:'center', templet: '#chickenCountChildBar'}
                                        ]]
                                        ,done: function () {
                                            soulTable.render(this);
                                        }
                                        ,toolEvent: function (obj, pobj) {
                                            // obj 子表当前行对象
                                            // pobj 父表当前行对象
                                            var objData = obj.data;
                                            var childId = this.id; // 通过 this 对象获取当前子表的id
                                            if (obj.event === 'deleteChickenCount'){
                                                layer.confirm('真的删除吗', function(index){
                                                    $.ajax({
                                                        url: "/erp/deleteposttreatmentbyid",
                                                        type: 'POST',
                                                        data: {
                                                            id: objData.id
                                                        },
                                                        success: function (res) {
                                                            if (res.data > 0) {
                                                                layer.close(index);
                                                                layer.msg("删除成功！", {icon: 1});
                                                                table.reload(childId);
                                                            } else {
                                                                layer.msg("删除失败！", {icon: 2});
                                                            }
                                                        }, error: function () {
                                                            layer.msg("删除失败！", {icon: 2});
                                                        }
                                                    })
                                                });
                                            } else if (obj.event === 'updateChickenCount'){
                                                var index = layer.open({
                                                    type: 1 //Page层类型
                                                    , title: '修改'
                                                    , btn: ['保存']
                                                    , shade: 0.6 //遮罩透明度
                                                    , area: ['300px','250px']
                                                    , maxmin: false //允许全屏最小化
                                                    , anim: 0 //0-6的动画形式，-1不开启
                                                    , offset: '100px'
                                                    , content: "<div><table><tr>" +
                                                        "                                            <td style=\"text-align: right;margin-bottom: 15px;\">\n" +
                                                        "                                                <label class=\"layui-form-label\">数量</label>\n" +
                                                        "                                            </td>\n" +
                                                        "                                            <td style=\"margin-bottom: 15px;\">\n" +
                                                        "                                                <input type=\"text\" name=\"updateChickenCount\" id=\"updateChickenCount\" autocomplete=\"off\" class=\"layui-input\">\n" +
                                                        "                                            </td></tr><tr>" +
                                                        "                                            <td style=\"text-align: right;margin-bottom: 15px;\">\n" +
                                                        "                                                <label class=\"layui-form-label\">位置</label>\n" +
                                                        "                                            </td>\n" +
                                                        "                                            <td style=\"margin-bottom: 15px;\">\n" +
                                                        "                                                <input type=\"text\" name=\"updateChickenLocation\" id=\"updateChickenLocation\" autocomplete=\"off\" class=\"layui-input\">\n" +
                                                        "                                            </td></tr><tr>" +
                                                        "                                            <td style=\"text-align: right;margin-bottom: 15px;\">\n" +
                                                        "                                                <label class=\"layui-form-label\">日期</label>\n" +
                                                        "                                            </td>\n" +
                                                        "                                            <td style=\"margin-bottom: 15px;\">\n" +
                                                        "                                                <input type=\"text\" name=\"updateChickenDate\" style='width: 200px' id=\"updateChickenDate\" autocomplete=\"off\" class=\"layui-input\">\n" +
                                                        "                                            </td>" +
                                                        "</tr></table></div>"
                                                    ,yes: function(i, layero){
                                                        var updateChickenCount = $("#updateChickenCount").val();
                                                        var updateChickenDate = $("#updateChickenDate").val();
                                                        var updateChickenLocation = $("#updateChickenLocation").val();
                                                        var postTreatmentData = {"id": objData.id, "chickenLocation": updateChickenLocation, "chickenCount": updateChickenCount, "createTime": updateChickenDate};
                                                        $.ajax({
                                                            url: "/erp/updateposttreatment",
                                                            type:'POST',
                                                            data: {
                                                                postTreatmentJson: JSON.stringify(postTreatmentData)
                                                            },
                                                            success:function(res){
                                                                if(res.data == 0) {
                                                                    table.reload(childId);
                                                                    layer.close(index);
                                                                    layer.msg("修改成功！", {icon: 1});
                                                                }else {
                                                                    layer.msg("修改失败！", {icon: 2});
                                                                }
                                                            }, error:function(){
                                                                layer.msg("修改失败！", {icon: 1});
                                                            }
                                                        });
                                                    }
                                                });
                                                setTimeout(function () {
                                                    $("#updateChickenCount").val(objData.chickenCount);
                                                    $("#updateChickenDate").val(objData.createTime);
                                                    $("#updateChickenLocation").val(objData.chickenLocation);
                                                    layui.laydate.render({
                                                        elem: '#updateChickenDate',
                                                        trigger: 'click'
                                                    });
                                                },100);
                                            }

                                        }
                                    }
                                ]}
                            ,{type:'radio'}
                            ,{type:'numbers', align:'center', title:'序号', width:70}
                            ,{field:'clothesVersionNumber', title:'单号', align:'center', width:150, sort: true, filter: true, totalRowText: '合计'}
                            ,{field:'orderName', title:'款号', align:'center', width:150, sort: true, filter: true}
                            ,{field:'orderCount', title:'订单', align:'center', minWidth:80, sort: true, filter: true, totalRow: true}
                            ,{field:'wellCount', title:'好片', align:'center', minWidth:80, sort: true, filter: true, totalRow: true}
                            ,{field:'checkCount', title:'查片', align:'center', minWidth:80, sort: true, filter: true, totalRow: true,templet:function (d) {
                                    if (d.checkCount > d.wellCount){
                                        return "<p style='color: red'>"+ d.checkCount +"</p>";
                                    } else {
                                        return "<p style='color: green'>"+ d.checkCount +"</p>";
                                    }
                                }}
                            ,{field:'hangCount', title:'挂片', align:'center', minWidth:80, sort: true, filter: true, totalRow: true,templet:function (d) {
                                    if (d.hangCount > d.wellCount){
                                        return "<p style='color: red'>"+ d.hangCount +"</p>";
                                    } else {
                                        return "<p style='color: green'>"+ d.hangCount +"</p>";
                                    }
                                }}
                            ,{field:'inspectionCount', title:'中查', align:'center', minWidth:80, sort: true, filter: true, totalRow: true,templet:function (d) {
                                    if (d.inspectionCount > d.wellCount){
                                        return "<p style='color: red'>"+ d.inspectionCount +"</p>";
                                    } else {
                                        return "<p style='color: green'>"+ d.inspectionCount +"</p>";
                                    }
                                }}
                            ,{field:'sampleCount', title:'抽办', align:'center', minWidth:80, sort: true, filter: true, totalRow: true,templet:function (d) {
                                    if (d.sampleCount > d.wellCount){
                                        return "<p style='color: red'>"+ d.sampleCount +"</p>";
                                    } else {
                                        return "<p style='color: green'>"+ d.sampleCount +"</p>";
                                    }
                                }}
                            ,{field:'washCount', title:'洗水', align:'center', minWidth:80, sort: true, filter: true, totalRow: true,templet:function (d) {
                                    if (d.washCount > d.wellCount){
                                        return "<p style='color: red'>"+ d.washCount +"</p>";
                                    } else {
                                        return "<p style='color: green'>"+ d.washCount +"</p>";
                                    }
                                }}
                            ,{field:'ironCount', title:'大烫', align:'center', minWidth:80, sort: true, filter: true, totalRow: true,templet:function (d) {
                                    if (d.ironCount > d.wellCount){
                                        return "<p style='color: red'>"+ d.ironCount +"</p>";
                                    } else {
                                        return "<p style='color: green'>"+ d.ironCount +"</p>";
                                    }
                                }}
                            ,{field:'tailCheckCount', title:'尾查', align:'center', minWidth:80, sort: true, filter: true, totalRow: true,templet:function (d) {
                                    if (d.tailCheckCount > d.wellCount){
                                        return "<p style='color: red'>"+ d.tailCheckCount +"</p>";
                                    } else {
                                        return "<p style='color: green'>"+ d.tailCheckCount +"</p>";
                                    }
                                }}
                            ,{field:'packageCount', title:'装箱', align:'center', minWidth:80, sort: true, filter: true, totalRow: true,templet:function (d) {
                                    if (d.packageCount > d.wellCount){
                                        return "<p style='color: red'>"+ d.packageCount +"</p>";
                                    } else {
                                        return "<p style='color: green'>"+ d.packageCount +"</p>";
                                    }
                                }}
                            ,{field:'shipCount', title:'出货', align:'center', minWidth:80, sort: true, filter: true, totalRow: true,templet:function (d) {
                                    if (d.shipCount > d.wellCount){
                                        return "<p style='color: red'>"+ d.shipCount +"</p>";
                                    } else {
                                        return "<p style='color: green'>"+ d.shipCount +"</p>";
                                    }
                                }}
                            ,{field:'surplusCount', title:'正品余数', align:'center', width:110, sort: true, filter: true, totalRow: true,templet:function (d) {
                                    if (d.surplusCount > d.wellCount){
                                        return "<p style='color: red'>"+ d.surplusCount +"</p>";
                                    } else {
                                        return "<p style='color: green'>"+ d.surplusCount +"</p>";
                                    }
                                }}
                            ,{field:'defectiveCount', title:'次品', align:'center', minWidth:80, sort: true, filter: true, totalRow: true,templet:function (d) {
                                    if (d.defectiveCount > d.wellCount){
                                        return "<p style='color: red'>"+ d.defectiveCount +"</p>";
                                    } else {
                                        return "<p style='color: green'>"+ d.defectiveCount +"</p>";
                                    }
                                }}
                            ,{field:'chickenCount', title:'次片', align:'center', minWidth:80, sort: true, filter: true, totalRow: true,templet:function (d) {
                                    if (d.chickenCount > d.wellCount){
                                        return "<p style='color: red'>"+ d.chickenCount +"</p>";
                                    } else {
                                        return "<p style='color: green'>"+ d.chickenCount +"</p>";
                                    }
                                }}
                            ,{field:'diffCount', title:'差数', align:'center', minWidth:80, sort: true, filter: true, totalRow: true,templet:function (d) {
                                    if (d.diffCount > d.wellCount){
                                        return "<p style='color: red'>"+ d.diffCount +"</p>";
                                    } else {
                                        return "<p style='color: green'>"+ d.diffCount +"</p>";
                                    }
                                }}
                        ]]
                        ,excel: {
                            filename: '完工报告.xlsx'
                        }
                        ,data: reportData   // 将新数据重新载入表格
                        ,overflow: 'tips'
                        ,even: true
                        ,done: function () {
                            soulTable.render(this);
                            var divArr = $(".layui-table-total div.layui-table-cell");
                            $.each(divArr,function (index,item) {
                                var _div = $(item);
                                var content = _div.html();
                                content = content.replace(".00","");
                                _div.html(content);
                            });
                            layer.close(load);
                        }
                    });

                } else {
                    layer.msg("获取失败！", {icon: 2});
                    layer.close(load);
                }
            }, error: function () {
                layer.msg("获取失败！", {icon: 2});
                layer.close(load);
            }
        });
        return false;
    }

});

function toDecimal(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*100)/100;
    return f;
}

function toDecimalFour(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*10000)/10000;
    return f;
}
