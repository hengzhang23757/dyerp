layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.21'
}).extend({                         // 模块别名
    soulTable: 'soulTable',
});
var accessoryStorageTable;
layui.use(['form', 'soulTable', 'table', 'upload','yutons_sug'], function () {
    var table = layui.table,
        soulTable = layui.soulTable
    var load = layer.load();
    accessoryStorageTable = table.render({
        elem: '#accessoryStorageTable'
        ,url:'getallaccessorystorage'
        ,method:'post'
        ,cols: [[]]
        ,loading:true
        ,done: function (res, curr, count) {
            table.init('accessoryStorageTable',{//转换成静态表格
                cols:[[
                    {type:'numbers', align:'center', title:'序号', width:60}
                    ,{field:'orderName', title:'款号', align:'center', width:130, sort: true, filter: true, totalRowText: '合计'}
                    ,{field:'clothesVersionNumber', title:'版单号', align:'center', width:130, sort: true, filter: true}
                    ,{field:'accessoryName', title:'辅料名称', align:'center', width:120, sort: true, filter: true}
                    ,{field:'specification', title:'规格', align:'center', width:120, sort: true, filter: true}
                    ,{field:'accessoryUnit', title:'单位', align:'center', width:220, sort: true, filter: true}
                    ,{field:'accessoryColor', title:'辅料颜色', align:'center', width:100, sort: true, filter: true}
                    ,{field:'pieceUsage', title:'单件用量', align:'center', width:100, sort: true, filter: true}
                    ,{field:'sizeName', title:'尺码', align:'center',width:100, sort: true, filter: true}
                    ,{field:'colorName', title:'色组', align:'center', width:100, sort: true, filter: true}
                    ,{field:'storageCount', title:'库存量', align:'center', width:150, sort: true, filter: true}
                    ,{field:'accessoryLocation', title:'仓位', align:'center', width:90, sort: true, filter: true, totalRow: true}
                    ,{field:'supplier', title:'供应商', align:'center', width:90, sort: true, filter: true}
                    ,{field:'accessoryType', title:'是否通用', align:'center', width:90, sort: true, filter: true, totalRow: true}
                    ,{field:'orderGroup', title:'款号组', align:'center', width:100, sort: true, filter: true}
                    ,{field:'updateTime', title:'最后修改时间', align:'center', width:200, sort: true, filter: true,templet: function (d) {
                            return moment(d.updateTime).format("YYYY-MM-DD HH:mm:ss");
                        }}
                    // ,{field:'fabricStorageID', fixed: 'right', title:'操作', align:'center', toolbar: '#barTop', width:100}
                ]]
                ,data:res.data
                ,height: 'full-50'
                ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                ,defaultToolbar: ['filter', 'print']
                ,title: '辅料库存表'
                ,totalRow: true
                ,page: true
                ,even: true
                ,limits: [50, 100, 200]
                ,limit: 100 //每页默认显示的数量
                ,overflow: 'tips'
                ,done: function (res, curr, count) {
                    soulTable.render(this);
                    layer.close(load);
                }
                ,filter: {
                    bottom: true,
                    items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                }
            });
        }
    });


    table.on('toolbar(accessoryStorageTable)', function(obj) {
        if (obj.event === 'clearFilter') {
            // 清除所有筛选条件并重载表格
            // 参数: tableId
            soulTable.clearFilter('accessoryStorageTable')
        } else if (obj.event === 'refresh') {
            accessoryStorageTable.reload();
        } else if (obj.event === 'exportExcel') {
            soulTable.export('accessoryStorageTable');
        }
    });


    //监听行工具事件
    // table.on('tool(accessoryStorageTable)', function(obj){
    //     var data = obj.data;
    //     if(obj.event === 'del'){
    //         layer.confirm('真的删除吗', function(index){
    //             $.ajax({
    //                 url: "/erp/deletefabricstorage",
    //                 type: 'POST',
    //                 data: {
    //                     fabricStorageID:data.fabricStorageID
    //                 },
    //                 success: function (res) {
    //                     if (res.error) {
    //                         layer.msg("删除失败！", {icon: 2});
    //                     } else if (res.info) {
    //                         fabricQueryTable.reload();
    //                         layer.close(index);
    //                         layer.msg(res.info, {icon: 1});
    //                     }
    //                 }, error: function () {
    //                     layer.msg("删除失败！", {icon: 2});
    //                 }
    //             })
    //
    //         });
    //     }
    // })

    });