layui.use('form', function(){
    var form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
    form.render();
});
layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.21'
}).extend({                         // 模块别名
    soulTable: 'soulTable'
});

var accessoryReturnTable;
$(document).on('keydown', '.layui-table-edit', function (e) {
    var td = $(this).parent('td')
        ,tr = td.parent('tr')
        ,trs = tr.parent().parent().find('tr')
        ,tr_index = tr.index()
        ,td_index = td.index()
        ,td_last_index = tr.find('[data-edit="text"]:last').index()
        ,td_first_index = tr.find('[data-edit="text"]:first').index();
    switch (e.keyCode) {
        case 13:
            tr.next().find('td').eq(td_index).click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            return false;
        case 39:
            td.nextAll('[data-edit="text"]:first').click();
            if (td_index == td_last_index){
                tr.next().find('td').eq(td_first_index).click();
                if (tr_index == trs.length - 1){
                    trs.eq(0).find('td').eq(td_first_index).click();
                }
            }
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
        case 37:
            td.prevAll('[data-edit="text"]:first').click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
        case 38:
            tr.prev().find('td').eq(td_index).click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
        case 40:
            tr.next().find('td').eq(td_index).click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
    }

})

$(document).ready(function () {

    layui.use(['autocomplete'], function () {
        layui.autocomplete.render({
            elem: $('#checkNumber')[0],
            url: 'getaccessorychecknumberhint',
            template_val: '{{d}}',
            template_txt: '{{d}}',
            onselect: function (resp) {
            }
        })
    });

    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumber", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });
});

layui.use(['form', 'soulTable', 'table', 'laydate'], function () {

    var table = layui.table,
        soulTable = layui.soulTable,
        laydate = layui.laydate,
        form = layui.form,
        $ = layui.$;

    accessoryReturnTable = table.render({
        elem: '#accessoryReturnTable'
        ,excel: {
            filename: '辅料操作表.xlsx'
        }
        ,height: 'full-100'
        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
        ,cols: [[]]
        ,loading:false
        ,totalRow: true
        ,page: true
        ,even: true
        ,limits: [50, 100, 200]
        ,limit: 100 //每页默认显示的数量
        ,done: function (res, curr, count) {
            soulTable.render(this);
        }
    });

    // initTable('');
    function initTable(orderName){
        var load = layer.load();
        var param = {};
        if (orderName != null && orderName != ''){
            param.orderName = orderName;
        }
        $.ajax({
            url: "/erp/getoneyearmanufactureaccessorylay",
            type: 'GET',
            data: param,
            success: function (res) {
                if (res.data) {
                    var reportData = res.data;
                    table.render({
                        elem: '#accessoryReturnTable'
                        ,cols:[[
                            {title: '#', width:50 , show: 2, layerOption: {title: '子表', area: ['1300px','650px']}, children:[
                                    {
                                        title: '入库记录'
                                        ,url: 'getaccessoryinstorerecordbyinfo'
                                        ,where: function(row){
                                            return {
                                                orderName: row.orderName
                                            }
                                        }
                                        ,loading:true
                                        ,page: false
                                        ,limit: Number.MAX_VALUE //每页默认显示的数量
                                        ,totalRow: true
                                        ,height:500
                                        ,cols: [[
                                            {field: 'id',align:'center', hide:true},
                                            {field: 'orderName', title: '款号',align:'center', hide:true},
                                            {field: 'clothesVersionNumber', title: '版单',align:'center', hide:true},
                                            {field: 'accessoryName', title: '辅料名称',align:'center', minWidth: 100, totalRowText: '合计', filter: true},
                                            {field: 'accessoryNumber', title: '辅料编号',align:'center', minWidth: 100, filter: true},
                                            {field: 'supplier', title: '供应商',align:'center', filter: true},
                                            {field: 'specification', title: '辅料规格',align:'center', minWidth: 100 , filter: true},
                                            {field: 'accessoryUnit', title: '辅料单位',align:'center', minWidth: 100 , filter: true},
                                            {field: 'accessoryColor', title: '辅料颜色',align:'center', minWidth: 100 , filter: true},
                                            {field: 'colorName', title: '色组',align:'center', minWidth: 200, filter: true },
                                            {field: 'sizeName', title: '尺码',align:'center', minWidth: 200, filter: true},
                                            {field: 'inStoreCount', title: '入库数量',align:'center', minWidth: 100, totalRow: true, filter: true},
                                            {field: 'accountCount', title: '对账数量',align:'center', minWidth: 100, totalRow: true, filter: true},
                                            {field: 'colorNumber', title: '色号',align:'center', minWidth: 100, filter: true},
                                            {field: 'inStoreTime', title: '入库时间',align:'center', minWidth: 120,templet:function (d) {
                                                    return moment(d.inStoreTime).format("YYYY-MM-DD");
                                                }, filter: true},
                                            {field: 'accessoryLocation', title: '位置',align:'center', minWidth: 100, filter: true},
                                            {field: 'accessoryID',title: '操作', minWidth: 180,align:'center', templet: '#accessoryChildBar'}
                                        ]]
                                        ,done: function () {
                                            soulTable.render(this);
                                        }
                                        ,toolEvent: function (obj, pobj) {
                                            // obj 子表当前行对象
                                            // pobj 父表当前行对象
                                            var objData = obj.data;
                                            var childId = this.id; // 通过 this 对象获取当前子表的id
                                            if (obj.event === 'deleteAccessoryInStore'){
                                                layer.confirm('真的删除吗', function(index){
                                                    var id = objData.id;
                                                    var orderName = objData.orderName;
                                                    var accessoryID = objData.accessoryID;
                                                    var accessoryName = objData.accessoryName;
                                                    var colorName = objData.colorName;
                                                    var sizeName = objData.sizeName;
                                                    var inStoreCount = objData.inStoreCount;
                                                    var accessoryLocation = objData.accessoryLocation;
                                                    $.ajax({
                                                        url: "/erp/deleteaccessoryinstoredata",
                                                        type: 'POST',
                                                        data: {
                                                            id:id,
                                                            orderName:orderName,
                                                            accessoryName:accessoryName,
                                                            colorName:colorName,
                                                            sizeName:sizeName,
                                                            inStoreCount:inStoreCount,
                                                            accessoryLocation:accessoryLocation
                                                        },
                                                        success: function (res) {
                                                            if (res.result == 0) {
                                                                layer.close(index);
                                                                layer.msg("删除成功！", {icon: 1});
                                                                obj.del();
                                                                table.reload(childId);
                                                            } else if (res.result == 3) {
                                                                layer.msg("库存不足,无法删除！", {icon: 2});
                                                            } else {
                                                                layer.msg("删除失败！", {icon: 2});
                                                            }
                                                        },
                                                        error: function () {
                                                            layer.msg("删除失败！", {icon: 2});
                                                        }
                                                    })
                                                });
                                            } else if (obj.event === 'change') {
                                                var index = layer.open({
                                                    type: 1 //Page层类型
                                                    , title: '入库记录转补料单'
                                                    , btn: ['保存']
                                                    , shade: 0.6 //遮罩透明度
                                                    , anim: 0 //0-6的动画形式，-1不开启
                                                    , content: "<div id='changeDiv'>" +
                                                        "            <form class=\"layui-form\" style=\"margin: 0 auto;max-width:100%; padding-top: 0px;\">\n" +
                                                        "                <table class=\"layui-hide\" id=\"accessoryChangeTable\" lay-filter=\"accessoryChangeTable\"></table>\n" +
                                                        "            </form>\n" +
                                                        "      </div>"
                                                    ,yes: function(index, layero){
                                                        var accessoryChangeTableData = table.cache.accessoryChangeTable;
                                                        var param = {};
                                                        param.id = objData.id;
                                                        var flag = true;
                                                        $.each(accessoryChangeTableData, function (index, item){
                                                            if (item.LAY_CHECKED){
                                                                param.accessoryName = item.accessoryName;
                                                                param.accessoryID = item.accessoryID;
                                                                flag = false;
                                                            }
                                                        });
                                                        if (flag){
                                                            layer.msg("请选择要转的行~~~");
                                                            return false;
                                                        }
                                                        if (objData.accessoryName != param.accessoryName){
                                                            layer.msg("辅料不对应,不能乱转~~~");
                                                            return false;
                                                        }
                                                        $.ajax({
                                                            url: "/erp/changeaccessoryinstoredatatoanother",
                                                            type: 'POST',
                                                            data: param,
                                                            success: function (res) {
                                                                if (res.result == 0) {
                                                                    table.reload(childId);
                                                                    layer.close(index);
                                                                    layer.msg("出库成功！", {icon: 1});
                                                                } else if (res.result == 3) {
                                                                    layer.msg("还未审核通过！", {icon: 2});
                                                                } else if (res.result == 4) {
                                                                    layer.msg("财务已经对账,不能转了！", {icon: 2});
                                                                } else {
                                                                    layer.msg("出库失败！", {icon: 2});
                                                                }
                                                            }, error: function () {
                                                                layer.msg("出库失败！", {icon: 2});
                                                            }
                                                        })
                                                    }, cancel : function (i,layero) {

                                                    }
                                                });
                                                layer.full(index);
                                                setTimeout(function () {
                                                    $.ajax({
                                                        url: "/erp/getmanufactureaccessorybyorder",
                                                        type: 'GET',
                                                        data: {
                                                            orderName: objData.orderName
                                                        },
                                                        success: function (res) {
                                                            if (res.data) {
                                                                var title = [
                                                                    {type: 'radio'},
                                                                    {field: 'clothesVersionNumber', title: '单号', align: 'center', minWidth: 100, sort: true, filter: true},
                                                                    {field: 'orderName', title: '款号', align: 'center', minWidth: 100, sort: true, filter: true},
                                                                    {field: 'accessoryName', title: '辅料名称', align: 'center', minWidth: 120, sort: true, filter: true},
                                                                    {field: 'accessoryNumber', title: '编号', align: 'center', minWidth: 80, sort: true, filter: true},
                                                                    {field: 'specification', title: '规格', align: 'center', minWidth: 80, sort: true, filter: true},
                                                                    {field: 'accessoryUnit', title: '单位', align: 'center', minWidth: 80, sort: true, filter: true},
                                                                    {field: 'accessoryColor', title: '辅料颜色', align: 'center', minWidth: 100, sort: true, filter: true},
                                                                    {field: 'sizeName', title: '尺码', align: 'center', minWidth: 100, sort: true, filter: true},
                                                                    {field: 'colorName', title: '颜色', align: 'center', minWidth: 100, sort: true, filter: true},
                                                                    {field: 'supplier', title: '供应商', align: 'center', minWidth: 80, sort: true, filter: true},
                                                                    {field: 'pieceUsage', title: '单件用量', align: 'center', minWidth: 90, sort: true, filter: true},
                                                                    {field: 'orderCount', title: '订单量', align: 'center', minWidth: 90, sort: true, filter: true, totalRow: true},
                                                                    {field: 'accessoryCount', title: '订购量', align: 'center', minWidth: 90, sort: true, filter: true, totalRow: true},
                                                                    {field: 'remark', title: '备注', align: 'center', minWidth: 60, sort: true, filter: true},
                                                                    {field: 'accessoryID', title: '', hide:true}
                                                                ];
                                                                var accessoryChangeData = [];
                                                                $.each(res.data,function(index1,value1){
                                                                    var thisAccessoryData = {};
                                                                    thisAccessoryData.accessoryID = value1.accessoryID;
                                                                    thisAccessoryData.orderName = value1.orderName;
                                                                    thisAccessoryData.clothesVersionNumber = value1.clothesVersionNumber;
                                                                    thisAccessoryData.accessoryName = value1.accessoryName;
                                                                    thisAccessoryData.accessoryNumber = value1.accessoryNumber;
                                                                    thisAccessoryData.specification = value1.specification;
                                                                    thisAccessoryData.accessoryUnit = value1.accessoryUnit;
                                                                    thisAccessoryData.accessoryColor = value1.accessoryColor;
                                                                    thisAccessoryData.pieceUsage = value1.pieceUsage;
                                                                    thisAccessoryData.orderCount = value1.orderCount;
                                                                    thisAccessoryData.sizeName = value1.sizeName;
                                                                    thisAccessoryData.colorName = value1.colorName;
                                                                    thisAccessoryData.accessoryCount = value1.accessoryCount;
                                                                    thisAccessoryData.supplier = value1.supplier;
                                                                    thisAccessoryData.remark = value1.remark;
                                                                    accessoryChangeData.push(thisAccessoryData);
                                                                });
                                                                table.render({
                                                                    elem: '#accessoryChangeTable'
                                                                    , cols: [title]
                                                                    , data: accessoryChangeData
                                                                    , height: 'full-180'
                                                                    , even: true
                                                                    , overflow: 'tips'
                                                                    , totalRow: true
                                                                    , limit: Number.MAX_VALUE //每页默认显示的数量
                                                                    , done: function (res) {
                                                                        soulTable.render(this);
                                                                    }
                                                                });
                                                                //监听单元格编辑
                                                                table.on('tool(accessoryReturnAddTable)', function (obj) {
                                                                    var data = obj.data;
                                                                    var tableData = table.cache.accessoryReturnAddTable;
                                                                    var that = this;
                                                                    var field = $(that).data('field');
                                                                    if(obj.event === 'date'){
                                                                        laydate.render({
                                                                            elem: $(this).find('.layui-table-edit').get(0)//
                                                                            ,show: true //直接显示
                                                                            ,done: function(value, date){
                                                                                obj.update({
                                                                                    [field] : value
                                                                                });
                                                                            }
                                                                        });
                                                                    }
                                                                });
                                                            } else {
                                                                layer.msg("获取失败！", {icon: 2});
                                                            }
                                                        }, error: function () {
                                                            layer.msg("获取失败！", {icon: 2});
                                                        }
                                                    })
                                                }, 100);
                                            }
                                        }
                                    },
                                    {
                                        title: '库存记录'
                                        ,url: 'getaccessorystoragebyinfo'
                                        ,where: function(row){
                                            return {
                                                orderName: row.orderName
                                            }
                                        }
                                        ,loading:true
                                        ,page: false
                                        ,height:500
                                        ,limit: Number.MAX_VALUE //每页默认显示的数量
                                        ,totalRow: true
                                        ,cols: [[
                                            {field: 'id',align:'center', hide:true},
                                            {field: 'accessoryName', title: '辅料名称',align:'center', minWidth: 100, totalRowText: '合计', filter: true},
                                            {field: 'accessoryNumber', title: '编号',align:'center', minWidth: 100, filter: true},
                                            {field: 'orderName', title: '款号',align:'center', hide:true},
                                            {field: 'clothesVersionNumber', title: '版单',align:'center', hide:true},
                                            {field: 'supplier', title: '供应商',align:'center', hide:true},
                                            {field: 'specification', title: '辅料规格',align:'center', minWidth: 100 , filter: true},
                                            {field: 'accessoryUnit', title: '辅料单位',align:'center', minWidth: 100 , filter: true},
                                            {field: 'accessoryColor', title: '辅料颜色',align:'center', minWidth: 100 , filter: true},
                                            {field: 'colorName', title: '色组',align:'center', minWidth: 100, filter: true },
                                            {field: 'sizeName', title: '尺码',align:'center', minWidth: 100, filter: true},
                                            {field: 'storageCount', title: '库存数量',align:'center', minWidth: 100, totalRow: true, filter: true},
                                            {field: 'updateTime', title: '记录时间',align:'center', filter: true, minWidth: 120,templet:function (d) {
                                                    return moment(d.updateTime).format("YYYY-MM-DD");
                                                }},
                                            {field: 'accessoryLocation', title: '位置',align:'center', minWidth: 100, filter: true},
                                            {field: 'accessoryID',title: '操作', minWidth: 120,align:'center', templet: '#accessoryStorageChildBar'},
                                            {field: 'pieceUsage',align:'center', hide:true}
                                        ]]
                                        ,done: function () {
                                            soulTable.render(this);
                                        }
                                        ,toolEvent: function (obj, pobj) {
                                            // obj 子表当前行对象
                                            // pobj 父表当前行对象
                                            var objData = obj.data;
                                            var childId = this.id; // 通过 this 对象获取当前子表的id
                                            if (obj.event === 'outStoreSingle') {
                                                var index = layer.open({
                                                    type: 1 //Page层类型
                                                    , title: '辅料出库'
                                                    , btn: ['保存']
                                                    , shade: 0.6 //遮罩透明度
                                                    ,area: ['420px', '300px'] //宽高
                                                    , anim: 0 //0-6的动画形式，-1不开启
                                                    , content: '<div style="padding-top: 30px">\n' +
                                                        '                    <table>' +
                                                        '                        <tr>' +
                                                        '                            <td style="text-align: right;margin-bottom: 15px;width: 100px">\n' +
                                                        '                                 <label class="layui-form-label" style="100px">库存</label>\n' +
                                                        '                            </td>\n' +
                                                        '                            <td style="margin-bottom: 15px;">\n' +
                                                        '                                 <input type="text" style="width: 260px;" id="storageCount" readonly class="layui-input">\n' +
                                                        '                            </td>' +
                                                        '                        </tr>' +
                                                        '                        <tr>' +
                                                        '                            <td style="text-align: right;margin-bottom: 15px;width: 100px">\n' +
                                                        '                                 <label class="layui-form-label" style="100px">出库</label>\n' +
                                                        '                            </td>\n' +
                                                        '                            <td style="margin-bottom: 15px;">\n' +
                                                        '                                 <input type="text" style="width: 260px;" id="outStoreCount" class="layui-input">\n' +
                                                        '                            </td>' +
                                                        '                        </tr>' +
                                                        '                        <tr>' +
                                                        '                            <td style="text-align: right;margin-bottom: 15px;width: 100px">\n' +
                                                        '                                 <label class="layui-form-label" style="100px">备注</label>\n' +
                                                        '                            </td>\n' +
                                                        '                            <td style="margin-bottom: 15px;">\n' +
                                                        '                                 <input type="text" name="remark" style="width: 260px;" id="remark" value="无" class="layui-input">\n' +
                                                        '                            </td>' +
                                                        '                        </tr>' +
                                                        '                    </table>' +
                                                        '                </div>'
                                                    ,yes: function(index, layero){
                                                        var storageCount = $("#storageCount").val();
                                                        var outStoreCount = $("#outStoreCount").val();
                                                        var remark = $("#remark").val();
                                                        if (outStoreCount == 0 || outStoreCount == "" || (Number(outStoreCount) > Number(storageCount))){
                                                            layer.msg("数量有误！", {icon: 2});
                                                            return false;
                                                        }
                                                        var accessoryOutStoreJson = [];
                                                        accessoryOutStoreJson.push({
                                                            orderName:objData.orderName,
                                                            clothesVersionNumber:objData.clothesVersionNumber,
                                                            id:objData.id,
                                                            accessoryID:objData.accessoryID,
                                                            outStoreCount:outStoreCount,
                                                            accessoryName : objData.accessoryName,
                                                            accessoryNumber : objData.accessoryNumber,
                                                            specification : objData.specification,
                                                            accessoryUnit : objData.accessoryUnit,
                                                            accessoryColor : objData.accessoryColor,
                                                            supplier : objData.supplier,
                                                            pieceUsage : objData.pieceUsage,
                                                            sizeName : objData.sizeName,
                                                            colorName : objData.colorName,
                                                            storageCount : objData.storageCount,
                                                            accessoryLocation : objData.accessoryLocation,
                                                            remark: remark
                                                        });
                                                        $.ajax({
                                                            url: "/erp/accessoryoutstorebatch",
                                                            type: 'POST',
                                                            data: {
                                                                accessoryOutStoreJson: JSON.stringify(accessoryOutStoreJson)
                                                            },
                                                            success: function (res) {
                                                                if (res.result == 0) {
                                                                    table.reload(childId);
                                                                    layer.close(index);
                                                                    layer.msg("出库成功！", {icon: 1});
                                                                } else {
                                                                    layer.msg("出库失败！", {icon: 2});
                                                                }
                                                            }, error: function () {
                                                                layer.msg("出库失败！", {icon: 2});
                                                            }
                                                        })
                                                    }, cancel : function (i,layero) {
                                                        $("#storageCount").val('');
                                                        $("#outStoreCount").val('');
                                                        $("#remark").val('');
                                                    }
                                                });
                                                setTimeout(function () {
                                                    $("#storageCount").val(objData.storageCount);
                                                }, 100);
                                            }
                                        }
                                    },
                                    {
                                        title: '出库记录'
                                        ,url: 'getaccessoryoutrecordbyinfo'
                                        ,where: function(row){
                                            return {
                                                orderName: row.orderName
                                            }
                                        }
                                        ,loading:true
                                        ,page: false
                                        ,limit: Number.MAX_VALUE //每页默认显示的数量
                                        ,totalRow: true
                                        ,height:500
                                        ,cols: [[
                                            {field: 'id',align:'center', hide:true},
                                            {field: 'orderName', title: '款号',align:'center', hide:true},
                                            {field: 'clothesVersionNumber', title: '版单',align:'center', hide:true},
                                            {field: 'accessoryName', title: '辅料名称',align:'center', minWidth: 200, totalRowText: '合计', filter: true},
                                            {field: 'accessoryNumber', title: '辅料编号',align:'center', minWidth: 200, filter: true},
                                            {field: 'supplier', title: '供应商',align:'center', filter: true},
                                            {field: 'specification', title: '辅料规格',align:'center', minWidth: 100, filter: true },
                                            {field: 'accessoryUnit', title: '辅料单位',align:'center', minWidth: 100, filter: true },
                                            {field: 'accessoryColor', title: '辅料颜色',align:'center', minWidth: 100, filter: true },
                                            {field: 'colorName', title: '色组',align:'center', minWidth: 100, filter: true },
                                            {field: 'sizeName', title: '尺码',align:'center', minWidth: 100, filter: true},
                                            {field: 'outStoreCount', title: '出库数量',align:'center', minWidth: 100, totalRow: true, filter: true},
                                            {field: 'createTime', title: '出库时间',align:'center', filter: true, minWidth: 180,templet:function (d) {
                                                    return moment(d.createTime).format("YYYY-MM-DD HH:MM:SS");
                                                }},
                                            {field: 'accessoryLocation', title: '位置', filter: true,align:'center', minWidth: 100},
                                            {field: 'receiver', title: '接收', filter: true,align:'center', minWidth: 100},
                                            {field: 'remark', title: '备注',align:'center', filter: true, minWidth: 100}
                                        ]]
                                        ,done: function () {
                                            soulTable.render(this);
                                        }
                                    }]}
                            ,{type:'radio'}
                            ,{type:'numbers', align:'center', title:'序号'}
                            ,{field: 'orderName', title: '订单',align:'center', minWidth: 120, filter: true}
                            ,{field: 'clothesVersionNumber', title: '版单',align:'center', minWidth: 120, filter: true}
                            ,{field: 'orderCount', title: '订单数量',align:'center', minWidth: 90, filter: true}
                            ,{field: 'wellCount', title: '好片数',align:'center', minWidth: 120, filter: true}
                        ]]
                        ,excel: {
                            filename: '辅料操作表.xlsx'
                        }
                        ,data: reportData
                        ,height: 'full-100'
                        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                        ,title: '辅料操作表'
                        ,totalRow: true
                        ,loading: false
                        ,page: true
                        ,even: true
                        ,limits: [50, 100, 200]
                        ,limit: 100 //每页默认显示的数量
                        ,done: function () {
                            soulTable.render(this);
                            layer.close(load);
                        }
                    });

                } else {
                    layer.msg("获取失败！", {icon: 2});
                }
            }, error: function () {
                layer.msg("获取失败！", {icon: 2});
            }
        });
    }

    function initCheckTable(checkNumber){
        var load = layer.load();
        var param = {};
        if (checkNumber != null && checkNumber != ''){
            param.checkNumber = checkNumber;
        }
        $.ajax({
            url: "/erp/getmanufactureaccessorybychecknumber",
            type: 'GET',
            data: param,
            success: function (res) {
                if (res.data) {
                    var reportData = res.data;
                    table.render({
                        elem: '#accessoryReturnTable'
                        ,cols:[[
                            {title: '#', width: 50, collapse: true,lazy: true, children:[
                                    {
                                        title: '入库记录'
                                        ,url: 'getaccessoryinstorebychecknumber'
                                        ,where: function(row){
                                            return {
                                                checkNumber: row.checkNumber
                                            }
                                        }
                                        ,loading:true
                                        ,page: false
                                        ,limit: Number.MAX_VALUE //每页默认显示的数量
                                        ,totalRow: true
                                        ,height:500
                                        ,cols: [[
                                            {field: 'id',align:'center', hide:true},
                                            {field: 'orderName', title: '款号',align:'center', hide:true},
                                            {field: 'clothesVersionNumber', title: '版单',align:'center', hide:true},
                                            {field: 'accessoryName', title: '辅料名称',align:'center', minWidth: 100, totalRowText: '合计', filter: true},
                                            {field: 'accessoryNumber', title: '辅料编号',align:'center', minWidth: 100, filter: true},
                                            {field: 'supplier', title: '供应商',align:'center', filter: true},
                                            {field: 'specification', title: '辅料规格',align:'center', minWidth: 100 , filter: true},
                                            {field: 'accessoryUnit', title: '辅料单位',align:'center', minWidth: 100 , filter: true},
                                            {field: 'accessoryColor', title: '辅料颜色',align:'center', minWidth: 100 , filter: true},
                                            {field: 'colorName', title: '色组',align:'center', minWidth: 200, filter: true },
                                            {field: 'sizeName', title: '尺码',align:'center', minWidth: 200, filter: true},
                                            {field: 'inStoreCount', title: '入库数量',align:'center', minWidth: 100, totalRow: true, filter: true},
                                            {field: 'accountCount', title: '对账数量',align:'center', minWidth: 100, totalRow: true, filter: true},
                                            {field: 'colorNumber', title: '色号',align:'center', minWidth: 100, filter: true},
                                            {field: 'inStoreTime', title: '入库时间',align:'center', minWidth: 120,templet:function (d) {
                                                    return moment(d.inStoreTime).format("YYYY-MM-DD");
                                                }, filter: true},
                                            {field: 'accessoryLocation', title: '位置',align:'center', minWidth: 100, filter: true},
                                            {field: 'accessoryID',title: '操作', minWidth: 120,align:'center', templet: '#accessoryChildBar'}
                                        ]]
                                        ,done: function () {
                                            soulTable.render(this);
                                        }
                                        ,toolEvent: function (obj, pobj) {
                                            // obj 子表当前行对象
                                            // pobj 父表当前行对象
                                            var objData = obj.data;
                                            var childId = this.id; // 通过 this 对象获取当前子表的id
                                            if (obj.event === 'deleteAccessoryInStore'){
                                                layer.confirm('真的删除吗', function(index){
                                                    var id = objData.id;
                                                    var orderName = objData.orderName;
                                                    var accessoryID = objData.accessoryID;
                                                    var accessoryName = objData.accessoryName;
                                                    var colorName = objData.colorName;
                                                    var sizeName = objData.sizeName;
                                                    var inStoreCount = objData.inStoreCount;
                                                    var accessoryLocation = objData.accessoryLocation;
                                                    $.ajax({
                                                        url: "/erp/deleteaccessoryinstoredata",
                                                        type: 'POST',
                                                        data: {
                                                            id:id,
                                                            orderName:orderName,
                                                            accessoryName:accessoryName,
                                                            colorName:colorName,
                                                            sizeName:sizeName,
                                                            inStoreCount:inStoreCount,
                                                            accessoryLocation:accessoryLocation
                                                        },
                                                        success: function (res) {
                                                            if (res.result == 0) {
                                                                layer.close(index);
                                                                layer.msg("删除成功！", {icon: 1});
                                                                obj.del();
                                                                table.reload(childId);
                                                            } else if (res.result == 3) {
                                                                layer.msg("库存不足,无法删除！", {icon: 2});
                                                            } else {
                                                                layer.msg("删除失败！", {icon: 2});
                                                            }
                                                        },
                                                        error: function () {
                                                            layer.msg("删除失败！", {icon: 2});
                                                        }
                                                    })
                                                });
                                            }
                                        }
                                    },{
                                        title: '库存记录'
                                        ,url: 'getaccessorystoragebyinfo'
                                        ,where: function(row){
                                            return {
                                                orderName: row.orderName
                                            }
                                        }
                                        ,loading:true
                                        ,page: false
                                        ,height:500
                                        ,limit: Number.MAX_VALUE //每页默认显示的数量
                                        ,totalRow: true
                                        ,cols: [[
                                            {field: 'id',align:'center', hide:true},
                                            {field: 'accessoryName', title: '辅料名称',align:'center', minWidth: 100, totalRowText: '合计', filter: true},
                                            {field: 'accessoryNumber', title: '编号',align:'center', minWidth: 100, filter: true},
                                            {field: 'orderName', title: '款号',align:'center', hide:true},
                                            {field: 'clothesVersionNumber', title: '版单',align:'center', hide:true},
                                            {field: 'supplier', title: '供应商',align:'center', hide:true},
                                            {field: 'specification', title: '辅料规格',align:'center', minWidth: 100 , filter: true},
                                            {field: 'accessoryUnit', title: '辅料单位',align:'center', minWidth: 100 , filter: true},
                                            {field: 'accessoryColor', title: '辅料颜色',align:'center', minWidth: 100 , filter: true},
                                            {field: 'colorName', title: '色组',align:'center', minWidth: 100, filter: true },
                                            {field: 'sizeName', title: '尺码',align:'center', minWidth: 100, filter: true},
                                            {field: 'storageCount', title: '库存数量',align:'center', minWidth: 100, totalRow: true, filter: true},
                                            {field: 'updateTime', title: '记录时间',align:'center', filter: true, minWidth: 120,templet:function (d) {
                                                    return moment(d.updateTime).format("YYYY-MM-DD");
                                                }},
                                            {field: 'accessoryLocation', title: '位置',align:'center', minWidth: 100, filter: true},
                                            {field: 'accessoryID',title: '操作', minWidth: 120,align:'center', templet: '#accessoryStorageChildBar'},
                                            {field: 'pieceUsage',align:'center', hide:true}
                                        ]]
                                        ,done: function () {
                                            soulTable.render(this);
                                        }
                                        ,toolEvent: function (obj, pobj) {
                                            // obj 子表当前行对象
                                            // pobj 父表当前行对象
                                            var objData = obj.data;
                                            var childId = this.id; // 通过 this 对象获取当前子表的id
                                            if (obj.event === 'outStoreSingle') {
                                                var index = layer.open({
                                                    type: 1 //Page层类型
                                                    , title: '辅料出库'
                                                    , btn: ['保存']
                                                    , shade: 0.6 //遮罩透明度
                                                    ,area: ['420px', '300px'] //宽高
                                                    , anim: 0 //0-6的动画形式，-1不开启
                                                    , content: '<div style="padding-top: 30px">\n' +
                                                        '                    <table>' +
                                                        '                        <tr>' +
                                                        '                            <td style="text-align: right;margin-bottom: 15px;width: 100px">\n' +
                                                        '                                 <label class="layui-form-label" style="100px">库存</label>\n' +
                                                        '                            </td>\n' +
                                                        '                            <td style="margin-bottom: 15px;">\n' +
                                                        '                                 <input type="text" style="width: 260px;" id="storageCount" readonly class="layui-input">\n' +
                                                        '                            </td>' +
                                                        '                        </tr>' +
                                                        '                        <tr>' +
                                                        '                            <td style="text-align: right;margin-bottom: 15px;width: 100px">\n' +
                                                        '                                 <label class="layui-form-label" style="100px">出库</label>\n' +
                                                        '                            </td>\n' +
                                                        '                            <td style="margin-bottom: 15px;">\n' +
                                                        '                                 <input type="text" style="width: 260px;" id="outStoreCount" class="layui-input">\n' +
                                                        '                            </td>' +
                                                        '                        </tr>' +
                                                        '                        <tr>' +
                                                        '                            <td style="text-align: right;margin-bottom: 15px;width: 100px">\n' +
                                                        '                                 <label class="layui-form-label" style="100px">备注</label>\n' +
                                                        '                            </td>\n' +
                                                        '                            <td style="margin-bottom: 15px;">\n' +
                                                        '                                 <input type="text" name="remark" style="width: 260px;" id="remark" value="无" class="layui-input">\n' +
                                                        '                            </td>' +
                                                        '                        </tr>' +
                                                        '                    </table>' +
                                                        '                </div>'
                                                    ,yes: function(index, layero){
                                                        var storageCount = $("#storageCount").val();
                                                        var outStoreCount = $("#outStoreCount").val();
                                                        var remark = $("#remark").val();
                                                        if (outStoreCount == 0 || outStoreCount == "" || (Number(outStoreCount) > Number(storageCount))){
                                                            layer.msg("数量有误！", {icon: 2});
                                                            return false;
                                                        }
                                                        var accessoryOutStoreJson = [];
                                                        accessoryOutStoreJson.push({
                                                            orderName:objData.orderName,
                                                            clothesVersionNumber:objData.clothesVersionNumber,
                                                            id:objData.id,
                                                            accessoryID:objData.accessoryID,
                                                            outStoreCount:outStoreCount,
                                                            accessoryName : objData.accessoryName,
                                                            accessoryNumber : objData.accessoryNumber,
                                                            specification : objData.specification,
                                                            accessoryUnit : objData.accessoryUnit,
                                                            accessoryColor : objData.accessoryColor,
                                                            supplier : objData.supplier,
                                                            pieceUsage : objData.pieceUsage,
                                                            sizeName : objData.sizeName,
                                                            colorName : objData.colorName,
                                                            storageCount : objData.storageCount,
                                                            accessoryLocation : objData.accessoryLocation,
                                                            remark: remark
                                                        });
                                                        $.ajax({
                                                            url: "/erp/accessoryoutstorebatch",
                                                            type: 'POST',
                                                            data: {
                                                                accessoryOutStoreJson: JSON.stringify(accessoryOutStoreJson)
                                                            },
                                                            success: function (res) {
                                                                if (res.result == 0) {
                                                                    table.reload(childId);
                                                                    layer.close(index);
                                                                    layer.msg("出库成功！", {icon: 1});
                                                                } else {
                                                                    layer.msg("出库失败！", {icon: 2});
                                                                }
                                                            }, error: function () {
                                                                layer.msg("出库失败！", {icon: 2});
                                                            }
                                                        })
                                                    }, cancel : function (i,layero) {
                                                        $("#storageCount").val('');
                                                        $("#outStoreCount").val('');
                                                        $("#remark").val('');
                                                    }
                                                });
                                                setTimeout(function () {
                                                    $("#storageCount").val(objData.storageCount);
                                                }, 100);
                                            }
                                        }
                                    },{
                                        title: '出库记录'
                                        ,url: 'getaccessoryoutrecordbyinfo'
                                        ,where: function(row){
                                            return {
                                                orderName: row.orderName
                                            }
                                        }
                                        ,loading:true
                                        ,page: false
                                        ,limit: Number.MAX_VALUE //每页默认显示的数量
                                        ,totalRow: true
                                        ,height:500
                                        ,cols: [[
                                            {field: 'id',align:'center', hide:true},
                                            {field: 'orderName', title: '款号',align:'center', hide:true},
                                            {field: 'clothesVersionNumber', title: '版单',align:'center', hide:true},
                                            {field: 'accessoryName', title: '辅料名称',align:'center', minWidth: 200, totalRowText: '合计', filter: true},
                                            {field: 'accessoryNumber', title: '辅料编号',align:'center', minWidth: 200, filter: true},
                                            {field: 'supplier', title: '供应商',align:'center', filter: true},
                                            {field: 'specification', title: '辅料规格',align:'center', minWidth: 100, filter: true },
                                            {field: 'accessoryUnit', title: '辅料单位',align:'center', minWidth: 100, filter: true },
                                            {field: 'accessoryColor', title: '辅料颜色',align:'center', minWidth: 100, filter: true },
                                            {field: 'colorName', title: '色组',align:'center', minWidth: 100, filter: true },
                                            {field: 'sizeName', title: '尺码',align:'center', minWidth: 100, filter: true},
                                            {field: 'outStoreCount', title: '出库数量',align:'center', minWidth: 100, totalRow: true, filter: true},
                                            {field: 'createTime', title: '出库时间',align:'center', filter: true, minWidth: 180,templet:function (d) {
                                                    return moment(d.createTime).format("YYYY-MM-DD HH:MM:SS");
                                                }},
                                            {field: 'accessoryLocation', title: '位置', filter: true,align:'center', minWidth: 100},
                                            {field: 'receiver', title: '接收', filter: true,align:'center', minWidth: 100},
                                            {field: 'remark', title: '备注',align:'center', filter: true, minWidth: 100}
                                        ]]
                                        ,done: function () {
                                            soulTable.render(this);
                                        }
                                    }]}
                            ,{type:'radio'}
                            ,{type:'numbers', align:'center', title:'序号'}
                            ,{field: 'checkNumber', title: '合同号',align:'center', minWidth: 150, filter: true}
                            ,{field: 'orderName', title: '款号',align:'center', minWidth: 220, filter: true}
                            ,{field: 'clothesVersionNumber', title: '版单',align:'center', minWidth: 220, filter: true}
                            ,{field: 'accessoryName', title: '辅料名称',align:'center', minWidth: 220, filter: true}
                            ,{field: 'accessoryNumber', title: '辅料编号',align:'center', minWidth: 220, filter: true}
                            ,{field: 'checkState', title: '审核状态',align:'center', minWidth: 150, filter: true}
                        ]]
                        ,excel: {
                            filename: '辅料操作表.xlsx'
                        }
                        ,data: reportData
                        ,height: 'full-100'
                        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                        ,title: '辅料操作表'
                        ,totalRow: true
                        ,loading: false
                        ,page: true
                        ,even: true
                        ,overflow: 'tips'
                        ,limits: [50, 100, 200]
                        ,limit: 100 //每页默认显示的数量
                        ,done: function () {
                            soulTable.render(this);
                            layer.close(load);
                        }
                    });

                } else {
                    layer.msg("获取失败！", {icon: 2});
                }
            }, error: function () {
                layer.msg("获取失败！", {icon: 2});
            }
        });
    }

    form.on('submit(searchBeat)', function(data){
        var orderName = $("#orderName").val();
        initTable(orderName);
        return false;
    });

    form.on('submit(searchNumberBeat)', function(data){
        var checkNumber = $("#checkNumber").val();
        if (checkNumber == null || checkNumber == ''){
            layer.msg("合同号不能为空~~~");
            return false;
        }
        initCheckTable(checkNumber);
        return false;
    });


    table.on('toolbar(accessoryReturnTable)', function(obj) {
        if (obj.event === 'clearFilter') {
            // 清除所有筛选条件并重载表格
            // 参数: tableId
            soulTable.clearFilter('accessoryReturnTable');
        }
        else if (obj.event === 'exportExcel') {
            soulTable.export('accessoryReturnTable');
        } 
        else if (obj.event === "addAccessoryReturn") {   //面料下单
            var orderNameSearch = $("#orderName").val();
            var checkStatus = table.checkStatus(obj.config.id); //获取选中行状态
            if (checkStatus.data.length == 0) {
                layer.msg('请先选中要入库的辅料');
                return false;
            } else {
                if (checkStatus.data[0].checkNumber != null && checkStatus.data[0].checkNumber != ''){
                    var orderName = checkStatus.data[0].orderName;
                    var wellCount = checkStatus.data[0].wellCount;
                    var checkNumber = checkStatus.data[0].checkNumber;
                    var clothesVersionNumber = checkStatus.data[0].clothesVersionNumber;
                    var label = "辅料按合同号入库&emsp;&emsp;&emsp;&emsp;" + "合同号：" + checkNumber;
                    var index = layer.open({
                        type: 1 //Page层类型
                        , title: label
                        , btn: ['保存','填充时间','填充位置']
                        , shade: 0.6 //遮罩透明度
                        , area: '1000px'
                        , maxmin: false //允许全屏最小化
                        , anim: 0 //0-6的动画形式，-1不开启
                        , content: "<div id=\"accessoryReturnAdd\">\n" +
                            "        <table><tr>" +
                            "                   <td style='text-align: right;margin-bottom: 15px;'>\n" +
                            "                       <label class='layui-form-label' style='width: 150px;'>总数</label>\n" +
                            "                   </td>\n" +
                            "                   <td style='margin-bottom: 15px;'>\n" +
                            "                       <input type='text' name='totalCount' id='totalCount' autocomplete='off' class='layui-input'>\n" +
                            "                   </td>" +
                            "                   <td style='text-align: right;margin-bottom: 15px;'>\n" +
                            "                       <label class='layui-form-label' style='width: 150px;'>精度</label>\n" +
                            "                   </td>\n" +
                            "                   <td style='margin-bottom: 15px;'>\n" +
                            "                       <select type='text' name='accuracy' id='accuracy' style='width: 100px' autocomplete='off' class='layui-input'><option value='0'>整数</option><option value='2'>两位小数</option></select>\n" +
                            "                   </td>" +
                            "                   <td style='text-align: right;margin-bottom: 15px;'>" +
                            "                       <label class='layui-form-label' style='width: 150px;'></label>" +
                            "                   </td>\n" +
                            "                   <td style='margin-bottom: 15px;'>\n" +
                            "                       <div class=\"layui-input-inline\">\n" +
                            "                                    <button class=\"layui-btn layui-btn-normal\" lay-submit lay-filter=\"fillPercent\">比例填充</button>\n" +
                            "                                </div>\n" +
                            "                   </td>" +
                            "                   <td style='text-align: right;margin-bottom: 15px;'>" +
                            "                       <label class='layui-form-label' style='width: 150px;'></label>" +
                            "                   </td>\n" +
                            "                   <td style='margin-bottom: 15px;'>\n" +
                            "                       <div class=\"layui-input-inline\">\n" +
                            "                                    <button class=\"layui-btn layui-btn-normal\" lay-submit lay-filter=\"fillTop\">顺序填充</button>\n" +
                            "                                </div>\n" +
                            "                   </td>" +
                            "       </tr></table>"+
                            "        <form class=\"layui-form\" style=\"margin: 0 auto;max-width:100%; padding-top: 0px;\">\n" +
                            "            <table class=\"layui-hide\" id=\"accessoryReturnAddTable\" lay-filter=\"accessoryReturnAddTable\"></table>\n" +
                            "        </form>\n" +
                            "    </div>"
                        , yes: function (i, layero) {
                            var accessoryReturnData = table.cache.accessoryReturnAddTable;
                            var isInput = true;
                            var accessoryDetails = [];
                            $.each(accessoryReturnData, function (i, item) {
                                var indexFlag = 0;
                                if (item.inStoreCount == ''){
                                    indexFlag ++;
                                }
                                if (item.inStoreTime == ''){
                                    indexFlag ++;
                                }
                                if (item.accessoryLocation == ''){
                                    indexFlag ++;
                                }
                                if (indexFlag == 0){
                                    accessoryDetails.push({
                                        orderName:item.orderName,
                                        clothesVersionNumber:item.clothesVersionNumber,
                                        accessoryName:item.accessoryName,
                                        accessoryNumber:item.accessoryNumber,
                                        specification:item.specification,
                                        accessoryColor:item.accessoryColor,
                                        sizeName:item.sizeName,
                                        colorName:item.colorName,
                                        supplier:item.supplier,
                                        pieceUsage:item.pieceUsage,
                                        accessoryUnit:item.accessoryUnit,
                                        inStoreCount:item.inStoreCount,
                                        storageCount:item.inStoreCount,
                                        inStoreTime:item.inStoreTime,
                                        accessoryID:item.accessoryID,
                                        colorNumber:item.colorNumber,
                                        accessoryLocation:item.accessoryLocation,
                                        inStoreType:'分款',
                                        price: item.price
                                    })
                                } else if (indexFlag == 1 || indexFlag == 2){
                                    isInput = false;
                                }

                            });
                            if (!isInput) {
                                layer.msg("请填写完整信息", {icon: 2});
                            } else {
                                $.ajax({
                                    url: "/erp/accessoryinstorebatchchecknumber",
                                    type: 'POST',
                                    data: {
                                        accessoryInStoreJson: JSON.stringify(accessoryDetails),
                                        checkNumber: checkNumber
                                    },
                                    success: function (res) {
                                        if (res.result == 0) {
                                            initCheckTable(checkNumber);
                                            layer.close(index);
                                            layer.msg("录入成功！", {icon: 1});
                                        } else if (res.result == 3) {
                                            layer.msg(res.msg);
                                        } else {
                                            layer.msg("录入失败！", {icon: 2});
                                        }
                                    },
                                    error: function () {
                                        layer.msg("录入失败！", {icon: 2});
                                    }
                                })
                            }
                        }
                        , btn2: function () {
                            var tmpData = table.cache.accessoryReturnAddTable;
                            var tmpTime;
                            $.each(tmpData,function(index1,value1){
                                if (value1.inStoreTime != null && value1.inStoreTime != ''){
                                    tmpTime = value1.inStoreTime;
                                }
                            });
                            $.each(tmpData,function(index1,value1){
                                if (value1.inStoreCount != null && value1.inStoreCount != ''){
                                    value1.inStoreTime = tmpTime;
                                }
                            });
                            table.reload("accessoryReturnAddTable",{
                                data:tmpData   // 将新数据重新载入表格
                            });
                            return false  //开启该代码可禁止点击该按钮关闭
                        }
                        , btn3: function () {
                            var tmpData = table.cache.accessoryReturnAddTable;
                            var tmpLocation;
                            $.each(tmpData,function(index1,value1){
                                if (value1.accessoryLocation != null && value1.accessoryLocation != ''){
                                    tmpLocation = value1.accessoryLocation;
                                }
                            });
                            $.each(tmpData,function(index1,value1){
                                if (value1.inStoreCount != null && value1.inStoreCount != ''){
                                    value1.accessoryLocation = tmpLocation;
                                }
                            });
                            table.reload("accessoryReturnAddTable",{
                                data:tmpData   // 将新数据重新载入表格
                            });
                            return false  //开启该代码可禁止点击该按钮关闭
                        }
                    });
                    layer.full(index);
                    setTimeout(function () {
                        $.ajax({
                            url: "/erp/getmanufactureaccessorydetailbychecknumber",
                            type: 'GET',
                            data: {
                                checkNumber: checkNumber
                            },
                            success: function (res) {
                                var title = [
                                    {field: 'clothesVersionNumber', title: '单号', align: 'center', minWidth: 100, sort: true, filter: true},
                                    {field: 'orderName', title: '款号', align: 'center', minWidth: 100, sort: true, filter: true},
                                    {field: 'accessoryName', title: '辅料名称', align: 'center', minWidth: 120, sort: true, filter: true},
                                    {field: 'accessoryNumber', title: '编号', align: 'center', minWidth: 80, sort: true, filter: true},
                                    {field: 'specification', title: '规格', align: 'center', minWidth: 80, sort: true, filter: true},
                                    {field: 'accessoryUnit', title: '单位', align: 'center', minWidth: 80, sort: true, filter: true},
                                    {field: 'accessoryColor', title: '辅料颜色', align: 'center', minWidth: 100, sort: true, filter: true},
                                    {field: 'sizeName', title: '尺码', align: 'center', minWidth: 100, sort: true, filter: true},
                                    {field: 'colorName', title: '颜色', align: 'center', minWidth: 100, sort: true, filter: true},
                                    {field: 'supplier', title: '供应商', align: 'center', minWidth: 80, sort: true, filter: true},
                                    {field: 'pieceUsage', title: '单件用量', align: 'center', minWidth: 90, sort: true, filter: true},
                                    {field: 'orderCount', title: '订单量', align: 'center', minWidth: 90, sort: true, filter: true, totalRow: true},
                                    {field: 'accessoryCount', title: '订购量', align: 'center', minWidth: 90, sort: true, filter: true, totalRow: true},
                                    {field: 'remark', title: '备注', align: 'center', minWidth: 60, sort: true, filter: true},
                                    {field: 'inStoreCount', title: '入库数量', align: 'center', minWidth: 90,edit:'text', sort: true, filter: true},
                                    {field: 'inStoreTime', title: '时间', align: 'center', minWidth: 100,edit:'text',event:'date', sort: true, filter: true},
                                    {field: 'accessoryLocation', title: '仓位',align:'center',minWidth:100,edit:'text', sort: true, filter: true},
                                    {field: 'colorNumber', title: '色号',align:'center',minWidth:100,edit:'text', sort: true, filter: true},
                                    {field: 'accessoryID', title: '', hide:true},
                                    {field: 'price', hide:true}
                                ];
                                var accessoryReturnData = [];

                                if (res.data){
                                    $.each(res.data,function(index1,value1){
                                        var thisAccessoryData = {};
                                        thisAccessoryData.accessoryID = value1.accessoryID;
                                        thisAccessoryData.orderName = value1.orderName;
                                        thisAccessoryData.clothesVersionNumber = value1.clothesVersionNumber;
                                        thisAccessoryData.accessoryName = value1.accessoryName;
                                        thisAccessoryData.accessoryNumber = value1.accessoryNumber;
                                        thisAccessoryData.specification = value1.specification;
                                        thisAccessoryData.accessoryUnit = value1.accessoryUnit;
                                        thisAccessoryData.accessoryColor = value1.accessoryColor;
                                        thisAccessoryData.pieceUsage = value1.pieceUsage;
                                        thisAccessoryData.orderCount = value1.orderCount;
                                        thisAccessoryData.sizeName = value1.sizeName;
                                        thisAccessoryData.colorName = value1.colorName;
                                        thisAccessoryData.accessoryCount = value1.accessoryCount;
                                        thisAccessoryData.supplier = value1.supplier;
                                        thisAccessoryData.remark = value1.remark;
                                        thisAccessoryData.price = value1.price;
                                        thisAccessoryData.inStoreCount = '';
                                        thisAccessoryData.inStoreTime = '';
                                        thisAccessoryData.accessoryLocation = '';
                                        thisAccessoryData.colorNumber = '';
                                        accessoryReturnData.push(thisAccessoryData);
                                    });
                                } else{
                                    layer.msg("未找到数据");
                                }
                                table.render({
                                    elem: '#accessoryReturnAddTable'
                                    , cols: [title]
                                    , data: accessoryReturnData
                                    , height: 'full-180'
                                    , even: true
                                    , overflow: 'tips'
                                    , totalRow: true
                                    , limit: Number.MAX_VALUE //每页默认显示的数量
                                    , done: function (res) {
                                        // $(".layui-table-main  tr").each(function (index, val) {
                                        //     $($(".layui-table-fixed .layui-table-body tbody tr")[index]).height($(val).height());
                                        // });
                                        soulTable.render(this);
                                    }
                                });
                                //监听单元格编辑
                                table.on('tool(accessoryReturnAddTable)', function (obj) {
                                    var data = obj.data;
                                    var tableData = table.cache.accessoryReturnAddTable;
                                    var that = this;
                                    var field = $(that).data('field');
                                    if(obj.event === 'date'){
                                        laydate.render({
                                            elem: $(this).find('.layui-table-edit').get(0)//
                                            ,show: true //直接显示
                                            ,done: function(value, date){
                                                obj.update({
                                                    [field] : value
                                                });
                                            }
                                        });
                                    }
                                });
                            },
                            error: function () {
                                layer.msg("获取辅料信息失败！", {icon: 2});
                            }
                        });
                        form.on('submit(fillPercent)', function(data){
                            var totalCount = Number($("#totalCount").val());
                            var accuracy = $("#accuracy").val();
                            if (totalCount == null || totalCount == ''){
                                layer.msg("请输入总数");
                                return false;
                            }
                            var tmpData = table.cache.accessoryReturnAddTable;
                            var totalOrder = 0;
                            $.each(tmpData,function(index1,value1){
                                totalOrder += Number(value1.accessoryCount);
                            });
                            $.each(tmpData,function(index1,value1){
                                if (accuracy == 0 || accuracy == '0'){
                                    value1.inStoreCount = Math.round(totalCount * Number(value1.accessoryCount)/Number(totalOrder));
                                } else {
                                    value1.inStoreCount = toDecimal(totalCount * Number(value1.accessoryCount)/Number(totalOrder));
                                }
                            });
                            table.reload("accessoryReturnAddTable",{
                                data:tmpData   // 将新数据重新载入表格
                            });
                            return false;
                        });
                        form.on('submit(fillTop)', function(data){
                            var totalCount = Number($("#totalCount").val());
                            // var accuracy = $("#accuracy").val();
                            if (totalCount == null || totalCount == ''){
                                layer.msg("请输入总数");
                                return false;
                            }
                            var tmpData = table.cache.accessoryReturnAddTable;
                            // var totalOrder = 0;
                            var thisCount = totalCount;
                            $.each(tmpData,function(index1,value1){
                                if (thisCount > 0){
                                    if (value1.accessoryCount >= thisCount){
                                        value1.inStoreCount = thisCount;
                                        thisCount = 0;
                                    } else {
                                        value1.inStoreCount = value1.accessoryCount;
                                        thisCount -= Number(value1.accessoryCount);
                                    }
                                }
                            });
                            table.reload("accessoryReturnAddTable",{
                                data:tmpData   // 将新数据重新载入表格
                            });
                            return false;
                        });
                    }, 100);
                }
                else {
                    var orderName = checkStatus.data[0].orderName;
                    var wellCount = checkStatus.data[0].wellCount;
                    var clothesVersionNumber = checkStatus.data[0].clothesVersionNumber;
                    var label = "辅料入库&emsp;&emsp;&emsp;&emsp;" + "单号：" + clothesVersionNumber + "&emsp;&emsp;&emsp;&emsp;款号：" + orderName + "&emsp;&emsp;&emsp;&emsp;好片数：" + wellCount;
                    var index = layer.open({
                        type: 1 //Page层类型
                        , title: label
                        , btn: ['保存','填充时间','填充位置']
                        , shade: 0.6 //遮罩透明度
                        , area: '1000px'
                        , maxmin: false //允许全屏最小化
                        , anim: 0 //0-6的动画形式，-1不开启
                        , content: "<div id=\"accessoryReturnAdd\">\n" +
                            "        <form class=\"layui-form\" style=\"margin: 0 auto;max-width:100%; min-height:400px; padding-top: 0px;\">\n" +
                            "            <table class=\"layui-hide\" id=\"accessoryReturnAddTable\" lay-filter=\"accessoryReturnAddTable\"></table>\n" +
                            "        </form>\n" +
                            "    </div>"
                        , yes: function (i, layero) {
                            var accessoryReturnData = table.cache.accessoryReturnAddTable;
                            var isMulti = true;
                            var isOver = true;
                            var accessoryDetails = [];
                            $.each(accessoryReturnData, function (i, item) {
                                var indexFlag = 0;
                                if (item.inStoreCount == '' && item.useCount == ''){
                                    indexFlag ++;
                                }
                                if (item.inStoreTime == ''){
                                    indexFlag ++;
                                }
                                if (item.accessoryLocation == ''){
                                    indexFlag ++;
                                }
                                if (indexFlag == 0){
                                    if (item.inStoreCount != null && item.useCount != null && item.inStoreCount != '' && item.useCount != ''){
                                        isMulti = false;
                                    }
                                    if (item.useCount != '' && Number(item.useCount) > Number(item.shareStorage)){
                                        isOver = false;
                                    }
                                    accessoryDetails.push({
                                        orderName:orderName,
                                        clothesVersionNumber:clothesVersionNumber,
                                        accessoryName:item.accessoryName,
                                        accessoryNumber:item.accessoryNumber,
                                        specification:item.specification,
                                        accessoryColor:item.accessoryColor,
                                        sizeName:item.sizeName,
                                        colorName:item.colorName,
                                        supplier:item.supplier,
                                        pieceUsage:item.pieceUsage,
                                        accessoryUnit:item.accessoryUnit,
                                        inStoreCount: item.inStoreCount === '' ? 0 : item.inStoreCount,
                                        useCount:item.useCount === '' ? 0 : item.useCount,
                                        storageCount:item.inStoreCount === '' ? item.useCount : item.inStoreCount,
                                        inStoreTime:item.inStoreTime,
                                        accessoryID:item.accessoryID,
                                        colorNumber:item.colorNumber,
                                        accessoryLocation:item.accessoryLocation,
                                        price:item.price,
                                    })
                                }
                            });
                            $.each(accessoryDetails, function (i, item) {
                                if (item.inStoreCount > 0){
                                    item.inStoreType = "分款";
                                } else {
                                    item.inStoreType = "共用";
                                }
                            });
                            if (accessoryDetails.length == 0) {
                                layer.msg("请填写完整信息", {icon: 2});
                            } else if (!isMulti) {
                                layer.msg("入库和调用不能同时进行~", {icon: 2});
                            } else if (!isOver) {
                                layer.msg("调用超数~", {icon: 2});
                            } else {
                                $.ajax({
                                    url: "/erp/accessoryinstorebatch",
                                    type: 'POST',
                                    data: {
                                        accessoryInStoreJson: JSON.stringify(accessoryDetails)
                                    },
                                    success: function (res) {
                                        if (res.result == 0) {
                                            initTable(orderNameSearch);
                                            layer.close(index);
                                            layer.msg("录入成功！", {icon: 1});
                                        } else if (res.result == 3) {
                                            layer.msg(res.msg);
                                        } else {
                                            layer.msg("录入失败！", {icon: 2});
                                        }
                                    },
                                    error: function () {
                                        layer.msg("录入失败！", {icon: 2});
                                    }
                                })
                            }
                        }
                        , btn2: function () {
                            var tmpData = table.cache.accessoryReturnAddTable;
                            var tmpTime;
                            $.each(tmpData,function(index1,value1){
                                if (value1.inStoreTime != null && value1.inStoreTime != ''){
                                    tmpTime = value1.inStoreTime;
                                }
                            });
                            $.each(tmpData,function(index1,value1){
                                if (value1.inStoreCount != null && value1.inStoreCount != ''){
                                    value1.inStoreTime = tmpTime;
                                }
                            });
                            table.reload("accessoryReturnAddTable",{
                                data:tmpData   // 将新数据重新载入表格
                            });
                            return false  //开启该代码可禁止点击该按钮关闭
                        }
                        , btn3: function () {
                            var tmpData = table.cache.accessoryReturnAddTable;
                            var tmpLocation;
                            $.each(tmpData,function(index1,value1){
                                if (value1.accessoryLocation != null && value1.accessoryLocation != ''){
                                    tmpLocation = value1.accessoryLocation;
                                }
                            });
                            $.each(tmpData,function(index1,value1){
                                if (value1.inStoreCount != null && value1.inStoreCount != ''){
                                    value1.accessoryLocation = tmpLocation;
                                }
                            });
                            table.reload("accessoryReturnAddTable",{
                                data:tmpData   // 将新数据重新载入表格
                            });
                            return false  //开启该代码可禁止点击该按钮关闭
                        }
                    });
                    layer.full(index);
                    setTimeout(function () {
                        $.ajax({
                            url: "/erp/getmanufactureaccessorybyorderforinstore",
                            type: 'GET',
                            data: {
                                orderName: orderName
                            },
                            success: function (res) {
                                var title = [
                                    {field: 'accessoryName', title: '辅料名称', align: 'center', minWidth: 150},
                                    {field: 'accessoryNumber', title: '编号', align: 'center', minWidth: 80},
                                    {field: 'specification', title: '规格', align: 'center', minWidth: 80},
                                    {field: 'accessoryUnit', title: '单位', align: 'center', minWidth: 80},
                                    {field: 'accessoryColor', title: '辅料颜色', align: 'center', minWidth: 100},
                                    {field: 'sizeName', title: '尺码', align: 'center', minWidth: 100},
                                    {field: 'colorName', title: '颜色', align: 'center', minWidth: 100},
                                    {field: 'supplier', title: '供应商', align: 'center', minWidth: 80},
                                    {field: 'pieceUsage', title: '单件用量', align: 'center', minWidth: 90},
                                    {field: 'orderCount', title: '订单量', align: 'center', minWidth: 90},
                                    {field: 'accessoryCount', title: '订购量', align: 'center', minWidth: 90, totalRow: true},
                                    {field: 'checkState', title: '审核', align: 'center', minWidth: 90},
                                    {field: 'remark', title: '备注', align: 'center', minWidth: 60},
                                    {field: 'shareStorage', title: '共用库存', align: 'center', minWidth: 90},
                                    {field: 'useCount', title: '调用量', align: 'center', minWidth: 90,edit:'text'},
                                    {field: 'inStoreCount', title: '入库数量', align: 'center', minWidth: 90,edit:'text'},
                                    {field: 'inStoreTime', title: '时间', align: 'center', minWidth: 100,edit:'text',event:'date'},
                                    {field: 'accessoryLocation', title: '仓位',align:'center',minWidth:100,edit:'text'},
                                    {field: 'colorNumber', title: '色号',align:'center',minWidth:100,edit:'text'},
                                    {field: 'accessoryID', hide:true},
                                    {field: 'price', hide:true}
                                ];
                                var accessoryReturnData = [];

                                if (res.data){
                                    $.each(res.data,function(index1,value1){
                                        var thisAccessoryData = {};
                                        thisAccessoryData.accessoryID = value1.accessoryID;
                                        thisAccessoryData.accessoryName = value1.accessoryName;
                                        thisAccessoryData.accessoryNumber = value1.accessoryNumber;
                                        thisAccessoryData.specification = value1.specification;
                                        thisAccessoryData.accessoryUnit = value1.accessoryUnit;
                                        thisAccessoryData.accessoryColor = value1.accessoryColor;
                                        thisAccessoryData.pieceUsage = value1.pieceUsage;
                                        thisAccessoryData.orderCount = value1.orderCount;
                                        thisAccessoryData.sizeName = value1.sizeName;
                                        thisAccessoryData.colorName = value1.colorName;
                                        thisAccessoryData.accessoryCount = value1.accessoryCount;
                                        thisAccessoryData.supplier = value1.supplier;
                                        thisAccessoryData.remark = value1.remark;
                                        thisAccessoryData.checkState = value1.checkState;
                                        thisAccessoryData.shareStorage = value1.shareStorage;
                                        thisAccessoryData.price = value1.price;
                                        thisAccessoryData.inStoreCount = '';
                                        thisAccessoryData.inStoreTime = '';
                                        thisAccessoryData.accessoryLocation = '';
                                        thisAccessoryData.colorNumber = '';
                                        accessoryReturnData.push(thisAccessoryData);
                                    });
                                } else{
                                    layer.msg("未找到数据");
                                }
                                table.render({
                                    elem: '#accessoryReturnAddTable'
                                    , cols: [title]
                                    , data: accessoryReturnData
                                    , height: 'full-180'
                                    , even: true
                                    , overflow: 'tips'
                                    , totalRow: true
                                    , limit: Number.MAX_VALUE //每页默认显示的数量
                                    , done: function (res) {
                                        $(".layui-table-main  tr").each(function (index, val) {
                                            $($(".layui-table-fixed .layui-table-body tbody tr")[index]).height($(val).height());
                                        });
                                        soulTable.render(this);
                                    }
                                });
                                //监听单元格编辑
                                table.on('tool(accessoryReturnAddTable)', function (obj) {
                                    var data = obj.data;
                                    var tableData = table.cache.accessoryReturnAddTable;
                                    var that = this;
                                    var field = $(that).data('field');
                                    if(obj.event === 'date'){
                                        laydate.render({
                                            elem: $(this).find('.layui-table-edit').get(0)//
                                            ,show: true //直接显示
                                            ,done: function(value, date){
                                                obj.update({
                                                    [field] : value
                                                });
                                            }
                                        });
                                    }else if(obj.event === 'del'){
                                        obj.del();
                                        $.each(tableData,function (index,item) {
                                            if(item instanceof Array) {
                                                tableData.splice(index,1)
                                            }
                                        });
                                        table.reload("accessoryReturnAddTable",{
                                            data:tableData   // 将新数据重新载入表格
                                        })
                                    } else if(obj.event === 'add'){
                                        tableData.push({
                                            "accessoryName": tableData[0].accessoryName
                                            ,"specification": tableData[0].specification
                                            ,"accessoryUnit": tableData[0].accessoryUnit
                                            ,"accessoryColor": tableData[0].accessoryColor
                                            ,"sizeName": tableData[0].sizeName
                                            ,"colorName": tableData[0].colorName
                                            ,"accessoryType": tableData[0].accessoryType
                                            ,"supplier": tableData[0].supplier
                                            ,"accessoryCount": tableData[0].accessoryCount
                                            ,"inStoreCount": ''
                                            ,"inStoreTime": ''
                                            ,"accessoryLocation": ''
                                            ,"colorNumber": ''

                                        });
                                        table.reload("accessoryReturnAddTable",{
                                            data:tableData   // 将新数据重新载入表格
                                        })
                                    }
                                    layui.form.render("select");
                                });
                            },
                            error: function () {
                                layer.msg("获取辅料信息失败！", {icon: 2});
                            }
                        })
                    }, 100);
                }
            }
        }
        else if (obj.event === "accessoryOutStore") {   //面料下单
            var checkStatus = table.checkStatus(obj.config.id); //获取选中行状态
            if (checkStatus.data.length == 0) {
                layer.msg('请先选择款号');
            } else {
                var orderName = checkStatus.data[0].orderName;
                var wellCount = checkStatus.data[0].wellCount;
                var clothesVersionNumber = checkStatus.data[0].clothesVersionNumber;
                var label = "辅料出库&emsp;&emsp;&emsp;&emsp;" + "单号：" + clothesVersionNumber + "&emsp;&emsp;&emsp;&emsp;款号：" + orderName + "&emsp;&emsp;&emsp;&emsp;好片数：" + wellCount;
                var index = layer.open({
                    type: 1 //Page层类型
                    , title: label
                    , btn: ['保存','填充单位']
                    , shade: 0.6 //遮罩透明度
                    , area: '1000px'
                    , maxmin: false //允许全屏最小化
                    , anim: 0 //0-6的动画形式，-1不开启
                    , content: "<div id=\"accessoryOutStore\">\n" +
                        "        <form class=\"layui-form\" style=\"margin: 0 auto;max-width:100%; min-height:400px; padding-top: 5px;\">\n" +
                        "            <table class=\"layui-hide\" id=\"accessoryOutStoreTable\" lay-filter=\"accessoryOutStoreTable\"></table>\n" +
                        "        </form>\n" +
                        "    </div>"
                    , yes: function (i, layero) {
                        var accessoryOutStoreData = table.cache.accessoryOutStoreTable;
                        var isInput = false;
                        var checkCount  = false;
                        var accessoryOutStoreJson = [];
                        $.each(accessoryOutStoreData, function (i, item) {
                            if (item.outStoreCount != '' && item.outStoreCount != 0){
                                isInput  = true;
                                accessoryOutStoreJson.push({
                                    orderName:orderName,
                                    clothesVersionNumber:clothesVersionNumber,
                                    id:item.id,
                                    accessoryID:item.accessoryID,
                                    outStoreCount:item.outStoreCount,
                                    accessoryName : item.accessoryName,
                                    accessoryNumber : item.accessoryNumber,
                                    specification : item.specification,
                                    accessoryUnit : item.accessoryUnit,
                                    accessoryColor : item.accessoryColor,
                                    supplier : item.supplier,
                                    pieceUsage : item.pieceUsage,
                                    sizeName : item.sizeName,
                                    colorName : item.colorName,
                                    storageCount : item.storageCount,
                                    receiver : item.receiver,
                                    accessoryLocation : item.accessoryLocation
                                })
                            }
                            if (item.outStoreCount > item.storageCount){
                                checkCount = true;
                            }
                        });
                        if (checkCount) {
                            layer.msg("出库数量大于库存数量", {icon: 2});
                        } else if(!isInput){
                            layer.msg("出库数量为空", {icon: 2});
                        } else {
                            $.ajax({
                                url: "/erp/accessoryoutstorebatch",
                                type: 'POST',
                                data: {
                                    accessoryOutStoreJson: JSON.stringify(accessoryOutStoreJson)
                                },
                                success: function (res) {
                                    if (res.result == 0) {
                                        initTable('');
                                        layer.close(index);
                                        layer.msg("出库成功！", {icon: 1});
                                    } else {
                                        layer.msg("出库失败！", {icon: 2});
                                    }
                                }, error: function () {
                                    layer.msg("出库失败！", {icon: 2});
                                }
                            })
                        }
                    }
                    ,btn2: function(i, layero){
                        var tmpData = table.cache.accessoryOutStoreTable;
                        var receiver = '';
                        $.each(tmpData,function(index1,value1){
                            if (value1.receiver != null && value1.receiver != ''){
                                receiver = value1.receiver;
                            }
                        });
                        $.each(tmpData,function(index1,value1){
                            if (value1.receiver == null || value1.receiver == ''){
                                value1.receiver = receiver;
                            }
                        });
                        table.reload("accessoryOutStoreTable",{
                            data:tmpData   // 将新数据重新载入表格
                        });
                        return false  //开启该代码可禁止点击该按钮关闭
                    }
                });
                layer.full(index);
                setTimeout(function () {
                    $.ajax({
                        url: "/erp/getaccessorystoragebyinfo",
                        type: 'GET',
                        data: {
                            orderName: orderName
                        },
                        success: function (res) {
                            var title = [
                                {field: 'id', align: 'center', hide:true},
                                {field: 'accessoryID', align: 'center', hide:true},
                                {field: 'accessoryName', title: '辅料名称', align: 'center', minWidth: 150},
                                {field: 'accessoryNumber', title: '编号', align: 'center', minWidth: 100},
                                {field: 'specification', title: '规格', align: 'center', minWidth: 100},
                                {field: 'accessoryUnit', title: '辅料单位', align: 'center', minWidth: 100},
                                {field: 'accessoryColor', title: '辅料颜色', align: 'center', minWidth: 100},
                                {field: 'sizeName', title: '尺码', align: 'center', minWidth: 70},
                                {field: 'colorName', title: '颜色', align: 'center', minWidth: 80},
                                {field: 'supplier', title: '供应商', align: 'center', minWidth: 80},
                                {field: 'pieceUsage', title: '单件用量', align: 'center', minWidth: 80},
                                {field: 'storageCount', title: '库存量', align: 'center', minWidth: 100},
                                {field: 'outStoreCount', title: '出库数量', align: 'center', minWidth: 120,edit:'text'},
                                {field: 'receiver', title: '接收', align: 'center', minWidth: 100,edit:'text'},
                                {field: 'accessoryLocation', title: '仓位',align:'center',minWidth:200}
                            ];
                            var accessoryOutStoreData = [];
                            if (res.data){
                                $.each(res.data,function(index1,value1){
                                    var thisAccessoryData = {};
                                    thisAccessoryData.id = value1.id;
                                    thisAccessoryData.accessoryID = value1.accessoryID;
                                    thisAccessoryData.accessoryName = value1.accessoryName;
                                    thisAccessoryData.accessoryNumber = value1.accessoryNumber;
                                    thisAccessoryData.specification = value1.specification;
                                    thisAccessoryData.accessoryUnit = value1.accessoryUnit;
                                    thisAccessoryData.accessoryColor = value1.accessoryColor;
                                    thisAccessoryData.supplier = value1.supplier;
                                    thisAccessoryData.pieceUsage = value1.pieceUsage;
                                    thisAccessoryData.sizeName = value1.sizeName;
                                    thisAccessoryData.colorName = value1.colorName;
                                    thisAccessoryData.storageCount = value1.storageCount;
                                    thisAccessoryData.accessoryLocation = value1.accessoryLocation;
                                    thisAccessoryData.outStoreCount = 0;
                                    accessoryOutStoreData.push(thisAccessoryData);
                                });
                            } else{
                                layer.msg("未找到数据");
                            }
                            table.render({
                                elem: '#accessoryOutStoreTable'
                                , cols: [title]
                                , data: accessoryOutStoreData
                                , height: 'full-100'
                                , limit: 10000
                            });
                        }, error: function () {
                            layer.msg("获取辅料库存失败！", {icon: 2});
                        }
                    })
                }, 100);
            }
        }
    })
});


function toDecimal(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*100)/100;
    return f;
}

function isNumber(val){

    var regPos = /^\d+(\.\d+)?$/; //非负浮点数
    var regNeg = /^(-(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*)))$/; //负浮点数
    if (regPos.test(val) || regNeg.test(val)){
        return true;
    }else{
        return false;
    }

}