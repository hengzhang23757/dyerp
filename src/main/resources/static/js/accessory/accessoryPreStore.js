layui.use('form', function(){
    var form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
    form.render();
});
layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.22'
}).extend({                         // 模块别名
    soulTable: 'soulTable'
});
var pageWidth = 1500;
var dataTable,supplierDemo;
$(document).on('keydown', '.layui-table-edit', function (e) {
    var td = $(this).parent('td')
        ,tr = td.parent('tr')
        ,trs = tr.parent().parent().find('tr')
        ,tr_index = tr.index()
        ,td_index = td.index()
        ,td_last_index = tr.find('[data-edit="text"]:last').index()
        ,td_first_index = tr.find('[data-edit="text"]:first').index();
    switch (e.keyCode) {
        case 13:
            tr.next().find('td').eq(td_index).click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            return false;
        case 39:
            td.nextAll('[data-edit="text"]:first').click();
            if (td_index == td_last_index){
                tr.next().find('td').eq(td_first_index).click();
                if (tr_index == trs.length - 1){
                    trs.eq(0).find('td').eq(td_first_index).click();
                }
            }
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
        case 37:
            td.prevAll('[data-edit="text"]:first').click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
        case 38:
            tr.prev().find('td').eq(td_index).click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
        case 40:
            tr.next().find('td').eq(td_index).click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
    }

})
$(document).ready(function () {

    pageWidth = $(document.body).width();

});
layui.use(['form', 'soulTable', 'table', 'laydate','yutons_sug'], function () {

    var table = layui.table,
        soulTable = layui.soulTable,
        laydate = layui.laydate,
        form = layui.form,
        $ = layui.$;
    var load = layer.load();

    dataTable = table.render({
        elem: '#dataTable'
        ,excel: {
            filename: '共用辅料.xlsx'
        }
        ,height: 'full-100'
        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
        ,cols: [[]]
        ,loading:true
        ,totalRow: true
        ,page: true
        ,even: true
        ,limits: [50, 100, 200]
        ,limit: 100 //每页默认显示的数量
        ,done: function (res, curr, count) {
            soulTable.render(this);
        }
    });

    initTable('', '');

    function initTable(accessoryName, accessoryNumber){
        var load = layer.load();
        var param = {};
        param.operateType = 'index';
        if (accessoryName != null && accessoryName != ''){
            param.accessoryName = accessoryName;
        }
        if (accessoryNumber != null && accessoryNumber != ''){
            param.accessoryNumber = accessoryNumber;
        }
        $.ajax({
            url: "/erp/getaccessoryprestorageindex",
            type: 'GET',
            data: param,
            success: function (res) {
                if (res.data) {
                    var reportData = res.data;
                    table.render({
                        elem: '#dataTable'
                        ,cols:[[
                            {title: '#', width: 50, collapse: true,lazy: true, children:[
                                    {
                                        title: '采购记录'
                                        ,url: 'getaccessoryprestorebytypepreid'
                                        ,where: function(row){
                                            return {
                                                preStoreId: row.id,
                                                operateType: "order"
                                            }
                                        }
                                        ,loading:true
                                        ,page: false
                                        ,limit: Number.MAX_VALUE //每页默认显示的数量
                                        ,totalRow: true
                                        ,height:300
                                        ,cols: [[
                                            {field: 'id', title: '操作', minWidth: 180, align:'center', templet: function(row) {
                                                    return '<a class="layui-btn layui-btn-xs" lay-event="inStore">入库</a><a class="layui-btn layui-btn-xs layui-btn-normal" lay-event="update">修改</a><a class="layui-btn layui-btn-xs layui-btn-danger" lay-event="delete">删除</a>'
                                             }},
                                            {field: 'accessoryName', title: '辅料名称',align:'center', minWidth: 100, totalRowText: '合计', filter: true},
                                            {field: 'accessoryNumber', title: '辅料编号',align:'center', minWidth: 100, filter: true},
                                            {field: 'supplier', title: '供应商',align:'center', filter: true},
                                            {field: 'specification', title: '辅料规格',align:'center', minWidth: 100 , filter: true},
                                            {field: 'accessoryUnit', title: '辅料单位',align:'center', minWidth: 100 , filter: true},
                                            {field: 'accessoryColor', title: '辅料颜色',align:'center', minWidth: 100 , filter: true},
                                            {field: 'price', title: '单价',align:'center', minWidth: 100 , filter: true},
                                            {field: 'accessoryCount', title: '采购数量',align:'center', minWidth: 100 , filter: true},
                                            {field: 'checkState', title: '审核状态',align:'center', minWidth: 100, filter: true},
                                            {field: 'checkNumber', title: '合同号',align:'center', minWidth: 100, filter: true},
                                            {field: 'preStoreId', hide: true}
                                        ]]
                                        ,done: function () {
                                            soulTable.render(this);
                                        }
                                        ,toolEvent: function (obj, pobj) {
                                            // obj 子表当前行对象
                                            // pobj 父表当前行对象
                                            var objData = obj.data;
                                            var checkNumber = objData.checkNumber;
                                            var preStoreId = objData.preStoreId;
                                            var childId = this.id; // 通过 this 对象获取当前子表的id
                                            if (obj.event === 'delete'){
                                                if (objData.checkState === '通过'){
                                                    layer.msg("已审核,无法删除~~~");
                                                    return false;
                                                }
                                                layer.confirm('真的删除吗', function(index){
                                                    var id = objData.id;
                                                    $.ajax({
                                                        url: "/erp/deleteaccessoryprestorebyid",
                                                        type: 'POST',
                                                        data: {
                                                            id:id
                                                        },
                                                        success: function (res) {
                                                            if (res.result == 0) {
                                                                layer.close(index);
                                                                layer.msg("删除成功！", {icon: 1});
                                                                table.reload(childId);
                                                            } else {
                                                                layer.msg("删除失败！", {icon: 2});
                                                            }
                                                        },
                                                        error: function () {
                                                            layer.msg("删除失败！", {icon: 2});
                                                        }
                                                    })
                                                });
                                            }
                                            else if (obj.event === 'update'){
                                                if (objData.checkState === '通过'){
                                                    layer.msg("已审核通过,无法修改~~~");
                                                    return false;
                                                }
                                                // var id = data.id;
                                                var index = layer.open({
                                                    type: 1 //Page层类型
                                                    , title: '修改'
                                                    , btn: ['保存']
                                                    , shade: 0.6 //遮罩透明度
                                                    , area: ['500px', '300px']
                                                    , maxmin: false //允许全屏最小化
                                                    , anim: 0 //0-6的动画形式，-1不开启
                                                    , content: "<div><table><tr>" +
                                                        "       <td style=\"text-align: right;margin-bottom: 15px;\">\n" +
                                                        "           <label class=\"layui-form-label\" style=\"width: 100px\">数量</label>\n" +
                                                        "       </td>\n" +
                                                        "       <td style=\"margin-bottom: 15px;\">\n" +
                                                        "           <input type=\"text\" id=\"accessoryCount\" autocomplete=\"off\" class=\"layui-input\">\n" +
                                                        "       </td></tr><tr>\n" +
                                                        "       <td style=\"text-align: right;margin-bottom: 15px;\">\n" +
                                                        "           <label class=\"layui-form-label\" style=\"width: 100px\">单价</label>\n" +
                                                        "       </td>\n" +
                                                        "       <td style=\"margin-bottom: 15px;\">\n" +
                                                        "           <input type=\"text\" id=\"price\" autocomplete=\"off\" class=\"layui-input\">\n" +
                                                        "       </td>" +
                                                        "</tr></table></div>"
                                                    ,yes: function(i, layero){
                                                        var param = {};
                                                        param.id = objData.id;
                                                        param.accessoryCount = $("#accessoryCount").val();
                                                        param.price = $("#price").val();
                                                        $.ajax({
                                                            url: "/erp/updateaccessoryprestoreaftercommit",
                                                            type: 'POST',
                                                            data: param,
                                                            success: function (res) {
                                                                if (res.result == 0) {
                                                                    layer.close(index);
                                                                    layer.msg("修改成功！", {icon: 1});
                                                                    table.reload(childId);
                                                                } else {
                                                                    layer.msg("修改失败！", {icon: 2});
                                                                }
                                                            },
                                                            error: function () {
                                                                layer.msg("修改失败！", {icon: 2});
                                                            }
                                                        });
                                                        return false;
                                                    }
                                                    , cancel : function (i,layero) {
                                                        $("#accessoryCount").val('');
                                                        $("#price").val('');
                                                    }
                                                });
                                                $("#accessoryCount").val(objData.accessoryCount);
                                                $("#price").val(objData.price);
                                            }
                                            else if (obj.event === 'inStore'){
                                                if (objData.checkState != '通过'){
                                                    layer.msg("未审核通过,无法入库~~~");
                                                    return false;
                                                }
                                                var index = layer.open({
                                                    type: 1 //Page层类型
                                                    , title: '下单'
                                                    , btn: ['入库']
                                                    , shade: 0.6 //遮罩透明度
                                                    , area: ['500px', '300px']
                                                    , maxmin: false //允许全屏最小化
                                                    , anim: 0 //0-6的动画形式，-1不开启
                                                    , content: "<div><table><tr>" +
                                                        "       <td style=\"text-align: right;margin-bottom: 15px;\">\n" +
                                                        "           <label class=\"layui-form-label\" style=\"width: 100px\">数量</label>\n" +
                                                        "       </td>\n" +
                                                        "       <td style=\"margin-bottom: 15px;\">\n" +
                                                        "           <input type=\"text\" id=\"inStoreCount\" autocomplete=\"off\" class=\"layui-input\">\n" +
                                                        "       </td>" +
                                                        "</tr>" +
                                                        "<tr>\n" +
                                                        "       <td style=\"text-align: right;margin-bottom: 15px;\">\n" +
                                                        "           <label class=\"layui-form-label\" style=\"width: 100px\">位置</label>\n" +
                                                        "       </td>\n" +
                                                        "       <td style=\"margin-bottom: 15px;\">\n" +
                                                        "           <input type=\"text\" id=\"accessoryLocation\" autocomplete=\"off\" class=\"layui-input\">\n" +
                                                        "       </td>" +
                                                        "</tr>" +
                                                        "<tr>\n" +
                                                        "       <td style=\"text-align: right;margin-bottom: 15px;\">\n" +
                                                        "           <label class=\"layui-form-label\" style=\"width: 100px\">日期</label>\n" +
                                                        "       </td>\n" +
                                                        "       <td style=\"margin-bottom: 15px;\">\n" +
                                                        "           <input type=\"text\" id=\"inStoreDate\" autocomplete=\"off\" class=\"layui-input\">\n" +
                                                        "       </td>" +
                                                        "</tr>" +
                                                        "</table></div>"
                                                    ,yes: function(i, layero){
                                                        var param = {};
                                                        param.preStoreId = preStoreId;
                                                        param.checkNumber = checkNumber;
                                                        param.inStoreCount = $("#inStoreCount").val();
                                                        param.accessoryLocation = $("#accessoryLocation").val();
                                                        param.inStoreDate = $("#inStoreDate").val();
                                                        if (param.inStoreDate == null || param.inStoreDate === ''){
                                                            layer.msg("日期必须~~~");
                                                            return false;
                                                        }
                                                        $.ajax({
                                                            url: "/erp/acessoryprestoreinstore",
                                                            type: 'POST',
                                                            data: param,
                                                            success: function (res) {
                                                                if (res.result == 0) {
                                                                    table.reload(childId);
                                                                    layer.msg("入库成功", {icon: 1});
                                                                    layer.close(index);
                                                                    $("#inStoreCount").val('');
                                                                    $("#accessoryLocation").val('');
                                                                } else {
                                                                    layer.msg("入库失败！", {icon: 2});
                                                                }
                                                            },
                                                            error: function () {
                                                                layer.msg("入库失败！", {icon: 2});
                                                            }
                                                        });
                                                        return false;
                                                    }
                                                    , cancel : function (i,layero) {
                                                        $("#inStoreCount").val('');
                                                        $("#accessoryLocation").val('');
                                                        $("#inStoreDate").val('');
                                                    }
                                                });
                                                layui.laydate.render({
                                                    elem: '#inStoreDate',
                                                    trigger: 'click',
                                                    value: new Date()
                                                });
                                            }
                                        }
                                    },{
                                        title: '入库记录'
                                        ,url: 'getaccessoryprestorebytypepreid'
                                        ,where: function(row){
                                            return {
                                                preStoreId: row.id,
                                                operateType: "in"
                                            }
                                        }
                                        ,loading:true
                                        ,page: false
                                        ,limit: Number.MAX_VALUE //每页默认显示的数量
                                        ,totalRow: true
                                        ,height:300
                                        ,cols: [[
                                            {field: 'id', title: '操作', minWidth: 120, align:'center', templet: function(row) {
                                                    return '<a class="layui-btn layui-btn-xs layui-btn-danger" lay-event="delete">删除</a>'
                                                }},
                                            {field: 'accessoryName', title: '辅料名称',align:'center', minWidth: 100, totalRowText: '合计', filter: true},
                                            {field: 'accessoryNumber', title: '辅料编号',align:'center', minWidth: 100, filter: true},
                                            {field: 'supplier', title: '供应商',align:'center', filter: true},
                                            {field: 'specification', title: '辅料规格',align:'center', minWidth: 100 , filter: true},
                                            {field: 'accessoryUnit', title: '辅料单位',align:'center', minWidth: 100 , filter: true},
                                            {field: 'accessoryColor', title: '辅料颜色',align:'center', minWidth: 100 , filter: true},
                                            {field: 'inStoreCount', title: '入库数量',align:'center', minWidth: 100, totalRow: true, filter: true},
                                            {field: 'createTime', title: '入库时间',align:'center', minWidth: 120,templet:function (d) {
                                                    return moment(d.createTime).format("YYYY-MM-DD");
                                                }, filter: true},
                                            {field: 'accessoryLocation', title: '位置',align:'center', minWidth: 100, filter: true},
                                            {field: 'checkNumber', title: '合同号',align:'center', minWidth: 100, filter: true},
                                            {field: 'preStoreId', hide: true}
                                        ]]
                                        ,done: function () {
                                            soulTable.render(this);
                                        }
                                        ,toolEvent: function (obj, pobj) {
                                            // obj 子表当前行对象
                                            // pobj 父表当前行对象
                                            var objData = obj.data;
                                            var childId = this.id; // 通过 this 对象获取当前子表的id
                                            if (obj.event === 'delete'){
                                                layer.confirm('真的删除吗', function(index){
                                                    var id = objData.id;
                                                    var checkNumber = objData.checkNumber;
                                                    var preStoreId = objData.preStoreId;
                                                    $.ajax({
                                                        url: "/erp/deleteaccessoryprestoreinbyid",
                                                        type: 'POST',
                                                        data: {
                                                            id:id,
                                                            preStoreId: preStoreId
                                                        },
                                                        success: function (res) {
                                                            if (res.result == 0) {
                                                                layer.close(index);
                                                                layer.msg("删除成功！", {icon: 1});
                                                                table.reload(childId);
                                                            } else if (res.result == 4) {
                                                                layer.msg("库存不足,无法删除！", {icon: 2});
                                                            } else {
                                                                layer.msg("删除失败！", {icon: 2});
                                                            }
                                                        },
                                                        error: function () {
                                                            layer.msg("删除失败！", {icon: 2});
                                                        }
                                                    })
                                                });
                                            }
                                        }
                                    },{
                                        title: '库存记录'
                                        ,url: 'getaccessoryprestorebytypepreid'
                                        ,where: function(row){
                                            return {
                                                preStoreId: row.id,
                                                operateType: "storage"
                                            }
                                        }
                                        ,loading:true
                                        ,page: false
                                        ,height:300
                                        ,limit: Number.MAX_VALUE //每页默认显示的数量
                                        ,totalRow: true
                                        ,cols: [[
                                            {field: 'id',align:'center', hide:true},
                                            {field: 'accessoryName', title: '辅料名称',align:'center', minWidth: 100, totalRowText: '合计', filter: true},
                                            {field: 'accessoryNumber', title: '辅料编号',align:'center', minWidth: 100, filter: true},
                                            {field: 'supplier', title: '供应商',align:'center', filter: true},
                                            {field: 'specification', title: '辅料规格',align:'center', minWidth: 100 , filter: true},
                                            {field: 'accessoryUnit', title: '辅料单位',align:'center', minWidth: 100 , filter: true},
                                            {field: 'accessoryColor', title: '辅料颜色',align:'center', minWidth: 100 , filter: true},
                                            {field: 'storageCount', title: '库存数量',align:'center', minWidth: 100, totalRow: true, filter: true},
                                            {field: 'accessoryLocation', title: '位置',align:'center', minWidth: 100, filter: true}
                                        ]]
                                        ,done: function () {
                                            soulTable.render(this);
                                        }
                                        ,toolEvent: function (obj, pobj) {
                                            // obj 子表当前行对象
                                            // pobj 父表当前行对象
                                            var objData = obj.data;
                                            var childId = this.id; // 通过 this 对象获取当前子表的id
                                            if (obj.event === 'outStoreSingle') {
                                                var index = layer.open({
                                                    type: 1 //Page层类型
                                                    , title: '辅料出库'
                                                    , btn: ['保存']
                                                    , shade: 0.6 //遮罩透明度
                                                    ,area: ['420px', '300px'] //宽高
                                                    , anim: 0 //0-6的动画形式，-1不开启
                                                    , content: '<div style="padding-top: 30px">\n' +
                                                        '                    <table>' +
                                                        '                        <tr>' +
                                                        '                            <td style="text-align: right;margin-bottom: 15px;width: 100px">\n' +
                                                        '                                 <label class="layui-form-label" style="100px">库存</label>\n' +
                                                        '                            </td>\n' +
                                                        '                            <td style="margin-bottom: 15px;">\n' +
                                                        '                                 <input type="text" style="width: 260px;" id="storageCount" readonly class="layui-input">\n' +
                                                        '                            </td>' +
                                                        '                        </tr>' +
                                                        '                        <tr>' +
                                                        '                            <td style="text-align: right;margin-bottom: 15px;width: 100px">\n' +
                                                        '                                 <label class="layui-form-label" style="100px">出库</label>\n' +
                                                        '                            </td>\n' +
                                                        '                            <td style="margin-bottom: 15px;">\n' +
                                                        '                                 <input type="text" style="width: 260px;" id="outStoreCount" class="layui-input">\n' +
                                                        '                            </td>' +
                                                        '                        </tr>' +
                                                        '                        <tr>' +
                                                        '                            <td style="text-align: right;margin-bottom: 15px;width: 100px">\n' +
                                                        '                                 <label class="layui-form-label" style="100px">备注</label>\n' +
                                                        '                            </td>\n' +
                                                        '                            <td style="margin-bottom: 15px;">\n' +
                                                        '                                 <input type="text" name="remark" style="width: 260px;" id="remark" value="无" class="layui-input">\n' +
                                                        '                            </td>' +
                                                        '                        </tr>' +
                                                        '                    </table>' +
                                                        '                </div>'
                                                    ,yes: function(index, layero){
                                                        var storageCount = $("#storageCount").val();
                                                        var outStoreCount = $("#outStoreCount").val();
                                                        var remark = $("#remark").val();
                                                        if (outStoreCount == 0 || outStoreCount == "" || (Number(outStoreCount) > Number(storageCount))){
                                                            layer.msg("数量有误！", {icon: 2});
                                                            return false;
                                                        }
                                                        var accessoryOutStoreJson = [];
                                                        accessoryOutStoreJson.push({
                                                            orderName:objData.orderName,
                                                            clothesVersionNumber:objData.clothesVersionNumber,
                                                            id:objData.id,
                                                            accessoryID:objData.accessoryID,
                                                            outStoreCount:outStoreCount,
                                                            accessoryName : objData.accessoryName,
                                                            accessoryNumber : objData.accessoryNumber,
                                                            specification : objData.specification,
                                                            accessoryUnit : objData.accessoryUnit,
                                                            accessoryColor : objData.accessoryColor,
                                                            supplier : objData.supplier,
                                                            pieceUsage : objData.pieceUsage,
                                                            sizeName : objData.sizeName,
                                                            colorName : objData.colorName,
                                                            storageCount : objData.storageCount,
                                                            accessoryLocation : objData.accessoryLocation,
                                                            remark: remark
                                                        });
                                                        $.ajax({
                                                            url: "/erp/accessoryoutstorebatch",
                                                            type: 'POST',
                                                            data: {
                                                                accessoryOutStoreJson: JSON.stringify(accessoryOutStoreJson)
                                                            },
                                                            success: function (res) {
                                                                if (res.result == 0) {
                                                                    table.reload(childId);
                                                                    layer.close(index);
                                                                    layer.msg("出库成功！", {icon: 1});
                                                                } else {
                                                                    layer.msg("出库失败！", {icon: 2});
                                                                }
                                                            }, error: function () {
                                                                layer.msg("出库失败！", {icon: 2});
                                                            }
                                                        })
                                                    }, cancel : function (i,layero) {
                                                        $("#storageCount").val('');
                                                        $("#outStoreCount").val('');
                                                        $("#remark").val('');
                                                    }
                                                });
                                                setTimeout(function () {
                                                    $("#storageCount").val(objData.storageCount);
                                                }, 100);
                                            }
                                        }
                                    },{
                                        title: '出库记录'
                                        ,url: 'getaccessoryprestorebytypepreid'
                                        ,where: function(row){
                                            return {
                                                preStoreId: row.id,
                                                operateType: "out"
                                            }
                                        }
                                        ,loading:true
                                        ,page: false
                                        ,limit: Number.MAX_VALUE //每页默认显示的数量
                                        ,totalRow: true
                                        ,height:300
                                        ,cols: [[
                                            {field: 'id',align:'center', hide:true},
                                            {field: 'accessoryName', title: '辅料名称',align:'center', minWidth: 100, totalRowText: '合计', filter: true},
                                            {field: 'accessoryNumber', title: '辅料编号',align:'center', minWidth: 100, filter: true},
                                            {field: 'supplier', title: '供应商',align:'center', filter: true},
                                            {field: 'specification', title: '辅料规格',align:'center', minWidth: 100 , filter: true},
                                            {field: 'accessoryUnit', title: '辅料单位',align:'center', minWidth: 100 , filter: true},
                                            {field: 'accessoryColor', title: '辅料颜色',align:'center', minWidth: 100 , filter: true},
                                            {field: 'outStoreCount', title: '出库数量',align:'center', minWidth: 100, totalRow: true, filter: true},
                                            {field: 'orderName', title: '款号',align:'center', minWidth: 100, totalRow: true, filter: true},
                                            {field: 'clothesVersionNumber', title: '单号',align:'center', minWidth: 100, totalRow: true, filter: true},
                                            {field: 'createTime', title: '出库时间',align:'center', minWidth: 120,templet:function (d) {
                                                    return moment(d.createTime).format("YYYY-MM-DD");
                                                }, filter: true}
                                        ]]
                                        ,done: function () {
                                            soulTable.render(this);
                                        }
                                    }]}
                            ,{type:'radio'}
                            ,{type:'numbers', align:'center', title:'序号'}
                            ,{field:'accessoryName', title:'名称', align:'center', minWidth:150, sort: true, filter: true}
                            ,{field:'accessoryNumber', title:'编号', align:'center', minWidth:150, sort: true, filter: true, totalRowText: '合计'}
                            ,{field:'specification', title:'规格', align:'center', minWidth:100, sort: true, filter: true}
                            ,{field:'accessoryColor', title:'颜色', align:'center', minWidth:100, sort: true, filter: true}
                            ,{field:'accessoryUnit', title:'单位', align:'center', minWidth:80, sort: true, filter: true}
                            ,{field:'supplier', title:'供应商', align:'center',minWidth:80, sort: true, filter: true}
                            ,{field:'price', title:'单价', align:'center', minWidth:80, sort: true, filter: true}
                            ,{fixed: 'right', title:'操作', align:'center', toolbar: '#barTop', minWidth:250}
                            ,{field:'id',hide:true}
                        ]]
                        ,excel: {
                            filename: '辅料操作表.xlsx'
                        }
                        ,data: reportData
                        ,height: 'full-100'
                        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                        ,title: '辅料操作表'
                        ,totalRow: true
                        ,loading: false
                        ,page: true
                        ,even: true
                        ,limits: [50, 100, 200]
                        ,limit: 100 //每页默认显示的数量
                        ,done: function () {
                            soulTable.render(this);
                            layer.close(load);
                        }
                    });
                } else {
                    layer.msg("获取失败！", {icon: 2});
                }
            }, error: function () {
                layer.msg("获取失败！", {icon: 2});
            }
        });
    }

    form.on('submit(searchBeat)', function(data){
        var accessoryName = $("#accessoryName").val();
        var accessoryNumber = $("#accessoryNumber").val();
        initTable(accessoryName, accessoryNumber);
        return false;
    });

    table.on('toolbar(dataTable)', function(obj) {
        var accessoryName = $("#accessoryName").val();
        var accessoryNumber = $("#accessoryNumber").val();
        if (obj.event === 'clearFilter') {
            // 清除所有筛选条件并重载表格
            // 参数: tableId
            soulTable.clearFilter('dataTable')
        }
        else if (obj.event === 'exportExcel') {
            soulTable.export('dataTable');
        }
        else if (obj.event === "addNew") {
            var index = layer.open({
                type: 1 //Page层类型
                , title: "新增"
                , btn: ['保存']
                , area: ['1200px','500px']
                , shade: 0.6 //遮罩透明度
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , content: $("#preStoreAdd")
                ,yes: function(i, layero){
                    var param = {};
                    var preFix = $("#accessoryTypeAdd").val();
                    param.accessoryName = $("#accessoryNameAdd").val();
                    param.accessoryNumber = preFix + $("#accessoryNumberAdd").val();
                    param.specification = $("#specificationAdd").val();
                    param.accessoryColor = $("#accessoryColorAdd").val();
                    param.accessoryUnit = $("#accessoryUnitAdd").val();
                    param.supplier = supplierDemo.getValue('nameStr');
                    param.price = $("#priceAdd").val();
                    if (preFix == null || preFix == '' || param.accessoryName == null || param.accessoryName == '' || param.accessoryNumber == null || param.accessoryNumber == '' || param.supplier == null || param.supplier == '' || param.accessoryUnit == null || param.accessoryUnit == ''){
                        layer.msg("请填写完整信息~~~");
                        return false;
                    }
                    $.ajax({
                        url: "/erp/addaccessoryprestoreindex",
                        type: 'POST',
                        data: {
                            accessoryPreStoreJson: JSON.stringify(param)
                        },
                        success: function (res) {
                            if (res.result == 0) {
                                initTable('', '');
                                layer.close(index);
                                layer.msg("添加成功！", {icon: 1});
                            } else if (res.result == 4) {
                                layer.msg("该辅料已经存在,请筛选后直接操作！", {icon: 2});
                            } else {
                                layer.msg("添加失败！", {icon: 2});
                            }
                        },
                        error: function () {
                            layer.msg("添加失败！", {icon: 2});
                        }
                    })

                }
            });
            setTimeout(function () {
                form.on('select(demo)', function(data){
                    var preFix = $("#accessoryTypeAdd").val();
                    if (preFix != null && preFix != ''){
                        $.ajax({
                            url: "/erp/getaccessorynameorderbyprefix",
                            data: {
                                preFix: preFix
                            },
                            success:function(data){
                                if (data.accessoryOrder){
                                    $("#accessoryNumberAdd").val(data.accessoryOrder);
                                }
                            }, error:function(){
                            }
                        });
                        form.render('select');
                    }
                });
                $.ajax({
                    url: "/erp/getallsupplierinfo",
                    data: {
                        supplierType: "辅料"
                    },
                    async:false,
                    success:function(res){
                        if (res.data){
                            var resultData = [];
                            $.each(res.data, function(index,element){
                                if (element != null && element != ''){
                                    resultData.push({name:element.supplierName, value:element.supplierName});
                                }
                            });
                            supplierDemo = xmSelect.render({
                                el: '#supplierAdd',
                                radio: true,
                                filterable: true,
                                data: resultData
                            });
                        }
                    }, error:function(){
                    }
                });
            }, 100)
        }
        else if (obj.event === 'combine') {
            var index = layer.open({
                type: 1 //Page层类型
                , title: '组合下单'
                , btn: ['保存']
                , shade: 0.6 //遮罩透明度
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , content: $("#combineDiv")
                ,yes: function(i, layero){
                    var combineData = table.cache.combineTable;
                    var combineList = [];
                    var nowDate = dateFormat("YYYY-mm-dd HH:MM:SS", new Date()).replace(" ", "").replace(/\:/g,'').replace(/\-/g,'');
                    $.each(combineData,function(i,item){
                        if(item.LAY_CHECKED) {
                            combineList.push({
                                id:item.id,
                                checkNumber: nowDate,
                                preCheck: '已提交',
                                checkState: '未提交'
                            })
                        }
                    });
                    if (combineList.length == 0){
                        layer.msg("请先选择数据再保存~~");
                        return false;
                    }
                    $.ajax({
                        url: "/erp/accessoryprestorecombineorder",
                        type: 'POST',
                        data: {
                            accessoryPreStoreJson: JSON.stringify(combineList)
                        },
                        success: function (res) {
                            if (res.result == 0) {
                                initTable(accessoryName, accessoryNumber);
                                layer.msg("下单成功,待财务审核,合同号" + nowDate, {icon: 1});
                                layer.close(index);
                            } else {
                                layer.msg("下单失败！", {icon: 2});
                            }
                        },
                        error: function () {
                            layer.msg("下单失败！", {icon: 2});
                        }
                    });
                }
                , cancel : function (i,layero) {

                }
            });
            layer.full(index);
            setTimeout(function () {
                $.ajax({
                    url: "/erp/getaccessoryprestorecombine",
                    type: 'GET',
                    data: {},
                    success: function (res) {
                        if (res.accessoryPreStoreList) {
                            var reportData = res.accessoryPreStoreList;
                            table.render({
                                elem: '#combineTable'
                                ,cols:[[
                                    {type:'checkbox'}
                                    ,{type:'numbers', align:'center', title:'序号'}
                                    ,{field:'accessoryName', title:'名称', align:'center', minWidth:150, sort: true, filter: true}
                                    ,{field:'accessoryNumber', title:'编号', align:'center', minWidth:150, sort: true, filter: true, totalRowText: '合计'}
                                    ,{field:'specification', title:'规格', align:'center', minWidth:100, sort: true, filter: true}
                                    ,{field:'accessoryColor', title:'颜色', align:'center', minWidth:100, sort: true, filter: true}
                                    ,{field:'accessoryUnit', title:'单位', align:'center', minWidth:80, sort: true, filter: true}
                                    ,{field:'supplier', title:'供应商', align:'center',minWidth:80, sort: true, filter: true}
                                    ,{field:'price', title:'单价', align:'center', minWidth:80, sort: true, filter: true}
                                    ,{field:'accessoryCount', title:'数量', align:'center', minWidth:80, sort: true, filter: true}
                                    ,{field:'checkState', title:'状态', align:'center', minWidth:80, sort: true, filter: true}
                                    ,{field:'id',hide:true}
                                    ,{field:'preStoreId',hide:true}
                                ]]
                                ,excel: {
                                    filename: '辅料操作表.xlsx'
                                }
                                ,data: reportData
                                ,height: 'full-100'
                                ,title: '辅料操作表'
                                ,totalRow: true
                                ,loading: false
                                ,page: true
                                ,even: true
                                ,limits: [50, 100, 200]
                                ,limit: 100 //每页默认显示的数量
                                ,done: function () {
                                    soulTable.render(this);
                                }
                            });
                        } else {
                            layer.msg("获取失败！", {icon: 2});
                        }
                    }, error: function () {
                        layer.msg("获取失败！", {icon: 2});
                    }
                });
            }, 100);
        }
    });

    table.on('tool(dataTable)', function(obj){
        var filterAccessoryName = $("#accessoryName").val();
        var filterAccessoryNumber = $("#accessoryNumber").val();
        var data = obj.data;
        if(obj.event === 'delete'){
            layer.confirm('真的删除吗', function(index){
                $.ajax({
                    url: "/erp/deleteaccessoryprestorefromroot",
                    type: 'POST',
                    data: {
                        id: data.id
                    },
                    success: function (res) {
                        if (res.result == 0) {
                            initTable(filterAccessoryName, filterAccessoryNumber);
                            layer.close(index);
                            layer.msg("删除成功！", {icon: 1});
                        } else if (res.result == 4) {
                            layer.msg("该辅料已经被调用,无法删除！", {icon: 2});
                        } else {
                            layer.msg("删除失败！", {icon: 2});
                        }
                    },
                    error: function () {
                        layer.msg("删除失败！", {icon: 2});
                    }
                })

            });
        }
        else if(obj.event === 'add'){
            var id = data.id;
            var index = layer.open({
                type: 1 //Page层类型
                , title: '新增'
                , btn: ['保存']
                , shade: 0.6 //遮罩透明度
                , area: ['1200px','500px']
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , content: $("#preStoreAdd")
                ,yes: function(i, layero){
                    var param = {};
                    var preFix = $("#accessoryTypeAdd").val();
                    param.accessoryNumber = preFix + $("#accessoryNumberAdd").val();
                    param.accessoryName = $("#accessoryNameAdd").val();
                    param.specification = $("#specificationAdd").val();
                    param.accessoryColor = $("#accessoryColorAdd").val();
                    param.accessoryUnit = $("#accessoryUnitAdd").val();
                    param.supplier = supplierDemo.getValue('nameStr');
                    param.price = $("#priceAdd").val();
                    param.accessoryCount = $("#accessoryCountAdd").val();
                    param.accessoryLocation = $("#accessoryLocationAdd").val();
                    $.ajax({
                        url: "/erp/addaccessoryprestoreindex",
                        type: 'POST',
                        data: {
                            accessoryPreStoreJson: JSON.stringify(param)
                        },
                        success: function (res) {
                            if (res.result == 0) {
                                initTable(filterAccessoryName, filterAccessoryNumber);
                                layer.close(index);
                                layer.msg("添加成功！", {icon: 1});
                            } else if (res.result == 4) {
                                layer.msg("该辅料已经存在,请筛选后直接操作！", {icon: 2});
                            } else {
                                layer.msg("添加失败！", {icon: 2});
                            }
                        },
                        error: function () {
                            layer.msg("修改失败！", {icon: 2});
                        }
                    });
                    return false;
                }
                , cancel : function (i,layero) {
                    $("#accessoryNameAdd").val('');
                    $("#accessoryNumberAdd").val('');
                    $("#specificationAdd").val('');
                    $("#accessoryColorAdd").val('');
                    $("#accessoryUnitAdd").val('');
                    $("#priceAdd").val('');
                    $("#accessoryCountAdd").val('');
                    $("#accessoryLocationAdd").val('');
                }
            });
            setTimeout(function () {
                $("#accessoryTypeAdd").val(data.accessoryNumber.slice(0, 2));
                $("#accessoryNumberAdd").val(data.accessoryNumber.slice(2));
                $("#accessoryNameAdd").val(data.accessoryName);
                $("#specificationAdd").val(data.specification);
                $("#accessoryColorAdd").val(data.accessoryColor);
                $("#accessoryUnitAdd").val(data.accessoryUnit);
                $("#priceAdd").val(data.price);
                $("#accessoryCountAdd").val(data.accessoryCount);
                $("#accessoryLocationAdd").val(data.accessoryLocation);
                form.render();
                $.ajax({
                    url: "/erp/getallsupplierinfo",
                    data: {
                        supplierType: "辅料"
                    },
                    async:false,
                    success:function(res){
                        if (res.data){
                            var resultData = [];
                            $.each(res.data, function(index,element){
                                if (element != null && element != ''){
                                    if (element.supplierName === data.supplier){
                                        resultData.push({name:element.supplierName, selected: true, value:element.supplierName});
                                    } else {
                                        resultData.push({name:element.supplierName, value:element.supplierName});
                                    }
                                }
                            });
                            supplierDemo = xmSelect.render({
                                el: '#supplierAdd',
                                radio: true,
                                filterable: true,
                                data: resultData
                            });
                        }
                    }, error:function(){
                    }
                });
            },100);
        }
        else if(obj.event === 'inStore'){
            var id = data.id;
            var index = layer.open({
                type: 1 //Page层类型
                , title: '入库'
                , btn: ['保存']
                , shade: 0.6 //遮罩透明度
                , area: ['500px', '300px']
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , content: "<div><table><tr><td style=\"text-align: right;margin-bottom: 15px;\">\n" +
                    "                                <label class=\"layui-form-label\" style=\"width: 100px\">数量</label>\n" +
                    "                            </td>\n" +
                    "                            <td style=\"margin-bottom: 15px;\">\n" +
                    "                                <input type=\"text\" id=\"accessoryInStoreCount\" autocomplete=\"off\" class=\"layui-input\">\n" +
                    "                            </td></tr><tr>\n" +
                    "                            <td style=\"text-align: right;margin-bottom: 15px;\">\n" +
                    "                                <label class=\"layui-form-label\" style=\"width: 100px\">位置</label>\n" +
                    "                            </td>\n" +
                    "                            <td style=\"margin-bottom: 15px;\">\n" +
                    "                                <input type=\"text\" id=\"accessoryInStoreLocation\" autocomplete=\"off\" class=\"layui-input\">\n" +
                    "                            </td></tr></table></div>"
                ,yes: function(i, layero){
                    var param = {};
                    param.id = data.id;
                    param.accessoryCount = $("#accessoryInStoreCount").val();
                    param.accessoryLocation = $("#accessoryInStoreLocation").val();
                    $.ajax({
                        url: "/erp/addaccessoryprestoreinstore",
                        type: 'POST',
                        data: param,
                        success: function (res) {
                            if (res.result == 0) {
                                initTable(filterAccessoryName, filterAccessoryNumber);
                                layer.msg("修改成功！", {icon: 1});
                                layer.close(index);
                            } else {
                                layer.msg("修改失败！", {icon: 2});
                            }
                        },
                        error: function () {
                            layer.msg("修改失败！", {icon: 2});
                        }
                    });
                    return false;
                }
                , cancel : function (i,layero) {
                    $("#accessoryInStoreCount").val('');
                    $("#accessoryInStoreLocation").val('');
                }
            });
        }
        else if(obj.event === 'buy'){
            var id = data.id;
            var index = layer.open({
                type: 1 //Page层类型
                , title: '下单'
                , btn: ['提交审核', '保存后组合下单']
                , shade: 0.6 //遮罩透明度
                , area: ['500px', '300px']
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , content: "<div><table><tr>" +
                    "       <td style=\"text-align: right;margin-bottom: 15px;\">\n" +
                    "           <label class=\"layui-form-label\" style=\"width: 100px\">数量</label>\n" +
                    "       </td>\n" +
                    "       <td style=\"margin-bottom: 15px;\">\n" +
                    "           <input type=\"text\" id=\"accessoryCount\" autocomplete=\"off\" class=\"layui-input\">\n" +
                    "       </td></tr><tr>\n" +
                    "       <td style=\"text-align: right;margin-bottom: 15px;\">\n" +
                    "           <label class=\"layui-form-label\" style=\"width: 100px\">单价</label>\n" +
                    "       </td>\n" +
                    "       <td style=\"margin-bottom: 15px;\">\n" +
                    "           <input type=\"text\" id=\"price\" autocomplete=\"off\" class=\"layui-input\">\n" +
                    "       </td>" +
                    "</tr></table></div>"
                ,yes: function(i, layero){
                    var nowDate = dateFormat("YYYY-mm-dd HH:MM:SS", new Date()).replace(" ", "").replace(/\:/g,'').replace(/\-/g,'');
                    var param = {};
                    param.id = data.id;
                    param.accessoryCount = $("#accessoryCount").val();
                    param.price = $("#price").val();
                    param.checkNumber = nowDate;
                    $.ajax({
                        url: "/erp/addaccessoryprestoredirectorder",
                        type: 'POST',
                        data: param,
                        success: function (res) {
                            if (res.result == 0) {
                                initTable(filterAccessoryName, filterAccessoryNumber);
                                layer.msg("下单成功,待财务审核,合同号" + nowDate, {icon: 1});
                                layer.close(index);
                            } else {
                                layer.msg("下单失败！", {icon: 2});
                            }
                        },
                        error: function () {
                            layer.msg("下单失败！", {icon: 2});
                        }
                    });
                    return false;
                }
                ,btn2: function(i, layero){
                    var param = {};
                    param.id = data.id;
                    param.accessoryCount = $("#accessoryCount").val();
                    param.price = $("#price").val();
                    $.ajax({
                        url: "/erp/addaccessoryprestorepreorder",
                        type: 'POST',
                        data: param,
                        success: function (res) {
                            if (res.result == 0) {
                                initTable(filterAccessoryName, filterAccessoryNumber);
                                layer.msg("操作成功,请在组合下单操作", {icon: 1});
                                layer.close(index);
                            } else {
                                layer.msg("下单失败！", {icon: 2});
                            }
                        },
                        error: function () {
                            layer.msg("下单失败！", {icon: 2});
                        }
                    });
                    return false;
                }
                , cancel : function (i,layero) {
                    $("#accessoryCount").val('');
                    $("#price").val('');
                }
            });
        }
    });

});

function toDecimal(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*100)/100;
    return f;
}

function isNumber(val){

    var regPos = /^\d+(\.\d+)?$/; //非负浮点数
    var regNeg = /^(-(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*)))$/; //负浮点数
    if (regPos.test(val) || regNeg.test(val)){
        return true;
    }else{
        return false;
    }

}

function dateFormat(fmt, date) {
    var ret;
    var opt = {
        "Y+": date.getFullYear().toString(),        // 年
        "m+": (date.getMonth() + 1).toString(),     // 月
        "d+": date.getDate().toString(),            // 日
        "H+": date.getHours().toString(),           // 时
        "M+": date.getMinutes().toString(),         // 分
        "S+": date.getSeconds().toString()          // 秒
        // 有其他格式化字符需求可以继续添加，必须转化成字符串
    };
    for (var k in opt) {
        ret = new RegExp("(" + k + ")").exec(fmt);
        if (ret) {
            fmt = fmt.replace(ret[1], (ret[1].length == 1) ? (opt[k]) : (opt[k].padStart(ret[1].length, "0")))
        }
    }
    return fmt;
}