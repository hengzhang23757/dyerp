var hot;
var basePath=$("#basePath").val();
$(document).ready(function () {

    layui.laydate.render({
        elem: '#from',
        trigger: 'click',
        type: 'datetime'
    });
    layui.laydate.render({
        elem: '#to',
        trigger: 'click',
        type: 'datetime'
    });

    layui.use(['autocomplete'], function () {
        layui.autocomplete.render({
            elem: $('#clothesVersionNumber')[0],
            url: 'getversionhint',
            template_val: '{{d}}',
            template_txt: '{{d}}',
            onselect: function (resp) {
                autoComplete(resp)
            }
        })
    });

});


function autoComplete(keywords) {
    $("#orderName").empty();
    $.ajax({
        url: "/erp/getorderbyversion",
        data: {"clothesVersionNumber": keywords},
        success:function(data){
            $("#orderName").empty();
            if (data.orderList) {
                $("#orderName").append("<option value=''>订单号</option>");
                $.each(data.orderList, function(index,element){
                    $("#orderName").append("<option value='"+element+"'>"+element+"</option>");
                })
            }
        },
        error:function(){
        }
    });
}



function search() {
    if($("#orderName").val().trim()=="") {
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入订单号！</span>",html: true});
        return false;
    }
    $.ajax({
        url: basePath + "erp/unloadreport",
        type:'GET',
        data: {
            orderName:$("#orderName").val(),
            from:$("#from").val(),
            to:$("#to").val(),
        },
        success: function (data) {
            console.log(data);
            var container = document.getElementById('reportExcel');
            if (hot == undefined) {
                hot = new Handsontable(container, {
                    data: hotData,
                    rowHeaders: true,
                    colHeaders: true,
                    autoColumnSize: true,
                    dropdownMenu: true,
                    contextMenu: true,
                    filters:true,
                    // minRows: 35,
                    minCols: 20,
                    colWidths:100,
                    // colWidths: 80,
                    language: 'zh-CN',
                    licenseKey: 'non-commercial-and-evaluation'

                });
            }
            if(data) {
                $("#exportDiv").show();
                var hotData = [];
                var colorData = data.color;
                var partIndex = 0;
                var sizeSummary = [];
                sizeSummary[0] = [""];
                sizeSummary[1] = ["尺码小计"];
                for(var colorKey in colorData) {
                    var colorRow1 = [];
                    var colorRow2 = [];
                    var colorRow3 = [];
                    var colorRow4 = [];
                    var colorRow5 = [];
                    colorRow2[0] = [];
                    colorRow3[0] = [];
                    colorRow4[0] = [];
                    colorRow5[0] = [];
                    colorRow1.push(colorKey);
                    colorRow1.push("订单量");
                    colorRow2.push("裁床量");
                    colorRow3.push("总下货数");
                    colorRow4.push("未下货数");
                    colorRow5.push("区间下货数");
                    var sum1 = 0;
                    var sum2 = 0;
                    var sum3 = 0;
                    var sum4 = 0;
                    var sum5 = 0;
                    var sizeData = colorData[colorKey];
                    var sizeRow = [];
                    var sizeIndex = 2;


                    var sizeDataList = [];
                    for (var i in sizeData){
                        var sizeDataItem = {};
                        sizeDataItem.sizeName = i;
                        sizeDataItem.sizeDataDetail = sizeData[i];
                        sizeDataList.push(sizeDataItem);
                    }
                    sizeDataList.sort(function(a,b){
                        // order是规则  objs是需要排序的数组
                        var order = ["070","70","073","075","75","080","80","085","090","90","095","95", "100", "105","110","115", "120","125", "130","135",
                            "140","145", "150","155", "160","165", "170","175", "180","185"];
                        return order.indexOf(a.sizeName) - order.indexOf(b.sizeName);
                    });

                    for (var i in sizeDataList){
                        if (partIndex == 0){
                            sizeRow[0] = [];
                            sizeRow[1] = [];
                            sizeRow.push(sizeDataList[i].sizeName);
                            sizeSummary[sizeIndex] = 0;
                        }
                        colorRow1.push(sizeDataList[i].sizeDataDetail.orderCount);
                        colorRow2.push(sizeDataList[i].sizeDataDetail.cutCount);
                        colorRow3.push(sizeDataList[i].sizeDataDetail.finishCount);
                        colorRow4.push(sizeDataList[i].sizeDataDetail.leakCount);
                        colorRow5.push(sizeDataList[i].sizeDataDetail.productionCount);
                        sum1 += sizeDataList[i].sizeDataDetail.orderCount;
                        sum2 += sizeDataList[i].sizeDataDetail.cutCount;
                        sum3 += sizeDataList[i].sizeDataDetail.finishCount;
                        sum4 += sizeDataList[i].sizeDataDetail.leakCount;
                        sum5 += sizeDataList[i].sizeDataDetail.productionCount;
                        sizeSummary[sizeIndex] += sizeDataList[i].sizeDataDetail.productionCount;
                        sizeIndex++;
                    }
                    if (partIndex == 0) {
                        sizeRow.push("合计");
                        hotData.push(sizeRow);
                    }
                    colorRow1.push(sum1);
                    colorRow2.push(sum2);
                    colorRow3.push(sum3);
                    colorRow4.push(sum4);
                    colorRow5.push(sum5);
                    hotData.push(colorRow1);
                    hotData.push(colorRow2);
                    hotData.push(colorRow3);
                    hotData.push(colorRow4);
                    hotData.push(colorRow5);
                    partIndex ++;
                }
                hotData.push(sizeSummary);
                hotData.push([]);
                hot.loadData(hotData);
            }else {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，没有查到相关信息！</span>",html: true});
            }
        },
        error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }
    })
}

















// function search() {
//     if($("#orderName").val().trim()=="") {
//         swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入订单号！</span>",html: true});
//         return false;
//     }
//
//     var container = document.getElementById('reportExcel');
//     hot = new Handsontable(container, {
//         // data: data,
//         rowHeaders: true,
//         colHeaders: true,
//         autoColumnSize:true,
//         dropdownMenu: true,
//         contextMenu:true,
//         renderAllRows: true,
//         minRows:1,
//         minCols:5,
//         // colWidths:70,
//         language:'zh-CN',
//         licenseKey: 'non-commercial-and-evaluation'
//     });
//
//     $.ajax({
//         url: basePath + "erp/unloadreport",
//         type:'GET',
//         data: {
//             orderName:$("#orderName").val(),
//             from:$("#from").val(),
//             to:$("#to").val(),
//         },
//         success: function (data) {
//             $("#exportDiv").show();
//             var hotData = [];
//             if(data) {
//                 console.log(data);
//                 var colorData = data["color"];
//                 var partIndex = 0;
//                 var sizeSummary = [];
//                 sizeSummary[0] = [""];
//                 sizeSummary[1] = ["尺码生产合计"];
//                 for(var color in colorData) {
//                     var colorRow1 = [];
//                     var colorRow2 = [];
//                     var colorRow3 = [];
//                     var colorRow4 = [];
//                     var colorRow5 = [];
//                     colorRow2[0] = [];
//                     colorRow3[0] = [];
//                     colorRow4[0] = [];
//                     colorRow5[0] = [];
//                     colorRow1.push(color);
//                     colorRow1.push("订单量");
//                     colorRow2.push("裁床量");
//                     colorRow3.push("总生产量");
//                     colorRow4.push("未生产量");
//                     colorRow5.push("区间生产量");
//                     var sum1 = 0;
//                     var sum2 = 0;
//                     var sum3 = 0;
//                     var sum4 = 0;
//                     var sum5 = 0;
//                     var sizeData = colorData[color];
//                     var sizeRow = [];
//                     var sizeIndex = 2;
//                     for (var sizeKey in sizeData){
//                         if (partIndex == 0){
//                             sizeRow[0] = [];
//                             sizeRow[1] = [];
//                             sizeRow.push(sizeKey);
//                             sizeSummary[sizeIndex] = 0;
//                         }
//                         colorRow1.push(sizeData[sizeKey].orderCount);
//                         colorRow2.push(sizeData[sizeKey].cutCount);
//                         colorRow3.push(sizeData[sizeKey].finishCount);
//                         colorRow4.push(sizeData[sizeKey].leakCount);
//                         colorRow5.push(sizeData[sizeKey].productionCount);
//                         sum1 += sizeData[sizeKey].orderCount;
//                         sum2 += sizeData[sizeKey].cutCount;
//                         sum3 += sizeData[sizeKey].finishCount;
//                         sum4 += sizeData[sizeKey].leakCount;
//                         sum5 += sizeData[sizeKey].productionCount;
//                         sizeSummary[sizeIndex] += sizeData[sizeKey].productionCount;
//                         sizeIndex++;
//                     }
//                     if (partIndex == 0) {
//                         sizeRow.push("合计");
//                         hotData.push(sizeRow);
//                     }
//                     colorRow1.push(sum1);
//                     colorRow2.push(sum2);
//                     colorRow3.push(sum3);
//                     colorRow4.push(sum4);
//                     colorRow5.push(sum5);
//                     hotData.push(colorRow1);
//                     hotData.push(colorRow2);
//                     hotData.push(colorRow3);
//                     hotData.push(colorRow4);
//                     hotData.push(colorRow5);
//                     partIndex ++;
//                 }
//                 hotData.push(sizeSummary);
//                 hotData.push([]);
//                 if(hotData.length>0){
//                     hot.loadData(hotData);
//                 }
//             }
//
//         },
//         error: function () {
//             swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
//         }
//     })
// }


function exportData() {
    var data = hot.getData();
    var result = [];
    var col = 0;
    $.each(data,function (index, item) {
        // if(item[0]!=null) {
        for(var i=0;i<item.length;i++) {
            if(item[i]==null) {
                item[i] = '';
            }else if(index==1) {
                col++;
            }
        }
        result.push(item);
        // }else {
        //     return false;
        // }
    });
    var orderName = $("#orderName").val();
    var clothesVersionNumber = $("#clothesVersionNumber").val();
    var fromDate = $("#from").val();
    var toDate = $("#to").val();
    export2Excel([orderName+"-"+clothesVersionNumber+'-下成品明细-'+fromDate+'-'+toDate],col, result, orderName+"-"+clothesVersionNumber+'-下成品明细-'+fromDate+'-'+toDate+".xls")
}