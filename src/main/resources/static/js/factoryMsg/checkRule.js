layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.6'
}).extend({                         // 模块别名
    soulTable: 'soulTable'
});
layui.use('form', function(){
    var form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
    form.render();
});
var dataTable;
var userRole = $("#userRole").val();
//时间选择器
layui.laydate.render({
    elem: '#checkTime'
    ,type: 'time'
});
layui.use(['form', 'soulTable', 'table', 'upload','yutons_sug'], function () {
    var table = layui.table,
        soulTable = layui.soulTable,
        form = layui.form;
    var load = layer.load();

    dataTable = table.render({
        elem: '#dataTable'
        ,url:'getallcheckrule'
        ,method:'post'
        ,cols: [[]]
        ,loading:true
        ,done: function (res, curr, count) {
            table.init('dataTable',{//转换成静态表格
                cols:[[
                    {type:'numbers', align:'center', title:'序号', width:150}
                    ,{field:'ruleName', title:'名称', align:'center', width:250, sort: true, filter: true}
                    ,{field:'workName', title:'班次', align:'center', width:250, sort: true, filter: true}
                    ,{field:'checkTime', title:'时间', align:'center', width:250, sort: true, filter: true}
                    ,{field: 'id', title:'操作', align:'center', toolbar: '#barTop', width:120}
                ]]
                ,data:res.data
                ,height: 'full-50'
                ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                ,defaultToolbar: ['filter', 'print']
                ,title: '尺码表'
                ,totalRow: true
                ,page: true
                ,limits: [50, 100, 200]
                ,limit: 100 //每页默认显示的数量
                ,overflow: 'tips'
                ,done: function () {
                    soulTable.render(this);
                    layer.close(load);
                }
                ,filter: {
                    bottom: true,
                    items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                }
            });
        }
    });


    table.on('toolbar(dataTable)', function(obj) {
        if (obj.event === 'refresh') {
            dataTable.reload();
        }
    });

    //监听行工具事件
    table.on('tool(dataTable)', function(obj){
        var data = obj.data;
        if(obj.event === 'update'){
            var index = layer.open({
                type: 1 //Page层类型
                , title: '修改'
                , btn: ['确认','取消']
                , shade: 0.6 //遮罩透明度
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , area: ['345px', '200px']
                , content: $('#ruleInput')
                , yes: function (index, layero) {
                    var workName= $("#workName").val();
                    var checkTime= $("#checkTime").val();
                    var param = {};
                    param.id = data.id;
                    param.checkTime = checkTime;
                    param.workName = workName;
                    if (checkTime == null || checkTime == "" || workName == null || workName == ""){
                        layer.msg("请填写完整", { icon: 2 });
                        return false;
                    }
                    $.ajax({
                        url: "/erp/updatecheckrule",
                        type: 'POST',
                        data: param,
                        success: function (res) {
                            layer.closeAll();
                            if (res.result == 0){
                                dataTable.reload();
                                $("#checkTime").val();
                                $("#workName").val();
                                layer.msg("修改成功！", {icon: 1});
                            }
                        }, error: function () {
                            layer.msg("修改失败！", {icon: 2});
                        }
                    })
                }
                ,btn2: function(){
                    $("#checkTime").val();
                    $("#workName").val();
                    layer.closeAll();
                }
                , cancel: function (i, layero) {
                    $("#checkTime").val();
                    $("#workName").val();
                    layer.closeAll();
                }
            });
            $("#workName").val(data.workName);
            $("#checkTime").val(data.checkTime);
            form.render();
            return false;
        }
    })

});