var basePath=$("#basePath").val();
var userRole=$("#userRole").val();
var picUrl;
var staffTable;
$(document).ready(function () {

    layui.laydate.render({
        elem: '#birthday', //指定元素
        trigger: 'click'
    });

    layui.laydate.render({
        elem: '#beginDate', //指定元素
        trigger: 'click'
    });

    layui.laydate.render({
        elem: '#leaveDate', //指定元素
        trigger: 'click'
    });

    layui.laydate.render({
        elem: '#contractBegin', //指定元素
        trigger: 'click'
    });

    layui.laydate.render({
        elem: '#contractEnd', //指定元素
        trigger: 'click'
    });

    $.ajax({
        url: "/erp/getallgroupnamelist",
        type: 'GET',
        data: {},
        success: function (res) {
            $("#groupName").empty();
            if (res.data) {
                $.each(res.data, function(index,element){
                    $("#groupName").append("<option value='"+element+"'>"+element+"</option>");
                })
            } else {
                layer.msg("获取失败！", {icon: 2});
            }
            form.render("select");
        }, error: function () {
            layer.msg("获取失败！", {icon: 2});
        }
    });

    $.ajax({
        url: "/erp/getalldepartmentnamelist",
        type: 'GET',
        data: {},
        success: function (res) {
            $("#department").empty();
            if (res.data) {
                $.each(res.data, function(index,element){
                    $("#department").append("<option value='"+element+"'>"+element+"</option>");
                })
            } else {
                layer.msg("获取失败！", {icon: 2});
            }
            form.render("select");
        }, error: function () {
            layer.msg("获取失败！", {icon: 2});
        }
    });

    layui.config({
        base: '/js/ext/',   // 模块目录
        version: 'v1.5.3'
    }).extend({                         // 模块别名
        soulTable: 'soulTable'
    });

    layui.use(['form', 'soulTable', 'table'], function () {

        var table = layui.table,
            soulTable = layui.soulTable,
            $ = layui.$;
        var load = layer.load();
        staffTable = table.render({
            elem: '#staffTable'
            ,url:'getAllInStateStaff'
            ,method:'post'
            ,cols: [[]]
            ,done: function (res, curr, count) {
                table.init('staffTable',{//转换成静态表格
                    cols:[[
                        {fixed:'left', type:'numbers', align:'center', title:'序号', width:80}
                        ,{fixed:'left', field:'employeeName', title:'姓名', align:'center', width:120, sort: true, filter: true}
                        ,{fixed:'left', field:'employeeNumber', title:'工号', align:'center', width:120, sort: true, filter: true}
                        ,{field:'passWord', title:'密码', align:'center', width:120, sort: true, filter: true}
                        ,{field:'gender', title:'性别', align:'center', width:80, sort: true, filter: true}
                        ,{field:'department', title:'部门', align:'center', width:120, sort: true, filter: true}
                        ,{field:'position', title:'职位', align:'center', width:120, sort: true, filter: true}
                        ,{field:'turner', title:'车位', align:'center', width:120, sort: true, filter: true}
                        ,{field:'groupName', title:'组别', align:'center', width:120, sort: true, filter: true}
                        ,{field:'role', title:'角色', align:'center', width:120, sort: true, filter: true, templet: function (d) {
                                if (d.role === 'role1'){
                                    return "计件";
                                } else if (d.role === 'role2') {
                                    return "派工";
                                } else {
                                    return "";
                                }
                            }}
                        ,{field:'salaryType', title:'工资类别', align:'center', width:120, sort: true, filter: true}
                        ,{field:'identifyCard', title:'工卡号', align:'center', width:120, sort: true, filter: true}
                        ,{field:'idcard', title:'身份证号', align:'center', width:250, sort: true, filter: true}
                        ,{field:'nation', title:'民族', align:'center', width:80, sort: true, filter: true}
                        ,{field:'province', title:'籍贯', align:'center', width:120, sort: true, filter: true}
                        ,{field:'birthday', title:'出生年月', align:'center', width:130, sort: true, filter: true, templet: function (d) {
                                return transDate(d.birthday);
                            }}
                        ,{field:'birthPlace', title:'家庭住址', align:'center', width:200, sort: true, filter: true}
                        ,{field:'education', title:'最高学历', align:'center', width:120, sort: true, filter: true}
                        ,{field:'telephone', title:'电话', align:'center', width:120, sort: true, filter: true}
                        ,{field:'emergencyContact', title:'紧急联系人', align:'center', width:150, sort: true, filter: true}
                        ,{field:'relation', title:'与本人关系', align:'center', width:120, sort: true, filter: true}
                        ,{field:'emergencyPhone', title:'紧急电话', align:'center', width:120, sort: true, filter: true}
                        ,{field:'maritalState', title:'婚姻状态', align:'center', width:100, sort: true, filter: true}
                        ,{field:'cardOfBank', title:'开户行', align:'center', width:120, sort: true, filter: true}
                        ,{field:'bankCardNumber', title:'银行卡号', align:'center', width:150, sort: true, filter: true}
                        ,{field:'beginDate', title:'入职日期', align:'center', width:130, sort: true, filter: true, templet: function (d) {
                                return transDate(d.beginDate);
                            },excel: {cellType: 'd'}}
                        ,{field:'positionState', title:'状态', align:'center', width:130, sort: true, filter: true}
                        ,{field:'contractBegin', title:'合同开始', align:'center', width:130, sort: true, filter: true, templet: function (d) {
                                return transDate(d.contractBegin);
                            },excel: {cellType: 'd'}}
                        ,{field:'contractEnd', title:'合同结束', align:'center', width:130, sort: true, filter: true, templet: function (d) {
                                return transDate(d.contractEnd);
                            },excel: {cellType: 'd'}}
                        ,{field:'leaveDate', title:'离职日期', align:'center', width:120, sort: true, filter: true, templet: function (d) {
                                return transDate(d.leaveDate);
                            },excel: {cellType: 'd'}}
                        ,{fixed:'right', title:'操作', align:'center', toolbar: '#barTop', width:120}

                    ]]
                    ,data:res.data
                    ,height: 'full-50'
                    ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                    ,defaultToolbar: ['filter', 'print']
                    ,title: '用户数据表'
                    ,page: true
                    ,limits: [20,50, 100, 200]
                    ,limit: 20 //每页默认显示的数量
                    ,done: function () {
                        soulTable.render(this);
                        layer.close(load);
                    }
                    ,filter: {
                        bottom: true,
                        items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                    }
                });
            }
        });

        table.on('toolbar(staffTable)', function(obj){
            if (obj.event === 'clearFilter') {
                // 清除所有筛选条件并重载表格
                // 参数: tableId
                soulTable.clearFilter('staffTable')
            } else if (obj.event === 'exportExcel') {
                soulTable.export('staffTable');
            }else if(obj.event === 'add') {
                add();
            }else if(obj.event === 'onState') {
                table.render({
                    elem: "#staffTable"
                    ,url:'getAllInStateStaff'
                    ,method:'post'
                    ,cols: [[]]
                    ,done: function (res, curr, count) {
                        table.init('staffTable',{//转换成静态表格
                            cols:[[
                                {fixed:'left', type:'numbers', align:'center', title:'序号', width:80}
                                ,{fixed:'left', field:'employeeName', title:'姓名', align:'center', width:120, sort: true, filter: true}
                                ,{fixed:'left', field:'employeeNumber', title:'工号', align:'center', width:120, sort: true, filter: true}
                                ,{field:'passWord', title:'密码', align:'center', width:120, sort: true, filter: true}
                                ,{field:'gender', title:'性别', align:'center', width:80, sort: true, filter: true}
                                ,{field:'department', title:'部门', align:'center', width:120, sort: true, filter: true}
                                ,{field:'position', title:'职位', align:'center', width:120, sort: true, filter: true}
                                ,{field:'turner', title:'车位', align:'center', width:120, sort: true, filter: true}
                                ,{field:'groupName', title:'组别', align:'center', width:120, sort: true, filter: true}
                                ,{field:'role', title:'角色', align:'center', width:120, sort: true, filter: true, templet: function (d) {
                                        if (d.role === 'role1'){
                                            return "计件";
                                        } else if (d.role === 'role2') {
                                            return "派工";
                                        } else {
                                            return "";
                                        }
                                    }}
                                ,{field:'salaryType', title:'工资类别', align:'center', width:120, sort: true, filter: true}
                                ,{field:'identifyCard', title:'工卡号', align:'center', width:120, sort: true, filter: true}
                                ,{field:'idcard', title:'身份证号', align:'center', width:250, sort: true, filter: true}
                                ,{field:'nation', title:'民族', align:'center', width:80, sort: true, filter: true}
                                ,{field:'province', title:'籍贯', align:'center', width:120, sort: true, filter: true}
                                ,{field:'birthday', title:'出生年月', align:'center', width:130, sort: true, filter: true, templet: function (d) {
                                        return transDate(d.birthday);
                                    }}
                                ,{field:'birthPlace', title:'家庭住址', align:'center', width:200, sort: true, filter: true}
                                ,{field:'education', title:'最高学历', align:'center', width:120, sort: true, filter: true}
                                ,{field:'telephone', title:'电话', align:'center', width:120, sort: true, filter: true}
                                ,{field:'emergencyContact', title:'紧急联系人', align:'center', width:150, sort: true, filter: true}
                                ,{field:'relation', title:'与本人关系', align:'center', width:120, sort: true, filter: true}
                                ,{field:'emergencyPhone', title:'紧急电话', align:'center', width:120, sort: true, filter: true}
                                ,{field:'maritalState', title:'婚姻状态', align:'center', width:100, sort: true, filter: true}
                                ,{field:'cardOfBank', title:'开户行', align:'center', width:120, sort: true, filter: true}
                                ,{field:'bankCardNumber', title:'银行卡号', align:'center', width:150, sort: true, filter: true}
                                ,{field:'beginDate', title:'入职日期', align:'center', width:130, sort: true, filter: true, templet: function (d) {
                                        return transDate(d.beginDate);
                                    },excel: {cellType: 'd'}}
                                ,{field:'positionState', title:'状态', align:'center', width:130, sort: true, filter: true}
                                ,{field:'contractBegin', title:'合同开始', align:'center', width:130, sort: true, filter: true, templet: function (d) {
                                        return transDate(d.contractBegin);
                                    },excel: {cellType: 'd'}}
                                ,{field:'contractEnd', title:'合同结束', align:'center', width:130, sort: true, filter: true, templet: function (d) {
                                        return transDate(d.contractEnd);
                                    },excel: {cellType: 'd'}}
                                ,{field:'leaveDate', title:'离职日期', align:'center', width:120, sort: true, filter: true, templet: function (d) {
                                        return transDate(d.leaveDate);
                                    },excel: {cellType: 'd'}}
                                ,{fixed:'right', title:'操作', align:'center', toolbar: '#barTop', width:120}

                            ]]
                            ,data:res.data
                            ,height: 'full-50'
                            ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                            ,defaultToolbar: ['filter', 'print']
                            ,title: '用户数据表'
                            ,page: true
                            ,limits: [20,50, 100, 200]
                            ,limit: 20 //每页默认显示的数量
                            ,done: function () {
                                soulTable.render(this);
                                layer.close(load);
                            }
                            ,filter: {
                                bottom: true,
                                items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                            }
                        });
                    }
                })
            }else if(obj.event === 'offState') {
                table.render({
                    elem: "#staffTable"
                    ,url:'getAllOffStateStaff'
                    ,method:'post'
                    ,cols: [[]]
                    ,done: function (res, curr, count) {
                        table.init('staffTable',{//转换成静态表格
                            cols:[[
                                {fixed:'left', type:'numbers', align:'center', title:'序号', width:80}
                                ,{fixed:'left', field:'employeeName', title:'姓名', align:'center', width:120, sort: true, filter: true}
                                ,{fixed:'left', field:'employeeNumber', title:'工号', align:'center', width:120, sort: true, filter: true}
                                ,{field:'passWord', title:'密码', align:'center', width:120, sort: true, filter: true}
                                ,{field:'gender', title:'性别', align:'center', width:80, sort: true, filter: true}
                                ,{field:'department', title:'部门', align:'center', width:120, sort: true, filter: true}
                                ,{field:'position', title:'职位', align:'center', width:120, sort: true, filter: true}
                                ,{field:'turner', title:'车位', align:'center', width:120, sort: true, filter: true}
                                ,{field:'groupName', title:'组别', align:'center', width:120, sort: true, filter: true}
                                ,{field:'role', title:'角色', align:'center', width:120, sort: true, filter: true, templet: function (d) {
                                        if (d.role === 'role1'){
                                            return "计件";
                                        } else if (d.role === 'role2') {
                                            return "派工";
                                        } else {
                                            return "";
                                        }
                                    }}
                                ,{field:'salaryType', title:'工资类别', align:'center', width:120, sort: true, filter: true}
                                ,{field:'identifyCard', title:'工卡号', align:'center', width:120, sort: true, filter: true}
                                ,{field:'idcard', title:'身份证号', align:'center', width:250, sort: true, filter: true}
                                ,{field:'nation', title:'民族', align:'center', width:80, sort: true, filter: true}
                                ,{field:'province', title:'籍贯', align:'center', width:120, sort: true, filter: true}
                                ,{field:'birthday', title:'出生年月', align:'center', width:130, sort: true, filter: true, templet: function (d) {
                                        return transDate(d.birthday);
                                    }}
                                ,{field:'birthPlace', title:'家庭住址', align:'center', width:200, sort: true, filter: true}
                                ,{field:'education', title:'最高学历', align:'center', width:120, sort: true, filter: true}
                                ,{field:'telephone', title:'电话', align:'center', width:120, sort: true, filter: true}
                                ,{field:'emergencyContact', title:'紧急联系人', align:'center', width:150, sort: true, filter: true}
                                ,{field:'relation', title:'与本人关系', align:'center', width:120, sort: true, filter: true}
                                ,{field:'emergencyPhone', title:'紧急电话', align:'center', width:120, sort: true, filter: true}
                                ,{field:'maritalState', title:'婚姻状态', align:'center', width:100, sort: true, filter: true}
                                ,{field:'cardOfBank', title:'开户行', align:'center', width:120, sort: true, filter: true}
                                ,{field:'bankCardNumber', title:'银行卡号', align:'center', width:150, sort: true, filter: true}
                                ,{field:'beginDate', title:'入职日期', align:'center', width:130, sort: true, filter: true, templet: function (d) {
                                        return transDate(d.beginDate);
                                    },excel: {cellType: 'd'}}
                                ,{field:'positionState', title:'状态', align:'center', width:130, sort: true, filter: true}
                                ,{field:'contractBegin', title:'合同开始', align:'center', width:130, sort: true, filter: true, templet: function (d) {
                                        return transDate(d.contractBegin);
                                    },excel: {cellType: 'd'}}
                                ,{field:'contractEnd', title:'合同结束', align:'center', width:130, sort: true, filter: true, templet: function (d) {
                                        return transDate(d.contractEnd);
                                    },excel: {cellType: 'd'}}
                                ,{field:'leaveDate', title:'离职日期', align:'center', width:120, sort: true, filter: true, templet: function (d) {
                                        return transDate(d.leaveDate);
                                    },excel: {cellType: 'd'}}
                                ,{fixed:'right', title:'操作', align:'center', toolbar: '#barTop', width:120}

                            ]]
                            ,data:res.data
                            ,height: 'full-50'
                            ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                            ,defaultToolbar: ['filter', 'print']
                            ,title: '用户数据表'
                            ,page: true
                            ,limits: [20,50, 100, 200]
                            ,limit: 20 //每页默认显示的数量
                            ,done: function () {
                                soulTable.render(this);
                                layer.close(load);
                            }
                            ,filter: {
                                bottom: true,
                                items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                            }
                        });
                    }
                })
            }
        });

        //监听行工具事件
        table.on('tool(staffTable)', function(obj){
            var data = obj.data;
            //console.log(obj)
            if(obj.event === 'del'){
                del(data.employeeID);
            } else if(obj.event === 'update'){
                update(data.employeeID);
            }
        });


    });


    layui.use(['form','upload'], function(){
        var form = layui.form;
        var upload = layui.upload;
        var uploadInst = upload.render({
            elem: '#uploadPic'
            ,url: "/erp/uploadPic"
            ,accept: 'images' //普通文件
            ,auto: true //选择文件后不自动上传
            // ,bindAction: '#updateBtn' //指向一个按钮触发上传
            ,acceptMime:'image/*'
            ,choose: function(obj){
                // var files = this.files = obj.pushFile(); //将每次选择的文件追加到文件队列
                obj.preview(function(index, file, result){
                    $('#imgUrl').attr('src', result); //图片链接（base64）
                    var $image = $('#imgUrl');
                    $image.viewer({
                        inline: false,
                        viewed: function() {
                            $image.viewer('zoomTo', 1);
                            $(".viewer-container").css("z-index",2022102211);
                        }
                    });
                });

            }
            ,done: function(res){
                if(!res.success) {
                    layer.alert('上传照片失败！');
                }else {
                    picUrl = res.picUrl;
                }
            }
            ,error: function(){
                //演示失败状态，并实现重传
                layer.alert('上传照片失败！');
            }
        });
    });

});

function add() {

    var index = layer.open({
        type: 1 //Page层类型
        , title: '人事简历录入'
        , btn: ['保存']
        , area: '1000px'
        , shade: 0.6 //遮罩透明度
        , maxmin: false //允许全屏最小化
        , anim: 0 //0-6的动画形式，-1不开启
        , content: $('#staffAdd')
        ,yes: function(index, layero){
            var birthday = $("#birthday").val();
            if(birthday == "") {
                birthday = null;
            }
            var beginDate = $("#beginDate").val();
            if(beginDate == "") {
                beginDate = null;
            }
            $.ajax({
                url: basePath + "erp/addemployee",
                type: 'POST',
                data: {
                    employeeName: $("#employeeName").val(),
                    gender: $("#gender").val(),
                    employeeNumber: $("#employeeNumber").val().trim(),
                    groupName: $("#groupName").val(),
                    position: $("#position").val(),
                    IDCard: $("#IDCard").val(),
                    birthday: birthday,
                    nation: $("#nation").val(),
                    education: $("#education").val(),
                    birthPlace: $("#birthPlace").val(),
                    telephone: $("#telephone").val(),
                    identifyCard: $("#identifyCard").val(),
                    maritalState: $("#maritalState").val(),
                    imgUrl: picUrl,
                    emergencyContact: $("#emergencyContact").val(),
                    relation: $("#relation").val(),
                    emergencyPhone: $("#emergencyPhone").val(),
                    beginDate: beginDate,
                    positionState:"在职",
                    bankCardNumber: $("#bankCardNumber").val(),
                    cardOfBank: $("#cardOfBank").val(),
                    isManagement: $("#isManagement").val(),
                    department: $("#department").val(),
                    salaryType: $("#salaryType").val(),
                    contractBegin: $("#contractBegin").val(),
                    contractEnd: $("#contractEnd").val(),
                    province: $("#province").val(),
                    turner: $("#turner").val(),
                    role: $("#role").val()
                },
                success: function (data) {
                    if (data == 0) {
                        layer.closeAll();
                        staffTable.reload();
                        layer.alert('恭喜你，录入成功！');
                        $("#staffAdd input").val("");
                        $("#staffAdd select").val("");
                        $("#positionStateDiv").css("display","block");
                        $("#positionStateLabel").css("display","block");
                        $("#leaveDate").css("display","block");
                        $("#leaveDateLabel").css("display","block");
                        $('#imgUrl').removeAttr('src');
                        var canvas = document.getElementById('canvas');
                        var ctx = canvas.getContext('2d');
                        ctx.clearRect(0, 0, 480, 320);
                        closeMedia();
                        // getStaffList();
                    } else if(data == 1){
                        layer.alert('对不起，录入失败！');
                    } else if(data == 2){
                        layer.alert('对不起，工号已存在，请重新输入！');
                    } else if(data == 3){
                        layer.alert('对不起，工卡号已存在，请重新输入！');

                    }
                }
            })
        }
        ,cancel: function(index, layero){
            layer.closeAll();
            $("#staffAdd input").val("");
            $("#staffAdd select").val("");
            $("#positionStateDiv").css("display","block");
            $("#positionStateLabel").css("display","block");
            $("#leaveDate").css("display","block");
            $("#leaveDateLabel").css("display","block");
            $('#imgUrl').removeAttr('src');
            var canvas = document.getElementById('canvas');
            var ctx = canvas.getContext('2d');
            ctx.clearRect(0, 0, 480, 320);
            closeMedia();
        }
    });
    $("#employeeName").focus();
    $("#positionStateDiv").css("display","none");
    $("#positionStateLabel").css("display","none");
    $("#leaveDate").css("display","none");
    $("#leaveDateLabel").css("display","none");
    layer.full(index);
    $("#role").val("role1");
    layui.form.render("select");
}

function del(employeeID) {
    swal({
        title: "",
        text: "<span style=\"font-weight:bolder;font-size: 20px\">您确定要删除该员工信息吗？</span>",
        type: "warning",
        html:true,
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: "确定",
        cancelButtonText:"我再想想",
        confirmButtonColor: "#ec6c62",
        showLoaderOnConfirm: true
    }, function() {
        $.ajax({
            url: "/erp/deleteemployee",
            type: 'POST',
            data: {
                employeeID: employeeID
            },
            success: function (data) {
                if (data == 0) {
                    swal({
                        type: "success",
                        title: "恭喜您",
                        text: "<span style=\"font-weight:bolder;font-size: 20px\">删除成功！</span>",
                        html: true
                    });
                    staffTable.reload();

                    // getStaffList();
                } else if (data == 1) {
                    swal({
                        type: "error",
                        title: "",
                        text: "<span style=\"font-weight:bolder;font-size: 20px\">本系统删除成功,考勤设备同步删除失败！</span>",
                        html: true
                    });
                } else if (data == 2) {
                    swal({
                        type: "error",
                        title: "",
                        text: "<span style=\"font-weight:bolder;font-size: 20px\">考勤设备删除成功,本系统删除失败！</span>",
                        html: true
                    });
                } else if (data == 3) {
                    swal({
                        type: "error",
                        title: "",
                        text: "<span style=\"font-weight:bolder;font-size: 20px\">考勤设备删除失败,本系统删除失败！</span>",
                        html: true
                    });
                } else {
                    swal({
                        type: "error",
                        title: "",
                        text: "<span style=\"font-weight:bolder;font-size: 20px\">删除失败！</span>",
                        html: true
                    });
                }
            },
            error: function () {
            }
        })
    });
}

function update(employeeID) {
    $("#identifyIdcardDiv").css("display","none");
    var index = layer.open({
        type: 1 //Page层类型
        , title: '人事简历修改'
        , btn: ['保存']
        , area: '1000px'
        , shade: 0.6 //遮罩透明度
        , maxmin: false //允许全屏最小化
        , anim: 0 //0-6的动画形式，-1不开启
        , content: $('#staffAdd')
        ,yes: function(index, layero){
            var birthday = $("#birthday").val();
            if(birthday == "") {
                birthday = null;
            }
            var beginDate = $("#beginDate").val();
            if(beginDate == "") {
                beginDate = null;
            }
            var leaveDate = $("#leaveDate").val();
            if(leaveDate == "") {
                leaveDate = null;
            }
            $.ajax({
                url: basePath + "erp/updateemployee",
                type: 'POST',
                data: {
                    employeeID:$("#employeeID").val(),
                    employeeName: $("#employeeName").val(),
                    gender: $("#gender").val(),
                    employeeNumber: $("#employeeNumber").val().trim(),
                    groupName: $("#groupName").val(),
                    IDCard: $("#IDCard").val(),
                    birthday: birthday,
                    nation: $("#nation").val(),
                    education: $("#education").val(),
                    birthPlace: $("#birthPlace").val(),
                    telephone: $("#telephone").val(),
                    identifyCard: $("#identifyCard").val(),
                    maritalState: $("#maritalState").val(),
                    imgUrl: picUrl,
                    emergencyContact: $("#emergencyContact").val(),
                    relation: $("#relation").val(),
                    emergencyPhone: $("#emergencyPhone").val(),
                    position: $("#position").val(),
                    role: $("#role").val(),
                    positionState: $("#positionState").val(),
                    beginDate: beginDate,
                    leaveDate: leaveDate,
                    bankCardNumber: $("#bankCardNumber").val(),
                    cardOfBank: $("#cardOfBank").val(),
                    isManagement: $("#isManagement").val(),
                    department: $("#department").val(),
                    salaryType: $("#salaryType").val(),
                    contractBegin: $("#contractBegin").val(),
                    contractEnd: $("#contractEnd").val(),
                    province: $("#province").val(),
                    turner: $("#turner").val()
                },
                success: function (data) {
                    if (data == 0) {
                        layer.closeAll();
                        layer.alert('恭喜你，修改成功！');
                        $("#staffAdd input").val("");
                        $("#staffAdd select").val("");
                        $('#imgUrl').removeAttr('src');
                        staffTable.reload();

                        // getStaffList();
                        $("#identifyIdcardDiv").css("display","block");
                    } else if(data == 1){
                        layer.alert('对不起，修改失败！');
                    } else if(data == 2){
                        layer.alert('对不起，工号重复，请修改工号！');
                    } else if(data == 3){
                        layer.alert('对不起，ID卡号已存在，请重新输入！');

                    } else if(data == 4){
                        layer.alert('修改成功, 考勤设备同步失败！');

                    } else if(data == 5){
                        layer.alert('修改失败, 考勤设备同步成功！');

                    } else if(data == 6){
                        layer.alert('修改失败, 考勤设备同步失败！');

                    }
                }
            })
        }
        ,cancel: function(index, layero){
            layer.closeAll();
            $("#staffAdd input").val("");
            $("#staffAdd select").val("");
            $('#imgUrl').removeAttr('src');
            $("#identifyIdcardDiv").css("display","block");
        }
    })
    layer.full(index);

    $.ajax({
        url: "/erp/getempbyid",
        type: 'GET',
        data: {
            employeeID: employeeID
        },
        success: function (data) {
            if (data) {
                $("#employeeID").val(data.employeeID);
                $("#employeeName").val(data.employeeName);
                $("#gender").val(data.gender);
                $("#employeeNumber").val(data.employeeNumber);
                $("#groupName").val(data.groupName);
                $("#IDCard").val(data.idcard);
                $("#birthday").val(transDate(data.birthday));
                $("#nation").val(data.nation);
                $("#education").val(data.education);
                $("#birthPlace").val(data.birthPlace);
                $("#telephone").val(data.telephone);
                $("#identifyCard").val(data.identifyCard);
                $("#maritalState").val(data.maritalState);
                picUrl= data.imgUrl;
                $("#imgUrl").attr("src",data.tmpImgUrl);
                $("#emergencyContact").val(data.emergencyContact);
                $("#relation").val(data.relation);
                $("#emergencyPhone").val(data.emergencyPhone);
                $("#beginDate").val(transDate(data.beginDate));
                $("#positionState").val(data.positionState);
                $("#leaveDate").val(transDate(data.leaveDate));
                $("#bankCardNumber").val(data.bankCardNumber);
                $("#cardOfBank").val(data.cardOfBank);
                $("#isManagement").val(data.isManagement);
                $("#department").val(data.department);
                $("#salaryType").val(data.salaryType);
                $("#contractBegin").val(transDate(data.contractBegin));
                $("#contractEnd").val(transDate(data.contractEnd));
                $("#province").val(data.province);
                $("#turner").val(data.turner);
                $("#role").val(data.role);
                $("#position").val(data.position);
                layui.form.render("select");
                layui.use('form', function() {
                    var form = layui.form;
                    form.render();
                });
                var $image = $('#imgUrl');
                $image.viewer({
                    inline: false,
                    viewed: function() {
                        $image.viewer('zoomTo', 1);
                        $(".viewer-container").css("z-index",2022102211);
                    }
                });
            }
        },
        error: function () {
        }
    })
    $("#employeeName").focus();
    layer.full(index);
}

function detail(employeeID) {
    $("#identifyIdcardDiv").css("display","none");
    var index = layer.open({
        type: 1 //Page层类型
        , title: '详情'
        , btn: ['关闭']
        , shade: 0.6 //遮罩透明度
        , maxmin: false //允许全屏最小化
        , anim: 0 //0-6的动画形式，-1不开启
        , content: $('#staffAdd')
        ,yes: function(index, layero){
            layer.closeAll();
            $("#identifyIdcardDiv").css("display","block");
            $("#staffAdd input").val("");
            $("#staffAdd select").val("");
            $('#imgUrl').removeAttr('src');

        }
        ,cancel: function(index, layero){
            layer.closeAll();
            $("#staffAdd input").val("");
            $("#staffAdd select").val("");
            $("#staffAdd input").removeAttr("disabled");
            $("#staffAdd select").removeAttr("disabled");
            $('#imgUrl').removeAttr('src');
            $("#identifyIdcardDiv").css("display","block");
        }
    })
    $("#staffAdd input").attr("disabled","disabled");
    $("#staffAdd select").attr("disabled","disabled");
    $.ajax({
        url: "/erp/getempbyid",
        type: 'GET',
        data: {
            employeeID: employeeID
        },
        success: function (data) {
            if (data) {
                $("#employeeID").val(data.employeeID);
                $("#employeeName").val(data.employeeName);
                $("#gender").val(data.gender);
                $("#employeeNumber").val(data.employeeNumber);
                $("#groupName").val(data.groupName);
                $("#IDCard").val(data.idcard);
                $("#birthday").val(transDate(data.birthday));
                $("#nation").val(data.nation);
                $("#education").val(data.education);
                $("#birthPlace").val(data.birthPlace);
                $("#telephone").val(data.telephone);
                $("#identifyCard").val(data.identifyCard);
                $("#maritalState").val(data.maritalState);
                picUrl= data.imgUrl;
                $("#imgUrl").attr("src",data.tmpImgUrl);
                $("#emergencyContact").val(data.emergencyContact);
                $("#relation").val(data.relation);
                $("#emergencyPhone").val(data.emergencyPhone);
                $("#beginDate").val(transDate(data.beginDate));
                $("#positionState").val(data.positionState);
                $("#leaveDate").val(transDate(data.leaveDate));
                $("#bankCardNumber").val(data.bankCardNumber);
                $("#cardOfBank").val(data.cardOfBank);
                $("#isManagement").val(data.isManagement);
                $("#department").val(data.department);
                $("#position").val(data.position);
                $("#salaryType").val(data.salaryType);
                $("#contractBegin").val(transDate(data.contractBegin));
                $("#contractEnd").val(transDate(data.contractEnd));
                $("#province").val(data.province);
                $("#turner").val(data.turner);
                $("#role").val(data.role);
                var $image = $('#imgUrl');
                $image.viewer({
                    inline: false,
                    viewed: function() {
                        $image.viewer('zoomTo', 1);
                        $(".viewer-container").css("z-index",2022102211);
                    }
                });
                layui.use('form', function() {
                    var form = layui.form;
                    form.render();
                });
            }
        },
        error: function () {
        }
    })
    layer.full(index);

}


var mediaStreamTrack=null; // 视频对象(全局)
function openMedia() {
    var constraints = {
        video: { width: 480, height: 320 },
        audio: true
    };
    //获得video摄像头
    var video = document.getElementById('video');
    var promise = navigator.mediaDevices.getUserMedia(constraints);
    promise.then(function(mediaStream) {
        mediaStreamTrack = typeof mediaStream.stop === 'function' ? mediaStream : mediaStream.getTracks()[1];
    video.srcObject = mediaStream;
    video.play();
});
}

// 拍照
function takePhoto() {
    //获得Canvas对象
    var video = document.getElementById('video');
    var canvas = document.getElementById('canvas');
    var ctx = canvas.getContext('2d');
    ctx.drawImage(video, 0, 0, 480, 320);


    // toDataURL  ---  可传入'image/png'---默认, 'image/jpeg'
    var img = document.getElementById('canvas').toDataURL();
    // 这里的img就是得到的图片
    var index = layer.load();
    $.ajax({
        url: "/erp/identifyIdcard",
        type: 'POST',
        data: {
            imgUrl: img
        },
        success: function (data) {
            if (data.flag) {
                $("#employeeName").val(data.employeeName);
                $("#gender").val(data.gender);
                $("#IDCard").val(data.IDCard);
                $("#birthday").val(data.birthday);
                $("#birthPlace").val(data.birthPlace);
                $("#nation").val(data.nation);
                layui.use('form', function() {
                    var form = layui.form;
                    form.render();
                });
                $("#imgUrl").attr("src",data.photo);
                var $image = $('#imgUrl');
                $image.viewer({
                    inline: false,
                    viewed: function() {
                        $image.viewer('zoomTo', 1);
                        $(".viewer-container").css("z-index",2022102211);
                    }
                });
                picUrl = data.photo;
                layer.close(index);
            }else {
                layer.close(index);
                swal({
                    type: "error",
                    title: "对不起",
                    text: "<span style=\"font-weight:bolder;font-size: 20px\">"+data.msg+"，请重新拍摄</span>",
                    html: true
                });
            }
        },
        error: function () {
            layer.close(index);
        }
    })

}


// 拍照
function takeBankCardPhoto() {
    //获得Canvas对象
    var video = document.getElementById('video');
    var canvas = document.getElementById('canvas');
    var ctx = canvas.getContext('2d');
    ctx.drawImage(video, 0, 0, 480, 320);


    // toDataURL  ---  可传入'image/png'---默认, 'image/jpeg'
    var img = document.getElementById('canvas').toDataURL();
    // 这里的img就是得到的图片
    var index = layer.load();
    $.ajax({
        url: "/erp/identifyBankCard",
        type: 'POST',
        data: {
            imgUrl: img
        },
        success: function (data) {
            if (data.flag) {
                $("#bankCardNumber").val(data.bank_card_number);
                $("#cardOfBank").val(data.bank_name);
                layer.close(index);
            }else {
                layer.close(index);
                swal({
                    type: "error",
                    title: "对不起",
                    text: "<span style=\"font-weight:bolder;font-size: 20px\">"+data.msg+"，请重新拍摄</span>",
                    html: true
                });
            }
        },
        error: function () {
            layer.close(index);
        }
    })

}

// 关闭摄像头
function closeMedia() {
    mediaStreamTrack.stop();
}


function transDate(data, type, full, meta) {
    if (data == null){
        return "";
    }else {
        return  moment(data).format("YYYY-MM-DD");
    }
}


