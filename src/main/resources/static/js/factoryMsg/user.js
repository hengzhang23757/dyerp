layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.6'
}).extend({                         // 模块别名
    soulTable: 'soulTable'
});
var userTable;
var userRole = $("#userRole").val();
layui.use(['form', 'soulTable', 'table', 'upload','yutons_sug'], function () {
    var table = layui.table,
        form = layui.form,
        soulTable = layui.soulTable;
    var load = layer.load();
    userTable = table.render({
        elem: '#userTable'
        ,url:'getalluser'
        ,cols: [[]]
        ,loading:true
        ,done: function (res, curr, count) {
            table.init('userTable',{//转换成静态表格
                cols:[[
                    {type:'numbers', align:'center', title:'序号', width:150}
                    ,{field:'department', title:'部门', align:'center', width:200, sort: true, filter: true}
                    ,{field:'positionName', title:'职位', align:'center', width:200, sort: true, filter: true}
                    ,{field:'userName', title:'用户名', align:'center', width:200, sort: true, filter: true}
                    ,{field:'passWord', title:'密码', align:'center', width:200, sort: true, filter: true}
                    ,{field: 'userID', title:'操作', align:'center', toolbar: '#barTop', width:120}
                ]]
                ,data:res.data
                ,height: 'full-50'
                ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                ,defaultToolbar: ['filter', 'print']
                ,title: '用户表'
                ,totalRow: true
                ,page: true
                ,limits: [50, 100, 200]
                ,limit: 100 //每页默认显示的数量
                ,overflow: 'tips'
                ,done: function () {
                    soulTable.render(this);
                    layer.close(load);
                }
                ,filter: {
                    bottom: true,
                    items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                }
            });
        }
    });


    table.on('toolbar(userTable)', function(obj) {
        if (obj.event === 'refresh') {
            userTable.reload();
        } else if (obj.event === 'addUser') {
            var index = layer.open({
                type: 1 //Page层类型
                , title: '添加用户'
                , btn: ['添加','取消']
                , shade: 0.6 //遮罩透明度
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , content: $('#userInput')
                , yes: function (index, layero) {
                    var department= $("#department").val();
                    var positionName= $("#positionName").val();
                    var userName= $("#userName").val();
                    var passWord= $("#passWord").val();
                    if (department == null || department == "" || positionName == null || positionName == "" || userName == null || userName == ""|| passWord == null || passWord == ""){
                        layer.msg("请填写完整", { icon: 2 });
                        return false;
                    }
                    $.ajax({
                        url: "/erp/adduser",
                        type: 'POST',
                        data: {
                            userName: userName,
                            passWord: passWord,
                            department: department,
                            positionName: $("#positionName").find("option:selected").text(),
                            role: $("#positionName").val()
                        },
                        success: function (res) {
                            if (res == 0){
                                userTable.reload();
                                layer.msg("录入成功！");
                                layer.closeAll();
                            } else if (res == 3){
                                layer.msg("用户已存在！");
                            } else {
                                layer.msg("添加失败！");
                            }
                        }, error: function () {
                            layer.msg("添加失败！", {icon: 2});
                        }
                    })
                }
                ,btn2: function(){
                    $("#department").val();
                    $("#positionName").val();
                    $("#userName").val();
                    $("#passWord").val();
                    layer.closeAll();
                }
                , cancel: function (i, layero) {
                    $("#department").val();
                    $("#positionName").val();
                    $("#userName").val();
                    $("#passWord").val();
                    layer.closeAll();
                }
            });
            layer.full(index);
            form.render();
            return false;
        }
    });

    //监听行工具事件
    table.on('tool(userTable)', function(obj){
        var data = obj.data;
        if(obj.event === 'del'){
            layer.confirm('真的删除吗', function(index){
                $.ajax({
                    url: "/erp/deleteuser",
                    type: 'POST',
                    data: {
                        userID: data.userID
                    },
                    success: function (res) {
                        if (res == 0) {
                            obj.del();
                            layer.close(index);
                            layer.msg("删除成功！", {icon: 1});
                        } else {
                            layer.msg("删除失败！", {icon: 2});
                        }
                    },
                    error: function () {
                        layer.msg("删除失败！", {icon: 2});
                    }
                })

            });
        }
    })

});