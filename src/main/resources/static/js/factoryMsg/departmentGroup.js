layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.6'
}).extend({                         // 模块别名
    soulTable: 'soulTable'
});
var deptTable;
layui.use(['form', 'soulTable', 'table'], function () {
    var table = layui.table,
        soulTable = layui.soulTable;
    var load = layer.load();
    deptTable = table.render({
        elem: '#deptTable'
        ,url:'getalldepartmentgroup'
        ,cols: [[]]
        ,loading:true
        ,done: function (res, curr, count) {
            table.init('deptTable',{//转换成静态表格
                cols:[[
                    {type:'numbers', align:'center', title:'序号', width:150}
                    ,{field:'departmentName', title:'部门', align:'center', width:250, sort: true, filter: true}
                    ,{field:'groupName', title:'组名', align:'center', width:250, sort: true, filter: true}
                    ,{field:'departmentId', title:'考勤机号', align:'center', width:250, sort: true, filter: true}
                    ,{field: 'id', title:'操作', align:'center', toolbar: '#barTop', width:120}
                ]]
                ,data:res.data
                ,height: 'full-50'
                ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                ,defaultToolbar: ['filter', 'print']
                ,title: '部门组名'
                ,totalRow: true
                ,page: true
                ,limits: [50, 100, 200]
                ,limit: 100 //每页默认显示的数量
                ,overflow: 'tips'
                ,done: function () {
                    soulTable.render(this);
                    layer.close(load);
                }
                ,filter: {
                    bottom: true,
                    items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                }
            });
        }
    });


    table.on('toolbar(deptTable)', function(obj) {
        if (obj.event === 'refresh') {
            deptTable.reload();
        } else if (obj.event === 'add') {
            var index = layer.open({
                type: 1 //Page层类型
                , title: '添加'
                , btn: ['添加','取消']
                , shade: 0.6 //遮罩透明度
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , area: ['345px', '200px']
                , content: "<table>\n" +
                    "                <tr>\n" +
                    "                    <td style=\"text-align: right;margin-bottom: 15px;margin-top: 15px;width: 100px\">\n" +
                    "                        <label class=\"layui-form-label\">部门</label>\n" +
                    "                    </td>\n" +
                    "                    <td style=\"margin-bottom: 15px;margin-top: 15px\">\n" +
                    "                        <input type=\"text\" id=\"departmentName\" class=\"layui-input\">\n" +
                    "                    </td>\n" +
                    "                </tr>\n" +
                    "                <tr>\n" +
                    "                    <td style=\"text-align: right;margin-bottom: 15px;width: 100px\">\n" +
                    "                        <label class=\"layui-form-label\">组名</label>\n" +
                    "                    </td>\n" +
                    "                    <td style=\"margin-bottom: 15px;\">\n" +
                    "                        <input type=\"text\" id=\"groupName\" class=\"layui-input\">\n" +
                    "                    </td>\n" +
                    "                </tr>\n" +
                    "            </table>"
                , yes: function (index, layero) {
                    var departmentName= $("#departmentName").val();
                    var groupName= $("#groupName").val();
                    var param = {};
                    param.departmentName = departmentName;
                    param.groupName = groupName;
                    if (departmentName == null || departmentName == "" || groupName == null || groupName == ""){
                        layer.msg("请填写完整", { icon: 2 });
                        return false;
                    }
                    $.ajax({
                        url: "/erp/adddepartmentgroup",
                        type: 'POST',
                        data: param,
                        success: function (res) {
                            layer.closeAll();
                            if (res.result == 0){
                                deptTable.reload();
                                layer.msg("录入成功！", {icon: 1});
                            }
                        }, error: function () {
                            layer.msg("添加失败！", {icon: 2});
                        }
                    })
                }
                ,btn2: function(){
                    $("#departmentName").val();
                    $("#groupName").val();
                    layer.closeAll();
                }
                , cancel: function (i, layero) {
                    $("#departmentName").val();
                    $("#groupName").val();
                    layer.closeAll();
                }
            });
            return false;
        }
    });

    //监听行工具事件
    table.on('tool(deptTable)', function(obj){
        var data = obj.data;
        if(obj.event === 'del'){
            layer.confirm('真的删除吗', function(index){
                $.ajax({
                    url: "/erp/deletedepartmentgroup",
                    type: 'POST',
                    data: {
                        id: data.id
                    },
                    success: function (res) {
                        if (res.result == 0) {
                            obj.del();
                            layer.close(index);
                            layer.msg("删除成功！", {icon: 1});
                        } else {
                            layer.msg("删除失败！", {icon: 2});
                        }
                    },
                    error: function () {
                        layer.msg("删除失败！", {icon: 2});
                    }
                })

            });
        }
    })

});