var form;
layui.use('form', function(){
    form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
    form.render();
});

layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.3'
}).extend({                         // 模块别名
    soulTable: 'soulTable'
});
$(document).ready(function () {

    form.render('select');
    layui.laydate.render({
        elem: '#month',
        type: 'month',
        trigger: 'click',
        value: doHandleDate(),
        isInitValue: true
    });
    // layui.laydate.render({
    //     elem: '#to',
    //     trigger: 'click'
    // });
    //
    // layui.laydate.render({
    //     elem: '#in1'
    //     ,type: 'time'
    //     ,format: 'HH:mm'
    //     ,btns: ['clear', 'confirm']
    //     ,ready: formatminutes
    //
    // });
    // layui.laydate.render({
    //     elem: '#out1'
    //     ,type: 'time'
    //     ,format: 'HH:mm'
    //     ,btns: ['clear', 'confirm']
    //     ,ready: formatminutes
    //
    // });
    // layui.laydate.render({
    //     elem: '#in2'
    //     ,type: 'time'
    //     ,format: 'HH:mm'
    //     ,btns: ['clear', 'confirm']
    //     ,ready: formatminutes
    //
    // });
    // layui.laydate.render({
    //     elem: '#out2'
    //     ,type: 'time'
    //     ,format: 'HH:mm'
    //     ,btns: ['clear', 'confirm']
    //     ,ready: formatminutes
    //
    // });
    // layui.laydate.render({
    //     elem: '#in3'
    //     ,type: 'time'
    //     ,format: 'HH:mm'
    //     ,btns: ['clear', 'confirm']
    //     ,ready: formatminutes
    //
    // });
    // layui.laydate.render({
    //     elem: '#out3'
    //     ,type: 'time'
    //     ,format: 'HH:mm'
    //     ,btns: ['clear', 'confirm']
    //     ,ready: formatminutes
    //
    // });

    // function  formatminutes(date) {
    //     var aa = $(".laydate-time-list li ol")[1];
    //     var showtime = $($(".laydate-time-list li ol")[1]).find("li");
    //     for (var i = 0; i < showtime.length; i++) {
    //         var t00 = showtime[i].innerText;
    //         if (t00 != "00" && t00 != "10" && t00 != "20" && t00 != "30" && t00 != "40" && t00 != "50") {
    //             showtime[i].remove()
    //         }
    //     }
    //     $($(".laydate-time-list li ol")[2]).find("li").remove();  //清空秒
    // }


    // layui.use(['autocomplete'], function () {
    //     layui.autocomplete.render({
    //         elem: $('#employeeName')[0],
    //         url: 'getempnamehint',
    //         template_val: '{{d}}',
    //         template_txt: '{{d}}',
    //         onselect: function (resp) {
    //             autoComplete(resp)
    //         }
    //     })
    // });


    $.ajax({
        url: "/erp/getallcheckgroupname",
        type: 'GET',
        data: {},
        success: function (res) {
            $("#groupName").empty();
            if (res.data) {
                $("#groupName").append("<option value=''>全部</option>");
                $.each(res.data, function(index,element){
                    $("#groupName").append("<option value='"+element+"'>"+element+"</option>");
                })
            } else {
                layer.msg("获取失败！", {icon: 2});
            }
            form.render("select");
        }, error: function () {
            layer.msg("获取失败！", {icon: 2});
        }
    });


});
var checkTable,from,to,employeeNumber;

layui.use(['form', 'soulTable', 'table'], function () {
    var table = layui.table,
        soulTable = layui.soulTable,
        $ = layui.$;
    var load = layer.load();

    $(".layui-form-select,.layui-input").css("padding-right",0);

    checkTable = table.render({
        elem: '#checkTable'
        ,url:'getchecksummarybymonth'
        ,where: {
            month: $("#month").val()
        }
        ,method:'post'
        ,cols:[[
            {type:'numbers', align:'center', title:'序号', width:60}
            ,{field:'employeeName', title:'姓名', align:'center', width:150, sort: true, filter: true, totalRowText: '合计'}
            ,{field:'employeeNumber', title:'工号', align:'center', width:150, sort: true, filter: true}
            ,{field:'departmentName', title:'部门', align:'center', width:150, sort: true, filter: true}
            ,{field:'groupName', title:'组名', align:'center', width:150, sort: true, filter: true}
            ,{field:'useraccount', title:'考勤机号', align:'center',width:160, sort: true, filter: true}
            ,{field:'workday', title:'排班(天)', align:'center', width:160, sort: true, filter: true}
            ,{field:'realday', title:'上班(天)', align:'center', width:160, sort: true, filter: true}
            ,{field:'worktimeHour', title:'排班(时)', align:'center', width:160, sort: true, filter: true}
            ,{field:'realtimeHour', title:'工作(时)', align:'center', width:180, sort: true, filter: true}
            ,{field:'notcheckin', title:'上班未打(次)', align:'center', width:150, sort: true, filter: true}
            ,{field:'notcheckout', title:'下班未打(次)', align:'center', width:150, sort: true, filter: true}
            ,{field:'latertimes', title:'迟到(次)', align:'center', width:150, sort: true, filter: true}
            ,{field:'latetimeHour', title:'迟到(时)', align:'center', width:150, sort: true, filter: true}
            ,{field:'earlytimes', title:'早退(次)', align:'center', width:150, sort: true, filter: true}
            ,{field:'earlytimeHour', title:'早退(时)', align:'center', width:150, sort: true, filter: true}
            ,{field:'absenttimes', title:'旷工(次)', align:'center', width:150, sort: true, filter: true}
            ,{field:'absenttimeHour', title:'旷工(时)', align:'center', width:150, sort: true, filter: true}
            ,{fixed: 'right', title:'操作', align:'center', toolbar: '#barTop', width:90}
        ]]
        ,loading:true
        ,data:[]
        ,overflow: 'tips'
        ,title: '考勤月报'
        ,height: 'full-80'
        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
        ,totalRow: true
        ,page: true
        ,limits: [50, 100, 200]
        ,limit: 100 //每页默认显示的数量
        ,done: function () {
            soulTable.render(this);
            layer.close(load);
        }
        ,filter: {
            bottom: true,
            clearFilter: false,
            items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
        }
    });

    //监听提交
    form.on('submit(searchBeat)', function(data){
        var data = {};
        var month = $("#month").val();
        var groupName = $("#groupName").val();
        data.month = month;
        if (groupName != "" || groupName != null){
            data.groupName = groupName;
        }
        if (month == "" || month == null){
            layer.msg("月份必须", { icon: 2 });
        }else {
            var load = layer.load(2);
            $.ajax({
                url: "/erp/getchecksummarybymonth",
                type: 'POST',
                data: data,
                success: function (res) {
                    if (res.data) {
                        layui.table.reload("checkTable", {
                            data: res.data   // 将新数据重新载入表格
                        });
                        layer.close(load);
                    } else {
                        layer.msg("获取失败！", {icon: 2});
                    }
                }, error: function () {
                    layer.msg("获取失败！", {icon: 2});
                }
            });
        }
        return false;
    });

    table.on('toolbar(checkTable)', function(obj) {
        if (obj.event === 'clearFilter') {
            // 清除所有筛选条件并重载表格
            // 参数: tableId
            soulTable.clearFilter('checkTable')
        } else if (obj.event === 'exportExcel') {
            soulTable.export('checkTable');
        }
    })

    // table.on('tool(checkTable)', function(obj){
    //     var data = obj.data;
    //     if(obj.event === 'updateCheckDetail'){
    //         var index = layer.open({
    //             type: 1 //Page层类型
    //             , title: '修改考勤信息'
    //             , btn: ['保存']
    //             , shade: 0.6 //遮罩透明度
    //             , maxmin: false //允许全屏最小化
    //             , anim: 0 //0-6的动画形式，-1不开启
    //             , content: $('#checkDetailUpdate')
    //             ,yes: function(index, layero){
    //                 var params = layui.form.val("checkDetailUpdate");
    //                 $.ajax({
    //                     url: "/erp/updatecheckdetail",
    //                     type: 'POST',
    //                     data: {
    //                         checkDetailJson:JSON.stringify(params)
    //                     },
    //                     success: function (res) {
    //                         if (res.result == 0) {
    //                             layer.close(index);
    //                             layer.msg("保存成功！", {icon: 1});
    //
    //                             var reloadParm = {};
    //                             reloadParm.from = from;
    //                             reloadParm.to = to;
    //                             reloadParm.employeeNumber = employeeNumber;
    //                             if (from != "" && to != "" && employeeNumber != ""){
    //                                 var load = layer.load(2);
    //                                 layui.table.reload("checkTable", {
    //                                     data: []   // 将新数据重新载入表格
    //                                 });
    //                                 $.ajax({
    //                                     url: "/erp/getcheckdetailbyinfo",
    //                                     type: 'GET',
    //                                     data: reloadParm,
    //                                     success: function (res) {
    //                                         if (res.data) {
    //                                             layui.table.reload("checkTable", {
    //                                                 data: res.data   // 将新数据重新载入表格
    //                                             });
    //                                             layer.close(load);
    //                                         } else {
    //                                             layer.msg("获取失败！", {icon: 2});
    //                                         }
    //                                     }, error: function () {
    //                                         layer.msg("获取失败！", {icon: 2});
    //                                     }
    //                                 });
    //                             }else {
    //                                 $.ajax({
    //                                     url: "/erp/getyesterdaycheckdetail",
    //                                     type: 'GET',
    //                                     data: {},
    //                                     success: function (res) {
    //                                         if (res.data) {
    //                                             layui.table.reload("checkTable", {
    //                                                 data: res.data   // 将新数据重新载入表格
    //                                             });
    //                                         } else {
    //                                             layer.msg("获取失败！", {icon: 2});
    //                                         }
    //                                     }, error: function () {
    //                                         layer.msg("获取失败！", {icon: 2});
    //                                     }
    //                                 });
    //                             }
    //                             return false;
    //                             $("#checkDetailUpdate").find("input").val("");
    //                         } else {
    //                             layer.msg("保存失败！", {icon: 2});
    //                         }
    //                     },
    //                     error: function () {
    //                         layer.msg("保存失败！", {icon: 2});
    //                     }
    //                 })
    //             }
    //             , cancel : function (i,layero) {
    //                 $("#checkDetailUpdate").find("input").val("");
    //             }
    //         });
    //         layer.full(index);
    //         $("#checkDetailUpdate input[name='employeeNumber']").val(data.employeeNumber);
    //         $("#checkDetailUpdate input[name='employeeName']").val(data.employeeName);
    //         $("#checkDetailUpdate input[name='signDate']").val(data.signDate);
    //         $("#checkDetailUpdate input[name='in1']").val(data.in1);
    //         $("#checkDetailUpdate input[name='out1']").val(data.out1);
    //         $("#checkDetailUpdate input[name='in2']").val(data.in2);
    //         $("#checkDetailUpdate input[name='out2']").val(data.out2);
    //         $("#checkDetailUpdate input[name='in3']").val(data.in3);
    //         $("#checkDetailUpdate input[name='out3']").val(data.out3);
    //         $("#checkDetailUpdate input[name='workDays']").val(data.workDays);
    //         $("#checkDetailUpdate input[name='absentDays']").val(data.absentDays);
    //         $("#checkDetailUpdate input[name='factHours']").val(data.factHours);
    //         $("#checkDetailUpdate input[name='otHours']").val(data.otHours);
    //         $("#checkDetailUpdate input[name='lateMinutes1']").val(data.lateMinutes1);
    //         $("#checkDetailUpdate input[name='leaveMinutes1']").val(data.leaveMinutes1);
    //         $("#checkDetailUpdate input[name='absentHours1']").val(data.absentHours1);
    //         $("#checkDetailUpdate select[name='notes']").append("<option selected value='"+data.notes+"'>"+data.notes+"</option>");
    //         layui.form.render("select");
    //     }
    // })
});



// function autoComplete(employeeName) {
//     $("#employeeNumber").empty();
//     $.ajax({
//         url: "/erp/getempnumbersbyname",
//         data: {"employeeName": employeeName},
//         success:function(data){
//             $("#employeeNumber").empty();
//             if (data.employeeNumberList) {
//                 $("#employeeNumber").val(data.employeeNumberList[0]);
//             }
//         },
//         error:function(){
//         }
//     });
// }
function doHandleDate() {
    var myDate = new Date();
    var tYear = myDate.getFullYear();
    var tMonth = myDate.getMonth();

    var m = tMonth + 1;
    if (m.toString().length == 1) {
        m = "0" + m;
    }
    return tYear +'-'+ m;
}