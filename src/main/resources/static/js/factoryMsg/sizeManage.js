layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.6'
}).extend({                         // 模块别名
    soulTable: 'soulTable'
});
var sizeNameTable;
var userRole = $("#userRole").val();
layui.use(['form', 'soulTable', 'table', 'upload','yutons_sug'], function () {
    var table = layui.table,
        soulTable = layui.soulTable;
    var load = layer.load();
    sizeNameTable = table.render({
        elem: '#sizeNameTable'
        ,url:'getallsizenamemanage'
        ,method:'post'
        ,cols: [[]]
        ,loading:true
        ,done: function (res, curr, count) {
            table.init('sizeNameTable',{//转换成静态表格
                cols:[[
                    {type:'numbers', align:'center', title:'序号', width:150}
                    ,{field:'sizeGroup', title:'尺码组', align:'center', width:250, sort: true, filter: true}
                    ,{field:'sizeName', title:'尺码名称', align:'center', width:250, sort: true, filter: true}
                    ,{field: 'storageID', title:'操作', align:'center', toolbar: '#barTop', width:120}
                ]]
                ,data:res.data
                ,height: 'full-50'
                ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                ,defaultToolbar: ['filter', 'print']
                ,title: '尺码表'
                ,totalRow: true
                ,page: true
                ,limits: [50, 100, 200]
                ,limit: 100 //每页默认显示的数量
                ,overflow: 'tips'
                ,done: function () {
                    soulTable.render(this);
                    layer.close(load);
                }
                ,filter: {
                    bottom: true,
                    items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                }
            });
        }
    });


    table.on('toolbar(sizeNameTable)', function(obj) {
        if (obj.event === 'refresh') {
            sizeNameTable.reload();
        } else if (obj.event === 'addSize') {
            var index = layer.open({
                type: 1 //Page层类型
                , title: '添加尺码'
                , btn: ['添加','取消']
                , shade: 0.6 //遮罩透明度
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , area: ['345px', '200px']
                , content: $('#sizeInput')
                , yes: function (index, layero) {
                    var sizeGroup= $("#sizeGroup").val();
                    var sizeName= $("#sizeName").val();
                    var param = {};
                    param.sizeGroup = sizeGroup;
                    param.sizeName = sizeName;
                    if (sizeGroup == null || sizeGroup == "" || sizeName == null || sizeName == ""){
                        layer.msg("请填写完整", { icon: 2 });
                        return false;
                    }
                    $.ajax({
                        url: "/erp/addsizemanageone",
                        type: 'POST',
                        data: param,
                        success: function (res) {
                            layer.closeAll();
                            if (res.result == 0){
                                sizeNameTable.reload();
                                layer.msg("录入成功！", {icon: 1});
                            }
                        }, error: function () {
                            layer.msg("添加失败！", {icon: 2});
                        }
                    })
                }
                ,btn2: function(){
                    $("#sizeGroup").val();
                    $("#sizeName").val();
                    layer.closeAll();
                }
                , cancel: function (i, layero) {
                    $("#sizeGroup").val();
                    $("#sizeName").val();
                    layer.closeAll();
                }
            });
            return false;
        }
    });

    //监听行工具事件
    table.on('tool(sizeNameTable)', function(obj){
        var data = obj.data;
        if(obj.event === 'del'){
            layer.confirm('真的删除吗', function(index){
                $.ajax({
                    url: "/erp/deletesizemanagebyid",
                    type: 'POST',
                    data: {
                        sizeManageID: data.sizeManageID
                    },
                    success: function (res) {
                        if (res.result == 0) {
                            obj.del();
                            layer.close(index);
                            layer.msg("删除成功！", {icon: 1});
                        } else {
                            layer.msg("删除失败！", {icon: 2});
                        }
                    },
                    error: function () {
                        layer.msg("删除失败！", {icon: 2});
                    }
                })

            });
        }
    })

});