var form;
layui.use('form', function(){
    form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
    form.render();
});
var userRole = $("#userRole").val();
layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.20'
}).extend({
    soulTable: 'soulTable'
});
$(document).ready(function () {
    layui.laydate.render({
        elem: '#monthStr',
        type: 'month',
        trigger: 'click',
        value: doHandleDate(),
        isInitValue: true
    });
    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumber", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });
});
$(document).on('keydown', '.layui-table-edit', function (e) {
    var td = $(this).parent('td')
        ,tr = td.parent('tr')
        ,trs = tr.parent().parent().find('tr')
        ,tr_index = tr.index()
        ,td_index = td.index()
        ,td_last_index = tr.find('[data-edit="text"]:last').index()
        ,td_first_index = tr.find('[data-edit="text"]:first').index();
    switch (e.keyCode) {
        case 13:
            tr.next().find('td').eq(td_index).click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            return false;

        case 39:
            td.nextAll('[data-edit="text"]:first').click();
            if (td_index == td_last_index){
                tr.next().find('td').eq(td_first_index).click();
                if (tr_index == trs.length - 1){
                    trs.eq(0).find('td').eq(td_first_index).click();
                }
            }
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
        case 37:
            td.prevAll('[data-edit="text"]:first').click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
        case 38:
            tr.prev().find('td').eq(td_index).click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
        case 40:
            tr.next().find('td').eq(td_index).click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
    }

});
var reportTable;
layui.use(['form', 'table', 'soulTable'], function () {
    var table = layui.table,
        soulTable = layui.soulTable,
        form = layui.form,
        $ = layui.$;
    reportTable = table.render({
        elem: '#reportTable'
        ,cols:[[]]
        ,excel: {
            filename: '成品进销存.xlsx'
        }
        ,height: 'full-130'
        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
        ,title: '成品进销存'
        ,totalRow: true
        ,loading: false
        ,page: true
        ,even: true
        ,limits: [500, 1000, 2000]
        ,limit: 1000 //每页默认显示的数量
        ,done: function () {
            soulTable.render(this);
        }
    });

    initTable('');

    function initTable(orderName){
        var param = {};
        if (orderName != null && orderName != ''){
            param.orderName = orderName;
        }
        $.ajax({
            url: "/erp/getendproductalltyperecord",
            type: 'GET',
            data: param,
            success: function (res) {
                if (res.endProductList) {
                    var reportData = res.endProductList;
                    table.render({
                        elem: '#reportTable'
                        ,cols:[[
                            {title: '#', width: '5%', collapse: true,lazy: true, icon: ['layui-icon layui-icon-triangle-r', 'layui-icon layui-icon-triangle-d'], children:[
                                    {
                                        title: '正品入库'
                                        ,url: 'getendproductinstorebyorder'
                                        ,where: function(row){
                                            return {
                                                orderName: row.orderName,
                                                endType: "正品"
                                            }
                                        }
                                        ,height: 300
                                        ,loading:true
                                        ,page: false
                                        ,toolbar: '<div><a class="layui-btn layui-btn-danger layui-btn-sm" lay-event="batchDeleteIn">批量删除</a></div>'
                                        ,totalRow: true
                                        ,cols: [[
                                            {type: 'checkbox', fixed: 'left'},
                                            {field: 'id', hide: true, fixed: 'left'},
                                            {field: 'clothesVersionNumber', title: '单号',align:'center', minWidth: 120},
                                            {field: 'orderName', title: '款号',align:'center', minWidth: 120},
                                            {field: 'colorName', title: '颜色',align:'center', minWidth: 120, totalRow: true},
                                            {field: 'sizeName', title: '尺码',align:'center', minWidth: 120},
                                            {field: 'inStoreCount', title: '入库数量',align:'center', minWidth: 120, totalRow: true},
                                            {field: 'createTime', title: '日期',align:'center', minWidth: 120,templet: function (d) {
                                                    return moment(d.createTime).format("YYYY-MM-DD");
                                                }},
                                            {title: '操作', minWidth: 120,align:'center', templet: function(row) {
                                                    return '<a class="layui-btn layui-btn-xs" lay-event="childEdit">修改</a>';
                                                }}
                                        ]]
                                        ,toolEvent: function (obj, pobj) {
                                            // obj 子表当前行对象
                                            // pobj 父表当前行对象
                                            var filterOrderName = $("#orderName").val();
                                            var rowData = obj.data;
                                            var childId = this.id; // 通过 this 对象获取当前子表的id
                                            if (obj.event === 'childEdit') {
                                                var index = layer.open({
                                                    type: 1 //Page层类型
                                                    , title: '修改'
                                                    , btn: ['保存','取消']
                                                    , shade: 0.6 //遮罩透明度
                                                    , maxmin: false //允许全屏最小化
                                                    , anim: 0 //0-6的动画形式，-1不开启
                                                    , area: ['345px', '200px']
                                                    , content: "<div>\n" +
                                                        "            <table>\n" +
                                                        "                <tr>\n" +
                                                        "                    <td style=\"text-align: right;margin-bottom: 15px;width: 100px\">\n" +
                                                        "                        <label class=\"layui-form-label\">原数量</label>\n" +
                                                        "                    </td>\n" +
                                                        "                    <td style=\"margin-bottom: 15px;\">\n" +
                                                        "                        <input type=\"text\" name=\"initCount\" id=\"initCount\" readonly class=\"layui-input\">\n" +
                                                        "                    </td>\n" +
                                                        "                </tr>\n" +
                                                        "                <tr>\n" +
                                                        "                    <td style=\"text-align: right;margin-bottom: 15px;width: 100px\">\n" +
                                                        "                        <label class=\"layui-form-label\">修改成</label>\n" +
                                                        "                    </td>\n" +
                                                        "                    <td style=\"margin-bottom: 15px;\">\n" +
                                                        "                        <input type=\"text\" name=\"toCount\" id=\"toCount\" class=\"layui-input\">\n" +
                                                        "                    </td>\n" +
                                                        "                </tr>\n" +
                                                        "            </table>\n" +
                                                        "    </div>"
                                                    , yes: function (index, layero) {
                                                        var initCount= $("#initCount").val();
                                                        var toCount= $("#toCount").val();
                                                        var param = {};
                                                        param.orderName = rowData.orderName;
                                                        param.initCount = initCount;
                                                        param.id = rowData.id;
                                                        param.toCount = toCount;
                                                        param.colorName = rowData.colorName;
                                                        param.sizeName = rowData.sizeName;
                                                        param.endType = "正品";
                                                        if (toCount == null || toCount == ""){
                                                            layer.msg("请填写完整", { icon: 2 });
                                                            return false;
                                                        }
                                                        $.ajax({
                                                            url: "/erp/updateinstorecount",
                                                            type: 'POST',
                                                            data: param,
                                                            success: function (res) {
                                                                layer.closeAll();
                                                                if (res.result == 0){
                                                                    initTable(filterOrderName);
                                                                    layer.msg("修改成功！", {icon: 1});
                                                                } else if (res.result == 1){
                                                                    layer.msg("修改失败！", {icon: 2});
                                                                } else if (res.result == 3){
                                                                    layer.msg("数据故障,不能修改！");
                                                                } else if (res.result == 4){
                                                                    layer.msg("库存不足,无法修改！");
                                                                }
                                                            }, error: function () {
                                                                layer.msg("添加失败！", {icon: 2});
                                                            }
                                                        })
                                                    }
                                                    ,btn2: function(){
                                                        $("#initCount").val();
                                                        $("#toCount").val();
                                                        layer.closeAll();
                                                    }
                                                    , cancel: function (i, layero) {
                                                        $("#initCount").val();
                                                        $("#toCount").val();
                                                        layer.closeAll();
                                                    }
                                                });
                                                $("#initCount").val(rowData.inStoreCount);
                                                return false;
                                            }
                                        }
                                        ,toolbarEvent: function (obj, pobj) {
                                            // obj 子表当前行对象
                                            // pobj 父表当前行对象
                                            var objData = obj.data;
                                            var childId = this.id; // 通过 this 对象获取当前子表的id
                                            if (obj.event === 'batchDeleteIn'){
                                                var checkStatus = table.checkStatus(obj.config.id);
                                                if(checkStatus.data.length == 0) {
                                                    layer.msg('请选择数据');
                                                }else {
                                                    var idList = [];
                                                    var inStoreCount = 0;
                                                    var orderName;
                                                    $.each(checkStatus.data, function (index, item) {
                                                        inStoreCount += item.inStoreCount;
                                                        idList.push(item.id);
                                                        orderName = item.orderName;
                                                    });
                                                    layer.confirm('真的删除吗', function(index){
                                                        $.ajax({
                                                            url: "/erp/deleteendproductinbatch",
                                                            type: 'POST',
                                                            data: {
                                                                idList: idList,
                                                                orderName: orderName,
                                                                endType: "正品"
                                                            },
                                                            traditional: true,
                                                            success: function (res) {
                                                                if (res.msg) {
                                                                    layer.close(index);
                                                                    layer.msg(res.msg);
                                                                } else if (res.result == 0) {
                                                                    layer.close(index);
                                                                    layer.msg("删除成功！", {icon: 1});
                                                                    table.reload(childId);
                                                                } else {
                                                                    layer.msg("删除失败！", {icon: 2});
                                                                }
                                                            }, error: function () {
                                                                layer.msg("删除失败！", {icon: 2});
                                                            }
                                                        })
                                                    });
                                                }
                                            }
                                        }
                                        ,done: function () {
                                            soulTable.render(this);
                                        }
                                    }, {
                                        title: '正品库存'
                                        ,url: 'getendproductstoragebyorder'
                                        ,where: function(row){
                                            return {
                                                orderName: row.orderName,
                                                endType: "正品"
                                            }
                                        }
                                        ,height: 300
                                        ,loading:true
                                        ,page: false
                                        ,cols: [[
                                            {field: 'clothesVersionNumber', title: '单号',align:'center', minWidth: 120},
                                            {field: 'orderName', title: '款号',align:'center', minWidth: 120},
                                            {field: 'colorName', title: '颜色',align:'center', minWidth: 120},
                                            {field: 'sizeName', title: '尺码',align:'center', minWidth: 120 },
                                            {field: 'storageCount', title: '数量',align:'center', minWidth: 120, totalRow: true},
                                            {field: 'createTime', title: '最后操作',align:'center', minWidth: 120,templet: function (d) {
                                                    return moment(d.cutDate).format("YYYY-MM-DD");
                                                }}
                                        ]]
                                        ,done: function () {
                                            soulTable.render(this);
                                        }
                                    }, {
                                        title: '正品出库'
                                        ,url: 'getendproductoutstorebyorder'
                                        ,where: function(row){
                                            return {
                                                orderName: row.orderName,
                                                endType: "正品"
                                            }
                                        }
                                        ,height: 300
                                        ,loading:true
                                        ,page: false
                                        ,toolbar: '<div><a class="layui-btn layui-btn-danger layui-btn-sm" lay-event="batchDeleteOut">批量删除</a></div>'
                                        ,totalRow: true
                                        ,cols: [[
                                            {type: 'checkbox', fixed: 'left'},
                                            {field: 'id', hide: true, fixed: 'left'},
                                            {field: 'clothesVersionNumber', title: '单号',align:'center', minWidth: 120},
                                            {field: 'orderName', title: '款号',align:'center', minWidth: 120},
                                            {field: 'colorName', title: '颜色',align:'center', minWidth: 120},
                                            {field: 'sizeName', title: '尺码',align:'center', minWidth: 120 },
                                            {field: 'outStoreCount', title: '数量',align:'center', minWidth: 120, totalRow: true},
                                            {field: 'createTime', title: '最后操作',align:'center', minWidth: 120,templet: function (d) {
                                                    return moment(d.createTime).format("YYYY-MM-DD");
                                                }}
                                        ]]
                                        ,toolbarEvent: function (obj, pobj) {
                                            // obj 子表当前行对象
                                            // pobj 父表当前行对象
                                            var objData = obj.data;
                                            var childId = this.id; // 通过 this 对象获取当前子表的id
                                            if (obj.event === 'batchDeleteOut'){
                                                var checkStatus = table.checkStatus(obj.config.id);
                                                if(checkStatus.data.length == 0) {
                                                    layer.msg('请选择数据');
                                                }else {
                                                    var idList = [];
                                                    var orderName;
                                                    $.each(checkStatus.data, function (index, item) {
                                                        idList.push(item.id);
                                                        orderName = item.orderName;
                                                    });
                                                    layer.confirm('真的删除吗', function(index){
                                                        $.ajax({
                                                            url: "/erp/deleteendproductoutbatch",
                                                            type: 'POST',
                                                            data: {
                                                                idList: idList,
                                                                orderName: orderName,
                                                                endType: "正品"
                                                            },
                                                            traditional: true,
                                                            success: function (res) {
                                                                if (res.result == 0) {
                                                                    layer.close(index);
                                                                    layer.msg("删除成功！", {icon: 1});
                                                                    table.reload(childId);
                                                                } else {
                                                                    layer.msg("删除失败！", {icon: 2});
                                                                }
                                                            }, error: function () {
                                                                layer.msg("删除失败！", {icon: 2});
                                                            }
                                                        })
                                                    });
                                                }
                                            }
                                        }
                                        ,done: function () {
                                            soulTable.render(this);
                                        }
                                    },{
                                        title: '次品入库'
                                        ,url: 'getdefectiveendproductinstorebyorder'
                                        ,where: function(row){
                                            return {
                                                orderName: row.orderName
                                            }
                                        }
                                        ,height: 300
                                        ,loading:true
                                        ,page: false
                                        ,toolbar: '<div><a class="layui-btn layui-btn-danger layui-btn-sm" lay-event="batchDeleteIn">批量删除</a></div>'
                                        ,totalRow: true
                                        ,cols: [[
                                            {type: 'checkbox', fixed: 'left'},
                                            {field: 'id', hide: true, fixed: 'left'},
                                            {field: 'clothesVersionNumber', title: '单号',align:'center', minWidth: 120},
                                            {field: 'orderName', title: '款号',align:'center', minWidth: 120},
                                            {field: 'colorName', title: '颜色',align:'center', minWidth: 120, totalRow: true},
                                            {field: 'sizeName', title: '尺码',align:'center', minWidth: 120},
                                            {field: 'inStoreCount', title: '入库数量',align:'center', minWidth: 120, totalRow: true},
                                            {field: 'defectiveType', title: '类别',align:'center', minWidth: 120, totalRow: true},
                                            {field: 'createTime', title: '日期',align:'center', minWidth: 120,templet: function (d) {
                                                    return moment(d.createTime).format("YYYY-MM-DD");
                                                }},
                                            {title: '操作', minWidth: 120,align:'center', templet: function(row) {
                                                    return '<a class="layui-btn layui-btn-xs" lay-event="childEdit">修改</a>';
                                                }}
                                        ]]
                                        ,toolEvent: function (obj, pobj) {
                                            // obj 子表当前行对象
                                            // pobj 父表当前行对象
                                            var filterOrderName = $("#orderName").val();
                                            var rowData = obj.data;
                                            var childId = this.id; // 通过 this 对象获取当前子表的id
                                            if (obj.event === 'childEdit') {
                                                var index = layer.open({
                                                    type: 1 //Page层类型
                                                    , title: '修改'
                                                    , btn: ['保存','取消']
                                                    , shade: 0.6 //遮罩透明度
                                                    , maxmin: false //允许全屏最小化
                                                    , anim: 0 //0-6的动画形式，-1不开启
                                                    , area: ['345px', '200px']
                                                    , content: "<div>\n" +
                                                        "            <table>\n" +
                                                        "                <tr>\n" +
                                                        "                    <td style=\"text-align: right;margin-bottom: 15px;width: 100px\">\n" +
                                                        "                        <label class=\"layui-form-label\">原数量</label>\n" +
                                                        "                    </td>\n" +
                                                        "                    <td style=\"margin-bottom: 15px;\">\n" +
                                                        "                        <input type=\"text\" name=\"initCount\" id=\"initCount\" readonly class=\"layui-input\">\n" +
                                                        "                    </td>\n" +
                                                        "                </tr>\n" +
                                                        "                <tr>\n" +
                                                        "                    <td style=\"text-align: right;margin-bottom: 15px;width: 100px\">\n" +
                                                        "                        <label class=\"layui-form-label\">修改成</label>\n" +
                                                        "                    </td>\n" +
                                                        "                    <td style=\"margin-bottom: 15px;\">\n" +
                                                        "                        <input type=\"text\" name=\"toCount\" id=\"toCount\" class=\"layui-input\">\n" +
                                                        "                    </td>\n" +
                                                        "                </tr>\n" +
                                                        "            </table>\n" +
                                                        "    </div>"
                                                    , yes: function (index, layero) {
                                                        var initCount= $("#initCount").val();
                                                        var toCount= $("#toCount").val();
                                                        var param = {};
                                                        param.orderName = rowData.orderName;
                                                        param.initCount = initCount;
                                                        param.id = rowData.id;
                                                        param.toCount = toCount;
                                                        param.colorName = rowData.colorName;
                                                        param.sizeName = rowData.sizeName;
                                                        param.endType = "次品";
                                                        if (toCount == null || toCount == ""){
                                                            layer.msg("请填写完整", { icon: 2 });
                                                            return false;
                                                        }
                                                        $.ajax({
                                                            url: "/erp/updatedefectiveinstorecount",
                                                            type: 'POST',
                                                            data: param,
                                                            success: function (res) {
                                                                layer.closeAll();
                                                                if (res.result == 0){
                                                                    initTable(filterOrderName);
                                                                    layer.msg("修改成功！", {icon: 1});
                                                                } else if (res.result == 1){
                                                                    layer.msg("修改失败！", {icon: 2});
                                                                } else if (res.result == 3){
                                                                    layer.msg("数据故障,不能修改！");
                                                                } else if (res.result == 4){
                                                                    layer.msg("库存不足,无法修改！");
                                                                }
                                                            }, error: function () {
                                                                layer.msg("添加失败！", {icon: 2});
                                                            }
                                                        })
                                                    }
                                                    ,btn2: function(){
                                                        $("#initCount").val();
                                                        $("#toCount").val();
                                                        layer.closeAll();
                                                    }
                                                    , cancel: function (i, layero) {
                                                        $("#initCount").val();
                                                        $("#toCount").val();
                                                        layer.closeAll();
                                                    }
                                                });
                                                $("#initCount").val(rowData.inStoreCount);
                                                return false;
                                            }
                                        }
                                        ,toolbarEvent: function (obj, pobj) {
                                            // obj 子表当前行对象
                                            // pobj 父表当前行对象
                                            var objData = obj.data;
                                            var childId = this.id; // 通过 this 对象获取当前子表的id
                                            if (obj.event === 'batchDeleteIn'){
                                                var checkStatus = table.checkStatus(obj.config.id);
                                                if(checkStatus.data.length == 0) {
                                                    layer.msg('请选择数据');
                                                }else {
                                                    var idList = [];
                                                    var inStoreCount = 0;
                                                    var orderName;
                                                    $.each(checkStatus.data, function (index, item) {
                                                        inStoreCount += item.inStoreCount;
                                                        idList.push(item.id);
                                                        orderName = item.orderName;
                                                    });
                                                    layer.confirm('真的删除吗', function(index){
                                                        $.ajax({
                                                            url: "/erp/deleteendproductinbatch",
                                                            type: 'POST',
                                                            data: {
                                                                idList: idList,
                                                                orderName: orderName,
                                                                endType: "次品"
                                                            },
                                                            traditional: true,
                                                            success: function (res) {
                                                                if (res.msg) {
                                                                    layer.close(index);
                                                                    layer.msg(res.msg);
                                                                } else if (res.result == 0) {
                                                                    layer.close(index);
                                                                    layer.msg("删除成功！", {icon: 1});
                                                                    table.reload(childId);
                                                                } else {
                                                                    layer.msg("删除失败！", {icon: 2});
                                                                }
                                                            }, error: function () {
                                                                layer.msg("删除失败！", {icon: 2});
                                                            }
                                                        })
                                                    });
                                                }
                                            }
                                        }
                                        ,done: function () {
                                            soulTable.render(this);
                                        }
                                    }, {
                                        title: '次品库存'
                                        ,url: 'getdefectiveendproductstoragebyorder'
                                        ,where: function(row){
                                            return {
                                                orderName: row.orderName
                                            }
                                        }
                                        ,height: 300
                                        ,loading:true
                                        ,page: false
                                        ,cols: [[
                                            {field: 'clothesVersionNumber', title: '单号',align:'center', minWidth: 120},
                                            {field: 'orderName', title: '款号',align:'center', minWidth: 120},
                                            {field: 'colorName', title: '颜色',align:'center', minWidth: 120},
                                            {field: 'sizeName', title: '尺码',align:'center', minWidth: 120 },
                                            {field: 'storageCount', title: '数量',align:'center', minWidth: 120, totalRow: true},
                                            {field: 'defectiveType', title: '类别',align:'center', minWidth: 120, totalRow: true},
                                            {field: 'createTime', title: '日期',align:'center', minWidth: 120,templet: function (d) {
                                                    return moment(d.createTime).format("YYYY-MM-DD");
                                                }}
                                        ]]
                                        ,done: function () {
                                            soulTable.render(this);
                                        }
                                    }, {
                                        title: '次品出库'
                                        ,url: 'getdefectiveendproductoutstorebyorder'
                                        ,where: function(row){
                                            return {
                                                orderName: row.orderName
                                            }
                                        }
                                        ,height: 300
                                        ,loading:true
                                        ,page: false
                                        ,toolbar: '<div><a class="layui-btn layui-btn-danger layui-btn-sm" lay-event="batchDeleteOut">批量删除</a></div>'
                                        ,totalRow: true
                                        ,cols: [[
                                            {type: 'checkbox', fixed: 'left'},
                                            {field: 'id', hide: true, fixed: 'left'},
                                            {field: 'clothesVersionNumber', title: '单号',align:'center', minWidth: 120},
                                            {field: 'orderName', title: '款号',align:'center', minWidth: 120},
                                            {field: 'colorName', title: '颜色',align:'center', minWidth: 120},
                                            {field: 'sizeName', title: '尺码',align:'center', minWidth: 120 },
                                            {field: 'outStoreCount', title: '数量',align:'center', minWidth: 120, totalRow: true},
                                            {field: 'defectiveType', title: '类别',align:'center', minWidth: 120, totalRow: true},
                                            {field: 'createTime', title: '最后操作',align:'center', minWidth: 120,templet: function (d) {
                                                    return moment(d.cutDate).format("YYYY-MM-DD");
                                                }},
                                            {field: 'remark', title: '备注',align:'center', minWidth: 120}
                                        ]]
                                        ,toolbarEvent: function (obj, pobj) {
                                            // obj 子表当前行对象
                                            // pobj 父表当前行对象
                                            var objData = obj.data;
                                            var childId = this.id; // 通过 this 对象获取当前子表的id
                                            if (obj.event === 'batchDeleteOut'){
                                                var checkStatus = table.checkStatus(obj.config.id);
                                                if(checkStatus.data.length == 0) {
                                                    layer.msg('请选择数据');
                                                }else {
                                                    var idList = [];
                                                    var orderName;
                                                    $.each(checkStatus.data, function (index, item) {
                                                        idList.push(item.id);
                                                        orderName = item.orderName;
                                                    });
                                                    layer.confirm('真的删除吗', function(index){
                                                        $.ajax({
                                                            url: "/erp/deletedefectiveendproductoutbatch",
                                                            type: 'POST',
                                                            data: {
                                                                idList: idList,
                                                                orderName: orderName,
                                                                endType: "次品"
                                                            },
                                                            traditional: true,
                                                            success: function (res) {
                                                                if (res.result == 0) {
                                                                    layer.close(index);
                                                                    layer.msg("删除成功！", {icon: 1});
                                                                    table.reload(childId);
                                                                } else {
                                                                    layer.msg("删除失败！", {icon: 2});
                                                                }
                                                            }, error: function () {
                                                                layer.msg("删除失败！", {icon: 2});
                                                            }
                                                        })
                                                    });
                                                }
                                            }
                                        }
                                        ,done: function () {
                                            soulTable.render(this);
                                        }
                                    }
                                ]}
                            ,{type:'numbers', align:'center', title:'序号', width: '5%'}
                            ,{field:'clothesVersionNumber', title:'单号', align:'center', width: '9%', sort: true, filter: true, totalRowText: '合计'}
                            ,{field:'orderName', title:'款号', align:'center', width: '9%', sort: true, filter: true}
                            ,{field:'inStoreCount', title:'入库数量', align:'center', width: '8%', sort: true, filter: true, totalRow: true}
                            ,{field:'storageCount', title:'库存数量', align:'center', width: '8%', sort: true, filter: true, totalRow: true}
                            ,{field:'outStoreCount', title:'出库数量', align:'center', width: '8%', sort: true, filter: true, totalRow: true}
                            ,{field:'defectiveInStore', title:'次品入库', align:'center', width: '8%', sort: true, filter: true, totalRow: true}
                            ,{field:'defectiveStorage', title:'次品库存', align:'center', width: '8%', sort: true, filter: true, totalRow: true}
                            ,{field:'defectiveOutStore', title:'次品出库', align:'center', width: '8%', sort: true, filter: true, totalRow: true}
                            ,{title: '操作', width: '24%', templet: '#barTop', align:'center'}
                        ]]
                        ,excel: {
                            filename: '成品进销存.xlsx'
                        }
                        ,data: reportData
                        ,height: 'full-130'
                        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                        ,title: '成品进销存'
                        ,totalRow: true
                        ,loading: false
                        ,page: true
                        ,even: true
                        ,limits: [500, 1000, 2000]
                        ,limit: 1000 //每页默认显示的数量
                        ,done: function () {
                            soulTable.render(this);
                        }
                    });

                } else {
                    layer.msg("获取失败！", {icon: 2});
                }
            }, error: function () {
                layer.msg("获取失败！", {icon: 2});
            }
        });
    }

    form.on('submit(searchBeat)', function(data){
        var filterOrderName = $("#orderName").val();
        initTable(filterOrderName);
        return false;
    });

    table.on('toolbar(reportTable)', function(obj) {
        var filterOrderName = $("#orderName").val();
        if (obj.event === 'clearFilter') {
            // 清除所有筛选条件并重载表格
            // 参数: tableId
            soulTable.clearFilter('reportTable')
        } else if (obj.event === 'exportExcel') {
            soulTable.export('reportTable');
        } else if (obj.event === 'refresh') {
            initTable(filterOrderName);
        } else if (obj.event === 'defectiveSummary'){
            var load = layer.load();
            $.ajax({
                url: "/erp/getdefectivesummary",
                type: 'GET',
                async: false,
                data: {},
                success: function (res) {
                    if (res.endProductList) {
                        var reportData = res.endProductList;
                        table.render({
                            elem: '#reportTable'
                            ,cols:[[
                                {type:'numbers', align:'center', title:'序号', width: '5%'}
                                ,{field:'clothesVersionNumber', title:'单号', align:'center', width: '9%', sort: true, filter: true, totalRowText: '合计'}
                                ,{field:'orderName', title:'款号', align:'center', width: '9%', sort: true, filter: true}
                                ,{field:'colorName', title:'颜色', align:'center', width: '8%', sort: true, filter: true}
                                ,{field:'sizeName', title:'尺码', align:'center', width: '8%', sort: true, filter: true}
                                ,{field:'inStoreCount', title:'数量', align:'center', width: '8%', sort: true, filter: true, totalRow: true}
                                ,{field:'defectiveType', title:'次品类型', align:'center', width: '8%', sort: true, filter: true, totalRow: true}
                            ]]
                            ,excel: {
                                filename: '次品汇总.xlsx'
                            }
                            ,data: reportData
                            ,height: 'full-130'
                            ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                            ,title: '次品汇总'
                            ,totalRow: true
                            ,loading: true
                            ,page: true
                            ,even: true
                            ,limits: [500, 1000, 2000]
                            ,limit: 1000 //每页默认显示的数量
                            ,done: function () {
                                soulTable.render(this);
                                layer.close(load);
                            }
                        });

                    } else {
                        layer.msg("获取失败！", {icon: 2});
                    }
                }, error: function () {
                    layer.msg("获取失败！", {icon: 2});
                }
            });
        } else if (obj.event === 'init') {
            initTable(filterOrderName);
        }
    });

    table.on('tool(reportTable)', function(obj){
        var data = obj.data;
        if(obj.event === 'outStore'){
            var sizeNameList,colorNameList,endProductList;
            var index = layer.open({
                type: 1 //Page层类型
                , title: '成品出库'
                , btn: ['保存']
                , shade: 0.6 //遮罩透明度
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , content: "<div id=\"endProductOut\" style=\"cursor:default;\">\n" +
                    "        <table id=\"endProductOutTable\" name = \"endProductOutTable\" style=\"background-color: white; color: black\">\n" +
                    "        </table>\n" +
                    "    </div>"
                , yes: function (index, layero) {
                    var endProductData = layui.table.cache.endProductOutTable;
                    var endProductOutList = [];
                    $.each(endProductData,function(i,item){
                        if (item.type === '本次出库'){
                            $.each(sizeNameList,function(s_i,s_item){
                                var tmp = {};
                                tmp.orderName = data.orderName;
                                tmp.clothesVersionNumber = data.clothesVersionNumber;
                                tmp.colorName = item.color;
                                tmp.sizeName = s_item;
                                tmp.outStoreCount = item[s_item];
                                tmp.operateType = "out";
                                tmp.endType = "正品";
                                if (item[s_item] > 0){
                                    endProductOutList.push(tmp);
                                }
                            });
                        }
                    });
                    if (endProductOutList.length === 0){
                        layer.msg("请输入正确数量~~", { icon: 2 });
                        return false;
                    }
                    $.ajax({
                        url: "/erp/addendproductoutbatch",
                        type: 'POST',
                        data: {
                            endProductOutStoreJson:JSON.stringify(endProductOutList),
                            orderName: data.orderName,
                            endType: "正品"
                        },
                        success: function (res) {
                            if (res.result == 0) {
                                layer.close(index);
                                layer.msg("保存成功！", {icon: 1});
                                initTable(data.orderName);
                            } else if (res.result == 3) {
                                layer.msg(res.msg);
                            } else {
                                layer.msg("保存失败！", {icon: 2});
                            }
                        },
                        error: function () {
                            layer.msg("保存失败！", {icon: 2});
                        }
                    });
                    return false;
                }
                , cancel: function (i, layero) {

                }
            });
            layer.full(index);
            setTimeout(function () {
                $.ajax({
                    url: "/erp/getendproductpreoutstoredata",
                    type: 'GET',
                    data: {
                        orderName: data.orderName,
                        endType: "正品"
                    },
                    success: function (res) {
                        colorNameList = res.colorNameList;
                        sizeNameList = res.sizeNameList;
                        endProductList = res.endProductList;
                        sizeNameList.sort(function(a,b){
                            var order = ["XXXXS","4XS","XXXS","3XS","XXS","2XS","xxs","XS","xs","S","s","M","m","L","l","XL","xl","XXL","2XL","xxl","XXXL","3XL","XXXXL","4XL","XXXXXL","5XL","xxxl","070","70","073","075","75","080","80","085","090","90","095","95", "100", "105","110","115", "120","125", "130","135",
                                "140","145", "150","155", "160","165", "170","175", "180","185","190","195","200","XXS/155/72A","XS/160/76A","S/165/80A","M/170/84A","L/175/88A","XL/180/92A","XXL/185/96A","XXXL/190/100A","XXXXL/190/104A","XXS/145/72A","XS/150/76A","S/155/80A","M/160/84A","L/165/88A","XL/170/92A","XXL/175/96A","XXXL/180/100A","XXXXL/180/104A","XXXXXL/185/108A","110/56","120/60","130/64","140/68","150/72","160/76","170/80","2T","3T","4T","5T","4/5","6/6X","7/8","10/12","04(110-56)","06(120-60)","08(130-64)","10(140-64)","12(150-72)","14(155-76)","18M/73","24M/80","3Y/90","4Y/100","5Y/110","6Y/120","7Y/130","9Y/140","11Y/150","13Y/160"];
                            return order.indexOf(a) - order.indexOf(b);
                        });
                        var title = [
                            {field: 'color', title: '颜色',minWidth:100, fixed: true},
                            {field: 'type', title: '类别',minWidth:100, fixed: true}
                        ];
                        $.each(sizeNameList,function(index,value){
                            var field = {};
                            field.field = value;
                            field.title = value;
                            field.minWidth = 80;
                            field.edit = 'text';
                            title.push(field);
                        });
                        var colorSizeData = [];
                        $.each(colorNameList,function(index,value){
                            var tmp1 = {};
                            var tmp2 = {};
                            var tmp3 = {};
                            tmp1.color = value;
                            tmp2.color = value;
                            tmp3.color = value;
                            tmp1.type = "入库";
                            tmp2.type = "库存";
                            tmp3.type = "本次出库";
                            $.each(sizeNameList,function(index2,value2){
                                $.each(endProductList,function(index3,value3){
                                    if (value3.colorName === value && value3.sizeName === value2){
                                        tmp1[value2] = value3.inStoreCount;
                                        tmp2[value2] = value3.storageCount;
                                        tmp3[value2] = 0;
                                    }
                                })
                            });
                            colorSizeData.push(tmp1);
                            colorSizeData.push(tmp2);
                            colorSizeData.push(tmp3);
                        });
                        var endProductOutTable = table.render({
                            elem: '#endProductOutTable'
                            ,cols: [title]
                            ,data: colorSizeData
                            ,limit: 500 //每页默认显示的数量
                            ,height: 'full-50'
                            ,done:function () {
                            }
                        });
                        return false;
                    },
                    error: function () {
                        layer.msg("获取数据失败！", {icon: 2});
                    }
                })
            }, 100);
        } else if (obj.event === 'outDetail'){
            var sizeNameList,colorNameList,endProductList;
            var index = layer.open({
                type: 1 //Page层类型
                , title: '<span style="font-weight: bolder; color: red;">单款进销存详情,若入库数量>=下单数则显示绿色,否则显示红色</span>'
                , btn: ['导出']
                , shade: 0.6 //遮罩透明度
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , content: "<div id=\"endProductDetail\" style=\"cursor:default;\">\n" +
                    "        <table id=\"endProductDetailTable\" lay-filter=\"endProductDetailTable\">\n" +
                    "        </table>\n" +
                    "    </div>"
                , yes: function (index, layero) {
                    soulTable.export('endProductDetailTable');
                }
                , cancel: function (i, layero) {

                }
            });
            layer.full(index);
            setTimeout(function () {
                $.ajax({
                    url: "/erp/getendproductdetailbyorder",
                    type: 'GET',
                    data: {
                        orderName: data.orderName,
                        endType: "正品"
                    },
                    success: function (res) {
                        colorNameList = res.colorNameList;
                        sizeNameList = res.sizeNameList;
                        endProductList = res.endProductList;
                        sizeNameList.sort(function(a,b){
                            var order = ["XXXXS","4XS","XXXS","3XS","XXS","2XS","xxs","XS","xs","S","s","M","m","L","l","XL","xl","XXL","2XL","xxl","XXXL","3XL","XXXXL","4XL","XXXXXL","5XL","xxxl","070","70","073","075","75","080","80","085","090","90","095","95", "100", "105","110","115", "120","125", "130","135",
                                "140","145", "150","155", "160","165", "170","175", "180","185","190","195","200","XXS/155/72A","XS/160/76A","S/165/80A","M/170/84A","L/175/88A","XL/180/92A","XXL/185/96A","XXXL/190/100A","XXXXL/190/104A","XXS/145/72A","XS/150/76A","S/155/80A","M/160/84A","L/165/88A","XL/170/92A","XXL/175/96A","XXXL/180/100A","XXXXL/180/104A","XXXXXL/185/108A","110/56","120/60","130/64","140/68","150/72","160/76","170/80","2T","3T","4T","5T","4/5","6/6X","7/8","10/12","04(110-56)","06(120-60)","08(130-64)","10(140-64)","12(150-72)","14(155-76)","18M/73","24M/80","3Y/90","4Y/100","5Y/110","6Y/120","7Y/130","9Y/140","11Y/150","13Y/160"];
                            return order.indexOf(a) - order.indexOf(b);
                        });
                        var title = [
                            {field: 'color', title: '颜色',minWidth:100, fixed: true, sort: true, filter: true},
                            {field: 'type', title: '类别',minWidth:100, fixed: true, sort: true, filter: true, totalRowText: '合计'}
                        ];
                        $.each(sizeNameList,function(index,value){
                            var field = {};
                            field.field = value;
                            field.title = value;
                            field.minWidth = 80;
                            field.sort = 'true';
                            field.filter = 'true';
                            field.totalRow = 'true';
                            field.templet = function (d) {
                                var titleStr = value + 'Color';
                                if (d[titleStr] != null){
                                    return "<span style='font-weight: bolder; color:"+ d[titleStr] +"'>"+ d[value] +"</span>";
                                } else {
                                    return d[value];
                                }
                            };
                            title.push(field);
                        });

                        title.push({field: 'smallSum', title: '合计',minWidth:100, sort: true, totalRow: true, filter: true});

                        var colorSizeData = [];
                        $.each(colorNameList,function(index,value){
                            var tmp1 = {};
                            var tmp2 = {};
                            var tmp3 = {};
                            var tmp4 = {};
                            var tmp5 = {};
                            tmp1.color = value;
                            tmp2.color = value;
                            tmp3.color = value;
                            tmp4.color = value;
                            tmp5.color = value;
                            tmp1.type = "下单";
                            tmp2.type = "好片";
                            tmp3.type = "入库";
                            tmp4.type = "库存";
                            tmp5.type = "出库";
                            tmp1.smallSum = 0;
                            tmp2.smallSum = 0;
                            tmp3.smallSum = 0;
                            tmp4.smallSum = 0;
                            tmp5.smallSum = 0;
                            $.each(sizeNameList,function(index2,value2){
                                $.each(endProductList,function(index3,value3){
                                    if (value3.colorName === value && value3.sizeName === value2){
                                        tmp1[value2] = value3.orderCount;
                                        tmp2[value2] = value3.wellCount;
                                        tmp3[value2] = value3.inStoreCount;
                                        tmp3[value2 + 'Color'] = value3.inStoreCount >= value3.orderCount ? 'green' : 'red';
                                        tmp4[value2] = value3.storageCount;
                                        tmp5[value2] = value3.outStoreCount;
                                        tmp1.smallSum += value3.orderCount;
                                        tmp2.smallSum += value3.wellCount;
                                        tmp3.smallSum += value3.inStoreCount;
                                        tmp4.smallSum += value3.storageCount;
                                        tmp5.smallSum += value3.outStoreCount;
                                    }
                                })
                            });
                            colorSizeData.push(tmp1);
                            colorSizeData.push(tmp2);
                            colorSizeData.push(tmp3);
                            colorSizeData.push(tmp4);
                            colorSizeData.push(tmp5);
                        });
                        var endProductOutTable = table.render({
                            elem: '#endProductDetailTable'
                            ,cols: [title]
                            ,data: colorSizeData
                            ,totalRow: true
                            ,page: true
                            ,even: true
                            ,limit: 500 //每页默认显示的数量
                            ,height: 'full-150'
                            ,excel: {
                                filename: data.orderName + '-进销存详情.xlsx'
                            }
                            ,done:function () {
                                soulTable.render(this);
                            }
                        });
                        return false;
                    },
                    error: function () {
                        layer.msg("获取数据失败！", {icon: 2});
                    }
                })
            }, 100);
        } else if (obj.event === 'addInStore'){
            addInStore(data.orderName, data.clothesVersionNumber);
        } else if(obj.event === 'defectiveOut'){
            var index = layer.open({
                type: 1 //Page层类型
                , title: '次品出库'
                , btn: ['保存']
                , shade: 0.6 //遮罩透明度
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , content: "<div id=\"endProductOut\" style=\"cursor:default;\">\n" +
                    "        <table id=\"endProductOutTable\" name = \"endProductOutTable\" style=\"background-color: white; color: black\">\n" +
                    "        </table>\n" +
                    "    </div>"
                , yes: function (index, layero) {
                    var endProductData = layui.table.cache.endProductOutTable;
                    var endProductOutList = [];
                    $.each(endProductData,function(i,item){
                        if (Number(item.outCount) > Number(item.storageCount)){
                            layer.msg("出库数量不能大于库存数量~~", { icon: 2 });
                            return false;
                        }
                        if (Number(item.outCount) > 0){
                            var outStore = {};
                            outStore.orderName = item.orderName;
                            outStore.clothesVersionNumber = item.clothesVersionNumber;
                            outStore.colorName = item.colorName;
                            outStore.sizeName = item.sizeName;
                            outStore.storageId = item.id;
                            outStore.outStoreCount = item.outCount;
                            outStore.operateType = 'out';
                            outStore.endType = '次品';
                            outStore.remark = item.remark;
                            outStore.defectiveType = item.defectiveType;
                            endProductOutList.push(outStore);
                        }
                    });
                    if (endProductOutList.length == 0){
                        layer.msg("请输入正确数量~~", { icon: 2 });
                        return false;
                    }
                    $.ajax({
                        url: "/erp/adddefectiveendproductoutbatch",
                        type: 'POST',
                        data: {
                            endProductOutStoreJson:JSON.stringify(endProductOutList),
                            orderName: data.orderName
                        },
                        success: function (res) {
                            if (res.result == 0) {
                                layer.close(index);
                                layer.msg("保存成功！", {icon: 1});
                                initTable(data.orderName);
                            } else if (res.result == 3) {
                                layer.msg(res.msg);
                            } else {
                                layer.msg("保存失败！", {icon: 2});
                            }
                        },
                        error: function () {
                            layer.msg("保存失败！", {icon: 2});
                        }
                    });
                    return false;
                }
                , cancel: function (i, layero) {

                }
            });
            layer.full(index);
            setTimeout(function () {
                $.ajax({
                    url: "/erp/getdefectiveendproductinstorebyorder",
                    type: 'GET',
                    data: {
                        orderName: data.orderName
                    },
                    success: function (res) {
                        var reportData = res.data;
                        $.each(reportData,function(i,item){
                            item.outCount = 0;
                        });
                        table.render({
                            elem: '#endProductOutTable'
                            ,cols:[[
                                {type:'numbers', align:'center', title:'序号', width: '5%'}
                                ,{field:'id', hide:true}
                                ,{field:'clothesVersionNumber', title:'单号', align:'center', width: '10%', sort: true, filter: true, totalRowText: '合计'}
                                ,{field:'orderName', title:'款号', align:'center', width: '10%', sort: true, filter: true}
                                ,{field:'colorName', title:'颜色', align:'center', width: '10%', sort: true, filter: true}
                                ,{field:'sizeName', title:'尺码', align:'center', width: '9%', sort: true, filter: true}
                                ,{field:'inStoreCount', title:'入库数量', align:'center', width: '9%', sort: true, filter: true, totalRow: true}
                                ,{field:'storageCount', title:'库存数量', align:'center', width: '9%', sort: true, filter: true, totalRow: true}
                                ,{field:'defectiveType', title:'次品类型', align:'center', width: '9%', sort: true, filter: true, totalRow: true}
                                ,{field: 'createTime', title: '入库时间',align:'center', width: '10%',templet: function (d) {
                                        return moment(d.createTime).format("YYYY-MM-DD");
                                    }}
                                ,{field:'outCount', title:'出库数量', align:'center', width: '9%', sort: true, edit:'text', filter: true, totalRow: true}
                                ,{field:'remark', title:'备注', align:'center', width: '10%', sort: true, edit:'text', filter: true, totalRow: true}
                            ]]
                            ,excel: {
                                filename: '次品详情.xlsx'
                            }
                            ,data: reportData
                            ,height: 'full-130'
                            ,title: '次品详情'
                            ,totalRow: true
                            ,loading: false
                            ,page: true
                            ,even: true
                            ,limits: [500, 1000, 2000]
                            ,limit: 1000 //每页默认显示的数量
                            ,done: function () {
                                soulTable.render(this);
                            }
                        });
                    },
                    error: function () {
                        layer.msg("获取数据失败！", {icon: 2});
                    }
                })
            }, 100);
        } else if (obj.event === 'defectiveDetail'){
            var index = layer.open({
                type: 1 //Page层类型
                , title: '<span style="font-weight: bolder; color: red;">单款次品进销存详情</span>'
                , btn: ['导出']
                , shade: 0.6 //遮罩透明度
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , content: "<div id=\"endProductDetail\" style=\"cursor:default;\">\n" +
                    "        <table id=\"endProductDetailTable\" lay-filter=\"endProductDetailTable\">\n" +
                    "        </table>\n" +
                    "    </div>"
                , yes: function (index, layero) {
                    soulTable.export('endProductDetailTable');
                }
                , cancel: function (i, layero) {

                }
            });
            layer.full(index);
            setTimeout(function () {
                $.ajax({
                    url: "/erp/getdefectiveendproductinstorebyorder",
                    type: 'GET',
                    data: {
                        orderName: data.orderName
                    },
                    success: function (res) {
                        var reportData = res.data;
                        table.render({
                            elem: '#endProductDetailTable'
                            ,cols:[[
                                {type:'numbers', align:'center', title:'序号', width: '5%'}
                                ,{field:'clothesVersionNumber', title:'单号', align:'center', width: '9%', sort: true, filter: true, totalRowText: '合计'}
                                ,{field:'orderName', title:'款号', align:'center', width: '9%', sort: true, filter: true}
                                ,{field:'colorName', title:'颜色', align:'center', width: '9%', sort: true, filter: true}
                                ,{field:'sizeName', title:'尺码', align:'center', width: '9%', sort: true, filter: true}
                                ,{field:'inStoreCount', title:'入库数量', align:'center', width: '8%', sort: true, filter: true, totalRow: true}
                                ,{field:'storageCount', title:'库存数量', align:'center', width: '8%', sort: true, filter: true, totalRow: true}
                                ,{field:'outStoreCount', title:'出库数量', align:'center', width: '8%', sort: true, filter: true, totalRow: true}
                                ,{field:'defectiveType', title:'次品类型', align:'center', width: '8%', sort: true, filter: true, totalRow: true}
                                ,{field: 'createTime', title: '最后操作',align:'center', minWidth: 120,templet: function (d) {
                                        return moment(d.createTime).format("YYYY-MM-DD");
                                    }}
                            ]]
                            ,excel: {
                                filename: '次品详情.xlsx'
                            }
                            ,data: reportData
                            ,height: 'full-130'
                            ,title: '次品详情'
                            ,totalRow: true
                            ,loading: false
                            ,page: true
                            ,even: true
                            ,limits: [500, 1000, 2000]
                            ,limit: 1000 //每页默认显示的数量
                            ,done: function () {
                                soulTable.render(this);
                            }
                        });
                    },
                    error: function () {
                        layer.msg("获取数据失败！", {icon: 2});
                    }
                })
            }, 100);
        } else if (obj.event === 'defectiveIn'){
            addDefectiveInStore(data.orderName, data.clothesVersionNumber);
        }
    });

});
function addInStore(initOrderName, initClothesVersionNumber) {
    var orderName,clothesVersionNumber;
    if (initOrderName != null && initOrderName != ""){
        orderName = initOrderName;
        clothesVersionNumber = initClothesVersionNumber;
    } else {
        orderName = $("#orderName").val();
        clothesVersionNumber = $("#clothesVersionNumber").val();
    }
    if (orderName == null || orderName === "" || clothesVersionNumber == null || clothesVersionNumber === ""){
        layer.msg("单号/款号不能为空", { icon: 2 });
        return false;
    }
    var sizeNameList,colorNameList,orderInfoList;
    layui.use(['table', 'soulTable'], function () {
        var table = layui.table,
            soulTable = layui.soulTable;
        var index = layer.open({
            type: 1 //Page层类型
            , title: '添加成品入库'
            , btn: ['保存','计算']
            , shade: 0.6 //遮罩透明度
            , maxmin: false //允许全屏最小化
            , anim: 0 //0-6的动画形式，-1不开启
            , content: "<div id=\"endProductAdd\" style=\"cursor:default;\">\n" +
                "        <table id=\"endProductAddTable\" name = \"endProductAddTable\" style=\"background-color: white; color: black\">\n" +
                "        </table>\n" +
                "    </div>"
            , yes: function (index, layero) {
                var endProductData = layui.table.cache.endProductAddTable;
                var endProductInList = [];
                var flag = false;
                $.each(endProductData,function(i,item){
                    if (item.type === '本次'){
                        $.each(sizeNameList,function(s_i,s_item){
                            var tmp = {};
                            tmp.orderName = orderName;
                            tmp.clothesVersionNumber = clothesVersionNumber;
                            tmp.colorName = item.color;
                            tmp.sizeName = s_item;
                            tmp.inStoreCount = item[s_item];
                            tmp.operateType = "in";
                            tmp.endType = "正品";
                            if (item[s_item] > 0){
                                endProductInList.push(tmp);
                            }
                        });
                    }
                });
                if (endProductInList.length === 0){
                    layer.msg("请输入正确数量~~", { icon: 2 });
                    return false;
                }
                $.ajax({
                    url: "/erp/addendproductinbatch",
                    type: 'POST',
                    data: {
                        endProductInStoreJson:JSON.stringify(endProductInList),
                        orderName: orderName,
                        endType: "正品"
                    },
                    success: function (res) {
                        if (res.result == 0) {
                            layer.close(index);
                            layer.msg("保存成功！", {icon: 1});
                            $.ajax({
                                url: "/erp/getendproductalltyperecord",
                                type: 'GET',
                                data: {
                                    orderName: orderName
                                },
                                success: function (res) {
                                    if (res.endProductList) {
                                        var reportData = res.endProductList;
                                        table.render({
                                            elem: '#reportTable'
                                            ,cols:[[
                                                {title: '#', width: '5%', collapse: true,lazy: true, icon: ['layui-icon layui-icon-triangle-r', 'layui-icon layui-icon-triangle-d'], children:[
                                                        {
                                                            title: '正品入库'
                                                            ,url: 'getendproductinstorebyorder'
                                                            ,where: function(row){
                                                                return {
                                                                    orderName: row.orderName,
                                                                    endType: "正品"
                                                                }
                                                            }
                                                            ,height: 300
                                                            ,loading:true
                                                            ,page: false
                                                            ,toolbar: '<div><a class="layui-btn layui-btn-danger layui-btn-sm" lay-event="batchDeleteIn">批量删除</a></div>'
                                                            ,totalRow: true
                                                            ,cols: [[
                                                                {type: 'checkbox', fixed: 'left'},
                                                                {field: 'id', hide: true, fixed: 'left'},
                                                                {field: 'clothesVersionNumber', title: '单号',align:'center', minWidth: 120},
                                                                {field: 'orderName', title: '款号',align:'center', minWidth: 120},
                                                                {field: 'colorName', title: '颜色',align:'center', minWidth: 120, totalRow: true},
                                                                {field: 'sizeName', title: '尺码',align:'center', minWidth: 120},
                                                                {field: 'inStoreCount', title: '入库数量',align:'center', minWidth: 120, totalRow: true},
                                                                {field: 'createTime', title: '日期',align:'center', minWidth: 120,templet: function (d) {
                                                                        return moment(d.createTime).format("YYYY-MM-DD");
                                                                    }},
                                                                {title: '操作', minWidth: 120,align:'center', templet: function(row) {
                                                                        return '<a class="layui-btn layui-btn-xs" lay-event="childEdit">修改</a>';
                                                                    }}
                                                            ]]
                                                            ,toolEvent: function (obj, pobj) {
                                                                // obj 子表当前行对象
                                                                // pobj 父表当前行对象
                                                                var filterOrderName = $("#orderName").val();
                                                                var rowData = obj.data;
                                                                var childId = this.id; // 通过 this 对象获取当前子表的id
                                                                if (obj.event === 'childEdit') {
                                                                    var index = layer.open({
                                                                        type: 1 //Page层类型
                                                                        , title: '修改'
                                                                        , btn: ['保存','取消']
                                                                        , shade: 0.6 //遮罩透明度
                                                                        , maxmin: false //允许全屏最小化
                                                                        , anim: 0 //0-6的动画形式，-1不开启
                                                                        , area: ['345px', '200px']
                                                                        , content: "<div>\n" +
                                                                            "            <table>\n" +
                                                                            "                <tr>\n" +
                                                                            "                    <td style=\"text-align: right;margin-bottom: 15px;width: 100px\">\n" +
                                                                            "                        <label class=\"layui-form-label\">原数量</label>\n" +
                                                                            "                    </td>\n" +
                                                                            "                    <td style=\"margin-bottom: 15px;\">\n" +
                                                                            "                        <input type=\"text\" name=\"initCount\" id=\"initCount\" readonly class=\"layui-input\">\n" +
                                                                            "                    </td>\n" +
                                                                            "                </tr>\n" +
                                                                            "                <tr>\n" +
                                                                            "                    <td style=\"text-align: right;margin-bottom: 15px;width: 100px\">\n" +
                                                                            "                        <label class=\"layui-form-label\">修改成</label>\n" +
                                                                            "                    </td>\n" +
                                                                            "                    <td style=\"margin-bottom: 15px;\">\n" +
                                                                            "                        <input type=\"text\" name=\"toCount\" id=\"toCount\" class=\"layui-input\">\n" +
                                                                            "                    </td>\n" +
                                                                            "                </tr>\n" +
                                                                            "            </table>\n" +
                                                                            "    </div>"
                                                                        , yes: function (index, layero) {
                                                                            var initCount= $("#initCount").val();
                                                                            var toCount= $("#toCount").val();
                                                                            var param = {};
                                                                            param.orderName = rowData.orderName;
                                                                            param.initCount = initCount;
                                                                            param.id = rowData.id;
                                                                            param.toCount = toCount;
                                                                            param.colorName = rowData.colorName;
                                                                            param.sizeName = rowData.sizeName;
                                                                            param.endType = "正品";
                                                                            if (toCount == null || toCount == ""){
                                                                                layer.msg("请填写完整", { icon: 2 });
                                                                                return false;
                                                                            }
                                                                            $.ajax({
                                                                                url: "/erp/updateinstorecount",
                                                                                type: 'POST',
                                                                                data: param,
                                                                                success: function (res) {
                                                                                    layer.closeAll();
                                                                                    if (res.result == 0){
                                                                                        initTable(filterOrderName);
                                                                                        layer.msg("修改成功！", {icon: 1});
                                                                                    } else if (res.result == 1){
                                                                                        layer.msg("修改失败！", {icon: 2});
                                                                                    } else if (res.result == 3){
                                                                                        layer.msg("数据故障,不能修改！");
                                                                                    } else if (res.result == 4){
                                                                                        layer.msg("库存不足,无法修改！");
                                                                                    }
                                                                                }, error: function () {
                                                                                    layer.msg("添加失败！", {icon: 2});
                                                                                }
                                                                            })
                                                                        }
                                                                        ,btn2: function(){
                                                                            $("#initCount").val();
                                                                            $("#toCount").val();
                                                                            layer.closeAll();
                                                                        }
                                                                        , cancel: function (i, layero) {
                                                                            $("#initCount").val();
                                                                            $("#toCount").val();
                                                                            layer.closeAll();
                                                                        }
                                                                    });
                                                                    $("#initCount").val(rowData.inStoreCount);
                                                                    return false;
                                                                }
                                                            }
                                                            ,toolbarEvent: function (obj, pobj) {
                                                                // obj 子表当前行对象
                                                                // pobj 父表当前行对象
                                                                var objData = obj.data;
                                                                var childId = this.id; // 通过 this 对象获取当前子表的id
                                                                if (obj.event === 'batchDeleteIn'){
                                                                    var checkStatus = table.checkStatus(obj.config.id);
                                                                    if(checkStatus.data.length == 0) {
                                                                        layer.msg('请选择数据');
                                                                    }else {
                                                                        var idList = [];
                                                                        var inStoreCount = 0;
                                                                        var orderName;
                                                                        $.each(checkStatus.data, function (index, item) {
                                                                            inStoreCount += item.inStoreCount;
                                                                            idList.push(item.id);
                                                                            orderName = item.orderName;
                                                                        });
                                                                        layer.confirm('真的删除吗', function(index){
                                                                            $.ajax({
                                                                                url: "/erp/deleteendproductinbatch",
                                                                                type: 'POST',
                                                                                data: {
                                                                                    idList: idList,
                                                                                    orderName: orderName,
                                                                                    endType: "正品"
                                                                                },
                                                                                traditional: true,
                                                                                success: function (res) {
                                                                                    if (res.msg) {
                                                                                        layer.close(index);
                                                                                        layer.msg(res.msg);
                                                                                    } else if (res.result == 0) {
                                                                                        layer.close(index);
                                                                                        layer.msg("删除成功！", {icon: 1});
                                                                                        table.reload(childId);
                                                                                    } else {
                                                                                        layer.msg("删除失败！", {icon: 2});
                                                                                    }
                                                                                }, error: function () {
                                                                                    layer.msg("删除失败！", {icon: 2});
                                                                                }
                                                                            })
                                                                        });
                                                                    }
                                                                }
                                                            }
                                                            ,done: function () {
                                                                soulTable.render(this);
                                                            }
                                                        }, {
                                                            title: '正品库存'
                                                            ,url: 'getendproductstoragebyorder'
                                                            ,where: function(row){
                                                                return {
                                                                    orderName: row.orderName,
                                                                    endType: "正品"
                                                                }
                                                            }
                                                            ,height: 300
                                                            ,loading:true
                                                            ,page: false
                                                            ,cols: [[
                                                                {field: 'clothesVersionNumber', title: '单号',align:'center', minWidth: 120},
                                                                {field: 'orderName', title: '款号',align:'center', minWidth: 120},
                                                                {field: 'colorName', title: '颜色',align:'center', minWidth: 120},
                                                                {field: 'sizeName', title: '尺码',align:'center', minWidth: 120 },
                                                                {field: 'storageCount', title: '数量',align:'center', minWidth: 120, totalRow: true},
                                                                {field: 'createTime', title: '最后操作',align:'center', minWidth: 120,templet: function (d) {
                                                                        return moment(d.cutDate).format("YYYY-MM-DD");
                                                                    }}
                                                            ]]
                                                            ,done: function () {
                                                                soulTable.render(this);
                                                            }
                                                        }, {
                                                            title: '正品出库'
                                                            ,url: 'getendproductoutstorebyorder'
                                                            ,where: function(row){
                                                                return {
                                                                    orderName: row.orderName,
                                                                    endType: "正品"
                                                                }
                                                            }
                                                            ,height: 300
                                                            ,loading:true
                                                            ,page: false
                                                            ,toolbar: '<div><a class="layui-btn layui-btn-danger layui-btn-sm" lay-event="batchDeleteOut">批量删除</a></div>'
                                                            ,totalRow: true
                                                            ,cols: [[
                                                                {type: 'checkbox', fixed: 'left'},
                                                                {field: 'id', hide: true, fixed: 'left'},
                                                                {field: 'clothesVersionNumber', title: '单号',align:'center', minWidth: 120},
                                                                {field: 'orderName', title: '款号',align:'center', minWidth: 120},
                                                                {field: 'colorName', title: '颜色',align:'center', minWidth: 120},
                                                                {field: 'sizeName', title: '尺码',align:'center', minWidth: 120 },
                                                                {field: 'outStoreCount', title: '数量',align:'center', minWidth: 120, totalRow: true},
                                                                {field: 'createTime', title: '最后操作',align:'center', minWidth: 120,templet: function (d) {
                                                                        return moment(d.createTime).format("YYYY-MM-DD");
                                                                    }}
                                                            ]]
                                                            ,toolbarEvent: function (obj, pobj) {
                                                                // obj 子表当前行对象
                                                                // pobj 父表当前行对象
                                                                var objData = obj.data;
                                                                var childId = this.id; // 通过 this 对象获取当前子表的id
                                                                if (obj.event === 'batchDeleteOut'){
                                                                    var checkStatus = table.checkStatus(obj.config.id);
                                                                    if(checkStatus.data.length == 0) {
                                                                        layer.msg('请选择数据');
                                                                    }else {
                                                                        var idList = [];
                                                                        var orderName;
                                                                        $.each(checkStatus.data, function (index, item) {
                                                                            idList.push(item.id);
                                                                            orderName = item.orderName;
                                                                        });
                                                                        layer.confirm('真的删除吗', function(index){
                                                                            $.ajax({
                                                                                url: "/erp/deleteendproductoutbatch",
                                                                                type: 'POST',
                                                                                data: {
                                                                                    idList: idList,
                                                                                    orderName: orderName,
                                                                                    endType: "正品"
                                                                                },
                                                                                traditional: true,
                                                                                success: function (res) {
                                                                                    if (res.result == 0) {
                                                                                        layer.close(index);
                                                                                        layer.msg("删除成功！", {icon: 1});
                                                                                        table.reload(childId);
                                                                                    } else {
                                                                                        layer.msg("删除失败！", {icon: 2});
                                                                                    }
                                                                                }, error: function () {
                                                                                    layer.msg("删除失败！", {icon: 2});
                                                                                }
                                                                            })
                                                                        });
                                                                    }
                                                                }
                                                            }
                                                            ,done: function () {
                                                                soulTable.render(this);
                                                            }
                                                        },{
                                                            title: '次品入库'
                                                            ,url: 'getdefectiveendproductinstorebyorder'
                                                            ,where: function(row){
                                                                return {
                                                                    orderName: row.orderName
                                                                }
                                                            }
                                                            ,height: 300
                                                            ,loading:true
                                                            ,page: false
                                                            ,toolbar: '<div><a class="layui-btn layui-btn-danger layui-btn-sm" lay-event="batchDeleteIn">批量删除</a></div>'
                                                            ,totalRow: true
                                                            ,cols: [[
                                                                {type: 'checkbox', fixed: 'left'},
                                                                {field: 'id', hide: true, fixed: 'left'},
                                                                {field: 'clothesVersionNumber', title: '单号',align:'center', minWidth: 120},
                                                                {field: 'orderName', title: '款号',align:'center', minWidth: 120},
                                                                {field: 'colorName', title: '颜色',align:'center', minWidth: 120, totalRow: true},
                                                                {field: 'sizeName', title: '尺码',align:'center', minWidth: 120},
                                                                {field: 'inStoreCount', title: '入库数量',align:'center', minWidth: 120, totalRow: true},
                                                                {field: 'defectiveType', title: '类别',align:'center', minWidth: 120, totalRow: true},
                                                                {field: 'createTime', title: '日期',align:'center', minWidth: 120,templet: function (d) {
                                                                        return moment(d.createTime).format("YYYY-MM-DD");
                                                                    }},
                                                                {title: '操作', minWidth: 120,align:'center', templet: function(row) {
                                                                        return '<a class="layui-btn layui-btn-xs" lay-event="childEdit">修改</a>';
                                                                    }}
                                                            ]]
                                                            ,toolEvent: function (obj, pobj) {
                                                                // obj 子表当前行对象
                                                                // pobj 父表当前行对象
                                                                var filterOrderName = $("#orderName").val();
                                                                var rowData = obj.data;
                                                                var childId = this.id; // 通过 this 对象获取当前子表的id
                                                                if (obj.event === 'childEdit') {
                                                                    var index = layer.open({
                                                                        type: 1 //Page层类型
                                                                        , title: '修改'
                                                                        , btn: ['保存','取消']
                                                                        , shade: 0.6 //遮罩透明度
                                                                        , maxmin: false //允许全屏最小化
                                                                        , anim: 0 //0-6的动画形式，-1不开启
                                                                        , area: ['345px', '200px']
                                                                        , content: "<div>\n" +
                                                                            "            <table>\n" +
                                                                            "                <tr>\n" +
                                                                            "                    <td style=\"text-align: right;margin-bottom: 15px;width: 100px\">\n" +
                                                                            "                        <label class=\"layui-form-label\">原数量</label>\n" +
                                                                            "                    </td>\n" +
                                                                            "                    <td style=\"margin-bottom: 15px;\">\n" +
                                                                            "                        <input type=\"text\" name=\"initCount\" id=\"initCount\" readonly class=\"layui-input\">\n" +
                                                                            "                    </td>\n" +
                                                                            "                </tr>\n" +
                                                                            "                <tr>\n" +
                                                                            "                    <td style=\"text-align: right;margin-bottom: 15px;width: 100px\">\n" +
                                                                            "                        <label class=\"layui-form-label\">修改成</label>\n" +
                                                                            "                    </td>\n" +
                                                                            "                    <td style=\"margin-bottom: 15px;\">\n" +
                                                                            "                        <input type=\"text\" name=\"toCount\" id=\"toCount\" class=\"layui-input\">\n" +
                                                                            "                    </td>\n" +
                                                                            "                </tr>\n" +
                                                                            "            </table>\n" +
                                                                            "    </div>"
                                                                        , yes: function (index, layero) {
                                                                            var initCount= $("#initCount").val();
                                                                            var toCount= $("#toCount").val();
                                                                            var param = {};
                                                                            param.orderName = rowData.orderName;
                                                                            param.initCount = initCount;
                                                                            param.id = rowData.id;
                                                                            param.toCount = toCount;
                                                                            param.colorName = rowData.colorName;
                                                                            param.sizeName = rowData.sizeName;
                                                                            param.endType = "次品";
                                                                            if (toCount == null || toCount == ""){
                                                                                layer.msg("请填写完整", { icon: 2 });
                                                                                return false;
                                                                            }
                                                                            $.ajax({
                                                                                url: "/erp/updatedefectiveinstorecount",
                                                                                type: 'POST',
                                                                                data: param,
                                                                                success: function (res) {
                                                                                    layer.closeAll();
                                                                                    if (res.result == 0){
                                                                                        initTable(filterOrderName);
                                                                                        layer.msg("修改成功！", {icon: 1});
                                                                                    } else if (res.result == 1){
                                                                                        layer.msg("修改失败！", {icon: 2});
                                                                                    } else if (res.result == 3){
                                                                                        layer.msg("数据故障,不能修改！");
                                                                                    } else if (res.result == 4){
                                                                                        layer.msg("库存不足,无法修改！");
                                                                                    }
                                                                                }, error: function () {
                                                                                    layer.msg("添加失败！", {icon: 2});
                                                                                }
                                                                            })
                                                                        }
                                                                        ,btn2: function(){
                                                                            $("#initCount").val();
                                                                            $("#toCount").val();
                                                                            layer.closeAll();
                                                                        }
                                                                        , cancel: function (i, layero) {
                                                                            $("#initCount").val();
                                                                            $("#toCount").val();
                                                                            layer.closeAll();
                                                                        }
                                                                    });
                                                                    $("#initCount").val(rowData.inStoreCount);
                                                                    return false;
                                                                }
                                                            }
                                                            ,toolbarEvent: function (obj, pobj) {
                                                                // obj 子表当前行对象
                                                                // pobj 父表当前行对象
                                                                var objData = obj.data;
                                                                var childId = this.id; // 通过 this 对象获取当前子表的id
                                                                if (obj.event === 'batchDeleteIn'){
                                                                    var checkStatus = table.checkStatus(obj.config.id);
                                                                    if(checkStatus.data.length == 0) {
                                                                        layer.msg('请选择数据');
                                                                    }else {
                                                                        var idList = [];
                                                                        var inStoreCount = 0;
                                                                        var orderName;
                                                                        $.each(checkStatus.data, function (index, item) {
                                                                            inStoreCount += item.inStoreCount;
                                                                            idList.push(item.id);
                                                                            orderName = item.orderName;
                                                                        });
                                                                        layer.confirm('真的删除吗', function(index){
                                                                            $.ajax({
                                                                                url: "/erp/deleteendproductinbatch",
                                                                                type: 'POST',
                                                                                data: {
                                                                                    idList: idList,
                                                                                    orderName: orderName,
                                                                                    endType: "次品"
                                                                                },
                                                                                traditional: true,
                                                                                success: function (res) {
                                                                                    if (res.msg) {
                                                                                        layer.close(index);
                                                                                        layer.msg(res.msg);
                                                                                    } else if (res.result == 0) {
                                                                                        layer.close(index);
                                                                                        layer.msg("删除成功！", {icon: 1});
                                                                                        table.reload(childId);
                                                                                    } else {
                                                                                        layer.msg("删除失败！", {icon: 2});
                                                                                    }
                                                                                }, error: function () {
                                                                                    layer.msg("删除失败！", {icon: 2});
                                                                                }
                                                                            })
                                                                        });
                                                                    }
                                                                }
                                                            }
                                                            ,done: function () {
                                                                soulTable.render(this);
                                                            }
                                                        }, {
                                                            title: '次品库存'
                                                            ,url: 'getdefectiveendproductstoragebyorder'
                                                            ,where: function(row){
                                                                return {
                                                                    orderName: row.orderName
                                                                }
                                                            }
                                                            ,height: 300
                                                            ,loading:true
                                                            ,page: false
                                                            ,cols: [[
                                                                {field: 'clothesVersionNumber', title: '单号',align:'center', minWidth: 120},
                                                                {field: 'orderName', title: '款号',align:'center', minWidth: 120},
                                                                {field: 'colorName', title: '颜色',align:'center', minWidth: 120},
                                                                {field: 'sizeName', title: '尺码',align:'center', minWidth: 120 },
                                                                {field: 'storageCount', title: '数量',align:'center', minWidth: 120, totalRow: true},
                                                                {field: 'defectiveType', title: '类别',align:'center', minWidth: 120, totalRow: true},
                                                                {field: 'createTime', title: '日期',align:'center', minWidth: 120,templet: function (d) {
                                                                        return moment(d.createTime).format("YYYY-MM-DD");
                                                                    }}
                                                            ]]
                                                            ,done: function () {
                                                                soulTable.render(this);
                                                            }
                                                        }, {
                                                            title: '次品出库'
                                                            ,url: 'getdefectiveendproductoutstorebyorder'
                                                            ,where: function(row){
                                                                return {
                                                                    orderName: row.orderName
                                                                }
                                                            }
                                                            ,height: 300
                                                            ,loading:true
                                                            ,page: false
                                                            ,toolbar: '<div><a class="layui-btn layui-btn-danger layui-btn-sm" lay-event="batchDeleteOut">批量删除</a></div>'
                                                            ,totalRow: true
                                                            ,cols: [[
                                                                {type: 'checkbox', fixed: 'left'},
                                                                {field: 'id', hide: true, fixed: 'left'},
                                                                {field: 'clothesVersionNumber', title: '单号',align:'center', minWidth: 120},
                                                                {field: 'orderName', title: '款号',align:'center', minWidth: 120},
                                                                {field: 'colorName', title: '颜色',align:'center', minWidth: 120},
                                                                {field: 'sizeName', title: '尺码',align:'center', minWidth: 120 },
                                                                {field: 'outStoreCount', title: '数量',align:'center', minWidth: 120, totalRow: true},
                                                                {field: 'defectiveType', title: '类别',align:'center', minWidth: 120, totalRow: true},
                                                                {field: 'createTime', title: '最后操作',align:'center', minWidth: 120,templet: function (d) {
                                                                        return moment(d.cutDate).format("YYYY-MM-DD");
                                                                    }}
                                                            ]]
                                                            ,toolbarEvent: function (obj, pobj) {
                                                                // obj 子表当前行对象
                                                                // pobj 父表当前行对象
                                                                var objData = obj.data;
                                                                var childId = this.id; // 通过 this 对象获取当前子表的id
                                                                if (obj.event === 'batchDeleteOut'){
                                                                    var checkStatus = table.checkStatus(obj.config.id);
                                                                    if(checkStatus.data.length == 0) {
                                                                        layer.msg('请选择数据');
                                                                    }else {
                                                                        var idList = [];
                                                                        var orderName;
                                                                        $.each(checkStatus.data, function (index, item) {
                                                                            idList.push(item.id);
                                                                            orderName = item.orderName;
                                                                        });
                                                                        layer.confirm('真的删除吗', function(index){
                                                                            $.ajax({
                                                                                url: "/erp/deletedefectiveendproductoutbatch",
                                                                                type: 'POST',
                                                                                data: {
                                                                                    idList: idList,
                                                                                    orderName: orderName,
                                                                                    endType: "次品"
                                                                                },
                                                                                traditional: true,
                                                                                success: function (res) {
                                                                                    if (res.result == 0) {
                                                                                        layer.close(index);
                                                                                        layer.msg("删除成功！", {icon: 1});
                                                                                        table.reload(childId);
                                                                                    } else {
                                                                                        layer.msg("删除失败！", {icon: 2});
                                                                                    }
                                                                                }, error: function () {
                                                                                    layer.msg("删除失败！", {icon: 2});
                                                                                }
                                                                            })
                                                                        });
                                                                    }
                                                                }
                                                            }
                                                            ,done: function () {
                                                                soulTable.render(this);
                                                            }
                                                        }
                                                    ]}
                                                ,{type:'numbers', align:'center', title:'序号', width: '5%'}
                                                ,{field:'clothesVersionNumber', title:'单号', align:'center', width: '9%', sort: true, filter: true, totalRowText: '合计'}
                                                ,{field:'orderName', title:'款号', align:'center', width: '9%', sort: true, filter: true}
                                                ,{field:'inStoreCount', title:'入库数量', align:'center', width: '8%', sort: true, filter: true, totalRow: true}
                                                ,{field:'storageCount', title:'库存数量', align:'center', width: '8%', sort: true, filter: true, totalRow: true}
                                                ,{field:'outStoreCount', title:'出库数量', align:'center', width: '8%', sort: true, filter: true, totalRow: true}
                                                ,{field:'defectiveInStore', title:'次品入库', align:'center', width: '8%', sort: true, filter: true, totalRow: true}
                                                ,{field:'defectiveStorage', title:'次品库存', align:'center', width: '8%', sort: true, filter: true, totalRow: true}
                                                ,{field:'defectiveOutStore', title:'次品出库', align:'center', width: '8%', sort: true, filter: true, totalRow: true}
                                                ,{title: '操作', width: '24%', templet: '#barTop', align:'center'}
                                            ]]
                                            ,excel: {
                                                filename: '成品进销存.xlsx'
                                            }
                                            ,data: reportData
                                            ,height: 'full-130'
                                            ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                                            ,title: '成品进销存'
                                            ,totalRow: true
                                            ,loading: false
                                            ,page: true
                                            ,even: true
                                            ,limits: [500, 1000, 2000]
                                            ,limit: 1000 //每页默认显示的数量
                                            ,done: function () {
                                                soulTable.render(this);
                                            }
                                        });

                                    } else {
                                        layer.msg("获取失败！", {icon: 2});
                                    }
                                }, error: function () {
                                    layer.msg("获取失败！", {icon: 2});
                                }
                            });
                        } else {
                            layer.msg("保存失败！", {icon: 2});
                        }
                    },
                    error: function () {
                        layer.msg("保存失败！", {icon: 2});
                    }
                });
                return false;
            }
            , btn2: function (index, layero) {
                var endProductData = layui.table.cache.endProductAddTable;
                $.each(endProductData,function(i,item){
                    if (item.type === '本次'){
                        var colorSum = 0;
                        $.each(sizeNameList,function(s_i,s_item){
                            colorSum += Number(item[s_item]);
                        });
                        item.colorSum = colorSum;
                    }
                });
                table.reload("endProductAddTable",{
                    data:endProductData   // 将新数据重新载入表格
                });
                return false;
            }
            , cancel: function (i, layero) {

            }
        });
        layer.full(index);
        setTimeout(function () {
            $.ajax({
                url: "/erp/getendproductpreinstoredata",
                type: 'GET',
                data: {
                    orderName: orderName,
                    endType: "正品"
                },
                success: function (res) {
                    colorNameList = res.colorNameList;
                    sizeNameList = res.sizeNameList;
                    orderInfoList = res.orderInfoList;
                    sizeNameList.sort(function(a,b){
                        var order = ["XXXXS","4XS","XXXS","3XS","XXS","2XS","xxs","XS","xs","S","s","M","m","L","l","XL","xl","XXL","2XL","xxl","XXXL","3XL","XXXXL","4XL","XXXXXL","5XL","xxxl","070","70","073","075","75","080","80","085","090","90","095","95", "100", "105","110","115", "120","125", "130","135",
                            "140","145", "150","155", "160","165", "170","175", "180","185","190","195","200","XXS/155/72A","XS/160/76A","S/165/80A","M/170/84A","L/175/88A","XL/180/92A","XXL/185/96A","XXXL/190/100A","XXXXL/190/104A","XXS/145/72A","XS/150/76A","S/155/80A","M/160/84A","L/165/88A","XL/170/92A","XXL/175/96A","XXXL/180/100A","XXXXL/180/104A","XXXXXL/185/108A","110/56","120/60","130/64","140/68","150/72","160/76","170/80","2T","3T","4T","5T","4/5","6/6X","7/8","10/12","04(110-56)","06(120-60)","08(130-64)","10(140-64)","12(150-72)","14(155-76)","18M/73","24M/80","3Y/90","4Y/100","5Y/110","6Y/120","7Y/130","9Y/140","11Y/150","13Y/160"];
                        return order.indexOf(a) - order.indexOf(b);
                    });
                    var title = [
                        {field: 'color', title: '颜色',minWidth:100, fixed: true},
                        {field: 'type', title: '类别',minWidth:100, fixed: true}
                    ];
                    $.each(sizeNameList,function(index,value){
                        var field = {};
                        field.field = value;
                        field.title = value;
                        field.minWidth = 80;
                        field.edit = 'text';
                        title.push(field);
                    });
                    title.push({field: 'colorSum', title: '合计',minWidth:100});
                    var colorSizeData = [];
                    $.each(colorNameList,function(index,value){
                        var tmp1 = {};
                        var tmp2 = {};
                        var tmp3 = {};
                        tmp1.color = value;
                        tmp2.color = value;
                        tmp3.color = value;
                        tmp1.type = "好片";
                        tmp2.type = "已入";
                        tmp3.type = "本次";
                        tmp1.colorSum = 0;
                        tmp2.colorSum = 0;
                        tmp3.colorSum = 0;
                        $.each(sizeNameList,function(index2,value2){
                            $.each(orderInfoList,function(index3,value3){
                                if (value3.colorName === value && value3.sizeName === value2){
                                    tmp1[value2] = value3.wellCount;
                                    tmp2[value2] = value3.finishCount;
                                    tmp3[value2] = 0;
                                    tmp1.colorSum += value3.wellCount;
                                    tmp2.colorSum += value3.finishCount;
                                }
                            })
                        });
                        colorSizeData.push(tmp1);
                        colorSizeData.push(tmp2);
                        colorSizeData.push(tmp3);
                    });
                    var basicOrderClothesTable = table.render({
                        elem: '#endProductAddTable'
                        ,cols: [title]
                        ,data: colorSizeData
                        ,limit: 500 //每页默认显示的数量
                        ,height: 'full-200'
                        ,done:function () {
                        }
                    });
                    return false;
                },
                error: function () {
                    layer.msg("获取数据失败！", {icon: 2});
                }
            })
        }, 100);

    })
    return false;
}
function addDefectiveInStore(initOrderName, initClothesVersionNumber) {
    var orderName,clothesVersionNumber;
    if (initOrderName != null && initOrderName != ""){
        orderName = initOrderName;
        clothesVersionNumber = initClothesVersionNumber;
    } else {
        orderName = $("#orderName").val();
        clothesVersionNumber = $("#clothesVersionNumber").val();
    }
    if (orderName == null || orderName === "" || clothesVersionNumber == null || clothesVersionNumber === ""){
        layer.msg("单号/款号不能为空", { icon: 2 });
        return false;
    }
    var sizeNameList,colorNameList,orderInfoList;
    layui.use(['table', 'soulTable'], function () {
        var table = layui.table,
            soulTable = layui.soulTable;
        var index = layer.open({
            type: 1 //Page层类型
            , title: '添加次品入库'
            , btn: ['保存','计算']
            , shade: 0.6 //遮罩透明度
            , maxmin: false //允许全屏最小化
            , anim: 0 //0-6的动画形式，-1不开启
            , content: "<div id=\"defectiveAdd\" style=\"cursor:default;\">\n" +
                "        <div class=\"layui-row\">\n" +
                "            <table>\n" +
                "                <tr>\n" +
                "                    <td style=\"text-align: right;margin-bottom: 15px;\">\n" +
                "                        <label class=\"layui-form-label\" style=\"width: 100px; font-weight: bolder; font-size: large\">类别</label>\n" +
                "                    </td>\n" +
                "                    <td style=\"margin-bottom: 15px;\">\n" +
                "                        <select type=\"text\" name=\"defectiveType\" id=\"defectiveType\" style=\"width: 200px\" autocomplete=\"off\" class=\"layui-input\">\n" +
                "                            <option value=\"面料\">次品-面料问题</option>\n" +
                "                            <option value=\"印花\">次品-印花问题</option>\n" +
                "                            <option value=\"绣花\">次品-绣花问题</option>\n" +
                "                            <option value=\"脏污\">次品-脏污问题</option>\n" +
                "                            <option value=\"车缝\">次品-车缝问题</option>\n" +
                "                            <option value=\"市场退残\">次品-市场退残</option>\n" +
                "                            <option value=\"其他\">次品-其他问题</option>\n" +
                "                            <option value=\"正品余数\">正品余数</option>\n" +
                "                        </select>\n" +
                "                    </td>\n" +
                "                </tr>\n" +
                "            </table>\n" +
                "        </div>\n" +
                "        <table id=\"defectiveAddTable\" name = \"defectiveAddTable\" style=\"background-color: white; color: black\">\n" +
                "        </table>\n" +
                "    </div>"
            , yes: function (index, layero) {
                var endProductData = layui.table.cache.defectiveAddTable;
                var endProductInList = [];
                var flag = false;
                var defectiveType = $("#defectiveType").val();
                $.each(endProductData,function(i,item){
                    if (item.type === '本次'){
                        $.each(sizeNameList,function(s_i,s_item){
                            var tmp = {};
                            tmp.orderName = orderName;
                            tmp.clothesVersionNumber = clothesVersionNumber;
                            tmp.colorName = item.color;
                            tmp.sizeName = s_item;
                            tmp.inStoreCount = item[s_item];
                            tmp.storageCount = item[s_item];
                            tmp.outStoreCount = 0;
                            tmp.operateType = "in";
                            tmp.endType = "次品";
                            tmp.defectiveType = defectiveType;
                            if (item[s_item] > 0){
                                endProductInList.push(tmp);
                            }
                        });
                    }
                });
                if (endProductInList.length === 0){
                    layer.msg("请输入正确数量~~", { icon: 2 });
                    return false;
                }
                $.ajax({
                    url: "/erp/adddefectiveendproductinbatch",
                    type: 'POST',
                    data: {
                        endProductInStoreJson:JSON.stringify(endProductInList),
                        orderName: orderName,
                        endType: "次品"
                    },
                    success: function (res) {
                        if (res.result == 0) {
                            layer.close(index);
                            layer.msg("保存成功！", {icon: 1});
                            $.ajax({
                                url: "/erp/getendproductalltyperecord",
                                type: 'GET',
                                data: {
                                    orderName: orderName
                                },
                                success: function (res) {
                                    if (res.endProductList) {
                                        var reportData = res.endProductList;
                                        table.render({
                                            elem: '#reportTable'
                                            ,cols:[[
                                                {title: '#', width: '5%', collapse: true,lazy: true, icon: ['layui-icon layui-icon-triangle-r', 'layui-icon layui-icon-triangle-d'], children:[
                                                        {
                                                            title: '正品入库'
                                                            ,url: 'getendproductinstorebyorder'
                                                            ,where: function(row){
                                                                return {
                                                                    orderName: row.orderName,
                                                                    endType: "正品"
                                                                }
                                                            }
                                                            ,height: 300
                                                            ,loading:true
                                                            ,page: false
                                                            ,toolbar: '<div><a class="layui-btn layui-btn-danger layui-btn-sm" lay-event="batchDeleteIn">批量删除</a></div>'
                                                            ,totalRow: true
                                                            ,cols: [[
                                                                {type: 'checkbox', fixed: 'left'},
                                                                {field: 'id', hide: true, fixed: 'left'},
                                                                {field: 'clothesVersionNumber', title: '单号',align:'center', minWidth: 120},
                                                                {field: 'orderName', title: '款号',align:'center', minWidth: 120},
                                                                {field: 'colorName', title: '颜色',align:'center', minWidth: 120, totalRow: true},
                                                                {field: 'sizeName', title: '尺码',align:'center', minWidth: 120},
                                                                {field: 'inStoreCount', title: '入库数量',align:'center', minWidth: 120, totalRow: true},
                                                                {field: 'createTime', title: '日期',align:'center', minWidth: 120,templet: function (d) {
                                                                        return moment(d.createTime).format("YYYY-MM-DD");
                                                                    }},
                                                                {title: '操作', minWidth: 120,align:'center', templet: function(row) {
                                                                        return '<a class="layui-btn layui-btn-xs" lay-event="childEdit">修改</a>';
                                                                    }}
                                                            ]]
                                                            ,toolEvent: function (obj, pobj) {
                                                                // obj 子表当前行对象
                                                                // pobj 父表当前行对象
                                                                var filterOrderName = $("#orderName").val();
                                                                var rowData = obj.data;
                                                                var childId = this.id; // 通过 this 对象获取当前子表的id
                                                                if (obj.event === 'childEdit') {
                                                                    var index = layer.open({
                                                                        type: 1 //Page层类型
                                                                        , title: '修改'
                                                                        , btn: ['保存','取消']
                                                                        , shade: 0.6 //遮罩透明度
                                                                        , maxmin: false //允许全屏最小化
                                                                        , anim: 0 //0-6的动画形式，-1不开启
                                                                        , area: ['345px', '200px']
                                                                        , content: "<div>\n" +
                                                                            "            <table>\n" +
                                                                            "                <tr>\n" +
                                                                            "                    <td style=\"text-align: right;margin-bottom: 15px;width: 100px\">\n" +
                                                                            "                        <label class=\"layui-form-label\">原数量</label>\n" +
                                                                            "                    </td>\n" +
                                                                            "                    <td style=\"margin-bottom: 15px;\">\n" +
                                                                            "                        <input type=\"text\" name=\"initCount\" id=\"initCount\" readonly class=\"layui-input\">\n" +
                                                                            "                    </td>\n" +
                                                                            "                </tr>\n" +
                                                                            "                <tr>\n" +
                                                                            "                    <td style=\"text-align: right;margin-bottom: 15px;width: 100px\">\n" +
                                                                            "                        <label class=\"layui-form-label\">修改成</label>\n" +
                                                                            "                    </td>\n" +
                                                                            "                    <td style=\"margin-bottom: 15px;\">\n" +
                                                                            "                        <input type=\"text\" name=\"toCount\" id=\"toCount\" class=\"layui-input\">\n" +
                                                                            "                    </td>\n" +
                                                                            "                </tr>\n" +
                                                                            "            </table>\n" +
                                                                            "    </div>"
                                                                        , yes: function (index, layero) {
                                                                            var initCount= $("#initCount").val();
                                                                            var toCount= $("#toCount").val();
                                                                            var param = {};
                                                                            param.orderName = rowData.orderName;
                                                                            param.initCount = initCount;
                                                                            param.id = rowData.id;
                                                                            param.toCount = toCount;
                                                                            param.colorName = rowData.colorName;
                                                                            param.sizeName = rowData.sizeName;
                                                                            param.endType = "正品";
                                                                            if (toCount == null || toCount == ""){
                                                                                layer.msg("请填写完整", { icon: 2 });
                                                                                return false;
                                                                            }
                                                                            $.ajax({
                                                                                url: "/erp/updateinstorecount",
                                                                                type: 'POST',
                                                                                data: param,
                                                                                success: function (res) {
                                                                                    layer.closeAll();
                                                                                    if (res.result == 0){
                                                                                        initTable(filterOrderName);
                                                                                        layer.msg("修改成功！", {icon: 1});
                                                                                    } else if (res.result == 1){
                                                                                        layer.msg("修改失败！", {icon: 2});
                                                                                    } else if (res.result == 3){
                                                                                        layer.msg("数据故障,不能修改！");
                                                                                    } else if (res.result == 4){
                                                                                        layer.msg("库存不足,无法修改！");
                                                                                    }
                                                                                }, error: function () {
                                                                                    layer.msg("添加失败！", {icon: 2});
                                                                                }
                                                                            })
                                                                        }
                                                                        ,btn2: function(){
                                                                            $("#initCount").val();
                                                                            $("#toCount").val();
                                                                            layer.closeAll();
                                                                        }
                                                                        , cancel: function (i, layero) {
                                                                            $("#initCount").val();
                                                                            $("#toCount").val();
                                                                            layer.closeAll();
                                                                        }
                                                                    });
                                                                    $("#initCount").val(rowData.inStoreCount);
                                                                    return false;
                                                                }
                                                            }
                                                            ,toolbarEvent: function (obj, pobj) {
                                                                // obj 子表当前行对象
                                                                // pobj 父表当前行对象
                                                                var objData = obj.data;
                                                                var childId = this.id; // 通过 this 对象获取当前子表的id
                                                                if (obj.event === 'batchDeleteIn'){
                                                                    var checkStatus = table.checkStatus(obj.config.id);
                                                                    if(checkStatus.data.length == 0) {
                                                                        layer.msg('请选择数据');
                                                                    }else {
                                                                        var idList = [];
                                                                        var inStoreCount = 0;
                                                                        var orderName;
                                                                        $.each(checkStatus.data, function (index, item) {
                                                                            inStoreCount += item.inStoreCount;
                                                                            idList.push(item.id);
                                                                            orderName = item.orderName;
                                                                        });
                                                                        layer.confirm('真的删除吗', function(index){
                                                                            $.ajax({
                                                                                url: "/erp/deleteendproductinbatch",
                                                                                type: 'POST',
                                                                                data: {
                                                                                    idList: idList,
                                                                                    orderName: orderName,
                                                                                    endType: "正品"
                                                                                },
                                                                                traditional: true,
                                                                                success: function (res) {
                                                                                    if (res.msg) {
                                                                                        layer.close(index);
                                                                                        layer.msg(res.msg);
                                                                                    } else if (res.result == 0) {
                                                                                        layer.close(index);
                                                                                        layer.msg("删除成功！", {icon: 1});
                                                                                        table.reload(childId);
                                                                                    } else {
                                                                                        layer.msg("删除失败！", {icon: 2});
                                                                                    }
                                                                                }, error: function () {
                                                                                    layer.msg("删除失败！", {icon: 2});
                                                                                }
                                                                            })
                                                                        });
                                                                    }
                                                                }
                                                            }
                                                            ,done: function () {
                                                                soulTable.render(this);
                                                            }
                                                        }, {
                                                            title: '正品库存'
                                                            ,url: 'getendproductstoragebyorder'
                                                            ,where: function(row){
                                                                return {
                                                                    orderName: row.orderName,
                                                                    endType: "正品"
                                                                }
                                                            }
                                                            ,height: 300
                                                            ,loading:true
                                                            ,page: false
                                                            ,cols: [[
                                                                {field: 'clothesVersionNumber', title: '单号',align:'center', minWidth: 120},
                                                                {field: 'orderName', title: '款号',align:'center', minWidth: 120},
                                                                {field: 'colorName', title: '颜色',align:'center', minWidth: 120},
                                                                {field: 'sizeName', title: '尺码',align:'center', minWidth: 120 },
                                                                {field: 'storageCount', title: '数量',align:'center', minWidth: 120, totalRow: true},
                                                                {field: 'createTime', title: '最后操作',align:'center', minWidth: 120,templet: function (d) {
                                                                        return moment(d.cutDate).format("YYYY-MM-DD");
                                                                    }}
                                                            ]]
                                                            ,done: function () {
                                                                soulTable.render(this);
                                                            }
                                                        }, {
                                                            title: '正品出库'
                                                            ,url: 'getendproductoutstorebyorder'
                                                            ,where: function(row){
                                                                return {
                                                                    orderName: row.orderName,
                                                                    endType: "正品"
                                                                }
                                                            }
                                                            ,height: 300
                                                            ,loading:true
                                                            ,page: false
                                                            ,toolbar: '<div><a class="layui-btn layui-btn-danger layui-btn-sm" lay-event="batchDeleteOut">批量删除</a></div>'
                                                            ,totalRow: true
                                                            ,cols: [[
                                                                {type: 'checkbox', fixed: 'left'},
                                                                {field: 'id', hide: true, fixed: 'left'},
                                                                {field: 'clothesVersionNumber', title: '单号',align:'center', minWidth: 120},
                                                                {field: 'orderName', title: '款号',align:'center', minWidth: 120},
                                                                {field: 'colorName', title: '颜色',align:'center', minWidth: 120},
                                                                {field: 'sizeName', title: '尺码',align:'center', minWidth: 120 },
                                                                {field: 'outStoreCount', title: '数量',align:'center', minWidth: 120, totalRow: true},
                                                                {field: 'createTime', title: '最后操作',align:'center', minWidth: 120,templet: function (d) {
                                                                        return moment(d.createTime).format("YYYY-MM-DD");
                                                                    }}
                                                            ]]
                                                            ,toolbarEvent: function (obj, pobj) {
                                                                // obj 子表当前行对象
                                                                // pobj 父表当前行对象
                                                                var objData = obj.data;
                                                                var childId = this.id; // 通过 this 对象获取当前子表的id
                                                                if (obj.event === 'batchDeleteOut'){
                                                                    var checkStatus = table.checkStatus(obj.config.id);
                                                                    if(checkStatus.data.length == 0) {
                                                                        layer.msg('请选择数据');
                                                                    }else {
                                                                        var idList = [];
                                                                        var orderName;
                                                                        $.each(checkStatus.data, function (index, item) {
                                                                            idList.push(item.id);
                                                                            orderName = item.orderName;
                                                                        });
                                                                        layer.confirm('真的删除吗', function(index){
                                                                            $.ajax({
                                                                                url: "/erp/deleteendproductoutbatch",
                                                                                type: 'POST',
                                                                                data: {
                                                                                    idList: idList,
                                                                                    orderName: orderName,
                                                                                    endType: "正品"
                                                                                },
                                                                                traditional: true,
                                                                                success: function (res) {
                                                                                    if (res.result == 0) {
                                                                                        layer.close(index);
                                                                                        layer.msg("删除成功！", {icon: 1});
                                                                                        table.reload(childId);
                                                                                    } else {
                                                                                        layer.msg("删除失败！", {icon: 2});
                                                                                    }
                                                                                }, error: function () {
                                                                                    layer.msg("删除失败！", {icon: 2});
                                                                                }
                                                                            })
                                                                        });
                                                                    }
                                                                }
                                                            }
                                                            ,done: function () {
                                                                soulTable.render(this);
                                                            }
                                                        },{
                                                            title: '次品入库'
                                                            ,url: 'getdefectiveendproductinstorebyorder'
                                                            ,where: function(row){
                                                                return {
                                                                    orderName: row.orderName
                                                                }
                                                            }
                                                            ,height: 300
                                                            ,loading:true
                                                            ,page: false
                                                            ,toolbar: '<div><a class="layui-btn layui-btn-danger layui-btn-sm" lay-event="batchDeleteIn">批量删除</a></div>'
                                                            ,totalRow: true
                                                            ,cols: [[
                                                                {type: 'checkbox', fixed: 'left'},
                                                                {field: 'id', hide: true, fixed: 'left'},
                                                                {field: 'clothesVersionNumber', title: '单号',align:'center', minWidth: 120},
                                                                {field: 'orderName', title: '款号',align:'center', minWidth: 120},
                                                                {field: 'colorName', title: '颜色',align:'center', minWidth: 120, totalRow: true},
                                                                {field: 'sizeName', title: '尺码',align:'center', minWidth: 120},
                                                                {field: 'inStoreCount', title: '入库数量',align:'center', minWidth: 120, totalRow: true},
                                                                {field: 'defectiveType', title: '类别',align:'center', minWidth: 120, totalRow: true},
                                                                {field: 'createTime', title: '日期',align:'center', minWidth: 120,templet: function (d) {
                                                                        return moment(d.createTime).format("YYYY-MM-DD");
                                                                    }},
                                                                {title: '操作', minWidth: 120,align:'center', templet: function(row) {
                                                                        return '<a class="layui-btn layui-btn-xs" lay-event="childEdit">修改</a>';
                                                                    }}
                                                            ]]
                                                            ,toolEvent: function (obj, pobj) {
                                                                // obj 子表当前行对象
                                                                // pobj 父表当前行对象
                                                                var filterOrderName = $("#orderName").val();
                                                                var rowData = obj.data;
                                                                var childId = this.id; // 通过 this 对象获取当前子表的id
                                                                if (obj.event === 'childEdit') {
                                                                    var index = layer.open({
                                                                        type: 1 //Page层类型
                                                                        , title: '修改'
                                                                        , btn: ['保存','取消']
                                                                        , shade: 0.6 //遮罩透明度
                                                                        , maxmin: false //允许全屏最小化
                                                                        , anim: 0 //0-6的动画形式，-1不开启
                                                                        , area: ['345px', '200px']
                                                                        , content: "<div>\n" +
                                                                            "            <table>\n" +
                                                                            "                <tr>\n" +
                                                                            "                    <td style=\"text-align: right;margin-bottom: 15px;width: 100px\">\n" +
                                                                            "                        <label class=\"layui-form-label\">原数量</label>\n" +
                                                                            "                    </td>\n" +
                                                                            "                    <td style=\"margin-bottom: 15px;\">\n" +
                                                                            "                        <input type=\"text\" name=\"initCount\" id=\"initCount\" readonly class=\"layui-input\">\n" +
                                                                            "                    </td>\n" +
                                                                            "                </tr>\n" +
                                                                            "                <tr>\n" +
                                                                            "                    <td style=\"text-align: right;margin-bottom: 15px;width: 100px\">\n" +
                                                                            "                        <label class=\"layui-form-label\">修改成</label>\n" +
                                                                            "                    </td>\n" +
                                                                            "                    <td style=\"margin-bottom: 15px;\">\n" +
                                                                            "                        <input type=\"text\" name=\"toCount\" id=\"toCount\" class=\"layui-input\">\n" +
                                                                            "                    </td>\n" +
                                                                            "                </tr>\n" +
                                                                            "            </table>\n" +
                                                                            "    </div>"
                                                                        , yes: function (index, layero) {
                                                                            var initCount= $("#initCount").val();
                                                                            var toCount= $("#toCount").val();
                                                                            var param = {};
                                                                            param.orderName = rowData.orderName;
                                                                            param.initCount = initCount;
                                                                            param.id = rowData.id;
                                                                            param.toCount = toCount;
                                                                            param.colorName = rowData.colorName;
                                                                            param.sizeName = rowData.sizeName;
                                                                            param.endType = "次品";
                                                                            if (toCount == null || toCount == ""){
                                                                                layer.msg("请填写完整", { icon: 2 });
                                                                                return false;
                                                                            }
                                                                            $.ajax({
                                                                                url: "/erp/updatedefectiveinstorecount",
                                                                                type: 'POST',
                                                                                data: param,
                                                                                success: function (res) {
                                                                                    layer.closeAll();
                                                                                    if (res.result == 0){
                                                                                        initTable(filterOrderName);
                                                                                        layer.msg("修改成功！", {icon: 1});
                                                                                    } else if (res.result == 1){
                                                                                        layer.msg("修改失败！", {icon: 2});
                                                                                    } else if (res.result == 3){
                                                                                        layer.msg("数据故障,不能修改！");
                                                                                    } else if (res.result == 4){
                                                                                        layer.msg("库存不足,无法修改！");
                                                                                    }
                                                                                }, error: function () {
                                                                                    layer.msg("添加失败！", {icon: 2});
                                                                                }
                                                                            })
                                                                        }
                                                                        ,btn2: function(){
                                                                            $("#initCount").val();
                                                                            $("#toCount").val();
                                                                            layer.closeAll();
                                                                        }
                                                                        , cancel: function (i, layero) {
                                                                            $("#initCount").val();
                                                                            $("#toCount").val();
                                                                            layer.closeAll();
                                                                        }
                                                                    });
                                                                    $("#initCount").val(rowData.inStoreCount);
                                                                    return false;
                                                                }
                                                            }
                                                            ,toolbarEvent: function (obj, pobj) {
                                                                // obj 子表当前行对象
                                                                // pobj 父表当前行对象
                                                                var objData = obj.data;
                                                                var childId = this.id; // 通过 this 对象获取当前子表的id
                                                                if (obj.event === 'batchDeleteIn'){
                                                                    var checkStatus = table.checkStatus(obj.config.id);
                                                                    if(checkStatus.data.length == 0) {
                                                                        layer.msg('请选择数据');
                                                                    }else {
                                                                        var idList = [];
                                                                        var inStoreCount = 0;
                                                                        var orderName;
                                                                        $.each(checkStatus.data, function (index, item) {
                                                                            inStoreCount += item.inStoreCount;
                                                                            idList.push(item.id);
                                                                            orderName = item.orderName;
                                                                        });
                                                                        layer.confirm('真的删除吗', function(index){
                                                                            $.ajax({
                                                                                url: "/erp/deleteendproductinbatch",
                                                                                type: 'POST',
                                                                                data: {
                                                                                    idList: idList,
                                                                                    orderName: orderName,
                                                                                    endType: "次品"
                                                                                },
                                                                                traditional: true,
                                                                                success: function (res) {
                                                                                    if (res.msg) {
                                                                                        layer.close(index);
                                                                                        layer.msg(res.msg);
                                                                                    } else if (res.result == 0) {
                                                                                        layer.close(index);
                                                                                        layer.msg("删除成功！", {icon: 1});
                                                                                        table.reload(childId);
                                                                                    } else {
                                                                                        layer.msg("删除失败！", {icon: 2});
                                                                                    }
                                                                                }, error: function () {
                                                                                    layer.msg("删除失败！", {icon: 2});
                                                                                }
                                                                            })
                                                                        });
                                                                    }
                                                                }
                                                            }
                                                            ,done: function () {
                                                                soulTable.render(this);
                                                            }
                                                        }, {
                                                            title: '次品库存'
                                                            ,url: 'getdefectiveendproductstoragebyorder'
                                                            ,where: function(row){
                                                                return {
                                                                    orderName: row.orderName
                                                                }
                                                            }
                                                            ,height: 300
                                                            ,loading:true
                                                            ,page: false
                                                            ,cols: [[
                                                                {field: 'clothesVersionNumber', title: '单号',align:'center', minWidth: 120},
                                                                {field: 'orderName', title: '款号',align:'center', minWidth: 120},
                                                                {field: 'colorName', title: '颜色',align:'center', minWidth: 120},
                                                                {field: 'sizeName', title: '尺码',align:'center', minWidth: 120 },
                                                                {field: 'storageCount', title: '数量',align:'center', minWidth: 120, totalRow: true},
                                                                {field: 'defectiveType', title: '类别',align:'center', minWidth: 120, totalRow: true},
                                                                {field: 'createTime', title: '日期',align:'center', minWidth: 120,templet: function (d) {
                                                                        return moment(d.createTime).format("YYYY-MM-DD");
                                                                    }}
                                                            ]]
                                                            ,done: function () {
                                                                soulTable.render(this);
                                                            }
                                                        }, {
                                                            title: '次品出库'
                                                            ,url: 'getdefectiveendproductoutstorebyorder'
                                                            ,where: function(row){
                                                                return {
                                                                    orderName: row.orderName
                                                                }
                                                            }
                                                            ,height: 300
                                                            ,loading:true
                                                            ,page: false
                                                            ,toolbar: '<div><a class="layui-btn layui-btn-danger layui-btn-sm" lay-event="batchDeleteOut">批量删除</a></div>'
                                                            ,totalRow: true
                                                            ,cols: [[
                                                                {type: 'checkbox', fixed: 'left'},
                                                                {field: 'id', hide: true, fixed: 'left'},
                                                                {field: 'clothesVersionNumber', title: '单号',align:'center', minWidth: 120},
                                                                {field: 'orderName', title: '款号',align:'center', minWidth: 120},
                                                                {field: 'colorName', title: '颜色',align:'center', minWidth: 120},
                                                                {field: 'sizeName', title: '尺码',align:'center', minWidth: 120 },
                                                                {field: 'outStoreCount', title: '数量',align:'center', minWidth: 120, totalRow: true},
                                                                {field: 'defectiveType', title: '类别',align:'center', minWidth: 120, totalRow: true},
                                                                {field: 'createTime', title: '最后操作',align:'center', minWidth: 120,templet: function (d) {
                                                                        return moment(d.cutDate).format("YYYY-MM-DD");
                                                                    }}
                                                            ]]
                                                            ,toolbarEvent: function (obj, pobj) {
                                                                // obj 子表当前行对象
                                                                // pobj 父表当前行对象
                                                                var objData = obj.data;
                                                                var childId = this.id; // 通过 this 对象获取当前子表的id
                                                                if (obj.event === 'batchDeleteOut'){
                                                                    var checkStatus = table.checkStatus(obj.config.id);
                                                                    if(checkStatus.data.length == 0) {
                                                                        layer.msg('请选择数据');
                                                                    }else {
                                                                        var idList = [];
                                                                        var orderName;
                                                                        $.each(checkStatus.data, function (index, item) {
                                                                            idList.push(item.id);
                                                                            orderName = item.orderName;
                                                                        });
                                                                        layer.confirm('真的删除吗', function(index){
                                                                            $.ajax({
                                                                                url: "/erp/deletedefectiveendproductoutbatch",
                                                                                type: 'POST',
                                                                                data: {
                                                                                    idList: idList,
                                                                                    orderName: orderName,
                                                                                    endType: "次品"
                                                                                },
                                                                                traditional: true,
                                                                                success: function (res) {
                                                                                    if (res.result == 0) {
                                                                                        layer.close(index);
                                                                                        layer.msg("删除成功！", {icon: 1});
                                                                                        table.reload(childId);
                                                                                    } else {
                                                                                        layer.msg("删除失败！", {icon: 2});
                                                                                    }
                                                                                }, error: function () {
                                                                                    layer.msg("删除失败！", {icon: 2});
                                                                                }
                                                                            })
                                                                        });
                                                                    }
                                                                }
                                                            }
                                                            ,done: function () {
                                                                soulTable.render(this);
                                                            }
                                                        }
                                                    ]}
                                                ,{type:'numbers', align:'center', title:'序号', width: '5%'}
                                                ,{field:'clothesVersionNumber', title:'单号', align:'center', width: '9%', sort: true, filter: true, totalRowText: '合计'}
                                                ,{field:'orderName', title:'款号', align:'center', width: '9%', sort: true, filter: true}
                                                ,{field:'inStoreCount', title:'入库数量', align:'center', width: '8%', sort: true, filter: true, totalRow: true}
                                                ,{field:'storageCount', title:'库存数量', align:'center', width: '8%', sort: true, filter: true, totalRow: true}
                                                ,{field:'outStoreCount', title:'出库数量', align:'center', width: '8%', sort: true, filter: true, totalRow: true}
                                                ,{field:'defectiveInStore', title:'次品入库', align:'center', width: '8%', sort: true, filter: true, totalRow: true}
                                                ,{field:'defectiveStorage', title:'次品库存', align:'center', width: '8%', sort: true, filter: true, totalRow: true}
                                                ,{field:'defectiveOutStore', title:'次品出库', align:'center', width: '8%', sort: true, filter: true, totalRow: true}
                                                ,{title: '操作', width: '24%', templet: '#barTop', align:'center'}
                                            ]]
                                            ,excel: {
                                                filename: '成品进销存.xlsx'
                                            }
                                            ,data: reportData
                                            ,height: 'full-130'
                                            ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                                            ,title: '成品进销存'
                                            ,totalRow: true
                                            ,loading: false
                                            ,page: true
                                            ,even: true
                                            ,limits: [500, 1000, 2000]
                                            ,limit: 1000 //每页默认显示的数量
                                            ,done: function () {
                                                soulTable.render(this);
                                            }
                                        });

                                    } else {
                                        layer.msg("获取失败！", {icon: 2});
                                    }
                                }, error: function () {
                                    layer.msg("获取失败！", {icon: 2});
                                }
                            });
                        } else {
                            layer.msg("保存失败！", {icon: 2});
                        }
                    },
                    error: function () {
                        layer.msg("保存失败！", {icon: 2});
                    }
                });
                return false;
            }
            , btn2: function (index, layero) {
                var endProductData = layui.table.cache.defectiveAddTable;
                $.each(endProductData,function(i,item){
                    if (item.type === '本次'){
                        var colorSum = 0;
                        $.each(sizeNameList,function(s_i,s_item){
                            colorSum += Number(item[s_item]);
                        });
                        item.colorSum = colorSum;
                    }
                });
                table.reload("defectiveAddTable",{
                    data:endProductData   // 将新数据重新载入表格
                });
                return false;
            }
            , cancel: function (i, layero) {

            }
        });
        layer.full(index);
        setTimeout(function () {
            $.ajax({
                url: "/erp/getendproductpreinstoredata",
                type: 'GET',
                data: {
                    orderName: orderName,
                    endType: "次品"
                },
                success: function (res) {
                    colorNameList = res.colorNameList;
                    sizeNameList = res.sizeNameList;
                    orderInfoList = res.orderInfoList;
                    sizeNameList.sort(function(a,b){
                        var order = ["XXXXS","4XS","XXXS","3XS","XXS","2XS","xxs","XS","xs","S","s","M","m","L","l","XL","xl","XXL","2XL","xxl","XXXL","3XL","XXXXL","4XL","XXXXXL","5XL","xxxl","070","70","073","075","75","080","80","085","090","90","095","95", "100", "105","110","115", "120","125", "130","135",
                            "140","145", "150","155", "160","165", "170","175", "180","185","190","195","200","XXS/155/72A","XS/160/76A","S/165/80A","M/170/84A","L/175/88A","XL/180/92A","XXL/185/96A","XXXL/190/100A","XXXXL/190/104A","XXS/145/72A","XS/150/76A","S/155/80A","M/160/84A","L/165/88A","XL/170/92A","XXL/175/96A","XXXL/180/100A","XXXXL/180/104A","XXXXXL/185/108A","110/56","120/60","130/64","140/68","150/72","160/76","170/80","2T","3T","4T","5T","4/5","6/6X","7/8","10/12","04(110-56)","06(120-60)","08(130-64)","10(140-64)","12(150-72)","14(155-76)","18M/73","24M/80","3Y/90","4Y/100","5Y/110","6Y/120","7Y/130","9Y/140","11Y/150","13Y/160"];
                        return order.indexOf(a) - order.indexOf(b);
                    });
                    var title = [
                        {field: 'color', title: '颜色',minWidth:100, fixed: true},
                        {field: 'type', title: '类别',minWidth:100, fixed: true}
                    ];
                    $.each(sizeNameList,function(index,value){
                        var field = {};
                        field.field = value;
                        field.title = value;
                        field.minWidth = 80;
                        field.edit = 'text';
                        title.push(field);
                    });
                    title.push({field: 'colorSum', title: '合计',minWidth:100});
                    var colorSizeData = [];
                    $.each(colorNameList,function(index,value){
                        var tmp1 = {};
                        var tmp2 = {};
                        var tmp3 = {};
                        tmp1.color = value;
                        tmp2.color = value;
                        tmp3.color = value;
                        tmp1.type = "好片";
                        tmp2.type = "已入";
                        tmp3.type = "本次";
                        tmp1.colorSum = 0;
                        tmp2.colorSum = 0;
                        tmp3.colorSum = 0;
                        $.each(sizeNameList,function(index2,value2){
                            $.each(orderInfoList,function(index3,value3){
                                if (value3.colorName === value && value3.sizeName === value2){
                                    tmp1[value2] = value3.wellCount;
                                    tmp2[value2] = value3.finishCount;
                                    tmp3[value2] = 0;
                                    tmp1.colorSum += value3.wellCount;
                                    tmp2.colorSum += value3.finishCount;
                                }
                            })
                        });
                        colorSizeData.push(tmp1);
                        colorSizeData.push(tmp2);
                        colorSizeData.push(tmp3);
                    });
                    var basicOrderClothesTable = table.render({
                        elem: '#defectiveAddTable'
                        ,cols: [title]
                        ,data: colorSizeData
                        ,limit: 500 //每页默认显示的数量
                        ,height: 'full-200'
                        ,done:function () {
                        }
                    });
                    return false;
                },
                error: function () {
                    layer.msg("获取数据失败！", {icon: 2});
                }
            })
        }, 100);

    })
    return false;
}
function toDecimal(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*100)/100;
    return f;
}
function toDecimalFour(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*10000)/10000;
    return f;
}
function doHandleDate() {
    var myDate = new Date();
    var tYear = myDate.getFullYear();
    var tMonth = myDate.getMonth();

    var m = tMonth + 1;
    if (m.toString().length == 1) {
        m = "0" + m;
    }
    return tYear +'-'+ m;
}