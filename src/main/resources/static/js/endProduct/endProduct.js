var form;
layui.use('form', function(){
    form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
    form.render();
});
layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.20'
}).extend({
    soulTable: 'soulTable'
});
$(document).ready(function () {

    form.render('select');
    layui.laydate.render({
        elem: '#monthStr',
        type: 'month',
        trigger: 'click',
        value: doHandleDate(),
        isInitValue: true
    });
    $.ajax({
        url: "/erp/getallcustomer",
        type: 'GET',
        data: {
        },
        success: function (res) {
            $("#customerName").empty();
            if (res.data) {
                $.each(res.data, function(index,element){
                    $("#customerName").append("<option value='"+element.customerName+"'>"+element.customerName+"</option>");
                })
            }
            layui.form.render("select");
        },
        error: function () {
            layer.msg("获取跟单失败", { icon: 2 });
        }
    });

    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumber", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });
});
$(document).on('keydown', '.layui-table-edit', function (e) {
    var td = $(this).parent('td')
        ,tr = td.parent('tr')
        ,trs = tr.parent().parent().find('tr')
        ,tr_index = tr.index()
        ,td_index = td.index()
        ,td_last_index = tr.find('[data-edit="text"]:last').index()
        ,td_first_index = tr.find('[data-edit="text"]:first').index();
    switch (e.keyCode) {
        case 13:
            tr.next().find('td').eq(td_index).click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            return false;

        case 39:
            td.nextAll('[data-edit="text"]:first').click();
            if (td_index == td_last_index){
                tr.next().find('td').eq(td_first_index).click();
                if (tr_index == trs.length - 1){
                    trs.eq(0).find('td').eq(td_first_index).click();
                }
            }
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
        case 37:
            td.prevAll('[data-edit="text"]:first').click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
        case 38:
            tr.prev().find('td').eq(td_index).click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
        case 40:
            tr.next().find('td').eq(td_index).click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
    }

});
var reportTable;
var userName = $("#userName").val();
layui.use(['form', 'table', 'soulTable'], function () {
    var table = layui.table,
        soulTable = layui.soulTable,
        form = layui.form,
        $ = layui.$;
    reportTable = table.render({
        elem: '#reportTable'
        ,cols:[[]]
        ,excel: {
            filename: '成品结算.xlsx'
        }
        ,height: 'full-130'
        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
        ,title: '成品结算'
        ,totalRow: true
        ,loading: false
        ,page: true
        ,even: true
        ,limits: [500, 1000, 2000]
        ,limit: 1000 //每页默认显示的数量
        ,done: function () {
            soulTable.render(this);
        }
    });

    initTable('', '', '', '分款');

    function initTable(monthStr, customerName, orderName, type){
        var load = layer.load();
        var param = {};
        if (monthStr != null && monthStr != ''){
            param.monthStr = monthStr;
        }
        if (customerName != null && customerName != ''){
            param.customerName = customerName;
        }
        if (orderName != null && orderName != ''){
            param.orderName = orderName;
        }
        if (type === '分款'){
            $.ajax({
                url: "/erp/getendproductpaysummary",
                type: 'GET',
                data: param,
                success: function (res) {
                    if (res.endProductPayList) {
                        var reportData = res.endProductPayList;
                        table.render({
                            elem: '#reportTable'
                            , cols: [[
                                {title: '#', width: '5%', collapse: true, lazy: true, icon: ['layui-icon layui-icon-triangle-r', 'layui-icon layui-icon-triangle-d'], children: [
                                        {
                                            title: '出货记录'
                                            ,url: 'getendproductoutstorebyorder'
                                            ,where: function(row){
                                                return {
                                                    orderName: row.orderName,
                                                    endType: "正品"
                                                }
                                            }
                                            ,height: 300
                                            ,loading:true
                                            ,page: false
                                            ,toolbar: '<div><a class="layui-btn layui-btn-danger layui-btn-sm" lay-event="quitAccount">取消入账</a></div>'
                                            ,totalRow: true
                                            ,cols: [[
                                                {type: 'checkbox', fixed: 'left'},
                                                {field: 'id', hide: true, fixed: 'left'},
                                                {field: 'clothesVersionNumber', title: '单号',align:'center', minWidth: 120},
                                                {field: 'orderName', title: '款号',align:'center', minWidth: 120},
                                                {field: 'colorName', title: '颜色',align:'center', minWidth: 120},
                                                {field: 'sizeName', title: '尺码',align:'center', minWidth: 120 },
                                                {field: 'outStoreCount', title: '数量',align:'center', minWidth: 120, totalRow: true},
                                                {field: 'accountState', title: '入账',align:'center', minWidth: 120},
                                                {field: 'updateTime', title: '最后操作',align:'center', minWidth: 120,templet: function (d) {
                                                        return moment(d.updateTime).format("YYYY-MM-DD");
                                                    }},
                                                {field: 'payNumber', title: '对账号',align:'center', minWidth: 120}
                                            ]]
                                            ,toolbarEvent: function (obj, pobj) {
                                                // obj 子表当前行对象
                                                // pobj 父表当前行对象
                                                var objData = obj.data;
                                                var childId = this.id; // 通过 this 对象获取当前子表的id
                                                if (obj.event === 'quitAccount'){
                                                    var checkStatus = table.checkStatus(obj.config.id);
                                                    if(checkStatus.data.length == 0) {
                                                        layer.msg('请选择数据');
                                                    }else {
                                                        var idList = [];
                                                        var payNumberList = [];
                                                        var orderName;
                                                        $.each(checkStatus.data, function (index, item) {
                                                            idList.push(item.id);
                                                            orderName = item.orderName;
                                                            if (payNumberList.indexOf(item.payNumber) == -1){
                                                                payNumberList.push(item.payNumber);
                                                            }
                                                        });
                                                        if (idList.length == 0){
                                                            layer.msg("请先选择数据");
                                                            return false;
                                                        }
                                                        if (payNumberList.length != 1){
                                                            layer.msg("一次只能取消一个对账号,且取消一条数据该对账号的所有数据都会取消~~~");
                                                            return false;
                                                        }
                                                        layer.confirm('确认取消吗', function(index){
                                                            $.ajax({
                                                                url: "/erp/quitaccountbypaynumber",
                                                                type: 'POST',
                                                                data: {
                                                                    payNumber: payNumberList[0]
                                                                },
                                                                traditional: true,
                                                                success: function (res) {
                                                                    if (res.result == 0) {
                                                                        layer.close(index);
                                                                        layer.msg("提交成功！", {icon: 1});
                                                                        table.reload(childId);
                                                                    } else {
                                                                        layer.msg("提交失败！", {icon: 2});
                                                                    }
                                                                }, error: function () {
                                                                    layer.msg("提交失败！", {icon: 2});
                                                                }
                                                            })
                                                        });
                                                    }
                                                }
                                            }
                                            ,done: function () {
                                                soulTable.render(this);
                                            }
                                        }
                                    ]}
                                , {type: 'numbers', align: 'center', title: '序号', width: '5%'}
                                , {field: 'customerName', title: '客户', align: 'center', width: '7%', sort: true, filter: true, totalRowText: '合计'}
                                , {field: 'clothesVersionNumber', title: '单号', align: 'center', width: '12%', sort: true, filter: true, totalRowText: '合计'}
                                , {field: 'orderName', title: '款号', align: 'center', width: '9%', sort: true, filter: true}
                                , {field: 'inStoreCount', title: '入库数量', align: 'center', width: '9%', sort: true, filter: true, totalRow: true}
                                , {field: 'storageCount', title: '库存数量', align: 'center', width: '9%', sort: true, filter: true, totalRow: true}
                                , {field: 'outStoreCount', title: '出库数量', align: 'center', width: '9%', sort: true, filter: true, totalRow: true}
                                , {field: 'accountCount', title: '对账数量', align: 'center', width: '9%', sort: true, filter: true, totalRow: true}
                                , {field: 'price', title: '单价', align: 'center', width: '7%', sort: true, filter: true, totalRow: true}
                                , {field: 'totalMoney', title: '对账金额', align: 'center', width: '9%', sort: true, filter: true, totalRow: true}
                                , {field: 'remark', title: '备注', align: 'center', width: '7%', sort: true, filter: true, totalRow: true}
                            ]]
                            ,excel: {
                                filename: '成品结算.xlsx'
                            }
                            ,data: reportData
                            ,height: 'full-130'
                            ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                            ,title: '成品结算'
                            ,totalRow: true
                            ,loading: false
                            ,page: true
                            ,even: true
                            ,limits: [500, 1000, 2000]
                            ,limit: 1000 //每页默认显示的数量
                            ,done: function () {
                                soulTable.render(this);
                                layer.close(load);
                            }
                        });

                    } else {
                        layer.msg("获取失败！", {icon: 2});
                    }
                }, error: function () {
                    layer.msg("获取失败！", {icon: 2});
                }
            });
        } else if (type === '明细'){
            $.ajax({
                url: "/erp/getendproductoutstorebyinfo",
                type: 'GET',
                data: param,
                success: function (res) {
                    if (res.data) {
                        var reportData = res.data;
                        table.render({
                            elem: '#reportTable'
                            , cols: [[
                                {type: 'numbers', align: 'center', title: '序号', width: '5%'}
                                , {field: 'customerName', title: '客户', align: 'center', width: '9%', sort: true, filter: true, totalRowText: '合计'}
                                , {field: 'clothesVersionNumber', title: '单号', align: 'center', width: '9%', sort: true, filter: true}
                                , {field: 'orderName', title: '款号', align: 'center', width: '9%', sort: true, filter: true}
                                , {field: 'colorName', title: '颜色', align: 'center', width: '9%', sort: true, filter: true}
                                , {field: 'sizeName', title: '尺码', align: 'center', width: '9%', sort: true, filter: true}
                                , {field: 'outStoreCount', title: '出库数量', align: 'center', width: '7%', sort: true, filter: true, totalRow: true}
                                , {field:'price', title:'单价', align:'center', width: '7%', sort: true, filter: true}
                                , {field: 'accountState', title: '入账状态', align: 'center', width: '7%', sort: true, filter: true}
                                , {field:'totalMoney', title:'金额', align:'center', width: '7%', sort: true, filter: true, totalRow: true}
                                , {field:'createTime', title:'出库日期', align:'center', width: '9%', sort: true, filter: true, templet:function (d) {
                                        return moment(d.createTime).format("YYYY-MM-DD");
                                    }}
                            ]]
                            ,excel: {
                                filename: '出库明细.xlsx'
                            }
                            ,data: reportData
                            ,height: 'full-130'
                            ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                            ,title: '出库明细'
                            ,totalRow: true
                            ,loading: false
                            ,page: true
                            ,even: true
                            ,limits: [500, 1000, 2000]
                            ,limit: 1000 //每页默认显示的数量
                            ,done: function () {
                                soulTable.render(this);
                                layer.close(load);
                            }
                        });

                    } else {
                        layer.msg("获取失败！", {icon: 2});
                    }
                }, error: function () {
                    layer.msg("获取失败！", {icon: 2});
                }
            });
        } else if (type === '客户') {
            if (monthStr == null || monthStr == ''){
                layer.msg("月份必须~~~");
                layer.close(load);
                return false;
            }
            $.ajax({
                url: "/erp/getendproductpaybyinfo",
                type: 'GET',
                data: param,
                success: function (res) {
                    if (res.data) {
                        var reportData = res.data;
                        table.render({
                            elem: '#reportTable'
                            , cols: [[
                                {type: 'numbers', align: 'center', title: '序号', width: '5%'}
                                , {field: 'payMonth', title: '月份', align: 'center', width: '6%', sort: true, filter: true, totalRowText: '合计'}
                                , {field: 'customerName', title: '客户', align: 'center', width: '8%', sort: true, filter: true}
                                , {field: 'payType', title: '类别', align: 'center', width: '7%', sort: true, filter: true}
                                , {field: 'clothesVersionNumber', title: '单号', align: 'center', width: '10%', sort: true, filter: true}
                                , {field: 'orderName', title: '款号', align: 'center', width: '10%', sort: true, filter: true}
                                , {field: 'payNumber', title: '对账号', align: 'center', width: '10%', sort: true, filter: true}
                                , {field: 'payDate', title: '日期', align: 'center', width: '7%', sort: true, filter: true, templet:function (d) {
                                        return moment(d.payDate).format("YYYY-MM-DD");
                                    }}
                                , {field: 'sumMoney', title:'对账金额', align:'center', width: '8%', sort: true, filter: true, totalRow: true}
                                , {field: 'decreaseMoney', title: '扣款金额', align: 'center', width: '8%', sort: true, filter: true, totalRow: true}
                                , {field: 'increaseMoney', title: '收款金额', align: 'center', width: '8%', sort: true, filter: true, totalRow: true}
                                , {field: 'balanceMoney', title: '结余', align: 'center', width: '8%', sort: true, filter: true, totalRow: true}
                                , {field: 'id', title:'操作', align:'center', toolbar: '#customerBarTop', width:'5%'}
                            ]]
                            ,excel: {
                                filename: '月度汇总.xlsx'
                            }
                            ,data: reportData
                            ,height: 'full-130'
                            ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                            ,title: '月度汇总'
                            ,totalRow: true
                            ,loading: false
                            ,page: true
                            ,even: true
                            ,limits: [500, 1000, 2000]
                            ,limit: 1000 //每页默认显示的数量
                            ,done: function () {
                                soulTable.render(this);
                                layer.close(load);
                            }
                        });

                    } else {
                        layer.msg("获取失败！", {icon: 2});
                    }
                }, error: function () {
                    layer.msg("获取失败！", {icon: 2});
                }
            });
        }

    }
    form.on('submit(searchBeat)', function(data){
        var monthStr = $("#monthStr").val();
        var customerName = $("#customerName").val();
        var type = $("#type").val();
        var orderName = $("#orderName").val();
        initTable(monthStr, customerName, orderName, type);
        return false;
    });

    table.on('toolbar(reportTable)', function(obj) {
        var filterOrderName = $("#orderName").val();
        var filterMonth = $("#monthStr").val();
        var filterCustomerName = $("#customerName").val();
        var filterType = $("#type").val();
        if (obj.event === 'clearFilter') {
            // 清除所有筛选条件并重载表格
            // 参数: tableId
            soulTable.clearFilter('reportTable')
        } else if (obj.event === 'exportExcel') {
            soulTable.export('reportTable');
        } else if (obj.event === 'refresh') {
            initTable();
        } else if (obj.event === 'summary') {
            $.ajax({
                url: "/erp/getendproductpaysummarybycustomer",
                type: 'GET',
                data: {
                    monthStr: $("#monthStr").val()
                },
                success: function (res) {
                    if (res.endProductList) {
                        var reportData = res.endProductList;
                        table.render({
                            elem: '#reportTable'
                            ,cols:[[
                                {type:'numbers', align:'center', title:'序号', width: '5%'}
                                ,{field:'customerName', title:'客户', align:'center', width: '10%', sort: true, filter: true, totalRowText: '合计'}
                                ,{field:'monthStr', title:'月份', align:'center', width: '15%', sort: true, filter: true, totalRowText: '合计'}
                                ,{field:'sumMoney', title:'应收', align:'center', width: '15%', sort: true, filter: true}
                                ,{field:'receiveMoney', title:'已收', align:'center', width: '10%', sort: true, filter: true, totalRow: true}
                                ,{field:'deduction', title:'扣款', align:'center', width: '10%', sort: true, filter: true, totalRow: true}
                                ,{field:'remainMoney', title:'未收', align:'center', width: '10%', sort: true, filter: true, totalRow: true}
                            ]]
                            ,excel: {
                                filename: '品牌月度账单.xlsx'
                            }
                            ,data: reportData
                            ,height: 'full-130'
                            ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                            ,title: '品牌月度账单'
                            ,totalRow: true
                            ,loading: false
                            ,page: true
                            ,even: true
                            ,limits: [500, 1000, 2000]
                            ,limit: 1000 //每页默认显示的数量
                            ,done: function () {
                                soulTable.render(this);
                            }
                        });

                    } else {
                        layer.msg("获取失败！", {icon: 2});
                    }
                }, error: function () {
                    layer.msg("获取失败！", {icon: 2});
                }
            });
        } else if (obj.event === 'increase') {
            var index = layer.open({
                type: 1 //Page层类型
                , title: '收款'
                , btn: ['保存','取消']
                , shade: 0.6 //遮罩透明度
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , area: ['345px', '240px']
                , content: "<div>\n" +
                    "            <table>\n" +
                    "                <tr>\n" +
                    "                    <td style=\"text-align: right;margin-bottom: 15px;width: 100px\">\n" +
                    "                        <label class=\"layui-form-label\">客户</label>\n" +
                    "                    </td>\n" +
                    "                    <td style=\"margin-bottom: 15px;\">\n" +
                    "                        <select type=\"text\" name=\"payCustomer\" autocomplete=\"off\" id=\"payCustomer\" class=\"layui-input\"></select>\n" +
                    "                    </td>\n" +
                    "                </tr>\n" +
                    "                <tr>\n" +
                    "                    <td style=\"text-align: right;margin-bottom: 15px;width: 100px\">\n" +
                    "                        <label class=\"layui-form-label\">日期</label>\n" +
                    "                    </td>\n" +
                    "                    <td style=\"margin-bottom: 15px;\">\n" +
                    "                        <input type=\"text\" name=\"payDate\" id=\"payDate\" class=\"layui-input\">\n" +
                    "                    </td>\n" +
                    "                </tr>\n" +
                    "                <tr>\n" +
                    "                    <td style=\"text-align: right;margin-bottom: 15px;width: 100px\">\n" +
                    "                        <label class=\"layui-form-label\">金额</label>\n" +
                    "                    </td>\n" +
                    "                    <td style=\"margin-bottom: 15px;\">\n" +
                    "                        <input type=\"text\" name=\"sumMoney\" id=\"sumMoney\" class=\"layui-input\">\n" +
                    "                    </td>\n" +
                    "                </tr>\n" +
                    "            </table>\n" +
                    "    </div>"
                , yes: function (index, layero) {
                    var customerName= $("#payCustomer").val();
                    var payDate= $("#payDate").val();
                    var sumMoney= $("#sumMoney").val();
                    var param = {};
                    param.customerName = customerName;
                    param.payDate = payDate;
                    param.sumMoney = 0;
                    param.payType = '收款';
                    param.payMonth = payDate.substring(0, 7);
                    param.increaseMoney = sumMoney;
                    param.decreaseMoney = 0;
                    param.balanceMoney = -sumMoney;
                    if (customerName == null || customerName == "" || payDate == null || payDate == "" || sumMoney == null || sumMoney == ""){
                        layer.msg("请填写完整", { icon: 2 });
                        return false;
                    }
                    $.ajax({
                        url: "/erp/addendproductpay",
                        type: 'POST',
                        data: {
                            endProductPayJson: JSON.stringify(param)
                        },
                        success: function (res) {
                            layer.closeAll();
                            if (res.result == 0){
                                initTable(filterMonth, filterCustomerName,filterOrderName,filterType);
                                layer.msg("录入成功！", {icon: 1});
                            }
                        }, error: function () {
                            layer.msg("添加失败！", {icon: 2});
                        }
                    })
                }
                ,btn2: function(){
                    $("#payCustomer").val();
                    $("#payDate").val();
                    $("#sumMoney").val();
                    layer.closeAll();
                }
                , cancel: function (i, layero) {
                    $("#payCustomer").val();
                    $("#payDate").val();
                    $("#sumMoney").val();
                    layer.closeAll();
                }
            });
            setTimeout(function () {
                $.ajax({
                    url: "/erp/getallcustomer",
                    type: 'GET',
                    data: {
                    },
                    success: function (res) {
                        $("#payCustomer").empty();
                        if (res.data) {
                            $.each(res.data, function(index,element){
                                $("#payCustomer").append("<option value='"+element.customerName+"'>"+element.customerName+"</option>");
                            })
                        }
                        layui.form.render("select");
                    },
                    error: function () {
                        layer.msg("获取跟单失败", { icon: 2 });
                    }
                });
            }, 100);
            form.render('select');
            layui.laydate.render({
                elem: '#payDate',
                type: 'date',
                trigger: 'click',
                value: new Date(),
                isInitValue: true
            });
            return false;
        } else if (obj.event === 'decrease') {
            var index = layer.open({
                type: 1 //Page层类型
                , title: '扣款'
                , btn: ['保存','取消']
                , shade: 0.6 //遮罩透明度
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , area: ['345px', '240px']
                , content: "<div>\n" +
                    "            <table>\n" +
                    "                <tr>\n" +
                    "                    <td style=\"text-align: right;margin-bottom: 15px;width: 100px\">\n" +
                    "                        <label class=\"layui-form-label\">客户</label>\n" +
                    "                    </td>\n" +
                    "                    <td style=\"margin-bottom: 15px;\">\n" +
                    "                        <select type=\"text\" name=\"payCustomer\" autocomplete=\"off\" id=\"payCustomer\" class=\"layui-input\"></select>\n" +
                    "                    </td>\n" +
                    "                </tr>\n" +
                    "                <tr>\n" +
                    "                    <td style=\"text-align: right;margin-bottom: 15px;width: 100px\">\n" +
                    "                        <label class=\"layui-form-label\">日期</label>\n" +
                    "                    </td>\n" +
                    "                    <td style=\"margin-bottom: 15px;\">\n" +
                    "                        <input type=\"text\" name=\"payDate\" id=\"payDate\" class=\"layui-input\">\n" +
                    "                    </td>\n" +
                    "                </tr>\n" +
                    "                <tr>\n" +
                    "                    <td style=\"text-align: right;margin-bottom: 15px;width: 100px\">\n" +
                    "                        <label class=\"layui-form-label\">金额</label>\n" +
                    "                    </td>\n" +
                    "                    <td style=\"margin-bottom: 15px;\">\n" +
                    "                        <input type=\"text\" name=\"sumMoney\" id=\"sumMoney\" class=\"layui-input\">\n" +
                    "                    </td>\n" +
                    "                </tr>\n" +
                    "            </table>\n" +
                    "    </div>"
                , yes: function (index, layero) {
                    var customerName= $("#payCustomer").val();
                    var payDate= $("#payDate").val();
                    var sumMoney= $("#sumMoney").val();
                    var param = {};
                    param.customerName = customerName;
                    param.payDate = payDate;
                    param.sumMoney = 0;
                    param.payType = '扣款';
                    param.payMonth = payDate.substring(0, 7);
                    param.increaseMoney = 0;
                    param.decreaseMoney = -sumMoney;
                    param.balanceMoney = -sumMoney;
                    if (customerName == null || customerName == "" || payDate == null || payDate == "" || sumMoney == null || sumMoney == ""){
                        layer.msg("请填写完整", { icon: 2 });
                        return false;
                    }
                    $.ajax({
                        url: "/erp/addendproductpay",
                        type: 'POST',
                        data: {
                            endProductPayJson: JSON.stringify(param)
                        },
                        success: function (res) {
                            layer.closeAll();
                            if (res.result == 0){
                                initTable(filterMonth, filterCustomerName,filterOrderName,filterType);
                                layer.msg("录入成功！", {icon: 1});
                            }
                        }, error: function () {
                            layer.msg("添加失败！", {icon: 2});
                        }
                    })
                }
                ,btn2: function(){
                    $("#payCustomer").val();
                    $("#payDate").val();
                    $("#sumMoney").val();
                    layer.closeAll();
                }
                , cancel: function (i, layero) {
                    $("#payCustomer").val();
                    $("#payDate").val();
                    $("#sumMoney").val();
                    layer.closeAll();
                }
            });
            setTimeout(function () {
                $.ajax({
                    url: "/erp/getallcustomer",
                    type: 'GET',
                    data: {
                    },
                    success: function (res) {
                        $("#payCustomer").empty();
                        if (res.data) {
                            $.each(res.data, function(index,element){
                                $("#payCustomer").append("<option value='"+element.customerName+"'>"+element.customerName+"</option>");
                            })
                        }
                        layui.form.render("select");
                    },
                    error: function () {
                        layer.msg("获取跟单失败", { icon: 2 });
                    }
                });
            }, 100);
            form.render('select');
            layui.laydate.render({
                elem: '#payDate',
                type: 'date',
                trigger: 'click',
                value: new Date(),
                isInitValue: true
            });
            return false;
        }
    });

    table.on('tool(reportTable)', function(obj){
        var data = obj.data;
        var filterOrderName = $("#orderName").val();
        var filterMonth = $("#monthStr").val();
        var filterCustomerName = $("#customerName").val();
        var filterType = $("#type").val();
        if (obj.event === 'updatePrice'){
            var index = layer.open({
                type: 1 //Page层类型
                , title: '修改单价'
                , btn: ['保存','取消']
                , shade: 0.6 //遮罩透明度
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , area: ['345px', '180px']
                , content: "<div>\n" +
                    "            <table>\n" +
                    "                <tr>\n" +
                    "                    <td style=\"text-align: right;margin-bottom: 15px;width: 100px\">\n" +
                    "                        <label class=\"layui-form-label\">原单价</label>\n" +
                    "                    </td>\n" +
                    "                    <td style=\"margin-bottom: 15px;\">\n" +
                    "                        <input type=\"text\" name=\"initPrice\" id=\"initPrice\" readonly class=\"layui-input\">\n" +
                    "                    </td>\n" +
                    "                </tr>\n" +
                    "                <tr>\n" +
                    "                    <td style=\"text-align: right;margin-bottom: 15px;width: 100px\">\n" +
                    "                        <label class=\"layui-form-label\">修改成</label>\n" +
                    "                    </td>\n" +
                    "                    <td style=\"margin-bottom: 15px;\">\n" +
                    "                        <input type=\"text\" name=\"toPrice\" id=\"toPrice\" class=\"layui-input\">\n" +
                    "                    </td>\n" +
                    "                </tr>\n" +
                    "            </table>\n" +
                    "    </div>"
                , yes: function (index, layero) {
                    var price= $("#toPrice").val();
                    var param = {};
                    param.orderName = data.orderName;
                    param.colorName = data.colorName;
                    param.price = price;
                    if (price == null || price == ""){
                        layer.msg("请填写完整", { icon: 2 });
                        return false;
                    }
                    $.ajax({
                        url: "/erp/updatepricebyordername",
                        type: 'POST',
                        data: param,
                        success: function (res) {
                            layer.closeAll();
                            if (res.result == 0){
                                initTable(filterMonth, filterCustomerName,filterOrderName,filterType);
                                layer.msg("录入成功！", {icon: 1});
                            }
                        }, error: function () {
                            layer.msg("添加失败！", {icon: 2});
                        }
                    })
                }
                ,btn2: function(){
                    $("#initPrice").val();
                    $("#toPrice").val();
                    layer.closeAll();
                }
                , cancel: function (i, layero) {
                    $("#initPrice").val();
                    $("#toPrice").val();
                    layer.closeAll();
                }
            });
            $("#initPrice").val(data.price);
            return false;
        } else if (obj.event === 'unNormal'){
            var index = layer.open({
                type: 1 //Page层类型
                , title: '扣款'
                , btn: ['保存','取消']
                , shade: 0.6 //遮罩透明度
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , area: ['345px', '280px']
                , content: "<div>\n" +
                    "            <table>\n" +
                    "                <tr>\n" +
                    "                    <td style=\"text-align: right;margin-bottom: 15px;width: 100px\">\n" +
                    "                        <label class=\"layui-form-label\">总金额</label>\n" +
                    "                    </td>\n" +
                    "                    <td style=\"margin-bottom: 15px;\">\n" +
                    "                        <input type=\"text\" name=\"sumMoney\" id=\"sumMoney\" class=\"layui-input\">\n" +
                    "                    </td>\n" +
                    "                </tr>\n" +
                    "                <tr>\n" +
                    "                    <td style=\"text-align: right;margin-bottom: 15px;width: 100px\">\n" +
                    "                        <label class=\"layui-form-label\">日期</label>\n" +
                    "                    </td>\n" +
                    "                    <td style=\"margin-bottom: 15px;\">\n" +
                    "                        <input type=\"text\" name=\"deDate\" id=\"deDate\" class=\"layui-input\">\n" +
                    "                    </td>\n" +
                    "                </tr>\n" +
                    "                <tr>\n" +
                    "                    <td style=\"text-align: right;margin-bottom: 15px;width: 100px\">\n" +
                    "                        <label class=\"layui-form-label\">备注</label>\n" +
                    "                    </td>\n" +
                    "                    <td style=\"margin-bottom: 15px;\">\n" +
                    "                        <input type=\"text\" name=\"deRemark\" id=\"deRemark\" class=\"layui-input\">\n" +
                    "                    </td>\n" +
                    "                </tr>\n" +
                    "            </table>\n" +
                    "    </div>"
                , yes: function (index, layero) {
                    var sumMoney= $("#sumMoney").val();
                    var payDate= $("#deDate").val();
                    var remark= $("#deRemark").val();
                    var param = {};
                    param.orderName = data.orderName;
                    param.clothesVersionNumber = data.clothesVersionNumber;
                    param.customerName = data.customerName;
                    param.payDate = payDate;
                    param.remark = remark;
                    param.sumMoney = sumMoney;
                    param.payType = 1;
                    if (sumMoney == null || sumMoney == "" || payDate == null || payDate == ""){
                        layer.msg("请填写完整", { icon: 2 });
                        return false;
                    }
                    $.ajax({
                        url: "/erp/addendproductpay",
                        type: 'POST',
                        data: {
                            endProductPayJson: JSON.stringify(param)
                        },
                        success: function (res) {
                            layer.closeAll();
                            if (res.result == 0){
                                initTable(filterMonth, filterCustomerName,filterOrderName,filterType);
                                layer.msg("录入成功！", {icon: 1});
                            }
                        }, error: function () {
                            layer.msg("添加失败！", {icon: 2});
                        }
                    })
                }
                ,btn2: function(){
                    $("#sumMoney").val();
                    $("#deDate").val();
                    $("#deRemark").val();
                    layer.closeAll();
                }
                , cancel: function (i, layero) {
                    $("#sumMoney").val();
                    $("#deDate").val();
                    $("#deRemark").val();
                    layer.closeAll();
                }
            });
            layui.laydate.render({
                elem: '#deDate',
                trigger: 'click'
            });
            return false;
        } else if (obj.event === 'delete'){
            layer.confirm('真的删除吗', function(index){
                $.ajax({
                    url: "/erp/deleteendproductpay",
                    type: 'POST',
                    data: {
                        id:data.id
                    },
                    success: function (res) {
                        if (res.result == 0) {
                            initTable(filterMonth, filterCustomerName,filterOrderName,filterType);
                            layer.close(index);
                            layer.msg("删除成功！", {icon: 1});
                        } else {
                            layer.msg("删除失败！", {icon: 2});
                        }
                    },
                    error: function () {
                        layer.msg("删除失败！", {icon: 2});
                    }
                })

            });
        }
    });

});

function addPay() {
    var orderName = $("#orderName").val();
    var clothesVersionNumber = $("#clothesVersionNumber").val();
    if (orderName == null || orderName === ""){
        layer.msg("款号不能为空", { icon: 2 });
        return false;
    }

    layui.use(['table', 'soulTable'], function () {
        var table = layui.table,
            soulTable = layui.soulTable,
            $ = layui.$;
        var index = layer.open({
            type: 1 //Page层类型
            , title: '添加成品入库'
            , btn: ['保存']
            , shade: 0.6 //遮罩透明度
            , maxmin: false //允许全屏最小化
            , area: ["600px","300px"]
            , anim: 0 //0-6的动画形式，-1不开启
            , content: "<div><table><tr style='margin-top: 10px'>" +
                "                            <td style=\"text-align: right;margin-bottom: 15px;\">\n" +
                "                                <label class=\"layui-form-label\">总出库</label>\n" +
                "                            </td>\n" +
                "                            <td style=\"margin-bottom: 15px;\">\n" +
                "                                <input type='text' id='outStoreCount' readonly class='layui-input'>\n" +
                "                            </td>" +
                "                            <td style=\"text-align: right;margin-bottom: 15px;\">\n" +
                "                                <label class=\"layui-form-label\">已收款</label>\n" +
                "                            </td>\n" +
                "                            <td style=\"margin-bottom: 15px;\">\n" +
                "                                <input type='text' id='finishPayCount' readonly class='layui-input'>\n" +
                "                            </td></tr><tr>" +
                "                            <td style=\"text-align: right;margin-bottom: 15px;\">\n" +
                "                                <label class=\"layui-form-label\">单价</label>\n" +
                "                            </td>\n" +
                "                            <td style=\"margin-bottom: 15px;\">\n" +
                "                                <input type='text' id='price' class='layui-input' value='无'>\n" +
                "                            </td>" +
                "                            <td style=\"text-align: right;margin-bottom: 15px;\">\n" +
                "                                <label class=\"layui-form-label\">数量</label>\n" +
                "                            </td>\n" +
                "                            <td style=\"margin-bottom: 15px;\">\n" +
                "                                <input type='text' id='payCount' class='layui-input'>\n" +
                "                            </td></tr><tr>" +
                "                            <td style=\"text-align: right;margin-bottom: 15px;\">\n" +
                "                                <label class=\"layui-form-label\">备注</label>\n" +
                "                            </td>\n" +
                "                            <td style=\"margin-bottom: 15px;\">\n" +
                "                                <input type='text' id='remark' class='layui-input' value='无'>\n" +
                "                            </td>" +
                "                            <td style=\"text-align: right;margin-bottom: 15px;\">\n" +
                "                                <label class=\"layui-form-label\">日期</label>\n" +
                "                            </td>\n" +
                "                            <td style=\"margin-bottom: 15px;\">\n" +
                "                                <input type='text' id='payDate' class='layui-input'>\n" +
                "                            </td>" +
                "</tr></table></div>"
            , yes: function (index, layero) {

                var outStoreCount = $("#outStoreCount").val();
                var finishPayCount = $("#finishPayCount").val();
                var price = $("#price").val();
                var payCount = $("#payCount").val();
                var remark = $("#remark").val();
                var payDate = $("#payDate").val();
                if (price == null || price == '' || payCount == null || payCount == '' || remark == null || remark == '' || payDate == null || payDate == ''){
                    layer.msg("请填写完整内容！");
                    return false;
                }
                if (payCount > (outStoreCount - finishPayCount)){
                    layer.msg("数量有误！");
                    return false;
                }
                var endProductPay = {};
                endProductPay.orderName = orderName;
                endProductPay.clothesVersionNumber = clothesVersionNumber;
                endProductPay.price = price;
                endProductPay.payDate = payDate;
                endProductPay.remark = remark;
                endProductPay.payCount = payCount;
                endProductPay.sumMoney = Number(payCount) * Number(price);
                $.ajax({
                    url: "/erp/addendproductpay",
                    type: 'POST',
                    data: {
                        endProductPayJson: JSON.stringify(endProductPay)
                    },
                    success: function (res) {
                        if (res.result === 0){
                            layer.msg("添加成功！", {icon: 1});
                            layer.close(index);
                            $.ajax({
                                url: "/erp/getendproductpaysummary",
                                type: 'GET',
                                data: {},
                                success: function (res) {
                                    if (res.endProductPayList) {
                                        var reportData = res.endProductPayList;
                                        table.render({
                                            elem: '#reportTable'
                                            ,cols:[[
                                                {title: '#', minWidth: 100, collapse: true,lazy: true, icon: ['layui-icon layui-icon-triangle-r', 'layui-icon layui-icon-triangle-d'], children:[
                                                        {
                                                            title: '付款记录'
                                                            ,url: 'getendproductpaybyordername'
                                                            ,where: function(row){
                                                                return {
                                                                    orderName: row.orderName
                                                                }
                                                            }
                                                            ,height: 300
                                                            ,loading:true
                                                            ,page: false
                                                            ,toolbar: '<div><a class="layui-btn layui-btn-danger layui-btn-sm" lay-event="batchDeletePay">批量删除</a></div>'
                                                            ,totalRow: true
                                                            ,cols: [[
                                                                {type: 'checkbox', fixed: 'left'},
                                                                {field: 'id', hide: true, fixed: 'left'},
                                                                {field: 'clothesVersionNumber', title: '生产款号',align:'center', minWidth: 120},
                                                                {field: 'orderName', title: '客户款号',align:'center', minWidth: 120},
                                                                {field: 'price', title: '单价',align:'center', minWidth: 120},
                                                                {field: 'payCount', title: '数量',align:'center', minWidth: 120, totalRow: true},
                                                                {field: 'sumMoney', title: '金额',align:'center', minWidth: 120, totalRow: true},
                                                                {field: 'createTime', title: '操作日期',align:'center', minWidth: 120,templet: function (d) {
                                                                        return moment(d.createTime).format("YYYY-MM-DD");
                                                                    }}
                                                            ]]
                                                            ,toolbarEvent: function (obj, pobj) {
                                                                // obj 子表当前行对象
                                                                // pobj 父表当前行对象
                                                                var objData = obj.data;
                                                                var childId = this.id; // 通过 this 对象获取当前子表的id
                                                                if (obj.event === 'batchDeletePay'){
                                                                    var checkStatus = table.checkStatus(obj.config.id);
                                                                    if(checkStatus.data.length == 0) {
                                                                        layer.msg('请选择数据');
                                                                    }else {
                                                                        var idList = [];
                                                                        $.each(checkStatus.data, function (index, item) {
                                                                            idList.push(item.id);
                                                                        });
                                                                        layer.confirm('真的删除吗', function(index){
                                                                            $.ajax({
                                                                                url: "/erp/deleteendproductpaybatch",
                                                                                type: 'POST',
                                                                                data: {
                                                                                    idList: idList
                                                                                },
                                                                                traditional: true,
                                                                                success: function (res) {
                                                                                    if (res.result == 0) {
                                                                                        layer.close(index);
                                                                                        layer.msg("删除成功！", {icon: 1});
                                                                                        table.reload(childId);
                                                                                    } else {
                                                                                        layer.msg("删除失败！", {icon: 2});
                                                                                    }
                                                                                }, error: function () {
                                                                                    layer.msg("删除失败！", {icon: 2});
                                                                                }
                                                                            })
                                                                        });
                                                                    }
                                                                }
                                                            }
                                                            ,done: function () {
                                                                soulTable.render(this);
                                                            }
                                                        }
                                                    ]}
                                                ,{type:'numbers', align:'center', title:'序号', minWidth:70}
                                                ,{field:'clothesVersionNumber', title:'单号', align:'center', minWidth:150, sort: true, filter: true, totalRowText: '合计'}
                                                ,{field:'orderName', title:'款号', align:'center', minWidth:150, sort: true, filter: true}
                                                ,{field:'inStoreCount', title:'入库数量', align:'center', minWidth:120, sort: true, filter: true, totalRow: true}
                                                ,{field:'storageCount', title:'库存数量', align:'center', minWidth:120, sort: true, filter: true, totalRow: true}
                                                ,{field:'outStoreCount', title:'出库数量', align:'center', minWidth:120, sort: true, filter: true, totalRow: true}
                                                ,{field:'payCount', title:'收款数量', align:'center', minWidth:120, sort: true, filter: true, totalRow: true}
                                                ,{field:'remainCount', title:'未收数量', align:'center', minWidth:120, sort: true, filter: true, totalRow: true}
                                                ,{field:'price', title:'单价', align:'center', minWidth:90, sort: true, filter: true, totalRow: true}
                                                ,{field:'sumMoney', title:'收款金额', align:'center', minWidth:120, sort: true, filter: true, totalRow: true}
                                                ,{field:'remainMoney', title:'未收金额', align:'center', minWidth:120, sort: true, filter: true, totalRow: true}
                                            ]]
                                            ,excel: {
                                                filename: '成品结算.xlsx'
                                            }
                                            ,data: reportData
                                            ,height: 'full-130'
                                            ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                                            ,title: '成品结算'
                                            ,totalRow: true
                                            ,loading: false
                                            ,page: true
                                            ,even: true
                                            ,limits: [500, 1000, 2000]
                                            ,limit: 1000 //每页默认显示的数量
                                            ,done: function () {
                                                soulTable.render(this);
                                            }
                                        });

                                    } else {
                                        layer.msg("获取失败！", {icon: 2});
                                    }
                                }, error: function () {
                                    layer.msg("获取失败！", {icon: 2});
                                }
                            });
                        } else {
                            layer.msg("添加失败！", {icon: 2});
                        }
                    },
                    error: function () {
                        layer.msg("添加失败！", {icon: 2});
                    }
                })
            }
            , cancel: function (i, layero) {
                $("#price").val('');
                $("#payCount").val('');
                $("#remark").val('无');
                $("#payDate").val('');
                $("#outStoreCount").val('');
                $("#finishPayCount").val('');
            }
        });
        setTimeout(function () {
            layui.laydate.render({
                elem: '#payDate',
                trigger: 'click'
            });
            $.ajax({
                url: "/erp/getmanufacturepricebyorder",
                type: 'GET',
                data: {
                    orderName: orderName
                },
                success: function (res) {
                    $("#price").val(res.price);
                },
                error: function () {
                    layer.msg("获取数据失败！", {icon: 2});
                }
            });
            $.ajax({
                url: "/erp/getoutstorecountbyordername",
                type: 'GET',
                data: {
                    orderName: orderName
                },
                success: function (res) {
                    $("#outStoreCount").val(res.outStoreCount);
                },
                error: function () {
                    layer.msg("获取数据失败！", {icon: 2});
                }
            });
            $.ajax({
                url: "/erp/getpaycountbyordername",
                type: 'GET',
                data: {
                    orderName: orderName
                },
                success: function (res) {
                    $("#finishPayCount").val(res.payCount);
                },
                error: function () {
                    layer.msg("获取数据失败！", {icon: 2});
                }
            });
        }, 100);
        return false;
    })
}

function toDecimal(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*100)/100;
    return f;
}

function toDecimalFour(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*10000)/10000;
    return f;
}


function doHandleDate() {
    var myDate = new Date();
    var tYear = myDate.getFullYear();
    var tMonth = myDate.getMonth();

    var m = tMonth + 1;
    if (m.toString().length == 1) {
        m = "0" + m;
    }
    return tYear +'-'+ m;
}