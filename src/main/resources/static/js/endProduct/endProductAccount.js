var form;
layui.use('form', function(){
    form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
    form.render();
});
var userRole = $("#userRole").val();
layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.20'
}).extend({
    soulTable: 'soulTable'
});
$(document).ready(function () {});
$(document).on('keydown', '.layui-table-edit', function (e) {
    var td = $(this).parent('td')
        ,tr = td.parent('tr')
        ,trs = tr.parent().parent().find('tr')
        ,tr_index = tr.index()
        ,td_index = td.index()
        ,td_last_index = tr.find('[data-edit="text"]:last').index()
        ,td_first_index = tr.find('[data-edit="text"]:first').index();
    switch (e.keyCode) {
        case 13:
            tr.next().find('td').eq(td_index).click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            return false;

        case 39:
            td.nextAll('[data-edit="text"]:first').click();
            if (td_index == td_last_index){
                tr.next().find('td').eq(td_first_index).click();
                if (tr_index == trs.length - 1){
                    trs.eq(0).find('td').eq(td_first_index).click();
                }
            }
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
        case 37:
            td.prevAll('[data-edit="text"]:first').click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
        case 38:
            tr.prev().find('td').eq(td_index).click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
        case 40:
            tr.next().find('td').eq(td_index).click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
    }

});
var reportTable,hidenTable;

layui.use(['form', 'table', 'soulTable'], function () {
    var table = layui.table,
        soulTable = layui.soulTable,
        form = layui.form,
        $ = layui.$;
    reportTable = table.render({
        elem: '#reportTable'
        ,cols:[[]]
        ,excel: {
            filename: '成品待审核.xlsx'
        }
        ,height: 'full-130'
        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
        ,title: '成品待审核'
        ,totalRow: true
        ,loading: false
        ,page: true
        ,even: true
        ,limits: [500, 1000, 2000]
        ,limit: 1000 //每页默认显示的数量
        ,done: function () {
            soulTable.render(this);
        }
    });

    initTable();

    function initTable(){
        $.ajax({
            url: "/erp/getpreaccountendproduct",
            type: 'GET',
            data: {},
            success: function (res) {
                if (res.data) {
                    var reportData = res.data;
                    table.render({
                        elem: '#reportTable'
                        ,cols:[[
                            {type:'checkbox'}
                            ,{type:'numbers', align:'center', title:'序号', width: '5%'}
                            ,{field:'customerName', title:'客户', align:'center', width: '10%', sort: true, filter: true, totalRowText: '合计'}
                            ,{field:'clothesVersionNumber', title:'单号', align:'center', width: '12%', sort: true, filter: true}
                            ,{field:'orderName', title:'款号', align:'center', width: '12%', sort: true, filter: true}
                            ,{field:'colorName', title:'颜色', align:'center', width: '9%', sort: true, filter: true}
                            ,{field:'sizeName', title:'尺码', align:'center', width: '7%', sort: true, filter: true}
                            ,{field:'price', title:'单价', align:'center', width: '7%', sort: true, filter: true}
                            ,{field:'endType', title:'类别', align:'center', width: '7%', sort: true, filter: true}
                            ,{field:'outStoreCount', title:'出库数量', align:'center', width: '7%', sort: true, filter: true, totalRow: true}
                            ,{field:'totalMoney', title:'金额', align:'center', width: '7%', sort: true, filter: true, totalRow: true}
                            ,{field:'createTime', title:'出库日期', align:'center', width: '9%', sort: true, filter: true, templet:function (d) {
                                    return moment(d.createTime).format("YYYY-MM-DD");
                                }}
                        ]]
                        ,excel: {
                            filename: '成品进销存.xlsx'
                        }
                        ,data: reportData
                        ,height: 'full-130'
                        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                        ,title: '成品进销存'
                        ,totalRow: true
                        ,loading: false
                        ,page: true
                        ,even: true
                        ,limits: [500, 1000, 2000]
                        ,limit: 1000 //每页默认显示的数量
                        ,done: function () {
                            soulTable.render(this);
                        }
                    });

                } else {
                    layer.msg("获取失败！", {icon: 2});
                }
            }, error: function () {
                layer.msg("获取失败！", {icon: 2});
            }
        });
    }

    table.on('toolbar(reportTable)', function(obj) {
        if (obj.event === 'clearFilter') {
            // 清除所有筛选条件并重载表格
            // 参数: tableId
            soulTable.clearFilter('reportTable')
        } else if (obj.event === 'exportExcel') {
            soulTable.export('reportTable');
        } else if (obj.event === 'refresh') {
            initTable();
        } else if (obj.event === 'account') {
            var index = layer.open({
                type: 1 //Page层类型
                , title: "成品入账确认"
                , btn: ['保存']
                , shade: 0.6 //遮罩透明度
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , area: ['300px', '180px']
                , content: "<div><table><tr><td><label class='layui-form-label'>日期</label></td><td><input type='text' id='accountDate' autocomplete='off' class='layui-input'></td></tr></table></div>"
                ,yes: function(index, layero){
                    var reportData = table.cache.reportTable;
                    var accountDate = $("#accountDate").val();
                    var idList = [];
                    var orderList = [];
                    $.each(reportData,function(i,item){
                        if (item.LAY_CHECKED){
                            idList.push(item.id);
                            if (orderList.indexOf(item.orderName) == -1){
                                orderList.push(item.orderName);
                            }
                        }
                    });
                    if (idList.length == 0){
                        layer.msg("请先选择数据");
                        return false;
                    }
                    if (orderList.length != 1){
                        layer.msg("一次只能入账一个款~~~");
                        return false;
                    }
                    if (accountDate == null || accountDate == ''){
                        layer.msg("日期必须");
                        return false;
                    }
                    var nowDate = dateFormat("YYYY-mm-dd HH:MM:SS", new Date()).replace(" ", "").replace(/\:/g,'').replace(/\-/g,'');
                    $.ajax({
                        url: "/erp/endproducttakeintoaccount",
                        type: 'POST',
                        data: {
                            idList: idList,
                            accountDate: accountDate,
                            payNumber: nowDate
                        },
                        traditional: true,
                        success: function (res) {
                            if (res.result == 0) {
                                layer.close(index);
                                layer.msg("保存成功！", {icon: 1});
                                initTable();
                            } else {
                                layer.msg("保存失败！", {icon: 2});
                            }
                        },
                        error: function () {
                            layer.msg("保存失败！", {icon: 2});
                        }
                    })
                }
                , cancel : function (i,layero) {
                    $("#accountDate").val("");
                }
            });
            layui.laydate.render({
                elem: '#accountDate',
                trigger: 'click'
            });
        }
    });

});

function toDecimal(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*100)/100;
    return f;
}

function toDecimalFour(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*10000)/10000;
    return f;
}

function doHandleDate() {
    var myDate = new Date();
    var tYear = myDate.getFullYear();
    var tMonth = myDate.getMonth();

    var m = tMonth + 1;
    if (m.toString().length == 1) {
        m = "0" + m;
    }
    return tYear +'-'+ m;
}

function dateFormat(fmt, date) {
    var ret;
    var opt = {
        "Y+": date.getFullYear().toString(),        // 年
        "m+": (date.getMonth() + 1).toString(),     // 月
        "d+": date.getDate().toString(),            // 日
        "H+": date.getHours().toString(),           // 时
        "M+": date.getMinutes().toString(),         // 分
        "S+": date.getSeconds().toString()          // 秒
        // 有其他格式化字符需求可以继续添加，必须转化成字符串
    };
    for (var k in opt) {
        ret = new RegExp("(" + k + ")").exec(fmt);
        if (ret) {
            fmt = fmt.replace(ret[1], (ret[1].length == 1) ? (opt[k]) : (opt[k].padStart(ret[1].length, "0")))
        }
    }
    return fmt;
}