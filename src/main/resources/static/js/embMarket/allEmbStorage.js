layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.21'
}).extend({                         // 模块别名
    soulTable: 'soulTable',
});
var form;
layui.use('form', function(){
    form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
    form.render();
})
var embStorageTable;
var userRole = $("#userRole").val();
$(document).ready(function () {

    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumber", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });

    $("#orderName").bind('blur change',function(){
        var orderName = $("#orderName").val();
        $("#colorName").empty();
        $.ajax({
            url: "/erp/getordercolornamesbyorder",
            data: {"orderName": orderName},
            success:function(data){
                $("#colorName").empty();
                if (data.colorNameList) {
                    console.log(data.colorNameList);
                    $("#colorName").empty();
                    $("#colorName").append("<option value=''>选择颜色</option>");
                    $.each(data.colorNameList, function(index,element){
                        $("#colorName").append("<option value='"+element+"'>"+element+"</option>");
                    })
                    form.render();
                }
            },
            error:function(){
            }
        });

        $("#sizeName").empty();
        $.ajax({
            url: "/erp/getordersizenamesbyorder",
            data: {"orderName": orderName},
            success:function(data){
                $("#sizeName").empty();
                if (data.sizeNameList) {
                    $("#sizeName").empty();
                    $("#sizeName").append("<option value=''>选择尺码</option>");
                    $.each(data.sizeNameList, function(index,element){
                        $("#sizeName").append("<option value='"+element+"'>"+element+"</option>");
                    })
                    form.render();
                }
            },
            error:function(){
            }
        });
    });

    $("#clothesVersionNumber").bind('blur change',function(){
        var orderName = $("#orderName").val();
        $("#colorName").empty();
        $.ajax({
            url: "/erp/getordercolornamesbyorder",
            data: {"orderName": orderName},
            success:function(data){
                $("#colorName").empty();
                if (data.colorNameList) {
                    $("#colorName").empty();
                    $("#colorName").append("<option value=''>选择颜色</option>");
                    $.each(data.colorNameList, function(index,element){
                        $("#colorName").append("<option value='"+element+"'>"+element+"</option>");
                    })
                    form.render();
                }
            },
            error:function(){
            }
        });

        $("#sizeName").empty();
        $.ajax({
            url: "/erp/getordersizenamesbyorder",
            data: {"orderName": orderName},
            success:function(data){
                $("#sizeName").empty();
                if (data.sizeNameList) {
                    $("#sizeName").empty();
                    $("#sizeName").append("<option value=''>选择尺码</option>");
                    $.each(data.sizeNameList, function(index,element){
                        $("#sizeName").append("<option value='"+element+"'>"+element+"</option>");
                    })
                    form.render();
                }
            },
            error:function(){
            }
        });
    });

});
layui.use(['form', 'soulTable', 'table', 'upload','yutons_sug'], function () {
    var table = layui.table,
        soulTable = layui.soulTable
    var load = layer.load();
    embStorageTable = table.render({
        elem: '#embStorageTable'
        ,cols: [[]]
        ,loading:true
        ,height: 'full-100'
        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
        ,defaultToolbar: ['filter', 'print']
        ,title: '衣胚库存表'
        ,totalRow: true
        ,page: true
        ,even: true
        ,limits: [200, 1000, 2000]
        ,limit: 200 //每页默认显示的数量
        ,overflow: 'tips'
        ,done: function () {
            soulTable.render(this);
            layer.close(load);
        }
        ,filter: {
            bottom: true,
            items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
        }
    });

    initTable();

    function initTable(){
        $.ajax({
            url: "/erp/getAllEmbStorage",
            type: 'POST',
            data: {},
            success: function (res) {
                if (res.data) {
                    var reportData = res.data;
                    table.reload("embStorageTable", {
                        cols:[[
                            {type:'numbers', align:'center', title:'序号', width:100}
                            ,{field:'orderName', title:'款号', align:'center', width:150, sort: true, filter: true, totalRowText: '合计'}
                            ,{field:'bedNumber', title:'床号', align:'center', width:150, sort: true, filter: true}
                            ,{field:'colorName', title:'颜色', align:'center', width:150, sort: true, filter: true}
                            ,{field:'sizeName', title:'尺码', align:'center',width:150, sort: true, filter: true}
                            ,{field:'packageNumber', title:'扎号', align:'center', width:150, sort: true, filter: true}
                            ,{field:'layerCount', title:'数量', align:'center', width:150, sort: true, filter: true, totalRow: true}
                            ,{field:'embInStoreTime', title:'入库时间', align:'center', width:180, sort: true, filter: true,templet: function (d) {
                                    return layui.util.toDateString(d.embInStoreTime, 'yyyy-MM-dd');
                                }}
                            ,{field:'embStoreLocation', title:'位置', align:'center', width:180, sort: true, filter: true}
                            ,{field: 'embStorageID', title:'操作', align:'center', toolbar: '#barTop', width:90}
                        ]]
                        ,excel: {
                            filename: '衣胚库存.xlsx',
                        }
                        ,data: reportData   // 将新数据重新载入表格
                        ,overflow: 'tips'
                        ,done: function () {
                            soulTable.render(this);
                            layer.close(load);
                        }
                        ,filter: {
                            bottom: true,
                            clearFilter: false,
                            items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                        }
                    });

                } else {
                    layer.msg("获取失败！", {icon: 2});
                }
            }, error: function () {
                layer.msg("获取失败！", {icon: 2});
            }
        });
    }




    table.on('toolbar(embStorageTable)', function(obj) {
        if (obj.event === 'clearFilter') {
            // 清除所有筛选条件并重载表格
            // 参数: tableId
            soulTable.clearFilter('embStorageTable')
        } else if (obj.event === 'refresh') {
            embStorageTable.reload();
        } else if (obj.event === 'exportExcel') {
            soulTable.export('embStorageTable');
        }
    });


    //监听提交
    form.on('submit(searchBeat)', function(data){
        var data = {};
        var orderName = $("#orderName").val();
        var colorName = $("#colorName").val();
        var sizeName = $("#sizeName").val();
        data.orderName = orderName;
        if (colorName != null && colorName != ""){
            data.colorName = colorName;
        }
        if (sizeName != null && sizeName != ""){
            data.sizeName = sizeName;
        }
        var load = layer.load(2);
        if (orderName == null || orderName == ""){
            $.ajax({
                url: "/erp/getAllEmbStorage",
                type: 'POST',
                data: {},
                success: function (res) {
                    if (res.data) {
                        var reportData = res.data;
                        table.reload("embStorageTable", {
                            cols:[[
                                {type:'numbers', align:'center', title:'序号', width:100}
                                ,{field:'orderName', title:'款号', align:'center', width:150, sort: true, filter: true, totalRowText: '合计'}
                                ,{field:'bedNumber', title:'床号', align:'center', width:150, sort: true, filter: true}
                                ,{field:'colorName', title:'颜色', align:'center', width:150, sort: true, filter: true}
                                ,{field:'sizeName', title:'尺码', align:'center',width:150, sort: true, filter: true}
                                ,{field:'packageNumber', title:'扎号', align:'center', width:150, sort: true, filter: true}
                                ,{field:'layerCount', title:'数量', align:'center', width:150, sort: true, filter: true, totalRow: true}
                                ,{field:'embInStoreTime', title:'入库时间', align:'center', width:180, sort: true, filter: true,templet: function (d) {
                                        return layui.util.toDateString(d.embInStoreTime, 'yyyy-MM-dd');
                                    }}
                                ,{field:'embStoreLocation', title:'位置', align:'center', width:180, sort: true, filter: true}
                                ,{field: 'embStorageID', title:'操作', align:'center', toolbar: '#barTop', width:90}
                            ]]
                            ,excel: {
                                filename: '衣胚库存.xlsx',
                            }
                            ,data: reportData   // 将新数据重新载入表格
                            ,overflow: 'tips'
                            ,done: function () {
                                soulTable.render(this);
                                layer.close(load);
                            }
                            ,filter: {
                                bottom: true,
                                clearFilter: false,
                                items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                            }
                        });

                    } else {
                        layer.msg("获取失败！", {icon: 2});
                    }
                }, error: function () {
                    layer.msg("获取失败！", {icon: 2});
                }
            });
        } else {
            $.ajax({
                url: "/erp/getEmbStorageByInfo",
                type: 'GET',
                data: data,
                success: function (res) {
                    if (res.data) {
                        var reportData = res.data;
                        table.reload("embStorageTable", {
                            cols:[[
                                {type:'numbers', align:'center', title:'序号', width:100}
                                ,{field:'orderName', title:'款号', align:'center', width:150, sort: true, filter: true, totalRowText: '合计'}
                                ,{field:'bedNumber', title:'床号', align:'center', width:150, sort: true, filter: true}
                                ,{field:'colorName', title:'颜色', align:'center', width:150, sort: true, filter: true}
                                ,{field:'sizeName', title:'尺码', align:'center',width:150, sort: true, filter: true}
                                ,{field:'packageNumber', title:'扎号', align:'center', width:150, sort: true, filter: true}
                                ,{field:'layerCount', title:'数量', align:'center', width:150, sort: true, filter: true, totalRow: true}
                                ,{field:'embInStoreTime', title:'入库时间', align:'center', width:180, sort: true, filter: true,templet: function (d) {
                                        return layui.util.toDateString(d.embInStoreTime, 'yyyy-MM-dd');
                                    }}
                                ,{field:'embStoreLocation', title:'位置', align:'center', width:180, sort: true, filter: true}
                                ,{field: 'embStorageID', title:'操作', align:'center', toolbar: '#barTop', width:90}
                            ]]
                            ,excel: {
                                filename: '裁片库存.xlsx',
                            }
                            ,data: reportData   // 将新数据重新载入表格
                            ,overflow: 'tips'
                            ,done: function () {
                                soulTable.render(this);
                                layer.close(load);
                            }
                            ,filter: {
                                bottom: true,
                                clearFilter: false,
                                items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                            }
                        });

                    } else {
                        layer.msg("获取失败！", {icon: 2});
                    }
                }, error: function () {
                    layer.msg("获取失败！", {icon: 2});
                }
            });
        }
        return false;
    });


    //监听行工具事件
    table.on('tool(embStorageTable)', function(obj){
        var data = obj.data;
        if (userRole != "role1" && userRole != "root"){
            layer.msg("您没有权限！", {icon: 2});
            return false;
        }
        if(obj.event === 'del'){
            layer.confirm('真的删除吗', function(index){
                $.ajax({
                    url: "/erp/deleteembstoragebyid",
                    type: 'POST',
                    data: {
                        embStorageID:data.embStorageID
                    },
                    success: function (res) {
                        if (res.result == 0) {
                            obj.del();
                            layer.close(index);
                            layer.msg("删除成功！", {icon: 1});
                        } else {
                            layer.msg("删除失败！", {icon: 2});
                        }
                    },
                    error: function () {
                        layer.msg("删除失败！", {icon: 2});
                    }
                })

            });
        }
    })


});