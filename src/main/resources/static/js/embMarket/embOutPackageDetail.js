var hot;
var basePath=$("#basePath").val();
layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.21'
}).extend();
$(document).ready(function () {
    layui.laydate.render({
        elem: '#from',
        trigger: 'click',
        type: 'datetime'
    });
    layui.laydate.render({
        elem: '#to',
        trigger: 'click',
        type: 'datetime'
    });

    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumber", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });
    $.ajax({
        url: "/erp/getallgroupname",
        data: {},
        success:function(data){
            $("#groupName").empty();
            if (data.groupList) {
                $("#groupName").append("<option value=''>选择组名</option>");
                $.each(data.groupList, function(index,element){
                    $("#groupName").append("<option value='"+element+"'>"+element+"</option>");
                })
            }
        }, error:function(){
        }
    });

});

orderName = $("#orderName").val();
var colNum = 0;

function search() {
    if($("#orderName").val().trim()=="") {
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入订单号！</span>",html: true});
        return false;
    }
    if($("#groupName").val().trim()=="") {
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入组名！</span>",html: true});
        return false;
    }
    $.blockUI({ css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        },
        message: "<h3>稍等一下... 奋力搜索中</h3>"
    });
    $.ajax({
        url: basePath + "erp/getemboutpackagedetail",
        type:'GET',
        data: {
            orderName:$("#orderName").val(),
            from:$("#from").val(),
            to:$("#to").val(),
            groupName:$("#groupName").val(),
        },
        success: function (data) {
            console.log(data);
            var container = document.getElementById('reportExcel');
            if (hot == undefined) {
                hot = new Handsontable(container, {
                    data: hotData,
                    rowHeaders: true,
                    colHeaders: true,
                    autoColumnSize: true,
                    dropdownMenu: true,
                    contextMenu: true,
                    filters:true,
                    stretchH: 'all',
                    autoWrapRow: true,
                    height: $(document.body).height() - 270,
                    manualRowResize: true,
                    manualColumnResize: true,
                    // minRows: 35,
                    minCols: 16,
                    colWidths:[60,100,60,60,60,60,100,60,60,60,60,100,60,60,60,60],
                    // colWidths: 80,
                    language: 'zh-CN',
                    licenseKey: 'non-commercial-and-evaluation'

                });
            }
            if(data) {
                $("#exportDiv").show();
                var detail = data.detail;
                var oneLength = data.oneLength;
                var summary = data.summary;
                var index = 1;
                var hotData = [];
                var title = [];
                title.push("序号");
                title.push("颜色");
                title.push("尺码");
                title.push("扎号");
                title.push("数量");
                var colCount = 0;
                if (oneLength < 1){
                    if (detail){
                        hotData.push(title);
                        for (var i=0;i<detail.length;i++){
                            var rowData = [];
                            rowData[0] = index;
                            rowData[1] = detail[i].colorName;
                            rowData[2] = detail[i].sizeName;
                            rowData[3] = detail[i].packageNumber;
                            rowData[4] = detail[i].layerCount;
                            hotData.push(rowData);
                            index ++;
                        }
                    }

                }else {
                    title.push("序号");
                    title.push("颜色");
                    title.push("尺码");
                    title.push("扎号");
                    title.push("数量");
                    title.push("序号");
                    title.push("颜色");
                    title.push("尺码");
                    title.push("扎号");
                    title.push("数量");
                    hotData.push(title);
                    for (var i = 0; i<oneLength; i++){
                        var rowData = [];
                        rowData[0] = index;
                        rowData[1] = detail[i].colorName;
                        rowData[2] = detail[i].sizeName;
                        rowData[3] = detail[i].packageNumber;
                        rowData[4] = detail[i].layerCount;
                        hotData.push(rowData);
                        index ++;
                    }
                    hotData.push([]);
                    hotData.push([]);
                    for (var i = oneLength; i<2*oneLength; i++){
                        hotData[i-oneLength+1][5] = index;
                        hotData[i-oneLength+1][6] = detail[i].colorName;
                        hotData[i-oneLength+1][7] = detail[i].sizeName;
                        hotData[i-oneLength+1][8] = detail[i].packageNumber;
                        hotData[i-oneLength+1][9] = detail[i].layerCount;
                        index ++;
                    }
                    for (var i = 2*oneLength; i<detail.length; i++){
                        hotData[i-2*oneLength+1][10] = index;
                        hotData[i-2*oneLength+1][11] = detail[i].colorName;
                        hotData[i-2*oneLength+1][12] = detail[i].sizeName;
                        hotData[i-2*oneLength+1][13] = detail[i].packageNumber;
                        hotData[i-2*oneLength+1][14] = detail[i].layerCount;
                        index ++;
                    }
                }
                hotData.push(["汇总"]);
                var summaryIndex = 0;
                var sizeRow = [];
                sizeRow[0] = [];
                sizeRow[1] = [];
                var sizeSum = [];
                sizeSum[0] = [];
                sizeSum[1] = ["小计"];
                var totalSum = 0;
                if (summary){
                    for (var colorKey in summary){
                        var sizeData = summary[colorKey];
                        var colorRow = [];
                        colorRow[0] = [];
                        colorRow[1] = [colorKey];
                        var colorSum = 0;
                        var sizeIndex = 1;


                        var sizeDataList = [];
                        for (var i in sizeData){
                            var sizeDataItem = {};
                            sizeDataItem.sizeName = i;
                            sizeDataItem.sizeDataDetail = sizeData[i];
                            sizeDataList.push(sizeDataItem);
                        }
                        sizeDataList = globalSizeDataSort(sizeDataList);
                        for (var i in sizeDataList){
                            if (summaryIndex == 0){
                                sizeRow.push(sizeDataList[i].sizeName);
                                sizeSum[sizeIndex+1] = 0;
                            }
                            colorRow.push(sizeDataList[i].sizeDataDetail);
                            sizeSum[sizeIndex+1] += sizeDataList[i].sizeDataDetail;
                            colorSum += sizeDataList[i].sizeDataDetail;
                            sizeIndex ++;
                        }

                        if (summaryIndex == 0){
                            sizeRow.push("小计");
                            hotData.push(sizeRow);
                        }
                        colorRow.push(colorSum);
                        totalSum += colorSum;
                        hotData.push(colorRow);
                        summaryIndex ++;
                    }
                    hotData.push(sizeSum);
                }
                hotData.push([[],["总计"],[totalSum]]);
                hot.loadData(hotData);

            }else {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，没有查到相关信息！</span>",html: true});
            }
            $.unblockUI();
        },
        error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }
    })
}


function exportData() {
    var data = hot.getData();
    var result = [];
    var col = 0;
    $.each(data,function (index, item) {
        // if(item[0]!=null) {
        for(var i=0;i<item.length;i++) {
            if(item[i]==null) {
                item[i] = '';
            }else if(index==1) {
                col++;
            }
        }
        result.push(item);
        // }else {
        //     return false;
        // }
    });
    var orderName = $("#orderName").val();
    var clothesVersionNumber = $("#clothesVersionNumber").val();
    var groupName = $("#groupSelect").val();
    var fromDate = $("#from").val();
    var toDate = $("#to").val();
    var myDate = new Date();
    export2Excel([orderName+"-"+clothesVersionNumber+'-下货到'+groupName+'扎号明细-'+myDate.toLocaleString()],col, result, orderName+"-"+clothesVersionNumber+'-下货到'+groupName+'扎号明细-'+myDate.toLocaleString()+".xls")
}