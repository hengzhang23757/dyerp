var hot;
var basePath=$("#basePath").val();
layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.21'
}).extend();
$(document).ready(function () {

    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumber", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });
});

function search() {
    var container = document.getElementById('reportExcel');
    if(hot==undefined) {
        hot = new Handsontable(container, {
            // data: data,
            rowHeaders: true,
            colHeaders: true,
            autoColumnSize: true,
            dropdownMenu: true,
            contextMenu: true,
            stretchH: 'all',
            autoWrapRow: true,
            height: $(document.body).height() - 230,
            manualRowResize: true,
            manualColumnResize: true,
            // minRows: 20,
            minCols: 24,
            colWidths:[100,60,60,60,60,60,60,60,60,60,60,60,60,60,60],
            language: 'zh-CN',
            licenseKey: 'non-commercial-and-evaluation'
        });
    }


    if($("#orderName").val().trim()=="") {
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入要查询的订单号！</span>",html: true});
        return false;
    }
    $.blockUI({ css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        },
        message: "<h3>稍等一下... 奋力搜索中</h3>"
    });
    $.ajax({
        url: basePath+"erp/queryembleak",
        type:'GET',
        data: {
            orderName:$("#orderName").val(),
        },
        success: function (data) {
            $("#exportDiv").show();
            var hotData = [];
            if(data) {
                var totalData = data["total"];
                var sizeRow = [];
                sizeRow[0] = [];
                sizeRow[1] = [];
                var totalIndex = 0;
                var totalSum1 = 0;
                var totalSum2 = 0;
                var totalSum3 = 0;
                for (var totalColorKey in totalData){
                    var colorRow1 = [];
                    var colorRow2 = [];
                    var colorRow3 = [];
                    colorRow1[0] = [totalColorKey];
                    colorRow1[1] = ["裁床数"];
                    colorRow2[1] = ["出衣胚数"];
                    colorRow3[1] = ["对比"];
                    var planSum = 0;
                    var actualSum = 0;
                    var constructSum = 0;
                    var colorData = totalData[totalColorKey];

                    var sizeDataList = [];
                    for (var i in colorData){
                        var sizeDataItem = {};
                        sizeDataItem.sizeName = i;
                        sizeDataItem.sizeDataDetail = colorData[i];
                        sizeDataList.push(sizeDataItem);
                    }
                    sizeDataList = globalSizeDataSort(sizeDataList);

                    for (var i in sizeDataList){
                        if (totalIndex == 0){
                            sizeRow.push(sizeDataList[i].sizeName);
                        }
                        colorRow1.push(sizeDataList[i].sizeDataDetail.planCount);
                        colorRow2.push(sizeDataList[i].sizeDataDetail.actualCount);
                        colorRow3.push(sizeDataList[i].sizeDataDetail.contrastCount);
                        planSum += sizeDataList[i].sizeDataDetail.planCount;
                        actualSum += sizeDataList[i].sizeDataDetail.actualCount;
                        constructSum += sizeDataList[i].sizeDataDetail.contrastCount;
                    }
                    if (totalIndex == 0){
                        sizeRow.push(["合计"]);
                    }
                    hotData.push(sizeRow);
                    colorRow1.push(planSum);
                    colorRow2.push(actualSum);
                    colorRow3.push(constructSum);
                    totalSum1 += planSum;
                    totalSum2 += actualSum;
                    totalSum3 += constructSum;
                    hotData.push(colorRow1);
                    hotData.push(colorRow2);
                    hotData.push(colorRow3);
                    totalIndex ++;
                }
                hotData.push([],[["总裁数"],totalSum1,["总出数"],totalSum2,["总对比"],totalSum3]);

                var dailyData = data["daily"];
                for (var dateKey in dailyData){
                    var dailyColorData = dailyData[dateKey];
                    var dateRow = [];
                    dateRow[0] = [transDate(dateKey)];
                    hotData.push(dateRow);
                    var dailySizeRow = [];
                    dailySizeRow[0] = [];
                    var colorIndex = 0;
                    var dailySum = 0;
                    var dailySummary = [];
                    for (var dailyColorKey in dailyColorData){
                        var dailySizeData = dailyColorData[dailyColorKey];
                        var dailyColorRow = [];
                        dailyColorRow[0] = [dailyColorKey];
                        var dailyColorSum = 0;


                        var sizeDataList = [];
                        for (var i in dailySizeData){
                            var sizeDataItem = {};
                            sizeDataItem.sizeName = i;
                            sizeDataItem.sizeDataDetail = dailySizeData[i];
                            sizeDataList.push(sizeDataItem);
                        }
                        sizeDataList = globalSizeDataSort(sizeDataList);
                        for (var i in sizeDataList){
                            if (colorIndex == 0){
                                dailySizeRow.push(sizeDataList[i].sizeName);
                                dailySummary.push([]);
                            }
                            dailyColorRow.push(sizeDataList[i].sizeDataDetail);
                            dailyColorSum += sizeDataList[i].sizeDataDetail;
                        }
                        if (colorIndex == 0){
                            dailySizeRow.push("合计");
                            hotData.push(dailySizeRow);
                        }
                        dailyColorRow.push(dailyColorSum);
                        hotData.push(dailyColorRow);
                        dailySum += dailyColorSum;
                        colorIndex ++;
                    }
                    dailySummary.push("小计");
                    dailySummary.push(dailySum);
                    hotData.push(dailySummary);
                    hotData.push([]);
                }

            }
            hot.loadData(hotData);
            $.unblockUI();
        }, error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }
    })
}

function exportData() {
    var orderName = $("#orderName").val();
    var versionNumber = $("#clothesVersionNumber").val();
    var myDate = new Date();
    var title = orderName+'-'+versionNumber+'-衣胚出库汇总-'+myDate.toLocaleString();
    var data = utl.Object.copyJson(hot.getData());//<=====读取handsontable的数据
    var result = [];
    var firstRow = new Array(data[0].length);
    firstRow[0] = title;
    for(var i=1;i<data[0].length;i++) {
        firstRow[i] = "";
    }
    result.push(firstRow);
    var col = 0;
    $.each(data,function (index, item) {
        for(var i=0;i<item.length;i++) {
            if(item[i]==null) {
                item[i] = '';
            }else if(index==1) {
                col++;
            }
        }
        result.push(item);
    });
    utl.XLSX.onExport(result,"Sheet1","xlsx",title+".xlsx");//<====导出
}

function transDate(data) {return  moment(data).format("YYYY-MM-DD");};