var hot;
var basePath=$("#basePath").val();
layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.21'
}).extend();
$(document).ready(function () {

    layui.laydate.render({
        elem: '#from',
        trigger: 'click',
        type: 'datetime'
    });
    layui.laydate.render({
        elem: '#to',
        trigger: 'click',
        type: 'datetime'
    });

    $.ajax({
        url: "/erp/getallgroupname",
        data: {},
        success:function(data){
            $("#groupName").empty();
            if (data.groupList) {
                $("#groupName").append("<option value=''>选择组名</option>");
                $.each(data.groupList, function(index,element){
                    $("#groupName").append("<option value='"+element+"'>"+element+"</option>");
                })
            }
        }, error:function(){
        }
    });
    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumber", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"50",
            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"50",
            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });
});




var colNum = 0;

function search() {
    var orderName = $("#orderName").val();
    var clothesVersionNumber = $("#clothesVersionNumber").val();
    if(orderName == null || orderName == "") {
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入要查询的订单号！</span>",html: true});
        return false;
    }
    var param = {};
    param.orderName = orderName;
    var groupName = $("#groupName").val();
    var from = $("#from").val();
    var to = $("#to").val();
    if (groupName != "" && groupName != null){
        param.groupName = groupName;
    }
    if (from != "" && from != null){
        param.from = from;
    }
    if (to != "" && to != null){
        param.to = to;
    }
    $.ajax({
        url: basePath + "erp/getemboutpackagedetail",
        type:'GET',
        data: param,
        success: function (data) {
            $("#exportDiv").show();
            var tableHtml = "";
            $("#entities").empty();
            if(data) {
                tableHtml = "<div class=\"col-md-12\" name='printTable'>\n" +
                    "               <section class=\"panel panel-default\" style='page-break-after:always;'>\n" +
                    "                   <header class=\"panel-heading font-bold\" style=\"text-align: center\">\n" +
                    "                       <div style=\"font-size: 20px;font-weight: 700\">德悦服饰衣胚外发单</div><br>\n" +
                    "                       <div style=\"font-size: 14px;float: center;font-weight: 700\">版单："+clothesVersionNumber+"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;订单："+orderName+"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;组名:"+ groupName +" &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;开始时间:"+ from +" &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;结束时间:"+ to +"</div>\n"+
                    "                   </header>\n" +
                    "                   <table class=\"table table-bordered\" style='width: 100%'>\n" +
                    "                       <tbody>";
                tableHtml += "<tr style='font-weight: 700'><td style='background-color: #0f9d58'>床号</td><td>扎号</td><td>颜色</td><td>尺码</td><td>数量</td><td style='background-color: #0f9d58'>床号</td><td>扎号</td><td>颜色</td><td>尺码</td><td>数量</td><td style='background-color: #0f9d58'>床号</td><td>扎号</td><td>颜色</td><td>尺码</td><td>数量</td></tr>";
                var title = [["扎号"],["床号"],["颜色"],["尺码"],["数量"],["缸号"],["扎号"],["床号"],["颜色"],["尺码"],["数量"],["缸号"],["扎号"],["床号"],["颜色"],["尺码"],["数量"],["缸号"]];
                var detail = data.detail;
                var summary = data.summary;
                var sizeList = data.sizeList;
                var colorList = data.colorList;
                var oneLength = data.oneLength;
                var twoLength = data.twoLength;
                var threeLength = data.threeLength;
                var packageCountData = data.packageCount;
                var hotData = [];
                var index_num = 1;
                sizeList = globalSizeSort(sizeList);
                if (detail){
                    hotData.push(title);
                    if (oneLength > 0){
                        if (oneLength == twoLength && oneLength == threeLength){
                            for (var i = 0; i < oneLength; i++ ){
                                tableHtml += "<tr><td style='background-color: #0f9d58'>" + detail[i].bedNumber+ "</td><td>" + detail[i].packageNumber + "</td><td>" + detail[i].colorName + "</td><td>" + detail[i].sizeName + "</td><td>" + detail[i].layerCount + "</td><td style='background-color: #0f9d58'>" + detail[i+oneLength].bedNumber + "</td><td>" + detail[i+oneLength].packageNumber + "</td><td>" + detail[i+oneLength].colorName + "</td><td>" + detail[i+oneLength].sizeName + "</td><td>" + detail[i+oneLength].layerCount + "</td><td style='background-color: #0f9d58'>" + detail[i+oneLength+twoLength].bedNumber + "</td><td>" + detail[i+oneLength+twoLength].packageNumber + "</td><td>" + detail[i+oneLength+twoLength].colorName + "</td><td>" + detail[i+oneLength+twoLength].sizeName + "</td><td>" + detail[i+oneLength+twoLength].layerCount + "</td></tr>";
                                var rowData = [];
                                rowData[0] = detail[i].bedNumber;
                                rowData[1] = detail[i].packageNumber;
                                rowData[2] = detail[i].colorName;
                                rowData[3] = detail[i].sizeName;
                                rowData[4] = detail[i].layerCount;
                                rowData[5] = detail[i + oneLength].bedNumber;
                                rowData[6] = detail[i + oneLength].packageNumber;
                                rowData[7] = detail[i + oneLength].colorName;
                                rowData[8] = detail[i + oneLength].sizeName;
                                rowData[9] = detail[i + oneLength].layerCount;
                                rowData[10] = detail[i + oneLength + twoLength].bedNumber;
                                rowData[11] = detail[i + oneLength + twoLength].packageNumber;
                                rowData[12] = detail[i + oneLength + twoLength].colorName;
                                rowData[13] = detail[i + oneLength + twoLength].sizeName;
                                rowData[14] = detail[i + oneLength + twoLength].layerCount;
                                hotData[index_num] = rowData;
                                index_num ++;
                            }
                        } else if (oneLength == twoLength && oneLength > threeLength){
                            for (var i = 0; i < oneLength - 1; i++ ){
                                tableHtml += "<tr><td style='background-color: #0f9d58'>" + detail[i].bedNumber + "</td><td>"+ detail[i].packageNumber + "</td><td>" + detail[i].colorName + "</td><td>" + detail[i].sizeName + "</td><td>" + detail[i].layerCount + "</td><td style='background-color: #0f9d58'>" + detail[i+oneLength].bedNumber + "</td><td>" + detail[i+oneLength].packageNumber + "</td><td>" + detail[i+oneLength].colorName + "</td><td>" + detail[i+oneLength].sizeName + "</td><td>" + detail[i+oneLength].layerCount + "</td><td style='background-color: #0f9d58'>" + detail[i+oneLength+twoLength].bedNumber + "</td><td>" + detail[i+oneLength+twoLength].packageNumber + "</td><td>" + detail[i+oneLength+twoLength].colorName + "</td><td>" + detail[i+oneLength+twoLength].sizeName + "</td><td>" + detail[i+oneLength+twoLength].layerCount + "</td></tr>";
                                var rowData = [];
                                rowData[0] = detail[i].bedNumber;
                                rowData[1] = detail[i].packageNumber;
                                rowData[2] = detail[i].colorName;
                                rowData[3] = detail[i].sizeName;
                                rowData[4] = detail[i].layerCount;
                                rowData[5] = detail[i + oneLength].bedNumber;
                                rowData[6] = detail[i + oneLength].packageNumber;
                                rowData[7] = detail[i + oneLength].colorName;
                                rowData[8] = detail[i + oneLength].sizeName;
                                rowData[9] = detail[i + oneLength].layerCount;
                                rowData[10] = detail[i + oneLength + twoLength].bedNumber;
                                rowData[11] = detail[i + oneLength + twoLength].packageNumber;
                                rowData[12] = detail[i + oneLength + twoLength].colorName;
                                rowData[13] = detail[i + oneLength + twoLength].sizeName;
                                rowData[14] = detail[i + oneLength + twoLength].layerCount;
                                hotData[index_num] = rowData;
                                index_num ++;
                            }
                            tableHtml += "<tr><td style='background-color: #0f9d58'>" + detail[oneLength - 1].bedNumber+ "</td><td>" +detail[oneLength - 1].packageNumber + "</td><td>" + detail[oneLength - 1].colorName + "</td><td>" + detail[oneLength - 1].sizeName + "</td><td>" + detail[oneLength - 1].layerCount + "</td><td style='background-color: #0f9d58'>" + detail[oneLength + twoLength - 1].bedNumber + "</td><td>"+ detail[oneLength + twoLength - 1].packageNumber + "</td><td>" + detail[oneLength + twoLength - 1].colorName + "</td><td>" + detail[oneLength + twoLength - 1].sizeName + "</td><td>" + detail[oneLength + twoLength - 1].layerCount + "</td><td></td><td></td><td></td><td></td><td></td></tr>";
                            var rowData = [];
                            rowData[0] = detail[oneLength - 1].bedNumber;
                            rowData[1] = detail[oneLength - 1].packageNumber;
                            rowData[2] = detail[oneLength - 1].colorName;
                            rowData[3] = detail[oneLength - 1].sizeName;
                            rowData[4] = detail[oneLength - 1].layerCount;
                            rowData[5] = detail[oneLength + twoLength - 1].bedNumber;
                            rowData[6] = detail[oneLength + twoLength - 1].packageNumber;
                            rowData[7] = detail[oneLength + twoLength - 1].colorName;
                            rowData[8] = detail[oneLength + twoLength - 1].sizeName;
                            rowData[9] = detail[oneLength + twoLength - 1].layerCount;
                            hotData[index_num] = rowData;
                            index_num ++;
                        } else if (oneLength > twoLength && oneLength > threeLength){
                            for (var i = 0; i < oneLength - 1; i++ ){
                                tableHtml += "<tr><td style='background-color: #0f9d58'>" + detail[i].bedNumber + "</td><td>" + detail[i].packageNumber + "</td><td>" + detail[i].colorName + "</td><td>" + detail[i].sizeName + "</td><td>" + detail[i].layerCount + "</td><td style='background-color: #0f9d58'>" + detail[i+oneLength].bedNumber + "</td><td>" + detail[i+oneLength].packageNumber + "</td><td>" + detail[i+oneLength].colorName + "</td><td>" + detail[i+oneLength].sizeName + "</td><td>" + detail[i+oneLength].layerCount + "</td><td style='background-color: #0f9d58'>" + detail[i+oneLength+twoLength].bedNumber + "</td><td>" + detail[i+oneLength+twoLength].packageNumber + "</td><td>" + detail[i+oneLength+twoLength].colorName + "</td><td>" + detail[i+oneLength+twoLength].sizeName + "</td><td>" + detail[i+oneLength+twoLength].layerCount + "</td></tr>";
                                var rowData = [];
                                rowData[0] = detail[i].bedNumber;
                                rowData[1] = detail[i].packageNumber;
                                rowData[2] = detail[i].colorName;
                                rowData[3] = detail[i].sizeName;
                                rowData[4] = detail[i].layerCount;
                                rowData[5] = detail[i + oneLength].bedNumber;
                                rowData[6] = detail[i + oneLength].packageNumber;
                                rowData[7] = detail[i + oneLength].colorName;
                                rowData[8] = detail[i + oneLength].sizeName;
                                rowData[9] = detail[i + oneLength].layerCount;
                                rowData[10] = detail[i + oneLength + twoLength].bedNumber;
                                rowData[11] = detail[i + oneLength + twoLength].packageNumber;
                                rowData[12] = detail[i + oneLength + twoLength].colorName;
                                rowData[13] = detail[i + oneLength + twoLength].sizeName;
                                rowData[14] = detail[i + oneLength + twoLength].layerCount;
                                hotData[index_num] = rowData;
                                index_num ++;
                            }
                            tableHtml += "<tr><td style='background-color: #0f9d58'>" + detail[oneLength - 1].bedNumber + "</td><td>" + detail[oneLength - 1].packageNumber + "</td><td>" + detail[oneLength - 1].colorName + "</td><td>" + detail[oneLength - 1].sizeName + "</td><td>" + detail[oneLength - 1].layerCount + "</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
                            var rowData = [];
                            rowData[0] = detail[oneLength - 1].bedNumber;
                            rowData[1] = detail[oneLength - 1].packageNumber;
                            rowData[2] = detail[oneLength - 1].colorName;
                            rowData[3] = detail[oneLength - 1].sizeName;
                            rowData[4] = detail[oneLength - 1].layerCount;
                            hotData[index_num] = rowData;
                            index_num ++;
                        }
                    }
                }
                tableHtml +=  "                       </tbody>" +
                    "                   </table>";

                if (summary) {
                    hotData.push([]);
                    hotData.push(["汇总"]);
                    tableHtml += "                   <table class=\"table table-bordered\" style='width: 100%'>\n" +
                        "                       <tbody>";
                    var sizeRow = [];
                    sizeRow[0] = [];
                    var sizeSumRow = [];
                    var sizePackageSumRow = [];
                    sizeSumRow[0] = ["小计"];
                    sizePackageSumRow[0] = ["小计"];
                    var totalSum = 0;
                    var totalPackageSum = 0;
                    if (sizeList){
                        var widthSummary = sizeList.length + 2;
                        tableHtml += "<tr><td colspan='"+ widthSummary +"' style='text-align:center; font-size: 15px; font-weight: 700'></td></tr>";
                        tableHtml += "<tr><td colspan='"+ widthSummary +"' style='text-align:center; font-size: 15px; font-weight: 700'>汇总</td></tr>";
                        tableHtml += "<tr style='font-weight: 700'><td></td>";
                        var flag = true;
                        for (var j = 0; j < sizeList.length; j++){
                            tableHtml += "<td>" + sizeList[j]+"</td>";
                            sizeSumRow[j+1] = 0;
                            sizePackageSumRow[j+1] = 0;
                        }
                        tableHtml += "<td>合计</td></tr>";
                    }

                    for (var colorKey in summary){
                        var sizeData = summary[colorKey];
                        var tmpRow = [];
                        var colorSum = 0;
                        var colorPackageSum = 0;
                        tmpRow.push(colorKey);
                        tableHtml += "<tr><td>" + colorKey + "</td>";
                        for (var m = 0; m < sizeList.length; m ++){
                            var packageCount = 0;
                            $.each(packageCountData, function (pc_index, pc_item) {
                                if (pc_item.colorName === colorKey && pc_item.sizeName === sizeList[m]){
                                    packageCount = pc_item.packageCount;
                                }
                            });
                            colorPackageSum += packageCount;
                            if (sizeData[sizeList[m]]){
                                tmpRow.push(sizeData[sizeList[m]]);
                                colorSum += sizeData[sizeList[m]];
                                tableHtml += "<td>" + sizeData[sizeList[m]] + "[" + packageCount + "]</td>"
                                sizeSumRow[m+1] += sizeData[sizeList[m]];
                                sizePackageSumRow[m+1] += packageCount;
                            }else {
                                tmpRow.push([0]);
                                tableHtml += "<td>0[0]</td>";
                            }
                        }
                        tableHtml += "<td>" + colorSum + "[" + colorPackageSum + "]</td></tr>";
                        totalSum += colorSum;
                        totalPackageSum += colorPackageSum;

                        hotData[index_num] = tmpRow;
                        index_num ++;
                    }
                    tableHtml += "<tr><td>小计</td>";
                    for (var n = 1; n < sizeSumRow.length; n++){
                        tableHtml += "<td>" + sizeSumRow[n] + "[" + sizePackageSumRow[n] + "]</td>";
                    }
                    tableHtml += "<td>" + totalSum + "[" + totalPackageSum + "]</td></tr>";

                    hotData[index_num] = sizeSumRow;
                    index_num ++;
                }

                tableHtml +=  "                       </tbody>" +
                    "                   </table>";
                tableHtml +=  "                       </tbody>" +
                    "                   </table>" +
                    "               </section>" +
                    "              </div>";
                $("#entities").append(tableHtml);
            }else {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，没有查到相关信息！</span>",html: true});
            }
        },
        error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }
    })

}


function exportData() {
    var data = hot.getData();
    var result = [];
    $.each(data,function (index, item) {
        // if(item[0]!=null) {
            for(var i=0;i<item.length;i++) {
                if(item[i]==null) {
                    item[i] = '';
                }
            }
            result.push(item);
        // }else {
        //     return false;
        // }
    });
    var orderName = $("#orderName").val();
    var clothesVersionNumber = $("#clothesVersionNumber").val();
    var bedNumber = $("#bedNumber").val();
    var myDate = new Date();
    export2Excel([orderName+"-"+clothesVersionNumber+"-第"+bedNumber+'床-裁床报表-'+myDate.toLocaleString()],colNum, result, orderName+"-"+clothesVersionNumber+"-第"+bedNumber+'床-裁床报表-'+myDate.toLocaleString()+".xls")
}

function printDeal(){
    $("#printf").empty();
    var printBoxs = document.getElementsByName('printTable');
    var newContent = '';
    for(var i=0;i<printBoxs.length;i++) {
        newContent += printBoxs[i].innerHTML;
    }
    var f = document.getElementById('printf');
    // f.contentDocument.write('<link rel="stylesheet" type="text/css" href="/css/app.v2.css">');
    f.contentDocument.write('<style>' +
        'table {' +
        'border-collapse:collapse;' +
        '}' +
        'td {\n' +
        '        border: 1px solid #000000;\n' +
        '    }'+
        '</style>');
    f.contentDocument.write(newContent);
    f.contentDocument.close();
    window.frames['printf'].focus();
    try{
        window.frames['printf'].print();
    }catch(err){
        f.contentWindow.print();
    }
}