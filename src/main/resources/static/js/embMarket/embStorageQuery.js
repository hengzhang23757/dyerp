var hot;
var basePath=$("#basePath").val();
var userName=$("#userName").val();
layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.21'
}).extend();
$(document).ready(function () {

    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumber", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });

    $("#orderName").bind('blur change',function(){
        var orderName = $("#orderName").val();
        $.ajax({
            url: "/erp/getembcolorhint",
            data: {"orderName": orderName},
            success:function(data){
                $("#colorName").empty();
                if (data.embColorList) {
                    $("#colorName").append("<option value=''>选择颜色</option>");
                    $.each(data.embColorList, function(index,element){
                        $("#colorName").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });
    });

    $("#clothesVersionNumber").bind('blur change',function(){
        var orderName = $("#orderName").val();
        $.ajax({
            url: "/erp/getembcolorhint",
            data: {"orderName": orderName},
            success:function(data){
                $("#colorName").empty();
                if (data.embColorList) {
                    $("#colorName").append("<option value=''>选择颜色</option>");
                    $.each(data.embColorList, function(index,element){
                        $("#colorName").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });
    });

    $("#colorName").change(function () {
        var colorName = $("#colorName").val();
        $.ajax({
            url: "/erp/getembsizehint",
            data: {
                "orderName": $('#orderName').val(),
                "colorName": colorName
            },
            success:function(data){
                $("#sizeName").empty();
                if (data.embSizeList) {
                    $("#sizeName").append("<option value=''>选择尺码</option>");
                    $.each(data.embSizeList, function(index,element){
                        $("#sizeName").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });
    })
});
var embStorageTable;
function search() {
    if (embStorageTable != undefined) {
        embStorageTable.clear(); //清空一下table
        embStorageTable.destroy(); //还原初始化了的datatable
    }
    if($("#orderName").val().trim()=="") {
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入要查询的款号！</span>",html: true});
        return false;
    }

    $.ajax({
        url: basePath + "erp/embstoragequery",
        type:'GET',
        data: {
            orderName:$("#orderName").val(),
            colorName:$("#colorName").val(),
            sizeName:$("#sizeName").val(),
        },
        success: function (html) {
            $("#entities").html(html);
            $('html, body').scrollTop($("#entities").offset().top);

            embStorageTable = $('#embStorageTable').DataTable({
                language : {
                    processing : "载入中",//处理页面数据的时候的显示
                    paginate : {//分页的样式文本内容。
                        previous : "上一页",
                        next : "下一页",
                        first : "第一页",
                        last : "最后一页"
                    },
                    search:"搜索：",
                    zeroRecords : "没有内容",//table tbody内容为空时，tbody的内容。
                    //下面三者构成了总体的左下角的内容。
                    info : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//左下角的信息显示，大写的词为关键字。
                    infoEmpty : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//筛选为空时左下角的显示。
                    infoFiltered : ""//筛选之后的左下角筛选提示(另一个是分页信息显示，在上面的info中已经设置，所以可以不显示)，
                },
                searching:true,
                lengthChange:true,
                // pageLength : 15,// 每页显示10条数据
                // pagingType : "full_numbers",
                "paging" : false,
                "info": false,
                scrollY: $(document.body).height() - 270,
                scrollCollapse: true,
                scroller:       true,
                fixedHeader: true,
                dom: 'Bfrtip',
                "buttons": [{
                    'extend': 'excel',
                    'text': '导出',//定义导出excel按钮的文字
                    'className': 'btn btn-success', //按钮的class样式
                    'title': "衣胚单款位置",
                    'exportOptions': {
                        'modifier': {
                            'page': 'all'
                        }
                    }
                }],
                "footerCallback": function ( row, data, start, end, display ) {
                    var api = this.api(), data;

                    // Remove the formatting to get integer data for summation
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };

                    // Total over all pages
                    total = api
                        .column( 4 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Total over this page
                    pageTotal = api
                        .column( 4, { page: 'current'} )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );


                    // Update footer
                    $( api.column( 4 ).footer() ).html(
                        "当前页数量："+pageTotal +"，总计："+ total
                    );
                }
            });
        },
        error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }
    })
}

function deleteStorage(orderName, colorName, sizeName, embStoreLocation, obj) {
    swal({
        title: "",
        text: "<span style=\"font-weight:bolder;font-size: 20px\">您确定要删除吗？</span>",
        type: "warning",
        html:true,
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: "确定",
        cancelButtonText:"我再想想",
        confirmButtonColor: "#ec6c62",
        showLoaderOnConfirm: true
    }, function() {
        $.ajax({
            url: basePath + "erp/deleteembstoragebyinfo",
            type:'POST',
            data: {
                orderName:orderName,
                colorName:colorName,
                sizeName:sizeName,
                embStoreLocation:embStoreLocation
            },
            success: function (data) {
                if(data.result == 0) {
                    $.unblockUI();
                    swal({
                            type:"success",
                            title:"",
                            text: "<span style=\"font-weight:bolder;font-size: 20px\">恭喜你，删除成功！</span>",
                            html: true
                        },
                        function(){
                            // location.href=basePath+"erp/cutStorageQueryStart";
                            embStorageTable.row($(obj).parents('tr')).remove().draw(false);
                        });
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，删除失败！</span>",html: true});
                }
            },
            error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        })
    });
}

function exportData() {
    var orderName = $("#orderName").val();
    var versionNumber = $("#clothesVersionNumber").val();
    var myDate = new Date();
    var title = orderName+'-'+versionNumber+'-衣胚库存-'+myDate.toLocaleString();
    var data = utl.Object.copyJson(hot.getData());//<=====读取handsontable的数据
    var result = [];
    var firstRow = new Array(data[0].length);
    firstRow[0] = title;
    for(var i=1;i<data[0].length;i++) {
        firstRow[i] = "";
    }
    result.push(firstRow);
    var col = 0;
    $.each(data,function (index, item) {
        for(var i=0;i<item.length;i++) {
            if(item[i]==null) {
                item[i] = '';
            }else if(index==1) {
                col++;
            }
        }
        result.push(item);
    });
    utl.XLSX.onExport(result,"Sheet1","xlsx",title+".xlsx");//<====导出
}

