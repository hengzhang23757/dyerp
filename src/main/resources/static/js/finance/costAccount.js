layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.21'
}).extend();
$(document).ready(function () {
    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumber", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });

});

var basePath=$("#basePath").val();

function search() {
    var orderName = $("#orderName").val();
    var pieceCost = $("#pieceCost").val();
    if (orderName == null || orderName === "" || pieceCost == null || pieceCost === ""){
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入完整信息！</span>",html: true});
        return false;
    }
    $.ajax({
        url: "/erp/getcostaccountbyorder",
        type:'GET',
        data: {
            orderName: orderName,
            pieceCost: pieceCost
        },
        success: function (data) {
            if(data) {
                $("#detailDiv").hide();
                $("#figureDiv").hide();
                $("#costCountTable").empty();
                $("#costCountTable").append("<tr>\n" +
                    "                                <th style=\"text-align: right\">单号:</th>\n" +
                    "                                <td style=\"text-align: left\">"+ data.clothesVersionNumber +"</td>\n" +
                    "                                <th style=\"text-align: right\">款号:</th>\n" +
                    "                                <td style=\"text-align: left\">"+ data.orderName +"</td>\n" +
                    "                                <th style=\"text-align: right\">品牌:</th>\n" +
                    "                                <td style=\"text-align: left\">"+ data.customerName +"</td>\n" +
                    "                                <th style=\"text-align: right\">订单量:</th>\n" +
                    "                                <td style=\"text-align: left\">"+ data.orderCount +"</td>\n" +
                    "                            </tr>\n" +
                    "                            <tr>\n" +
                    "                                <th style=\"text-align: right\">客供单价:</th>\n" +
                    "                                <td style=\"text-align: left\">"+ toDecimal(data.pieceCost) +"(元)</td>\n" +
                    "                                <th style=\"text-align: right\">客供总价:</th>\n" +
                    "                                <td style=\"text-align: left\">"+ toDecimal(data.customerTotalCost) +"(元)</td>\n" +
                    "                                <th style=\"text-align: right\">面料总价:</th>\n" +
                    "                                <td style=\"text-align: left\">"+ toDecimal(data.totalFabricCost) +"</td>\n" +
                    "                                <th style=\"text-align: right\">单件面料:</th>\n" +
                    "                                <td style=\"text-align: left\">"+ toDecimal(data.pieceFabricCost) +"(元)</td>\n" +
                    "                            </tr>\n" +
                    "                            <tr>\n" +
                    "                                <th style=\"text-align: right\">辅料总价:</th>\n" +
                    "                                <td style=\"text-align: left\">"+ toDecimal(data.totalAccessoryCost) +"</td>\n" +
                    "                                <th style=\"text-align: right\">单件辅料:</th>\n" +
                    "                                <td style=\"text-align: left\">"+ toDecimal(data.pieceAccessoryCost) +"(元)</td>\n" +
                    "                                <th style=\"text-align: right\">裁床总计:</th>\n" +
                    "                                <td style=\"text-align: left\">"+ toDecimal(data.cutSumPrice) +"(元)</td>\n" +
                    "                                <th style=\"text-align: right\">车缝总计:</th>\n" +
                    "                                <td style=\"text-align: left\">"+ toDecimal(data.sewSumPrice) +"(元)</td>\n" +
                    "                            </tr>\n"+
                    "                            <tr>\n" +
                    "                                <th style=\"text-align: right\">后整总计:</th>\n" +
                    "                                <td style=\"text-align: left\">"+ toDecimal(data.finishSumPrice) +"(元)</td>\n" +
                    "                                <th style=\"text-align: right\">单件加工:</th>\n" +
                    "                                <td style=\"text-align: left\">"+ toDecimal(data.pieceProcessCost) +"(元)</td>\n" +
                    "                                <th style=\"text-align: right\">单件采购:</th>\n" +
                    "                                <td style=\"text-align: left\">"+ toDecimal(data.pieceFabricAccessoryCost) +"(元)</td>\n" +
                    "                                <th style=\"text-align: right\">单件总:</th>\n" +
                    "                                <td style=\"text-align: left\">"+ toDecimal(data.pieceTotalCost) +"(元)</td>\n" +
                    "                            </tr>\n"+
                    "                            <tr>\n" +
                    "                                <th colspan='4' style=\"text-align: right\">倍率:</th>\n" +
                    "                                <th colspan='4' style=\"text-align: left\">"+ toDecimal(data.magnification) +"</th>\n" +
                    "                            </tr>"
                );
                $("#detailDiv").show();
                $("#figureDiv").show();
                var processTotal = toDecimal(data.cutSumPrice + data.sewSumPrice + data.finishSumPrice);
                var purchaseTotal = toDecimal(data.totalFabricCost + data.totalAccessoryCost);
                var cut = toDecimal(data.cutSumPrice);
                var sew = toDecimal(data.sewSumPrice);
                var finish = toDecimal(data.finishSumPrice);
                var fabric = toDecimal(data.totalFabricCost);
                var accessory = toDecimal(data.totalAccessoryCost);

                var dom = document.getElementById("container");
                var myChart = echarts.init(dom);
                var app = {};
                option = null;
                option = {
                    tooltip: {
                        trigger: 'item',
                        formatter: '{a} <br/>{b}: {c} ({d}%)'
                    },
                    legend: {
                        orient: 'vertical',
                        left: 10,
                        data: ['加工成本', '采购成本', '裁床总计', '车缝总计', '后整总计', '面料总计', '辅料总计']
                    },
                    series: [
                        {
                            name: '成本构成',
                            type: 'pie',
                            selectedMode: 'single',
                            radius: [0, '30%'],

                            label: {
                                position: 'inner'
                            },
                            labelLine: {
                                show: false
                            },
                            data: [
                                {value: processTotal, name: '加工成本', selected: true},
                                {value: purchaseTotal, name: '采购成本'}
                            ]
                        },
                        {
                            name: '成本构成',
                            type: 'pie',
                            radius: ['40%', '55%'],
                            label: {
                                formatter: '{a|{a}}{abg|}\n{hr|}\n  {b|{b}：}{c}  {per|{d}%}  ',
                                backgroundColor: '#eee',
                                borderColor: '#aaa',
                                borderWidth: 1,
                                borderRadius: 4,
                                // shadowBlur:3,
                                // shadowOffsetX: 2,
                                // shadowOffsetY: 2,
                                // shadowColor: '#999',
                                // padding: [0, 7],
                                rich: {
                                    a: {
                                        color: '#999',
                                        lineHeight: 22,
                                        align: 'center'
                                    },
                                    // abg: {
                                    //     backgroundColor: '#333',
                                    //     width: '100%',
                                    //     align: 'right',
                                    //     height: 22,
                                    //     borderRadius: [4, 4, 0, 0]
                                    // },
                                    hr: {
                                        borderColor: '#aaa',
                                        width: '100%',
                                        borderWidth: 0.5,
                                        height: 0
                                    },
                                    b: {
                                        fontSize: 16,
                                        lineHeight: 33
                                    },
                                    per: {
                                        color: '#eee',
                                        backgroundColor: '#334455',
                                        padding: [2, 4],
                                        borderRadius: 2
                                    }
                                }
                            },
                            data: [
                                {value: cut, name: '裁床总计'},
                                {value: sew, name: '车缝总计'},
                                {value: finish, name: '后整总计'},
                                {value: fabric, name: '面料总计'},
                                {value: accessory, name: '辅料总计'}
                            ]
                        }
                    ]
                };
                if (option && typeof option === "object") {
                    myChart.setOption(option, true);
                }

            }else {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，没有查到相关信息！</span>",html: true});
            }
        }, error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }
    });
}

function onlyNumber(obj){
    //得到第一个字符是否为负号
    var t = obj.value.charAt(0);
    //先把非数字的都替换掉，除了数字和.
    obj.value = obj.value.replace(/[^\d\.]/g,'');
    //必须保证第一个为数字而不是.
    obj.value = obj.value.replace(/^\./g,'');
    //保证只有出现一个.而没有多个.
    obj.value = obj.value.replace(/\.{2,}/g,'.');
    //保证.只出现一次，而不能出现两次以上
    obj.value = obj.value.replace('.','$#$').replace(/\./g,'').replace('$#$','.');
    //如果第一位是负号，则允许添加
    if(t == '-'){
        obj.value = '-'+obj.value;
    }
}

function toDecimal(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*100)/100;
    return f;
}