var form;
layui.use('form', function(){
    form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
    form.render();
});
layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.20'
}).extend({
    soulTable: 'soulTable'
});
$(document).ready(function () {
    layui.laydate.render({
        elem: '#from',
        trigger: 'click',
        value: new Date(),
        isInitValue: true
    });
    layui.laydate.render({
        elem: '#to',
        trigger: 'click',
        value: new Date(),
        isInitValue: true
    });
    form.render('select');
    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumber", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });

});

var reportTable;

layui.use(['form', 'table', 'soulTable'], function () {
    var table = layui.table,
        soulTable = layui.soulTable,
        form = layui.form,
        $ = layui.$;

    reportTable = table.render({
        elem: '#reportTable'
        ,cols:[[]]
        ,excel: {
            filename: '爆数查询.xlsx'
        }
        ,height: 'full-130'
        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
        ,defaultToolbar: ['filter', 'print']
        ,title: '爆数查询'
        ,totalRow: true
        ,page: true
        ,even: true
        ,loading: true
        ,limits: [500, 1000, 2000]
        ,limit: 1000 //每页默认显示的数量
        ,done: function () {
            soulTable.render(this);
        }
        ,filter: {
            bottom: false
        }
    });

    initTable();
    function initTable(from, to, orderName){
        var param = {};
        if (from != null && from !== ''){
            param.from = from;
        }
        if (to != null && to !== ''){
            param.to = to;
        }
        if (orderName != null && orderName !== ''){
            param.orderName = orderName;
        }
        var load = layer.load(2);
        $.ajax({
            url: "/erp/getexcessreport",
            type: 'GET',
            data: param,
            success: function (res) {
                if (res.data) {
                    var reportData = res.data;
                    table.render({
                        elem: '#reportTable'
                        ,cols:[[
                            {type:'checkbox', fixed:'left'}
                            ,{type:'numbers', align:'center', title:'序号', width:70}
                            ,{field:'clothesVersionNumber', title:'单号', align:'center', width:180, sort: true, filter: true, totalRowText: '合计'}
                            ,{field:'orderName', title:'款号', align:'center', width:180, sort: true, filter: true}
                            ,{field:'orderCount', title:'订单数', align:'center', width:100, sort: true, filter: true}
                            ,{field:'wellCount', title:'好片数', align:'center', width:100, sort: true, filter: true}
                            ,{field:'procedureNumber', title:'工序号', align:'center', width:100, sort: true, filter: true}
                            ,{field:'procedureName', title:'工序名', align:'center', width:150, sort: true, filter: true}
                            ,{field:'pieceCount', title:'总计件数', align:'center', width:120, sort: true, filter: true, totalRow: true}
                            ,{field:'cutCount', title:'超数', align:'center', width:120, sort: true, filter: true, totalRow: true}
                            ,{title:'操作', align:'center', width:80, toolbar: '#barTop', sort: true, filter: true}
                        ]]
                        ,excel: {
                            filename: '爆数.xlsx'
                        }
                        ,data: reportData   // 将新数据重新载入表格
                        ,overflow: 'tips'
                        ,even: true
                        ,height: 'full-130'
                        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                        ,defaultToolbar: ['filter', 'print']
                        ,title: '爆数查询'
                        ,totalRow: true
                        ,page: true
                        ,loading: true
                        ,limits: [500, 1000, 2000]
                        ,limit: 1000 //每页默认显示的数量
                        ,done: function () {
                            soulTable.render(this);
                            layer.close(load);
                        }
                        ,filter: {
                            bottom: false,
                        }
                    });

                } else {
                    layer.msg("获取失败！", {icon: 2});
                }
            }, error: function () {
                layer.msg("获取失败！", {icon: 2});
            }
        });
        return false;
    }

    //监听提交
    form.on('submit(searchBeat)', function(data){
        var data = {};
        var from = $("#from").val();
        var to = $("#to").val();
        var orderName = $("#orderName").val();
        if ((from == null || from === "" || to == null || to === "") && (orderName == null || orderName === "")){
            layer.msg("时间和款号至少输入一个", { icon: 2 });
            return false;
        }
        initTable(from, to, orderName);
        return false;
    });

    function reloadTable(monthString, orderName){
        var data = {};
        if (monthString == null || monthString === ""){
            data.orderName = orderName;
            var load = layer.load(2);
            $.ajax({
                url: "/erp/getexcessreport",
                type: 'GET',
                data: data,
                success: function (res) {
                    if (res.data) {
                        var reportData = res.data;
                        table.render({
                            elem: '#reportTable'
                            ,cols:[[
                                {type:'numbers', align:'center', title:'序号', width:70}
                                ,{field:'clothesVersionNumber', title:'单号', align:'center', width:180, sort: true, filter: true, totalRowText: '合计'}
                                ,{field:'orderName', title:'款号', align:'center', width:180, sort: true, filter: true}
                                ,{field:'orderCount', title:'订单数', align:'center', width:100, sort: true, filter: true}
                                ,{field:'wellCount', title:'好片数', align:'center', width:100, sort: true, filter: true}
                                ,{field:'procedureNumber', title:'工序号', align:'center', width:100, sort: true, filter: true}
                                ,{field:'procedureName', title:'工序名', align:'center', width:150, sort: true, filter: true}
                                ,{field:'pieceCount', title:'总计件数', align:'center', width:120, sort: true, filter: true, totalRow: true}
                                ,{field:'cutCount', title:'超数', align:'center', width:120, sort: true, filter: true, totalRow: true}
                                ,{title:'操作', align:'center', width:250, toolbar: '#barTop', sort: true, filter: true}
                            ]]
                            ,excel: {
                                filename: '爆数查询.xlsx'
                            }
                            ,data: reportData   // 将新数据重新载入表格
                            ,overflow: 'tips'
                            ,even: true
                            ,height: 'full-130'
                            ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                            ,defaultToolbar: ['filter', 'print']
                            ,title: '爆数查询'
                            ,totalRow: true
                            ,page: true
                            ,loading: true
                            ,limits: [500, 1000, 2000]
                            ,limit: 1000 //每页默认显示的数量
                            ,done: function () {
                                soulTable.render(this);
                                layer.close(load);
                            }
                            ,filter: {
                                bottom: true,
                                clearFilter: false,
                                items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                            }
                        });
                    } else {
                        layer.msg("获取失败！", {icon: 2});
                    }
                }, error: function () {
                    layer.msg("获取失败！", {icon: 2});
                }
            });
            return false;
        } else {
            data.monthString = monthString;
            if (orderName != null && orderName != ''){
                data.orderName = orderName;
            }
            var load = layer.load(2);
            $.ajax({
                url: "/erp/getexcessreport",
                type: 'GET',
                data: data,
                success: function (res) {
                    if (res.data) {
                        var reportData = res.data;
                        table.render({
                            elem: '#reportTable'
                            ,cols:[[
                                {type:'numbers', align:'center', title:'序号', width:70}
                                ,{field:'clothesVersionNumber', title:'单号', align:'center', width:180, sort: true, filter: true, totalRowText: '合计'}
                                ,{field:'orderName', title:'款号', align:'center', width:180, sort: true, filter: true}
                                ,{field:'orderCount', title:'订单数', align:'center', width:100, sort: true, filter: true}
                                ,{field:'wellCount', title:'好片数', align:'center', width:100, sort: true, filter: true}
                                ,{field:'procedureNumber', title:'工序号', align:'center', width:100, sort: true, filter: true}
                                ,{field:'procedureName', title:'工序名', align:'center', width:150, sort: true, filter: true}
                                ,{field:'pieceCount', title:'总计件数', align:'center', width:120, sort: true, filter: true, totalRow: true}
                                ,{field:'cutCount', title:'超数', align:'center', width:120, sort: true, filter: true, totalRow: true}
                                ,{title:'操作', align:'center', width:250, toolbar: '#barTop', sort: true, filter: true}
                            ]]
                            ,excel: {
                                filename: '爆数查询.xlsx'
                            }
                            ,data: reportData   // 将新数据重新载入表格
                            ,overflow: 'tips'
                            ,even: true
                            ,height: 'full-130'
                            ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                            ,defaultToolbar: ['filter', 'print']
                            ,title: '爆数查询'
                            ,totalRow: true
                            ,page: true
                            ,loading: true
                            ,limits: [500, 1000, 2000]
                            ,limit: 1000 //每页默认显示的数量
                            ,done: function () {
                                soulTable.render(this);
                                layer.close(load);
                            }
                            ,filter: {
                                bottom: true,
                                clearFilter: false,
                                items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                            }
                        });

                    } else {
                        layer.msg("获取失败！", {icon: 2});
                    }
                }, error: function () {
                    layer.msg("获取失败！", {icon: 2});
                }
            });
            return false;
        }
    }

    table.on('toolbar(reportTable)', function(obj) {
        if (obj.event === 'clearFilter') {
            soulTable.clearFilter('reportTable')
        }
        else if (obj.event === 'exportExcel') {
            var tableData = table.cache.reportTable;
            var procedureInfoList = [];
            $.each(tableData, function (t_index, t_item){
                if (t_item.LAY_CHECKED){
                    procedureInfoList.push(t_item);
                }
            });
            if (procedureInfoList.length === 0){
                layer.msg("请先选择");
                return false;
            }
            var loading = layer.load(2);
            $.ajax({
                url: "/erp/getexcessdetailbyinfo",
                type: 'GET',
                async: false,
                data: {
                    procedureJson: JSON.stringify(procedureInfoList)
                },
                success: function (res) {
                    if (res.data) {
                        var reportData = res.data;
                        table.render({
                            elem: '#exportTable'
                            ,cols:[[
                                {type:'numbers', align:'center', title:'序号', width:70}
                                ,{field:'orderName', title:'款号', align:'center', width:180, sort: true, filter: true}
                                ,{field:'orderCount', title:'订单数', align:'center', width:100, sort: true, filter: true, excel:{cellType: 'n'}}
                                ,{field:'wellCount', title:'好片数', align:'center', width:100, sort: true, filter: true, excel:{cellType: 'n'}}
                                ,{field:'procedureNumber', title:'工序号', align:'center', width:100, sort: true, filter: true}
                                ,{field:'procedureName', title:'工序名', align:'center', width:150, sort: true, filter: true}
                                ,{field:'pieceCount', title:'总计件数', align:'center', width:120, sort: true, filter: true, totalRow: true, excel:{cellType: 'n'}}
                                ,{field:'overCount', title:'超数', align:'center', width:120, sort: true, filter: true, totalRow: true, excel:{cellType: 'n'}}
                                ,{field:'employeeNumber', title:'工号', align:'center', width:150, sort: true, filter: true}
                                ,{field:'employeeName', title:'姓名', align:'center', width:150, sort: true, filter: true}
                                ,{field:'groupName', title:'组名', align:'center', width:150, sort: true, filter: true}
                                ,{field:'employeeCount', title:'员工计件', align:'center', width:150, sort: true, filter: true, excel:{cellType: 'n'}}
                                ,{field:'decreaseCount', title:'应减', align:'center', width:150, sort: true, filter: true, excel:{cellType: 'n'}}
                                ,{field:'price', title:'单价', align:'center', width:150, sort: true, filter: true, excel:{cellType: 'n'}}
                                ,{field:'decreaseMoney', title:'金额', align:'center', width:150, sort: true, filter: true, excel:{cellType: 'n'}}
                            ]]
                            ,excel: {
                                filename: '爆数详情.xlsx'
                            }
                            ,data: reportData   // 将新数据重新载入表格
                            ,overflow: 'tips'
                            ,even: true
                            ,height: 'full-130'
                            ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                            ,defaultToolbar: ['filter', 'print']
                            ,title: '爆数查询'
                            ,totalRow: true
                            ,page: true
                            ,loading: true
                            ,limits: [500, 1000, 2000]
                            ,limit: 1000 //每页默认显示的数量
                            ,done: function () {
                                soulTable.render(this);
                            }
                        });
                    } else {
                        layer.msg("获取失败！", {icon: 2});
                    }
                }, error: function () {
                    layer.msg("获取失败！", {icon: 2});
                }
            });
            layer.close(loading);
            soulTable.export('exportTable');
        }
    });

    table.on('tool(reportTable)', function(obj){
        var data = obj.data;
        var filterMonth = $("#monthString").val();
        var filterOrder = $("#orderName").val();
        if(obj.event === 'decrease'){
            var orderName = data.orderName;
            var procedureNumber = data.procedureNumber;
            var wellCount = data.wellCount;
            var pieceCount = data.pieceCount;
            var index = layer.open({
                type: 1 //Page层类型
                , title: "一键减数"
                , btn: ['保存']
                , shade: 0.6 //遮罩透明度
                , area: '1000px'
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , content: "<div><table>" +
                    "                                <td style=\"text-align: right;margin-bottom: 15px;padding-top: 15px\">\n" +
                    "                                    <label class=\"layui-form-label\">款号</label>\n" +
                    "                                </td>\n" +
                    "                                <td style=\"margin-bottom: 15px;padding-top: 15px\">\n" +
                    "                                    <input type=\"text\" style='width: 200px' readonly name=\"decreaseOrder\" id=\"decreaseOrder\" autocomplete=\"off\" class=\"layui-input\">\n" +
                    "                                </td>" +
                    "                                <td style=\"text-align: right;margin-bottom: 15px;padding-top: 15px\">\n" +
                    "                                    <label class=\"layui-form-label\">工序</label>\n" +
                    "                                </td>\n" +
                    "                                <td style=\"margin-bottom: 15px;padding-top: 15px\">\n" +
                    "                                    <input type=\"text\" style='width: 200px' readonly name=\"decreaseProcedure\" id=\"decreaseProcedure\" autocomplete=\"off\" class=\"layui-input\">\n" +
                    "                                </td>" +
                    "                                <td style=\"text-align: right;margin-bottom: 15px;padding-top: 15px\">\n" +
                    "                                    <label class=\"layui-form-label\">减数日期</label>\n" +
                    "                                </td>\n" +
                    "                                <td style=\"margin-bottom: 15px;padding-top: 15px\">\n" +
                    "                                    <input type=\"text\" style='width: 200px' name=\"decreaseDate\" id=\"decreaseDate\" autocomplete=\"off\" class=\"layui-input\">\n" +
                    "                                </td>" +
                    "</table></div>"
                ,yes: function(i, layero){
                    var decreaseDate = $("#decreaseDate").val();
                    if (decreaseDate == null || decreaseDate == ''){
                        layer.msg("日期必须！");
                        return false;
                    }
                    $.ajax({
                        url: "/erp/excessdecrease",
                        type: 'POST',
                        data: {
                            createTime: decreaseDate,
                            orderName: orderName,
                            procedureNumber: procedureNumber,
                            wellCount: wellCount,
                            pieceCount: pieceCount
                        },
                        success: function (res) {
                            if (res.result == 0) {
                                layer.msg("操作成功！", {icon: 1});
                                layer.close(index);
                                reloadTable(filterMonth, filterOrder);
                            } else if (res.result == 4) {
                                layer.msg("本月工资已锁定！", {icon: 1});
                            } else if (res.result == 10) {
                                layer.msg("该款号已锁定！", {icon: 1});
                            } else {
                                layer.msg("操作失败！", {icon: 2});
                            }
                        },
                        error: function () {
                            layer.msg("录入失败！", {icon: 2});
                        }
                    })
                }
                ,cancel: function () {
                    layer.close(index);
                }
            });
            setTimeout(function () {
                layui.laydate.render({
                    elem: '#decreaseDate',
                    type: 'date',
                    trigger: 'click'
                });
                $("#decreaseOrder").val(orderName);
                $("#decreaseProcedure").val(procedureNumber);
            },100);

        } else if(obj.event === 'detail'){
            var orderName = data.orderName;
            var procedureNumber = data.procedureNumber;
            var index = layer.open({
                type: 1 //Page层类型
                , title: "详情"
                , btn: ['保存']
                , shade: 0.6 //遮罩透明度
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , content: "<div><table id='excessTable' lay-filter='excessTable'></table></div>"
                ,cancel: function () {
                    layer.close(index);
                }
            });
            layer.full(index);
            setTimeout(function () {
                $.ajax({
                    url: "/erp/getexcessdetaildata",
                    type: 'GET',
                    data: {
                        orderName: orderName,
                        procedureNumber: procedureNumber
                    },
                    success: function (res) {
                        if (res.data) {
                            var reportData = res.data;
                            table.render({
                                elem: '#excessTable'
                                ,cols:[[
                                    {type:'numbers', align:'center', title:'序号', width:70}
                                    ,{field:'clothesVersionNumber', title:'单号', align:'center', width:180, sort: true, filter: true, totalRowText: '合计'}
                                    ,{field:'orderName', title:'款号', align:'center', width:180, sort: true, filter: true}
                                    ,{field:'procedureNumber', title:'工序号', align:'center', width:100, sort: true, filter: true}
                                    ,{field:'procedureName', title:'工序名', align:'center', width:100, sort: true, filter: true}
                                    ,{field:'employeeNumber', title:'工号', align:'center', width:100, sort: true, filter: true}
                                    ,{field:'employeeName', title:'姓名', align:'center', width:100, sort: true, filter: true}
                                    ,{field:'groupName', title:'组名', align:'center', width:100, sort: true, filter: true}
                                    ,{field:'pieceCount', title:'总计件数', align:'center', width:120, sort: true, filter: true, totalRow: true}
                                ]]
                                ,data: reportData   // 将新数据重新载入表格
                                ,overflow: 'tips'
                                ,even: true
                                ,height: 'full-130'
                                ,defaultToolbar: ['filter', 'print']
                                ,title: '爆数查询'
                                ,totalRow: true
                                ,page: true
                                ,limits: [500, 1000, 2000]
                                ,limit: 1000 //每页默认显示的数量
                                ,done: function () {
                                    soulTable.render(this);
                                }
                                ,filter: {
                                    bottom: true,
                                    clearFilter: false,
                                    items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                                }
                            });

                        } else {
                            layer.msg("获取失败！", {icon: 2});
                        }
                    }, error: function () {
                        layer.msg("获取失败！", {icon: 2});
                    }
                });
            },100);

        }
    });
});



function toDecimal(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*100)/100;
    return f;
}

function toDecimalFour(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*10000)/10000;
    return f;
}

function doHandleDate() {
    var myDate = new Date();
    var tYear = myDate.getFullYear();
    var tMonth = myDate.getMonth();

    var m = tMonth + 1;
    if (m.toString().length == 1) {
        m = "0" + m;
    }
    return tYear +'-'+ m;
}