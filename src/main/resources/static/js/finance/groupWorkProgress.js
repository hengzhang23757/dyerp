layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.21'
}).extend();
var form;
var gxm;
layui.use('form', function(){
    form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
    form.render();
});
$(document).ready(function () {
    layui.laydate.render({
        elem: '#from',
        trigger: 'click',
        value: new Date()
    });
    layui.laydate.render({
        elem: '#to',
        trigger: 'click',
        value: new Date()
    });

    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumber", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });

    $.ajax({
        url: "/erp/getallcheckgroupname",
        data: {},
        success:function(res){
            var groupData = [];
            if (res.data){
                $.each(res.data, function (index, item) {
                    groupData.push({name: item, value: item, selected: false});
                })
            }
            gxm = xmSelect.render({
                el: '#xmGroup',
                filterable: true,
                data: groupData
            });
        }, error:function(){
        }
    });

});

var basePath=$("#basePath").val();

layui.use(['form', 'soulTable', 'table', 'element'], function () {

    var table = layui.table,
        soulTable = layui.soulTable,
        $ = layui.$;
    var load = layer.load();

    reportTable = table.render({
        elem: '#reportTable'
        ,cols:[[]]
        ,loading:true
        ,data:[]
        ,height: 'full-130'
        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
        ,defaultToolbar: ['filter', 'print']
        ,title: '分组产能汇总表'
        ,totalRow: true
        ,page: true
        ,limits: [500, 1000, 2000]
        ,limit: 1000 //每页默认显示的数量
        ,done: function () {
            soulTable.render(this);
            layer.close(load);
        }
        ,filter: {
            bottom: true,
            items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
        }
    });

    // initTable();
    // function initTable(){
    //     var param = {};
    //     var from = $("#from").val();
    //     var to = $("#to").val();
    //     var type = $("#type").val();
    //     var procedureName = $("#procedureName").val();
    //     param.from = from;
    //     param.to = to;
    //     param.type = type;
    //     param.procedureName = procedureName;
    //     var load = layer.load(2);
    //     $.ajax({
    //         url: "/erp/getgroupprogressbyinfo",
    //         type: 'GET',
    //         data: param,
    //         success: function (res) {
    //             if (res.groupProgressList) {
    //                 var reportData = res.groupProgressList;
    //                 table.reload("reportTable", {
    //                     cols:[[
    //                         {type:'numbers', align:'center', title:'序号', width:'6%'}
    //                         ,{field:'clothesVersionNumber', title:'单号', align:'center', width:'15%', sort: true, filter: true}
    //                         ,{field:'orderName', title:'款号', align:'center', width:'15%', sort: true, filter: true}
    //                         ,{field:'groupName', title:'组名', align:'center', width:'14%', sort: true, filter: true}
    //                         ,{field:'procedureName', title:'工序', align:'center', width:'15%', sort: true, filter: true}
    //                         ,{field:'layerCount', title:'生产数量', align:'center', width:'20%', sort: true, filter: true, totalRow: true,excel: {cellType: 'n'}}
    //                         ,{field:'progressDate', title:'日期', align:'center', width:'15%', sort: true, filter: true}
    //                     ]]
    //                     ,excel: {
    //                         filename: '分组产能汇总表.xlsx'
    //                     }
    //                     ,data: reportData   // 将新数据重新载入表格
    //                     ,overflow: 'tips'
    //                     ,done: function () {
    //                         soulTable.render(this);
    //                         layer.close(load);
    //                     }
    //                     ,filter: {
    //                         bottom: true,
    //                         clearFilter: false,
    //                         items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
    //                     }
    //                 });
    //
    //             } else {
    //                 layer.msg("获取失败！", {icon: 2});
    //             }
    //         }, error: function () {
    //             layer.msg("获取失败！", {icon: 2});
    //         }
    //     });
    //     return false;
    // }


    table.on('toolbar(reportTable)', function(obj){
        if (obj.event === 'clearFilter') {
            // 清除所有筛选条件并重载表格
            // 参数: tableId
            soulTable.clearFilter('reportTable')
        } else if (obj.event === 'exportExcel') {
            soulTable.export('reportTable');
        }
    });

    //监听提交
    form.on('submit(searchBeat)', function(data){
        var param = {};
        var groupSelect = gxm.getValue('value');
        var from = $("#from").val();
        var to = $("#to").val();
        var orderName = $("#orderName").val();
        var type = $("#type").val();
        var procedureName = $("#procedureName").val();
        if (from == '' || from == null || to == '' || to == null){
            layer.msg("开始和结束日期必须");
            return false;
        }
        param.from = from;
        param.to = to;
        param.type = type;
        param.procedureName = procedureName;
        if (groupSelect.length != 0){
            param.groupList = groupSelect;
        }
        if (orderName != null && orderName != ""){
            param.orderName = orderName;
        }
        var load = layer.load(2);
        $.ajax({
            url: "/erp/getgroupprogressbyinfo",
            type: 'GET',
            data: param,
            success: function (res) {
                if (res.groupProgressList) {
                    var reportData = res.groupProgressList;
                    table.reload("reportTable", {
                        cols:[[
                            {type:'numbers', align:'center', title:'序号', width:'6%'}
                            ,{field:'clothesVersionNumber', title:'单号', align:'center', width:'15%', sort: true, filter: true}
                            ,{field:'orderName', title:'款号', align:'center', width:'15%', sort: true, filter: true}
                            ,{field:'groupName', title:'组名', align:'center', width:'14%', sort: true, filter: true}
                            ,{field:'procedureName', title:'工序', align:'center', width:'15%', sort: true, filter: true}
                            ,{field:'layerCount', title:'生产数量', align:'center', width:'20%', sort: true, filter: true, totalRow: true,excel: {cellType: 'n'}}
                            ,{field:'progressDate', title:'日期', align:'center', width:'15%', sort: true, filter: true}
                        ]]
                        ,excel: {
                            filename: '分组产能汇总表.xlsx'
                        }
                        ,data: reportData   // 将新数据重新载入表格
                        ,overflow: 'tips'
                        ,done: function () {
                            soulTable.render(this);
                            layer.close(load);
                        }
                        ,filter: {
                            bottom: true,
                            clearFilter: false,
                            items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                        }
                    });

                } else {
                    layer.msg("获取失败！", {icon: 2});
                }
            }, error: function () {
                layer.msg("获取失败！", {icon: 2});
            }
        });
        return false;
    });

});
function toDecimal(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*100)/100;
    return f;
}

function toDecimalFour(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*10000)/10000;
    return f;
}


function transDate(data) {return  moment(data).format("YYYY-MM-DD");}


function printDeal(){
    $("#printf").empty();
    var printBoxs = document.getElementsByName('printTable');
    var newContent = '';
    for(var i=0;i<printBoxs.length;i++) {
        newContent += printBoxs[i].innerHTML;
    }
    var f = document.getElementById('printf');
    // f.contentDocument.write('<link rel="stylesheet" type="text/css" href="/css/app.v2.css">');
    f.contentDocument.write('<style>' +
        'table {' +
        'border-collapse:collapse;' +
        '}' +
        'td {\n' +
        '        border: 1px solid #000000;\n' +
        '    }'+
        '</style>');
    f.contentDocument.write(newContent);
    f.contentDocument.close();
    window.frames['printf'].focus();
    try{
        window.frames['printf'].print();
    }catch(err){
        f.contentWindow.print();
    }
}
