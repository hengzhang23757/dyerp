var form;
layui.use('form', function(){
    form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
    form.render();
});
layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.15'
}).extend({
    soulTable: 'soulTable'
});
$(document).ready(function () {

    form.render('select');
    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumber", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });

});

var reportTable;

layui.use(['form', 'table', 'soulTable'], function () {
    var table = layui.table,
        soulTable = layui.soulTable,
        $ = layui.$;
    var load = layer.load();

    reportTable = table.render({
        elem: '#reportTable'
        ,cols:[[]]
        ,loading:true
        ,data:[]
        ,height: 'full-130'
        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
        ,defaultToolbar: ['filter', 'print']
        ,title: '单款工序工价汇总报表'
        ,totalRow: true
        ,page: true
        ,limits: [500, 1000, 2000]
        ,limit: 1000 //每页默认显示的数量
        ,done: function () {
            soulTable.render(this);
            layer.close(load);
        }
        ,filter: {
            bottom: true,
            items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
        }
    });

    //监听提交
    form.on('submit(searchBeat)', function(data){
        var data = {};
        var orderName = $("#orderName").val();
        var type = $("#type").val();
        if (orderName == null || orderName === ""){
            layer.msg("款号必须", { icon: 2 });
            return false;
        }
        data.orderName = orderName;
        data.type = type;
        data.viewType = 1;
        var load = layer.load(2);
        $.ajax({
            url: "/erp/getordersalarybyorderprocedure",
            type: 'GET',
            data: data,
            success: function (res) {
                if (res.orderSalaryList) {
                    var reportData = res.orderSalaryList;
                    layui.table.reload("reportTable", {
                        cols:[[
                            {type:'numbers', align:'center', title:'序号', width:70}
                            ,{field:'clothesVersionNumber', title:'单号', align:'center', width:130, sort: true, filter: true, totalRowText: '合计'}
                            ,{field:'orderName', title:'款号', align:'center', width:130, sort: true, filter: true}
                            ,{field:'orderCount', title:'订单数', align:'center', width:100, sort: true, filter: true, totalRow: true}
                            ,{field:'wellCount', title:'好片数', align:'center', width:100, sort: true, filter: true, totalRow: true}
                            ,{field:'pieceCount', title:'计件数', align:'center', width:100, sort: true, filter: true, totalRow: true}
                            ,{field:'procedureNumber', title:'工序号', align:'center', width:120, sort: true, filter: true}
                            ,{field:'procedureName', title:'工序名', align:'center', width:180, sort: true, filter: true}
                            ,{field:'priceSalary', title:'单价', align:'center', width:90, sort: true, filter: true, totalRow: true,templet: function (d) {
                                    return toDecimalFour(d.priceSalary);
                                },excel: {cellType: 'n'}}
                            ,{field:'pieceSalaryTwo', title:'上浮', align:'center', width:100, sort: true, filter: true, totalRow: true,templet: function (d) {
                                    return toDecimalFour(d.pieceSalaryTwo);
                                },excel: {cellType: 'n'}}
                            ,{field:'addition', title:'补贴比例(%)', align:'center',width:120, sort: true, filter: true, totalRow: true,templet: function (d) {
                                    return d.addition * 100;
                                },excel: {cellType: 'n'}}
                            ,{field:'sumPrice', title:'总单价', align:'center', width:100, sort: true, filter: true, totalRow: true,templet: function (d) {
                                    return toDecimalFour(d.sumPrice);
                                },excel: {cellType: 'n'}}
                            ,{field:'sumSalary', title:'工序总工资', align:'center', width:150, sort: true, filter: true, totalRow: true,templet: function (d) {
                                    return toDecimalFour(d.sumSalary);
                                },excel: {cellType: 'n'}}
                        ]]
                        ,excel: {
                            filename: '订单工序工价汇总.xlsx'
                        }
                        ,data: reportData   // 将新数据重新载入表格
                        ,overflow: 'tips'
                        ,done: function () {
                            soulTable.render(this);
                            layer.close(load);
                        }
                        ,filter: {
                            bottom: true,
                            clearFilter: false,
                            items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                        }
                    });

                } else {
                    layer.msg("获取失败！", {icon: 2});
                }
            }, error: function () {
                layer.msg("获取失败！", {icon: 2});
            }
        });
        return false;
    });

    table.on('toolbar(reportTable)', function(obj) {
        if (obj.event === 'clearFilter') {
            // 清除所有筛选条件并重载表格
            // 参数: tableId
            soulTable.clearFilter('reportTable')
        } else if (obj.event === 'exportExcel') {
            soulTable.export('reportTable');
        }
    });

});



function toDecimal(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*100)/100;
    return f;
}

function toDecimalFour(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*10000)/10000;
    return f;
}