var basePath=$("#basePath").val();
var dom = document.getElementById("container");
var yesterdayFinish = document.getElementById("yesterdayFinish");
var yesterdayTailor = document.getElementById("yesterdayTailor");
var yesterdayWorkShop = document.getElementById("yesterdayWorkShop");
var myChart = echarts.init(dom);
var yesterdayFinishChart = echarts.init(yesterdayFinish);
var yesterdayTailorChart = echarts.init(yesterdayTailor);
var yesterdayWorkShopChart = echarts.init(yesterdayWorkShop);
$(document).ready(function () {

    var finishLegendList = [];
    var finishOrderCountList = [];
    var finishTailorCountList = [];
    var finishWellCountList = [];
    var finishEmbOutList = [];
    var finishWorkShopList = [];
    var finishSumCountList = [];
    var yesterdayFinishCountList = [];
    $.ajax({
        url: "yesterdayfinishreport",
        type:'GET',
        data: {},
        success: function (data) {
            if(data) {
                var yesterdayFinishData = data.yesterdayFinishList;
                for (var i = 0; i < yesterdayFinishData.length; i++){
                    finishLegendList.push(yesterdayFinishData[i]["clothesVersionNumber"]+"\n"+yesterdayFinishData[i]["orderName"]);
                    finishOrderCountList.push(yesterdayFinishData[i]["orderCount"]);
                    finishTailorCountList.push(yesterdayFinishData[i]["tailorCount"]);
                    finishWellCountList.push(yesterdayFinishData[i]["wellCount"]);
                    finishEmbOutList.push(yesterdayFinishData[i]["embCount"]);
                    finishWorkShopList.push(yesterdayFinishData[i]["workShopCount"]);
                    finishSumCountList.push(yesterdayFinishData[i]["finishCount"]);
                    yesterdayFinishCountList.push(yesterdayFinishData[i]["yesterdayFinishCount"]);

                }
                option = null;

                option = {
                    title: {
                        text: '昨日后整',
                        textStyle: {//主标题文本样式{"fontSize": 18,"fontWeight": "bolder","color": "#333"}
                            fontSize: 16
                        }
                    },
                    tooltip: {
                        trigger: 'axis',
                        axisPointer: {
                            type: 'shadow'
                        }
                    },
                    legend: {
                        data: ['订单','裁数','好片','衣胚','成品','后整','昨日']
                    },
                    grid: {
                        left: '3%',
                        right: '4%',
                        bottom: '3%',
                        containLabel: true
                    },
                    toolbox: {
                        show: true
                    },
                    xAxis: {
                        type: 'value',
                        axisLabel: {
                            formatter: '{value}'
                        }
                    },
                    yAxis: {
                        type: 'category',
                        inverse: true,
                        data: finishLegendList,
                        axisLabel: {
                            textStyle:{
                                color:'#333',
                                fontSize:11
                            }
                        }

                    },
                    series: [
                        {
                            name: '订单',
                            type: 'bar',
                            // color:"#92DBEC",
                            data: finishOrderCountList
                        },
                        {
                            name: '裁数',
                            type: 'bar',
                            // color:"#87B8D6",
                            data: finishTailorCountList
                        },{
                            name: '好片',
                            type: 'bar',
                            // color:"#7B7380",
                            data: finishWellCountList
                        },{
                            name: '衣胚',
                            type: 'bar',
                            // color:"#CF8095",
                            data: finishEmbOutList
                        },{
                            name: '成品',
                            type: 'bar',
                            // color:"#EF8F91",
                            data: finishWorkShopList
                        },{
                            name: '后整',
                            type: 'bar',
                            // color:"#F1BCB6",
                            data: finishSumCountList
                        },{
                            name: '昨日',
                            type: 'bar',
                            // color:"#FFD3CA",
                            data: yesterdayFinishCountList
                        }
                    ]
                };
                if (option && typeof option === "object") {
                    yesterdayFinishChart.setOption(option, true);
                }
                // tools.loopShowTooltip(yesterdayTailorChart, option, {loopSeries: true});
                window.onresize = yesterdayFinishChart.resize;

            }else {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，没有查到相关信息！</span>",html: true});
            }
        },
        error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }
    });


    var orderNameList = [];
    var orderCountList = [];
    var tailorCountList = [];
    var yesterdayTailorList = [];
    $.ajax({
        url: "yesterdaytailorreport",
        type:'GET',
        data: {},
        success: function (data) {
            if(data) {
                var yesterdayTailorData = data.yesterdayTailorList;
                for (var i = 0; i < yesterdayTailorData.length; i++){
                    orderNameList.push(yesterdayTailorData[i]["clothesVersionNumber"]+"\n"+yesterdayTailorData[i]["orderName"]);
                    orderCountList.push(yesterdayTailorData[i]["orderCount"]);
                    tailorCountList.push(yesterdayTailorData[i]["tailorCount"]);
                    yesterdayTailorList.push(yesterdayTailorData[i]["yesterdayCount"]);
                }
                var app = {};
                option = null;
                var posList = [
                    'left', 'right', 'top', 'bottom',
                    'inside',
                    'insideTop', 'insideLeft', 'insideRight', 'insideBottom',
                    'insideTopLeft', 'insideTopRight', 'insideBottomLeft', 'insideBottomRight'
                ];

                app.configParameters = {
                    rotate: {
                        min: -90,
                        max: 90
                    },
                    align: {
                        options: {
                            left: 'left',
                            center: 'center',
                            right: 'right'
                        }
                    },
                    verticalAlign: {
                        options: {
                            top: 'top',
                            middle: 'middle',
                            bottom: 'bottom'
                        }
                    },
                    position: {
                        options: echarts.util.reduce(posList, function (map, pos) {
                            map[pos] = pos;
                            return map;
                        }, {})
                    },
                    distance: {
                        min: 0,
                        max: 100
                    }
                };

                app.config = {
                    rotate: 90,
                    align: 'left',
                    verticalAlign: 'middle',
                    position: 'insideBottom',
                    distance: 15,
                    onChange: function () {
                        var labelOption = {
                            normal: {
                                rotate: app.config.rotate,
                                align: app.config.align,
                                verticalAlign: app.config.verticalAlign,
                                position: app.config.position,
                                distance: app.config.distance
                            }
                        };
                        myChart.setOption({
                            series: [{
                                label: labelOption
                            }, {
                                label: labelOption
                            }, {
                                label: labelOption
                            }, {
                                label: labelOption
                            }]
                        });
                    }
                };


                var labelOption = {
                    normal: {
                        show: false,
                        position: app.config.position,
                        distance: app.config.distance,
                        align: app.config.align,
                        verticalAlign: app.config.verticalAlign,
                        rotate: app.config.rotate,
                        formatter: '{c}  {name|{a}}',
                        fontSize: 16,
                        rich: {
                            name: {
                                textBorderColor: '#fff'
                            }
                        }
                    }
                };

                option = {
                    title: {
                        text: "昨日裁床",
                        textStyle: {//主标题文本样式{"fontSize": 18,"fontWeight": "bolder","color": "#333"}
                            fontSize: 16
                        }
                    },
                    color: ['#006699', '#4cabce', '#e5323e'],
                    tooltip: {
                        trigger: 'axis',
                        triggerOn:"mousemove",
                        showContent:true,
                        axisPointer: {
                            type: 'cross'
                            // type: 'shadow'
                        }
                    },
                    legend: {
                        data: ['订单数', '已裁数', '昨日裁数']
                    },
                    calculable: true,
                    grid: {
                        left: '3%',
                        right: '4%',
                        bottom: '3%',
                        containLabel: true
                    },
                    xAxis: [
                        {
                            type: 'category',
                            axisTick: {show: true},
                            data: orderNameList,
                            axisLabel:{
                                rotate: 20,
                                textStyle:{
                                    color:'#333',
                                    fontSize:9
                                }
                            }
                        }
                    ],
                    yAxis: [
                        {
                            type: 'value',
                            axisLabel:{
                                show:true,
                                textStyle:{
                                    color:'#333',
                                    fontSize:9
                                }
                            }
                        }

                    ],
                    series: [
                        {
                            name: '订单数',
                            type: 'bar',
                            barGap: 0,
                            label: labelOption,
                            data: orderCountList
                        },
                        {
                            name: '已裁数',
                            type: 'bar',
                            label: labelOption,
                            data: tailorCountList
                        },
                        {
                            name: '昨日裁数',
                            type: 'bar',
                            label: labelOption,
                            data: yesterdayTailorList
                        }
                    ]
                };
                if (option && typeof option === "object") {
                    yesterdayTailorChart.setOption(option, true);
                }
                // tools.loopShowTooltip(yesterdayTailorChart, option, {loopSeries: true});
                window.onresize = yesterdayTailorChart.resize;

            }else {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，没有查到相关信息！</span>",html: true});
            }
        },
        error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }
    });


    var legendList = [];
    var orderSumList = [];
    var finishCountList = [];
    var yesterdayCountList = [];
    $.ajax({
        url: "yesterdayworkshopreport",
        type:'GET',
        data: {},
        success: function (data) {
            if(data) {
                var yesterdayHangData = data.yesterdayHangList;
                for (var i = 0; i < yesterdayHangData.length; i++){
                    legendList.push(yesterdayHangData[i]["groupName"] + "\n" +yesterdayHangData[i]["clothesVersionNumber"]+"\n"+yesterdayHangData[i]["orderName"]);
                    orderSumList.push(yesterdayHangData[i]["orderCount"]);
                    finishCountList.push(yesterdayHangData[i]["finishCount"]);
                    yesterdayCountList.push(yesterdayHangData[i]["yesterdayCount"]);
                }
                option = null;

                var seriesLabel = {
                    normal: {
                        show: true,
                        textBorderColor: '#333',
                        textBorderWidth: 2
                    }
                };

                option = {
                    title: {
                        text: '昨日车缝',
                        textStyle: {//主标题文本样式{"fontSize": 18,"fontWeight": "bolder","color": "#333"}
                            fontSize: 16
                        }
                    },
                    tooltip: {
                        trigger: 'axis',
                        axisPointer: {
                            type: 'shadow'
                        }
                    },
                    legend: {
                        data: ['订单量','已完成','昨日完成']
                    },
                    grid: {
                        left: '3%',
                        right: '4%',
                        bottom: '3%',
                        containLabel: true
                    },
                    toolbox: {
                        show: true
                    },
                    xAxis: {
                        type: 'value',
                        axisLabel: {
                            formatter: '{value}',
                            textStyle:{
                                color:'#333',
                                fontSize:9
                            }
                        }
                    },
                    yAxis: {
                        type: 'category',
                        inverse: true,
                        data: legendList,
                        axisLabel: {
                            textStyle:{
                                color:'#333',
                                fontSize:9
                            }
                        }

                    },
                    series: [
                        {
                            name: '订单量',
                            type: 'bar',
                            color:"#fd5825",
                            data: orderSumList
                        },
                        {
                            name: '已完成',
                            type: 'bar',
                            color:"#f7efe5",
                            data: finishCountList
                        },
                        {
                            name: '昨日完成',
                            type: 'bar',
                            color:"#3fabaf",
                            data: yesterdayCountList
                        }
                    ]
                };
                if (option && typeof option === "object") {
                    yesterdayWorkShopChart.setOption(option, true);
                }
                // tools.loopShowTooltip(yesterdayTailorChart, option, {loopSeries: true});
                window.onresize = yesterdayWorkShopChart.resize;

            }else {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，没有查到相关信息！</span>",html: true});
            }
        },
        error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }
    });

    // $.ajax({
    //     url: "yesterdayreworkreport",
    //     type:'GET',
    //     data: {},
    //     success: function (data) {
    //         if(data) {
    //             var i = 0;
    //             for (var orderNameKey in data){
    //                 var reWorkList = [];
    //                 var procedureInfoList = [];
    //                 var yesterdayReWorkData = data[orderNameKey];
    //                 for (var j = 0; j < yesterdayReWorkData.length; j++){
    //                     var reWork = {};
    //                     procedureInfoList.push(yesterdayReWorkData[j].procedureNumber + "-" + yesterdayReWorkData[j].procedureName);
    //                     reWork.name= yesterdayReWorkData[j].procedureNumber + "-" + yesterdayReWorkData[j].procedureName;
    //                     reWork.value = yesterdayReWorkData[j].reWorkCount;
    //                     reWorkList.push(reWork);
    //                 }
    //
    //                 var yesterdayReWork = document.getElementById("yesterdayReWork"+i);
    //                 var yesterdayReWorkChart = echarts.init(yesterdayReWork);
    //                 var option = {
    //                     title : {
    //                         text: yesterdayReWorkData[0].clothesVersionNumber+"\n"+ orderNameKey,
    //                         subtext: '昨日返工',
    //                         x:'right',
    //                         textStyle: {//主标题文本样式{"fontSize": 18,"fontWeight": "bolder","color": "#333"}
    //                             fontSize: 16
    //                         }
    //
    //                     },
    //                     tooltip : {
    //                         trigger: 'item',
    //                         formatter: "{a} <br/>{b} : {c} ({d}%)"
    //                     },
    //                     legend: {
    //                         orient: 'vertical',
    //                         left: 'left',
    //                         data: procedureInfoList
    //                     },
    //                     grid: {
    //                         left: '3%',
    //                         right: '4%',
    //                         bottom: '3%',
    //                         containLabel: true
    //                     },
    //                     series : [
    //                         {
    //                             name: '返工',
    //                             type: 'pie',
    //                             radius : '55%',
    //                             center: ['50%', '60%'],
    //                             data:reWorkList,
    //                             itemStyle: {
    //                                 emphasis: {
    //                                     shadowBlur: 10,
    //                                     shadowOffsetX: 0,
    //                                     shadowColor: 'rgba(0, 0, 0, 0.5)'
    //                                 }
    //                             }
    //                         }
    //                     ]
    //                 };
    //                 if (option && typeof option === "object") {
    //                     yesterdayReWorkChart.setOption(option, true);
    //                 }
    //                 window.onresize = yesterdayReWorkChart.resize;
    //                 i++;
    //             }
    //
    //         }else {
    //             swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，没有查到相关信息！</span>",html: true});
    //         }
    //     },
    //     error: function () {
    //         swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
    //     }
    // });

    var dateList = [];
    var amount1 = [];
    var amount2 = [];
    var amount3 = [];
    var amount4 = [];
    $.ajax({
        url: "getweekworkamount",
        type:'GET',
        data: {},
        success: function (data) {
            if(data) {
                console.log(data);
                dateList = data["dateList"];
                var data1 = data["1"];
                var data2 = data["2"];
                var data3 = data["3"];
                var data4 = data["4"];
                $.each(dateList, function(index,element){
                    amount1.push(data1[element]);
                    amount2.push(data2[element]);
                    amount3.push(data3[element]);
                    amount4.push(data4[element]);
                });

                option = null;
                option = {
                    title: {
                        text: '一周看板'
                    },
                    color:['#d06d6a','#697883','#5bd6e9','#69d18c'],
                    tooltip : {
                        trigger: 'axis',
                        axisPointer: {
                            type: 'cross',
                            label: {
                                backgroundColor: '#6a7985'
                            }
                        }
                    },
                    legend: {
                        data:['裁床裁数','下衣胚数','车间产量','后整产量'],
                        textStyle:{
                            fontSize: 18
                        }
                    },
                    grid: {
                        left: '3%',
                        right: '6%',
                        bottom: '3%',
                        containLabel: true
                    },
                    xAxis : [
                        {
                            type : 'category',
                            boundaryGap : false,
                            data : dateList,
                            // data : [getDay(-7),getDay(-6),getDay(-5),getDay(-4),getDay(-3),getDay(-2),getDay(-1)]
                        }
                    ],
                    yAxis : [
                        {
                            type : 'value'
                        }
                    ],
                    series : [
                        {
                            name:'后整产量',
                            type:'line',
                            stack: '总量1',
                            label: {
                                normal: {
                                    show: true
                                }
                            },
                            areaStyle: {
                                normal: {
                                    color: "#00000000"
                                }
                            },
                            // data:[220, 182, 191, 234, 290, 330, 310]
                            data : amount4
                        },
                        {
                            name:'车间产量',
                            type:'line',
                            stack: '总量2',
                            label: {
                                normal: {
                                    show: true
                                }
                            },
                            areaStyle: {
                                normal: {
                                    color: "#00000000"
                                }
                            },
                            // data:[150, 232, 201, 154, 190, 330, 410]
                            data : amount3
                        },
                        {
                            name:'下衣胚数',
                            type:'line',
                            stack: '总量3',
                            label: {
                                normal: {
                                    show: true
                                }
                            },
                            areaStyle: {
                                normal: {
                                    color: "#00000000"
                                }
                            },
                            // data:[320, 332, 301, 334, 390, 330, 320]
                            data : amount2
                        },
                        {
                            name:'裁床裁数',
                            type:'line',
                            stack: '总量4',
                            label: {
                                normal: {
                                    show: true,
                                    position: 'top'
                                }
                            },
                            areaStyle: {
                                normal: {
                                    color: "#00000000"
                                }
                            },
                            // data:[820, 932, 901, 934, 1290, 1330, 1320]
                            data : amount1
                        }
                    ]
                };
                if (option && typeof option === "object") {
                    myChart.setOption(option, true);
                }
                // tools.loopShowTooltip(myChart, option, {loopSeries: true});
                window.onresize = myChart.resize;

            }else {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，没有查到相关信息！</span>",html: true});
            }
        },
        error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }
    });

});


function getDay(day){
    var today = new Date();
    var targetday_milliseconds=today.getTime() + 1000*60*60*24*day;
    today.setTime(targetday_milliseconds); //注意，这行是关键代码
    var tYear = today.getFullYear();
    var tMonth = today.getMonth();
    var tDate = today.getDate();
    tMonth = doHandleMonth(tMonth + 1);
    tDate = doHandleMonth(tDate);
    return tYear+"-"+tMonth+"-"+tDate;
}
function doHandleMonth(month){
    var m = month;
    if(month.toString().length == 1){
        m = "0" + month;
    }
    return m;
}

function transDate(data) {return  moment(data).format("YYYY-MM-DD");};

function toDecimal(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*100)/100;
    return f;
}