var basePath=$("#basePath").val();
$(document).ready(function () {

    var isShow = true;

    $('#animation-left-nav').click(function(){

        //选择出所有的span，并判断是不是hidden
        $('.layui-nav-item span').each(function(){
            if($(this).is(':hidden')){
                $(this).show();
                $(this).prev().off('mouseenter').unbind('mouseleave');
            }else{
                var that = this;
                $(that).prev().hover(function(){
                    layer.tips($(that).text(), $(that).prev(), {tips:[2,'#009688'],time:0});
                },function(){
                    layer.closeAll('tips');
                });
                $(that).hide();
            }
        });

        //判断isshow的状态
        if(isShow){
            $('.layui-side.layui-bg-black').width(60); //设置宽度
            //将footer和body的宽度修改
            $('.layui-body').css('left', 60+'px');
            //将二级导航栏隐藏
            $('dd span').each(function(){
                var that = this;
                $(that).prev().hover(function(){
                    layer.tips($(that).text(), $(that).prev(), {tips:[2,'#009688'],time:0});
                },function(){
                    layer.closeAll('tips');
                });
                $(that).hide();
            });
            //修改标志位
            isShow =false;

            //左侧导航栏收缩提示
        }else{
            $('.layui-side.layui-bg-black').width(200);
            $('.layui-body').css('left', 200+'px');
            $('dd span').each(function(){
                $(this).show();
                $(this).prev().off('mouseenter').unbind('mouseleave');
            });
            isShow =true;
        }
    });

});


layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.4'
}).extend({                         // 模块别名
    soulTable: 'soulTable',
});
var homepageTable;
layui.use(['form', 'soulTable', 'table', 'upload','yutons_sug'], function () {
    var table = layui.table,
        soulTable = layui.soulTable;
    var load = layer.load();
    homepageTable = table.render({
        elem: '#homepageTable'
        ,url:'gethomepagereport'
        ,method:'post'
        ,cols: [[]]
        ,loading:true
        ,done: function (res, curr, count) {
            table.init('homepageTable',{//转换成静态表格
                cols:[[
                    {type:'numbers', align:'center', title:'序号', width:'4%'}
                    ,{field:'orderName', title:'款号', align:'center', width:'8%', sort: true, filter: true, totalRowText: '合计'}
                    ,{field:'season', title:'季度', align:'center', width:'8%', sort: true, filter: true}
                    ,{field:'customerName', title:'品牌', align:'center', width:'6%', sort: true, filter: true}
                    ,{field:'orderCount', title:'订单量', align:'center',width:'7%', sort: true, filter: true, totalRow: true}
                    ,{field:'receivedDate', title:'收单日期', align:'center', width:'8%', sort: true, filter: true,templet: function (d) {
                            return moment(d.receivedDate).format("YYYY-MM-DD");
                        }}
                    ,{field:'orderPartPercent', title:'制单', align:'center', width:'8%', sort: true, filter: true,templet: function(d){
                            //***重点***：判断颜色
                            var colorCount = toDecimal(Number(d.orderPartPercent)*100);
                            var color = '';
                            if(30<colorCount && colorCount<90){
                                color='layui-bg-orange'
                            }else if(0<colorCount && colorCount<=30){
                                color='layui-bg-red'
                            }
                            //***重点***：拼接进度条
                            return '<div class="layui-progress layui-progress-big" lay-showpercent="true"><div class="layui-progress-bar '+color+'" lay-percent="'+colorCount+'%"></div></div><br>'
                        }}
                    ,{field:'returnFabricPercent', title:'回布', align:'center', width:'9%', sort: true, filter: true,templet: function(d){
                            //***重点***：判断颜色
                            var colorCount = toDecimal((d.returnFabricPercent)*100);
                            var color = '';
                            if(30<colorCount && colorCount<90){
                                color='layui-bg-orange'
                            }else if(0<colorCount && colorCount<=30){
                                color='layui-bg-red'
                            }
                            //***重点***：拼接进度条
                            return '<div class="layui-progress layui-progress-big" lay-showpercent="true"><div class="layui-progress-bar '+color+'" lay-percent="'+colorCount+'%"></div></div><br>'
                        }}
                    ,{field:'looseFabricPercent', title:'松布', align:'center', width:'8%', sort: true, filter: true,templet: function(d){
                            //***重点***：判断颜色
                            var colorCount = toDecimal((d.looseFabricPercent)*100);
                            var color = '';
                            if(30<colorCount && colorCount<90){
                                color='layui-bg-orange'
                            }else if(0<colorCount && colorCount<=30){
                                color='layui-bg-red'
                            }
                            //***重点***：拼接进度条
                            return '<div class="layui-progress layui-progress-big" lay-showpercent="true"><div class="layui-progress-bar '+color+'" lay-percent="'+colorCount+'%"></div></div><br>'
                        }}
                    ,{field:'cutPercent', title:'好片', align:'center', width:'9%', sort: true, filter: true,templet: function(d){
                            //***重点***：判断颜色
                            var colorCount = toDecimal(d.cutPercent*100);
                            var color = '';
                            if(30<colorCount && colorCount<90){
                                color='layui-bg-orange'
                            }else if(0<colorCount && colorCount<=30){
                                color='layui-bg-red'
                            }
                            //***重点***：拼接进度条
                            return '<div class="layui-progress layui-progress-big" lay-showpercent="true"><div class="layui-progress-bar '+color+'" lay-percent="'+colorCount+'%"></div></div><br>'
                        }}
                    ,{field:'embPercent', title:'衣胚', align:'center', width:'8%', sort: true, filter: true,templet: function(d){
                            //***重点***：判断颜色
                            var colorCount = toDecimal(d.embPercent*100);
                            var color = '';
                            if(30<colorCount && colorCount<90){
                                color='layui-bg-orange'
                            }else if(0<colorCount && colorCount<=30){
                                color='layui-bg-red'
                            }
                            //***重点***：拼接进度条
                            return '<div class="layui-progress layui-progress-big" lay-showpercent="true"><div class="layui-progress-bar '+color+'" lay-percent="'+colorCount+'%"></div></div><br>'
                        }}
                    ,{field:'hangPercent', title:'挂片', align:'center', width:'9%', sort: true, filter: true,templet: function(d){
                            //***重点***：判断颜色
                            var colorCount = toDecimal(d.hangPercent*100);
                            var color = '';
                            if(30<colorCount && colorCount<90){
                                color='layui-bg-orange'
                            }else if(0<colorCount && colorCount<=30){
                                color='layui-bg-red'
                            }
                            //***重点***：拼接进度条
                            return '<div class="layui-progress layui-progress-big" lay-showpercent="true"><div class="layui-progress-bar '+color+'" lay-percent="'+colorCount+'%"></div></div><br>'
                        }}
                    ,{field:'clothesPercent', title:'成衣', align:'center', width:'9%', sort: true, filter: true,templet: function(d){
                            //***重点***：判断颜色
                            var colorCount = toDecimal(d.clothesPercent*100);
                            var color = '';
                            if(30<colorCount && colorCount<90){
                                color='layui-bg-orange'
                            }else if(0<colorCount && colorCount<=30){
                                color='layui-bg-red'
                            }
                            //***重点***：拼接进度条
                            return '<div class="layui-progress layui-progress-big" lay-showpercent="true"><div class="layui-progress-bar '+color+'" lay-percent="'+colorCount+'%"></div></div><br>'
                        }}
                ]]
                ,data:res.data
                ,height: 'full-150'
                ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                ,title: '首页总览'
                ,totalRow: true
                ,page: false
                ,even: true
                ,limit: Number.MAX_VALUE //每页默认显示的数量
                ,overflow: 'tips'
                ,soulSort: false
                ,done: function () {
                    soulTable.render(this);
                    layer.close(load);
                    element.render();
                }
                ,filter: {
                    bottom: true,
                    items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                }
            });
        }
    });

    table.on('sort(homepageTable)', function() {
        $.each($(".layui-table-body,.layui-table-main tr"),function (index,item) {
            var percent = $(item).find("td").eq(6).children().children().children().attr("lay-percent");
            $(item).find("td").eq(6).children().children().children().css("width",percent);
            if(index!=0)
            $(item).find("td").eq(6).children().children().children().append("<span class=\"layui-progress-text\">"+percent+"</span>");
            var percent = $(item).find("td").eq(7).children().children().children().attr("lay-percent");
            $(item).find("td").eq(7).children().children().children().css("width",percent);
            if(index!=0)
            $(item).find("td").eq(7).children().children().children().append("<span class=\"layui-progress-text\">"+percent+"</span>");
            var percent = $(item).find("td").eq(8).children().children().children().attr("lay-percent");
            $(item).find("td").eq(8).children().children().children().css("width",percent);
            if(index!=0)
            $(item).find("td").eq(8).children().children().children().append("<span class=\"layui-progress-text\">"+percent+"</span>");
            var percent = $(item).find("td").eq(9).children().children().children().attr("lay-percent");
            $(item).find("td").eq(9).children().children().children().css("width",percent);
            if(index!=0)
            $(item).find("td").eq(9).children().children().children().append("<span class=\"layui-progress-text\">"+percent+"</span>");
            var percent = $(item).find("td").eq(10).children().children().children().attr("lay-percent");
            $(item).find("td").eq(10).children().children().children().css("width",percent);
            if(index!=0)
            $(item).find("td").eq(10).children().children().children().append("<span class=\"layui-progress-text\">"+percent+"</span>");
            var percent = $(item).find("td").eq(11).children().children().children().attr("lay-percent");
            $(item).find("td").eq(11).children().children().children().css("width",percent);
            if(index!=0)
            $(item).find("td").eq(11).children().children().children().append("<span class=\"layui-progress-text\">"+percent+"</span>");
            var percent = $(item).find("td").eq(12).children().children().children().attr("lay-percent");
            $(item).find("td").eq(12).children().children().children().css("width",percent);
            if(index!=0)
            $(item).find("td").eq(12).children().children().children().append("<span class=\"layui-progress-text\">"+percent+"</span>");
        })
    })

    table.on('toolbar(homepageTable)', function(obj) {
        if (obj.event === 'clearFilter') {
            // 清除所有筛选条件并重载表格
            // 参数: tableId
            soulTable.clearFilter('homepageTable')
        } else if (obj.event === 'refresh') {
            homepageTable.reload();
        } else if (obj.event === 'exportExcel') {
            soulTable.export('homepageTable');
        }
    });

});



var element;
layui.use(['element'],function(){
    element = layui.element;
    //监听左侧菜单点击
    element.on('nav(left-menu)', function(elem){
        layer.closeAll('tips');
        if(elem[0].className == 'menuTag')
            addTab(elem[0].text,elem[0].attributes[3].nodeValue,elem[0].id);
    });
    // 监听tab选项卡切换
    // element.on('tab(tab-switch)', function(data){
    //
    //     var id = data.elem.context.activeElement.id;
    //     layui.each($(".layui-nav-child"), function () {
    //         $(this).find("dd").removeClass("layui-this");
    //     });
    //     $("#"+id).attr("class","layui-this");
    //
    // });
});

/**
 * 新增tab选项卡，如果已经存在则打开已经存在的，不存在则新增
 * @param tabTitle 选项卡标题名称
 * @param tabUrl 选项卡链接的页面URL
 * @param tabId 选项卡id
 */
function addTab(tabTitle,tabUrl,tabId){
    if ($(".layui-tab-title li[lay-id=" + tabId + "]").length > 0) {
        element.tabChange('tab-switch', tabId);
    }else{
        $.ajax({
            url: tabUrl,
            success: function (html) {
                //切换到新增的tab上
                element.tabAdd('tab-switch', {
                    title: tabTitle
                    ,content: '<iframe src='+tabUrl+' width="100%" style="height: 550px;" frameborder="0" scrolling="auto" onload="setIframeHeight(this)"></iframe>' // 选项卡内容，支持传入html
                    ,id: tabId //选项卡标题的lay-id属性值
                });
                element.tabChange('tab-switch', tabId);
            }
        })
    }
}

/**
 * ifrme自适应页面高度，需要设定min-height
 * @param iframe
 */
function setIframeHeight(iframe) {
    // if (iframe) {
    //     var iframeWin = iframe.contentWindow || iframe.contentDocument.parentWindow;
    //     if (iframeWin.document.body) {
    //         iframe.height = iframeWin.document.documentElement.scrollHeight || iframeWin.document.body.scrollHeight;
    //     }
    // }
    if (iframe) {
        var h = $(window).height() - 41 - 60 - 10 - 44 - 10;
        $(iframe).css("height", h + "px");
    }
};

function loginOut() {
    swal({
        title: "",
        text: "<span style=\"font-weight:bolder;font-size: 20px\">你确定退出么？</span>",
        type: "warning",
        html:true,
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: "确定",
        cancelButtonText:"我再想想",
        confirmButtonColor: "#ec6c62",
        showLoaderOnConfirm: false
    }, function() {
        window.location.href = "/";
    });
}

function updatePwd() {
    $.blockUI({
        css: {
            width: '30%',
            top: '15%',
            border: 'none',
            padding: '15px',
            backgroundColor: '#fff',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: 1,
            color: '#000'
        },
        message: $('#pwdEdit')
    });

    $("#pwdEditYes").unbind("click").bind("click", function () {
        if($("#originPassWord").val().trim()=="") {
            swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入原密码！</span>",html: true});
            return false;
        }
        if($("#newPassWord").val().trim()=="") {
            swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入新密码！</span>",html: true});
            return false;
        }
        if($("#secondNewPassWord").val().trim()=="") {
            swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请再次输入新密码！</span>",html: true});
            return false;
        }
        if($("#newPassWord").val().trim()!=$("#secondNewPassWord").val().trim()) {
            swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">两次输入新密码不一致！</span>",html: true});
            return false;
        }
        $.ajax({
            url: "updateUser",
            type: 'GET',
            data: {
                originPassWord:$("#originPassWord").val(),
                newPassWord:$("#newPassWord").val(),
                userName:$("#loginUserName").val()
            },
            success: function (data) {
                if(data == 1) {
                    $("#originPassWord").val("");
                    $("#secondNewPassWord").val("");
                    $("#newPassWord").val("");
                    $.unblockUI();
                    swal({
                        type:"success",
                        title:"",
                        text: "<span style=\"font-weight:bolder;font-size: 20px\">恭喜你，保存成功！</span>",
                        html: true
                    });
                }else if(data == -1){
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，原密码错误！</span>",html: true});
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，保存失败！</span>",html: true});
                }
            },
            error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        });
    });

    $("#pwdEditNo").unbind("click").bind("click", function () {
        $("#originPassWord").val("");
        $("#secondNewPassWord").val("");
        $("#newPassWord").val("");
        $.unblockUI();
    });
}


function getDay(day){
    var today = new Date();
    var targetday_milliseconds=today.getTime() + 1000*60*60*24*day;
    today.setTime(targetday_milliseconds); //注意，这行是关键代码
    var tYear = today.getFullYear();
    var tMonth = today.getMonth();
    var tDate = today.getDate();
    tMonth = doHandleMonth(tMonth + 1);
    tDate = doHandleMonth(tDate);
    return tYear+"-"+tMonth+"-"+tDate;
}
function doHandleMonth(month){
    var m = month;
    if(month.toString().length == 1){
        m = "0" + month;
    }
    return m;
}

function transDate(data) {return  moment(data).format("YYYY-MM-DD");};

function toDecimal(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*100)/100;
    return f;
}