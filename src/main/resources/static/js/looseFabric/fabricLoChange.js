var basePath=$("#basePath").val();
layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.21'
}).extend();
$(document).ready(function () {

    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumber", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });

    $("#orderName").bind('blur change',function(){
        var orderName = $("#orderName").val();
        $("#colorName").empty();
        $.ajax({
            url: "/erp/getloosefabriccolornamehint",
            data: {"orderName": orderName},
            success:function(data){
                $("#colorName").empty();
                if (data.colorNameList) {
                    $("#colorName").append("<option value=''>选择颜色</option>");
                    $.each(data.colorNameList, function(index,element){
                        $("#colorName").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });
    });

    $("#clothesVersionNumber").bind('blur change',function(){
        var orderName = $("#orderName").val();
        $("#colorName").empty();
        $.ajax({
            url: "/erp/getloosefabriccolornamehint",
            data: {"orderName": orderName},
            success:function(data){
                $("#colorName").empty();
                if (data.colorNameList) {
                    $("#colorName").append("<option value=''>选择颜色</option>");
                    $.each(data.colorNameList, function(index,element){
                        $("#colorName").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });
    });

    $("#colorName").change(function () {
        var orderName = $("#orderName").val();
        var colorName = $("#colorName").val();
        $("#fabricName").empty();
        $.ajax({
            url: "/erp/getloosefabricfabricnamehint",
            data: {
                "orderName": orderName,
                "colorName": colorName
            },
            success:function(data){
                $("#fabricName").empty();
                if (data.fabricNameList) {
                    $("#fabricName").append("<option value=''>选择面料</option>");
                    $.each(data.fabricNameList, function(index,element){
                        $("#fabricName").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });
    });

    $("#fabricName").change(function () {
        var orderName = $("#orderName").val();
        var colorName = $("#colorName").val();
        var fabricName = $(this).val();
        $("#fabricColor").empty();
        $.ajax({
            url: "/erp/getloosefabriccolorhint",
            data: {
                "orderName": orderName,
                "colorName": colorName,
                "fabricName": fabricName
            },
            success:function(data){
                $("#fabricColor").empty();
                if (data.fabricColorList) {
                    $("#fabricColor").append("<option value=''>选择颜色</option>");
                    $.each(data.fabricColorList, function(index,element){
                        $("#fabricColor").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });

    });

});


function search() {
    if($("#orderName").val().trim()=="") {
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入要查询的订单号！</span>",html: true});
        return false;
    }
    $.ajax({
        url: basePath + "erp/searchjarbyinfo",
        type:'GET',
        data: {
            orderName:$("#orderName").val(),
            colorName:$("#colorName").val(),
            fabricName:$("#fabricName").val(),
            fabricColor:$("#fabricColor").val()
        },
        success: function (data) {
            if(data) {
                createTailorInfoTable(data.looseFabricList);
            }else {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，没有查到相关信息！</span>",html: true});
            }
        },
        error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }
    })

}

var tailorInfoTable;

function createTailorInfoTable(data) {
    if (tailorInfoTable != undefined) {
        tailorInfoTable.clear(); //清空一下table
        tailorInfoTable.destroy(); //还原初始化了的datatable
    }
    tailorInfoTable = $('#tailorInfoTable').DataTable({
        "data":data,
        language : {
            processing : "载入中",//处理页面数据的时候的显示
            paginate : {//分页的样式文本内容。
                previous : "上一页",
                next : "下一页",
                first : "第一页",
                last : "最后一页"
            },
            search:"搜索：",
            zeroRecords : "没有内容",//table tbody内容为空时，tbody的内容。
            //下面三者构成了总体的左下角的内容。
            info : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//左下角的信息显示，大写的词为关键字。
            infoEmpty : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//筛选为空时左下角的显示。
            infoFiltered : ""//筛选之后的左下角筛选提示(另一个是分页信息显示，在上面的info中已经设置，所以可以不显示)，
        },
        // pageLength : 50,// 每页显示10条数据
        pagingType : "full_numbers", // 分页样式：simple,simple_numbers,full,full_numbers，
        "paging": false,
        "info": true,
        searching:false,
        ordering:true,
        lengthChange:false,
        scrollX: 1500,
        scrollY: $(document.body).height() - 270,
        fixedHeader: true,
        scrollCollapse: true,
        scroller:       true,
        "columns": [
            {
                "data": null,
                "title":"序号",
                "width":"40px",
                "defaultContent": "",
                "sClass": "text-center",
                render: function (data, type, row, meta) {
                    var no = meta.settings._iDisplayStart + meta.row + 1;
                    return no;
                }
            },{
                "data": "orderName",
                "title":"订单号",
                "width":"120px",
                "defaultContent": "",
                "sClass": "text-center"
            },{
                "data": "clothesVersionNumber",
                "title":"版单号",
                "width":"120px",
                "defaultContent": "",
                "sClass": "text-center"
            },{
                "data": "colorName",
                "title":"订单颜色",
                "width":"80px",
                "defaultContent": "",
                "sClass": "text-center"
            }, {
                "data": "fabricName",
                "title":"面料",
                "width":"300px",
                "defaultContent": "",
                "sClass": "text-center"
            }, {
                "data": "fabricColor",
                "title":"面料颜色",
                "width":"90px",
                "defaultContent": "",
                "sClass": "text-center"
            }, {
                "data": "jarNumber",
                "title":"缸号",
                "width":"120px",
                "defaultContent": "",
                "sClass": "text-center"
            }, {
                "title":"操作",
                "width":"60px",
                "defaultContent": "",
                "sClass": "text-center"
            }
        ],
        "columnDefs" :
            [{
                "orderable" : false, // 禁用排序
                "targets" : [7], // 指定的列
                "render" : function(data, type, full, meta) {
                    return "<a href='#' style='color:#ff0000' onclick='updateJar(this)'>修改缸号</a>";
                }
            }]
    });
}


function updateJar(obj) {
    $("#orderName1").val($(obj).parent().parent().find("td").eq(1).text());
    $("#clothesVersionNumber1").val($(obj).parent().parent().find("td").eq(2).text());
    $("#colorName1").val($(obj).parent().parent().find("td").eq(3).text());
    $("#fabricName1").val($(obj).parent().parent().find("td").eq(4).text());
    $("#fabricColor1").val($(obj).parent().parent().find("td").eq(5).text());
    $("#jarNumber1").val($(obj).parent().parent().find("td").eq(6).text());
    $("#updateJar1").val($(obj).parent().parent().find("td").eq(6).text());
    $.blockUI({
        css: {
            width: '60%',
            top: '10%',
            left: '20%',
            border: 'none',
            padding: '15px',
            backgroundColor: '#fff',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: 1,
            color: '#000'
        },
        message: $('#updateLoColor')
    });

    $("#updateLoYes").unbind("click").bind("click", function () {
        var flag = false;
        $("#updateLoColor input").each(function () {
            if($(this).val().trim() == "") {
                flag = true;
                return false;
            }
        });
        if(flag) {
            swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请填写完所有字段信息！</span>",html: true});
            return false;
        }
        $.ajax({
            url: "/erp/updatejarbyinfo",
            type:'POST',
            data: {
                orderName: $("#orderName1").val(),
                fabricName: $("#fabricName1").val(),
                fabricColor: $("#fabricColor1").val(),
                jarNumber: $("#jarNumber1").val(),
                colorName: $("#colorName1").val(),
                updateJar: $("#updateJar1").val().trim()
            },
            success:function(data){
                if(data == 0) {
                    $.unblockUI();
                    search();
                    swal({type:"success",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">修改成功！</span>",html: true});
                    $("#updateLoColor input").val("");
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">添加失败！</span>",html: true});
                }
            },
            error:function(){
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务器发生了未知错误～！</span>",html: true});
            }
        });
        $.unblockUI();
        $("#updateLoColor input").val("");

    });
    $("#updateLoNo").unbind("click").bind("click", function () {
        $.unblockUI();
        $("#updateLoColor input").val("");
    });

}
