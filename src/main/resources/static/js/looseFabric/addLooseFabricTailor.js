var basePath=$("#basePath").val();
var userRole=$("#userRole").val();
layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.21'
}).extend();
$(document).ready(function () {
    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumber", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });

    $("#orderName").bind('blur change',function(){
        var orderName = $(this).val();
        $("#colorName").empty();
        $("#fabricName").empty();
        $("#fabricColor").empty();
        $("#jarNumber").empty();
        $("#partName").empty();
        $("#batchNumber").val("");
        $.ajax({
            url: "/erp/getloosefabriccolornamehint",
            data: {"orderName": orderName},
            success:function(data){
                $("#colorName").empty();
                if (data.colorNameList) {
                    $("#colorName").append("<option value=''>选择色组</option>");
                    $.each(data.colorNameList, function(index,element){
                        $("#colorName").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });
        $.ajax({
            url: "/erp/getotherprintpartnamesbyorder",
            data: {"orderName": orderName},
            success:function(data){
                $("#partName").empty();
                if (data.printPartNameList) {
                    $("#partName").append("<option value=''>选择部位</option>");
                    $("#partName").append("<option value='主身'>主身</option>");
                    $.each(data.printPartNameList, function(index,element){
                        $("#partName").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });
    });

    $("#clothesVersionNumber").bind('blur change',function(){
        var orderName = $("#orderName").val();
        $("#colorName").empty();
        $("#fabricName").empty();
        $("#fabricColor").empty();
        $("#jarNumber").empty();
        $("#partName").empty();
        $("#batchNumber").val("");
        $.ajax({
            url: "/erp/getloosefabriccolornamehint",
            data: {"orderName": orderName},
            success:function(data){
                $("#colorName").empty();
                if (data.colorNameList) {
                    $("#colorName").append("<option value=''>选择色组</option>");
                    $.each(data.colorNameList, function(index,element){
                        $("#colorName").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });
        $.ajax({
            url: "/erp/getotherprintpartnamesbyorder",
            data: {"orderName": orderName},
            success:function(data){
                $("#partName").empty();
                if (data.printPartNameList) {
                    $("#partName").append("<option value=''>选择部位</option>");
                    $("#partName").append("<option value='主身'>主身</option>");
                    $.each(data.printPartNameList, function(index,element){
                        $("#partName").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });
    });

    $("#colorName").change(function () {
        var orderName = $("#orderName").val();
        var colorName = $(this).val();
        $("#fabricName").empty();
        $("#fabricColor").empty();
        $("#jarNumber").empty();
        $("#batchNumber").val("");
        $.ajax({
            url: "/erp/getloosefabricfabricnamehint",
            data: {
                "orderName": orderName,
                "colorName": colorName
            },
            success:function(data){
                $("#fabricName").empty();
                if (data.fabricNameList) {
                    $("#fabricName").append("<option value=''>选择面料</option>");
                    $.each(data.fabricNameList, function(index,element){
                        $("#fabricName").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });

    });

    $("#fabricName").change(function () {
        var orderName = $("#orderName").val();
        var colorName = $("#colorName").val();
        var fabricName = $(this).val();
        $("#fabricColor").empty();
        $("#jarNumber").empty();
        $("#batchNumber").val("");
        $.ajax({
            url: "/erp/getloosefabriccolorhint",
            data: {
                "orderName": orderName,
                "colorName": colorName,
                "fabricName": fabricName,
            },
            success:function(data){
                $("#fabricColor").empty();
                if (data.fabricColorList) {
                    $("#fabricColor").append("<option value=''>选择颜色</option>");
                    $.each(data.fabricColorList, function(index,element){
                        $("#fabricColor").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });

    });

    $("#fabricColor").change(function () {
        var orderName = $("#orderName").val();
        var colorName = $("#colorName").val();
        var fabricName = $("#fabricName").val();
        var fabricColor = $(this).val();
        $("#jarNumber").empty();
        $("#batchNumber").val("");
        $("#unit").val("");
        $.ajax({
            url: "/erp/getloosefabricjarnameofunprint",
            data: {
                "orderName": orderName,
                "colorName": colorName,
                "fabricName": fabricName,
                "fabricColor": fabricColor
            },
            success:function(data){
                $("#jarNumber").empty();
                if (data.jarNumberList) {
                    $("#jarNumber").append("<option value=''>选择缸号</option>");
                    $.each(data.jarNumberList, function(index,element){
                        $("#jarNumber").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });

        $.ajax({
            url: "/erp/getloosefabricunitbyinfo",
            data: {
                "orderName": orderName,
                "colorName": colorName,
                "fabricName": fabricName,
                "fabricColor": fabricColor
            },
            success:function(data){
                $("#unit").val("");
                if (data) {
                    $("#unit").val(data)
                }
            },
            error:function(){
            }
        });
    });

    $("#jarNumber").change(function () {
        var orderName = $("#orderName").val();
        var colorName = $("#colorName").val();
        var fabricName = $("#fabricName").val();
        var fabricColor = $("#fabricColor").val();
        var jarNumber = $(this).val();
        $("#batchNumber").val("");
        $("select[name='location']").empty();
        $.ajax({
            url: "/erp/getloosefabricbatchbystylecolorjar",
            data: {
                "orderName": orderName,
                "colorName": colorName,
                "fabricName": fabricName,
                "fabricColor": fabricColor,
                "jarNumber": jarNumber
            },
            success:function(data){
                $("#batchNumber").val("");
                if (data) {
                    $("#batchNumber").val(data)
                }
            },
            error:function(){
            }
        });

        $.ajax({
            url: "/erp/getfabriclocationhint",
            data: {
                "orderName": orderName,
                "colorName": colorName,
                "fabricName": fabricName,
                "fabricColor": fabricColor,
                "jarName": jarNumber
            },
            success:function(data){
                $("#location").empty();
                if (data.fabricLocationList) {
                    $("#location").append("<option value=''>"+"选择位置"+"</option>");
                    $.each(data.fabricLocationList, function(index,element){
                        $("#location").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });

    });


    $("#location").change(function () {
        var orderName = $("#orderName").val();
        var colorName = $("#colorName").val();
        var fabricName = $("#fabricName").val();
        var fabricColor = $("#fabricColor").val();
        var jarNumber = $("#jarNumber").val();
        var location = $(this).val();
        $("#finishCount").text('');
        $("#remainCount").text('');
        $("#locationCount").text('');
        $.ajax({
            url: "/erp/getloosefabricinfobyjarlocation",
            data: {
                "orderName": orderName,
                "colorName": colorName,
                "fabricName": fabricName,
                "fabricColor": fabricColor,
                "jarNumber": jarNumber,
                "location": location
            },
            success:function(data){
                $("#finishCount").text('');
                $("#remainCount").text('');
                $("#locationCount").text('');
                if (data) {
                    $("#remainCount").text(data.remain);
                    $("#finishCount").text(data.finish);
                    $("#locationCount").text(data.locationCount);

                    var orderOfBatch = data.finish + 1;
                    var orderOfLayerArr = $('[name="orderOfLayer"]');
                    for (var i = 0; i < orderOfLayerArr.length; i++){
                        $(orderOfLayerArr[i]).text(i + 1);
                        if (data.locationCount > 0){
                            $("input[name='batchOrder']").eq(i).val(orderOfBatch);
                            $("input[name='batchNumber']").eq(i).val(orderOfBatch);
                            orderOfBatch ++;
                        }
                    }
                }
            },
            error:function(){
            }
        });
    })

    $("#partName").change(function () {
        var orderName = $("#orderName").val();
        var colorName = $("#colorName").val();
        var partName = $("#partName").val();

        $.ajax({
            url: "/erp/historyunitconsumptioninfo",
            data: {
                "orderName": orderName,
                "colorName": colorName,
                "partName": partName
            },
            success:function(data){
                $("#cutInfo").val("");
                if (data) {
                    $("#cutInfo").val("单/已/未:" + data.colorOrderCount + "/" + data.layerCount + "/" + data.unFinishCount);
                    $("#historyUnit").val(toDecimalThree(data.historyUnit));
                    $("#needFabric").val(toDecimal(data.needFabric) + "/" + toDecimal(data.preCut) + "/" + toDecimal(data.actNeed));
                }
            }, error:function(){
            }
        });

    });
});

var tailorTable;

function kgToP() {
    $("input[name='batchOrder']").each(function (index,item){
        var thisWeight = Number($("input[name='weight']").eq(index).val());
        $("input[name='weight']").eq(index).val(toDecimalFour(thisWeight * 2.2046));
    });
}

function pToKg() {
    $("input[name='batchOrder']").each(function (index,item){
        var thisWeight = Number($("input[name='weight']").eq(index).val());
        $("input[name='weight']").eq(index).val(toDecimalFour(thisWeight / 2.2046));
    });
}

function addFabric() {
    var flag = false;
    $("#generateFabricInfo input,select").each(function () {
        if($(this).val().trim() == "") {
            flag = true;
            return false;
        }
    });
    if(flag) {
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请填写完基本信息！</span>",html: true});
        return false;
    }
    var looseFabricJson = [];
    $("input[name='batchOrder']").each(function (index,item){
        var looseFabric = {};
        looseFabric.orderName = $("#orderName").val();
        looseFabric.clothesVersionNumber = $("#clothesVersionNumber").val();
        looseFabric.fabricName = $("#fabricName").val();
        looseFabric.fabricColor = $("#fabricColor").val();
        if ($("#loColor").val() == "无"){
            looseFabric.jarNumber = $("#jarNumber").val() + "#";
        }else {
            looseFabric.jarNumber = $("#jarNumber").val() + "#" + $("#loColor").val();
        }
        looseFabric.looseHour = $("#looseHour").val();
        looseFabric.totalBatch = $("#batchNumber").val();
        looseFabric.colorName = $("#colorName").val();
        looseFabric.batchOrder = $("input[name='batchOrder']").eq(index).val();
        looseFabric.batchNumber = $("input[name='batchNumber']").eq(index).val();
        looseFabric.weight = $("input[name='weight']").eq(index).val();
        looseFabric.operateType = $("select[name='operateType']").eq(index).val();
        looseFabric.fromLocation = $("#location").val();
        looseFabric.unit = $("#unit").val();
        looseFabricJson.push(looseFabric);
    });
    $.ajax({
        url: "/erp/generateloosefabricinfo",
        type:'POST',
        data: {
            looseFabricJson:JSON.stringify(looseFabricJson)
        },
        success: function (data) {
            if(data.looseFabricList && data.looseFabricList != "null") {
                var looseFabricData = data.looseFabricList;
                swal({   type:"success",
                        title:"",
                        text: "<span style=\"font-weight:bolder;font-size: 20px\">恭喜你，生成成功！</span>",
                        html: true
                    },function () {
                        var href = $("li.active a[data-toggle='tab']",parent.document).attr("href");
                        var tabId = href.substr(7);
                        window.parent.document.getElementById("tailorA").click();
                        var numberId = window.parent.document.getElementById("numberID").value;
                        var list = "";
                        if ($('#tailorTable', window.parent.document).hasClass('dataTable')) {
                            $('#tailorTable_wrapper', window.parent.document).remove();
                            var $div = $("<table class=\"table table-striped\" id=\"tailorTable\" >\n" +
                                "             <thead>\n" +
                                "             <tr bgcolor=\"#ffcb99\" style=\"color: black;\">\n" +
                                "                 <input type=\"text\" hidden id=\"numberID\" value=\"1\">\n" +
                                "                 <th style=\"width: 30px;text-align:center;font-size:14px\"><input type=\"checkbox\" onclick=\"checkAll(this)\"></th>\n" +
                                "                 <th style=\"width: 60px;text-align:center;font-size:14px\">序号</th>\n" +
                                "                 <th style=\"width: 90px;text-align:center;font-size:14px\">订单号</th>\n" +
                                "                 <th style=\"width: 90px;text-align:center;font-size:14px\">版单号</th>\n" +
                                "                 <th style=\"width: 180px;text-align:center;font-size:14px\">面料名称</th>\n" +
                                "                 <th style=\"width: 90px;text-align:center;font-size:14px\">面料颜色</th>\n" +
                                "                 <th style=\"width: 90px;text-align:center;font-size:14px\">缸号</th>\n" +
                                "                 <th style=\"width: 60px;text-align:center;font-size:14px\">卷次</th>\n" +
                                "                 <th style=\"width: 60px;text-align:center;font-size:14px\">卷号</th>\n" +
                                "                 <th style=\"width: 60px;text-align:center;font-size:14px\">总卷数</th>\n" +
                                "                 <th style=\"width: 60px;text-align:center;font-size:14px\">数量</th>\n" +
                                "                 <th style=\"width: 60px;text-align:center;font-size:14px\">单位</th>\n" +
                                "                 <th style=\"width: 90px;text-align:center;font-size:14px\">订单颜色</th>\n" +
                                "                 <th style=\"width: 220px;text-align:center;font-size:14px\">时间</th>\n" +
                                "                 <th style=\"width: 90px;text-align:center;font-size:14px\">松布时长</th>\n" +
                                "                 <th style=\"width: 120px;text-align:center;font-size:14px\">出库类型</th>\n" +
                                "                 <th style=\"width: 60px;text-align:center;font-size:14px\">位置</th>\n" +
                                "                 <th style=\"width: 90px;text-align:center;font-size:14px\">二维码</th>\n" +
                                "                 <th style=\"width: 120px;text-align:center;font-size:14px\">操作</th>\n" +
                                "             </tr>\n" +
                                "             </thead>\n" +
                                "             <tbody id=\"tailorBody\">\n" +
                                "             </tbody>\n" +
                                "         </table>");
                            $("#tailorTableDiv",window.parent.document).append($div);
                        }
                        var $tailorBody = window.parent.document.getElementById("tailorBody");
                        var $saveButton = window.parent.document.getElementById("saveButton");
                    $saveButton.style.display='block';

                        $.each(looseFabricData,function (index,item) {
                            list +=  "<tr>" +
                                "<td><input type='checkbox' value='"+numberId+"'></td>" +
                                "<td>"+numberId+"</td>" +
                                "<td>"+item.orderName+"</td>" +
                                "<td>"+item.clothesVersionNumber+"</td>" +
                                "<td>"+item.fabricName+"</td>" +
                                "<td>"+item.fabricColor+"</td>" +
                                "<td>"+item.jarNumber+"</td>" +
                                "<td>"+item.batchOrder+"</td>" +
                                "<td>"+item.batchNumber+"</td>" +
                                "<td>"+item.totalBatch+"</td>" +
                                "<td>"+item.weight+"</td>" +
                                "<td>"+item.unit+"</td>" +
                                "<td>"+item.colorName+"</td>" +
                                "<td>"+transDate(item.looseTime)+"</td>" +
                                "<td>"+item.looseHour+"</td>" +
                                "<td>"+item.operateType+"</td>" +
                                "<td>"+item.fromLocation+"</td>" +
                                "<td>"+item.qCodeID+"</td>" +
                                "<td><a href='#' style='color:#3e8eea' onclick='showQrCode(this)'>查看</a>&nbsp;&nbsp;&nbsp;<a href='#' style='color:#3e8eea' onclick='delTailor(this)'>删除</a></td>" +
                                "</tr>";
                            numberId++;
                        });
                        $tailorBody.innerHTML = list;
                        window.parent.document.getElementById("tailorBody").value=numberId;

                        tailorTable = $('#tailorTable',window.parent.document).DataTable({
                            language : {
                                processing : "载入中",//处理页面数据的时候的显示
                                paginate : {//分页的样式文本内容。
                                    previous : "上一页",
                                    next : "下一页",
                                    first : "第一页",
                                    last : "最后一页"
                                },
                                search:"搜索：",
                                lengthMenu:"显示 _MENU_ 条",
                                zeroRecords : "没有内容",//table tbody内容为空时，tbody的内容。
                                //下面三者构成了总体的左下角的内容。
                                info : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//左下角的信息显示，大写的词为关键字。
                                infoEmpty : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//筛选为空时左下角的显示。
                                infoFiltered : ""//筛选之后的左下角筛选提示(另一个是分页信息显示，在上面的info中已经设置，所以可以不显示)，
                            },
                            searching:false,
                            ordering:false,
                            "paging" : false,
                            "info": true,
                            "destroy":true,
                            // pageLength : 50,// 每页显示10条数据
                            pagingType : "full_numbers", // 分页样式：simple,simple_numbers,full,full_numbers，
                            scrollX: 1500,
                            scrollY: 650,
                            fixedHeader: true,
                            scrollCollapse: true,
                            scroller:       true,
                            lengthChange:false
                        });
                    });
            }else if (data.msg){
                swal({
                    type: "error",
                    title: "对不起",
                    text: "<span style=\"font-weight:bolder;font-size: 20px\">"+data.msg+"</span>",
                    html: true
                })
            }else {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，生成失败！</span>",html: true});
            }
        },
        error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }
    })
}

function deleteRow(obj) {
    tailorTable.row($(obj).parents('tr')).remove().draw(false);
}

function addSize(obj) {
    $(obj).parent().parent().after($("div[name='sizeRadioDiv']:last").clone());
    $("input[name='radio']:last").val("");
    $(obj).next().show();
    $(obj).remove();
}

function delNumber(obj) {
    $(obj).parent().parent().remove();

    var orderOfBatch = Number($("#finishCount").text()) + 1;
    var orderOfLayerArr = $('[name="orderOfLayer"]');
    for (var i = 0; i < orderOfLayerArr.length; i++){
        $(orderOfLayerArr[i]).text(i + 1);
        $("input[name='batchOrder']").eq(i).val(orderOfBatch);
        orderOfBatch ++;
    }

    var calWeight = 0;
    $("input[name='weight']").each(function (index,item) {
        var tmpWeight = Number($(item).val());
        calWeight += tmpWeight;
    });
    $("span[name='totalWeight']").text(calWeight);
}

function moveCursor(event,obj) {
    if(event.keyCode==38){
        var name = $(obj).attr("name");
        var index = $("input[name='"+name+"']").index(obj);
        $("input[name='"+name+"']").eq(index-1).focus();
    }else if(event.keyCode==40){
        var name = $(obj).attr("name");
        var index = $("input[name='"+name+"']").index(obj);
        $("input[name='"+name+"']").eq(index+1).focus();
    }
}

function addNumber(obj) {
    if ($("#locationCount").text() == ""){
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">无剩余卷数,无法继续输入！</span>",html: true});
        return false;
    }

    if ($('[name="orderOfLayer"]').length >= Number($("#locationCount").text())){
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">无剩余卷数,无法继续输入！</span>",html: true});
        return false;
    }
    $("#addNumberDiv").append($("div[name='fabricInfoDiv']:last").clone());

    var orderOfBatch = Number($("#finishCount").text()) + 1;
    var orderOfLayerArr = $('[name="orderOfLayer"]');
    for (var i = 0; i < orderOfLayerArr.length; i++){
        $(orderOfLayerArr[i]).text(i + 1);
        $("input[name='batchOrder']").eq(i).val(orderOfBatch);
        orderOfBatch ++;
    }
    $("input[name='batchNumber']").eq(orderOfLayerArr.length - 1).val(orderOfBatch - 1);
    $("input[name='weight']:last").val("");
    $("span[name='totalWeight']").parent().hide();
    $("span[name='totalWeight']:last").parent().show();
    $("button[name='addNumber']:last").hide();
    $("button[name='addNumber']:last").next().show();
}

function totalWeight(obj) {
    var calWeight = 0;
    $("input[name='weight']").each(function (index,item) {
        var tmp = Number($(item).val());
        calWeight += tmp;
    });

    $("span[name='totalWeight']").text(calWeight);
}

function delSize(obj) {
    $(obj).parent().parent().remove();
    $("span[name='totalWeight']:last").parent().show();

}


function PrefixZero(num, n) {
    return (Array(n).join(0) + num).slice(-n);
}

function transDate(data) {return  moment(data).format("YYYY-MM-DD HH:mm:ss");}

function toDecimal(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*100)/100;
    return f;
}

function toDecimalThree(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*1000)/1000;
    return f;
}

function toDecimalFour(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*10000)/10000;
    return f;
}