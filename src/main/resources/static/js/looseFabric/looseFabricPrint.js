var basePath=$("#basePath").val();
layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.21'
}).extend();
$(document).ready(function () {

    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumber", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });

    $("#orderName").bind('blur change',function(){
        var orderName = $("#orderName").val();
        $("#colorName").empty();
        $.ajax({
            url: "/erp/getloosefabriccolornamehint",
            data: {"orderName": orderName},
            success:function(data){
                $("#colorName").empty();
                if (data.colorNameList) {
                    $("#colorName").append("<option value=''>选择颜色</option>");
                    $.each(data.colorNameList, function(index,element){
                        $("#colorName").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });
    });

    $("#clothesVersionNumber").bind('blur change',function(){
        var orderName = $("#orderName").val();
        $("#colorName").empty();
        $.ajax({
            url: "/erp/getloosefabriccolornamehint",
            data: {"orderName": orderName},
            success:function(data){
                $("#colorName").empty();
                if (data.colorNameList) {
                    $("#colorName").append("<option value=''>选择颜色</option>");
                    $.each(data.colorNameList, function(index,element){
                        $("#colorName").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });
    });

    $("#colorName").change(function () {
        var orderName = $("#orderName").val();
        var colorName = $("#colorName").val();
        $("#fabricName").empty();
        $.ajax({
            url: "/erp/getloosefabricfabricnamehint",
            data: {
                "orderName": orderName,
                "colorName": colorName
            },
            success:function(data){
                $("#fabricName").empty();
                if (data.fabricNameList) {
                    $("#fabricName").append("<option value=''>选择面料</option>");
                    $.each(data.fabricNameList, function(index,element){
                        $("#fabricName").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });
    });

    $("#fabricName").change(function () {
        var orderName = $("#orderName").val();
        var colorName = $("#colorName").val();
        var fabricName = $(this).val();
        $("#fabricColor").empty();
        $.ajax({
            url: "/erp/getloosefabriccolorhint",
            data: {
                "orderName": orderName,
                "colorName": colorName,
                "fabricName": fabricName
            },
            success:function(data){
                $("#fabricColor").empty();
                if (data.fabricColorList) {
                    $("#fabricColor").append("<option value=''>选择颜色</option>");
                    $.each(data.fabricColorList, function(index,element){
                        $("#fabricColor").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });

    });

});

function search() {
    if($("#orderName").val().trim()=="") {
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入要查询的订单号！</span>",html: true});
        return false;
    }
    $.ajax({
        url: basePath + "erp/searchloosefabricinfo",
        type:'GET',
        data: {
            orderName:$("#orderName").val(),
            colorName:$("#colorName").val(),
            fabricName:$("#fabricName").val(),
            fabricColor:$("#fabricColor").val()
        },
        success: function (data) {
            if(data) {
                createTailorInfoTable(data.looseFabricList);
            }else {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，没有查到相关信息！</span>",html: true});
            }
        },
        error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }
    })

}

var tailorInfoTable;

function createTailorInfoTable(data) {
    if (tailorInfoTable != undefined) {
        tailorInfoTable.clear(); //清空一下table
        tailorInfoTable.destroy(); //还原初始化了的datatable
    }
    tailorInfoTable = $('#tailorInfoTable').DataTable({
        "data":data,
        language : {
            processing : "载入中",//处理页面数据的时候的显示
            paginate : {//分页的样式文本内容。
                previous : "上一页",
                next : "下一页",
                first : "第一页",
                last : "最后一页"
            },
            search:"搜索：",
            zeroRecords : "没有内容",//table tbody内容为空时，tbody的内容。
            //下面三者构成了总体的左下角的内容。
            info : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//左下角的信息显示，大写的词为关键字。
            infoEmpty : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//筛选为空时左下角的显示。
            infoFiltered : ""//筛选之后的左下角筛选提示(另一个是分页信息显示，在上面的info中已经设置，所以可以不显示)，
        },
        pageLength : 50,// 每页显示10条数据
        pagingType : "full_numbers", // 分页样式：simple,simple_numbers,full,full_numbers，
        "paging": true,
        "info": true,
        searching:true,
        ordering:true,
        lengthChange:false,
        scrollX: 1500,
        scrollY: $(document.body).height() - 270,
        fixedHeader: true,
        scrollCollapse: true,
        scroller:       true,
        "columns": [
            {
                "data": "looseFabricID",
                "title":"<input type=\"checkbox\" onclick=\"checkAll(this)\">",
                "width":"20px",
                "defaultContent": "",
                "sClass": "text-center",
                "orderable":false,
                render: function (data, type, row, meta) {
                    return "<input type='checkbox'>";
                }
            },
            {
                "data": null,
                "title":"序号",
                "width":"40px",
                "defaultContent": "",
                "sClass": "text-center",
                render: function (data, type, row, meta) {
                    var no = meta.settings._iDisplayStart + meta.row + 1;
                    return no;
                }
            },{
                "data": "orderName",
                "title":"订单号",
                "width":"120px",
                "defaultContent": "",
                "sClass": "text-center",
            },{
                "data": "clothesVersionNumber",
                "title":"版单号",
                "width":"120px",
                "defaultContent": "",
                "sClass": "text-center",
            },{
                "data": "colorName",
                "title":"订单颜色",
                "width":"80px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "fabricName",
                "title":"面料",
                "width":"220px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "fabricColor",
                "title":"面料颜色",
                "width":"90px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "jarNumber",
                "title":"缸号",
                "width":"45px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "batchOrder",
                "title":"卷次",
                "width":"45px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "batchNumber",
                "title":"卷号",
                "width":"45px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "totalBatch",
                "title":"总卷数",
                "width":"45px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "weight",
                "title":"重量",
                "width":"45px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "unit",
                "title":"单位",
                "width":"45px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "looseTime",
                "title":"开始时间",
                "width":"80px",
                "defaultContent": "",
                "sClass": "text-center",
                "render": function(data, type, full, meta) {
                    return  moment(data).format("YYYY-MM-DD HH:mm:ss");
                }

            }, {
                "data": "looseHour",
                "title":"松布时长",
                "width":"80px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "operateType",
                "title":"出库类型",
                "width":"80px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "qCodeID",
                "title":"二维码",
                "width":"80px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "printTimes",
                "title":"打印次数",
                "width":"80px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "fromLocation",
                "title":"位置",
                "width":"80px",
                "defaultContent": "",
                "sClass": "text-center",
            },{
                "data": "looseFabricID",
                "title":"操作",
                "width":"40px",
                "defaultContent": "",
                "sClass": "text-center",
            }
        ],
        "columnDefs" :
            [{
                "orderable" : false, // 禁用排序
                "targets" : [19], // 指定的列
                "data" : "looseFabricID",
                "width":"40px",
                "render" : function(data, type, full, meta) {
                    return "<a href='#' style='color:#3e8eea' onclick='updateTailor(this)' id='"+data+"'>改数</a>";
                }
            }]
    });
}


function checkAll(obj) {
    if($(obj).is(':checked')) {
        $("#tailorInfoTable tbody input[type='checkbox']").prop("checked",true);
    }else {
        $("#tailorInfoTable tbody input[type='checkbox']").prop("checked",false);
    }
}

var LODOP;
function printer() {
    var looseFabricList = [];
    var looseFabricList2 = [];
    $("#tailorInfoTable tbody input[type='checkbox']:checked").each(function () {
        var looseFabric = {};
        var looseFabric2 = {};
        looseFabric.orderName = $(this).parent().parent().find("td").eq(2).text();
        looseFabric.clothesVersionNumber = $(this).parent().parent().find("td").eq(3).text();
        looseFabric.colorName = $(this).parent().parent().find("td").eq(4).text();
        looseFabric.fabricName = $(this).parent().parent().find("td").eq(5).text();
        looseFabric.fabricColor = $(this).parent().parent().find("td").eq(6).text();
        looseFabric.jarNumber = $(this).parent().parent().find("td").eq(7).text();
        looseFabric.batchOrder = $(this).parent().parent().find("td").eq(8).text();
        looseFabric.batchNumber = $(this).parent().parent().find("td").eq(9).text();
        looseFabric.totalBatch = $(this).parent().parent().find("td").eq(10).text();
        looseFabric.weight = $(this).parent().parent().find("td").eq(11).text();
        looseFabric.unit = $(this).parent().parent().find("td").eq(12).text();
        looseFabric.looseTime = $(this).parent().parent().find("td").eq(13).text();
        looseFabric.looseHour = $(this).parent().parent().find("td").eq(14).text();
        looseFabric.operateType = $(this).parent().parent().find("td").eq(15).text();
        looseFabric.qCodeID = $(this).parent().parent().find("td").eq(16).text();
        looseFabric.fromLocation = $(this).parent().parent().find("td").eq(18).text();
        looseFabricList.push(looseFabric);

        looseFabric2.looseFabricID = $(this).parent().parent().find("a").eq(0).attr("id");
        looseFabric2.weight = $(this).parent().parent().find("td").eq(11).text();
        looseFabric2.printTimes = Number($(this).parent().parent().find("td").eq(17).text()) + 1;
        looseFabricList2.push(looseFabric2);
    });
    if(looseFabricList.length<1){
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请选择要打印的信息！</span>",html: true});
    }else{

        $.ajax({
            url: "/erp/updateloosefabricinprint",
            type:'POST',
            data: {
                looseFabricJson: JSON.stringify(looseFabricList2)
            },
            success:function(data){
                if(data == 0) {
                    swal({type:"success",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">打印启动成功！</span>",html: true});
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，保存扎号信息失败！</span>",html: true});
                }
            },
            error:function(){
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务器发生了未知错误～！</span>",html: true});
            }
        });
        LODOP=getLodop();
        for(var i=0;i<looseFabricList.length;i++) {
            var beginTime = getEndTime(transDate(looseFabricList[i].looseTime), 0);
            var endTime = getEndTime(transDate(looseFabricList[i].looseTime), looseFabricList[i].looseHour);
            LODOP.PRINT_INIT("");
            LODOP.SET_PRINT_PAGESIZE(1,"60mm","79.5mm","");
            LODOP.SET_PRINT_STYLE("FontSize",10);
            LODOP.SET_PRINT_STYLEA("FontName","楷体");
            CreateOnePage(looseFabricList[i].qCodeID,looseFabricList[i].clothesVersionNumber,looseFabricList[i].orderName,looseFabricList[i].jarNumber,looseFabricList[i].batchNumber,looseFabricList[i].totalBatch,looseFabricList[i].weight,beginTime,looseFabricList[i].looseHour, endTime,looseFabricList[i].colorName, looseFabricList[i].fabricColor, looseFabricList[i].fromLocation, looseFabricList[i].fabricName);
            LODOP.SET_PRINT_MODE("CUSTOM_TASK_NAME","票菲"+i);//为每个打印单独设置任务名
            LODOP.PRINT();
            // LODOP.PREVIEW();
        }

    }
}

function PrefixZero(num, n) {
    return (Array(n).join(0) + num).slice(-n);
}

function updateTailor(obj) {
    $("#initCount1").val($(obj).parent().parent().find("td").eq(9).text());
    $("#updateCount1").val("");
    $.blockUI({
        css: {
            width: '40%',
            top: '15%',
            left: '26%',
            border: 'none',
            padding: '15px',
            backgroundColor: '#fff',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: 1,
            color: '#000'
        },
        message: $('#updateCount')
    });

    $("#updateCountYes").unbind("click").bind("click", function () {
        var flag = false;
        $("#updateCount input").each(function () {
            if($(this).val().trim() == "") {
                flag = true;
                return false;
            }
        });
        if(flag) {
            swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请填写完所有字段信息！</span>",html: true});
            return false;
        }
        $(obj).parent().parent().find("td").eq(9).text($("#updateCount1").val().trim());
        $.unblockUI();
        $("#updateCount input").val("");

    });
    $("#updateCountNo").unbind("click").bind("click", function () {
        $.unblockUI();
        $("#updateCount input").val("");
    });
}


function getEndTime(startTime, hour){
    var nd = new Date(Date.parse(startTime.replace(/-/g, "/"))); //改为标准格式：2016/04/05 09:29:15
    nd = nd.valueOf();
    nd = nd +  hour * 60 * 60 * 1000;
    nd = new Date(nd);
    var y = nd.getFullYear();
    var m = nd.getMonth()+1;
    var d = nd.getDate();
    var hh = nd.getHours();
    var mm = nd.getMinutes();
    var ss = nd.getSeconds();
    if(m <= 9) m = "0"+m;
    if(d <= 9) d = "0"+d;
    if(hh<= 9) hh = "0"+hh;
    if(mm<= 9) mm = "0"+mm;
    if(ss<= 9) ss = "0"+ss;
    return y+"-"+m+"-"+d+" "+hh+":"+mm;
}

function CreateOnePage(qCodeID, clothesVersionNumber, orderName, jarNumber, batchNumber, totalBatch, weight, beginTime, looseHour, endTime, colorName, fabricColor, fromLocation, fabricName){
    LODOP.NewPage();
    // LODOP.ADD_PRINT_IMAGE("-2mm","2mm","60mm","20mm","<img border='0' src='/images/deyoo.png'>");
    // LODOP.SET_PRINT_STYLEA(0,"Stretch",2);//(可变形)扩展缩放模式
    LODOP.ADD_PRINT_TEXTA("text00","2mm","1mm","45mm","5mm","德悦服饰松布卡");
    LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
    LODOP.ADD_PRINT_BARCODE("8mm","12mm","20mm","20mm","QRCode",qCodeID);
    LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
    LODOP.ADD_PRINT_TEXTA("text01","26mm","1mm","45mm","3mm",qCodeID);
    LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
    LODOP.ADD_PRINT_TEXTA("text02","30mm","1mm","45mm","3mm",orderName);
    LODOP.SET_PRINT_STYLEA(0,"Alignment",1);
    LODOP.ADD_PRINT_TEXTA("text03","35mm","1mm","45mm","3mm",jarNumber+"缸");
    LODOP.SET_PRINT_STYLEA(0,"Alignment",1);
    LODOP.SET_PRINT_STYLE("Bold",1);
    LODOP.ADD_PRINT_TEXTA("text04","39mm","1mm","45mm","3mm",batchNumber+"("+totalBatch+")卷" + "---重量:"+weight);
    LODOP.SET_PRINT_STYLEA(0,"Alignment",1);
    LODOP.SET_PRINT_STYLE("Bold",1);
    LODOP.ADD_PRINT_TEXTA("text06","43mm","0mm","50mm","3mm",fabricColor+"("+colorName+")");
    LODOP.SET_PRINT_STYLEA(0,"Alignment",1);
    LODOP.ADD_PRINT_TEXTA("text07","47mm","0mm","50mm","3mm","开始:" + beginTime);
    LODOP.SET_PRINT_STYLEA(0,"Alignment",1);
    LODOP.ADD_PRINT_TEXTA("text08","51mm","0mm","50mm","3mm","松布:" + looseHour + "小时");
    LODOP.SET_PRINT_STYLEA(0,"Alignment",1);
    LODOP.ADD_PRINT_TEXTA("text09","55mm","0mm","50mm","3mm","结束:" + endTime);
    LODOP.SET_PRINT_STYLEA(0,"Alignment",1);
    LODOP.ADD_PRINT_TEXTA("text10","59mm","0mm","50mm","3mm",fromLocation);
    LODOP.SET_PRINT_STYLEA(0,"Alignment",1);
    LODOP.ADD_PRINT_TEXTA("text11","63mm","0mm","50mm","3mm",fabricName);
    LODOP.SET_PRINT_STYLEA(0,"Alignment",1);
    LODOP.SET_PRINT_STYLE("Bold",1);
}



function transDate(data, type, full, meta) {
    return moment(data).format("YYYY-MM-DD HH:mm:ss");
}