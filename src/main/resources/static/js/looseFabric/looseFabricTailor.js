var basePath=$("#basePath").val();
$(document).ready(function () {
    // createOrderTable();
    $('#mainFrameTabs').bTabs();

    $('#orderListTab').click(function(){
        createOrderTable();
    });
});
function createOrderTable() {

}

function checkAll(obj) {
    if($(obj).is(':checked')) {
        $("#tailorTable tbody input[type='checkbox']").prop("checked",true);
    }else {
        $("#tailorTable tbody input[type='checkbox']").prop("checked",false);
    }
}

function calcHeight(){
    var browserHeight = $(window).innerHeight();
    var topHeight = $('#mainFrameHeadBar').outerHeight(true);
    var tabMarginTop = parseInt($('#mainFrameTabs').css('margin-top'));//获取间距
    var tabHeadHeight = $('ul.nav-tabs',$('#mainFrameTabs')).outerHeight(true) + tabMarginTop;
    var contentMarginTop = parseInt($('div.tab-content',$('#mainFrameTabs')).css('margin-top'));//获取内容区间距
    var contentHeight = browserHeight - topHeight - tabHeadHeight - contentMarginTop;
    $('div.tab-content',$('#mainFrameTabs')).height(contentHeight);
}


var userRole=$("#userRole").val();

var tabId = 1;
function addLooseFabric() {
    if(userRole!='root' && userRole!='role11'){
        swal("SORRY!", "对不起，您没有操作权限！", "warning");
        return;
    }
    $('#mainFrameTabs').bTabsAdd("tabId" + tabId, "松布信息录入", "/erp/addLooseFabricStart");
    tabId++;
    // calcHeight();
}

var LODOP;

function printer() {
    var looseFabricList = [];
    $("#tailorBody input[type='checkbox']:checked").each(function () {
        var looseFabric = {};
        looseFabric.orderName = $(this).parent().parent().find("td").eq(2).text();
        looseFabric.clothesVersionNumber = $(this).parent().parent().find("td").eq(3).text();
        looseFabric.fabricName = $(this).parent().parent().find("td").eq(4).text();
        looseFabric.fabricColor = $(this).parent().parent().find("td").eq(5).text();
        looseFabric.jarNumber = $(this).parent().parent().find("td").eq(6).text();
        looseFabric.batchOrder = $(this).parent().parent().find("td").eq(7).text();
        looseFabric.batchNumber = $(this).parent().parent().find("td").eq(8).text();
        looseFabric.totalBatch = $(this).parent().parent().find("td").eq(9).text();
        looseFabric.weight = $(this).parent().parent().find("td").eq(10).text();
        looseFabric.unit = $(this).parent().parent().find("td").eq(11).text();
        looseFabric.colorName = $(this).parent().parent().find("td").eq(12).text();
        looseFabric.looseTime = $(this).parent().parent().find("td").eq(13).text();
        looseFabric.looseHour = $(this).parent().parent().find("td").eq(14).text();
        looseFabric.operateType = $(this).parent().parent().find("td").eq(15).text();
        looseFabric.fromLocation = $(this).parent().parent().find("td").eq(16).text();
        looseFabric.qCodeID = $(this).parent().parent().find("td").eq(17).text();
        looseFabricList.push(looseFabric);
    });
    if(looseFabricList.length<1){
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请选择要打印的信息！</span>",html: true});
    }else{
        $.ajax({
            url: "/erp/saveloosefabricdata",
            type: 'POST',
            data: {
                looseFabricJson: JSON.stringify(looseFabricList)
            },
            success:function(data){
                if(data == 0) {
                    swal({type:"success",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">打印启动成功！</span>",html: true},function (){
                        $("#saveButton").hide();
                    });
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，保存扎号信息失败！</span>",html: true});
                }
            },
            error:function(){
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务器发生了未知错误～！</span>",html: true});
            }
        });
        LODOP=getLodop();
        for (var i=0;i<looseFabricList.length;i++){
            var beginTime = getEndTime(transDate(looseFabricList[i].looseTime), 0);
            var endTime = getEndTime(transDate(looseFabricList[i].looseTime), looseFabricList[i].looseHour);
            LODOP.PRINT_INIT("");
            LODOP.SET_PRINT_PAGESIZE(1,"60mm","79.5mm","");
            LODOP.SET_PRINT_STYLE("FontSize",10);
            LODOP.SET_PRINT_STYLEA("FontName","楷体");
            CreateOnePage(looseFabricList[i].qCodeID,looseFabricList[i].clothesVersionNumber,looseFabricList[i].orderName,looseFabricList[i].jarNumber,looseFabricList[i].batchNumber,looseFabricList[i].totalBatch,looseFabricList[i].weight,beginTime,looseFabricList[i].looseHour,endTime,looseFabricList[i].colorName,looseFabricList[i].fabricColor,looseFabricList[i].fromLocation,looseFabricList[i].fabricName);
            LODOP.SET_PRINT_MODE("CUSTOM_TASK_NAME","票菲"+i);//为每个打印单独设置任务名
            LODOP.PRINT();
        }
    }
}



function showQrCode(obj) {
    $("#printOrderName").text($(obj).parent().parent().find("td").eq(2).text());
    $("#printClothesVersionNumber").text($(obj).parent().parent().find("td").eq(3).text());
    $("#printFabricName").text($(obj).parent().parent().find("td").eq(4).text());
    $("#printFabricColor").text($(obj).parent().parent().find("td").eq(5).text());
    $("#printJarNumber").text($(obj).parent().parent().find("td").eq(6).text());
    $("#printBatchOrder").text($(obj).parent().parent().find("td").eq(7).text());
    $("#printColorName").text($(obj).parent().parent().find("td").eq(9).text());
    $("#printLooseTime").text($(obj).parent().parent().find("td").eq(10).text());
    $("#printHour").text($(obj).parent().parent().find("td").eq(11).text());
    $("#printOperateType").text($(obj).parent().parent().find("td").eq(12).text());
    var fabricQcodeID = $(obj).parent().parent().find("td").eq(13).text();
    $.blockUI({
        css: {
            width: '40%',
            top: '10%',
            left:'20%',
            border: 'none',
            padding: '15px',
            backgroundColor: '#fff',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: 1,
            color: '#000'
        },
        message: $('#qrCodeWin')
    });
    // $('#qrcode').qrcode(storehouseLocation);
    var qrcode = new QRCode(document.getElementById("qrCode"), {
        width : 70,
        height : 70,
        correctLevel:1
    });
    qrcode.makeCode(fabricQcodeID);
    $("#closeQrCodeWin").unbind("click").bind("click", function () {
        $.unblockUI();
        $('#qrCode').empty();
    });
}


function delTailor(obj) {
    swal({
        title: "",
        text: "<span style=\"font-weight:bolder;font-size: 20px\">您确定要删除该信息吗？</span>",
        type: "warning",
        html:true,
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: "确定",
        cancelButtonText:"我再想想",
        confirmButtonColor: "#ec6c62",
        showLoaderOnConfirm: false
    }, function() {
        $("#mainFrameTabs iframe:last")[0].contentWindow.deleteRow(obj);
        swal({
            type:"success",
            title:"",
            text: "<span style=\"font-weight:bolder;font-size: 20px\">恭喜你，删除成功！</span>",
            html: true
        });
    });

}


function transDate(data, type, full, meta) {
    return  moment(data).format("YYYY-MM-DD HH:MM:SS");
}


function getEndTime(startTime, hour){
    var nd = new Date(Date.parse(startTime.replace(/-/g, "/"))); //改为标准格式：2016/04/05 09:29:15
    nd = nd.valueOf();
    nd = nd +  hour * 60 * 60 * 1000;
    nd = new Date(nd);
    var y = nd.getFullYear();
    var m = nd.getMonth()+1;
    var d = nd.getDate();
    var hh = nd.getHours();
    var mm = nd.getMinutes();
    var ss = nd.getSeconds();
    if(m <= 9) m = "0"+m;
    if(d <= 9) d = "0"+d;
    if(hh<= 9) hh = "0"+hh;
    if(mm<= 9) mm = "0"+mm;
    if(ss<= 9) ss = "0"+ss;
    return y+"-"+m+"-"+d+" "+hh+":"+mm;
}

function CreateOnePage(qCodeID, clothesVersionNumber, orderName, jarNumber, batchNumber, totalBatch, weight, beginTime, looseHour, endTime, colorName, fabricColor, fromLocation, fabricName){
    LODOP.NewPage();
    // LODOP.ADD_PRINT_IMAGE("-2mm","2mm","60mm","20mm","<img border='0' src='/images/deyoo.png'>");
    // LODOP.SET_PRINT_STYLEA(0,"Stretch",2);//(可变形)扩展缩放模式
    LODOP.ADD_PRINT_TEXTA("text00","2mm","1mm","45mm","5mm","德悦服饰松布卡");
    LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
    LODOP.ADD_PRINT_BARCODE("8mm","12mm","20mm","20mm","QRCode",qCodeID);
    LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
    LODOP.ADD_PRINT_TEXTA("text01","26mm","1mm","45mm","3mm",qCodeID);
    LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
    LODOP.ADD_PRINT_TEXTA("text02","30mm","1mm","45mm","3mm",orderName);
    LODOP.SET_PRINT_STYLEA(0,"Alignment",1);
    LODOP.ADD_PRINT_TEXTA("text03","35mm","1mm","45mm","3mm",jarNumber+"缸");
    LODOP.SET_PRINT_STYLEA(0,"Alignment",1);
    LODOP.SET_PRINT_STYLE("Bold",1);
    LODOP.ADD_PRINT_TEXTA("text04","39mm","1mm","45mm","3mm",batchNumber+"("+totalBatch+")卷" + "---重量:"+weight);
    LODOP.SET_PRINT_STYLEA(0,"Alignment",1);
    LODOP.SET_PRINT_STYLE("Bold",1);
    LODOP.ADD_PRINT_TEXTA("text06","43mm","0mm","50mm","3mm",fabricColor+"("+colorName+")");
    LODOP.SET_PRINT_STYLEA(0,"Alignment",1);
    LODOP.ADD_PRINT_TEXTA("text07","47mm","0mm","50mm","3mm","开始:" + beginTime);
    LODOP.SET_PRINT_STYLEA(0,"Alignment",1);
    LODOP.ADD_PRINT_TEXTA("text08","51mm","0mm","50mm","3mm","松布:" + looseHour + "小时");
    LODOP.SET_PRINT_STYLEA(0,"Alignment",1);
    LODOP.ADD_PRINT_TEXTA("text09","55mm","0mm","50mm","3mm","结束:" + endTime);
    LODOP.SET_PRINT_STYLEA(0,"Alignment",1);
    LODOP.ADD_PRINT_TEXTA("text10","59mm","0mm","50mm","3mm",fromLocation);
    LODOP.SET_PRINT_STYLEA(0,"Alignment",1);
    LODOP.ADD_PRINT_TEXTA("text11","63mm","0mm","50mm","3mm",fabricName);
    LODOP.SET_PRINT_STYLEA(0,"Alignment",1);
    LODOP.SET_PRINT_STYLE("Bold",1);
}