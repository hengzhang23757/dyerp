var form;
layui.use('form', function(){
    form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
    form.render();
});
layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.21'
}).extend({                         // 模块别名
    soulTable: 'soulTable'
});
$(document).on('keydown', '.layui-table-edit', function (e) {
    var td = $(this).parent('td')
        ,tr = td.parent('tr')
        ,trs = tr.parent().parent().find('tr')
        ,tr_index = tr.index()
        ,td_index = td.index()
        ,td_last_index = tr.find('[data-edit="text"]:last').index()
        ,td_first_index = tr.find('[data-edit="text"]:first').index();
    switch (e.keyCode) {
        case 13:
            tr.next().find('td').eq(td_index).click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            return false;
        case 39:
            td.nextAll('[data-edit="text"]:first').click();
            if (td_index == td_last_index){
                tr.next().find('td').eq(td_first_index).click();
                if (tr_index == trs.length - 1){
                    trs.eq(0).find('td').eq(td_first_index).click();
                }
            }
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
        case 37:
            td.prevAll('[data-edit="text"]:first').click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
        case 38:
            tr.prev().find('td').eq(td_index).click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
        case 40:
            tr.next().find('td').eq(td_index).click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
    }

})
$(document).ready(function () {

    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumber", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });

    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumberTwo", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumberTwo',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderNameTwo',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumberTwo',
                    field: 'clothesVersionNumberTwo'
                }, {
                    name: 'orderNameTwo',
                    field: 'orderNameTwo'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderNameTwo", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumberTwo',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderNameTwo',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumberTwo',
                    field: 'clothesVersionNumberTwo'
                }, {
                    name: 'orderNameTwo',
                    field: 'orderNameTwo'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });

});
var dataTable;
layui.use(['form', 'soulTable', 'table', 'laydate'], function () {
    var table = layui.table,
        soulTable = layui.soulTable,
        laydate = layui.laydate,
        form = layui.form,
        $ = layui.$;
    dataTable = table.render({
        elem: '#dataTable'
        ,excel: {
            filename: '暂存面料.xlsx'
        }
        ,height: 'full-100'
        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
        ,cols: [[]]
        ,loading:true
        ,totalRow: true
        ,page: true
        ,even: true
        ,overflow: 'tips'
        ,limits: [50, 100, 200]
        ,limit: 100 //每页默认显示的数量
        ,done: function (res, curr, count) {
            soulTable.render(this);
        }
    });
    initTable();
    function initTable(orderName){
        var param = {};
        if (orderName != null && orderName != ''){
            param.orderName = orderName;
        }
        var load = layer.load();
        $.ajax({
            url: "/erp/getalltmpfabric",
            type: 'GET',
            data: param,
            success: function (res) {
                if (res.data) {
                    var reportData = res.data;
                    table.render({
                        elem: '#dataTable'
                        ,cols:[[
                            {type:'numbers', align:'center', title:'序号'}
                            ,{field:'clothesVersionNumber', title:'单号', align:'center', width:150, sort: true, filter: true, totalRowText: '合计'}
                            ,{field:'orderName', title:'款号', align:'center', width:150, sort: true, filter: true}
                            ,{field:'fabricName', title:'面料名称', align:'center', width:200, sort: true, filter: true}
                            ,{field:'fabricNumber', title:'面料号', align:'center', width:200, sort: true, filter: true}
                            ,{field:'unit', title:'单位', align:'center', width:80, sort: true, filter: true}
                            ,{field:'colorName', title:'色组', align:'center', width:90, sort: true, filter: true}
                            ,{field:'fabricColor', title:'面料颜色', align:'center', width:100, sort: true, filter: true}
                            ,{field:'fabricColorNumber', title:'面料色号', align:'center', width:100, sort: true, filter: true}
                            ,{field: 'batchNumber', title: '卷数',align:'center', minWidth: 100, totalRow: true}
                            ,{field: 'weight', title: '数量',align:'center', minWidth: 100, totalRow: true}
                            ,{field: 'location', title: '位置',align:'center', minWidth: 100}
                            ,{field: 'returnTime', title: '入库时间',align:'center', minWidth: 120,templet:function (d) {
                                    return moment(d.returnTime).format("YYYY-MM-DD");
                                }}
                            ,{field: 'supplier', title: '供应商',align:'center', minWidth: 100}
                            ,{field: 'fabricDetailID', align:'center', hide:true}
                            ,{field: 'fabricID', align:'center', hide:true}
                            ,{title: '操作', minWidth: 250,align:'center', toolbar: '#barTop', fixed: 'right'}
                        ]]
                        ,excel: {
                            filename: '暂存面料.xlsx'
                        }
                        ,data: reportData
                        ,height: 'full-100'
                        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                        ,title: '暂存面料'
                        ,totalRow: true
                        ,loading: false
                        ,page: true
                        ,overflow: 'tips'
                        ,even: true
                        ,limits: [50, 100, 200]
                        ,limit: 100 //每页默认显示的数量
                        ,done: function () {
                            soulTable.render(this);
                            layer.close(load);
                        }
                    });

                } else {
                    layer.msg("获取失败！", {icon: 2});
                }
            }, error: function () {
                layer.msg("获取失败！", {icon: 2});
            }
        });
    }
    form.on('submit(searchBeat)', function(data){
        var orderName = $("#orderName").val();
        initTable(orderName);
        return false;
    });
    table.on('toolbar(dataTable)', function(obj) {
        var filterOrder = $("#orderName").val();
        if (obj.event === 'clearFilter') {
            // 清除所有筛选条件并重载表格
            // 参数: tableId
            soulTable.clearFilter('dataTable');
        } else if (obj.event === 'exportExcel') {
            soulTable.export('dataTable');
        }
    });

    table.on('tool(dataTable)', function(obj){
        var data = obj.data;
        if(obj.event === 'return'){
            layer.confirm('确认退回?', function(index){
                $.ajax({
                    url: "/erp/deletetmpfabricbyid",
                    type: 'POST',
                    data: {
                        fabricDetailID: data.fabricDetailID
                    },
                    success: function (res) {
                        if (res.result == 0) {
                            initTable('');
                            layer.close(index);
                            layer.msg("提交成功！", {icon: 1});
                        } else {
                            layer.msg("提交失败！", {icon: 2});
                        }
                    },
                    error: function () {
                        layer.msg("提交失败！", {icon: 2});
                    }
                })

            });
        } else if(obj.event === 'thisOrder'){
            var index = layer.open({
                type: 1 //Page层类型
                , title: '本款补料'
                , btn: ['保存','取消']
                , shade: 0.6 //遮罩透明度
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , zIndex:1 //层优先级
                , area: ['300px', '220px']
                , content: "<div><table><tr><td><label class='layui-form-label'>卷数:</label></td><td><input type='text' id='thisBatchNumber' autocomplete='off' class='layui-input'></td></tr><tr><td><label class='layui-form-label'>重量:</label></td><td><input type='text' id='thisWeight' autocomplete='off' class='layui-input'></td></tr></table></div>"
                ,yes: function(index, layero){
                    var params = {};
                    params.batchNumber = $("#thisBatchNumber").val();
                    params.weight = $("#thisWeight").val();
                    params.orderName = data.orderName;
                    params.fabricDetailID = data.fabricDetailID;
                    if (params.batchNumber == null || params.batchNumber == '' || params.weight == null || params.weight == ''){
                        layer.msg("不能为空~~~");
                        return false;
                    }
                    if (params.batchNumber > data.batchNumber || params.weight > data.weight){
                        layer.msg("数量有误~~~");
                        return false;
                    }
                    $.ajax({
                        url: "/erp/tmpfabrictrans",
                        type: 'POST',
                        data: params,
                        success: function (res) {
                            if (res.result == 0) {
                                layer.close(index);
                                layer.msg("保存成功！", {icon: 1});
                                initTable('');
                                $("#thisBatchNumber").val("");
                                $("#thisWeight").val("");
                            } else {
                                layer.msg("保存失败！", {icon: 2});
                            }
                        },
                        error: function () {
                            layer.msg("保存失败！", {icon: 2});
                        }
                    })
                }
                , cancel : function (i,layero) {
                    $("#thisBatchNumber").val("");
                    $("#thisWeight").val("");
                }
            });
            $("#thisBatchNumber").val(data.batchNumber);
            $("#thisWeight").val(data.weight);
        } else if(obj.event === 'otherOrder'){
            var index = layer.open({
                type: 1 //Page层类型
                , title: '面料换款'
                , btn: ['保存']
                , shade: 0.6 //遮罩透明度
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , content: $("#operateDiv")
                ,yes: function(index, layero){
                    var checkStatus = table.checkStatus('fabricChangeTable'); //获取选中行状态
                    if(checkStatus.data.length == 0) {
                        layer.msg('请先选择订单');
                    }else {
                        var changeBatch = $("#changeBatch").val();
                        var changeWeight = $("#changeWeight").val();
                        if (Number(changeWeight) > data.weight || Number(changeBatch) > data.batchNumber){
                            layer.msg('数量有误~');
                            return false;
                        }
                        var toData = checkStatus.data[0];
                        if (toData.fabricName != data.fabricName || toData.fabricColor != data.fabricColor){
                            layer.msg('选择的面料名称和颜色不对应,请确认~~~');
                            return false;
                        }
                        $.ajax({
                            url: "/erp/changetmpfabricorder",
                            type: 'POST',
                            data: {
                                fabricDetailID: data.fabricDetailID,
                                fabricID: toData.fabricID,
                                changeWeight:changeWeight,
                                changeBatch:changeBatch
                            },
                            success: function (res) {
                                if (res.over === 1){
                                    var tipHtml = "本次录入超过了允许接收的上限:<br><div style='font-weight: bolder;font-size: larger;color: orange;margin-bottom: 10px'>订布量:" + toDecimal(res.fabricActCount) + "<br>已入库:" + toDecimal(res.historyWeight) + "<br>允许超数:" + toDecimal(res.overWeight) +  "<br>最多还可入:" + toDecimal(res.availableWeight) + "<br>";
                                    var confirmIndex = layer.confirm(tipHtml, {
                                        title: '超数提醒'
                                        ,btn: ['返回修改'] //按钮
                                    }, function(){
                                        layer.close(confirmIndex);
                                    });
                                } else {
                                    if (res.result == 0) {
                                        layer.close(index);
                                        layer.msg("保存成功！", {icon: 1});
                                        initTable();
                                    } else if (res.result == 3) {
                                        layer.msg("该面料未审核,请联系审核人员审核~~");
                                    } else if (res.result == 4) {
                                        layer.msg("面料库存异常,请联系IT~~");
                                    } else {
                                        layer.msg("提交失败！", {icon: 2});
                                    }
                                }
                            },
                            error: function () {
                                layer.msg("保存失败！", {icon: 2});
                            }
                        })
                    }
                }
                , cancel : function (i,layero) {
                    $("#operateDiv").find("input").val("");
                }
            });
            layer.full(index);
            setTimeout(function () {
                $("#operateDiv input[name='clothesVersionNumber']").val(data.clothesVersionNumber);
                $("#operateDiv input[name='orderName']").val(data.orderName);
                $("#operateDiv input[name='fabricName']").val(data.fabricName);
                $("#operateDiv input[name='fabricNumber']").val(data.fabricNumber);
                $("#operateDiv input[name='supplier']").val(data.supplier);
                $("#operateDiv input[name='colorName']").val(data.colorName);
                $("#operateDiv input[name='fabricColor']").val(data.fabricColor);
                $("#operateDiv input[name='fabricColorNumber']").val(data.fabricColorNumber);
                $("#operateDiv input[name='jarName']").val(data.jarName);
                $("#operateDiv input[name='unit']").val(data.unit);
                $("#operateDiv input[name='batchNumber']").val(data.batchNumber);
                $("#operateDiv input[name='changeBatch']").val(data.batchNumber);
                $("#operateDiv input[name='weight']").val(data.weight);
                $("#operateDiv input[name='changeWeight']").val(data.weight);
                $("#operateDiv input[name='location']").val(data.location);
                $("#operateDiv input[name='returnTime']").val(moment(data.returnTime).format("YYYY-MM-DD"));
                form.on('submit(searchChangeBeat)', function(data){
                    var toOrderName = $("#orderNameTwo").val();
                    if (toOrderName == null || toOrderName == ''){
                        layer.msg("款号必须~~");
                        return false;
                    }
                    $.ajax({
                        url: "/erp/getmanufacturefabricbyorder",
                        type: 'GET',
                        data: {
                            orderName: toOrderName
                        },
                        success: function (res) {
                            if (res.data) {
                                var reportData = res.data;
                                table.render({
                                    elem: '#fabricChangeTable'
                                    ,cols:[[
                                        {type:'radio'},
                                        {field: 'orderName', title: '款号', minWidth: 130, sort: true, filter: true},
                                        {field: 'clothesVersionNumber', title: '单号', minWidth: 130, sort: true, filter: true},
                                        {field: 'fabricName', title: '面料名称', minWidth: 130, sort: true, filter: true},
                                        {field: 'fabricNumber', title: '面料号', minWidth: 130, sort: true, filter: true},
                                        {field: 'supplier', title: '供应商', minWidth: 100, sort: true, filter: true},
                                        {field: 'unit', title: '单位', minWidth: 100, sort: true, filter: true},
                                        {field: 'fabricWidth', title: '布封', minWidth: 165 , filter: true},
                                        {field: 'fabricWeight', title: '克重', minWidth: 123, filter: true},
                                        {field: 'fabricColor', title: '面料颜色', minWidth: 123, filter: true},
                                        {field: 'fabricColorNumber', title: '面料色号', minWidth: 123, filter: true},
                                        {field: 'fabricID', hide: true}
                                    ]]
                                    ,excel: {
                                        filename: '面料表.xlsx'
                                    }
                                    ,data: reportData
                                    ,height: '400'
                                    ,title: '辅料操作表'
                                    ,totalRow: true
                                    ,loading: false
                                    ,page: true
                                    ,even: true
                                    ,overflow: 'tips'
                                    ,limits: [50, 100, 200]
                                    ,limit: 100 //每页默认显示的数量
                                    ,done: function () {
                                        soulTable.render(this);
                                    }
                                });
                            } else {
                                layer.msg("获取失败！", {icon: 2});
                            }
                        }, error: function () {
                            layer.msg("获取失败！", {icon: 2});
                        }
                    });
                    return false;
                });
            });
        }
    })

});
function toDecimal(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*100)/100;
    return f;
}
function isNumber(val){

    var regPos = /^\d+(\.\d+)?$/; //非负浮点数
    var regNeg = /^(-(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*)))$/; //负浮点数
    if (regPos.test(val) || regNeg.test(val)){
        return true;
    }else{
        return false;
    }

}
