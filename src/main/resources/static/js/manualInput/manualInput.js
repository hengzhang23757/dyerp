var basePath=$("#basePath").val();
var userName=$("#userName").val();
var userRole=$("#userRole").val();
var procedureInfoList;
var form;
var procedureDemo;
layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.23'
}).extend();
$(document).ready(function () {
    layui.use('form', function(){
        form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
        form.render();
    });

    layui.laydate.render({
        elem: '#fromDate',
        trigger: 'click'
    });

    layui.laydate.render({
        elem: '#toDate',
        trigger: 'click'
    });

    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumber", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });
    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "employeeNumber", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'employeeNumber',
                    title: '工号',
                    align: 'left'
                }, {
                    field: 'employeeName',
                    title: '姓名',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'employeeNumber',
                    field: 'employeeNumber'
                }, {
                    name: 'employeeName',
                    field: 'employeeName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getemployeeinfohint?keyWord=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "employeeName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'employeeNumber',
                    title: '工号',
                    align: 'left'
                }, {
                    field: 'employeeName',
                    title: '姓名',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'employeeNumber',
                    field: 'employeeNumber'
                }, {
                    name: 'employeeName',
                    field: 'employeeName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getemployeeinfohint?keyWord=" //设置异步数据接口,url为必填项,params为字段名
        });
    });

    $("#employeeNumber").bind('blur change',function(){
        var employeeNumber = $("#employeeNumber").val();
        $.ajax({
            url: "/erp/getemployeebyemployeenumber",
            data: {"employeeNumber": employeeNumber},
            success:function(data){
                $("#groupName").empty();
                if (data.employeeList) {
                    $("#groupName").val(data.employeeList[0].groupName);
                }
            }, error:function(){
            }
        });
    });
    $("#employeeName").bind('blur change',function(){
        var employeeNumber = $("#employeeNumber").val();
        $.ajax({
            url: "/erp/getemployeebyemployeenumber",
            data: {"employeeNumber": employeeNumber},
            success:function(data){
                $("#groupName").empty();
                if (data.employeeList) {
                    $("#groupName").val(data.employeeList[0].groupName);
                }
            }, error:function(){
            }
        });
    });
});

var employeeNumberCheck = '@';
var orderNameCheck = '@';
var procedureNumberCheck = '@';
var createTimeCheck = '@';
var colorNameCheck = '@';
var sizeNameCheck = '@';
var layerCountCheck = 100;

layui.use(['form', 'soulTable', 'table', 'element'], function () {
    var table = layui.table,
        soulTable = layui.soulTable,
        element = layui.element,
        $ = layui.$;
    var load = layer.load();
    reportTable = table.render({
        elem: '#reportTable'
        ,cols:[[]]
        ,loading:true
        ,data:[]
        ,height: 'full-130'
        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
        ,defaultToolbar: ['filter', 'print']
        ,title: '入数'
        ,totalRow: true
        ,page: true
        ,overflow: 'tips'
        ,limits: [50, 100, 200]
        ,limit: 100 //每页默认显示的数量
        ,done: function () {
            soulTable.render(this);
            layer.close(load);
        }
        ,filter: {
            bottom: true,
            items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
        }
    });

    initTable();

    function initTable(){
        var load = layer.load(2);
        $.ajax({
            url: "/erp/gettodaymanualinput",
            type: 'GET',
            data: {},
            success: function (res) {
                if (res.todayManualInputList) {
                    var reportData = res.todayManualInputList;
                    table.render({
                        elem: '#reportTable'
                        ,cols:[[
                            {type:'checkbox'}
                            ,{type:'numbers', align:'center', title:'序号', width:70}
                            ,{field:'updateTime', title:'录入时间', align:'center', width:120, sort: true, filter: true, templet: function (d) {
                                    return  moment(d.updateTime).format("YYYY-MM-DD");
                                }}
                            ,{field:'employeeName', title:'姓名', align:'center', width:120, sort: true, filter: true}
                            ,{field:'employeeNumber', title:'工号', align:'center', width:100, sort: true, filter: true}
                            ,{field:'groupName', title:'组名', align:'center', width:80, sort: true, filter: true}
                            ,{field:'orderName', title:'款号', align:'center', width:120, sort: true, filter: true}
                            ,{field:'clothesVersionNumber', title:'单号', align:'center', width:120, sort: true, filter: true, totalRowText: '合计'}
                            ,{field:'colorName', title:'颜色', align:'center', width:100, sort: true, filter: true}
                            ,{field:'sizeName', title:'尺码', align:'center', width:100, sort: true, filter: true}
                            ,{field:'layerCount', title:'数量', align:'center', width:100, sort: true, filter: true}
                            ,{field:'procedureNumber', title:'工序号', align:'center', width:100, sort: true, filter: true}
                            ,{field:'procedureName', title:'工序名', align:'center', width:150, sort: true, filter: true}
                            ,{field:'createTime', title:'日期', align:'center', width:120, sort: true, filter: true, templet: function (d) {
                                    return  moment(d.createTime).format("YYYY-MM-DD");
                                }}
                            ,{field:'userName', title:'录入人', align:'center', width:120, sort: true, filter: true, totalRow: true}
                            ,{field:'pieceWorkID', title:'操作', align:'center', width:90, toolbar: '#barTop', sort: true, filter: true}
                        ]]
                        ,loading:true
                        ,data: reportData
                        ,height: 'full-130'
                        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                        ,defaultToolbar: ['filter', 'print']
                        ,title: '入数'
                        ,totalRow: true
                        ,page: true
                        ,overflow: 'tips'
                        ,limits: [50, 100, 200]
                        ,limit: 100 //每页默认显示的数量
                        ,done: function () {
                            soulTable.render(this);
                            layer.close(load);
                        }
                        ,filter: {
                            bottom: true,
                            items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                        }
                    });

                } else {
                    layer.msg("获取失败！", {icon: 2});
                }
            }, error: function () {
                layer.msg("获取失败！", {icon: 2});
            }
        });
        return false;
    }

    table.on('toolbar(reportTable)', function(obj) {
        if (obj.event === 'clearFilter') {
            soulTable.clearFilter('reportTable')
        } else if (obj.event === 'refresh') {
            reportTable.reload();
        } else if (obj.event === 'exportExcel') {
            soulTable.export('reportTable');
        } else if (obj.event === 'add'){
            var index = layer.open({
                type: 1 //Page层类型
                , title: '手工入数'
                , btn: ['保存']
                , shade: 0.6 //遮罩透明度
                , area: ['1000px','600px']
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , offset: '100px'
                , content: "<div><table class='layui-form'>" +
                    "                                <tr><td style=\"text-align: right;margin-bottom: 15px;padding-top: 15px\">\n" +
                    "                                    <label class=\"layui-form-label\">工号</label>\n" +
                    "                                </td>\n" +
                    "                                <td style=\"margin-bottom: 15px;padding-top: 15px\">\n" +
                    "                                    <input type=\"text\" style='width: 200px' name=\"employeeNumber\" id=\"employeeNumber\" autocomplete=\"off\" class=\"layui-input\">\n" +
                    "                                </td>" +
                    "                                <td style=\"text-align: right;margin-bottom: 15px;padding-top: 15px\">\n" +
                    "                                    <label class=\"layui-form-label\">姓名</label>\n" +
                    "                                </td>\n" +
                    "                                <td style=\"margin-bottom: 15px;padding-top: 15px\">\n" +
                    "                                    <input type=\"text\" style='width: 200px' name=\"employeeName\" id=\"employeeName\" readonly autocomplete=\"off\" class=\"layui-input\">\n" +
                    "                                </td>" +
                    "                                <td style=\"text-align: right;margin-bottom: 15px;padding-top: 15px\">\n" +
                    "                                    <label class=\"layui-form-label\">组别</label>\n" +
                    "                                </td>\n" +
                    "                                <td style=\"margin-bottom: 15px;padding-top: 15px\">\n" +
                    "                                    <input type=\"text\" name=\"groupName\" id=\"groupName\" autocomplete=\"off\" readonly class=\"layui-input\">\n" +
                    "                                </td></tr>" +
                    "                                <tr><td style=\"text-align: right;margin-bottom: 15px;padding-top: 15px\">\n" +
                    "                                    <label class=\"layui-form-label\">单号</label>\n" +
                    "                                </td>\n" +
                    "                                <td style=\"margin-bottom: 15px;padding-top: 15px\">\n" +
                    "                                    <input type=\"text\" style='width: 200px' name=\"clothesVersionNumber\" id=\"clothesVersionNumberTwo\" autocomplete=\"off\" class=\"layui-input\">\n" +
                    "                                </td>" +
                    "                                <td style=\"text-align: right;margin-bottom: 15px;padding-top: 15px\">\n" +
                    "                                    <label class=\"layui-form-label\">款号</label>\n" +
                    "                                </td>\n" +
                    "                                <td style=\"margin-bottom: 15px;padding-top: 15px\">\n" +
                    "                                    <input type=\"text\" style='width: 200px' name=\"orderName\" id=\"orderNameTwo\" autocomplete=\"off\" class=\"layui-input\">\n" +
                    "                                </td>" +
                    "                                <td style=\"text-align: right;margin-bottom: 15px;padding-top: 15px\">\n" +
                    "                                    <label class=\"layui-form-label\">颜色</label>\n" +
                    "                                </td>\n" +
                    "                                <td style=\"margin-bottom: 15px;padding-top: 15px\">\n" +
                    "                                    <select type=\"text\" name=\"colorName\" id=\"colorName\" autocomplete=\"off\" class=\"layui-input\" lay-filter=\"colorDemo\"></select>\n" +
                    "                                </td></tr>" +
                    "                                <tr><td style=\"text-align: right;margin-bottom: 15px;padding-top: 15px\">\n" +
                    "                                    <label class=\"layui-form-label\">尺码</label>\n" +
                    "                                </td>\n" +
                    "                                <td style=\"margin-bottom: 15px;padding-top: 15px\">\n" +
                    "                                    <select type=\"text\" style='width: 200px' name=\"sizeName\" id=\"sizeName\" autocomplete=\"off\" class=\"layui-input\" lay-filter=\"sizeDemo\"></select>\n" +
                    "                                </td>" +
                    "                                <td style=\"text-align: right;margin-bottom: 15px;padding-top: 15px\">\n" +
                    "                                    <label class=\"layui-form-label\">数量</label>\n" +
                    "                                </td>\n" +
                    "                                <td style=\"margin-bottom: 15px;padding-top: 15px\">\n" +
                    "                                    <input type=\"text\" style='width: 200px' name=\"layerCount\" id=\"layerCount\" autocomplete=\"off\" class=\"layui-input\">\n" +
                    "                                </td>" +
                    "                                <td style=\"text-align: right;margin-bottom: 15px;padding-top: 15px\">\n" +
                    "                                    <label class=\"layui-form-label\">工序</label>\n" +
                    "                                </td>\n" +
                    "                                <td style=\"margin-bottom: 15px;padding-top: 15px\">\n" +
                    "                                    <select type=\"text\" name=\"procedureNumber\" id=\"procedureNumber\" autocomplete=\"off\" class=\"layui-input\" lay-filter=\"procedureDemo\"></select>\n" +
                    "                                </td></tr>" +
                    "                                <tr>" +
                    "                                <td style=\"text-align: right;margin-bottom: 15px;padding-top: 15px\">\n" +
                    "                                    <label class=\"layui-form-label\">日期</label>\n" +
                    "                                </td>\n" +
                    "                                <td style=\"margin-bottom: 15px;padding-top: 15px\">\n" +
                    "                                    <input type=\"text\" style='width: 200px' name=\"pieceDate\" id=\"pieceDate\" autocomplete=\"off\" class=\"layui-input\">\n" +
                    "                                </td>" +
                    "                                <td style=\"text-align: right;margin-bottom: 15px;padding-top: 15px\">\n" +
                    "                                    <label class=\"layui-form-label\">历史</label>\n" +
                    "                                </td>\n" +
                    "                                <td style=\"margin-bottom: 15px;padding-top: 15px\">\n" +
                    "                                    <input type=\"text\" style='width: 200px' name=\"pieceHistory\" id=\"pieceHistory\" autocomplete=\"off\" class=\"layui-input\">\n" +
                    "                                </td>" +
                    "                                </tr>" +
                    "</table></div>"
                ,yes: function(i, layero){
                    if (userRole != 'root' && userRole != 'role5' && userRole != 'role10' && userRole != 'role14' && userRole != 'role12'){
                        layer.msg("您没有权限！");
                        return false;
                    }

                    var employeeNumber = $("#employeeNumber").val();
                    var employeeName = $("#employeeName").val();
                    var groupName = $("#groupName").val();
                    var orderName = $("#orderNameTwo").val();
                    var clothesVersionNumber = $("#clothesVersionNumberTwo").val();
                    var pi = $("#procedureNumber").val();
                    var pNumber = pi.slice(0,pi.indexOf('-'));
                    var pName = pi.slice(pi.indexOf('-')+1);
                    var colorName = $("#colorName").val();
                    var sizeName = $("#sizeName").val();
                    var createTime = $("#pieceDate").val();
                    var layerCount = $("#layerCount").val();
                    if (employeeNumber == null || employeeNumber == '' || employeeName == null || employeeName == '' || groupName == null || groupName == '' || orderName == null || orderName == '' || clothesVersionNumber == null || clothesVersionNumber == '' || pNumber == null || pNumber == '' || createTime == null || createTime == '' || layerCount == null || layerCount == ''){
                        layer.msg("请填写完整信息！");
                        return false;
                    }
                    if (employeeNumber == employeeNumberCheck && orderName == orderNameCheck && pNumber == procedureNumberCheck && createTime == createTimeCheck && colorName == colorNameCheck && sizeName == sizeNameCheck && layerCount == layerCountCheck){
                        layer.confirm('您刚录入过相同数据,是否继续录入?', function(index){
                            $.ajax({
                                url: "/erp/addpieceworktotal",
                                type: 'POST',
                                data: {
                                    employeeNumber: employeeNumber,
                                    employeeName: employeeName,
                                    groupName: groupName,
                                    orderName: orderName,
                                    clothesVersionNumber: clothesVersionNumber,
                                    colorName: colorName,
                                    sizeName: sizeName,
                                    layerCount: layerCount,
                                    procedureNumber: pNumber,
                                    procedureName: pName,
                                    userName:userName,
                                    createTime: createTime
                                },
                                success: function (res) {
                                    if(res == 0) {
                                        layer.msg("录入成功！");
                                    }else if (res == 5){
                                        layer.msg("对不起,数量大于裁数！");
                                    }else if (res == 2){
                                        layer.msg("无裁床记录,请检查颜色尺码是否正确！");
                                    }else if (res == 3){
                                        layer.msg("本次入数导致爆数,请重新核对数量！");
                                    }else if (res == 7){
                                        layer.msg("本次入数导致爆数,整款数量超数！");
                                    }else if (res == 4){
                                        layer.msg("本月工资已经锁定,无法继续录入！");
                                    }else if (res == 10){
                                        layer.msg("款号已经锁定,无法继续录入！");
                                    }else if (res == 44){
                                        layer.msg("该工人没有录入的计件记录,无法减数！");
                                    }else {
                                        layer.msg("保存失败！");
                                    }
                                },
                                error: function () {
                                    layer.msg("保存失败！", {icon: 2});
                                }
                            })

                        });
                    } else {
                        employeeNumberCheck = employeeNumber;
                        orderNameCheck = orderName;
                        procedureNumberCheck = pNumber;
                        createTimeCheck = createTime;
                        colorNameCheck = colorName;
                        sizeNameCheck = sizeName;
                        layerCountCheck = layerCount;
                        $.ajax({
                            url: "/erp/addpieceworktotal",
                            type: 'POST',
                            data: {
                                employeeNumber: employeeNumber,
                                employeeName: employeeName,
                                groupName: groupName,
                                orderName: orderName,
                                clothesVersionNumber: clothesVersionNumber,
                                colorName: colorName,
                                sizeName: sizeName,
                                layerCount: layerCount,
                                procedureNumber: pNumber,
                                procedureName: pName,
                                userName:userName,
                                createTime: createTime
                            },
                            success: function (res) {
                                if(res == 0) {
                                    layer.msg("录入成功！");
                                }else if (res == 5){
                                    layer.msg("对不起,数量大于裁数！");
                                }else if (res == 2){
                                    layer.msg("无裁床记录,请检查颜色尺码是否正确！");
                                }else if (res == 3){
                                    layer.msg("本次入数导致爆数,请重新核对数量！");
                                }else if (res == 7){
                                    layer.msg("本次入数导致爆数,整款数量超数！");
                                }else if (res == 4){
                                    layer.msg("本月工资已经锁定,无法继续录入！");
                                }else if (res == 10){
                                    layer.msg("款号已经锁定,无法继续录入！");
                                }else if (res == 44){
                                    layer.msg("该工人没有录入的计件记录,无法减数！");
                                }else {
                                    layer.msg("保存失败！");
                                }
                            },
                            error: function () {
                                layer.msg("保存失败！", {icon: 2});
                            }
                        })
                    }
                }
                ,cancel: function () {
                    layer.close(index);
                    initTable();
                }
            });
            setTimeout(function () {
                form.render();
                layui.use(['yutons_sug'], function () {
                    layui.yutons_sug.render({
                        id: "clothesVersionNumberTwo", //设置容器唯一id
                        height: "300",
                        width: "550",
                        limit:"10",            limits:[10,20,50,100],
                        cols: [
                            [{
                                field: 'clothesVersionNumberTwo',
                                title: '单号',
                                align: 'left'
                            }, {
                                field: 'orderNameTwo',
                                title: '款号',
                                align: 'left'
                            }]
                        ], //设置表头
                        params: [
                            {
                                name: 'clothesVersionNumberTwo',
                                field: 'clothesVersionNumberTwo'
                            }, {
                                name: 'orderNameTwo',
                                field: 'orderNameTwo'
                            }],//设置字段映射，适用于输入一个字段，回显多个字段
                        type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
                        url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
                    });

                    layui.yutons_sug.render({
                        id: "orderNameTwo", //设置容器唯一id
                        height: "300",
                        width: "550",
                        limit:"10",            limits:[10,20,50,100],
                        cols: [
                            [{
                                field: 'clothesVersionNumberTwo',
                                title: '单号',
                                align: 'left'
                            }, {
                                field: 'orderNameTwo',
                                title: '款号',
                                align: 'left'
                            }]
                        ], //设置表头
                        params: [
                            {
                                name: 'clothesVersionNumberTwo',
                                field: 'clothesVersionNumberTwo'
                            }, {
                                name: 'orderNameTwo',
                                field: 'orderNameTwo'
                            }],//设置字段映射，适用于输入一个字段，回显多个字段
                        type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
                        url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
                    });
                });
                $("#orderNameTwo").bind('blur change',function(){
                    var orderName = $("#orderNameTwo").val();
                    $("#colorName").empty();
                    $.ajax({
                        url: "/erp/getordercolornamesbyorder",
                        data: {"orderName": orderName},
                        success:function(data){
                            $("#colorName").empty();
                            if (data.colorNameList) {
                                $("#colorName").append("<option value=''>颜色</option>");
                                $.each(data.colorNameList, function(index,element){
                                    $("#colorName").append("<option value='"+element+"'>"+element+"</option>");
                                })
                            }
                            form.render();
                        }, error:function(){
                        }
                    });
                    $("#sizeName").empty();
                    $.ajax({
                        url: "/erp/getordersizenamesbyorder",
                        data: {"orderName": orderName},
                        success:function(data){
                            $("#sizeName").empty();
                            if (data.sizeNameList) {
                                $("#sizeName").append("<option value=''>尺码</option>");
                                $.each(data.sizeNameList, function(index,element){
                                    $("#sizeName").append("<option value='"+element+"'>"+element+"</option>");
                                })
                            }
                            form.render();
                        }, error:function(){
                        }
                    });
                    $("#procedureNumber").empty();
                    $.ajax({
                        url: "/erp/getprocedureinfobyorder",
                        data: {"orderName": orderName},
                        success:function(data){
                            $("#procedureNumber").empty();
                            if (data.procedureInfoList) {
                                procedureInfoList = data.procedureInfoList;
                                $("#procedureNumber").append("<option value=''>选择工序</option>");
                                $.each(data.procedureInfoList, function(index,element){
                                    var procedureNum = element.procedureNumber;
                                    var procedureName = element.procedureName;
                                    var pi = procedureNum+"-"+procedureName;
                                    $("#procedureNumber").append("<option value="+pi+">"+ pi +"</option>");
                                })
                            }
                            form.render();
                        }, error:function(){
                        }
                    });
                });
                $("#clothesVersionNumberTwo").bind('blur change',function(){
                    var orderName = $("#orderNameTwo").val();
                    $("#colorName").empty();
                    $.ajax({
                        url: "/erp/getordercolornamesbyorder",
                        data: {"orderName": orderName},
                        success:function(data){
                            $("#colorName").empty();
                            if (data.colorNameList) {
                                $("#colorName").append("<option value=''>颜色</option>");
                                $.each(data.colorNameList, function(index,element){
                                    $("#colorName").append("<option value='"+element+"'>"+element+"</option>");
                                })
                            }
                            form.render();
                        }, error:function(){
                        }
                    });
                    $("#sizeName").empty();
                    $.ajax({
                        url: "/erp/getordersizenamesbyorder",
                        data: {"orderName": orderName},
                        success:function(data){
                            $("#sizeName").empty();
                            if (data.sizeNameList) {
                                $("#sizeName").append("<option value=''>尺码</option>");
                                $.each(data.sizeNameList, function(index,element){
                                    $("#sizeName").append("<option value='"+element+"'>"+element+"</option>");
                                })
                            }
                            form.render();
                        }, error:function(){
                        }
                    });
                    $("#procedureNumber").empty();
                    $.ajax({
                        url: "/erp/getprocedureinfobyorder",
                        data: {"orderName": orderName},
                        success:function(data){
                            $("#procedureNumber").empty();
                            if (data.procedureInfoList) {
                                procedureInfoList = data.procedureInfoList;
                                $("#procedureNumber").append("<option value=''>选择工序</option>");
                                $.each(data.procedureInfoList, function(index,element){
                                    var procedureNum = element.procedureNumber;
                                    var procedureName = element.procedureName;
                                    var pi = procedureNum+"-"+procedureName;
                                    $("#procedureNumber").append("<option value="+pi+">"+ pi +"</option>");
                                })
                            }
                            form.render();
                        }, error:function(){
                        }
                    });
                });
                layui.use(['yutons_sug'], function () {
                    layui.yutons_sug.render({
                        id: "employeeNumber", //设置容器唯一id
                        height: "300",
                        width: "550",
                        limit:"10",            limits:[10,20,50,100],
                        cols: [
                            [{
                                field: 'employeeNumber',
                                title: '工号',
                                align: 'left'
                            }, {
                                field: 'employeeName',
                                title: '姓名',
                                align: 'left'
                            }]
                        ], //设置表头
                        params: [
                            {
                                name: 'employeeNumber',
                                field: 'employeeNumber'
                            }, {
                                name: 'employeeName',
                                field: 'employeeName'
                            }],//设置字段映射，适用于输入一个字段，回显多个字段
                        type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
                        url: "/erp/getemployeeinfohint?keyWord=" //设置异步数据接口,url为必填项,params为字段名
                    });

                    layui.yutons_sug.render({
                        id: "employeeName", //设置容器唯一id
                        height: "300",
                        width: "550",
                        limit:"10",            limits:[10,20,50,100],
                        cols: [
                            [{
                                field: 'employeeNumber',
                                title: '工号',
                                align: 'left'
                            }, {
                                field: 'employeeName',
                                title: '姓名',
                                align: 'left'
                            }]
                        ], //设置表头
                        params: [
                            {
                                name: 'employeeNumber',
                                field: 'employeeNumber'
                            }, {
                                name: 'employeeName',
                                field: 'employeeName'
                            }],//设置字段映射，适用于输入一个字段，回显多个字段
                        type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
                        url: "/erp/getemployeeinfohint?keyWord=" //设置异步数据接口,url为必填项,params为字段名
                    });
                });
                $("#employeeNumber").bind('blur change',function(){
                    var employeeNumber = $("#employeeNumber").val();
                    $.ajax({
                        url: "/erp/getemployeebyemployeenumber",
                        data: {"employeeNumber": employeeNumber},
                        success:function(data){
                            $("#groupName").empty();
                            if (data.employeeList) {
                                $("#groupName").val(data.employeeList[0].groupName);
                            }
                        }, error:function(){
                        }
                    });
                });
                $("#employeeName").bind('blur change',function(){
                    var employeeNumber = $("#employeeNumber").val();
                    $.ajax({
                        url: "/erp/getemployeebyemployeenumber",
                        data: {"employeeNumber": employeeNumber},
                        success:function(data){
                            $("#groupName").empty();
                            if (data.employeeList) {
                                $("#groupName").val(data.employeeList[0].groupName);
                            }
                        }, error:function(){
                        }
                    });
                });
                layui.laydate.render({
                    elem: '#pieceDate',
                    trigger: 'click'
                });
                form.on('select(colorDemo)', function(data){
                    var orderName = $("#orderNameTwo").val();
                    var colorName = $("#colorName").val();
                    var sizeName = $("#sizeName").val();
                    var pi = $("#procedureNumber").val();
                    var pNumber = pi.slice(0,pi.indexOf('-'));
                    var param = {};
                    param.orderName = orderName;
                    param.procedureNumber = pNumber;
                    if (colorName != null && colorName !== ''){
                        param.colorName = colorName;
                    }
                    if (sizeName != null && sizeName !== ''){
                        param.sizeName = sizeName;
                    }
                    if (orderName != null && orderName !== '' && pNumber != null && pNumber !== ''){
                        $.ajax({
                            url: "/erp/getpieceworkhistoryrecord",
                            type: 'GET',
                            async: false,
                            data: param,
                            success: function (res) {
                                if (res.code == 0) {
                                    var hisInfo = res.pieceCount + "/" + res.wellCount;
                                    $("#pieceHistory").val(hisInfo);
                                }
                            },
                            error: function () {
                                layer.msg("获取失败！", {icon: 2});
                            }
                        })
                    }
                });
                form.on('select(sizeDemo)', function(data){
                    var orderName = $("#orderNameTwo").val();
                    var colorName = $("#colorName").val();
                    var sizeName = $("#sizeName").val();
                    var pi = $("#procedureNumber").val();
                    var pNumber = pi.slice(0,pi.indexOf('-'));
                    var param = {};
                    param.orderName = orderName;
                    param.procedureNumber = pNumber;
                    if (colorName != null && colorName !== ''){
                        param.colorName = colorName;
                    }
                    if (sizeName != null && sizeName !== ''){
                        param.sizeName = sizeName;
                    }
                    if (orderName != null && orderName !== '' && pNumber != null && pNumber !== ''){
                        $.ajax({
                            url: "/erp/getpieceworkhistoryrecord",
                            type: 'GET',
                            async: false,
                            data: param,
                            success: function (res) {
                                if (res.code == 0) {
                                    var hisInfo = res.pieceCount + "/" + res.wellCount;
                                    $("#pieceHistory").val(hisInfo);
                                }
                            },
                            error: function () {
                                layer.msg("获取失败！", {icon: 2});
                            }
                        })
                    }
                })
                form.on('select(procedureDemo)', function(data){
                    var orderName = $("#orderNameTwo").val();
                    var colorName = $("#colorName").val();
                    var sizeName = $("#sizeName").val();
                    var pi = $("#procedureNumber").val();
                    var pNumber = pi.slice(0,pi.indexOf('-'));
                    var param = {};
                    param.orderName = orderName;
                    param.procedureNumber = pNumber;
                    if (colorName != null && colorName !== ''){
                        param.colorName = colorName;
                    }
                    if (sizeName != null && sizeName !== ''){
                        param.sizeName = sizeName;
                    }
                    if (orderName != null && orderName !== '' && pNumber != null && pNumber !== ''){
                        $.ajax({
                            url: "/erp/getpieceworkhistoryrecord",
                            type: 'GET',
                            async: false,
                            data: param,
                            success: function (res) {
                                if (res.code == 0) {
                                    var hisInfo = res.pieceCount + "/" + res.wellCount;
                                    $("#pieceHistory").val(hisInfo);
                                }
                            },
                            error: function () {
                                layer.msg("获取失败！", {icon: 2});
                            }
                        })
                    }
                })
            },100);
        } else if (obj.event === 'batchAdd'){
            var index = layer.open({
                type: 1 //Page层类型
                , title: '批量入数'
                , btn: ['保存']
                , shade: 0.6 //遮罩透明度
                , area: ['1000px','600px']
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , offset: '100px'
                , content: "<div><table>" +
                    "                                <tr><td style=\"text-align: right;margin-bottom: 15px;padding-top: 15px\">\n" +
                    "                                    <label class=\"layui-form-label\">工号</label>\n" +
                    "                                </td>\n" +
                    "                                <td style=\"margin-bottom: 15px;padding-top: 15px\">\n" +
                    "                                    <input type=\"text\" style='width: 200px' name=\"employeeNumber\" id=\"employeeNumber\" autocomplete=\"off\" class=\"layui-input\">\n" +
                    "                                </td>" +
                    "                                <td style=\"text-align: right;margin-bottom: 15px;padding-top: 15px\">\n" +
                    "                                    <label class=\"layui-form-label\">姓名</label>\n" +
                    "                                </td>\n" +
                    "                                <td style=\"margin-bottom: 15px;padding-top: 15px\">\n" +
                    "                                    <input type=\"text\" style='width: 200px' name=\"employeeName\" id=\"employeeName\" readonly autocomplete=\"off\" class=\"layui-input\">\n" +
                    "                                </td>" +
                    "                                <td style=\"text-align: right;margin-bottom: 15px;padding-top: 15px\">\n" +
                    "                                    <label class=\"layui-form-label\">组别</label>\n" +
                    "                                </td>\n" +
                    "                                <td style=\"margin-bottom: 15px;padding-top: 15px\">\n" +
                    "                                    <input type=\"text\" name=\"groupName\" id=\"groupName\" autocomplete=\"off\" readonly class=\"layui-input\">\n" +
                    "                                </td></tr>" +
                    "                                <tr><td style=\"text-align: right;margin-bottom: 15px;padding-top: 15px\">\n" +
                    "                                    <label class=\"layui-form-label\">单号</label>\n" +
                    "                                </td>\n" +
                    "                                <td style=\"margin-bottom: 15px;padding-top: 15px\">\n" +
                    "                                    <input type=\"text\" style='width: 200px' name=\"clothesVersionNumber\" id=\"clothesVersionNumberTwo\" autocomplete=\"off\" class=\"layui-input\">\n" +
                    "                                </td>" +
                    "                                <td style=\"text-align: right;margin-bottom: 15px;padding-top: 15px\">\n" +
                    "                                    <label class=\"layui-form-label\">款号</label>\n" +
                    "                                </td>\n" +
                    "                                <td style=\"margin-bottom: 15px;padding-top: 15px\">\n" +
                    "                                    <input type=\"text\" style='width: 200px' name=\"orderName\" id=\"orderNameTwo\" autocomplete=\"off\" class=\"layui-input\">\n" +
                    "                                </td>" +
                    "                                <td style=\"text-align: right;margin-bottom: 15px;padding-top: 15px\">\n" +
                    "                                    <label class=\"layui-form-label\">日期</label>\n" +
                    "                                </td>\n" +
                    "                                <td style=\"margin-bottom: 15px;padding-top: 15px\">\n" +
                    "                                    <input type=\"text\" style='width: 200px' name=\"pieceDate\" id=\"pieceDate\" autocomplete=\"off\" class=\"layui-input\">\n" +
                    "                                </td>" +
                    "</tr>" +
                    "                                <tr><td style=\"text-align: right;margin-bottom: 15px;padding-top: 15px\">\n" +
                    "                                    <label class=\"layui-form-label\">工序</label>\n" +
                    "                                </td>\n" +
                    "                                <td style=\"margin-bottom: 15px;padding-top: 15px\">\n" +
                    "                                    <select id=\"procedureNumber\" autocomplete=\"off\" class=\"layui-input\"></select>\n" +
                    "                                </td>" +
                    "                                <td style=\"text-align: right;margin-bottom: 15px;padding-top: 15px\">\n" +
                    "                                    <label class=\"layui-form-label\">颜色</label>\n" +
                    "                                </td>\n" +
                    "                                <td style=\"margin-bottom: 15px;padding-top: 15px\">\n" +
                    "                                    <select type=\"text\" style='width: 200px' name=\"colorName\" id=\"colorName\" autocomplete=\"off\" class=\"layui-input\"></select>\n" +
                    "                                </td>" +
                    "                                <td style=\"text-align: right;margin-bottom: 15px;padding-top: 15px\">\n" +
                    "                                    <label class=\"layui-form-label\">尺码</label>\n" +
                    "                                </td>\n" +
                    "                                <td style=\"margin-bottom: 15px;padding-top: 15px\">\n" +
                    "                                    <select type=\"text\" style='width: 200px' name=\"sizeName\" id=\"sizeName\" autocomplete=\"off\" class=\"layui-input\"></select>\n" +
                    "                                </td>" +
                    "</tr>" +
                    "</table></div>"
                ,yes: function(i, layero){
                    if (userRole != 'root' && userRole != 'role5' && userRole != 'role10' && userRole != 'role14' && userRole != 'role12'){
                        layer.msg("您没有权限！");
                        return false;
                    }
                    var employeeNumber = $("#employeeNumber").val();
                    var employeeName = $("#employeeName").val();
                    var groupName = $("#groupName").val();
                    var orderName = $("#orderNameTwo").val();
                    var clothesVersionNumber = $("#clothesVersionNumberTwo").val();
                    var createTime = $("#pieceDate").val();
                    var colorName = $("#colorName").val();
                    var sizeName = $("#sizeName").val();
                    var procedureInfo = $("#procedureNumber").val();
                    if (procedureInfo == null || procedureInfo == '' || employeeNumber == null || employeeNumber == '' || employeeName == null || employeeName == '' || groupName == null || groupName == '' || orderName == null || orderName == '' || clothesVersionNumber == null || clothesVersionNumber == '' || createTime == null || createTime == ''){
                        layer.msg("请填写完整信息！");
                        return false;
                    }
                    var proInfo = procedureInfo.split("@");
                    var procedureName = proInfo[0];
                    var procedureNumber = proInfo[1];
                    var scanPart = proInfo[2];
                    $.ajax({
                        url: "/erp/addpieceworkonce",
                        type: 'POST',
                        data: {
                            employeeNumber: employeeNumber,
                            employeeName: employeeName,
                            groupName: groupName,
                            orderName: orderName,
                            clothesVersionNumber: clothesVersionNumber,
                            procedureName: procedureName,
                            procedureNumber: procedureNumber,
                            scanPart: scanPart,
                            colorName: colorName,
                            sizeName: sizeName,
                            userName:userName,
                            createTime: createTime
                        },
                        traditional:true,
                        success: function (res) {
                            if(res.data == 1) {
                                layer.msg("该工序已有计件记录,无法批量计件!");
                            }else if (res.data == 2){
                                layer.msg("未开裁,无法计件！");
                            }else if (res.data == 0){
                                layer.msg("录入成功！");
                            } else {
                                layer.msg("录入失败！");
                            }
                        },
                        error: function () {
                            layer.msg("录入失败！", {icon: 2});
                        }
                    })
                }
                ,cancel: function () {
                    layer.close(index);
                    initTable();
                }
            });
            setTimeout(function () {
                form.render();
                layui.use(['yutons_sug'], function () {
                    layui.yutons_sug.render({
                        id: "clothesVersionNumberTwo", //设置容器唯一id
                        height: "300",
                        width: "550",
                        limit:"10",            limits:[10,20,50,100],
                        cols: [
                            [{
                                field: 'clothesVersionNumberTwo',
                                title: '单号',
                                align: 'left'
                            }, {
                                field: 'orderNameTwo',
                                title: '款号',
                                align: 'left'
                            }]
                        ], //设置表头
                        params: [
                            {
                                name: 'clothesVersionNumberTwo',
                                field: 'clothesVersionNumberTwo'
                            }, {
                                name: 'orderNameTwo',
                                field: 'orderNameTwo'
                            }],//设置字段映射，适用于输入一个字段，回显多个字段
                        type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
                        url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
                    });

                    layui.yutons_sug.render({
                        id: "orderNameTwo", //设置容器唯一id
                        height: "300",
                        width: "550",
                        limit:"10",            limits:[10,20,50,100],
                        cols: [
                            [{
                                field: 'clothesVersionNumberTwo',
                                title: '单号',
                                align: 'left'
                            }, {
                                field: 'orderNameTwo',
                                title: '款号',
                                align: 'left'
                            }]
                        ], //设置表头
                        params: [
                            {
                                name: 'clothesVersionNumberTwo',
                                field: 'clothesVersionNumberTwo'
                            }, {
                                name: 'orderNameTwo',
                                field: 'orderNameTwo'
                            }],//设置字段映射，适用于输入一个字段，回显多个字段
                        type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
                        url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
                    });
                });
                $("#orderNameTwo").bind('blur change',function(){
                    var orderName = $("#orderNameTwo").val();
                    $.ajax({
                        url: "/erp/getprocedureinfobyorder",
                        type: 'GET',
                        data: {
                            orderName:orderName
                        },
                        success:function(data){
                            if (data.procedureInfoList) {
                                $("#procedureNumber").empty();
                                $.each(data.procedureInfoList, function(index,element){
                                    $("#procedureNumber").append("<option value='"+ element.procedureName + "@" + element.procedureNumber + "@" +  element.scanPart  +"'>"+ element.procedureName + "@" + element.procedureNumber + "@" +  element.scanPart  +"</option>");
                                })
                                form.render();
                            }
                        }, error: function () {
                            layer.msg("获取工序信息失败", { icon: 2 });
                        }
                    });
                    $("#colorName").empty();
                    $.ajax({
                        url: "/erp/getordercolornamesbyorder",
                        data: {"orderName": orderName},
                        success:function(data){
                            $("#colorName").empty();
                            if (data.colorNameList) {
                                $("#colorName").append("<option value=''>全部</option>");
                                $.each(data.colorNameList, function(index,element){
                                    $("#colorName").append("<option value='"+element+"'>"+element+"</option>");
                                })
                            }
                            form.render();
                        }, error:function(){
                        }
                    });
                    $("#sizeName").empty();
                    $.ajax({
                        url: "/erp/getordersizenamesbyorder",
                        data: {"orderName": orderName},
                        success:function(data){
                            $("#sizeName").empty();
                            if (data.sizeNameList) {
                                $("#sizeName").append("<option value=''>全部</option>");
                                $.each(data.sizeNameList, function(index,element){
                                    $("#sizeName").append("<option value='"+element+"'>"+element+"</option>");
                                })
                            }
                            form.render();
                        }, error:function(){
                        }
                    });
                });
                $("#clothesVersionNumberTwo").bind('blur change',function(){
                    var orderName = $("#orderNameTwo").val();
                    $.ajax({
                        url: "/erp/getprocedureinfobyorder",
                        type: 'GET',
                        data: {
                            orderName:orderName
                        },
                        success:function(data){
                            if (data.procedureInfoList) {
                                $("#procedureNumber").empty();
                                $.each(data.procedureInfoList, function(index,element){
                                    $("#procedureNumber").append("<option value='"+ element.procedureName + "@" + element.procedureNumber + "@" +  element.scanPart  +"'>"+ element.procedureName + "@" + element.procedureNumber + "@" +  element.scanPart  +"</option>");
                                })
                                form.render();
                            }
                        }, error: function () {
                            layer.msg("获取工序信息失败", { icon: 2 });
                        }
                    });
                    $("#colorName").empty();
                    $.ajax({
                        url: "/erp/getordercolornamesbyorder",
                        data: {"orderName": orderName},
                        success:function(data){
                            $("#colorName").empty();
                            if (data.colorNameList) {
                                $("#colorName").append("<option value=''>全部</option>");
                                $.each(data.colorNameList, function(index,element){
                                    $("#colorName").append("<option value='"+element+"'>"+element+"</option>");
                                })
                            }
                            form.render();
                        }, error:function(){
                        }
                    });
                    $("#sizeName").empty();
                    $.ajax({
                        url: "/erp/getordersizenamesbyorder",
                        data: {"orderName": orderName},
                        success:function(data){
                            $("#sizeName").empty();
                            if (data.sizeNameList) {
                                $("#sizeName").append("<option value=''>全部</option>");
                                $.each(data.sizeNameList, function(index,element){
                                    $("#sizeName").append("<option value='"+element+"'>"+element+"</option>");
                                })
                            }
                            form.render();
                        }, error:function(){
                        }
                    });
                });
                layui.use(['yutons_sug'], function () {
                    layui.yutons_sug.render({
                        id: "employeeNumber", //设置容器唯一id
                        height: "300",
                        width: "550",
                        limit:"10",            limits:[10,20,50,100],
                        cols: [
                            [{
                                field: 'employeeNumber',
                                title: '工号',
                                align: 'left'
                            }, {
                                field: 'employeeName',
                                title: '姓名',
                                align: 'left'
                            }]
                        ], //设置表头
                        params: [
                            {
                                name: 'employeeNumber',
                                field: 'employeeNumber'
                            }, {
                                name: 'employeeName',
                                field: 'employeeName'
                            }],//设置字段映射，适用于输入一个字段，回显多个字段
                        type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
                        url: "/erp/getemployeeinfohint?keyWord=" //设置异步数据接口,url为必填项,params为字段名
                    });

                    layui.yutons_sug.render({
                        id: "employeeName", //设置容器唯一id
                        height: "300",
                        width: "550",
                        limit:"10",            limits:[10,20,50,100],
                        cols: [
                            [{
                                field: 'employeeNumber',
                                title: '工号',
                                align: 'left'
                            }, {
                                field: 'employeeName',
                                title: '姓名',
                                align: 'left'
                            }]
                        ], //设置表头
                        params: [
                            {
                                name: 'employeeNumber',
                                field: 'employeeNumber'
                            }, {
                                name: 'employeeName',
                                field: 'employeeName'
                            }],//设置字段映射，适用于输入一个字段，回显多个字段
                        type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
                        url: "/erp/getemployeeinfohint?keyWord=" //设置异步数据接口,url为必填项,params为字段名
                    });
                });
                $("#employeeNumber").bind('blur change',function(){
                    var employeeNumber = $("#employeeNumber").val();
                    $.ajax({
                        url: "/erp/getemployeebyemployeenumber",
                        data: {"employeeNumber": employeeNumber},
                        success:function(data){
                            if (data.employeeList) {
                                console.log(data.employeeList);
                                $("#groupName").val(data.employeeList[0].groupName);
                            }
                        }, error:function(){
                        }
                    });
                });
                $("#employeeName").bind('blur change',function(){
                    var employeeNumber = $("#employeeNumber").val();
                    $.ajax({
                        url: "/erp/getemployeebyemployeenumber",
                        data: {"employeeNumber": employeeNumber},
                        success:function(data){
                            if (data.employeeList) {
                                $("#groupName").val(data.employeeList[0].groupName);
                            }
                        }, error:function(){
                        }
                    });
                });
                layui.laydate.render({
                    elem: '#pieceDate',
                    trigger: 'click'
                });
            },100);
        } else if (obj.event === 'delete'){
            var checkStatus = table.checkStatus(obj.config.id); //获取选中行状态
            if (userRole!='root' && userRole!='role5' && userRole!='role1' && userRole!='role10' && userRole!='role12'){
                layer.msg('您没有权限!');
                return false;
            } else if(checkStatus.data.length == 0) {
                layer.msg('请先选择数据');
                return false;
            } else {
                var pieceWorkIDList = [];
                $.each(checkStatus.data, function (index, item) {
                    pieceWorkIDList.push(item.pieceWorkID);
                });
                layer.confirm('真的删除吗', function(index){
                    $.ajax({
                        url: "/erp/deletepieceworkbatch",
                        type:'POST',
                        data: {
                            userName:userName,
                            pieceWorkIDList:pieceWorkIDList
                        },
                        traditional:true,
                        success: function (data) {
                            if(data == 0) {
                                layer.msg("删除成功！",{icon: 1});
                                initTable();
                            }else {
                                layer.msg("删除失败！",{icon: 2});
                            }
                        }, error: function () {
                            layer.msg("删除失败！",{icon: 2});
                        }
                    });
                });
            }
        } else if (obj.event === 'today'){
            initTable();
        }else if (obj.event === 'thisMonth'){
            var load = layer.load(2);
            $.ajax({
                url: "/erp/getonemonthmanualinput",
                type: 'GET',
                data: {},
                success: function (res) {
                    if (res.oneMonthManualInputList) {
                        var reportData = res.oneMonthManualInputList;
                        table.render({
                            elem: '#reportTable'
                            ,cols:[[
                                {type:'checkbox'}
                                ,{type:'numbers', align:'center', title:'序号', width:70}
                                ,{field:'updateTime', title:'录入时间', align:'center', width:120, sort: true, filter: true, templet: function (d) {
                                        return  moment(d.updateTime).format("YYYY-MM-DD");
                                    }}
                                ,{field:'employeeName', title:'姓名', align:'center', width:120, sort: true, filter: true}
                                ,{field:'employeeNumber', title:'工号', align:'center', width:100, sort: true, filter: true}
                                ,{field:'groupName', title:'组名', align:'center', width:80, sort: true, filter: true}
                                ,{field:'orderName', title:'款号', align:'center', width:120, sort: true, filter: true}
                                ,{field:'clothesVersionNumber', title:'单号', align:'center', width:120, sort: true, filter: true, totalRowText: '合计'}
                                ,{field:'colorName', title:'颜色', align:'center', width:100, sort: true, filter: true}
                                ,{field:'sizeName', title:'尺码', align:'center', width:100, sort: true, filter: true}
                                ,{field:'layerCount', title:'数量', align:'center', width:100, sort: true, filter: true}
                                ,{field:'procedureNumber', title:'工序号', align:'center', width:100, sort: true, filter: true}
                                ,{field:'procedureName', title:'工序名', align:'center', width:150, sort: true, filter: true}
                                ,{field:'createTime', title:'日期', align:'center', width:120, sort: true, filter: true, templet: function (d) {
                                        return  moment(d.createTime).format("YYYY-MM-DD");
                                    }}
                                ,{field:'userName', title:'录入人', align:'center', width:120, sort: true, filter: true, totalRow: true}
                                ,{field:'pieceWorkID', title:'操作', align:'center', width:90, toolbar: '#barTop', sort: true, filter: true}
                            ]]
                            ,loading:true
                            ,data: reportData
                            ,height: 'full-130'
                            ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                            ,defaultToolbar: ['filter', 'print']
                            ,title: '入数'
                            ,totalRow: true
                            ,page: true
                            ,overflow: 'tips'
                            ,limits: [50, 100, 200]
                            ,limit: 100 //每页默认显示的数量
                            ,done: function () {
                                soulTable.render(this);
                                layer.close(load);
                            }
                            ,filter: {
                                bottom: true,
                                items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                            }
                        });

                    } else {
                        layer.msg("获取失败！", {icon: 2});
                    }
                }, error: function () {
                    layer.msg("获取失败！", {icon: 2});
                }
            });
            return false;
        }
    });
    //监听提交
    form.on('submit(searchBeat)', function(data){
        var data = {};
        var orderName = $("#orderName").val();
        var from = $("#fromDate").val();
        var to = $("#toDate").val();
        if ((orderName == null || orderName == '') && (from == null || from == '' || to == null || to == '')){
            layer.msg("款号-时间区间不能同时为空", { icon: 2 });
            return false;
        }
        if (orderName != null && orderName != ''){
            data.orderName = orderName;
        }
        if (from != null && from != ''){
            data.from = from;
        }
        if (to != null && to != ''){
            data.to = to;
        }
        var load = layer.load(2);
        $.ajax({
            url: "/erp/getmanualinputbyinfo",
            type: 'GET',
            data: data,
            success: function (res) {
                if (res.data) {
                    var reportData = res.data;
                    table.render({
                        elem: '#reportTable'
                        ,cols:[[
                            {type:'checkbox'}
                            ,{type:'numbers', align:'center', title:'序号', width:70}
                            ,{field:'updateTime', title:'录入时间', align:'center', width:120, sort: true, filter: true, templet: function (d) {
                                    return  moment(d.updateTime).format("YYYY-MM-DD");
                                }}
                            ,{field:'employeeName', title:'姓名', align:'center', width:120, sort: true, filter: true}
                            ,{field:'employeeNumber', title:'工号', align:'center', width:100, sort: true, filter: true}
                            ,{field:'groupName', title:'组名', align:'center', width:80, sort: true, filter: true}
                            ,{field:'orderName', title:'款号', align:'center', width:120, sort: true, filter: true}
                            ,{field:'clothesVersionNumber', title:'单号', align:'center', width:120, sort: true, filter: true, totalRowText: '合计'}
                            ,{field:'colorName', title:'颜色', align:'center', width:100, sort: true, filter: true}
                            ,{field:'sizeName', title:'尺码', align:'center', width:100, sort: true, filter: true}
                            ,{field:'layerCount', title:'数量', align:'center', width:100, sort: true, filter: true}
                            ,{field:'procedureNumber', title:'工序号', align:'center', width:100, sort: true, filter: true}
                            ,{field:'procedureName', title:'工序名', align:'center', width:150, sort: true, filter: true}
                            ,{field:'createTime', title:'日期', align:'center', width:120, sort: true, filter: true, templet: function (d) {
                                    return  moment(d.createTime).format("YYYY-MM-DD");
                                }}
                            ,{field:'userName', title:'录入人', align:'center', width:120, sort: true, filter: true, totalRow: true}
                            ,{field:'pieceWorkID', title:'操作', align:'center', width:90, toolbar: '#barTop', sort: true, filter: true}
                        ]]
                        ,loading:true
                        ,data: reportData
                        ,height: 'full-130'
                        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                        ,defaultToolbar: ['filter', 'print']
                        ,title: '入数'
                        ,totalRow: true
                        ,page: true
                        ,overflow: 'tips'
                        ,limits: [50, 100, 200]
                        ,limit: 100 //每页默认显示的数量
                        ,done: function () {
                            soulTable.render(this);
                            layer.close(load);
                        }
                        ,filter: {
                            bottom: true,
                            items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                        }
                    });
                } else {
                    layer.msg("获取失败！", {icon: 2});
                }
            }, error: function () {
                layer.msg("获取失败！", {icon: 2});
            }
        });
        return false;
    });

    table.on('tool(reportTable)', function(obj){
        var data = obj.data;
        if(obj.event === 'update'){
            if (userRole!='root' && userRole!='role5' && userRole!='role1' && userRole!='role10' && userRole!='role12'){
                layer.msg('您没有权限!');
                return false;
            }
            var index = layer.open({
                type: 1 //Page层类型
                , title: '修改'
                , btn: ['保存']
                , shade: 0.6 //遮罩透明度
                ,area: ['1000', '600px'] //宽高
                , offset: '100px'
                , anim: 0 //0-6的动画形式，-1不开启
                , content: '<div style="padding-top: 30px">\n' +
                    '                    <table>' +
                    '                        <tr>' +
                    '                            <td style="text-align: right;margin-bottom: 15px;width: 100px">\n' +
                    '                                 <label class="layui-form-label">单号: </label>\n' +
                    '                            </td>\n' +
                    '                            <td style="margin-bottom: 15px;">\n' +
                    '                                 <input type="text" style="width: 150px;" readonly autocomplete="off" id="updateClothesVersionNumber" class="layui-input">\n' +
                    '                            </td>' +
                    '                            <td style="text-align: right;margin-bottom: 15px;width: 100px">\n' +
                    '                                 <label class="layui-form-label">款号: </label>\n' +
                    '                            </td>\n' +
                    '                            <td style="margin-bottom: 15px;">\n' +
                    '                                 <input type="text" style="width: 150px;" readonly autocomplete="off" id="updateOrderName" class="layui-input">\n' +
                    '                            </td>' +
                    '                            <td style="text-align: right;margin-bottom: 15px;width: 100px">\n' +
                    '                                 <label class="layui-form-label">颜色: </label>\n' +
                    '                            </td>\n' +
                    '                            <td style="margin-bottom: 15px;">\n' +
                    '                                 <input type="text" style="width: 150px;" readonly autocomplete="off" id="updateColorName" class="layui-input">\n' +
                    '                            </td>' +
                    '                        </tr>' +
                    '                        <tr>' +
                    '                            <td style="text-align: right;margin-bottom: 15px;width: 100px">\n' +
                    '                                 <label class="layui-form-label">尺码: </label>\n' +
                    '                            </td>\n' +
                    '                            <td style="margin-bottom: 15px;">\n' +
                    '                                 <input type="text" style="width: 150px;" readonly autocomplete="off" id="updateSizeName" class="layui-input">\n' +
                    '                            </td>' +
                    '                            <td style="text-align: right;margin-bottom: 15px;width: 100px">\n' +
                    '                                 <label class="layui-form-label">工序名: </label>\n' +
                    '                            </td>\n' +
                    '                            <td style="margin-bottom: 15px;">\n' +
                    '                                 <input type="text" style="width: 150px;" readonly id="updateProcedureName" autocomplete="off" class="layui-input">\n' +
                    '                            </td>' +
                    '                            <td style="text-align: right;margin-bottom: 15px;width: 100px">\n' +
                    '                                 <label class="layui-form-label">工序号: </label>\n' +
                    '                            </td>\n' +
                    '                            <td style="margin-bottom: 15px;">\n' +
                    '                                 <input type="text" style="width: 150px;" readonly id="updateProcedureNumber" autocomplete="off" class="layui-input">\n' +
                    '                            </td>' +
                    '                        </tr>' +
                    '                        <tr>' +
                    '                            <td style="text-align: right;margin-bottom: 15px;width: 100px">\n' +
                    '                                 <label class="layui-form-label">工号: </label>\n' +
                    '                            </td>\n' +
                    '                            <td style="margin-bottom: 15px;">\n' +
                    '                                 <input type="text" style="width: 150px;" readonly id="updateEmployeeNumber" autocomplete="off" class="layui-input">\n' +
                    '                            </td>' +
                    '                            <td style="text-align: right;margin-bottom: 15px;width: 100px">\n' +
                    '                                 <label class="layui-form-label">姓名: </label>\n' +
                    '                            </td>\n' +
                    '                            <td style="margin-bottom: 15px;">\n' +
                    '                                 <input type="text" style="width: 150px;" readonly id="updateEmployeeName" autocomplete="off" class="layui-input">\n' +
                    '                            </td>' +
                    '                            <td style="text-align: right;margin-bottom: 15px;width: 100px">\n' +
                    '                                 <label class="layui-form-label">数量: </label>\n' +
                    '                            </td>\n' +
                    '                            <td style="margin-bottom: 15px;">\n' +
                    '                                 <input type="text" style="width: 150px;" id="updateLayerCount" autocomplete="off" class="layui-input">\n' +
                    '                            </td>' +
                    '                        </tr>' +
                    '                        <tr>' +
                    '                            <td style="text-align: right;margin-bottom: 15px;width: 100px">\n' +
                    '                                 <label class="layui-form-label">日期: </label>\n' +
                    '                            </td>\n' +
                    '                            <td style="margin-bottom: 15px;">\n' +
                    '                                 <input type="text" style="width: 150px;" id="updatePieceDate" autocomplete="off" class="layui-input">\n' +
                    '                            </td>' +
                    '                        </tr>' +
                    '                    </table>' +
                    '                </div>'
                ,yes: function(index, layero){
                    var manualInput = {};
                    manualInput.pieceWorkID = data.pieceWorkID;
                    manualInput.employeeName = data.employeeName;
                    manualInput.employeeNumber = data.employeeNumber;
                    manualInput.groupName = data.groupName;
                    manualInput.orderName = data.orderName;
                    manualInput.clothesVersionNumber = data.clothesVersionNumber;
                    manualInput.colorName = data.colorName;
                    manualInput.sizeName = data.sizeName;
                    manualInput.layerCount = $("#updateLayerCount").val();
                    manualInput.procedureNumber = data.procedureNumber;
                    manualInput.procedureName = data.procedureName;
                    manualInput.userName = userName;
                    manualInput.createTime = $("#updatePieceDate").val();

                    if ($("#updateLayerCount").val() == null || $("#updateLayerCount").val() == ""){
                        layer.msg("填写有误！", {icon: 2});
                        return false;
                    }
                    if ($("#updatePieceDate").val() == null || $("#updatePieceDate").val() == ""){
                        layer.msg("填写有误！", {icon: 2});
                        return false;
                    }
                    $.ajax({
                        url: "/erp/updatemanualinput",
                        type: 'POST',
                        data: {
                            manualInput:JSON.stringify(manualInput)
                        },
                        success: function (res) {
                            if (res == 0) {
                                layer.msg("修改成功！",{icon: 1});
                                initTable();
                                layer.close(index);
                            } else if (res == 1) {
                                layer.msg("修改失败！");
                            } else if (res == 2) {
                                layer.msg("无裁床记录,请检查颜色尺码是否正确！！");
                            } else if (res == 3) {
                                layer.msg("修改失败！");
                            } else if (res == 4) {
                                layer.msg("本月工资已经锁定!无法继续录入！");
                            } else if (res == 5) {
                                layer.msg("本次入数导致爆数, 请重新核对数量！");
                            } else if (res == 7) {
                                layer.msg("本次入数导致爆数, 整款数量超数！");
                            } else if (res == 10) {
                                layer.msg("款号已锁定, 无法继续录入！");
                            } else {
                                layer.msg("修改失败！", {icon: 2});
                            }
                        }, error: function () {
                            layer.msg("修改失败！", {icon: 2});
                        }
                    })
                }, cancel : function (i,layero) {}
            });
            $("#updateClothesVersionNumber").val(data.clothesVersionNumber);
            $("#updateOrderName").val(data.orderName);
            $("#updateColorName").val(data.colorName);
            $("#updateSizeName").val(data.sizeName);
            $("#updateProcedureNumber").val(data.procedureNumber);
            $("#updateProcedureName").val(data.procedureName);
            $("#updateEmployeeNumber").val(data.employeeNumber);
            $("#updateEmployeeName").val(data.employeeName);
            $("#updateLayerCount").val(data.layerCount);
            $("#updatePieceDate").val(moment(data.createTime).format("YYYY-MM-DD"));
            layui.laydate.render({
                elem: '#updatePieceDate',
                trigger: 'click'
            });
        }
    })
});

function autoComplete(employeeNumber) {
    $.ajax({
        url: "/erp/getemployeebyemployeenumber",
        data: {"employeeNumber": employeeNumber},
        success:function(data){
            $("#employeeName").empty();
            $("#groupName").empty();
            if (data.employeeList) {
                $("#employeeName").val(data.employeeList[0].employeeName);
                $("#groupName").val(data.employeeList[0].groupName);
            }
        }, error:function(){
        }
    });

}