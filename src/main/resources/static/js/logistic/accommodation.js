var basePath=$("#basePath").val();
$(document).ready(function () {

    layui.laydate.render({
        elem: '#checkInDate',
        trigger: 'click'
    });

    layui.use(['autocomplete'], function () {
        layui.autocomplete.render({
            elem: $('#employeeNumber')[0],
            url: 'getemphint',
            template_val: '{{d.employeeNumber}}',
            template_txt: '{{d.employeeNumber}}',
            onselect: function (resp) {
                autoComplete(resp.employeeNumber)
            }
        })
    });

    $("#floor").empty();
    $.ajax({
        url: "/erp/getallfloor",
        data: {},
        success:function(data){
            if (data) {
                $("#floor").empty();
                if (data.floorList) {
                    $("#floor").append("<option value=''>选择楼层</option>");
                    $.each(data.floorList, function(index,element){
                        $("#floor").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
            }
        },
        error:function(){
        }
    });

    $("#floor").change(function () {
        var floorValue = $(this).val();
        $("#roomNumber").empty();
        $("#bedNumber").empty();
        $.ajax({
            url: "/erp/getavailableroomoffloor",
            data: {"floor": floorValue},
            async:false,
            success:function(data){
                $("#roomNumber").empty();
                if (data.roomNumberList) {
                    $("#roomNumber").append("<option value=''>选择房间</option>");
                    $.each(data.roomNumberList, function(index,element){
                        $("#roomNumber").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });
    });

    $("#roomNumber").change(function () {
        var roomNumberValue = $(this).val();
        $("#bedNumber").empty();
        $.ajax({
            url: "/erp/getavailablebednumber",
            data: {"roomNumber": roomNumberValue},
            success:function(data){
                $("#bedNumber").empty();
                if (data.bedNumberList) {
                    $("#bedNumber").append("<option value=''>选择床号</option>");
                    $.each(data.bedNumberList, function(index,element){
                        $("#bedNumber").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });
    });
});

function autoComplete(keywords) {
    $("#employeeName").empty();
    $.ajax({
        url: "/erp/getempnamebyempnum",
        data: {"employeeNumber": keywords},
        success:function(data){
            if (data) {
                $("#employeeName").empty();
                $("#employeeName").val(data);
                $("#employeeName").attr("disabled","disabled");
            }
        },
        error:function(){
        }
    });

}

function addEmpRoom(floor,room) {
    $("#floor").val(floor);
    $('#floor').trigger("change");
    $("#roomNumber").val(room);
    $('#roomNumber').trigger("change");
    layer.open({
        type: 1 //Page层类型
        , title: "入住信息"
        , area: '900px'
        , btn: ['保存']
        , shade: 0.6 //遮罩透明度
        , maxmin: false //允许全屏最小化
        , anim: 0 //0-6的动画形式，-1不开启
        , content: $('#editPro')
        , yes: function (index, layero) {
            var flag = false;
            $("#editPro input,select").each(function () {
                if($(this).val().trim() == "") {
                    flag = true;
                    return false;
                }
            })
            if (flag) {
                swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入完全部信息！</span>",html: true});
                return false;
            }
            var empRoom = {};
            empRoom.employeeNumber = $("#employeeNumber").val();
            empRoom.employeeName = $("#employeeName").val();
            empRoom.floor = $("#floor").val();
            empRoom.roomNumber = $("#roomNumber").val();
            empRoom.bedNumber = $("#bedNumber").val();
            empRoom.checkInDate = $("#checkInDate").val();
            $.ajax({
                url: basePath + "erp/addemproom",
                type: 'POST',
                data: {
                    empRoom:JSON.stringify(empRoom)
                },
                success: function (data) {
                    if(data == 0) {
                        layer.closeAll();
                        $("input").val("");
                        swal({
                                type:"success",
                                title:"",
                                text: "<span style=\"font-weight:bolder;font-size: 20px\">恭喜你，保存成功！</span>",
                                html: true
                            },
                            function(){
                                location.href=basePath+"erp/accommodationStart";
                            });
                    }else {
                        swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，保存失败！</span>",html: true});
                    }
                },
                error: function () {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
                }
            });
        }
        ,cancel: function(index, layero){
            layer.closeAll();
            $("input").val("");
        }
    })
}

function deleteEmpRoom(empRoomID,employeeName) {
    swal({
        title: "",
        text: "<span style=\"font-weight:bolder;font-size: 20px\">您确定要给【"+employeeName+"】退房吗？</span>",
        type: "warning",
        html:true,
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: "确定",
        cancelButtonText:"我再想想",
        confirmButtonColor: "#ec6c62",
        showLoaderOnConfirm: true
    }, function() {
        $.ajax({
            url: basePath + "erp/deleteemproom",
            type:'POST',
            data: {
                empRoomID:empRoomID
            },
            success: function (data) {
                if(data == 0) {
                    $("input").val("");
                    swal({
                            type:"success",
                            title:"",
                            text: "<span style=\"font-weight:bolder;font-size: 20px\">恭喜你，退房成功！</span>",
                            html: true
                        },
                        function(){
                            location.href=basePath+"erp/accommodationStart";
                        });
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，退房失败！</span>",html: true});
                }
            },
            error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        })
    });
}

