var basePath=$("#basePath").val();
$(document).ready(function () {
    $.ajax({
        url: basePath + "erp/getallroom",
        type:'GET',
        data: {},
        success: function (data) {
            if(data) {
                createRoomTable(data.room);
            }else {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，没有查到相关信息！</span>",html: true});
            }
        },
        error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }
    });
});




var roomTable;
function createRoomTable(data) {
    if (roomTable != undefined) {
        roomTable.clear(); //清空一下table
        roomTable.destroy(); //还原初始化了的datatable
    }
    roomTable = $('#roomTable').DataTable({
        "retrieve": true,
        data: data,
        language : {
            processing : "载入中",//处理页面数据的时候的显示
            paginate : {//分页的样式文本内容。
                previous : "上一页",
                next : "下一页",
                first : "第一页",
                last : "最后一页"
            },
            search:"搜索：",
            zeroRecords : "没有内容",//table tbody内容为空时，tbody的内容。
            //下面三者构成了总体的左下角的内容。
            info : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//左下角的信息显示，大写的词为关键字。
            infoEmpty : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//筛选为空时左下角的显示。
            infoFiltered : ""//筛选之后的左下角筛选提示(另一个是分页信息显示，在上面的info中已经设置，所以可以不显示)，
        },
        pageLength : 20,// 每页显示10条数据
        pagingType : "full_numbers", // 分页样式：simple,simple_numbers,full,full_numbers，
        "paging": true,
        // "ordering" : false,
        "info": false,
        searching:true,
        lengthChange:false,
        dom: 'Bfrtip',
        "buttons": [{
            'extend': 'excel',
            'text': '导出',//定义导出excel按钮的文字
            'className': 'btn btn-success', //按钮的class样式
            'title': '宿舍',
            'exportOptions': {
                'modifier': {
                    'page': 'all'
                }
            }
        }],
        "columns": [
            {
                "data": null,
                "title":"序号",
                "width":"8%",
                "defaultContent": "",
                "sClass": "text-center",
                "searchable":false,
                render: function (data, type, row, meta) {
                    var no = meta.settings._iDisplayStart + meta.row + 1;
                    return no;
                }
            }, {
                "data": "floor",
                "title":"楼层",
                "width":"15%",
                "defaultContent": "",
                "sClass": "text-center"
            }, {
                "data": "roomNumber",
                "title":"房间号",
                "width":"15%",
                "defaultContent": "",
                "sClass": "text-center"
            }, {
                "data": "capacity",
                "title":"容量",
                "width":"15%",
                "defaultContent": "",
                "sClass": "text-center"
            }, {
                "data": "remark",
                "title": "备注",
                "width":"20%",
                "defaultContent": "",
                "sClass": "text-center"
            }, {
                "data": "roomID",
                "title": "操作",
                "width":"10%",
                "defaultContent": "",
                "sClass": "text-center"
            }
        ],
        "columnDefs" :
            [{
                "orderable" : false, // 禁用排序
                "targets" : [5], // 指定的列
                "data" : "roomID",
                "render" : function(data, type, full, meta) {
                    return "<a href='#' style='color:#ff0000' onclick='deleteRoom("+data+")'>删除</a>&nbsp;&nbsp;&nbsp;<a href='#' style='color:#3e8eea' onclick='updateRoom(this,"+data+")'>修改</a>";
                }
            }]

    });
    $('#roomTable_wrapper .dt-buttons').append("<button class=\"btn btn-success\" style=\"outline:none;margin-left: 10px;\" onclick=\"addRoom()\">添加宿舍</button>");
}



function addRoom() {
    layer.open({
        type: 1 //Page层类型
        , title: "添加宿舍"
        , area: ['900px', '300px']
        , btn: ['保存']
        , shade: 0.6 //遮罩透明度
        , maxmin: false //允许全屏最小化
        , anim: 0 //0-6的动画形式，-1不开启
        , content: $('#editPro')
        , yes: function (index, layero) {
            var flag = false;
            $("input[name='floor']").each(function (index,item){
                if ($(item).val() == ""){
                    flag = true;
                }
            });
            $("input[name='roomNumber']").each(function (index,item){
                if ($(item).val() == ""){
                    flag = true;
                }
            });
            $("input[name='capacity']").each(function (index,item){
                if (!(/(^[1-9]\d*$)/.test($(item).val()))){
                    swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">容量应为数字！</span>",html: true});
                    return false;
                }
            });
            if (flag) {
                swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入完全部信息！</span>",html: true});
                return false;
            }
            var roomJson = [];
            $("input[name='floor']").each(function (index,item) {
                var room = {};
                room.floor = $(item).val();
                room.roomNumber = $("input[name='roomNumber']").eq(index).val();
                room.capacity = $("input[name='capacity']").eq(index).val();
                room.remark = $("input[name='remark']").eq(index).val();
                roomJson.push(room);
            });
            $.ajax({
                url: basePath + "erp/addroombatch",
                type: 'POST',
                data: {
                    roomJson:JSON.stringify(roomJson)
                },
                success: function (data) {
                    if(data == 0) {
                        layer.closeAll();
                        $("#editPro input").val("");
                        swal({
                                type:"success",
                                title:"",
                                text: "<span style=\"font-weight:bolder;font-size: 20px\">恭喜你，保存成功！</span>",
                                html: true
                            },
                            function(){
                                location.href=basePath+"erp/roomStart";
                            });
                    }else {
                        swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，保存失败！</span>",html: true});
                    }
                },
                error: function () {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
                }
            });
        }
        ,cancel: function(index, layero){
            layer.closeAll();
            $("#editPro input").val("");
        }
    })
}



function updateRoom(obj,roomID){
    $("#floor1").val($(obj).parent().parent().find("td").eq(1).text());
    $("#roomNumber1").val($(obj).parent().parent().find("td").eq(2).text());
    $("#capacity1").val($(obj).parent().parent().find("td").eq(3).text());
    $("#remark1").val($(obj).parent().parent().find("td").eq(4).text());
    layer.open({
        type: 1 //Page层类型
        , title: "修改信息"
        , btn: ['保存']
        , shade: 0.6 //遮罩透明度
        , maxmin: false //允许全屏最小化
        , anim: 0 //0-6的动画形式，-1不开启
        , content: $('#editRoomPro')
        , yes: function (index, layero) {
            var flag = false;
            if ($("#floor1").val().trim()==""){
                flag = true;
            }
            if ($("#roomNumber1").val().trim() == ""){
                flag = true;
            }
            if ($("#capacity1").val().trim() == ""){
                flag = true;
            }
            if(flag) {
                swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请填写完所有字段信息！</span>",html: true});
                return false;
            }
            var room = {};
            room.roomID = roomID;
            room.floor = $("#floor1").val();
            room.roomNumber = $("#roomNumber1").val();
            room.capacity = $("#capacity1").val();
            if ($("#remark1").val() != ""){
                room.remark = $("#remark1").val();
            }
            console.log(room);
            $.ajax({
                url: basePath + "erp/updateroom",
                type:'POST',
                data: {
                    room:JSON.stringify(room)
                },
                success: function (data) {
                    if(data == 0) {
                        layer.closeAll();
                        $("#editRoomPro input").val("");
                        swal({
                                type:"success",
                                title:"",
                                text: "<span style=\"font-weight:bolder;font-size: 20px\">恭喜你，保存成功！</span>",
                                html: true
                            },
                            function(){
                                location.href=basePath+"erp/roomStart";
                            });
                    }else {
                        swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，保存失败！</span>",html: true});
                    }
                },
                error: function () {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
                }
            })
        }
        ,cancel: function(index, layero){
            layer.closeAll();
            $("#editRoomPro input").val("");
        }
    })

}




function deleteRoom(roomID) {
    swal({
        title: "",
        text: "<span style=\"font-weight:bolder;font-size: 20px\">您确定要删除吗？</span>",
        type: "warning",
        html:true,
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: "确定",
        cancelButtonText:"我再想想",
        confirmButtonColor: "#ec6c62",
        showLoaderOnConfirm: true
    }, function() {
        $.ajax({
            url: basePath + "erp/deleteroom",
            type:'POST',
            data: {
                roomID:roomID
            },
            success: function (data) {
                if(data == 0) {
                    $("input").val("");
                    swal({
                            type:"success",
                            title:"",
                            text: "<span style=\"font-weight:bolder;font-size: 20px\">恭喜你，删除成功！</span>",
                            html: true
                        },
                        function(){
                            location.href=basePath+"erp/roomStart";
                        });
                }else if(data == 2) {
                    swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">该宿舍还有员工未退房！</span>",html: true});
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，删除失败！</span>",html: true});
                }
            },
            error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        })
    });
}


function addSize(obj) {
    $("#roomTableDiv").append($("tr[name='roomDiv']:last").clone());
    $("input[name='floor']:last").val($("input[name='floor']:first").val());
    $("input[name='roomNumber']:last").val($("input[name='roomNumber']:first").val());
    $("input[name='capacity']:last").val($("input[name='capacity']:first").val());
    $("input[name='remark']:last").val("");
    $("button[name='addSizeBtn']").remove();
    $("button[name='delSizeBtn']:first").before('<button name="addSizeBtn" class="btn" style="margin-bottom: 15px;outline:none;border-radius: 5px;color: white;" onclick="addSize(this)"><i class="fa fa-plus"></i></button>');
    $("button[name='delSizeBtn']:last").show();
}

function delSize(obj) {
    $(obj).parent().parent().parent().remove();
}
