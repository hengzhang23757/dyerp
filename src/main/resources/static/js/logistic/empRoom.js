var basePath=$("#basePath").val();
$(document).ready(function () {
    $.ajax({
        url: basePath + "erp/getallemproom",
        type:'GET',
        data: {},
        success: function (data) {
            if(data) {
                createEmpRoomTable(data.empRoom);
            }else {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，没有查到相关信息！</span>",html: true});
            }
        },
        error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }
    });

    layui.laydate.render({
        elem: '#checkInDate',
        trigger: 'click'
    });

    layui.use(['autocomplete'], function () {
        layui.autocomplete.render({
            elem: $('#employeeNumber')[0],
            url: 'getemphint',
            template_val: '{{d.employeeNumber}}',
            template_txt: '{{d.employeeNumber}}',
            onselect: function (resp) {
                autoComplete(resp.employeeNumber)
            }
        })
    });

    $("#floor").change(function () {
        var floorValue = $(this).val();
        $("#roomNumber").empty();
        $("#bedNumber").empty();
        $.ajax({
            url: "/erp/getavailableroomoffloor",
            data: {"floor": floorValue},
            success:function(data){
                $("#roomNumber").empty();
                if (data.roomNumberList) {
                    $("#roomNumber").append("<option value=''>选择房间</option>");
                    $.each(data.roomNumberList, function(index,element){
                        $("#roomNumber").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });
    });

    $("#roomNumber").change(function () {
        var roomNumberValue = $(this).val();
        $("#bedNumber").empty();
        $.ajax({
            url: "/erp/getavailablebednumber",
            data: {"roomNumber": roomNumberValue},
            success:function(data){
                $("#bedNumber").empty();
                if (data.bedNumberList) {
                    $("#bedNumber").append("<option value=''>选择床号</option>");
                    $.each(data.bedNumberList, function(index,element){
                        $("#bedNumber").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });
    });
});

function autoComplete(keywords) {
    $("#employeeName").empty();
    $.ajax({
        url: "/erp/getempnamebyempnum",
        data: {"employeeNumber": keywords},
        success:function(data){
            if (data) {
                $("#employeeName").empty();
                $("#employeeName").val(data);
                $("#employeeName").attr("disabled","disabled");
            }
        },
        error:function(){
        }
    });

    $("#floor").empty();
    $.ajax({
        url: "/erp/getallfloor",
        data: {},
        success:function(data){
            if (data) {
                $("#floor").empty();
                if (data.floorList) {
                    $("#floor").append("<option value=''>选择楼层</option>");
                    $.each(data.floorList, function(index,element){
                        $("#floor").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
            }
        },
        error:function(){
        }
    });
}


var empRoomTable;
function createEmpRoomTable(data) {
    if (empRoomTable != undefined) {
        empRoomTable.clear(); //清空一下table
        empRoomTable.destroy(); //还原初始化了的datatable
    }
    empRoomTable = $('#empRoomTable').DataTable({
        "retrieve": true,
        data: data,
        language : {
            processing : "载入中",//处理页面数据的时候的显示
            paginate : {//分页的样式文本内容。
                previous : "上一页",
                next : "下一页",
                first : "第一页",
                last : "最后一页"
            },
            search:"搜索：",
            zeroRecords : "没有内容",//table tbody内容为空时，tbody的内容。
            //下面三者构成了总体的左下角的内容。
            info : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//左下角的信息显示，大写的词为关键字。
            infoEmpty : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//筛选为空时左下角的显示。
            infoFiltered : ""//筛选之后的左下角筛选提示(另一个是分页信息显示，在上面的info中已经设置，所以可以不显示)，
        },
        pageLength : 20,// 每页显示10条数据
        pagingType : "full_numbers", // 分页样式：simple,simple_numbers,full,full_numbers，
        "paging": true,
        // "ordering" : false,
        "info": false,
        searching:true,
        lengthChange:false,
        dom: 'Bfrtip',
        "buttons": [{
            'extend': 'excel',
            'text': '导出',//定义导出excel按钮的文字
            'className': 'btn btn-success', //按钮的class样式
            'title': '住宿详情',
            'exportOptions': {
                'modifier': {
                    'page': 'all'
                }
            }
        }],
        "columns": [
            {
                "data": null,
                "title":"序号",
                "width":"8%",
                "defaultContent": "",
                "sClass": "text-center",
                "searchable":false,
                render: function (data, type, row, meta) {
                    var no = meta.settings._iDisplayStart + meta.row + 1;
                    return no;
                }
            }, {
                "data": "employeeNumber",
                "title":"工号",
                "width":"8%",
                "defaultContent": "",
                "sClass": "text-center"
            }, {
                "data": "employeeName",
                "title":"姓名",
                "width":"10%",
                "defaultContent": "",
                "sClass": "text-center"
            }, {
                "data": "groupName",
                "title":"组名",
                "width":"10%",
                "defaultContent": "",
                "sClass": "text-center"
            }, {
                "data": "floor",
                "title": "楼层",
                "width":"10%",
                "defaultContent": "",
                "sClass": "text-center"
            }, {
                "data": "roomNumber",
                "title": "房间号",
                "width":"10%",
                "defaultContent": "",
                "sClass": "text-center"
            }, {
                "data": "bedNumber",
                "title": "床号",
                "width":"10%",
                "defaultContent": "",
                "sClass": "text-center"
            }, {
                "data": "checkInDate",
                "title": "入住日期",
                "width":"10%",
                "defaultContent": "",
                "sClass": "text-center",
                "render": function(data, type, full, meta) {
                    return  moment(data).format("YYYY-MM-DD");
                }
            }, {
                "data": "empRoomID",
                "title": "操作",
                "width":"10%",
                "defaultContent": "",
                "sClass": "text-center"
            }
        ],
        "columnDefs" :
            [{
                "orderable" : false, // 禁用排序
                "targets" : [8], // 指定的列
                "data" : "empRoomID",
                "render" : function(data, type, full, meta) {
                    return "<a href='#' style='color:#ff0000' onclick='deleteEmpRoom("+data+")'>删除</a>";
                }
            }]

    });
    $('#empRoomTable_wrapper .dt-buttons').append("<button class=\"btn btn-success\" style=\"outline:none;margin-left: 10px;\" onclick=\"addEmpRoom()\">入住</button>");
}



function addEmpRoom() {
    layer.open({
        type: 1 //Page层类型
        , title: "添加宿舍"
        , area: '900px'
        , btn: ['保存']
        , shade: 0.6 //遮罩透明度
        , maxmin: false //允许全屏最小化
        , anim: 0 //0-6的动画形式，-1不开启
        , content: $('#editPro')
        , yes: function (index, layero) {
            var flag = false;
            $("#editPro input,select").each(function () {
                if($(this).val().trim() == "") {
                    flag = true;
                    return false;
                }
            })
            if (flag) {
                swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入完全部信息！</span>",html: true});
                return false;
            }
            var empRoom = {};
            empRoom.employeeNumber = $("#employeeNumber").val();
            empRoom.employeeName = $("#employeeName").val();
            empRoom.floor = $("#floor").val();
            empRoom.roomNumber = $("#roomNumber").val();
            empRoom.bedNumber = $("#bedNumber").val();
            empRoom.checkInDate = $("#checkInDate").val();
            $.ajax({
                url: basePath + "erp/addemproom",
                type: 'POST',
                data: {
                    empRoom:JSON.stringify(empRoom)
                },
                success: function (data) {
                    if(data == 0) {
                        layer.closeAll();
                        $("input").val("");
                        swal({
                                type:"success",
                                title:"",
                                text: "<span style=\"font-weight:bolder;font-size: 20px\">恭喜你，保存成功！</span>",
                                html: true
                            },
                            function(){
                                location.href=basePath+"erp/empRoomStart";
                            });
                    }else {
                        swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，保存失败！</span>",html: true});
                    }
                },
                error: function () {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
                }
            });
        }
        ,cancel: function(index, layero){
            layer.closeAll();
            $("input").val("");
        }
    })
}

function deleteEmpRoom(empRoomID) {
    swal({
        title: "",
        text: "<span style=\"font-weight:bolder;font-size: 20px\">您确定要删除吗？</span>",
        type: "warning",
        html:true,
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: "确定",
        cancelButtonText:"我再想想",
        confirmButtonColor: "#ec6c62",
        showLoaderOnConfirm: true
    }, function() {
        $.ajax({
            url: basePath + "erp/deleteemproom",
            type:'POST',
            data: {
                empRoomID:empRoomID
            },
            success: function (data) {
                if(data == 0) {
                    $("input").val("");
                    swal({
                            type:"success",
                            title:"",
                            text: "<span style=\"font-weight:bolder;font-size: 20px\">恭喜你，删除成功！</span>",
                            html: true
                        },
                        function(){
                            location.href=basePath+"erp/empRoomStart";
                        });
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，删除失败！</span>",html: true});
                }
            },
            error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        })
    });
}

