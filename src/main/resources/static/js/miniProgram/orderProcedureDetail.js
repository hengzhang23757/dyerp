var basePath=$("#basePath").val();
var orderName = $("#orderName").val();
var hot;
var cutHeight;
var procedureHeight;
var finishHeight;
$(document).ready(function () {

    var orderName=$("#orderName").val();
    var type=$("#type").val();
    var param = {};
    param.orderName = orderName;
    var url = "getorderproceudrebyorder";
    if(type == "review") {
        url = "getorderprocedurebystate";
        param.procedureStateList = [1];
    }

    $.ajax({
        url: url,
        type:'POST',
        data: param,
        traditional: true,
        success: function (data) {
            // console.log(data);
            var hotData = [];
            var samSum = 0;
            var packageSum = 0;
            var pieceSum = 0;
            var floatSum = 0;
            var hourSam = 0;
            var hourPackage = 0;
            var hourPiece = 0;
            var hourPieceTwo = 0;
            var subsidyPer = 0;
            var cutSum = [[],[],[],[],[],[],[],[],[],[],["裁床合计"],0,0,0,0,[],[],[]];
            var produceSum = [[],[],[],[],[],[],[],[],[],[],["车缝合计"],0,0,0,0,[],[],[]];
            var finishSum = [[],[],[],[],[],[],[],[],[],[],["后整合计"],0,0,0,0,[],[],[]];
            hotData.push(['中山市德悦服饰有限公司','','','','','','','','','']);
            hotData.push(['工序工价表','','','','','','','','','']);
            var heightIndex = 5;
            if(data.orderProcedureList && data.orderProcedureList.length>0) {
                var record = data.orderProcedureList[0];
                var tmp1 = [];
                tmp1[0] = "订单号";
                tmp1[1] = record.orderName;
                tmp1[2] = "款式描述";
                tmp1[3] = record.styleDescription;
                tmp1[4] = "";
                tmp1[5] = "版单号";
                tmp1[6] = record.clothesVersionNumber;
                tmp1[7] = "";
                tmp1[8] = "图片";
                tmp1[9] = "";
                tmp1[10] = "补贴比例：";
                tmp1[11] = "";
                tmp1[12] = "";
                tmp1[13] = "";
                tmp1[14] = "";
                tmp1[15] = "";
                hotData.push(tmp1);
                var tmp2 = [];
                tmp2[0] = "客户";
                tmp2[1] = record.customerName;
                tmp2[2] = "生产数量";
                tmp2[3] = record.orderCount;
                tmp2[4] = "";
                tmp2[5] = "开单日期";
                tmp2[6] = record.beginDate;
                tmp2[7] = "";
                hotData.push(tmp2);
                var tmp3 = ['款式','部位','工序代码','工序号','工序名称','工序描述','备注','码段','设备','等级','工段','SAM值','工价/打','工价/件','浮动','审核状态','计件部位','特殊计件'];
                hotData.push(tmp3);
                var changeIndex = "裁床";
                $.each(data.orderProcedureList,function (index,item) {
                    var tmp = [];
                    if (item.subsidy > 0){
                        subsidyPer = item.subsidy;
                    }
                    tmp[0] = item.styleType;
                    tmp[1] = item.partName;
                    tmp[2] = item.procedureCode;
                    tmp[3] = item.procedureNumber;
                    tmp[4] = item.procedureName;
                    tmp[5] = item.procedureDescription;
                    tmp[6] = item.remark;
                    tmp[7] = item.segment;
                    tmp[8] = item.equType;
                    tmp[9] = item.procedureLevel;
                    tmp[10] = item.procedureSection;
                    tmp[11] = item.sam;
                    tmp[12] = item.packagePrice;
                    tmp[13] = item.piecePrice;
                    tmp[14] = item.piecePriceTwo;
                    if (item.procedureState == 0){
                        tmp[15] = "未提交";
                    }else if (item.procedureState == 1){
                        tmp[15] = "审核中";
                    }else if (item.procedureState == 2){
                        tmp[15] = "审核通过";
                    }else if (item.procedureState == 3){
                        tmp[15] = "未通过";
                    }
                    tmp[16] = item.scanPart;
                    tmp[17] = item.specialProcedure;
                    if (item.procedureSection == "裁床"){
                        changeIndex = "裁床";
                        cutSum[11] += item.sam;
                        cutSum[12] += item.packagePrice;
                        cutSum[13] += item.piecePrice;
                        cutSum[14] += item.piecePriceTwo;
                    }
                    if(item.procedureSection == "车缝"){
                        if (changeIndex == "裁床"){
                            hotData.push([[],[],[],[],[],[],[],[],[],[],["裁床合计"],[toDecimal(cutSum[11])],[toDecimal(cutSum[12])],[toDecimalFour(cutSum[13])],[toDecimalFour(cutSum[14])],[],[],[],[]]);
                            cutHeight = heightIndex;
                        }
                        changeIndex = "车缝";
                        produceSum[11] += item.sam;
                        produceSum[12] += item.packagePrice;
                        produceSum[13] += item.piecePrice;
                        produceSum[14] += item.piecePriceTwo;
                    }
                    if(item.procedureSection == "后整") {
                        if (changeIndex == "车缝"){
                            hotData.push([[],[],[],[],[],[],[],[],[],[],["车缝合计"],[toDecimal(produceSum[11])],[toDecimal(produceSum[12])],[toDecimalFour(produceSum[13])],[toDecimalFour(produceSum[14])],[],[]]);
                            procedureHeight = heightIndex;
                        }
                        changeIndex = "后整";
                        finishSum[11] += item.sam;
                        finishSum[12] += item.packagePrice;
                        finishSum[13] += item.piecePrice;
                        finishSum[14] += item.piecePriceTwo;
                    }
                    if (item.procedureName == "时工"){
                        if (changeIndex == "后整"){
                            hotData.push([[],[],[],[],[],[],[],[],[],[],["后整合计"],[toDecimal(finishSum[11])],[toDecimal(finishSum[12])],[toDecimalFour(finishSum[13])],[toDecimalFour(finishSum[14])],[],[]]);
                            finishHeight = heightIndex;
                        }
                        changeIndex = "时工";
                        hourSam += item.sam;
                        hourPackage += item.packagePrice;
                        hourPiece += item.piecePrice;
                        hourPieceTwo += item.piecePriceTwo;
                    }
                    samSum += tmp[11];
                    packageSum += tmp[12];
                    pieceSum += tmp[13];
                    floatSum += tmp[14];
                    hotData.push(tmp);
                    heightIndex ++;
                })
            }
            if (changeIndex == "后整"){
                hotData.push([[],[],[],[],[],[],[],[],[],[],["后整合计"],[toDecimal(finishSum[11])],[toDecimal(finishSum[12])],[toDecimalFour(finishSum[13])],[toDecimalFour(finishSum[14])],[],[]]);
                finishHeight = heightIndex;
            }
            hotData.push([[],[],[],[],[],[],[],[],[],[],["合计"],[toDecimal(samSum-hourSam)],[toDecimal(packageSum-hourPackage)],[toDecimalFour(pieceSum-hourPiece)],[toDecimalFour(floatSum-hourPieceTwo)],[],[]]);
            hotData.push([[],[],["制表:"],[],[],["复核:"],[],["审批:"],[],[],[],[],[],[],[],[]]);
            hotData[2][11] = ((100*subsidyPer).toFixed(2)).toString() + "%";
            var container = document.getElementById('addOrderExcel');
            var colorRenderer = function (instance, td, row, col, prop, value, cellProperties) {
                Handsontable.renderers.TextRenderer.apply(this, arguments);
                td.style.backgroundColor = '#2BC08B';
            };
            hot = new Handsontable(container, {
                data: hotData,
                fixedRowsTop:5,
                stretchH: 'all',
                autoWrapRow: true,
                height: $(document.body).height() - 200,
                manualRowResize: true,
                manualColumnResize: true,
                rowHeaders: true,
                colHeaders: true,
                autoColumnSize:true,
                dropdownMenu: true,
                contextMenu:true,
                // minRows:35,
                minCols:18,
                colWidths:[60,120,70,60,80,80,80,80,60,60,80,70,70,70,70,70,70,60],
                language:'zh-CN',
                licenseKey: 'non-commercial-and-evaluation',
                mergeCells: [
                    {row:0, col:0, rowspan:1, colspan:17},
                    {row:1, col:0, rowspan:1, colspan:17},
                    {row:2, col:3, rowspan:1, colspan:2},
                    {row:2, col:6, rowspan:1, colspan:2},
                    {row:2, col:9, rowspan:2, colspan:1},
                    {row:2, col:8, rowspan:2, colspan:1},
                    {row:3, col:3, rowspan:1, colspan:2},
                    {row:3, col:6, rowspan:1, colspan:2}
                ],
                cell: [
                    {row: 0, col: 0, className: "htCenter htMiddle"}, // 设置下标为0,0的单元格样式 水平居中、垂直居中
                    {row: 1, col: 0, className: "htCenter htMiddle"} // 设置下标为0,0的单元格样式 水平居中、垂直居中
                ],
                cells: function (row, col, prop) {
                    if (row === cutHeight || row === procedureHeight+1 || row === finishHeight+2) {
                        this.renderer = colorRenderer;
                    }
                },
                columns: [
                    {},
                    {},
                    {},
                    {},
                    {},
                    {},
                    {},
                    {},
                    {},
                    {},
                    {},
                    {},
                    {},
                    {},
                    {},
                    {},
                    {},
                    {}
                ],
                exportFile: true,
                customBorders: [
                    {
                        range: {
                            from: {
                                row: cutHeight,
                                col: 0
                            },
                            to: {
                                row: cutHeight,
                                col: 17
                            }
                        },
                        top: {
                            width: 2,
                            color: '#2BC08B'
                        },
                        left: {
                            width: 2,
                            color: '#2BC08B'
                        },
                        bottom: {
                            width: 2,
                            color: '#2BC08B'
                        },
                        right: {
                            width: 2,
                            color: '#2BC08B'
                        }
                    },{
                        range: {
                            from: {
                                row: procedureHeight+1,
                                col: 0
                            },
                            to: {
                                row: procedureHeight+1,
                                col: 17
                            }
                        },
                        top: {
                            width: 2,
                            color: '#2BC08B'
                        },
                        left: {
                            width: 2,
                            color: '#2BC08B'
                        },
                        bottom: {
                            width: 2,
                            color: '#2BC08B'
                        },
                        right: {
                            width: 2,
                            color: '#2BC08B'
                        }
                    },{
                        range: {
                            from: {
                                row: finishHeight+2,
                                col: 0
                            },
                            to: {
                                row: finishHeight+2,
                                col: 17
                            }
                        },
                        top: {
                            width: 2,
                            color: '#2BC08B'
                        },
                        left: {
                            width: 2,
                            color: '#2BC08B'
                        },
                        bottom: {
                            width: 2,
                            color: '#2BC08B'
                        },
                        right: {
                            width: 2,
                            color: '#2BC08B'
                        }
                    }]
            });
            // var cell1;
            // var cell2;
            // var cell3;
            // for(var col=0; col<13; col++){  // go through each column of the row
            //     cell1 = hot.getCell(cutHeight,col);
            //     cell2 = hot.getCell(procedureHeight+1,col);
            //     cell3 = hot.getCell(finishHeight+2,col);
            //     cell1.style.background = "#2BC08B";
            //     cell2.style.background = "#2BC08B";
            //     cell3.style.background = "#2BC08B";
            // }

        },
        error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }
    })
});


function unPass() {
    swal({
        title: "",
        text: "<span style=\"font-weight:bolder;font-size: 20px\">确定不通过吗？</span>",
        type: "warning",
        html:true,
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: "确认",
        cancelButtonText:"取消",
        confirmButtonColor: "#ec6c62",
        showLoaderOnConfirm: true
    }, function() {
        $.ajax({
            url: "/erp/orderProcedureAudit",
            type:'POST',
            data: {
                orderName:$("#orderName").val(),
                procedureState:3,
            },
            success: function (data) {
                if(data == 0) {
                    swal({
                            type:"success",
                            title:"",
                            text: "<span style=\"font-weight:bolder;color:#F00;font-size: 50px\">提交成功！</span>",
                            html: true
                        },
                        function(){
                            document.getElementById("reviewButton").style.visibility="hidden";
                        });
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，提交失败！</span>",html: true});
                }
            },
            error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        })
    })
}

function getPass() {
    swal({
        title: "",
        text: "<span style=\"font-weight:bolder;font-size: 20px\">确定通过吗？</span>",
        type: "warning",
        html:true,
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: "确认",
        cancelButtonText:"取消",
        confirmButtonColor: "#ec6c62",
        showLoaderOnConfirm: true
    }, function() {
        $.ajax({
            url: "/erp/orderProcedureAudit",
            type:'POST',
            data: {
                orderName:$("#orderName").val(),
                procedureState:2,
            },
            success: function (data) {
                if(data == 0) {
                    swal({
                            type:"success",
                            title:"",
                            text: "<span style=\"font-weight:bolder;color:#F00;font-size: 50px\">提交成功！</span>",
                            html: true
                        },
                        function(){
                            document.getElementById("reviewButton").style.visibility="hidden";
                        });
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，提交失败！</span>",html: true});
                }
            },
            error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        })
    })
}


function exportData() {
    var data = hot.getData();
    var result = [];
    var col = 0;
    $.each(data,function (index, item) {
        // if(item[0]!=null) {
        for(var i=0;i<item.length;i++) {
            if(item[i]==null) {
                item[i] = '';
            }else if(index==1) {
                col++;
            }
        }
        result.push(item);
        // }else {
        //     return false;
        // }
    });
    var orderName = $("#orderName").val();
    var myDate = new Date();
    export2Excel([orderName+'-订单工序表-'+myDate.toLocaleString()],col, result, orderName+'-订单工序表-'+myDate.toLocaleString()+".xls")
}

function toDecimal(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*100)/100;
    return f;
}

function toDecimalFour(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*10000)/10000;
    return f;
}