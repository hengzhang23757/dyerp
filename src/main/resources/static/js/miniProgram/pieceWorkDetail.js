$(document).ready(function () {
    layui.laydate.render({
        elem: '#from',
        trigger: 'click'
    });
    $.ajax({
        url: basePath + "erp/getpieceworktoday",
        type:'GET',
        data: {},
        success: function (data) {
            if(data) {
                createPieceWorkDetailTable(data.pieceWorkToday);
            }else {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，没有查到相关信息！</span>",html: true});
            }
        },
        error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }
    });
    $('#employeeNumber').keyup(function(){
        var keywords = $(this).val();
        if (keywords=='') { $('#employeeNumberTips').hide(); return };
        $.ajax({
            url: "/erp/getemphint",
            data: {"subEmployeeNumber": $(this).val()},
            success:function(data){
                $('#employeeNumberTips').empty().show();
                if (data.employeeNumberList=='')
                {
                    $('#employeeNumberTips').append('<li class="error" style="color:red;list-style-type:none">没有结果</li>');
                }
                $.each(data.employeeNumberList, function(index,element){
                    if(index < 10) {
                        $('#employeeNumberTips').append('<li id="'+element.employeeNumber+'" class="employeeNumber_click_work" style="list-style-type:none">' + element.employeeNumber + '</li>');
                    } else {
                        $('#employeeNumberTips').append('<div style="color:lightgray">更多选项请搜索</div>');
                        return false;
                    }
                })
            },
            error:function(){
                $('#employeeNumberTips').empty().show();
                $('#employeeNumberTips').append('<li style="color:red">搜索发生错误</li>');
            }
        });
        autoComplete(keywords);
    });



    $('#orderName').keyup(function(){
        var keywords = $(this).val();
        if (keywords=='') { $('#orderNameTips').hide(); return };
        $.ajax({
            url: "/erp/getorderhint",
            data: {"subOrderName": $(this).val()},
            success:function(data){
                $('#orderNameTips').empty().show();
                if (data.orderNameList=='')
                {
                    $('#orderNameTips').append('<li class="error" style="color:red;list-style-type:none">没有结果</li>');
                }
                $.each(data.orderNameList, function(index,element){
                    if(index < 10) {
                        $('#orderNameTips').append('<li id="'+element.orderName+'" class="click_work" style="list-style-type:none">' + element.orderName + '</li>');
                    } else {
                        $('#orderNameTips').append('<div style="color:lightgray">更多选项请搜索</div>');
                        return false;
                    }
                })
            },
            error:function(){
                $('#orderNameTips').empty().show();
                $('#orderNameTips').append('<li style="color:red">搜索发生错误</li>');
            }
        });

        autoCompleteAnother(keywords);
    });


    $(document).on('click','.employeeNumber_click_work',function(){
        var word = $(this).attr("id");
        $('#employeeNumber').val(word);
        $('#employeeNumberTips').hide();
        autoComplete(word);
    });

    $("#employeeNumber").blur(function(){
        setTimeout(function(){
            $('#employeeNumberTips').hide();
        },300);
        autoComplete($(this).val());
    });


    $(document).on('click','.click_work',function(){
        var word = $(this).attr("id");
        $('#orderName').val(word);
        $('#orderNameTips').hide();

        autoCompleteAnother(word);
    });

    $("#orderName").blur(function(){
        setTimeout(function(){
            $('#orderNameTips').hide();
        },300);
        autoCompleteAnother($(this).val());
    });

});


function autoComplete(employeeNumber) {
    $.ajax({
        url: "/erp/getempnamebyempnum",
        data: {"employeeNumber": employeeNumber},
        success:function(data){
            $("#employeeName").val(data);
            $("#employeeName").attr("disabled",true);
        },
        error:function(){
        }
    });
    $("#groupName").empty();
    $.ajax({
        url: "/erp/getgroupnamebyempnum",
        data: {"employeeNumber": employeeNumber},
        success:function(data){
            $("#groupName").val(data);
            $("#groupName").attr("disabled",true);
        },
        error:function(){
        }
    });

}


function autoCompleteAnother(orderName) {
    $("#colorName").empty();
    $.ajax({
        url: "/erp/getcolorhint",
        data: {"orderName": orderName},
        success:function(data){
            $("#colorName").empty();
            if (data.colorList) {
                $("#colorName").append("<option value=''>选择颜色</option>");
                $.each(data.colorList, function(index,element){
                    $("#colorName").append("<option value='"+element.colorName+"'>"+element.colorName+"</option>");
                })
            }
        },
        error:function(){
        }
    });
    $("#sizeName").empty();
    $.ajax({
        url: "/erp/getordersizenamesbyorder",
        data: {"orderName": orderName},
        success:function(data){
            $("#sizeName").empty();
            console.log(data.sizeNameList);
            if (data.sizeNameList) {
                $("#sizeName").append("<option value=''>尺码</option>");
                $.each(data.sizeNameList, function(index,element){
                    $("#sizeName").append("<option value='"+element+"'>"+element+"</option>");
                })
            }
        },
        error:function(){
        }
    });
    $("#procedureNumber").empty();
    $.ajax({
        url: "/erp/getprocedureinfobyorder",
        data: {"orderName": orderName},
        success:function(data){
            $("#procedureNumber").empty();
            if (data.procedureInfoList) {
                $("#procedureNumber").append("<option value=''>选择工序</option>");
                $.each(data.procedureInfoList, function(index,element){
                    var procedureNum = element.procedureNumber;
                    var procedureName = element.procedureName;
                    $("#procedureNumber").append("<option value="+procedureNum+">"+procedureNum+"-"+procedureName+"</option>");
                })
            }
        },
        error:function(){
        }
    });
}



var basePath=$("#basePath").val();
var userRole=$("#userRole").val();
var pieceWorkDetailTable;
var myDate = new Date();
function createPieceWorkDetailTable(data){
    if (pieceWorkDetailTable != undefined){
        pieceWorkDetailTable.clear();
        pieceWorkDetailTable.destroy();
    }
    pieceWorkDetailTable = $('#pieceWorkDetailTable').DataTable({
        "data":data,
        language : {
            processing : "载入中",//处理页面数据的时候的显示
            paginate : {//分页的样式文本内容。
                previous : "上一页",
                next : "下一页",
                first : "第一页",
                last : "最后一页"
            },
            search:"搜索：",
            zeroRecords : "没有内容",//table tbody内容为空时，tbody的内容。
            //下面三者构成了总体的左下角的内容。
            info : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//左下角的信息显示，大写的词为关键字。
            infoEmpty : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//筛选为空时左下角的显示。
            infoFiltered : ""//筛选之后的左下角筛选提示(另一个是分页信息显示，在上面的info中已经设置，所以可以不显示)，
        },
        dom: 'Bfrtip',
        "buttons": [{
            'extend': 'excel',
            'text': '导出',//定义导出excel按钮的文字
            'className': 'btn', //按钮的class样式
            'title': '计件明细-查询时间'+myDate.toLocaleString( ),
            'exportOptions': {
                'modifier': {
                    'page': 'all'
                }
            }
        }],
        pageLength : 50,// 每页显示10条数据
        pagingType : "full_numbers", // 分页样式：simple,simple_numbers,full,full_numbers，
        "paging": true,
        "info": false,
        searching:true,
        lengthChange:false,
        scrollX: 2000,
        scrollCollapse: true,
        scroller:       true,
        "columns": [
            {
                "data": null,
                "title":"序号",
                "width":"45px",
                "defaultContent": "",
                "sClass": "text-center",
                "searchable":false,
                render: function (data, type, row, meta) {
                    var no = meta.settings._iDisplayStart + meta.row + 1;
                    return no;
                }
            },{
                "data": "employeeName",
                "title":"姓名",
                "width":"60px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "employeeNumber",
                "title":"工号",
                "width":"60px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "groupName",
                "title":"组名",
                "width":"60px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "orderName",
                "title":"订单号",
                "width":"110px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "bedNumber",
                "title":"床号",
                "width":"60px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "colorName",
                "title":"颜色",
                "width":"60px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "sizeName",
                "title":"尺码",
                "width":"60px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "packageNumber",
                "title":"扎号",
                "width":"60px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "layerCount",
                "title":"数量",
                "width":"45px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "procedureNumber",
                "title":"工序号",
                "width":"45px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "procedureName",
                "title":"工序",
                "width":"90px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "pieceWorkID",
                "title": "操作",
                "width":"60px",
                "defaultContent": "",
                "sClass": "text-center",
            }
        ],
        "columnDefs" :
            [{
                "orderable" : false, // 禁用排序
                "targets" : [12], // 指定的列
                "data" : "pieceWorkID",
                "render" : function(data, type, full, meta) {
                    return "<a href='#' style='color:#ff0000' onclick='deletePieceWorkDetail("+data+")'>删除</a>";
                }
            }],
    });
}


function deletePieceWorkDetail(pieceWorkID) {
    if(userRole=='role5'){
        swal("SORRY!", "对不起，您没有操作权限！", "warning");
        return;
    }
    swal({
        title: "",
        text: "<span style=\"font-weight:bolder;font-size: 20px\">您确定要删除吗？</span>",
        type: "warning",
        html:true,
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: "确定",
        cancelButtonText:"我再想想",
        confirmButtonColor: "#ec6c62",
        showLoaderOnConfirm: true
    }, function() {
        $.ajax({
            url: basePath + "erp/deletepiecework",
            type:'POST',
            data: {
                pieceWorkID:pieceWorkID
            },
            success: function (data) {
                if(data == 0) {
                    $.unblockUI();
                    $("input").val("");
                    swal({
                            type:"success",
                            title:"",
                            text: "<span style=\"font-weight:bolder;font-size: 20px\">恭喜你，删除成功！</span>",
                            html: true
                        },
                        function(){
                            location.href=basePath+"erp/pieceWorkDetailStart";
                        });
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，删除失败！</span>",html: true});
                }
            },
            error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        })
    });
}

function changeTable(obj){
    var opt = obj.options[obj.selectedIndex];
    // if (opt.value == "thisMonth"){
    //     $.ajax({
    //         url: basePath + "erp/getpieceworkthismonth",
    //         type:'GET',
    //         data: {},
    //         success: function (data) {
    //             if(data) {
    //                 createPieceWorkDetailTable(data.pieceWorkThisMonth);
    //             }else {
    //                 swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，没有查到相关信息！</span>",html: true});
    //             }
    //         },
    //         error: function () {
    //             swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
    //         }
    //     })
    // }
    if (opt.value == "today"){
        $.ajax({
            url: basePath + "erp/getpieceworktoday",
            type:'GET',
            data: {},
            success: function (data) {
                if(data) {
                    createPieceWorkDetailTable(data.pieceWorkToday);
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，没有查到相关信息！</span>",html: true});
                }
            },
            error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        })
    }
}

function addPieceWork(){
    if(userRole=='role5'){
        swal("SORRY!", "对不起，您没有操作权限！", "warning");
        return;
    }
    $.blockUI({
        css: {
            width: '30%',
            top: '15%',
            border: 'none',
            padding: '15px',
            backgroundColor: '#fff',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: 1,
            color: '#000'
        },
        message: $('#editPro')
    });
    var url = basePath + "erp/addpieceworktotal";
    $("#editYes").unbind("click").bind("click", function () {
        if($("#employeeNumber").val().trim()=="") {
            swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请填写完整信息！</span>",html: true});
            return false;
        }
        if($("#orderName").val().trim()=="") {
            swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请填写完整信息！</span>",html: true});
            return false;
        }
        if($("#layerCount").val().trim()=="") {
            swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请填写完整信息！</span>",html: true});
            return false;
        }
        if($("#procedureNumber").val().trim()=="") {
            swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请填写完整信息！</span>",html: true});
            return false;
        }
        if($("#from").val().trim()=="") {
            swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请填写完整信息！</span>",html: true});
            return false;
        }
        $.ajax({
            url: url,
            type: 'POST',
            data: {
                employeeNumber:$("#employeeNumber").val(),
                orderName:$("#orderName").val(),
                colorName:$("#colorName").val(),
                sizeName:$("#sizeName").val(),
                layerCount:$("#layerCount").val(),
                procedureNumber:$("#procedureNumber").val(),
                createTime:$("#from").val()
            },
            success: function (data) {
                if(data == 0) {
                    $.unblockUI();
                    $("input").val("");
                    swal({
                            type:"success",
                            title:"",
                            text: "<span style=\"font-weight:bolder;font-size: 20px\">恭喜你，保存成功！</span>",
                            html: true
                        },
                        function(){
                            location.href=basePath+"erp/pieceWorkDetailStart";
                        });
                }else if (data == 1){
                    swal("数量不正确!", "对不起，数量大于裁数！", "warning");
                }else if (data == 2){
                    swal("无裁床记录!", "对不起，请检查颜色尺码是否正确！", "warning");
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，保存失败！</span>",html: true});
                }
            },
            error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        });
    });
    $("#editNo").unbind("click").bind("click", function () {
        $.unblockUI();
        $("input").val("");
    });


}

