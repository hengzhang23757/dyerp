layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.21'
}).extend({                         // 模块别名
    soulTable: 'soulTable',
});
$(document).ready(function () {
    var type = $("#hideType").val();
    layui.laydate.render({
        elem: '#beginDate',
        trigger: 'click'
    });

    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumber", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });

    $("#orderName").bind('blur change',function(){
        var orderName = $("#orderName").val();
        $("#customerName").empty();
        $.ajax({
            url: "/erp/getcustomernamebyordername",
            data: {"orderName": orderName},
            success:function(data){
                $("#customerName").val(data);
            },
            error:function(){
            }
        });
        $("#styleDescription").empty();
        $.ajax({
            url: "/erp/getstyledescriptionbyorder",
            data: {"orderName": orderName},
            success:function(data){
                $("#styleDescription").val(data);
            },
            error:function(){
            }
        });
        $("#orderCount").empty();
        $.ajax({
            url: "/erp/getordertotalcount",
            data: {"orderName": orderName},
            success:function(data){
                $("#orderCount").val(data);
            },
            error:function(){
            }
        });
    });

    $("#clothesVersionNumber").bind('blur change',function(){
        var orderName = $("#orderName").val();
        $("#customerName").empty();
        $.ajax({
            url: "/erp/getcustomernamebyordername",
            data: {"orderName": orderName},
            success:function(data){
                $("#customerName").val(data);
            },
            error:function(){
            }
        });
        $("#styleDescription").empty();
        $.ajax({
            url: "/erp/getstyledescriptionbyorder",
            data: {"orderName": orderName},
            success:function(data){
                $("#styleDescription").val(data);
            },
            error:function(){
            }
        });
        $("#orderCount").empty();
        $.ajax({
            url: "/erp/getordertotalcount",
            data: {"orderName": orderName},
            success:function(data){
                $("#orderCount").val(data);
            },
            error:function(){
            }
        });
    });

    var orderName = $("#hideOrderName").val();
    var selData = [];
    if(type != 'add') {
        $.ajax({
            url: "/erp/getorderproceudrebyorder",
            data: {"orderName": orderName},
            type:'POST',
            success:function(data){
                selData = data.orderProcedureList;
                createOrderProcedureSelTable(selData);
                if(type=='update' && data.orderProcedureList[0]) {
                    var procedureDetail = data.orderProcedureList[0];
                    $("#orderName").val(procedureDetail.orderName);
                    $("#customerName").val(procedureDetail.customerName);
                    $("#styleDescription").val(procedureDetail.styleDescription);
                    $("#clothesVersionNumber").val(procedureDetail.clothesVersionNumber);
                    $("#beginDate").val(procedureDetail.beginDate);
                    $("#orderCount").val(procedureDetail.orderCount);
                    $("#subsidyPer").val(toDecimal(procedureDetail.subsidy)*100);
                }
            },
            error:function(){
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        });
    }else {
        createOrderProcedureSelTable(selData);
    }

    layui.use(['form','table'], function(){
        layui.form.on('switch(specialFilter)', function(data){
            var result = "否";
            if(this.checked) {
                result = "是";
            }
            var index = $(this).parent().parent().parent().attr("data-index");
            orderProcedureSelTable = layui.table.cache.orderProcedureSelTable;
            $.each(orderProcedureSelTable, function(i,element){
                if(index == i) {
                    layui.table.cache.orderProcedureSelTable[i].specialProcedure = result;
                    return false;
                }
            })
        });
    });

    createOrderProcedureTable();

});

var orderProcedureAddTable;
var orderProcedureSelTable;

function createOrderProcedureTable() {
    var type = $("#hideType").val();
    layui.use('table', function(){
        var table = layui.table;
        //展示已知数据
        orderProcedureAddTable = table.render({
            elem: '#orderProcedureAddTable'
            ,loading:true
            , url: "getallproceduretemplate"
            ,cols: [[ //标题栏
                {field: 'procedureID', title: '操作',align: 'center', width: 60, fixed: 'left',templet: function(d){
                        return "<a href='#' style='color:#3f86ff' value='"+d.procedureID+"' lay-event='addProcedure'>添加</a>"
                    }}
                ,{field: 'styleType', title: '款式',align: 'center', width: 100}
                ,{field: 'partName', title: '部位', width: 100,align: 'center'}
                ,{field: 'procedureCode', title: '代码', width: 100,align: 'center'}
                ,{field: 'procedureNumber', title: '工序号', width: 120,align: 'center'}
                ,{field: 'procedureName', title: '工序名', width: 120,align: 'center'}
                ,{field: 'procedureSection', title: '工段', width: 100,align: 'center'}
                ,{field: 'procedureDescription', title: '工序描述', width: 220,align: 'center'}
                ,{field: 'remark', title: '备注', width: 100,align: 'center'}
                ,{field: 'segment', title: '码段', width: 100,align: 'center'}
                ,{field: 'equType', title: '设备', width: 100,align: 'center'}
                ,{field: 'procedureLevel', title: '等级', width: 100,align: 'center'}
                ,{field: 'sam', title: 'SAM', width: 100,align: 'center'}
                ,{field: 'packagePrice', title: '价/打', width: 120,align: 'center'}
                ,{field: 'piecePrice', title: '价/件', width: 120,align: 'center'}
            ]]
            // ,data: data
            ,initSort: {field:'procedureNumber', type:'asc'}
            //,skin: 'line' //表格风格
            // ,page: true //是否显示分页
            // ,even: true
            ,limit: Number.MAX_VALUE //每页默认显示的数量
            ,height: 'full-290'
            ,done: function () {
                $("div[lay-id='orderProcedureAddTable']").css("margin",0);
            }
        });

        table.on('tool(addTableFilter)', function(obj){
            var data = obj.data;
            if (data.procedureSection == "后整"){
                data.scanPart = "成品";
                if (data.procedureNumber == "666" || data.procedureNumber == "777"){
                    data.scanPart = "返工";
                }
                if (data.procedureNumber == "561" || data.procedureNumber == "563" || data.procedureNumber == "6563"){
                    data.scanPart = "包装";
                }
            } else if (data.procedureNumber == "45" || data.procedureNumber == "46"|| data.procedureNumber == "126"|| data.procedureNumber == "121"){
                data.scanPart = "罗纹";
            } else{
                data.scanPart = "主身";
            }

            data.specialProcedure = "否";
            data.piecePriceTwo = 0;

            var dataBak = [];   //定义一个空数组,用来存储之前编辑过的数据已经存放新数据

            var tableBak = table.cache.orderProcedureSelTable;
            var isInsert = true;
            for (var i = 0; i < tableBak.length; i++) {
                if(tableBak[i].procedureNumber!=data.procedureNumber) {
                    dataBak.push(tableBak[i]);      //将之前的数组备份
                }else if((tableBak[i].procedureState==1 || tableBak[i].procedureState==2) && (type != "reAdd")) {
                    dataBak.push(tableBak[i]);      //将之前的数组备份
                    isInsert = false;
                }
            }
            if(isInsert) {
                dataBak.push(data);
            }
            table.reload("orderProcedureSelTable",{
                data:dataBak   // 将新数据重新载入表格
            })
        });


        var tableFilter = layui.tableFilter;

        tableFilter.render({
            'elem' : '#orderProcedureAddTable',//table的选择器
            'mode' : 'local',//过滤模式
            'filters' : [
                {
                    'field': 'styleType',
                    'type':'checkbox'
                },{
                    'field': 'partName',
                    'type':'checkbox'
                },{
                    'field': 'procedureCode',
                    'type':'input'
                },{
                    'field': 'procedureNumber',
                    'type':'input',
                    'order':'asc'
                },{
                    'field': 'procedureName',
                    'type':'input'
                },{
                    'field': 'procedureSection',
                    'type':'checkbox'
                },{
                    'field': 'procedureDescription',
                    'type':'input'
                },{
                    'field': 'remark',
                    'type':'input'
                },{
                    'field': 'segment',
                    'type':'checkbox'
                },{
                    'field': 'equType',
                    'type':'checkbox'
                },{
                    'field': 'procedureLevel',
                    'type':'checkbox'
                },{
                    'field': 'sam',
                    'type':'input'
                },{
                    'field': 'packagePrice',
                    'type':'input'
                },{
                    'field': 'piecePrice',
                    'type':'input'
                }],//过滤项配置
            'done': function(filters){
                //结果回调
            }
        })

    });
}

function createOrderProcedureSelTable(data){
    var type = $("#hideType").val();
    layui.use('table', function(){
        var table = layui.table;
        //展示已知数据
        orderProcedureSelTable = table.render({
            elem: '#orderProcedureSelTable'
            ,cols: [[ //标题栏
                {field: 'styleType', title: '款式',align: 'center',edit: 'text', width: 100}
                ,{field: 'partName', title: '部位', width: 100,align: 'center',edit: 'text'}
                ,{field: 'procedureCode', title: '代码', width: 100,align: 'center', sort: true}
                ,{field: 'procedureNumber', title: '工序号', width: 90,align: 'center',edit: 'text', sort: true}
                ,{field: 'procedureName', title: '工序名', width: 100,align: 'center',edit: 'text'}
                ,{field: 'procedureSection', title: '工段', width: 100,align: 'center',edit: 'text'}
                ,{field: 'procedureDescription', title: '工序描述', width: 220,align: 'center',edit: 'text'}
                ,{field: 'remark', title: '备注', width: 100,align: 'center',edit: 'text'}
                ,{field: 'segment', title: '码段', width: 90,align: 'center',edit: 'text'}
                ,{field: 'equType', title: '设备', width: 80,align: 'center',edit: 'text'}
                ,{field: 'procedureLevel', title: '等级', width: 80,align: 'center',edit: 'text'}
                ,{field: 'sam', title: 'SAM', width: 80,align: 'center'}
                ,{field: 'packagePrice', title: '价/打', width: 90,align: 'center'}
                ,{field: 'piecePrice', title: '价/件', width: 90,align: 'center'}
                ,{field: 'piecePriceTwo', title: '浮动', width: 90,align: 'center', edit: 'text'}
                ,{field: 'scanPart', title: '计件', width: 90,align: 'center',edit: 'text'}
                ,{field: 'specialProcedure', title: '特殊', width: 90,align: 'center',templet: '#switchTpl'}
                ,{field: 'procedureID', title: '操作',align: 'center', width: 60, fixed: 'right',templet: function(d){
                        if((d.procedureState=='1' || d.procedureState=='2') && type != 'reAdd') {
                            return "";
                        }else {
                            return "<a href='#' style='color:#3f86ff' value='" + d.procedureID + "' lay-event='delProcedure' >移除</a>"
                        }
                    }}
            ]]
            ,data: data
            // ,initSort: {field:'timePeriod', type:'desc'}
            //,skin: 'line' //表格风格
            // ,page: true //是否显示分页
            // ,even: true
            ,limit: Number.MAX_VALUE //每页默认显示的数量
            ,height: 'full-290'
            ,done: function (res, curr, count) {
                $("div[lay-id='orderProcedureSelTable']").css("margin",0);
                if(1==2) {
                    for(var i=0;i<res.data.length;i++) {
                        if (res.data[i].procedureState == 1 || res.data[i].procedureState == 2) {
                            $("div[lay-id='orderProcedureSelTable'] .layui-table-body").find("tr").eq(i).find('td[data-field="styleType"]').data('edit', false);
                            $("div[lay-id='orderProcedureSelTable'] .layui-table-body").find("tr").eq(i).find('td[data-field="partName"]').data('edit', false);
                            $("div[lay-id='orderProcedureSelTable'] .layui-table-body").find("tr").eq(i).find('td[data-field="procedureNumber"]').data('edit', false);
                            $("div[lay-id='orderProcedureSelTable'] .layui-table-body").find("tr").eq(i).find('td[data-field="procedureName"]').data('edit', false);
                            $("div[lay-id='orderProcedureSelTable'] .layui-table-body").find("tr").eq(i).find('td[data-field="procedureSection"]').data('edit', false);
                            $("div[lay-id='orderProcedureSelTable'] .layui-table-body").find("tr").eq(i).find('td[data-field="procedureDescription"]').data('edit', false);
                            $("div[lay-id='orderProcedureSelTable'] .layui-table-body").find("tr").eq(i).find('td[data-field="remark"]').data('edit', false);
                            $("div[lay-id='orderProcedureSelTable'] .layui-table-body").find("tr").eq(i).find('td[data-field="segment"]').data('edit', false);
                            $("div[lay-id='orderProcedureSelTable'] .layui-table-body").find("tr").eq(i).find('td[data-field="equType"]').data('edit', false);
                            $("div[lay-id='orderProcedureSelTable'] .layui-table-body").find("tr").eq(i).find('td[data-field="procedureLevel"]').data('edit', false);
                            $("div[lay-id='orderProcedureSelTable'] .layui-table-body").find("tr").eq(i).find('td[data-field="scanPart"]').data('edit', false);
                            $("div[lay-id='orderProcedureSelTable'] .layui-table-body").find("tr").eq(i).find('td[data-field="specialProcedure"]').data('edit', false);
                        }
                    }
                }
            }
        });

        table.on('tool(selTableFilter)', function(obj){
            if(obj.event === 'delProcedure'){
                obj.del();
            }
        });

    });
}

function add(){
    var basePath=$("#basePath").val();
    var userName=$("#userName").val();
    var operateType = $("#hideType").val();
    var subsidyPer = $("#subsidyPer").val();
    var orderProcedureJson = [];
    var selectedProcedures = layui.table.cache.orderProcedureSelTable;
    var orderName = $("#orderName").val();
    if(selectedProcedures.length == 0) {
        swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请选择工序！</span>",html: true});
        return false;
    }
    if($("#clothesVersionNumber").val().trim()=="") {
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入完整信息！</span>",html: true});
        return false;
    }
    if($("#orderName").val()=="" || $("#orderName").val()==null) {
        if ($("#orderCount").val().trim() == 0){
            orderName = $("#clothesVersionNumber").val().trim();
        }else{
            swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入完整信息！</span>",html: true});
            return false;
        }

    }
    if($("#beginDate").val().trim()=="") {
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入完整信息！</span>",html: true});
        return false;
    }
    if($("#orderCount").val().trim()=="") {
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入完整信息！</span>",html: true});
        return false;
    }
    if($("#styleDescription").val().trim()=="") {
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入完整信息！</span>",html: true});
        return false;
    }
    if(subsidyPer == "") {
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入完整信息！</span>",html: true});
        return false;
    }
    if (!(/(^[0-9]\d*$)/.test(subsidyPer))){
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">补贴比例输入有误！</span>",html: true});;
        return false;
    }

    // var orderName = $("#orderName").val();
    var customerName = $("#customerName").val();
    var styleDescription = $("#styleDescription").val();
    var subsidy = (subsidyPer/100).toFixed(2);
    console.log(subsidy);
    var beginDate = $("#beginDate").val();
    var orderCount = $("#orderCount").val();
    var clothesVersionNumber = $("#clothesVersionNumber").val();
    var procedureNumberList = [];
    var isRepeat = false;
    var priceFloat = false;
    $.each(selectedProcedures,function (index,item) {
        if(item.length!=0) {
            var orderProcedure = {};
            orderProcedure.orderName = orderName;
            orderProcedure.customerName = customerName;
            orderProcedure.styleDescription = styleDescription;
            orderProcedure.beginDate = beginDate;
            orderProcedure.orderCount = orderCount;
            orderProcedure.clothesVersionNumber = clothesVersionNumber;

            orderProcedure.styleType = item.styleType;
            orderProcedure.partName = item.partName;
            orderProcedure.procedureCode = item.procedureCode;
            orderProcedure.procedureNumber = item.procedureNumber;
            orderProcedure.procedureName = item.procedureName;
            orderProcedure.procedureSection = item.procedureSection;
            orderProcedure.procedureDescription = item.procedureDescription;
            orderProcedure.remark = item.remark;
            orderProcedure.segment = item.segment;
            orderProcedure.equType = item.equType;
            orderProcedure.procedureLevel = item.procedureLevel;
            orderProcedure.SAM = item.sam;
            orderProcedure.packagePrice = item.packagePrice;
            orderProcedure.piecePrice = item.piecePrice;
            if(item.piecePriceTwo == "" || item.piecePriceTwo == null) {
                orderProcedure.piecePriceTwo = 0;
            }else if (!(/^[0-9]+.?[0-9]*$/.test(item.piecePriceTwo))){
                priceFloat = true;
            }else{
                orderProcedure.piecePriceTwo = item.piecePriceTwo;
            }
            orderProcedure.orderProcedureID = item.procedureID;
            if (item.scanPart == "" || item.scanPart == null){
                orderProcedure.scanPart = "主身";
            }else {
                orderProcedure.scanPart = item.scanPart.replace(/\ +/g,"").replace(/[\r\n]/g,"");
            }
            orderProcedure.specialProcedure = item.specialProcedure;
            orderProcedure.procedureState = 0;
            orderProcedure.subsidy = subsidy;

            if (procedureNumberList.indexOf(item.procedureNumber) != -1) {
                isRepeat = true;
                return false;
            }
            procedureNumberList.push(item.procedureNumber);

            orderProcedureJson.push(orderProcedure);
        }
    });

    if(isRepeat) {
        swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">已选工序中存在工序号相同的工序！</span>",html: true});
        return false;
    }
    if (priceFloat){
        swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">浮动工价有误！</span>",html: true});
        return false;
    }
    if (operateType == "update"){
        $.ajax({
            url: basePath + "erp/updateorderprocedurebatch",
            type:'POST',
            data:{
                orderProcedureJson:JSON.stringify(orderProcedureJson),
                userName:userName
            },
            success:function(data)
            {
                if(data == 0) {
                    swal({type:"success",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">添加成功！</span>",html: true});
                    layui.table.reload("orderProcedureSelTable",{
                        data:[]   // 将新数据重新载入表格
                    });
                    $("#baseInfo input").val("");
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，添加失败！</span>",html: true});
                }
            },
            error:function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        });
    }else {
        $.ajax({
            url: basePath + "erp/addorderprocedurebatch",
            type:'POST',
            data:{
                orderProcedureJson:JSON.stringify(orderProcedureJson),
                userName:userName
            },
            success:function(data)
            {
                if(data == 0) {
                    swal({type:"success",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">添加成功！</span>",html: true});
                    layui.table.reload("orderProcedureSelTable",{
                        data:[]   // 将新数据重新载入表格
                    });
                    $("#baseInfo input").val("");
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，添加失败！</span>",html: true});
                }
            },
            error:function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        });
    }
}

function toDecimal(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*100)/100;
    return f;
}