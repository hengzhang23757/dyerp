var hot;
$(document).ready(function () {

    layui.laydate.render({
        elem: '#from',
        trigger: 'click'
    });
    layui.laydate.render({
        elem: '#to',
        trigger: 'click'
    });

    layui.use(['autocomplete'], function () {
        layui.autocomplete.render({
            elem: $('#clothesVersionNumber')[0],
            url: 'getversionhint',
            template_val: '{{d}}',
            template_txt: '{{d}}',
            onselect: function (resp) {
                autoComplete(resp);
            }
        })
    });
});

function autoComplete(keywords) {
    $("#orderName").empty();
    $.ajax({
        url: "/erp/getorderbyversion",
        data: {"clothesVersionNumber": keywords},
        success:function(data){
            $("#orderName").empty();
            if (data.orderList) {
                $("#orderName").append("<option value=''>订单号</option>");
                $.each(data.orderList, function(index,element){
                    $("#orderName").append("<option value='"+element+"'>"+element+"</option>");
                })
            }
        },
        error:function(){
        }
    });
}

var basePath=$("#basePath").val();

function search() {
    var container = document.getElementById('reportExcel');
    if (hot==undefined){
        hot = new Handsontable(container, {
            // data: data,
            rowHeaders: true,
            colHeaders: true,
            autoColumnSize: true,
            autoRowSize: true,
            dropdownMenu: true,
            contextMenu: true,
            filters:true,
            //minRows: 20,
            //minCols: 18,
            // colWidths:70,
            language: 'zh-CN',
            licenseKey: 'non-commercial-and-evaluation'
        });
    }
    if($("#orderName").val().trim()=="") {
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入订单号！</span>",html: true});
        return false;
    }

    $.ajax({
        url: basePath + "erp/getorderhourempdetail",
        type:'GET',
        data: {
            orderName:$("#orderName").val(),
            from:$("#from").val(),
            to:$("#to").val(),
        },
        success: function (data) {
            $("#exportDiv").show();
            var hotData = [["版单号","订单号","姓名","工号","组名","工序号","数量"]];
            var clothesVersionNumber = $("#clothesVersionNumber").val();
            var i = 1;
            var sumPieceCount = 0;
            if(data.hourEmpList) {
                $.each(data.hourEmpList,function (index,item) {
                    var tmp = [];
                    tmp[0] = clothesVersionNumber;
                    tmp[1] = item.orderName;
                    tmp[2] = item.employeeName;
                    tmp[3] = item.employeeNumber;
                    tmp[4] = item.groupName;
                    tmp[5] = item.procedureNumber;
                    tmp[6] = toDecimal(item.layerCount);
                    sumPieceCount += tmp[6];
                    hotData[i] = tmp;
                    i++;
                })
            }
            hotData[i+1] = [[],[],[],[],[],["总数"],toDecimal(sumPieceCount)];
            hot.loadData(hotData);
        },
        error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }
    })
}


function exportData() {
    var orderName = $("#orderName").val();
    var versionNumber = $("#clothesVersionNumber").val();
    var myDate = new Date();
    var title = orderName+'-'+versionNumber+'-时工汇总-'+myDate.toLocaleString();
    var data = utl.Object.copyJson(hot.getData());//<=====读取handsontable的数据
    var result = [];
    var firstRow = new Array(data[0].length);
    firstRow[0] = title;
    for(var i=1;i<data[0].length;i++) {
        firstRow[i] = "";
    }
    result.push(firstRow);
    var col = 0;
    $.each(data,function (index, item) {
        for(var i=0;i<item.length;i++) {
            if(item[i]==null) {
                item[i] = '';
            }else if(index==1) {
                col++;
            }
        }
        result.push(item);
    });
    utl.XLSX.onExport(result,"Sheet1","xlsx",title+".xlsx");//<====导出
}



function toDecimal(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*100)/100;
    return f;
}