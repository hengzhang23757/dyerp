var basePath=$("#basePath").val();
var userRole=$("#userRole").val();
var userName=$("#userName").val();
$(document).ready(function () {
    $.ajax({
        url: basePath + "erp/getuniqueorderprocedure",
        type:'GET',
        data: {},
        success: function (data) {
            if(data) {
                createOrderProcedureTable(data.orderProcedureList);
            }else {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，没有查到相关信息！</span>",html: true});
            }
        },
        error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }
    });
    $('#mainFrameTabs').bTabs();

    $('#orderListTab').click(function(){
        $.ajax({
            url: basePath + "erp/getuniqueorderprocedure",
            type:'GET',
            data: {},
            success: function (data) {
                if(data) {
                    createOrderProcedureTable(data.orderProcedureList);
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，没有查到相关信息！</span>",html: true});
                }
            },
            error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        });
    });
});

var orderProcedureTable;
var myDate = new Date();
function createOrderProcedureTable(data) {
    if (orderProcedureTable != undefined) {
        orderProcedureTable.clear(); //清空一下table
        orderProcedureTable.destroy(); //还原初始化了的datatable
    }
    var visible = false;
    if (userRole == 'root' || userRole == 'role5' || userRole == 'role10'){
        visible = true;
    }
    orderProcedureTable = $('#orderProcedureTable').DataTable({
        "data":data,
        "retrieve": true,
        language : {
            processing : "载入中",//处理页面数据的时候的显示
            paginate : {//分页的样式文本内容。
                previous : "上一页",
                next : "下一页",
                first : "第一页",
                last : "最后一页"
            },
            search:"搜索：",
            zeroRecords : "没有内容",//table tbody内容为空时，tbody的内容。
            //下面三者构成了总体的左下角的内容。
            info : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//左下角的信息显示，大写的词为关键字。
            infoEmpty : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//筛选为空时左下角的显示。
            infoFiltered : ""//筛选之后的左下角筛选提示(另一个是分页信息显示，在上面的info中已经设置，所以可以不显示)，
        },
        dom: 'Bfrtip',
        "buttons": [{
            'extend': 'excel',
            'text': '导出',//定义导出excel按钮的文字
            'className': 'btn', //按钮的class样式
            'title': '订单工序'+myDate.toLocaleString( ),
            'exportOptions': {
                'modifier': {
                    'page': 'all'
                }
            }
        }],
        pageLength : 20,// 每页显示10条数据
        pagingType : "full_numbers", // 分页样式：simple,simple_numbers,full,full_numbers，
        "paging": true,
        // "ordering" : false,
        "info": true,
        searching:true,
        lengthChange:false,
        dom: 'Bfrtip',
        "buttons": [{
            'extend': 'excel',
            'text': '导出',//定义导出excel按钮的文字
            'className': 'btn btn-success', //按钮的class样式
            'title': "订单工序表",
            'exportOptions': {
                'modifier': {
                    'page': 'all'
                }
            }
        }],
        "order": [[ 7, 'desc' ]],
        "columns": [
            {
                "data": "orderName",
                "title":"订单号",
                "width":"15%",
                "defaultContent": "",
                "sClass": "text-center",
            },{
                "data": "clothesVersionNumber",
                "title":"版单号",
                "width":"15%",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "customerName",
                "title":"客户名",
                "width":"10%",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "styleDescription",
                "title":"款式描述",
                "width":"20%",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "beginDate",
                "title":"开单日期",
                "width":"10%",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "reviewState",
                "title": "操作",
                "width":"18%",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "reviewState",
                "title": "审核",
                "width":"10%",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "createTime",
                "title": "最后修改日期",
                "width":"10%",
                "defaultContent": "",
                "sClass": "text-center",
            }
        ],
        "columnDefs" :
            [{
                "orderable" : false, // 禁用排序
                "targets" : [5], // 指定的列
                "data" : "reviewState",
                "render" : function(data, type, full, meta) {
                    if (data==0 || data==3 || data==4){
                        return "<a href='#' style='color:#3e8eea' onclick=orderDetail('"+full.orderName+"')>订单详情</a>&nbsp;&nbsp;<a href='#' style='color:#3e8eea' onclick=deleteOrder('"+full.orderName+"')>删除</a>&nbsp;&nbsp;<a href='#' style='color:#3e8eea' onclick=addOrder('"+full.orderName+"')>新增</a>&nbsp;&nbsp;<a href='#' style='color:#3e8eea' onclick=updateOrder('"+full.orderName+"')>修改</a>";
                    }else if (data==1 || data==2 || data==7){
                        return "<a href='#' style='color:#3e8eea' onclick=orderDetail('"+full.orderName+"')>订单详情</a>&nbsp;&nbsp;删除&nbsp;&nbsp;<a href='#' style='color:#3e8eea' onclick=addOrder('"+full.orderName+"')>新增</a>&nbsp;&nbsp;修改";
                    }else if(data==5 || data==6 || data==8 || data==9 || data==10 || data==11) {
                        return "<a href='#' style='color:#3e8eea' onclick=orderDetail('"+full.orderName+"')>订单详情</a>&nbsp;&nbsp;删除&nbsp;&nbsp;<a href='#' style='color:#3e8eea' onclick=addOrder('"+full.orderName+"')>新增</a>&nbsp;&nbsp;<a href='#' style='color:#3e8eea' onclick=updateOrder('"+full.orderName+"')>修改</a>";
                    }

                }
            }, {
                "orderable" : false, // 禁用排序
                "targets" : [6], // 指定的列
                "visible" : visible,
                "data" : "reviewState",
                "render" : function(data, type, full, meta) {
                    if (data == 0){
                        if(userRole == 'role5'){
                            return "<a href='#' style='color:#3e8eea' onclick=commitReview('"+full.orderName+"')>提交审核</a>";
                        }else{
                            return "未提交";
                        }
                    }else if (data == 1){
                        if (userRole == 'role10'){
                            return "<a href='#' style='color:#ec6c62' onclick=reviewProcedure('"+full.orderName+"')>待审核</a>";
                        }else{
                            return "已提交";
                        }
                    }else if (data == 2){
                        if (userRole == 'role10'){
                            return "<a href='#' style='color:#3e8eea' onclick=returnReview('"+full.orderName+"')>反审核</a>";
                        }else {
                            return "审核通过";
                        }
                    }else if (data == 3){
                        if(userRole == 'role5') {
                            return "<p style='color: red; margin-bottom: 0;'>未通过</p>，<a href='#' style='color:#3e8eea' onclick=commitReview('" + full.orderName + "')>提交审核</a>";
                        }else {
                            return "<p style='color: red; margin-bottom: 0;'>未通过</p>";
                        }
                    }else if(data == 4){
                        if(userRole == 'role5') {
                            return "<p style='color: red; margin-bottom: 0;'>部分未通过</p>，<a href='#' style='color:#3e8eea' onclick=commitReview('" + full.orderName + "')>提交审核</a>";
                        }else {
                            return "未提交";
                        }
                    }else if(data == 5){
                        if(userRole == 'role5') {
                            return "部分已提交，<a href='#' style='color:#3e8eea' onclick=commitReview('" + full.orderName + "')>提交审核</a>";
                        }else {
                            return "<a href='#' style='color:#ec6c62' onclick=reviewProcedure('"+full.orderName+"')>待审核</a>";
                        }
                    }else if(data == 6){
                        if(userRole == 'role5') {
                            return "部分已通过，<a href='#' style='color:#3e8eea' onclick=commitReview('" + full.orderName + "')>提交审核</a>";
                        }else {
                            return "<a href='#' style='color:#3e8eea' onclick=returnReview('"+full.orderName+"')>反审核</a>";
                        }
                    }else if(data == 7){
                        if(userRole == 'role5') {
                            return "部分已提交，部分已通过";
                        }else {
                            return "<a href='#' style='color:#ec6c62' onclick=reviewProcedure('"+full.orderName+"')>待审核</a>，<a href='#' style='color:#3e8eea' onclick=returnReview('"+full.orderName+"')>反审核</a>";
                        }
                    }else if(data == 8){
                        if(userRole == 'role5') {
                            return "<p style='color: red; margin-bottom: 0;'>部分未通过</p>，部分已提交，<a href='#' style='color:#3e8eea' onclick=commitReview('" + full.orderName + "')>提交审核</a>";
                        }else {
                            return "<a href='#' style='color:#ec6c62' onclick=reviewProcedure('"+full.orderName+"')>待审核</a>";
                        }
                    }else if(data == 9){
                        if(userRole == 'role5') {
                            return "部分已通过，部分未通过，<a href='#' style='color:#3e8eea' onclick=commitReview('" + full.orderName + "')>提交审核</a>";
                        }else {
                            return "<a href='#' style='color:#3e8eea' onclick=returnReview('"+full.orderName+"')>反审核</a>";
                        }
                    }else if(data == 10){
                        if(userRole == 'role5') {
                            return "部分已提交，部分已通过，<a href='#' style='color:#3e8eea' onclick=commitReview('" + full.orderName + "')>提交审核</a>";
                        }else {
                            return "<a href='#' style='color:#ec6c62' onclick=reviewProcedure('"+full.orderName+"')>待审核</a>，<a href='#' style='color:#3e8eea' onclick=returnReview('"+full.orderName+"')>反审核</a>";
                        }
                    }else if(data == 11){
                        if(userRole == 'role5') {
                            return "部分已提交，部分已通过，<p style='color: red; margin-bottom: 0;'>部分未通过</p>，<a href='#' style='color:#3e8eea' onclick=commitReview('" + full.orderName + "')>提交审核</a>";
                        }else {
                            return "<a href='#' style='color:#ec6c62' onclick=reviewProcedure('"+full.orderName+"')>待审核</a>，<a href='#' style='color:#3e8eea' onclick=returnReview('"+full.orderName+"')>反审核</a>";
                        }
                    }

                }
            },{"targets":7,"visible":false}]
    });
    $('#orderProcedureTable_wrapper .dt-buttons').append("<button class=\"btn btn-s-lg  btn-success\" style=\"outline:none;margin-left: 10px\" onclick=\"addOrderProcedure()\">添加订单工序</button>");
}


function deleteOrder(orderName) {
    if(userRole!='root' && userRole!='role5'){
        swal("SORRY!", "对不起，您没有操作权限！", "warning");
        return;
    }
    swal({
        title: "",
        text: "<span style=\"font-weight:bolder;font-size: 20px\">您确定要删除吗？</span>",
        type: "warning",
        html:true,
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: "确定",
        cancelButtonText:"我再想想",
        confirmButtonColor: "#ec6c62",
        showLoaderOnConfirm: true
    }, function() {
        $.ajax({
            url: basePath + "erp/deleteorderprocedurebyname",
            type:'POST',
            data: {
                orderName:orderName,
                userName:userName,
            },
            success: function (data) {
                if(data == 0) {
                    swal({
                            type:"success",
                            title:"",
                            text: "<span style=\"font-weight:bolder;font-size: 20px\">恭喜你，删除成功！</span>",
                            html: true,
                        },
                        function(){
                            location.href="/erp/orderProcedureStart";
                        });
                    // $.ajax({
                    //     url: basePath + "erp/getuniqueorderprocedure",
                    //     type:'GET',
                    //     data: {},
                    //     success: function (data) {
                    //         if(data) {
                    //             createOrderProcedureTable(data.orderProcedureList);
                    //         }else {
                    //             swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，没有查到相关信息！</span>",html: true});
                    //         }
                    //     },
                    //     error: function () {
                    //         swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
                    //     }
                    // });
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，删除失败！</span>",html: true});
                }
            },
            error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        })
    });
}

var tabId = 1;
function addOrderProcedure() {
    if(userRole!='root' && userRole!='role5'){
        swal("SORRY!", "对不起，您没有操作权限！", "warning");
        return;
    }
    $('#mainFrameTabs').bTabsAdd("tabId" + tabId, "工序录入", "/erp/addOrderProcedureStart?type=add");
    tabId++;
    // calcHeight();
}

function addOrder(orderName) {
    if(userRole!='root' && userRole!='role5'){
        swal("SORRY!", "对不起，您没有操作权限！", "warning");
        return;
    }
    var urlName = encodeURIComponent(orderName);
    var tabName = orderName.replace(/[&\|\\\*^%$#@\-+~`！!……——=「」{};:：；‘’\"\"/<>.,，。（）()《》]/g,"");
    $('#mainFrameTabs').bTabsAdd("tabIdAdd" + tabName, "新增工序", "/erp/addOrderProcedureStart?type=reAdd&orderName="+urlName);
}

function updateOrder(orderName) {
    if(userRole!='root' && userRole!='role5'){
        swal("SORRY!", "对不起，您没有操作权限！", "warning");
        return;
    }
    var urlName = encodeURIComponent(orderName);
    var tabName = orderName.replace(/[&\|\\\*^%$#@\-+~`！!……——=「」{};:：；‘’\"\"/<>.,，。（）()《》]/g,"");
    $('#mainFrameTabs').bTabsAdd("tabIdUpate" + tabName, "修改工序", "/erp/addOrderProcedureStart?type=update&orderName="+urlName);
    tabId++;
}

function orderDetail(orderName){
    var urlName = encodeURIComponent(orderName);
    var tabName = orderName.replace(/[&\|\\\*^%$#@\-+~`！!……——=「」{};:：；‘’\"\"/<>.,，。（）()《》]/g,"");
    $('#mainFrameTabs').bTabsAdd("tabId" + tabName, "订单详情", "/erp/orderProcedureDetailStart?orderName="+urlName);
}

function reviewProcedure(orderName) {
    var urlName = encodeURIComponent(orderName);
    var tabName = orderName.replace(/[&\|\\\*^%$#@\-+~`！!……——=「」{};:：；‘’\"\"/<>.,，。（）()《》]/g,"");
    $('#mainFrameTabs').bTabsAdd("tabIdReview" + tabName, "审核订单详情", "/erp/orderProcedureDetailStart?orderName="+urlName+"&type=review");
}


function commitReview(orderName) {
    $.ajax({
        url:"getorderprocedurebystate",
        data: {
            orderName:orderName,
            procedureStateList:[0,1,2,3],
        },
        type:"POST",
        traditional: true,
        success: function (data) {
            createOrderProcedureAuditTable(data.orderProcedureList,1);
            var index = layer.open({
                type: 1 //Page层类型
                , title: '待审核工序列表'
                , btn: ['确定']
                ,area: '900px'
                , shade: 0.6 //遮罩透明度
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , content: $('#proceduteAuditDiv')
                ,yes: function(index, layero){
                    var selectList = [];
                    $("#orderProcedureAuditTable tbody input[name='checkBtn']").each(function(){
                        if($(this).is(':checked')) {
                            selectList.push($(this).val());
                        }
                    });
                    if(selectList.length == 0) {
                        swal("SORRY!", "请先选择要审核的数据！", "error");
                        return;
                    }
                    swal({
                        title: "",
                        text: "<span style=\"font-weight:bolder;font-size: 20px\">确定提交审核吗？</span>",
                        type: "warning",
                        html:true,
                        showCancelButton: true,
                        closeOnConfirm: false,
                        confirmButtonText: "确定",
                        cancelButtonText:"取消",
                        confirmButtonColor: "#ec6c62",
                        showLoaderOnConfirm: true
                    }, function() {
                        $.ajax({
                            url: "changeprocedurestatebatch",
                            type:'POST',
                            data: {
                                orderName:orderName,
                                procedureState:1,
                                procedureNumberList:selectList
                            },
                            traditional: true,
                            success: function (data) {
                                if(data == 0) {
                                    swal({
                                            type:"success",
                                            title:"",
                                            text: "<span style=\"font-weight:bolder;color:#F00;font-size: 50px\">提交成功！</span>",
                                            html: true
                                        },
                                        function(){
                                            location.href=basePath+"erp/orderProcedureStart";
                                        });

                                }else {
                                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，提交失败！</span>",html: true});                                        }
                            },
                            error: function () {
                                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
                            }
                        })
                    })
                }
                ,cancel: function(index, layero){
                    layer.closeAll();
                }
            })
        },
        error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">获取待审核列表失败～</span>",html: true});
        }
    })
}

var orderProcedureAuditTable;
function createOrderProcedureAuditTable(data, type) {
    if (orderProcedureAuditTable != undefined) {
        orderProcedureAuditTable.clear(); //清空一下table
        orderProcedureAuditTable.destroy(); //还原初始化了的datatable
    }
    orderProcedureAuditTable = $('#orderProcedureAuditTable').DataTable({
        "data":data,
        "retrieve": true,
        language : {
            search:"搜索：",
            zeroRecords : "没有内容",//table tbody内容为空时，tbody的内容。
        },
        "paging": false,
        "info": false,
        searching:true,
        lengthChange:false,
        ordering:false,
        "columns": [
            {
                "data": "procedureNumber",
                "title":"<input type='checkbox'>",
                "width":"5%",
                "defaultContent": "",
                "sClass": "text-center",
            },{
                "data": "procedureNumber",
                "title":"工序号",
                "width":"10%",
                "defaultContent": "",
                "sClass": "text-center",
                "ordering":true
            },{
                "data": "procedureName",
                "title":"工序名",
                "width":"10%",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "procedureDescription",
                "title":"工序描述",
                "width":"20%",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "procedureSection",
                "title":"工段",
                "width":"10%",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "sam",
                "title":"SAM值",
                "width":"10%",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "packagePrice",
                "title":"工价/打",
                "width":"10%",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "piecePrice",
                "title":"工价/件",
                "width":"10%",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "piecePriceTwo",
                "title":"浮动",
                "width":"8%",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "procedureState",
                "title":"审核状态",
                "width":"15%",
                "defaultContent": "",
                "sClass": "text-center",
            }
        ],
        "columnDefs" :
            [{
                "orderable" : false, // 禁用排序
                "targets" : [0], // 指定的列
                "data" : "procedureNumber",
                "render" : function(data, type, full, meta) {
                    if ((full.procedureState == 1 || full.procedureState == 2) && type == 1){
                        return "";
                    }else{
                        return "<input name='checkBtn' type='checkbox' value='"+data+"'>";
                    }

                }
            }, {
                "orderable" : false, // 禁用排序
                "targets" : [9], // 指定的列
                "data" : "procedureState",
                "render" : function(data, type, full, meta) {
                    if(data == 0) {
                        return "未提交"
                    }else if(data == 3){
                        return "未通过"
                    }else if(data == 2) {
                        return "审核通过"
                    }else if(data == 1) {
                        return "提交审核"
                    }
                }
            }
            ]
    });
}

function returnReview(orderName) {
    $.ajax({
        url:"getorderprocedurebystate",
        data: {
            orderName:orderName,
            procedureStateList:[2],
        },
        type:"POST",
        traditional: true,
        success: function (data) {
            createOrderProcedureAuditTable(data.orderProcedureList,2);
            var index = layer.open({
                type: 1 //Page层类型
                , title: '反审核工序列表'
                , btn: ['确定']
                ,area: '900px'
                , shade: 0.6 //遮罩透明度
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , content: $('#proceduteAuditDiv')
                ,yes: function(index, layero){
                    var selectList = [];
                    $("#orderProcedureAuditTable tbody input[name='checkBtn']").each(function(){
                        if($(this).is(':checked')) {
                            selectList.push($(this).val());
                        }
                    });
                    if(selectList.length == 0) {
                        swal("SORRY!", "请先选择要反审核的数据！", "error");
                        return;
                    }
                    swal({
                        title: "",
                        text: "<span style=\"font-weight:bolder;font-size: 20px\">确定提交反审核吗？</span>",
                        type: "warning",
                        html:true,
                        showCancelButton: true,
                        closeOnConfirm: false,
                        confirmButtonText: "确定",
                        cancelButtonText:"取消",
                        confirmButtonColor: "#ec6c62",
                        showLoaderOnConfirm: true
                    }, function() {
                        $.ajax({
                            url: "changeprocedurestatebatch",
                            type:'POST',
                            data: {
                                orderName:orderName,
                                procedureState:0,
                                procedureNumberList:selectList
                            },
                            traditional: true,
                            success: function (data) {
                                if(data == 0) {
                                    swal({
                                            type:"success",
                                            title:"",
                                            text: "<span style=\"font-weight:bolder;color:#F00;font-size: 50px\">提交成功！</span>",
                                            html: true
                                        },
                                        function(){
                                            location.href=basePath+"erp/orderProcedureStart";
                                        });

                                }else {
                                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，提交失败！</span>",html: true});                                        }
                            },
                            error: function () {
                                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
                            }
                        })
                    })
                }
                ,cancel: function(index, layero){
                    layer.closeAll();
                }
            })
        },
        error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">获取待审核列表失败～</span>",html: true});
        }
    })
}

function compare(property) {
    return function (a, b) {
        var value1 = a[property];
        var value2 = b[property];
        return value2 - value1;
    }
}