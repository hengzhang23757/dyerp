layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.21'
}).extend();
$(document).ready(function () {
    layui.laydate.render({
        elem: '#from',
        trigger: 'click'
    });
    layui.laydate.render({
        elem: '#from1',
        trigger: 'click'
    });
    $.blockUI({ css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        },
        message: "<h3>稍等一下... 奋力搜索中</h3>"
    });
    $.ajax({
        url: basePath + "erp/gettodayhouremp",
        type:'GET',
        data: {},
        success: function (data) {
            if(data) {
                createPieceWorkDetailTable(data.todayHourEmp);
                $.unblockUI();
            }else {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，没有查到相关信息！</span>",html: true});
            }
        },
        error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }
    });

    layui.use(['autocomplete'], function () {
        layui.autocomplete.render({
            elem: $('#employeeName')[0],
            url: 'getempnamehint',
            template_val: '{{d}}',
            template_txt: '{{d}}',
            onselect: function (resp) {
                autoComplete(resp)
            }
        })
    });

    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumber", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });

    $("#employeeNumber").change(function () {
        var employeeNumber = $("#employeeNumber").val();
        $.ajax({
            url: "/erp/getgroupnamebyempnum",
            data: {"employeeNumber": employeeNumber},
            success:function(data){
                if (data) {
                    $("#groupName").val(data);
                    $("#groupName").attr("disabled",true);
                }
            },
            error:function(){
            }
        });
    });

});


function autoComplete(employeeName) {
    $.ajax({
        url: "/erp/getempnumbersbyname",
        data: {"employeeName": employeeName},
        success:function(data){
            $("#employeeNumber").empty();
            if (data.employeeNumberList) {
                $("#employeeNumber").append("<option value=''>工号</option>");
                $.each(data.employeeNumberList, function(index,element){
                    $("#employeeNumber").append("<option value='"+element+"'>"+element+"</option>");
                })
            }
        },
        error:function(){
        }
    });

}


var basePath=$("#basePath").val();
var userRole=$("#userRole").val();
var userName=$("#userName").val();
var pieceWorkDetailTable;
var myDate = new Date();
function createPieceWorkDetailTable(data){
    if (pieceWorkDetailTable != undefined){
        pieceWorkDetailTable.clear();
        pieceWorkDetailTable.destroy();
    }
    pieceWorkDetailTable = $('#pieceWorkDetailTable').DataTable({
        "data":data,
        language : {
            processing : "载入中",//处理页面数据的时候的显示
            paginate : {//分页的样式文本内容。
                previous : "上一页",
                next : "下一页",
                first : "第一页",
                last : "最后一页"
            },
            search:"搜索：",
            zeroRecords : "没有内容",//table tbody内容为空时，tbody的内容。
            //下面三者构成了总体的左下角的内容。
            info : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//左下角的信息显示，大写的词为关键字。
            infoEmpty : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//筛选为空时左下角的显示。
            infoFiltered : ""//筛选之后的左下角筛选提示(另一个是分页信息显示，在上面的info中已经设置，所以可以不显示)，
        },
        dom: 'Bfrtip',
        "buttons": [{
            'extend': 'excel',
            'text': '导出',//定义导出excel按钮的文字
            'className': 'btn btn-success', //按钮的class样式
            'title': '时工数据-查询时间'+myDate.toLocaleString( ),
            'exportOptions': {
                'modifier': {
                    'page': 'all'
                }
            }
        }],
        pageLength : 15,// 每页显示10条数据
        pagingType : "full_numbers", // 分页样式：simple,simple_numbers,full,full_numbers，
        "paging": true,
        "info": false,
        searching:true,
        lengthChange:false,
        scrollX: 2000,
        scrollCollapse: true,
        scroller:       true,
        "columns": [
            {
                "data": null,
                "title":"序号",
                "width":"45px",
                "defaultContent": "",
                "sClass": "text-center",
                "searchable":false,
                render: function (data, type, row, meta) {
                    var no = meta.settings._iDisplayStart + meta.row + 1;
                    return no;
                }
            },{
                "data": "updateTime",
                "title":"录入时间",
                "width":"80px",
                "defaultContent": "",
                "sClass": "text-center",
                "render": function(data, type, full, meta) {
                    return  moment(data).format("YYYY-MM-DD");
                }
            },{
                "data": "employeeName",
                "title":"姓名",
                "width":"120px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "employeeNumber",
                "title":"工号",
                "width":"60px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "groupName",
                "title":"组名",
                "width":"60px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "orderName",
                "title":"订单号",
                "width":"110px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "layerCount",
                "title":"数量",
                "width":"45px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "procedureNumber",
                "title":"工序号",
                "width":"45px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "createTime",
                "title":"日期",
                "width":"90px",
                "defaultContent": "",
                "sClass": "text-center",
                "render": function(data, type, full, meta) {
                    return  moment(data).format("YYYY-MM-DD");
                }
            }, {
                "data": "userName",
                "title":"录入人",
                "width":"60px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "hourEmpID",
                "title": "操作",
                "width":"90px",
                "defaultContent": "",
                "sClass": "text-center",
            }
        ],
        "columnDefs" :
            [{
                "orderable" : false, // 禁用排序
                "targets" : [10], // 指定的列
                "data" : "hourEmpID",
                "render" : function(data, type, full, meta) {
                    return "<a href='#' style='color:#ff0000' onclick='deletePieceWorkDetail("+data+")'>删除</a>";
                }
            }]
    });
}


function deletePieceWorkDetail(hourEmpID) {
    if(userRole != 'root' && userRole != 'role5'){
        swal("SORRY!", "对不起，您没有操作权限！", "warning");
        return;
    }
    swal({
        title: "",
        text: "<span style=\"font-weight:bolder;font-size: 20px\">您确定要删除吗？</span>",
        type: "warning",
        html:true,
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: "确定",
        cancelButtonText:"我再想想",
        confirmButtonColor: "#ec6c62",
        showLoaderOnConfirm: true
    }, function() {
        $.ajax({
            url: basePath + "erp/deletehouremp",
            type:'POST',
            data: {
                hourEmpID:hourEmpID
            },
            success: function (data) {
                if(data == 0) {
                    $.unblockUI();
                    $("input").val("");
                    swal({
                            type:"success",
                            title:"",
                            text: "<span style=\"font-weight:bolder;font-size: 20px\">恭喜你，删除成功！</span>",
                            html: true
                        },
                        function(){
                            location.href=basePath+"erp/hourEmpStart";
                        });
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，删除失败！</span>",html: true});
                }
            },
            error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        })
    });
}


function addPieceWork(){
    if(userRole != 'root' && userRole != 'role5'){
        swal("SORRY!", "对不起，您没有操作权限！", "warning");
        return;
    }
    $.blockUI({
        css: {
            width: '65%',
            top: '15%',
            left: '15%',
            border: 'none',
            padding: '15px',
            backgroundColor: '#fff',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: 1,
            color: '#000'
        },
        message: $('#editPro')
    });
    var url = basePath + "erp/addhouremp";
    $("#editYes").unbind("click").bind("click", function () {
        if($("#employeeNumber").val().trim()=="") {
            swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请填写完整信息！</span>",html: true});
            return false;
        }
        if($("#employeeName").val().trim()=="") {
            swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请填写完整信息！</span>",html: true});
            return false;
        }
        if($("#groupName").val().trim()=="") {
            swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请填写完整信息！</span>",html: true});
            return false;
        }
        if($("#orderName").val().trim()=="") {
            swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请填写完整信息！</span>",html: true});
            return false;
        }
        if($("#clothesVersionNumber").val().trim()=="") {
            swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请填写完整信息！</span>",html: true});
            return false;
        }
        if($("#layerCount").val().trim()=="") {
            swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请填写完整信息！</span>",html: true});
            return false;
        }
        if($("#from").val().trim()=="") {
            swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请填写完整信息！</span>",html: true});
            return false;
        }
        $.ajax({
            url: url,
            type: 'POST',
            data: {
                employeeNumber:$("#employeeNumber").val(),
                employeeName:$("#employeeName").val(),
                groupName:$("#groupName").val(),
                orderName:$("#orderName").val(),
                clothesVersionNumber:$("#clothesVersionNumber").val(),
                layerCount:$("#layerCount").val(),
                userName:userName,
                createTime:$("#from").val()
            },
            success: function (data) {
                if(data == 0) {
                    $.unblockUI();
                    $("input").val("");
                    swal({
                            type:"success",
                            title:"",
                            text: "<span style=\"font-weight:bolder;font-size: 20px\">恭喜你，保存成功！</span>",
                            html: true
                        },
                        function(){
                            location.href=basePath+"erp/hourEmpStart";
                        });
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，保存失败！</span>",html: true});
                }
            },
            error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        });
    });
    $("#editNo").unbind("click").bind("click", function () {
        $.unblockUI();
        $("input").val("");
    });


}



function updateHourEmp(obj,hourEmpID){
    if(userRole != 'root' && userRole != 'role5'){
        swal("SORRY!", "对不起，您没有操作权限！", "warning");
        return;
    }
    $("#employeeNumber1").val($(obj).parent().parent().find("td").eq(3).text());
    $("#orderName1").val($(obj).parent().parent().find("td").eq(5).text());
    $("#layerCount1").val($(obj).parent().parent().find("td").eq(6).text());
    $("#from1").val($(obj).parent().parent().find("td").eq(8).text());
    $.blockUI({
        css: {
            width: '65%',
            top: '15%',
            left: '15%',
            border: 'none',
            padding: '15px',
            backgroundColor: '#fff',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: 1,
            color: '#000'
        },
        message: $('#editHourEmpPro')
    });

    $("#editHourEmpYes").unbind("click").bind("click", function () {
        var flag = false;
        if ($("#employeeNumber1").val().trim()==""){
            flag = true;
        }
        if ($("#orderName1").val().trim() == ""){
            flag = true;
        }
        if ($("#layerCount1").val().trim() == ""){
            flag = true;
        }
        if ($("#from1").val().trim() == ""){
            flag = true;
        }
        if(flag) {
            swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请填写完所有字段信息！</span>",html: true});
            return false;
        }
        var hourEmp = {};
        hourEmp.hourEmpID = hourEmpID;
        hourEmp.employeeNumber = $("#employeeNumber1").val();
        hourEmp.orderName = $("#orderName1").val();
        hourEmp.layerCount = $("#layerCount1").val();
        hourEmp.userName = userName;
        hourEmp.createTime = $("#from1").val();
        hourEmp.procedureNumber = 11111;
        $.ajax({
            url: basePath + "erp/updatehouremp",
            type:'POST',
            data: {
                hourEmp:JSON.stringify(hourEmp)
            },
            success: function (data) {
                if(data == 0) {
                    $.unblockUI();
                    $("#editHourEmpPro input").val("");
                    swal({
                            type:"success",
                            title:"",
                            text: "<span style=\"font-weight:bolder;font-size: 20px\">恭喜你，保存成功！</span>",
                            html: true
                        },
                        function(){
                            location.href=basePath+"erp/hourEmpStart";
                        });
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，保存失败！</span>",html: true});
                }
            },
            error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        })
    });
    $("#editHourEmpNo").unbind("click").bind("click", function () {
        $.unblockUI();
        $("#editHourEmpPro input").val("");
    });
}


function changeTable(obj){
    var opt = obj.options[obj.selectedIndex];
    if (opt.value == "oneMonth"){
        $.blockUI({ css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            },
            message: "<h3>稍等一下... 奋力搜索中</h3>"
        });
        $.ajax({
            url: basePath + "erp/getonemonthhouremp",
            type:'GET',
            data: {},
            success: function (data) {
                if(data) {
                    createPieceWorkDetailTable(data.oneMonthHourEmp,2);
                    $.unblockUI();
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，没有查到相关信息！</span>",html: true});
                }
            },
            error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        });
    }
    if (opt.value == "threeMonths"){
        $.blockUI({ css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            },
            message: "<h3>稍等一下... 奋力搜索中</h3>"
        });
        $.ajax({
            url: basePath + "erp/getthreemonthshouremp",
            type:'GET',
            data: {},
            success: function (data) {
                if(data) {
                    createPieceWorkDetailTable(data.threeMonthsHourEmp, 3);
                    $.unblockUI();
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，没有查到相关信息！</span>",html: true});
                }
            },
            error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        });
    }
    if (opt.value == "today"){
        $.blockUI({ css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            },
            message: "<h3>稍等一下... 奋力搜索中</h3>"
        });
        $.ajax({
            url: basePath + "erp/getpieceworktoday",
            type:'GET',
            data: {},
            success: function (data) {
                if(data) {
                    createPieceWorkDetailTable(data.todayHourEmp, 1);
                    $.unblockUI();
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，没有查到相关信息！</span>",html: true});
                }
            },
            error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        });
    }
}