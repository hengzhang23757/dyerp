layui.config({
    base: '/layuiadmin/' //静态资源所在路径
}).extend({
    index: 'lib/index' //主入口模块
}).use(['index', 'message']);


$(document).ready(function () {
    $.ajax({
        url: "/erp/getunfinishmessagecountinfobytype",
        type: 'GET',
        data: {},
        success: function (data) {
            if(data.messageList) {
                var messageData = data.messageList;
                var totalCount = 0;
                $("#messageAll").hide();
                $("#messageNotice").hide();
                $("#messageDirect").hide();
                $("#messageSended").hide();
                $("#messageOrder").hide();
                $("#messageFabric").hide();
                $("#messageAccessory").hide();
                $("#messageIe").hide();
                $("#messageCut").hide();
                $("#messageSew").hide();
                $("#messageFinish").hide();
                $("#messageFinance").hide();
                for (var i = 0; i < messageData.length; i++){
                    if (messageData[i].messageType === '通知'){
                        $("#messageNotice").show();
                        $("#messageNotice").text(messageData[i].messageCount);
                        totalCount += messageData[i].messageCount;
                    } else if (messageData[i].messageType == '私信'){
                        $("#messageDirect").show();
                        $("#messageDirect").text(messageData[i].messageCount);
                        totalCount += messageData[i].messageCount;
                    } else if (messageData[i].messageType == '跟单'){
                        $("#messageOrder").show();
                        $("#messageOrder").text(messageData[i].messageCount);
                        totalCount += messageData[i].messageCount;
                    } else if (messageData[i].messageType == '面料'){
                        $("#messageFabric").show();
                        $("#messageFabric").text(messageData[i].messageCount);
                        totalCount += messageData[i].messageCount;
                    } else if (messageData[i].messageType == '辅料'){
                        $("#messageAccessory").show();
                        $("#messageAccessory").text(messageData[i].messageCount);
                        totalCount += messageData[i].messageCount;
                    } else if (messageData[i].messageType == 'IE'){
                        $("#messageIe").show();
                        $("#messageIe").text(messageData[i].messageCount);
                        totalCount += messageData[i].messageCount;
                    } else if (messageData[i].messageType == '裁床'){
                        $("#messageCut").show();
                        $("#messageCut").text(messageData[i].messageCount);
                        totalCount += messageData[i].messageCount;
                    } else if (messageData[i].messageType == '车缝'){
                        $("#messageSew").show();
                        $("#messageSew").text(messageData[i].messageCount);
                        totalCount += data.messageList[i].messageCount;
                    } else if (messageData[i].messageType == '后整'){
                        $("#messageFinish").show();
                        $("#messageFinish").text(messageData[i].messageCount);
                        totalCount += messageData[i].messageCount;
                    } else if (messageData[i].messageType == '财务'){
                        $("#messageFinance").show();
                        $("#messageFinance").text(messageData[i].messageCount);
                        totalCount += messageData[i].messageCount;
                    }
                }
                if(totalCount > 0){
                    $("#messageAll").show();
                    $("#messageAll").text(totalCount);
                }
            }
        },
        error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }
    });
});