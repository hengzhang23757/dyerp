/**
 * @Title：yutons_sug搜索框提示插件||输入框提示插件
 * @Version：1.0
 * @Auth：yutons
 * @Date: 2018/10/4
 * @Time: 14:07
 */
layui.define(['jquery', 'table', 'tableFilter'], function(exports) {
	"use strict";
	const $ = layui.jquery,
		tableFilter = layui.tableFilter,
		table = layui.table;

	let yutons_sug = function() {
		this.v = '1.0.1';
	};
	/**
	 * yutons_sug搜索框提示插件||输入框提示插件初始化
	 */
	yutons_sug.prototype.render = function(opt) {
        opt.urlBak=opt.url;
		//设置默认初始化参数
		opt.type = opt.type || 'sug'; //默认sug，传入sug||sugTable
		opt.elem = "#yutons_sug_" + opt.id;
		opt.height = opt.height || '229';
		opt.cellMinWidth = opt.cellMinWidth || '80'; //最小列宽
		opt.page = opt.page || true;
		opt.limits = opt.limits || [3];
		opt.loading = opt.loading || true;
		opt.limit = opt.limit || 3;
		opt.size = opt.size || 'sm'; //小尺寸的表格

		//初始化输入框提示容器
		$("#" + opt.id).after("<div id='sugItem' style='background-color: #fff;display: none;z-index:99999;position: absolute;width:100%;'></div>");

		//输入框提示容器移出事件：鼠标移出隐藏输入提示框
		$("#" + opt.id).parent().mouseleave(function() {
			if($(".layui-table-filter-view.layui-anim.layui-anim-fadein").length<=0) {
				$("#" + opt.id).next().hide().html("");
			}
		});

		
		if (opt.type == "sugTable") {
			//如果type为sugTable，则初始化下拉表格
			/* 输入框鼠标松开事件 */
			$("#" + opt.id).mouseup(function(e) {
				opt.obj = this;
				getSugTable(opt);
			})
			/* 输入框键盘抬起事件 */
			$("#" + opt.id).keyup(function(e) {
				opt.obj = this;
				setTimeout(function () {
					getSugTable(opt);
				}, 500);

			})
		} else if (opt.type == "sug") {
			//如果type为sug，则初始化下拉框
			$("#" + opt.id).next().css("border", "solid #e6e6e6 0.5px");
			/* 输入框鼠标松开事件 */
			$("#" + opt.id).mouseup(function(e) {
				opt.obj = this;
				getSug(opt);
			})
			/* 输入框键盘抬起事件 */
			$("#" + opt.id).keyup(function(e) {
				opt.obj = this;
				getSug(opt);
			})
		}
	}

	//搜索框提示插件||输入框提示插件--sugTable-下拉表格
	function getSugTable(opt) {
		//如果输入信息为"",则隐藏输入提示框,不再执行下边代码
        let kw = $.trim($(opt.obj).val());
        if (kw == "") {
            $("#" + opt.id).next().hide().html("");
            return false;
        }
		//下拉表格初始化table容器
		let html = '<table id="yutons_sug_' + opt.obj.getAttribute("id") + '" lay-filter="yutons_sug_' + opt.obj.getAttribute(
				"id") +
			'"></table>';
		$("#" + opt.obj.getAttribute("id")).next().show().html(html);

		//下拉表格初始化
        opt.url=opt.urlBak+kw;
		table.render(opt);
		//设置下拉表格样式
		$(opt.elem).next().css("margin-top", "0").css("background-color", "#ffffff");
		//监听下拉表格行单击事件（单击||双击事件为：row||rowDouble）设置单击或双击选中对应的行
		table.on('rowDouble(' + "yutons_sug_" + opt.id + ')', function(obj) {
			if(opt.id=="clothesVersionNumber") {
                $("#orderName").val(obj.data["orderName"]);
            }else if(opt.id=="orderName") {
                $("#clothesVersionNumber").val(obj.data["clothesVersionNumber"]);
            }else if(opt.id=="clothesVersionNumberTwo") {
				$("#orderNameTwo").val(obj.data["orderName"]);
			}else if(opt.id=="orderNameTwo") {
				$("#clothesVersionNumberTwo").val(obj.data["clothesVersionNumber"]);
			}else if(opt.id=="changeCustomerCode") {
				$("#changeCustomerName").val(obj.data["changeCustomerName"]);
			}else if(opt.id=="changeCustomerName") {
				$("#changeCustomerCode").val(obj.data["changeCustomerCode"]);
			}else if(opt.id=="supplierName") {
				$("#supplierFullName").val(obj.data["supplierFullName"]);
				$("#supplierAddress").val(obj.data["supplierAddress"]);
			}else if(opt.id.indexOf("procedureNumber")!=-1 || opt.id.indexOf("procedureName")!=-1 || opt.id.indexOf("procedureDescription")!=-1 || opt.id.indexOf("remark")!=-1) {
                var dataCache = table.cache.procedureTable;
                var isFlag = false;
                $.each(dataCache,function (index,item) {
					if(opt.id.split("-")[1]!=item.LAY_TABLE_INDEX && obj.data.procedureNumber==item.procedureNumber) {
                        layer.msg("已存在该工序号，请重新选择！", {icon: 2});

                        isFlag = true;
                        return false;
                    }
                })
				if(isFlag) {
                	return false;
				}
                dataCache[opt.id.split("-")[1]].styleType = obj.data.styleType;
                dataCache[opt.id.split("-")[1]].partName = obj.data.partName;
                dataCache[opt.id.split("-")[1]].procedureCode = obj.data.procedureCode;
                dataCache[opt.id.split("-")[1]].procedureNumber = obj.data.procedureNumber;
                dataCache[opt.id.split("-")[1]].procedureName = obj.data.procedureName;
                dataCache[opt.id.split("-")[1]].procedureSection = obj.data.procedureSection;
                dataCache[opt.id.split("-")[1]].procedureDescription = obj.data.procedureDescription;
                dataCache[opt.id.split("-")[1]].remark = obj.data.remark;
                dataCache[opt.id.split("-")[1]].segment = obj.data.segment;
                dataCache[opt.id.split("-")[1]].equType = obj.data.equType;
                dataCache[opt.id.split("-")[1]].procedureLevel = obj.data.procedureLevel;
                dataCache[opt.id.split("-")[1]].sam = obj.data.sam;
                dataCache[opt.id.split("-")[1]].packagePrice = obj.data.packagePrice;
                dataCache[opt.id.split("-")[1]].piecePrice = obj.data.piecePrice;
                dataCache[opt.id.split("-")[1]].initPrice = obj.data.piecePrice;
                if (obj.data.procedureSection == "后整"){
                    dataCache[opt.id.split("-")[1]].scanPart = "成品";
                    if (obj.data.procedureNumber == "666" || obj.data.procedureNumber == "777"){
                        dataCache[opt.id.split("-")[1]].scanPart = "返工";
                    }
                    if (obj.data.procedureNumber == "561" || obj.data.procedureNumber == "563" || obj.data.procedureNumber == "6563"){
                        dataCache[opt.id.split("-")[1]].scanPart = "成品";
                    }
                } else if (obj.data.procedureNumber == "45" || obj.data.procedureNumber == "46"|| obj.data.procedureNumber == "126"|| obj.data.procedureNumber == "121" || obj.data.procedureNumber == "2121" || obj.data.procedureNumber == "2126" || obj.data.procedureNumber == "2045" || obj.data.procedureNumber == "2046"){
                    dataCache[opt.id.split("-")[1]].scanPart = "罗纹";
                } else{
                    dataCache[opt.id.split("-")[1]].scanPart = "主身";
                }

                table.reload("procedureTable", {
                    data: dataCache   // 将新数据重新载入表格
                })
            } else {
                var accessoryData = table.cache.accessoryDataTable;
                var fabricData = table.cache.fabricDataTable;
                console.log(fabricData);
                var isFabricData = true;
                $.each(obj.data, function (index, item) {
                    if (index == "accessoryName" || index == "accessoryNumber") {
                        isFabricData = false;
                        return false;
                    }
                });
                if (!isFabricData) {
                    accessoryData[opt.id.split("-")[1]].accessoryName = obj.data.accessoryName;
                    accessoryData[opt.id.split("-")[1]].accessoryNumber = obj.data.accessoryNumber;
                    accessoryData[opt.id.split("-")[1]].specification = obj.data.specification;
                    accessoryData[opt.id.split("-")[1]].accessoryColor = obj.data.accessoryColor;
                    accessoryData[opt.id.split("-")[1]].accessoryUnit = obj.data.accessoryUnit;
                    accessoryData[opt.id.split("-")[1]].accessoryType = obj.data.accessoryType;
                    accessoryData[opt.id.split("-")[1]].supplier = obj.data.supplier;
                    accessoryData[opt.id.split("-")[1]].pieceUsage = obj.data.pieceUsage;
                    accessoryData[opt.id.split("-")[1]].orderPieceUsage = obj.data.orderPieceUsage;
                    table.reload("accessoryDataTable", {
                        data: accessoryData   // 将新数据重新载入表格
                    })
                } else {
                    fabricData[opt.id.split("-")[1]].fabricName = obj.data.fabricName;
                    fabricData[opt.id.split("-")[1]].fabricNumber = obj.data.fabricNumber;
                    fabricData[opt.id.split("-")[1]].supplier = obj.data.supplier;
                    fabricData[opt.id.split("-")[1]].unit = fabricData[opt.id.split("-")[1]].units[0].val();
                    fabricData[opt.id.split("-")[1]].fabricWidth = obj.data.fabricWidth;
                    fabricData[opt.id.split("-")[1]].fabricWeight = obj.data.fabricWeight;
                    fabricData[opt.id.split("-")[1]].partName = obj.data.partName;
                    fabricData[opt.id.split("-")[1]].fabricAdd = obj.data.fabricAdd;
                    fabricData[opt.id.split("-")[1]].fabricLoss = obj.data.fabricLoss;
                    fabricData[opt.id.split("-")[1]].orderPieceUsage = obj.data.orderPieceUsage;
                    fabricData[opt.id.split("-")[1]].pieceUsage = obj.data.pieceUsage;
                    table.reload("fabricDataTable", {
                        data: fabricData   // 将新数据重新载入表格
                    })
                }
            }
			//获取选中行所传入字段的值
			$("#" + opt.id).focus();
			$("#" + opt.id).val(obj.data[opt.id.split("-")[0]]);
			$("#" + opt.id).blur();
			$("#" + opt.id).next().hide().html("");

        });
		if(opt.id.indexOf("procedureNumber")!=-1 || opt.id.indexOf("procedureName")!=-1 || opt.id.indexOf("procedureDescription")!=-1 || opt.id.indexOf("remark")!=-1) {
			tableFilter.render({
				'elem': '#yutons_sug_' + opt.obj.getAttribute("id"),//table的选择器
				'mode': 'local',//过滤模式
				'filters': [
					{
						'field': 'partName',
						'type': 'radio'
					},
					{
						'field': 'segment',
						'type': 'radio'
					},
					{
						'field': 'procedureNumber',
						'type': 'radio'
					},
					{
						'field': 'procedureName',
						'type': 'radio'
					},
					{
						'field': 'procedureDescription',
						'type': 'radio'
					},
					{
						'field': 'remark',
						'type': 'radio'
					}
				],//过滤项配置
				'done': function (filters) {
					//结果回调
				}
			});
		}
	}

	//搜索框提示插件||输入框提示插件--sug-下拉框
	function getSug(opt) {
		sessionStorage.setItem("inputId", opt.id)
		let kw = $.trim($(opt.obj).val());
		if (kw == "") {
			$("#" + opt.id).next().hide().html("");
			return false;
		}
		//sug下拉框异步加载数据并渲染下拉框
		$.ajax({
			type: "get",
			url: opt.urlBak+kw,
			success: function(data) {
				let html = "";
				layui.each(data.data, (index, item) => {
					//if (item[sessionStorage.getItem("inputId")].indexOf(decodeURI(kw)) >= 0) {
						html +=
							"<div class='item' style='padding: 3px 10px;cursor: pointer;' onmouseenter='getFocus(this)' onClick='getCon(this);'>" +
							item[sessionStorage.getItem("inputId")] + "</div>";
					//}
				});
				if (html != "") {
					$("#" + sessionStorage.getItem("inputId")).next().show().html(html);
				} else {
					$("#" + sessionStorage.getItem("inputId")).next().hide().html("");
				}
			}
		});
	}
	//搜索框提示插件||输入框提示插件--sug-下拉框上下键移动事件和回车事件
	$(document).keydown(function(e) {
		e = e || window.event;
		let keycode = e.which ? e.which : e.keyCode;
		if (keycode == 38) {
			//上键事件
			if ($.trim($("#" + sessionStorage.getItem("inputId")).next().html()) == "") {
				return;
			}
			movePrev(sessionStorage.getItem("inputId"));
		} else if (keycode == 40) {
			//下键事件
			if ($.trim($("#" + sessionStorage.getItem("inputId")).next().html()) == "") {
				return;
			}
			$("#" + sessionStorage.getItem("inputId")).blur();
			if ($(".item").hasClass("addbg")) {
				moveNext();
			} else {
				$(".item").removeClass('addbg').css("background", "").eq(0).addClass('addbg').css("background", "#e6e6e6");
			}
		} else if (keycode == 13) {
			//回车事件
			dojob();
		}
	});
	//上键事件
	let movePrev = function(id) {
		$("#" + id).blur();
		let index = $(".addbg").prevAll().length;
		if (index == 0) {
			$(".item").removeClass('addbg').css("background", "").eq($(".item").length - 1).addClass('addbg').css(
				"background", "#e6e6e6");
		} else {
			$(".item").removeClass('addbg').css("background", "").eq(index - 1).addClass('addbg').css("background", "#e6e6e6");
		}
	}
	//下键事件
	let moveNext = function() {
		let index = $(".addbg").prevAll().length;
		if (index == $(".item").length) {
			$(".item").removeClass('addbg').css("background", "").eq(0).addClass('addbg').css("background", "#e6e6e6");
		} else {
			$(".item").removeClass('addbg').css("background", "").eq(index + 1).addClass('addbg').css("background", "#e6e6e6");
		}
	}
	//回车事件
	let dojob = function() {
		let value = $(".addbg").text();
		$("#" + sessionStorage.getItem("inputId")).blur();
		$("#" + sessionStorage.getItem("inputId")).val(value);
		$("#" + sessionStorage.getItem("inputId")).next().hide().html("");
	}

	//上下键选择和鼠标选择事件改变颜色
	window.getFocus = function(obj) {
		$(".item").css("background", "");
		$(obj).css("background", "#e6e6e6");
	}

	//点击选中事件，获取选中内容并回显到输入框
	window.getCon = function(obj) {
		let value = $(obj).text();
		$("#" + $(".item").parent().prev().attr("id")).val(value);
		$("#" + $(".item").parent().prev().attr("id")).next().hide().html("");
	}

	//自动完成渲染
	yutons_sug = new yutons_sug();
	//暴露方法
	exports("yutons_sug", yutons_sug);
});
