layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.21'
}).extend({                         // 模块别名
    soulTable: 'soulTable',
});
var fabricQueryTable;
var cutFabricTable;

$(document).ready(function () {

});


layui.use(['form', 'soulTable', 'table', 'upload','yutons_sug', 'element'], function () {
    var table = layui.table,
        soulTable = layui.soulTable,
        element = layui.element;
    var load = layer.load();
    cutFabricTable = table.render({
        elem: '#cutFabricTable'
        ,url:'getunfinishedcutlooseplan'
        ,cols: [[]]
        ,loading:true
        ,done: function (res, curr, count) {
            table.init('cutFabricTable',{//转换成静态表格
                cols:[[
                    {field:'orderName', title:'款号', align:'center', width:'25%'}
                    ,{field:'colorName', title:'颜色', align:'center', width:'15%'}
                    ,{field:'orderCount', title:'订单数', align:'center', width:'15%'}
                    ,{field:'cutCount', title:'已裁', align:'center', width:'15%'}
                    ,{field:'storageBatch', title:'库存(卷)', align:'center', width:'15%'}
                    ,{field:'preCutBatch', title:'待裁(卷)', align:'center', width:'15%'}
                ]]
                ,data: res.data
                ,title: '表'
                ,even: true
                ,page: false
                ,height: 300
                ,overflow: 'tips'
                ,done: function (res, curr, count) {
                    soulTable.render(this);
                    layer.close(load);
                    element.render();
                    $("div[lay-id='cutFabricTable'] th").css("font-size","20px");
                    $("div[lay-id='cutFabricTable'] th").css("font-weight","700");
                    $("div[lay-id='cutFabricTable'] td").css("font-size","20px");
                    $("div[lay-id='cutFabricTable'] td").css("font-weight","700");
                }
                ,filter: {
                    bottom: true,
                    items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                }
            });
        }
    });

    fabricQueryTable = table.render({
        elem: '#fabricQueryTable'
        ,url:'getscreenfabricstorage'
        ,method:'post'
        ,cols: [[]]
        ,loading:true
        ,done: function (res, curr, count) {
            table.init('fabricQueryTable',{//转换成静态表格
                cols:[[
                    {type:'numbers', align:'center', title:'序号', width:70}
                    ,{field:'orderName', title:'款号', align:'center', width:180, sort: true, filter: true}
                    ,{field:'clothesVersionNumber', title:'版单号', align:'center', width:180, sort: true, filter: true}
                    ,{field:'location', title:'仓位', align:'center', width:130, sort: true, filter: true}
                    ,{field:'fabricName', title:'面料', align:'center', width:300, sort: true, filter: true}
                    ,{field:'colorName', title:'色组', align:'center', width:150, sort: true, filter: true}
                    ,{field:'fabricColor', title:'面料颜色', align:'center',width:150, sort: true, filter: true}
                    ,{field:'jarName', title:'缸号', align:'center', width:200, sort: true, filter: true}
                    ,{field:'weight', title:'数量', align:'center', width:120, sort: true, filter: true, totalRow: true}
                    ,{field:'unit', title:'单位', align:'center', width:90, sort: true, filter: true}
                    ,{field:'batchNumber', title:'卷数', align:'center', width:90, sort: true, filter: true, totalRow: true}
                    ,{field:'returnTime', title:'时间', align:'center', width:180, sort: true, filter: true,templet: function (d) {
                            return layui.util.toDateString(d.returnTime, 'yyyy-MM-dd');
                        }}
                ]]
                ,data:res.data
                ,height: 'full-420'
                // ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                ,defaultToolbar: ['filter', 'print']
                ,title: '面料库存表'
                ,totalRow: false
                ,page: true
                ,limits: [13, 20, 30]
                ,limit: 13 //每页默认显示的数量
                ,overflow: 'tips'
                ,done: function () {
                    soulTable.render(this);
                    layer.close(load);
                    $("div[lay-id='fabricQueryTable'] th").css("font-size","20px");
                    $("div[lay-id='fabricQueryTable'] th").css("font-weight","700");
                    $("div[lay-id='fabricQueryTable'] td").css("font-size","20px");
                    $("div[lay-id='fabricQueryTable'] td").css("font-weight","700");

                }
                ,filter: {
                    bottom: true,
                    items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                }
            });
        }
    });



    table.on('toolbar(fabricQueryTable)', function(obj) {
        if (obj.event === 'clearFilter') {
            // 清除所有筛选条件并重载表格
            // 参数: tableId
            soulTable.clearFilter('fabricQueryTable')
        } else if (obj.event === 'refresh') {
            fabricQueryTable.reload();
        } else if (obj.event === 'exportExcel') {
            soulTable.export('fabricQueryTable');
        } else if(obj.event === 'exportScreen') {
            exportScreen();
        }
    });


    //监听行工具事件
    table.on('tool(fabricQueryTable)', function(obj){
        var data = obj.data;
        if(obj.event === 'del'){
            layer.confirm('真的删除吗', function(index){
                $.ajax({
                    url: "/erp/deletefabricstorage",
                    type: 'POST',
                    data: {
                        fabricStorageID:data.fabricStorageID
                    },
                    success: function (res) {
                        if (res.error) {
                            layer.msg("删除失败！", {icon: 2});
                        } else if (res.info) {
                            fabricQueryTable.reload();
                            layer.close(index);
                            layer.msg(res.info, {icon: 1});
                        }
                    }, error: function () {
                        layer.msg("删除失败！", {icon: 2});
                    }
                })

            });
        }
    })

    });






var currentPage = 1;
window.onload = function () {
    //全屏
    // initEcharts();
    var ele = document.documentElement
        ,reqFullScreen = ele.requestFullScreen || ele.webkitRequestFullScreen
        || ele.mozRequestFullScreen || ele.msRequestFullscreen;
    if(typeof reqFullScreen !== 'undefined' && reqFullScreen) {
        reqFullScreen.call(ele);
    }
    //定时刷新
    repeat();
};

function initEcharts() {
    var dom = document.getElementById("container");
    var myChart = echarts.init(dom);
    var app = {};

    var option;


    var posList = [
        'left', 'right', 'top', 'bottom',
        'inside',
        'insideTop', 'insideLeft', 'insideRight', 'insideBottom',
        'insideTopLeft', 'insideTopRight', 'insideBottomLeft', 'insideBottomRight'
    ];

    app.configParameters = {
        rotate: {
            min: -90,
            max: 90
        },
        align: {
            options: {
                left: 'left',
                center: 'center',
                right: 'right'
            }
        },
        verticalAlign: {
            options: {
                top: 'top',
                middle: 'middle',
                bottom: 'bottom'
            }
        },
        position: {
            options: posList.reduce(function (map, pos) {
                map[pos] = pos;
                return map;
            }, {})
        },
        distance: {
            min: 0,
            max: 100
        }
    };

    app.config = {
        rotate: 0,
        align: 'center',
        verticalAlign: 'middle',
        position: 'insideTop',
        distance: 15,
        onChange: function () {
            var labelOption = {
                normal: {
                    rotate: app.config.rotate,
                    align: app.config.align,
                    verticalAlign: app.config.verticalAlign,
                    position: app.config.position,
                    distance: app.config.distance
                }
            };
            myChart.setOption({
                series: [{
                    label: labelOption
                }, {
                    label: labelOption
                }, {
                    label: labelOption
                }, {
                    label: labelOption
                }]
            });
        }
    };


    var labelOption = {
        show: true,
        position: app.config.position,
        distance: app.config.distance,
        align: app.config.align,
        verticalAlign: app.config.verticalAlign,
        rotate: app.config.rotate,
        formatter: '{c}',
        fontSize: 16,
        rich: {
            name: {
            }
        }
    };


        option = {
        tooltip: {
            trigger: 'axis',
            axisPointer: {
                type: 'shadow'
            }
        },
        legend: {
            data: ['回布', '已松', '已裁', '库存'],
            padding:[30,0,-20,0],
            show: true,
            fontSize: 16,
            fontWeight: 700
        },
        toolbox: {
            show: false,
            orient: 'vertical',
            left: 'right',
            top: 'center',
            feature: {
                mark: {show: true},
                dataView: {show: true, readOnly: false},
                magicType: {show: true, type: ['line', 'bar', 'stack', 'tiled']},
                restore: {show: true},
                saveAsImage: {show: true}
            }
        },
        xAxis: [
            {
                type: 'category',
                axisTick: {show: false},
                data: ['R112031020', 'R212231602', 'R212131035', '209221117002', '202121104003衣'],
                axisLabel: {
                    textStyle: {
                        color: '#000',
                        fontSize:'15',
                        fontWeight:'700',
                        itemSize:''

                    }
                }
            }
        ],
        yAxis: [
            {
                type: 'value',
                axisLabel: {
                    inside: false,
                    textStyle: {
                        color: '#000',
                        fontSize:'15',
                        fontWeight:'700',
                        itemSize:''

                    }
                }
            }
        ],
        series: [
            {
                name: '回布',
                type: 'bar',
                barGap: 0,
                label: labelOption,
                emphasis: {
                    focus: 'series'
                },
                data: [20, 85, 19, 90, 99]
            },
            {
                name: '已松',
                type: 'bar',
                label: labelOption,
                emphasis: {
                    focus: 'series'
                },
                data: [20, 0, 0, 86, 0]
            },
            {
                name: '已裁',
                type: 'bar',
                label: labelOption,
                emphasis: {
                    focus: 'series'
                },
                data: [0, 1, 0, 4, 0]
            },
            {
                name: '库存',
                type: 'bar',
                label: labelOption,
                emphasis: {
                    focus: 'series'
                },
                data: [0, 85, 19, 0, 99]
            }
        ]
    };
    if (option && typeof option === 'object') {
        myChart.setOption(option);
    }
}


function repeat() {
    // var lastPage = $(".layui-laypage-last").text();
    var lastPage = $("div[lay-id='fabricQueryTable'] .layui-box.layui-laypage.layui-laypage-default").find("a").eq(-2).text();
    lastPage = lastPage === "" ? 1:lastPage;

    $(".layui-laypage-ski,.layui-input").eq(0).val(currentPage);
    $(".layui-laypage-btn").trigger("click");
    if(currentPage<lastPage)
        currentPage++;
    else{
        currentPage=1;
    }
    //定时刷新
    setTimeout(repeat,3000)
}

function toDecimal(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*100)/100;
    return f;
}