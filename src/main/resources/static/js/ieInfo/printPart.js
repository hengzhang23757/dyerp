var basePath=$("#basePath").val();
layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.21'
}).extend();
$(document).ready(function () {
    $.ajax({
        url: basePath + "erp/getallprintpart",
        type:'GET',
        data: {},
        success: function (data) {
            if(data) {
                createPrintPartTable(data.printPartList);
            }else {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，没有查到相关信息！</span>",html: true});
            }
        },
        error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }
    });

    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumber", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });
});


var printPartTable;
function createPrintPartTable(data) {
    if (printPartTable != undefined) {
        printPartTable.clear(); //清空一下table
        printPartTable.destroy(); //还原初始化了的datatable
    }
    printPartTable = $('#printPartTable').DataTable({
        "data":data,
        language : {
            processing : "载入中",//处理页面数据的时候的显示
            paginate : {//分页的样式文本内容。
                previous : "上一页",
                next : "下一页",
                first : "第一页",
                last : "最后一页"
            },
            search:"搜索：",
            zeroRecords : "没有内容",//table tbody内容为空时，tbody的内容。
            //下面三者构成了总体的左下角的内容。
            info : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//左下角的信息显示，大写的词为关键字。
            infoEmpty : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//筛选为空时左下角的显示。
            infoFiltered : ""//筛选之后的左下角筛选提示(另一个是分页信息显示，在上面的info中已经设置，所以可以不显示)，
        },
        // pageLength : 25,// 每页显示10条数据
        pagingType : "full_numbers", // 分页样式：simple,simple_numbers,full,full_numbers，
        "paging": false,
        // "ordering" : false,
        "info": true,
        searching:true,
        lengthChange:false,
        scrollY: $(document.body).height() - 180,
        fixedHeader: true,
        scrollCollapse: true,
        scroller:       true,
        dom: 'Bfrtip',
        "buttons": [{
            'extend': 'excel',
            'text': '导出',//定义导出excel按钮的文字
            'className': 'btn btn-success', //按钮的class样式
            'title': "票菲部位表",
            'exportOptions': {
                'modifier': {
                    'page': 'all'
                }
            }
        }],
        "columns": [
            {
                "data": null,
                "title":"序号",
                "width":"15%",
                "defaultContent": "",
                "sClass": "text-center",
                "searchable":false,
                render: function (data, type, row, meta) {
                    var no = meta.settings._iDisplayStart + meta.row + 1;
                    return no;
                }
            }, {
                "data": "clothesVersionNumber",
                "title":"版单号",
                "width":"20%",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "orderName",
                "title":"订单号",
                "width":"20%",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "printPartName",
                "title": "部位",
                "width":"20%",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "printPartID",
                "title": "操作",
                "width":"20%",
                "defaultContent": "",
                "sClass": "text-center",
            }
        ],
        "columnDefs" :
            [{
                "orderable" : false, // 禁用排序
                "targets" : [4], // 指定的列
                "data" : "printPartID",
                "render" : function(data, type, full, meta) {
                    return "<a href='#' style='color:#ff0000' onclick='deletePrintPart("+data+")'>删除</a>";
                }
            }],

    });
    $('#printPartTable_wrapper .dt-buttons').append("<button class=\"btn btn-success\" style=\"outline:none;margin-left: 10px\" onclick=\"addPrintPart()\">添加部位</button>");
}



function addPrintPart(printPartID,clothesVersionNumber,orderName,printPartName) {
    $.blockUI({
        css: {
            width: '60%',
            top: '10%',
            left: '20%',
            border: 'none',
            padding: '15px',
            backgroundColor: '#fff',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: 1,
            color: '#000'
        },
        message: $('#editPro')
    });
    var url = basePath + "erp/addprintpartbatch";
    $("#editYes").unbind("click").bind("click", function () {
        var flag = false;
        $("#editPro input,select").each(function (index, item) {
            if($(this).val() == "") {
                flag = true;
                return false;
            }
        });
        if(flag) {
            swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入完全部信息！</span>",html: true});
            return false;
        }
        var printPartJson = [];
        $("select[name='printPart']").each(function (index,item) {
            var printPart = {};
            printPart.clothesVersionNumber = $("#clothesVersionNumber").val();
            printPart.orderName = $("#orderName").val();
            printPart.printPartName = $(item).val()+$("input[name='printName']").eq(index).val()
            printPartJson.push(printPart);
        });
        $.ajax({
            url: url,
            type: 'POST',
            data: {
                printPartJson:JSON.stringify(printPartJson)
            },
            success: function (data) {
                if(data == 0) {
                    $.unblockUI();
                    $("input").val("");
                    swal({
                            type:"success",
                            title:"",
                            text: "<span style=\"font-weight:bolder;font-size: 20px\">恭喜你，保存成功！</span>",
                            html: true
                        },
                        function(){
                            location.href=basePath+"erp/printPartStart";
                        });
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，保存失败！</span>",html: true});
                }
            },
            error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        });
    });
    $("#editNo").unbind("click").bind("click", function () {
        $.unblockUI();
        $("input").val("");
    });
}








function deletePrintPart(printPartID) {
    swal({
        title: "",
        text: "<span style=\"font-weight:bolder;font-size: 20px\">您确定要删除吗？</span>",
        type: "warning",
        html:true,
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: "确定",
        cancelButtonText:"我再想想",
        confirmButtonColor: "#ec6c62",
        showLoaderOnConfirm: true
    }, function() {
        $.ajax({
            url: basePath + "erp/deleteprintpart",
            type:'POST',
            data: {
                printPartID:printPartID
            },
            success: function (data) {
                if(data == 0) {
                    $.unblockUI();
                    $("input").val("");
                    swal({
                            type:"success",
                            title:"",
                            text: "<span style=\"font-weight:bolder;font-size: 20px\">恭喜你，删除成功！</span>",
                            html: true
                        },
                        function(){
                            location.href=basePath+"erp/printPartStart";
                        });
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，删除失败！</span>",html: true});
                }
            },
            error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        })
    });
}


function addPart(obj) {
    $("#printPartDiv").append($("tr[name='partDiv']:last").clone());
    $("input[name='printName']:last").val("");
    $("button[name='addPartBtn']").remove();
    $("button[name='delPartBtn']:first").before('<button name="addPartBtn" class="btn" style="margin-bottom: 15px;outline:none;border-radius: 5px;color: white;" onclick="addPart(this)"><i class="fa fa-plus"></i></button>');
    $("button[name='delPartBtn']:last").show();
}

function delPart(obj) {
    $(obj).parent().parent().parent().remove();
}