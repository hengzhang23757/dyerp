var basePath=$("#basePath").val();
var colorNameSel,sizeNameSel;
var colorNameList,sizeNameList;
layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.21'
}).extend();
$(document).ready(function () {
    $.ajax({
        url: basePath + "erp/getallspecialprocedure",
        type:'GET',
        data: {},
        success: function (data) {
            if(data) {
                createSpecialProcedureTable(data.specialProcedureList);
            }else {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，没有查到相关信息！</span>",html: true});
            }
        },
        error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }
    });

    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumber", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });

    $("#orderName").bind('blur change',function(){
        var orderName = $("#orderName").val();
        $("select[name='procedureInfo']").empty();
        $.ajax({
            url: "/erp/getspecialprocedurebyorder",
            data: {"orderName": orderName},
            success:function(data){
                $("select[name='procedureInfo']").empty();
                if (data.procedureInfoList) {
                    $("select[name='procedureInfo']").append("<option value=''>选择工序</option>");
                    $.each(data.procedureInfoList, function(index,element){
                        var procedureNum = element.procedureNumber;
                        var procedureName = element.procedureName;
                        var procedureDescription = element.procedureDescription;
                        $("select[name='procedureInfo']").append("<option value="+procedureNum+"-"+procedureName+">"+procedureNum+"-"+procedureName+"-"+procedureDescription+"</option>");
                    })
                }
            },
            error:function(){
            }
        });
        $("select[name='colorName']").empty();
        $.ajax({
            url: "/erp/getordercolornamesbyorder",
            data: {"orderName": orderName},
            async:false,
            success:function(data){
                if (data.colorNameList) {
                    var array = [];
                    $.each(data.colorNameList, function(index,element){
                        var tmp = {};
                        tmp.name = element;
                        tmp.value = element;
                        array.push(tmp);
                    });
                    colorNameList = array;
                    console.log(colorNameList);
                }
            },
            error:function(){
            }
        });
        $.ajax({
            url: "/erp/getordersizenamesbyorder",
            data: {"orderName": orderName},
            async:false,
            success:function(data){
                if (data.sizeNameList) {
                    var array = [];
                    $.each(data.sizeNameList, function(index,element){
                        var tmp = {};
                        tmp.name = element;
                        tmp.value = element;
                        array.push(tmp);
                    })
                    sizeNameList = array;
                    console.log(sizeNameList);
                }
            },
            error:function(){
            }
        });
        $("div[id^='colorNameSel']").each(function (index,item) {
            var str = $(item)[0].id.split("colorNameSel");
            xmSelect.render({
                el: '#colorNameSel'+str[1],
                theme: {
                    color: '#aaaaaa',
                },
                filterable: false,
                toolbar: {
                    show: true,
                },
                data: colorNameList
            });
            xmSelect.render({
                el: '#sizeNameSel'+str[1],
                theme: {
                    color: '#aaaaaa',
                },
                filterable: false,
                toolbar: {
                    show: true,
                },
                data: sizeNameList
            });
        });
    });

    $("#clothesVersionNumber").bind('blur change',function(){
        var orderName = $("#orderName").val();
        $("select[name='procedureInfo']").empty();
        $.ajax({
            url: "/erp/getspecialprocedurebyorder",
            data: {"orderName": orderName},
            success:function(data){
                $("select[name='procedureInfo']").empty();
                if (data.procedureInfoList) {
                    $("select[name='procedureInfo']").append("<option value=''>选择工序</option>");
                    $.each(data.procedureInfoList, function(index,element){
                        var procedureNum = element.procedureNumber;
                        var procedureName = element.procedureName;
                        var procedureDescription = element.procedureDescription;
                        $("select[name='procedureInfo']").append("<option value="+procedureNum+"-"+procedureName+">"+procedureNum+"-"+procedureName+"-"+procedureDescription+"</option>");
                    })
                }
            },
            error:function(){
            }
        });
        $("select[name='colorName']").empty();
        $.ajax({
            url: "/erp/getordercolornamesbyorder",
            data: {"orderName": orderName},
            async:false,
            success:function(data){
                if (data.colorNameList) {
                    var array = [];
                    $.each(data.colorNameList, function(index,element){
                        var tmp = {};
                        tmp.name = element;
                        tmp.value = element;
                        array.push(tmp);
                    });
                    colorNameList = array;
                    console.log(colorNameList);
                }
            },
            error:function(){
            }
        });
        $.ajax({
            url: "/erp/getordersizenamesbyorder",
            data: {"orderName": orderName},
            async:false,
            success:function(data){
                if (data.sizeNameList) {
                    var array = [];
                    $.each(data.sizeNameList, function(index,element){
                        var tmp = {};
                        tmp.name = element;
                        tmp.value = element;
                        array.push(tmp);
                    })
                    sizeNameList = array;
                    console.log(sizeNameList);
                }
            },
            error:function(){
            }
        });
        $("div[id^='colorNameSel']").each(function (index,item) {
            var str = $(item)[0].id.split("colorNameSel");
            xmSelect.render({
                el: '#colorNameSel'+str[1],
                theme: {
                    color: '#aaaaaa',
                },
                filterable: false,
                toolbar: {
                    show: true,
                },
                data: colorNameList
            });
            xmSelect.render({
                el: '#sizeNameSel'+str[1],
                theme: {
                    color: '#aaaaaa',
                },
                filterable: false,
                toolbar: {
                    show: true,
                },
                data: sizeNameList
            });
        });
    });

    xmSelect.render({
        el: '#colorNameSel0',
        theme: {
            color: '#aaaaaa',
        },
        filterable: false,
        toolbar: {
            show: true,
        },
        data: []
    });

    xmSelect.render({
        el: '#sizeNameSel0',
        theme: {
            color: '#aaaaaa',
        },
        filterable: false,
        toolbar: {
            show: true,
        },
        data: []
    });

});

var specialProcedureTable;
function createSpecialProcedureTable(data) {
    if (specialProcedureTable != undefined) {
        specialProcedureTable.clear(); //清空一下table
        specialProcedureTable.destroy(); //还原初始化了的datatable
    }
    specialProcedureTable = $('#specialProcedureTable').DataTable({
        "data":data,
        language : {
            processing : "载入中",//处理页面数据的时候的显示
            paginate : {//分页的样式文本内容。
                previous : "上一页",
                next : "下一页",
                first : "第一页",
                last : "最后一页"
            },
            search:"搜索：",
            zeroRecords : "没有内容",//table tbody内容为空时，tbody的内容。
            //下面三者构成了总体的左下角的内容。
            info : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//左下角的信息显示，大写的词为关键字。
            infoEmpty : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//筛选为空时左下角的显示。
            infoFiltered : ""//筛选之后的左下角筛选提示(另一个是分页信息显示，在上面的info中已经设置，所以可以不显示)，
        },
        pageLength : 30,// 每页显示10条数据
        pagingType : "full_numbers", // 分页样式：simple,simple_numbers,full,full_numbers，
        "paging": true,
        // "ordering" : false,
        "info": true,
        searching:true,
        lengthChange:false,
        dom: 'Bfrtip',
        "buttons": [{
            'extend': 'excel',
            'text': '导出',//定义导出excel按钮的文字
            'className': 'btn btn-success', //按钮的class样式
            'title': "特殊工序",
            'exportOptions': {
                'modifier': {
                    'page': 'all'
                }
            }
        }],
        "columns": [
            {
                "data": null,
                "title":"序号",
                "width":"10%",
                "defaultContent": "",
                "sClass": "text-center",
                "searchable":false,
                render: function (data, type, row, meta) {
                    var no = meta.settings._iDisplayStart + meta.row + 1;
                    return no;
                }
            }, {
                "data": "clothesVersionNumber",
                "title":"版单号",
                "width":"15%",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "orderName",
                "title":"订单号",
                "width":"15%",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "procedureNumber",
                "title": "工序号",
                "width":"10%",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "procedureName",
                "title": "工序",
                "width":"15%",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "colorName",
                "title": "颜色",
                "width":"15%",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "sizeName",
                "title": "尺码",
                "width":"15%",
                "defaultContent": "",
                "sClass": "text-center",
            }
        ],
        "columnDefs" :
            [{
                "orderable" : false, // 禁用排序
                "targets" : [7], // 指定的列
                "data" : "specialID",
                "render" : function(data, type, full, meta) {
                    return "<a href='#' style='color:#ff0000' onclick='deleteSpecialProcedure("+data+")'>删除</a>";
                }
            }],

    });
    $('#specialProcedureTable_wrapper .dt-buttons').append("<button class=\"btn btn-s-lg  btn-success\" style=\"outline:none;margin-left: 10px\" onclick=\"addSpecialProcedure()\">添加</button>");
}


function addSpecialProcedure() {
    $.blockUI({
        css: {
            width: '80%',
            top: '10%',
            left: '10%',
            border: 'none',
            padding: '15px',
            backgroundColor: '#fff',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: 1,
            color: '#000'
        },
        message: $('#editPro')
    });
    var url = basePath + "erp/addspecialprocedurebatch";
    $("#editYes").unbind("click").bind("click", function () {
        var flag = false;
        // $("#editPro input,select").each(function (index, item) {
        //     if($(this).val() == "") {
        //         flag = true;
        //         return false;
        //     }
        // });
        if(flag) {
            swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入完全部信息！</span>",html: true});
            return false;
        }
        var selectArr = xmSelect.batch(null, 'getValue', 'name');
        var specialProcedureJson = [];
        $("select[name='procedureInfo']").each(function (index,item) {
            var selColor = $(item).parent().parent().find("input").eq(0).val().split(",");
            var selSize = $(item).parent().parent().find("input").eq(2).val().split(",");
            console.log(selSize);
            if(selColor.length == colorNameList.length) {
                selColor = ['全部'];
            }
            if(selSize.length == sizeNameList.length) {
                selSize = ['全部'];
            }
            $.each(selColor,function (c_index, c_item) {
                $.each(selSize,function (s_index, s_item) {
                    var specialProcedure = {};
                    specialProcedure.clothesVersionNumber = $("#clothesVersionNumber").val();
                    specialProcedure.orderName = $("#orderName").val();
                    var pi = $(item).val().split("-");
                    specialProcedure.procedureNumber = pi[0];
                    specialProcedure.procedureName = pi[1];
                    specialProcedure.colorName = c_item;
                    specialProcedure.sizeName = s_item;
                    specialProcedureJson.push(specialProcedure);
                })
            })
        });
        $.ajax({
            url: url,
            type: 'POST',
            data: {
                specialProcedureJson:JSON.stringify(specialProcedureJson)
            },
            success: function (data) {
                if(data == 0) {
                    $.unblockUI();
                    $("input").val("");
                    swal({
                            type:"success",
                            title:"",
                            text: "<span style=\"font-weight:bolder;font-size: 20px\">恭喜你，保存成功！</span>",
                            html: true
                        },
                        function(){
                            location.href=basePath+"erp/specialProcedureStart";
                        });
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，保存失败！</span>",html: true});
                }
            },
            error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        });
    });
    $("#editNo").unbind("click").bind("click", function () {
        $.unblockUI();
        $("input").val("");
    });
}


function deleteSpecialProcedure(specialID) {
    swal({
        title: "",
        text: "<span style=\"font-weight:bolder;font-size: 20px\">您确定要删除吗？</span>",
        type: "warning",
        html:true,
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: "确定",
        cancelButtonText:"我再想想",
        confirmButtonColor: "#ec6c62",
        showLoaderOnConfirm: true
    }, function() {
        $.ajax({
            url: basePath + "erp/deletespecialprocedure",
            type:'POST',
            data: {
                specialID:specialID
            },
            success: function (data) {
                if(data == 0) {
                    $.unblockUI();
                    $("input").val("");
                    swal({
                            type:"success",
                            title:"",
                            text: "<span style=\"font-weight:bolder;font-size: 20px\">恭喜你，删除成功！</span>",
                            html: true
                        },
                        function(){
                            location.href=basePath+"erp/specialProcedureStart";
                        });
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，删除失败！</span>",html: true});
                }
            },
            error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        })
    });
}

var div_no = 1;

function addPart(obj) {

    // $(obj).parent().parent().parent().after($("tr[name='partDiv']:last").clone());
    // $("select[name='procedureInfo']:last").val($("select[name='procedureInfo']:first").val());
    // $("select[name='colorName']:last").val($("select[name='colorName']:first").val());
    // $("select[name='sizeName']:last").val($("select[name='sizeName']:first").val());
    var option = $(obj).parent().parent().parent().find("select")[0].innerHTML;
    var html = "<tr name=\"partDiv\">\n" +
        "                        <td>\n" +
        "                            <label class=\"control-label\" style=\"margin-bottom: 15px;text-align: right;\">工序</label>\n" +
        "                        </td>\n" +
        "                        <td>\n" +
        "                            <select name=\"procedureInfo\" class=\"form-control\" style=\"margin-bottom: 15px;width: 200px;border-top: none;border-right: none;border-left: none;\">"+option+"</select>\n" +
        "                        </td>\n" +
        "                        <td>\n" +
        "                            <label class=\"control-label\" style=\"margin-bottom: 15px;text-align: right;\">颜色</label>\n" +
        "                        </td>\n" +
        "                        <td style=\"width:15%\">\n" +
        "                            <div id=\"colorNameSel"+div_no+"\"></div>\n" +
        "                        </td>\n" +
        "                        <td style=\"width:10%\">\n" +
        "                            <label class=\"control-label\" style=\"margin-bottom: 15px;text-align: right;\">尺码</label>\n" +
        "                        </td>\n" +
        "                        <td style=\"width:15%\">\n" +
        "                            <div id=\"sizeNameSel"+div_no+"\"></div>\n" +
        "                        </td>\n" +
        "                        <td style=\"width:10%\">\n" +
        "                            <form class=\"form-inline\">\n" +
        "                                <button class=\"btn\" style=\"margin-bottom: 15px;outline:none;border-radius: 5px;color: white;\" onclick=\"addPart(this)\"><i class=\"fa fa-plus\"></i></button>\n" +
        "                                <button class=\"btn\" style=\"margin-bottom: 15px;outline:none;border-radius: 5px;color: white;;display: none;background-color: rgb(236, 108, 98);\" onclick=\"delPart(this)\"><i class=\"fa fa-minus\"></i></button>\n" +
        "                            </form>\n" +
        "                        </td>\n" +
        "                    </tr>";
    $(obj).parent().parent().parent().after(html);
    xmSelect.render({
        el: '#colorNameSel'+div_no,
        theme: {
            color: '#aaaaaa',
        },
        filterable: false,
        toolbar: {
            show: true,
        },
        data: colorNameList
    });
    xmSelect.render({
        el: '#sizeNameSel'+div_no,
        theme: {
            color: '#aaaaaa',
        },
        filterable: false,
        toolbar: {
            show: true,
        },
        data: sizeNameList
    });
    $(obj).next().show();
    $(obj).remove();
    div_no = div_no+1;
}

function delPart(obj) {
    $(obj).parent().parent().parent().remove();
}