var basePath=$("#basePath").val();
$(document).ready(function () {
    layui.laydate.render({
        elem: '#from',
        trigger: 'click'
    });
    layui.laydate.render({
        elem: '#to',
        trigger: 'click'
    });
    layui.laydate.render({
        elem: '#to1',
        trigger: 'click'
    });
    $.ajax({
        url: basePath + "erp/getallgroupplan",
        type:'GET',
        data: {},
        success: function (data) {
            if(data) {
                createGroupPlanTable(data.groupPlanList);
            }else {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，没有查到相关信息！</span>",html: true});
            }
        },
        error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }
    });

    layui.use(['autocomplete'], function () {
        layui.autocomplete.render({
            elem: $('#clothesVersionNumber')[0],
            url: 'getversionhint',
            template_val: '{{d}}',
            template_txt: '{{d}}',
            onselect: function (resp) {
                autoComplete(resp)
            }
        })
    });

    $("#orderName").change(function () {
        var orderName = $("#orderName").val();
        $("#planCount").empty();
        $.ajax({
            url: "/erp/getordertotalcount",
            data: {"orderName": orderName},
            success:function(data){
                $("#planCount").val(data);
            },
            error:function(){
            }
        });

    });


});


function autoComplete(keywords) {
    $("#orderName").empty();
    $.ajax({
        url: "/erp/getorderbyversion",
        data: {"clothesVersionNumber": keywords},
        success:function(data){
            $("#orderName").empty();
            if (data.orderList) {
                $("#orderName").append("<option value=''>订单号</option>");
                $.each(data.orderList, function(index,element){
                    $("#orderName").append("<option value='"+element+"'>"+element+"</option>");
                })
            }
        },
        error:function(){
        }
    });
}

var groupPlanTable;
function createGroupPlanTable(data) {
    if (groupPlanTable != undefined) {
        groupPlanTable.clear(); //清空一下table
        groupPlanTable.destroy(); //还原初始化了的datatable
    }
    groupPlanTable = $('#groupPlanTable').DataTable({
        "data":data,
        language : {
            processing : "载入中",//处理页面数据的时候的显示
            paginate : {//分页的样式文本内容。
                previous : "上一页",
                next : "下一页",
                first : "第一页",
                last : "最后一页"
            },
            search:"搜索：",
            zeroRecords : "没有内容",//table tbody内容为空时，tbody的内容。
            //下面三者构成了总体的左下角的内容。
            info : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//左下角的信息显示，大写的词为关键字。
            infoEmpty : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//筛选为空时左下角的显示。
            infoFiltered : ""//筛选之后的左下角筛选提示(另一个是分页信息显示，在上面的info中已经设置，所以可以不显示)，
        },
        pageLength : 20,// 每页显示10条数据
        pagingType : "full_numbers", // 分页样式：simple,simple_numbers,full,full_numbers，
        "paging": true,
        // "ordering" : false,
        "info": true,
        searching:true,
        lengthChange:false,
        dom: 'Bfrtip',
        "buttons": [{
            'extend': 'excel',
            'text': '导出',//定义导出excel按钮的文字
            'className': 'btn btn-success', //按钮的class样式
            'title': "计划产能",
            'exportOptions': {
                'modifier': {
                    'page': 'all'
                }
            }
        }],
        "columns": [
            {
                "data": null,
                "title":"序号",
                "width":"45px",
                "defaultContent": "",
                "sClass": "text-center",
                "searchable":false,
                render: function (data, type, row, meta) {
                    var no = meta.settings._iDisplayStart + meta.row + 1;
                    return no;
                }
            }, {
                "data": "groupName",
                "title":"组名",
                "width":"45px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "clothesVersionNumber",
                "title":"版单号",
                "width":"100px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "orderName",
                "title":"订单号",
                "width":"100px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "endDate",
                "title": "结束日期",
                "width":"60px",
                "defaultContent": "",
                "sClass": "text-center",
                "render": function(data, type, full, meta) {
                    return  moment(data).format("YYYY-MM-DD");
                }
            }, {
                "data": "workHour",
                "title": "每日工时(小时)",
                "width":"70px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "planCount",
                "title": "计划数量",
                "width":"60px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "groupPlanID",
                "title": "操作",
                "width":"80px",
                "defaultContent": "",
                "sClass": "text-center",
            }
        ],
        "columnDefs" :
            [{
                "orderable" : false, // 禁用排序
                "targets" : [7], // 指定的列
                "data" : "groupPlanID",
                "render" : function(data, type, full, meta) {
                    return "<a href='#' style='color:#ff0000' onclick='deleteGroupPlan("+data+")'>删除</a>&nbsp;&nbsp;&nbsp;<a href='#' style='color:#3e8eea' onclick='updateGroupPlan(this,"+data+")'>修改</a>";
                }
            }],

    });
    $('#groupPlanTable_wrapper .dt-buttons').append("<button class=\"btn btn-success\" style=\"outline:none;margin-left: 10px\" onclick=\"addGroupPlan()\">录入计划</button>");
}



function addGroupPlan(groupPlanID,clothesVersionNumber,orderName,groupName,workHour,to,planCount) {
    $.blockUI({
        css: {
            width: '30%',
            top: '10%',
            border: 'none',
            padding: '15px',
            backgroundColor: '#fff',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: 1,
            color: '#000'
        },
        message: $('#editPro')
    });
    $("#workHour").val(10);
    var url = basePath + "erp/addgroupplan";
    $("#editYes").unbind("click").bind("click", function () {
        if($("#clothesVersionNumber").val().trim()=="") {
            swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入完整信息！</span>",html: true});
            return false;
        }
        if($("#orderName").val().trim()=="") {
            swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入完整信息！</span>",html: true});
            return false;
        }
        if($("#groupName").val().trim()=="") {
            swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入完整信息！</span>",html: true});
            return false;
        }
        if($("#workHour").val().trim()=="") {
            swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入完整信息！</span>",html: true});
            return false;
        }
        if($("#to").val().trim()=="") {
            swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入完整信息！</span>",html: true});
            return false;
        }
        if($("#planCount").val().trim()=="") {
            swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入完整信息！</span>",html: true});
            return false;
        }
        if (!(/(^[1-9]\d*$)/.test($("#planCount").val().trim()))){
            swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">计划数字有误！</span>",html: true});;
            return false;
        }
        $.ajax({
            url: url,
            type: 'POST',
            data: {
                groupPlanID:groupPlanID,
                clothesVersionNumber:$("#clothesVersionNumber").val(),
                orderName:$("#orderName").val(),
                groupName:$("#groupName").val(),
                workHour:$("#workHour").val(),
                to:$("#to").val(),
                planCount:$("#planCount").val().trim(),
            },
            success: function (data) {
                if(data == 0) {
                    $.unblockUI();
                    $("input").val("");
                    swal({
                            type:"success",
                            title:"",
                            text: "<span style=\"font-weight:bolder;font-size: 20px\">恭喜你，保存成功！</span>",
                            html: true
                        },
                        function(){
                            location.href=basePath+"erp/groupPlanStart";
                        });
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，保存失败！</span>",html: true});
                }
            },
            error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        });
    });
    $("#editNo").unbind("click").bind("click", function () {
        $.unblockUI();
        $("input").val("");
    });
}



function deleteGroupPlan(groupPlanID) {
    swal({
        title: "",
        text: "<span style=\"font-weight:bolder;font-size: 20px\">您确定要删除吗？</span>",
        type: "warning",
        html:true,
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: "确定",
        cancelButtonText:"我再想想",
        confirmButtonColor: "#ec6c62",
        showLoaderOnConfirm: true
    }, function() {
        $.ajax({
            url: basePath + "erp/deletegroupplan",
            type:'POST',
            data: {
                groupPlanID:groupPlanID
            },
            success: function (data) {
                if(data == 0) {
                    $.unblockUI();
                    $("input").val("");
                    swal({
                            type:"success",
                            title:"",
                            text: "<span style=\"font-weight:bolder;font-size: 20px\">恭喜你，删除成功！</span>",
                            html: true
                        },
                        function(){
                            location.href=basePath+"erp/groupPlanStart";
                        });
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，删除失败！</span>",html: true});
                }
            },
            error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        })
    });
}


function updateGroupPlan(obj,groupPlanID){
    $("#groupName1").val($(obj).parent().parent().find("td").eq(1).text());
    $("#clothesVersionNumber1").val($(obj).parent().parent().find("td").eq(2).text());
    $("#orderName1").val($(obj).parent().parent().find("td").eq(3).text());
    $("#to1").val($(obj).parent().parent().find("td").eq(4).text());
    $("#workHour1").val($(obj).parent().parent().find("td").eq(5).text());
    $("#planCount1").val($(obj).parent().parent().find("td").eq(6).text());
    $.blockUI({
        css: {
            width: '30%',
            top: '10%',
            border: 'none',
            padding: '15px',
            backgroundColor: '#fff',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: 1,
            color: '#000'
        },
        message: $('#editUpdatePro')
    });

    $("#editUpdateYes").unbind("click").bind("click", function () {
        var flag = false;
        if ($("#groupName1").val().trim()==""){
            flag = true;
        }
        if ($("#clothesVersionNumber1").val().trim() == ""){
            flag = true;
        }
        if ($("#orderName1").val().trim() == ""){
            flag = true;
        }
        if ($("#groupName1").val().trim() == ""){
            flag = true;
        }
        if ($("#to1").val().trim() == ""){
            flag = true;
        }
        if ($("#workHour1").val().trim() == ""){
            flag = true;
        }
        if ($("#planCount1").val().trim() == ""){
            flag = true;
        }
        if(flag) {
            swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请填写完所有字段信息！</span>",html: true});
            return false;
        }
        var groupPlan = {};
        groupPlan.groupPlanID = groupPlanID;
        groupPlan.groupName = $("#groupName1").val();
        groupPlan.clothesVersionNumber = $("#clothesVersionNumber1").val();
        groupPlan.orderName = $("#orderName1").val();
        groupPlan.endDate = $("#to1").val();
        groupPlan.workHour = $("#workHour1").val();
        groupPlan.planCount = $("#planCount1").val();
        $.ajax({
            url: basePath + "erp/updategroupplan",
            type:'POST',
            data: {
                groupPlan:JSON.stringify(groupPlan)
            },
            success: function (data) {
                if(data == 0) {
                    $.unblockUI();
                    $("#editUpdatePro input").val("");
                    swal({
                            type:"success",
                            title:"",
                            text: "<span style=\"font-weight:bolder;font-size: 20px\">恭喜你，保存成功！</span>",
                            html: true
                        },
                        function(){
                            location.href=basePath+"erp/groupPlanStart";
                        });
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，保存失败！</span>",html: true});
                }
            },
            error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        })
    });
    $("#editUpdateNo").unbind("click").bind("click", function () {
        $.unblockUI();
        $("#editUpdatePro input,select").val("");
    });
}


function transDate(data) {return  moment(data).format("YYYY-MM-DD");};