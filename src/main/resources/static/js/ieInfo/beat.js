var form;
var groupNameDemo;
var procedureDemo;
var employeeNameDemo;
layui.use('form', function(){
    form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
    form.render();
});
layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.21'
}).extend({                         // 模块别名
    soulTable: 'soulTable'
});

$(document).ready(function () {

    form.render('select');
    // $("#orderName").next().find("input")[0].style.width = '200px';
    layui.laydate.render({
        elem: '#beginDate',
        trigger: 'click'
    });
    layui.laydate.render({
        elem: '#endDate',
        trigger: 'click'
    });

    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumber", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });

    $("#orderName").bind('blur change',function(){
        var orderName = $("#orderName").val();
        $.ajax({
            url: "/erp/getprocedureinfobyorder",
            type: 'GET',
            data: {
                orderName:orderName
            },
            success:function(data){
                var procedureData = [];
                if (data.procedureInfoList) {
                    $.each(data.procedureInfoList, function(index,element){
                        var tmpProcedure = {};
                        tmpProcedure.name = element.procedureNumber+'-'+element.procedureName;
                        tmpProcedure.value = element.procedureNumber;
                        procedureData.push(tmpProcedure);
                    })
                }
                procedureDemo.update({
                    data: procedureData
                });
            }, error: function () {
                layer.msg("获取工序信息失败", { icon: 2 });
            }
        });
    });

    $("#clothesVersionNumber").bind('blur change',function(){
        var orderName = $("#orderName").val();
        $.ajax({
            url: "/erp/getprocedureinfobyorder",
            type: 'GET',
            data: {
                orderName:orderName
            },
            success:function(data){
                var procedureData = [];
                if (data.procedureInfoList) {
                    $.each(data.procedureInfoList, function(index,element){
                        var tmpProcedure = {};
                        tmpProcedure.name = element.procedureNumber+'-'+element.procedureName;
                        tmpProcedure.value = element.procedureNumber;
                        procedureData.push(tmpProcedure);
                    })
                }
                procedureDemo.update({
                    data: procedureData
                });
            }, error: function () {
                layer.msg("获取工序信息失败", { icon: 2 });
            }
        });
    });

    groupNameDemo = xmSelect.render({
        el: '#groupName',
        toolbar: {
            show: true
        },
        on: function(data){
            //arr:  当前多选已选中的数据
            var arr = data.arr;
            var groupList = [];
            $.each(arr, function(index,element){
                groupList.push(element.value);
            });
            if (groupList.length > 0){

                $.ajax({
                    url: "/erp/getemployeebygroupnames",
                    type: 'GET',
                    data: {
                        groupList:groupList
                    },
                    success:function(data){
                        var employeeData = [];
                        if (data.groupEmployeeList) {
                            $.each(data.groupEmployeeList, function(index,element){
                                var tmpEmployee = {};
                                tmpEmployee.name = element.employeeName;
                                tmpEmployee.value = element.employeeName;
                                employeeData.push(tmpEmployee);
                            })
                        }
                        employeeNameDemo.update({
                            data: employeeData
                        });
                    }, error: function () {
                        layer.msg("获取员工信息失败", { icon: 2 });
                    }
                });
            }
        },
        data: []
    });

    procedureDemo = xmSelect.render({
        el: '#procedureNumber',
        toolbar: {
            show: true
        },
        data: []
    });
    employeeNameDemo = xmSelect.render({
        el: '#employeeName',
        toolbar: {
            show: true
        },
        data: []
    });

    $.ajax({
        url: "/erp/getallgroupname",
        type: 'GET',
        data: {
        },
        success:function(data){
            var groupData = [];
            if (data.groupList) {
                $.each(data.groupList, function(index,element){
                    var tmpGroup = {};
                    tmpGroup.name = element;
                    tmpGroup.value = element;
                    groupData.push(tmpGroup);
                })
            }
            groupNameDemo.update({
                data: groupData
            });
        }, error: function () {
            layer.msg("获取组别信息失败", { icon: 2 });
        }
    });

});

var beatTable;

layui.use(['form', 'soulTable', 'table'], function () {
    var table = layui.table,
        soulTable = layui.soulTable,
        $ = layui.$;
    var load = layer.load();

    $(".layui-form-select,.layui-input").css("padding-right",0);

    beatTable = table.render({
        elem: '#beatTable'
        ,cols:[[
            {type:'numbers', align:'center', title:'序号', width:80}
            ,{field:'employeeName', title:'姓名', align:'center', width:100, sort: true, filter: true, totalRowText: '合计'}
            ,{field:'employeeNumber', title:'工号', align:'center', width:100, sort: true, filter: true}
            ,{field:'groupName', title:'组名', align:'center', width:90, sort: true, filter: true}
            ,{field:'beginTime', title:'开始时间', align:'center', width:200, sort: true, filter: true,templet: function (d) {
                    return moment(d.beginTime).format("YYYY-MM-DD HH:mm:ss");
                }}
            ,{field:'endTime', title:'结束时间', align:'center', width:200, sort: true, filter: true,templet: function (d) {
                    return moment(d.endTime).format("YYYY-MM-DD HH:mm:ss");
                }}
            ,{field:'timeCount', title:'计时', align:'center',width:90, sort: true, totalRow: true,templet: function (d) {
                    return toDecimalFour(d.timeCount);
                },filter: true,excel:{cellType: 'n'}}
            ,{field:'colorName', title:'颜色', align:'center', width:90, sort: true, filter: true}
            ,{field:'sizeName', title:'尺码', align:'center', width:90, sort: true, filter: true}
            ,{field:'procedureNumber', title:'工序号', align:'center', width:90, sort: true, filter: true,excel:{cellType: 'n'}}
            ,{field:'procedureName', title:'工序名', align:'center', width:150, sort: true, filter: true}
            ,{field:'layerCount', title:'数量', align:'center', width:90, sort: true, filter: true, totalRow: true,excel:{cellType: 'n'}}
        ]]
        ,loading:true
        ,data:[]
        ,height: 'full-130'
        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
        ,defaultToolbar: ['filter', 'print']
        ,title: '节拍数据表'
        ,totalRow: true
        ,page: true
        ,even: true
        ,limits: [500, 1000, 1500]
        ,limit: 500 //每页默认显示的数量
        ,done: function () {
            soulTable.render(this);
            layer.close(load);
        }
        ,filter: {
            bottom: true,
            clearFilter: false,
            items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
        }
    });

    //监听提交
    form.on('submit(searchBeat)', function(data){
        var data = {};
        var isInput = true;
        var orderName = $("#orderName").val();
        if (orderName == null || orderName == null){
            isInput = false;
        }
        var procedureNumberList = [];
        var procedureInfo = {};
        var groupNameList = [];
        var employeeNameList = [];
        $.each(procedureDemo.getValue(), function(index,element){
            procedureNumberList.push(element.value);
            procedureInfo[element.value] = element.name.split("-")[1];
        });
        $.each(groupNameDemo.getValue(), function(index,element){
            if (element.value.indexOf("车缝") != -1){
                groupNameList.push(element.value.substring(2));
            } else if (element.value.indexOf("后整") != -1){
                groupNameList.push(element.value.substring(2));
            }else {
                groupNameList.push(element.value);
            }

        });
        $.each(employeeNameDemo.getValue(), function(index,element){
            employeeNameList.push(element.value);
        });
        if (procedureNumberList.length === 0){
            isInput = false;
        }
        if (!isInput){
            layer.msg("款号-工序必须", { icon: 2 });
        }else {
            data.orderName = orderName;
            data.procedureNumberList = procedureNumberList;
            data.procedureInfo = JSON.stringify(procedureInfo);
            if (groupNameList.length > 0) {
                data.groupNameList = groupNameList;
            }
            if (employeeNameList.length > 0) {
                data.employeeNameList = employeeNameList;
            }
            var from = $("#beginDate").val();
            var to = $("#endDate").val();
            if (from != null && from != "") {
                data.from = from;
            }
            if (to != null && to != "") {
                data.to = to;
            }
            var load = layer.load(2);
            $.ajax({
                url: "/erp/getmetrebyinfo",
                type: 'GET',
                data: data,
                success: function (res) {
                    if (res.metreList) {
                        layui.table.reload("beatTable", {
                            cols:[[
                                {type:'numbers', align:'center', title:'序号', width:80}
                                ,{field:'employeeName', title:'姓名', align:'center', width:100, sort: true, filter: true, totalRowText: '合计'}
                                ,{field:'employeeNumber', title:'工号', align:'center', width:100, sort: true, filter: true}
                                ,{field:'groupName', title:'组名', align:'center', width:90, sort: true, filter: true}
                                ,{field:'beginTime', title:'开始时间', align:'center', width:200, sort: true, filter: true,templet: function (d) {
                                        return moment(d.beginTime).format("YYYY-MM-DD HH:mm:ss");
                                    }}
                                ,{field:'endTime', title:'结束时间', align:'center', width:200, sort: true, filter: true,templet: function (d) {
                                        return moment(d.endTime).format("YYYY-MM-DD HH:mm:ss");
                                    }}
                                ,{field:'timeCount', title:'计时', align:'center',width:90, sort: true, totalRow: true,templet: function (d) {
                                        return toDecimalFour(d.timeCount);
                                    },filter: true,excel:{cellType: 'n'}}
                                ,{field:'colorName', title:'颜色', align:'center', width:90, sort: true, filter: true}
                                ,{field:'sizeName', title:'尺码', align:'center', width:90, sort: true, filter: true}
                                ,{field:'procedureNumber', title:'工序号', align:'center', width:90, sort: true, filter: true,excel:{cellType: 'n'}}
                                ,{field:'procedureName', title:'工序名', align:'center', width:150, sort: true, filter: true}
                                ,{field:'layerCount', title:'数量', align:'center', width:90, sort: true, filter: true, totalRow: true,excel:{cellType: 'n'}}
                            ]]
                            ,excel: {
                                filename: '节拍查询表.xlsx'
                            }
                            ,data: res.metreList   // 将新数据重新载入表格
                            ,overflow: 'tips'
                            ,done: function () {
                                soulTable.render(this);
                                layer.close(load);
                            }
                            ,filter: {
                                bottom: true,
                                clearFilter: false,
                                items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                            }
                        });
                    } else {
                        layer.msg("获取失败！", {icon: 2});
                    }
                }, error: function () {
                    layer.msg("获取失败！", {icon: 2});
                }
            });
        }
        return false;
    });

    table.on('toolbar(beatTable)', function(obj) {
        if (obj.event === 'clearFilter') {
            // 清除所有筛选条件并重载表格
            // 参数: tableId
            soulTable.clearFilter('beatTable')
        } else if (obj.event === 'exportExcel') {
            soulTable.export('beatTable');
        }
    })

});

function toDecimalFour(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*10000)/10000;
    return f;
}