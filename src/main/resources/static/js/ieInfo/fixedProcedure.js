layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.6'
}).extend({                         // 模块别名
    soulTable: 'soulTable'
});
var reportTable;
layui.use(['form', 'soulTable', 'table'], function () {
    var table = layui.table,
        form = layui.form,
        soulTable = layui.soulTable;
    reportTable = table.render({
        elem: '#reportTable'
        ,url:'getallfixedprocedure'
        ,cols: [[]]
        ,loading:true
        ,done: function (res, curr, count) {
            table.init('reportTable',{//转换成静态表格
                cols:[[
                    {type:'numbers', align:'center', title:'序号', width:150}
                    ,{field:'procedureNumber', title:'工序号', align:'center', width:250, sort: true, filter: true}
                    ,{field:'procedureName', title:'工序名', align:'center', width:250, sort: true, filter: true}
                    ,{field:'procedureSection', title:'工段', align:'center', width:250, sort: true, filter: true}
                    ,{field: 'id', title:'操作', align:'center', toolbar: '#barTop', width:120}
                ]]
                ,data:res.data
                ,height: 'full-50'
                ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                ,defaultToolbar: ['filter', 'print']
                ,title: '固定工序'
                ,totalRow: true
                ,page: true
                ,limits: [50, 100, 200]
                ,limit: 100 //每页默认显示的数量
                ,overflow: 'tips'
                ,done: function () {
                    soulTable.render(this);
                }
                ,filter: {
                    bottom: true,
                    items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                }
            });
        }
    });


    table.on('toolbar(reportTable)', function(obj) {
        if (obj.event === 'refresh') {
            reportTable.reload();
        } else if (obj.event === 'add') {
            var index = layer.open({
                type: 1 //Page层类型
                , title: '添加'
                , btn: ['添加']
                , shade: 0.6 //遮罩透明度
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , area: ['345px', '300px']
                , content: "<table>\n" +
                    "                <tr>\n" +
                    "                    <td style=\"text-align: right;margin-bottom: 15px;margin-top: 15px;width: 100px\">\n" +
                    "                        <label class=\"layui-form-label\">工序号</label>\n" +
                    "                    </td>\n" +
                    "                    <td style=\"margin-bottom: 15px;margin-top: 15px\">\n" +
                    "                        <input type=\"text\" id=\"procedureNumber\" class=\"layui-input\">\n" +
                    "                    </td>\n" +
                    "                </tr>\n" +
                    "                <tr>\n" +
                    "                    <td style=\"text-align: right;margin-bottom: 15px;width: 100px\">\n" +
                    "                        <label class=\"layui-form-label\">工序名</label>\n" +
                    "                    </td>\n" +
                    "                    <td style=\"margin-bottom: 15px;\">\n" +
                    "                        <input type=\"text\" id=\"procedureName\" class=\"layui-input\">\n" +
                    "                    </td>\n" +
                    "                </tr>\n" +
                    "                <tr>\n" +
                    "                    <td style=\"text-align: right;margin-bottom: 15px;width: 100px\">\n" +
                    "                        <label class=\"layui-form-label\">工段</label>\n" +
                    "                    </td>\n" +
                    "                    <td style=\"margin-bottom: 15px;\">\n" +
                    "                        <select type=\"text\" id=\"procedureSection\" class=\"layui-input\"><option value='裁床'>裁床</option><option value='车缝'>车缝</option><option value='后整'>后整</option></select>\n" +
                    "                    </td>\n" +
                    "                </tr>\n" +
                    "            </table>"
                , yes: function (index, layero) {
                    var procedureNumber= $("#procedureNumber").val();
                    var procedureName= $("#procedureName").val();
                    var procedureSection= $("#procedureSection").val();
                    var param = {};
                    param.procedureSection = procedureSection;
                    param.procedureNumber = procedureNumber;
                    param.procedureName = procedureName;
                    if (procedureNumber == null || procedureNumber == "" || procedureName == null || procedureName == ""){
                        layer.msg("请填写完整", { icon: 2 });
                        return false;
                    }
                    $.ajax({
                        url: "/erp/addfixedprocedure",
                        type: 'POST',
                        data: param,
                        success: function (res) {
                            layer.closeAll();
                            if (res.result == 0){
                                reportTable.reload();
                                layer.msg("录入成功！", {icon: 1});
                            }
                        }, error: function () {
                            layer.msg("添加失败！", {icon: 2});
                        }
                    })
                }
                , cancel: function (i, layero) {
                    $("#procedureName").val();
                    $("#procedureNumber").val();
                    layer.closeAll();
                }
            });
            form.render('select');
            return false;
        }
    });

    //监听行工具事件
    table.on('tool(reportTable)', function(obj){
        var data = obj.data;
        if(obj.event === 'del'){
            layer.confirm('真的删除吗', function(index){
                $.ajax({
                    url: "/erp/deletefixedprocedure",
                    type: 'POST',
                    data: {
                        id: data.id
                    },
                    success: function (res) {
                        if (res.result == 0) {
                            reportTable.reload();
                            layer.close(index);
                            layer.msg("删除成功！", {icon: 1});
                        } else {
                            layer.msg("删除失败！", {icon: 2});
                        }
                    },
                    error: function () {
                        layer.msg("删除失败！", {icon: 2});
                    }
                })

            });
        }
    })

});