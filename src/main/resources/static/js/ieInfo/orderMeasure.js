var form;
layui.use('form', function(){
    form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
    form.render();
});
layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.21'
}).extend({                         // 模块别名
    soulTable: 'soulTable'
});

$(document).ready(function () {

    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumber", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '款号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '单号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '款号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '单号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });

});
$(document).on('keydown', '.layui-table-edit', function (e) {
    var td = $(this).parent('td')
        ,tr = td.parent('tr')
        ,trs = tr.parent().parent().find('tr')
        ,tr_index = tr.index()
        ,td_index = td.index()
        ,td_last_index = tr.find('[data-edit="text"]:last').index()
        ,td_first_index = tr.find('[data-edit="text"]:first').index();
    switch (e.keyCode) {
        case 13:
            tr.next().find('td').eq(td_index).click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            return false;
        case 39:
            td.nextAll('[data-edit="text"]:first').click();
            if (td_index == td_last_index){
                tr.next().find('td').eq(td_first_index).click();
                if (tr_index == trs.length - 1){
                    trs.eq(0).find('td').eq(td_first_index).click();
                }
            }
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
        case 37:
            td.prevAll('[data-edit="text"]:first').click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
        case 38:
            tr.prev().find('td').eq(td_index).click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
        case 40:
            tr.next().find('td').eq(td_index).click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
    }

});
var dataTable, orderMeasureItemTable;

layui.use(['form', 'soulTable', 'table'], function () {
    var table = layui.table,
        soulTable = layui.soulTable,
        form = layui.form,
        $ = layui.$;

    dataTable = table.render({
        elem: '#dataTable'
        ,cols:[[]]
        ,loading:true
        ,data:[]
        ,height: 'full-130'
        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
        ,title: '尺寸数据'
        ,totalRow: true
        ,page: true
        ,even: true
        ,limits: [500, 1000, 1500]
        ,limit: 500 //每页默认显示的数量
        ,done: function () {
            soulTable.render(this);
        }
    });

    initTable('');

    function initTable(orderName){
        var param = {};
        if (orderName != null && orderName != ''){
            param.orderName = orderName;
        }
        $.ajax({
            url: "/erp/getordermeasurebyinfo",
            type: 'GET',
            data: param,
            success: function (res) {
                if (res.orderMeasureList) {
                    var reportData = res.orderMeasureList;
                    dataTable = table.render({
                        elem: '#dataTable'
                        ,cols:[[
                            {type:'numbers', align:'center', title:'序号'}
                            ,{field:'clothesVersionNumber', title:'款号', align:'center', minWidth:100, sort: true, filter: true}
                            ,{field:'orderName', title:'单号', align:'center', minWidth:100, sort: true, filter: true}
                            ,{field:'createTime', title:'创建时间', align:'center', minWidth:200, sort: true, filter: true,templet: function (d) {
                                    return moment(d.createTime).format("YYYY-MM-DD HH:mm:ss");
                            }}
                            ,{fixed: 'right', title:'操作', align:'center', toolbar: '#toolbar', width:180}
                        ]]
                        ,data: reportData
                        ,height: 'full-130'
                        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                        ,title: '尺寸数据'
                        ,totalRow: true
                        ,page: true
                        ,even: true
                        ,limits: [500, 1000, 1500]
                        ,limit: 500 //每页默认显示的数量
                        ,done: function () {
                            soulTable.render(this);
                        }
                    });
                } else {
                    layer.msg("获取失败！", {icon: 2});
                }
            }, error: function () {
                layer.msg("获取失败！", {icon: 2});
            }
        });
    }

    //监听提交
    form.on('submit(searchBeat)', function(data){
        var data = {};
        var isInput = true;
        var orderName = $("#orderName").val();
        if (orderName == null || orderName == null){
            isInput = false;
        }
        var procedureNumberList = [];
        var procedureInfo = {};
        var groupNameList = [];
        var employeeNameList = [];
        $.each(procedureDemo.getValue(), function(index,element){
            procedureNumberList.push(element.value);
            procedureInfo[element.value] = element.name.split("-")[1];
        });
        $.each(groupNameDemo.getValue(), function(index,element){
            if (element.value.indexOf("车缝") != -1){
                groupNameList.push(element.value.substring(2));
            } else if (element.value.indexOf("后整") != -1){
                groupNameList.push(element.value.substring(2));
            }else {
                groupNameList.push(element.value);
            }

        });
        $.each(employeeNameDemo.getValue(), function(index,element){
            employeeNameList.push(element.value);
        });
        if (procedureNumberList.length === 0){
            isInput = false;
        }
        if (!isInput){
            layer.msg("单号-工序必须", { icon: 2 });
        }else {
            data.orderName = orderName;
            data.procedureNumberList = procedureNumberList;
            data.procedureInfo = JSON.stringify(procedureInfo);
            if (groupNameList.length > 0) {
                data.groupNameList = groupNameList;
            }
            if (employeeNameList.length > 0) {
                data.employeeNameList = employeeNameList;
            }
            var from = $("#beginDate").val();
            var to = $("#endDate").val();
            if (from != null && from != "") {
                data.from = from;
            }
            if (to != null && to != "") {
                data.to = to;
            }
            var load = layer.load(2);
            $.ajax({
                url: "/erp/getmetrebyinfo",
                type: 'GET',
                data: data,
                success: function (res) {
                    if (res.metreList) {
                        layui.table.reload("beatTable", {
                            cols:[[
                                {type:'numbers', align:'center', title:'序号', width:80}
                                ,{field:'employeeName', title:'姓名', align:'center', width:100, sort: true, filter: true, totalRowText: '合计'}
                                ,{field:'employeeNumber', title:'工号', align:'center', width:100, sort: true, filter: true}
                                ,{field:'groupName', title:'组名', align:'center', width:90, sort: true, filter: true}
                                ,{field:'beginTime', title:'开始时间', align:'center', width:200, sort: true, filter: true,templet: function (d) {
                                        return moment(d.beginTime).format("YYYY-MM-DD HH:mm:ss");
                                    }}
                                ,{field:'endTime', title:'结束时间', align:'center', width:200, sort: true, filter: true,templet: function (d) {
                                        return moment(d.endTime).format("YYYY-MM-DD HH:mm:ss");
                                    }}
                                ,{field:'timeCount', title:'计时', align:'center',width:90, sort: true, totalRow: true,templet: function (d) {
                                        return toDecimalFour(d.timeCount);
                                    },filter: true,excel:{cellType: 'n'}}
                                ,{field:'colorName', title:'颜色', align:'center', width:90, sort: true, filter: true}
                                ,{field:'sizeName', title:'尺码', align:'center', width:90, sort: true, filter: true}
                                ,{field:'procedureNumber', title:'工序号', align:'center', width:90, sort: true, filter: true,excel:{cellType: 'n'}}
                                ,{field:'procedureName', title:'工序名', align:'center', width:150, sort: true, filter: true}
                                ,{field:'layerCount', title:'数量', align:'center', width:90, sort: true, filter: true, totalRow: true,excel:{cellType: 'n'}}
                            ]]
                            ,excel: {
                                filename: '节拍查询表.xlsx'
                            }
                            ,data: res.metreList   // 将新数据重新载入表格
                            ,overflow: 'tips'
                            ,done: function () {
                                soulTable.render(this);
                                layer.close(load);
                            }
                            ,filter: {
                                bottom: true,
                                clearFilter: false,
                                items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                            }
                        });
                    } else {
                        layer.msg("获取失败！", {icon: 2});
                    }
                }, error: function () {
                    layer.msg("获取失败！", {icon: 2});
                }
            });
        }
        return false;
    });

    table.on('toolbar(dataTable)', function(obj) {
        if (obj.event === 'clearFilter') {
            // 清除所有筛选条件并重载表格
            // 参数: tableId
            soulTable.clearFilter('beatTable')
        } else if (obj.event === 'exportExcel') {
            soulTable.export('beatTable');
        } else if (obj.event === 'add'){
            var sizeNameList = [];
            var index = layer.open({
                type: 1 //Page层类型
                , title: "尺寸工艺新增"
                , btn: ['保存','填充']
                , shade: 0.6 //遮罩透明度
                , area: '1000px'
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , content: $("#sizeDiv")
                ,yes: function(i, layero){
                    var orderMeasureHtml = tinymce.activeEditor.getContent();
                    var head = "<p style=\"text-align: center;\"><span style=\"font-size: 24pt;\"><strong>恒逸内衣尺寸表</strong></span></p>\n" +
                        "<p style=\"text-align: center;\"><span style=\"font-size: 14pt;\"><strong>单号:"+ orderName +"&nbsp; &nbsp; &nbsp;款号:"+ clothesVersionNumber +"</strong></span></p>";
                    if (orderMeasureHtml.indexOf("单号") < 0 && orderMeasureHtml.indexOf("款号") < 0){
                        orderMeasureHtml = head + orderMeasureHtml;
                    }
                    $.ajax({
                        url: "/erp/addordermeasuretext",
                        type: 'POST',
                        data: {
                            orderName: orderName,
                            clothesVersionNumber: clothesVersionNumber,
                            orderMeasureImg: orderMeasureHtml
                        },
                        success: function (res) {
                            if (res.result == 0) {
                                tinymce.activeEditor.setContent("");
                                tinymce.remove('#sizeDetailTextarea');
                                layer.msg("录入成功！", {icon: 1});
                                layer.close(index);
                            } else {
                                layer.msg("录入失败！", {icon: 2});
                            }
                        },
                        error: function () {
                            layer.msg("录入失败！", {icon: 2});
                        }
                    });
                    return false;
                }
                ,btn2: function(i, layero){
                    var data = table.cache['orderMeasureItemTable'];
                    console.log(data);
                    $.each(data, function (index, item){
                        if (item.classSize != null && item.classSize != ''){
                            var flag;
                            var flagIndex;
                            $.each(sizeNameList, function (s_index, s_item){
                                if (item[s_item] != null && item[s_item] != ''){
                                    flag = Number(item[s_item]);
                                    flagIndex = s_index;
                                }
                            });
                            var firstFlag = flag - Number(flagIndex * Number(item.classSize));
                            $.each(sizeNameList, function (s_index, s_item){
                                item[s_item] = firstFlag + Number(s_index * Number(item.classSize));
                            });
                        }
                    });
                    orderMeasureItemTable.reload({
                        data: data   // 将新数据重新载入表格
                    });
                    return false;
                }
                , cancel : function (i,layero) {
                    tinymce.activeEditor.setContent("");
                    tinymce.remove('#sizeDetailTextarea');
                }
            });
            layer.full(index);
            setTimeout(function () {
                tinymce.init({
                    selector: '#sizeDetailTextarea',
                    language: 'zh_CN',//中文
                    directionality: 'ltr',  //文字方向,ltr文字方向从左到右，rtl从右到左
                    browser_spellcheck: true,
                    contextmenu: false,
                    branding: false, //技术支持(Powered by Tiny || 由Tiny驱动)
                    menubar: false, //菜单栏
                    plugins: [
                        "print image autoresize table powerpaste"
                    ],
                    powerpaste_word_import: 'propmt',// 参数可以是propmt, merge, clear，效果自行切换对比
                    powerpaste_html_import: 'propmt',// propmt, merge, clear
                    powerpaste_allow_local_images: true,
                    paste_data_images: true,
                    toolbar: "print | undo redo | bold | alignleft aligncenter alignright alignjustify | fontselect | fontsizeselect | image table",
                    images_upload_handler: function (blobInfo, success, failure) {
                        handleImgUpload(blobInfo, success, failure)
                    }
                });
                layui.use(['yutons_sug'], function () {
                    layui.yutons_sug.render({
                        id: "clothesVersionNumberTwo", //设置容器唯一id
                        height: "300",
                        width: "550",
                        limit:"10",            limits:[10,20,50,100],
                        cols: [
                            [{
                                field: 'clothesVersionNumberTwo',
                                title: '款号',
                                align: 'left'
                            }, {
                                field: 'orderNameTwo',
                                title: '单号',
                                align: 'left'
                            }]
                        ], //设置表头
                        params: [
                            {
                                name: 'clothesVersionNumberTwo',
                                field: 'clothesVersionNumberTwo'
                            }, {
                                name: 'orderNameTwo',
                                field: 'orderNameTwo'
                            }],//设置字段映射，适用于输入一个字段，回显多个字段
                        type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
                        url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
                    });

                    layui.yutons_sug.render({
                        id: "orderNameTwo", //设置容器唯一id
                        height: "300",
                        width: "550",
                        limit:"10",            limits:[10,20,50,100],
                        cols: [
                            [{
                                field: 'clothesVersionNumberTwo',
                                title: '款号',
                                align: 'left'
                            }, {
                                field: 'orderNameTwo',
                                title: '单号',
                                align: 'left'
                            }]
                        ], //设置表头
                        params: [
                            {
                                name: 'clothesVersionNumberTwo',
                                field: 'clothesVersionNumberTwo'
                            }, {
                                name: 'orderNameTwo',
                                field: 'orderNameTwo'
                            }],//设置字段映射，适用于输入一个字段，回显多个字段
                        type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
                        url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
                    });
                });
                orderMeasureItemTable = table.render({
                    elem: '#orderMeasureItemTable'
                    ,cols: [[]]
                    ,height: '500'
                    ,loading:false
                    ,limit: 200 //每页默认显示的数量
                    ,even: true
                    ,data: []
                    ,overflow: 'tips'
                    ,done: function () {
                        soulTable.render(this);
                    }
                });
                form.on('submit(searchBeatSize)', function(data){
                    var orderNameTwo = $("#orderNameTwo").val();
                    $.ajax({
                        url: "/erp/getordermeasureitemlistbyorder",
                        type: 'GET',
                        data: {
                            orderName:orderNameTwo
                        },
                        success: function (res) {
                            if (res.sizeNameList){
                                sizeNameList = res.sizeNameList;
                                sizeNameList.sort(function(a,b){
                                    // order是规则  objs是需要排序的数组
                                    var order = ["XXXXS","4XS","XXXS","3XS","XXS","2XS","xxs","XS","xs","S","s","M","m","L","l","XL","xl","XXL","2XL","xxl","XXXL","3XL","XXXXL","4XL","XXXXXL","5XL","xxxl","070","70","073","075","75","080","80","085","090","90","095","95", "100", "105","110","115", "120","125", "130","135",
                                        "140","145", "150","155", "160","165", "170","175", "180","185","190","195","200","XXS/155/72A","XS/160/76A","S/165/80A","M/170/84A","L/175/88A","XL/180/92A","XXL/185/96A","XXXL/190/100A","XXXXL/190/104A","XXS/145/72A","XS/150/76A","S/155/80A","M/160/84A","L/165/88A","XL/170/92A","XXL/175/96A","XXXL/180/100A","XXXXL/180/104A","XXXXXL/185/108A","110/56","120/60","130/64","140/68","150/72","160/76","170/80","2T","3T","4T","5T","4/5","6/6X","7/8","10/12","04(110-56)","06(120-60)","08(130-64)","10(140-64)","12(150-72)","14(155-76)","18M/73","24M/80","3Y/90","4Y/100","5Y/110","6Y/120","7Y/130","9Y/140","11Y/150","13Y/160"];
                                    return order.indexOf(a) - order.indexOf(b);
                                });
                                var title = [
                                    {field:'partCode', title:'部位', align:'center', minWidth:70, sort: true, filter: true, edit:'text'}
                                    ,{field:'partName', title:'部位名称', align:'center', minWidth:120, sort: true, filter: true, edit:'text'}
                                ]
                                var firstLine = {partCode:'',partName:'',diffSize:'',classSize:''};
                                $.each(sizeNameList, function (index, item){
                                    firstLine[item] = '';
                                    var tmp = {};
                                    tmp.field = item;
                                    tmp.title = item;
                                    tmp.align = 'center';
                                    tmp.minWidth = 70;
                                    tmp.sort = true;
                                    tmp.filter = true;
                                    tmp.edit = 'text';
                                    title.push(tmp);
                                });
                                title.push({field: 'diffSize', title:'误差', align:'center', minWidth:80, sort: true, filter: true, edit:'text'});
                                title.push({field: 'classSize', title:'档差', align:'center', minWidth:80, sort: true, filter: true, edit:'text'});
                                title.push({field: 'operation', title: '操作', align:'center', minWidth:120, sort: true, filter: true, templet: function(d){
                                        return '<a class="layui-btn layui-btn-xs" lay-event="add"><i class="layui-icon layui-icon-addition" style="margin-right:0"></i></a><a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del"><i class="layui-icon layui-icon-subtraction" style="margin-right:0"></i></a>'
                                    }});
                                var reportData = [];
                                reportData.push(firstLine);
                                orderMeasureItemTable = table.render({
                                    elem: '#orderMeasureItemTable'
                                    ,cols: [title]
                                    ,height: '500'
                                    ,loading:false
                                    ,limit: 200 //每页默认显示的数量
                                    ,even: true
                                    ,data: reportData
                                    ,overflow: 'tips'
                                    ,done: function () {
                                        soulTable.render(this);
                                    }
                                });
                                table.on('tool(orderMeasureItemTable)', function(obj){
                                    var data = table.cache.orderMeasureItemTable;
                                    if(obj.event === 'del'){
                                        if (data.length > 1){
                                            obj.del();
                                            $.each(data,function (index,item) {
                                                if(item instanceof Array) {
                                                    data.splice(index,1);
                                                }
                                            })
                                            orderMeasureItemTable.reload({
                                                data:data   // 将新数据重新载入表格
                                            })
                                        }
                                    } else if(obj.event === 'add') {
                                        data.push(firstLine);
                                        orderMeasureItemTable.reload({
                                            data:data   // 将新数据重新载入表格
                                        })
                                    }
                                });
                            }
                        }, error: function () {
                            layer.msg("获取失败");
                        }
                    });
                    return false;
                });
            },100);
        }
    })

});

function toDecimalFour(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*10000)/10000;
    return f;
}

function handleImgUpload(blobInfo, success, failure){
    var formdata = new FormData();
    // append 方法中的第一个参数就是 我们要上传文件 在后台接收的文件名
    // 这个值要根据后台来定义
    // 第二个参数是我们上传的文件
    formdata.append('file', blobInfo.blob());
    $.ajax({
        url: "/erp/wangEditorUploadImg",
        type: 'post',
        dataType:'json',
        contentType:false,//ajax上传图片需要添加
        processData:false,//ajax上传图片需要添加
        data: formdata,
        success: function (res) {
            if(res.data) {
                console.log(res)
                success(res.data[0]);
            }else {
                failure("上传图片失败");
            }
        },
        error: function () {
            layer.msg("上传图片失败", { icon: 2 });
        }
    });

}