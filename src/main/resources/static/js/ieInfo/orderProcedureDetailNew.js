var basePath=$("#basePath").val();
var orderName = $("#orderName").val();
layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.22'
}).extend({                         // 模块别名
    soulTable: 'soulTable',
});
$(document).ready(function () {
    var orderName=$("#orderName").val();
    layui.use(['form', 'soulTable', 'table'], function () {
        var table = layui.table,
            soulTable = layui.soulTable,
            $ = layui.$;
        var orderInfoHtml = "";
        var index = layer.open({
            type: 1 //Page层类型
            , title: '订单工序详情'
            , shade: 0.6 //遮罩透明度
            , maxmin: false //允许全屏最小化
            , anim: 0 //0-6的动画形式，-1不开启
            , closeBtn :0
            , content: "<div>" +
                "    <div class='row' id='orderInfo'>" +
                "    </div>" +
                "        <table class='layui-hide' id='orderProcedureDetailTable' lay-filter='orderProcedureDetailTable'></table>" +
                "    </div>"
            , cancel : function (i,layero) {
                $("#orderInfo").empty();
            }
        });
        layer.full(index);
        setTimeout(function () {
            $.ajax({
                url: "/erp/getorderproceudrebyorder",
                type: 'POST',
                data: {
                    orderName: orderName
                },
                success: function (res) {
                    if(res.orderProcedureList) {
                        var record = res.orderProcedureList[0];
                        var subSide = "0%";
                        $.each(res.orderProcedureList,function (index,item) {
                            if (item.subsidy > 0){
                                subSide = ((100 * item.subsidy).toFixed(2)).toString() + "%";
                            }
                        });
                        var procedureList = [];
                        var changeIndex = "裁床";
                        var cutSum = {'segment':'裁床合计', 'sam':0, 'packagePrice':0, 'piecePrice':0, 'piecePriceTwo':0};
                        var produceSum = {'segment':'车缝合计', 'sam':0, 'packagePrice':0, 'piecePrice':0, 'piecePriceTwo':0};
                        var finishSum = {'segment':'后整合计', 'sam':0, 'packagePrice':0, 'piecePrice':0, 'piecePriceTwo':0};
                        var totalSum = {'segment':'总计', 'sam':0, 'packagePrice':0, 'piecePrice':0, 'piecePriceTwo':0};
                        $.each(res.orderProcedureList,function (index,item) {
                            if (item.procedureSection == "裁床"){
                                changeIndex = "裁床";
                                cutSum.sam += Number(item.sam);
                                cutSum.packagePrice += Number(item.packagePrice);
                                cutSum.piecePrice += Number(item.piecePrice);
                                cutSum.piecePriceTwo += Number(item.piecePriceTwo);
                            }
                            if(item.procedureSection == "车缝"){
                                if (changeIndex == "裁床"){
                                    cutSum.sam = toDecimalFour(cutSum.sam);
                                    cutSum.packagePrice = toDecimalFour(cutSum.packagePrice);
                                    cutSum.piecePrice = toDecimalFour(cutSum.piecePrice);
                                    cutSum.piecePriceTwo = toDecimalFour(cutSum.piecePriceTwo);
                                    procedureList.push(cutSum);
                                }
                                changeIndex = "车缝";
                                produceSum.sam += Number(item.sam);
                                produceSum.packagePrice += Number(item.packagePrice);
                                produceSum.piecePrice += Number(item.piecePrice);
                                produceSum.piecePriceTwo += Number(item.piecePriceTwo);
                            }
                            if(item.procedureSection == "后整") {
                                if (changeIndex == "车缝"){
                                    produceSum.sam = toDecimalFour(produceSum.sam);
                                    produceSum.packagePrice = toDecimalFour(produceSum.packagePrice);
                                    produceSum.piecePrice = toDecimalFour(produceSum.piecePrice);
                                    produceSum.piecePriceTwo = toDecimalFour(produceSum.piecePriceTwo);
                                    procedureList.push(produceSum);
                                }
                                changeIndex = "后整";
                                finishSum.sam += Number(item.sam);
                                finishSum.packagePrice += Number(item.packagePrice);
                                finishSum.piecePrice += Number(item.piecePrice);
                                finishSum.piecePriceTwo += Number(item.piecePriceTwo);
                            }
                            procedureList.push(item);
                        });
                        finishSum.sam = toDecimalFour(finishSum.sam);
                        finishSum.packagePrice = toDecimalFour(finishSum.packagePrice);
                        finishSum.piecePrice = toDecimalFour(finishSum.piecePrice);
                        finishSum.piecePriceTwo = toDecimalFour(finishSum.piecePriceTwo);
                        procedureList.push(finishSum);
                        totalSum.sam += Number(cutSum.sam);
                        totalSum.sam += Number(produceSum.sam);
                        totalSum.sam += Number(finishSum.sam);
                        totalSum.packagePrice += Number(cutSum.packagePrice);
                        totalSum.packagePrice += Number(produceSum.packagePrice);
                        totalSum.packagePrice += Number(finishSum.packagePrice);
                        totalSum.piecePrice += Number(cutSum.piecePrice);
                        totalSum.piecePrice += Number(produceSum.piecePrice);
                        totalSum.piecePrice += Number(finishSum.piecePrice);
                        totalSum.piecePriceTwo += Number(cutSum.piecePriceTwo);
                        totalSum.piecePriceTwo += Number(produceSum.piecePriceTwo);
                        totalSum.piecePriceTwo += Number(finishSum.piecePriceTwo);
                        totalSum.sam = toDecimalFour(totalSum.sam);
                        totalSum.packagePrice = toDecimalFour(totalSum.packagePrice);
                        totalSum.piecePrice = toDecimalFour(totalSum.piecePrice);
                        totalSum.piecePriceTwo = toDecimalFour(totalSum.piecePriceTwo);
                        procedureList.push(totalSum);
                        orderInfoHtml += "<div class='col-md-12'><table><tr><td style='width: 7%; font-size: medium; font-weight: bolder; color: #34a9db; padding-top: 20px; padding-bottom: 10px; padding-left: 10px;' align='right'>款号:</td><td style='width: 7%; font-size: medium; padding-left: 10px; padding-top: 20px; padding-bottom: 10px'>" + record.orderName + "</td>" +
                            "                                               <td style='width: 7%; font-size: medium; font-weight: bolder; color: #34a9db; padding-top: 20px; padding-bottom: 10px; padding-left: 10px;' align='right'>单号:</td><td style='width: 7%; font-size: medium; padding-left: 10px; padding-top: 20px; padding-bottom: 10px'>" + record.clothesVersionNumber + "</td>" +
                            "                                               <td style='width: 7%; font-size: medium; font-weight: bolder; color: #34a9db; padding-top: 20px; padding-bottom: 10px; padding-left: 10px;' align='right'>款式描述:</td><td style='width: 7%; font-size: medium; padding-left: 10px; padding-top: 20px; padding-bottom: 10px'>" + record.styleDescription + "</td>" +
                            "                                               <td style='width: 7%; font-size: medium; font-weight: bolder; color: #34a9db; padding-top: 20px; padding-bottom: 10px; padding-left: 10px;' align='right'>客户:</td><td style='width: 7%; font-size: medium; padding-left: 10px; padding-top: 20px; padding-bottom: 10px'>"+ record.customerName +"</td>" +
                            "                                               <td style='width: 7%; font-size: medium; font-weight: bolder; color: #34a9db; padding-top: 20px; padding-bottom: 10px; padding-left: 10px;' align='right'>生产数量:</td><td style='width: 7%; font-size: medium; padding-left: 10px; padding-top: 20px; padding-bottom: 10px'>" + record.orderCount + "</td>" +
                            "                                               <td style='width: 7%; font-size: medium; font-weight: bolder; color: #34a9db; padding-top: 20px; padding-bottom: 10px; padding-left: 10px;' align='right'>开单日期:</td><td style='width: 7%; font-size: medium; padding-left: 10px; padding-top: 20px; padding-bottom: 10px'>" + record.beginDate + "</td>" +
                            "                                               <td style='width: 7%; font-size: medium; font-weight: bolder; color: #34a9db; padding-top: 20px; padding-bottom: 10px; padding-left: 10px;' align='right'>补贴比例:</td><td style='width: 7%; font-size: medium; padding-left: 10px; padding-top: 20px; padding-bottom: 10px'>" + subSide + "</td></tr>";
                        orderInfoHtml += "</table>";
                        $("#orderInfo").append(orderInfoHtml);
                        var load = layer.load();
                        table.render({
                            elem: '#orderProcedureDetailTable'
                            ,cols: [[
                                {field: 'styleType', title: '款式',align:'center', minWidth: 90, sort: true, filter: true},
                                {field: 'partName', title: '部位',align:'center', minWidth: 100, sort: true, filter: true},
                                {field: 'procedureCode', title: '工序代码',align:'center', minWidth: 80, sort: true, filter: true},
                                {field: 'procedureNumber', title: '工序号',align:'center', minWidth: 80, sort: true, filter: true},
                                {field: 'procedureName', title: '工序名称',align:'center', minWidth: 120, sort: true, filter: true},
                                {field: 'procedureDescription', title: '工序描述',align:'center', minWidth: 120, sort: true, filter: true},
                                {field: 'remark', title: '备注',align:'center', minWidth: 80, sort: true, filter: true},
                                {field: 'segment', title: '码段',align:'center', minWidth: 80, sort: true, filter: true},
                                {field: 'equType', title: '设备',align:'center', minWidth: 80, sort: true, filter: true},
                                {field: 'procedureLevel', title: '等级',align:'center', minWidth: 60, sort: true, filter: true},
                                {field: 'procedureSection', title: '工段',align:'center', minWidth: 60, totalRowText: '合计', sort: true, filter: true},
                                {field: 'sam', title: 'SAM值',align:'center', minWidth: 80, sort: true, filter: true, excel:{cellType: 'n'}},
                                {field: 'packagePrice', title: '工价/打',align:'center', minWidth: 80, sort: true, filter: true, excel:{cellType: 'n'}},
                                {field: 'piecePrice', title: '工价/件',align:'center', minWidth: 80, sort: true, filter: true, excel:{cellType: 'n'}},
                                {field: 'piecePriceTwo', title: '浮动',align:'center', minWidth: 80, sort: true, filter: true, excel:{cellType: 'n'}},
                                {field: 'procedureState', title: '审核状态',align:'center', minWidth: 90, sort: true, filter: true, templet:function (d) {
                                        if (d.procedureState === 0){
                                            return "未提交";
                                        } else if (d.procedureState === 1){
                                            return "审核中";
                                        } else if (d.procedureState === 2){
                                            return "审核通过";
                                        } else if (d.procedureState === 3){
                                            return "未通过";
                                        } else {
                                            return "";
                                        }
                                    }},
                                {field: 'scanPart', title: '计件部位',align:'center', minWidth: 100, sort: true, filter: true},
                                {field: 'colorName', title: '颜色',align:'center', minWidth: 80, sort: true, filter: true},
                                {field: 'sizeName', title: '尺码',align:'center', minWidth: 80, sort: true, filter: true}
                            ]]
                            ,loading:false
                            ,data: procedureList
                            ,height: 'full-150'
                            ,title: '订单工序详情'
                            ,totalRow: false
                            ,even: true
                            ,page: true
                            ,overflow: 'tips'
                            ,toolbar: '<script type="text/html" id="toolbarTop">\n' +
                                '                <div class="layui-btn-container">\n' +
                                '                    <button class="layui-btn layui-btn-sm" lay-event="exportExcel">导出</button>\n' +
                                '                </div>\n' +
                                '            </script>'
                            ,defaultToolbar: ['filter', 'print']
                            ,limit: 100
                            ,limits: [100, 200, 500]
                            ,excel:{ // 导出excel配置, （以下值均为默认值）
                                on: true, //是否启用, 默认开启
                                add: {
                                    top: {
                                        data: [
                                            ['德悦服饰工序工价表', '', '', '', '', '', '', '', ''],
                                            ['款号',record.orderName,'单号',record.clothesVersionNumber,'款式描述',record.styleDescription,'客户',record.customerName, ''], //头部第一行数据，由于我设置了后面的数据merge了，就只写一个
                                            ['订单数量', record.orderCount, '开单日期', record.beginDate, '补贴比例', subSide, ''], // 头部第二行数据, 中间的空数据是为了 merge 使用
                                            ['', '', '', '', '', '', '', '', ''] // 头部第二行数据, 中间的空数据是为了 merge 使用
                                        ],
                                        merge: [['1,1','1,9'],['4,1','4,9']]
                                    }
                                },
                                filename: orderName + '工序表.xlsx',
                                columns: ['procedureNumber', 'procedureName', 'procedureDescription', 'remark', 'segment', 'sam', 'packagePrice', 'piecePrice', 'piecePriceTwo']
                            }
                            ,done: function (res, curr, count) {
                                soulTable.render(this);
                                layer.close(load);
                            }
                        });

                        table.on('toolbar(orderProcedureDetailTable)', function(obj){
                            if (obj.event === 'exportExcel') {
                                soulTable.export('orderProcedureDetailTable');
                            }
                        })

                    } else {
                        layer.msg("未获取到数据！");
                        layer.close(index);
                    }
                },
                error: function () {
                    layer.msg("获取详情信息失败！", {icon: 2});
                    layer.close(index);
                }
            })
        },500)
    })
});

function toDecimal(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*100)/100;
    return f;
}

function toDecimalFour(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*10000)/10000;
    return f;
}