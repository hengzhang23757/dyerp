layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.24'
}).extend({                         // 模块别名
    soulTable: 'soulTable',
});
var colorData = [];
var sizeData = [];
var printPartList = [];
$(document).ready(function () {
    var type = $("#hideType").val();
    layui.laydate.render({
        elem: '#beginDate',
        trigger: 'click',
        value: new Date()
    });
    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumber", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",
            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",
            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });
    $("#orderName").bind('blur change',function(){
        var orderName = $("#orderName").val();
        $("#customerName").empty();
        $.ajax({
            url: "/erp/getcustomernamebyordername",
            data: {"orderName": orderName},
            success:function(data){
                $("#customerName").val(data);
            },
            error:function(){
            }
        });
        $("#styleDescription").empty();
        $.ajax({
            url: "/erp/getstyledescriptionbyorder",
            data: {"orderName": orderName},
            success:function(data){
                $("#styleDescription").val(data);
            },
            error:function(){
            }
        });
        $("#orderCount").empty();
        $.ajax({
            url: "/erp/getordertotalcount",
            data: {"orderName": orderName},
            success:function(data){
                $("#orderCount").val(data);
            },
            error:function(){
            }
        });
        $.ajax({
            url: "/erp/getsizeandcolorbyorder",
            type: 'GET',
            data: {"orderName": orderName},
            success: function (res) {
                colorData = [];
                sizeData = [];
                colorData.push({
                    name:"全部",
                    value:"全部"
                });
                sizeData.push({
                    name:"全部",
                    value:"全部"
                });
                $.each(res.colorNameList,function (index,item) {
                    colorData.push({
                        name:item,
                        value: item
                    })
                });
                $.each(res.sizeNameList,function (index,item) {
                    sizeData.push({
                        name:item,
                        value: item
                    })
                });
            }
        });
        $.ajax({
            url: "/erp/getotherprintpartnamesbyorder",
            data: {"orderName": orderName},
            success:function(data){
                printPartList = [];
                $.each(data.printPartNameList,function (index,item) {
                    if (!item.startsWith("士啤")){
                        printPartList.push(item)
                    }
                });
            },
            error:function(){
            }
        });
    });
    $("#clothesVersionNumber").bind('blur change',function(){
        var orderName = $("#orderName").val();
        $("#customerName").empty();
        $.ajax({
            url: "/erp/getcustomernamebyordername",
            data: {"orderName": orderName},
            success:function(data){
                $("#customerName").val(data);
            },
            error:function(){
            }
        });
        $("#styleDescription").empty();
        $.ajax({
            url: "/erp/getstyledescriptionbyorder",
            data: {"orderName": orderName},
            success:function(data){
                $("#styleDescription").val(data);
            },
            error:function(){
            }
        });
        $.ajax({
            url: "/erp/getotherprintpartnamesbyorder",
            data: {"orderName": orderName},
            success:function(data){
                printPartList = [];
                $.each(data.printPartNameList,function (index,item) {
                    if (!item.startsWith("士啤")){
                        printPartList.push(item)
                    }
                });
            },
            error:function(){
            }
        });
        $("#orderCount").empty();
        $.ajax({
            url: "/erp/getordertotalcount",
            data: {"orderName": orderName},
            success:function(data){
                $("#orderCount").val(data);
            },
            error:function(){
            }
        });
        $.ajax({
            url: "/erp/getsizeandcolorbyorder",
            type: 'GET',
            data: {"orderName": orderName},
            success: function (res) {
                colorData = [];
                sizeData = [];
                colorData.push({
                    name:"全部",
                    value:"全部"
                });
                sizeData.push({
                    name:"全部",
                    value:"全部"
                });
                $.each(res.colorNameList,function (index,item) {
                    colorData.push({
                        name:item,
                        value: item
                    })
                });
                $.each(res.sizeNameList,function (index,item) {
                    sizeData.push({
                        name:item,
                        value: item
                    })
                });
            }
        })
    });
    var orderName = $("#hideOrderName").val();
    var selData = [];
    if(type === 'reAdd') {
        $("#isScanLabel").hide();
        $("#isScanSelect").hide();
        $.ajax({
            url: "/erp/getorderproceudrebyorder",
            data: {"orderName": orderName},
            type:'POST',
            async:false,
            success:function(data){

                $.each(data.orderProcedureList,function (index,item) {
                    if (item.procedureNumber > 10){
                        selData.push(item);
                    }
                });

                colorData = [];
                sizeData = [];
                colorData.push({
                    name:"全部",
                    value:"全部"
                });
                sizeData.push({
                    name:"全部",
                    value:"全部"
                });
                $.each(data.colorNameList,function (index,item) {
                    colorData.push({
                        name:item,
                        value: item
                    })
                });
                $.each(data.sizeNameList,function (index,item) {
                    sizeData.push({
                        name:item,
                        value: item
                    })
                });
                createOrderProcedureTableNew(selData);
            }, error:function(){
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        });
    }
    else if(type === 'update') {
        $("#isScanLabel").hide();
        $("#isScanSelect").hide();
        $.ajax({
            url: "/erp/getorderproceudrebyorder",
            data: {"orderName": orderName},
            type:'POST',
            async:false,
            success:function(data){
                selData = data.orderProcedureList;
                colorData = [];
                sizeData = [];
                colorData.push({
                    name:"全部",
                    value:"全部"
                });
                sizeData.push({
                    name:"全部",
                    value:"全部"
                });
                $.each(data.colorNameList,function (index,item) {
                    colorData.push({
                        name:item,
                        value: item
                    })
                });
                $.each(data.sizeNameList,function (index,item) {
                    sizeData.push({
                        name:item,
                        value: item
                    })
                });
                var procedureDetail = data.orderProcedureList[0];
                $("#orderName").val(procedureDetail.orderName);
                $("#customerName").val(procedureDetail.customerName);
                $("#styleDescription").val(procedureDetail.styleDescription);
                $("#clothesVersionNumber").val(procedureDetail.clothesVersionNumber);
                $("#beginDate").val(procedureDetail.beginDate);
                $("#orderCount").val(procedureDetail.orderCount);
                var subSide = 0;
                $.each(data.orderProcedureList,function (index,item) {
                    if (item.subsidy > 0){
                        subSide = 100 * item.subsidy.toFixed(2);
                    }
                });
                $("#subsidyPer").val(subSide);
                createOrderProcedureTableNew(selData);
            }, error:function(){
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        });
    } else {
        $("#isScanLabel").show();
        $("#isScanSelect").show();
        createOrderProcedureTableNew(selData);
    }

});
$(document).on('keydown', '.layui-table-edit', function (e) {
    var td = $(this).parent('td')
        ,tr = td.parent('tr')
        ,trs = tr.parent().parent().find('tr')
        ,tr_index = tr.index()
        ,td_index = td.index()
        ,td_last_index = tr.find('[data-edit="text"]:last').index()
        ,td_first_index = tr.find('[data-edit="text"]:first').index();
    switch (e.keyCode) {
        case 13:
            tr.next().find('td').eq(td_index).click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            return false;
        case 39:
            td.nextAll('[data-edit="text"]:first').click();
            if (td_index == td_last_index){
                tr.next().find('td').eq(td_first_index).click();
                if (tr_index == trs.length - 1){
                    trs.eq(0).find('td').eq(td_first_index).click();
                }
            }
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
        case 37:
            td.prevAll('[data-edit="text"]:first').click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
        case 38:
            tr.prev().find('td').eq(td_index).click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
        case 40:
            tr.next().find('td').eq(td_index).click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
    }

})

function submitProcedure(){
    var basePath=$("#basePath").val();
    var userName=$("#userName").val();
    var operateType = $("#hideType").val();
    var subsidyPer = $("#subsidyPer").val();
    var isScan = $("#scanAvailable").val();
    var orderProcedureJson = [];
    var selectedProcedures = layui.table.cache.procedureTable;
    var orderName = $("#orderName").val();
    if(selectedProcedures.length == 0) {
        swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请选择工序！</span>",html: true});
        return false;
    }
    if($("#clothesVersionNumber").val().trim()=="") {
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入完整信息！</span>",html: true});
        return false;
    }
    if($("#orderName").val()=="" || $("#orderName").val()==null) {
        if ($("#orderCount").val().trim() == 0){
            orderName = $("#clothesVersionNumber").val().trim();
        }else{
            swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入完整信息！</span>",html: true});
            return false;
        }

    }
    if($("#beginDate").val().trim()=="") {
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入完整信息！</span>",html: true});
        return false;
    }
    if($("#orderCount").val().trim()=="") {
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入完整信息！</span>",html: true});
        return false;
    }
    if($("#styleDescription").val().trim()=="") {
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入完整信息！</span>",html: true});
        return false;
    }
    if(subsidyPer == "") {
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入完整信息！</span>",html: true});
        return false;
    }
    if (!(/(^[0-9]\d*$)/.test(subsidyPer))){
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">补贴比例输入有误！</span>",html: true});;
        return false;
    }
    var customerName = $("#customerName").val();
    var styleDescription = $("#styleDescription").val();
    var subsidy = (subsidyPer/100).toFixed(2);
    var beginDate = $("#beginDate").val();
    var orderCount = $("#orderCount").val();
    var clothesVersionNumber = $("#clothesVersionNumber").val();
    var procedureNumberList = [];
    var procedureNameList = [];
    var isRepeat = false;
    var priceFloat = false;
    $.each(selectedProcedures,function (index,item) {
        if(item.length!=0) {
            var orderProcedure = {};
            orderProcedure.orderName = orderName;
            orderProcedure.customerName = customerName;
            orderProcedure.styleDescription = styleDescription;
            orderProcedure.beginDate = beginDate;
            orderProcedure.orderCount = orderCount;
            orderProcedure.clothesVersionNumber = clothesVersionNumber;

            orderProcedure.styleType = item.styleType;
            orderProcedure.partName = item.partName;
            orderProcedure.procedureCode = item.procedureCode;
            orderProcedure.procedureNumber = item.procedureNumber;
            orderProcedure.procedureName = item.procedureName;
            orderProcedure.procedureSection = item.procedureSection;
            orderProcedure.procedureDescription = item.procedureDescription;
            orderProcedure.remark = item.remark;
            orderProcedure.segment = item.segment;
            orderProcedure.equType = item.equType;
            orderProcedure.procedureLevel = item.procedureLevel;
            orderProcedure.SAM = item.sam;
            orderProcedure.packagePrice = item.packagePrice;
            orderProcedure.piecePrice = item.piecePrice;
            orderProcedure.initPrice = item.initPrice;
            if (item.isScan == 0 || item.isScan == 1){
                orderProcedure.isScan = item.isScan;
            } else {
                orderProcedure.isScan = 1;
            }
            if(item.piecePriceTwo == "" || item.piecePriceTwo == null) {
                orderProcedure.piecePriceTwo = 0;
            }else if (!(/^[0-9]+.?[0-9]*$/.test(item.piecePriceTwo))){
                priceFloat = true;
            }else{
                orderProcedure.piecePriceTwo = item.piecePriceTwo;
            }
            orderProcedure.orderProcedureID = item.procedureID;
            if (item.scanPart == "" || item.scanPart == null){
                orderProcedure.scanPart = "主身";
            }else {
                orderProcedure.scanPart = item.scanPart.replace(/\ +/g,"").replace(/[\r\n]/g,"");
            }
            orderProcedure.procedureState = 0;
            if (orderProcedure.procedureSection == '车缝'){
                orderProcedure.subsidy = subsidy;
            } else {
                orderProcedure.subsidy = 0;
            }
            if (item.colorName.getValue("valueStr") == "" || item.colorName.getValue("valueStr") == null){
                orderProcedure.colorName = "全部";
            } else {
                orderProcedure.colorName = item.colorName.getValue("valueStr");
            }
            if (item.sizeName.getValue("valueStr") == "" || item.sizeName.getValue("valueStr") == null){
                orderProcedure.sizeName = "全部";
            } else {
                orderProcedure.sizeName = item.sizeName.getValue("valueStr");
            }
            if (procedureNumberList.indexOf(item.procedureNumber) != -1) {
                isRepeat = true;
                return false;
            }
            procedureNumberList.push(item.procedureNumber);
            procedureNameList.push(item.procedureName);

            orderProcedureJson.push(orderProcedure);
        }
    });

    if(isRepeat) {
        swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">已选工序中存在工序号相同的工序！</span>",html: true});
        return false;
    }
    if (priceFloat){
        swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">浮动工价有误！</span>",html: true});
        return false;
    }
    if (operateType == "add"){
        $.each(selectedProcedures,function (index,item) {
            if (item.procedureSection === '车缝'){
                item.isScan = isScan;
            }
        });
        if (procedureNumberList.indexOf('2026') == -1) {
            orderProcedureJson.push({orderName:orderName,customerName:customerName,clothesVersionNumber:clothesVersionNumber,styleDescription:styleDescription,beginDate:beginDate,orderCount:orderCount,styleType:'全款',
                partName:'无',procedureCode:1804,procedureNumber:2026,procedureName:'挂成衣',procedureSection:'车缝',procedureDescription:'无',remark:'无',segment:'不分',equType:'无',procedureLevel:'D',SAM:'0',packagePrice:'0',piecePrice:'0',piecePriceTwo:'0',
                scanPart:'主身',procedureState:'0',subsidy:subsidy,colorName:'全部',sizeName:'全部'})
        }
        if (procedureNumberList.indexOf('666') == -1) {
            orderProcedureJson.push({orderName:orderName,customerName:customerName,clothesVersionNumber:clothesVersionNumber,styleDescription:styleDescription,beginDate:beginDate,orderCount:orderCount,styleType:'全款',
                partName:'无',procedureCode:468,procedureNumber:666,procedureName:'大烫返工',procedureSection:'后整',procedureDescription:'无',remark:'无',segment:'不分',equType:'无',procedureLevel:'D',SAM:'0',packagePrice:'0',piecePrice:'0',piecePriceTwo:'0',
                scanPart:'主身',procedureState:'0',subsidy:subsidy,colorName:'全部',sizeName:'全部'})
        }
        if (procedureNumberList.indexOf('777') == -1) {
            orderProcedureJson.push({orderName:orderName,customerName:customerName,clothesVersionNumber:clothesVersionNumber,styleDescription:styleDescription,beginDate:beginDate,orderCount:orderCount,styleType:'全款',
                partName:'无',procedureCode:469,procedureNumber:777,procedureName:'尾查返工',procedureSection:'后整',procedureDescription:'无',remark:'无',segment:'不分',equType:'无',procedureLevel:'D',SAM:'0',packagePrice:'0',piecePrice:'0',piecePriceTwo:'0',
                scanPart:'主身',procedureState:'0',subsidy:subsidy,colorName:'全部',sizeName:'全部'})
        }
        $.each(printPartList,function (index,item) {
            var procedureName = "下" + item;
            if (procedureNameList.indexOf(procedureName) == -1){
                orderProcedureJson.push({orderName:orderName,customerName:customerName,clothesVersionNumber:clothesVersionNumber,styleDescription:styleDescription,beginDate:beginDate,orderCount:orderCount,styleType:'全款',
                    partName:'无',procedureCode: 4475 + index,procedureNumber: index+1,procedureName: procedureName,procedureSection:'裁床',procedureDescription:'无',remark:'无',segment:'不分',equType:'无',procedureLevel:'D',SAM:'0',packagePrice:'0',piecePrice:'0',piecePriceTwo:'0',
                    scanPart: item,procedureState:'0',subsidy:subsidy,colorName:'全部',sizeName:'全部'})
            }
        });
    }
    if (operateType == "update"){
        $.ajax({
            url: basePath + "erp/updateorderprocedurebatch",
            type:'POST',
            data:{
                orderProcedureJson:JSON.stringify(orderProcedureJson),
                userName:userName
            },
            success:function(data)
            {
                if(data == 0) {
                    swal({type:"success",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">添加成功！</span>",html: true});
                    layui.table.reload("procedureTable",{
                        data:[]   // 将新数据重新载入表格
                    });
                    $("#baseInfo input").val("");
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，添加失败！</span>",html: true});
                }
            },
            error:function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        });
    }
    else {
        $.ajax({
            url: basePath + "erp/addorderprocedurebatch",
            type:'POST',
            data:{
                orderProcedureJson:JSON.stringify(orderProcedureJson),
                userName:userName
            },
            success:function(data)
            {
                if(data == 0) {
                    swal({type:"success",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">添加成功！</span>",html: true});
                    layui.table.reload("procedureTable",{
                        data:[]   // 将新数据重新载入表格
                    });
                    $("#baseInfo input").val("");
                } else if(data == 4){
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">该款号工序已存在不能直接覆盖！</span>",html: true});
                } else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，添加失败！</span>",html: true});
                }
            },
            error:function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        });
    }
}

var isModified = false;
function createOrderProcedureTableNew(dataValue) {
    var type = $("#hideType").val();
    layui.use(['table','yutons_sug'], function(){
        var table = layui.table;
        //展示已知数据
        table.render({
            elem: '#procedureTable'
            ,toolbar: '#toolbarTop'
            ,cols: [[ //标题栏
                {field: 'procedureID', title: '操作',align: 'center',templet: function(d){
                        if((d.procedureState=='1' || d.procedureState=='2') && type != 'reAdd') {
                            return "";
                        }else {
                            return "<a href='#' style='color:#3f86ff' value='" + d.procedureID + "' lay-event='delProcedure' >移除</a>"
                        }
                    }}
                ,{field: 'procedureNumber', title: '工序号', width: 120,templet: function(d){
                        return '<input class="layui-input" autocomplete="off" type="text" name="procedureNumber" id="procedureNumber-'+d.LAY_TABLE_INDEX+'"/>'
                    }}
                ,{field: 'procedureName', title: '工序名', width: 200,templet: function(d){
                        return '<input class="layui-input" autocomplete="off" type="text" name="procedureName" id="procedureName-'+d.LAY_TABLE_INDEX+'"/>'
                    }}
                ,{field: 'procedureDescription', title: '工序描述', width: 250,templet: function(d){
                        return '<input class="layui-input" autocomplete="off" type="text" name="procedureDescription" id="procedureDescription-'+d.LAY_TABLE_INDEX+'"/>'
                    }}
                ,{field: 'remark', title: '备注', width: 200,templet: function(d){
                        return '<input class="layui-input" autocomplete="off" type="text" name="remark" id="remark-'+d.LAY_TABLE_INDEX+'"/>'
                    }}
                ,{field: 'styleType', title: '款式',align: 'center',edit: 'text', width: 100}
                ,{field: 'partName', title: '部位', width: 100,align: 'center',edit: 'text'}
                ,{field: 'procedureCode', title: '代码', width: 100,align: 'center'}
                ,{field: 'procedureSection', title: '工段', width: 100,align: 'center',edit: 'text'}
                ,{field: 'segment', title: '码段', width: 90,align: 'center',edit: 'text'}
                ,{field: 'equType', title: '设备', width: 80,align: 'center',edit: 'text'}
                ,{field: 'procedureLevel', title: '等级', width: 80,align: 'center',edit: 'text'}
                ,{field: 'sam', title: 'SAM', width: 80,align: 'center', edit: 'text'}
                ,{field: 'packagePrice', title: '工价/打', width: 90,align: 'center', edit: 'text'}
                ,{field: 'piecePrice', title: '工价/件', width: 90,align: 'center', edit: 'text'}
                ,{field: 'initPrice', title: '初始价', width: 90,align: 'center', edit: 'text'}
                ,{field: 'piecePriceTwo', title: '浮动', width: 90,align: 'center', edit: 'text'}
                ,{field: 'scanPart', title: '计件', width: 90,align: 'center',edit: 'text'}
                ,{field: 'colorName', title: '色组',minWidth:240,templet: function(d){
                        return '<div id="colorName-'+d.LAY_TABLE_INDEX+'"></div>'
                    }}
                ,{field: 'sizeName', title: '尺码',minWidth:240,templet: function(d){
                        return '<div id="sizeName-'+d.LAY_TABLE_INDEX+'"></div>'
                    }}
                ,{field: 'isScan', hide: true}
            ]]
            ,data: dataValue
            ,limit:Number.MAX_VALUE
            ,height: 'full-150'
            ,done: function (res, curr, count) {
                var data = layui.table.cache.procedureTable;
                var colorArray = [];
                var sizeArray = [];
                //渲染多选
                $.each(res.data,function (index, item) {
                    var initColorData = [];
                    var initSizeData = [];
                    if (data[index].colorName != '' && data[index].colorName != null) {
                        if(dataValue.length==0 || isModified) {
                            initColorData = data[index].colorName.getValue('value');
                        }else {
                            initColorData = data[index].colorName.split(",");
                        }
                    } else {
                        initColorData = ["全部"];
                    }
                    colorArray.push(initColorData);
                    if (data[index].sizeName != '' && data[index].sizeName != null) {
                        if(dataValue.length==0 || isModified) {
                            initSizeData = data[index].sizeName.getValue('value');
                        }else {
                            initSizeData = data[index].sizeName.split(",");
                        }
                    } else {
                        initSizeData = ["全部"];
                    }
                    sizeArray.push(initSizeData);

                    layui.yutons_sug.render({
                        id: "procedureNumber-" + item.LAY_TABLE_INDEX, //设置容器唯一id
                        height: "300",
                        width: "1500",
                        limit: "50",
                        limits: [10, 20, 50, 100],
                        cols: [
                            [{
                                field: 'procedureNumber',
                                title: '工序号',
                                width: '6%'
                            },{
                                field: 'procedureName',
                                title: '工序名',
                                width: '15%'
                            }, {
                                field: 'procedureDescription',
                                title: '工序描述',
                                width: '13%'
                            }, {
                                field: 'remark',
                                title: '备注',
                                width: '6%'
                            }, {
                                field: 'styleType',
                                title: '款式',
                                width: '6%'
                            }, {
                                field: 'partName',
                                title: '部位',
                                width: '7%'
                            }, {
                                field: 'procedureCode',
                                title: '代码',
                                width: '6%'
                            },  {
                                field: 'procedureSection',
                                title: '工段',
                                width: '6%'
                            }, {
                                field: 'segment',
                                title: '码段',
                                width: '5%'
                            }, {
                                field: 'equType',
                                title: '设备',
                                width: '5%'
                            }, {
                                field: 'procedureLevel',
                                title: '等级',
                                width: '5%'
                            }, {
                                field: 'sam',
                                title: 'SAM',
                                width: '5%'
                            }, {
                                field: 'packagePrice',
                                title: '价/打',
                                width: '5%'
                            }, {
                                field: 'piecePrice',
                                title: '价/件',
                                width: '5%'
                            }, {
                                field: 'initPrice',
                                title: '初始价',
                                width: '5%'
                            }]
                        ], //设置表头
                        params: [
                            {
                                name: 'procedureNumber',
                                field: 'procedureNumber'
                            }],//设置字段映射，适用于输入一个字段，回显多个字段
                        type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
                        url: "/erp/getproceduretemplatehintbyinfo?procedureNumber=" //设置异步数据接口,url为必填项,params为字段名
                    });

                    layui.yutons_sug.render({
                        id: "procedureName-" + item.LAY_TABLE_INDEX, //设置容器唯一id
                        height: "300",
                        width: "1500",
                        limit: "50",
                        limits: [10, 20, 50, 100],
                        cols: [
                            [{
                                field: 'procedureNumber',
                                title: '工序号',
                                width: '6%'
                            },{
                                field: 'procedureName',
                                title: '工序名',
                                width: '15%'
                            }, {
                                field: 'procedureDescription',
                                title: '工序描述',
                                width: '13%'
                            }, {
                                field: 'remark',
                                title: '备注',
                                width: '6%'
                            }, {
                                field: 'styleType',
                                title: '款式',
                                width: '6%'
                            }, {
                                field: 'partName',
                                title: '部位',
                                width: '7%'
                            }, {
                                field: 'procedureCode',
                                title: '代码',
                                width: '6%'
                            },  {
                                field: 'procedureSection',
                                title: '工段',
                                width: '6%'
                            }, {
                                field: 'segment',
                                title: '码段',
                                width: '5%'
                            }, {
                                field: 'equType',
                                title: '设备',
                                width: '5%'
                            }, {
                                field: 'procedureLevel',
                                title: '等级',
                                width: '5%'
                            }, {
                                field: 'sam',
                                title: 'SAM',
                                width: '5%'
                            }, {
                                field: 'packagePrice',
                                title: '价/打',
                                width: '5%'
                            }, {
                                field: 'piecePrice',
                                title: '价/件',
                                width: '5%'
                            }, {
                                field: 'initPrice',
                                title: '初始价',
                                width: '5%'
                            }]
                        ], //设置表头
                        params: [
                            {
                                name: 'procedureName',
                                field: 'procedureName'
                            }],//设置字段映射，适用于输入一个字段，回显多个字段
                        type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
                        url: "/erp/getproceduretemplatehintbyinfo?subProcedureName=" //设置异步数据接口,url为必填项,params为字段名
                    });

                    layui.yutons_sug.render({
                        id: "procedureDescription-" + item.LAY_TABLE_INDEX, //设置容器唯一id
                        height: "300",
                        width: "1500",
                        limit: "50",
                        limits: [10, 20, 50, 100],
                        cols: [
                            [{
                                field: 'procedureNumber',
                                title: '工序号',
                                width: '6%'
                            },{
                                field: 'procedureName',
                                title: '工序名',
                                width: '15%'
                            }, {
                                field: 'procedureDescription',
                                title: '工序描述',
                                width: '13%'
                            }, {
                                field: 'remark',
                                title: '备注',
                                width: '6%'
                            }, {
                                field: 'styleType',
                                title: '款式',
                                width: '6%'
                            }, {
                                field: 'partName',
                                title: '部位',
                                width: '7%'
                            }, {
                                field: 'procedureCode',
                                title: '代码',
                                width: '6%'
                            },  {
                                field: 'procedureSection',
                                title: '工段',
                                width: '6%'
                            }, {
                                field: 'segment',
                                title: '码段',
                                width: '5%'
                            }, {
                                field: 'equType',
                                title: '设备',
                                width: '5%'
                            }, {
                                field: 'procedureLevel',
                                title: '等级',
                                width: '5%'
                            }, {
                                field: 'sam',
                                title: 'SAM',
                                width: '5%'
                            }, {
                                field: 'packagePrice',
                                title: '价/打',
                                width: '5%'
                            }, {
                                field: 'piecePrice',
                                title: '价/件',
                                width: '5%'
                            }, {
                                field: 'initPrice',
                                title: '初始价',
                                width: '5%'
                            }]
                        ], //设置表头
                        params: [
                            {
                                name: 'procedureDescription',
                                field: 'procedureDescription'
                            }],//设置字段映射，适用于输入一个字段，回显多个字段
                        type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
                        url: "/erp/getproceduretemplatehintbyinfo?subProcedureDescription=" //设置异步数据接口,url为必填项,params为字段名
                    });

                    layui.yutons_sug.render({
                        id: "remark-" + item.LAY_TABLE_INDEX, //设置容器唯一id
                        height: "300",
                        width: "1500",
                        limit: "50",
                        limits: [10, 20, 50, 100],
                        cols: [
                            [{
                                field: 'procedureNumber',
                                title: '工序号',
                                width: '6%'
                            },{
                                field: 'procedureName',
                                title: '工序名',
                                width: '15%'
                            }, {
                                field: 'procedureDescription',
                                title: '工序描述',
                                width: '13%'
                            }, {
                                field: 'remark',
                                title: '备注',
                                width: '6%'
                            }, {
                                field: 'styleType',
                                title: '款式',
                                width: '6%'
                            }, {
                                field: 'partName',
                                title: '部位',
                                width: '7%'
                            }, {
                                field: 'procedureCode',
                                title: '代码',
                                width: '6%'
                            },  {
                                field: 'procedureSection',
                                title: '工段',
                                width: '6%'
                            }, {
                                field: 'segment',
                                title: '码段',
                                width: '5%'
                            }, {
                                field: 'equType',
                                title: '设备',
                                width: '5%'
                            }, {
                                field: 'procedureLevel',
                                title: '等级',
                                width: '5%'
                            }, {
                                field: 'sam',
                                title: 'SAM',
                                width: '5%'
                            }, {
                                field: 'packagePrice',
                                title: '价/打',
                                width: '5%'
                            }, {
                                field: 'piecePrice',
                                title: '价/件',
                                width: '5%'
                            }, {
                                field: 'initPrice',
                                title: '初始价',
                                width: '5%'
                            }]
                        ], //设置表头
                        params: [
                            {
                                name: 'remark',
                                field: 'remark'
                            }],//设置字段映射，适用于输入一个字段，回显多个字段
                        type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
                        url: "/erp/getproceduretemplatehintbyinfo?subRemark=" //设置异步数据接口,url为必填项,params为字段名
                    });

                    $("#procedureNumber-" + item.LAY_TABLE_INDEX).parent().css("overflow", "unset");
                    $("#procedureNumber-" + item.LAY_TABLE_INDEX).parent().css("height", "auto");
                    $("#procedureNumber-" + item.LAY_TABLE_INDEX).val(item.procedureNumber);
                    $("#procedureNumber-" + item.LAY_TABLE_INDEX).bind("input propertychange", function (event) {
                        data[item.LAY_TABLE_INDEX].procedureNumber = $("#procedureNumber-" + item.LAY_TABLE_INDEX).val();
                    });

                    $("#procedureName-" + item.LAY_TABLE_INDEX).parent().css("overflow", "unset");
                    $("#procedureName-" + item.LAY_TABLE_INDEX).parent().css("height", "auto");
                    $("#procedureName-" + item.LAY_TABLE_INDEX).val(item.procedureName);
                    $("#procedureName-" + item.LAY_TABLE_INDEX).bind("input propertychange", function (event) {
                        data[item.LAY_TABLE_INDEX].procedureName = $("#procedureName-" + item.LAY_TABLE_INDEX).val();
                    });

                    $("#procedureDescription-" + item.LAY_TABLE_INDEX).parent().css("overflow", "unset");
                    $("#procedureDescription-" + item.LAY_TABLE_INDEX).parent().css("height", "auto");
                    $("#procedureDescription-" + item.LAY_TABLE_INDEX).val(item.procedureDescription);
                    $("#procedureDescription-" + item.LAY_TABLE_INDEX).bind("input propertychange", function (event) {
                        data[item.LAY_TABLE_INDEX].procedureDescription = $("#procedureDescription-" + item.LAY_TABLE_INDEX).val();
                    });

                    $("#remark-" + item.LAY_TABLE_INDEX).parent().css("overflow", "unset");
                    $("#remark-" + item.LAY_TABLE_INDEX).parent().css("height", "auto");
                    $("#remark-" + item.LAY_TABLE_INDEX).val(item.remark);
                    $("#remark-" + item.LAY_TABLE_INDEX).bind("input propertychange", function (event) {
                        data[item.LAY_TABLE_INDEX].remark = $("#remark-" + item.LAY_TABLE_INDEX).val();
                    });

                    $("#colorName-" + item.LAY_TABLE_INDEX).parent().css("overflow", "unset");
                    $("#colorName-" + item.LAY_TABLE_INDEX).parent().css("height", "auto");

                    $("#sizeName-" + item.LAY_TABLE_INDEX).parent().css("overflow", "unset");
                    $("#sizeName-" + item.LAY_TABLE_INDEX).parent().css("height", "auto");
                });

                $.each(res.data,function (index, item) {
                    var cxm = xmSelect.render({
                        el: '#colorName-' + item.LAY_TABLE_INDEX,
                        initValue: colorArray[index],
                        data: colorData
                    });
                    item.colorName = cxm;
                    var sxm = xmSelect.render({
                        el: '#sizeName-' + item.LAY_TABLE_INDEX,
                        initValue: sizeArray[index],
                        data: sizeData
                    });
                    item.sizeName = sxm;
                })

                isModified = true;

                $("div[lay-id='procedureTable']").css("margin",0);
                for(var i=0;i<res.data.length;i++) {
                    if (res.data[i].procedureState == 1 || res.data[i].procedureState == 2) {
                        $("div[lay-id='procedureTable'] .layui-table-body").find("tr").eq(i).find('input[name="procedureNumber"]').attr("readOnly","true");
                        $("div[lay-id='procedureTable'] .layui-table-body").find("tr").eq(i).find('input[name="procedureName"]').attr("readOnly","true");
                        $("div[lay-id='procedureTable'] .layui-table-body").find("tr").eq(i).find('input[name="procedureDescription"]').attr("readOnly","true");
                        $("div[lay-id='procedureTable'] .layui-table-body").find("tr").eq(i).find('input[name="remark"]').attr("readOnly","true");
                        $("div[lay-id='procedureTable'] .layui-table-body").find("tr").eq(i).find('td[data-field="styleType"]').data('edit', false);
                        $("div[lay-id='procedureTable'] .layui-table-body").find("tr").eq(i).find('td[data-field="partName"]').data('edit', false);
                        $("div[lay-id='procedureTable'] .layui-table-body").find("tr").eq(i).find('td[data-field="procedureNumber"]').css('pointer-events','none');
                        $("div[lay-id='procedureTable'] .layui-table-body").find("tr").eq(i).find('td[data-field="procedureName"]').css('pointer-events','none');
                        $("div[lay-id='procedureTable'] .layui-table-body").find("tr").eq(i).find('td[data-field="procedureSection"]').data('edit', false);
                        $("div[lay-id='procedureTable'] .layui-table-body").find("tr").eq(i).find('td[data-field="procedureDescription"]').css('pointer-events','none');
                        $("div[lay-id='procedureTable'] .layui-table-body").find("tr").eq(i).find('td[data-field="remark"]').css('pointer-events','none');
                        $("div[lay-id='procedureTable'] .layui-table-body").find("tr").eq(i).find('td[data-field="segment"]').data('edit', false);
                        $("div[lay-id='procedureTable'] .layui-table-body").find("tr").eq(i).find('td[data-field="equType"]').data('edit', false);
                        $("div[lay-id='procedureTable'] .layui-table-body").find("tr").eq(i).find('td[data-field="procedureLevel"]').data('edit', false);
                        $("div[lay-id='procedureTable'] .layui-table-body").find("tr").eq(i).find('td[data-field="scanPart"]').data('edit', false);
                        $("div[lay-id='procedureTable'] .layui-table-body").find("tr").eq(i).find('td[data-field="sam"]').data('edit', false);
                        $("div[lay-id='procedureTable'] .layui-table-body").find("tr").eq(i).find('td[data-field="packagePrice"]').data('edit', false);
                        $("div[lay-id='procedureTable'] .layui-table-body").find("tr").eq(i).find('td[data-field="piecePrice"]').data('edit', false);
                        $("div[lay-id='procedureTable'] .layui-table-body").find("tr").eq(i).find('td[data-field="initPrice"]').data('edit', false);
                        $("div[lay-id='procedureTable'] .layui-table-body").find("tr").eq(i).find('td[data-field="piecePriceTwo"]').data('edit', false);
                        res.data[i].colorName.update({disabled: true });
                        res.data[i].sizeName.update({disabled: true });

                    }
                }

            }
        });

        table.on('toolbar(procedureTable)', function(obj) {
            if (obj.event === 'addRow') {
                addRow();
            }else if  (obj.event === 'submitProcedure') {
                submitProcedure();
            }else if  (obj.event === 'procedureNumberAsc') {
                procedureNumberAsc();
            }else if  (obj.event === 'procedureNumberDesc') {
                procedureNumberDesc();
            }else if  (obj.event === 'calPrice') {
                calPrice();
            }
        })

        table.on('tool(procedureTable)', function(obj){
            var data = table.cache.procedureTable;
            if(obj.event === 'delProcedure'){
                obj.del();
                $.each(data,function (index,item) {
                    if(item instanceof Array) {
                        data.splice(index,1);
                    }
                })
                table.reload("procedureTable",{
                    data:data   // 将新数据重新载入表格
                })
            }
        });
    });
}

function addRow() {
    if ($("#orderName").val() == "" || $("#orderName").val() == null){
        layer.msg('请先输入订单信息！');
        return false;
    }

    var data = layui.table.cache.procedureTable;
    data.unshift({
        "styleType": ""
        ,"partName": ""
        ,"procedureCode": ""
        ,"procedureNumber": ""
        ,"procedureName": ""
        ,"procedureSection": ""
        ,"procedureDescription": ""
        ,"remark": ""
        ,"segment": ""
        ,"equType": ""
        ,"procedureLevel": ""
        ,"sam": ""
        ,"packagePrice": ""
        ,"piecePrice": ""
        ,"initPrice": ""
        ,"piecePriceTwo": 0
        ,"scanPart": ""
    });
    layui.table.reload("procedureTable",{
        data:data   // 将新数据重新载入表格
    });
}

function procedureNumberAsc() {
    var data = layui.table.cache.procedureTable;
    data.sort(function(a, b){return  a.procedureNumber - b.procedureNumber});
    layui.table.reload("procedureTable",{
        data:data   // 将新数据重新载入表格
    });
}

function procedureNumberDesc() {
    var data = layui.table.cache.procedureTable;
    data.sort(function(a, b){return  b.procedureNumber - a.procedureNumber});
    layui.table.reload("procedureTable",{
        data:data   // 将新数据重新载入表格
    });
}

function calPrice() {
    var data = layui.table.cache.procedureTable;
    var subsidyPer = $("#subsidyPer").val();
    if (subsidyPer != null && subsidyPer != '' && subsidyPer >= 0){
        $.each(data,function (index,item) {
            if (item.procedureSection === '车缝'){
                item.piecePrice = toDecimalFour(item.initPrice * (1 + subsidyPer/100));
                item.packagePrice = toDecimalFour(item.piecePrice * 12);
            }
        })
    }
    layui.table.reload("procedureTable",{
        data:data   // 将新数据重新载入表格
    });
}

function toDecimal(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*100)/100;
    return f;
}

function toDecimalFour(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*10000)/10000;
    return f;
}