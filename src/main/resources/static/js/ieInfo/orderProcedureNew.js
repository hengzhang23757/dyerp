layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.22'
}).extend({                         // 模块别名
    soulTable: 'soulTable',
});
var basePath=$("#basePath").val();
var userRole=$("#userRole").val();
var userName=$("#userName").val();
var form;
var orderProcedureTable;

initOrderProcedureTable();

function initOrderProcedureTable(){
    layui.use(['form','yutons_sug', 'table'], function () {
        layui.laydate.render({
            elem: '#beginDate',
            trigger: 'click'
        });
        layui.use(['yutons_sug'], function () {
            layui.yutons_sug.render({
                id: "clothesVersionNumber", //设置容器唯一id
                height: "300",
                width: "550",
                limit:"10",
                limits:[10,20,50,100],
                cols: [
                    [{
                        field: 'clothesVersionNumber',
                        title: '版单号',
                        align: 'left'
                    }, {
                        field: 'orderName',
                        title: '订单号',
                        align: 'left'
                    }]
                ], //设置表头
                params: [
                    {
                        name: 'clothesVersionNumber',
                        field: 'clothesVersionNumber'
                    }, {
                        name: 'orderName',
                        field: 'orderName'
                    }],//设置字段映射，适用于输入一个字段，回显多个字段
                type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
                url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
            });

            layui.yutons_sug.render({
                id: "orderName", //设置容器唯一id
                height: "300",
                width: "550",
                limit:"10",
                limits:[10,20,50,100],
                cols: [
                    [{
                        field: 'clothesVersionNumber',
                        title: '单号',
                        align: 'left'
                    }, {
                        field: 'orderName',
                        title: '款号',
                        align: 'left'
                    }]
                ], //设置表头
                params: [
                    {
                        name: 'clothesVersionNumber',
                        field: 'clothesVersionNumber'
                    }, {
                        name: 'orderName',
                        field: 'orderName'
                    }],//设置字段映射，适用于输入一个字段，回显多个字段
                type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
                url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
            });
        });
        var visible = false;
        if (userRole == 'root' || userRole == 'role5' || userRole == 'role10'){
            visible = true;
        }

        var table = layui.table,
            form = layui.form,
            orderProcedureTable = table.render({
                elem: '#orderProcedureTable'
                , url: 'getuniqueorderprocedure'
                , page: true
                , toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                , cols: [[
                    {type:'numbers', align:'center', title:'序号', width:90}
                    ,{field:'orderName',minWidth:120, title: '款号'}
                    ,{field:'clothesVersionNumber',minWidth:120,  title: '单号'}
                    ,{field:'customerName',minWidth:100,  title: '客户名'}
                    ,{field:'styleDescription',minWidth:110,  title: '款式描述'}
                    ,{field:'lastUpdateTime',minWidth:120, title: '最后修改',templet: function (d) {
                            return layui.util.toDateString(d.lastUpdateTime, 'MM-dd HH:mm');
                        }}
                    ,{field:'lastReviewTime',minWidth:120, title: '最后审核',templet: function (d) {
                            return layui.util.toDateString(d.lastReviewTime, 'MM-dd HH:mm');
                        }}
                    ,{field:'reviewState', title: '操作' ,align:'center',fixed: 'right',minWidth:250,templet: function (d) {
                            if (d.reviewState==0 || d.reviewState==3 || d.reviewState==4){
                                return "<a href='#' style='color:#3e8eea' onclick=orderDetail('"+d.orderName+"')>订单详情</a>&nbsp;&nbsp;<a href='#' style='color:#3e8eea' onclick=deleteOrder('"+d.orderName+"')>删除</a>&nbsp;&nbsp;<a href='#' style='color:#3e8eea' onclick=addOrder('"+d.orderName+"')>新增</a>&nbsp;&nbsp;<a href='#' style='color:#3e8eea' onclick=updateOrder('"+d.orderName+"')>修改</a>&nbsp;&nbsp;<a href='#' style='color:#3e8eea' onclick=printOrder('"+d.orderName+"')>打印</a>";
                            }else if (d.reviewState==1 || d.reviewState==2 || d.reviewState==7){
                                return "<a href='#' style='color:#3e8eea' onclick=orderDetail('"+d.orderName+"')>订单详情</a>&nbsp;&nbsp;删除&nbsp;&nbsp;<a href='#' style='color:#3e8eea' onclick=addOrder('"+d.orderName+"')>新增</a>&nbsp;&nbsp;修改&nbsp;&nbsp;<a href='#' style='color:#3e8eea' onclick=printOrder('"+d.orderName+"')>打印</a>";
                            }else if(d.reviewState==5 || d.reviewState==6 || d.reviewState==8 || d.reviewState==9 || d.reviewState==10 || d.reviewState==11) {
                                return "<a href='#' style='color:#3e8eea' onclick=orderDetail('"+d.orderName+"')>订单详情</a>&nbsp;&nbsp;删除&nbsp;&nbsp;<a href='#' style='color:#3e8eea' onclick=addOrder('"+d.orderName+"')>新增</a>&nbsp;&nbsp;<a href='#' style='color:#3e8eea' onclick=updateOrder('"+d.orderName+"')>修改</a>&nbsp;&nbsp;<a href='#' style='color:#3e8eea' onclick=printOrder('"+d.orderName+"')>打印</a>";
                            }
                        }}
                    ,{field:'reviewState', title: '审核',align:'center',fixed: 'right',minWidth:150,templet: function (d) {
                            if (d.reviewState == 0){
                                if(userRole == 'role5'){
                                    return "<a href='#' style='color:#3e8eea' onclick=commitReview('"+d.orderName+"')>提交审核</a>";
                                }else{
                                    return "未提交";
                                }
                            }else if (d.reviewState == 1){
                                if (userRole == 'role10'){
                                    return "<a href='#' style='color:#ec6c62' onclick=reviewProcedure('"+d.orderName+"')>待审核</a>";
                                }else{
                                    return "已提交";
                                }
                            }else if (d.reviewState == 2){
                                if (userRole == 'role10'){
                                    return "<a href='#' style='color:#3e8eea' onclick=returnReview('"+d.orderName+"')>反审核</a>";
                                }else {
                                    return "审核通过";
                                }
                            }else if (d.reviewState == 3){
                                if(userRole == 'role5') {
                                    return "<a href='#' style='color:red'>未通过</a>&nbsp;&nbsp;<a href='#' style='color:#3e8eea' onclick=commitReview('" + d.orderName + "')>提交审核</a>";
                                }else {
                                    return "<p style='color: red; margin-bottom: 0;'>未通过</p>";
                                }
                            }else if(d.reviewState == 4){
                                if(userRole == 'role5') {
                                    return "<a href='#' style='color:red'>部分未通过</a>&nbsp;&nbsp;<a href='#' style='color:#3e8eea' onclick=commitReview('" + d.orderName + "')>提交审核</a>";
                                }else {
                                    return "未提交";
                                }
                            }else if(d.reviewState == 5){
                                if(userRole == 'role5') {
                                    return "<a href='#' style='color:black'>部分已提交</a>&nbsp;&nbsp;<a href='#' style='color:#3e8eea' onclick=commitReview('" + d.orderName + "')>提交审核</a>";
                                }else {
                                    return "<a href='#' style='color:#ec6c62' onclick=reviewProcedure('"+d.orderName+"')>待审核</a>";
                                }
                            }else if(d.reviewState == 6){
                                if(userRole == 'role5') {
                                    return "<a href='#' style='color:black'>部分已通过</a>&nbsp;&nbsp;<a href='#' style='color:#3e8eea' onclick=commitReview('" + d.orderName + "')>提交审核</a>";
                                }else {
                                    return "<a href='#' style='color:#3e8eea' onclick=returnReview('"+d.orderName+"')>反审核</a>";
                                }
                            }else if(d.reviewState == 7){
                                if(userRole == 'role5') {
                                    return "部分已提交，部分已通过";
                                }else {
                                    return "<a href='#' style='color:#ec6c62' onclick=reviewProcedure('"+d.orderName+"')>待审核</a>，<a href='#' style='color:#3e8eea' onclick=returnReview('"+d.orderName+"')>反审核</a>";
                                }
                            }else if(d.reviewState == 8){
                                if(userRole == 'role5') {
                                    return "<a href='#' style='color: red'>部分未通过</a>&nbsp;&nbsp;<a href='#' style='color: black'>部分已提交</a>&nbsp;&nbsp;<a href='#' style='color:#3e8eea' onclick=commitReview('" + d.orderName + "')>提交审核</a>";
                                }else {
                                    return "<a href='#' style='color:#ec6c62' onclick=reviewProcedure('"+d.orderName+"')>待审核</a>";
                                }
                            }else if(d.reviewState == 9){
                                if(userRole == 'role5') {
                                    return "<a href='#' style='color: black'>部分已通过</a>&nbsp;&nbsp;<a href='#' style='color: red'>部分未通过</a>&nbsp;&nbsp;<a href='#' style='color:#3e8eea' onclick=commitReview('" + d.orderName + "')>提交审核</a>";
                                }else {
                                    return "<a href='#' style='color:#3e8eea' onclick=returnReview('"+d.orderName+"')>反审核</a>";
                                }
                            }else if(d.reviewState == 10){
                                if(userRole == 'role5') {
                                    return "部分已提交,&nbsp;&nbsp;部分已通过,&nbsp;&nbsp;<a href='#' style='color:#3e8eea' onclick=commitReview('" + d.orderName + "')>提交审核</a>";
                                }else {
                                    return "<a href='#' style='color:#ec6c62' onclick=reviewProcedure('"+d.orderName+"')>待审核</a>，<a href='#' style='color:#3e8eea' onclick=returnReview('"+d.orderName+"')>反审核</a>";
                                }
                            }else if(d.reviewState == 11){
                                if(userRole == 'role5') {
                                    return "部分已提交&nbsp;&nbsp;部分已通过&nbsp;&nbsp;部分未通过<a href='#' style='color:#3e8eea' onclick=commitReview('" + d.orderName + "')>提交审核</a>";
                                }else {
                                    return "<a href='#' style='color:#ec6c62' onclick=reviewProcedure('"+d.orderName+"')>待审核</a>,&nbsp;&nbsp;<a href='#' style='color:#3e8eea' onclick=returnReview('"+d.orderName+"')>反审核</a>";
                                }
                            }
                        }}
                    ,{field:'beginDate',hide: true}
                ]]
                , limit:20
                , limits:[10,20,50,100,200,500]
                , hide:visible
                , height: 'full-170'
                , loading: true
                , done: function (res, curr, count) {}
            });

        form.on('submit(search)', function (data) {
            var orderName = $("#orderName").val();
            var clothesVersionNumber = $("#clothesVersionNumber").val();
            var customerName = $("#customerName").val();
            var styleDescription = $("#styleDescription").val();
            var beginDate = $("#beginDate").val();
            var procedureState = $("#procedureState").val();

            orderProcedureTable = table.render({
                elem: '#orderProcedureTable'
                , url: 'getuniqueorderprocedure'
                , where: {
                    orderName: orderName,
                    clothesVersionNumber: clothesVersionNumber,
                    customerName: customerName,
                    styleDescription: styleDescription,
                    beginDate: beginDate,
                    procedureState: procedureState
                }
                , page: true
                , toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                , cols: [[
                    {type:'numbers', align:'center', title:'序号', width:90}
                    ,{field:'orderName',minWidth:120, title: '款号'}
                    ,{field:'clothesVersionNumber',minWidth:120,  title: '单号'}
                    ,{field:'customerName',minWidth:100,  title: '客户名'}
                    ,{field:'styleDescription',minWidth:110,  title: '款式描述'}
                    ,{field:'lastUpdateTime',minWidth:120, title: '最后修改',templet: function (d) {
                            return layui.util.toDateString(d.lastUpdateTime, 'MM-dd HH:mm');
                        }}
                    ,{field:'lastReviewTime',minWidth:120, title: '最后审核',templet: function (d) {
                            return layui.util.toDateString(d.lastReviewTime, 'MM-dd HH:mm');
                        }}
                    ,{field:'reviewState', title: '操作' ,align:'center',fixed: 'right',minWidth:250,templet: function (d) {
                            if (d.reviewState==0 || d.reviewState==3 || d.reviewState==4){
                                return "<a href='#' style='color:#3e8eea' onclick=orderDetail('"+d.orderName+"')>订单详情</a>&nbsp;&nbsp;<a href='#' style='color:#3e8eea' onclick=deleteOrder('"+d.orderName+"')>删除</a>&nbsp;&nbsp;<a href='#' style='color:#3e8eea' onclick=addOrder('"+d.orderName+"')>新增</a>&nbsp;&nbsp;<a href='#' style='color:#3e8eea' onclick=updateOrder('"+d.orderName+"')>修改</a>&nbsp;&nbsp;<a href='#' style='color:#3e8eea' onclick=printOrder('"+d.orderName+"')>打印</a>";
                            }else if (d.reviewState==1 || d.reviewState==2 || d.reviewState==7){
                                return "<a href='#' style='color:#3e8eea' onclick=orderDetail('"+d.orderName+"')>订单详情</a>&nbsp;&nbsp;删除&nbsp;&nbsp;<a href='#' style='color:#3e8eea' onclick=addOrder('"+d.orderName+"')>新增</a>&nbsp;&nbsp;修改&nbsp;&nbsp;<a href='#' style='color:#3e8eea' onclick=printOrder('"+d.orderName+"')>打印</a>";
                            }else if(d.reviewState==5 || d.reviewState==6 || d.reviewState==8 || d.reviewState==9 || d.reviewState==10 || d.reviewState==11) {
                                return "<a href='#' style='color:#3e8eea' onclick=orderDetail('"+d.orderName+"')>订单详情</a>&nbsp;&nbsp;删除&nbsp;&nbsp;<a href='#' style='color:#3e8eea' onclick=addOrder('"+d.orderName+"')>新增</a>&nbsp;&nbsp;<a href='#' style='color:#3e8eea' onclick=updateOrder('"+d.orderName+"')>修改</a>&nbsp;&nbsp;<a href='#' style='color:#3e8eea' onclick=printOrder('"+d.orderName+"')>打印</a>";
                            }
                        }}
                    ,{field:'reviewState', title: '审核',align:'center',fixed: 'right',minWidth:150,templet: function (d) {
                            if (d.reviewState == 0){
                                if(userRole == 'role5'){
                                    return "<a href='#' style='color:#3e8eea' onclick=commitReview('"+d.orderName+"')>提交审核</a>";
                                }else{
                                    return "未提交";
                                }
                            }else if (d.reviewState == 1){
                                if (userRole == 'role10'){
                                    return "<a href='#' style='color:#ec6c62' onclick=reviewProcedure('"+d.orderName+"')>待审核</a>";
                                }else{
                                    return "已提交";
                                }
                            }else if (d.reviewState == 2){
                                if (userRole == 'role10'){
                                    return "<a href='#' style='color:#3e8eea' onclick=returnReview('"+d.orderName+"')>反审核</a>";
                                }else {
                                    return "审核通过";
                                }
                            }else if (d.reviewState == 3){
                                if(userRole == 'role5') {
                                    return "<a href='#' style='color:red'>未通过</a>&nbsp;&nbsp;<a href='#' style='color:#3e8eea' onclick=commitReview('" + d.orderName + "')>提交审核</a>";
                                }else {
                                    return "<p style='color: red; margin-bottom: 0;'>未通过</p>";
                                }
                            }else if(d.reviewState == 4){
                                if(userRole == 'role5') {
                                    return "<a href='#' style='color:red'>部分未通过</a>&nbsp;&nbsp;<a href='#' style='color:#3e8eea' onclick=commitReview('" + d.orderName + "')>提交审核</a>";
                                }else {
                                    return "未提交";
                                }
                            }else if(d.reviewState == 5){
                                if(userRole == 'role5') {
                                    return "<a href='#' style='color:black'>部分已提交</a>&nbsp;&nbsp;<a href='#' style='color:#3e8eea' onclick=commitReview('" + d.orderName + "')>提交审核</a>";
                                }else {
                                    return "<a href='#' style='color:#ec6c62' onclick=reviewProcedure('"+d.orderName+"')>待审核</a>";
                                }
                            }else if(d.reviewState == 6){
                                if(userRole == 'role5') {
                                    return "<a href='#' style='color:black'>部分已通过</a>&nbsp;&nbsp;<a href='#' style='color:#3e8eea' onclick=commitReview('" + d.orderName + "')>提交审核</a>";
                                }else {
                                    return "<a href='#' style='color:#3e8eea' onclick=returnReview('"+d.orderName+"')>反审核</a>";
                                }
                            }else if(d.reviewState == 7){
                                if(userRole == 'role5') {
                                    return "部分已提交，部分已通过";
                                }else {
                                    return "<a href='#' style='color:#ec6c62' onclick=reviewProcedure('"+d.orderName+"')>待审核</a>，<a href='#' style='color:#3e8eea' onclick=returnReview('"+d.orderName+"')>反审核</a>";
                                }
                            }else if(d.reviewState == 8){
                                if(userRole == 'role5') {
                                    return "<a href='#' style='color: red'>部分未通过</a>&nbsp;&nbsp;<a href='#' style='color: black'>部分已提交</a>&nbsp;&nbsp;<a href='#' style='color:#3e8eea' onclick=commitReview('" + d.orderName + "')>提交审核</a>";
                                }else {
                                    return "<a href='#' style='color:#ec6c62' onclick=reviewProcedure('"+d.orderName+"')>待审核</a>";
                                }
                            }else if(d.reviewState == 9){
                                if(userRole == 'role5') {
                                    return "<a href='#' style='color: black'>部分已通过</a>&nbsp;&nbsp;<a href='#' style='color: red'>部分未通过</a>&nbsp;&nbsp;<a href='#' style='color:#3e8eea' onclick=commitReview('" + d.orderName + "')>提交审核</a>";
                                }else {
                                    return "<a href='#' style='color:#3e8eea' onclick=returnReview('"+d.orderName+"')>反审核</a>";
                                }
                            }else if(d.reviewState == 10){
                                if(userRole == 'role5') {
                                    return "部分已提交,&nbsp;&nbsp;部分已通过,&nbsp;&nbsp;<a href='#' style='color:#3e8eea' onclick=commitReview('" + d.orderName + "')>提交审核</a>";
                                }else {
                                    return "<a href='#' style='color:#ec6c62' onclick=reviewProcedure('"+d.orderName+"')>待审核</a>，<a href='#' style='color:#3e8eea' onclick=returnReview('"+d.orderName+"')>反审核</a>";
                                }
                            }else if(d.reviewState == 11){
                                if(userRole == 'role5') {
                                    return "部分已提交&nbsp;&nbsp;部分已通过&nbsp;&nbsp;部分未通过<a href='#' style='color:#3e8eea' onclick=commitReview('" + d.orderName + "')>提交审核</a>";
                                }else {
                                    return "<a href='#' style='color:#ec6c62' onclick=reviewProcedure('"+d.orderName+"')>待审核</a>,&nbsp;&nbsp;<a href='#' style='color:#3e8eea' onclick=returnReview('"+d.orderName+"')>反审核</a>";
                                }
                            }
                        }}
                    ,{field:'beginDate',hide: true}
                ]]
                , limit:20
                , limits:[10,20,50,100,200,500]
                , hide:visible
                , height: 'full-170'
                , loading: true
                , done: function (res, curr, count) {}
            });
            // table.reload('orderProcedureTable',{
            //     url: 'getuniqueorderprocedure'
            //
            // })
        });

        table.on('toolbar(orderProcedureTable)', function(obj) {
            if (obj.event === 'addOrderProcedure') {
                addOrderProcedure();
            }
        })
    });
}


$(document).ready(function () {
    $('#mainFrameTabs').bTabs();
});

function deleteOrder(orderName) {
    if(userRole!='root' && userRole!='role5'){
        swal("SORRY!", "对不起，您没有操作权限！", "warning");
        return;
    }
    swal({
        title: "",
        text: "<span style=\"font-weight:bolder;font-size: 20px\">您确定要删除吗？</span>",
        type: "warning",
        html:true,
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: "确定",
        cancelButtonText:"我再想想",
        confirmButtonColor: "#ec6c62",
        showLoaderOnConfirm: true
    }, function() {
        $.ajax({
            url: basePath + "erp/deleteorderprocedurebyname",
            type:'POST',
            data: {
                orderName:orderName,
                userName:userName,
            },
            success: function (data) {
                if(data == 0) {
                    swal({
                            type:"success",
                            title:"",
                            text: "<span style=\"font-weight:bolder;font-size: 20px\">恭喜你，删除成功！</span>",
                            html: true,
                        },
                        function(){
                            location.href="/erp/orderProcedureNewStart";
                        });
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，删除失败！</span>",html: true});
                }
            },
            error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        })
    });
}

var tabId = 1;
function addOrderProcedure() {
    if(userRole!='root' && userRole!='role5'){
        swal("SORRY!", "对不起，您没有操作权限！", "warning");
        return;
    }
    $('#mainFrameTabs').bTabsAdd("tabId" + tabId, "工序录入", "/erp/addOrderProcedureNewStart?type=add");
    tabId++;
    // calcHeight();
}

function addOrder(orderName) {
    if(userRole!='root' && userRole!='role5'){
        swal("SORRY!", "对不起，您没有操作权限！", "warning");
        return;
    }
    var urlName = encodeURIComponent(orderName);
    var tabName = orderName.replace(/[&\|\\\*^%$#@\-+~`！!……——=「」{};:：；‘’\"\"/<>.,，。（）()《》]/g,"");
    $('#mainFrameTabs').bTabsAdd("tabIdAdd" + tabName, "新增工序", "/erp/addOrderProcedureNewStart?type=reAdd&orderName="+urlName);
}

function updateOrder(orderName) {
    if(userRole!='root' && userRole!='role5'){
        swal("SORRY!", "对不起，您没有操作权限！", "warning");
        return;
    }
    var urlName = encodeURIComponent(orderName);
    var tabName = orderName.replace(/[&\|\\\*^%$#@\-+~`！!……——=「」{};:：；‘’\"\"/<>.,，。（）()《》]/g,"");
    $('#mainFrameTabs').bTabsAdd("tabIdUpate" + tabName, "修改工序", "/erp/addOrderProcedureNewStart?type=update&orderName="+urlName);
    tabId++;
}

function orderDetail(orderName){
    var urlName = encodeURIComponent(orderName);
    var tabName = orderName.replace(/[&\|\\\*^%$#@\-+~`！!……——=「」{};:：；‘’\"\"/<>.,，。（）()《》]/g,"");
    $('#mainFrameTabs').bTabsAdd("tabId" + tabName, "订单详情", "/erp/orderProcedureDetailNewStart?orderName="+urlName);
}

function printOrder(orderName){
    layui.use(['form', 'soulTable', 'table'], function () {
        var table = layui.table,
            soulTable = layui.soulTable,
            $ = layui.$;
        var tableHtml = "";
        var index = layer.open({
            type: 1 //Page层类型
            , title: '订单工序表'
            , btn: ['下载/打印']
            , shade: 0.6 //遮罩透明度
            , maxmin: false //允许全屏最小化
            , anim: 0 //0-6的动画形式，-1不开启
            , content: "<div><div class='row'>" +
                "        <div id='orderProcedurePrintDiv'>" +
                "        </div>" +
                "    </div>" +
                "    <iframe id='printf' src='' width='0' height='0' frameborder='0'></iframe></div>"
            , yes: function () {
                printDeal();
            }
            , cancel : function (i,layero) {
                $("#orderProcedurePrintDiv").empty();
            }
        });
        layer.full(index);
        setTimeout(function () {
            $.ajax({
                url: "/erp/getorderproceudrebyorder",
                type: 'POST',
                data: {
                    orderName: orderName
                },
                success: function (res) {
                    if(res.orderProcedureList) {
                        var record = res.orderProcedureList[0];
                        var subSide = "0%";
                        $.each(res.orderProcedureList,function (index,item) {
                            if (item.subsidy > 0){
                                subSide = ((100 * item.subsidy).toFixed(2)).toString() + "%";
                            }
                        });
                        var samSum = 0;
                        var packageSum = 0;
                        var pieceSum = 0;
                        var driftSum = 0;
                        tableHtml = "<div class=\"col-md-12\" name='printTable'>\n" +
                            "               <section class=\"panel panel-default\" style='page-break-after:always;'>\n" +
                            "                   <header class=\"panel-heading font-bold\" style=\"text-align: center\">\n" +
                            "                       <div style=\"font-size: 20px;font-weight: 700\">中山德悦服饰订单工序表</div><br>\n" +
                            "                       <div style=\"font-size: 16px;font-weight: 700\">客户:" + record.customerName + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;单号:" + record.clothesVersionNumber + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;款号:" + record.orderName + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;款式:" + record.styleDescription + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;开单日期:" + record.beginDate + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;订单数量:" + record.orderCount + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;补贴比例:" + subSide +"</div><br>\n" +
                            "                   </header>\n" +
                            "                   <table class=\"table table-bordered\" style='width: 100%'>\n" +
                            "                       <tbody align='center'>";
                        tableHtml += "<tr><td>工序号</td><td>工序名</td><td>工序描述</td><td>备注</td><td>码段</td><td>SAM值</td><td>工价/打</td><td>工价/件</td><td>浮动</td></tr>\n";
                        $.each(res.orderProcedureList,function (index,item) {
                            samSum += item.sam;
                            packageSum += item.packagePrice;
                            pieceSum += item.piecePrice;
                            driftSum += item.piecePriceTwo;
                            tableHtml += "<tr><td>" + item.procedureNumber + "</td><td>" + item.procedureName + "</td><td>" + item.procedureDescription + "</td><td>" + item.remark + "</td><td>" + item.segment + "</td><td>" + item.sam + "</td><td>" + item.packagePrice + "</td><td>" + item.piecePrice + "</td><td>" + item.piecePriceTwo + "</td></tr>\n";
                        });
                        tableHtml += "<tr><td colspan='5'>合计</td><td>" + toDecimal(samSum) + "</td><td>" + toDecimal(packageSum) + "</td><td>" + toDecimal(pieceSum) + "</td><td>" + toDecimal(driftSum) + "</td></tr>\n";

                        tableHtml += "</tbody></table></section></div>";
                        $("#orderProcedurePrintDiv").append(tableHtml);
                    } else {
                        layer.msg("未获取到数据！");
                        layer.close(index);
                    }
                },
                error: function () {
                    layer.msg("获取详情信息失败！", {icon: 2});
                    layer.close(index);
                }
            })
        },500)
    })

}


function reviewProcedure(orderName) {
    layui.use(['form', 'soulTable', 'table'], function () {
        var table = layui.table,
            soulTable = layui.soulTable,
            $ = layui.$;
        var tableHtml = "";
        var orderInfoHtml = "";
        var index = layer.open({
            type: 1 //Page层类型
            , title: '待审核工序'
            , shade: 0.6 //遮罩透明度
            , maxmin: false //允许全屏最小化
            , btn: ['通过', '不通过']
            , anim: 0 //0-6的动画形式，-1不开启
            , content: "<div>" +
                "    <div class='row' id='orderInfo'>" +
                "    </div>" +
                "        <table class='layui-hide' id='orderProcedureDetailTable' lay-filter='orderProcedureDetailTable'></table>" +
                "    </div>"
            , yes: function () {
                layer.confirm('确定通过？', {
                    btn: ['确定'] //按钮
                }, function(){
                    $.ajax({
                        url: "/erp/orderProcedureAudit",
                        type:'POST',
                        data: {
                            orderName: orderName,
                            procedureState:2
                        },
                        success: function (data) {
                            if(data == 0) {
                                layer.msg('提交成功');
                                layer.close(index);
                                initOrderProcedureTable();
                            }else {
                                layer.msg('提交失败');
                            }
                        },
                        error: function () {
                            layer.msg('提交失败');
                        }
                    })


                });
            }
            , btn2: function () {
                layer.confirm('确定不通过？', {
                    btn: ['确定'] //按钮
                }, function(){
                    $.ajax({
                        url: "/erp/orderProcedureAudit",
                        type:'POST',
                        data: {
                            orderName: orderName,
                            procedureState:3
                        },
                        success: function (data) {
                            if(data == 0) {
                                layer.msg('提交成功');
                                layer.close(index);
                                initOrderProcedureTable();
                            }else {
                                layer.msg('提交失败');
                            }
                        },
                        error: function () {
                            layer.msg('提交失败');
                        }
                    })


                });
            }
            , cancel : function (i,layero) {
                $("#orderInfo").empty();
            }
        });
        layer.full(index);
        setTimeout(function () {
            $.ajax({
                url: "/erp/getorderprocedurebystate",
                type: 'POST',
                data: {
                    orderName: orderName,
                    procedureStateList: [1]
                },
                traditional: true,
                success: function (res) {
                    if(res.orderProcedureList) {
                        var record = res.orderProcedureList[0];
                        var subSide = "0%";
                        $.each(res.orderProcedureList,function (index,item) {
                            if (item.subsidy > 0){
                                subSide = ((100 * item.subsidy).toFixed(2)).toString() + "%";
                            }
                        });
                        orderInfoHtml += "<div class='col-md-12'><table><tr><td style='width: 12%; font-size: x-large; font-weight: bolder; color: #1E9FFF; padding-left: 20px; padding-bottom: 15px' align='right'>款号:</td><td style='width: 12%; font-size: x-large; font-weight: bolder; padding-left: 20px; padding-bottom: 15px'>" + record.orderName + "</td><td style='width: 12%; font-size: x-large; font-weight: bolder; color: #1E9FFF; padding-left: 20px; padding-bottom: 15px' align='right'>单号:</td><td style='width: 12%; font-size: x-large; font-weight: bolder; padding-left: 20px; padding-bottom: 15px'>" + record.clothesVersionNumber + "</td><td style='width: 12%; font-size: x-large; font-weight: bolder; color: #1E9FFF; padding-left: 20px; padding-bottom: 15px' align='right'>款式描述:</td><td style='width: 12%; font-size: x-large; font-weight: bolder; padding-left: 20px; padding-bottom: 15px'>" + record.styleDescription + "</td><td style='width: 12%; font-size: x-large; font-weight: bolder; color: #1E9FFF; padding-left: 20px; padding-bottom: 15px' align='right'>客户:</td><td style='width: 12%; font-size: x-large; font-weight: bolder; padding-left: 20px; padding-bottom: 15px'>"+ record.customerName +"</td></tr>";
                        orderInfoHtml += "<tr><td style='width: 12%; font-size: x-large; font-weight: bolder; color: #1E9FFF; padding-left: 20px; padding-bottom: 15px' align='right'>生产数量:</td><td style='width: 12%; font-size: x-large; font-weight: bolder; padding-left: 20px; padding-bottom: 15px'>" + record.orderCount + "</td><td style='width: 12%; font-size: x-large; font-weight: bolder; color: #1E9FFF; padding-left: 20px; padding-bottom: 15px' align='right'>开单日期:</td><td style='width: 12%; font-size: x-large; font-weight: bolder; padding-left: 20px; padding-bottom: 15px'>" + record.beginDate + "</td><td style='width: 12%; font-size: x-large; font-weight: bolder; color: #1E9FFF; padding-left: 20px; padding-bottom: 15px' align='right'>补贴比例:</td><td style='width: 12%; font-size: x-large; font-weight: bolder; padding-left: 20px; padding-bottom: 15px'>" + subSide + "</td><td></td><td></td></tr></table>";
                        $("#orderInfo").append(orderInfoHtml);
                        var load = layer.load();
                        table.render({
                            elem: '#orderProcedureDetailTable'
                            ,cols: [[
                                {field: 'styleType', title: '款式',align:'center', minWidth: 90, sort: true, filter: true},
                                {field: 'procedureCode', title: '工序代码',align:'center', minWidth: 80, sort: true, filter: true},
                                {field: 'procedureNumber', title: '工序号',align:'center', minWidth: 80, sort: true, filter: true},
                                {field: 'procedureName', title: '工序名称',align:'center', minWidth: 120, sort: true, filter: true},
                                {field: 'procedureDescription', title: '工序描述',align:'center', minWidth: 120, sort: true, filter: true},
                                {field: 'remark', title: '备注',align:'center', minWidth: 80, sort: true, filter: true},
                                {field: 'segment', title: '码段',align:'center', minWidth: 80, sort: true, filter: true},
                                {field: 'procedureLevel', title: '等级',align:'center', minWidth: 60, sort: true, filter: true},
                                {field: 'procedureSection', title: '工段',align:'center', minWidth: 60, totalRowText: '合计', sort: true, filter: true},
                                {field: 'sam', title: 'SAM值',align:'center', minWidth: 80, totalRow: true, sort: true, filter: true},
                                {field: 'packagePrice', title: '工价/打',align:'center', minWidth: 80, totalRow: true, sort: true, filter: true},
                                {field: 'piecePrice', title: '工价/件',align:'center', minWidth: 80, totalRow: true, sort: true, filter: true},
                                {field: 'piecePriceTwo', title: '浮动',align:'center', minWidth: 80, sort: true, filter: true},
                                {field: 'procedureState', title: '审核状态',align:'center', minWidth: 90, sort: true, filter: true, templet:function (d) {
                                        if (d.procedureState === 0){
                                            return "未提交";
                                        } else if (d.procedureState === 1){
                                            return "审核中";
                                        } else if (d.procedureState === 2){
                                            return "审核通过";
                                        } else if (d.procedureState === 3){
                                            return "未通过";
                                        }
                                    }}
                            ]]
                            ,loading:false
                            ,data:res.orderProcedureList
                            ,height: 'full-220'
                            ,title: '订单工序详情'
                            ,totalRow: true
                            ,even: true
                            ,page: true
                            ,overflow: 'tips'
                            ,limit: 100
                            ,limits: [100, 200, 500]
                            ,done: function (res, curr, count) {
                                soulTable.render(this);
                                layer.close(load);
                            }
                        });
                    } else {
                        layer.msg("未获取到数据！");
                        layer.close(index);
                    }
                },
                error: function () {
                    layer.msg("获取详情信息失败！", {icon: 2});
                    layer.close(index);
                }
            })
        },500)
    })
}


function commitReview(orderName) {
    $.ajax({
        url:"getorderprocedurebystate",
        data: {
            orderName:orderName,
            procedureStateList:[0,1,2,3],
        },
        type:"POST",
        traditional: true,
        success: function (data) {
            createOrderProcedureAuditTable(data.orderProcedureList,1);
            var index = layer.open({
                type: 1 //Page层类型
                , title: '待审核工序列表'
                , btn: ['确定']
                ,area: '1200px'
                , shade: 0.6 //遮罩透明度
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , content: $('#proceduteAuditDiv')
                ,yes: function(index, layero){
                    var selectList = [];
                    $("#orderProcedureAuditTable tbody input[name='checkBtn']").each(function(){
                        if($(this).is(':checked')) {
                            selectList.push($(this).val());
                        }
                    });
                    if(selectList.length == 0) {
                        swal("SORRY!", "请先选择要审核的数据！", "error");
                        return;
                    }
                    swal({
                        title: "",
                        text: "<span style=\"font-weight:bolder;font-size: 20px\">确定提交审核吗？</span>",
                        type: "warning",
                        html:true,
                        showCancelButton: true,
                        closeOnConfirm: false,
                        confirmButtonText: "确定",
                        cancelButtonText:"取消",
                        confirmButtonColor: "#ec6c62",
                        showLoaderOnConfirm: true
                    }, function() {
                        $.ajax({
                            url: "changeprocedurestatebatch",
                            type:'POST',
                            data: {
                                orderName:orderName,
                                procedureState:1,
                                procedureNumberList:selectList
                            },
                            traditional: true,
                            success: function (data) {
                                if(data == 0) {
                                    swal({
                                            type:"success",
                                            title:"",
                                            text: "<span style=\"font-weight:bolder;color:#F00;font-size: 50px\">提交成功！</span>",
                                            html: true
                                        },
                                        function(){
                                            location.href=basePath+"erp/orderProcedureNewStart";
                                        });

                                }else {
                                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，提交失败！</span>",html: true});                                        }
                            },
                            error: function () {
                                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
                            }
                        })
                    })
                }
                ,cancel: function(index, layero){
                    layer.closeAll();
                }
            })
        },
        error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">获取待审核列表失败～</span>",html: true});
        }
    })
}

var orderProcedureAuditTable;
function createOrderProcedureAuditTable(data, type) {
    if (orderProcedureAuditTable != undefined) {
        orderProcedureAuditTable.clear(); //清空一下table
        orderProcedureAuditTable.destroy(); //还原初始化了的datatable
    }
    orderProcedureAuditTable = $('#orderProcedureAuditTable').DataTable({
        "data":data,
        "retrieve": true,
        language : {
            search:"搜索：",
            zeroRecords : "没有内容",//table tbody内容为空时，tbody的内容。
        },
        "paging": false,
        "info": false,
        searching:true,
        lengthChange:false,
        ordering:false,
        "columns": [
            {
                "data": "procedureNumber",
                "title":"<input type='checkbox'>",
                "width":"5%",
                "defaultContent": "",
                "sClass": "text-center",
            },{
                "data": "procedureNumber",
                "title":"工序号",
                "width":"10%",
                "defaultContent": "",
                "sClass": "text-center",
                "ordering":true
            },{
                "data": "procedureName",
                "title":"工序名",
                "width":"10%",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "procedureDescription",
                "title":"工序描述",
                "width":"20%",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "procedureSection",
                "title":"工段",
                "width":"10%",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "sam",
                "title":"SAM值",
                "width":"10%",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "packagePrice",
                "title":"工价/打",
                "width":"10%",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "piecePrice",
                "title":"工价/件",
                "width":"10%",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "piecePriceTwo",
                "title":"浮动",
                "width":"8%",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "procedureState",
                "title":"审核状态",
                "width":"15%",
                "defaultContent": "",
                "sClass": "text-center",
            }
        ],
        "columnDefs" :
            [{
                "orderable" : false, // 禁用排序
                "targets" : [0], // 指定的列
                "data" : "procedureNumber",
                "render" : function(data, type, full, meta) {
                    if ((full.procedureState == 1 || full.procedureState == 2) && type == 1){
                        return "";
                    }else{
                        return "<input name='checkBtn' type='checkbox' value='"+data+"'>";
                    }

                }
            }, {
                "orderable" : false, // 禁用排序
                "targets" : [9], // 指定的列
                "data" : "procedureState",
                "render" : function(data, type, full, meta) {
                    if(data == 0) {
                        return "未提交"
                    }else if(data == 3){
                        return "未通过"
                    }else if(data == 2) {
                        return "审核通过"
                    }else if(data == 1) {
                        return "提交审核"
                    }
                }
            }
            ]
    });
}

function returnReview(orderName) {
    $.ajax({
        url:"getorderprocedurebystate",
        data: {
            orderName:orderName,
            procedureStateList:[2],
        },
        type:"POST",
        traditional: true,
        success: function (data) {
            createOrderProcedureAuditTable(data.orderProcedureList,2);
            var index = layer.open({
                type: 1 //Page层类型
                , title: '反审核工序列表'
                , btn: ['确定']
                ,area: '900px'
                , shade: 0.6 //遮罩透明度
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , content: $('#proceduteAuditDiv')
                ,yes: function(index, layero){
                    var selectList = [];
                    $("#orderProcedureAuditTable tbody input[name='checkBtn']").each(function(){
                        if($(this).is(':checked')) {
                            selectList.push($(this).val());
                        }
                    });
                    if(selectList.length == 0) {
                        swal("SORRY!", "请先选择要反审核的数据！", "error");
                        return;
                    }
                    swal({
                        title: "",
                        text: "<span style=\"font-weight:bolder;font-size: 20px\">确定提交反审核吗？</span>",
                        type: "warning",
                        html:true,
                        showCancelButton: true,
                        closeOnConfirm: false,
                        confirmButtonText: "确定",
                        cancelButtonText:"取消",
                        confirmButtonColor: "#ec6c62",
                        showLoaderOnConfirm: true
                    }, function() {
                        $.ajax({
                            url: "changeprocedurestatebatch",
                            type:'POST',
                            data: {
                                orderName:orderName,
                                procedureState:0,
                                procedureNumberList:selectList
                            },
                            traditional: true,
                            success: function (data) {
                                if(data == 0) {
                                    swal({
                                            type:"success",
                                            title:"",
                                            text: "<span style=\"font-weight:bolder;color:#F00;font-size: 50px\">提交成功！</span>",
                                            html: true
                                        },
                                        function(){
                                            location.href=basePath+"erp/orderProcedureNewStart";
                                        });

                                }else {
                                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，提交失败！</span>",html: true});                                        }
                            },
                            error: function () {
                                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
                            }
                        })
                    })
                }
                ,cancel: function(index, layero){
                    layer.closeAll();
                }
            })
        },
        error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">获取待审核列表失败～</span>",html: true});
        }
    })
}

function compare(property) {
    return function (a, b) {
        var value1 = a[property];
        var value2 = b[property];
        return value2 - value1;
    }
}

function printDeal(){
    $("#printf").empty();
    var printBoxs = document.getElementsByName('printTable');
    var newContent = '';
    for(var i=0;i<printBoxs.length;i++) {
        newContent += printBoxs[i].innerHTML;
    }
    var f = document.getElementById('printf');
    // f.contentDocument.write('<link rel="stylesheet" type="text/css" href="/css/app.v2.css">');
    f.contentDocument.write('<style>' +
        'table {' +
        'border-collapse:collapse;' +
        '}' +
        'td {\n' +
        '        border: 1px solid #000000;\n' +
        '    }'+
        '</style>');
    f.contentDocument.write(newContent);
    f.contentDocument.close();
    window.frames['printf'].focus();
    try{
        window.frames['printf'].print();
    }catch(err){
        f.contentWindow.print();
    }
}

function toDecimal(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*100)/100;
    return f;
}

function toDecimalFour(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*10000)/10000;
    return f;
}