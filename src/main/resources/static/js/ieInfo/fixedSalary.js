var basePath=$("#basePath").val();
$(document).ready(function () {
    $.ajax({
        url: basePath + "erp/getallfixedsalary",
        type:'GET',
        data: {},
        success: function (data) {
            if(data) {
                createFixedSalaryTable(data.fixedSalaryList);
            }else {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，没有查到相关信息！</span>",html: true});
            }
        },
        error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }
    });

    layui.laydate.render({
        elem: '#fixedValue',
        type: 'month',
        trigger: 'click'
    });
});

var fixedSalaryTable;
function createFixedSalaryTable(data) {
    if (fixedSalaryTable != undefined) {
        fixedSalaryTable.clear(); //清空一下table
        fixedSalaryTable.destroy(); //还原初始化了的datatable
    }
    fixedSalaryTable = $('#fixedSalaryTable').DataTable({
        "data":data,
        language : {
            processing : "载入中",//处理页面数据的时候的显示
            paginate : {//分页的样式文本内容。
                previous : "上一页",
                next : "下一页",
                first : "第一页",
                last : "最后一页"
            },
            search:"搜索：",
            zeroRecords : "没有内容",//table tbody内容为空时，tbody的内容。
            //下面三者构成了总体的左下角的内容。
            info : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//左下角的信息显示，大写的词为关键字。
            infoEmpty : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//筛选为空时左下角的显示。
            infoFiltered : ""//筛选之后的左下角筛选提示(另一个是分页信息显示，在上面的info中已经设置，所以可以不显示)，
        },
        pageLength : 25,// 每页显示10条数据
        pagingType : "full_numbers", // 分页样式：simple,simple_numbers,full,full_numbers，
        "paging": true,
        // "ordering" : false,
        "info": true,
        searching:true,
        lengthChange:false,
        dom: 'Bfrtip',
        "buttons": [{
            'extend': 'excel',
            'text': '导出',//定义导出excel按钮的文字
            'className': 'btn btn-success', //按钮的class样式
            'title': "工资锁定信息",
            'exportOptions': {
                'modifier': {
                    'page': 'all'
                }
            }
        }],
        "columns": [
            {
                "data": null,
                "title":"序号",
                "width":"30%",
                "defaultContent": "",
                "sClass": "text-center",
                "searchable":false,
                render: function (data, type, row, meta) {
                    var no = meta.settings._iDisplayStart + meta.row + 1;
                    return no;
                }
            }, {
                "data": "fixedValue",
                "title":"月份",
                "width":"50%",
                "defaultContent": "",
                "sClass": "text-center"
            }, {
                "data": "fixedID",
                "title": "操作",
                "width":"20%",
                "defaultContent": "",
                "sClass": "text-center"
            }
        ],
        "columnDefs" :
            [{
                "orderable" : false, // 禁用排序
                "targets" : [2], // 指定的列
                "data" : "fixedID",
                "render" : function(data, type, full, meta) {
                    return "<a href='#' style='color:#ff0000' onclick='deleteFixedSalary("+data+")'>删除</a>&nbsp;&nbsp;&nbsp;<a href='#' style='color:#3e8eea' onclick='reLock(this)'>重新锁定</a>";
                }
            }]

    });
    $('#fixedSalaryTable_wrapper .dt-buttons').append("<button class=\"btn btn-success\" style=\"outline:none;margin-left: 10px\" onclick=\"addFixedSalary()\">添加</button>");
}



function addFixedSalary() {
    $.blockUI({
        css: {
            width: '30%',
            top: '20%',
            left: '30%',
            border: 'none',
            padding: '15px',
            backgroundColor: '#fff',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: 1,
            color: '#000'
        },
        message: $('#editPro')
    });
    var url = basePath + "erp/addfixedsalary";
    $("#editYes").unbind("click").bind("click", function () {
        var flag = false;
        $("#editPro input,select").each(function (index, item) {
            if($(this).val() == "") {
                flag = true;
                return false;
            }
        });
        if(flag) {
            swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入完全部信息！</span>",html: true});
            return false;
        }
        $.blockUI({ css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            },
            message: "<h3>稍等一下... 正在填充工价</h3>"
        });
        $.ajax({
            url: url,
            type: 'POST',
            data: {
                fixedValue: $("#fixedValue").val()
            },
            success: function (data) {
                if(data == 0) {
                    $.unblockUI();
                    $("input").val("");
                    swal({
                            type:"success",
                            title:"",
                            text: "<span style=\"font-weight:bolder;font-size: 20px\">恭喜你，保存成功！</span>",
                            html: true
                        },
                        function(){
                            location.href=basePath+"erp/fixedSalaryStart";
                        });
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，保存失败！</span>",html: true});
                }
                $.unblockUI();
            },
            error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        });
    });
    $("#editNo").unbind("click").bind("click", function () {
        $.unblockUI();
        $("input").val("");
    });
}


function deleteFixedSalary(fixedID) {
    swal({
        title: "",
        text: "<span style=\"font-weight:bolder;font-size: 20px\">您确定要删除吗？</span>",
        type: "warning",
        html:true,
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: "确定",
        cancelButtonText:"我再想想",
        confirmButtonColor: "#ec6c62",
        showLoaderOnConfirm: true
    }, function() {
        $.ajax({
            url: basePath + "erp/deletefixedsalary",
            type:'POST',
            data: {
                fixedID: fixedID
            },
            success: function (data) {
                if(data == 0) {
                    $.unblockUI();
                    $("input").val("");
                    swal({
                            type:"success",
                            title:"",
                            text: "<span style=\"font-weight:bolder;font-size: 20px\">恭喜你，删除成功！</span>",
                            html: true
                        },
                        function(){
                            location.href=basePath+"erp/fixedSalaryStart";
                        });
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，删除失败！</span>",html: true});
                }
            },
            error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        })
    });
}


function reLock(obj) {
    swal({
        title: "",
        text: "<span style=\"font-weight:bolder;font-size: 20px\">确定重新锁定本月吗？</span>",
        type: "warning",
        html:true,
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: "确定",
        cancelButtonText:"我再想想",
        confirmButtonColor: "#ec6c62",
        showLoaderOnConfirm: true
    }, function() {
        $.blockUI({ css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            },
            message: "<h3>稍等一下... 正在填充工价</h3>"
        });
        $.ajax({
            url: basePath + "erp/refixedsalary",
            type:'POST',
            data: {
                fixedValue: $(obj).parent().parent().find("td").eq(1).text()
            },
            success: function (data) {
                if(data == 0) {
                    $.unblockUI();
                    swal({
                            type:"success",
                            title:"",
                            text: "<span style=\"font-weight:bolder;font-size: 20px\">恭喜你，操作成功！</span>",
                            html: true
                        },
                        function(){
                            location.href=basePath+"erp/fixedSalaryStart";
                        });
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，操作失败！</span>",html: true});
                }
            },
            error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        })
    });

}
