var form;
layui.use('form', function(){
    form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
    form.render();
});
layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.21'
}).extend({                         // 模块别名
    soulTable: 'soulTable'
});

$(document).ready(function () {
    form.render('select');
    layui.laydate.render({
        elem: '#from',
        trigger: 'click'
    });
    layui.laydate.render({
        elem: '#to',
        trigger: 'click'
    });
    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumber", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });
});

var reportTable;

layui.use(['form', 'soulTable', 'table'], function () {
    var table = layui.table,
        soulTable = layui.soulTable,
        $ = layui.$;
    var load = layer.load();
    reportTable = table.render({
        elem: '#reportTable'
        ,cols: [[]]
        ,loading:true
        ,data:[]
        ,height: 'full-80'
        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
        ,defaultToolbar: ['filter', 'print']
        ,title: '工序状态'
        ,totalRow: true
        ,page: true
        ,even: true
        ,overflow: 'tips'
        ,limits: [500, 1000, 2000]
        ,limit: 500 //每页默认显示的数量
        ,done: function (res, curr, count) {
            soulTable.render(this);
            var divArr = $(".layui-table-total div.layui-table-cell");
            $.each(divArr,function (index,item) {
                var _div = $(item);
                var content = _div.html();
                content = content.replace(".00","");
                _div.html(content);
            });
            layer.close(load);
        }
        ,filter: {
            bottom: true,
            items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
        }
    });

    function initTable(from, to, orderName, procedureState){
        var param = {};
        param.from = from;
        param.to = to;
        if (orderName != null && orderName != ''){
            param.orderName = orderName;
        }
        if (procedureState != null && procedureState != ''){
            param.procedureState = procedureState;
        }
        var load = layer.load();
        $.ajax({
            url: "/erp/monthprocedurereviewreport",
            type: 'GET',
            data: param,
            success: function (res) {
                if (res.data) {
                    var reportData = res.data;
                    console.log(reportData);
                    table.render({
                        elem: '#reportTable'
                        ,cols:[[
                            {type: 'checkbox', fixed: 'left'}
                            ,{type:'numbers', align:'center', title:'序号', width:60}
                            ,{field:'orderName', title:'款号', align:'center', width:170, sort: true, filter: true}
                            ,{field:'clothesVersionNumber', title:'单号', align:'center', width:170, sort: true, filter: true}
                            ,{field:'procedureCode', title:'工序代码', align:'center', width:170, sort: true, filter: true}
                            ,{field:'procedureNumber', title:'工序号', align:'center', width:120, sort: true, filter: true}
                            ,{field:'procedureName', title:'工序名称', align:'center', width:200, sort: true, filter: true}
                            ,{field:'procedureSection', title:'工段', align:'center', width:120, sort: true, filter: true}
                            ,{field:'piecePrice', title:'单价', align:'center', width:120, sort: true, filter: true}
                            ,{field:'procedureState', title:'工序状态', align:'center',width:200, sort: true, filter: true, templet: function (d) {
                                    if (d.procedureState === 0){
                                        return "未提交";
                                    } else if (d.procedureState === 1) {
                                        return "已提交";
                                    } else if (d.procedureState === 2) {
                                        return "审核通过";
                                    } else if (d.procedureState === 3) {
                                        return "未通过";
                                    } else {
                                        return "";
                                    }
                                }}
                            ,{field:'orderProcedureID', hide: true}
                        ]]
                        ,data:reportData
                        ,height: 'full-80'
                        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                        ,defaultToolbar: ['filter', 'print']
                        ,title: '工序状态表'
                        ,loading:true
                        ,totalRow: true
                        ,page: true
                        ,even: true
                        ,overflow: 'tips'
                        ,limits: [500, 1000, 2000]
                        ,limit: 500 //每页默认显示的数量
                        ,done: function (res, curr, count) {
                            soulTable.render(this);
                            layer.close(load);
                        }
                        ,filter: {
                            bottom: true,
                            items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                        }
                    });


                }
            }
        })
    }

    //监听提交
    form.on('submit(searchBeat)', function(data){
        var filterFrom = $("#from").val();
        var filterTo = $("#to").val();
        if (filterFrom == null || filterFrom == '' || filterTo == null || filterTo == ''){
            layer.msg("开始-结束日期不能为空~~~");
            return false;
        }
        var filterOrderName = $("#orderName").val();
        var filterProcedureState = $("#procedureState").val();
        initTable(filterFrom, filterTo, filterOrderName, filterProcedureState);
        return false;
    });

    table.on('toolbar(reportTable)', function(obj) {
        var filterFrom = $("#from").val();
        var filterTo = $("#to").val();
        var filterOrderName = $("#orderName").val();
        var filterProcedureState = $("#procedureState").val();
        if (obj.event === 'clearFilter') {
            soulTable.clearFilter('reportTable')
        } else if (obj.event === 'exportExcel') {
            soulTable.export('reportTable');
        } else if (obj.event === 'change') {
            var checkStatus = table.checkStatus(obj.config.id); //获取选中行状态
            if(checkStatus.data.length == 0) {
                layer.msg('请先选择数据');
                return false;
            } else {
                var orderProcedureIDList = [];
                $.each(checkStatus.data, function (index, item) {
                    orderProcedureIDList.push(item.orderProcedureID);
                });
                layer.confirm('确认提交吗？', function(index){
                    $.ajax({
                        url: "/erp/batchcommitorderprocedure",
                        type:'POST',
                        data: {
                            orderProcedureIDList:orderProcedureIDList,
                            procedureState: 1
                        },
                        traditional:true,
                        success: function (data) {
                            if(data.result == 0) {
                                layer.msg("提交成功！",{icon: 1});
                                initTable(filterFrom, filterTo, filterOrderName, filterProcedureState);
                            }else {
                                layer.msg("提交失败！",{icon: 2});
                            }
                        }, error: function () {
                            layer.msg("提交失败！",{icon: 2});
                        }
                    });
                });
            }
        }
    })

});
