var basePath=$("#basePath").val();
var userRole=$("#userRole").val();
var procedureLevelList = [];
var form;
layui.use('form', function(){
    form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
    form.render();
});

layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.21'
}).extend({                         // 模块别名
    soulTable: 'soulTable',
});

$(document).ready(function () {
    $.ajax({
        url: basePath + "erp/getallprocedurelevel",
        type:'GET',
        data: {},
        success: function (data) {
            if(data) {
                procedureLevelList = data.procedureLevelList;
            }else {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，没有查到相关信息！</span>",html: true});
            }
        },
        error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }
    });
});
var hot;
var procedureTemplateTable;
layui.use(['form', 'soulTable', 'table'], function () {
    var table = layui.table,
        form = layui.form,
        $ = layui.$,
        soulTable = layui.soulTable;
    procedureTemplateTable = table.render({
        id: 'procedureTemplateTable'
        ,elem: '#procedureTemplateTable'
        ,url:'getallproceduretemplate'
        ,page: true
        ,cols:[[
            {type:'checkbox'}
            ,{type:'numbers', align:'center', title:'序号', width:60}
            ,{field:'styleType', title:'款式类型', align:'center', width:120, sort: true, filter: true}
            ,{field:'partName', title:'部位', align:'center', width:120, sort: true, filter: true}
            ,{field:'procedureCode', title:'工序代码', align:'center', width:100, sort: true, filter: true}
            ,{field:'procedureNumber', title:'工序号', align:'center',width:100, sort: true, filter: true}
            ,{field:'procedureName', title:'工序名', align:'center', width:150, sort: true, filter: true}
            ,{field:'procedureSection', title:'工段', align:'center', width:120, sort: true, filter: true, totalRow: true}
            ,{field:'procedureDescription', title:'工序描述', align:'center', width:200, sort: true, filter: true}
            ,{field:'remark', title:'备注', align:'center', width:100, sort: true, filter: true}
            ,{field:'segment', title:'码段', align:'center', width:100, sort: true, filter: true}
            ,{field:'equType', title:'设备类型', align:'center', width:100, sort: true, filter: true}
            ,{field:'procedureLevel', title:'级别', align:'center', width:80, sort: true, filter: true}
            ,{field:'sam', title:'SAM值', align:'center', width:80, sort: true, filter: true}
            ,{field:'packagePrice', title:'工价/打', align:'center', width:100, sort: true, filter: true}
            ,{field:'piecePrice', title:'工价/件', align:'center', width:100, sort: true, filter: true}
            ,{field: 'procedureID', title:'操作', align:'center', fixed: 'right', toolbar: '#barTop', width:200}
        ]]
        ,loading:true
        ,limit: 200 //每页默认显示的数量
        ,limits: [200, 1000, 2000, 3000, 4000]
        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
        ,overflow: 'tips'
        ,title: '工序库'
        ,height: 'full-100'
        ,done: function () {
            // soulTable.render(this);
        }

    });

    form.on('submit(search)', function (data) {
        var searchProcedureNumber = $("#searchProcedureNumber").val();
        var searchProcedureCode = $("#searchProcedureCode").val();
        var searchProcedureName = $("#searchProcedureName").val();
        var searchProcedureDescription = $("#searchProcedureDescription").val();
        var searchSegment = $("#searchSegment").val();
        var searchRemark = $("#searchRemark").val();
        procedureTemplateTable = table.render({
            id: 'procedureTemplateTable'
            ,elem: '#procedureTemplateTable'
            ,url:'getallproceduretemplate'
            ,where: {
                procedureNumber: searchProcedureNumber,
                procedureCode: searchProcedureCode,
                subProcedureName: searchProcedureName,
                subProcedureDescription: searchProcedureDescription,
                subSegment: searchSegment,
                subRemark: searchRemark
            }
            ,page: true
            ,cols:[[
                {type:'checkbox'}
                ,{type:'numbers', align:'center', title:'序号', width:60}
                ,{field:'styleType', title:'款式类型', align:'center', width:120, sort: true, filter: true}
                ,{field:'partName', title:'部位', align:'center', width:120, sort: true, filter: true}
                ,{field:'procedureCode', title:'工序代码', align:'center', width:100, sort: true, filter: true}
                ,{field:'procedureNumber', title:'工序号', align:'center',width:100, sort: true, filter: true}
                ,{field:'procedureName', title:'工序名', align:'center', width:150, sort: true, filter: true}
                ,{field:'procedureSection', title:'工段', align:'center', width:120, sort: true, filter: true, totalRow: true}
                ,{field:'procedureDescription', title:'工序描述', align:'center', width:200, sort: true, filter: true}
                ,{field:'remark', title:'备注', align:'center', width:100, sort: true, filter: true}
                ,{field:'segment', title:'码段', align:'center', width:100, sort: true, filter: true}
                ,{field:'equType', title:'设备类型', align:'center', width:100, sort: true, filter: true}
                ,{field:'procedureLevel', title:'级别', align:'center', width:80, sort: true, filter: true}
                ,{field:'sam', title:'SAM值', align:'center', width:80, sort: true, filter: true}
                ,{field:'packagePrice', title:'工价/打', align:'center', width:100, sort: true, filter: true}
                ,{field:'piecePrice', title:'工价/件', align:'center', width:100, sort: true, filter: true}
                ,{field: 'procedureID', title:'操作', align:'center', fixed: 'right', toolbar: '#barTop', width:200}
            ]]
            ,loading:true
            ,limit: 200 //每页默认显示的数量
            ,limits: [200, 1000, 2000, 3000, 4000]
            ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
            ,overflow: 'tips'
            ,title: '工序库'
            ,height: 'full-100'
            ,done: function () {
                // soulTable.render(this);
            }
        });

    });
    table.on('toolbar(procedureTemplateTable)', function(obj) {
        if (obj.event === 'clearFilter') {
            // 清除所有筛选条件并重载表格
            // 参数: tableId
            soulTable.clearFilter('procedureTemplateTable')
        } else if (obj.event === 'refresh') {
            procedureTemplateTable.reload();
        } else if (obj.event === 'addBatch') {
            var index = layer.open({
                type: 1 //Page层类型
                , title: '批量添加'
                , btn: ['提交']
                , shade: 0.6 //遮罩透明度
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , content: $('#procedureAdd')
                , yes: function () {

                    var procedureTemplateJson = [];
                    var data = hot.getData();
                    if (data[0][0] == null || data[0][0] == ""){
                        layer.msg("输入有误！");
                        return false;
                    }
                    $.each(data,function (index, item) {
                        if(item[0]!=null && item[0]!="") {
                            var procedureTemplate = {};
                            procedureTemplate.styleType = item[0].trim();
                            procedureTemplate.partName = item[1].trim();
                            procedureTemplate.procedureNumber = item[2].trim();
                            procedureTemplate.procedureName = item[3].trim();
                            procedureTemplate.procedureSection = item[4].trim();
                            procedureTemplate.procedureDescription = item[5].trim();
                            procedureTemplate.remark = item[6].trim();
                            procedureTemplate.segment = item[7].trim();
                            procedureTemplate.equType = item[8].trim();
                            procedureTemplate.procedureLevel = item[9].trim();
                            procedureTemplate.SAM = item[10].trim();
                            procedureTemplate.packagePrice = item[11].trim();
                            procedureTemplate.piecePrice = item[12].trim();
                            procedureTemplateJson.push(procedureTemplate);
                        }
                    });
                    if (procedureTemplateJson.length == 0){
                        layer.msg("输入不能为空！");
                        return false;
                    }
                    $.ajax({
                        url: "/erp/addproceduretemplatebatch",
                        type:'POST',
                        data: {
                            procedureTemplateJson:JSON.stringify(procedureTemplateJson),
                        },
                        success: function (data) {
                            if(data == 0) {
                                layer.msg("录入成功！", {icon: 1});
                                $("#addProcedureTemplateExcel").empty();
                                layer.close(index);
                                procedureTemplateTable.reload();
                            }else {
                                layer.msg("录入失败！");
                            }
                        }, error: function () {
                            layer.msg("发生错误！");
                        }
                    })

                }
                , cancel : function (i,layero) {
                    $("#addProcedureTemplateExcel").empty();
                }
            });
            layer.full(index);
            setTimeout(function () {
                var container = document.getElementById('addProcedureTemplateExcel');
                hot = new Handsontable(container, {
                    rowHeaders: true,
                    colHeaders:['款式类型','部位','工序号','工序名','工段','工序描述','备注','码段','设备类型','级别','SAM值','工价/打','工价/件'],
                    autoColumnSize:true,
                    dropdownMenu: true,
                    contextMenu:true,
                    stretchH: 'all',
                    autoWrapRow: true,
                    manualRowResize: true,
                    manualColumnResize: true,
                    minRows:20,
                    minCols:13,
                    colWidths:[60,60,60,100,60,120,60,60,60,60,60,60,60],
                    language:'zh-CN',
                    licenseKey: 'non-commercial-and-evaluation'
                });
            },100)
        } else if (obj.event === 'exportExcel') {
            soulTable.export('procedureTemplateTable');
        } else if (obj.event === 'batchDelete') {
            var checkStatus = table.checkStatus(obj.config.id); //获取选中行状态
            if (userRole!='root' && userRole!='role5'){
                layer.msg('您没有权限!');
                return false;
            } else if(checkStatus.data.length == 0) {
                layer.msg('请先选择数据');
                return false;
            } else {
                var procedureIDList = [];
                $.each(checkStatus.data, function (index, item) {
                    procedureIDList.push(item.procedureID);
                });
                layer.confirm('真的删除吗', function(index){
                    $.ajax({
                        url: basePath + "erp/deleteproceduretemplatebatch",
                        type:'POST',
                        data: {
                            procedureIDList:procedureIDList
                        },
                        traditional:true,
                        success: function (data) {
                            if(data.result == 0) {
                                layer.msg("删除成功！",{icon: 1});
                                procedureTemplateTable.reload();
                            }else {
                                layer.msg("删除失败！",{icon: 2});
                            }
                        }, error: function () {
                            layer.msg("删除失败！",{icon: 2});
                        }
                    });
                });
            }
        }
    });

    //监听行工具事件
    table.on('tool(procedureTemplateTable)', function(obj){
        var data = obj.data;
        if(obj.event === 'del'){
            layer.confirm('真的删除吗', function(index){
                $.ajax({
                    url: "/erp/deleteproceduretemplate",
                    type: 'POST',
                    data: {
                        procedureID: data.procedureID
                    },
                    success: function (res) {
                        if (res == 0) {
                            obj.del();
                            layer.close(index);
                            layer.msg("删除成功！", {icon: 1});
                        } else {
                            layer.msg("删除失败！", {icon: 2});
                        }
                    },
                    error: function () {
                        layer.msg("删除失败！", {icon: 2});
                    }
                })

            });
        } else if(obj.event === 'update'){
            var index = layer.open({
                type: 1 //Page层类型
                , title: '修改工序'
                , btn: ['保存']
                , shade: 0.6 //遮罩透明度
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , content: $('#procedureUpdate')
                ,yes: function(index, layero){
                    var params = {};
                    params.styleType = $("#procedureUpdate input[name='styleType']").val();
                    params.partName = $("#procedureUpdate input[name='partName']").val();
                    params.procedureCode = $("#procedureUpdate input[name='procedureCode']").val();
                    params.procedureNumber = $("#procedureUpdate input[name='procedureNumber']").val();
                    params.procedureName = $("#procedureUpdate input[name='procedureName']").val();
                    params.procedureSection = $("#procedureUpdate input[name='procedureSection']").val();
                    params.procedureDescription = $("#procedureUpdate input[name='procedureDescription']").val();
                    params.remark = $("#procedureUpdate input[name='remark']").val();
                    params.segment = $("#procedureUpdate input[name='segment']").val();
                    params.equType = $("#procedureUpdate input[name='equType']").val();
                    params.SAM = $("#procedureUpdate input[name='sam']").val();
                    params.packagePrice = $("#procedureUpdate input[name='packagePrice']").val();
                    params.piecePrice = $("#procedureUpdate input[name='piecePrice']").val();
                    params.procedureID = data.procedureID;
                    var thisProcedureLevel = "A";
                    $.each(procedureLevelList, function (index, item) {
                        if ($("#procedureLevel").val() == item.levelValue){
                            thisProcedureLevel = item.level;
                        }
                    });
                    params.procedureLevel = thisProcedureLevel;
                    $.ajax({
                        url: "/erp/updateproceduretemplate",
                        type: 'POST',
                        data: params,
                        success: function (res) {
                            if (res == 0) {
                                layer.close(index);
                                layer.msg("保存成功！", {icon: 1});
                                procedureTemplateTable.reload();
                                $("#procedureUpdate").find("input").val("");
                                $("#procedureUpdate").find("select").empty();
                            } else {
                                layer.msg("保存失败！", {icon: 2});
                            }
                        },
                        error: function () {
                            layer.msg("保存失败！", {icon: 2});
                        }
                    })
                }
                , cancel : function (i,layero) {
                    $("#procedureUpdate").find("input").val("");
                    $("#procedureUpdate").find("select").empty();
                }
            });
            layer.full(index);
            setTimeout(function () {
                $("#procedureUpdate input[name='procedureNumber']").attr("readonly","readonly");
                $("#procedureUpdate input[name='procedureCode']").attr("readonly","readonly");
                $("#procedureUpdate input[name='styleType']").val(data.styleType);
                $("#procedureUpdate input[name='partName']").val(data.partName);
                $("#procedureUpdate input[name='procedureCode']").val(data.procedureCode);
                $("#procedureUpdate input[name='procedureNumber']").val(data.procedureNumber);
                $("#procedureUpdate input[name='procedureName']").val(data.procedureName);
                $("#procedureUpdate input[name='procedureSection']").val(data.procedureSection);
                $("#procedureUpdate input[name='procedureDescription']").val(data.procedureDescription);
                $("#procedureUpdate input[name='remark']").val(data.remark);
                $("#procedureUpdate input[name='segment']").val(data.segment);
                $("#procedureUpdate input[name='equType']").val(data.equType);
                $("#procedureUpdate input[name='sam']").val(data.sam);
                $("#procedureUpdate input[name='packagePrice']").val(data.packagePrice);
                $("#procedureUpdate input[name='piecePrice']").val(data.piecePrice);
                $("#procedureUpdate select[name='procedureLevel']").empty();
                $.each(procedureLevelList, function (index, item) {
                    if (data.procedureLevel === item.level){
                        $("#procedureUpdate select[name='procedureLevel']").append("<option value="+item.levelValue+" selected='selected'>"+item.level+"</option>");
                    } else {
                        $("#procedureUpdate select[name='procedureLevel']").append("<option value="+item.levelValue+">"+item.level+"</option>");
                    }
                });

                layui.form.render("select");
            }, 100);


        } else if(obj.event === 'copy'){
            var index = layer.open({
                type: 1 //Page层类型
                , title: '修改工序'
                , btn: ['保存']
                , shade: 0.6 //遮罩透明度
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , content: $('#procedureUpdate')
                ,yes: function(index, layero){
                    var params = {};
                    params.styleType = $("#procedureUpdate input[name='styleType']").val();
                    params.partName = $("#procedureUpdate input[name='partName']").val();
                    params.procedureNumber = $("#procedureUpdate input[name='procedureNumber']").val();
                    params.procedureName = $("#procedureUpdate input[name='procedureName']").val();
                    params.procedureSection = $("#procedureUpdate input[name='procedureSection']").val();
                    params.procedureDescription = $("#procedureUpdate input[name='procedureDescription']").val();
                    params.remark = $("#procedureUpdate input[name='remark']").val();
                    params.segment = $("#procedureUpdate input[name='segment']").val();
                    params.equType = $("#procedureUpdate input[name='equType']").val();
                    params.SAM = $("#procedureUpdate input[name='sam']").val();
                    params.packagePrice = $("#procedureUpdate input[name='packagePrice']").val();
                    params.piecePrice = $("#procedureUpdate input[name='piecePrice']").val();
                    var thisProcedureLevel = "A";
                    $.each(procedureLevelList, function (index, item) {
                        if ($("#procedureLevel").val() == item.levelValue){
                            thisProcedureLevel = item.level;
                        }
                    });
                    params.procedureLevel = thisProcedureLevel;
                    var procedureTemplateJson = [params];
                    $.ajax({
                        url: "/erp/addproceduretemplatebatch",
                        type: 'POST',
                        data: {
                            procedureTemplateJson:JSON.stringify(procedureTemplateJson)
                        },
                        success: function (res) {
                            if (res == 0) {
                                layer.close(index);
                                layer.msg("保存成功！", {icon: 1});
                                procedureTemplateTable.reload();
                                $("#procedureUpdate").find("input").val("");
                                $("#procedureUpdate").find("select").empty();
                            } else {
                                layer.msg("保存失败！", {icon: 2});
                            }
                        },
                        error: function () {
                            layer.msg("保存失败！", {icon: 2});
                        }
                    })
                }
                , cancel : function (i,layero) {
                    $("#procedureUpdate").find("input").val("");
                    $("#procedureUpdate").find("select").empty();
                }
            });
            layer.full(index);
            setTimeout(function () {
                $("#procedureUpdate input[name='procedureNumber']").removeAttr("readonly");
                $("#procedureUpdate input[name='procedureCode']").attr("readonly","readonly");
                $("#procedureUpdate input[name='styleType']").val(data.styleType);
                $("#procedureUpdate input[name='partName']").val(data.partName);
                $("#procedureUpdate input[name='procedureNumber']").val(data.procedureNumber);
                $("#procedureUpdate input[name='procedureCode']").val("自动生成");
                $("#procedureUpdate input[name='procedureName']").val(data.procedureName);
                $("#procedureUpdate input[name='procedureSection']").val(data.procedureSection);
                $("#procedureUpdate input[name='procedureDescription']").val(data.procedureDescription);
                $("#procedureUpdate input[name='remark']").val(data.remark);
                $("#procedureUpdate input[name='segment']").val(data.segment);
                $("#procedureUpdate input[name='equType']").val(data.equType);
                $("#procedureUpdate input[name='sam']").val(data.sam);
                $("#procedureUpdate input[name='packagePrice']").val(data.packagePrice);
                $("#procedureUpdate input[name='piecePrice']").val(data.piecePrice);
                $("#procedureUpdate select[name='procedureLevel']").empty();
                $.each(procedureLevelList, function (index, item) {
                    if (data.procedureLevel === item.level){
                        $("#procedureUpdate select[name='procedureLevel']").append("<option value="+item.levelValue+" selected='selected'>"+item.level+"</option>");
                    } else {
                        $("#procedureUpdate select[name='procedureLevel']").append("<option value="+item.levelValue+">"+item.level+"</option>");
                    }
                });

                layui.form.render("select");
            }, 100);
        }
    });

    form.on('select(levelTest)', function(data){
        var opt = Number(data.value);
        var samValue = $("#sam").val().trim();
        var tmp1 = toDecimal(opt*12*samValue);
        var tmp2 = toDecimalFour(opt*samValue);
        $("#packagePrice").val(tmp1);
        $("#piecePrice").val(tmp2);
    });

});


function changeFunction(){
    var opt = Number($("#procedureLevel").val());
    var samValue = $("#sam").val().trim();
    var tmp1 = toDecimal(opt*12*samValue);
    var tmp2 = toDecimalFour(opt*samValue);
    $("#packagePrice").val(tmp1);
    $("#piecePrice").val(tmp2);
}

function toDecimal(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*100)/100;
    return f;
}

function toDecimalFour(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*10000)/10000;
    return f;
}

function isNumber(val){

    var regPos = /^\d+(\.\d+)?$/; //非负浮点数
    var regNeg = /^(-(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*)))$/; //负浮点数
    if (regPos.test(val) || regNeg.test(val)){
        return true;
    }else{
        return false;
    }

}