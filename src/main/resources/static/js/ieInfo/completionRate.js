var basePath=$("#basePath").val();
var hot;
layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.21'
}).extend();
$(document).ready(function () {

    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumber", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });

    layui.laydate.render({
        elem: '#from',
        min: -7,
        max: 0,
        trigger: 'click'
    });
    layui.laydate.render({
        elem: '#to',
        min: -7,
        max: 0,
        trigger: 'click'
    });
});

function search() {
    if ($('#from').val() == ""){
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入起始日期信息！</span>",html: true});
    }
    if ($('#to').val() == ""){
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入起始日期信息！</span>",html: true});
    }
    if (comDate($('#from').val(),$('#to').val())){
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">日期范围有误！</span>",html: true});
    }
    $.blockUI({ css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        },
        message: "<h3>稍等一下... 奋力搜索中</h3>"
    });
    var param = {};
    param.from = $('#from').val();
    param.to = $('#to').val();
    param.workHour = $('#workHour').val();
    if ($('#groupName').val() != null && $('#groupName').val() != ''){
        param.groupName = $('#groupName').val();
    }
    if ($('#orderName').val() != null && $('#orderName').val() != ''){
        param.orderName = $('#orderName').val();
    }
    $.ajax({
        url: "/erp/getcompletionratebytime",
        data: param,
        success:function(data){
            if (data){
                $("#exportDiv").show();
                var date = [];
                var hotData = [];
                date[0] = [];
                date[1] = [];
                date[2] = [];
                date[3] = [];
                date[4] = [];
                var totalIndex = 0;
                for (var indexKey in data){
                    var titleRow = [];
                    var row1 = [];
                    var row2 = [];
                    var row3 = [];
                    var row4 = [];
                    titleRow[0]=["组名"];
                    titleRow[1]=["版单"];
                    titleRow[2]=["订单"];
                    titleRow[3]=["款式"];
                    titleRow[4]=["累计完成"];
                    row2[0] = [];
                    row2[1] = [];
                    row2[2] = [];
                    row2[3] = [];
                    row2[4] = ["当日计划完成"];
                    row3[0] = [];
                    row3[1] = [];
                    row3[2] = [];
                    row3[3] = [];
                    row3[4] = ["当日实际完成"];
                    row4[0] = [];
                    row4[1] = [];
                    row4[2] = [];
                    row4[3] = [];
                    row4[4] = ["达成率"];
                    var completionData = data[indexKey];
                    var beginDate;
                    var deadLine;
                    var planFinishDate;
                    var planCount = 0;
                    var divideIndex = 0;
                    for (var divideKey in completionData){
                        var divideCompletionData = completionData[divideKey];
                        if (totalIndex == 0){
                            date.push(transDate(divideCompletionData.recordDate))
                        }
                        if (divideIndex == 0){
                            beginDate = transDate(divideCompletionData.beginDate);
                            deadLine = transDate(divideCompletionData.deadLine);
                            row1[0] = [divideCompletionData.groupName];
                            row1[1] = [divideCompletionData.clothesVersionNumber];
                            row1[2] = [divideCompletionData.orderName];
                            row1[3] = [divideCompletionData.styleDescription];
                            row1[4] = ["当日员工数"];
                        }
                        if (divideCompletionData.planCount > 0){
                            planCount = divideCompletionData.planCount;
                        }
                        titleRow.push(divideCompletionData.finishSum);
                        row1.push(divideCompletionData.todayEmpCount);
                        row2.push(divideCompletionData.todayPlanCount);
                        row3.push(divideCompletionData.todayActualCount);
                        row4.push(toPercent(divideCompletionData.achieveRate));
                        planFinishDate = transDate(divideCompletionData.planFinishDate);
                        divideIndex ++;
                    }
                    if (totalIndex == 0){
                        date.push(["上线日期"]);
                        date.push(["本单本组计划产能"]);
                        date.push(["计划下线日期"]);
                        date.push(["预计完成日期"]);
                        date.push([""]);
                        hotData.push(date);
                    }
                    totalIndex ++;
                    titleRow.push([beginDate]);
                    titleRow.push([planCount]);
                    titleRow.push([deadLine]);
                    titleRow.push([planFinishDate]);
                    hotData.push(titleRow);
                    hotData.push(row1);
                    hotData.push(row2);
                    hotData.push(row3);
                    hotData.push(row4);
                    hotData.push([]);
                }
                if(hotData.length == 0) {
                    hotData = [[]];
                }
                var container = document.getElementById('reportExcel');
                if (hot == undefined) {
                    hot = new Handsontable(container, {
                        data: hotData,
                        rowHeaders: true,
                        colHeaders: true,
                        autoColumnSize: true,
                        dropdownMenu: true,
                        contextMenu: true,
                        // minRows: 35,
                        // minCols: 16,
                        // colWidths: 80,
                        stretchH: "all",
                        language: 'zh-CN',
                        licenseKey: 'non-commercial-and-evaluation'

                    });
                }else {
                    hot.loadData(hotData);
                }

            }else {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，没有查到相关信息！</span>",html: true});
            }
            $.unblockUI();
        },
        error: function () {
            $.unblockUI();
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }
    });

}


function exportData() {
    var data = hot.getData();
    var result = [];
    var col = 0;
    $.each(data,function (index, item) {
        for(var i=0;i<item.length;i++) {
            if(item[i]==null) {
                item[i] = '';
            }else if(index==1) {
                col++;
            }
        }
        result.push(item);
    });
    var myDate = new Date();
    export2Excel(['达成率'+myDate.toLocaleString()], col, result, '达成率'+myDate.toLocaleString()+".xls");
}



function comDate(date1,date2){
    var oDate1 = new Date(date1);
    var oDate2 = new Date(date2);
    if(oDate1.getTime() > oDate2.getTime()){
        return true;
    } else {
        return false;
    }
}

function toPercent(point){
    var str=Number(point*100).toFixed(1);
    str+="%";
    return str;
}

function transDate(data) {return  moment(data).format("YYYY-MM-DD");};