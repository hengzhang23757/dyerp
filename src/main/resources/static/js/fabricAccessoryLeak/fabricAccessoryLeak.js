var form;
layui.use('form', function(){
    form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
    form.render();
});
layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.24'
}).extend({                         // 模块别名
    soulTable: 'soulTable'
});
$(document).on('keydown', '.layui-table-edit', function (e) {
    var td = $(this).parent('td')
        ,tr = td.parent('tr')
        ,trs = tr.parent().parent().find('tr')
        ,tr_index = tr.index()
        ,td_index = td.index()
        ,td_last_index = tr.find('[data-edit="text"]:last').index()
        ,td_first_index = tr.find('[data-edit="text"]:first').index();
    switch (e.keyCode) {
        case 13:
            tr.next().find('td').eq(td_index).click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            return false;
        case 39:
            td.nextAll('[data-edit="text"]:first').click();
            if (td_index == td_last_index){
                tr.next().find('td').eq(td_first_index).click();
                if (tr_index == trs.length - 1){
                    trs.eq(0).find('td').eq(td_first_index).click();
                }
            }
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
        case 37:
            td.prevAll('[data-edit="text"]:first').click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
        case 38:
            tr.prev().find('td').eq(td_index).click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
        case 40:
            tr.next().find('td').eq(td_index).click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
    }

})
$(document).ready(function () {
    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumber", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });
    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumberTwo", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumberTwo',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderNameTwo',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumberTwo',
                    field: 'clothesVersionNumberTwo'
                }, {
                    name: 'orderNameTwo',
                    field: 'orderNameTwo'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderNameTwo", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumberTwo',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderNameTwo',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumberTwo',
                    field: 'clothesVersionNumberTwo'
                }, {
                    name: 'orderNameTwo',
                    field: 'orderNameTwo'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });
    $("#orderName").bind('blur change',function(){
        var orderName = $("#orderName").val();
        $("#colorName").empty();
        $.ajax({
            url: "/erp/getcolorhint",
            data: {"orderName": orderName},
            success:function(data){
                $("#colorName").empty();
                if (data.colorList) {
                    $("#colorName").empty();
                    $("#colorName").append("<option value=''>选择颜色</option>");
                    $.each(data.colorList, function(index,element){
                        $("#colorName").append("<option value='"+element.colorName+"'>"+element.colorName+"</option>");
                    });
                    form.render();
                }
            },
            error:function(){
            }
        });
    });
    $("#clothesVersionNumber").bind('blur change',function(){
        var orderName = $("#orderName").val();
        $("#colorName").empty();
        $.ajax({
            url: "/erp/getcolorhint",
            data: {"orderName": orderName},
            success:function(data){
                $("#colorName").empty();
                if (data.colorList) {
                    $("#colorName").empty();
                    $("#colorName").append("<option value=''>选择颜色</option>");
                    $.each(data.colorList, function(index,element){
                        $("#colorName").append("<option value='"+element.colorName+"'>"+element.colorName+"</option>");
                    });
                    form.render();
                }
            },
            error:function(){
            }
        });
    });
});
var summaryTable,inStoreTable,storageTable,tmpStorageTable,outStoreTable;
layui.use(['form', 'soulTable', 'table', 'laydate'], function () {
    var table = layui.table,
        soulTable = layui.soulTable,
        form = layui.form,
        $ = layui.$;
    table.render({
        elem: '#summaryTable'
        ,height: 'full-130'
        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
        ,cols: [[]]
        ,loading:false
        ,totalRow: true
        ,page: true
        ,even: true
        ,limits: [50, 100, 200]
        ,limit: 100 //每页默认显示的数量
        ,done: function (res, curr, count) {
            soulTable.render(this);
        }
    });
    table.render({
        elem: '#inStoreTable'
        ,height: 'full-130'
        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
        ,cols: [[]]
        ,loading:false
        ,totalRow: true
        ,page: true
        ,even: true
        ,limits: [50, 100, 200]
        ,limit: 100 //每页默认显示的数量
        ,done: function (res, curr, count) {
            soulTable.render(this);
        }
    });
    table.render({
        elem: '#storageTable'
        ,height: 'full-130'
        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
        ,cols: [[]]
        ,loading:false
        ,totalRow: true
        ,page: true
        ,even: true
        ,limits: [50, 100, 200]
        ,limit: 100 //每页默认显示的数量
        ,done: function (res, curr, count) {
            soulTable.render(this);
        }
    });
    table.render({
        elem: '#tmpStorageTable'
        ,height: 'full-130'
        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
        ,cols: [[]]
        ,loading:false
        ,totalRow: true
        ,page: true
        ,even: true
        ,limits: [50, 100, 200]
        ,limit: 100 //每页默认显示的数量
        ,done: function (res, curr, count) {
            soulTable.render(this);
        }
    });
    table.render({
        elem: '#outStoreTable'
        ,height: 'full-130'
        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
        ,cols: [[]]
        ,loading:false
        ,totalRow: true
        ,page: true
        ,even: true
        ,limits: [50, 100, 200]
        ,limit: 100 //每页默认显示的数量
        ,done: function (res, curr, count) {
            soulTable.render(this);
        }
    });
    function initTable(orderName, colorName, type){
        var param = {};
        param.orderName = orderName;
        if (colorName != null && colorName != ''){
            param.colorName = colorName;
        }
        var load = layer.load(2);
        if (type === "面料"){
            $.ajax({
                url: "/erp/searchfabricleakbyorder",
                type:'GET',
                data: param,
                success: function (data) {
                    layer.close(load);
                    var fabricDifferenceList = data.fabricDifferenceList;
                    var fabricDetailList = data.fabricDetailList;
                    var fabricStorageList = data.fabricStorageList;
                    var fabricDetailTmpList = data.fabricDetailTmpList;
                    var fabricOutRecordList = data.fabricOutRecordList;
                    table.render({
                        elem: '#summaryTable'
                        ,height: 'full-150'
                        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                        ,cols: [[
                            {type: 'numbers', align:'center', title:'序号', minWidth:80}
                            ,{field: 'clothesVersionNumber', title:'单号', align:'center', minWidth:120, sort: true, filter: true, totalRowText: '合计'}
                            ,{field: 'orderName', title:'款号', align:'center', minWidth:120, sort: true, filter: true}
                            ,{field: 'fabricName', title:'面料名', align:'center', minWidth:120, sort: true, filter: true}
                            ,{field: 'fabricNumber', title:'面料号', align:'center', minWidth:120, sort: true, filter: true}
                            ,{field: 'supplier', title:'供应商', align:'center', minWidth:100, sort: true, filter: true}
                            ,{field: 'colorName', title:'色组', align:'center', minWidth:100, sort: true, filter: true}
                            ,{field: 'fabricColor', title:'面料颜色', align:'center', minWidth:120, sort: true, filter: true}
                            ,{field: 'fabricUse', title:'用途', align:'center', minWidth:120, sort: true, filter: true}
                            ,{field: 'unit', title:'单位', align:'center', minWidth:90, sort: true, filter: true}
                            ,{field: 'orderCount', title:'订购量', align:'center', minWidth:100, sort: true, filter: true, totalRow: true}
                            ,{field: 'actualCount', title:'回布量', align:'center', minWidth:100, sort: true, filter: true, totalRow: true}
                            ,{field: 'differenceCount', title:'差异', align:'center', minWidth:100, sort: true, filter: true, totalRow: true}
                            ,{field: 'batchNumber', title:'回布卷数', align:'center', minWidth:100, sort: true, filter: true, totalRow: true}

                        ]]
                        ,data: fabricDifferenceList
                        ,loading:false
                        ,totalRow: true
                        ,page: true
                        ,even: true
                        ,overflow: "tips"
                        ,limits: [50, 100, 200]
                        ,limit: 100 //每页默认显示的数量
                        ,done: function (res, curr, count) {
                            soulTable.render(this);
                        }
                    });
                    table.render({
                        elem: '#inStoreTable'
                        ,height: 'full-150'
                        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                        ,cols: [[
                            {type: 'numbers', align:'center', title:'序号', minWidth:80}
                            ,{field: 'clothesVersionNumber', title:'单号', align:'center', minWidth:120, sort: true, filter: true, totalRowText: '合计'}
                            ,{field: 'orderName', title:'款号', align:'center', minWidth:120, sort: true, filter: true}
                            ,{field: 'fabricName', title:'面料名', align:'center', minWidth:120, sort: true, filter: true}
                            ,{field: 'fabricNumber', title:'面料号', align:'center', minWidth:120, sort: true, filter: true}
                            ,{field: 'supplier', title:'供应商', align:'center', minWidth:100, sort: true, filter: true}
                            ,{field: 'colorName', title:'色组', align:'center', minWidth:100, sort: true, filter: true}
                            ,{field: 'fabricColor', title:'面料颜色', align:'center', minWidth:120, sort: true, filter: true}
                            ,{field: 'unit', title:'单位', align:'center', minWidth:90, sort: true, filter: true}
                            ,{field: 'weight', title:'数量', align:'center', minWidth:100, sort: true, filter: true, totalRow: true}
                            ,{field: 'batchNumber', title:'卷数', align:'center', minWidth:100, sort: true, filter: true, totalRow: true}
                            ,{field: 'jarName', title:'缸号', align:'center', minWidth:100, sort: true, filter: true}
                            ,{field: 'location', title:'位置', align:'center', minWidth:100, sort: true, filter: true}
                            ,{field: 'returnTime', title:'日期', align:'center', minWidth:100, sort: true, filter: true, templet: function (d) {
                                    return moment(d.returnTime).format("YYYY-MM-DD");
                                }}
                            ,{field: 'operateType', title:'操作类型', align:'center', minWidth:100, sort: true, filter: true}
                            ,{field: 'remark', title:'备注', align:'center', minWidth:100, sort: true, filter: true}

                        ]]
                        ,data: fabricDetailList
                        ,loading:false
                        ,totalRow: true
                        ,page: true
                        ,even: true
                        ,overflow: "tips"
                        ,limits: [50, 100, 200]
                        ,limit: 100 //每页默认显示的数量
                        ,done: function (res, curr, count) {
                            soulTable.render(this);
                        }
                    });
                    table.render({
                        elem: '#storageTable'
                        ,height: 'full-150'
                        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                        ,cols: [[
                            {type: 'numbers', align:'center', title:'序号', minWidth:80}
                            ,{field: 'clothesVersionNumber', title:'单号', align:'center', minWidth:120, sort: true, filter: true, totalRowText: '合计'}
                            ,{field: 'orderName', title:'款号', align:'center', minWidth:120, sort: true, filter: true}
                            ,{field: 'fabricName', title:'面料名', align:'center', minWidth:120, sort: true, filter: true}
                            ,{field: 'fabricNumber', title:'面料号', align:'center', minWidth:120, sort: true, filter: true}
                            ,{field: 'supplier', title:'供应商', align:'center', minWidth:100, sort: true, filter: true}
                            ,{field: 'colorName', title:'色组', align:'center', minWidth:100, sort: true, filter: true}
                            ,{field: 'fabricColor', title:'面料颜色', align:'center', minWidth:120, sort: true, filter: true}
                            ,{field: 'unit', title:'单位', align:'center', minWidth:90, sort: true, filter: true}
                            ,{field: 'weight', title:'数量', align:'center', minWidth:100, sort: true, filter: true, totalRow: true}
                            ,{field: 'batchNumber', title:'卷数', align:'center', minWidth:100, sort: true, filter: true, totalRow: true}
                            ,{field: 'jarName', title:'缸号', align:'center', minWidth:100, sort: true, filter: true}
                            ,{field: 'location', title:'位置', align:'center', minWidth:100, sort: true, filter: true}
                            ,{field: 'returnTime', title:'日期', align:'center', minWidth:100, sort: true, filter: true, templet: function (d) {
                                    return moment(d.returnTime).format("YYYY-MM-DD");
                                }}
                            ,{field: 'operateType', title:'操作类型', align:'center', minWidth:100, sort: true, filter: true}
                            ,{field: 'remark', title:'备注', align:'center', minWidth:100, sort: true, filter: true}

                        ]]
                        ,data: fabricStorageList
                        ,loading:false
                        ,totalRow: true
                        ,page: true
                        ,even: true
                        ,overflow: "tips"
                        ,limits: [50, 100, 200]
                        ,limit: 100 //每页默认显示的数量
                        ,done: function (res, curr, count) {
                            soulTable.render(this);
                        }
                    });
                    table.render({
                        elem: '#tmpStorageTable'
                        ,height: 'full-150'
                        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                        ,cols: [[
                            {type: 'numbers', align:'center', title:'序号', minWidth:80}
                            ,{field: 'clothesVersionNumber', title:'单号', align:'center', minWidth:120, sort: true, filter: true, totalRowText: '合计'}
                            ,{field: 'orderName', title:'款号', align:'center', minWidth:120, sort: true, filter: true}
                            ,{field: 'fabricName', title:'面料名', align:'center', minWidth:120, sort: true, filter: true}
                            ,{field: 'fabricNumber', title:'面料号', align:'center', minWidth:120, sort: true, filter: true}
                            ,{field: 'supplier', title:'供应商', align:'center', minWidth:100, sort: true, filter: true}
                            ,{field: 'colorName', title:'色组', align:'center', minWidth:100, sort: true, filter: true}
                            ,{field: 'fabricColor', title:'面料颜色', align:'center', minWidth:120, sort: true, filter: true}
                            ,{field: 'unit', title:'单位', align:'center', minWidth:90, sort: true, filter: true}
                            ,{field: 'weight', title:'数量', align:'center', minWidth:100, sort: true, filter: true, totalRow: true}
                            ,{field: 'batchNumber', title:'卷数', align:'center', minWidth:100, sort: true, filter: true, totalRow: true}
                            ,{field: 'jarName', title:'缸号', align:'center', minWidth:100, sort: true, filter: true}
                            ,{field: 'location', title:'位置', align:'center', minWidth:100, sort: true, filter: true}
                            ,{field: 'returnTime', title:'日期', align:'center', minWidth:100, sort: true, filter: true, templet: function (d) {
                                    return moment(d.returnTime).format("YYYY-MM-DD");
                                }}
                            ,{field: 'operateType', title:'操作类型', align:'center', minWidth:100, sort: true, filter: true}
                            ,{field: 'remark', title:'备注', align:'center', minWidth:100, sort: true, filter: true}

                        ]]
                        ,data: fabricDetailTmpList
                        ,loading:false
                        ,totalRow: true
                        ,page: true
                        ,even: true
                        ,overflow: "tips"
                        ,limits: [50, 100, 200]
                        ,limit: 100 //每页默认显示的数量
                        ,done: function (res, curr, count) {
                            soulTable.render(this);
                        }
                    });
                    table.render({
                        elem: '#outStoreTable'
                        ,height: 'full-150'
                        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                        ,cols: [[
                            {type: 'numbers', align:'center', title:'序号', minWidth:80}
                            ,{field: 'clothesVersionNumber', title:'单号', align:'center', minWidth:120, sort: true, filter: true, totalRowText: '合计'}
                            ,{field: 'orderName', title:'款号', align:'center', minWidth:120, sort: true, filter: true}
                            ,{field: 'fabricName', title:'面料名', align:'center', minWidth:120, sort: true, filter: true}
                            ,{field: 'fabricNumber', title:'面料号', align:'center', minWidth:120, sort: true, filter: true}
                            ,{field: 'supplier', title:'供应商', align:'center', minWidth:100, sort: true, filter: true}
                            ,{field: 'colorName', title:'色组', align:'center', minWidth:100, sort: true, filter: true}
                            ,{field: 'fabricColor', title:'面料颜色', align:'center', minWidth:120, sort: true, filter: true}
                            ,{field: 'unit', title:'单位', align:'center', minWidth:90, sort: true, filter: true}
                            ,{field: 'weight', title:'数量', align:'center', minWidth:100, sort: true, filter: true, totalRow: true}
                            ,{field: 'batchNumber', title:'卷数', align:'center', minWidth:100, sort: true, filter: true, totalRow: true}
                            ,{field: 'jarName', title:'缸号', align:'center', minWidth:100, sort: true, filter: true}
                            ,{field: 'location', title:'位置', align:'center', minWidth:100, sort: true, filter: true}
                            ,{field: 'returnTime', title:'日期', align:'center', minWidth:100, sort: true, filter: true, templet: function (d) {
                                    return moment(d.returnTime).format("YYYY-MM-DD");
                                }}
                            ,{field: 'operateType', title:'操作类型', align:'center', minWidth:100, sort: true, filter: true}
                            ,{field: 'receiver', title:'接收', align:'center', minWidth:100, sort: true, filter: true}
                            ,{field: 'remark', title:'备注', align:'center', minWidth:100, sort: true, filter: true}

                        ]]
                        ,data: fabricOutRecordList
                        ,loading:false
                        ,totalRow: true
                        ,page: true
                        ,even: true
                        ,overflow: "tips"
                        ,limits: [50, 100, 200]
                        ,limit: 100 //每页默认显示的数量
                        ,done: function (res, curr, count) {
                            soulTable.render(this);
                        }
                    });
                }, error: function () {
                    layer.msg("出错了~~~");
                }
            })
        }
        else if (type === "辅料"){
            $.ajax({
                url: "/erp/searchaccessoryleakbyorder",
                type:'GET',
                data: param,
                success: function (data) {
                    layer.close(load);
                    var summaryData = data.plan;
                    $.each(summaryData, function(index,element){
                        element.diffCount = element.accessoryReturnCount - element.accessoryPlanCount;
                        element.wellCountAccessory = toDecimalFour(element.wellCount * element.pieceUsage);
                        element.wellCountDiff = toDecimalFour(element.accessoryReturnCount - element.wellCountAccessory);
                    });
                    var inStoreData = data.inStore;
                    var outData = data.out;
                    var storageData = data.storage;
                    table.render({
                        elem: '#summaryTable'
                        ,height: 'full-150'
                        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                        ,cols: [[
                            {type: 'numbers', align:'center', title:'序号', minWidth:80}
                            ,{field: 'clothesVersionNumber', title:'单号', align:'center', minWidth:120, sort: true, filter: true, totalRowText: '合计'}
                            ,{field: 'orderName', title:'款号', align:'center', minWidth:120, sort: true, filter: true}
                            ,{field: 'accessoryName', title:'名称', align:'center', minWidth:120, sort: true, filter: true}
                            ,{field: 'accessoryNumber', title:'编号', align:'center', minWidth:120, sort: true, filter: true}
                            ,{field: 'specification', title:'规格', align:'center', minWidth:100, sort: true, filter: true}
                            ,{field: 'accessoryColor', title:'颜色', align:'center', minWidth:100, sort: true, filter: true}
                            ,{field: 'accessoryUnit', title:'单位', align:'center', minWidth:90, sort: true, filter: true}
                            ,{field: 'supplier', title:'供应商', align:'center', minWidth:100, sort: true, filter: true}
                            ,{field: 'colorName', title:'色组', align:'center', minWidth:100, sort: true, filter: true}
                            ,{field: 'sizeName', title:'尺码', align:'center', minWidth:100, sort: true, filter: true}
                            ,{field: 'accessoryCount', title:'订购量', align:'center', minWidth:90, sort: true, filter: true, totalRow: true, templet: function (d) {
                                    return toDecimalFour(d.accessoryCount);
                                }}
                            ,{field: 'accessoryReturnCount', title:'入库量', align:'center', minWidth:90, sort: true, filter: true, totalRow: true, templet: function (d) {
                                    return toDecimalFour(d.accessoryReturnCount);
                                }}
                            ,{field: 'orderCount', title:'订单量', align:'center', minWidth:90, sort: true, filter: true}
                            ,{field: 'pieceUsage', title:'单耗', align:'center', minWidth:90, sort: true, filter: true, templet: function (d) {
                                    return toDecimalFour(d.pieceUsage);
                                }}
                            ,{field: 'accessoryPlanCount', title:'订需', align:'center', minWidth:90, sort: true, filter: true, totalRow: true, templet: function (d) {
                                    return toDecimalFour(d.accessoryPlanCount);
                                }}
                            ,{field: 'diffCount', title:'订需差', align:'center', minWidth:90, sort: true, filter: true, totalRow: true, templet: function (d) {
                                    if (d.diffCount < 0){
                                        return "<p style='color: red'>" + toDecimalFour(d.diffCount) + "</p>";
                                    } else {
                                        return "<p style='color: green'>" + toDecimalFour(d.diffCount) + "</p>";
                                    }
                                }}
                            ,{field: 'wellCount', title:'裁数', align:'center', minWidth:90, sort: true, filter: true, totalRow: true}
                            ,{field: 'wellCountAccessory', title:'裁需', align:'center', minWidth:90, sort: true, filter: true, totalRow: true}
                            ,{field: 'wellCountDiff', title:'裁需差', align:'center', minWidth:90, sort: true, filter: true, totalRow: true, templet: function (d) {
                                    if (d.wellCountDiff < 0){
                                        return "<p style='color: red'>" + toDecimalFour(d.wellCountDiff) + "</p>";
                                    } else {
                                        return "<p style='color: green'>" + toDecimalFour(d.wellCountDiff) + "</p>";
                                    }
                                }}
                        ]]
                        ,data: summaryData
                        ,loading:false
                        ,totalRow: true
                        ,page: true
                        ,even: true
                        ,overflow: "tips"
                        ,limits: [50, 100, 200]
                        ,limit: 100 //每页默认显示的数量
                        ,done: function (res, curr, count) {
                            soulTable.render(this);
                        }
                    });
                    table.render({
                        elem: '#inStoreTable'
                        ,height: 'full-150'
                        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                        ,cols: [[
                            {type: 'numbers', align:'center', title:'序号', minWidth:80}
                            ,{field: 'clothesVersionNumber', title:'单号', align:'center', minWidth:120, sort: true, filter: true, totalRowText: '合计'}
                            ,{field: 'orderName', title:'款号', align:'center', minWidth:120, sort: true, filter: true}
                            ,{field: 'accessoryName', title:'名称', align:'center', minWidth:120, sort: true, filter: true}
                            ,{field: 'accessoryNumber', title:'编号', align:'center', minWidth:120, sort: true, filter: true}
                            ,{field: 'specification', title:'规格', align:'center', minWidth:100, sort: true, filter: true}
                            ,{field: 'accessoryColor', title:'颜色', align:'center', minWidth:100, sort: true, filter: true}
                            ,{field: 'accessoryUnit', title:'单位', align:'center', minWidth:90, sort: true, filter: true}
                            ,{field: 'supplier', title:'供应商', align:'center', minWidth:100, sort: true, filter: true}
                            ,{field: 'colorName', title:'色组', align:'center', minWidth:100, sort: true, filter: true}
                            ,{field: 'sizeName', title:'尺码', align:'center', minWidth:100, sort: true, filter: true}
                            ,{field: 'orderCount', title:'订单量', align:'center', minWidth:90, sort: true, filter: true}
                            ,{field: 'wellCount', title:'好片数', align:'center', minWidth:90, sort: true, filter: true}
                            ,{field: 'inStoreCount', title:'入库量', align:'center', minWidth:90, sort: true, filter: true, totalRow: true, templet: function (d) {
                                    return toDecimalFour(d.inStoreCount);
                                }}
                            ,{field: 'createTime', title:'日期', align:'center', minWidth:90, sort: true, filter: true, templet: function (d) {
                                    return moment(d.createTime).format("YYYY-MM-DD");
                                }}
                            ,{field: 'accessoryLocation', title:'位置', align:'center', minWidth:90, sort: true, filter: true}
                            ,{field: 'colorNumber', title:'色号', align:'center', minWidth:90, sort: true, filter: true}
                        ]]
                        ,data: inStoreData
                        ,loading:false
                        ,totalRow: true
                        ,page: true
                        ,even: true
                        ,overflow: "tips"
                        ,limits: [50, 100, 200]
                        ,limit: 100 //每页默认显示的数量
                        ,done: function (res, curr, count) {
                            soulTable.render(this);
                        }
                    });
                    table.render({
                        elem: '#storageTable'
                        ,height: 'full-150'
                        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                        ,cols: [[
                            {type: 'numbers', align:'center', title:'序号', minWidth:80}
                            ,{field: 'clothesVersionNumber', title:'单号', align:'center', minWidth:120, sort: true, filter: true, totalRowText: '合计'}
                            ,{field: 'orderName', title:'款号', align:'center', minWidth:120, sort: true, filter: true}
                            ,{field: 'accessoryName', title:'名称', align:'center', minWidth:120, sort: true, filter: true}
                            ,{field: 'accessoryNumber', title:'编号', align:'center', minWidth:120, sort: true, filter: true}
                            ,{field: 'specification', title:'规格', align:'center', minWidth:100, sort: true, filter: true}
                            ,{field: 'accessoryColor', title:'颜色', align:'center', minWidth:100, sort: true, filter: true}
                            ,{field: 'accessoryUnit', title:'单位', align:'center', minWidth:90, sort: true, filter: true}
                            ,{field: 'supplier', title:'供应商', align:'center', minWidth:100, sort: true, filter: true}
                            ,{field: 'colorName', title:'色组', align:'center', minWidth:100, sort: true, filter: true}
                            ,{field: 'sizeName', title:'尺码', align:'center', minWidth:100, sort: true, filter: true}
                            ,{field: 'orderCount', title:'订单量', align:'center', minWidth:90, sort: true, filter: true}
                            ,{field: 'wellCount', title:'好片数', align:'center', minWidth:90, sort: true, filter: true}
                            ,{field: 'storageCount', title:'库存量', align:'center', minWidth:90, sort: true, filter: true, totalRow: true, templet: function (d) {
                                    return toDecimalFour(d.storageCount);
                                }}
                            ,{field: 'updateTime', title:'最后更新', align:'center', minWidth:120, sort: true, filter: true, templet: function (d) {
                                    return moment(d.updateTime).format("YYYY-MM-DD");
                                }}
                            ,{field: 'accessoryLocation', title:'位置', align:'center', minWidth:90, sort: true, filter: true}
                        ]]
                        ,data: storageData
                        ,loading:false
                        ,totalRow: true
                        ,page: true
                        ,even: true
                        ,overflow: "tips"
                        ,limits: [50, 100, 200]
                        ,limit: 100 //每页默认显示的数量
                        ,done: function (res, curr, count) {
                            soulTable.render(this);
                        }
                    });
                    table.render({
                        elem: '#tmpStorageTable'
                        ,height: 'full-150'
                        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                        ,cols: [[]]
                        ,data: []
                        ,loading:false
                        ,totalRow: true
                        ,page: true
                        ,even: true
                        ,overflow: "tips"
                        ,limits: [50, 100, 200]
                        ,limit: 100 //每页默认显示的数量
                        ,done: function (res, curr, count) {
                            soulTable.render(this);
                        }
                    });
                    table.render({
                        elem: '#outStoreTable'
                        ,height: 'full-150'
                        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                        ,cols: [[
                            {type: 'numbers', align:'center', title:'序号', minWidth:80}
                            ,{field: 'clothesVersionNumber', title:'单号', align:'center', minWidth:120, sort: true, filter: true, totalRowText: '合计'}
                            ,{field: 'orderName', title:'款号', align:'center', minWidth:120, sort: true, filter: true}
                            ,{field: 'accessoryName', title:'名称', align:'center', minWidth:120, sort: true, filter: true}
                            ,{field: 'accessoryNumber', title:'编号', align:'center', minWidth:120, sort: true, filter: true}
                            ,{field: 'specification', title:'规格', align:'center', minWidth:100, sort: true, filter: true}
                            ,{field: 'accessoryColor', title:'颜色', align:'center', minWidth:100, sort: true, filter: true}
                            ,{field: 'accessoryUnit', title:'单位', align:'center', minWidth:90, sort: true, filter: true}
                            ,{field: 'supplier', title:'供应商', align:'center', minWidth:100, sort: true, filter: true}
                            ,{field: 'colorName', title:'色组', align:'center', minWidth:100, sort: true, filter: true}
                            ,{field: 'sizeName', title:'尺码', align:'center', minWidth:100, sort: true, filter: true}
                            ,{field: 'orderCount', title:'订单量', align:'center', minWidth:90, sort: true, filter: true}
                            ,{field: 'wellCount', title:'好片数', align:'center', minWidth:90, sort: true, filter: true}
                            ,{field: 'outStoreCount', title:'出库量', align:'center', minWidth:90, sort: true, filter: true, totalRow: true, templet: function (d) {
                                    return toDecimalFour(d.outStoreCount);
                                }}
                            ,{field: 'createTime', title:'日期', align:'center', minWidth:120, sort: true, filter: true, templet: function (d) {
                                    return moment(d.createTime).format("YYYY-MM-DD");
                                }}
                            ,{field: 'accessoryLocation', title:'位置', align:'center', minWidth:90, sort: true, filter: true}
                        ]]
                        ,data: outData
                        ,loading:false
                        ,totalRow: true
                        ,page: true
                        ,even: true
                        ,overflow: "tips"
                        ,limits: [50, 100, 200]
                        ,limit: 100 //每页默认显示的数量
                        ,done: function (res, curr, count) {
                            soulTable.render(this);
                        }
                    });
                }, error: function () {
                    layer.msg("出错了~~~");
                }
            });
        }
    }
    form.on('submit(searchBeat)', function(data){
        var orderName = $("#orderName").val();
        var colorName = $("#colorName").val();
        var type = $("#type").val();
        initTable(orderName, colorName, type);
        return false;
    });
    table.on('toolbar(summaryTable)', function(obj) {
        if (obj.event === 'clearFilter') {
            // 清除所有筛选条件并重载表格
            // 参数: tableId
            soulTable.clearFilter('summaryTable')
        } else if (obj.event === 'exportExcel') {
            soulTable.export('summaryTable');
        }
    });
    table.on('toolbar(inStoreTable)', function(obj) {
        if (obj.event === 'clearFilter') {
            // 清除所有筛选条件并重载表格
            // 参数: tableId
            soulTable.clearFilter('inStoreTable')
        } else if (obj.event === 'exportExcel') {
            soulTable.export('inStoreTable');
        }
    });
    table.on('toolbar(storageTable)', function(obj) {
        if (obj.event === 'clearFilter') {
            // 清除所有筛选条件并重载表格
            // 参数: tableId
            soulTable.clearFilter('storageTable')
        } else if (obj.event === 'exportExcel') {
            soulTable.export('storageTable');
        }
    });
    table.on('toolbar(tmpStorageTable)', function(obj) {
        if (obj.event === 'clearFilter') {
            // 清除所有筛选条件并重载表格
            // 参数: tableId
            soulTable.clearFilter('tmpStorageTable')
        } else if (obj.event === 'exportExcel') {
            soulTable.export('tmpStorageTable');
        }
    });
    table.on('toolbar(outStoreTable)', function(obj) {
        if (obj.event === 'clearFilter') {
            // 清除所有筛选条件并重载表格
            // 参数: tableId
            soulTable.clearFilter('outStoreTable')
        } else if (obj.event === 'exportExcel') {
            soulTable.export('outStoreTable');
        }
    });
});
function toDecimal(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*100)/100;
    return f;
}
function toDecimalFour(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*10000)/10000;
    return f;
}
function isNumber(val){

    var regPos = /^\d+(\.\d+)?$/; //非负浮点数
    var regNeg = /^(-(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*)))$/; //负浮点数
    if (regPos.test(val) || regNeg.test(val)){
        return true;
    }else{
        return false;
    }

}
