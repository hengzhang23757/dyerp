layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.6'
}).extend({                         // 模块别名
    soulTable: 'soulTable'
});
var dataTable;
var userRole = $("#userRole").val();
layui.use(['form', 'soulTable', 'table', 'upload','yutons_sug'], function () {
    var table = layui.table,
        form = layui.form,
        soulTable = layui.soulTable;
    dataTable = table.render({
        elem: '#dataTable'
        ,cols: [[]]
        ,loading:true
        ,data:[]
        ,height: 'full-80'
        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
        ,defaultToolbar: ['filter', 'print']
        ,title: '款式管理'
        ,totalRow: true
        ,page: true
        ,even: true
        ,overflow: 'tips'
        ,limits: [50, 100, 200]
        ,limit: 100 //每页默认显示的数量
        ,done: function (res, curr, count) {
            soulTable.render(this);
        }
    });

    initTable();

    function initTable(){
        var load = layer.load();
        $.ajax({
            url: "/erp/getallstylemanage",
            type: 'GET',
            data: {},
            success: function (res) {
                if (res.data) {
                    var reportData = res.data;
                    table.render({
                        elem: '#dataTable'
                        ,cols:[[
                            {type:'numbers', align:'center', title:'序号', width:'10%'}
                            ,{field:'styleCategory', title:'类别', align:'center', width:'15%', sort: true, filter: true}
                            ,{field:'categoryCode', title:'类别代码', align:'center', width:'15%', sort: true, filter: true}
                            ,{field:'styleName', title:'款式名称', align:'center', width:'20%', sort: true, filter: true}
                            ,{field:'styleNumber', title:'款式代码', align:'center', width:'10%', sort: true, filter: true}
                            ,{field:'styleDescription', title:'款式描述', align:'center', width:'30%', sort: true, filter: true}
                            ,{field:'id', fixed: 'right', title:'操作', align:'center', toolbar: '#barTop', width:'15%'}
                        ]]
                        ,data:reportData
                        ,height: 'full-80'
                        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                        ,title: '款式'
                        ,totalRow: true
                        ,page: true
                        ,even: true
                        ,overflow: 'tips'
                        ,limits: [50, 100, 200]
                        ,limit: 100 //每页默认显示的数量
                        ,done: function (res, curr, count) {
                            soulTable.render(this);
                            layer.close(load);
                        }
                    });
                }
            }
        })
    }


    table.on('toolbar(dataTable)', function(obj) {
        if (obj.event === 'refresh') {
            initTable();
        } else if (obj.event === 'add') {
            var index = layer.open({
                type: 1 //Page层类型
                , title: '添加'
                , btn: ['添加','取消']
                , shade: 0.6 //遮罩透明度
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , area: ['600px', '300px']
                , content: $("#operateDiv")
                , yes: function (index, layero) {
                    // 通过form批量取值
                    var param = form.val("formBaseInfo");
                    if (param.styleCategory == null || param.styleCategory == "" || param.categoryCode == null || param.categoryCode == "" || param.styleName == null || param.styleName == "" || param.styleNumber == null || param.styleNumber == ""){
                        layer.msg("请填写完整", { icon: 2 });
                        return false;
                    }
                    $.ajax({
                        url: "/erp/addstylemanage",
                        type: 'POST',
                        data: {
                            styleManageJson: JSON.stringify(param)
                        },
                        success: function (res) {
                            if (res.result == 0){
                                layer.closeAll();
                                $("#operateDiv").find("input").val("");
                                initTable();
                                layer.msg("录入成功！", {icon: 1});
                            }
                        }, error: function () {
                            layer.msg("添加失败！", {icon: 2});
                        }
                    })
                }
                , cancel: function (i, layero) {
                    $("#operateDiv").find("input").val("");
                    layer.closeAll();
                }
            });
            form.render('select');
            return false;
        }
    });

    //监听行工具事件
    table.on('tool(dataTable)', function(obj){
        var data = obj.data;
        if(obj.event === 'del'){
            layer.confirm('真的删除吗', function(index){
                $.ajax({
                    url: "/erp/deletestylemanage",
                    type: 'POST',
                    data: {
                        id: data.id
                    },
                    success: function (res) {
                        if (res.result == 0) {
                            initTable();
                            layer.close(index);
                            layer.msg("删除成功！", {icon: 1});
                        } else {
                            layer.msg("删除失败！", {icon: 2});
                        }
                    },
                    error: function () {
                        layer.msg("删除失败！", {icon: 2});
                    }
                })

            });
        } else if (obj.event === 'update'){
            var index = layer.open({
                type: 1 //Page层类型
                , title: '修改尺码'
                , btn: ['保存','取消']
                , shade: 0.6 //遮罩透明度
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , area: ['600px', '300px']
                , content: $("#operateDiv")
                , yes: function (index, layero) {
                    var param = form.val("formBaseInfo");
                    param.id = data.id;
                    if (param.styleName == null || param.styleName == "" || param.styleNumber == null || param.styleNumber == ""){
                        layer.msg("请填写完整", { icon: 2 });
                        return false;
                    }
                    $.ajax({
                        url: "/erp/updatestylemanage",
                        type: 'POST',
                        data: {
                            styleManageJson: JSON.stringify(param)
                        },
                        success: function (res) {
                            if (res.result == 0){
                                layer.closeAll();
                                $("#operateDiv").find("input").val("");
                                initTable();
                                layer.msg("录入成功！", {icon: 1});
                            }
                        }, error: function () {
                            layer.msg("添加失败！", {icon: 2});
                        }
                    })
                }
                , cancel: function (i, layero) {
                    $("#operateDiv").find("input").val("");
                    layer.closeAll();
                }
            });
            // 通过form批量赋值
            form.val("formBaseInfo", { //formTest 即 class="layui-form" 所在元素属性 lay-filter="" 对应的值
                "styleName": data.styleName // "name": "value"
                ,"styleCategory": data.styleCategory
                ,"categoryCode": data.categoryCode
                ,"styleNumber": data.styleNumber
                ,"styleDescription": data.styleDescription
            });
        } else if (obj.event === 'process'){
            var styleName = data.styleName;
            var styleCategory = data.styleCategory;
            var label = "<span style='font-size: larger; font-weight: bolder'>工艺模板编辑&nbsp;&nbsp;款名:"+ styleName + "&nbsp;&nbsp;类别:" + styleCategory + "</span>";
            var index = layer.open({
                type: 1 //Page层类型
                , title: label
                , btn: ['保存']
                , shade: 0.6 //遮罩透明度
                , area: '1000px'
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , content: "<div id=\"styleManageProcess\">\n" +
                    "        <form class=\"layui-form\" style=\"margin: 0 auto;max-width:90%;padding-top: 10px;\">\n" +
                    "            <div class=\"layui-row layui-col-space10\">\n" +
                    "                <div class=\"layui-col-md12\" id=\"processOperate\">\n" +
                    "                    <div class=\"layui-tab layui-tab-card\" style=\"width: 90%; \">\n" +
                    "                        <textarea id=\"processTextarea\"></textarea>\n" +
                    "                    </div>\n" +
                    "                </div>\n" +
                    "            </div>\n" +
                    "        </form>\n" +
                    "    </div>"
                ,yes: function(i, layero){
                    $.ajax({
                        url: "/erp/updateprocessinstylemanage",
                        type: 'POST',
                        data: {
                            id: data.id,
                            processText: tinymce.activeEditor.getContent()
                        },
                        success: function (res) {
                            if (res.result == 0) {
                                tinymce.activeEditor.setContent("");
                                tinymce.remove('#processTextarea');
                                layer.msg("录入成功！", {icon: 1});
                                layer.close(index);
                            } else {
                                layer.msg("录入失败！", {icon: 2});
                            }
                        },
                        error: function () {
                            layer.msg("录入失败！", {icon: 2});
                        }
                    });
                    return false;
                }
                , cancel : function (i,layero) {
                    tinymce.activeEditor.setContent("");
                    tinymce.remove('#processTextarea');
                }
            });
            layer.full(index);
            setTimeout(function () {
                $.ajax({
                    url: "/erp/getstylemanagebyid",
                    type: 'GET',
                    data: {
                        id: data.id
                    },
                    success: function (res) {
                        tinymce.init({
                            selector: '#processTextarea',
                            language: 'zh_CN',//中文
                            height: "400px",
                            width: "100%",
                            placeholder: '工艺要求模板,一次填写后续直接引用~~~', // 占位符
                            directionality: 'ltr',  //文字方向,ltr文字方向从左到右，rtl从右到左
                            browser_spellcheck: true,
                            contextmenu: false,
                            branding: false, //技术支持(Powered by Tiny || 由Tiny驱动)
                            menubar: false, //菜单栏
                            plugins: [
                                "print image autoresize table powerpaste"
                            ],
                            powerpaste_word_import: 'propmt',// 参数可以是propmt, merge, clear，效果自行切换对比
                            powerpaste_html_import: 'propmt',// propmt, merge, clear
                            powerpaste_allow_local_images: true,
                            paste_data_images: true,
                            relative_urls : false,
                            remove_script_host : false,
                            convert_urls : true,
                            toolbar: "print | undo redo | bold | alignleft aligncenter alignright alignjustify | fontselect | fontsizeselect | image table",
                            images_upload_handler: function(blobInfo, success, failure){
                                handleImgUpload(blobInfo, success, failure)
                            }
                        });
                        if (res.data){
                            setTimeout(function () {
                                tinymce.activeEditor.setContent(res.data.processText);
                            }, 100);
                        }
                    }, error: function () {
                        tinymce.init({
                            selector: '#processTextarea',
                            language: 'zh_CN',//中文
                            height: "400px",
                            width: "100%",
                            directionality: 'ltr',  //文字方向,ltr文字方向从左到右，rtl从右到左
                            browser_spellcheck: true,
                            contextmenu: false,
                            branding: false, //技术支持(Powered by Tiny || 由Tiny驱动)
                            menubar: false, //菜单栏
                            plugins: [
                                "print image autoresize table powerpaste"
                            ],
                            powerpaste_word_import: 'propmt',// 参数可以是propmt, merge, clear，效果自行切换对比
                            powerpaste_html_import: 'propmt',// propmt, merge, clear
                            powerpaste_allow_local_images: true,
                            paste_data_images: true,
                            relative_urls : false,
                            remove_script_host : false,
                            convert_urls : true,
                            toolbar: "print | undo redo | bold | alignleft aligncenter alignright alignjustify | fontselect | fontsizeselect | image table",
                            images_upload_handler: function(blobInfo, success, failure){
                                handleImgUpload(blobInfo, success, failure)
                            }
                        });
                        layer.msg("获取失败");
                    }
                });
            },100);
        } else if (obj.event === 'size'){
            var styleName = data.styleName;
            var styleCategory = data.styleCategory;
            var label = "<span style='font-size: larger; font-weight: bolder'>尺寸模板编辑&nbsp;&nbsp;款名:"+ styleName + "&nbsp;&nbsp;类别:" + styleCategory + "</span>";
            var index = layer.open({
                type: 1 //Page层类型
                , title: label
                , btn: ['保存']
                , shade: 0.6 //遮罩透明度
                , area: '1000px'
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , content: "<div id=\"styleManageSize\">\n" +
                    "        <form class=\"layui-form\" style=\"margin: 0 auto;max-width:90%;padding-top: 10px;\">\n" +
                    "            <div class=\"layui-row layui-col-space10\">\n" +
                    "                <div class=\"layui-col-md12\" id=\"sizeOperate\">\n" +
                    "                    <div class=\"layui-tab layui-tab-card\" style=\"width: 90%; \">\n" +
                    "                        <textarea id=\"sizeTextarea\"></textarea>\n" +
                    "                    </div>\n" +
                    "                </div>\n" +
                    "            </div>\n" +
                    "        </form>\n" +
                    "    </div>"
                ,yes: function(i, layero){
                    $.ajax({
                        url: "/erp/updatesizeinstylemanage",
                        type: 'POST',
                        data: {
                            id: data.id,
                            sizeText: tinymce.activeEditor.getContent()
                        },
                        success: function (res) {
                            if (res.result == 0) {
                                tinymce.activeEditor.setContent("");
                                tinymce.remove('#sizeTextarea');
                                layer.msg("录入成功！", {icon: 1});
                                layer.close(index);
                            } else {
                                layer.msg("录入失败！", {icon: 2});
                            }
                        },
                        error: function () {
                            layer.msg("录入失败！", {icon: 2});
                        }
                    });
                    return false;
                }
                , cancel : function (i,layero) {
                    tinymce.activeEditor.setContent("");
                    tinymce.remove('#sizeTextarea');
                }
            });
            layer.full(index);
            setTimeout(function () {
                $.ajax({
                    url: "/erp/getstylemanagebyid",
                    type: 'GET',
                    data: {
                        id: data.id
                    },
                    success: function (res) {
                        tinymce.init({
                            selector: '#sizeTextarea',
                            language: 'zh_CN',//中文
                            height: "400px",
                            width: "100%",
                            placeholder: '尺寸模板,一次填写后续直接引用~~~', // 占位符
                            directionality: 'ltr',  //文字方向,ltr文字方向从左到右，rtl从右到左
                            browser_spellcheck: true,
                            contextmenu: false,
                            branding: false, //技术支持(Powered by Tiny || 由Tiny驱动)
                            menubar: false, //菜单栏
                            plugins: [
                                "print image autoresize table powerpaste"
                            ],
                            powerpaste_word_import: 'propmt',// 参数可以是propmt, merge, clear，效果自行切换对比
                            powerpaste_html_import: 'propmt',// propmt, merge, clear
                            powerpaste_allow_local_images: true,
                            paste_data_images: true,
                            relative_urls : false,
                            remove_script_host : false,
                            convert_urls : true,
                            toolbar: "print | undo redo | bold | alignleft aligncenter alignright alignjustify | fontselect | fontsizeselect | image table",
                            images_upload_handler: function(blobInfo, success, failure){
                                handleImgUpload(blobInfo, success, failure)
                            }
                        });
                        if (res.data){
                            setTimeout(function () {
                                tinymce.activeEditor.setContent(res.data.sizeText);
                            }, 100);
                        }
                    }, error: function () {
                        tinymce.init({
                            selector: '#sizeTextarea',
                            language: 'zh_CN',//中文
                            height: "400px",
                            width: "100%",
                            directionality: 'ltr',  //文字方向,ltr文字方向从左到右，rtl从右到左
                            browser_spellcheck: true,
                            contextmenu: false,
                            branding: false, //技术支持(Powered by Tiny || 由Tiny驱动)
                            menubar: false, //菜单栏
                            plugins: [
                                "print image autoresize table powerpaste"
                            ],
                            powerpaste_word_import: 'propmt',// 参数可以是propmt, merge, clear，效果自行切换对比
                            powerpaste_html_import: 'propmt',// propmt, merge, clear
                            powerpaste_allow_local_images: true,
                            paste_data_images: true,
                            relative_urls : false,
                            remove_script_host : false,
                            convert_urls : true,
                            toolbar: "print | undo redo | bold | alignleft aligncenter alignright alignjustify | fontselect | fontsizeselect | image table",
                            images_upload_handler: function(blobInfo, success, failure){
                                handleImgUpload(blobInfo, success, failure)
                            }
                        });
                        layer.msg("获取失败");
                    }
                });
            },100);
        }
    })

});

function handleImgUpload(blobInfo, success, failure){
    var formdata = new FormData();
    // append 方法中的第一个参数就是 我们要上传文件 在后台接收的文件名
    // 这个值要根据后台来定义
    // 第二个参数是我们上传的文件
    formdata.append('file', blobInfo.blob());
    $.ajax({
        url: "/erp/wangEditorUploadImg",
        type: 'post',
        dataType:'json',
        contentType:false,//ajax上传图片需要添加
        processData:false,//ajax上传图片需要添加
        data: formdata,
        success: function (res) {
            if(res.data) {
                console.log(res)
                success(res.data[0]);
            }else {
                failure("上传图片失败");
            }
        },
        error: function () {
            layer.msg("上传图片失败", { icon: 2 });
        }
    });

}