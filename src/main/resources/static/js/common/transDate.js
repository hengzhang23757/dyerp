function globalTransDate(data, type, full, meta) {
    if (data == null){
        return "";
    }else {
        return  moment(data).format("YYYY-MM-DD");
    }
}

function globalTransTime(data, type, full, meta) {
    if (data == null){
        return "";
    }else {
        return  moment(data).format("MM-DD HH:mm");
    }
}