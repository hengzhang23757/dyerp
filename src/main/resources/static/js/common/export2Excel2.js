let idTmr;
function getExplorer () {
    var explorer = window.navigator.userAgent;
    //ie
    if (explorer.indexOf("Trident") >= 0) {
        return 'ie';
    }
    //firefox

    else if (explorer.indexOf("Firefox") >= 0) {
        return 'Firefox';
    }
    //Chrome
    else if (explorer.indexOf("Chrome") >= 0) {
        return 'Chrome';
    }
    //Opera
    else if (explorer.indexOf("Opera") >= 0) {
        return 'Opera';
    }
    //Safari
    else if (explorer.indexOf("Safari") >= 0) {
        return 'Safari';
    }
}
// 判断浏览器是否为IE
function exportToExcel (data,name){

    // 判断是否为IE
    if (getExplorer() == 'ie') {
        tableToIE(data, name)
    } else {
        tableToNotIE(data,name)
    }
}

function Cleanup (){
    window.clearInterval(idTmr);
}

// ie浏览器下执行
function tableToIE (data, name) {
    var curTbl = data;
    var oXL;
    try{
        oXL = new ActiveXObject("Excel.Application"); //创建AX对象excel
    }catch(e){
        alert("无法启动Excel!\n\n如果您确信您的电脑中已经安装了Excel，"+"那么请调整IE的安全级别。\n\n具体操作：\n\n"+"工具 → Internet选项 → 安全 → 自定义级别 → 对未标记为可安全执行脚本的ActiveX初始化并脚本运行 → 启用");
        return false;
    }

    //创建AX对象excel
    var oWB = oXL.Workbooks.Add();
    //获取workbook对象
    var xlsheet = oWB.Worksheets(1);
    //激活当前sheet
    var sel = document.body.createTextRange();
    // sel.moveToElementText('<table>'+curTbl+'</table>');
    //把表格中的内容移到TextRange中
    // sel.select;
    //全选TextRange中内容
    // sel.execCommand("Copy");
    //复制TextRange中内容
    window.clipboardData.setData('Text', '<table>'+curTbl+'</table>');
    xlsheet.Paste;
    //粘贴到活动的EXCEL中

    oXL.Visible = true;
    //设置excel可见属性

    try {
        var nameArr = name.split(/[ .]/);
        if(nameArr.length == 3) {
            name = nameArr[0]+".xls"
        }
        var fname = oXL.Application.GetSaveAsFilename(name, "Excel Spreadsheets (*.xls), *.xls");
    } catch (e) {
        print("Nested catch caught " + e);
    } finally {
        oWB.SaveAs(fname);

        oWB.Close(savechanges = false);
        //xls.visible = false;
        oXL.Quit();
        oXL = null;
        // 结束excel进程，退出完成
        window.setInterval("Cleanup();", 1);
        idTmr = window.setInterval("Cleanup();", 1);
    }
}

function base64(s) {
    return window.btoa(unescape(encodeURIComponent(s)));
}

// 非ie浏览器下执行
function tableToNotIE(table, name){
    // 编码要用utf-8不然默认gbk会出现中文乱码
    var uri = 'data:application/vnd.ms-excel;base64,',
        template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><meta charset="UTF-8"><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>'+name+'</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>'+table+'</table></body></html>';

    var link = document.createElement('a');
    link.setAttribute('href', uri + base64(template));
    link.setAttribute('download', name);
    // window.location.href = uri + base64(format(template, ctx))
    link.click();
}



// 导出函数
function export2Excel (theadData,num, tbodyData, dataname) {

    var re = /http/ // 字符串中包含http,则默认为图片地址
    var th_len = theadData.length // 表头的长度
    var tb_len = tbodyData.length // 记录条数
    var width = 40 // 设置图片大小
    var height = 60

    // 添加表头信息
    var thead = '<thead><tr>'
    for (var i = 0; i < th_len; i++) {
        thead += '<th colspan='+num+'>' + theadData[i] + '</th>'
    }
    thead += '</tr></thead>'

    // 添加每一行数据
    var tbody = '<tbody>'
    for (var i = 0; i < tb_len; i++) {
        tbody += '<tr>'
        var row = tbodyData[i] // 获取每一行数据

        for (var key in row) {
            if (re.test(row[key])) { // 如果为图片，则需要加div包住图片
                //
                tbody += '<td style="width:' + width + 'px; height:' + height + 'px; text-align: center; vertical-align: middle"><div style="display:inline"><img src=\'' + row[key] + '\' ' + ' ' + 'width=' + '\"' + width + '\"' + ' ' + 'height=' + '\"' + height + '\"' + '></div></td>'
            } else {
                tbody += '<td style="text-align:center">' + row[key] + '</td>'
            }
        }
        tbody += '</tr>'
    }
    tbody += '</tbody>'

    var table = thead + tbody

    // 导出表格
    exportToExcel(table, dataname)
}