var basePath=$("#basePath").val();
layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.21'
}).extend();
$(document).ready(function () {
    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumber", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });

    $("#orderName").bind('blur change',function(){
        var orderName = $("#orderName").val();
        $("#colorName").empty();
        $.ajax({
            url: "/erp/getordercolornamesbyorder",
            data: {"orderName": orderName},
            success:function(data){
                $("#colorName").empty();
                if (data.colorNameList) {
                    $("#colorName").empty();
                    $("#colorName").append("<option value=''>颜色</option>");
                    $.each(data.colorNameList, function(index,element){
                        $("#colorName").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });
        $("#sizeName").empty();
        $.ajax({
            url: "/erp/getordersizenamesbyorder",
            data: {"orderName": orderName},
            success:function(data){
                $("#sizeName").empty();
                if (data.sizeNameList) {
                    $("#sizeName").empty();
                    $("#sizeName").append("<option value=''>尺码</option>");
                    $.each(data.sizeNameList, function(index,element){
                        $("#sizeName").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });
    });

    $("#clothesVersionNumber").bind('blur change',function(){
        var orderName = $("#orderName").val();
        $("#colorName").empty();
        $.ajax({
            url: "/erp/getordercolornamesbyorder",
            data: {"orderName": orderName},
            success:function(data){
                $("#colorName").empty();
                if (data.colorNameList) {
                    $("#colorName").empty();
                    $("#colorName").append("<option value=''>颜色</option>");
                    $.each(data.colorNameList, function(index,element){
                        $("#colorName").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });
        $("#sizeName").empty();
        $.ajax({
            url: "/erp/getordersizenamesbyorder",
            data: {"orderName": orderName},
            success:function(data){
                $("#sizeName").empty();
                if (data.sizeNameList) {
                    $("#sizeName").empty();
                    $("#sizeName").append("<option value=''>尺码</option>");
                    $.each(data.sizeNameList, function(index,element){
                        $("#sizeName").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });
    });
});

function search() {

    if($("#orderName").val().trim()=="") {
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入要查询的订单号！</span>",html: true});
        return false;
    }
    $.blockUI({ css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        },
        message: "<h3>稍等一下... 奋力搜索中</h3>"
    });
    $.ajax({
        url: basePath + "erp/getfinishtailorbyordercolorsize",
        type:'GET',
        data: {
            orderName:$("#orderName").val(),
            colorName:$("#colorName").val(),
            sizeName:$("#sizeName").val(),
            partName:$("#partName").val()
        },
        success: function (data) {
            if(data) {
                // var json = JSON.parse(data);
                createTailorInfoTable(data.finishTailorList);
            }else {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，没有查到相关信息！</span>",html: true});
            }
            $.unblockUI();
        }, error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }
    })

}

var tailorInfoTable;

function createTailorInfoTable(data) {
    if (tailorInfoTable != undefined) {
        tailorInfoTable.clear(); //清空一下table
        tailorInfoTable.destroy(); //还原初始化了的datatable
    }
    tailorInfoTable = $('#tailorInfoTable').DataTable({
        "data":data,
        language : {
            processing : "载入中",//处理页面数据的时候的显示
            paginate : {//分页的样式文本内容。
                previous : "上一页",
                next : "下一页",
                first : "第一页",
                last : "最后一页"
            },
            search:"搜索：",
            zeroRecords : "没有内容",//table tbody内容为空时，tbody的内容。
            //下面三者构成了总体的左下角的内容。
            info : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//左下角的信息显示，大写的词为关键字。
            infoEmpty : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//筛选为空时左下角的显示。
            infoFiltered : ""//筛选之后的左下角筛选提示(另一个是分页信息显示，在上面的info中已经设置，所以可以不显示)，
        },
        pageLength : 50,// 每页显示10条数据
        pagingType : "full_numbers", // 分页样式：simple,simple_numbers,full,full_numbers，
        "paging": true,
        "info": true,
        searching:true,
        ordering:true,
        lengthChange:false,
        scrollX: 1500,
        fixedHeader: true,
        scrollY: $(document.body).height() - 250,
        scrollCollapse: true,
        scroller:       true,
        "columns": [
            {
                "data": null,
                "title":"<input type=\"checkbox\" onclick=\"checkAll(this)\">",
                "width":"20px",
                "defaultContent": "",
                "sClass": "text-center",
                "orderable":false,
                render: function (data, type, row, meta) {
                    return "<input type='checkbox'>";
                }
            },
            {
                "data": null,
                "title":"序号",
                "width":"30px",
                "defaultContent": "",
                "searchable":false,
                "sClass": "text-center",
                render: function (data, type, row, meta) {
                    var no = meta.settings._iDisplayStart + meta.row + 1;
                    return no;
                }
            },{
                "data": "orderName",
                "title":"订单号",
                "width":"120px",
                "defaultContent": "",
                "searchable":false,
                "sClass": "text-center"
            },{
                "data": "clothesVersionNumber",
                "title":"版单号",
                "width":"120px",
                "defaultContent": "",
                "searchable":false,
                "sClass": "text-center"
            }, {
                "data": "customerName",
                "title":"客户",
                "width":"60px",
                "defaultContent": "",
                "searchable":false,
                "sClass": "text-center"
            }, {
                "data": "bedNumber",
                "title":"床号",
                "width":"45px",
                "defaultContent": "",
                "searchable":false,
                "sClass": "text-center"
            }, {
                "data": "jarName",
                "title":"缸号",
                "width":"60px",
                "defaultContent": "",
                "searchable":false,
                "sClass": "text-center"
            }, {
                "data": "colorName",
                "title":"颜色",
                "width":"60px",
                "defaultContent": "",
                "searchable":false,
                "sClass": "text-center"
            }, {
                "data": "sizeName",
                "title":"尺码",
                "width":"45px",
                "defaultContent": "",
                "searchable":false,
                "sClass": "text-center"
            }, {
                "data": "partName",
                "title":"部位",
                "width":"80px",
                "defaultContent": "",
                "searchable":false,
                "sClass": "text-center"
            }, {
                "data": "layerCount",
                "title":"数量",
                "width":"45px",
                "defaultContent": "",
                "searchable":false,
                "sClass": "text-center"
            }, {
                "data": "packageNumber",
                "title":"扎号",
                "width":"60px",
                "defaultContent": "",
                "searchable":false,
                "sClass": "text-center"
            }, {
                "data": "loColor",
                "title":"LOT色",
                "width":"60px",
                "defaultContent": "",
                "searchable":false,
                "sClass": "text-center"
            }, {
                "data": "tailorQcodeID",
                "title":"二维码",
                "width":"90px",
                "defaultContent": "",
                "sClass": "text-center",
                "searchable":false,
                render: function (data, type, row, meta) {
                    return PrefixZero(data,9);
                }
            }, {
                "data": "printTimes",
                "title":"打印次数",
                "width":"60px",
                "defaultContent": "",
                "sClass": "text-center"
            }, {
                "data": "tailorID",
                "title":"操作",
                "width":"40px",
                "defaultContent": "",
                "sClass": "text-center",
            }
        ],
        "columnDefs" :
            [{
                "orderable" : false, // 禁用排序
                "targets" : [15], // 指定的列
                "data" : "tailorID",
                "width":"40px",
                "render" : function(data, type, full, meta) {
                    return "<a href='#' style='color:#3e8eea' onclick='updateTailor(this)' id='"+data+"'>改数</a>";
                }
            }],
    });
}


function checkAll(obj) {
    if($(obj).is(':checked')) {
        $("#tailorInfoTable tbody input[type='checkbox']").prop("checked",true);
    }else {
        $("#tailorInfoTable tbody input[type='checkbox']").prop("checked",false);
    };
}

var LODOP;
function printer(type) {
    var tailorList = [];
    $("#tailorInfoTable tbody input[type='checkbox']:checked").each(function () {
        var tailor = {};
        tailor.orderName = $(this).parent().parent().find("td").eq(2).text();
        tailor.clothesVersionNumber = $(this).parent().parent().find("td").eq(3).text();
        tailor.customerName = $(this).parent().parent().find("td").eq(4).text();
        tailor.bedNumber = $(this).parent().parent().find("td").eq(5).text();
        tailor.jarName = $(this).parent().parent().find("td").eq(6).text();
        tailor.colorName = $(this).parent().parent().find("td").eq(7).text();
        tailor.sizeName = $(this).parent().parent().find("td").eq(8).text();
        tailor.partName = $(this).parent().parent().find("td").eq(9).text();
        tailor.layerCount = $(this).parent().parent().find("td").eq(10).text();
        tailor.packageNumber = $(this).parent().parent().find("td").eq(11).text();
        tailor.loColor = $(this).parent().parent().find("td").eq(12).text();
        tailor.tailorQcodeID = $(this).parent().parent().find("td").eq(13).text();
        tailor.printTimes = $(this).parent().parent().find("td").eq(14).text();
        tailor.tailorID = $(this).parent().parent().find("a").eq(0).attr("id");
        tailorList.push(tailor);
    });
    if(tailorList.length<1){
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请选择要打印的信息！</span>",html: true});
    }else{
        $.ajax({
            url: "/erp/updatefinishtailordata",
            type:'POST',
            data: {
                finishTailorList: JSON.stringify(tailorList)
            },
            success:function(data){
                if(data == 0) {
                    $("#tailorInfoTable tbody input[type='checkbox']:checked").each(function () {
                        $(this).parent().parent().find("td").eq(14).text(Number($(this).parent().parent().find("td").eq(14).text()) + 1);
                    });
                    swal({type:"success",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">打印启动成功！</span>",html: true});

                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，保存扎号信息失败！</span>",html: true});
                }
            },
            error:function(){
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务器发生了未知错误～！</span>",html: true});
            }
        });
        LODOP=getLodop();
        for (var i=0;i<tailorList.length;i++){
            LODOP.PRINT_INIT("");
            LODOP.SET_PRINT_PAGESIZE(1,"35mm","90mm","");
            LODOP.SET_PRINT_STYLE("FontSize",10);
            LODOP.SET_PRINT_STYLEA("FontName","黑体");
            CreateOnePage(tailorList[i].clothesVersionNumber,tailorList[i].orderName,tailorList[i].colorName,tailorList[i].packageNumber,tailorList[i].layerCount,tailorList[i].sizeName,tailorList[i].loColor,tailorList[i].customerName,tailorList[i].partName,tailorList[i].tailorQcodeID);
            // LODOP.SET_PRINT_MODE("CUSTOM_TASK_NAME","票菲"+i);//为每个打印单独设置任务名
            LODOP.PRINT();
        }
    }
}

function PrefixZero(num, n) {
    return (Array(n).join(0) + num).slice(-n);
}


function updateTailor(obj) {
    $("#initCount1").val($(obj).parent().parent().find("td").eq(11).text());
    $("#layerCount1").val($(obj).parent().parent().find("td").eq(10).text());
    $.blockUI({
        css: {
            width: '40%',
            top: '15%',
            left: '26%',
            border: 'none',
            padding: '15px',
            backgroundColor: '#fff',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: 1,
            color: '#000'
        },
        message: $('#updateCount')
    });

    $("#updateCountYes").unbind("click").bind("click", function () {
        var flag = false;
        $("#editTailor input").each(function () {
            if($(this).val().trim() == "") {
                flag = true;
                return false;
            }
        });
        if(flag) {
            swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请填写完所有字段信息！</span>",html: true});
            return false;
        }
        $(obj).parent().parent().find("td").eq(10).text($("#layerCount1").val().trim());
        $.unblockUI();
        $("#updateCount input").val("");

    });
    $("#updateCountNo").unbind("click").bind("click", function () {
        $.unblockUI();
        $("#updateCount input").val("");
    });
}

function CreateOnePage(clothesVersionNumber, orderName, colorName, packageNumber, layerCount, sizeName, loColor, customerName, partName,qcodeid){
    LODOP.NewPage();
    LODOP.ADD_PRINT_IMAGE("9mm","-3mm","40mm","15mm","<img border='0' src='/images/deyoo.png'>");
    LODOP.SET_PRINT_STYLEA(0,"Stretch",1);//(可变形)扩展缩放模式
    LODOP.ADD_PRINT_TEXTA("text01","22mm","1mm","33mm","4mm",clothesVersionNumber);
    LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
    LODOP.ADD_PRINT_TEXTA("text02","26mm","1mm","33mm","4mm",orderName);
    LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
    LODOP.ADD_PRINT_TEXTA("text03","30mm","1mm","33mm","4mm",colorName);
    LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
    LODOP.ADD_PRINT_TEXTA("text04","34mm","0mm","16mm","4mm",packageNumber+"扎");
    LODOP.SET_PRINT_STYLEA(0,"Alignment",3);
    LODOP.SET_PRINT_STYLE("Bold",1);
    LODOP.ADD_PRINT_TEXTA("text05","34mm","18mm","16mm","4mm",layerCount+"件");
    LODOP.SET_PRINT_STYLEA(0,"Alignment",1);
    LODOP.SET_PRINT_STYLE("Bold",1);
    LODOP.ADD_PRINT_TEXTA("text06","38mm","0mm","16mm","4mm",sizeName);
    LODOP.SET_PRINT_STYLEA(0,"Alignment",3);
    LODOP.SET_PRINT_STYLE("Bold",1);
    LODOP.ADD_PRINT_TEXTA("text06","38mm","18mm","16mm","4mm","L色:"+loColor);
    LODOP.SET_PRINT_STYLEA(0,"Alignment",1);
    LODOP.SET_PRINT_STYLE("Bold",1);
    LODOP.ADD_PRINT_TEXTA("text10","42mm","1mm","33mm","4mm",customerName);
    LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
    LODOP.SET_PRINT_STYLE("Bold",1);
    LODOP.ADD_PRINT_TEXTA("text11","46mm","1mm","33mm","4mm",partName);
    LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
    LODOP.SET_PRINT_STYLE("Bold",0);
    LODOP.ADD_PRINT_BARCODE("50mm","11mm","18mm","18mm","QRCode",qcodeid);
    LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
    LODOP.ADD_PRINT_TEXTA("text12","63mm","1mm","33mm","3mm",qcodeid);
    LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
    // LODOP.PRINT_DESIGN();
}