var basePath=$("#basePath").val();
layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.21'
}).extend();
$(document).ready(function () {

    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumber", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });

    $("#orderName").bind('blur change',function(){
        var orderName = $("#orderName").val();
        $.ajax({
            url: "/erp/getcustomernamebyordername",
            data: {"orderName": orderName},
            success:function(data){
                $("#customerName").empty();
                $("#customerName").val(data);
            },
            error:function(){
            }
        });
        $("select[name='colorName']").empty();
        $.ajax({
            url: "/erp/getordercolornamesbyorder",
            data: {"orderName": orderName},
            success:function(data){
                if (data.colorNameList) {
                    $.each(data.colorNameList, function(index,element){
                        $("select[name='colorName']").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
            }, error:function(){
            }
        });

        $("select[name='size']").empty();
        $("select[name='size']").append("<option value=''>尺码</option>");
        $.ajax({
            url: "/erp/getordersizenamesbyorder",
            data: {"orderName": orderName},
            success:function(data){
                if (data.sizeNameList) {
                    $.each(data.sizeNameList, function(index,element){
                        $("select[name='size']").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });
    });

    $("#clothesVersionNumber").bind('blur change',function(){
        var orderName = $("#orderName").val();
        $.ajax({
            url: "/erp/getcustomernamebyordername",
            data: {"orderName": orderName},
            success:function(data){
                $("#customerName").empty();
                $("#customerName").val(data);
            },
            error:function(){
            }
        });
        $("select[name='colorName']").empty();
        $.ajax({
            url: "/erp/getordercolornamesbyorder",
            data: {"orderName": orderName},
            success:function(data){
                if (data.colorNameList) {
                    $.each(data.colorNameList, function(index,element){
                        $("select[name='colorName']").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
            }, error:function(){
            }
        });

        $("select[name='size']").empty();
        $("select[name='size']").append("<option value=''>尺码</option>");
        $.ajax({
            url: "/erp/getordersizenamesbyorder",
            data: {"orderName": orderName},
            success:function(data){
                if (data.sizeNameList) {
                    $.each(data.sizeNameList, function(index,element){
                        $("select[name='size']").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });
    });


    $("#partName").change(function () {
        var orderName = $("#orderName").val();
        var partName = $("#partName").val();
        if (partName != null && partName != ""){
            $.ajax({
                url: "/erp/searchfinishcutqueryleak",
                type:'GET',
                data: {
                    orderName:orderName,
                    partName:partName
                },
                success: function (data) {
                    if (data){
                        $("#finishTailorTable").show();
                        $("#finishTailorTable").empty();
                        var colorIndex = 0;
                        var sizeSum = [];
                        var sizeSumIndex = 2;
                        for (var color in data){
                            var row1 = "<tr><td style='width: 60px'>"+color+"</td><td style='width: 60px'>"+"订单量"+"</td>";
                            var row2 = "<tr><td style='width: 60px'></td><td style='width: 60px'>"+"好片数"+"</td>";
                            var row3 = "<tr><td style='width: 60px'></td><td style='width: 60px'>"+"已打印"+"</td>";
                            var row4 = "<tr><td style='width: 60px'></td><td style='width: 60px'>"+"未打印"+"</td>";
                            if (colorIndex == 0){
                                tableHtml = "<div name='printTable'>\n" +
                                    "               <section class=\"panel panel-default\" style='page-break-after:always;'>\n" +
                                    "                   <header class=\"panel-heading font-bold\" style=\"text-align: center\">\n" +
                                    "                       <div style=\"font-size: 14px; align-items: center;margin-top:10px;font-weight: 700\">"+"已打印详情"+"</div>\n"+
                                    "                   </header>\n" +
                                    "                   <table class='table table-bordered' style='margin-top:10px;border: 2px;font-weight: 700;white-space: nowrap;'>\n" +
                                    "                       <tbody>";
                                tableHtml += "<tr><td style='width: 60px'></td><td style='width: 60px'></td>";
                            }
                            var colorSizeSum1 = 0;
                            var colorSizeSum2 = 0;
                            var colorSizeSum3 = 0;
                            var colorSizeSum4 = 0;
                            var sizeData = data[color];
                            var thisSizeSumIndex = 2;
                            var sizeDataList = [];
                            for (var i in sizeData){
                                var sizeDataItem = {};
                                sizeDataItem.sizeName = i;
                                sizeDataItem.sizeDataDetail = sizeData[i];
                                sizeDataList.push(sizeDataItem);
                            }
                            sizeDataList = globalSizeDataSort(sizeDataList);
                            for (var i in sizeDataList){
                                if (colorIndex == 0){
                                    tableHtml += "<td style='width: 40px'>"+sizeDataList[i].sizeName+"</td>";
                                }

                                row1 += "<td style='width: 40px'>"+sizeDataList[i].sizeDataDetail.orderCount+"</td>";
                                row2 += "<td style='width: 40px'>"+sizeDataList[i].sizeDataDetail.wellCount+"</td>";
                                row3 += "<td style='width: 40px'>"+sizeDataList[i].sizeDataDetail.finishCount+"</td>";
                                row4 += "<td style='width: 40px'>"+sizeDataList[i].sizeDataDetail.diffCount+"</td>";
                                colorSizeSum1 += sizeDataList[i].sizeDataDetail.orderCount;
                                colorSizeSum2 += sizeDataList[i].sizeDataDetail.wellCount;
                                colorSizeSum3 += sizeDataList[i].sizeDataDetail.finishCount;
                                colorSizeSum4 += sizeDataList[i].sizeDataDetail.diffCount;
                                thisSizeSumIndex ++;
                            }
                            if (colorIndex == 0){
                                tableHtml += "<td style='width: 40px'>"+"合计"+"</td>";
                            }

                            row1 += "<td style='width: 40px'>"+colorSizeSum1+"</td></tr>";
                            row2 += "<td style='width: 40px'>"+colorSizeSum2+"</td></tr>";
                            row3 += "<td style='width: 40px'>"+colorSizeSum3+"</td></tr>";
                            row4 += "<td style='width: 40px'>"+colorSizeSum4+"</td></tr>";
                            tableHtml += row1;
                            tableHtml += row2;
                            tableHtml += row3;
                            tableHtml += row4;
                            colorIndex ++;
                        }
                        tableHtml +=  "                       </tbody>" +
                            "                   </table>" +
                            "               </section>" +
                            "              </div>";
                        $("#finishTailorTable").append(tableHtml);
                    }
                }, error: function () {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
                }
            });
        }
    });

});

var tailorTable;
function addOrder() {
    var flag = false;
    $("#baseInfo input").each(function () {
        if($(this).val() === "" || $(this).val() == null) {
            flag = true;
            return false;
        }
    });
    $("#baseInfo select").each(function () {
        if($(this).val() === "" || $(this).val() == null) {
            flag = true;
            return false;
        }
    });
    if(flag) {
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请填写完基本信息！</span>",html: true});
        return false;
    }
    var tailorDataJson = {};
    tailorDataJson.orderName = $("#orderName").val();
    tailorDataJson.customerName = $("#customerName").val();
    tailorDataJson.bedNumber = "0";
    tailorDataJson.partName = $("#partName").val();;
    tailorDataJson.layerCount = $("#layerCount").val();
    tailorDataJson.clothesVersionNumber = $("#clothesVersionNumber").val();
    var colorList = {};
    var loColorList = {};
    flag = false;
    $("select[name='colorName']").each(function (index,item) {
        var color = $(this).val();
        var loColor = $("select[name='LOColor']").eq(index).val();
        colorList[index+1] = color;
        loColorList[index+1] = loColor;
    });
    if(flag) {
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入完整信息！</span>",html: true});
        return false;
    }
    tailorDataJson.colorList = colorList;
    tailorDataJson.loColorList = loColorList;

    // var sizeRatio = {};
    var sizeNameList = {};
    var sizeCountList = {};
    flag = true;
    $("select[name='size']").each(function (index,item) {
        var size = $(this).val();
        var radio = $("input[name='radio']").eq(index).val();
        if(size!=null && size!="" && radio!=null && radio!="") {
            sizeNameList[index] = size;
            sizeCountList[index] = radio;
            flag = false;
        }
    });
    if(flag) {
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入唛架配比！</span>",html: true});
        return false;
    }
    tailorDataJson.sizeNameList = sizeNameList;
    tailorDataJson.sizeCountList = sizeCountList;
    $.ajax({
        url: "/erp/generatefinishtailordata",
        type:'POST',
        data: {
            tailorDataJson:JSON.stringify(tailorDataJson)
        },
        success: function (data) {
            console.log(data);
            if(data && data!= "null") {
                if (data.msg){
                    swal({
                        type: "error",
                        title: "对不起",
                        text: "<span style=\"font-weight:bolder;font-size: 20px\">"+data.msg+"</span>",
                        html: true
                    });
                }
                if (data.finishTailorList){
                    var finishTailorData = data.finishTailorList;
                    swal(
                        {   type:"success",
                            title:"",
                            text: "<span style=\"font-weight:bolder;font-size: 20px\">恭喜你，生成成功！</span>",
                            html: true
                        },function () {
                            var href = $("li.active a[data-toggle='tab']",parent.document).attr("href");
                            var tabId = href.substr(7);
                            window.parent.document.getElementById("tailorA").click();
                            var numberId = window.parent.document.getElementById("numberID").value;
                            var list = "";
                            if ($('#tailorTable', window.parent.document).hasClass('dataTable')) {
                                $('#tailorTable_wrapper', window.parent.document).remove();
                                var $div = $("<table class=\"table table-striped\" id=\"tailorTable\" >\n" +
                                    "             <thead>\n" +
                                    "             <tr bgcolor=\"#ffcb99\" style=\"color: black;\">\n" +
                                    "                 <input type=\"text\" hidden id=\"numberID\" value=\"1\">\n" +
                                    "                 <th style=\"width: 30px;text-align:left;font-size:14px\"><input type=\"checkbox\" onclick=\"checkAll(this)\"></th>\n" +
                                    "                 <th style=\"width: 30px;text-align:center;font-size:14px\">序号</th>\n" +
                                    "                 <th style=\"width: 90px;text-align:center;font-size:14px\">订单号</th>\n" +
                                    "                 <th style=\"width: 90px;text-align:center;font-size:14px\">版单号</th>\n" +
                                    "                 <th style=\"width: 60px;text-align:center;font-size:14px\">客户</th>\n" +
                                    "                 <th style=\"width: 60px;text-align:center;font-size:14px\">颜色</th>\n" +
                                    "                 <th style=\"width: 45px;text-align:center;font-size:14px\">缸号</th>\n" +
                                    "                 <th style=\"width: 45px;text-align:center;font-size:14px\">床号</th>\n" +
                                    "                 <th style=\"width: 45px;text-align:center;font-size:14px\">数量</th>\n" +
                                    "                 <th style=\"width: 45px;text-align:center;font-size:14px\">扎号</th>\n" +
                                    "                 <th style=\"width: 60px;text-align:center;font-size:14px\">部位</th>\n" +
                                    "                 <th style=\"width: 60px;text-align:center;font-size:14px\">尺码</th>\n" +
                                    "                 <th style=\"width: 60px;text-align:center;font-size:14px\">LOT色</th>\n" +
                                    "                 <th style=\"width: 90px;text-align:center;font-size:14px\">二维码</th>\n" +
                                    "                 <th style=\"width: 120px;text-align:center;font-size:14px\">操作</th>\n" +
                                    "             </tr>\n" +
                                    "             </thead>\n" +
                                    "             <tbody id=\"tailorBody\">\n" +
                                    "             </tbody>\n" +
                                    "         </table>");
                                $("#tailorTableDiv",window.parent.document).append($div);
                            }
                            var $tailorBody = window.parent.document.getElementById("tailorBody");
                            var $saveButton = window.parent.document.getElementById("saveButton");
                            $saveButton.style.display='block';
                            $.each(finishTailorData,function (index,item) {
                                list +=  "<tr>" +
                                    "<td><input type='checkbox' value='"+numberId+"'></td>" +
                                    "<td>"+numberId+"</td>" +
                                    "<td>"+item.orderName+"</td>" +
                                    "<td>"+item.clothesVersionNumber+"</td>" +
                                    "<td>"+item.customerName+"</td>" +
                                    "<td>"+item.colorName+"</td>" +
                                    "<td>"+item.jarName+"</td>" +
                                    "<td>"+item.bedNumber+"</td>" +
                                    "<td>"+item.layerCount+"</td>" +
                                    "<td>"+item.packageNumber+"</td>" +
                                    "<td>"+item.partName+"</td>" +
                                    "<td>"+item.sizeName+"</td>" +
                                    "<td>"+item.loColor+"</td>" +
                                    "<td>"+PrefixZero(item.tailorQcodeID,9)+"</td>" +
                                    "<td><a href='#' style='color:#3e8eea' onclick='showQrCode(this)'>查看</a>&nbsp;&nbsp;&nbsp;<a href='#' style='color:#3e8eea' onclick='updateTailor(this)'>修改</a>&nbsp;&nbsp;&nbsp;<a href='#' style='color:#3e8eea' onclick='delTailor(this)'>删除</a></td>" +
                                    "</tr>";
                                numberId++;
                            });
                            $tailorBody.innerHTML = list;
                            window.parent.document.getElementById("tailorBody").value=numberId;
                            tailorTable = $('#tailorTable',window.parent.document).DataTable({
                                language : {
                                    processing : "载入中",//处理页面数据的时候的显示
                                    paginate : {//分页的样式文本内容。
                                        previous : "上一页",
                                        next : "下一页",
                                        first : "第一页",
                                        last : "最后一页"
                                    },
                                    search:"搜索：",
                                    lengthMenu:"显示 _MENU_ 条",
                                    zeroRecords : "没有内容",//table tbody内容为空时，tbody的内容。
                                    //下面三者构成了总体的左下角的内容。
                                    info : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//左下角的信息显示，大写的词为关键字。
                                    infoEmpty : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//筛选为空时左下角的显示。
                                    infoFiltered : ""//筛选之后的左下角筛选提示(另一个是分页信息显示，在上面的info中已经设置，所以可以不显示)，
                                },
                                searching:false,
                                ordering:true,
                                "paging" : false,
                                "info": true,
                                "destroy":true,
                                pagingType : "full_numbers", // 分页样式：simple,simple_numbers,full,full_numbers，
                                scrollX: 1500,
                                scrollY: 650,
                                fixedHeader: true,
                                scrollCollapse: true,
                                scroller:       true,
                                lengthChange:false
                            });
                        });
                }
            }else {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，生成失败！</span>",html: true});
            }
        },
        error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }
    })
}

function deleteRow(obj) {
    tailorTable.row($(obj).parents('tr')).remove().draw(false);
}

function addSize(obj) {
    $("#addSizeDiv").append($("div[name='sizeRadioDiv']:last").clone());
    var orderOfSizeRadioArr = $('[name="orderOfSizeRadio"]');
    for (var i = 0; i < orderOfSizeRadioArr.length; i++){
        $(orderOfSizeRadioArr[i]).text(i + 1);
    }
    $("input[name='radio']:last").val("");
    $("span[name='totalSizeRadio']").parent().hide();
    $("span[name='totalSizeRadio']:last").parent().show();
    $("span[name='totalClothesCount']").parent().hide();
    $("span[name='totalClothesCount']:last").parent().show();

    $("button[name='addSize']:last").hide();
    $("button[name='addSize']:last").next().show();
}

function delNumber(obj) {
    $(obj).parent().parent().remove();

    var orderOfLayerArr = $('[name="orderOfLayer"]');
    for (var i = 0; i < orderOfLayerArr.length; i++){
        $(orderOfLayerArr[i]).text(i + 1);
    }
}

function addNumber(obj) {
    $("#numberAddDiv").append($("div[name='numberDiv']:last").clone());
    var orderOfLayerArr = $('[name="orderOfLayer"]');
    for (var i = 0; i < orderOfLayerArr.length; i++){
        $(orderOfLayerArr[i]).text(i + 1);
    }
    $("select[name='colorName']:last").val($("select[name='colorName']").eq(-2).val());
    $("select[name='LOColor']:last").val($("select[name='LOColor']").eq(-2).val());

    $("button[name='addNumber']:last").hide();
    $("button[name='addNumber']:last").next().show();
}


function totalLayer(obj) {
    var num = 0;
    $("input[name='layer']").each(function (index,item) {
        var tmp = Number($(item).val());
        num += tmp;
    });

    $("span[name='totalLayer']").text(num);
}


function delSize(obj) {
    $(obj).parent().parent().remove();
    var orderOfSizeRadioArr = $('[name="orderOfSizeRadio"]');
    for (var i = 0; i < orderOfSizeRadioArr.length; i++){
        $(orderOfSizeRadioArr[i]).text(i + 1);
    }

    var num = 0;
    $("input[name='radio']").each(function (index,item) {
        var tmp = Number($(item).val());
        num += tmp;
    });

    $("span[name='totalSizeRadio']").text(num);

    $("span[name='totalSizeRadio']:last").parent().show();

    var layer = Number($("#layerCount").val());
    $("span[name='totalClothesCount']").text(num * layer);
    $("span[name='totalClothesCount']:last").parent().show();
}


function totalSizeRadio(obj) {
    var num = 0;
    $("input[name='radio']").each(function (index,item) {
        var tmp = Number($(item).val());
        num += tmp;
    });

    $("span[name='totalSizeRadio']").text(num);

    var layer = Number($("#layerCount").val());
    $("span[name='totalClothesCount']").text(num * layer);
}

function PrefixZero(num, n) {
    return (Array(n).join(0) + num).slice(-n);
}