$(document).ready(function () {
    // createOrderTable();
    $('#mainFrameTabs').bTabs();

    $('#orderListTab').click(function(){
        createOrderTable();
    });
});

function checkAll(obj) {
    if($(obj).is(':checked')) {
        $("#tailorTable tbody input[type='checkbox']").prop("checked",true);
    }else {
        $("#tailorTable tbody input[type='checkbox']").prop("checked",false);
    };
}

//计算内容区域高度
function calcHeight(){
    var browserHeight = $(window).innerHeight();
    var topHeight = $('#mainFrameHeadBar').outerHeight(true);
    var tabMarginTop = parseInt($('#mainFrameTabs').css('margin-top'));//获取间距
    var tabHeadHeight = $('ul.nav-tabs',$('#mainFrameTabs')).outerHeight(true) + tabMarginTop;
    var contentMarginTop = parseInt($('div.tab-content',$('#mainFrameTabs')).css('margin-top'));//获取内容区间距
    var contentHeight = browserHeight - topHeight - tabHeadHeight - contentMarginTop;
    $('div.tab-content',$('#mainFrameTabs')).height(contentHeight);
};

var basePath=$("#basePath").val();

function createOrderTable() {

}

var tabId = 1;
function addTailor(orderName) {
    if(orderName) {
        var urlName = encodeURIComponent(orderName);
        var tabName = orderName.replace(/[&\|\\\*^%$#@\-+~`！!……——=「」{};:：；‘’\"\"/<>.,，。《》]/g,"");
        $('#mainFrameTabs').bTabsAdd("tabId" + tabName, "订单详情", "/erp/addFinishTailorStart?orderName="+urlName);
    }else {
        $('#mainFrameTabs').bTabsAdd("tabId" + tabId, "裁片信息录入", "/erp/addFinishTailorStart");
    }
    tabId++;
    // calcHeight();
}



function printer() {
    var tailorList = [];
    $("#tailorBody input[type='checkbox']:checked").each(function () {
        var tailor = {};
        tailor.orderName = $(this).parent().parent().find("td").eq(2).text();
        tailor.clothesVersionNumber = $(this).parent().parent().find("td").eq(3).text();
        tailor.customerName = $(this).parent().parent().find("td").eq(4).text();
        tailor.colorName = $(this).parent().parent().find("td").eq(5).text();
        tailor.jarName = $(this).parent().parent().find("td").eq(6).text();
        tailor.bedNumber = $(this).parent().parent().find("td").eq(7).text();
        tailor.layerCount = $(this).parent().parent().find("td").eq(8).text();
        tailor.packageNumber = $(this).parent().parent().find("td").eq(9).text();
        tailor.partName = $(this).parent().parent().find("td").eq(10).text();
        tailor.sizeName = $(this).parent().parent().find("td").eq(11).text();
        tailor.tailorQcode = tailor.orderName +"-"+tailor.customerName+"-"+tailor.bedNumber+"-"+tailor.jarName+"-"+tailor.colorName+"-"+tailor.sizeName+"-"+tailor.layerCount+"-"+tailor.packageNumber+"-"+tailor.partName;
        tailor.tailorQcodeID = $(this).parent().parent().find("td").eq(13).text();
        tailor.loColor = $(this).parent().parent().find("td").eq(12).text();
        tailor.printTimes = 0;
        tailorList.push(tailor);
    });
    if(tailorList.length<1){
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请选择要打印的信息！</span>",html: true});
    }else{

        $("div[id^='qrcode']").remove();
        $("div[id^='page']").remove();
        $.ajax({
            url: "/erp/savefinishtailordata",
            data: {
                tailorList: JSON.stringify(tailorList)
            },
            success:function(data){
                if(data == 0) {
                    swal({
                        type:"success",
                        title:"",
                        text: "<span style=\"font-weight:bolder;font-size: 20px\">保存成功！</span>",
                        html: true
                    },function (){
                        $("#saveButton").hide();
                    })
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，保存扎号信息失败！</span>",html: true});
                }
            },
            error:function(){
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务器发生了未知错误～！</span>",html: true});
            }
        });
    }
}

function showQrCode(obj) {
    $("#printOrderName").text($(obj).parent().parent().find("td").eq(2).text());
    $("#printClothesVersionNumber").text($(obj).parent().parent().find("td").eq(3).text());
    $("#printCustomerName").text($(obj).parent().parent().find("td").eq(4).text());
    $("#printColorName").text($(obj).parent().parent().find("td").eq(5).text());
    $("#printJarName").text($(obj).parent().parent().find("td").eq(6).text());
    $("#printBedNumber").text($(obj).parent().parent().find("td").eq(7).text());
    $("#printLayerCount").text($(obj).parent().parent().find("td").eq(8).text());
    $("#printPackageNumber").text($(obj).parent().parent().find("td").eq(9).text());
    $("#printPartName").text($(obj).parent().parent().find("td").eq(10).text());
    $("#printSizeName").text($(obj).parent().parent().find("td").eq(11).text());
    var tailorQcodeID = $(obj).parent().parent().find("td").eq(12).text();
    $.blockUI({
        css: {
            width: '20%',
            top: '10%',
            left:'45%',
            border: 'none',
            padding: '15px',
            backgroundColor: '#fff',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: 1,
            color: '#000'
        },
        message: $('#qrCodeWin')
    });
    // $('#qrcode').qrcode(storehouseLocation);
    var qrcode = new QRCode(document.getElementById("qrCode"), {
        width : 70,
        height : 70,
        correctLevel:1
    });
    qrcode.makeCode(tailorQcodeID);
    $("#closeQrCodeWin").unbind("click").bind("click", function () {
        $.unblockUI();
        $('#qrCode').empty();
    });
}

function updateTailor(obj) {
    $("#orderName").val($(obj).parent().parent().find("td").eq(2).text());
    $("#customerName").val($(obj).parent().parent().find("td").eq(4).text());
    $("#colorName").val($(obj).parent().parent().find("td").eq(5).text());
    $("#jarName").val($(obj).parent().parent().find("td").eq(6).text());
    $("#bedNumber").val($(obj).parent().parent().find("td").eq(7).text());
    $("#layerCount").val($(obj).parent().parent().find("td").eq(8).text());
    $("#packageNumber").val($(obj).parent().parent().find("td").eq(9).text());
    $("#partName").val($(obj).parent().parent().find("td").eq(10).text());
    $("#sizeName").val($(obj).parent().parent().find("td").eq(11).text());
    $.blockUI({
        css: {
            width: '50%',
            top: '10%',
            left:'15%',
            border: 'none',
            padding: '15px',
            backgroundColor: '#fff',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: 1,
            color: '#000'
        },
        message: $('#editTailor')
    });

    $("#editTailorYes").unbind("click").bind("click", function () {
        var flag = false;
        $("#editTailor input").each(function () {
            if($(this).val().trim() == "") {
                flag = true;
                return false;
            }
        });
        if(flag) {
            swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请填写完所有字段信息！</span>",html: true});
            return false;
        }
        $(obj).parent().parent().find("td").eq(2).text($("#orderName").val());
        $(obj).parent().parent().find("td").eq(4).text($("#customerName").val());
        $(obj).parent().parent().find("td").eq(5).text($("#colorName").val());
        $(obj).parent().parent().find("td").eq(6).text($("#jarName").val());
        $(obj).parent().parent().find("td").eq(7).text($("#bedNumber").val());
        $(obj).parent().parent().find("td").eq(8).text($("#layerCount").val());
        $(obj).parent().parent().find("td").eq(9).text($("#packageNumber").val());
        $(obj).parent().parent().find("td").eq(10).text($("#partName").val());
        $(obj).parent().parent().find("td").eq(11).text($("#sizeName").val());
        // $(obj).parent().parent().find("td").eq(12).text($("#orderName").val()+"-"+$("#customerName").val()+"-"+$("#bedNumber").val()+"-"+$("#jarName").val()+"-"+$("#colorName").val()+"-"+$("#sizeName").val()+"-"+$("#layerCount").val()+"-"+$("#packageNumber").val()+"-"+$("#partName").val());
        $.unblockUI();
        $("#editTailor input").val("");

    });
    $("#editTailorNo").unbind("click").bind("click", function () {
        $.unblockUI();
        $("#editTailor input").val("");
    });
}

function delTailor(obj) {
    swal({
        title: "",
        text: "<span style=\"font-weight:bolder;font-size: 20px\">您确定要删除该条裁片信息吗？</span>",
        type: "warning",
        html:true,
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: "确定",
        cancelButtonText:"我再想想",
        confirmButtonColor: "#ec6c62",
        showLoaderOnConfirm: false
    }, function() {
        $("#mainFrameTabs iframe:last")[0].contentWindow.deleteRow(obj);
        swal({
            type:"success",
            title:"",
            text: "<span style=\"font-weight:bolder;font-size: 20px\">恭喜你，删除成功！</span>",
            html: true
        });
    });

}
