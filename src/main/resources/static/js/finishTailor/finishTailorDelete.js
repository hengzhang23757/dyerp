var basePath=$("#basePath").val();
var userName=$("#userName").val();
layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.21'
}).extend();
var form;
layui.use('form', function(){
    form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
    form.render();
})
$(document).ready(function () {
    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumber", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });

    $("#orderName").bind('blur change',function(){
        var orderName = $("#orderName").val();
        $("#colorName").empty();
        $.ajax({
            url: "/erp/getcolorhint",
            data: {"orderName": orderName},
            success:function(data){
                $("#colorName").empty();
                if (data.colorList) {
                    $("#colorName").append("<option value=''>选择颜色</option>");
                    $.each(data.colorList, function(index,element){
                        $("#colorName").append("<option value='"+element.colorName+"'>"+element.colorName+"</option>");
                    })
                    form.render();
                }
            },
            error:function(){
            }
        });
        $("#sizeName").empty();
        $.ajax({
            url: "/erp/getordersizenamesbyorder",
            data: {"orderName": orderName},
            success:function(data){
                $("#sizeName").empty();
                if (data.sizeNameList) {
                    $("#sizeName").append("<option value=''>选择尺码</option>");
                    $.each(data.sizeNameList, function(index,element){
                        $("#sizeName").append("<option value='"+element+"'>"+element+"</option>");
                    })
                    form.render();
                }
            },
            error:function(){
            }
        });
    });

    $("#clothesVersionNumber").bind('blur change',function(){
        var orderName = $("#orderName").val();
        $("#colorName").empty();
        $.ajax({
            url: "/erp/getcolorhint",
            data: {"orderName": orderName},
            success:function(data){
                $("#colorName").empty();
                if (data.colorList) {
                    $("#colorName").append("<option value=''>选择颜色</option>");
                    $.each(data.colorList, function(index,element){
                        $("#colorName").append("<option value='"+element.colorName+"'>"+element.colorName+"</option>");
                    })
                    form.render();
                }
            },
            error:function(){
            }
        });
        $("#sizeName").empty();
        $.ajax({
            url: "/erp/getordersizenamesbyorder",
            data: {"orderName": orderName},
            success:function(data){
                $("#sizeName").empty();
                if (data.sizeNameList) {
                    $("#sizeName").append("<option value=''>选择尺码</option>");
                    $.each(data.sizeNameList, function(index,element){
                        $("#sizeName").append("<option value='"+element+"'>"+element+"</option>");
                    })
                    form.render();
                }
            },
            error:function(){
            }
        });
    });
});
var tailorInfoTable;
layui.use(['form', 'soulTable', 'table', 'upload','yutons_sug'], function () {
    var table = layui.table,
        soulTable = layui.soulTable,
        form = layui.form;
    var load = layer.load();

    tailorInfoTable = table.render({
        elem: '#tailorInfoTable'
        ,cols: [[]]
        ,height: 'full-100'
        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
        ,loading:true
        ,totalRow: true
        ,page: true
        ,limits: [1000, 2000, 5000]
        ,limit: 2000//每页默认显示的数量
        ,even: true
        ,overflow: 'tips'
        ,done: function () {
            soulTable.render(this);
            layer.close(load);
        }
    });

    initTable('', '', '', '');

    function initTable(orderName, colorName, sizeName, partName){
        var param = {}
        param.partName = partName;
        param.orderName = orderName;
        if (colorName != null && colorName != ''){
            param.colorName = colorName;
        }
        if (sizeName != null && sizeName != ''){
            param.sizeName = sizeName;
        }
        $.ajax({
            url: "/erp/getfinishtailorbyordercolorsize",
            type: 'GET',
            data: param,
            success: function (res) {
                if (res.finishTailorList) {
                    var reportData = res.finishTailorList;
                    table.render({
                        elem: '#tailorInfoTable'
                        ,cols:[[
                            {type:'checkbox'}
                            ,{type:'numbers', align:'center', title:'序号', minWidth:60}
                            ,{field:'clothesVersionNumber', title:'单号', align:'center', minWidth:150, sort: true, filter: true}
                            ,{field:'orderName', title:'款号', align:'center', minWidth:150, sort: true, filter: true, totalRowText: '合计'}
                            ,{field:'bedNumber', title:'床号', align:'center', minWidth:120, sort: true, filter: true}
                            ,{field:'colorName', title:'颜色', align:'center', minWidth:100, sort: true, filter: true}
                            ,{field:'sizeName', title:'尺码', align:'center',minWidth:100, sort: true, filter: true}
                            ,{field:'packageNumber', title:'扎号', align:'center', minWidth:90, sort: true, filter: true}
                            ,{field:'layerCount', title:'数量', align:'center', minWidth:90, sort: true, filter: true, totalRow: true}
                            ,{field:'isScan', title:'计件', align:'center', minWidth:90, sort: true, filter: true,templet: function (d) {
                                    if (d.isScan === 1){
                                        return "是";
                                    }else {
                                        return "否";
                                    }
                                }}
                            ,{field:'shelfEmp', title:'上架', align:'center', minWidth:100, sort: true, filter: true}
                            ,{field:'shelfName', title:'车号', align:'center', minWidth:100, sort: true, filter: true}
                            ,{field:'shelfOutEmp', title:'下架', align:'center', minWidth:100, sort: true, filter: true}
                        ]]
                        ,data: reportData   // 将新数据重新载入表格
                        ,overflow: 'tips'
                        ,height: 'full-100'
                        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                        ,loading:true
                        ,totalRow: true
                        ,page: true
                        ,limits: [1000, 2000, 5000]
                        ,limit: 2000//每页默认显示的数量
                        ,even: true
                        ,done: function () {
                            soulTable.render(this);
                            layer.close(load);
                        }
                    });

                } else {
                    layer.msg("获取失败！", {icon: 2});
                }
            }, error: function () {
                layer.msg("获取失败！", {icon: 2});
            }
        });
    }

    //监听提交
    form.on('submit(searchBeat)', function(data){
        var orderName = $("#orderName").val();
        var colorName = $("#colorName").val();
        var sizeName = $("#sizeName").val();
        var partName = $("#partName").val();
        if (orderName == null || orderName === ""){
            layer.msg("款号必须~~~");
            return false;
        }
        initTable(orderName, colorName, sizeName, partName);
        return false;
    });

    table.on('toolbar(tailorInfoTable)', function(obj) {
        var orderName = $("#orderName").val();
        var colorName = $("#colorName").val();
        var sizeName = $("#sizeName").val();
        var partName = $("#partName").val();
        if (obj.event === 'clearFilter') {
            // 清除所有筛选条件并重载表格
            // 参数: tableId
            soulTable.clearFilter('cutStorageTable')
        } else if (obj.event === 'exportExcel') {
            soulTable.export('tailorInfoTable');
        } else if (obj.event === 'batchDelete') {
            var checkStatus = table.checkStatus(obj.config.id);
            if(checkStatus.data.length == 0) {
                layer.msg('请选择数据');
            }else {
                var packageNumberList = [];
                $.each(checkStatus.data, function (index, item) {
                    packageNumberList.push(item.packageNumber);
                });
                layer.confirm('确认全部删除吗?', function(index){
                    $.ajax({
                        url: "/erp/deletefinishtailorbyorderbedpack",
                        type: 'POST',
                        data: {
                            orderName:orderName,
                            packageNumberList:packageNumberList
                        },
                        traditional: true,
                        success: function (res) {
                            if (res == 0) {
                                layer.close(index);
                                layer.msg("删除成功！", {icon: 1});
                                initTable(orderName, colorName, sizeName, partName);
                            } else {
                                layer.msg("删除失败！", {icon: 2});
                            }
                        }, error: function () {
                            layer.msg("删除失败！", {icon: 2});
                        }
                    })
                });
            }
        }
    });
});