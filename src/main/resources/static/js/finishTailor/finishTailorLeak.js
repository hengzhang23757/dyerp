var hot;
var basePath=$("#basePath").val();
layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.21'
}).extend();
$(document).ready(function () {
    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumber", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });
});
function search() {
    var container = document.getElementById('addOrderExcel');
    if(hot==undefined) {
        hot = new Handsontable(container, {
            // data: data,
            rowHeaders: true,
            colHeaders: true,
            autoColumnSize: true,
            dropdownMenu: true,
            contextMenu: true,
            stretchH: 'all',
            autoWrapRow: true,
            height: $(document.body).height() - 230,
            manualRowResize: true,
            manualColumnResize: true,
            minCols: 17,
            colWidths:[100,100,60,60,60,60,60,60,60,60,60,60,60,60,60,60,60],
            language: 'zh-CN',
            licenseKey: 'non-commercial-and-evaluation'
        });
    }


    if($("#orderName").val()=="") {
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入要查询的订单号！</span>",html: true});
        return false;
    }
    $.blockUI({ css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        },
        message: "<h3>稍等一下... 奋力搜索中</h3>"
    });
    $.ajax({
        url: basePath+"erp/searchfinishcutqueryleak",
        type:'GET',
        data: {
            orderName:$("#orderName").val(),
            partName:$("#partName").val()
        },
        success: function (data) {
            $("#exportDiv").show();
            var hotData = [];
            if(data) {
                var planSum = 0;
                var actualSum = 0;
                var wellSum = 0;
                var finishSum = 0;
                var differenceSum = 0;
                var colorIndex = 0;
                for (var colorKey in data){
                    var colorRow1 = [];
                    var colorRow2 = [];
                    var colorRow3 = [];
                    var colorRow4 = [];
                    var colorRow5 = [];
                    colorRow2[0] = [];
                    colorRow3[0] = [];
                    colorRow4[0] = [];
                    colorRow5[0] = [];
                    colorRow1.push(colorKey);
                    colorRow1.push("订单量");
                    colorRow2.push("裁床数量");
                    colorRow3.push("好片数量");
                    colorRow4.push("后整裁数");
                    colorRow5.push("对比");
                    var sizeRow = [];
                    sizeRow[0] = [];
                    sizeRow[1] = [];
                    var sizeData = data[colorKey];
                    var colorPlanSum = 0;
                    var colorActualSum = 0;
                    var colorWellSum = 0;
                    var colorFinishSum = 0;
                    var colorDifferenceSum = 0;

                    var sizeDataList = [];
                    for (var i in sizeData){
                        var sizeDataItem = {};
                        sizeDataItem.sizeName = i;
                        sizeDataItem.sizeDataDetail = sizeData[i];
                        sizeDataList.push(sizeDataItem);
                    }
                    sizeDataList = globalSizeDataSort(sizeDataList);
                    for (var i in sizeDataList){
                        if (colorIndex == 0){
                            sizeRow.push(sizeDataList[i].sizeName);
                        }
                        colorRow1.push(sizeDataList[i].sizeDataDetail.orderCount);
                        colorPlanSum += sizeDataList[i].sizeDataDetail.orderCount;
                        colorRow2.push(sizeDataList[i].sizeDataDetail.cutCount);
                        colorActualSum += sizeDataList[i].sizeDataDetail.cutCount;
                        colorRow3.push(sizeDataList[i].sizeDataDetail.wellCount);
                        colorWellSum += sizeDataList[i].sizeDataDetail.wellCount;
                        colorRow4.push(sizeDataList[i].sizeDataDetail.finishCount);
                        colorFinishSum += sizeDataList[i].sizeDataDetail.finishCount;
                        colorRow5.push(sizeDataList[i].sizeDataDetail.diffCount);
                        colorDifferenceSum += sizeDataList[i].sizeDataDetail.diffCount;
                    }
                    if (colorIndex == 0){
                        sizeRow.push("合计");
                        hotData.push(sizeRow);
                    }

                    planSum += colorPlanSum;
                    actualSum += colorActualSum;
                    wellSum += colorWellSum;
                    finishSum += colorFinishSum;
                    differenceSum += colorDifferenceSum;
                    colorRow1.push(colorPlanSum);
                    colorRow2.push(colorActualSum);
                    colorRow3.push(colorWellSum);
                    colorRow4.push(colorFinishSum);
                    colorRow5.push(colorDifferenceSum);
                    hotData.push(colorRow1);
                    hotData.push(colorRow2);
                    hotData.push(colorRow3);
                    hotData.push(colorRow4);
                    hotData.push(colorRow5);

                    colorIndex ++;
                }
                hotData.push([["合计"],["订单总数"],planSum,["实裁数"],actualSum,["好片数"],wellSum,["后整数"],finishSum,["差异"],differenceSum]);
                hotData.push([]);

            }
            hot.loadData(hotData);
            $.unblockUI();
        },
        error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }
    })
}


function exportData() {
    var data = hot.getData();
    var result = [];
    var col = 0;
    $.each(data,function (index, item) {
        // if(item[0]!=null) {
        for(var i=0;i<item.length;i++) {
            if(item[i]==null) {
                item[i] = '';
            }else if(index==1) {
                col++;
            }
        }
        result.push(item);
        // }else {
        //     return false;
        // }
    });
    var orderName = $("#orderName").val();
    var clothesVersionNumber = $("#clothesVersionNumber").val();
    var myDate = new Date();
    export2Excel([orderName+"-"+clothesVersionNumber+'-裁床汇总-'+myDate.toLocaleString()],col, result, orderName+"-"+clothesVersionNumber+'-裁床汇总-'+myDate.toLocaleString()+".xls")
}