var basePath=$("#basePath").val();
var userName=$("#userName").val();
layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.21'
}).extend();
$(document).ready(function () {

    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumber", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });

    $("#orderName").bind('blur change',function(){
        var orderName = $("#orderName").val();
        $("#partName").empty();
        $.ajax({
            url: "/erp/getotherpartnamesbyordername",
            data: {"orderName": orderName},
            success:function(data){
                $("#partName").empty();
                if (data.otherPartNameList) {
                    $("#partName").append("<option value=''>选择部位</option>");
                    $.each(data.otherPartNameList, function(index,element){
                        $("#partName").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });
    });

    $("#clothesVersionNumber").bind('blur change',function(){
        var orderName = $("#orderName").val();
        $("#partName").empty();
        $.ajax({
            url: "/erp/getotherpartnamesbyordername",
            data: {"orderName": orderName},
            success:function(data){
                $("#partName").empty();
                if (data.otherPartNameList) {
                    $("#partName").append("<option value=''>选择部位</option>");
                    $.each(data.otherPartNameList, function(index,element){
                        $("#partName").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });
    });

    $("#partName").change(function () {
        var partName = $("#partName").val();
        $.ajax({
            url: "/erp/getotherbednumbersbyorderpart",
            data: {
                "orderName": $("#orderName").val(),
                "partName": partName
            },
            success:function(data){
                $("#bedNumber").empty();
                if (data.otherBedNumberList) {
                    $.each(data.otherBedNumberList, function(index,element){
                        $("#bedNumber").append("<option value="+element+">"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });
    })

});

var orderName,bedNumber,partName;

function searchTailorInfo() {
    orderName = $("#orderName").val().trim();
    if($("#orderName").val().trim()=="") {
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入要查询的订单号！</span>",html: true});
        return false;
    }
    partName = $("#partName").val().trim();
    if($("#partName").val().trim()=="") {
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入要查询的床号！</span>",html: true});
        return false;
    }
    bedNumber = $("#bedNumber").val();
    if($("#bedNumber").val().trim()=="") {
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入要查询的部位！</span>",html: true});
        return false;
    }

    $("#delBtn").show();
    $.ajax({
        url: basePath + "erp/getdetailothertailorbyorderpartbed",
        type:'GET',
        data: {
            orderName:$("#orderName").val(),
            partName:$("#partName").val(),
            bedNumber:$("#bedNumber").val(),
        },
        success: function (data) {
            if(data) {
                // var json = JSON.parse(data);
                createTailorInfoTable(data.otherTailorDetailList);
            }else {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，没有查到相关信息！</span>",html: true});
            }
        },
        error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }
    })

}



var tailorInfoTable;
function createTailorInfoTable(data) {
    if (tailorInfoTable != undefined) {
        tailorInfoTable.clear(); //清空一下table
        tailorInfoTable.destroy(); //还原初始化了的datatable
    }
    tailorInfoTable = $('#tailorInfoTable').DataTable({
        "data":data,
        language : {
            processing : "载入中",//处理页面数据的时候的显示
            paginate : {//分页的样式文本内容。
                previous : "上一页",
                next : "下一页",
                first : "第一页",
                last : "最后一页"
            },
            search:"搜索：",
            zeroRecords : "没有内容",//table tbody内容为空时，tbody的内容。
            //下面三者构成了总体的左下角的内容。
            info : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//左下角的信息显示，大写的词为关键字。
            infoEmpty : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//筛选为空时左下角的显示。
            infoFiltered : ""//筛选之后的左下角筛选提示(另一个是分页信息显示，在上面的info中已经设置，所以可以不显示)，
        },
        pageLength : 100,// 每页显示10条数据
        pagingType : "full_numbers", // 分页样式：simple,simple_numbers,full,full_numbers，
        "paging": true,
        "info": false,
        searching:false,
        lengthChange:false,
        fixedHeader: true,
        scrollY: $(document.body).height() - 220,
        "columns": [
            {
                "data": "packageNumber",
                "title":"<input type='checkbox'>",
                "width":"8%",
                "defaultContent": "",
                "sClass": "text-center"
            },
            {
                "data": null,
                "title":"序号",
                "width":"8%",
                "defaultContent": "",
                "sClass": "text-center",
                render: function (data, type, row, meta) {
                    var no = meta.settings._iDisplayStart + meta.row + 1;
                    return no;
                }
            },{
                "data": "orderName",
                "title":"订单号",
                "width":"20%",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "bedNumber",
                "title":"床号",
                "width":"10%",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "colorName",
                "title":"颜色",
                "width":"15%",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "sizeName",
                "title":"尺码",
                "width":"10%",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "packageNumber",
                "title":"扎号",
                "width":"15%",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "layerCount",
                "title":"数量",
                "width":"10%",
                "defaultContent": "",
                "sClass": "text-center",
            }
        ],
        "columnDefs" :
            [{
                "orderable" : false, // 禁用排序
                "targets" : [0], // 指定的列
                "render" : function(data, type, full, meta) {
                    return "<input name='checkBtn' type='checkbox' value='"+data+"'>";
                }
            }],
    });
}


function batchDelete() {
    var packageNumberList = [];
    $("#tailorInfoTable tbody input[name='checkBtn']").each(function(){
        if($(this).is(':checked')) {
            packageNumberList.push($(this).val());
        }
    });
    if(packageNumberList.length == 0) {
        swal("SORRY!", "请先选择要删除的数据！", "error");
        return;
    }
    swal({
        title: "",
        text: "<span style=\"font-weight:bolder;font-size: 20px\">您确定要删除吗？</span>",
        type: "warning",
        html:true,
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: "确定",
        cancelButtonText:"我再想想",
        confirmButtonColor: "#ec6c62",
        showLoaderOnConfirm: true
    }, function() {
        $.ajax({
            url: basePath + "erp/deleteothertailorbyorderbedpack",
            type:'POST',
            data: {
                orderName:$("#orderName").val(),
                bedNumber:$("#bedNumber").val(),
                userName:userName,
                packageNumberList:packageNumberList
            },
            traditional:true,
            success: function (data) {
                if(data == 0) {
                    $.unblockUI();
                    $("input").val("");
                    swal({
                            type:"success",
                            title:"",
                            text: "<span style=\"font-weight:bolder;font-size: 20px\">恭喜你，删除成功！</span>",
                            html: true
                        },
                        function(){
                            location.href=basePath+"erp/oneOtherTailorInfoStart";
                        });
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，删除失败！</span>",html: true});
                }
            },
            error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        })
    });
}

