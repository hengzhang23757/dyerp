var form;
var dom = document.getElementById("container");
var myChart = echarts.init(dom);
layui.use('form', function(){
    form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
    form.render();
});

var userRole = $("#userRole").val();
var userName = $("#userName").val();
layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.21'
}).extend({                         // 模块别名
    soulTable: 'soulTable'
});

$(document).ready(function () {
    form.render('select');
    $.ajax({
        url: "getallcheckgroupname",
        type: 'GET',
        data: {},
        success:function(res){
            if (res) {
                $("#groupName").empty();
                $("#groupName").append("<option value=''>全部</option>");
                $.each(res.data, function (index, item) {
                    $("#groupName").append("<option value='"+item+"'>"+item+"</option>");
                });
                form.render('select');
            }
        }, error: function () {
            layer.msg("获取数据失败", { icon: 2 });
        }
    });



    layui.laydate.render({
        elem: '#from',
        trigger: 'click'
    });
    layui.laydate.render({
        elem: '#to',
        trigger: 'click'
    });

    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumber", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });

    $("#progressDetailDiv").html("<p style='font-size: large; color: #1E9FFF; font-weight: bolder'>进度详情</p>");
    $("#employeeDetailDiv").html("<p style='font-size: large; color: #1E9FFF; font-weight: bolder'>员工详情</p>");
});

var reportTable,detailTable,employeeTable;
layui.use(['form', 'table', 'soulTable'], function () {
    var table = layui.table,
        soulTable = layui.soulTable,
        $ = layui.$;

    reportTable = table.render({
        elem: '#reportTable'
        , cols: [[]]
        , loading: true
        , data: []
        , height: '800'
        , title: '平衡表'
        , toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
        , totalRow: false
        , page: false
        , limit: 1000 //每页默认显示的数量
        , done: function (res, curr, count) {}
    });

    detailTable = table.render({
        elem: '#detailTable'
        , cols: [[]]
        , loading: true
        , data: []
        , height: '400'
        , title: '报表'
        , page: false
        , limit: 1000 //每页默认显示的数量
        , done: function (res, curr, count) {
        }
    });

    employeeTable = table.render({
        elem: '#employeeTable'
        , cols: [[]]
        , loading: true
        , data: []
        , height: '300'
        , title: '报表'
        , totalRow: true
        , page: false
        , limit: 1000 //每页默认显示的数量
        , done: function (res, curr, count) {
        }
    });

    form.on('submit(searchBeat)', function (data) {
        var from = $("#from").val();
        var to = $("#to").val();
        var groupName = $("#groupName").val();
        var orderName = $("#orderName").val();
        var clothesVersionNumber = $("#clothesVersionNumber").val();
        var load = layer.load(2);
        $.ajax({
            url: "getproductionprogressbyordertimegroup",
            type: 'GET',
            data: {
                orderName: orderName,
                clothesVersionNumber: clothesVersionNumber,
                from: from,
                to: to,
                groupName: groupName
            },
            success:function(data){
                if (data.productionProgressList) {
                    var reportData = data.productionProgressList;
                    var personData = data.person;
                    createProcedureBalanceFigure(personData);
                    detailTable = table.render({
                        elem: '#detailTable'
                        , cols: [[]]
                        , loading: true
                        , data: []
                        , height: '400'
                        , title: '报表'
                        , page: false
                        , limit: 1000 //每页默认显示的数量
                        , done: function (res, curr, count) {
                        }
                    });
                    employeeTable = table.render({
                        elem: '#employeeTable'
                        , cols: [[]]
                        , loading: true
                        , data: []
                        , height: '300'
                        , title: '报表'
                        , page: false
                        , limit: 1000 //每页默认显示的数量
                        , done: function (res, curr, count) {
                        }
                    });
                    table.render({
                        elem: '#reportTable'
                        ,cols:[[
                            {field:'procedureNumber', title:'工序号', align:'center', width:'16%', sort: true, filter: true, totalRowText: '合计'}
                            ,{field:'procedureName', title:'工序名', align:'center', width:'20%', sort: true, filter: true}
                            ,{field:'orderCount', title:'订单量', align:'center', width:'16%', sort: true, filter: true}
                            ,{field:'cutCount', title:'好片数', align:'center', width:'16%', sort: true, filter: true}
                            ,{field:'productionCount', title:'生产数', align:'center', width:'16%', sort: true, filter: true}
                            ,{field:'differenceCount', title:'差异', align:'center', width:'16%', sort: true, filter: true}
                        ]]
                        ,excel: {
                            filename: '报表.xlsx'
                        }
                        , toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                        , data: reportData   // 将新数据重新载入表格
                        , overflow: 'tips'
                        , height: '800'
                        , title: '报表'
                        , totalRow: false
                        , page: false
                        , limit: 1000 //每页默认显示的数量
                        ,done: function (res, curr, count) {
                            layer.close(load);
                        }
                    });
                }
            }, error: function () {
                layer.msg("获取数据失败", { icon: 2 });
            }
        });
        return false;



    });

    table.on('toolbar(reportTable)', function(obj) {
        if (obj.event === 'refresh') {
            reportTable.reload();
        }
    });


    table.on('row(reportTable)', function(obj){
        var data = obj.data;
        //标注选中样式
        $('div[lay-id="reportTable"]').find(".layui-table-body tr ").attr({ "style": "background:#FFFFFF" });//其他tr恢复原样
        $('div[lay-id="reportTable"]').find(obj.tr.selector).attr({ "style": "background:#5FB878" });//改变当前tr颜色
        var from = $("#from").val();
        var to = $("#to").val();
        var groupName = $("#groupName").val();
        var orderName = $("#orderName").val();
        var load = layer.load(2);
        if (data.procedureNumber == '0'){
            $.ajax({
                url: "getotherproductionprogressdetailbyordertime",
                type: 'GET',
                data: {
                    orderName: orderName,
                    from: from,
                    to: to,
                    procedureName: data.procedureName
                },
                success:function(res){
                    if (res.color) {
                        var colorData = res.color;
                        var sizeList = res.sizeList;
                        sizeList = globalSizeSort(sizeList);
                        var cols = [
                            {field:'color', title:'', align:'center', width:100}
                            ,{field:'type', title:'', align:'center', width:100}
                        ];
                        for (var i = 0; i < sizeList.length; i ++){
                            cols.push({field:sizeList[i], title: sizeList[i], align:'center', width:100})
                        }
                        cols.push({field:'summary', title: '合计', align:'center', width:100});
                        var reportData = [];
                        for (var color in colorData){
                            var colorSizeData = colorData[color];
                            var tmpData1 = {color: color, type: '订单量'};
                            var tmpData2 = {type: '好片数'};
                            var tmpData3 = {type: '生产量'};
                            var tmpData4 = {type: '未生产'};
                            var summary1 = 0;
                            var summary2 = 0;
                            var summary3 = 0;
                            var summary4 = 0;
                            for (var j = 0; j < sizeList.length; j ++){
                                var thisSize = sizeList[j];
                                tmpData1[thisSize] = colorSizeData[thisSize].orderCount;
                                tmpData2[thisSize] = colorSizeData[thisSize].cutCount;
                                tmpData3[thisSize] = colorSizeData[thisSize].productionCount;
                                tmpData4[thisSize] = colorSizeData[thisSize].leakCount;
                                summary1 += colorSizeData[thisSize].orderCount;
                                summary2 += colorSizeData[thisSize].cutCount;
                                summary3 += colorSizeData[thisSize].productionCount;
                                summary4 += colorSizeData[thisSize].leakCount;
                            }
                            tmpData1.summary = summary1;
                            tmpData2.summary = summary2;
                            tmpData3.summary = summary3;
                            tmpData4.summary = summary4;
                            reportData.push(tmpData1);
                            reportData.push(tmpData2);
                            reportData.push(tmpData3);
                            reportData.push(tmpData4);
                        }
                        table.render({
                            elem: '#detailTable'
                            ,cols: [cols]
                            ,data: reportData   // 将新数据重新载入表格
                            ,overflow: 'tips'
                            , loading: true
                            , height: '400'
                            , title: '报表'
                            , page: false
                            , limit: 1000 //每页默认显示的数量
                            ,done: function (res, curr, count) {
                                layer.close(load);
                            }
                        });
                        $("#progressDetailDiv").html("<p style='font-size: large; color: #1E9FFF; font-weight: bolder'>工序号:"+ data.procedureNumber +"&nbsp;&nbsp;工序名:"+ data.procedureName +"&nbsp;&nbsp;工序描述:"+ data.procedureDescription +"&nbsp;&nbsp;完成比例:"+ toDecimal((data.productionCount/data.cutCount)*100) +"%</p>");
                        $("#employeeDetailDiv").html("<p style='font-size: large; color: #1E9FFF; font-weight: bolder'>工序号:"+ data.procedureNumber +"&nbsp;&nbsp;工序名:"+ data.procedureName +"&nbsp;&nbsp;工序描述:"+ data.procedureDescription+ "</p>");
                    }
                }, error: function () {
                    layer.msg("获取数据失败", { icon: 2 });
                }
            });
            return false;
        } else {
            $.ajax({
                url: "getproductionprogressdetailbyordertimegroup",
                type: 'GET',
                data: {
                    orderName: orderName,
                    from: from,
                    to: to,
                    groupName: groupName,
                    procedureNumber: data.procedureNumber
                },
                success:function(res){
                    if (res.color) {
                        var colorData = res.color;
                        var employeeData = res.person;
                        var sizeList = res.sizeList;
                        sizeList = globalSizeSort(sizeList);
                        var cols = [
                            {field:'color', title:'', align:'center', width:100}
                            ,{field:'type', title:'', align:'center', width:100}
                        ];
                        for (var i = 0; i < sizeList.length; i ++){
                            cols.push({field:sizeList[i], title: sizeList[i], align:'center', width:100})
                        }
                        cols.push({field:'summary', title: '合计', align:'center', width:100});
                        var reportData = [];
                        for (var color in colorData){
                            var colorSizeData = colorData[color];
                            var tmpData1 = {color: color, type: '订单量'};
                            var tmpData2 = {type: '好片数'};
                            var tmpData3 = {type: '生产量'};
                            var tmpData4 = {type: '未生产'};
                            var summary1 = 0;
                            var summary2 = 0;
                            var summary3 = 0;
                            var summary4 = 0;
                            for (var j = 0; j < sizeList.length; j ++){
                                var thisSize = sizeList[j];
                                tmpData1[thisSize] = colorSizeData[thisSize].orderCount;
                                tmpData2[thisSize] = colorSizeData[thisSize].cutCount;
                                tmpData3[thisSize] = colorSizeData[thisSize].productionCount;
                                tmpData4[thisSize] = colorSizeData[thisSize].leakCount;
                                summary1 += colorSizeData[thisSize].orderCount;
                                summary2 += colorSizeData[thisSize].cutCount;
                                summary3 += colorSizeData[thisSize].productionCount;
                                summary4 += colorSizeData[thisSize].leakCount;
                            }
                            tmpData1.summary = summary1;
                            tmpData2.summary = summary2;
                            tmpData3.summary = summary3;
                            tmpData4.summary = summary4;
                            reportData.push(tmpData1);
                            reportData.push(tmpData2);
                            reportData.push(tmpData3);
                            reportData.push(tmpData4);
                        }
                        table.render({
                            elem: '#detailTable'
                            ,cols: [cols]
                            ,data: reportData   // 将新数据重新载入表格
                            ,overflow: 'tips'
                            , loading: true
                            , height: '400'
                            , title: '报表'
                            , page: false
                            , limit: 1000 //每页默认显示的数量
                            ,done: function (res, curr, count) {
                                soulTable.render(this);
                                layer.close(load);
                            }
                        });
                        $("#progressDetailDiv").html("<p style='font-size: large; color: #1E9FFF; font-weight: bolder'>工序号:"+ data.procedureNumber +"&nbsp;&nbsp;工序名:"+ data.procedureName +"&nbsp;&nbsp;工序描述:"+ data.procedureDescription +"&nbsp;&nbsp;完成比例:"+ toDecimal((data.productionCount/data.cutCount)*100) +"%</p>");
                        table.render({
                            elem: '#employeeTable'
                            ,cols: [[
                                {field:'groupName', title:'组名', align:'center', width:'25%', sort: true, filter: true, totalRowText: '合计'}
                                ,{field:'employeeNumber', title:'工号', align:'center', width:'25%', sort: true, filter: true}
                                ,{field:'employeeName', title:'姓名', align:'center', width:'25%', sort: true, filter: true}
                                ,{field:'productionCount', title:'数量', align:'center', width:'25%', sort: true, filter: true}
                            ]]
                            ,data: employeeData   // 将新数据重新载入表格
                            ,overflow: 'tips'
                            , loading: true
                            , height: '300'
                            , title: '报表'
                            , page: false
                            , limit: 1000 //每页默认显示的数量
                            ,done: function (res, curr, count) {
                                layer.close(load);
                            }
                        });
                        $("#employeeDetailDiv").html("<p style='font-size: large; color: #1E9FFF; font-weight: bolder'>工序号:"+ data.procedureNumber +"&nbsp;&nbsp;工序名:"+ data.procedureName +"&nbsp;&nbsp;工序描述:"+ data.procedureDescription+ "</p>");
                    }
                }, error: function () {
                    layer.msg("获取数据失败", { icon: 2 });
                }
            });
            return false;
        }
    });

});

function createProcedureBalanceFigure(data){
    var xSeries = [];
    var personSeries = [];
    for (var procedureKey in data){
        xSeries.push(procedureKey);
        var personData = data[procedureKey];
        for (var personKey in personData){
            if (!personSeries.includes(personKey)){
                personSeries.push(personKey);
            }
        }
    }
    var series = [];
    $.each(personSeries, function(index,element){
        var seriesObject={};
        seriesObject.name=element;
        seriesObject.type='bar';
        seriesObject.barWidth='20';
        seriesObject.stack='汇总';
        var seriesObjectData = [];
        for (var procedureKey in data){
            if (data[procedureKey][element] != null){
                seriesObjectData.push(data[procedureKey][element]);
            }else{
                seriesObjectData.push(0);
            }
        }
        seriesObject.data=seriesObjectData;
        series.push(seriesObject);
    });

    var app = {};
    option = null;
    app.title = '堆叠柱状图';

    option = {
        toolbox:{
            feature : {
                saveAsImage : {}
            }
        },
        tooltip : {
            trigger: 'axis',
            formatter: function(params){
                var resultText = '';
                resultText += params[0].name + "<br/>";
                for(var x in params){
                    if (params[x].value > 0){
                        resultText += params[x].seriesName +":"+params[x].value + "<br/>";
                    }else{
                        resultText += '';
                    }
                }
                return resultText;

            },
            axisPointer : {            // 坐标轴指示器，坐标轴触发有效
                type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
            }

        },
        grid: {
            left: '3%',
            right: '4%',
            bottom: '3%',
            containLabel: true
        },
        xAxis : [
            {
                type : 'category',
                data : xSeries,
                axisLabel:{
                    interval:0,
                    rotate:40
                }
            }
        ],
        yAxis : [
            {
                type : 'value'
            }
        ],
        series : series
    };
    if (series.length > 0){
        tools.loopShowTooltip(myChart, option, {loopSeries: true});
        if (option && typeof option === "object") {
            myChart.setOption(option, true);
        }
    }
    window.onresize = myChart.resize;
}

function toDecimal(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*100)/100;
    return f;
}