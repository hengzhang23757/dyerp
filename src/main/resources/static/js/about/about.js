var form;
layui.use('form', function(){
    form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
    form.render();
});
var userName = $("#userName").val();
var editor;
layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.21'
}).extend();
$(document).ready(function () {
    $("#contentDiv").height($(window).height() - 100);
    var E = window.wangEditor;
    editor = new E('#div1');
    editor.config.zIndex = 500;
    editor.config.height = 330;
    editor.config.showLinkImg = false;
    editor.config.menus = [
        'head',
        'bold',
        'fontSize',
        'fontName',
        'italic',
        'underline',
        'strikeThrough',
        'indent',
        'lineHeight',
        'foreColor',
        'backColor',
        'link',
        'list',
        'justify',
        'quote',
        'image',
        'table',
        'splitLine',
        'undo',
        'redo'
    ];
    editor.config.placeholder = '正文在这里写';
    editor.config.uploadImgShowBase64 = true;
    editor.config.uploadImgMaxSize = 2 * 1024 * 1024;
    editor.config.uploadImgMaxLength = 4;
    editor.create();


    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumber", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });
});


layui.use(['tree', 'util', 'form'], function(){
    var tree = layui.tree
        ,layer = layui.layer
        ,form = layui.form
        ,$ = layui.$
        ,util = layui.util;

    var checkData;
    //基本演示
    tree.render({
        elem: '#departmentUser'
        ,data: getData()
        ,showCheckbox: true  //是否显示复选框
        ,id: 'id'
        ,oncheck: function () {
            checkData = tree.getChecked('id');
        }
    });
    form.on('submit(commitData)', function(data){
        var editorContent = editor.txt.html();
        var message = [];
        if (checkData){
            for (var i = 0; i < checkData.length; i ++){
                if (checkData[i].children){
                    var tmpUser = checkData[i].children;
                    for (var j = 0; j < tmpUser.length; j++){
                        var tmpMessage = {};
                        tmpMessage.messageType = $("#messageType").val();
                        tmpMessage.title = $("#title").val();
                        tmpMessage.messageBodyOne = editorContent;
                        tmpMessage.createUser = userName;
                        tmpMessage.clothesVersionNumber = $("#clothesVersionNumber").val();
                        tmpMessage.orderName = $("#orderName").val();
                        tmpMessage.receiveUser = tmpUser[j].title;
                        tmpMessage.readType = "未读";
                        message.push(tmpMessage);
                    }
                }
            }
        }
        if (message.length == 0){
            layer.msg("请选择发送对象!");
            return false;
        }

        $.ajax({
            url: "/erp/addmessagebatch",
            type: 'POST',
            data: {
                messageJson: JSON.stringify(message)
            },
            success: function (res) {
                if (res.data == 0){
                    layer.msg("发送成功!");
                    $("#messageType").val('');
                    $("#title").val('');
                    $("#orderName").val('');
                    $("#clothesVersionNumber").val('');
                    editor.txt.clear();
                    form.render();
                } else {
                    layer.msg("发送失败!");
                }
            }, error: function () {
                layer.msg("获取失败！", {icon: 2});
            }
        });
        return false;
    });

});
//搜索节点值
$('#btn_query').click(function () {
    var name = $("#tree_name").val(); //搜索值
    var elem = $("#departmentUser").find('.layui-tree-txt').css('color', ''); //搜索文本与设置默认颜色
    if (!name) {
        return; //无搜索值返回
    }
    elem.filter(':contains(' + name + ')').css('color', '#FFB800'); //搜索文本并设置标志颜色
    elem.filter(':contains(' + name + ')').parents('.layui-tree-pack').prev().find('.layui-tree-iconClick').click(); //展开选项
    //console.log(elem);
})

function getData(){
    var resultData = [];
    $.ajax({
        url: "/erp/getalluser",    //后台数据请求地址
        type: "GET",
        data:{},
        async:false,
        success: function(resut){
            var userList = resut.data;
            var departmentLit = resut.departmentSet;
            for (var i = 0; i < departmentLit.length; i ++){
                var tmpDepart = {};
                tmpDepart['title'] = departmentLit[i];
                tmpDepart['id'] = i + 1;
                // tmpDepart['spread'] = true;
                resultData.push(tmpDepart)
            }
            for (var j = 0; j < resultData.length; j ++){
                var children = [];
                for (var k = 0; k < userList.length; k ++){
                    var tmpChildren = {};
                    if (resultData[j].title == userList[k].department){
                        tmpChildren['title'] = userList[k].userName;
                        tmpChildren['id'] = 100 + k;
                        tmpChildren['spread'] = true;
                        children.push(tmpChildren);
                    }
                }
                resultData[j].children = children;
            }
        }
    });
    return resultData;
}
