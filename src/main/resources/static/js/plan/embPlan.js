var userRole = $("#userRole").val();
$(document).ready(function () {
    $.ajax({
        url: "/erp/getonemonthembplan",
        type:'GET',
        data: {},
        success: function (data) {
            if(data) {
                createEmbPlanTable(data.oneMonth);
            }else {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，没有查到相关信息！</span>",html: true});
            }
        },
        error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }
    });

    layui.laydate.render({
        elem: "input[name='endDate0']", //指定元素
        trigger: 'click'
    });

    layui.laydate.render({
        elem: "input[name='endDateUpdate']", //指定元素
        trigger: 'click'
    });

    $("input[name='clothesVersionNumber0']").empty();
    layui.use(['autocomplete'], function () {
        layui.autocomplete.render({
            elem: $("input[name='clothesVersionNumber0']")[0],
            url: 'getversionhint',
            template_val: '{{d}}',
            template_txt: '{{d}}',
            onselect: function (resp) {
                autoComplete(resp,0)
            }
        })
    });

    layui.use(['autocomplete'], function () {
        layui.autocomplete.render({
            elem: $('#clothesVersionNumberUpdate')[0],
            url: 'getversionhint',
            template_val: '{{d}}',
            template_txt: '{{d}}',
            onselect: function (resp) {
                autoCompleteUpdate(resp)
            }
        })
    });


    $("select[name^='orderName']").change(function () {
        var orderName = $(this).val();
        var i = $(this).attr("name").replace("orderName","");
        $("select[name='colorName"+i+"']").empty();
        $("select[name='sizeName"+i+"']").empty();
        $.ajax({
            url: "/erp/getordercolornamesbyorder",
            data: {"orderName": orderName},
            success:function(data){
                $("select[name='colorName"+i+"']").empty();
                if (data.colorNameList) {
                    $("select[name='colorName"+i+"']").append("<option value='全部'>全部</option>");
                    $.each(data.colorNameList, function(index,element){
                        $("select[name='colorName"+i+"']").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });
        $.ajax({
            url: "/erp/getordersizenamesbyorder",
            data: {"orderName": orderName},
            success:function(data){
                $("select[name='sizeName"+i+"']").empty();
                if (data.sizeNameList) {
                    $("select[name='sizeName"+i+"']").append("<option value='全部'>全部</option>");
                    $.each(data.sizeNameList, function(index,element){
                        $("select[name='sizeName"+i+"']").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });
    });


});


function autoComplete(keywords,i){
    $("select[name='orderName"+i+"']").empty();
    $.ajax({
        url: "/erp/getorderbyversion",
        data: {"clothesVersionNumber": keywords},
        success:function(data){
            if (data.orderList) {
                $("select[name='orderName"+i+"']").append("<option value=''>订单</option>");
                $.each(data.orderList, function(index,element){
                    $("select[name='orderName"+i+"']").append("<option value='"+element+"'>"+element+"</option>");
                })
            }
        },
        error:function(){
        }
    });
}

function autoCompleteUpdate(keywords) {
    $("#orderNameUpdate").empty();
    $.ajax({
        url: "/erp/getorderbyversion",
        data: {"clothesVersionNumber": keywords},
        success:function(data){
            $("#orderNameUpdate").empty();
            if (data.orderList) {
                $("#orderNameUpdate").append("<option value=''>订单号</option>");
                $.each(data.orderList, function(index,element){
                    $("#orderNameUpdate").append("<option value='"+element+"'>"+element+"</option>");
                })
            }
        },
        error:function(){
        }
    });
}


var basePath=$("#basePath").val();
var embPlanTable;
function createEmbPlanTable(data) {
    if (embPlanTable != undefined) {
        embPlanTable.clear(); //清空一下table
        embPlanTable.destroy(); //还原初始化了的datatable
    }
    embPlanTable = $('#embPlanTable').DataTable({
        "retrieve": true,
        data: data,
        language : {
            processing : "载入中",//处理页面数据的时候的显示
            paginate : {//分页的样式文本内容。
                previous : "上一页",
                next : "下一页",
                first : "第一页",
                last : "最后一页"
            },
            search:"搜索：",
            zeroRecords : "没有内容",//table tbody内容为空时，tbody的内容。
            //下面三者构成了总体的左下角的内容。
            info : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//左下角的信息显示，大写的词为关键字。
            infoEmpty : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//筛选为空时左下角的显示。
            infoFiltered : ""//筛选之后的左下角筛选提示(另一个是分页信息显示，在上面的info中已经设置，所以可以不显示)，
        },
        pageLength : 30,// 每页显示10条数据
        pagingType : "full_numbers", // 分页样式：simple,simple_numbers,full,full_numbers，
        "paging": true,
        // "ordering" : false,
        "info": false,
        searching:true,
        lengthChange:false,
        scrollY: $(document.body).height() - 200,
        fixedHeader: true,

        "columns": [
            {
                "data": null,
                "title":"序号",
                "width":"6%",
                "defaultContent": "",
                "sClass": "text-center",
                "searchable":false,
                render: function (data, type, row, meta) {
                    var no = meta.settings._iDisplayStart + meta.row + 1;
                    return no;
                }
            },{
                "data": "groupName",
                "title":"组名",
                "width":"7%",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "clothesVersionNumber",
                "title":"版单号",
                "width":"10%",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "orderName",
                "title":"款号",
                "width":"10%",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "colorName",
                "title":"颜色",
                "width":"6%",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "sizeName",
                "title":"尺码",
                "width":"6%",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "planCount",
                "title":"数量",
                "width":"6%",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "driftPercent",
                "title":"上浮",
                "width":"6%",
                "defaultContent": "",
                "sClass": "text-center",
                "render": function(data, type, full, meta) {
                    return (data).toString() + "%";
                }
            }, {
                "data": "beginDate",
                "title":"开始",
                "width":"10%",
                "defaultContent": "",
                "sClass": "text-center",
                "render": function(data, type, full, meta) {
                    return  moment(data).format("YYYY-MM-DD HH:mm:ss");
                }
            }, {
                "data": "endDate",
                "title":"结束",
                "width":"10%",
                "defaultContent": "",
                "sClass": "text-center",
                "render": function(data, type, full, meta) {
                    return  moment(data).format("YYYY-MM-DD HH:mm:ss");
                }
            }, {
                "data": "actCount",
                "title": "达成",
                "width":"6%",
                "defaultContent": "",
                "sClass": "text-center"
            }, {
                "data": "achievePercent",
                "title": "达成率",
                "width":"6%",
                "defaultContent": "",
                "sClass": "text-center",
                "render": function(data, type, full, meta) {
                    return ((100*data).toFixed(2)).toString() + "%";
                }
            }, {
                "data": "embPlanID",
                "title": "操作",
                "width":"8%",
                "defaultContent": "",
                "sClass": "text-center",
            }
        ],
        "columnDefs" :
            [{
                "orderable" : false, // 禁用排序
                "targets" : [12], // 指定的列
                "data" : "embPlanID",
                "render" : function(data, type, full, meta) {
                    return "<a href='#' style='color:#ff0000' onclick='deleteEmbPlan("+data+")'>删除</a>&nbsp;&nbsp;&nbsp;<a href='#' style='color:#3e8eea' onclick='updateEmbPlan(this,"+data+")'>修改</a>";
                }
            }]

    });
}


function deleteEmbPlan(embPlanID) {
    swal({
        title: "",
        text: "<span style=\"font-weight:bolder;font-size: 20px\">您确定要删除吗？</span>",
        type: "warning",
        html:true,
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: "确定",
        cancelButtonText:"我再想想",
        confirmButtonColor: "#ec6c62",
        showLoaderOnConfirm: true
    }, function() {
        $.ajax({
            url: basePath + "erp/deleteembplan",
            type:'POST',
            data: {
                embPlanID:embPlanID
            },
            success: function (data) {
                if(data.success) {
                    $.unblockUI();
                    $("input").val("");
                    swal({
                            type:"success",
                            title:"",
                            text: "<span style=\"font-weight:bolder;font-size: 20px\">"+data.success+"</span>",
                            html: true
                        },
                        function(){
                            location.href=basePath+"erp/embPlanStart";
                        });
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">"+data.error+"</span>",html: true});
                }
            },
            error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        })
    });
}

var div_no = 1;
function addSize(obj) {
    var html = $("table[name='sizeDiv']:last").clone();
    html.find("select").each(function (index,item) {
        var name = $(item).attr("name").replace(/[0-9]/g,"");
        $(item).attr("name",name+div_no);
    });
    html.find("input").each(function (index,item) {
        var name = $(item).attr("name").replace(/[0-9]/g,"");
        $(item).attr("name",name+div_no);
    });

    $("#sizeTableDiv").append(html);

    $("input[name='endDate"+div_no+"']").attr("lay-key",div_no+1);

    layui.laydate.render({
        elem: "input[name='endDate"+div_no+"']", //指定元素
        trigger: 'click'
    });

    layui.use(['autocomplete'], function () {
        layui.autocomplete.render({
            elem: $("input[name='clothesVersionNumber"+ div_no +"']")[0],
            url: 'getversionhint',
            template_val: '{{d}}',
            template_txt: '{{d}}',
            onselect: function (resp) {
                var i = this.elem.attr("name").replace("clothesVersionNumber","");
                autoComplete(resp,i);
            }
        })
    });
    div_no = div_no+1;


    $("select[name^='orderName']").change(function () {
        var orderName = $(this).val();
        var i = $(this).attr("name").replace("orderName","");
        $("select[name='colorName"+i+"']").empty();
        $("select[name='sizeName"+i+"']").empty();
        $.ajax({
            url: "/erp/getordercolornamesbyorder",
            data: {"orderName": orderName},
            success:function(data){
                $("select[name='colorName"+i+"']").empty();
                if (data.colorNameList) {
                    $("select[name='colorName"+i+"']").append("<option value='全部'>全部</option>");
                    $.each(data.colorNameList, function(index,element){
                        $("select[name='colorName"+i+"']").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });
        $.ajax({
            url: "/erp/getordersizenamesbyorder",
            data: {"orderName": orderName},
            success:function(data){
                $("select[name='sizeName"+i+"']").empty();
                if (data.sizeNameList) {
                    $("select[name='sizeName"+i+"']").append("<option value='全部'>全部</option>");
                    $.each(data.sizeNameList, function(index,element){
                        $("select[name='sizeName"+i+"']").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });
    });


    var orderOfLayerArr = $('[name^="orderOfLayer"]');
    for (var i = 0; i < orderOfLayerArr.length; i++){
        $(orderOfLayerArr[i]).text(i + 1);
    }
    $("button[name='addSizeBtn']").remove();
    $("button[name='delSizeBtn']:first").before('<button name="addSizeBtn" class="btn" style="margin-bottom: 15px;outline:none;border-radius: 5px;color: white;" onclick="addSize(this)"><i class="fa fa-plus"></i></button>');
    $("button[name='delSizeBtn']:last").show();

}

function delSize(obj) {
    $(obj).parent().parent().parent().parent().remove();
    var orderOfLayerArr = $('[name="orderOfLayer"]');
    for (var i = 0; i < orderOfLayerArr.length; i++){
        $(orderOfLayerArr[i]).text(i + 1);
    }
}

var userRole=$("#userRole").val();
function addEmbPlan() {
    if(userRole!='root' && userRole!='role5'){
        swal("SORRY!", "对不起，您没有操作权限！", "warning");
        return;
    }
    $.blockUI({
        css: {
            width: '60%',
            top: '5%',
            left: '20%',
            border: 'none',
            padding: '15px',
            backgroundColor: '#fff',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: 1,
            color: '#000'
        },
        message: $('#editPro')
    });
    var url = basePath + "erp/addembplanbatch";
    $("#editYes").unbind("click").bind("click", function () {
        var flag = false;
        $("#editPro input").each(function (index, item) {
            if($(this).val() == "") {
                flag = true;
                return false;
            }
        });
        $("#editPro select").each(function (index, item) {
            if($(this).val() == "") {
                flag = true;
                return false;
            }
        });
        if(flag) {
            swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入完全部信息！</span>",html: true});
            return false;
        }
        var embPlanJson = [];
        $("select[name^='orderName']").each(function (index,item){
            var embPlan = {};
            var i = $(item).attr("name").replace("orderName","");
            embPlan.clothesVersionNumber = $("input[name='clothesVersionNumber"+i+"']").val();
            embPlan.orderName = $("select[name='orderName"+i+"']").val();
            embPlan.colorName = $("select[name='colorName"+i+"']").val();
            embPlan.sizeName = $("select[name='sizeName"+i+"']").val();
            embPlan.groupName = $("select[name='groupSelect"+i+"']").val();
            embPlan.endDate = $("input[name='endDate"+i+"']").val();
            embPlan.planCount = $("input[name='planCount"+i+"']").val();
            embPlan.driftPercent = $("input[name='driftPercent"+i+"']").val();
            embPlanJson.push(embPlan);
        });
        $.ajax({
            url: url,
            type: 'POST',
            data: {
                embPlanJson:JSON.stringify(embPlanJson)
            },
            success: function (data) {
                if(data.success) {
                    $.unblockUI();
                    $("input").val("");
                    swal({
                            type:"success",
                            title:"",
                            text: "<span style=\"font-weight:bolder;font-size: 20px\">"+data.success+"</span>",
                            html: true
                        },
                        function(){
                            location.href=basePath+"erp/embPlanStart";
                        });
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">"+data.error+"</span>",html: true});
                }
            },
            error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        });
    });
    $("#editNo").unbind("click").bind("click", function () {
        $.unblockUI();
        $("#editPro input").val("");

    });
}

function updateEmbPlan(obj,embPlanID){
    if(userRole!='root' && userRole!='role5'){
        swal("SORRY!", "对不起，您没有操作权限！", "warning");
        return;
    }
    $("#groupSelectUpdate").val($(obj).parent().parent().find("td").eq(1).text());
    $("#clothesVersionNumberUpdate").val($(obj).parent().parent().find("td").eq(2).text());
    $("#orderNameUpdate").val($(obj).parent().parent().find("td").eq(3).text());
    $("#endDateUpdate").val(moment($(obj).parent().parent().find("td").eq(9).text()).format("YYYY-MM-DD"));
    $("#colorNameUpdate").val($(obj).parent().parent().find("td").eq(4).text());
    $("#sizeNameUpdate").val($(obj).parent().parent().find("td").eq(5).text());
    $("#planCountUpdate").val($(obj).parent().parent().find("td").eq(6).text());
    var tmpPercent = $(obj).parent().parent().find("td").eq(7).text();
    var actCount = $(obj).parent().parent().find("td").eq(10).text();
    $("#driftPercentUpdate").val(tmpPercent.substring(0, tmpPercent.length-1));
    $.blockUI({
        css: {
            width: '50%',
            top: '15%',
            left: '25%',
            border: 'none',
            padding: '15px',
            backgroundColor: '#fff',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: 1,
            color: '#000'
        },
        message: $('#updatePro')
    });

    $("#updateYes").unbind("click").bind("click", function () {
        var flag = false;
        if($("#planCountUpdate").val() == "" || $("#planCountUpdate").val() == null){
            flag = true;
        }
        if($("#driftPercentUpdate").val() == "" || $("#driftPercentUpdate").val() == null){
            flag = true;
        }
        if(flag) {
            swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入完全部信息！</span>",html: true});
            return false;
        }
        var embPlanJson = {};
        embPlanJson.embPlanID = embPlanID;
        embPlanJson.groupName = $("#groupSelectUpdate").val();
        embPlanJson.endDate = $("#endDateUpdate").val();
        embPlanJson.orderName = $("#orderNameUpdate").val();
        embPlanJson.colorName = $("#colorNameUpdate").val();
        embPlanJson.sizeName = $("#sizeNameUpdate").val();
        embPlanJson.clothesVersionNumber = $("#clothesVersionNumberUpdate").val();
        embPlanJson.planCount = $("#planCountUpdate").val();
        embPlanJson.driftPercent = $("#driftPercentUpdate").val();
        embPlanJson.actCount = actCount;
        embPlanJson.achievePercent = toDecimalFour(actCount/$("#planCountUpdate").val());

        $.ajax({
            url: basePath + "erp/updateembplan",
            type:'POST',
            data: {
                embPlanJson:JSON.stringify(embPlanJson)
            },
            success: function (data) {
                if(data.success) {
                    $.unblockUI();
                    $("#updatePro input").val("");
                    swal({
                            type:"success",
                            title:"",
                            text: "<span style=\"font-weight:bolder;font-size: 20px\">"+data.success+"</span>",
                            html: true
                        },
                        function(){
                            location.href=basePath+"erp/embPlanStart";
                        });
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">"+data.error+"</span>",html: true});
                }
            },
            error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        })
    });
    $("#updateNo").unbind("click").bind("click", function () {
        $.unblockUI();
        $("#updatePro input").val("");
    });
}

function toDecimalFour(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*10000)/10000;
    return f;
}

