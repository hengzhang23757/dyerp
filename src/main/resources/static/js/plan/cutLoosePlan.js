layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.21'
}).extend({                         // 模块别名
    soulTable: 'soulTable'
});

var form = layui.form;
var reportTable;
var colorStr = '';
var cxm;
$(document).ready(function () {
    cxm = xmSelect.render({
        // 这里绑定css选择器
        el: '#xmColor',
        on: function(data){
            colorStr = '';
            $.each(data.arr, function (index, item) {
                colorStr += (item.value + ",");
            });
            colorStr = colorStr.substring(0, colorStr.length - 1);
            var orderName = $("#orderName").val();
            $.ajax({
                url: "/erp/getplaninfobyordercolor",
                data: {
                    "orderName": orderName,
                    "colorName": colorStr
                },
                success:function(data){
                    $("#inStoreBatch").val(data.inStoreBatch);
                    $("#storageBatch").val(data.storageBatch);
                    $("#preCutBatch").val(data.preCutBatch);
                    $("#orderCount").val(data.orderCount);
                    $("#cutCount").val(data.cutCount);
                },
                error:function(){
                }
            });
        },
        // 渲染的数据
        data: []
    });
    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumber", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });

    layui.laydate.render({
        elem: "#beginDate", //指定元素
        trigger: 'click'
    });
    layui.laydate.render({
        elem: "#endDate", //指定元素
        trigger: 'click'
    });
    layui.laydate.render({
        elem: "#returnDate", //指定元素
        trigger: 'click'
    });

    $("#clothesVersionNumber").bind('blur change',function(){
        var orderName = $("#orderName").val();
        $.ajax({
            url: "/erp/getordercolornamesbyorder",
            data: {
                "orderName": orderName
            },
            success:function(data){
                var colorData = [];
                if (data.colorNameList){
                    $.each(data.colorNameList, function (index, item) {
                        colorData.push({name: item, value: item, selected: false});
                    })
                }
                cxm.update({
                    data: colorData
                });
            }, error:function(){
            }
        });
    });

    $("#orderName").bind('blur change',function(){
        var orderName = $("#orderName").val();
        $.ajax({
            url: "/erp/getordercolornamesbyorder",
            data: {
                "orderName": orderName
            },
            success:function(data){
                var colorData = [];
                if (data.colorNameList){
                    $.each(data.colorNameList, function (index, item) {
                        colorData.push({name: item, value: item, selected: false});
                    })
                }
                cxm.update({
                    data: colorData
                });
            }, error:function(){
            }
        });
    });
});

layui.use(['form', 'soulTable', 'table','yutons_sug'], function () {

    var table = layui.table,
        soulTable = layui.soulTable,
        $ = layui.$;
    var load = layer.load();
    form.render();


    reportTable = table.render({
        elem: '#reportTable'
        ,url:'getunfinishedcutlooseplan'
        ,cols: [[]]
        ,loading:true
        ,done: function (res, curr, count) {
            table.init('reportTable',{//转换成静态表格
                cols:[[
                    {type:'numbers', align:'center', title:'序号', width:'3%'}
                    ,{field:'clothesVersionNumber', title:'单号', align:'center', width:'8%', sort: true, filter: true}
                    ,{field:'orderName', title:'款号', align:'center', width:'8%', sort: true, filter: true}
                    ,{field:'colorName', title:'颜色', align:'center', width:'10%', sort: true, filter: true}
                    ,{field:'orderCount', title:'订单数', align:'center', width:'6%', sort: true, filter: true}
                    ,{field:'cutCount', title:'已裁', align:'center', width:'6%', sort: true, filter: true}
                    ,{field:'beginDate', title:'计划开始', align:'center', width:'9%', sort: true, filter: true, templet:function (d) {
                            return moment(d.beginDate).format("YYYY-MM-DD");
                        }}
                    ,{field:'endDate', title:'预计完成', align:'center', width:'9%', sort: true, filter: true, templet:function (d) {
                            return moment(d.endDate).format("YYYY-MM-DD");
                        }}
                    ,{field:'inStoreBatch', title:'入库(卷)', align:'center',width:'6%', sort: true, filter: true}
                    ,{field:'storageBatch', title:'库存(卷)', align:'center',width:'6%', sort: true, filter: true}
                    ,{field:'preCutBatch', title:'已松待裁(卷)', align:'center',width:'7%', sort: true, filter: true}
                    ,{field:'finishState', title:'是否完成', align:'center',width:'6%', sort: true, filter: true}
                    ,{field:'printingPart', title:'印绣花部位', align:'center',width:'7%', sort: true, filter: true}
                    ,{field:'printingFactory', title:'印绣花厂', align:'center',width:'7%', sort: true, filter: true}
                    ,{field:'returnDate', title:'回厂日期', align:'center', width:'9%', sort: true, filter: true, templet:function (d) {
                            return moment(d.returnDate).format("YYYY-MM-DD");
                        }}
                    ,{field:'remark', title:'备注', align:'center',width:'9%', sort: true, filter: true}
                    ,{fixed: 'right', field: 'id', title:'操作', align:'center', toolbar: '#barTop', width:'8%'}
                ]]
                ,data:res.data
                ,height: 'full-50'
                ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                ,defaultToolbar: ['filter', 'print']
                ,title: '计划总表'
                ,totalRow: true
                ,page: true
                ,overflow: 'tips'
                ,limits: [50, 100, 200]
                ,limit: 100 //每页默认显示的数量
                ,done: function () {
                    soulTable.render(this);
                    layer.close(load);
                }
                ,filter: {
                    bottom: true,
                    items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                }
            });
        }
    });

    table.on('toolbar(reportTable)', function(obj){
        if (obj.event === 'clearFilter') {
            // 清除所有筛选条件并重载表格
            // 参数: tableId
            soulTable.clearFilter('reportTable')
        }else if (obj.event === 'refresh') {
            reportTable.reload();
        } else if (obj.event === 'exportExcel') {
            soulTable.export('reportTable');
        } else if(obj.event === 'addCutLoosePlan') {  //录入基础信息
            var index = layer.open({
                type: 1 //Page层类型
                , title: "录入计划"
                , btn: ['保存']
                , shade: 0.6 //遮罩透明度
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , content: $("#cutLoosePlanAdd")
                ,yes: function(i, layero){
                    var cutLoosePlanJson = {};
                    cutLoosePlanJson.clothesVersionNumber = $("#clothesVersionNumber").val();
                    cutLoosePlanJson.orderName = $("#orderName").val();
                    cutLoosePlanJson.colorName = colorStr;
                    cutLoosePlanJson.orderCount = $("#orderCount").val();
                    cutLoosePlanJson.cutCount = $("#cutCount").val();
                    cutLoosePlanJson.inStoreBatch = $("#inStoreBatch").val();
                    cutLoosePlanJson.preCutBatch = $("#preCutBatch").val();
                    cutLoosePlanJson.storageBatch = $("#storageBatch").val();
                    cutLoosePlanJson.beginDate = $("#beginDate").val();
                    cutLoosePlanJson.endDate = $("#endDate").val();
                    cutLoosePlanJson.returnDate = $("#returnDate").val();
                    cutLoosePlanJson.printingPart = $("#printingPart").val();
                    cutLoosePlanJson.printingFactory = $("#printingFactory").val();
                    cutLoosePlanJson.remark = $("#remark").val();
                    cutLoosePlanJson.finishState = "未完成";
                    $.ajax({
                        url: "/erp/addorupdatecutlooseplan",
                        type: 'POST',
                        data: {
                            cutLoosePlanJson: JSON.stringify(cutLoosePlanJson)
                        },
                        success: function (res) {
                            if (res.result == 0){
                                layer.msg("录入成功！", {icon: 1});
                                reportTable.reload();
                                $("#orderCount").val("");
                                $("#cutCount").val("");
                                $("#inStoreBatch").val("");
                                $("#preCutBatch").val("");
                                $("#storageBatch").val("");
                                $("#beginDate").val("");
                                $("#endDate").val("");
                                $("#clothesVersionNumber").val("");
                                $("#orderName").val("");
                                $("#printingPart").val("");
                                $("#returnDate").val("");
                                $("#remark").val("");
                                cxm.update({
                                    data: []
                                });
                                layer.close(index);
                            } else {
                                layer.msg("录入失败！", {icon: 2});
                            }
                        }, error: function () {
                            layer.msg("录入失败！", {icon: 2});
                        }
                    });
                    return false;
                }, cancel : function (i,layero) {
                    $("#orderCount").val("");
                    $("#cutCount").val("");
                    $("#inStoreBatch").val("");
                    $("#preCutBatch").val("");
                    $("#storageBatch").val("");
                    $("#beginDate").val("");
                    $("#endDate").val("");
                    $("#clothesVersionNumber").val("");
                    $("#orderName").val("");
                    $("#printingPart").val("");
                    $("#returnDate").val("");
                    $("#remark").val("");
                    cxm.update({
                        data: []
                    });
                }
            });
            layer.full(index);
        } else if (obj.event === 'finishData') {
            $.ajax({
                url: "/erp/getfinishedcutlooseplan",
                data: {},
                success:function(data){
                    table.reload("reportTable", {
                        data: data.data   // 将新数据重新载入表格
                    });
                }, error:function(){
                }
            });

        } else if (obj.event === 'unFinishData') {
            $.ajax({
                url: "/erp/getunfinishedcutlooseplan",
                data: {},
                success:function(data){
                    table.reload("reportTable", {
                        data: data.data   // 将新数据重新载入表格
                    });
                }, error:function(){
                }
            });

        }
    });

    //监听行工具事件
    table.on('tool(reportTable)', function(obj){
        var data = obj.data;
        if(obj.event === 'del'){
            layer.confirm('真的删除吗', function(index){
                $.ajax({
                    url: "/erp/deletecutlooseplan",
                    type: 'POST',
                    data: {
                        id: data.id
                    },
                    success: function (res) {
                        if (res.result == 0) {
                            reportTable.reload();
                            layer.close(index);
                            layer.msg("删除成功！", {icon: 1});
                        } else {
                            layer.msg("删除失败！", {icon: 2});
                        }
                    }, error: function () {
                        layer.msg("删除失败！", {icon: 2});
                    }
                })
            });
        } else if(obj.event === 'finish'){
            layer.confirm('确认完成?', function(index){
                $.ajax({
                    url: "/erp/updatefinishstate",
                    type: 'POST',
                    data: {
                        id: data.id
                    },
                    success: function (res) {
                        if (res.result == 0) {
                            reportTable.reload();
                            layer.close(index);
                            layer.msg("确认成功！", {icon: 1});
                        } else {
                            layer.msg("确认失败！", {icon: 2});
                        }
                    }, error: function () {
                        layer.msg("确认失败！", {icon: 2});
                    }
                })

            });
        }
    });

});


function toDecimal(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*100)/100;
    return f;
}