var form;
layui.use('form', function(){
    form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
    form.render();
});
layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.21'
}).extend({
    soulTable: 'soulTable'
});
$(document).ready(function () {

    layui.laydate.render({
        elem: '#from',
        trigger: 'click',
        value: getFormatDate()
    });
    layui.laydate.render({
        elem: '#to',
        trigger: 'click',
        value: getFormatDate()
    });
    form.render('select');
    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumber", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });
});

var dataTable;

layui.use(['form', 'table', 'soulTable'], function () {
    var table = layui.table,
        soulTable = layui.soulTable,
        $ = layui.$;

    dataTable = table.render({
        elem: '#dataTable'
        ,cols:[[]]
        ,loading:true
        ,data:[]
        ,height: 'full-130'
        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
        ,defaultToolbar: ['filter', 'print']
        ,title: '产前进度报表'
        ,totalRow: true
        ,page: true
        ,overflow: 'tips'
        ,limits: [500, 1000, 2000]
        ,limit: 1000 //每页默认显示的数量
        ,done: function () {
            soulTable.render(this);
        }
        ,filter: {
            bottom: true,
            items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
        }
    });

    var from = $("#from").val();
    var to = $("#to").val();
    var orderName = $("#orderName").val();
    var clothesVersionNumber = $("#clothesVersionNumber").val();

    initTable(from, to, orderName, clothesVersionNumber);

    function initTable(from, to, orderName, clothesVersionNumber){
        var param = {};
        if (from != null && from != ''){
            param.from = from;
        }
        if (to != null && to != ''){
            param.to = to;
        }
        if (orderName != null && orderName != ''){
            param.orderName = orderName;
        }
        if (clothesVersionNumber != null && clothesVersionNumber != ''){
            param.clothesVersionNumber = clothesVersionNumber;
        }
        var load = layer.load();
        $.ajax({
            url: "/erp/getprenatalprogresssummary",
            type: 'GET',
            data: param,
            success: function (res) {
                if (res.prenatalProgressList) {
                    var reportData = res.prenatalProgressList;
                    dataTable = table.render({
                        elem: '#dataTable'
                        ,cols:[[
                            {type:'numbers', align:'center', title:'序号', width:60}
                            ,{field:'clothesVersionNumber', title:'单号', align:'center', width:150, sort: true, filter: true, totalRowText: '合计'}
                            ,{field:'orderName', title:'款号', align:'center', width:150, sort: true, filter: true}
                            ,{field:'colorName', title:'颜色', align:'center', width:120, sort: true, filter: true}
                            ,{field:'partName', title:'部位', align:'center', width:120, sort: true, filter: true}
                            ,{field:'orderCount', title:'订单数', align:'center', width:90, sort: true, filter: true, totalRow: true,excel: {cellType: 'n'}}
                            ,{field:'wellCount', title:'好片', align:'center', width:90, sort: true, filter: true, totalRow: true,excel: {cellType: 'n'}}
                            ,{field:'cutDiff', title:'裁数差', align:'center', width:90, sort: true, filter: true, totalRow: true,excel: {cellType: 'n'}}
                            ,{field:'looseCount', title:'松布未裁(卷)', align:'center', width:120, sort: true, filter: true, totalRow: true,excel: {cellType: 'n'}}
                            ,{field:'opaCount', title:'花片外发', align:'center', width:120, sort: true, filter: true, totalRow: true,excel: {cellType: 'n'}}
                            ,{field:'opaBackCount', title:'花片回厂', align:'center', width:120, sort: true, filter: true, totalRow: true,excel: {cellType: 'n'}}
                            ,{field:'opaDiff', title:'花片差异', align:'center', width:120, sort: true, filter: true, totalRow: true,excel: {cellType: 'n'}}
                            ,{field:'matchCount', title:'配扎', align:'center', width:90, sort: true, filter: true, totalRow: true,excel: {cellType: 'n'}}
                            ,{field:'embInStore', title:'衣胚入库', align:'center', width:130, sort: true, filter: true, totalRow: true,excel: {cellType: 'n'}}
                            ,{field:'unloadCount', title:'下车间', align:'center', width:90, sort: true, filter: true, totalRow: true,excel: {cellType: 'n'}}
                            ,{field:'embDiff', title:'衣胚库存', align:'center', width:120, sort: true, filter: true, totalRow: true,excel: {cellType: 'n'}}
                            ,{field:'hangCount', title:'挂片数', align:'center', width:120, sort: true, filter: true, totalRow: true,excel: {cellType: 'n'}}
                            ,{fixed: 'right', title:'操作', align:'center', toolbar: '#barTop', width:120}
                        ]]
                        ,loading:false
                        ,height: 'full-130'
                        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                        ,defaultToolbar: ['filter', 'print']
                        ,title: '产前进度报表'
                        ,totalRow: true
                        ,page: true
                        ,overflow: 'tips'
                        ,limits: [500, 1000, 2000]
                        ,limit: 1000 //每页默认显示的数量
                        ,data: reportData   // 将新数据重新载入表格
                        ,excel: {
                            filename: '产前数据统计.xlsx',
                            add: {
                                top: {
                                    data: [
                                        ['产前数据统计'] //头部第一行数据，由于我设置了后面的数据merge了，就只写一个
                                    ],
                                    heights: [30,15],
                                    merge: [['1,1','1,10'],['2,1','2,3']]
                                }
                            }
                        }
                        ,done: function () {
                            soulTable.render(this);
                            layer.close(load);
                        }
                    });
                } else {
                    layer.msg("获取失败！", {icon: 2});
                }
            }, error: function () {
                layer.msg("获取失败！", {icon: 2});
            }
        });
        return false;
    }

    //监听提交
    form.on('submit(searchData)', function(data){
        clothesVersionNumber = $("#clothesVersionNumber").val();
        orderName = $("#orderName").val();
        from = $("#from").val();
        to = $("#to").val();
        if ((orderName == null || orderName === "") && (from == null || from === "" || to == null || to === "")){
            layer.msg("填写必要信息", { icon: 2 });
            return false;
        }
        initTable(from, to, orderName, clothesVersionNumber);
        return false;
    });

    table.on('toolbar(dataTable)', function(obj) {
        if (obj.event === 'clearFilter') {
            // 清除所有筛选条件并重载表格
            // 参数: tableId
            soulTable.clearFilter('dataTable')
        } else if (obj.event === 'exportExcel') {
            soulTable.export('dataTable');
        }
    });

    table.on('tool(dataTable)', function(obj){
        var data = obj.data;
        var thisOrderName = data.orderName;
        var thisColorName = data.colorName;
        var thisClothesVersionNumber = data.clothesVersionNumber;
        if(obj.event === 'fabricDetail'){
            var index = layer.open({
                type: 1 //Page层类型
                , title: '面料详情'
                , btn: ['下载/打印']
                , shade: 0.6 //遮罩透明度
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , content: "<div id='materialDetail'></div><iframe id='printf' src='' width='0' height='0' frameborder='0'></iframe>"
                , yes: function () {
                    $("#printf").empty();
                    var printBoxs = document.getElementsByName('printTable');
                    var newContent = '';
                    for(var i=0;i<printBoxs.length;i++) {
                        newContent += printBoxs[i].innerHTML;
                    }
                    var f = document.getElementById('printf');
                    // f.contentDocument.write('<link rel="stylesheet" type="text/css" href="/css/app.v2.css">');
                    f.contentDocument.write('<style>' +
                        'table {' +
                        'border-collapse:collapse;' +
                        '}' +
                        'td {\n' +
                        '        border: 1px solid #000000;\n' +
                        '    }'+
                        '</style>');
                    f.contentDocument.write(newContent);
                    f.contentDocument.close();
                    window.frames['printf'].focus();
                    try{
                        window.frames['printf'].print();
                    }catch(err){
                        f.contentWindow.print();
                    }
                }
                , cancel : function (i,layero) {
                    $("#materialDetail").empty();
                }
            });
            layer.full(index);
            setTimeout(function () {
                $.ajax({
                    url: "searchfabricleakbyorder",
                    type:'GET',
                    data: {
                        orderName: thisOrderName,
                        colorName: thisColorName
                    },
                    success: function (data) {
                        var tableHtml = "";
                        $("#materialDetail").empty();
                        if(data) {
                            tableHtml = "<div class=\"col-md-12\" name='printTable'>\n" +
                                "               <section class=\"panel panel-default\" style='page-break-after:always;'>\n" +
                                "                   <header class=\"panel-heading font-bold\" style=\"text-align: center\">\n" +
                                "                       <div style=\"font-size: xx-large;font-weight: bolder\">德悦服饰单款面料报表</div><br>\n" +
                                "                       <div style=\"font-size: larger;font-weight: bolder; height: 30px\">" +  "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;版单:"+thisClothesVersionNumber+"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;订单:"+thisOrderName+"</div>\n"+
                                "                   </header>\n" +
                                "                   <table class=\"table table-bordered\" style='width: 100%'>\n" +
                                "                       <tbody>";

                            tableHtml += "<tr><td colspan='10' style='text-align: center; font-size: larger; font-weight: bolder; color: black'>面料回布汇总</td></tr>";
                            tableHtml += "<tr style='font-size: larger; font-weight: bolder color: black'><td style='width: 10%'>供应商</td><td style='width: 8%'>订单颜色</td><td style='width: 23%'>面料名称</td><td style='width: 10%'>面料颜色</td><td style='width: 10%'>面料用途</td><td style='width: 8%'>订布数量</td><td style='width: 8%'>单位</td><td style='width: 8%'>回布数量</td><td style='width: 8%'>差异</td><td style='width: 10%'>回布卷数</td></tr>";
                            var orderSum = 0;
                            var actualSum = 0;
                            var differenceSum = 0;
                            var batchSum = 0;

                            if (data["fabricDifferenceList"]){
                                var differenceData = data["fabricDifferenceList"];
                                for (var i=0;i<differenceData.length;i++){
                                    tableHtml += "<tr style='font-size: larger; font-weight: bolder color: black'><td>"+differenceData[i].supplier+"</td><td>"+differenceData[i].colorName+"</td><td>"+differenceData[i].fabricName+"</td><td>"+differenceData[i].fabricColor+"</td><td>"+differenceData[i].fabricUse+"</td><td>"+toDecimal(differenceData[i].orderCount)+"</td><td>"+differenceData[i].unit+"</td><td>"+toDecimal(differenceData[i].actualCount)+"</td>";
                                    if (differenceData[i].differenceCount >= 0){
                                        tableHtml += "<td style='background-color: #5FB878'>"+toDecimal(differenceData[i].differenceCount)+"</td><td>"+differenceData[i].batchNumber+"</td></tr>";
                                    } else {
                                        tableHtml += "<td style='background-color: #FF5722'>"+toDecimal(differenceData[i].differenceCount)+"</td><td>"+differenceData[i].batchNumber+"</td></tr>";
                                    }
                                    var differenceRow = [];
                                    differenceRow.push(differenceData[i].fabricName);
                                    differenceRow.push(differenceData[i].supplier);
                                    differenceRow.push(differenceData[i].colorName);
                                    differenceRow.push(differenceData[i].fabricColor);
                                    differenceRow.push(differenceData[i].fabricUse);
                                    differenceRow.push(toDecimal(differenceData[i].orderCount));
                                    differenceRow.push(differenceData[i].unit);
                                    differenceRow.push(toDecimal(differenceData[i].actualCount));
                                    differenceRow.push(toDecimal(differenceData[i].differenceCount));
                                    differenceRow.push(differenceData[i].batchNumber);
                                    orderSum += differenceData[i].orderCount;
                                    actualSum += differenceData[i].actualCount;
                                    differenceSum += differenceData[i].differenceCount;
                                    batchSum += differenceData[i].batchNumber;
                                }
                            }
                            tableHtml += "<tr style='font-size: larger; font-weight: bolder color: black'><td colspan='5' style='text-align:center'>小计：</td><td>"+toDecimal(orderSum)+"</td><td></td><td>"+toDecimal(actualSum)+"</td><td>"+toDecimal(differenceSum)+"</td><td>"+batchSum+"</td></tr>";
                            var detailWeightSum = 0;
                            var detailBatchSum = 0;
                            tableHtml += "<tr style='font-size: larger; font-weight: bolder color: black'><td colspan='10' style='text-align: center; font-weight: 700; color: black'>回布详情</td></tr>";
                            tableHtml += "<tr style='font-size: larger; font-weight: bolder color: black'><td >日期</td><td>订单颜色</td><td>面料名称</td><td>面料颜色</td><td>回布数量</td><td>单位</td><td>回布卷数</td><td>缸号</td><td>位置</td><td>操作类型</td></tr>";
                            if (data["fabricDetailList"]){
                                var detailData = data["fabricDetailList"];
                                for (var i=0;i<detailData.length;i++){
                                    detailData[i].returnTime = moment(detailData[i].returnTime).format("YYYY-MM-DD");
                                }
                                detailData.sort(function(a,b){
                                    return Date.parse(b.returnTime) - Date.parse(a.returnTime);
                                });
                                var lastDate = detailData[0].returnTime;
                                for (var i=0;i<detailData.length;i++){
                                    if (detailData[i].returnTime == lastDate){
                                        tableHtml += "<tr style='font-size: larger; font-weight: bolder color: black'><td>"+moment(detailData[i].returnTime).format("YYYY-MM-DD")+"<sup id='supText' style='font-weight: bolder; font-style:italic; font-size: large; color: red'>new</sup></td><td>"+detailData[i].colorName+"</td><td>"+detailData[i].fabricName+"</td><td>"+detailData[i].fabricColor+"</td><td>"+detailData[i].weight+"</td><td>"+detailData[i].unit+"</td><td>"+detailData[i].batchNumber+"</td><td>"+detailData[i].jarName+"</td><td>"+detailData[i].location+"</td><td>"+detailData[i].operateType+"</td></tr>";
                                    } else {
                                        tableHtml += "<tr style='font-size: larger; font-weight: bolder color: black'><td>"+moment(detailData[i].returnTime).format("YYYY-MM-DD")+"</td><td>"+detailData[i].colorName+"</td><td>"+detailData[i].fabricName+"</td><td>"+detailData[i].fabricColor+"</td><td>"+detailData[i].weight+"</td><td>"+detailData[i].unit+"</td><td>"+detailData[i].batchNumber+"</td><td>"+detailData[i].jarName+"</td><td>"+detailData[i].location+"</td><td>"+detailData[i].operateType+"</td></tr>";
                                    }

                                    var detailRow = [];
                                    detailRow.push(moment(detailData[i].returnTime).format("YYYY-MM-DD"));
                                    detailRow.push(detailData[i].colorName);
                                    detailRow.push(detailData[i].fabricName);
                                    detailRow.push(detailData[i].fabricColor);
                                    detailRow.push(detailData[i].weight);
                                    detailRow.push(detailData[i].unit);
                                    detailRow.push(detailData[i].batchNumber);
                                    detailRow.push(detailData[i].jarName);
                                    detailRow.push(detailData[i].location);
                                    detailRow.push(detailData[i].operateType);
                                    detailWeightSum += detailData[i].weight;
                                    detailBatchSum += detailData[i].batchNumber;
                                }
                            }
                            tableHtml += "<tr style='font-size: larger; font-weight: bolder color: black'><td colspan='4' style='text-align:center'>小计：</td><td>"+toDecimal(detailWeightSum)+"</td><td></td><td>"+detailBatchSum+"</td><td></td><td></td><td></td></tr>";
                            tableHtml += "<tr style='font-size: larger; font-weight: bolder color: black'><td colspan='10' style='text-align: center; font-weight: 700; color: black;'>出库汇总</td></tr>";
                            tableHtml += "<tr style='font-size: larger; font-weight: bolder color: black'><td>出库日期</td><td>订单颜色</td><td>面料名称</td><td>面料颜色</td><td>出库数量</td><td>出库卷数</td><td>缸号</td><td>位置</td><td>出库类型</td><td></td></tr>";
                            var outCountSum = 0;
                            var outBatchSum = 0;
                            if (data["fabricOutRecordList"]){
                                var outData = data["fabricOutRecordList"];
                                var outHead=[["出库日期"],["订单颜色"]["面料名称"],["面料颜色"],["出库数量"],["出库卷数"],["缸号"],["位置"],["出库类型"]];
                                for (var j=0;j<outData.length;j++){
                                    tableHtml += "<tr style='font-size: larger; font-weight: bolder color: black'><td>"+moment(outData[j].returnTime).format("YYYY-MM-DD")+"</td><td>"+outData[j].colorName+"</td><td>"+outData[j].fabricName+"</td><td>"+outData[j].fabricColor+"</td><td>"+toDecimal(outData[j].weight)+"</td><td>"+outData[j].batchNumber+"</td><td>"+outData[j].jarName+"</td><td>"+outData[j].location+"</td><td>"+outData[j].operateType+"</td><td></td></tr>";
                                    var outRow = [];
                                    outRow.push(moment(outData[j].returnTime).format("YYYY-MM-DD"));
                                    outRow.push(outData[j].colorName);
                                    outRow.push(outData[j].fabricName);
                                    outRow.push(outData[j].fabricColor);
                                    outRow.push(toDecimal(outData[j].weight));
                                    outRow.push(outData[j].batchNumber);
                                    outRow.push(outData[j].jarName);
                                    outRow.push(outData[j].location);
                                    outRow.push(outData[j].operateType);
                                    outCountSum += outData[j].weight;
                                    outBatchSum += outData[j].batchNumber;
                                }
                            }
                            tableHtml += "<tr style='font-size: larger; font-weight: bolder color: black'><td colspan='4' style='text-align:center'>小计：</td><td>"+toDecimal(outCountSum)+"</td><td>"+outBatchSum+"</td><td></td><td></td><td></td></tr>";
                            tableHtml += "<tr style='font-size: larger; font-weight: bolder color: black'><td colspan='10' style='text-align: center; font-weight: 700; color: black'>库存汇总</td></tr>";
                            tableHtml += "<tr style='font-size: larger; font-weight: bolder color: black'><td>最后操作</td><td>订单颜色</td><td>面料名称</td><td>面料颜色</td><td>剩余数量</td><td>剩余卷数</td><td>缸号</td><td>位置</td><td></td><td></td></tr>";
                            var storageCountSum = 0;
                            var storageBatchSum = 0;
                            if (data["fabricStorageList"]){
                                var storageData = data["fabricStorageList"];
                                for (var k=0;k<storageData.length;k++){
                                    tableHtml += "<tr style='font-size: larger; font-weight: bolder color: black'><td>"+moment(storageData[k].returnTime).format("YYYY-MM-DD")+"</td><td>"+storageData[k].colorName+"</td><td>"+storageData[k].fabricName+"</td><td>"+storageData[k].fabricColor+"</td><td>"+toDecimal(storageData[k].weight)+"</td><td>"+storageData[k].batchNumber+"</td><td>"+storageData[k].jarName+"</td><td>"+storageData[k].location+"</td><td></td><td></td></tr>";
                                    var storageRow = [];
                                    storageRow.push(moment(storageData[k].returnTime).format("YYYY-MM-DD"));
                                    storageRow.push(storageData[k].colorName);
                                    storageRow.push(storageData[k].fabricName);
                                    storageRow.push(storageData[k].fabricColor);
                                    storageRow.push(toDecimal(storageData[k].weight));
                                    storageRow.push(storageData[k].batchNumber);
                                    storageRow.push(storageData[k].jarName);
                                    storageRow.push(storageData[k].location);
                                    storageCountSum += storageData[k].weight;
                                    storageBatchSum += storageData[k].batchNumber;
                                }
                            }
                            tableHtml += "<tr style='font-size: larger; font-weight: bolder color: black'><td colspan='4' style='text-align:center'>小计：</td><td>"+toDecimal(storageCountSum)+"</td><td>"+storageBatchSum+"</td><td></td><td></td><td></td><td></td></tr>";
                            tableHtml +=  "                       </tbody>" +
                                "                   </table>" +
                                "               </section>" +
                                "              </div>";
                            $("#materialDetail").append(tableHtml);
                            // setInterval(changeColor(),200);
                        }
                    }, error: function () {
                        swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
                    }
                });
            },500)
        } else if(obj.event === 'accessoryDetail') {
            var index = layer.open({
                type: 1 //Page层类型
                , title: '辅料详情'
                , btn: ['下载/打印']
                , shade: 0.6 //遮罩透明度
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , content: "<div id='materialDetail'></div><iframe id='printf' src='' width='0' height='0' frameborder='0'></iframe>"
                , yes: function () {
                    $("#printf").empty();
                    var printBoxs = document.getElementsByName('printTable');
                    var newContent = '';
                    for(var i=0;i<printBoxs.length;i++) {
                        newContent += printBoxs[i].innerHTML;
                    }
                    var f = document.getElementById('printf');
                    // f.contentDocument.write('<link rel="stylesheet" type="text/css" href="/css/app.v2.css">');
                    f.contentDocument.write('<style>' +
                        'table {' +
                        'border-collapse:collapse;' +
                        '}' +
                        'td {\n' +
                        '        border: 1px solid #000000;\n' +
                        '    }'+
                        '</style>');
                    f.contentDocument.write(newContent);
                    f.contentDocument.close();
                    window.frames['printf'].focus();
                    try{
                        window.frames['printf'].print();
                    }catch(err){
                        f.contentWindow.print();
                    }
                }
                , cancel : function (i,layero) {
                    $("#materialDetail").empty();
                }
            });
            layer.full(index);
            setTimeout(function () {
                var load = layer.load();
                $.ajax({
                    url: "searchaccessoryleakbyorder",
                    type:'GET',
                    data: {
                        orderName:thisOrderName
                    },
                    success: function (data) {
                        var tableHtml = "";
                        $("#materialDetail").empty();
                        if(data) {
                            tableHtml = "<div class=\"col-md-12\" name='printTable'>\n" +
                                "               <section class=\"panel panel-default\" style='page-break-after:always;'>\n" +
                                "                   <header class=\"panel-heading font-bold\" style=\"text-align: center\">\n" +
                                "                       <div style=\"font-size: xx-large;font-weight: bolder\">德悦服饰单款辅料报表</div><br>\n" +
                                "                       <div style=\"font-size: larger; font-weight: bolder; height: 30px\">" +  "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;版单:"+thisClothesVersionNumber+"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;订单:"+thisOrderName+"</div>\n"+
                                "                   </header>\n" +
                                "                   <table class=\"table table-bordered\" style='width: 100%; font-size: medium; color: black'>" +
                                "                       <tbody>";

                            tableHtml += "<tr><td colspan='14' style='font-weight: bolder;' align='center'>辅料汇总</td></tr>";

                            tableHtml += "<tr><th style='width: 15%'>辅料名称</th><th style='width: 8%'>辅料规格</th><th style='width: 5%'>单位</th><th style='width: 8%'>辅料颜色</th><th style='width: 10%'>色组</th><th style='width: 8%'>尺码</th><th style='width: 6%'>供应商</th><th style='width: 6%'>订购量</th><th style='width: 10%'>入库数量</th><th style='width: 6%'>订单数</th><th style='width: 5%'>单耗</th><th style='width: 5%'>理论</th><th style='width: 5%'>差异</th><th style='width: 5%'>好片</th></tr>";
                            var orderSum = 0;
                            var actualSum = 0;
                            var differenceSum = 0;
                            var planSum = 0;

                            if (data["plan"]){
                                var differenceData = data["plan"];
                                for (var i=0;i<differenceData.length;i++){
                                    tableHtml += "<tr><td>"+differenceData[i].accessoryName+"</td><td>"+differenceData[i].specification+"</td><td>"+differenceData[i].accessoryUnit+"</td><td>"+differenceData[i].accessoryColor+"</td><td>"+differenceData[i].colorName+"</td><td>"+differenceData[i].sizeName+"</td><td>"+differenceData[i].supplier+"</td><td>"+toDecimal(differenceData[i].accessoryCount)+"</td><td>"+toDecimal(differenceData[i].accessoryReturnCount)+"</td><td>"+differenceData[i].orderCount+"</td><td>"+differenceData[i].pieceUsage+"</td><td>"+differenceData[i].accessoryPlanCount+"</td>";
                                    if (differenceData[i].accessoryReturnCount - differenceData[i].accessoryPlanCount < 0){
                                        tableHtml += "<td style='color: red; font-weight: bolder'>"+toDecimal(differenceData[i].accessoryReturnCount - differenceData[i].accessoryPlanCount)+"</td><td>"+differenceData[i].wellCount+"</td></tr>";
                                    } else {
                                        tableHtml += "<td style='color: green; font-weight: bolder'>"+toDecimal(differenceData[i].accessoryReturnCount - differenceData[i].accessoryPlanCount)+"</td><td>"+differenceData[i].wellCount+"</td></tr>";
                                    }
                                    var differenceRow = [];
                                    differenceRow.push(differenceData[i].accessoryName);
                                    differenceRow.push(differenceData[i].specification);
                                    differenceRow.push(differenceData[i].accessoryUnit);
                                    differenceRow.push(differenceData[i].accessoryColor);
                                    differenceRow.push(differenceData[i].colorName);
                                    differenceRow.push(differenceData[i].sizeName);
                                    differenceRow.push(differenceData[i].supplier);
                                    differenceRow.push(toDecimal(differenceData[i].accessoryCount));
                                    differenceRow.push(toDecimal(differenceData[i].accessoryReturnCount));
                                    differenceRow.push(differenceData[i].orderCount);
                                    differenceRow.push(differenceData[i].pieceUsage);
                                    differenceRow.push(differenceData[i].accessoryPlanCount);
                                    differenceRow.push(toDecimal(differenceData[i].accessoryReturnCount - differenceData[i].accessoryPlanCount));
                                    differenceRow.push(differenceData[i].wellCount);
                                    orderSum += differenceData[i].accessoryCount;
                                    actualSum += differenceData[i].accessoryReturnCount;
                                    planSum += differenceData[i].accessoryPlanCount;
                                    differenceSum += toDecimal(differenceData[i].accessoryReturnCount - differenceData[i].accessoryPlanCount);
                                }
                            }
                            tableHtml += "<tr><td colspan='7' style='text-align:center'>小计：</td><td>"+toDecimal(orderSum)+"</td><td>"+toDecimal(actualSum)+"</td><td></td><td></td><td>"+ toDecimal(planSum) +"</td><td>"+toDecimal(differenceSum)+"</td><td></td></tr>";
                            var detailSum = 0;
                            tableHtml += "<tr><td colspan='14' style='font-weight: bolder' align='center'>入库详情</td></tr>";
                            tableHtml += "<tr><td >辅料名称</td><td>辅料规格</td><td>单位</td><td>辅料颜色</td><td>色组</td><td>尺码</td><td>供应商</td><td>入库数量</td><td>日期</td><td>位置</td><td>订单数</td><td>好片数</td><td></td><td></td></tr>";
                            if (data["inStore"]){
                                var detailData = data["inStore"];
                                for (var i=0;i<detailData.length;i++){
                                    tableHtml += "<tr><td>"+detailData[i].accessoryName+"</td><td>"+detailData[i].specification+"</td><td>"+detailData[i].accessoryUnit+"</td><td>"+detailData[i].accessoryColor+"</td><td>"+detailData[i].colorName+"</td><td>"+detailData[i].sizeName+"</td><td>"+detailData[i].supplier+"</td><td>"+toDecimal(detailData[i].inStoreCount)+"</td><td>"+moment(detailData[i].createTime).format("YYYY-MM-DD")+"</td><td>"+ detailData[i].accessoryLocation +"</td><td>"+ detailData[i].orderCount +"</td><td>"+ detailData[i].wellCount +"</td><td></td><td></td></tr>";
                                    var detailRow = [];
                                    detailRow.push(detailData[i].accessoryName);
                                    detailRow.push(detailData[i].specification);
                                    detailRow.push(detailData[i].accessoryUnit);
                                    detailRow.push(detailData[i].accessoryColor);
                                    detailRow.push(detailData[i].colorName);
                                    detailRow.push(detailData[i].sizeName);
                                    detailRow.push(detailData[i].supplier);
                                    detailRow.push(toDecimal(detailData[i].inStoreCount));
                                    detailRow.push(moment(detailData[i].createTime).format("YYYY-MM-DD"));
                                    detailRow.push(detailData[i].accessoryLocation);
                                    detailRow.push(detailData[i].orderCount);
                                    detailRow.push(detailData[i].wellCount);
                                    detailSum += toDecimal(detailData[i].inStoreCount);
                                }
                            }
                            tableHtml += "<tr><td colspan='7' style='text-align:center'>小计：</td><td>"+toDecimal(detailSum)+"</td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
                            tableHtml += "<tr><td colspan='14' style='font-weight: bolder;' align='center'>出库汇总</td></tr>";
                            tableHtml += "<tr><th>辅料名称</th><th>辅料规格</th><th>单位</th><th>辅料颜色</th><th>色组</th><th>尺码</th><th>供应商</th><th>入库数量</th><th>出库日期</th><th>位置</th><th>订单数</th><td>好片数</td><th></th><th></th></tr>";
                            var outCountSum = 0;
                            if (data["out"]){
                                var outData = data["out"];
                                for (var i=0;i<outData.length;i++){
                                    tableHtml += "<tr><td>"+outData[i].accessoryName+"</td><td>"+outData[i].specification+"</td><td>"+outData[i].accessoryUnit+"</td><td>"+outData[i].accessoryColor+"</td><td>"+outData[i].colorName+"</td><td>"+outData[i].sizeName+"</td><td>"+outData[i].supplier+"</td><td>"+toDecimal(outData[i].outStoreCount)+"</td><td>"+moment(outData[i].createTime).format("YYYY-MM-DD")+"</td><td>"+ outData[i].accessoryLocation +"</td><td>"+ outData[i].orderCount +"</td><td>"+ outData[i].wellCount +"</td><td></td></tr>";
                                    var detailRow = [];
                                    detailRow.push(outData[i].accessoryName);
                                    detailRow.push(outData[i].specification);
                                    detailRow.push(outData[i].accessoryUnit);
                                    detailRow.push(outData[i].accessoryColor);
                                    detailRow.push(outData[i].colorName);
                                    detailRow.push(outData[i].sizeName);
                                    detailRow.push(outData[i].supplier);
                                    detailRow.push(toDecimal(outData[i].outStoreCount));
                                    detailRow.push(moment(outData[i].createTime).format("YYYY-MM-DD"));
                                    detailRow.push(outData[i].accessoryLocation);
                                    detailRow.push(outData[i].orderCount);
                                    detailRow.push(outData[i].wellCount);
                                    outCountSum += toDecimal(outData[i].outStoreCount);
                                }
                            }
                            tableHtml += "<tr><td colspan='7' style='text-align:center'>小计：</td><td>"+toDecimal(outCountSum)+"</td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
                            tableHtml += "<tr><td colspan='14' style='font-weight: bolder; color: black' align='center'>库存汇总</td></tr>";
                            tableHtml += "<tr><th>辅料名称</th><th>辅料规格</th><th>单位</th><th>辅料颜色</th><th>色组</th><th>尺码</th><th>供应商</th><th>入库数量</th><th>最后操作</th><th>位置</th><th>订单数</th><th>好片数</th><th></th><th></th></tr>";
                            var storageSum = 0;
                            if (data["storage"]){
                                var storageData = data["storage"];
                                for (var i=0;i<storageData.length;i++){
                                    tableHtml += "<tr><td>"+storageData[i].accessoryName+"</td><td>"+storageData[i].specification+"</td><td>"+storageData[i].accessoryUnit+"</td><td>"+storageData[i].accessoryColor+"</td><td>"+storageData[i].colorName+"</td><td>"+storageData[i].sizeName+"</td><td>"+storageData[i].supplier+"</td><td>"+toDecimal(storageData[i].storageCount)+"</td><td>"+moment(storageData[i].updateTime).format("YYYY-MM-DD")+"</td><td>"+ storageData[i].accessoryLocation +"</td><td>"+ storageData[i].orderCount +"</td><td>"+ storageData[i].wellCount +"</td><td></td><td></td></tr>";
                                    var detailRow = [];
                                    detailRow.push(storageData[i].accessoryName);
                                    detailRow.push(storageData[i].specification);
                                    detailRow.push(storageData[i].accessoryUnit);
                                    detailRow.push(storageData[i].accessoryColor);
                                    detailRow.push(storageData[i].colorName);
                                    detailRow.push(storageData[i].sizeName);
                                    detailRow.push(storageData[i].supplier);
                                    detailRow.push(toDecimal(storageData[i].storageCount));
                                    detailRow.push(moment(storageData[i].updateTime).format("YYYY-MM-DD"));
                                    detailRow.push(storageData[i].accessoryLocation);
                                    detailRow.push(storageData[i].orderCount);
                                    detailRow.push(storageData[i].wellCount);
                                    storageSum += toDecimal(storageData[i].storageCount);
                                }
                            }
                            tableHtml += "<tr><td colspan='7' style='text-align:center'>小计：</td><td>"+toDecimal(storageSum)+"</td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
                            tableHtml +=  "                       </tbody>" +
                                "                   </table>" +
                                "               </section>" +
                                "              </div>";
                            $("#materialDetail").append(tableHtml);
                            layer.close(load);
                        }
                    }, error: function () {
                        swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
                    }
                })
            },500)
        }
    });

});

function getFormatDate() {
    var date = new Date();
    var month = date.getMonth() + 1;
    var strDate = date.getDate();
    if (month >= 1 && month <= 9) {
        month = "0" + month;
    }
    if (strDate >= 0 && strDate <= 9) {
        strDate = "0" + strDate;
    }
    var currentDate = date.getFullYear() + "-" + month + "-" + strDate;
    return currentDate;
}

function toDecimal(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*100)/100;
    return f;
}

function toDecimalFour(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*10000)/10000;
    return f;
}