var userRole = $("#userRole").val();
$(document).ready(function () {

    //初始化gantt
    $(".gantt").gantt({
        source: [
            {
                name: "车缝A组",
                desc: "计划",
                values: [{
                    from: "/Date(1588262400000)/",
                    to: "/Date(1588608000000)/",
                    label: "2021款计划",
                    customClass: "ganttRed"
                }]
            }, {
                name: "",
                desc: "实际",
                values: [{
                    from: "/Date(1588608000000)/",
                    to: "/Date(1589040000000)/",
                    label: "2021款实际",
                    customClass: "ganttRed"
                }]
            }, {
                name: "车缝B组",
                desc: "计划",
                values: [{
                    from: "/Date(1588262400000)/",
                    to: "/Date(1588608000000)/",
                    label: "",
                    customClass: "ganttGreen"
                }]
            }, {
                name: "",
                desc: "实际",
                values: [{
                    from: "/Date(1588608000000)/",
                    to: "/Date(1589040000000)/",
                    label: "",
                    customClass: "ganttBlue"
                }]
            }, {
                name: "车缝C组",
                desc: "计划",
                values: [{
                    from: "/Date(1588262400000)/",
                    to: "/Date(1588608000000)/",
                    label: "",
                    customClass: "ganttGreen"
                }]
            }, {
                name: "",
                desc: "实际",
                values: [{
                    from: "/Date(1588608000000)/",
                    to: "/Date(1589040000000)/",
                    label: "",
                    customClass: "ganttBlue"
                }]
            }, {
                name: "车缝D组",
                desc: "计划",
                values: [{
                    from: "/Date(1588262400000)/",
                    to: "/Date(1588608000000)/",
                    label: "",
                    customClass: "ganttOrange"
                }]
            }, {
                name: "",
                desc: "实际",
                values: [{
                    from: "/Date(1588608000000)/",
                    to: "/Date(1589040000000)/",
                    label: "",
                    customClass: "ganttOrange"
                }]
            }, {
                name: "车缝E组",
                desc: "计划",
                values: [
                    {
                        from: "/Date(1588262400000)/",
                        to: "/Date(1588608000000)/",
                        label: "",
                        customClass: "ganttBlue"
                    },
                    {
                        from: "/Date(1588608000000)/",
                        to: "/Date(1589040000000)/",
                        label: "",
                        customClass: "ganttOrange"
                    },
                    {
                        from: "/Date(1589040000000)/",
                        to: "/Date(1589472000000)/",
                        label: "",
                        customClass: "ganttGreen"
                    }
                ]
            }, {
                name: "",
                desc: "实际",
                values: [
                    {
                        from: "/Date(1588262400000)/",
                        to: "/Date(1588608000000)/",
                        label: "",
                        customClass: "ganttBlue"
                    },
                    {
                        from: "/Date(1588608000000)/",
                        to: "/Date(1589040000000)/",
                        label: "",
                        customClass: "ganttOrange"
                    },
                    {
                        from: "/Date(1589040000000)/",
                        to: "/Date(1589472000000)/",
                        label: "",
                        customClass: "ganttGreen"
                    }
                ]
            }],
        navigate: 'scroll',//buttons  scroll
        scale: "weeks",// months  weeks days  hours
        maxScale: "weeks",
        minScale: "months",
        itemsPerPage: 20,
        // onItemClick: function (data) {
        //     alert("Item clicked - show some details");
        // },
        // onAddClick: function (dt, rowId) {
        //     alert("Empty space clicked - add an item!");
        // },
        onRender: function () {
            if (window.console && typeof console.log === "function") {
                console.log("chart rendered");
            }
        }
    });


    //弹窗功能
    $(".gantt").popover({
        selector: ".bar",
        title: "I'm a popover",
        content: "And I'm the content of said popover.",
        trigger: "hover",
        placement: "auto right"
    });

});
