var userRole = $("#userRole").val();
var procedureDemo;
layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.21'
}).extend({                         // 模块别名
    soulTable: 'soulTable'
});
layui.use('form', function(){
    form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
    form.render();
});
$(document).ready(function () {

    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumber", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });

    procedureDemo = xmSelect.render({
        el: '#procedureNumber',
        toolbar: {
            show: true
        },
        data: []
    });


    $("#orderName").bind('blur change',function(){
        var orderName = $("#orderName").val();
        $.ajax({
            url: "/erp/getsewsectionprocedure",
            type: 'GET',
            data: {
                orderName:orderName
            },
            success:function(data){
                var procedureData = [];
                if (data.orderProcedureList) {
                    $.each(data.orderProcedureList, function(index,element){
                        var tmpProcedure = {};
                        tmpProcedure.name = element.procedureNumber+'-'+element.procedureName;
                        tmpProcedure.value = element.procedureNumber;
                        tmpProcedure.selected = true;
                        procedureData.push(tmpProcedure);
                    })
                }
                procedureDemo.update({
                    data: procedureData
                });
            }, error: function () {
                layer.msg("获取工序信息失败", { icon: 2 });
            }
        });
    });

    $("#clothesVersionNumber").bind('blur change',function(){
        var orderName = $("#orderName").val();
        $.ajax({
            url: "/erp/getsewsectionprocedure",
            type: 'GET',
            data: {
                orderName:orderName
            },
            success:function(data){
                var procedureData = [];
                if (data.orderProcedureList) {
                    $.each(data.orderProcedureList, function(index,element){
                        var tmpProcedure = {};
                        tmpProcedure.name = element.procedureNumber+'-'+element.procedureName;
                        tmpProcedure.value = element.procedureNumber;
                        tmpProcedure.selected = true;
                        procedureData.push(tmpProcedure);
                    })
                }
                procedureDemo.update({
                    data: procedureData
                });
            }, error: function () {
                layer.msg("获取工序信息失败", { icon: 2 });
            }
        });
    });

});

$(document).on('keydown', '.layui-table-edit', function (e) {
    var td = $(this).parent('td')
        ,tr = td.parent('tr')
        ,trs = tr.parent().parent().find('tr')
        ,tr_index = tr.index()
        ,td_index = td.index()
        ,td_last_index = tr.find('[data-edit="text"]:last').index()
        ,td_first_index = tr.find('[data-edit="text"]:first').index();
    switch (e.keyCode) {
        case 13:
        case 39:
            td.nextAll('[data-edit="text"]:first').click();
            if (td_index == td_last_index){
                tr.next().find('td').eq(td_first_index).click();
                if (tr_index == trs.length - 1){
                    trs.eq(0).find('td').eq(td_first_index).click();
                }
            }
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
        case 37:
            td.prevAll('[data-edit="text"]:first').click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
        case 38:
            tr.prev().find('td').eq(td_index).click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
        case 40:
            tr.next().find('td').eq(td_index).click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
    }

})


var scheduleTable;
var scheduleColumns;
var selectedProcedureList = [];
var globalEmployeeSkills = [];
layui.use(['form', 'soulTable', 'table'], function () {
    var table = layui.table,
        soulTable = layui.soulTable,
        form = layui.form,
        $ = layui.$;
    var load = layer.load();

    //加载历史缩略行
    scheduleTable = table.render({
        elem: '#scheduleTable'
        ,url:'getuniqueschedulerecorddata'
        ,cols:[[]]
        ,loading:true
        ,data:[]
        ,done: function (res, curr, count) {
            table.init('scheduleTable',{//转换成静态表格
                cols:[[
                    {type:'numbers', align:'center', title:'序号', width:60, sort: true, filter: true}
                    ,{field: 'clothesVersionNumber', title: '单号',align:'center', minWidth: 200, sort: true, filter: true}
                    ,{field: 'orderName', title: '款号',align:'center', minWidth: 200, sort: true, filter: true}
                    ,{field: 'groupName', title: '组名',align:'center', minWidth: 200, sort: true, filter: true}
                    ,{field: 'createTime', title: '保存时间',align:'center', minWidth: 200, sort: true, filter: true,templet: function (d) {
                            return layui.util.toDateString(d.createTime, 'yyyy-MM-dd');
                        }}
                    ,{title:'操作', align:'center', toolbar: '#barTop', width:220}
                ]]
                ,data:res.data
                ,height: 'full-80'
                ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                ,defaultToolbar: ['filter', 'print']
                ,title: '历史排产表'
                ,totalRow: true
                ,even: true
                ,page: true
                ,limits: [50, 100, 200]
                ,limit: 100 //每页默认显示的数量
                ,overflow: 'tips'
                ,done: function () {
                    soulTable.render(this);
                    layer.close(load);
                }
                ,filter: {
                    bottom: true,
                    items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                }
            });
        }
        ,filter: {
            bottom: true,
            clearFilter: false,
            items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
        }
    });


    //监听行工具事件
    table.on('tool(scheduleTable)', function(obj){
        var data = obj.data;
        if(obj.event === 'del'){
            layer.confirm('真的删除吗', function(index){
                $.ajax({
                    url: "/erp/deleteschedulerecordbyordergroup",
                    type: 'POST',
                    data: {
                        orderName:data.orderName,
                        groupName:data.groupName
                    },
                    success: function (res) {
                        if (res.result == 0) {
                            obj.del();
                            layer.close(index);
                            layer.msg("删除成功！", {icon: 1});
                        } else {
                            layer.msg("删除失败！", {icon: 2});
                        }
                    },
                    error: function () {
                        layer.msg("删除失败！", {icon: 2});
                    }
                })

            });
        }
        else if (obj.event === 'detail'){
            var paramData = {};
            paramData.orderName = data.orderName;
            paramData.groupName = data.groupName;
            $("#orderName").val(data.orderName);
            $("#clothesVersionNumber").val(data.clothesVersionNumber);
            $("#groupName").val(data.groupName);
            form.render();
            var load = layer.load(2);
            $.ajax({
                url: "/erp/getscheduledetailrecorddata",
                type: 'GET',
                data: paramData,
                success: function (res) {
                    var procedureNumberList = [];

                    var procedureData = [];
                    if (res.totalOrderProcedureList && res.orderProcedureList) {
                        $.each(res.totalOrderProcedureList, function(index1,element1){
                            var tmpProcedure = {};
                            tmpProcedure.name = element1.procedureNumber+'-'+element1.procedureName;
                            tmpProcedure.value = element1.procedureNumber;
                            $.each(res.orderProcedureList, function(index2,element2){
                                if (element1.procedureNumber === element2.procedureNumber){
                                    tmpProcedure.selected = true;
                                }
                            });
                            procedureData.push(tmpProcedure);
                        })
                    }
                    procedureDemo.update({
                        data: procedureData
                    });

                    if (res.orderProcedureList){
                        var totalProcedureList = res.orderProcedureList;
                        selectedProcedureList = res.orderProcedureList;
                        for (var i = 0; i < totalProcedureList.length; i ++){
                            procedureNumberList.push(totalProcedureList[i].procedureNumber);
                        }
                    }
                    if (selectedProcedureList) {
                        var sortedProcedureData = selectedProcedureList.sort(compare("procedureNumber"));
                        var procedureInfo = [];
                        scheduleColumns = [{type:'checkbox', filed:'LAY_CHECKED', fixed: 'left'},{field:"employeeInfo",title:"姓名",width:120,align:'center', fixed: 'left'}];
                        for (var i = 0; i < sortedProcedureData.length; i++){
                            procedureInfo.push(sortedProcedureData[i]["procedureCode"] + "-" + sortedProcedureData[i]["procedureName"]);
                            var tmpCol = {};
                            tmpCol.field = sortedProcedureData[i]["procedureNumber"] + "号";
                            tmpCol.title = sortedProcedureData[i]["procedureName"];
                            tmpCol.align = 'center';
                            tmpCol.width = 90;
                            tmpCol.totalRow = true;
                            tmpCol.templet = function (d) {
                                if(d[this.field+"isDefault"]) {
                                    return '<input type="checkbox" value='+d[this.field]+' lay-skin="switch" lay-text="'+d[this.field]+'|'+d[this.field]+'" lay-filter="sexDemo">';
                                }else {
                                    return '<input type="checkbox" value='+ d[this.field] +' checked="" lay-skin="switch" lay-text="' + d[this.field] + '|' + d[this.field] + '" lay-filter="sexDemo">';
                                }
                            };
                            scheduleColumns.push(tmpCol);
                            var anotherCol = {};
                            anotherCol.field = sortedProcedureData[i]["procedureNumber"] + "号工时";
                            anotherCol.title = "工时";
                            anotherCol.align = 'center';
                            anotherCol.width = 70;
                            anotherCol.edit = 'text';
                            scheduleColumns.push(anotherCol);
                        }
                        var empSkills = [];
                        if (res.employeeList){
                            if (!res.scheduleRecordList){
                                layer.msg("无历史排产数据！");
                            }
                            for (var j = 0; j < res.employeeList.length; j ++){
                                var tmpEmpSkill = {};
                                tmpEmpSkill.employeeInfo = res.employeeList[j]["employeeNumber"] + "-" + res.employeeList[j]["employeeName"];
                                for (var k = 0; k < sortedProcedureData.length; k ++){
                                    var field = sortedProcedureData[k]["procedureNumber"] + "号";
                                    var hourField = sortedProcedureData[k]["procedureNumber"] + "号工时";
                                    //首先赋值标准的小时产量
                                    tmpEmpSkill[field] = toDecimal(sortedProcedureData[k]["sam"]);
                                    tmpEmpSkill[field+"isDefault"] = true;
                                    if (res.empSkillsList){
                                        for (var m = 0; m < res.empSkillsList.length; m ++){
                                            if (res.empSkillsList[m]['procedureCode'] == sortedProcedureData[k]["procedureCode"] && res.empSkillsList[m]['employeeNumber'] == res.employeeList[j]["employeeNumber"]){
                                                //如果员工技能库里有则换成员工技能
                                                tmpEmpSkill[field] = toDecimal(res.empSkillsList[m]["timeCount"]);
                                            }
                                        }
                                    }
                                    if (res.scheduleRecordList){
                                        for (var n = 0; n < res.scheduleRecordList.length; n ++){
                                            if (res.scheduleRecordList[n]['procedureNumber'] == sortedProcedureData[k]["procedureNumber"] && res.scheduleRecordList[n]['employeeNumber'] == res.employeeList[j]["employeeNumber"]){
                                                //如果有历史数据，就把小时数据填充
                                                tmpEmpSkill['LAY_CHECKED'] = true;
                                                tmpEmpSkill[field+"isDefault"]  = false;
                                                tmpEmpSkill[hourField] = res.scheduleRecordList[n]['hourCount'];
                                            }

                                        }
                                    }
                                }
                                empSkills.push(tmpEmpSkill);
                            }
                        }
                        globalEmployeeSkills = empSkills;

                        scheduleTable = table.render({
                            elem: '#scheduleTable'
                            ,cols:[scheduleColumns]
                            ,excel: {
                                filename: '排产报表.xlsx'
                            }
                            ,loading:true
                            ,data:empSkills
                            ,overflow: 'tips'
                            ,height: 'full-80'
                            ,even: true
                            ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                            ,defaultToolbar: false
                            ,title: '排产数据表'
                            // ,totalRow: true
                            ,page: false
                            ,limits: [50, 100, 200]
                            ,limit: Number.MAX_VALUE //每页默认显示的数量
                            ,done: function () {
                                soulTable.render(this);
                                layer.close(load);
                            }
                            ,filter: {
                                bottom: true,
                                clearFilter: false,
                                items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                            }
                        });
                    } else {
                        layer.msg("获取失败！", {icon: 2});
                    }
                }, error: function () {
                    layer.msg("获取失败！", {icon: 2});
                }
            });
            return false;
        }
    });

    //监听提交
    form.on('submit(loadBasicData)', function(data){
        var data = {};
        var isInput = true;
        var orderName = $("#orderName").val();
        var workHour = $("#workHour").val();
        if (orderName == null || orderName == ""){
            isInput = false;
        }
        globalEmployeeSkills = layui.table.cache.scheduleTable;
        if (!isInput){
            layer.msg("款号-工时必须", { icon: 2 });
        }else {
            data.orderName = orderName;
            data.workHour = workHour;
            data.groupName = $("#groupName").val();
            var load = layer.load(2);
            $.ajax({
                url: "/erp/getorderscheduledata",
                type: 'GET',
                data: data,
                success: function (res) {
                    var procedureNumberList = [];
                    $.each(procedureDemo.getValue(), function(index,element){
                        procedureNumberList.push(element.value);
                    });
                    selectedProcedureList = [];
                    if (res.orderProcedureList){
                        var totalProcedureList = res.orderProcedureList;
                        for (var i = 0; i < procedureNumberList.length; i ++){
                            for (var j = 0; j < totalProcedureList.length; j ++){
                                if (procedureNumberList[i] == totalProcedureList[j].procedureNumber){
                                    selectedProcedureList.push(totalProcedureList[j]);
                                }
                            }
                        }
                    }

                    if (selectedProcedureList) {
                        var sortedProcedureData = selectedProcedureList.sort(compare("procedureNumber"));
                        var procedureInfo = [];
                        scheduleColumns = [{type:'checkbox', fixed: 'left'},{field:"employeeInfo",title:"姓名",width:120,align:'center', fixed: 'left'}];
                        for (var i = 0; i < sortedProcedureData.length; i++){
                            procedureInfo.push(sortedProcedureData[i]["procedureCode"] + "-" + sortedProcedureData[i]["procedureName"]);
                            var tmpCol = {};
                            tmpCol.field = sortedProcedureData[i]["procedureNumber"] + "号";
                            tmpCol.title = sortedProcedureData[i]["procedureName"];
                            tmpCol.align = 'center';
                            tmpCol.width = 90;
                            tmpCol.totalRow = true;
                            tmpCol.templet = function (d) {
                                if(d[this.field+"isDefault"]) {
                                    return '<input type="checkbox" value='+d[this.field]+' lay-skin="switch" lay-text="'+d[this.field]+'|'+d[this.field]+'" lay-filter="sexDemo">';
                                }else {
                                    return '<input type="checkbox" value='+ d[this.field] +' checked="" lay-skin="switch" lay-text="' + d[this.field] + '|' + d[this.field] + '" lay-filter="sexDemo">';
                                }
                            };
                            tmpCol.event = 'changeState';
                            scheduleColumns.push(tmpCol);
                            var anotherCol = {};
                            anotherCol.field = sortedProcedureData[i]["procedureNumber"] + "号工时";
                            anotherCol.title = "工时";
                            // anotherCol.title = sortedProcedureData[i]["procedureNumber"] + "工时";
                            anotherCol.align = 'center';
                            anotherCol.width = 70;
                            anotherCol.edit = 'text';
                            scheduleColumns.push(anotherCol);
                        }
                        var empSkills = [];
                        if (res.employeeList){
                            for (var j = 0; j < res.employeeList.length; j ++){
                                var tmpEmpSkill = {};
                                tmpEmpSkill.employeeInfo = res.employeeList[j]["employeeNumber"] + "-" + res.employeeList[j]["employeeName"];
                                for (var k = 0; k < sortedProcedureData.length; k ++){
                                    var field = sortedProcedureData[k]["procedureNumber"] + "号";
                                    //首先赋值标准的小时产量
                                    tmpEmpSkill[field] = toDecimal(sortedProcedureData[k]["sam"]);
                                    tmpEmpSkill[field+"isDefault"] = true;
                                    if (globalEmployeeSkills){
                                        $.each(globalEmployeeSkills, function(index00, item00){
                                            if (item00["employeeInfo"] === tmpEmpSkill.employeeInfo){
                                                if (item00[field+"工时"]){
                                                    tmpEmpSkill[field+"工时"] = item00[field+"工时"];
                                                }
                                            }
                                        });
                                    }
                                    if (res.empSkillsList){
                                        for (var m = 0; m < res.empSkillsList.length; m ++){
                                            if (res.empSkillsList[m]['procedureCode'] == sortedProcedureData[k]["procedureCode"] && res.empSkillsList[m]['employeeNumber'] == res.employeeList[j]["employeeNumber"]){
                                                //如果员工技能库里有则换成员工技能
                                                tmpEmpSkill[field] = toDecimal(res.empSkillsList[m]["timeCount"]);
                                                tmpEmpSkill[field+"isDefault"]  = false;
                                            }

                                        }
                                    }
                                }
                                empSkills.push(tmpEmpSkill);
                            }
                        }
                        globalEmployeeSkills = empSkills;

                        scheduleTable = table.render({
                            elem: '#scheduleTable'
                            ,cols:[scheduleColumns]
                            ,excel: {
                                filename: '排产报表.xlsx'
                            }
                            ,loading:true
                            ,data:empSkills
                            ,overflow: 'tips'
                            ,height: 'full-80'
                            ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                            ,defaultToolbar: false
                            ,title: '排产数据表'
                            // ,totalRow: true
                            ,page: false
                            ,even: true
                            ,limits: [50, 100, 200]
                            ,limit: Number.MAX_VALUE //每页默认显示的数量
                            ,done: function () {
                                soulTable.render(this);
                                layer.close(load);
                            }
                            ,filter: {
                                bottom: true,
                                clearFilter: false,
                                items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                            }
                        });


                        //监听单元格编辑
                        table.on('tool(scheduleTable)', function (obj) {
                            var data = obj.data;
                            var that = this;
                            var field = $(that).data('field');
                            if(obj.event === 'changeState'){
                                var changeField = field+"isDefault";
                                if (data[changeField]){
                                    obj.update({
                                        [changeField] : false
                                    });
                                } else {
                                    obj.update({
                                        [changeField] : true
                                    });
                                }
                            }
                        });
                    } else {
                        layer.msg("获取失败！", {icon: 2});
                    }
                }, error: function () {
                    layer.msg("获取失败！", {icon: 2});
                }
            });
        }
        return false;
    });

    //加载历史数据
    // form.on('submit(scheduleRecordData)', function(data){
    //     var data = {};
    //     var isInput = true;
    //     var orderName = $("#orderName").val();
    //     var groupName = $("#groupName").val();
    //     var exceptCount = $("#exceptCount").val();
    //     if (orderName == null || orderName == "" || groupName == null || groupName == ""){
    //         isInput = false;
    //     }
    //     if (!isInput){
    //         layer.msg("款号-组名必须", { icon: 2 });
    //     }else {
    //         data.orderName = orderName;
    //         data.groupName = $("#groupName").val();
    //         var load = layer.load(2);
    //         $.ajax({
    //             url: "/erp/getschedulerecorddata",
    //             type: 'GET',
    //             data: data,
    //             success: function (res) {
    //                 var procedureNumberList = [];
    //                 $.each(procedureDemo.getValue(), function(index,element){
    //                     procedureNumberList.push(element.value);
    //                 });
    //                 selectedProcedureList = [];
    //                 if (res.orderProcedureList){
    //                     var totalProcedureList = res.orderProcedureList;
    //                     for (var i = 0; i < procedureNumberList.length; i ++){
    //                         for (var j = 0; j < totalProcedureList.length; j ++){
    //                             if (procedureNumberList[i] == totalProcedureList[j].procedureNumber){
    //                                 selectedProcedureList.push(totalProcedureList[j]);
    //                             }
    //                         }
    //                     }
    //                 }
    //
    //                 if (selectedProcedureList) {
    //                     var sortedProcedureData = selectedProcedureList.sort(compare("procedureNumber"));
    //                     var procedureInfo = [];
    //                     var cols = [{type:'checkbox', filed:'LAY_CHECKED', fixed: 'left'},{field:"employeeInfo",title:"姓名",width:120,align:'center', fixed: 'left'}];
    //                     for (var i = 0; i < sortedProcedureData.length; i++){
    //                         procedureInfo.push(sortedProcedureData[i]["procedureCode"] + "-" + sortedProcedureData[i]["procedureName"]);
    //                         var tmpCol = {};
    //                         tmpCol.field = sortedProcedureData[i]["procedureNumber"] + "号";
    //                         tmpCol.title = sortedProcedureData[i]["procedureName"];
    //                         tmpCol.align = 'center';
    //                         tmpCol.width = 90;
    //                         tmpCol.totalRow = true;
    //                         tmpCol.templet = function (d) {
    //                             if(d[this.field+"isDefault"]) {
    //                                 return '<input type="checkbox" value='+d[this.field]+' lay-skin="switch" lay-text="'+d[this.field]+'|'+d[this.field]+'" lay-filter="sexDemo">';
    //                             }else {
    //                                 return '<input type="checkbox" value='+ d[this.field] +' checked="" lay-skin="switch" lay-text="' + d[this.field] + '|' + d[this.field] + '" lay-filter="sexDemo">';
    //                             }
    //                         };
    //                         cols.push(tmpCol);
    //                         var anotherCol = {};
    //                         anotherCol.field = sortedProcedureData[i]["procedureNumber"] + "号工时";
    //                         anotherCol.title = "工时";
    //                         // anotherCol.title = sortedProcedureData[i]["procedureNumber"] + "工时";
    //                         anotherCol.align = 'center';
    //                         anotherCol.width = 70;
    //                         anotherCol.edit = 'text';
    //                         cols.push(anotherCol);
    //                     }
    //                     var empSkills = [];
    //                     if (res.employeeList && res.empSkillsList){
    //                         if (!res.scheduleRecordList){
    //                             layer.msg("无历史排产数据！");
    //                         }
    //                         for (var j = 0; j < res.employeeList.length; j ++){
    //                             var tmpEmpSkill = {};
    //                             tmpEmpSkill.employeeInfo = res.employeeList[j]["employeeNumber"] + "-" + res.employeeList[j]["employeeName"];
    //                             for (var k = 0; k < sortedProcedureData.length; k ++){
    //                                 var field = sortedProcedureData[k]["procedureNumber"] + "号";
    //                                 var hourField = sortedProcedureData[k]["procedureNumber"] + "号工时";
    //                                 //首先赋值标准的小时产量
    //                                 tmpEmpSkill[field] = toDecimal(sortedProcedureData[k]["sam"]);
    //                                 tmpEmpSkill[field+"isDefault"] = true;
    //                                 for (var m = 0; m < res.empSkillsList.length; m ++){
    //                                     if (res.empSkillsList[m]['procedureCode'] == sortedProcedureData[k]["procedureCode"] && res.empSkillsList[m]['employeeNumber'] == res.employeeList[j]["employeeNumber"]){
    //                                         //如果员工技能库里有则换成员工技能
    //                                         tmpEmpSkill[field] = toDecimal(res.empSkillsList[m]["timeCount"]);
    //                                         tmpEmpSkill[field+"isDefault"]  = false;
    //                                     }
    //
    //                                 }
    //                                 if (res.scheduleRecordList){
    //                                     for (var n = 0; n < res.scheduleRecordList.length; n ++){
    //                                         if (res.scheduleRecordList[n]['procedureNumber'] == sortedProcedureData[k]["procedureNumber"] && res.scheduleRecordList[n]['employeeNumber'] == res.employeeList[j]["employeeNumber"]){
    //                                             //如果有历史数据，就把小时数据填充
    //                                             tmpEmpSkill['LAY_CHECKED'] = true;
    //                                             tmpEmpSkill[hourField] = res.scheduleRecordList[n]['hourCount'];
    //                                         }
    //
    //                                     }
    //                                 }
    //                             }
    //                             empSkills.push(tmpEmpSkill);
    //                         }
    //                     }
    //
    //                     scheduleTable = table.render({
    //                         elem: '#scheduleTable'
    //                         ,cols:[cols]
    //                         ,excel: {
    //                             filename: '排产报表.xlsx'
    //                         }
    //                         ,loading:true
    //                         ,data:empSkills
    //                         ,overflow: 'tips'
    //                         ,height: 'full-80'
    //                         ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
    //                         ,defaultToolbar: false
    //                         ,title: '排产数据表'
    //                         // ,totalRow: true
    //                         ,page: false
    //                         ,limits: [50, 100, 200]
    //                         ,limit: Number.MAX_VALUE //每页默认显示的数量
    //                         ,done: function () {
    //                             soulTable.render(this);
    //                             layer.close(load);
    //                         }
    //                         ,filter: {
    //                             bottom: true,
    //                             clearFilter: false,
    //                             items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
    //                         }
    //                     });
    //                 } else {
    //                     layer.msg("获取失败！", {icon: 2});
    //                 }
    //             }, error: function () {
    //                 layer.msg("获取失败！", {icon: 2});
    //             }
    //         });
    //     }
    //     return false;
    // });

    //排产
    // form.on('submit(orderSchedule)', function(data){
    //     var allData = layui.table.cache.scheduleTable;
    //     var selectData = [];
    //     $.each(allData,function (index,item) {
    //         if(item.LAY_CHECKED) {
    //             var sData = {};
    //             sData.employeeInfo = item.employeeInfo;
    //             $.each($(".layui-form.layui-border-box.layui-table-view[lay-id='scheduleTable']  .layui-table-body.layui-table-main").find("tr[data-index="+item.LAY_TABLE_INDEX+"]").find("td"),function (c_index,c_item) {
    //                 if(c_index>1) {
    //                     if($(c_item).find("input")[0].checked) {
    //                         sData[$(c_item).attr("data-field")] = Number($(c_item).find("input").eq(0).val());
    //                     }else {
    //                         sData[$(c_item).attr("data-field")] = 0;
    //                     }
    //                 }
    //             });
    //             selectData.push(sData);
    //         }
    //     });
    //     var checkNull = {};
    //     for (var i = 0; i < selectData.length; i ++){
    //         $.each(selectData[i],function (index,item) {
    //             if (index != "employeeInfo"){
    //                 if (checkNull[index] != null){
    //                     checkNull[index] += Number(item);
    //                 } else {
    //                     checkNull[index] = 0;
    //                 }
    //             }
    //         })
    //     }
    //     var procedureFlag = false;
    //     $.each(checkNull,function (index,item) {
    //         if (item == 0){
    //             procedureFlag = true;
    //         }
    //     });
    //     if(selectData.length==0) {
    //         layer.msg("请选择待排产的员工！", {icon: 2});
    //         return false;
    //     }
    //     if (procedureFlag){
    //         layer.msg("请为所有工序安排员工！", {icon: 2});
    //         return false;
    //     }
    //     var exceptCount = $("#exceptCount").val();
    //     if (exceptCount == null || exceptCount == ""){
    //         exceptCount = 2000;
    //     }
    //     layui.use(['table', 'soulTable'], function () {
    //         var table = layui.table,
    //             soulTable = layui.soulTable;
    //         var label = "排产明细";
    //         var index = layer.open({
    //             type: 1 //Page层类型
    //             , title: label
    //             , shade: 0.6 //遮罩透明度
    //             , maxmin: false //允许全屏最小化
    //             , anim: 0 //0-6的动画形式，-1不开启
    //             , content: $("#scheduleDetailTable")
    //         });
    //         layer.full(index);
    //         $.ajax({
    //             url: "/erp/getscheduleofordergroup",
    //             type: 'GET',
    //             data: {
    //                 selectData: JSON.stringify(selectData),
    //                 workHour: $("#workHour").val(),
    //                 exceptCount: exceptCount
    //             },
    //             success: function (res) {
    //
    //                 var scheduleData = res.scheduleList;
    //                 scheduleData.push(res.summaryData);
    //                 console.log(res.scheduleList);
    //                 var sortedProcedureData = selectedProcedureList.sort(compare("procedureNumber"));
    //                 var cols = [{field: "employeeInfo", title: "姓名", width: 150, align: 'center', fixed: 'left'}];
    //                 for (var i = 0; i < sortedProcedureData.length; i++) {
    //                     var tmpCol = {};
    //                     tmpCol.field = sortedProcedureData[i]["procedureCode"] + "号";
    //                     tmpCol.title = sortedProcedureData[i]["procedureName"];
    //                     tmpCol.align = 'center';
    //                     tmpCol.width = 150;
    //                     tmpCol.totalRow = false;
    //                     cols.push(tmpCol);
    //                 }
    //                 table.render({
    //                     elem: '#scheduleDetailTable'
    //                     // ,totalRow: true
    //                     ,cols: [cols]
    //                     ,page: true
    //                     ,height: 'full-100'
    //                     ,limit:100
    //                     ,limits:[50,100,200]
    //                     ,data: scheduleData
    //                     ,done: function () {
    //                         soulTable.render(this);
    //                     }
    //                 });
    //             },
    //             error: function () {
    //                 layer.msg("获取数据失败！", {icon: 2});
    //             }
    //         })
    //     });
    //     return false;
    // });

    //排产
    form.on('submit(orderSchedule)', function(data){
        var allData = layui.table.cache.scheduleTable;
        var selectData = [];
        var selectHourData = [];
        var selectPercentData = [];
        $.each(allData,function (index,item) {
            if(item.LAY_CHECKED) {
                var sData = {};
                var sHourData = {};
                var sPercentData = {};
                var hourList = [];
                sData.employeeInfo = item.employeeInfo;
                sHourData.employeeInfo = item.employeeInfo;
                sPercentData.employeeInfo = item.employeeInfo;
                $.each($(".layui-form.layui-border-box.layui-table-view[lay-id='scheduleTable']  .layui-table-body.layui-table-main").find("tr[data-index="+item.LAY_TABLE_INDEX+"]").find("td"),function (c_index,c_item) {
                    if(c_index>1) {
                        if ($(c_item).find("input").length > 0){
                            if($(c_item).find("input")[0].checked) {
                                sData[$(c_item).attr("data-field")] = Number($(c_item).find("input").eq(0).val());
                            }else {
                                sData[$(c_item).attr("data-field")] = 0;
                            }
                        } else {
                            if ($(c_item).attr("data-field").indexOf("工时") >= 0){
                                if ($(c_item).context.innerText != "" && $(c_item).context.innerText != null){
                                    sHourData[$(c_item).attr("data-field")] = Number($(c_item).context.innerText);
                                    sPercentData[$(c_item).attr("data-field")] = 1;
                                    hourList.push(Number($(c_item).context.innerText));
                                } else {
                                    sHourData[$(c_item).attr("data-field")] = 0;
                                    sPercentData[$(c_item).attr("data-field")] = 0;
                                }
                            }
                        }
                    }
                });
                hourList.sort(function(a,b){
                    return b - a;
                });
                $.each(hourList,function (index2,item2) {
                    $.each(sHourData,function (index3,item3) {
                        if (index3 != "employeeInfo" && Number(item3) > 0){
                            if (item3 === Number(item2) && (1 - index2*0.2) > 0){
                                sPercentData[index3] = 1 - index2*0.2;
                            }
                        }
                    });
                });
                selectData.push(sData);
                selectHourData.push(sHourData);
                selectPercentData.push(sPercentData);
            }
        });
        var checkNull = {};
        for (var i = 0; i < selectData.length; i ++){
            $.each(selectData[i],function (index,item) {
                if (index != "employeeInfo"){
                    if (checkNull[index] == null){
                        checkNull[index] = Number(item);
                    } else {
                        checkNull[index] += Number(item);
                    }
                }
            })
        }
        console.log("=====");
        console.log(checkNull);
        var procedureFlag = false;
        $.each(checkNull, function(index,element){
            if (element == 0){
                procedureFlag = true;
            }
        });
        if(selectData.length==0) {
            layer.msg("请选择待排产的员工！", {icon: 2});
            return false;
        }
        if (procedureFlag){
            layer.msg("请为所有工序安排员工！", {icon: 2});
            return false;
        }
        var procedureSum = {};
        var title = [];
        var xData = [];
        var yData = [];
        $.each(selectHourData[0],function (index1,item1) {
            var titleLine = {};
            if (index1 === "employeeInfo") {
                titleLine.field = "employeeInfo";
                titleLine.title = "员工";
                titleLine.align = 'center';
                titleLine.width = 120;
                procedureSum[index1] = "sum";
            } else {
                xData.push(index1);
                titleLine.field = index1;
                $.each(selectedProcedureList,function (index3,item3) {
                    if (item3.procedureNumber === Number(index1.replace("号工时",""))){
                        titleLine.title = item3.procedureName
                    }
                });
                titleLine.align = 'center';
                titleLine.width = 90;
                procedureSum[index1] = 0;
            }
            title.push(titleLine);
        });
        var balanceData = [];
        $.each(selectedProcedureList,function (index3,item3) {
            var procedureLine = {};
            procedureLine[item3.procedureNumber + "号"] = 0;
            procedureLine[item3.procedureNumber + "号和"] = 0;
            procedureLine[item3.procedureNumber + "号个工"] = 0;
            balanceData.push(procedureLine);
        });
        var workHour = $("#workHour").val();
        var resultData = [];


        $.each(selectHourData,function (index1,item1) {
            var resultLine = {};
            $.each(selectData,function (index2,item2) {
                if (item1["employeeInfo"] == item2["employeeInfo"]){
                    resultLine["employeeInfo"] = item1["employeeInfo"];
                    var percentLine = selectPercentData[index2];
                    console.log(percentLine);
                    $.each(item1,function (index3,item3) {
                        if (index3 != "employeeInfo"){
                            if (item3 == 0){
                                resultLine[index3] = '';
                            } else {
                                $.each(item2,function (index4,item4) {
                                    if (index3.replace('工时', '') == index4){
                                        resultLine[index3] = Math.round((60/item4) * item3 * percentLine[index3]);
                                        procedureSum[index3] += Math.round((60/item4) * item3 * percentLine[index3]);
                                        $.each(balanceData,function (index5,item5) {
                                            if (item5[index4] != null){
                                                item5[index4] += item3;
                                                item5[index4+"和"] += item3 * item4;
                                                item5[index4+"个工"] += item3/workHour;
                                            }
                                        })
                                    }
                                });

                            }
                        }
                    })
                }
            });
            resultData.push(resultLine);
        });
        resultData.push(procedureSum);
        var maxSam = 0.0001;
        var sumSam = 0;
        $.each(balanceData,function (index6,item6) {
            var procedureSum = 0,procedureHour = 1,procedureCount = 1;
            $.each(item6,function (index7,item7) {
                if (index7.indexOf("号和") >= 0){
                    procedureSum = item7
                } else if (index7.indexOf("号个工") >= 0) {
                    procedureCount = item7
                } else {
                    procedureHour = item7;
                }
            });
            sumSam += toDecimal(procedureSum/(procedureHour*procedureCount));
            if (procedureSum/(procedureHour*procedureCount) > maxSam){
                maxSam = toDecimal(procedureSum/(procedureHour*procedureCount));
            }
        });
        var balanceIndex = toDecimal(sumSam/(maxSam * balanceData.length))*100;
        $.each(xData,function (index5,item5) {
            yData.push(procedureSum[index5]);
        });

        layui.use(['table', 'soulTable'], function () {
            var table = layui.table,
                soulTable = layui.soulTable;
            var label = "排产明细&nbsp;&nbsp;&nbsp;&nbsp;平衡率：" + balanceIndex + "%";
            var index = layer.open({
                type: 1 //Page层类型
                , title: label
                , shade: 0.6 //遮罩透明度
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , content: $("#scheduleDetailTable")
            });
            layer.full(index);

            table.render({
                elem: '#scheduleDetailTable'
                // ,totalRow: true
                ,cols: [title]
                ,page: true
                ,height: 'full-80'
                ,toolbar: '#toolbarDetailTop' //开启头部工具栏，并为其绑定左侧模板
                ,limit:100
                ,even: true
                ,limits:[50,100,200]
                ,data: resultData
                ,done: function () {
                    soulTable.render(this);
                }
            });

            table.on('toolbar(scheduleDetailTable)', function(obj) {
                if (obj.event === 'exportExcel') {
                    soulTable.export('scheduleDetailTable');
                }
            });

            //生成echarts
            var balanceFigure = document.getElementById("balanceFigure");
            var myChart = echarts.init(balanceFigure);
            option = null;
            option = {
                xAxis: {
                    type: 'category',
                    data: xData
                },
                yAxis: {
                    type: 'value'
                },
                series: [{
                    data: yData,
                    type: 'bar',
                    showBackground: true,
                    backgroundStyle: {
                        color: 'rgba(220, 220, 220, 0.8)'
                    }
                }]
            };
            if (option && typeof option === "object") {
                myChart.setOption(option, true);
            }
        });
        return false;
    });

    table.on('toolbar(scheduleTable)', function(obj) {
        if (obj.event === 'exportExcel') {
            soulTable.export('scheduleTable');
        } else if (obj.event === 'saveScheduleData'){
            var scheduleRecordList = [];
            var allScheduleData = layui.table.cache.scheduleTable;
            $.each(allScheduleData,function (index,item) {
                if (item.LAY_CHECKED) {
                    $.each(item,function (index1,item1) {
                        var tmpRecord = {};
                        var isRecord = false;
                        tmpRecord.employeeNumber = item["employeeInfo"].split("-")[0];
                        tmpRecord.employeeName = item["employeeInfo"].split("-")[1];
                        tmpRecord.orderName = $("#orderName").val();
                        tmpRecord.clothesVersionNumber = $("#clothesVersionNumber").val();
                        tmpRecord.groupName = $("#groupName").val();
                        if (index1.indexOf("号工时") >= 0){
                            tmpRecord.procedureNumber = Number(index1.replace("号工时", ""));
                            tmpRecord.hourCount = Number(item1);
                            tmpRecord.timeCount = Number(item[tmpRecord.procedureNumber + "号"]);
                            if (Number(item1) > 0){
                                isRecord = true;
                            }
                        }
                        if (isRecord){
                            scheduleRecordList.push(tmpRecord);
                        }
                    })
                }
            });
            $.ajax({
                url: "/erp/addschedulerecordbatch",
                type: 'POST',
                data: {
                    scheduleRecordJson: JSON.stringify(scheduleRecordList),
                    orderName: $("#orderName").val(),
                    groupName: $("#groupName").val()
                },
                success:function(data){
                    if (data.result == 0){
                        layer.msg("保存成功！");
                    } else {
                        layer.msg("保存失败！");
                    }
                }, error: function () {
                    layer.msg("保存失败！");
                }
            });
        } else if (obj.event === 'transRecord'){

            scheduleTable = table.render({
                elem: '#scheduleTable'
                ,url:'getuniqueschedulerecorddata'
                ,cols:[[]]
                ,loading:true
                ,data:[]
                ,done: function (res, curr, count) {
                    table.init('scheduleTable',{//转换成静态表格
                        cols:[[
                            {type:'numbers', align:'center', title:'序号', width:60, sort: true, filter: true}
                            ,{field: 'clothesVersionNumber', title: '单号',align:'center', minWidth: 200, sort: true, filter: true}
                            ,{field: 'orderName', title: '款号',align:'center', minWidth: 200, sort: true, filter: true}
                            ,{field: 'groupName', title: '组名',align:'center', minWidth: 200, sort: true, filter: true}
                            ,{field: 'createTime', title: '保存时间',align:'center', minWidth: 200, sort: true, filter: true,templet: function (d) {
                                    return layui.util.toDateString(d.createTime, 'yyyy-MM-dd');
                                }}
                            ,{title:'操作', align:'center', toolbar: '#barTop', width:220}
                        ]]
                        ,data:res.data
                        ,height: 'full-80'
                        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                        ,defaultToolbar: ['filter', 'print']
                        ,title: '历史排产表'
                        ,totalRow: true
                        ,even: true
                        ,page: true
                        ,limits: [50, 100, 200]
                        ,limit: 100 //每页默认显示的数量
                        ,overflow: 'tips'
                        ,done: function () {
                            soulTable.render(this);
                            layer.close(load);
                        }
                        ,filter: {
                            bottom: true,
                            items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                        }
                    });
                }
                ,filter: {
                    bottom: true,
                    clearFilter: false,
                    items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                }
            });

        } else if (obj.event === 'fillHour'){
            var allData = layui.table.cache.scheduleTable;
            console.log(allData);
            //首先清除历史工时数据
            $.each(allData,function (index, item) {
                $.each(item,function (index1, item1) {
                    if (index1.indexOf("工时") >= 0){
                        delete item[index1]
                    }
                });
            });
            //根据逻辑判断工时分配
            $.each(allData,function (index, item) {
                var count = 0;
                var selectData = {};
                $.each(item,function (index1, item1) {
                    if (index1.indexOf("isDefault") >= 0){
                        if (!item1){
                            count ++;
                            var tmpStr = index1.substring(0, index1.indexOf("号") +1);
                            selectData[tmpStr] = item[tmpStr];
                        }

                    }
                });
                if (count === 1){
                    $.each(selectData,function (index2, item2) {
                        item[index2+"工时"] = 11;
                    });
                } else if (count > 1){
                    var sumSam = 0;
                    $.each(selectData,function (index2, item2) {
                        sumSam += Number(item2);
                    });
                    $.each(selectData,function (index2, item2) {
                        item[index2+"工时"] = toDecimalOne(11*Number(item2)/sumSam);
                    });
                }
            });
            //重载表格
            scheduleTable = table.render({
                elem: '#scheduleTable'
                ,cols:[scheduleColumns]
                ,excel: {
                    filename: '排产报表.xlsx'
                }
                ,loading:true
                ,data:allData
                ,overflow: 'tips'
                ,height: 'full-80'
                ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                ,defaultToolbar: false
                ,title: '排产数据表'
                // ,totalRow: true
                ,page: false
                ,even: true
                ,limits: [50, 100, 200]
                ,limit: Number.MAX_VALUE //每页默认显示的数量
                ,done: function () {
                    soulTable.render(this);
                    layer.close(load);
                }
                ,filter: {
                    bottom: true,
                    clearFilter: false,
                    items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                }
            });
        }
    })

});

function generateFigure() {
    var yesterdayFinish = document.getElementById("balanceFigure");
    var myChart = echarts.init(dom);
    var app = {};
    option = null;
    option = {
        xAxis: {
            type: 'category',
            data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
        },
        yAxis: {
            type: 'value'
        },
        series: [{
            data: [120, 200, 150, 80, 70, 110, 130],
            type: 'bar',
            showBackground: true,
            backgroundStyle: {
                color: 'rgba(220, 220, 220, 0.8)'
            }
        }]
    };
    ;
    if (option && typeof option === "object") {
        myChart.setOption(option, true);
    }

}

function compare(property) {
    return function (a, b) {
        var value1 = a[property];
        var value2 = b[property];
        return value1 - value2;
    }
}

function toDecimal(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*100)/100;
    return f;
}

function toDecimalOne(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*10)/10;
    return f;
}
