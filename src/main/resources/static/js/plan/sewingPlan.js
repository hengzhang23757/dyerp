var userRole = $("#userRole").val();
var windowHeight = $(window).height();
var demo_task = {};
var ganttTable,fabricTable;
var mainData = [];
var linkData = [];
layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.21'
}).extend({                         // 模块别名
    soulTable: 'soulTable'
});
var cxm;
var sxm;
var colorStr = '';
var sizeStr = '';
var sewPlanAddUpdateTable;
$(document).ready(function () {
    $.ajax({
        url: "/erp/getsewgroupname",
        type:'GET',
        data: {
        },
        success: function (data) {
            if(data) {
                $("#groupName").empty();
                if (data.groupList) {
                    $("#groupName").append("<option value=''>选择组名</option>");
                    $.each(data.groupList, function(index,element){
                        $("#groupName").append("<option value='"+element+"'>"+element+"</option>");
                    });
                }
            }else {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，没有查到相关信息！</span>",html: true});
            }
        },
        error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }
    });
    $("#samValue").on("input propertychange",function(){
        var sam = $(this).val();
        var employeeCount = $("#employeeCount").val();
        $("#dailyCount").val(Math.round((employeeCount * 11 * 60) / sam));
    });
    cxm = xmSelect.render({
        // 这里绑定css选择器
        el: '#xmColor',
        on: function(data){
            colorStr = '';
            $.each(data.arr, function (index, item) {
                colorStr += (item.value + ",");
            });
            colorStr = colorStr.substring(0, colorStr.length - 1);
            var orderName = $("#orderName").val();
            $.ajax({
                url: "/erp/getordercountbycolorsizelist",
                data: {
                    "orderName": orderName,
                    "colorStr": colorStr,
                    "sizeStr": sizeStr
                },
                success:function(data){
                    $("#planCount").val(data.planCount)
                },
                error:function(){
                }
            });
        },
        // 渲染的数据
        data: []
    });
    sxm = xmSelect.render({
        // 这里绑定css选择器
        el: '#xmSize',
        on: function(data){
            sizeStr = '';
            $.each(data.arr, function (index, item) {
                sizeStr += (item.value + ",");
            });
            sizeStr = sizeStr.substring(0, sizeStr.length - 1);
            var orderName = $("#orderName").val();
            $.ajax({
                url: "/erp/getordercountbycolorsizelist",
                data: {
                    "orderName": orderName,
                    "sizeStr": sizeStr,
                    "colorStr": colorStr
                },
                success:function(data){
                    $("#planCount").val(data.planCount)
                },
                error:function(){
                }
            });
        },
        // 渲染的数据
        data: []
    });
    layui.laydate.render({
        elem: "#deadLine", //指定元素
        trigger: 'click'
    });
    layui.laydate.render({
        elem: "#initDate", //指定元素
        trigger: 'click',
        min: 1,
        max: 1000
    });
    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumber", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });
    initOn();
    $("#orderName").bind('blur change',function(){
        var orderName = $("#orderName").val();
        $("#taskDataID").val('');
        $.ajax({
            url: "/erp/getordercolornamesbyorder",
            data: {
                "orderName": orderName
            },
            success:function(data){
                var colorData = [{name: "全部", value: "全部", selected: false}];
                if (data.colorNameList){
                    $.each(data.colorNameList, function (index, item) {
                        colorData.push({name: item, value: item, selected: false});
                    })
                }
                cxm.update({
                    data: colorData
                });
            }, error:function(){
            }
        });
        $.ajax({
            url: "/erp/getordersizenamesbyorder",
            data: {
                "orderName": orderName
            },
            success:function(data){
                var sizeData = [{name: "全部", value: "全部", selected: false}];
                if (data.sizeNameList){
                    $.each(data.sizeNameList, function (index, item) {
                        sizeData.push({name: item, value: item, selected: false});
                    })
                }
                sxm.update({
                    data: sizeData
                });
            }, error:function(){
            }
        });
        var employeeCount = $("#employeeCount").val();
        $.ajax({
            url: "/erp/getsectionsamvaluebyordername",
            type:'GET',
            data: {
                orderName: orderName,
                procedureSection: "车缝"
            },
            success: function (data) {
                if(data) {
                    $("#samValue").val("");
                    $("#samValue").val(data.sam);
                    var employeeCount = $("#employeeCount").val();
                    $("#dailyCount").val(Math.round((employeeCount * 11 * 60) / data.sam));
                }
            }, error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        });
    });
    $("#clothesVersionNumber").bind('blur change',function(){
        var orderName = $("#orderName").val();
        $("#taskDataID").val('');
        $.ajax({
            url: "/erp/getordercolornamesbyorder",
            data: {
                "orderName": orderName
            },
            success:function(data){
                var colorData = [{name: "全部", value: "全部", selected: false}];
                if (data.colorNameList){
                    $.each(data.colorNameList, function (index, item) {
                        colorData.push({name: item, value: item, selected: false});
                    })
                }
                cxm.update({
                    data: colorData
                });
            }, error:function(){
            }
        });
        $.ajax({
            url: "/erp/getordersizenamesbyorder",
            data: {
                "orderName": orderName
            },
            success:function(data){
                var sizeData = [{name: "全部", value: "全部", selected: false}];
                if (data.sizeNameList){
                    $.each(data.sizeNameList, function (index, item) {
                        sizeData.push({name: item, value: item, selected: false});
                    })
                }
                sxm.update({
                    data: sizeData
                });
            }, error:function(){
            }
        });
        var employeeCount = $("#employeeCount").val();
        $.ajax({
            url: "/erp/getsectionsamvaluebyordername",
            type:'GET',
            data: {
                orderName: orderName,
                procedureSection: "车缝"
            },
            success: function (data) {
                if(data) {
                    $("#samValue").val("");
                    $("#samValue").val(data.sam);
                    var employeeCount = $("#employeeCount").val();
                    $("#dailyCount").val(Math.round((employeeCount * 11 * 60) / data.sam));
                }
            }, error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        });
    });
});

function initOn() {

    $.ajax({
        url: "/erp/getalltaskdata",
        type:'GET',
        data: {
        },
        success: function (data) {
            if(data) {
                mainData = [{id: 1000000, text: "车缝计划总览", type: "project", progress: 1, open: true, start_date: "01-09-2020 00:00", duration: 10000, parent: 0}];
                $.each(data.mainTaskList, function (index, item) {
                    var tmpItem = {};
                    if (item.text.indexOf("车缝") >= 0){
                        tmpItem.id = item.id;
                        tmpItem.text = item.text;
                        tmpItem.start_date = item.start_date + ' 00:00';
                        tmpItem.type = "project";
                        tmpItem.render = "split";
                        tmpItem.parent = '1000000';
                        tmpItem.progress = 0;
                        tmpItem.open = false;
                        tmpItem.duration = 10000;
                    } else {
                        tmpItem.id = item.id;
                        tmpItem.text = item.text;
                        tmpItem.start_date = item.start_date + ' 00:00';
                        tmpItem.duration = item.duration;
                        tmpItem.parent = item.parent  + '';
                        tmpItem.progress = item.progress;
                        tmpItem.open = true;
                        tmpItem.plan = 5000;
                        tmpItem.taskType = 2;
                    }
                    mainData.push(tmpItem);
                });
                $.each(data.linkList, function (index, item) {
                    var tmpItem = {};
                    tmpItem.id = item.id + '';
                    tmpItem.source = item.source + '';
                    tmpItem.target = item.target + '';
                    tmpItem.type = item.type + '';
                    linkData.push(tmpItem);
                });
                initPlan(100, [100,200]);
                gantt.parse({data: mainData, links:linkData});
            }else {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，没有查到相关信息！</span>",html: true});
            }
        }, error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }
    });

}

function initPlan(finishCount, dataList) {
    $("#gantt_here").height(windowHeight + 80);
    $("#gantt_here").width("100%");
    /*  汉化  */
    gantt.i18n.setLocale({
        date: {
            month_full: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
            month_short: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"],
            day_full: ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"],
            day_short: ["日", "一", "二", "三", "四", "五", "六"]
        },
        labels: {
            dhx_cal_today_button: "今天",
            day_tab: "日",
            week_tab: "周",
            month_tab: "月",
            new_event: "新建日程",
            icon_save: "保存",
            icon_cancel: "关闭",
            icon_details: "详细",
            icon_edit: "编辑",
            icon_delete: "删除",
            confirm_closing: "请确认是否撤销修改!", //Your changes will be lost, are your sure?
            confirm_deleting: "是否删除日程?",
            section_description: "描述",
            section_time: "时间范围",
            section_type: "类型",

            /* grid columns */

            column_text: "任务名",
            column_start_date: "开始",
            column_duration: "持续",
            column_add: "",

            /* link confirmation */

            link: "关联",
            confirm_link_deleting: "将被删除",
            link_start: " (开始)",
            link_end: " (结束)",

            type_task: "任务",
            type_project: "项目",
            type_milestone: "里程碑",

            minutes: "分钟",
            hours: "小时",
            days: "天",
            weeks: "周",
            months: "月",
            years: "年"
        }
    });


    /*  定制左侧的列  */
    gantt.config.columns = [
        {name: "text", tree: true, width: "180", resize: true, template: function (task) {
                if (task.text.indexOf("计划") != -1){
                    return "<div class='plan'>" + task.text  +"</div>";
                }else if (task.text.indexOf("实际") != -1){
                    return "<div class='acture'>" + task.text  +"</div>";
                }else {
                    return  task.text;
                }

        }},
        {name: "start_date", align: "center", resize: true},
        {name: "duration", align: "center", resize: true}
    ];



    /**全屏**/
    gantt.plugins({
        fullscreen: true
    });

    gantt.attachEvent("onTemplatesReady", function () {
        var toggle = document.createElement("i");
        toggle.className = "fa fa-expand gantt-fullscreen";
        gantt.toggleIcon = toggle;
        gantt.$container.appendChild(toggle);
        toggle.onclick = function() {
            gantt.ext.fullscreen.toggle();
        };
    });
    gantt.attachEvent("onExpand", function () {
        var icon = gantt.toggleIcon;
        if (icon) {
            icon.className = icon.className.replace("fa-expand", "fa-compress");
        }

    });
    gantt.attachEvent("onCollapse", function () {
        var icon = gantt.toggleIcon;
        if (icon) {
            icon.className = icon.className.replace("fa-compress", "fa-expand");
        }
    });
    /**全屏结束**/

    /*  今天标识  */
    gantt.plugins({
        marker: true
    });

    var dateToStr = gantt.date.date_to_str(gantt.config.task_date);
    var today = new Date(dayjs().year(), dayjs().month(), dayjs().date());
    gantt.addMarker({
        start_date: today,
        css: "today",
        text: "今天",
        title: "Today: " + dateToStr(today)
    });
    var normalList = [];
    var unNormalList = [];
    $.ajax({
        url: "/erp/getactidlistgroupbydeliverystate",
        type:'GET',
        data: {},
        async:false,
        success: function (data) {
            if(data) {
                normalList = data.normalList;
                unNormalList = data.unNormalList;
            }
        }, error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }
    });


    /*  进度提示                强化一下      */
    // gantt.templates.task_text = function(start, end, task){
    //     if (normalList.indexOf(task.id) >= 0){
    //         return "<span style='align:center;'>"+task.text + "&nbsp;&nbsp;&nbsp;<i class='layui-icon layui-icon-face-smile' style='font-size: 25px;color: green'></i></span>";
    //     } else if (unNormalList.indexOf(task.id) >= 0){
    //         return "<span style='align:center;'>"+task.text + "&nbsp;&nbsp;&nbsp;<i class='layui-icon layui-icon-face-cry' style='font-size: 25px;color: orange'></i></span>";
    //     } else {
    //         return "<span style='align:center;'>"+task.text + "</span>";
    //     }
    // };

    gantt.templates.task_class = function (start, end, task) {
        switch (task.id % 2) {
            case 0:
                return "low";
                break;
            case 1:
                return "high";
                break;
        }
    };



    /*  行颜色  */
    gantt.templates.grid_row_class = function (start_date, end_date, item) {
        if (item.backType == ""){
        } else if (item.backType == "red"){
            return "red"
        }else if (item.backType == "green"){
            return "green"
        }
    };
    gantt.templates.task_row_class = function (start_date, end_date, item) {
        if (item.backType == ""){
        } else if (item.backType == "red"){
            return "red"
        }else if (item.backType == "green"){
            return "green"
        }
    };
    /*
    gantt.templates.task_row_class = function (start_date, end_date, item) {
        if (item.progress == 0) return "red";
        if (item.progress >= 1) return "green";
    };*/

    // 双击任务事件
    gantt.attachEvent("onTaskDblClick", function(id, e) {
        if (id != null && id != ""){
            inputTask(id);
        }
    });


    /*  日期样式 */
    gantt.config.min_column_width = 50;
    gantt.config.scale_height = 90;

    var weekScaleTemplate = function (date) {
        var dateToStr = gantt.date.date_to_str("%M%d");
        var endDate = gantt.date.add(gantt.date.add(date, 1, "week"), -1, "day");
        return dateToStr(date) + " - " + dateToStr(endDate);
    };

    var daysStyle = function(date){
        // you can use gantt.isWorkTime(date)
        // when gantt.config.work_time config is enabled
        // In this sample it's not so we just check week days

        if(date.getDay() === 0 || date.getDay() === 6){
            return "weekend";
        }
        return "";
    };

    gantt.plugins({
        auto_scheduling: true
    });

    gantt.config.auto_types = true;
    gantt.config.auto_scheduling = true;
    gantt.config.auto_scheduling_compatibility = true;
    gantt.locale.labels.section_split = "Display";
    gantt.config.lightbox.project_sections = [
        {name: "description", height: 70, map_to: "text", type: "textarea", focus: true},
        {name: "split", type:"checkbox", map_to: "render", options:[
                {key:"split", label:"Split Task"}
            ]},
        {name: "time", type: "duration", readonly: true, map_to: "auto"}
    ];

    gantt.config.scales = [

        {unit: "month", step: 1, format: "%Y %F"},
        {unit: "week", step: 1, format: weekScaleTemplate},
        {unit: "day", step:1, format: "%D", css:daysStyle }

    ];
    gantt.config.scale_height = 3 * 28;
    var formDate = dayjs().subtract(7, 'day');
    var toDate = dayjs().add(2, 'month');
    gantt.init("gantt_here", new Date(formDate.year(), formDate.month(), formDate.date()), new Date(toDate.year(), toDate.month(), toDate.date()));

}

function createGanttTable(data) {
    if (ganttTable != undefined) {
        ganttTable.clear(); //清空一下table
        ganttTable.destroy(); //还原初始化了的datatable
    }
    ganttTable = $('#ganttTable').DataTable({
        "retrieve": true,
        data: data,
        language : {
            processing : "载入中",//处理页面数据的时候的显示
            paginate : {//分页的样式文本内容。
                previous : "上一页",
                next : "下一页",
                first : "第一页",
                last : "最后一页"
            },
            search:"搜索：",
            zeroRecords : "没有内容",//table tbody内容为空时，tbody的内容。
            //下面三者构成了总体的左下角的内容。
            info : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//左下角的信息显示，大写的词为关键字。
            infoEmpty : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//筛选为空时左下角的显示。
            infoFiltered : ""//筛选之后的左下角筛选提示(另一个是分页信息显示，在上面的info中已经设置，所以可以不显示)，
        },
        pageLength : 30,// 每页显示10条数据
        pagingType : "full_numbers", // 分页样式：simple,simple_numbers,full,full_numbers，
        "paging": true,
        // "ordering" : false,
        "info": false,
        searching:true,
        lengthChange:false,
        scrollY: $(document.body).height() - 250,
        fixedHeader: true,

        "columns": [
            {
                "data": null,
                "title":"序号",
                "width":"6%",
                "defaultContent": "",
                "sClass": "text-center",
                "searchable":false,
                render: function (data, type, row, meta) {
                    var no = meta.settings._iDisplayStart + meta.row + 1;
                    return no;
                }
            },{
                "data": "text",
                "title":"任务名",
                "width":"8%",
                "defaultContent": "",
                "sClass": "text-center"
            }, {
                "data": "clothesVersionNumber",
                "title":"单号",
                "width":"8%",
                "defaultContent": "",
                "sClass": "text-center"
            }, {
                "data": "orderName",
                "title":"款号",
                "width":"8%",
                "defaultContent": "",
                "sClass": "text-center"
            }, {
                "data": "groupName",
                "title":"组名",
                "width":"8%",
                "defaultContent": "",
                "sClass": "text-center"
            }, {
                "data": "startDate",
                "title":"开始日期",
                "width":"10%",
                "defaultContent": "",
                "sClass": "text-center",
                "render": function(data, type, full, meta) {
                    return  moment(data).format("YYYY-MM-DD");
                }
            }, {
                "data": "duration",
                "title":"持续(天)",
                "width":"8%",
                "defaultContent": "",
                "sClass": "text-center"
            }, {
                "data": "endDate",
                "title":"结束日期",
                "width":"10%",
                "defaultContent": "",
                "sClass": "text-center",
                "render": function(data, type, full, meta) {
                    return  moment(data).format("YYYY-MM-DD");
                }
            }, {
                "data": "sam",
                "title":"车缝SAM",
                "width":"8%",
                "defaultContent": "",
                "sClass": "text-center"
            }, {
                "data": "taskCount",
                "title":"数量",
                "width":"8%",
                "defaultContent": "",
                "sClass": "text-center"
            }, {
                "data": "progress",
                "title":"进度",
                "width":"8%",
                "defaultContent": "",
                "sClass": "text-center",
                "render": function(data, type, full, meta) {
                    return (data*100).toString() + "%";
                }
            }, {
                "data": "id",
                "title": "操作",
                "width":"10%",
                "defaultContent": "",
                "sClass": "text-center"
            }
        ],
        "columnDefs" :
            [{
                "orderable" : false, // 禁用排序
                "targets" : [11], // 指定的列
                "data" : "embPlanID",
                "render" : function(data, type, full, meta) {
                    return "<a href='#' style='color:#ff0000' onclick='deleteTaskData("+data+")'>删除</a>&nbsp;&nbsp;&nbsp;<a href='#' style='color:#3e8eea' onclick='updateTaskData(this,"+data+")'>修改</a>";
                }
            }]

    });
}

function onlyNumber(obj){
    //得到第一个字符是否为负号
    var t = obj.value.charAt(0);
    //先把非数字的都替换掉，除了数字和.
    obj.value = obj.value.replace(/[^\d\.]/g,'');
    //必须保证第一个为数字而不是.
    obj.value = obj.value.replace(/^\./g,'');
    //保证只有出现一个.而没有多个.
    obj.value = obj.value.replace(/\.{2,}/g,'.');
    //保证.只出现一次，而不能出现两次以上
    obj.value = obj.value.replace('.','$#$').replace(/\./g,'').replace('$#$','.');
    //如果第一位是负号，则允许添加
    if(t == '-'){
        obj.value = '-'+obj.value;
    }
}

var parentID;
var currentTaskDate;
var embStorageTable;
var accessoryStorageTable;
var dailyActionTable;
function inputTask(id) {
    parentID = id;
    currentTaskDate = "";
    console.log(id);
    layui.use(['form', 'soulTable', 'table','element'], function () {
        var table = layui.table,
            soulTable = layui.soulTable,
            element = layui.element,
            $ = layui.$;
        var load = layer.load();
        var taskType = '';
        var taskData = '';
        var globalGroupName = '';
        var currentTitle = "";
        $.ajax({
            url: "/erp/getalltypetaskdatabyid",
            type:'GET',
            data: {
                id: id
            },
            async:false,
            success: function (data) {
                if(data.taskData) {
                    taskType = data.taskData.taskType;
                    globalGroupName = data.taskData.groupName;
                    taskData = data.taskData;
                    if (data.currentTask){
                        currentTaskDate = moment(data.currentTask.endDate).format("YYYY-MM-DD");
                        var thisEndDate = dayjs(currentTaskDate);
                        $("#initDate").val(thisEndDate.add(1, 'day').format('YYYY-MM-DD'));
                        currentTitle += "<span style='font-weight: bolder; font-size: x-large;color: red'>该组当前款结束日期:"+ moment(data.currentTask.endDate).format("YYYY-MM-DD") +"本次排产默认接着当前款,也可手动选择排产启动日期！</span>";
                    } else {
                        currentTitle += "<span style='font-weight: bolder; font-size: x-large;color: red'>该组当前无任务,本次排产需要手动选择启动日期！</span>";
                    }
                }else {

                }
            },
            error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        });
        if (taskType === "1"){
            var title = "<span style='font-weight: bolder; font-size: x-large;color: #1E9FFF'>分组计划</span>" + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + currentTitle;
            var index = layer.open({
                type: 1 //Page层类型
                , title: title
                , btn: ['保存','计算']
                , shade: 0.6 //遮罩透明度
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , content: $('#sewPlanAdd')
                , yes: function (i, layero) {
                    var tableData = table.cache.sewPlanAddUpdateTable;
                    $.each(tableData, function (index, item) {
                        item.id = 0;
                    });
                    $.ajax({
                        url: "/erp/taskdataunifiedprocess",
                        type: 'POST',
                        data: {
                            taskDataJson:JSON.stringify(tableData),
                            groupName: globalGroupName
                        },
                        success: function (res) {
                            if (res.result == 0) {
                                // 重载甘特图
                                initOn();
                                layer.close(index);
                                $("#clothesVersionNumber").val("");
                                $("#orderName").val("");
                                $("#planCount").val("");
                                $("#groupName").val("");
                                $("#employeeCount").val("");
                                $("#samValue").val("");
                                $("#planDay").val("");
                                $("#beginDate").val("");
                                $("#endDate").val("");
                                $("#deadLine").val("");
                                $("#taskDataID").val('');
                                $("#remark").val("无");
                                sxm.update({
                                    data: []
                                });
                                cxm.update({
                                    data: []
                                });
                            } else {
                                layer.msg("保存失败！", {icon: 2});
                            }
                        },
                        error: function () {
                            layer.msg("保存失败！", {icon: 2});
                        }
                    })
                }
                , btn2: function (i, layero) {
                    var initDate = $("#initDate").val();
                    var employeeCount = $("#employeeCount").val();
                    if (initDate == '' || initDate == null){
                        layer.msg("启动日期不能为空");
                        layer.close(load);
                        return false;
                    }
                    var taskResultData = table.cache.sewPlanAddUpdateTable;
                    taskResultData.sort(function (a, b) {
                        var x = 'deadLine';
                        if (a[x] < b[x]) {
                            return -1
                        }
                        if (a[x] > b[x]) {
                            return 1
                        }
                        return 0;
                    });
                    // 计算 startDate duration  endDate  然后比较 endDate 与 deadLine 看是否逾期

                    var globalBegin = initDate;
                    $.each(taskResultData, function (index, item) {
                        item.startDate = globalBegin;
                        item.duration = Math.round((item.sam * item.taskCount)/(employeeCount * 11 * 60));
                        if (item.duration == 0){
                            item.endDate = dayjs(globalBegin).add(item.duration, 'day').format('YYYY-MM-DD');
                        } else {
                            item.endDate = dayjs(globalBegin).add(item.duration - 1, 'day').format('YYYY-MM-DD');
                        }
                        if (item.endDate > item.deadLine){
                            item.deliveryState = "逾期";
                        } else {
                            item.deliveryState = "正常";
                        }
                        globalBegin = dayjs(item.endDate).add(1, 'day').format('YYYY-MM-DD');
                    });
                    table.reload("sewPlanAddUpdateTable", {
                        data: taskResultData   // 将新数据重新载入表格
                        ,done: function () {
                            soulTable.render(this);
                            layer.close(load);
                            $("#clothesVersionNumber").val("");
                            $("#orderName").val("");
                            $("#planCount").val("");
                            $("#samValue").val("");
                            $("#dailyCount").val("");
                            $("#planDay").val("");
                            $("#beginDate").val("");
                            $("#endDate").val("");
                            $("#deadLine").val("");
                            $("#taskDataID").val('');
                            $("#remark").val("无");
                            sxm.update({
                                data: []
                            });
                            cxm.update({
                                data: []
                            });
                        }

                    });
                    return false;
                }
                , cancel: function (i, layero) {
                    $("#clothesVersionNumber").val("");
                    $("#orderName").val("");
                    $("#planCount").val("");
                    $("#groupName").val("");
                    $("#employeeCount").val("");
                    $("#dailyCount").val("");
                    $("#samValue").val("");
                    $("#planDay").val("");
                    $("#beginDate").val("");
                    $("#endDate").val("");
                    $("#deadLine").val("");
                    $("#taskDataID").val('');
                    $("#remark").val("无");
                    sxm.update({
                        data: []
                    });
                    cxm.update({
                        data: []
                    });
                }
            });
            layer.full(index);
            setTimeout(function () {
                $("#groupName").val(taskData.groupName);
                $.ajax({
                    url: "/erp/getemployeecountbygroup",
                    type:'GET',
                    data: {
                        groupName: taskData.groupName
                    },
                    async:false,
                    success: function (data) {
                        if(data) {
                            $("#employeeCount").val("");
                            $("#employeeCount").val(data.employeeCount);
                        }else {
                            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，没有查到相关信息！</span>",html: true});
                        }
                    },
                    error: function () {
                        swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
                    }
                });
                $.ajax({
                    url: "/erp/getemployeecountbygroup",
                    type:'GET',
                    data: {
                        groupName: taskData.groupName
                    },
                    async:false,
                    success: function (data) {
                        if(data) {
                            $("#employeeCount").val("");
                            $("#employeeCount").val(data.employeeCount);
                        }else {
                            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，没有查到相关信息！</span>",html: true});
                        }
                    },
                    error: function () {
                        swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
                    }
                });
                $.ajax({
                    url: "/erp/getunfinishtaskdatabygrouptype",
                    type: 'GET',
                    data: {
                        groupName: taskData.groupName,
                        taskType: 3
                    },
                    async:false,
                    success: function (res) {
                        var title = [
                            {field: 'clothesVersionNumber', title: '单号',align:'center',minWidth:140, sort: true, filter: true},
                            {field: 'orderName', title: '款号',align:'center',minWidth:140, sort: true, filter: true},
                            {field: 'colorName', title: '颜色',align:'center',minWidth:90, sort: true, filter: true},
                            {field: 'sizeName', title: '尺码',align:'center',minWidth:90, sort: true, filter: true},
                            {field: 'taskCount', title: '数量',align:'center',minWidth:80, sort: true, filter: true},
                            {field: 'groupName', title: '组名',align:'center',minWidth:90, sort: true, filter: true},
                            {field: 'startDate', title: '开始日期',align:'center',minWidth:120, sort: true, filter: true, templet:function (d) {
                                    return moment(d.startDate).format("YYYY-MM-DD");
                                }},
                            {field: 'duration', title: '持续',align:'center',minWidth:70, sort: true, filter: true},
                            {field: 'endDate', title: '结束日期',align:'center',minWidth:120, sort: true, filter: true, templet:function (d) {
                                    return moment(d.endDate).format("YYYY-MM-DD");
                                }},
                            {field: 'deadLine', title: '交期',align:'center',minWidth:120, sort: true, filter: true, templet:function (d) {
                                    return moment(d.deadLine).format("YYYY-MM-DD");
                                }},
                            {field: 'deliveryState', title: '状态',align:'center',minWidth:80, sort: true, filter: true, templet: function (d) {
                                    if (d.deliveryState == "正常") {
                                        return "<span style='color: green; font-weight: bolder'>"+ d.deliveryState +"</span>";
                                    } else {
                                        return "<span style='color: red; font-weight: bolder'>"+ d.deliveryState +"</span>";
                                    }
                                }},
                            {field: 'remark', title: '备注',align:'center',minWidth:120, sort: true, filter: true},
                            {field: 'id', title:'操作', align:'center', toolbar: '#barTop', width:70},
                            {field: 'sam', hide:true}
                        ];
                        var fabricData = res.taskDataList;
                        sewPlanAddUpdateTable = table.render({
                            elem: '#sewPlanAddUpdateTable'
                            ,cols: [title]
                            ,data: fabricData
                            ,height: 'full-330'
                            ,even: true
                            ,page: true
                            ,overflow: 'tips'
                            ,limit: 1000 //每页默认显示的数量
                            ,limits: [1000,2000] //每页默认显示的数量
                            ,filter: true
                            ,done:function (res) {
                                soulTable.render(this);
                                layer.close(load);
                            }
                        });
                        table.on('tool(sewPlanAddUpdateTable)', function(obj){
                            var data = table.cache.sewPlanAddUpdateTable;
                            if(obj.event === 'remove'){
                                layer.confirm('真的删除吗', function(index){
                                    $("#taskDataID").val('');
                                    layer.close(index);
                                    obj.del();
                                    $.each(data,function (index,item) {
                                        if(item instanceof Array) {
                                            data.splice(index,1)
                                        }
                                    });
                                    table.reload("sewPlanAddUpdateTable",{
                                        data:data   // 将新数据重新载入表格
                                    })
                                })
                            }
                        });
                        table.on('row(sewPlanAddUpdateTable)', function(obj){
                            var rowData = obj.data;
                            $("#clothesVersionNumber").val(rowData.clothesVersionNumber);
                            $("#orderName").val(rowData.orderName);
                            $("#planCount").val(rowData.taskCount);
                            $("#samValue").val(rowData.sam);
                            $("#taskDataID").val(rowData.id);
                            $("#planDay").val(rowData.duration);
                            $("#beginDate").val(moment(rowData.beginDate).format("YYYY-MM-DD"));
                            $("#endDate").val(moment(rowData.endDate).format("YYYY-MM-DD"));
                            $("#deadLine").val(moment(rowData.deadLine).format("YYYY-MM-DD"));
                            $("#remark").val(rowData.remark);
                            var colorGroup = rowData.colorName.split(",");
                            var sizeGroup = rowData.sizeName.split(",");
                            $.ajax({
                                url: "/erp/getordercolornamesbyorder",
                                data: {
                                    "orderName": rowData.orderName
                                },
                                async:false,
                                success:function(data){
                                    var colorData = [];
                                    var initFlag = true;
                                    $.each(colorGroup, function (index, item) {
                                        if (item === "全部"){
                                            initFlag = false;
                                        }
                                    });
                                    if (initFlag){
                                        colorData.push({name: "全部", value: "全部", selected: false})
                                    } else {
                                        colorData.push({name: "全部", value: "全部", selected: true})
                                    }
                                    if (data.colorNameList){
                                        $.each(data.colorNameList, function (index, item) {
                                            var flag = true;
                                            $.each(colorGroup, function (index2, item2) {
                                               if (item === item2){
                                                   flag = false;
                                               }
                                            });
                                            if (flag){
                                                colorData.push({name: item, value: item, selected: false});
                                            } else {
                                                colorData.push({name: item, value: item, selected: true});
                                            }
                                        })
                                    }
                                    cxm.update({
                                        data: colorData
                                    });
                                }, error:function(){
                                }
                            });
                            $.ajax({
                                url: "/erp/getordersizenamesbyorder",
                                data: {
                                    "orderName": rowData.orderName
                                },
                                async:false,
                                success:function(data){
                                    var sizeData = [];
                                    var initFlag = true;
                                    $.each(sizeGroup, function (index, item) {
                                        if (item === "全部"){
                                            initFlag = false;
                                        }
                                    });
                                    if (initFlag){
                                        sizeData.push({name: "全部", value: "全部", selected: false})
                                    } else {
                                        sizeData.push({name: "全部", value: "全部", selected: true})
                                    }
                                    if (data.sizeNameList){
                                        $.each(data.sizeNameList, function (index, item) {
                                            var flag = true;
                                            $.each(sizeGroup, function (index2, item2) {
                                                if (item === item2){
                                                    flag = false;
                                                }
                                            });
                                            if (flag){
                                                sizeData.push({name: item, value: item, selected: false});
                                            } else {
                                                sizeData.push({name: item, value: item, selected: true});
                                            }
                                        })
                                    }
                                    sxm.update({
                                        data: sizeData
                                    });
                                }, error:function(){
                                }
                            });
                            //标注选中样式
                            obj.tr.addClass('layui-table-click').siblings().removeClass('layui-table-click');
                        });
                    },
                    error: function () {
                        layer.msg("获取失败！", {icon: 2});
                    }
                })
            }, 100);
            layui.form.render("select");
        } else if (taskType === "3" || taskType === "4") {
            var title = "<span style='font-weight: bolder; font-size: x-large;color: #1E9FFF'>计划详情</span>" + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style='font-size: larger; font-weight: bolder; color: #FF5722'>组名:" + globalGroupName + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;单号:" + taskData.clothesVersionNumber + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;款号:" + taskData.orderName +"</span>";
            var index = layer.open({
                type: 1 //Page层类型
                , title: title
                , shade: 0.6 //遮罩透明度
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , area: '1500px'
                , offset: '10px'
                , content: $('#sewPlanDetail')
                , cancel: function (i, layero) {
                    layer.close(load);
                }
            });
            setTimeout(function () {
                $.ajax({
                    url: "/erp/gethintinfobyorderparent",
                    type:'GET',
                    data: {
                        id: id
                    },
                    success: function (data) {
                        if(data) {
                            $("#clothesVersionNumber1").text(data.taskData.clothesVersionNumber);
                            $("#orderName1").text(data.taskData.orderName);
                            $("#colorName1").text(data.taskData.colorName);
                            $("#sizeName1").text(data.taskData.sizeName);
                            $("#planCount1").text(data.taskData.taskCount);
                            $("#finishCount1").text(data.taskData.finishCount);
                            $("#dailyCount1").text(data.taskData.dailyCount);
                            $("#progress1").text(toDecimal(data.taskData.progress*100) + "%");
                            $("#startDate1").text(moment(data.taskData.startDate).format("YYYY-MM-DD"));
                            $("#endDate1").text(moment(data.taskData.endDate).format("YYYY-MM-DD"));
                            $("#deadLine1").text(moment(data.taskData.deadLine).format("YYYY-MM-DD"));
                            $("#remark1").text(data.taskData.remark);

                            // 填充表格
                            var title1 = [
                                {field: 'colorName', title: '颜色',align:'center',minWidth:120, sort: true, filter: true},
                                {field: 'sizeName', title: '尺码',align:'center',minWidth:90, sort: true, filter: true},
                                {field: 'layerCount', title: '数量',align:'center',minWidth:80, sort: true, filter: true},
                            ];
                            var embData = data.embStorageList;
                            embStorageTable = table.render({
                                elem: '#embStorageTable'
                                ,cols: [title1]
                                ,data: embData
                                ,height: 'full-330'
                                ,even: true
                                ,page: true
                                ,overflow: 'tips'
                                ,limit: 1000 //每页默认显示的数量
                                ,limits: [1000,2000] //每页默认显示的数量
                                ,filter: true
                                ,done:function (res) {
                                    soulTable.render(this);
                                    layer.close(load);
                                }
                            });


                            var title2 = [
                                {field: 'accessoryName', title: '名称',align:'center',minWidth:120, sort: true, filter: true},
                                {field: 'colorName', title: '颜色',align:'center',minWidth:120, sort: true, filter: true},
                                {field: 'sizeName', title: '尺码',align:'center',minWidth:90, sort: true, filter: true},
                                {field: 'accessoryCount', title: '订购',align:'center',minWidth:80, sort: true, filter: true},
                                {field: 'accessoryReturnCount', title: '入库',align:'center',minWidth:80, sort: true, filter: true},
                                {field: 'accessoryStorageCount', title: '库存',align:'center',minWidth:80, sort: true, filter: true},
                            ];
                            var accessoryData = data.manufactureAccessoryList;
                            accessoryStorageTable = table.render({
                                elem: '#accessoryStorageTable'
                                ,cols: [title2]
                                ,data: accessoryData
                                ,height: 'full-330'
                                ,even: true
                                ,page: true
                                ,overflow: 'tips'
                                ,limit: 1000 //每页默认显示的数量
                                ,limits: [1000,2000] //每页默认显示的数量
                                ,filter: true
                                ,done:function (res) {
                                    soulTable.render(this);
                                    layer.close(load);
                                }
                            });
                            var title3 = [
                                {field:'generalDate', title:'日期', align:'center', width:'50%', sort: true, filter: true, templet:function (d) {
                                        return moment(d.generalDate).format("YYYY-MM-DD");
                                    }},
                                {field: 'pieceCount', title: '数量',align:'center',width:'50%', sort: true, filter: true}
                            ];
                            var generalData = data.generalSalaryList;
                            dailyActionTable = table.render({
                                elem: '#dailyActionTable'
                                ,cols: [title3]
                                ,data: generalData
                                ,height: 'full-310'
                                ,even: true
                                ,page: true
                                ,overflow: 'tips'
                                ,limit: 1000 //每页默认显示的数量
                                ,limits: [1000,2000] //每页默认显示的数量
                                ,filter: true
                                ,done:function (res) {
                                    soulTable.render(this);
                                    layer.close(load);
                                }
                            });
                        }else {
                            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，没有查到相关信息！</span>",html: true});
                        }
                    }, error: function () {
                        swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
                    }
                });
            }, 500);
            // layer.full(index);
            // layui.form.render("select");
        } else {
            layer.close(load);
            return "";
        }
    })
}

function confirmTaskData() {
    layui.use(['form', 'soulTable', 'table'], function () {
        var table = layui.table,
            soulTable = layui.soulTable,
            $ = layui.$;
        var load = layer.load();
        var taskDataID = $("#taskDataID").val();
        var taskCacheData = table.cache.sewPlanAddUpdateTable;
        // 检测是否为空
        var orderName = $("#orderName").val();
        var clothesVersionNumber = $("#clothesVersionNumber").val();
        var groupName = $("#groupName").val();
        var taskCount = $("#planCount").val();
        var employeeCount = $("#employeeCount").val();
        var sam = $("#samValue").val();
        var deadLine = $("#deadLine").val();
        var initDate = $("#initDate").val();
        var colorName = cxm.getValue("valueStr");
        var sizeName = sxm.getValue("valueStr");
        var remark = $("#remark").val();
        var dailyCount = $("#dailyCount").val();
        if (orderName == '' || orderName == null || initDate == '' || initDate == null || dailyCount == '' || dailyCount == null || clothesVersionNumber == '' || clothesVersionNumber == null || groupName == '' || groupName == null || taskCount == '' || taskCount == null || employeeCount == '' || employeeCount == null || sam == '' || sam == null || colorName == '' || colorName == null || sizeName == '' || sizeName == null){
            layer.msg("请输入完整信息");
            layer.close(load);
            return false;
        }
        if (sam == 0){
            layer.alert("CT值不能是0, 如果没有录入工序请手动输入车缝CT值", {
                title: 'CT值异常'
            });
            layer.close(load);
            return false;
        }
        taskCacheData.sort(function (a, b) {
            var x = 'deadLine';
            if (a[x] < b[x]) {
                return -1
            }
            if (a[x] > b[x]) {
                return 1
            }
            return 0;
        });
        var taskResultData = [];
        if (taskDataID == '' || taskDataID == null){
            var tmpTask = {};
            tmpTask.id = new Date().getTime();
            tmpTask.text = orderName;
            tmpTask.orderName = orderName;
            tmpTask.clothesVersionNumber = clothesVersionNumber;
            tmpTask.groupName = groupName;
            tmpTask.taskCount = taskCount;
            tmpTask.employeeCount = employeeCount;
            tmpTask.sam = sam;
            tmpTask.progress = 1;
            tmpTask.taskOpen = 'true';
            tmpTask.deadLine = deadLine;
            tmpTask.parent = parentID;
            tmpTask.colorName = colorName;
            tmpTask.sizeName = sizeName;
            tmpTask.remark = remark;
            if (taskCacheData == null || taskCacheData == null || taskCacheData.length == 0){
                taskResultData.push(tmpTask);
            } else {
                if (deadLine <= taskCacheData[0].deadLine){
                    taskResultData[0] = tmpTask;
                    $.each(taskCacheData, function (index, item) {
                        taskResultData.push(item);
                    })
                } else if (deadLine >= taskCacheData[0].deadLine) {
                    $.each(taskCacheData, function (index, item) {
                        taskResultData.push(item);
                    });
                    taskResultData.push(tmpTask);
                } else {
                    for (var i = 0; i < taskCacheData.length - 1; i ++){
                        if (deadLine < taskCacheData[i].deadLine && deadLine >= taskCacheData[i+1].deadLine){
                            taskResultData.push(tmpTask);
                        }
                        taskResultData.push(taskCacheData[i])
                    }
                }
            }
        } else {
            $.each(taskCacheData, function (index, item) {
                if (item.id){
                    if (item.id == taskDataID){
                        item.orderName = orderName;
                        item.text = orderName;
                        item.clothesVersionNumber = clothesVersionNumber;
                        item.sam = sam;
                        item.deadLine = deadLine;
                        item.colorName = colorName;
                        item.sizeName = sizeName;
                        item.remark = remark;
                        item.taskCount = taskCount;
                        item.employeeCount = employeeCount;
                    }
                }
            });
            taskResultData = taskCacheData;
        }
        taskResultData.sort(function (a, b) {
            var x = 'deadLine';
            if (a[x] < b[x]) {
                return -1
            }
            if (a[x] > b[x]) {
                return 1
            }
            return 0;
        });
        // 计算 startDate duration  endDate  然后比较 endDate 与 deadLine 看是否逾期
        var globalBegin = initDate;
        $.each(taskResultData, function (index, item) {
            item.startDate = globalBegin;
            item.duration = Math.round((item.sam * item.taskCount)/(employeeCount * 11 * 60));
            if (item.duration == 0){
                item.endDate = dayjs(globalBegin).add(item.duration, 'day').format('YYYY-MM-DD');
            } else {
                item.endDate = dayjs(globalBegin).add(item.duration - 1, 'day').format('YYYY-MM-DD');
            }
            if (item.endDate > item.deadLine){
                item.deliveryState = "逾期";
            } else {
                item.deliveryState = "正常";
            }
            globalBegin = dayjs(item.endDate).add(1, 'day').format('YYYY-MM-DD');
        });
        table.reload("sewPlanAddUpdateTable", {
            data: taskResultData   // 将新数据重新载入表格
            ,done: function () {
                soulTable.render(this);
                layer.close(load);
                $("#clothesVersionNumber").val("");
                $("#orderName").val("");
                $("#planCount").val("");
                $("#samValue").val("");
                $("#dailyCount").val("");
                $("#planDay").val("");
                $("#beginDate").val("");
                $("#endDate").val("");
                $("#deadLine").val("");
                $("#taskDataID").val('');
                $("#remark").val("无");
                sxm.update({
                    data: []
                });
                cxm.update({
                    data: []
                });
            }

        });
    })
}
function toDecimal(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*100)/100;
    return f;
}
