layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.21'
}).extend({                         // 模块别名
    soulTable: 'soulTable',
});
var empSkillsTable;
layui.use(['form', 'soulTable', 'table', 'upload','yutons_sug'], function () {
    var table = layui.table,
        soulTable = layui.soulTable
    var load = layer.load();
    empSkillsTable = table.render({
        elem: '#empSkillsTable'
        ,url:'getallempskills'
        ,method:'post'
        ,cols: [[]]
        ,loading:true
        ,done: function (res, curr, count) {
            table.init('empSkillsTable',{//转换成静态表格
                cols:[[
                    {type:'numbers', align:'center', title:'序号', width:60}
                    ,{field:'groupName', title:'组名', align:'center', width:120, sort: true, filter: true, totalRowText: '合计'}
                    ,{field:'employeeName', title:'姓名', align:'center', width:120, sort: true, filter: true}
                    ,{field:'employeeNumber', title:'工号', align:'center', width:120, sort: true, filter: true}
                    ,{field:'procedureName', title:'工序名', align:'center',width:200, sort: true, filter: true}
                    ,{field:'procedureNumber', title:'工序号', align:'center', width:120, sort: true, filter: true}
                    ,{field:'procedureCode', title:'工序代码', align:'center', width:120, sort: true, filter: true, totalRow: true}
                    ,{field:'sam', title:'标准SAM', align:'center', width:120, sort: true, filter: true}
                    ,{field:'timeCount', title:'技能', align:'center', width:120, sort: true, filter: true}
                    ,{field:'updateTime', title:'最后更新', align:'center', width:200, sort: true, filter: true,templet: function (d) {
                            return layui.util.toDateString(d.updateTime, 'yyyy-MM-dd');
                        }}
                ]]
                ,data:res.data
                ,height: 'full-50'
                ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                ,defaultToolbar: ['filter', 'print']
                ,title: '员工技能表'
                ,totalRow: true
                ,even: true
                ,page: true
                ,limits: [500, 1000, 2000]
                ,limit: 1000 //每页默认显示的数量
                ,overflow: 'tips'
                ,done: function () {
                    soulTable.render(this);
                    layer.close(load);
                }
                ,filter: {
                    bottom: true,
                    items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                }
            });
        }
    });


    table.on('toolbar(empSkillsTable)', function(obj) {
        if (obj.event === 'clearFilter') {
            // 清除所有筛选条件并重载表格
            // 参数: tableId
            soulTable.clearFilter('empSkillsTable')
        } else if (obj.event === 'refresh') {
            empSkillsTable.reload();
        } else if (obj.event === 'exportExcel') {
            soulTable.export('empSkillsTable');
        }
    });

});