var userName = $("#userName").val();
var userRole = $("#userRole").val();
var form;
var supplierDemo;
layui.use('form', function(){
    form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
    form.render();
});

$(document).ready(function () {



});
$(document).on('keydown', '.layui-table-edit', function (e) {
    var td = $(this).parent('td')
    ,tr = td.parent('tr')
    ,trs = tr.parent().parent().find('tr')
    ,tr_index = tr.index()
    ,td_index = td.index()
    ,td_last_index = tr.find('[data-edit="text"]:last').index()
    ,td_first_index = tr.find('[data-edit="text"]:first').index();
    switch (e.keyCode) {
        case 13:
            tr.next().find('td').eq(td_index).click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            return false;
        case 39:
            td.nextAll('[data-edit="text"]:first').click();
            if (td_index == td_last_index){
                tr.next().find('td').eq(td_first_index).click();
                if (tr_index == trs.length - 1){
                    trs.eq(0).find('td').eq(td_first_index).click();
                }
            }
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
        case 37:
            td.prevAll('[data-edit="text"]:first').click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
        case 38:
            tr.prev().find('td').eq(td_index).click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
        case 40:
            tr.next().find('td').eq(td_index).click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
    }

});
layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.26'
}).extend({                         // 模块别名
    soulTable: 'soulTable'
});
var dataTable;
layui.use(['form', 'soulTable', 'table', 'upload','yutons_sug', 'element'], function () {

    var table = layui.table,
        soulTable = layui.soulTable,
        element = layui.element,
        form = layui.form,
        $ = layui.$;

    dataTable = table.render({
        elem: '#dataTable'
        ,cols: [[]]
        ,loading:true
        ,data:[]
        ,height: 'full-80'
        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
        ,title: '检测模板'
        ,totalRow: true
        ,page: true
        ,even: true
        ,overflow: 'tips'
        ,limits: [50, 100, 200]
        ,limit: 50 //每页默认显示的数量
        ,done: function (res, curr, count) {
            soulTable.render(this);
        }
    });

    initTable();

    function initTable(){
        $.ajax({
            url: "/erp/getdetectiontemplatebyinfo",
            type: 'GET',
            data: {},
            success: function (res) {
                if (res.detectionTemplateList) {
                    var reportData = res.detectionTemplateList;
                    table.render({
                        elem: '#dataTable'
                        ,cols:[[
                            {type:'numbers', align:'center', title:'序号', width:60}
                            ,{field:'detectionType', title:'类别', align:'center', width:170, sort: true, filter: true}
                            ,{field:'detectionName', title:'名称', align:'center', width:200, sort: true, filter: true, totalRowText: '合计'}
                            ,{field:'updateTime', title:'最后修改', align:'center', width:200, sort: true, filter: true,templet: function (d) {
                                    return moment(d.updateTime).format("YYYY-MM-DD HH:mm:ss");
                                }}
                            ,{field:'id', fixed: 'right', title:'操作', align:'center', toolbar: '#barTop', width:280}
                        ]]
                        ,data:reportData
                        ,height: 'full-120'
                        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                        ,title: '检测模板'
                        ,totalRow: true
                        ,page: true
                        ,even: true
                        ,overflow: 'tips'
                        ,limits: [50, 100, 200]
                        ,limit: 50 //每页默认显示的数量
                        ,done: function (res, curr, count) {
                            soulTable.render(this);
                        }
                    });
                }
            }
        })
    }

    // form.on('submit(searchBeat)', function(data){
    //     var filterFabricName = $("#fabricName").val();
    //     var filterFabricNumber = $("#fabricNumber").val();
    //     initTable(filterFabricName, filterFabricNumber);
    //     return false;
    // });

    table.on('toolbar(dataTable)', function(obj){
        if (obj.event === 'clearFilter') {
            // 清除所有筛选条件并重载表格
            // 参数: tableId
            soulTable.clearFilter('dataTable');
        }else if (obj.event === 'refresh') {
            initTable();
        } else if (obj.event === 'exportExcel') {
            soulTable.export('dataTable');
        }else if(obj.event === 'addNew') {  //录入基础信息
            var index = layer.open({
                type: 1 //Page层类型
                , title: "新增"
                , btn: ['保存']
                , shade: 0.6 //遮罩透明度
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , content: $("#templateOperate")
                ,yes: function(i, layero){
                    var param = {};
                    param.detectionType = $("#detectionType").val();
                    param.detectionName = $("#detectionName").val();
                    param.detectionUrl = tinymce.get('detectionTemplateTextarea').getContent();
                    if (param.detectionType == null || param.detectionType === '' || param.detectionName == null || param.detectionName === ''){
                        layer.msg("请输入完整必要信息~~~");
                        return false;
                    }
                    $.ajax({
                        url: "/erp/adddetectiontemplate",
                        type: 'POST',
                        data: {
                            detectionTemplateJson: JSON.stringify(param)
                        },
                        success: function (res) {
                            if (res.result == 0) {
                                layer.close(index);
                                layer.msg("添加成功！", {icon: 1});
                                initTable('', '');
                            } else {
                                layer.msg("添加失败！", {icon: 2});
                            }
                        },
                        error: function () {
                            layer.msg("添加失败", { icon: 2 });
                        }
                    });
                }
                ,cancel:function () {
                    tinymce.remove("#detectionTemplateTextarea");
                    $("#detectionType").val('');
                    $("#detectionName").val('');
                }
            });
            layer.full(index);
            form.render('select');
            setTimeout(function () {
                tinymce.init({
                    selector: '#detectionTemplateTextarea',
                    language: 'zh_CN',//中文
                    placeholder: '创建好模板后面就能多次复用~~',
                    directionality: 'ltr',  //文字方向,ltr文字方向从左到右，rtl从右到左
                    browser_spellcheck: true,
                    contextmenu: false,
                    branding: false, //技术支持(Powered by Tiny || 由Tiny驱动)
                    menubar: false, //菜单栏
                    plugins: [
                        "print image autoresize table powerpaste"
                    ],
                    powerpaste_word_import: 'propmt',// 参数可以是propmt, merge, clear，效果自行切换对比
                    powerpaste_html_import: 'propmt',// propmt, merge, clear
                    powerpaste_allow_local_images: true,
                    paste_data_images: true,
                    relative_urls : false,
                    remove_script_host : false,
                    convert_urls : true,
                    toolbar: "print | undo redo | bold | alignleft aligncenter alignright alignjustify | fontselect | fontsizeselect | image table",
                    images_upload_handler: function(blobInfo, success, failure){
                        handleImgUpload(blobInfo, success, failure)
                    }
                });
            },100);
        }
    });

    //监听行工具事件
    table.on('tool(dataTable)', function(obj){
        var data = obj.data;
        if(obj.event === 'del'){
            layer.confirm('确认删除吗?', function(index){
                $.ajax({
                    url: "/erp/deletedetectiontemplate",
                    type: 'POST',
                    data: {
                        id:data.id
                    },
                    success: function (res) {
                        if (res.result == 0) {
                            initTable();
                            layer.close(index);
                            layer.msg("删除成功！", {icon: 1});
                        } else {
                            layer.msg("删除失败！", {icon: 2});
                        }
                    },
                    error: function () {
                        layer.msg("删除失败！", {icon: 2});
                    }
                })

            });
        } else if(obj.event === 'update'){
            var index = layer.open({
                type: 1 //Page层类型
                , title: "修改"
                , btn: ['保存']
                , shade: 0.6 //遮罩透明度
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , content: $("#templateOperate")
                ,yes: function(i, layero){
                    var param = {};
                    param.id = data.id;
                    param.detectionType = $("#detectionType").val();
                    param.detectionName = $("#detectionName").val();
                    param.detectionUrl = tinymce.get('detectionTemplateTextarea').getContent();
                    if (param.detectionType == null || param.detectionType === '' || param.detectionName == null || param.detectionName === ''){
                        layer.msg("请输入完整必要信息~~~");
                        return false;
                    }
                    $.ajax({
                        url: "/erp/updatedetectiontemplate",
                        type: 'POST',
                        data: {
                            detectionTemplateJson: JSON.stringify(param)
                        },
                        success: function (res) {
                            if (res.result == 0) {
                                layer.close(index);
                                layer.msg("修改成功！", {icon: 1});
                                initTable();
                                tinymce.remove("#detectionTemplateTextarea");
                                $("#detectionType").val('');
                                $("#detectionName").val('');
                            } else {
                                layer.msg("修改失败！", {icon: 2});
                            }
                        },
                        error: function () {
                            layer.msg("修改失败", { icon: 2 });
                        }
                    });
                }
                ,cancel:function () {
                    tinymce.remove("#detectionTemplateTextarea");
                    $("#detectionType").val('');
                    $("#detectionName").val('');
                }
            });
            layer.full(index);
            form.render('select');
            setTimeout(function () {
                tinymce.init({
                    selector: '#detectionTemplateTextarea',
                    language: 'zh_CN',//中文
                    placeholder: '创建好模板后面就能多次复用~~',
                    directionality: 'ltr',  //文字方向,ltr文字方向从左到右，rtl从右到左
                    browser_spellcheck: true,
                    contextmenu: false,
                    branding: false, //技术支持(Powered by Tiny || 由Tiny驱动)
                    menubar: false, //菜单栏
                    plugins: [
                        "print image autoresize table powerpaste"
                    ],
                    powerpaste_word_import: 'propmt',// 参数可以是propmt, merge, clear，效果自行切换对比
                    powerpaste_html_import: 'propmt',// propmt, merge, clear
                    powerpaste_allow_local_images: true,
                    paste_data_images: true,
                    relative_urls : false,
                    remove_script_host : false,
                    convert_urls : true,
                    toolbar: "print | undo redo | bold | alignleft aligncenter alignright alignjustify | fontselect | fontsizeselect | image table",
                    images_upload_handler: function(blobInfo, success, failure){
                        handleImgUpload(blobInfo, success, failure)
                    }
                });

                $.ajax({
                    url: "/erp/getdetectiontemplatebyid",
                    type: 'GET',
                    data: {
                        id: data.id
                    },
                    success: function (res) {
                        if (res.detectionTemplate) {
                            setTimeout(function () {
                                tinymce.get('detectionTemplateTextarea').setContent(res.detectionTemplate.detectionUrl);
                                $("#detectionType").val(res.detectionTemplate.detectionType);
                                $("#detectionName").val(res.detectionTemplate.detectionName);
                            }, 100);
                        } else {
                            layer.msg("添加失败！", {icon: 2});
                        }
                    },
                    error: function () {
                        layer.msg("添加失败", { icon: 2 });
                    }
                });

            },100);
            form.render('select');
        }
    });
});

function toDecimal(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*100)/100;
    return f;
}

function moveCursor(event,obj) {
    if(event.keyCode==38){
        var name = $(obj).attr("name");
        var index = $("input[name='"+name+"']").index(obj);
        $("input[name='"+name+"']").eq(index-1).focus();
    }else if(event.keyCode==40){
        var name = $(obj).attr("name");
        var index = $("input[name='"+name+"']").index(obj);
        $("input[name='"+name+"']").eq(index+1).focus();
    }
}

function handleImgUpload(blobInfo, success, failure){
    var formdata = new FormData();
    // append 方法中的第一个参数就是 我们要上传文件 在后台接收的文件名
    // 这个值要根据后台来定义
    // 第二个参数是我们上传的文件
    formdata.append('file', blobInfo.blob());
    $.ajax({
        url: "/erp/wangEditorUploadImg",
        type: 'post',
        dataType:'json',
        contentType:false,//ajax上传图片需要添加
        processData:false,//ajax上传图片需要添加
        data: formdata,
        success: function (res) {
            if(res.data) {
                console.log(res)
                success(res.data[0]);
            }else {
                failure("上传图片失败");
            }
        },
        error: function () {
            layer.msg("上传图片失败", { icon: 2 });
        }
    });

}
