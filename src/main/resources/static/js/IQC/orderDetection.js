var userName = $("#userName").val();
var userRole = $("#userRole").val();
var form;
var supplierDemo;
layui.use('form', function(){
    form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
    form.render();
});

$(document).ready(function () {

    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumber", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });

});
$(document).on('keydown', '.layui-table-edit', function (e) {
    var td = $(this).parent('td')
    ,tr = td.parent('tr')
    ,trs = tr.parent().parent().find('tr')
    ,tr_index = tr.index()
    ,td_index = td.index()
    ,td_last_index = tr.find('[data-edit="text"]:last').index()
    ,td_first_index = tr.find('[data-edit="text"]:first').index();
    switch (e.keyCode) {
        case 13:
            tr.next().find('td').eq(td_index).click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            return false;
        case 39:
            td.nextAll('[data-edit="text"]:first').click();
            if (td_index == td_last_index){
                tr.next().find('td').eq(td_first_index).click();
                if (tr_index == trs.length - 1){
                    trs.eq(0).find('td').eq(td_first_index).click();
                }
            }
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
        case 37:
            td.prevAll('[data-edit="text"]:first').click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
        case 38:
            tr.prev().find('td').eq(td_index).click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
        case 40:
            tr.next().find('td').eq(td_index).click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
    }

});
layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.26'
}).extend({                         // 模块别名
    soulTable: 'soulTable'
});
var dataTable;
layui.use(['form', 'soulTable', 'table', 'upload','yutons_sug', 'element'], function () {

    var table = layui.table,
        soulTable = layui.soulTable,
        element = layui.element,
        form = layui.form,
        $ = layui.$;

    dataTable = table.render({
        elem: '#dataTable'
        ,cols: [[]]
        ,loading:true
        ,data:[]
        ,height: 'full-80'
        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
        ,title: '检测管理'
        ,totalRow: true
        ,page: true
        ,even: true
        ,overflow: 'tips'
        ,limits: [50, 100, 200]
        ,limit: 50 //每页默认显示的数量
        ,done: function (res, curr, count) {
            soulTable.render(this);
        }
    });

    initTable('');

    function initTable(orderName){
        var param = {};
        if (orderName != null && orderName != ''){
            param.orderName = orderName;
        }
        $.ajax({
            url: "/erp/getorderdetectionbyinfo",
            type: 'GET',
            data: param,
            success: function (res) {
                if (res.orderDetectionList) {
                    var reportData = res.orderDetectionList;
                    table.render({
                        elem: '#dataTable'
                        ,cols:[[
                            {type:'numbers', align:'center', title:'序号', width:60}
                            ,{field:'clothesVersionNumber', title:'单号', align:'center', width:170, sort: true, filter: true}
                            ,{field:'orderName', title:'款号', align:'center', width:170, sort: true, filter: true}
                            ,{field:'customerName', title:'客户', align:'center', width:170, sort: true, filter: true}
                            ,{field:'detectionType', title:'类别', align:'center', width:170, sort: true, filter: true}
                            ,{field:'detectionName', title:'检测名称', align:'center', width:170, sort: true, filter: true}
                            ,{field:'sendDate', title:'送检日期', align:'center', width:170, sort: true, filter: true,templet: function (d) {
                                    return moment(d.sendDate).format("YYYY-MM-DD");
                                }}
                            ,{field:'detectionDate', title:'检测日期', align:'center', width:170, sort: true, filter: true,templet: function (d) {
                                    return moment(d.detectionDate).format("YYYY-MM-DD");
                                }}
                            ,{field:'detectionDepartment', title:'检测部门', align:'center', width:170, sort: true, filter: true}
                            ,{field:'id', fixed: 'right', title:'操作', align:'center', toolbar: '#barTop', width:180}
                        ]]
                        ,data:reportData
                        ,height: 'full-120'
                        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                        ,title: '检测模板'
                        ,totalRow: true
                        ,page: true
                        ,even: true
                        ,overflow: 'tips'
                        ,limits: [50, 100, 200]
                        ,limit: 50 //每页默认显示的数量
                        ,done: function (res, curr, count) {
                            soulTable.render(this);
                        }
                    });
                }
            }
        })
    }

    form.on('submit(searchBeat)', function(data){
        var orderName = $("#orderName").val();
        initTable(orderName);
        return false;
    });

    table.on('toolbar(dataTable)', function(obj){
        if (obj.event === 'clearFilter') {
            // 清除所有筛选条件并重载表格
            // 参数: tableId
            soulTable.clearFilter('dataTable');
        }else if (obj.event === 'refresh') {
            initTable('');
        } else if (obj.event === 'exportExcel') {
            soulTable.export('dataTable');
        }else if(obj.event === 'addNew') {  //录入基础信息
            var index = layer.open({
                type: 1 //Page层类型
                , title: "新增"
                , btn: ['保存']
                , shade: 0.6 //遮罩透明度
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , content: $("#templateOperate")
                ,yes: function(i, layero){
                    var param = {};
                    param.orderName = $("#orderNameTwo").val();
                    param.clothesVersionNumber = $("#clothesVersionNumberTwo").val();
                    param.customerName = $("#customerName").val();
                    param.detectionType = $("#detectionType").val();
                    param.detectionName = $("#detectionName").val();
                    param.sendDate = $("#sendDate").val();
                    var detectionDate = $("#detectionDate").val();
                    if (detectionDate != null && detectionDate != ''){
                        param.detectionDate = detectionDate;
                    }
                    param.detectionDepartment = $("#detectionDepartment").val();
                    param.detectionUrl = tinymce.get('orderDetectionTextarea').getContent();
                    if (param.orderName == null || param.orderName === '' || param.clothesVersionNumber == null || param.clothesVersionNumber === '' || param.customerName == null || param.customerName === '' || param.detectionUrl == null || param.detectionUrl === '' || param.detectionDepartment == null || param.detectionDepartment === '' || param.detectionType == null || param.detectionType === '' || param.detectionName == null || param.detectionName === '' || param.sendDate == null || param.sendDate === ''){
                        layer.msg("请输入完整必要信息~~~");
                        return false;
                    }
                    $.ajax({
                        url: "/erp/addorderdetection",
                        type: 'POST',
                        data: {
                            orderDetectionJson: JSON.stringify(param)
                        },
                        success: function (res) {
                            if (res.result == 0) {
                                layer.close(index);
                                layer.msg("添加成功！", {icon: 1});
                                initTable('');
                                tinymce.remove("#orderDetectionTextarea");
                                $("#orderNameTwo").val('');
                                $("#clothesVersionNumberTwo").val('');
                                $("#customerName").val('');
                                $("#detectionType").val('');
                                $("#detectionName").val('');
                                $("#sendDate").val('');
                                $("#detectionDate").val('');
                                $("#detectionDepartment").val('');

                            } else {
                                layer.msg("添加失败！", {icon: 2});
                            }
                        },
                        error: function () {
                            layer.msg("添加失败", { icon: 2 });
                        }
                    });
                }
                ,cancel:function () {
                    tinymce.remove("#orderDetectionTextarea");
                    $("#orderNameTwo").val('');
                    $("#clothesVersionNumberTwo").val('');
                    $("#customerName").val('');
                    $("#detectionType").val('');
                    $("#detectionName").val('');
                    $("#sendDate").val('');
                    $("#detectionDate").val('');
                    $("#detectionDepartment").val('');
                }
            });
            layer.full(index);
            form.render('select');
            setTimeout(function () {
                tinymce.init({
                    selector: '#orderDetectionTextarea',
                    language: 'zh_CN',//中文
                    placeholder: '通过类别、名称直接加载出模板,只需填空即可~~',
                    directionality: 'ltr',  //文字方向,ltr文字方向从左到右，rtl从右到左
                    browser_spellcheck: true,
                    contextmenu: false,
                    branding: false, //技术支持(Powered by Tiny || 由Tiny驱动)
                    menubar: false, //菜单栏
                    plugins: [
                        "print image autoresize table powerpaste"
                    ],
                    powerpaste_word_import: 'propmt',// 参数可以是propmt, merge, clear，效果自行切换对比
                    powerpaste_html_import: 'propmt',// propmt, merge, clear
                    powerpaste_allow_local_images: true,
                    paste_data_images: true,
                    relative_urls : false,
                    remove_script_host : false,
                    convert_urls : true,
                    toolbar: "print | undo redo | bold | alignleft aligncenter alignright alignjustify | fontselect | fontsizeselect | image table",
                    images_upload_handler: function(blobInfo, success, failure){
                        handleImgUpload(blobInfo, success, failure)
                    }
                });

                layui.laydate.render({
                    elem: '#sendDate',
                    trigger: 'click'
                });

                layui.laydate.render({
                    elem: '#detectionDate',
                    trigger: 'click'
                });

                layui.use(['yutons_sug'], function () {
                    layui.yutons_sug.render({
                        id: "clothesVersionNumberTwo", //设置容器唯一id
                        height: "300",
                        width: "550",
                        limit:"10",            limits:[10,20,50,100],
                        cols: [
                            [{
                                field: 'clothesVersionNumberTwo',
                                title: '单号',
                                align: 'left'
                            }, {
                                field: 'orderNameTwo',
                                title: '款号',
                                align: 'left'
                            }]
                        ], //设置表头
                        params: [
                            {
                                name: 'clothesVersionNumberTwo',
                                field: 'clothesVersionNumberTwo'
                            }, {
                                name: 'orderNameTwo',
                                field: 'orderNameTwo'
                            }],//设置字段映射，适用于输入一个字段，回显多个字段
                        type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
                        url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
                    });

                    layui.yutons_sug.render({
                        id: "orderNameTwo", //设置容器唯一id
                        height: "300",
                        width: "550",
                        limit:"10",            limits:[10,20,50,100],
                        cols: [
                            [{
                                field: 'clothesVersionNumberTwo',
                                title: '单号',
                                align: 'left'
                            }, {
                                field: 'orderNameTwo',
                                title: '款号',
                                align: 'left'
                            }]
                        ], //设置表头
                        params: [
                            {
                                name: 'clothesVersionNumberTwo',
                                field: 'clothesVersionNumberTwo'
                            }, {
                                name: 'orderNameTwo',
                                field: 'orderNameTwo'
                            }],//设置字段映射，适用于输入一个字段，回显多个字段
                        type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
                        url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
                    });
                });

                $("#orderNameTwo").bind('blur change',function(){
                    var orderName = $("#orderNameTwo").val();
                    $.ajax({
                        url: "/erp/getonlymanufactureorderbyordername",
                        data: {"orderName": orderName},
                        success:function(data){
                            $("#customerName").val('');
                            if (data.manufactureOrder) {
                                $("#customerName").val(data.manufactureOrder.customerName);
                            }
                        },
                        error:function(){
                        }
                    });
                });

                $("#clothesVersionNumberTwo").bind('blur change',function(){
                    var orderName = $("#orderNameTwo").val();
                    $.ajax({
                        url: "/erp/getonlymanufactureorderbyordername",
                        data: {"orderName": orderName},
                        success:function(data){
                            $("#customerName").val('');
                            if (data.manufactureOrder) {
                                $("#customerName").val(data.manufactureOrder.customerName);
                            }
                        },
                        error:function(){
                        }
                    });
                });

                form.on('select(demo)', function(data){
                    var detectionType = $("#detectionType").val();
                    if (detectionType != null && detectionType != ''){
                        $.ajax({
                            url: "/erp/getdetectionnamebydetectiontype",
                            data: {
                                detectionType: detectionType
                            },
                            success:function(data){
                                if (data.detectionNameList){
                                    $("#detectionName").empty();
                                    $("#detectionName").append("<option value=''>选择模板</option>");
                                    $.each(data.detectionNameList, function(index,element){
                                        $("#detectionName").append("<option value='"+element+"'>"+element+"</option>");
                                    });
                                    form.render('select');
                                }
                            }, error:function(){
                            }
                        });
                        form.render('select');
                    }
                });

                form.on('submit(searchBeatTemplate)', function(data){
                    var detectionName = $("#detectionName").val();
                    $.ajax({
                        url: "/erp/getdetectiontemplatebydetectionname",
                        type: 'GET',
                        data: {
                            detectionName: detectionName
                        },
                        success: function (res) {
                            if (res.detectionTemplate) {
                                setTimeout(function () {
                                    tinymce.get('orderDetectionTextarea').setContent(res.detectionTemplate.detectionUrl);
                                }, 100);
                            } else {
                                layer.msg("添加失败！", {icon: 2});
                            }
                        },
                        error: function () {
                            layer.msg("添加失败", { icon: 2 });
                        }
                    });

                    return false;
                });
            },100);
        }
    });

    //监听行工具事件
    table.on('tool(dataTable)', function(obj){
        var data = obj.data;
        if (obj.event === 'detail'){
            var pdfName = data.orderName + "-" + data.detectionName + '报告.pdf';
            var index = layer.open({
                type: 1 //Page层类型
                , title: '检测报告'
                , btn: ['下载']
                , shade: 0.6 //遮罩透明度
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , content: "<div id=\"processDetail\" name=\"processDetail\" style=\"background-color: white; color: black;font-size: 20px;\">\n" +
                    "    <div id=\"contentPage\" style=\"background-color: white\">\n" +
                    "        <div class=\"row\" style=\"margin:0\">\n" +
                    "            <div class=\"col-md-12\">\n" +
                    "                <section class=\"panel panel-default\">\n" +
                    "                    <div id=\"detectionDiv\"></div>\n" +
                    "                </section>\n" +
                    "            </div>\n" +
                    "        </div>\n" +
                    "    </div>\n" +
                    "</div>"
                , yes: function () {
                    var newWin=window.open('about:blank', '', '');
                    var html = '<html><head><title>'+pdfName+'</title></head><body><div>'+$("#contentPage").html()+'</div></body>' +
                        '<style>' +
                        'section {border:1px solid #000} .col-md-6,.row{margin: 0 0 20px 0 !important;}' +
                        '</style></html>';
                    newWin.document.write(html);
                    newWin.document.close();
                    newWin.print();

                }
                , cancel : function (i,layero) {
                    $("#detectionDiv").empty();
                }
            });
            layer.full(index);
            setTimeout(function () {
                $("#detectionDiv").empty();
                $.ajax({
                    url: "/erp/getorderdetectionbyid",
                    type: 'GET',
                    data: {
                        id: data.id
                    },
                    success: function (res) {
                        if (res.orderDetection) {
                            if(res.orderDetection.detectionUrl) {
                                $("#detectionDiv").html(res.orderDetection.detectionUrl);
                            } else {
                                $("#detectionDiv").html("<h3>检测报告为空信息未录入</h3>");
                            }
                        } else {
                            layer.msg("添加失败！", {icon: 2});
                        }
                    },
                    error: function () {
                        layer.msg("添加失败", { icon: 2 });
                    }
                });
            },50)
        } else if(obj.event === 'del'){
            layer.confirm('确认删除吗?', function(index){
                $.ajax({
                    url: "/erp/deleteorderdetection",
                    type: 'POST',
                    data: {
                        id:data.id
                    },
                    success: function (res) {
                        if (res.result == 0) {
                            initTable('');
                            layer.close(index);
                            layer.msg("删除成功！", {icon: 1});
                        } else {
                            layer.msg("删除失败！", {icon: 2});
                        }
                    },
                    error: function () {
                        layer.msg("删除失败！", {icon: 2});
                    }
                })

            });
        } else if(obj.event === 'update'){
            var index = layer.open({
                type: 1 //Page层类型
                , title: "修改"
                , btn: ['保存']
                , shade: 0.6 //遮罩透明度
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , content: $("#templateOperate")
                ,yes: function(i, layero){
                    var param = {};
                    param.id = data.id;
                    param.orderName = $("#orderNameTwo").val();
                    param.clothesVersionNumber = $("#clothesVersionNumberTwo").val();
                    param.customerName = $("#customerName").val();
                    param.detectionType = $("#detectionType").val();
                    param.detectionName = $("#detectionName").val();
                    param.sendDate = $("#sendDate").val();
                    var detectionDate = $("#detectionDate").val();
                    if (detectionDate != null && detectionDate != ''){
                        param.detectionDate = detectionDate;
                    }
                    param.detectionDepartment = $("#detectionDepartment").val();
                    param.detectionUrl = tinymce.get('orderDetectionTextarea').getContent();
                    if (param.orderName == null || param.orderName === '' || param.clothesVersionNumber == null || param.clothesVersionNumber === '' || param.customerName == null || param.customerName === '' || param.detectionUrl == null || param.detectionUrl === '' || param.detectionDepartment == null || param.detectionDepartment === '' || param.detectionType == null || param.detectionType === '' || param.detectionName == null || param.detectionName === '' || param.sendDate == null || param.sendDate === ''){
                        layer.msg("请输入完整必要信息~~~");
                        return false;
                    }
                    $.ajax({
                        url: "/erp/updateorderdetection",
                        type: 'POST',
                        data: {
                            orderDetectionJson: JSON.stringify(param)
                        },
                        success: function (res) {
                            if (res.result == 0) {
                                layer.close(index);
                                layer.msg("添加成功！", {icon: 1});
                                initTable('');
                                tinymce.remove("#orderDetectionTextarea");
                                $("#orderNameTwo").val('');
                                $("#clothesVersionNumberTwo").val('');
                                $("#customerName").val('');
                                $("#detectionType").val('');
                                $("#detectionName").val('');
                                $("#sendDate").val('');
                                $("#detectionDate").val('');
                                $("#detectionDepartment").val('');

                            } else {
                                layer.msg("添加失败！", {icon: 2});
                            }
                        },
                        error: function () {
                            layer.msg("添加失败", { icon: 2 });
                        }
                    });
                }
                ,cancel:function () {
                    tinymce.remove("#orderDetectionTextarea");
                    $("#orderNameTwo").val('');
                    $("#clothesVersionNumberTwo").val('');
                    $("#customerName").val('');
                    $("#detectionType").val('');
                    $("#detectionName").val('');
                    $("#sendDate").val('');
                    $("#detectionDate").val('');
                    $("#detectionDepartment").val('');
                }
            });
            layer.full(index);
            form.render('select');
            setTimeout(function () {
                tinymce.init({
                    selector: '#orderDetectionTextarea',
                    language: 'zh_CN',//中文
                    placeholder: '通过类别、名称直接加载出模板,只需填空即可~~',
                    directionality: 'ltr',  //文字方向,ltr文字方向从左到右，rtl从右到左
                    browser_spellcheck: true,
                    contextmenu: false,
                    branding: false, //技术支持(Powered by Tiny || 由Tiny驱动)
                    menubar: false, //菜单栏
                    plugins: [
                        "print image autoresize table powerpaste"
                    ],
                    powerpaste_word_import: 'propmt',// 参数可以是propmt, merge, clear，效果自行切换对比
                    powerpaste_html_import: 'propmt',// propmt, merge, clear
                    powerpaste_allow_local_images: true,
                    paste_data_images: true,
                    relative_urls : false,
                    remove_script_host : false,
                    convert_urls : true,
                    toolbar: "print | undo redo | bold | alignleft aligncenter alignright alignjustify | fontselect | fontsizeselect | image table",
                    images_upload_handler: function(blobInfo, success, failure){
                        handleImgUpload(blobInfo, success, failure)
                    }
                });

                layui.laydate.render({
                    elem: '#sendDate',
                    trigger: 'click'
                });

                layui.laydate.render({
                    elem: '#detectionDate',
                    trigger: 'click'
                });

                layui.use(['yutons_sug'], function () {
                    layui.yutons_sug.render({
                        id: "clothesVersionNumberTwo", //设置容器唯一id
                        height: "300",
                        width: "550",
                        limit:"10",            limits:[10,20,50,100],
                        cols: [
                            [{
                                field: 'clothesVersionNumberTwo',
                                title: '单号',
                                align: 'left'
                            }, {
                                field: 'orderNameTwo',
                                title: '款号',
                                align: 'left'
                            }]
                        ], //设置表头
                        params: [
                            {
                                name: 'clothesVersionNumberTwo',
                                field: 'clothesVersionNumberTwo'
                            }, {
                                name: 'orderNameTwo',
                                field: 'orderNameTwo'
                            }],//设置字段映射，适用于输入一个字段，回显多个字段
                        type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
                        url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
                    });

                    layui.yutons_sug.render({
                        id: "orderNameTwo", //设置容器唯一id
                        height: "300",
                        width: "550",
                        limit:"10",            limits:[10,20,50,100],
                        cols: [
                            [{
                                field: 'clothesVersionNumberTwo',
                                title: '单号',
                                align: 'left'
                            }, {
                                field: 'orderNameTwo',
                                title: '款号',
                                align: 'left'
                            }]
                        ], //设置表头
                        params: [
                            {
                                name: 'clothesVersionNumberTwo',
                                field: 'clothesVersionNumberTwo'
                            }, {
                                name: 'orderNameTwo',
                                field: 'orderNameTwo'
                            }],//设置字段映射，适用于输入一个字段，回显多个字段
                        type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
                        url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
                    });
                });

                $("#orderNameTwo").bind('blur change',function(){
                    var orderName = $("#orderNameTwo").val();
                    $.ajax({
                        url: "/erp/getonlymanufactureorderbyordername",
                        data: {"orderName": orderName},
                        success:function(data){
                            $("#customerName").val('');
                            if (data.manufactureOrder) {
                                $("#customerName").val(data.manufactureOrder.customerName);
                            }
                        },
                        error:function(){
                        }
                    });
                });

                $("#clothesVersionNumberTwo").bind('blur change',function(){
                    var orderName = $("#orderNameTwo").val();
                    $.ajax({
                        url: "/erp/getonlymanufactureorderbyordername",
                        data: {"orderName": orderName},
                        success:function(data){
                            $("#customerName").val('');
                            if (data.manufactureOrder) {
                                $("#customerName").val(data.manufactureOrder.customerName);
                            }
                        },
                        error:function(){
                        }
                    });
                });

                form.on('select(demo)', function(data){
                    var detectionType = $("#detectionType").val();
                    if (detectionType != null && detectionType != ''){
                        $.ajax({
                            url: "/erp/getdetectionnamebydetectiontype",
                            data: {
                                detectionType: detectionType
                            },
                            success:function(data){
                                if (data.detectionNameList){
                                    $("#detectionName").empty();
                                    $("#detectionName").append("<option value=''>选择模板</option>");
                                    $.each(data.detectionNameList, function(index,element){
                                        $("#detectionName").append("<option value='"+element+"'>"+element+"</option>");
                                    });
                                    form.render('select');
                                }
                            }, error:function(){
                            }
                        });
                        form.render('select');
                    }
                });

                form.on('submit(searchBeatTemplate)', function(data){
                    var detectionName = $("#detectionName").val();
                    $.ajax({
                        url: "/erp/getdetectiontemplatebydetectionname",
                        type: 'GET',
                        data: {
                            detectionName: detectionName
                        },
                        success: function (res) {
                            if (res.detectionTemplate) {
                                setTimeout(function () {
                                    tinymce.get('orderDetectionTextarea').setContent(res.detectionTemplate.detectionUrl);
                                }, 100);
                            } else {
                                layer.msg("添加失败！", {icon: 2});
                            }
                        },
                        error: function () {
                            layer.msg("添加失败", { icon: 2 });
                        }
                    });

                    return false;
                });

                $.ajax({
                    url: "/erp/getorderdetectionbyid",
                    type: 'GET',
                    data: {
                        id: data.id
                    },
                    success: function (res) {
                        if (res.orderDetection) {
                            setTimeout(function () {
                                tinymce.get('orderDetectionTextarea').setContent(res.orderDetection.detectionUrl);

                                $("#detectionType").val(res.orderDetection.detectionType);
                                $("#detectionName").append("<option value='"+ res.orderDetection.detectionName +"' selected>"+ res.orderDetection.detectionName +"</option>")
                                // $("#detectionName").val(res.orderDetection.detectionName);
                                $("#orderNameTwo").val(res.orderDetection.orderName);
                                $("#clothesVersionNumberTwo").val(res.orderDetection.clothesVersionNumber);
                                $("#customerName").val(res.orderDetection.customerName);
                                if (res.orderDetection.sendDate != null && res.orderDetection.sendDate != ''){
                                    $("#sendDate").val(moment(res.orderDetection.sendDate).format("YYYY-MM-DD"));
                                }
                                if (res.orderDetection.detectionDate != null && res.orderDetection.detectionDate != ''){
                                    $("#detectionDate").val(moment(res.orderDetection.detectionDate).format("YYYY-MM-DD"));
                                }
                                $("#detectionDepartment").val(res.orderDetection.detectionDepartment);
                                form.render('select');
                            }, 100);
                        } else {
                            layer.msg("添加失败！", {icon: 2});
                        }
                    },
                    error: function () {
                        layer.msg("添加失败", { icon: 2 });
                    }
                });
            },100);
        }
    });
});

function toDecimal(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*100)/100;
    return f;
}

function moveCursor(event,obj) {
    if(event.keyCode==38){
        var name = $(obj).attr("name");
        var index = $("input[name='"+name+"']").index(obj);
        $("input[name='"+name+"']").eq(index-1).focus();
    }else if(event.keyCode==40){
        var name = $(obj).attr("name");
        var index = $("input[name='"+name+"']").index(obj);
        $("input[name='"+name+"']").eq(index+1).focus();
    }
}

function handleImgUpload(blobInfo, success, failure){
    var formdata = new FormData();
    // append 方法中的第一个参数就是 我们要上传文件 在后台接收的文件名
    // 这个值要根据后台来定义
    // 第二个参数是我们上传的文件
    formdata.append('file', blobInfo.blob());
    $.ajax({
        url: "/erp/wangEditorUploadImg",
        type: 'post',
        dataType:'json',
        contentType:false,//ajax上传图片需要添加
        processData:false,//ajax上传图片需要添加
        data: formdata,
        success: function (res) {
            if(res.data) {
                console.log(res)
                success(res.data[0]);
            }else {
                failure("上传图片失败");
            }
        },
        error: function () {
            layer.msg("上传图片失败", { icon: 2 });
        }
    });

}

function processOrderDetail() {
    var pdfName = orderName + '--制单.pdf';
    var index = layer.open({
        type: 1 //Page层类型
        , title: '制单工艺表'
        , btn: ['下载']
        , shade: 0.6 //遮罩透明度
        , maxmin: false //允许全屏最小化
        , anim: 0 //0-6的动画形式，-1不开启
        , content: "<div id=\"processDetail\" name=\"processDetail\" style=\"background-color: white; color: black;font-size: 20px;\">\n" +
            "    <div id=\"contentPage\" style=\"background-color: white\">\n" +
            "        <div class=\"row\" style=\"margin:0\">\n" +
            "            <div class=\"col-md-12\">\n" +
            "                <section class=\"panel panel-default\">\n" +
            "                    <div id=\"detectionDiv\"></div>\n" +
            "                </section>\n" +
            "            </div>\n" +
            "        </div>\n" +
            "    </div>\n" +
            "</div>"
        , yes: function () {
            var newWin=window.open('about:blank', '', '');
            var html = '<html><head><title>'+pdfName+'</title></head><body><div>'+$("#contentPage").html()+'</div></body>' +
                '<style>' +
                'section {border:1px solid #000} .col-md-6,.row{margin: 0 0 20px 0 !important;}' +
                '</style></html>';
            newWin.document.write(html);
            newWin.document.close();
            newWin.print();

        }
        , cancel : function (i,layero) {
            $("#detectionDiv").empty();
        }
    });
    layer.full(index);
    setTimeout(function () {
        $("#detectionDiv").empty();
        $.ajax({
            url: "/erp/getAllProcessDetail",
            type: 'GET',
            data: {
                orderName:orderName
            },
            success: function (res) {
                if(res.manufactureOrder) {
                    var tableHtml = "<table style='width: 100%; font-size: 13px; border: 1px solid black;margin-bottom: 0px;word-wrap:break-word;border-spacing:0 0' class='table'>";
                    tableHtml += "<tr><td style='font-weight: bolder; border: 1px solid black;'>名称:</td><td style='border: 1px solid black;'>"+ res.manufactureOrder.styleName +"</td><td style='font-weight: bolder; border: 1px solid black;'>品牌:</td><td style='border: 1px solid black;'>"+ res.manufactureOrder.customerName +"</td><td style='font-weight: bolder; border: 1px solid black;'>订购方式:</td><td style='border: 1px solid black;'>"+ res.manufactureOrder.purchaseMethod +"</td><td style='font-weight: bolder; border: 1px solid black;'>季度:</td><td style='border: 1px solid black;'>"+ res.manufactureOrder.season +"</td></tr>";
                    tableHtml += "<tr><td style='font-weight: bolder; border: 1px solid black;'>单号:</td><td style='border: 1px solid black;'>"+ res.manufactureOrder.clothesVersionNumber +"</td><td style='font-weight: bolder; border: 1px solid black;'>款号:</td><td style='border: 1px solid black;'>"+ res.manufactureOrder.orderName +"</td><td style='font-weight: bolder; border: 1px solid black;'>分组系列:</td><td style='border: 1px solid black;'>"+ res.manufactureOrder.modelType +"</td><td style='font-weight: bolder; border: 1px solid black;'>跟单:</td><td style='border: 1px solid black;'>"+ res.manufactureOrder.buyer +"</td></tr>";
                    tableHtml += "<tr><td style='font-weight: bolder; border: 1px solid black;'>收单日期:</td><td style='border: 1px solid black;'>"+ moment(res.manufactureOrder.deadLine).format("YYYY-MM-DD") +"</td><td style='font-weight: bolder; border: 1px solid black;'>交货日期:</td><td style='border: 1px solid black;'>"+ moment(res.manufactureOrder.deliveryDate).format("YYYY-MM-DD") +"</td><td style='font-weight: bolder; border: 1px solid black;'>允许加裁:</td><td style='border: 1px solid black;'>"+ res.manufactureOrder.additional +"%</td><td style='font-weight: bolder; border: 1px solid black;'>订单量:</td><td style='border: 1px solid black;'>"+ res.orderCount +"件</td></tr>";
                    tableHtml += "<tr><td style='font-weight: bolder; border: 1px solid black;'>颜色:</td><td colspan='3' style='border: 1px solid black;'>"+ res.manufactureOrder.colorInfo +"</td><td style='font-weight: bolder; border: 1px solid black;'>尺码:</td><td colspan='3' style='border: 1px solid black;'>"+ res.manufactureOrder.sizeInfo +"</td></tr>";
                    tableHtml += "<tr><td style='font-weight: bolder; border: 1px solid black;'>备注:</td><td colspan='7' style='border: 1px solid black;'>"+ res.manufactureOrder.remark +"</td></tr></table>";
                    $("#basicDetailInfo").append(tableHtml);
                }
                $("#styleImgDiv").empty();
                if(res.styleImage) {
                    $("#styleImgDiv").html(res.styleImage.imageText);
                    $("#styleImgDiv img").each(function () {
                        var heightStr = $(this).css("height");
                        var weightStr = $(this).css("width");
                        var hl = heightStr.indexOf('p');
                        var wl = weightStr.indexOf('p');
                        var initHeight = heightStr.substring(0, hl);
                        var initWidth = heightStr.substring(0, wl);
                        $(this).css("height", 290);
                        $(this).css("width", initWidth/initHeight * 290 - 30);
                    })
                } else {
                    $("#styleImgDiv").html("<h3>暂无款式图</h3>");
                }
                var sizeList = [];
                var colorList = [];
                var sizeSum = [];
                var totalWidth = $("#contentPage").width() - 60;
                var colorCount;
                if(res.orderClothesList) {
                    $.each(res.orderClothesList,function (index,item) {
                        if (sizeList.indexOf(item.sizeName) == -1){
                            sizeList.push(item.sizeName);
                        }
                        if (colorList.indexOf(item.colorName) == -1){
                            colorList.push(item.colorName);
                        }
                    });
                    sizeList = globalSizeSort(sizeList);
                    var widthNumber = Number(totalWidth/(sizeList.length + 2));
                    colorCount = colorList.length;
                    var title = [
                        {field: 'colorName', title: '', width: widthNumber, align: 'center',totalRowText: '合计'}
                    ];
                    $.each(sizeList,function (index,item) {
                        title.push({
                            field: item, title: item, width: widthNumber, align: 'center', totalRow: true
                        });
                        sizeSum.push(0);
                    });
                    title.push({
                        field: 'colorSum', title: '合计', width: widthNumber, align: 'center', totalRow: true
                    });
                    var orderClothesData = [];
                    $.each(colorList,function (index2,item2) {
                        var dataLine = {};
                        var colorSum = 0;
                        dataLine.colorName = item2;
                        $.each(sizeList,function (index3,item3) {
                            var flag = true;
                            $.each(res.orderClothesList,function (index1,item1) {
                                if (item1.colorName == item2 && item1.sizeName == item3){
                                    dataLine[item3] = item1.count;
                                    flag = false;
                                    colorSum += item1.count;
                                }
                                if (flag){
                                    dataLine[item3] = 0;
                                }
                            });
                        });
                        dataLine.colorSum = colorSum;
                        orderClothesData.push(dataLine);
                    });
                    var tableHtml = "<table style='width: 100%; font-size: 13px; border: 1px solid black;margin-bottom: 0px;word-wrap:break-word;border-spacing:0 0' class='table'>";
                    tableHtml += "<tr><td style='font-weight: bolder; border: 1px solid black;'>颜色</td>";
                    $.each(sizeList,function (index,item) {
                        tableHtml += "<td style='font-weight: bolder; border: 1px solid black;'>"+item+"</td>";
                    });
                    tableHtml += "<td style='font-weight: bolder; border: 1px solid black;'>合计</td></tr>";
                    $.each(orderClothesData,function (index,item) {
                        tableHtml += "<tr><td style='border: 1px solid black;'>"+item.colorName+"</td>";
                        $.each(sizeList,function(sIndex, sItem) {
                            tableHtml += "<td style='border: 1px solid black;'>"+item[sItem]+"</td>";
                            sizeSum[sIndex] += item[sItem];
                        });
                        tableHtml += "<td style='border: 1px solid black;'>"+item.colorSum+"</td></tr>";
                    });
                    tableHtml += "<tr><td style='border: 1px solid black;'>合计</td>";
                    var totalSum = 0;
                    for (var m = 0; m < sizeSum.length; m ++){
                        tableHtml += "<td style='border: 1px solid black;'>"+ sizeSum[m] +"</td>";
                        totalSum += sizeSum[m];
                    }
                    tableHtml += "<td style='border: 1px solid black;'>" + totalSum +"</td></tr>";
                    $("#orderClothesTable").append(tableHtml);

                }else {
                    $("#orderClothesTable").append("无制单数据");
                }
                if(res.manufactureFabricList) {
                    var data = res.manufactureFabricList;
                    var fabricHtml = "<table style='width: 100%; font-size: 9px; border: 1px solid black;margin-bottom: 0px;word-wrap:break-word;border-spacing:0 0' class='table'>";
                    fabricHtml += "<tr><td style='font-weight: bolder; border: 1px solid black;'>部位</td><td style='font-weight: bolder; border: 1px solid black;width: '20%'>名称</td><td style='font-weight: bolder; border: 1px solid black;'>编号</td><td style='font-weight: bolder; border: 1px solid black;'>布封(cm)</td><td style='font-weight: bolder; border: 1px solid black;'>克重(g)</td><td style='font-weight: bolder; border: 1px solid black;'>单位</td><td style='font-weight: bolder; border: 1px solid black;'>单耗</td><td style='font-weight: bolder; border: 1px solid black;'>单位2</td><td style='font-weight: bolder; border: 1px solid black;'>色组</td><td style='font-weight: bolder; border: 1px solid black;'>面料颜色</td><td style='font-weight: bolder; border: 1px solid black;'>损耗</td><td style='font-weight: bolder; border: 1px solid black;'>订单量</td><td style='font-weight: bolder; border: 1px solid black;'>总用量</td></tr>";
                    for (var i=0; i < data.length; i ++){
                        fabricHtml += "<tr><td style='border: 1px solid black;'>"+data[i].partName+"</td><td style='border: 1px solid black;'>"+data[i].fabricName+"</td><td style='border: 1px solid black;'>"+data[i].fabricNumber+"</td><td style='border: 1px solid black;'>"+data[i].fabricWidth+"</td><td style='border: 1px solid black;'>"+data[i].fabricWeight+"</td><td style='border: 1px solid black;'>"+data[i].unit+"</td><td style='border: 1px solid black;'>"+data[i].pieceUsage+"</td><td style='border: 1px solid black;'>"+ data[i].unitTwo +"</td><td style='border: 1px solid black;'>"+data[i].colorName+"</td><td style='border: 1px solid black;'>"+data[i].fabricColor+"</td><td style='border: 1px solid black;'>"+data[i].fabricLoss+"%</td><td style='border: 1px solid black;'>"+data[i].orderCount+"</td><td style='border: 1px solid black;'>"+ toDecimal(data[i].fabricActCount)+"</td></tr>";
                    }
                    fabricHtml += "</table>";
                    $("#manufactureFabricTable").append(fabricHtml);
                } else {
                    $("#manufactureFabricTable").append("面料未入");
                }
                if(res.manufactureAccessoryList) {
                    // var data = res.manufactureAccessoryList;
                    // var accessoryHtml = "<table style='width: 100%; font-size: 9px; border: 1px solid black;margin-bottom: 0px;word-wrap:break-word;border-spacing:0 0' class='table'>";
                    // accessoryHtml += "<tr><td style='font-weight: bolder; border: 1px solid black;'>名称</td><td style='font-weight: bolder; border: 1px solid black;'>编号</td><td style='font-weight: bolder; border: 1px solid black;'>规格</td><td style='font-weight: bolder; border: 1px solid black;'>颜色</td><td style='font-weight: bolder; border: 1px solid black;'>单位</td><td style='font-weight: bolder; border: 1px solid black;'>单耗</td><td style='font-weight: bolder; border: 1px solid black;'>损耗</td><td style='font-weight: bolder; border: 1px solid black;'>色组</td><td style='font-weight: bolder; border: 1px solid black;'>尺码</td><td style='font-weight: bolder; border: 1px solid black;'>订单量</td><td style='font-weight: bolder; border: 1px solid black;'>总用量</td></tr>";
                    // for (var i=0; i < data.length; i ++){
                    //     accessoryHtml += "<tr><td style='border: 1px solid black;'>"+data[i].accessoryName+"</td><td style='border: 1px solid black;'>"+data[i].accessoryNumber+"</td><td style='border: 1px solid black;'>"+data[i].specification+"</td><td style='border: 1px solid black;'>"+data[i].accessoryColor+"</td><td style='border: 1px solid black;'>"+data[i].accessoryUnit+"</td><td style='border: 1px solid black;'>"+data[i].pieceUsage+"</td><td style='border: 1px solid black;'>"+data[i].accessoryAdd+"%</td><td style='border: 1px solid black;'>"+data[i].colorName+"</td><td style='border: 1px solid black;'>"+data[i].sizeName+"</td><td style='border: 1px solid black;'>"+data[i].orderCount+"</td><td style='border: 1px solid black;'>"+toDecimal(data[i].accessoryCount)+"</td></tr>";
                    // }
                    // accessoryHtml += "</table>";
                    // $("#manufactureAccessoryTable").append(accessoryHtml);

                    var data = res.manufactureAccessoryList;
                    var accessoryHtml = "<table style='width: 100%; font-size: 13px; border: 1px solid black;margin-bottom: 0px;word-wrap:break-word;border-spacing:0 0' class='table'>";
                    var widthNumber = 7 + sizeList.length;
                    if (res.sewAccessoryList){
                        var sewAccessoryList = res.sewAccessoryList;
                        accessoryHtml += "<tr><td style='font-weight: bolder; border: 1px solid black; text-align: center' colspan='"+ widthNumber +"'>车缝辅料</td>";
                        accessoryHtml += "<tr><td style='font-weight: bolder; border: 1px solid black;'>名称</td><td style='font-weight: bolder; border: 1px solid black;'>编号</td><td style='font-weight: bolder; border: 1px solid black;'>规格</td><td style='font-weight: bolder; border: 1px solid black;'>单位</td><td style='font-weight: bolder; border: 1px solid black;'>单耗</td><td style='font-weight: bolder; border: 1px solid black;'>色组</td><td style='font-weight: bolder; border: 1px solid black;'>辅色</td>";
                        $.each(sizeList,function (index,item) {
                            accessoryHtml += "<td style='font-weight: bolder; border: 1px solid black;'>" + item + "</td>";
                        });
                        accessoryHtml += "<td style='font-weight: bolder; border: 1px solid black;'>合计</td></tr>";
                        $.each(sewAccessoryList,function (n_index, n_item) {
                            if (n_item.colorChild === 1 && n_item.sizeChild === 1){
                                $.each(colorList,function (c_index,c_item) {
                                    var line = "<tr><td style='font-weight: bolder; border: 1px solid black;'>"+ n_item.accessoryName + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.accessoryNumber + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.specification + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.accessoryUnit + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + toDecimal(n_item.pieceUsage) + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + c_item + "</td>";
                                    var cpFlag = false;
                                    var colorSum = 0;
                                    $.each(data, function (d_index, d_item) {
                                        if (d_item.accessoryKey == n_item.accessoryKey && d_item.colorName == c_item && !cpFlag){
                                            cpFlag = true;
                                            line += "<td style='font-weight: bolder; border: 1px solid black;'>"+ d_item.colorProperty + "</td>";
                                            $.each(sizeList,function (s_index,s_item) {
                                                var sizeFlag = false;
                                                $.each(data, function (d_index, d_item) {
                                                    if (d_item.accessoryKey == n_item.accessoryKey && d_item.colorName == c_item && d_item.sizeName == s_item){
                                                        sizeFlag = true;
                                                        line += "<td style='font-weight: bolder; border: 1px solid black;'>"+ d_item.sizeProperty + "[" + d_item.accessoryCount + "]</td>";
                                                        colorSum += d_item.accessoryCount;
                                                    }
                                                });
                                                if (!sizeFlag){
                                                    line += "<td style='font-weight: bolder; border: 1px solid black;'></td>";
                                                }
                                            });
                                        }
                                    });
                                    if (cpFlag){
                                        line += "<td style='font-weight: bolder; border: 1px solid black;'>"+ colorSum +"</td></tr>";
                                        accessoryHtml += line;
                                    }
                                });
                            }
                            if (n_item.colorChild === 1 && n_item.sizeChild === 0){
                                $.each(colorList,function (c_index,c_item) {
                                    var line = "<tr><td style='font-weight: bolder; border: 1px solid black;'>"+ n_item.accessoryName + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.accessoryNumber + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.specification + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.accessoryUnit + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + toDecimal(n_item.pieceUsage) + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + c_item + "</td>";
                                    var cpFlag = false;
                                    var colorSum = 0;
                                    $.each(data, function (d_index, d_item) {
                                        if (d_item.accessoryKey == n_item.accessoryKey && d_item.colorName == c_item && !cpFlag){
                                            cpFlag = true;
                                            line += "<td style='font-weight: bolder; border: 1px solid black;'>"+ d_item.colorProperty + "</td>";
                                            colorSum += d_item.accessoryCount;
                                            $.each(sizeList,function (s_index,s_item) {
                                                line += "<td style='font-weight: bolder; border: 1px solid black;'>/</td>";
                                            });
                                        }
                                    });
                                    if (cpFlag){
                                        line += "<td style='font-weight: bolder; border: 1px solid black;'>"+ colorSum +"</td></tr>";
                                        accessoryHtml += line;
                                    }
                                });
                            }
                            if (n_item.colorChild === 0 && n_item.sizeChild === 1){
                                var line = "<tr><td style='font-weight: bolder; border: 1px solid black;'>"+ n_item.accessoryName + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.accessoryNumber + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.specification + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.accessoryUnit + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + toDecimal(n_item.pieceUsage) + "</td><td style='font-weight: bolder; border: 1px solid black;'>通用</td>" + "</td><td style='font-weight: bolder; border: 1px solid black;'>"+ n_item.accessoryColor +"</td>";
                                var colorSum = 0;
                                $.each(sizeList,function (s_index,s_item) {
                                    var sizeFlag = false;
                                    $.each(data, function (d_index, d_item) {
                                        if (d_item.accessoryKey == n_item.accessoryKey  && d_item.sizeName == s_item && !sizeFlag){
                                            sizeFlag = true;
                                            colorSum += d_item.accessoryCount;
                                            line += "<td style='font-weight: bolder; border: 1px solid black;'>"+ d_item.sizeProperty + "[" + d_item.accessoryCount + "]</td>";
                                        }
                                    });
                                    if (!sizeFlag){
                                        line += "<td style='font-weight: bolder; border: 1px solid black;'></td>";
                                    }
                                });
                                line += "<td style='font-weight: bolder; border: 1px solid black;'>"+ colorSum +"</td></tr>";
                                accessoryHtml += line;
                            }
                            if (n_item.colorChild === 0 && n_item.sizeChild === 0){
                                var line = "<tr><td style='font-weight: bolder; border: 1px solid black;'>"+ n_item.accessoryName + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.accessoryNumber + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.specification + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.accessoryUnit + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + toDecimal(n_item.pieceUsage) + "</td><td style='font-weight: bolder; border: 1px solid black;'>通用</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.accessoryColor + "</td>";
                                var colorSum = 0;
                                $.each(sizeList,function (s_index,s_item) {
                                    line += "<td style='font-weight: bolder; border: 1px solid black;'>/</td>";
                                });
                                $.each(data, function (d_index, d_item) {
                                    if (d_item.accessoryKey == n_item.accessoryKey){
                                        colorSum += d_item.accessoryCount;
                                    }
                                });
                                line += "<td style='font-weight: bolder; border: 1px solid black;'>"+ colorSum +"</td></tr>";
                                accessoryHtml += line;
                            }
                            if (n_item.colorChild === 2 && n_item.sizeChild === 0){
                                var line = "<tr><td style='font-weight: bolder; border: 1px solid black;'>"+ n_item.accessoryName + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.accessoryNumber + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.specification + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.accessoryUnit + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + toDecimal(n_item.pieceUsage) + "</td><td style='font-weight: bolder; border: 1px solid black;'>"+ n_item.colorName +"</td><td style='font-weight: bolder; border: 1px solid black;'>"+ n_item.accessoryColor +"</td>";
                                var colorSum = 0;
                                $.each(sizeList,function (s_index,s_item) {
                                    line += "<td style='font-weight: bolder; border: 1px solid black;'>/</td>";
                                });
                                $.each(data, function (d_index, d_item) {
                                    if (d_item.accessoryKey == n_item.accessoryKey){
                                        colorSum += d_item.accessoryCount;
                                    }
                                });
                                line += "<td style='font-weight: bolder; border: 1px solid black;'>"+ colorSum +"</td></tr>";
                                accessoryHtml += line;
                            }
                            if (n_item.colorChild === 2 && n_item.sizeChild === 1){
                                var line = "<tr><td style='font-weight: bolder; border: 1px solid black;'>"+ n_item.accessoryName + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.accessoryNumber + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.specification + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.accessoryUnit + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + toDecimal(n_item.pieceUsage) + "</td><td style='font-weight: bolder; border: 1px solid black;'>"+ n_item.colorName +"</td><td style='font-weight: bolder; border: 1px solid black;'>"+ n_item.accessoryColor +"</td>";
                                var colorSum = 0;
                                $.each(sizeList,function (s_index,s_item) {
                                    var sizeFlag = false;
                                    $.each(data, function (d_index, d_item) {
                                        if (d_item.accessoryKey == n_item.accessoryKey  && d_item.sizeName == s_item && !sizeFlag){
                                            sizeFlag = true;
                                            colorSum += d_item.accessoryCount;
                                            line += "<td style='font-weight: bolder; border: 1px solid black;'>"+ d_item.sizeProperty + "[" + d_item.accessoryCount + "]</td>";
                                        }
                                    });
                                    if (!sizeFlag){
                                        line += "<td style='font-weight: bolder; border: 1px solid black;'></td>";
                                    }
                                });
                                line += "<td style='font-weight: bolder; border: 1px solid black;'>"+ colorSum +"</td></tr>";
                                accessoryHtml += line;
                            }
                            if (n_item.colorChild === 2 && n_item.sizeChild === 2){
                                var line = "<tr><td style='font-weight: bolder; border: 1px solid black;'>"+ n_item.accessoryName + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.accessoryNumber + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.specification + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.accessoryUnit + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + toDecimal(n_item.pieceUsage) + "</td><td style='font-weight: bolder; border: 1px solid black;'>"+ n_item.colorName +"</td><td style='font-weight: bolder; border: 1px solid black;'>"+ n_item.accessoryColor +"</td>";
                                var colorSum = 0;
                                var thisSizeList = n_item.sizeName.split(",");
                                $.each(sizeList,function (s_index,s_item) {
                                    var tsFlag = false;
                                    $.each(thisSizeList,function (ts_index,ts_item) {
                                        if (!tsFlag){
                                            if (s_item == ts_item){
                                                tsFlag = true;
                                                line += "<td style='font-weight: bolder; border: 1px solid black;'>/</td>";
                                            } else {
                                                tsFlag = true;
                                                line += "<td style='font-weight: bolder; border: 1px solid black;'></td>";
                                            }
                                        }
                                    })
                                });
                                $.each(data, function (d_index, d_item) {
                                    if (d_item.accessoryKey == n_item.accessoryKey){
                                        colorSum += d_item.accessoryCount;
                                    }
                                });
                                line += "<td style='font-weight: bolder; border: 1px solid black;'>"+ colorSum +"</td></tr>";
                                accessoryHtml += line;
                            }
                            if (n_item.colorChild === 0 && n_item.sizeChild === 2){
                                var line = "<tr><td style='font-weight: bolder; border: 1px solid black;'>"+ n_item.accessoryName + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.accessoryNumber + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.specification + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.accessoryUnit + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + toDecimal(n_item.pieceUsage) + "</td><td style='font-weight: bolder; border: 1px solid black;'>通用</td><td style='font-weight: bolder; border: 1px solid black;'>"+ n_item.accessoryColor +"</td>";
                                var thisSizeList = n_item.sizeName.split(",");
                                var sizeDeal = {};
                                $.each(sizeList,function (s_index,s_item) {
                                    sizeDeal[s_item] = '';
                                });
                                $.each(thisSizeList,function (s_index,s_item) {
                                    sizeDeal[s_item] = '/';
                                });
                                $.each(sizeList,function (s_index,s_item) {
                                    line += "<td style='font-weight: bolder; border: 1px solid black;'>"+ sizeDeal[s_item] +"</td>";
                                });
                                var colorSum = 0;
                                $.each(data, function (d_index, d_item) {
                                    if (d_item.accessoryKey == n_item.accessoryKey){
                                        colorSum += d_item.accessoryCount;
                                    }
                                });
                                line += "<td style='font-weight: bolder; border: 1px solid black;'>"+ colorSum +"</td></tr>";
                                accessoryHtml += line;
                            }
                            if (n_item.colorChild === 1 && n_item.sizeChild === 2){
                                $.each(colorList,function (c_index,c_item) {
                                    var line = "<tr><td style='font-weight: bolder; border: 1px solid black;'>"+ n_item.accessoryName + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.accessoryNumber + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.specification + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.accessoryUnit + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + toDecimal(n_item.pieceUsage) + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + c_item + "</td>";
                                    var cpFlag = false;
                                    var colorSum = 0;
                                    $.each(data, function (d_index, d_item) {
                                        if (d_item.accessoryKey == n_item.accessoryKey && d_item.colorName == c_item && !cpFlag){
                                            cpFlag = true;
                                            colorSum += d_item.accessoryCount;
                                            line += "<td style='font-weight: bolder; border: 1px solid black;'>"+ d_item.colorProperty + "</td>";
                                            var thisSizeList = n_item.sizeName.split(",");
                                            var sizeDeal = {};
                                            $.each(sizeList,function (s_index,s_item) {
                                                sizeDeal[s_item] = '';
                                            });
                                            $.each(thisSizeList,function (s_index,s_item) {
                                                sizeDeal[s_item] = '/';
                                            });
                                            $.each(sizeList,function (s_index,s_item) {
                                                line += "<td style='font-weight: bolder; border: 1px solid black;'>"+ sizeDeal[s_item] +"</td>";
                                            });
                                        }
                                    });
                                    if (cpFlag){
                                        line += "<td style='font-weight: bolder; border: 1px solid black;'>"+ colorSum +"</td></tr>";
                                        accessoryHtml += line;
                                    }
                                });
                            }
                        });
                    }
                    if (res.packAccessoryList){
                        var packAccessoryList = res.packAccessoryList;
                        accessoryHtml += "<tr><td style='font-weight: bolder; border: 1px solid black; text-align: center' colspan='"+ widthNumber +"'>包装辅料</td>";
                        accessoryHtml += "<tr><td style='font-weight: bolder; border: 1px solid black;'>名称</td><td style='font-weight: bolder; border: 1px solid black;'>编号</td><td style='font-weight: bolder; border: 1px solid black;'>规格</td><td style='font-weight: bolder; border: 1px solid black;'>单位</td><td style='font-weight: bolder; border: 1px solid black;'>单耗</td><td style='font-weight: bolder; border: 1px solid black;'>色组</td><td style='font-weight: bolder; border: 1px solid black;'>辅色</td>";
                        $.each(sizeList,function (index,item) {
                            accessoryHtml += "<td style='font-weight: bolder; border: 1px solid black;'>" + item + "</td>";
                        });
                        accessoryHtml += "<td style='font-weight: bolder; border: 1px solid black;'>合计</td></tr>";
                        $.each(packAccessoryList,function (n_index, n_item) {
                            if (n_item.colorChild === 1 && n_item.sizeChild === 1){
                                $.each(colorList,function (c_index,c_item) {
                                    var line = "<tr><td style='font-weight: bolder; border: 1px solid black;'>"+ n_item.accessoryName + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.accessoryNumber + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.specification + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.accessoryUnit + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + toDecimal(n_item.pieceUsage) + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + c_item + "</td>";
                                    var cpFlag = false;
                                    var colorSum = 0;
                                    $.each(data, function (d_index, d_item) {
                                        if (d_item.accessoryKey == n_item.accessoryKey && d_item.colorName == c_item && !cpFlag){
                                            cpFlag = true;
                                            line += "<td style='font-weight: bolder; border: 1px solid black;'>"+ d_item.colorProperty + "</td>";
                                            $.each(sizeList,function (s_index,s_item) {
                                                var sizeFlag = false;
                                                $.each(data, function (d_index, d_item) {
                                                    if (d_item.accessoryKey == n_item.accessoryKey && d_item.colorName == c_item && d_item.sizeName == s_item){
                                                        sizeFlag = true;
                                                        line += "<td style='font-weight: bolder; border: 1px solid black;'>"+ d_item.sizeProperty + "[" + d_item.accessoryCount + "]</td>";
                                                        colorSum += d_item.accessoryCount;
                                                    }
                                                });
                                                if (!sizeFlag){
                                                    line += "<td style='font-weight: bolder; border: 1px solid black;'></td>";
                                                }
                                            });
                                        }
                                    });
                                    if (cpFlag){
                                        line += "<td style='font-weight: bolder; border: 1px solid black;'>"+ colorSum +"</td></tr>";
                                        accessoryHtml += line;
                                    }
                                });
                            }
                            if (n_item.colorChild === 1 && n_item.sizeChild === 0){
                                $.each(colorList,function (c_index,c_item) {
                                    var line = "<tr><td style='font-weight: bolder; border: 1px solid black;'>"+ n_item.accessoryName + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.accessoryNumber + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.specification + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.accessoryUnit + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + toDecimal(n_item.pieceUsage) + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + c_item + "</td>";
                                    var cpFlag = false;
                                    var colorSum = 0;
                                    $.each(data, function (d_index, d_item) {
                                        if (d_item.accessoryKey == n_item.accessoryKey && d_item.colorName == c_item && !cpFlag){
                                            cpFlag = true;
                                            line += "<td style='font-weight: bolder; border: 1px solid black;'>"+ d_item.colorProperty + "</td>";
                                            colorSum += d_item.accessoryCount;
                                            $.each(sizeList,function (s_index,s_item) {
                                                line += "<td style='font-weight: bolder; border: 1px solid black;'>/</td>";
                                            });
                                        }
                                    });
                                    if (cpFlag){
                                        line += "<td style='font-weight: bolder; border: 1px solid black;'>"+ colorSum +"</td></tr>";
                                        accessoryHtml += line;
                                    }
                                });
                            }
                            if (n_item.colorChild === 0 && n_item.sizeChild === 1){
                                var line = "<tr><td style='font-weight: bolder; border: 1px solid black;'>"+ n_item.accessoryName + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.accessoryNumber + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.specification + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.accessoryUnit + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + toDecimal(n_item.pieceUsage) + "</td><td style='font-weight: bolder; border: 1px solid black;'>通用</td>" + "</td><td style='font-weight: bolder; border: 1px solid black;'>"+ n_item.accessoryColor +"</td>";
                                var colorSum = 0;
                                $.each(sizeList,function (s_index,s_item) {
                                    var sizeFlag = false;
                                    $.each(data, function (d_index, d_item) {
                                        if (d_item.accessoryKey == n_item.accessoryKey  && d_item.sizeName == s_item && !sizeFlag){
                                            sizeFlag = true;
                                            colorSum += d_item.accessoryCount;
                                            line += "<td style='font-weight: bolder; border: 1px solid black;'>"+ d_item.sizeProperty + "[" + d_item.accessoryCount + "]</td>";
                                        }
                                    });
                                    if (!sizeFlag){
                                        line += "<td style='font-weight: bolder; border: 1px solid black;'></td>";
                                    }
                                });
                                line += "<td style='font-weight: bolder; border: 1px solid black;'>"+ colorSum +"</td></tr>";
                                accessoryHtml += line;
                            }
                            if (n_item.colorChild === 0 && n_item.sizeChild === 0){
                                var line = "<tr><td style='font-weight: bolder; border: 1px solid black;'>"+ n_item.accessoryName + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.accessoryNumber + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.specification + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.accessoryUnit + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + toDecimal(n_item.pieceUsage) + "</td><td style='font-weight: bolder; border: 1px solid black;'>通用</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.accessoryColor + "</td>";
                                var colorSum = 0;
                                $.each(sizeList,function (s_index,s_item) {
                                    line += "<td style='font-weight: bolder; border: 1px solid black;'>/</td>";
                                });
                                $.each(data, function (d_index, d_item) {
                                    if (d_item.accessoryKey == n_item.accessoryKey){
                                        colorSum += d_item.accessoryCount;
                                    }
                                });
                                line += "<td style='font-weight: bolder; border: 1px solid black;'>"+ colorSum +"</td></tr>";
                                accessoryHtml += line;
                            }
                            if (n_item.colorChild === 2 && n_item.sizeChild === 0){
                                var line = "<tr><td style='font-weight: bolder; border: 1px solid black;'>"+ n_item.accessoryName + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.accessoryNumber + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.specification + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.accessoryUnit + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + toDecimal(n_item.pieceUsage) + "</td><td style='font-weight: bolder; border: 1px solid black;'>"+ n_item.colorName +"</td><td style='font-weight: bolder; border: 1px solid black;'>"+ n_item.accessoryColor +"</td>";
                                var colorSum = 0;
                                $.each(sizeList,function (s_index,s_item) {
                                    line += "<td style='font-weight: bolder; border: 1px solid black;'>/</td>";
                                });
                                $.each(data, function (d_index, d_item) {
                                    if (d_item.accessoryKey == n_item.accessoryKey){
                                        colorSum += d_item.accessoryCount;
                                    }
                                });
                                line += "<td style='font-weight: bolder; border: 1px solid black;'>"+ colorSum +"</td></tr>";
                                accessoryHtml += line;
                            }
                            if (n_item.colorChild === 2 && n_item.sizeChild === 1){
                                var line = "<tr><td style='font-weight: bolder; border: 1px solid black;'>"+ n_item.accessoryName + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.accessoryNumber + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.specification + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.accessoryUnit + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + toDecimal(n_item.pieceUsage) + "</td><td style='font-weight: bolder; border: 1px solid black;'>"+ n_item.colorName +"</td><td style='font-weight: bolder; border: 1px solid black;'>"+ n_item.accessoryColor +"</td>";
                                var colorSum = 0;
                                $.each(sizeList,function (s_index,s_item) {
                                    var sizeFlag = false;
                                    $.each(data, function (d_index, d_item) {
                                        if (d_item.accessoryKey == n_item.accessoryKey  && d_item.sizeName == s_item && !sizeFlag){
                                            sizeFlag = true;
                                            colorSum += d_item.accessoryCount;
                                            line += "<td style='font-weight: bolder; border: 1px solid black;'>"+ d_item.sizeProperty + "[" + d_item.accessoryCount + "]</td>";
                                        }
                                    });
                                    if (!sizeFlag){
                                        line += "<td style='font-weight: bolder; border: 1px solid black;'></td>";
                                    }
                                });
                                line += "<td style='font-weight: bolder; border: 1px solid black;'>"+ colorSum +"</td></tr>";
                                accessoryHtml += line;
                            }
                            if (n_item.colorChild === 2 && n_item.sizeChild === 2){
                                var line = "<tr><td style='font-weight: bolder; border: 1px solid black;'>"+ n_item.accessoryName + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.accessoryNumber + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.specification + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.accessoryUnit + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + toDecimal(n_item.pieceUsage) + "</td><td style='font-weight: bolder; border: 1px solid black;'>"+ n_item.colorName +"</td><td style='font-weight: bolder; border: 1px solid black;'>"+ n_item.accessoryColor +"</td>";
                                var colorSum = 0;
                                var thisSizeList = n_item.sizeName.split(",");
                                $.each(sizeList,function (s_index,s_item) {
                                    var tsFlag = false;
                                    $.each(thisSizeList,function (ts_index,ts_item) {
                                        if (!tsFlag){
                                            if (s_item == ts_item){
                                                tsFlag = true;
                                                line += "<td style='font-weight: bolder; border: 1px solid black;'>/</td>";
                                            } else {
                                                tsFlag = true;
                                                line += "<td style='font-weight: bolder; border: 1px solid black;'></td>";
                                            }
                                        }
                                    })
                                });
                                $.each(data, function (d_index, d_item) {
                                    if (d_item.accessoryKey == n_item.accessoryKey){
                                        colorSum += d_item.accessoryCount;
                                    }
                                });
                                line += "<td style='font-weight: bolder; border: 1px solid black;'>"+ colorSum +"</td></tr>";
                                accessoryHtml += line;
                            }
                            if (n_item.colorChild === 0 && n_item.sizeChild === 2){
                                var line = "<tr><td style='font-weight: bolder; border: 1px solid black;'>"+ n_item.accessoryName + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.accessoryNumber + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.specification + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.accessoryUnit + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + toDecimal(n_item.pieceUsage) + "</td><td style='font-weight: bolder; border: 1px solid black;'>通用</td><td style='font-weight: bolder; border: 1px solid black;'>"+ n_item.accessoryColor +"</td>";
                                var thisSizeList = n_item.sizeName.split(",");
                                var sizeDeal = {};
                                $.each(sizeList,function (s_index,s_item) {
                                    sizeDeal[s_item] = '';
                                });
                                $.each(thisSizeList,function (s_index,s_item) {
                                    sizeDeal[s_item] = '/';
                                });
                                $.each(sizeList,function (s_index,s_item) {
                                    line += "<td style='font-weight: bolder; border: 1px solid black;'>"+ sizeDeal[s_item] +"</td>";
                                });
                                var colorSum = 0;
                                $.each(data, function (d_index, d_item) {
                                    if (d_item.accessoryKey == n_item.accessoryKey){
                                        colorSum += d_item.accessoryCount;
                                    }
                                });
                                line += "<td style='font-weight: bolder; border: 1px solid black;'>"+ colorSum +"</td></tr>";
                                accessoryHtml += line;
                            }
                            if (n_item.colorChild === 1 && n_item.sizeChild === 2){
                                $.each(colorList,function (c_index,c_item) {
                                    var line = "<tr><td style='font-weight: bolder; border: 1px solid black;'>"+ n_item.accessoryName + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.accessoryNumber + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.specification + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.accessoryUnit + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + toDecimal(n_item.pieceUsage) + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + c_item + "</td>";
                                    var cpFlag = false;
                                    var colorSum = 0;
                                    $.each(data, function (d_index, d_item) {
                                        if (d_item.accessoryKey == n_item.accessoryKey && d_item.colorName == c_item && !cpFlag){
                                            cpFlag = true;
                                            colorSum += d_item.accessoryCount;
                                            line += "<td style='font-weight: bolder; border: 1px solid black;'>"+ d_item.colorProperty + "</td>";
                                            var thisSizeList = n_item.sizeName.split(",");
                                            var sizeDeal = {};
                                            $.each(sizeList,function (s_index,s_item) {
                                                sizeDeal[s_item] = '';
                                            });
                                            $.each(thisSizeList,function (s_index,s_item) {
                                                sizeDeal[s_item] = '/';
                                            });
                                            $.each(sizeList,function (s_index,s_item) {
                                                line += "<td style='font-weight: bolder; border: 1px solid black;'>"+ sizeDeal[s_item] +"</td>";
                                            });
                                        }
                                    });
                                    if (cpFlag){
                                        line += "<td style='font-weight: bolder; border: 1px solid black;'>"+ colorSum +"</td></tr>";
                                        accessoryHtml += line;
                                    }
                                });
                            }
                        });
                    }
                    accessoryHtml += "</table>";
                    $("#manufactureAccessoryTable").append(accessoryHtml);
                }else {
                    $("#manufactureAccessoryTable").append("辅料未入");
                }
                $("#measureProcess").empty();
                if(res.orderMeasure) {
                    $("#measureProcess").html(res.orderMeasure.orderMeasureImg);
                } else {
                    $("#measureProcess").html("<h3>尺寸信息未录入</h3>");
                }
                $("#processDiv").empty();
                if(res.processRequirement) {
                    $("#processDiv").html(res.processRequirement.requirement);
                } else {
                    $("#processDiv").html("<h3>工艺信息未录入</h3>");
                }
            },
            error: function () {
                layer.msg("获取详情信息失败！", {icon: 2});
                layer.close(index);
            }
        })
    },50)
}
