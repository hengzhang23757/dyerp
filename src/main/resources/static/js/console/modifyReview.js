var form;
var userName = $("#userName").val();
var userRole = $("#userRole").val();
layui.use('form', function(){
    form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
    form.render();
});
layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.21'
}).extend({                         // 模块别名
    soulTable: 'soulTable',
});
var dataTable;


layui.use(['form', 'soulTable', 'table'], function () {
    var table = layui.table,
        soulTable = layui.soulTable,
        $ = layui.$;

    dataTable = table.render({
        id:'dataTable'
        ,elem: '#dataTable'
        ,cols:[[]]
        ,data:[]
        ,loading:false
        ,height: 'full-100'
        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
        ,page: true
        ,title: '修改审核'
        ,totalRow: true
        ,even: true
        ,overflow: 'tips'
        ,limit:50
        ,limits:[50, 100, 200]
        ,done: function (res, curr, count) {
            soulTable.render(this);
        }
    });

    initTable();

    function initTable(){
        var load = layer.load();
        $.ajax({
            url: "/erp/getmanufactureordercountreviewdata",
            type: 'GET',
            data: {},
            success: function (res) {
                if (res.data) {
                    var reportData = res.data;
                    dataTable = table.render({
                        elem: '#dataTable'
                        ,cols:[[
                            {type:'numbers', align:'center', title:'序号', minWidth:60}
                            ,{field: 'remark', title: '类别',align:'center', minWidth: 90, sort: true, filter: true}
                            ,{field: 'clothesVersionNumber', title: '单号',align:'center', minWidth: 120, sort: true, filter: true}
                            ,{field: 'orderName', title: '款号',align:'center', minWidth: 120, sort: true, filter: true}
                            ,{field: 'buyer', title: '跟单',align:'center', minWidth: 100, sort: true, filter: true}
                            ,{field: 'manufactureOrderID', title:'操作', align:'center', toolbar: '#barTopReview', minWidth:240, fixed: 'right'}
                        ]]
                        ,loading:true
                        ,data:reportData
                        ,height: 'full-100'
                        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                        ,title: '修改审核'
                        ,totalRow: true
                        ,even: true
                        ,page: true
                        ,overflow: 'tips'
                        ,limit:50
                        ,limits:[50, 100, 200]
                        ,done: function (res, curr, count) {
                            soulTable.render(this);
                            layer.close(load);
                        }
                    });
                }
            }
        })
    }

    $.ajax({
        url: "/erp/getmanufactureordercountreviewdatacount",
        type: 'GET',
        data: {},
        success: function (res) {
            if (res.count != null) {
                $("#orderClothesModify").text(res.count);
            } else {
                layer.msg("获取失败！", {icon: 2});
            }
        }, error: function () {
            layer.msg("获取失败！", {icon: 2});
        }
    });

    table.on('tool(dataTable)', function(obj){
        var data = obj.data;
        if(obj.event === 'pass'){
            if (userRole != "role10" && userRole != "root"){
                layer.msg("您没有权限！", {icon: 2});
                return false;
            } else {
                $.ajax({
                    url: "/erp/updatemanufactureordermodifystate",
                    type: 'POST',
                    data: {
                        orderName:data.orderName,
                        modifyState:"开放"
                    },
                    success: function (res) {
                        if (res.result == 0) {
                            initTable();
                            layer.close(index);
                            layer.msg("提交成功！", {icon: 1});
                        } else {
                            layer.msg("提交失败！", {icon: 2});
                        }
                    },
                    error: function () {
                        layer.msg("提交失败！", {icon: 2});
                    }
                })
            }
        }
    });
});


function toDecimal(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*100)/100;
    return f;
}


function toDecimalFour(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*10000)/10000;
    return f;
}

function toDecimalSix(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*1000000)/1000000;
    return f;
}

function dateFormat(fmt, date) {
    var ret;
    var opt = {
        "Y+": date.getFullYear().toString(),        // 年
        "m+": (date.getMonth() + 1).toString(),     // 月
        "d+": date.getDate().toString(),            // 日
        "H+": date.getHours().toString(),           // 时
        "M+": date.getMinutes().toString(),         // 分
        "S+": date.getSeconds().toString()          // 秒
        // 有其他格式化字符需求可以继续添加，必须转化成字符串
    };
    for (var k in opt) {
        ret = new RegExp("(" + k + ")").exec(fmt);
        if (ret) {
            fmt = fmt.replace(ret[1], (ret[1].length == 1) ? (opt[k]) : (opt[k].padStart(ret[1].length, "0")))
        }
    }
    return fmt;
}

function getFormatDate() {
    var date = new Date();
    var month = date.getMonth() + 1;
    var strDate = date.getDate();
    if (month >= 1 && month <= 9) {
        month = "0" + month;
    }
    if (strDate >= 0 && strDate <= 9) {
        strDate = "0" + strDate;
    }
    var currentDate = date.getFullYear() + "-" + month + "-" + strDate;
    return currentDate;
}


function reloadInfo() {
    $.ajax({
        url: "/erp/getchecknumbercount",
        type: 'GET',
        data: {},
        success: function (res) {
            if (res.checkNumberCount != null) {
                $("#checkNumberCount").text(res.checkNumberCount);
            } else {
                layer.msg("获取失败！", {icon: 2});
            }
        }, error: function () {
            layer.msg("获取失败！", {icon: 2});
        }
    });
}