var form;
var userName = $("#userName").val();
var userRole = $("#userRole").val();
layui.use('form', function(){
    form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
    form.render();
});
layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.21'
}).extend({                         // 模块别名
    soulTable: 'soulTable',
});
var dataTable;


layui.use(['form', 'soulTable', 'table'], function () {
    var table = layui.table,
        soulTable = layui.soulTable,
        $ = layui.$;

    dataTable = table.render({
        id:'dataTable'
        ,elem: '#dataTable'
        ,cols:[[]]
        ,data:[]
        ,loading:false
        ,height: 'full-100'
        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
        ,page: true
        ,title: '超数审核'
        ,totalRow: true
        ,even: true
        ,overflow: 'tips'
        ,limit:50
        ,limits:[50, 100, 200]
        ,done: function (res, curr, count) {
            soulTable.render(this);
        }
    });

    initTable();

    function initTable(){
        var load = layer.load();
        $.ajax({
            url: "/erp/getfabricdetailreviewdata",
            type: 'GET',
            data: {},
            success: function (res) {
                if (res.data) {
                    var reportData = res.data;
                    dataTable = table.render({
                        elem: '#dataTable'
                        ,cols:[[
                            {type:'numbers', align:'center', title:'序号', minWidth:60}
                            ,{field: 'reviewType', title: '类别',align:'center', minWidth: 90, sort: true, filter: true}
                            ,{field: 'clothesVersionNumber', title: '单号',align:'center', minWidth: 120, sort: true, filter: true}
                            ,{field: 'orderName', title: '款号',align:'center', minWidth: 120, sort: true, filter: true}
                            ,{field: 'supplier', title: '供应商',align:'center', minWidth: 100, sort: true, filter: true}
                            ,{field: 'fabricName', title: '面料名称',align:'center', minWidth: 150, sort: true, filter: true}
                            ,{field: 'colorName', title: '色组',align:'center', minWidth: 100, sort: true, filter: true}
                            ,{field: 'fabricColor', title: '面料颜色',align:'center', minWidth: 100, sort: true, filter: true}
                            ,{field: 'jarName', title: '缸号',align:'center', minWidth: 120, sort: true, filter: true}
                            ,{field: 'unit', title: '单位',align:'center', minWidth: 120, sort: true, filter: true}
                            ,{field: 'fabricActCount', title: '订购量',align:'center', minWidth: 120, sort: true, filter: true, fixed: 'right'}
                            ,{field: 'finishWeight', title: '已回',align:'center', minWidth: 120, sort: true, filter: true, fixed: 'right'}
                            ,{field: 'weight', title: '本次',align:'center', minWidth: 120, sort: true, filter: true, fixed: 'right'}
                            ,{field: 'price', title: '单价',align:'center', minWidth: 100, sort: true, filter: true, fixed: 'right'}
                            ,{field: 'fabricDetailID', title:'操作', align:'center', toolbar: '#barTopReview', minWidth:240, fixed: 'right'}
                        ]]
                        ,loading:true
                        ,data:reportData
                        ,height: 'full-100'
                        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                        ,title: '超数审核表'
                        ,totalRow: true
                        ,even: true
                        ,page: true
                        ,overflow: 'tips'
                        ,limit:50
                        ,limits:[50, 100, 200]
                        ,done: function (res, curr, count) {
                            soulTable.render(this);
                            layer.close(load);
                        }
                    });
                }
            }
        })
    }

    $.ajax({
        url: "/erp/getfabricdetailreviewdatacount",
        type: 'GET',
        data: {},
        success: function (res) {
            if (res.count != null) {
                $("#fabricAccessoryOverReviewCount").text(res.count);
            } else {
                layer.msg("获取失败！", {icon: 2});
            }
        }, error: function () {
            layer.msg("获取失败！", {icon: 2});
        }
    });

    table.on('tool(dataTable)', function(obj){
        var data = obj.data;
        if(obj.event === 'inStore'){
            if (userRole != "role10" && userRole != "root"){
                layer.msg("您没有权限！", {icon: 2});
                return false;
            } else {
                layer.confirm('确认直接入库吗?', function(index){
                    $.ajax({
                        url: "/erp/changefabricdetailoperatetype",
                        type: 'POST',
                        data: {
                            fabricDetailID:data.fabricDetailID,
                            operateType:"入库"
                        },
                        success: function (res) {
                            if (res.result == 0) {
                                initTable();
                                layer.close(index);
                                layer.msg("提价成功！", {icon: 1});
                            } else {
                                layer.msg("提交失败！", {icon: 2});
                            }
                        },
                        error: function () {
                            layer.msg("提交失败！", {icon: 2});
                        }
                    })

                });
            }
        } else if(obj.event === 'return'){
            if (userRole != "role10" && userRole != "root"){
                layer.msg("您没有权限！", {icon: 2});
                return false;
            } else {

            }
        } else if(obj.event === 'discount'){
            if (userRole != "role10" && userRole != "root"){
                layer.msg("您没有权限！", {icon: 2});
                return false;
            } else {
                var index = layer.open({
                    type: 1 //Page层类型
                    , title: '折扣确认'
                    , btn: ['保存']
                    , shade: 0.6 //遮罩透明度
                    , maxmin: false //允许全屏最小化
                    , anim: 0 //0-6的动画形式，-1不开启
                    , area: ['300px', '180px']
                    , content: "<div><table><tr><td><label class='layui-form-label'>折扣</label></td><td><input type='text' id='discountVal' autocomplete='off' class='layui-input'></td></tr></table></div>"
                    ,yes: function(index, layero){
                        var discount = $("#discountVal").val();
                        if (discount == null || discount == ''){
                            layer.msg("折扣不能为空哦~~~~", {icon: 2});
                            return false;
                        }
                        if (Number(discount) > 1){
                            layer.msg("折扣要小于1哦~~~", {icon: 2});
                            return false;
                        }
                        $.ajax({
                            url: "/erp/changefabricdiscountbyid",
                            type: 'POST',
                            data: {
                                fabricDetailID:data.fabricDetailID,
                                discount:discount
                            },
                            success: function (res) {
                                if (res.result == 0) {
                                    layer.close(index);
                                    layer.msg("提交成功！", {icon: 1});
                                    initTable();
                                    $("#discountVal").val("");
                                } else {
                                    layer.msg("提交失败！", {icon: 2});
                                }
                            },
                            error: function () {
                                layer.msg("提交失败！", {icon: 2});
                            }
                        })
                    }
                    , cancel : function (i,layero) {
                        $("#discountVal").val("");
                    }
                });
            }
        }
    });
});


function toDecimal(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*100)/100;
    return f;
}


function toDecimalFour(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*10000)/10000;
    return f;
}

function toDecimalSix(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*1000000)/1000000;
    return f;
}

function dateFormat(fmt, date) {
    var ret;
    var opt = {
        "Y+": date.getFullYear().toString(),        // 年
        "m+": (date.getMonth() + 1).toString(),     // 月
        "d+": date.getDate().toString(),            // 日
        "H+": date.getHours().toString(),           // 时
        "M+": date.getMinutes().toString(),         // 分
        "S+": date.getSeconds().toString()          // 秒
        // 有其他格式化字符需求可以继续添加，必须转化成字符串
    };
    for (var k in opt) {
        ret = new RegExp("(" + k + ")").exec(fmt);
        if (ret) {
            fmt = fmt.replace(ret[1], (ret[1].length == 1) ? (opt[k]) : (opt[k].padStart(ret[1].length, "0")))
        }
    }
    return fmt;
}

function getFormatDate() {
    var date = new Date();
    var month = date.getMonth() + 1;
    var strDate = date.getDate();
    if (month >= 1 && month <= 9) {
        month = "0" + month;
    }
    if (strDate >= 0 && strDate <= 9) {
        strDate = "0" + strDate;
    }
    var currentDate = date.getFullYear() + "-" + month + "-" + strDate;
    return currentDate;
}


function reloadInfo() {
    $.ajax({
        url: "/erp/getchecknumbercount",
        type: 'GET',
        data: {},
        success: function (res) {
            if (res.checkNumberCount != null) {
                $("#checkNumberCount").text(res.checkNumberCount);
            } else {
                layer.msg("获取失败！", {icon: 2});
            }
        }, error: function () {
            layer.msg("获取失败！", {icon: 2});
        }
    });
}