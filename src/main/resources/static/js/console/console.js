// var basePath=$("#basePath").val();
layui.config({
    base: '/layuiadmin/' //静态资源所在路径
}).extend({
    index: 'lib/index'
}).use(['index', 'console']);
var userRole = $("#userRole").val();
// layui.config({
//     base: '/js/ext/',   // 模块目录
//     version: 'v1.5.4'
// }).extend({                         // 模块别名
//     soulTable: 'soulTable',
// });
var weekLooseTable,weekCutTable;
$(document).ready(function () {

    layui.use(['form','soulTable', 'table', 'element'], function () {
        var table = layui.table,
            soulTable = layui.soulTable,
            element = layui.element,
            $ = layui.$;
        var load = layer.load();
        table.init('weekLooseTable',{//转换成静态表格
            cols:[[
                {field:'orderName', title:'款号', align:'center', width:'25%'}
                ,{field:'colorName', title:'颜色', align:'center', width:'15%'}
                ,{field:'orderCount', title:'订单数', align:'center', width:'15%'}
                ,{field:'cutCount', title:'已裁', align:'center', width:'15%'}
                ,{field:'storageBatch', title:'库存(卷)', align:'center', width:'15%'}
                ,{field:'preCutBatch', title:'待裁(卷)', align:'center', width:'15%'}
            ]]
            ,data: []
            ,title: '用户数据表'
            ,height: '440'
            ,even: true
            ,page: false
            ,overflow: 'tips'
            ,done: function (res, curr, count) {
                soulTable.render(this);
                layer.close(load);
                element.render();
            }
            ,filter: {
                bottom: true,
                items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
            }
        });
        // weekLooseTable = table.render({
        //     elem: '#weekLooseTable'
        //     ,url:'getunfinishedcutlooseplan'
        //     ,cols: [[]]
        //     ,loading:true
        //     ,done: function (res, curr, count) {
        //         table.init('weekLooseTable',{//转换成静态表格
        //             cols:[[
        //                 {field:'orderName', title:'款号', align:'center', width:'25%'}
        //                 ,{field:'colorName', title:'颜色', align:'center', width:'15%'}
        //                 ,{field:'orderCount', title:'订单数', align:'center', width:'15%'}
        //                 ,{field:'cutCount', title:'已裁', align:'center', width:'15%'}
        //                 ,{field:'storageBatch', title:'库存(卷)', align:'center', width:'15%'}
        //                 ,{field:'preCutBatch', title:'待裁(卷)', align:'center', width:'15%'}
        //             ]]
        //             ,data: res.data
        //             ,title: '用户数据表'
        //             ,height: '440'
        //             ,even: true
        //             ,page: false
        //             ,overflow: 'tips'
        //             ,done: function (res, curr, count) {
        //                 soulTable.render(this);
        //                 layer.close(load);
        //                 element.render();
        //             }
        //             ,filter: {
        //                 bottom: true,
        //                 items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
        //             }
        //         });
        //     }
        // });
    });


    //面辅料审核
    $.ajax({
        url: "/erp/getchecknumbercount",
        type: 'GET',
        data: {},
        success: function (res) {
            if (res.checkNumberCount != null) {
                $("#fabricAccessoryReviewCount").text(res.checkNumberCount);
            } else {
                layer.msg("获取失败！", {icon: 2});
            }
        }, error: function () {
            layer.msg("获取失败！", {icon: 2});
        }
    });
    //待阅消息
    $.ajax({
        url: "/erp/getunfinishmessagetotalcount",
        type: 'GET',
        data: {},
        success: function (data) {
            if(data.messageCount) {
                $("#pendingMessageCount").text(data.messageCount);
            }
        }, error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }
    });
    // 面辅料超数审核
    $.ajax({
        url: "/erp/getfabricdetailreviewdatacount",
        type: 'GET',
        data: {},
        success: function (res) {
            if (res.count != null) {
                $("#fabricAccessoryOverReviewCount").text(res.count);
            } else {
                layer.msg("获取失败！", {icon: 2});
            }
        }, error: function () {
            layer.msg("获取失败！", {icon: 2});
        }
    });
    // 修改审核数量
    $.ajax({
        url: "/erp/getmanufactureordercountreviewdatacount",
        type: 'GET',
        data: {},
        success: function (res) {
            if (res.count != null) {
                $("#orderClothesModify").text(res.count);
            } else {
                layer.msg("获取失败！", {icon: 2});
            }
        }, error: function () {
            layer.msg("获取失败！", {icon: 2});
        }
    });
    //待审工价
    $.ajax({
        url: "/erp/getpendingorderprocedurecount",
        type: 'GET',
        data: {},
        success: function (data) {
            if(data.pendingCount) {
                $("#pendingOrderProcedureCount").text(data.pendingCount);
            }
        }, error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }
    });

    //松布裁剪计划
    // $.ajax({
    //     url: "/erp/getdepartmentplanonemonth",
    //     type: 'GET',
    //     data: {},
    //     success: function (data) {
    //         if(data.data) {
    //
    //         }
    //     }, error: function () {
    //         swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
    //     }
    // });

    // var dom = document.getElementById("container");
    // var myChart = echarts.init(dom);
    //
    // var dateList = [];
    // var amount1 = [];
    // var amount2 = [];
    // var amount3 = [];
    // var amount4 = [];
    // $.ajax({
    //     url: "getweekworkamount",
    //     type:'GET',
    //     data: {},
    //     success: function (data) {
    //         if(data) {
    //             dateList = data["dateList"];
    //             var data1 = data["1"];
    //             var data2 = data["2"];
    //             var data3 = data["3"];
    //             var data4 = data["4"];
    //             $.each(dateList, function(index,element){
    //                 amount1.push(data1[element]);
    //                 amount2.push(data2[element]);
    //                 amount3.push(data3[element]);
    //                 amount4.push(data4[element]);
    //             });
    //
    //             option = null;
    //             option = {
    //                 title: {},
    //                 color:['#d06d6a','#697883','#5bd6e9','#69d18c'],
    //                 tooltip : {
    //                     trigger: 'axis',
    //                     axisPointer: {
    //                         type: 'cross',
    //                         label: {
    //                             backgroundColor: '#6a7985'
    //                         }
    //                     }
    //                 },
    //                 legend: {
    //                     data:['裁床裁数','下衣胚数','车间产量','后整产量'],
    //                     textStyle:{
    //                         fontSize: 18
    //                     }
    //                 },
    //                 grid: {
    //                     left: '3%',
    //                     right: '6%',
    //                     bottom: '3%',
    //                     containLabel: true
    //                 },
    //                 xAxis : [
    //                     {
    //                         type : 'category',
    //                         boundaryGap : false,
    //                         data : dateList,
    //                         // data : [getDay(-7),getDay(-6),getDay(-5),getDay(-4),getDay(-3),getDay(-2),getDay(-1)]
    //                     }
    //                 ],
    //                 yAxis : [
    //                     {
    //                         type : 'value'
    //                     }
    //                 ],
    //                 series : [
    //                     {
    //                         name:'后整产量',
    //                         type:'line',
    //                         stack: '总量1',
    //                         label: {
    //                             normal: {
    //                                 show: true
    //                             }
    //                         },
    //                         areaStyle: {
    //                             normal: {
    //                                 color: "#00000000"
    //                             }
    //                         },
    //                         // data:[220, 182, 191, 234, 290, 330, 310]
    //                         data : amount4
    //                     },
    //                     {
    //                         name:'车间产量',
    //                         type:'line',
    //                         stack: '总量2',
    //                         label: {
    //                             normal: {
    //                                 show: true
    //                             }
    //                         },
    //                         areaStyle: {
    //                             normal: {
    //                                 color: "#00000000"
    //                             }
    //                         },
    //                         // data:[150, 232, 201, 154, 190, 330, 410]
    //                         data : amount3
    //                     },
    //                     {
    //                         name:'下衣胚数',
    //                         type:'line',
    //                         stack: '总量3',
    //                         label: {
    //                             normal: {
    //                                 show: true
    //                             }
    //                         },
    //                         areaStyle: {
    //                             normal: {
    //                                 color: "#00000000"
    //                             }
    //                         },
    //                         // data:[320, 332, 301, 334, 390, 330, 320]
    //                         data : amount2
    //                     },
    //                     {
    //                         name:'裁床裁数',
    //                         type:'line',
    //                         stack: '总量4',
    //                         label: {
    //                             normal: {
    //                                 show: true,
    //                                 position: 'top'
    //                             }
    //                         },
    //                         areaStyle: {
    //                             normal: {
    //                                 color: "#00000000"
    //                             }
    //                         },
    //                         // data:[820, 932, 901, 934, 1290, 1330, 1320]
    //                         data : amount1
    //                     }
    //                 ]
    //             };
    //             if (option && typeof option === "object") {
    //                 myChart.setOption(option, true);
    //             }
    //             // tools.loopShowTooltip(myChart, option, {loopSeries: true});
    //             window.onresize = myChart.resize;
    //
    //         }else {
    //             swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，没有查到相关信息！</span>",html: true});
    //         }
    //     },
    //     error: function () {
    //         swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
    //     }
    // });
    //
    //

});

function fabricAccessoryOrder(){
    // layer.msg("您没有权限");
    return this.location.href="/erp/fabricAccessoryCheckStart";
}
function toDecimal(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*100)/100;
    return f;
}