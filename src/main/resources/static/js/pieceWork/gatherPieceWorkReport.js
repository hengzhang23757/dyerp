var basePath=$("#basePath").val();
var userName=$("#userName").val();
var userRole=$("#userRole").val();
var procedureInfoList;
var form;
layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.21'
}).extend();
$(document).ready(function () {
    layui.use('form', function(){
        form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
        form.render();
    });

    layui.laydate.render({
        elem: '#from',
        trigger: 'click'
    });
    layui.laydate.render({
        elem: '#to',
        trigger: 'click'
    });

    $.ajax({
        url: "/erp/getallcheckgroupname",
        type: 'GET',
        data: {},
        success:function(res){
            $("#groupName").empty();
            if (res.data){
                $("#groupName").append('<option value="">全部</option>');
                $.each(res.data, function(index,element){
                    $("#groupName").append("<option value="+element+">"+element+"</option>");
                });
            }
            form.render();
        }, error: function () {
            layer.msg("获取组别信息失败", { icon: 2 });
        }
    });

    layui.use(['autocomplete'], function () {
        layui.autocomplete.render({
            elem: $('#employeeNumber')[0],
            url: 'getemphint',
            template_val: '{{d.employeeNumber}}',
            template_txt: '{{d.employeeNumber}}',
            onselect: function (resp) {
                autoComplete(resp.employeeNumber);
            }
        })
    });

    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumber", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });
});
layui.use(['form', 'soulTable', 'table', 'element'], function () {
    var table = layui.table,
        soulTable = layui.soulTable,
        $ = layui.$;
    var load = layer.load();
    reportTable = table.render({
        elem: '#reportTable'
        ,cols:[[]]
        ,loading:true
        ,data:[]
        ,height: 'full-130'
        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
        ,title: '产能汇总'
        ,totalRow: true
        ,page: true
        ,overflow: 'tips'
        ,limits: [500, 1000, 2000]
        ,limit: 1000 //每页默认显示的数量
        ,filter: {
            clearFilter: true
        }
        ,done: function () {
            soulTable.render(this);
            layer.close(load);
        }
    });
    table.on('toolbar(reportTable)', function(obj) {
        if (obj.event === 'clearFilter') {
            // 清除所有筛选条件并重载表格
            // 参数: tableId
            soulTable.clearFilter('reportTable')
        } else if (obj.event === 'refresh') {
            reportTable.reload();
        } else if (obj.event === 'exportExcel') {
            soulTable.export('reportTable');
        }
    });
    //监听提交
    form.on('submit(searchBeat)', function(data){
        var param = {};
        var orderName = $("#orderName").val();
        var groupName = $("#groupName").val();
        var from = $("#from").val();
        var to = $("#to").val();
        var employeeNumber = $("#employeeNumber").val();
        var procedureNumber = $("#procedureNumber").val();
        var procedureState = $("#procedureState").val();
        var searchType = $("#searchType").val();
        param.searchType = searchType;
        if ((orderName == '' || orderName == null) && ((from == "" || from == null) && (to == "" || to == null))){
            layer.msg("款号和区间不能同时为空必须", { icon: 2 });
            return false;
        }
        if (orderName != "" && orderName != null){
            param.orderName = orderName;
        }
        if (groupName != "" && groupName != null){
            param.groupName = groupName;
        }
        if (employeeNumber != "" && employeeNumber != null){
            param.employeeNumber = employeeNumber;
        }
        if (from != "" && from != null){
            param.from = from;
        }
        if (to != "" && to != null){
            param.to = to;
        }
        if (procedureNumber != "" && procedureNumber != null){
            param.procedureNumber = procedureNumber;
        }
        if (procedureState != "" && procedureState != null){
            param.procedureState = procedureState;
        }
        var load = layer.load(2);
        $.ajax({
            url: "/erp/searchgatherpieceworkbyinfo",
            type: 'GET',
            data: param,
            success: function (res) {
                if (res.generalSalaryList) {
                    var reportData = res.generalSalaryList;
                    if (searchType === '产能详情'){
                        table.render({
                            elem:"#reportTable"
                            ,totalRow: true
                            ,cols:[[
                                {type:'numbers', align:'center', title:'序号', width:'5%'}
                                ,{field:'employeeName', title:'姓名', align:'center', width:'8%', sort: true, filter: true, totalRowText: '合计'}
                                ,{field:'employeeNumber', title:'工号', align:'center', width:'8%', sort: true, filter: true}
                                ,{field:'groupName', title:'组名', align:'center', width:'8%', sort: true, filter: true}
                                ,{field:'clothesVersionNumber', title:'单号', align:'center', width:'11%', sort: true, filter: true}
                                ,{field:'orderName', title:'款号', align:'center', width:'11%', sort: true, filter: true}
                                ,{field:'colorName', title:'颜色', align:'center', width:'8%', sort: true, filter: true}
                                ,{field:'sizeName', title:'尺码', align:'center', width:'8%', sort: true, filter: true}
                                ,{field:'procedureNumber', title:'工序号', align:'center', width:'8%', sort: true, filter: true}
                                ,{field:'procedureName', title:'工序名', align:'center', width:'8%', sort: true, filter: true}
                                ,{field:'pieceCount', title:'数量', align:'center', width:'8%', sort: true, filter: true, totalRow: true}
                                ,{field:'generalDate', title:'日期', align:'center', width:'9%', sort: true, filter: true,templet: function (d) {
                                        return layui.util.toDateString(d.generalDate, 'yyyy-MM-dd');
                                    }}
                            ]]
                            ,excel: {
                                filename: '计件数据.xlsx'
                            }
                            ,data: reportData   // 将新数据重新载入表格
                            ,overflow: 'tips'
                            ,height: 'full-130'
                            ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                            ,title: '产能汇总'
                            ,page: true
                            ,limits: [500, 1000, 2000]
                            ,limit: 1000 //每页默认显示的数量
                            ,done: function () {
                                soulTable.render(this);
                                layer.close(load);
                            }
                        });
                    } else if (searchType === '产能汇总'){
                        table.render({
                            elem:"#reportTable"
                            ,totalRow: true
                            ,cols:[[
                                {type:'numbers', align:'center', title:'序号', width:'5%'}
                                ,{field:'employeeName', title:'姓名', align:'center', width:'10%', sort: true, filter: true, totalRowText: '合计'}
                                ,{field:'employeeNumber', title:'工号', align:'center', width:'10%', sort: true, filter: true}
                                ,{field:'groupName', title:'组名', align:'center', width:'10%', sort: true, filter: true}
                                ,{field:'clothesVersionNumber', title:'单号', align:'center', width:'15%', sort: true, filter: true}
                                ,{field:'orderName', title:'款号', align:'center', width:'15%', sort: true, filter: true}
                                ,{field:'procedureNumber', title:'工序号', align:'center', width:'10%', sort: true, filter: true}
                                ,{field:'procedureName', title:'工序名', align:'center', width:'15%', sort: true, filter: true}
                                ,{field:'pieceCount', title:'数量', align:'center', width:'10%', sort: true, filter: true, totalRow: true}
                            ]]
                            ,excel: {
                                filename: '计件数据.xlsx'
                            }
                            ,data: reportData   // 将新数据重新载入表格
                            ,overflow: 'tips'
                            ,height: 'full-130'
                            ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                            ,title: '产能汇总'
                            ,page: true
                            ,limits: [500, 1000, 2000]
                            ,limit: 1000 //每页默认显示的数量
                            ,done: function () {
                                soulTable.render(this);
                                layer.close(load);
                            }
                        });
                    } else if (searchType === '金额汇总'){
                        table.render({
                            elem:"#reportTable"
                            ,totalRow: true
                            ,cols:[[
                                {type:'numbers', align:'center', title:'序号', width:60}
                                ,{field:'employeeName', title:'姓名', align:'center', width:100, sort: true, filter: true, totalRowText: '合计'}
                                ,{field:'employeeNumber', title:'工号', align:'center', width:120, sort: true, filter: true}
                                ,{field:'groupName', title:'组名', align:'center', width:100, sort: true, filter: true}
                                ,{field:'salary', title:'金额', align:'center', width:120, sort: true, filter: true, totalRow: true, templet: function (d) {
                                        return toDecimal(d.salary);
                                    }}
                                ,{field:'salaryTwo', title:'上浮', align:'center', width:120, sort: true, filter: true, totalRow: true, templet: function (d) {
                                        return toDecimal(d.salaryTwo);
                                    }}
                                ,{field:'salarySum', title:'总金额', align:'center', width:120, sort: true, filter: true, totalRow: true, templet: function (d) {
                                        return toDecimal(d.salarySum);
                                    }}
                            ]]
                            ,excel: {
                                filename: '计件数据.xlsx'
                            }
                            ,data: reportData   // 将新数据重新载入表格
                            ,overflow: 'tips'
                            ,height: 'full-130'
                            ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                            ,title: '产能汇总'
                            ,page: true
                            ,limits: [500, 1000, 2000]
                            ,limit: 1000 //每页默认显示的数量
                            ,done: function () {
                                soulTable.render(this);
                                layer.close(load);
                            }
                        });
                    } else if (searchType === '金额详情'){
                        table.render({
                            elem:"#reportTable"
                            ,totalRow: true
                            ,cols:[[
                                {type:'numbers', align:'center', title:'序号', width:60}
                                ,{field:'employeeName', title:'姓名', align:'center', width:100, sort: true, filter: true, totalRowText: '合计'}
                                ,{field:'employeeNumber', title:'工号', align:'center', width:100, sort: true, filter: true}
                                ,{field:'groupName', title:'组名', align:'center', width:100, sort: true, filter: true}
                                ,{field:'clothesVersionNumber', title:'单号', align:'center', width:160, sort: true, filter: true}
                                ,{field:'orderName', title:'款号', align:'center', width:160, sort: true, filter: true}
                                ,{field:'procedureNumber', title:'工序号', align:'center', width:90, sort: true, filter: true}
                                ,{field:'procedureName', title:'工序名', align:'center', width:120, sort: true, filter: true}
                                ,{field:'pieceCount', title:'数量', align:'center', width:90, sort: true, filter: true, totalRow: true}
                                ,{field:'generalDate', title:'日期', align:'center', width:120, sort: true, filter: true,templet: function (d) {
                                        return layui.util.toDateString(d.generalDate, 'yyyy-MM-dd');
                                    }}
                                ,{field:'price', title:'单价', align:'center', width:90, sort: true, filter: true, totalRow: true, templet: function (d) {
                                        return toDecimalFour(d.price);
                                    }}
                                ,{field:'salary', title:'金额', align:'center', width:90, sort: true, filter: true, totalRow: true, templet: function (d) {
                                        return toDecimal(d.salary);
                                    }}
                                ,{field:'priceTwo', title:'上浮', align:'center', width:90, sort: true, filter: true, totalRow: true, templet: function (d) {
                                        return toDecimalFour(d.priceTwo);
                                    }}
                                ,{field:'salaryTwo', title:'金额上浮', align:'center', width:100, sort: true, filter: true, totalRow: true, templet: function (d) {
                                        return toDecimal(d.salaryTwo);
                                    }}
                                ,{field:'salarySum', title:'总金额', align:'center', width:100, sort: true, filter: true, totalRow: true, templet: function (d) {
                                        return toDecimal(d.salarySum);
                                    }}
                            ]]
                            ,excel: {
                                filename: '计件数据.xlsx'
                            }
                            ,data: reportData   // 将新数据重新载入表格
                            ,overflow: 'tips'
                            ,height: 'full-130'
                            ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                            ,title: '产能汇总'
                            ,page: true
                            ,limits: [500, 1000, 2000]
                            ,limit: 1000 //每页默认显示的数量
                            ,done: function () {
                                soulTable.render(this);
                                layer.close(load);
                            }
                        });
                    }


                } else {
                    layer.msg("获取失败！", {icon: 2});
                }
            }, error: function () {
                layer.msg("获取失败！", {icon: 2});
            }
        });
        return false;
    });
});

function toDecimal(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*100)/100;
    return f;
}

function toDecimalFour(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*10000)/10000;
    return f;
}

function autoComplete(employeeNumber) {
    $("#employeeName").empty();
    $.ajax({
        url: "/erp/getemployeebyemployeenumber",
        data: {"employeeNumber": employeeNumber},
        success:function(data){
            $("#employeeName").empty();
            if (data.employeeList) {
                $("#employeeName").val(data.employeeList[0].employeeName);
            }
        }, error:function(){
        }
    });
}