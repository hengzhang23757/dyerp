var basePath=$("#basePath").val();
var userName=$("#userName").val();
var userRole=$("#userRole").val();
var procedureInfoList;
var form;
layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.21'
}).extend();
$(document).ready(function () {
    layui.use('form', function(){
        form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
        form.render();
    });
    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumber", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });

    $("#orderName").bind('blur change',function(){
        var orderName = $("#orderName").val();
        $("#colorName").empty();
        $.ajax({
            url: "/erp/getordercolornamesbyorder",
            data: {"orderName": orderName},
            success:function(data){
                $("#colorName").empty();
                if (data.colorNameList) {
                    $("#colorName").append("<option value=''>颜色</option>");
                    $.each(data.colorNameList, function(index,element){
                        $("#colorName").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
                form.render();
            }, error:function(){
            }
        });
        $("#sizeName").empty();
        $.ajax({
            url: "/erp/getordersizenamesbyorder",
            data: {"orderName": orderName},
            success:function(data){
                $("#sizeName").empty();
                if (data.sizeNameList) {
                    $("#sizeName").append("<option value=''>尺码</option>");
                    $.each(data.sizeNameList, function(index,element){
                        $("#sizeName").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
                form.render();
            }, error:function(){
            }
        });
        $("#bedNumber").empty();
        $.ajax({
            url: "/erp/getbednumbersbyordername",
            data: {"orderName": orderName},
            success:function(data){
                $("#bedNumber").empty();
                if (data.bedNumList) {
                    $("#bedNumber").append("<option value=''>选择床号</option>");
                    $.each(data.bedNumList, function(index,element){
                        $("#bedNumber").append("<option value="+element+">"+element+"</option>");
                    })
                }
                form.render();
            }, error:function(){
            }
        });
        $("#procedureNumber").empty();
        $.ajax({
            url: "/erp/getprocedureinfobyorder",
            data: {"orderName": orderName},
            success:function(data){
                $("#procedureNumber").empty();
                if (data.procedureInfoList) {
                    procedureInfoList = data.procedureInfoList;
                    $("#procedureNumber").append("<option value=''>选择工序</option>");
                    $.each(data.procedureInfoList, function(index,element){
                        var procedureNum = element.procedureNumber;
                        var procedureName = element.procedureName;
                        $("#procedureNumber").append("<option value="+procedureNum+">"+procedureNum+"-"+procedureName+"</option>");
                    })
                }
                form.render();
            }, error:function(){
            }
        });
    });

    $("#clothesVersionNumber").bind('blur change',function(){
        var orderName = $("#orderName").val();
        $("#colorName").empty();
        $.ajax({
            url: "/erp/getordercolornamesbyorder",
            data: {"orderName": orderName},
            success:function(data){
                $("#colorName").empty();
                if (data.colorNameList) {
                    $("#colorName").append("<option value=''>颜色</option>");
                    $.each(data.colorNameList, function(index,element){
                        $("#colorName").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
                form.render();
            }, error:function(){
            }
        });
        $("#sizeName").empty();
        $.ajax({
            url: "/erp/getordersizenamesbyorder",
            data: {"orderName": orderName},
            success:function(data){
                $("#sizeName").empty();
                if (data.sizeNameList) {
                    $("#sizeName").append("<option value=''>尺码</option>");
                    $.each(data.sizeNameList, function(index,element){
                        $("#sizeName").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
                form.render();
            }, error:function(){
            }
        });
        $("#bedNumber").empty();
        $.ajax({
            url: "/erp/getbednumbersbyordername",
            data: {"orderName": orderName},
            success:function(data){
                $("#bedNumber").empty();
                if (data.bedNumList) {
                    $("#bedNumber").append("<option value=''>选择床号</option>");
                    $.each(data.bedNumList, function(index,element){
                        $("#bedNumber").append("<option value="+element+">"+element+"</option>");
                    })
                }
                form.render();
            }, error:function(){
            }
        });
        $("#procedureNumber").empty();
        $.ajax({
            url: "/erp/getprocedureinfobyorder",
            data: {"orderName": orderName},
            success:function(data){
                $("#procedureNumber").empty();
                if (data.procedureInfoList) {
                    procedureInfoList = data.procedureInfoList;
                    $("#procedureNumber").append("<option value=''>选择工序</option>");
                    $.each(data.procedureInfoList, function(index,element){
                        var procedureNum = element.procedureNumber;
                        var procedureName = element.procedureName;
                        $("#procedureNumber").append("<option value="+procedureNum+">"+procedureNum+"-"+procedureName+"</option>");
                    })
                }
                form.render();
            }, error:function(){
            }
        });
    });
});


layui.use(['form', 'soulTable', 'table', 'element'], function () {
    var table = layui.table,
        soulTable = layui.soulTable,
        element = layui.element,
        $ = layui.$;
    var load = layer.load();
    reportTable = table.render({
        elem: '#reportTable'
        ,cols:[[]]
        ,loading:true
        ,data:[]
        ,height: 'full-130'
        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
        ,defaultToolbar: ['filter', 'print']
        ,title: '计件记录表'
        ,totalRow: true
        ,page: true
        ,overflow: 'tips'
        ,limits: [500, 1000, 2000]
        ,limit: 1000 //每页默认显示的数量
        ,done: function () {
            soulTable.render(this);
            layer.close(load);
        }
        ,filter: {
            bottom: true,
            items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
        }
    });
    table.on('toolbar(reportTable)', function(obj) {
        if (obj.event === 'clearFilter') {
            // 清除所有筛选条件并重载表格
            // 参数: tableId
            soulTable.clearFilter('reportTable')
        } else if (obj.event === 'refresh') {
            reportTable.reload();
        } else if (obj.event === 'exportExcel') {
            soulTable.export('reportTable');
        }
    });
    //监听提交
    form.on('submit(searchBeat)', function(data){
        var param = {};
        var orderName = $("#orderName").val();
        var colorName = $("#colorName").val();
        var sizeName = $("#sizeName").val();
        var bedNumber = $("#bedNumber").val();
        var packageNumber = $("#packageNumber").val();
        var procedureNumber = $("#procedureNumber").val();
        if (orderName == '' || orderName == null || procedureNumber == null || procedureNumber == ''){
            layer.msg("款号-工序必须", { icon: 2 });
            return false;
        }
        param.orderName = orderName;
        param.procedureNumber = procedureNumber;
        if (colorName != null && colorName != ""){
            param.colorName = colorName;
        }
        if (sizeName != null && sizeName != ''){
            param.sizeName = sizeName;
        }
        if (bedNumber != null && bedNumber != ''){
            param.bedNumber = bedNumber;
        }
        if (packageNumber != null && packageNumber != ''){
            param.packageNumber = packageNumber;
        }
        var load = layer.load(2);
        $.ajax({
            url: "searchpieceworkleak",
            type: 'GET',
            data: param,
            success: function (res) {
                if (res.cutLocationList) {
                    var reportData = res.cutLocationList;
                    table.reload("reportTable", {
                        cols:[[
                            {type:'numbers', align:'center', title:'序号', width:60}
                            ,{field:'clothesVersionNumber', title:'单号', align:'center', width:160, sort: true, filter: true}
                            ,{field:'orderName', title:'款号', align:'center', width:160, sort: true, filter: true}
                            ,{field:'bedNumber', title:'床号', align:'center', width:90, sort: true, filter: true}
                            ,{field:'colorName', title:'颜色', align:'center', width:150, sort: true, filter: true}
                            ,{field:'sizeName', title:'尺码', align:'center', width:150, sort: true, filter: true}
                            ,{field:'packageNumber', title:'扎号', align:'center', width:120, sort: true, filter: true}
                            ,{field:'layerCount', title:'数量', align:'center', width:120, sort: true, filter: true, totalRow: true}
                            ,{field:'cutLocation', title:'裁片超市', align:'center', width:150, sort: true, filter: true}
                            ,{field:'embLocation', title:'衣胚超市', align:'center', width:150, sort: true, filter: true}
                            ,{field:'workshop', title:'下车间', align:'center', width:120, sort: true, filter: true}
                            ,{field:'pieceWork', title:'计件', align:'center', width:120, sort: true, filter: true}
                        ]]
                        ,excel: {
                            filename: '计件数据.xlsx'
                        }
                        ,data: reportData   // 将新数据重新载入表格
                        ,overflow: 'tips'
                        ,done: function () {
                            soulTable.render(this);
                            layer.close(load);
                        }
                        ,filter: {
                            bottom: true,
                            clearFilter: false,
                            items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                        }
                    });

                } else {
                    layer.msg("获取失败！", {icon: 2});
                }
            }, error: function () {
                layer.msg("获取失败！", {icon: 2});
            }
        });
        return false;
    });
});
