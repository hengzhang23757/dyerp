var basePath=$("#basePath").val();
var userName=$("#userName").val();
var userRole=$("#userRole").val();
var procedureInfoList;
var form;
layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.21'
}).extend();
$(document).ready(function () {
    layui.use('form', function(){
        form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
        form.render();
    });
    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumber", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });

    $("#orderName").bind('blur change',function(){
        var orderName = $("#orderName").val();
        $("#colorName").empty();
        $.ajax({
            url: "/erp/getordercolornamesbyorder",
            data: {"orderName": orderName},
            success:function(data){
                $("#colorName").empty();
                if (data.colorNameList) {
                    $("#colorName").append("<option value=''>颜色</option>");
                    $.each(data.colorNameList, function(index,element){
                        $("#colorName").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
                form.render();
            }, error:function(){
            }
        });
        $("#sizeName").empty();
        $.ajax({
            url: "/erp/getordersizenamesbyorder",
            data: {"orderName": orderName},
            success:function(data){
                $("#sizeName").empty();
                if (data.sizeNameList) {
                    $("#sizeName").append("<option value=''>尺码</option>");
                    $.each(data.sizeNameList, function(index,element){
                        $("#sizeName").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
                form.render();
            }, error:function(){
            }
        });
        $("#bedNumber").empty();
        $.ajax({
            url: "/erp/getbednumbersbyordername",
            data: {"orderName": orderName},
            success:function(data){
                $("#bedNumber").empty();
                if (data.bedNumList) {
                    $("#bedNumber").append("<option value=''>选择床号</option>");
                    $.each(data.bedNumList, function(index,element){
                        $("#bedNumber").append("<option value="+element+">"+element+"</option>");
                    })
                }
                form.render();
            }, error:function(){
            }
        });
        $("#procedureNumber").empty();
        $.ajax({
            url: "/erp/getprocedureinfobyorder",
            data: {"orderName": orderName},
            success:function(data){
                $("#procedureNumber").empty();
                if (data.procedureInfoList) {
                    procedureInfoList = data.procedureInfoList;
                    $("#procedureNumber").append("<option value=''>选择工序</option>");
                    $.each(data.procedureInfoList, function(index,element){
                        var procedureNum = element.procedureNumber;
                        var procedureName = element.procedureName;
                        $("#procedureNumber").append("<option value="+procedureNum+">"+procedureNum+"-"+procedureName+"</option>");
                    })
                }
                form.render();
            }, error:function(){
            }
        });
    });

    $("#clothesVersionNumber").bind('blur change',function(){
        var orderName = $("#orderName").val();
        $("#colorName").empty();
        $.ajax({
            url: "/erp/getordercolornamesbyorder",
            data: {"orderName": orderName},
            success:function(data){
                $("#colorName").empty();
                if (data.colorNameList) {
                    $("#colorName").append("<option value=''>颜色</option>");
                    $.each(data.colorNameList, function(index,element){
                        $("#colorName").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
                form.render();
            }, error:function(){
            }
        });
        $("#sizeName").empty();
        $.ajax({
            url: "/erp/getordersizenamesbyorder",
            data: {"orderName": orderName},
            success:function(data){
                $("#sizeName").empty();
                if (data.sizeNameList) {
                    $("#sizeName").append("<option value=''>尺码</option>");
                    $.each(data.sizeNameList, function(index,element){
                        $("#sizeName").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
                form.render();
            }, error:function(){
            }
        });
        $("#bedNumber").empty();
        $.ajax({
            url: "/erp/getbednumbersbyordername",
            data: {"orderName": orderName},
            success:function(data){
                $("#bedNumber").empty();
                if (data.bedNumList) {
                    $("#bedNumber").append("<option value=''>选择床号</option>");
                    $.each(data.bedNumList, function(index,element){
                        $("#bedNumber").append("<option value="+element+">"+element+"</option>");
                    })
                }
                form.render();
            }, error:function(){
            }
        });
        $("#procedureNumber").empty();
        $.ajax({
            url: "/erp/getprocedureinfobyorder",
            data: {"orderName": orderName},
            success:function(data){
                $("#procedureNumber").empty();
                if (data.procedureInfoList) {
                    procedureInfoList = data.procedureInfoList;
                    $("#procedureNumber").append("<option value=''>选择工序</option>");
                    $.each(data.procedureInfoList, function(index,element){
                        var procedureNum = element.procedureNumber;
                        var procedureName = element.procedureName;
                        $("#procedureNumber").append("<option value="+procedureNum+">"+procedureNum+"-"+procedureName+"</option>");
                    })
                }
                form.render();
            }, error:function(){
            }
        });
    });
});
layui.use(['form', 'soulTable', 'table', 'element'], function () {
    var table = layui.table,
        soulTable = layui.soulTable,
        form = layui.form,
        element = layui.element,
        $ = layui.$;
    var load = layer.load();
    reportTable = table.render({
        elem: '#reportTable'
        ,cols:[[]]
        ,data:[]
        ,height: 'full-130'
        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
        ,defaultToolbar: ['filter', 'print']
        ,title: '计件记录表'
        ,totalRow: true
        ,page: true
        ,overflow: 'tips'
        ,limits: [500, 1000, 2000]
        ,limit: 1000 //每页默认显示的数量
        ,done: function () {
            soulTable.render(this);
            layer.close(load);
        }
        ,filter: {
            bottom: true,
            items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
        }
    });

    function initTable(orderName, colorName, sizeName, bedNumber, packageNumber, procedureNumber){
        var data = {};
        data.orderName = orderName;
        data.procedureNumber = procedureNumber;
        if (colorName != null && colorName != ""){
            data.colorName = colorName;
        }
        if (sizeName != null && sizeName != ''){
            data.sizeName = sizeName;
        }
        if (bedNumber != null && bedNumber != ''){
            data.bedNumber = bedNumber;
        }
        if (packageNumber != null && packageNumber != ''){
            data.packageNumber = packageNumber;
        }
        var load = layer.load(2);
        soulTable.clearFilter('reportTable');
        $.ajax({
            url: "/erp/searchpieceworkmanagement",
            type:'GET',
            data: data,
            success: function (res) {
                if(res.data) {
                    var reportData = res.data;
                    reportTable = table.render({
                        elem: '#reportTable'
                        ,page: true
                        ,cols:[[
                            {type:'numbers', align:'center', title:'序号', width:60}
                            ,{type:'checkbox', field:'pieceWorkID'}
                            ,{field:'employeeName', title:'姓名', align:'center', width:100, sort: true, filter: true, totalRowText: '合计'}
                            ,{field:'employeeNumber', title:'工号', align:'center', width:90, sort: true, filter: true}
                            ,{field:'groupName', title:'组名', align:'center', width:100, sort: true, filter: true}
                            ,{field:'clothesVersionNumber', title:'单号', align:'center', width:160, sort: true, filter: true}
                            ,{field:'orderName', title:'款号', align:'center', width:160, sort: true, filter: true}
                            ,{field:'bedNumber', title:'床号', align:'center', width:90, sort: true, filter: true}
                            ,{field:'colorName', title:'颜色', align:'center', width:120, sort: true, filter: true}
                            ,{field:'sizeName', title:'尺码', align:'center', width:120, sort: true, filter: true}
                            ,{field:'packageNumber', title:'扎号', align:'center', width:90, sort: true, filter: true}
                            ,{field:'procedureNumber', title:'工序号', align:'center', width:100, sort: true, filter: true}
                            ,{field:'procedureName', title:'工序名', align:'center', width:120, sort: true, filter: true}
                            ,{field:'layerCount', title:'数量', align:'center', width:90, sort: true, filter: true, totalRow: true}
                            ,{field:'pieceType', title:'类型', align:'center', width:90, sort: true, filter: true}
                            ,{field:'createTime', title:'计件时间', align:'center', width:180, sort: true, filter: true,templet: function (d) {
                                    return globalTransTime(d.createTime);
                                }}
                            ,{fixed: 'right', title:'操作', align:'center', toolbar: '#barTop', width:80}
                        ]]
                        ,excel: {
                            filename: '计件数据.xlsx',
                        }
                        ,data: reportData
                        ,height: 'full-100'
                        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                        ,defaultToolbar: ['filter', 'print']
                        ,title: '计件记录表'
                        ,totalRow: true
                        ,overflow: 'tips'
                        ,limits: [500, 1000, 2000]
                        ,limit: 1000 //每页默认显示的数量
                        ,done: function () {
                            soulTable.render(this);
                            layer.close(load);
                        }
                    });
                }
            }, error: function () {
                layer.msg("获取失败！",{icon: 2});
            }
        });
        return false;
    }

    //监听提交
    form.on('submit(searchBeat)', function(data){
        $("#checkInfo").val("0扎/0件");
        var data = {};
        var orderName = $("#orderName").val();
        var colorName = $("#colorName").val();
        var sizeName = $("#sizeName").val();
        var bedNumber = $("#bedNumber").val();
        var packageNumber = $("#packageNumber").val();
        var procedureNumber = $("#procedureNumber").val();
        if (orderName == '' || orderName == null || procedureNumber == null || procedureNumber == ''){
            layer.msg("款号-工序必须", { icon: 2 });
            return false;
        }
        initTable(orderName, colorName, sizeName, bedNumber, packageNumber, procedureNumber);
        return false;
    });

    table.on('toolbar(reportTable)', function(obj) {
        if (obj.event === 'clearFilter') {
            // 清除所有筛选条件并重载表格
            // 参数: tableId
            soulTable.clearFilter('reportTable')
        } else if (obj.event === 'refresh') {
            reportTable.reload();
        } else if (obj.event === 'exportExcel') {
            soulTable.export('reportTable');
        } else if (obj.event === 'updateName'){
            var orderName = $("#orderName").val();
            var procedureNumber = $("#procedureNumber").val();
            if (userRole!='root' && userRole!='role5' && userRole!='role1' && userRole!='role12'){
                layer.msg('您没有权限!');
                return false;
            } else if (orderName == null || orderName == '' || procedureNumber == '' || procedureNumber == null) {
                layer.msg('请先选择款号和工序!');
                return false;
            } else {
                var index = layer.open({
                    type: 1 //Page层类型
                    , title: "修改工序名"
                    , btn: ['保存']
                    , shade: 0.6 //遮罩透明度
                    , maxmin: false //允许全屏最小化
                    , anim: 0 //0-6的动画形式，-1不开启
                    , area: ['400px', '200px']
                    , content: "<div><table><tr><td style=\"text-align: right;margin-bottom: 15px;padding-top: 20px\">\n" +
                        "                                <label class=\"layui-form-label\">修改成:</label>\n" +
                        "                            </td>\n" +
                        "                            <td style=\"margin-bottom: 15px; padding-top: 20px\">\n" +
                        "                                <input type=\"text\" name=\"updateProcedureName\" id=\"updateProcedureName\" class=\"layui-input\">\n" +
                        "                            </td></tr></table></div>"
                    ,yes: function(i, layero){
                        $.ajax({
                            url: basePath + "erp/updateprocedurenamebyorderprocedure",
                            type:'POST',
                            data: {
                                orderName: $("#orderName").val(),
                                procedureName: $("#updateProcedureName").val(),
                                procedureNumber: $("#procedureNumber").val()
                            },
                            success: function (data) {
                                if(data.result == 0) {
                                    layer.close(index);
                                    layer.msg("修改成功！",{icon: 1});
                                    var orderName = $("#orderName").val();
                                    var colorName = $("#colorName").val();
                                    var sizeName = $("#sizeName").val();
                                    var bedNumber = $("#bedNumber").val();
                                    var packageNumber = $("#packageNumber").val();
                                    var procedureNumber = $("#procedureNumber").val();
                                    initTable(orderName, colorName, sizeName, bedNumber, packageNumber, procedureNumber);
                                }else {
                                    layer.msg("修改失败！",{icon: 2});
                                }
                            }, error: function () {
                                layer.msg("修改失败！",{icon: 2});
                            }
                        });
                    }, cancel : function (i,layero) {
                        $("#updateProcedureName").val('');
                    }
                });
                var checkText=$("#procedureNumber").find("option:selected").text();
                $("#updateProcedureName").val(checkText.split('-')[1]);
            }
        } else if (obj.event === 'delete'){
            var checkStatus = table.checkStatus(obj.config.id); //获取选中行状态
            if (userRole!='root' && userRole!='role10'){
                layer.msg('您没有权限!');
                return false;
            } else if(checkStatus.data.length == 0) {
                layer.msg('请先选择数据');
                return false;
            } else {
                var pieceWorkIDList = [];
                $.each(checkStatus.data, function (index, item) {
                    pieceWorkIDList.push(item.pieceWorkID);
                });
                layer.confirm('真的删除吗', function(index){
                    $.ajax({
                        url: basePath + "erp/deletepieceworkbatch",
                        type:'POST',
                        data: {
                            userName:userName,
                            pieceWorkIDList:pieceWorkIDList
                        },
                        traditional:true,
                        success: function (data) {
                            if(data == 0) {
                                layer.msg("修改成功！",{icon: 1});
                                var orderName = $("#orderName").val();
                                var colorName = $("#colorName").val();
                                var sizeName = $("#sizeName").val();
                                var bedNumber = $("#bedNumber").val();
                                var packageNumber = $("#packageNumber").val();
                                var procedureNumber = $("#procedureNumber").val();
                                initTable(orderName, colorName, sizeName, bedNumber, packageNumber, procedureNumber);
                            } else if (data == 6) {
                                layer.msg("工资已锁定,无法删除！",{icon: 2});
                            } else {
                                layer.msg("删除失败！",{icon: 2});
                            }
                        }, error: function () {
                            layer.msg("删除失败！",{icon: 2});
                        }
                    });
                });
            }
        }
    });

    table.on('tool(reportTable)', function(obj) {
        var data = obj.data;
        if (obj.event === 'update') {
            if (userRole != 'root' && userRole != 'role10'){
                layer.msg("您没有权限！");
                return false;
            }
            var index = layer.open({
                type: 1 //Page层类型
                , title: "改数"
                , btn: ['保存']
                , shade: 0.6 //遮罩透明度
                , area: ['500px','300px']
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , content: "<div id=\"addPackData\">\n" +
                    "        <form class=\"layui-form\" style=\"margin: 0 auto;max-width:100%;padding-top: 10px;\">\n" +
                    "            <table>" +
                    "               <tr>" +
                    "                   <td style=\"text-align: right;margin-bottom: 15px;\">\n" +
                    "                        <label class=\"layui-form-label\" style=\"width: 100px\">原数量</label>\n" +
                    "                   </td>\n" +
                    "                   <td style=\"margin-bottom: 15px;\">\n" +
                    "                       <input type=\"text\" id=\"initCount\" readonly autocomplete=\"off\" class=\"layui-input\">\n" +
                    "                   </td>" +
                    "               </tr>" +
                    "               <tr>" +
                    "                   <td style=\"text-align: right;margin-bottom: 15px;\">\n" +
                    "                        <label class=\"layui-form-label\" style=\"width: 100px\">修改成</label>\n" +
                    "                   </td>\n" +
                    "                   <td style=\"margin-bottom: 15px;\">\n" +
                    "                       <input type=\"text\" id=\"toCount\" autocomplete=\"off\" class=\"layui-input\">\n" +
                    "                   </td>" +
                    "               </tr>" +
                    "           </table>\n" +
                    "        </form>\n" +
                    "    </div>"
                ,yes: function(i, layero){
                    var pieceWorkID = data.pieceWorkID;
                    var toCount = $("#toCount").val();
                    if (!(Number(toCount) >= 0)){
                        layer.msg("请输入正确数量！！！");
                        return false;
                    }
                    $.ajax({
                        url: basePath + "erp/updatepieceworklayercountfinance",
                        type:'POST',
                        data: {
                            pieceWorkID:pieceWorkID,
                            layerCount:toCount
                        },
                        success: function (res) {
                            if(res.result == 0) {
                                layer.close(index);
                                layer.msg("修改成功！",{icon: 1});
                                var orderName = $("#orderName").val();
                                var colorName = $("#colorName").val();
                                var sizeName = $("#sizeName").val();
                                var bedNumber = $("#bedNumber").val();
                                var packageNumber = $("#packageNumber").val();
                                var procedureNumber = $("#procedureNumber").val();
                                initTable(orderName, colorName, sizeName, bedNumber, packageNumber, procedureNumber);
                            } else {
                                layer.msg("修改失败！",{icon: 2});
                            }
                        }, error: function () {
                            layer.msg("网络异常！",{icon: 2});
                        }
                    });
                }
            });
            $("#initCount").val(data.layerCount);
        }
    });

    table.on('checkbox(reportTable)', function(obj){
        var packCount = 0;
        var layerCount = 0;
        var reportData = table.cache.reportTable;
        $.each(reportData, function (index, item) {
            if (item.LAY_CHECKED){
                packCount ++;
                layerCount += item.layerCount;
            }
        });
        var text = packCount + "扎/" + layerCount + "件";
        $("#checkInfo").val(text);
    });

});