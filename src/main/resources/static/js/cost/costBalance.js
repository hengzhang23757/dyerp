var form;
layui.use('form', function(){
    form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
    form.render();
});
var userRole = $("#userRole").val();
var userName = $("#userName").val();
layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.21'
}).extend({                         // 模块别名
    soulTable: 'soulTable'
});
$(document).ready(function () {
    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumber", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });
    $("#progressDetailDiv").html("<p style='font-size: large; color: #1E9FFF; font-weight: bolder'>详情</p>");
});
var reportTable,detailTable;
layui.use(['form', 'table', 'soulTable'], function () {
    var table = layui.table,
        soulTable = layui.soulTable,
        form = layui.form,
        $ = layui.$;

    reportTable = table.render({
        elem: '#reportTable'
        , cols: [[]]
        , loading: true
        , data: []
        , height: 'full-250'
        , title: '汇总表'
        , toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
        , totalRow: false
        , page: false
        , limit: 1000 //每页默认显示的数量
        , done: function (res, curr, count) {
            soulTable.render(this);
        }
    });

    detailTable = table.render({
        elem: '#detailTable'
        , cols: [[]]
        , loading: true
        , data: []
        , height: 'full-250'
        , toolbar: '#toolbarTopDetail' //开启头部工具栏，并为其绑定左侧模板
        , title: '报表'
        , page: false
        , limit: 1000 //每页默认显示的数量
        , done: function (res, curr, count) {
            soulTable.render(this);
        }
    });

    form.on('submit(searchBeat)', function (data) {
        var orderName = $("#orderName").val();
        var load = layer.load(2);
        var opType = $("#opType").val();
        if (opType === '下单'){
            $.ajax({
                url: "/erp/getcostbalancesummarybyinstore",
                type: 'GET',
                data: {
                    orderName: orderName
                },
                success:function(res){
                    if (res.costAccount) {
                        var reportData = res.costAccount;
                        var summaryList = [];
                        summaryList.push({itemName: '面料金额', costCount: toDecimal(reportData.fabricCost), endProductCount: reportData.endProductCount, orderCount: reportData.orderCount, averageCount: toDecimal(reportData.avgFabricCost)});
                        summaryList.push({itemName: '辅料金额', costCount: toDecimal(reportData.accessoryCost), endProductCount: reportData.endProductCount, orderCount: reportData.orderCount, averageCount: toDecimal(reportData.avgAccessoryCost)});
                        summaryList.push({itemName: '印绣花金额', costCount: toDecimal(reportData.printCost), endProductCount: reportData.endProductCount, orderCount: reportData.orderCount, averageCount: toDecimal(reportData.avgPrintCost)});
                        summaryList.push({itemName: '裁剪金额', costCount: toDecimal(reportData.cutCost), endProductCount: reportData.endProductCount, orderCount: reportData.orderCount, averageCount: toDecimal(reportData.avgCutCost)});
                        summaryList.push({itemName: '车缝后整金额', costCount: toDecimal(reportData.sewCost), endProductCount: reportData.endProductCount, orderCount: reportData.orderCount, averageCount: toDecimal(reportData.avgSewCost)});
                        detailTable = table.render({
                            elem: '#detailTable'
                            , cols: [[]]
                            , loading: true
                            , data: []
                            , height: 'full-250'
                            , title: '报表'
                            , page: false
                            , limit: 1000 //每页默认显示的数量
                            , done: function (res, curr, count) {
                                soulTable.render(this);
                            }
                        });
                        table.render({
                            elem: '#reportTable'
                            ,cols:[[
                                {field:'itemName', title:'名称', align:'center', width:'25%', sort: true, filter: true, totalRowText: '合计'}
                                ,{field:'costCount', title:'总价', align:'center', width:'20%', sort: true, filter: true, totalRow: true}
                                ,{field:'orderCount', title:'订单量', align:'center', width:'20%', sort: true, filter: true}
                                ,{field:'endProductCount', title:'成品量', align:'center', width:'15%', sort: true, filter: true}
                                ,{field:'averageCount', title:'件均', align:'center', width:'20%', sort: true, filter: true, totalRow: true}
                            ]]
                            ,excel: {
                                filename: '单款汇总.xlsx'
                            }
                            , toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                            , data: summaryList   // 将新数据重新载入表格
                            , height: 'full-250'
                            , title: '单款汇总表'
                            , totalRow: true
                            , page: false
                            , limit: 1000 //每页默认显示的数量
                            , overflow: 'tips'
                            ,done: function (res, curr, count) {
                                soulTable.render(this);
                                layer.close(load);
                            }
                        });
                    }
                }, error: function () {
                    layer.msg("获取数据失败", { icon: 2 });
                }
            });
        }
        else if (opType === '入库') {
            $.ajax({
                url: "/erp/getcostbalancesummarybyorder",
                type: 'GET',
                data: {
                    orderName: orderName
                },
                success:function(res){
                    if (res.costAccount) {
                        var reportData = res.costAccount;
                        var summaryList = [];
                        summaryList.push({itemName: '面料金额', costCount: toDecimal(reportData.fabricCost), endProductCount: reportData.endProductCount, orderCount: reportData.orderCount, averageCount: toDecimal(reportData.avgFabricCost)});
                        summaryList.push({itemName: '辅料金额', costCount: toDecimal(reportData.accessoryCost), endProductCount: reportData.endProductCount, orderCount: reportData.orderCount, averageCount: toDecimal(reportData.avgAccessoryCost)});
                        summaryList.push({itemName: '印绣花金额', costCount: toDecimal(reportData.printCost), endProductCount: reportData.endProductCount, orderCount: reportData.orderCount, averageCount: toDecimal(reportData.avgPrintCost)});
                        summaryList.push({itemName: '裁剪金额', costCount: toDecimal(reportData.cutCost), endProductCount: reportData.endProductCount, orderCount: reportData.orderCount, averageCount: toDecimal(reportData.avgCutCost)});
                        summaryList.push({itemName: '车缝后整金额', costCount: toDecimal(reportData.sewCost), endProductCount: reportData.endProductCount, orderCount: reportData.orderCount, averageCount: toDecimal(reportData.avgSewCost)});
                        detailTable = table.render({
                            elem: '#detailTable'
                            , cols: [[]]
                            , loading: true
                            , data: []
                            , height: 'full-250'
                            , title: '报表'
                            , page: false
                            , limit: 1000 //每页默认显示的数量
                            , done: function (res, curr, count) {
                                soulTable.render(this);
                            }
                        });
                        table.render({
                            elem: '#reportTable'
                            ,cols:[[
                                {field:'itemName', title:'名称', align:'center', width:'25%', sort: true, filter: true, totalRowText: '合计'}
                                ,{field:'costCount', title:'总价', align:'center', width:'20%', sort: true, filter: true, totalRow: true}
                                ,{field:'orderCount', title:'订单量', align:'center', width:'20%', sort: true, filter: true}
                                ,{field:'endProductCount', title:'成品量', align:'center', width:'15%', sort: true, filter: true}
                                ,{field:'averageCount', title:'件均', align:'center', width:'20%', sort: true, filter: true, totalRow: true}
                            ]]
                            ,excel: {
                                filename: '单款汇总.xlsx'
                            }
                            , toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                            , data: summaryList   // 将新数据重新载入表格
                            , height: 'full-250'
                            , title: '单款汇总表'
                            , totalRow: true
                            , page: false
                            , limit: 1000 //每页默认显示的数量
                            , overflow: 'tips'
                            ,done: function (res, curr, count) {
                                soulTable.render(this);
                                layer.close(load);
                            }
                        });
                    }
                }, error: function () {
                    layer.msg("获取数据失败", { icon: 2 });
                }
            });
        }
        else if (opType === '汇总'){
            $.ajax({
                url: "/erp/getCostBalanceOrderSummary",
                type: 'GET',
                data: {
                    orderName: orderName
                },
                success:function(res){
                    if (res.costAccount) {
                        var reportData = res.costAccount;
                        var summaryList = [];
                        summaryList.push({
                            itemName: '面料金额',
                            customerCount: toDecimal(reportData.fabricCustomerCost),
                            payCount: toDecimal(reportData.fabricCost),
                            returnCount: toDecimal(reportData.fabricReturnCost),
                            endProductCount: reportData.endProductCount,
                            orderCount: reportData.orderCount,
                            averageCount: toDecimal(reportData.avgFabricCost)
                        });
                        summaryList.push({
                            itemName: '辅料金额',
                            customerCount: toDecimal(reportData.accessoryCustomerCost),
                            payCount: toDecimal(reportData.accessoryCost),
                            returnCount: toDecimal(reportData.accessoryReturnCost),
                            endProductCount: reportData.endProductCount,
                            orderCount: reportData.orderCount,
                            averageCount: toDecimal(reportData.avgAccessoryCost)
                        });
                        summaryList.push({
                            itemName: '印绣花金额',
                            customerCount: toDecimal(reportData.printCost),
                            payCount: toDecimal(reportData.printCost),
                            returnCount: toDecimal(reportData.printCost),
                            endProductCount: reportData.endProductCount,
                            orderCount: reportData.orderCount,
                            averageCount: toDecimal(reportData.avgPrintCost)
                        });
                        summaryList.push({
                            itemName: '裁剪金额',
                            customerCount: toDecimal(reportData.cutCost),
                            payCount: toDecimal(reportData.cutCost),
                            returnCount: toDecimal(reportData.cutCost),
                            endProductCount: reportData.endProductCount,
                            orderCount: reportData.orderCount,
                            averageCount: toDecimal(reportData.avgCutCost)
                        });
                        summaryList.push({
                            itemName: '车缝后整金额',
                            customerCount: toDecimal(reportData.sewCost),
                            payCount: toDecimal(reportData.sewCost),
                            returnCount: toDecimal(reportData.sewCost),
                            endProductCount: reportData.endProductCount,
                            orderCount: reportData.orderCount,
                            averageCount: toDecimal(reportData.avgSewCost)
                        });
                        detailTable = table.render({
                            elem: '#detailTable'
                            , cols: [[]]
                            , loading: true
                            , data: []
                            , height: 'full-250'
                            , title: '报表'
                            , page: false
                            , limit: 1000 //每页默认显示的数量
                            , done: function (res, curr, count) {
                                soulTable.render(this);
                            }
                        });
                        table.render({
                            elem: '#reportTable'
                            ,cols:[[
                                {field:'itemName', title:'名称', align:'center', width:'25%', sort: true, filter: true, totalRowText: '合计'}
                                ,{field:'customerCount', title:'客供总价', align:'center', width:'20%', sort: true, filter: true, totalRow: true}
                                ,{field:'payCount', title:'下单总价', align:'center', width:'20%', sort: true, filter: true, totalRow: true}
                                ,{field:'returnCount', title:'回料总价', align:'center', width:'20%', sort: true, filter: true, totalRow: true}
                                ,{field:'orderCount', title:'订单量', align:'center', width:'20%', sort: true, filter: true}
                                ,{field:'endProductCount', title:'成品量', align:'center', width:'15%', sort: true, filter: true}
                                ,{field:'averageCount', title:'件均', align:'center', width:'20%', sort: true, filter: true, totalRow: true}
                            ]]
                            ,excel: {
                                filename: '单款汇总.xlsx'
                            }
                            , toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                            , data: summaryList   // 将新数据重新载入表格
                            , height: 'full-250'
                            , title: '单款汇总表'
                            , totalRow: true
                            , page: false
                            , limit: 1000 //每页默认显示的数量
                            , overflow: 'tips'
                            ,done: function (res, curr, count) {
                                soulTable.render(this);
                                layer.close(load);
                            }
                        });
                    }
                }, error: function () {
                    layer.msg("获取数据失败", { icon: 2 });
                }
            });
        }
        return false;
    });

    table.on('toolbar(reportTable)', function(obj) {
        if (obj.event === 'refresh') {
            reportTable.reload();
        }
    });


    table.on('row(reportTable)', function(obj){
        var data = obj.data;
        var itemName = data.itemName;
        var opType = $("#opType").val();
        //标注选中样式
        $('div[lay-id="reportTable"]').find(".layui-table-body tr ").attr({ "style": "background:#FFFFFF" });//其他tr恢复原样
        $('div[lay-id="reportTable"]').find(obj.tr.selector).attr({ "style": "background:#5FB878" });//改变当前tr颜色
        var orderName = $("#orderName").val();
        var load = layer.load(2);
        if (opType === '下单'){
            $.ajax({
                url: "/erp/getcostdetailbyorderitemfororder",
                type: 'GET',
                data: {
                    orderName: orderName,
                    itemName: itemName
                },
                success:function(res){
                    if (res.data) {
                        var detailData = res.data;
                        if (itemName === '面料金额') {
                            table.render({
                                elem: '#detailTable'
                                ,cols: [[
                                    {field:'fabricName', title:'名称', align:'center', width:'16%', sort: true, filter: true, totalRowText: '合计'}
                                    ,{field:'fabricNumber', title:'编号', align:'center', width:'15%', sort: true, filter: true}
                                    ,{field:'colorName', title:'色组', align:'center', width:'15%', sort: true, filter: true}
                                    ,{field:'fabricColor', title:'面料颜色', align:'center', width:'15%', sort: true, filter: true}
                                    ,{field:'price', title:'单价', align:'center', width:'15%', sort: true, filter: true}
                                    ,{field:'unit', title:'单位', align:'center', width:'15%', sort: true, filter: true}
                                    ,{field:'fabricActCount', title:'数量', align:'center', width:'15%', sort: true, filter: true, totalRow: true}
                                    ,{field:'sumMoney', title:'金额', align:'center', width:'15%', sort: true, filter: true, totalRow: true}
                                    ,{field:'addFee', title:'附加', align:'center', width:'15%', sort: true, filter: true, totalRow: true}
                                    ,{field:'remark', title:'备注', align:'center', width:'15%', sort: true, filter: true}
                                ]]
                                , data: detailData   // 将新数据重新载入表格
                                , overflow: 'tips'
                                , loading: true
                                , totalRow: true
                                , height: 'full-250'
                                , toolbar: '#toolbarTopDetail' //开启头部工具栏，并为其绑定左侧模板
                                , title: '报表'
                                , page: false
                                , limit: 1000 //每页默认显示的数量
                                ,done: function (res, curr, count) {
                                    soulTable.render(this);
                                    layer.close(load);
                                }
                            });
                            $("#progressDetailDiv").html("<p style='font-size: large; color: #1E9FFF; font-weight: bolder'>面料金额详情</p>");
                        }
                        else if (itemName === '辅料金额'){
                            table.render({
                                elem: '#detailTable'
                                ,cols: [[
                                    {field:'accessoryName', title:'名称', align:'center', width:'16%', sort: true, filter: true, totalRowText: '合计'}
                                    ,{field:'accessoryNumber', title:'编号', align:'center', width:'15%', sort: true, filter: true}
                                    ,{field:'specification', title:'规格', align:'center', width:'15%', sort: true, filter: true}
                                    ,{field:'accessoryColor', title:'颜色', align:'center', width:'15%', sort: true, filter: true}
                                    ,{field:'colorName', title:'色组', align:'center', width:'15%', sort: true, filter: true}
                                    ,{field:'sizeName', title:'尺码', align:'center', width:'15%', sort: true, filter: true}
                                    ,{field:'orderCount', title:'订单量', align:'center', width:'15%', sort: true, filter: true}
                                    ,{field:'accessoryUnit', title:'单位', align:'center', width:'15%', sort: true, filter: true}
                                    ,{field:'accessoryCount', title:'数量', align:'center', width:'15%', sort: true, filter: true, totalRow: true}
                                    ,{field:'orderPrice', title:'客供单价', align:'center', width:'15%', sort: true, filter: true}
                                    ,{field:'price', title:'单价', align:'center', width:'15%', sort: true, filter: true}
                                    ,{field:'sumMoney', title:'金额', align:'center', width:'15%', sort: true, filter: true, totalRow: true}
                                    ,{field:'addFee', title:'附加', align:'center', width:'15%', sort: true, filter: true, totalRow: true}
                                    ,{field:'addRemark', title:'备注', align:'center', width:'15%', sort: true, filter: true}
                                ]]
                                , data: detailData   // 将新数据重新载入表格
                                , overflow: 'tips'
                                , loading: true
                                , totalRow: true
                                , height: 'full-250'
                                , toolbar: '#toolbarTopDetail' //开启头部工具栏，并为其绑定左侧模板
                                , title: '报表'
                                , page: false
                                , limit: 1000 //每页默认显示的数量
                                ,done: function (res, curr, count) {
                                    soulTable.render(this);
                                    layer.close(load);
                                }
                            });
                            $("#progressDetailDiv").html("<p style='font-size: large; color: #1E9FFF; font-weight: bolder'>辅料金额详情</p>");
                        }
                        else if (itemName === '印绣花金额'){
                            table.render({
                                elem: '#detailTable'
                                ,cols: [[
                                    {field:'colorName', title:'颜色', align:'center', width:'16%', sort: true, filter: true, totalRowText: '合计'}
                                    ,{field:'orderCount', title:'数量', align:'center', width:'20%', sort: true, filter: true}
                                    ,{field:'processOneFactory', title:'印花工厂', align:'center', width:'20%', sort: true, filter: true}
                                    ,{field:'processOnePrice', title:'印花单价', align:'center', width:'20%', sort: true, filter: true}
                                    ,{field:'oneTax', title:'是否含税', align:'center', width:'20%', sort: true, filter: true}
                                    ,{field:'processTwoFactory', title:'绣花工厂', align:'center', width:'20%', sort: true, filter: true}
                                    ,{field:'processTwoPrice', title:'绣花单价', align:'center', width:'20%', sort: true, filter: true}
                                    ,{field:'twoTax', title:'是否含税', align:'center', width:'20%', sort: true, filter: true}
                                    ,{field:'processThreeFactory', title:'压胶工厂', align:'center', width:'20%', sort: true, filter: true}
                                    ,{field:'processThreePrice', title:'压胶单价', align:'center', width:'20%', sort: true, filter: true}
                                    ,{field:'threeTax', title:'是否含税', align:'center', width:'20%', sort: true, filter: true}
                                    ,{field:'processFourFactory', title:'压标工厂', align:'center', width:'20%', sort: true, filter: true}
                                    ,{field:'processFourPrice', title:'压标单价', align:'center', width:'20%', sort: true, filter: true}
                                    ,{field:'fourTax', title:'是否含税', align:'center', width:'20%', sort: true, filter: true}
                                    ,{field:'sumMoney', title:'总金额', align:'center', width:'20%', sort: true, filter: true, totalRow: true}
                                ]]
                                , data: detailData   // 将新数据重新载入表格
                                , overflow: 'tips'
                                , loading: true
                                , totalRow: true
                                , height: 'full-250'
                                , toolbar: '#toolbarTopDetail' //开启头部工具栏，并为其绑定左侧模板
                                , title: '报表'
                                , page: false
                                , limit: 1000 //每页默认显示的数量
                                ,done: function (res, curr, count) {
                                    soulTable.render(this);
                                    layer.close(load);
                                }
                            });
                            $("#progressDetailDiv").html("<p style='font-size: large; color: #1E9FFF; font-weight: bolder'>印绣花金额详情</p>");
                        }
                        else if (itemName === '裁剪金额'){
                            table.render({
                                elem: '#detailTable'
                                ,cols: [[
                                    {field:'cutCount', title:'数量', align:'center', width:'20%', sort: true, filter: true}
                                    ,{field:'cutPrice', title:'裁剪单价', align:'center', width:'20%', sort: true, filter: true}
                                    ,{field:'sumMoney', title:'总金额', align:'center', width:'20%', sort: true, filter: true, totalRow: true}
                                ]]
                                , data: detailData   // 将新数据重新载入表格
                                , overflow: 'tips'
                                , loading: true
                                , totalRow: true
                                , height: 'full-250'
                                , toolbar: '#toolbarTopDetail' //开启头部工具栏，并为其绑定左侧模板
                                , title: '报表'
                                , page: false
                                , limit: 1000 //每页默认显示的数量
                                ,done: function (res, curr, count) {
                                    soulTable.render(this);
                                    layer.close(load);
                                }
                            });
                            $("#progressDetailDiv").html("<p style='font-size: large; color: #1E9FFF; font-weight: bolder'>裁剪金额详情</p>");
                        }
                        else if (itemName === '车缝后整金额'){
                            table.render({
                                elem: '#detailTable'
                                ,cols: [[
                                    {field:'procedureName', title:'工序名', align:'center', width:'15%', sort: true, filter: true}
                                    ,{field:'procedureNumber', title:'工序号', align:'center', width:'15%', sort: true, filter: true}
                                    ,{field:'pieceCount', title:'数量', align:'center', width:'15%', sort: true, filter: true, totalRow: true}
                                    ,{field:'price', title:'单价', align:'center', width:'15%', sort: true, filter: true, totalRow: true}
                                    ,{field:'addition', title:'补贴比例(%)', align:'center',width:120, sort: true, filter: true,templet: function (d) {
                                            return d.addition * 100;
                                        },excel: {cellType: 'n'}}
                                    ,{field:'sumPrice', title:'总单价', align:'center', width:100, sort: true, filter: true, totalRow: true,templet: function (d) {
                                            return toDecimalFour(d.sumPrice);
                                        },excel: {cellType: 'n'}}
                                    ,{field:'salarySum', title:'工序总工资', align:'center', width:150, sort: true, filter: true, totalRow: true,templet: function (d) {
                                            return toDecimal(d.salarySum);
                                        },excel: {cellType: 'n'}}
                                ]]
                                , data: detailData   // 将新数据重新载入表格
                                , overflow: 'tips'
                                , loading: true
                                , totalRow: true
                                , toolbar: '#toolbarTopDetail' //开启头部工具栏，并为其绑定左侧模板
                                , height: 'full-250'
                                , title: '报表'
                                , page: false
                                , limit: 1000 //每页默认显示的数量
                                ,done: function (res, curr, count) {
                                    soulTable.render(this);
                                    layer.close(load);
                                }
                            });
                            $("#progressDetailDiv").html("<p style='font-size: large; color: #1E9FFF; font-weight: bolder'>车缝后整详情</p>");
                        }
                    }
                }, error: function () {
                    layer.msg("获取数据失败", { icon: 2 });
                }
            });
        }
        else if (opType === '入库') {
            $.ajax({
                url: "/erp/getcostdetailbyorderitem",
                type: 'GET',
                data: {
                    orderName: orderName,
                    itemName: itemName
                },
                success:function(res){
                    if (res.data) {
                        var detailData = res.data;
                        if (itemName === '面料金额') {
                            table.render({
                                elem: '#detailTable'
                                ,cols: [[
                                    {field:'fabricName', title:'名称', align:'center', width:'16%', sort: true, filter: true, totalRowText: '合计'}
                                    ,{field:'fabricNumber', title:'编号', align:'center', width:'20%', sort: true, filter: true}
                                    ,{field:'colorName', title:'色组', align:'center', width:'20%', sort: true, filter: true}
                                    ,{field:'fabricColor', title:'面料颜色', align:'center', width:'20%', sort: true, filter: true}
                                    ,{field:'jarName', title:'缸号', align:'center', width:'20%', sort: true, filter: true}
                                    ,{field:'price', title:'单价', align:'center', width:'20%', sort: true, filter: true}
                                    ,{field:'unit', title:'单位', align:'center', width:'20%', sort: true, filter: true}
                                    ,{field:'batchNumber', title:'卷数', align:'center', width:'20%', sort: true, filter: true, totalRow: true}
                                    ,{field:'weight', title:'数量', align:'center', width:'20%', sort: true, filter: true, totalRow: true}
                                    ,{field:'returnMoney', title:'金额', align:'center', width:'20%', sort: true, filter: true, totalRow: true}
                                ]]
                                , data: detailData   // 将新数据重新载入表格
                                , overflow: 'tips'
                                , loading: true
                                , totalRow: true
                                , height: 'full-250'
                                , toolbar: '#toolbarTopDetail' //开启头部工具栏，并为其绑定左侧模板
                                , title: '报表'
                                , page: false
                                , limit: 1000 //每页默认显示的数量
                                ,done: function (res, curr, count) {
                                    soulTable.render(this);
                                    layer.close(load);
                                }
                            });
                            $("#progressDetailDiv").html("<p style='font-size: large; color: #1E9FFF; font-weight: bolder'>面料金额详情</p>");
                        } else if (itemName === '辅料金额'){
                            table.render({
                                elem: '#detailTable'
                                ,cols: [[
                                    {field:'accessoryName', title:'名称', align:'center', width:'16%', sort: true, filter: true, totalRowText: '合计'}
                                    ,{field:'accessoryNumber', title:'编号', align:'center', width:'20%', sort: true, filter: true}
                                    ,{field:'specification', title:'规格', align:'center', width:'20%', sort: true, filter: true}
                                    ,{field:'accessoryColor', title:'颜色', align:'center', width:'20%', sort: true, filter: true}
                                    ,{field:'colorName', title:'色组', align:'center', width:'20%', sort: true, filter: true}
                                    ,{field:'sizeName', title:'尺码', align:'center', width:'20%', sort: true, filter: true}
                                    ,{field:'accessoryUnit', title:'单位', align:'center', width:'20%', sort: true, filter: true}
                                    ,{field:'accountCount', title:'数量', align:'center', width:'20%', sort: true, filter: true, totalRow: true}
                                    ,{field:'price', title:'单价', align:'center', width:'20%', sort: true, filter: true}
                                    ,{field:'returnMoney', title:'金额', align:'center', width:'20%', sort: true, filter: true, totalRow: true}
                                ]]
                                , data: detailData   // 将新数据重新载入表格
                                , overflow: 'tips'
                                , loading: true
                                , totalRow: true
                                , height: 'full-250'
                                , toolbar: '#toolbarTopDetail' //开启头部工具栏，并为其绑定左侧模板
                                , title: '报表'
                                , page: false
                                , limit: 1000 //每页默认显示的数量
                                ,done: function (res, curr, count) {
                                    soulTable.render(this);
                                    layer.close(load);
                                }
                            });
                            $("#progressDetailDiv").html("<p style='font-size: large; color: #1E9FFF; font-weight: bolder'>辅料金额详情</p>");
                        } else if (itemName === '印绣花金额'){
                            table.render({
                                elem: '#detailTable'
                                ,cols: [[
                                    {field:'colorName', title:'颜色', align:'center', width:'16%', sort: true, filter: true, totalRowText: '合计'}
                                    ,{field:'orderCount', title:'数量', align:'center', width:'20%', sort: true, filter: true}
                                    ,{field:'processOneFactory', title:'印花工厂', align:'center', width:'20%', sort: true, filter: true}
                                    ,{field:'processOnePrice', title:'印花单价', align:'center', width:'20%', sort: true, filter: true}
                                    ,{field:'oneTax', title:'是否含税', align:'center', width:'20%', sort: true, filter: true}
                                    ,{field:'processTwoFactory', title:'绣花工厂', align:'center', width:'20%', sort: true, filter: true}
                                    ,{field:'processTwoPrice', title:'绣花单价', align:'center', width:'20%', sort: true, filter: true}
                                    ,{field:'twoTax', title:'是否含税', align:'center', width:'20%', sort: true, filter: true}
                                    ,{field:'processThreeFactory', title:'压胶工厂', align:'center', width:'20%', sort: true, filter: true}
                                    ,{field:'processThreePrice', title:'压胶单价', align:'center', width:'20%', sort: true, filter: true}
                                    ,{field:'threeTax', title:'是否含税', align:'center', width:'20%', sort: true, filter: true}
                                    ,{field:'processFourFactory', title:'压标工厂', align:'center', width:'20%', sort: true, filter: true}
                                    ,{field:'processFourPrice', title:'压标单价', align:'center', width:'20%', sort: true, filter: true}
                                    ,{field:'fourTax', title:'是否含税', align:'center', width:'20%', sort: true, filter: true}
                                    ,{field:'sumMoney', title:'总金额', align:'center', width:'20%', sort: true, filter: true, totalRow: true}
                                ]]
                                , data: detailData   // 将新数据重新载入表格
                                , overflow: 'tips'
                                , loading: true
                                , totalRow: true
                                , height: 'full-250'
                                , toolbar: '#toolbarTopDetail' //开启头部工具栏，并为其绑定左侧模板
                                , title: '报表'
                                , page: false
                                , limit: 1000 //每页默认显示的数量
                                ,done: function (res, curr, count) {
                                    soulTable.render(this);
                                    layer.close(load);
                                }
                            });
                            $("#progressDetailDiv").html("<p style='font-size: large; color: #1E9FFF; font-weight: bolder'>印绣花金额详情</p>");
                        } else if (itemName === '裁剪金额'){
                            table.render({
                                elem: '#detailTable'
                                ,cols: [[
                                    {field:'cutCount', title:'数量', align:'center', width:'20%', sort: true, filter: true}
                                    ,{field:'cutPrice', title:'裁剪单价', align:'center', width:'20%', sort: true, filter: true}
                                    ,{field:'sumMoney', title:'总金额', align:'center', width:'20%', sort: true, filter: true, totalRow: true}
                                ]]
                                , data: detailData   // 将新数据重新载入表格
                                , overflow: 'tips'
                                , loading: true
                                , totalRow: true
                                , height: 'full-250'
                                , toolbar: '#toolbarTopDetail' //开启头部工具栏，并为其绑定左侧模板
                                , title: '报表'
                                , page: false
                                , limit: 1000 //每页默认显示的数量
                                ,done: function (res, curr, count) {
                                    soulTable.render(this);
                                    layer.close(load);
                                }
                            });
                            $("#progressDetailDiv").html("<p style='font-size: large; color: #1E9FFF; font-weight: bolder'>裁剪金额详情</p>");
                        } else if (itemName === '车缝后整金额'){
                            table.render({
                                elem: '#detailTable'
                                ,cols: [[
                                    {field:'procedureName', title:'工序名', align:'center', width:'15%', sort: true, filter: true}
                                    ,{field:'procedureNumber', title:'工序号', align:'center', width:'15%', sort: true, filter: true}
                                    ,{field:'pieceCount', title:'数量', align:'center', width:'15%', sort: true, filter: true, totalRow: true}
                                    ,{field:'price', title:'单价', align:'center', width:'15%', sort: true, filter: true, totalRow: true}
                                    ,{field:'priceTwo', title:'上浮', align:'center', width:'15%', sort: true, filter: true, totalRow: true}
                                    ,{field:'salarySum', title:'工资', align:'center', width:'15%', sort: true, filter: true, totalRow: true}
                                ]]
                                , data: detailData   // 将新数据重新载入表格
                                , overflow: 'tips'
                                , loading: true
                                , totalRow: true
                                , toolbar: '#toolbarTopDetail' //开启头部工具栏，并为其绑定左侧模板
                                , height: 'full-250'
                                , title: '报表'
                                , page: false
                                , limit: 1000 //每页默认显示的数量
                                ,done: function (res, curr, count) {
                                    soulTable.render(this);
                                    layer.close(load);
                                }
                            });
                            $("#progressDetailDiv").html("<p style='font-size: large; color: #1E9FFF; font-weight: bolder'>车缝后整详情</p>");
                        }
                    }
                }, error: function () {
                    layer.msg("获取数据失败", { icon: 2 });
                }
            });
        }
        else if (opType === '汇总') {
            $.ajax({
                url: "/erp/getCostDetailBySummaryItem",
                type: 'GET',
                data: {
                    orderName: orderName,
                    itemName: itemName
                },
                success:function(res){
                    if (res.data) {
                        var detailData = res.data;
                        if (itemName === '面料金额') {
                            $.each(detailData, function (d_index, d_item){
                                d_item.orderFabricCount = toDecimal(d_item.orderCount * d_item.orderPieceUsage);
                                d_item.orderSumMoney = toDecimal(d_item.orderFabricCount * d_item.orderPrice);
                                d_item.sumMoney = toDecimal(d_item.sumMoney + d_item.addFee);
                                d_item.returnSumMoney = toDecimal(d_item.returnSumMoney + d_item.addFee);
                                d_item.diff1 = toDecimal(d_item.orderSumMoney - d_item.sumMoney);
                                d_item.diff2 = toDecimal(d_item.sumMoney - d_item.returnSumMoney);
                                d_item.diff3 = toDecimal(d_item.orderSumMoney - d_item.returnSumMoney);
                            });
                            table.render({
                                elem: '#detailTable'
                                ,cols: [[
                                    {field:'partName', title:'部位', align:'center', minWidth:120, sort: true, filter: true}
                                    ,{field:'fabricName', title:'名称', align:'center', minWidth:200, sort: true, filter: true, totalRowText: '合计'}
                                    ,{field:'unit', title:'单位', align:'center', minWidth:120, sort: true, filter: true}
                                    ,{field:'orderPieceUsage', title:'接单用量', align:'center', minWidth:120, sort: true, filter: true}
                                    ,{field:'orderPrice', title:'接单单价', align:'center', minWidth:120, sort: true, filter: true}
                                    ,{field:'orderFabricCount', title:'接单订购量', align:'center', minWidth:120, sort: true, filter: true, totalRow: true}
                                    ,{field:'orderSumMoney', title:'接单成本', align:'center', minWidth:120, sort: true, filter: true, totalRow: true}
                                    ,{field:'pieceUsage', title:'采购单耗', align:'center', minWidth:120, sort: true, filter: true}
                                    ,{field:'fabricActCount', title:'采购量', align:'center', minWidth:120, sort: true, filter: true, totalRow: true}
                                    ,{field:'price', title:'单价', align:'center', minWidth:120, sort: true, filter: true}
                                    ,{field:'addFee', title:'附加费', align:'center', minWidth:120, sort: true, filter: true}
                                    ,{field:'sumMoney', title:'采购金额', align:'center', minWidth:120, sort: true, filter: true, totalRow: true}
                                    ,{field:'fabricReturn', title:'实际回布', align:'center', minWidth:120, sort: true, filter: true, totalRow: true}
                                    ,{field:'returnSumMoney', title:'回布金额', align:'center', minWidth:120, sort: true, filter: true, totalRow: true}
                                    ,{field:'diff1', title:'接单与采购差异', align:'center', minWidth:120, sort: true, filter: true, totalRow: true}
                                    ,{field:'diff2', title:'采购与实际差异', align:'center', minWidth:120, sort: true, filter: true, totalRow: true}
                                    ,{field:'diff3', title:'接单与实际差异', align:'center', minWidth:120, sort: true, filter: true, totalRow: true}
                                ]]
                                , data: detailData   // 将新数据重新载入表格
                                , overflow: 'tips'
                                , loading: true
                                , totalRow: true
                                , height: 'full-250'
                                , toolbar: '#toolbarTopDetail' //开启头部工具栏，并为其绑定左侧模板
                                , title: '报表'
                                , page: false
                                , limit: 1000 //每页默认显示的数量
                                ,done: function (res, curr, count) {
                                    soulTable.render(this);
                                    layer.close(load);
                                }
                            });
                            $("#progressDetailDiv").html("<p style='font-size: large; color: #1E9FFF; font-weight: bolder'>面料金额详情</p>");
                        }
                        else if (itemName === '辅料金额'){
                            $.each(detailData, function (d_index, d_item){
                                d_item.diff = toDecimal( d_item.orderSumMoney - d_item.sumMoney - d_item.addFee);
                                d_item.sumTotalMoney = toDecimal(d_item.sumMoney + d_item.addFee);
                            });
                            table.render({
                                elem: '#detailTable'
                                ,cols: [[
                                    {field:'accessoryName', title:'名称', align:'center', width:'16%', sort: true, filter: true, totalRowText: '合计'}
                                    ,{field:'sumMoney', title:'采购金额', align:'center', width:'15%', sort: true, filter: true, totalRow: true}
                                    ,{field:'addFee', title:'附加费', align:'center', width:'15%', sort: true, filter: true, totalRow: true}
                                    ,{field:'sumTotalMoney', title:'采购总金额', align:'center', width:'15%', sort: true, filter: true, totalRow: true}
                                    ,{field:'orderSumMoney', title:'客供金额', align:'center', width:'15%', sort: true, filter: true, totalRow: true}
                                    ,{field:'diff', title:'差异', align:'center', width:'10%', sort: true, filter: true, totalRow: true}
                                ]]
                                , data: detailData   // 将新数据重新载入表格
                                , overflow: 'tips'
                                , loading: true
                                , totalRow: true
                                , height: 'full-250'
                                , toolbar: '#toolbarTopDetail' //开启头部工具栏，并为其绑定左侧模板
                                , title: '报表'
                                , page: false
                                , limit: 1000 //每页默认显示的数量
                                ,done: function (res, curr, count) {
                                    soulTable.render(this);
                                    layer.close(load);
                                }
                            });
                            $("#progressDetailDiv").html("<p style='font-size: large; color: #1E9FFF; font-weight: bolder'>辅料金额详情</p>");
                        }
                        else if (itemName === '印绣花金额'){
                            table.render({
                                elem: '#detailTable'
                                ,cols: [[
                                    {field:'colorName', title:'颜色', align:'center', width:'16%', sort: true, filter: true, totalRowText: '合计'}
                                    ,{field:'orderCount', title:'数量', align:'center', width:'20%', sort: true, filter: true}
                                    ,{field:'processOneFactory', title:'印花工厂', align:'center', width:'20%', sort: true, filter: true}
                                    ,{field:'processOnePrice', title:'印花单价', align:'center', width:'20%', sort: true, filter: true}
                                    ,{field:'oneTax', title:'是否含税', align:'center', width:'20%', sort: true, filter: true}
                                    ,{field:'processTwoFactory', title:'绣花工厂', align:'center', width:'20%', sort: true, filter: true}
                                    ,{field:'processTwoPrice', title:'绣花单价', align:'center', width:'20%', sort: true, filter: true}
                                    ,{field:'twoTax', title:'是否含税', align:'center', width:'20%', sort: true, filter: true}
                                    ,{field:'processThreeFactory', title:'压胶工厂', align:'center', width:'20%', sort: true, filter: true}
                                    ,{field:'processThreePrice', title:'压胶单价', align:'center', width:'20%', sort: true, filter: true}
                                    ,{field:'threeTax', title:'是否含税', align:'center', width:'20%', sort: true, filter: true}
                                    ,{field:'processFourFactory', title:'压标工厂', align:'center', width:'20%', sort: true, filter: true}
                                    ,{field:'processFourPrice', title:'压标单价', align:'center', width:'20%', sort: true, filter: true}
                                    ,{field:'fourTax', title:'是否含税', align:'center', width:'20%', sort: true, filter: true}
                                    ,{field:'sumMoney', title:'总金额', align:'center', width:'20%', sort: true, filter: true, totalRow: true}
                                ]]
                                , data: detailData   // 将新数据重新载入表格
                                , overflow: 'tips'
                                , loading: true
                                , totalRow: true
                                , height: 'full-250'
                                , toolbar: '#toolbarTopDetail' //开启头部工具栏，并为其绑定左侧模板
                                , title: '报表'
                                , page: false
                                , limit: 1000 //每页默认显示的数量
                                ,done: function (res, curr, count) {
                                    soulTable.render(this);
                                    layer.close(load);
                                }
                            });
                            $("#progressDetailDiv").html("<p style='font-size: large; color: #1E9FFF; font-weight: bolder'>印绣花金额详情</p>");
                        }
                        else if (itemName === '裁剪金额'){
                            table.render({
                                elem: '#detailTable'
                                ,cols: [[
                                    {field:'cutCount', title:'数量', align:'center', width:'20%', sort: true, filter: true}
                                    ,{field:'cutPrice', title:'裁剪单价', align:'center', width:'20%', sort: true, filter: true}
                                    ,{field:'sumMoney', title:'总金额', align:'center', width:'20%', sort: true, filter: true, totalRow: true}
                                ]]
                                , data: detailData   // 将新数据重新载入表格
                                , overflow: 'tips'
                                , loading: true
                                , totalRow: true
                                , height: 'full-250'
                                , toolbar: '#toolbarTopDetail' //开启头部工具栏，并为其绑定左侧模板
                                , title: '报表'
                                , page: false
                                , limit: 1000 //每页默认显示的数量
                                ,done: function (res, curr, count) {
                                    soulTable.render(this);
                                    layer.close(load);
                                }
                            });
                            $("#progressDetailDiv").html("<p style='font-size: large; color: #1E9FFF; font-weight: bolder'>裁剪金额详情</p>");
                        }
                        else if (itemName === '车缝后整金额'){
                            table.render({
                                elem: '#detailTable'
                                ,cols: [[
                                    {field:'procedureName', title:'工序名', align:'center', width:'15%', sort: true, filter: true}
                                    ,{field:'procedureNumber', title:'工序号', align:'center', width:'15%', sort: true, filter: true}
                                    ,{field:'pieceCount', title:'数量', align:'center', width:'15%', sort: true, filter: true, totalRow: true}
                                    ,{field:'price', title:'单价', align:'center', width:'15%', sort: true, filter: true, totalRow: true}
                                    ,{field:'priceTwo', title:'上浮', align:'center', width:'15%', sort: true, filter: true, totalRow: true}
                                    ,{field:'salarySum', title:'工资', align:'center', width:'15%', sort: true, filter: true, totalRow: true}
                                ]]
                                , data: detailData   // 将新数据重新载入表格
                                , overflow: 'tips'
                                , loading: true
                                , totalRow: true
                                , toolbar: '#toolbarTopDetail' //开启头部工具栏，并为其绑定左侧模板
                                , height: 'full-250'
                                , title: '报表'
                                , page: false
                                , limit: 1000 //每页默认显示的数量
                                ,done: function (res, curr, count) {
                                    soulTable.render(this);
                                    layer.close(load);
                                }
                            });
                            $("#progressDetailDiv").html("<p style='font-size: large; color: #1E9FFF; font-weight: bolder'>车缝后整详情</p>");
                        }
                    }
                }, error: function () {
                    layer.msg("获取数据失败", { icon: 2 });
                }
            });
        }
        return false;
    });


    table.on('toolbar(detailTable)', function(obj) {
        if (obj.event === 'export') {
            soulTable.export('detailTable');
        }
    });
});

function toDecimal(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*100)/100;
    return f;
}

function toDecimalFour(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*10000)/10000;
    return f;
}