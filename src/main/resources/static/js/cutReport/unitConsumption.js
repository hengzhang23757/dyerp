layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.21'
}).extend();
$(document).ready(function () {
    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumber", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });

    $("#orderName").bind('blur change',function(){
        var orderName = $("#orderName").val();
        $.ajax({
            url: "/erp/getordercolornamesbyorder",
            data: {"orderName": orderName},
            success:function(data){
                $("#colorName").empty();
                if (data.colorNameList) {
                    $("#colorName").append("<option value=''>选择颜色</option>");
                    $.each(data.colorNameList, function(index,element){
                        $("#colorName").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });
        $.ajax({
            url: "/erp/getotherprintpartnamesbyorder",
            data: {"orderName": orderName},
            success:function(data){
                $("#partName").empty();
                if (data.printPartNameList) {
                    $("#partName").append("<option value='主身布'>主身布</option>");
                    $.each(data.printPartNameList, function(index,element){
                        $("#partName").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });
    });

    $("#clothesVersionNumber").bind('blur change',function(){
        var orderName = $("#orderName").val();
        $.ajax({
            url: "/erp/getordercolornamesbyorder",
            data: {"orderName": orderName},
            success:function(data){
                $("#colorName").empty();
                if (data.colorNameList) {
                    $("#colorName").append("<option value=''>选择颜色</option>");
                    $.each(data.colorNameList, function(index,element){
                        $("#colorName").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });
        $.ajax({
            url: "/erp/getotherprintpartnamesbyorder",
            data: {"orderName": orderName},
            success:function(data){
                $("#partName").empty();
                if (data.printPartNameList) {
                    $("#partName").append("<option value='主身布'>主身布</option>");
                    $.each(data.printPartNameList, function(index,element){
                        $("#partName").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });
    });

});
var hot;
var basePath=$("#basePath").val();
function search() {
    var orderName = $("#orderName").val();
    if (orderName == null || orderName == ""){
        swal({
            type:"warning",
            title:"",
            text: "<span style=\"font-weight:bolder;font-size: 20px\">订单号不能为空！</span>",
            html: true
        });
    }
    var data = {};
    data.orderName = orderName;
    if ($("#colorName").val() != null && $("#colorName").val() != ""){
        data.colorName = $("#colorName").val();
    }
    $.blockUI({ css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        },
        message: "<h3>稍等一下... 奋力搜索中</h3>"
    });
    $("#entities").empty();
    var searchType = $("#partName").val();
    if (searchType == "主身布"){
        $.ajax({
            url: basePath+"erp/unitconsumptionreport",
            type:'GET',
            data: data,
            success: function (data) {
                $("#exportDiv").show();
                var pieceConsumptionGreen = [];
                var pieceConsumptionRed = [];
                var hotData = [];
                $("#reportExcel").empty();
                var container = document.getElementById('reportExcel');
                var colorRendererGreen = function (instance, td, row, col, prop, value, cellProperties) {
                    Handsontable.renderers.TextRenderer.apply(this, arguments);
                    td.style.backgroundColor = '#2BC08B';
                };
                var colorRendererRed = function (instance, td, row, col, prop, value, cellProperties) {
                    Handsontable.renderers.TextRenderer.apply(this, arguments);
                    td.style.backgroundColor = '#FF4500';
                };

                if(data) {

                    /***
                     * 头部单耗汇总
                     */
                    if (data["summary"]){
                        var summaryData = data["summary"];
                        var partRow = [["部位名称:"],["主身布"]];
                        hotData.push(partRow);
                        $.each(summaryData, function(index,element){
                            if (element.averageConsumption > element.planConsumption){
                                pieceConsumptionRed.push(index +1);
                            } else {
                                pieceConsumptionGreen.push(index +1);
                            }
                            hotData.push([["颜色:"],[element.colorName],["单耗:"],[toDecimalFour(element.averageConsumption)],["计划单耗:"],[toDecimalFour(element.planConsumption)],["回布数:"],[element.totalReturnCount],["累计用量:"],[element.actTotalConsumption],["剩余"],[element.totalReturnCount-element.actTotalConsumption]]);
                        })
                    }
                    /***
                     * 分床单耗汇总
                     * 1.根据后台传过来的颜色、尺码排序
                     * 2.对尺码进行统一排序
                     * 3.对每一个颜色的床号进行分行获取数据
                     */
                    var colorList =  data["colorList"];
                    var sizeList =  data["sizeList"];
                    var bedConsumption = data["bedConsumption"];
                    sizeList = globalSizeSort(sizeList);
                    var detailHead = [["单耗详情"]];
                    hotData.push([[""]]);
                    hotData.push(detailHead);
                    $.each(colorList, function(index1,element1){
                        var sizeRow = [];
                        if (data[element1]){
                            var bedList = data[element1];
                            sizeRow.push([element1]);
                            sizeRow.push(["日期"],["组名"]);
                            $.each(sizeList, function(index,element){
                                sizeRow.push([element]);
                            });
                            sizeRow.push(["合计"],["总重量"],["匹数"],["单耗"]);
                            hotData.push(sizeRow);
                            $.each(bedList, function(index2,element2){
                                var dataRow = [];
                                dataRow.push([element2]);
                                var flag = true;
                                var dateFlag = true;
                                var bedBatch = 0;
                                var bedActConsumption = 0;
                                var bedLayer = 0;
                                $.each(sizeList, function(index3,element3){
                                    var cutCount = 0;
                                    $.each(bedConsumption, function(index4,element4){
                                        if (element4.bedNumber == element2 && dateFlag){
                                            dataRow.push([element4.cutTime]);
                                            dataRow.push([element4.groupName]);
                                            dateFlag = false;
                                        }
                                        if (element1 == element4.colorName && element2 == element4.bedNumber && element3 == element4.sizeName){
                                            if (flag){
                                                bedBatch = element4.totalBatch;
                                                bedActConsumption = element4.totalWeight;
                                                flag = false;
                                            }
                                            bedLayer += element4.layerCount;
                                            cutCount = element4.layerCount;
                                        }
                                    });
                                    dataRow.push([cutCount]);
                                });
                                dataRow.push([bedLayer]);
                                dataRow.push([bedActConsumption]);
                                dataRow.push([bedBatch]);
                                dataRow.push([toDecimalFour(bedActConsumption/bedLayer)]);
                                hotData.push(dataRow);
                            })
                        }

                    });

                }

                hot = undefined;
                hot = new Handsontable(container, {
                    // data: hotData,
                    rowHeaders: true,
                    colHeaders: true,
                    autoColumnSize: true,
                    dropdownMenu: true,
                    contextMenu: true,
                    minCols: 20,
                    colWidths:[100,100,60,60,80,60,60,60,100,60,60,60,60,60,60],
                    autoWrapRow: true,
                    manualColumnResize:true,
                    height: $(document.body).height() - 200,
                    language: 'zh-CN',
                    licenseKey: 'non-commercial-and-evaluation',
                    cells: function (row, col, prop) {
                        for (var i = 0; i < pieceConsumptionRed.length; i++){
                            if (row === pieceConsumptionRed[i] && col === 3){
                                this.renderer = colorRendererRed;
                            }
                        }
                        for (var j = 0; j < pieceConsumptionGreen.length; j++){
                            if (row === pieceConsumptionGreen[j] && col === 3){
                                this.renderer = colorRendererGreen;
                            }
                        }
                    }
                });
                hot.loadData(hotData);
                $.unblockUI();
            },
            error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        });
    } else {
        data.partName = searchType;
        $.ajax({
            url: basePath+"erp/otherunitconsumptionreport",
            type:'GET',
            data: data,
            success: function (data) {
                $("#exportDiv").show();
                var pieceConsumptionGreen = [];
                var pieceConsumptionRed = [];
                var hotData = [];
                $("#reportExcel").empty();
                var container = document.getElementById('reportExcel');
                var colorRendererGreen = function (instance, td, row, col, prop, value, cellProperties) {
                    Handsontable.renderers.TextRenderer.apply(this, arguments);
                    td.style.backgroundColor = '#2BC08B';
                };
                var colorRendererRed = function (instance, td, row, col, prop, value, cellProperties) {
                    Handsontable.renderers.TextRenderer.apply(this, arguments);
                    td.style.backgroundColor = '#FF4500';
                };

                if(data) {

                    /***
                     * 头部单耗汇总
                     */
                    if (data["summary"]){
                        var summaryData = data["summary"];
                        var partRow = [["部位名称:"],[searchType]];
                        hotData.push(partRow);
                        $.each(summaryData, function(index,element){
                            if (element.averageConsumption > element.planConsumption){
                                pieceConsumptionRed.push(index +1);
                            } else {
                                pieceConsumptionGreen.push(index +1);
                            }
                            hotData.push([["颜色:"],[element.colorName],["单耗:"],[toDecimalFour(element.averageConsumption)],["计划单耗:"],[toDecimalFour(element.planConsumption)],["回布数:"],[element.totalReturnCount],["累计用量:"],[element.actTotalConsumption],["剩余"],[element.totalReturnCount-element.actTotalConsumption]]);
                        })
                    }
                    /***
                     * 分床单耗汇总
                     * 1.根据后台传过来的颜色、尺码排序
                     * 2.对尺码进行统一排序
                     * 3.对每一个颜色的床号进行分行获取数据
                     */
                    var colorList =  data["colorList"];
                    var sizeList =  data["sizeList"];
                    var bedConsumption = data["bedConsumption"];
                    sizeList = globalSizeSort(sizeList);
                    var detailHead = [["单耗详情"]];
                    hotData.push([[""]]);
                    hotData.push(detailHead);
                    $.each(colorList, function(index1,element1){
                        var sizeRow = [];
                        if (data[element1]){
                            var bedList = data[element1];
                            sizeRow.push([element1]);
                            sizeRow.push(["日期"],["组名"]);
                            $.each(sizeList, function(index,element){
                                sizeRow.push([element]);
                            });
                            sizeRow.push(["合计"],["总重量"],["匹数"],["单耗"]);
                            hotData.push(sizeRow);
                            $.each(bedList, function(index2,element2){
                                var dataRow = [];
                                dataRow.push([element2]);
                                var flag = true;
                                var dateFlag = true;
                                var bedBatch = 0;
                                var bedActConsumption = 0;
                                var bedLayer = 0;
                                $.each(sizeList, function(index3,element3){
                                    var cutCount = 0;
                                    $.each(bedConsumption, function(index4,element4){
                                        if (element4.bedNumber == element2 && dateFlag){
                                            dataRow.push([element4.cutTime]);
                                            dataRow.push([element4.groupName]);
                                            dateFlag = false;
                                        }
                                        if (element1 == element4.colorName && element2 == element4.bedNumber && element3 == element4.sizeName){
                                            if (flag){
                                                bedBatch = element4.totalBatch;
                                                bedActConsumption = element4.totalWeight;
                                                flag = false;
                                            }
                                            bedLayer += element4.layerCount;
                                            cutCount = element4.layerCount;
                                        }
                                    });
                                    dataRow.push([cutCount]);
                                });
                                dataRow.push([bedLayer]);
                                dataRow.push([bedActConsumption]);
                                dataRow.push([bedBatch]);
                                dataRow.push([toDecimalFour(bedActConsumption/bedLayer)]);
                                hotData.push(dataRow);
                            })
                        }

                    });

                }

                hot = undefined;
                hot = new Handsontable(container, {
                    // data: hotData,
                    rowHeaders: true,
                    colHeaders: true,
                    autoColumnSize: true,
                    dropdownMenu: true,
                    contextMenu: true,
                    minCols: 20,
                    colWidths:[100,100,60,60,80,60,60,60,100,60,60,60,60,60,60],
                    autoWrapRow: true,
                    manualColumnResize:true,
                    height: $(document.body).height() - 200,
                    language: 'zh-CN',
                    licenseKey: 'non-commercial-and-evaluation',
                    cells: function (row, col, prop) {
                        for (var i = 0; i < pieceConsumptionRed.length; i++){
                            if (row === pieceConsumptionRed[i] && col === 3){
                                this.renderer = colorRendererRed;
                            }
                        }
                        for (var j = 0; j < pieceConsumptionGreen.length; j++){
                            if (row === pieceConsumptionGreen[j] && col === 3){
                                this.renderer = colorRendererGreen;
                            }
                        }
                    }
                });
                hot.loadData(hotData);
                $.unblockUI();
            },
            error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        });
    }



}


function exportData() {
    var data = hot.getData();
    var result = [];
    var col = 0;
    $.each(data,function (index, item) {
        // if(item[0]!=null) {
        for(var i=0;i<item.length;i++) {
            if(item[i]==null) {
                item[i] = '';
            }else if(index==1) {
                col++;
            }
        }
        result.push(item);
        // }else {
        //     return false;
        // }
    });
    var orderName = $("#orderName").val();
    var clothesVersionNumber = $("#clothesVersionNumber").val();
    export2Excel([orderName+"-"+clothesVersionNumber+'单耗报表'],col, result, orderName+"-"+clothesVersionNumber+'单耗报表'+".xls")
}



function toDecimal(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*100)/100;
    return f;
}

function toDecimalFour(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*10000)/10000;
    return f;
}


function transDate(data) {return  moment(data).format("YYYY-MM-DD");}


function printDeal(){
    $("#printf").empty();
    var printBoxs = document.getElementsByName('printTable');
    var newContent = '';
    for(var i=0;i<printBoxs.length;i++) {
        newContent += printBoxs[i].innerHTML;
    }
    var f = document.getElementById('printf');
    // f.contentDocument.write('<link rel="stylesheet" type="text/css" href="/css/app.v2.css">');
    f.contentDocument.write('<style>' +
        'table {' +
        'border-collapse:collapse;' +
        '}' +
        'td {\n' +
        '        border: 1px solid #000000;\n' +
        '    }'+
        '</style>');
    f.contentDocument.write(newContent);
    f.contentDocument.close();
    window.frames['printf'].focus();
    try{
        window.frames['printf'].print();
    }catch(err){
        f.contentWindow.print();
    }
}
