var basePath=$("#basePath").val();
var userName=$("#userName").val();
var userRole=$("#userRole").val();
var procedureInfoList;
var form;
layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.21'
}).extend();
$(document).ready(function () {
    layui.use('form', function(){
        form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
        form.render();
    });
    layui.laydate.render({
        elem: '#from',
        trigger: 'click',
        value: new Date()
    });
    layui.laydate.render({
        elem: '#to',
        trigger: 'click',
        value: new Date()
    });
    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumber", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });
});
layui.use(['form', 'soulTable', 'table', 'element'], function () {
    var table = layui.table,
        soulTable = layui.soulTable,
        form = layui.form,
        $ = layui.$;
    reportTable = table.render({
        elem: '#reportTable'
        ,cols:[[]]
        ,data:[]
        ,height: 'full-130'
        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
        ,defaultToolbar: ['filter', 'print']
        ,title: '计件记录表'
        ,totalRow: true
        ,page: true
        ,overflow: 'tips'
        ,limits: [500, 1000, 2000]
        ,limit: 1000 //每页默认显示的数量
        ,done: function () {
            soulTable.render(this);
        }
    });

    setTimeout(function (){
        var initFrom = $("#from").val();
        var initTo = $("#to").val();
        initTable('', initFrom, initTo);
    }, 100);

    function initTable(orderName, from, to){
        var data = {};
        if (orderName != null && orderName != ""){
            data.orderName = orderName;
        }
        if (from != null && from != ''){
            data.from = from;
        }
        if (to != null && to != ''){
            data.to = to;
        }
        var load = layer.load(2);
        soulTable.clearFilter('reportTable');
        $.ajax({
            url: "/erp/getDeletePieceWorkByInfo",
            type:'GET',
            data: data,
            success: function (res) {
                if(res.data) {
                    var reportData = res.data;
                    reportTable = table.render({
                        elem: '#reportTable'
                        ,page: true
                        ,cols:[[
                            {type:'checkbox', fixed:'left', field:'id'}
                            ,{field:'employeeName', title:'姓名', align:'center', width:100, sort: true, filter: true, totalRowText: '合计'}
                            ,{field:'employeeNumber', title:'工号', align:'center', width:90, sort: true, filter: true}
                            ,{field:'groupName', title:'组名', align:'center', width:100, sort: true, filter: true}
                            ,{field:'clothesVersionNumber', title:'单号', align:'center', width:160, sort: true, filter: true}
                            ,{field:'orderName', title:'款号', align:'center', width:160, sort: true, filter: true}
                            ,{field:'bedNumber', title:'床号', align:'center', width:90, sort: true, filter: true}
                            ,{field:'colorName', title:'颜色', align:'center', width:120, sort: true, filter: true}
                            ,{field:'sizeName', title:'尺码', align:'center', width:120, sort: true, filter: true}
                            ,{field:'packageNumber', title:'扎号', align:'center', width:90, sort: true, filter: true}
                            ,{field:'procedureNumber', title:'工序号', align:'center', width:100, sort: true, filter: true}
                            ,{field:'procedureName', title:'工序名', align:'center', width:120, sort: true, filter: true}
                            ,{field:'layerCount', title:'数量', align:'center', width:90, sort: true, filter: true, totalRow: true}
                            ,{field:'createTime', title:'删除时间', align:'center', width:180, sort: true, filter: true,templet: function (d) {
                                    return globalTransTime(d.createTime);
                                }}
                            ,{field:'operateUser', title:'删除人', align:'center', width:120, sort: true, filter: true}
                        ]]
                        ,excel: {
                            filename: '计件数据.xlsx',
                        }
                        ,data: reportData
                        ,height: 'full-100'
                        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                        ,defaultToolbar: ['filter', 'print']
                        ,title: '计件记录表'
                        ,totalRow: true
                        ,overflow: 'tips'
                        ,limits: [500, 1000, 2000]
                        ,limit: 1000 //每页默认显示的数量
                        ,done: function () {
                            soulTable.render(this);
                            layer.close(load);
                        }
                    });
                }
            }, error: function () {
                layer.msg("获取失败！",{icon: 2});
            }
        });
        return false;
    }

    function reloadTable(){
        var orderName = $("#orderName").val();
        var from = $("#from").val();
        var to = $("#to").val();
        if ((orderName == null || orderName === '') && (from == null ||from === '' || to == null || to === '')){
            layer.msg("款号和日期不能同时为空");
            return false;
        }
        initTable(orderName, from, to);
        return false;
    }

    //监听提交
    form.on('submit(searchBeat)', function(data){
        reloadTable();
        return false;
    });

    table.on('toolbar(reportTable)', function(obj) {
        if (obj.event === 'clearFilter') {
            soulTable.clearFilter('reportTable')
        }
        else if (obj.event === 'refresh') {
            reportTable.reload();
        }
        else if (obj.event === 'exportExcel') {
            soulTable.export('reportTable');
        }
        else if (obj.event === 'recover'){
            if (userRole!='root' && userRole!='role10'){
                layer.msg('您没有权限!');
                return false;
            }
            var reportData = layui.table.cache.reportTable;
            var pieceWorkList = [];
            $.each(reportData, function (r_index, r_item){
                if (r_item.LAY_CHECKED){
                    pieceWorkList.push(r_item);
                }
            });
            if (pieceWorkList.length === 0){
                layer.msg('请先选择数据');
                return false;
            }
            layer.confirm('确认恢复吗', function(index){
                $.ajax({
                    url: basePath + "erp/pieceWorkDataRecover",
                    type:'POST',
                    data: {
                        pieceWorkDeleteJson: JSON.stringify(pieceWorkList)
                    },
                    success: function (data) {
                        if(data.result == 4) {
                            layer.msg(data.msg);
                        } else if (data.result == 0) {
                            layer.msg("恢复成功！",{icon: 1});
                            reloadTable();
                            return false;
                        } else {
                            layer.msg("恢复失败！",{icon: 2});
                        }
                    }, error: function () {
                        layer.msg("网络异常！",{icon: 2});
                    }
                });
            });
        }
    });

});