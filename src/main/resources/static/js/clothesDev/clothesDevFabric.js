var userName = $("#userName").val();
var userRole = $("#userRole").val();
var form;
var supplierDemo;
layui.use('form', function(){
    form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
    form.render();
});

$(document).ready(function () {

    layui.laydate.render({
        elem: '#deadLine',
        trigger: 'click'
    });
    layui.laydate.render({
        elem: '#deliveryDate',
        trigger: 'click'
    });

    layui.use(['autocomplete'], function () {
        layui.autocomplete.render({
            elem: $('#fabricName')[0],
            url: 'getclothesdevfabricnamehint',
            template_val: '{{d}}',
            template_txt: '{{d}}'
        })
    });
    layui.use(['autocomplete'], function () {
        layui.autocomplete.render({
            elem: $('#fabricNumber')[0],
            url: 'getclothesdevfabricnumberhint',
            template_val: '{{d}}',
            template_txt: '{{d}}'
        })
    });

});
$(document).on('keydown', '.layui-table-edit', function (e) {
    var td = $(this).parent('td')
    ,tr = td.parent('tr')
    ,trs = tr.parent().parent().find('tr')
    ,tr_index = tr.index()
    ,td_index = td.index()
    ,td_last_index = tr.find('[data-edit="text"]:last').index()
    ,td_first_index = tr.find('[data-edit="text"]:first').index();
    switch (e.keyCode) {
        case 13:
            tr.next().find('td').eq(td_index).click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            return false;
        case 39:
            td.nextAll('[data-edit="text"]:first').click();
            if (td_index == td_last_index){
                tr.next().find('td').eq(td_first_index).click();
                if (tr_index == trs.length - 1){
                    trs.eq(0).find('td').eq(td_first_index).click();
                }
            }
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
        case 37:
            td.prevAll('[data-edit="text"]:first').click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
        case 38:
            tr.prev().find('td').eq(td_index).click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
        case 40:
            tr.next().find('td').eq(td_index).click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
    }

});
layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.26'
}).extend({                         // 模块别名
    soulTable: 'soulTable'
});
var dataTable;
layui.use(['form', 'soulTable', 'table', 'upload','yutons_sug', 'element'], function () {

    var table = layui.table,
        soulTable = layui.soulTable,
        element = layui.element,
        form = layui.form,
        $ = layui.$;

    dataTable = table.render({
        elem: '#dataTable'
        ,cols: [[]]
        ,loading:true
        ,data:[]
        ,height: 'full-80'
        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
        ,title: '开发面料'
        ,totalRow: true
        ,page: true
        ,even: true
        ,overflow: 'tips'
        ,limits: [50, 100, 200]
        ,limit: 50 //每页默认显示的数量
        ,done: function (res, curr, count) {
            soulTable.render(this);
        }
    });

    initTable('','');

    function initTable(fabricName, fabricNumber){
        var param = {};
        if (fabricName != null && fabricName != ''){
            param.fabricName = fabricName;
        }
        if (fabricNumber != null && fabricNumber != ''){
            param.fabricNumber = fabricNumber;
        }
        param.storageType = "index";
        $.ajax({
            url: "/erp/getclothesdevfabricmanagebyinfo",
            type: 'GET',
            data: param,
            success: function (res) {
                if (res.data) {
                    var reportData = res.data;
                    table.render({
                        elem: '#dataTable'
                        ,cols:[[
                            {title: '#', width: 50, collapse: true,lazy: true, icon: ['layui-icon layui-icon-triangle-r', 'layui-icon layui-icon-triangle-d'], children:[
                                    {
                                        title: '入库信息'
                                        ,url: 'getclothesdevfabricmanagebyinfo'
                                        ,where: function(row){
                                            return {
                                                fabricID: row.id,
                                                storageType: "in"
                                            }
                                        }
                                        ,height: 300
                                        ,loading:true
                                        ,page: false
                                        ,totalRow: true
                                        ,toolbar: '<div><a class="layui-btn layui-btn-danger layui-btn-sm" lay-event="batchDelete">删除</a></div>'
                                        ,cols: [[
                                            {type:'checkbox'}
                                            ,{field:'fabricName', title:'名称', align:'center', width:170, sort: true, filter: true}
                                            ,{field:'fabricNumber', title:'编号', align:'center', width:120, sort: true, filter: true, totalRowText: '合计'}
                                            ,{field:'fabricWidth', title:'门幅', align:'center', width:120, sort: true, filter: true}
                                            ,{field:'fabricWeight', title:'克重', align:'center', width:120, sort: true, filter: true}
                                            ,{field:'fabricContent', title:'成分', align:'center',width:200, sort: true, filter: true}
                                            ,{field:'unit', title:'单位', align:'center', width:90, sort: true, filter: true}
                                            ,{field:'price', title:'单价', align:'center', width:90, sort: true, filter: true}
                                            ,{field:'fabricInCount', title:'数量', align:'center', width:90, sort: true, filter: true}
                                            ,{field:'unitTwo', title:'单位2', align:'center', width:90, sort: true, filter: true}
                                            ,{field:'priceTwo', title:'单价2', align:'center', width:90, sort: true, filter: true}
                                            ,{field:'fabricInCountTwo', title:'数量2', align:'center', width:90, sort: true, filter: true}
                                            ,{field:'fabricColor', title:'颜色', align:'center', width:100, sort: true, filter: true}
                                            ,{field:'fabricColorNumber', title:'色号', align:'center', width:100, sort: true, filter: true}
                                            ,{field:'supplier', title:'供应商', align:'center', width:120, sort: true, filter: true}
                                            ,{field:'batchNumberIn', title:'卷数', align:'center', width:90, sort: true, filter: true}
                                            ,{field:'remark', title:'备注', align:'center', width:120, sort: true, filter: true}
                                            ,{field:'id', hide: true}
                                            ,{field:'fabricID', hide: true}
                                        ]]
                                        ,toolbarEvent: function (obj, pobj) {
                                            // obj 子表当前行对象
                                            // pobj 父表当前行对象
                                            var objData = obj.data;
                                            var childId = this.id;
                                            if (obj.event === 'batchDelete') {
                                                var checkStatus = table.checkStatus(obj.config.id);
                                                if(checkStatus.data.length == 0) {
                                                    layer.msg('请选择数据');
                                                }else {
                                                    var idList = [];
                                                    var fabricInCount = 0;
                                                    var batchNumberIn = 0;
                                                    var fabricID;
                                                    $.each(checkStatus.data, function (index, item) {
                                                        idList.push(item.id);
                                                        fabricInCount += item.fabricInCount;
                                                        batchNumberIn += item.batchNumberIn;
                                                        fabricID = item.fabricID;
                                                    });
                                                    layer.confirm('确认全部删除吗?', function(index){
                                                        $.ajax({
                                                            url: "/erp/deleteclothesdevfabricmanageinbatch",
                                                            type: 'POST',
                                                            data: {
                                                                idList: idList,
                                                                fabricInCount: fabricInCount,
                                                                batchNumberIn: batchNumberIn,
                                                                id: fabricID
                                                            },
                                                            traditional: true,
                                                            success: function (res) {
                                                                if (res.result == 0) {
                                                                    layer.close(index);
                                                                    layer.msg("删除成功！", {icon: 1});
                                                                    table.reload(childId);
                                                                } else if (res.result == 4) {
                                                                    layer.msg("库存不足！", {icon: 2});
                                                                } else {
                                                                    layer.msg("删除失败！", {icon: 2});
                                                                }
                                                            }, error: function () {
                                                                layer.msg("删除失败！", {icon: 2});
                                                            }
                                                        })
                                                    });
                                                }
                                            }
                                        }
                                        ,done: function () {
                                            soulTable.render(this);
                                        }
                                        ,filter: {
                                            bottom: false,
                                            items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                                        }
                                    }, {
                                        title: '库存信息'
                                        ,url: 'getclothesdevfabricmanagebyinfo'
                                        ,where: function(row){
                                            return {
                                                fabricID: row.id,
                                                storageType: "storage"
                                            }
                                        }
                                        ,height: 300
                                        ,loading:true
                                        ,page: false
                                        ,totalRow: true
                                        ,cols: [[
                                            {title: '操作', minWidth: 90,align:'center', templet: function(row) {
                                                    return '<a class="layui-btn layui-btn-xs" lay-event="outStore">出库</a>';
                                                }}
                                            ,{field:'fabricName', title:'名称', align:'center', width:170, sort: true, filter: true}
                                            ,{field:'fabricNumber', title:'编号', align:'center', width:120, sort: true, filter: true, totalRowText: '合计'}
                                            ,{field:'fabricWidth', title:'门幅', align:'center', width:120, sort: true, filter: true}
                                            ,{field:'fabricWeight', title:'克重', align:'center', width:120, sort: true, filter: true}
                                            ,{field:'fabricContent', title:'成分', align:'center',width:200, sort: true, filter: true}
                                            ,{field:'unit', title:'单位', align:'center', width:90, sort: true, filter: true}
                                            ,{field:'price', title:'单价', align:'center', width:90, sort: true, filter: true}
                                            ,{field:'fabricStorageCount', title:'数量', align:'center', width:90, sort: true, filter: true}
                                            ,{field:'unitTwo', title:'单位2', align:'center', width:90, sort: true, filter: true}
                                            ,{field:'priceTwo', title:'单价2', align:'center', width:90, sort: true, filter: true}
                                            ,{field:'fabricStorageCountTwo', title:'数量2', align:'center', width:90, sort: true, filter: true}
                                            ,{field:'fabricColor', title:'颜色', align:'center', width:100, sort: true, filter: true}
                                            ,{field:'fabricColorNumber', title:'色号', align:'center', width:100, sort: true, filter: true}
                                            ,{field:'supplier', title:'供应商', align:'center', width:120, sort: true, filter: true}
                                            ,{field:'batchNumberStorage', title:'卷数', align:'center', width:90, sort: true, filter: true}
                                            ,{field:'remark', title:'备注', align:'center', width:120, sort: true, filter: true}
                                            ,{field:'id', hide: true}
                                        ]]
                                        ,toolEvent: function (obj, pobj) {
                                            var rowData = obj.data;
                                            var childId = this.id; // 通过 this 对象获取当前子表的id
                                            if (obj.event === 'outStore'){
                                                var index = layer.open({
                                                    type: 1 //Page层类型
                                                    , title: "出库"
                                                    , btn: ['保存']
                                                    , area: ['850px','300px']
                                                    , shade: 0.6 //遮罩透明度
                                                    , maxmin: false //允许全屏最小化
                                                    , anim: 0 //0-6的动画形式，-1不开启
                                                    , content: $("#outStoreOperate")
                                                    ,yes: function(i, layero){
                                                        var param = {};
                                                        param.batchNumberOut = $("#batchNumberOut").val();
                                                        var unitOut = $("#unitTwoOut").val();
                                                        if (unitOut == 'unit'){
                                                            param.fabricOutCount = $("#fabricOutCount").val();
                                                            param.fabricOutCountTwo = Number(param.fabricOutCount / rowData.ratio);
                                                        } else {
                                                            param.fabricOutCountTwo = $("#fabricOutCount").val();
                                                            param.fabricOutCount = Number(param.fabricOutCount * rowData.ratio);
                                                        }
                                                        if (param.fabricOutCount == null || param.fabricOutCount == '' || param.batchNumberOut == null || param.batchNumberOut == ''){
                                                            layer.msg("请输入完整必要信息~~~");
                                                            return false;
                                                        }
                                                        if (Number(param.fabricOutCount) > Number(rowData.fabricStorageCount) || Number(param.fabricOutCountTwo) > Number(rowData.fabricStorageCountTwo) || Number(param.batchNumberOut) > Number(rowData.batchNumberStorage)){
                                                            layer.msg("数量有误~~~");
                                                            return false;
                                                        }
                                                        $.ajax({
                                                            url: "/erp/clothesdevfabricmanageoutstore",
                                                            type: 'POST',
                                                            data: {
                                                                id: rowData.id,
                                                                fabricOutCount: param.fabricOutCount,
                                                                fabricOutCountTwo: param.fabricOutCountTwo,
                                                                batchNumberOut: param.batchNumberOut
                                                            },
                                                            success: function (res) {
                                                                if (res.result == 0) {
                                                                    layer.close(index);
                                                                    layer.msg("添加成功！", {icon: 1});
                                                                    initTable('', '');
                                                                    $("#fabricOutCount").val('');
                                                                    $("#batchNumberOut").val('');
                                                                    $("#unitTwoOut").empty();
                                                                } else if (res.result == 4) {
                                                                    layer.msg("库存不足！", {icon: 2});
                                                                } else {
                                                                    layer.msg("添加失败！", {icon: 2});
                                                                }
                                                            },
                                                            error: function () {
                                                                layer.msg("添加失败", { icon: 2 });
                                                            }
                                                        });
                                                    }
                                                    ,cancel: function () {
                                                        $("#fabricOutCount").val('');
                                                        $("#batchNumberOut").val('');
                                                        $("#unitTwoOut").empty();
                                                    }
                                                });
                                                $("#unitTwoOut").empty();
                                                $("#unitTwoOut").append("<option value='unit'>"+rowData.unit+"</option>");
                                                $("#unitTwoOut").append("<option value='unitTwo'>"+rowData.unitTwo+"</option>");
                                                form.render('select');
                                            }
                                        }
                                        ,done: function () {
                                            soulTable.render(this);
                                        }
                                        ,filter: {
                                            bottom: false,
                                            items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                                        }
                                    },{
                                        title: '出库信息'
                                        ,url: 'getclothesdevfabricmanagebyinfo'
                                        ,where: function(row){
                                            return {
                                                fabricID: row.id,
                                                storageType: "out"
                                            }
                                        }
                                        ,height: 300
                                        ,loading:true
                                        ,page: false
                                        ,totalRow: true
                                        ,toolbar: '<div><a class="layui-btn layui-btn-danger layui-btn-sm" lay-event="batchDelete">删除</a></div>'
                                        ,cols: [[
                                            {type:'checkbox'}
                                            ,{field:'fabricName', title:'名称', align:'center', width:170, sort: true, filter: true}
                                            ,{field:'fabricNumber', title:'编号', align:'center', width:120, sort: true, filter: true, totalRowText: '合计'}
                                            ,{field:'fabricWidth', title:'门幅', align:'center', width:120, sort: true, filter: true}
                                            ,{field:'fabricWeight', title:'克重', align:'center', width:120, sort: true, filter: true}
                                            ,{field:'fabricContent', title:'成分', align:'center',width:200, sort: true, filter: true}
                                            ,{field:'unit', title:'单位', align:'center', width:90, sort: true, filter: true}
                                            ,{field:'price', title:'单价', align:'center', width:90, sort: true, filter: true}
                                            ,{field:'fabricOutCount', title:'数量', align:'center', width:90, sort: true, filter: true}
                                            ,{field:'unitTwo', title:'单位2', align:'center', width:90, sort: true, filter: true}
                                            ,{field:'priceTwo', title:'单价2', align:'center', width:90, sort: true, filter: true}
                                            ,{field:'fabricOutCountTwo', title:'数量2', align:'center', width:90, sort: true, filter: true}
                                            ,{field:'fabricColor', title:'颜色', align:'center', width:100, sort: true, filter: true}
                                            ,{field:'fabricColorNumber', title:'色号', align:'center', width:100, sort: true, filter: true}
                                            ,{field:'supplier', title:'供应商', align:'center', width:120, sort: true, filter: true}
                                            ,{field:'batchNumberOut', title:'卷数', align:'center', width:90, sort: true, filter: true}
                                            ,{field:'remark', title:'备注', align:'center', width:120, sort: true, filter: true}
                                            ,{field:'id', hide: true}
                                            ,{field:'fabricID', hide: true}
                                        ]]
                                        ,toolbarEvent: function (obj, pobj) {
                                            // obj 子表当前行对象
                                            // pobj 父表当前行对象
                                            var objData = obj.data;
                                            var childId = this.id;
                                            if (obj.event === 'batchDelete') {
                                                var checkStatus = table.checkStatus(obj.config.id);
                                                if(checkStatus.data.length == 0) {
                                                    layer.msg('请选择数据');
                                                }else {
                                                    var idList = [];
                                                    var fabricOutCount = 0;
                                                    var batchNumberOut = 0;
                                                    var fabricID;
                                                    $.each(checkStatus.data, function (index, item) {
                                                        idList.push(item.id);
                                                        fabricOutCount += item.fabricOutCount;
                                                        batchNumberOut += item.batchNumberOut;
                                                        fabricID = item.fabricID;
                                                    });
                                                    layer.confirm('确认全部删除吗?', function(index){
                                                        $.ajax({
                                                            url: "/erp/deleteclothesdevfabricmanageoutbatch",
                                                            type: 'POST',
                                                            data: {
                                                                idList: idList,
                                                                fabricOutCount: fabricOutCount,
                                                                batchNumberOut: batchNumberOut,
                                                                id: fabricID
                                                            },
                                                            traditional: true,
                                                            success: function (res) {
                                                                if (res.result == 0) {
                                                                    layer.close(index);
                                                                    layer.msg("删除成功！", {icon: 1});
                                                                    table.reload(childId);
                                                                } else if(res.result == 4) {
                                                                    layer.msg("库存异常！", {icon: 2});
                                                                } else {
                                                                    layer.msg("删除失败！", {icon: 2});
                                                                }
                                                            }, error: function () {
                                                                layer.msg("删除失败！", {icon: 2});
                                                            }
                                                        })
                                                    });
                                                }
                                            }
                                        }
                                        ,done: function () {
                                            soulTable.render(this);
                                        }
                                        ,filter: {
                                            bottom: false,
                                            items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                                        }
                                    }
                                ]}
                            ,{type:'numbers', align:'center', title:'序号', width:60}
                            ,{field:'fabricName', title:'名称', align:'center', width:170, sort: true, filter: true}
                            ,{field:'fabricNumber', title:'编号', align:'center', width:120, sort: true, filter: true, totalRowText: '合计'}
                            ,{field:'fabricWidth', title:'门幅', align:'center', width:120, sort: true, filter: true}
                            ,{field:'fabricWeight', title:'克重', align:'center', width:120, sort: true, filter: true, totalRow: true}
                            ,{field:'fabricContent', title:'成分', align:'center',width:200, sort: true, filter: true}
                            ,{field:'unit', title:'单位', align:'center', width:90, sort: true, filter: true}
                            ,{field:'price', title:'单价', align:'center', width:90, sort: true, filter: true}
                            ,{field:'ratio', title:'转化比例', align:'center', width:90, sort: true, filter: true}
                            ,{field:'unitTwo', title:'单位2', align:'center', width:90, sort: true, filter: true}
                            ,{field:'priceTwo', title:'单价2', align:'center', width:90, sort: true, filter: true}
                            ,{field:'fabricColor', title:'颜色', align:'center', width:100, sort: true, filter: true}
                            ,{field:'fabricColorNumber', title:'色号', align:'center', width:100, sort: true, filter: true}
                            ,{field:'supplier', title:'供应商', align:'center', width:120, sort: true, filter: true}
                            ,{field:'remark', title:'备注', align:'center', width:120, sort: true, filter: true}
                            ,{field:'id', fixed: 'right', title:'操作', align:'center', toolbar: '#barTop', width:280}
                        ]]
                        ,data:reportData
                        ,height: 'full-80'
                        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                        ,title: '开发面料'
                        ,totalRow: true
                        ,page: true
                        ,even: true
                        ,overflow: 'tips'
                        ,limits: [50, 100, 200]
                        ,limit: 50 //每页默认显示的数量
                        ,done: function (res, curr, count) {
                            soulTable.render(this);
                        }
                    });
                }
            }
        })
    }

    form.on('submit(searchBeat)', function(data){
        var filterFabricName = $("#fabricName").val();
        var filterFabricNumber = $("#fabricNumber").val();
        initTable(filterFabricName, filterFabricNumber);
        return false;
    });

    table.on('toolbar(dataTable)', function(obj){
        if (obj.event === 'clearFilter') {
            // 清除所有筛选条件并重载表格
            // 参数: tableId
            soulTable.clearFilter('dataTable');
        }else if (obj.event === 'refresh') {
            initTable('', '');
        } else if (obj.event === 'exportExcel') {
            soulTable.export('dataTable');
        }else if(obj.event === 'addNew') {  //录入基础信息
            var index = layer.open({
                type: 1 //Page层类型
                , title: "新增"
                , btn: ['保存']
                , area: ['1000px','500px']
                , shade: 0.6 //遮罩透明度
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , content: $("#fabricOperate")
                ,yes: function(i, layero){
                    var param = {};
                    param.fabricName = $("#fabricNameAdd").val();
                    param.fabricNumber = $("#fabricNumberAdd").val() + $("#fabricNumberOrderAdd").val();
                    param.fabricWidth = $("#fabricWidth").val();
                    param.fabricWeight = $("#fabricWeight").val();
                    param.fabricContent = $("#fabricContent").val();
                    var fabricColor = colorSelect.getValue('name')[0];
                    param.fabricColorNumber = fabricColor.split("/")[0];
                    param.fabricColor = fabricColor.split("/")[1];
                    param.unit = $("#unit").val();
                    param.priceTwo = $("#priceTwo").val();
                    param.unitTwo = $("#unitTwo").val();
                    param.ratio = $("#ratio").val();
                    param.supplier = supplierDemo.getValue('nameStr');
                    param.remark = $("#remark").val();
                    param.storageType = "index";
                    if (param.ratio == null || param.ratio == '' || param.ratio == 0){
                        param.ratio = 1;
                    }
                    param.price = toDecimal(param.priceTwo / param.ratio);
                    if (param.fabricName == null || param.fabricName === '' || param.fabricNumber == null || param.fabricNumber === '' || param.unit == null || param.unit === '' || param.price == null || param.price === '' || param.fabricColorNumber == null || param.fabricColorNumber === '' || param.fabricColor == null || param.fabricColor === ''){
                        layer.msg("请输入完整必要信息~~~");
                        return false;
                    }
                    $.ajax({
                        url: "/erp/addclothesdevfabricmanage",
                        type: 'POST',
                        data: {
                            clothesDevFabricManageJson: JSON.stringify(param)
                        },
                        success: function (res) {
                            if (res.result == 0) {
                                layer.close(index);
                                layer.msg("添加成功！", {icon: 1});
                                initTable('', '');
                                form.val("fabricAdd", { //formTest 即 class="layui-form" 所在元素属性 lay-filter="" 对应的值
                                    "fabricNameAdd": '' // "name": "value"
                                    ,"fabricNumberAdd": ''
                                    ,"fabricNumberOrderAdd": ''
                                    ,"fabricWidth": ''
                                    ,"fabricWeight": ''
                                    ,"fabricContent": ''
                                    ,"unit": ''
                                    ,"priceTwo": ''
                                    ,"unitTwo": ''
                                    ,"ratio": ''
                                    ,"supplier": ''
                                    ,"remark": ''
                                });
                            } else {
                                layer.msg("添加失败！", {icon: 2});
                            }
                        },
                        error: function () {
                            layer.msg("添加失败", { icon: 2 });
                        }
                    });
                }
                ,cancel: function () {
                    form.val("fabricAdd", { //formTest 即 class="layui-form" 所在元素属性 lay-filter="" 对应的值
                        "fabricNameAdd": '' // "name": "value"
                        ,"fabricNumberAdd": ''
                        ,"fabricNumberOrderAdd": ''
                        ,"fabricWidth": ''
                        ,"fabricWeight": ''
                        ,"fabricContent": ''
                        ,"unit": ''
                        ,"priceTwo": ''
                        ,"unitTwo": ''
                        ,"ratio": ''
                        ,"supplier": ''
                        ,"remark": ''
                    });
                }
            });
            form.render('select');
            $.ajax({
                url: "/erp/getallcolormanage",
                type: 'GET',
                data: {},
                success: function (res) {
                    if (res.data) {
                        colorNameData = res.data;
                        var thisData = [];
                        $.each(res.data, function(index,element){
                            thisData.push({name:element.colorNumber + "/" + element.colorName, value: element.id});
                        });
                        colorSelect = xmSelect.render({
                            el: '#colorInfo',
                            radio: true,
                            filterable: true,
                            height: '200px',
                            data: thisData
                        });
                    }
                },
                error: function () {
                    layer.msg("获取版单失败", { icon: 2 });
                }
            });
            $.ajax({
                url: "/erp/getallsupplierinfo",
                data: {
                    supplierType: "面料"
                },
                async:false,
                success:function(res){
                    if (res.data){
                        var resultData = [];
                        $.each(res.data, function(index,element){
                            if (element != null && element != ''){
                                resultData.push({name:element.supplierName, value:element.supplierName});
                            }
                        });
                        supplierDemo = xmSelect.render({
                            el: '#supplier',
                            radio: true,
                            filterable: true,
                            data: resultData
                        });
                    }
                }, error:function(){
                }
            });
            $.ajax({
                url: "/erp/getallclothesdevfabricnumber",
                type: 'GET',
                data: {},
                success: function (res) {
                    if (res.data) {
                        $("#fabricNumberAdd").empty();
                        $("#fabricNumberAdd").append("<option value=''>选择代码</option>");
                        $.each(res.data, function(index,element){
                            $("#fabricNumberAdd").append("<option value='"+element.fabricNumberCode+"'>"+element.fabricNumberCode + "/" + element.fabricNumberName +"</option>")
                        });
                        form.render();
                    }
                },
                error: function () {
                    layer.msg("获取版单失败", { icon: 2 });
                }
            });
            form.on('select(demo)', function(data){
                var fabricNumberCode = $("#fabricNumberAdd").val();
                if (fabricNumberCode != null && fabricNumberCode != ''){
                    $.ajax({
                        url: "/erp/getfabricnameorderbyprefix",
                        data: {
                            fabricNumberCode: fabricNumberCode
                        },
                        success:function(data){
                            if (data.fabricOrder){
                                $("#fabricNumberOrderAdd").val(data.fabricOrder);
                            }
                        }, error:function(){
                        }
                    });
                    form.render('select');
                }
            });
        }else if(obj.event === 'storage') {  //录入基础信息
            var index = layer.open({
                type: 1 //Page层类型
                , title: "开发面料库存"
                , btn: []
                , shade: 0.6 //遮罩透明度
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , content: "<table id='storageTable' lay-filter='storageTable'></table>"
            });
            layer.full(index);
            setTimeout(function () {
                $.ajax({
                    url: "/erp/getsharefabricstorage",
                    type: 'GET',
                    data: {
                        storageType: 'storage'
                    },
                    success: function (res) {
                        if (res.data) {
                            var reportData = res.data;
                            table.render({
                                elem: '#storageTable'
                                ,cols:[[
                                    {type:'numbers', align:'center', title:'序号', width:60}
                                    ,{field:'fabricName', title:'名称', align:'center', width:170, sort: true, filter: true}
                                    ,{field:'fabricNumber', title:'编号', align:'center', width:120, sort: true, filter: true, totalRowText: '合计'}
                                    ,{field:'fabricWidth', title:'门幅', align:'center', width:120, sort: true, filter: true}
                                    ,{field:'fabricWeight', title:'克重', align:'center', width:120, sort: true, filter: true}
                                    ,{field:'fabricContent', title:'成分', align:'center',width:200, sort: true, filter: true}
                                    ,{field:'unit', title:'单位', align:'center', width:90, sort: true, filter: true}
                                    ,{field:'price', title:'单价', align:'center', width:90, sort: true, filter: true}
                                    ,{field:'fabricStorageCount', title:'数量', align:'center', width:90, sort: true, filter: true, totalRow: true}
                                    ,{field:'unitTwo', title:'单位2', align:'center', width:90, sort: true, filter: true}
                                    ,{field:'priceTwo', title:'单价2', align:'center', width:90, sort: true, filter: true}
                                    ,{field:'fabricStorageCountTwo', title:'数量2', align:'center', width:90, sort: true, filter: true}
                                    ,{field:'fabricColor', title:'颜色', align:'center', width:100, sort: true, filter: true}
                                    ,{field:'fabricColorNumber', title:'色号', align:'center', width:100, sort: true, filter: true}
                                    ,{field:'supplier', title:'供应商', align:'center', width:120, sort: true, filter: true}
                                    ,{field:'batchNumberStorage', title:'卷数', align:'center', width:90, sort: true, filter: true}
                                    ,{field:'jarName', title:'缸号', align:'center', width:90, sort: true, filter: true}
                                    ,{field:'remark', title:'备注', align:'center', width:120, sort: true, filter: true}
                                    ,{field:'location', title:'位置', align:'center', width:120, sort: true, filter: true}
                                    ,{field:'operateDate', title:'日期', align:'center', width:120, sort: true, filter: true, templet:function (d) {
                                            return moment(d.operateDate).format("YYYY-MM-DD");
                                        }}
                                    ,{field:'payState', title:'状态', align:'center', width:90, sort: true, filter: true}
                                    ,{field:'id', hide: true}
                                ]]
                                ,data:reportData
                                ,height: 'full-130'
                                ,title: '面料库存'
                                ,totalRow: true
                                ,page: true
                                ,even: true
                                ,overflow: 'tips'
                                ,limits: [50, 100, 200]
                                ,limit: 50 //每页默认显示的数量
                                ,done: function (res, curr, count) {
                                    soulTable.render(this);
                                }
                            });
                        }
                    }
                })
            },100)
        }
    });

    //监听行工具事件
    table.on('tool(dataTable)', function(obj){
        var data = obj.data;
        if(obj.event === 'del'){
            layer.confirm('该面料删除后所有的出入库记录都会删除,确认删除吗', function(index){
                $.ajax({
                    url: "/erp/deleteclothesdevfabricmanagealltype",
                    type: 'POST',
                    data: {
                        id:data.id
                    },
                    success: function (res) {
                        if (res.result == 0) {
                            initTable('', '');
                            layer.close(index);
                            layer.msg("删除成功！", {icon: 1});
                        } else {
                            layer.msg("删除失败！", {icon: 2});
                        }
                    },
                    error: function () {
                        layer.msg("删除失败！", {icon: 2});
                    }
                })

            });
        } else if(obj.event === 'inStore'){
            var index = layer.open({
                type: 1 //Page层类型
                , title: "入库"
                , btn: ['保存']
                , area: ['850px','300px']
                , shade: 0.6 //遮罩透明度
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , content: $("#inStoreOperate")
                ,yes: function(i, layero){
                    var param = {};
                    // param.fabricInCount = $("#fabricInCount").val();
                    param.batchNumberIn = $("#batchNumberIn").val();
                    param.operateDate = $("#operateDate").val();
                    param.location = $("#location").val();
                    param.payState = $("#payStateIn").val();
                    var unitIn = $("#unitTwoIn").val();
                    param.remark = $("#remarkIn").val();
                    if (unitIn == 'unit'){
                        param.fabricInCount = $("#fabricInCount").val();
                        param.price = $("#priceTwoIn").val();
                        param.fabricInCountTwo = Number(param.fabricInCount / data.ratio);
                        param.priceTwo = Number(param.price * data.ratio);
                    } else {
                        param.fabricInCountTwo = $("#fabricInCount").val();
                        param.priceTwo = $("#priceTwoIn").val();
                        param.price = Number(param.priceTwo / data.ratio);
                        param.fabricInCount = Number(param.fabricInCountTwo * data.ratio);
                    }
                    if (param.operateDate == null || param.operateDate === '' || param.location == null || param.location === '' || param.fabricInCount == null || param.fabricInCount === '' || param.batchNumberIn == null || param.batchNumberIn === ''){
                        layer.msg("请输入完整必要信息~~~");
                        return false;
                    }
                    $.ajax({
                        url: "/erp/clothesdevfabricmanageinstore",
                        type: 'POST',
                        data: {
                            id: data.id,
                            fabricInCount: param.fabricInCount,
                            fabricInCountTwo: param.fabricInCountTwo,
                            batchNumberIn: param.batchNumberIn,
                            price: param.price,
                            priceTwo: param.priceTwo,
                            operateDate: param.operateDate,
                            location: param.location,
                            payState: param.payState,
                            remark: param.remark
                        },
                        success: function (res) {
                            if (res.result == 0) {
                                layer.close(index);
                                layer.msg("添加成功！", {icon: 1});
                                initTable('', '');

                                form.val("inStoreAdd", { //formTest 即 class="layui-form" 所在元素属性 lay-filter="" 对应的值
                                    "fabricInCount": '' // "name": "value"
                                    ,"batchNumberIn": ''
                                    ,"unitTwoIn": ''
                                    ,"priceTwoIn": ''
                                    ,"operateDate": ''
                                    ,"location": ''
                                    ,"payStateIn": ''
                                    ,"remarkIn": ''
                                });

                            } else {
                                layer.msg("添加失败！", {icon: 2});
                            }
                        },
                        error: function () {
                            layer.msg("添加失败", { icon: 2 });
                        }
                    });
                }
                ,cancel: function () {
                    form.val("inStoreAdd", { //formTest 即 class="layui-form" 所在元素属性 lay-filter="" 对应的值
                        "fabricInCount": '' // "name": "value"
                        ,"batchNumberIn": ''
                        ,"unitTwoIn": ''
                        ,"priceTwoIn": ''
                        ,"operateDate": ''
                        ,"location": ''
                        ,"payStateIn": ''
                        ,"remarkIn": ''
                    });
                }
            });
            $("#unitTwoIn").empty();
            $("#unitTwoIn").append("<option value='unit'>"+data.unit+"</option>");
            $("#unitTwoIn").append("<option value='unitTwo'>"+data.unitTwo+"</option>");
            layui.laydate.render({
                elem: '#operateDate',
                trigger: 'click'
            });
            form.render('select');
        } else if(obj.event === 'add'){
            var index = layer.open({
                type: 1 //Page层类型
                , title: "新增"
                , btn: ['保存']
                , area: ['1000px','500px']
                , shade: 0.6 //遮罩透明度
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , content: $("#fabricOperate")
                ,yes: function(i, layero){
                    var param = {};
                    param.fabricName = $("#fabricNameAdd").val();
                    param.fabricNumber = $("#fabricNumberAdd").val() + $("#fabricNumberOrderAdd").val();
                    param.fabricWidth = $("#fabricWidth").val();
                    param.fabricWeight = $("#fabricWeight").val();
                    param.fabricContent = $("#fabricContent").val();
                    var fabricColor = colorSelect.getValue('name')[0];
                    param.fabricColorNumber = fabricColor.split("/")[0];
                    param.fabricColor = fabricColor.split("/")[1];
                    param.unit = $("#unit").val();
                    param.priceTwo = $("#priceTwo").val();
                    param.unitTwo = $("#unitTwo").val();
                    param.ratio = $("#ratio").val();
                    param.supplier = supplierDemo.getValue('nameStr');
                    param.remark = $("#remark").val();
                    param.storageType = "index";
                    if (param.ratio == null || param.ratio == '' || param.ratio == 0){
                        param.ratio = 1;
                    }
                    param.price = toDecimal(param.priceTwo / param.ratio);
                    if (param.fabricName == null || param.fabricName === '' || param.fabricNumber == null || param.fabricNumber === '' || param.unit == null || param.unit === '' || param.price == null || param.price === '' || param.fabricColorNumber == null || param.fabricColorNumber === '' || param.fabricColor == null || param.fabricColor === ''){
                        layer.msg("请输入完整必要信息~~~");
                        return false;
                    }
                    $.ajax({
                        url: "/erp/addclothesdevfabricmanage",
                        type: 'POST',
                        data: {
                            clothesDevFabricManageJson: JSON.stringify(param)
                        },
                        success: function (res) {
                            if (res.result == 0) {
                                layer.close(index);
                                layer.msg("添加成功！", {icon: 1});
                                initTable('', '');
                                form.val("fabricAdd", { //formTest 即 class="layui-form" 所在元素属性 lay-filter="" 对应的值
                                    "fabricNameAdd": '' // "name": "value"
                                    ,"fabricNumberAdd": ''
                                    ,"fabricNumberOrderAdd": ''
                                    ,"fabricWidth": ''
                                    ,"fabricWeight": ''
                                    ,"fabricContent": ''
                                    ,"unit": ''
                                    ,"priceTwo": ''
                                    ,"unitTwo": ''
                                    ,"ratio": ''
                                    // ,"supplier": ''
                                    ,"remark": ''
                                });
                            } else {
                                layer.msg("添加失败！", {icon: 2});
                            }
                        },
                        error: function () {
                            layer.msg("添加失败", { icon: 2 });
                        }
                    });
                }
                ,cancel: function () {
                    form.val("fabricAdd", { //formTest 即 class="layui-form" 所在元素属性 lay-filter="" 对应的值
                        "fabricNameAdd": '' // "name": "value"
                        ,"fabricNumberAdd": ''
                        ,"fabricNumberOrderAdd": ''
                        ,"fabricWidth": ''
                        ,"fabricWeight": ''
                        ,"fabricContent": ''
                        ,"unit": ''
                        ,"priceTwo": ''
                        ,"unitTwo": ''
                        ,"ratio": ''
                        // ,"supplier": ''
                        ,"remark": ''
                    });
                }
            });
            form.render('select');
            $.ajax({
                url: "/erp/getallcolormanage",
                type: 'GET',
                data: {},
                success: function (res) {
                    if (res.data) {
                        colorNameData = res.data;
                        var thisData = [];
                        $.each(res.data, function(index,element){
                            if (data.fabricColor == element.colorName && data.fabricColorNumber == element.colorNumber){
                                thisData.push({name:element.colorNumber + "/" + element.colorName, value: element.id, selected: true});
                            } else {
                                thisData.push({name:element.colorNumber + "/" + element.colorName, value: element.id});
                            }
                        });
                        colorSelect = xmSelect.render({
                            el: '#colorInfo',
                            radio: true,
                            filterable: true,
                            height: '200px',
                            data: thisData
                        });
                    }
                },
                error: function () {
                    layer.msg("获取版单失败", { icon: 2 });
                }
            });
            $.ajax({
                url: "/erp/getallsupplierinfo",
                data: {
                    supplierType: "面料"
                },
                async:false,
                success:function(res){
                    if (res.data){
                        var resultData = [];
                        $.each(res.data, function(index,element){
                            if (element != null && element != ''){
                                if (element.supplierName === data.supplier){
                                    resultData.push({name:element.supplierName, selected: true, value:element.supplierName});
                                } else {
                                    resultData.push({name:element.supplierName, value:element.supplierName});
                                }
                            }
                        });
                        supplierDemo = xmSelect.render({
                            el: '#supplier',
                            radio: true,
                            filterable: true,
                            data: resultData
                        });
                    }
                }, error:function(){
                }
            });
            $.ajax({
                url: "/erp/getallclothesdevfabricnumber",
                type: 'GET',
                data: {},
                success: function (res) {
                    if (res.data) {
                        $("#fabricNumberAdd").empty();
                        $("#fabricNumberAdd").append("<option value=''>选择代码</option>");
                        $.each(res.data, function(index,element){
                            if (element.fabricNumberCode == data.fabricNumber.slice(0,3)){
                                $("#fabricNumberAdd").append("<option value='"+element.fabricNumberCode+"' selected>"+element.fabricNumberCode + "/" + element.fabricNumberName +"</option>")
                            } else {
                                $("#fabricNumberAdd").append("<option value='"+element.fabricNumberCode+"'>"+element.fabricNumberCode + "/" + element.fabricNumberName +"</option>")
                            }
                        });
                        form.render();
                    }
                },
                error: function () {
                    layer.msg("获取版单失败", { icon: 2 });
                }
            });
            form.on('select(demo)', function(data){
                var fabricNumberCode = $("#fabricNumberAdd").val();
                if (fabricNumberCode != null && fabricNumberCode != ''){
                    $.ajax({
                        url: "/erp/getfabricnameorderbyprefix",
                        data: {
                            fabricNumberCode: fabricNumberCode
                        },
                        success:function(data){
                            if (data.fabricOrder){
                                $("#fabricNumberOrderAdd").val(data.fabricOrder);
                            }
                        }, error:function(){
                        }
                    });
                    form.render('select');
                }
            });
            form.val("fabricAdd", { //formTest 即 class="layui-form" 所在元素属性 lay-filter="" 对应的值
                "fabricNameAdd": data.fabricName // "name": "value"
                ,"fabricNumberAdd": data.fabricNumber.slice(0,3)
                ,"fabricNumberOrderAdd": data.fabricNumber.slice(3)
                ,"fabricWidth": data.fabricWidth
                ,"fabricWeight": data.fabricWeight
                ,"fabricContent": data.fabricContent
                ,"unit": data.unit
                ,"priceTwo": data.priceTwo
                ,"unitTwo": data.unitTwo
                ,"ratio": data.ratio
                // ,"supplier": data.supplier
                ,"remark": data.remark
            });
        } else if(obj.event === 'update'){
            var index = layer.open({
                type: 1 //Page层类型
                , title: "修改"
                , btn: ['保存']
                , area: ['1000px','500px']
                , shade: 0.6 //遮罩透明度
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , content: $("#fabricOperate")
                ,yes: function(i, layero){
                    var param = {};
                    param.id = data.id;
                    param.fabricName = $("#fabricNameAdd").val();
                    param.fabricNumber = $("#fabricNumberAdd").val() + $("#fabricNumberOrderAdd").val();
                    param.fabricWidth = $("#fabricWidth").val();
                    param.fabricWeight = $("#fabricWeight").val();
                    param.fabricContent = $("#fabricContent").val();
                    var fabricColor = colorSelect.getValue('name')[0];
                    param.fabricColorNumber = fabricColor.split("/")[0];
                    param.fabricColor = fabricColor.split("/")[1];
                    param.unit = $("#unit").val();
                    param.priceTwo = $("#priceTwo").val();
                    param.unitTwo = $("#unitTwo").val();
                    param.ratio = $("#ratio").val();
                    param.supplier = supplierDemo.getValue('nameStr');
                    param.remark = $("#remark").val();
                    param.storageType = "index";
                    if (param.ratio == null || param.ratio == '' || param.ratio == 0){
                        param.ratio = 1;
                    }
                    param.price = toDecimal(param.priceTwo / param.ratio);
                    if (param.fabricName == null || param.fabricName === '' || param.fabricNumber == null || param.fabricNumber === '' || param.unit == null || param.unit === '' || param.price == null || param.price === '' || param.fabricColorNumber == null || param.fabricColorNumber === '' || param.fabricColor == null || param.fabricColor === ''){
                        layer.msg("请输入完整必要信息~~~");
                        return false;
                    }
                    $.ajax({
                        url: "/erp/updateclothesdevfabricmanagetotal",
                        type: 'POST',
                        data: {
                            clothesDevFabricManageJson: JSON.stringify(param),
                            id: data.id
                        },
                        success: function (res) {
                            if (res.result == 0) {
                                layer.close(index);
                                layer.msg("添加成功！", {icon: 1});
                                initTable('', '');
                            } else {
                                layer.msg("添加失败！", {icon: 2});
                            }
                        },
                        error: function () {
                            layer.msg("添加失败", { icon: 2 });
                        }
                    });
                }
            });
            form.render('select');
            $.ajax({
                url: "/erp/getallcolormanage",
                type: 'GET',
                data: {},
                success: function (res) {
                    if (res.data) {
                        colorNameData = res.data;
                        var thisData = [];
                        $.each(res.data, function(index,element){
                            if (data.fabricColor == element.colorName && data.fabricColorNumber == element.colorNumber){
                                thisData.push({name:element.colorNumber + "/" + element.colorName, value: element.id, selected: true});
                            } else {
                                thisData.push({name:element.colorNumber + "/" + element.colorName, value: element.id});
                            }
                        });
                        colorSelect = xmSelect.render({
                            el: '#colorInfo',
                            radio: true,
                            filterable: true,
                            height: '200px',
                            data: thisData
                        });
                    }
                },
                error: function () {
                    layer.msg("获取版单失败", { icon: 2 });
                }
            });
            $.ajax({
                url: "/erp/getallsupplierinfo",
                data: {
                    supplierType: "面料"
                },
                async:false,
                success:function(res){
                    if (res.data){
                        var resultData = [];
                        $.each(res.data, function(index,element){
                            if (element != null && element != ''){
                                if (element.supplierName === data.supplier){
                                    resultData.push({name:element.supplierName, selected: true, value:element.supplierName});
                                } else {
                                    resultData.push({name:element.supplierName, value:element.supplierName});
                                }
                            }
                        });
                        supplierDemo = xmSelect.render({
                            el: '#supplier',
                            radio: true,
                            filterable: true,
                            data: resultData
                        });
                    }
                }, error:function(){
                }
            });
            $.ajax({
                url: "/erp/getallclothesdevfabricnumber",
                type: 'GET',
                data: {},
                success: function (res) {
                    if (res.data) {
                        $("#fabricNumberAdd").empty();
                        $("#fabricNumberAdd").append("<option value=''>选择代码</option>");
                        $.each(res.data, function(index,element){
                            if (element.fabricNumberCode == data.fabricNumber.slice(0,3)){
                                $("#fabricNumberAdd").append("<option value='"+element.fabricNumberCode+"' selected>"+element.fabricNumberCode + "/" + element.fabricNumberName +"</option>")
                            } else {
                                $("#fabricNumberAdd").append("<option value='"+element.fabricNumberCode+"'>"+element.fabricNumberCode + "/" + element.fabricNumberName +"</option>")
                            }
                        });
                        form.render();
                    }
                },
                error: function () {
                    layer.msg("获取版单失败", { icon: 2 });
                }
            });
            form.on('select(demo)', function(data){
                var fabricNumberCode = $("#fabricNumberAdd").val();
                if (fabricNumberCode != null && fabricNumberCode != ''){
                    $.ajax({
                        url: "/erp/getfabricnameorderbyprefix",
                        data: {
                            fabricNumberCode: fabricNumberCode
                        },
                        success:function(data){
                            if (data.fabricOrder){
                                $("#fabricNumberOrderAdd").val(data.fabricOrder);
                            }
                        }, error:function(){
                        }
                    });
                    form.render('select');
                }
            });
            form.val("fabricAdd", { //formTest 即 class="layui-form" 所在元素属性 lay-filter="" 对应的值
                "fabricNameAdd": data.fabricName // "name": "value"
                // ,"fabricNumberAdd": data.fabricNumber.slice(0,3)
                ,"fabricNumberOrderAdd": data.fabricNumber.slice(3)
                ,"fabricWidth": data.fabricWidth
                ,"fabricWeight": data.fabricWeight
                ,"fabricContent": data.fabricContent
                ,"unit": data.unit
                ,"priceTwo": data.priceTwo
                ,"unitTwo": data.unitTwo
                ,"ratio": data.ratio
                // ,"supplier": data.supplier
                ,"remark": data.remark
            });
        }
    });
});

function toDecimal(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*100)/100;
    return f;
}

function moveCursor(event,obj) {
    if(event.keyCode==38){
        var name = $(obj).attr("name");
        var index = $("input[name='"+name+"']").index(obj);
        $("input[name='"+name+"']").eq(index-1).focus();
    }else if(event.keyCode==40){
        var name = $(obj).attr("name");
        var index = $("input[name='"+name+"']").index(obj);
        $("input[name='"+name+"']").eq(index+1).focus();
    }
}
