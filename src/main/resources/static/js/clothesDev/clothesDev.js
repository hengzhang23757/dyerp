var $colorSel,sizeSelect,colorSelect,colorRender;
var colorSelectValue = [];
var userName = $("#userName").val();
var userRole = $("#userRole").val();
var form;
layui.use('form', function(){
    form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
    form.render();
});
var pageWidth = 1500;
var sizeNameData = [];
var colorNameData = [];
$(document).ready(function () {

    pageWidth = $(document.body).width() - 10;
    $colorSel = $('#colorSelect').selectize({
        plugins: ['remove_button'],
        maxItems: null,
        persist: false,
        create: true,
        valueField: 'id',
        labelField: 'title',
        searchField: ['title']
    });
    layui.laydate.render({
        elem: '#deadLine',
        trigger: 'click'
    });
    layui.laydate.render({
        elem: '#deliveryDate',
        trigger: 'click'
    });
    lay('#version').html('-v' + layui.laydate.v);
    $Date = layui.laydate;
    renderSeasonDate(document.getElementById('season'), 1);
    initDateForm();
    $.ajax({
        url: "/erp/getallsizenamebygroup",
        type: 'GET',
        data: {
        },
        success: function (res) {
            if (res.size) {
                sizeNameData = res.size;
                var data=[];
                for(var key in res.size) {
                    var parent = {};
                    parent.name = key;
                    parent.value = key;
                    var children=[];
                    var childrenSizeList = res.size[key];
                    childrenSizeList.sort(function(a,b){
                        // order是规则  objs是需要排序的数组
                        var order = ["XXXXS","4XS","XXXS","3XS","XXS","2XS","xxs","XS","xs","S","s","M","m","L","l","XL","xl","XXL","2XL","xxl","XXXL","3XL","XXXXL","4XL","XXXXXL","5XL","xxxl","070","70","073","075","75","080","80","085","090","90","095","95", "100", "105","110","115", "120","125", "130","135",
                            "140","145", "150","155", "160","165", "170","175", "180","185","190","195","200","XXS/155/72A","XS/160/76A","S/165/80A","M/170/84A","L/175/88A","XL/180/92A","XXL/185/96A","XXXL/190/100A","XXXXL/190/104A","XXS/145/72A","XS/150/76A","S/155/80A","M/160/84A","L/165/88A","XL/170/92A","XXL/175/96A","XXXL/180/100A","XXXXL/180/104A","XXXXXL/185/108A","110/56","120/60","130/64","140/68","150/72","160/76","170/80","2T","3T","4T","5T","4/5","6/6X","7/8","10/12","04(110-56)","06(120-60)","08(130-64)","10(140-64)","12(150-72)","14(155-76)","18M/73","24M/80","3Y/90","4Y/100","5Y/110","6Y/120","7Y/130","9Y/140","11Y/150","13Y/160"];
                        return order.indexOf(a) - order.indexOf(b);
                    });
                    $.each(childrenSizeList,function (index,item) {
                        children.push({
                            name:item,
                            value:item
                        })
                    });
                    parent.children = children;
                    data.push(parent);
                }
                sizeSelect = xmSelect.render({
                    el: '#sizeInfo',
                    autoRow: true,
                    cascader: {
                        show: true,
                        indent: 150
                    },
                    height: '200px',
                    data:data
                });
                $(".xm-body-cascader").css("overflow-y", "auto");
            }
        },
        error: function () {
            layer.msg("获取尺码信息失败", { icon: 2 });
        }
    });
    $("#stepForm").hide();
});
$(document).on('keydown', '.layui-table-edit', function (e) {
    var td = $(this).parent('td')
    ,tr = td.parent('tr')
    ,trs = tr.parent().parent().find('tr')
    ,tr_index = tr.index()
    ,td_index = td.index()
    ,td_last_index = tr.find('[data-edit="text"]:last').index()
    ,td_first_index = tr.find('[data-edit="text"]:first').index();
    switch (e.keyCode) {
        case 13:
            tr.next().find('td').eq(td_index).click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            return false;
        case 39:
            td.nextAll('[data-edit="text"]:first').click();
            if (td_index == td_last_index){
                tr.next().find('td').eq(td_first_index).click();
                if (tr_index == trs.length - 1){
                    trs.eq(0).find('td').eq(td_first_index).click();
                }
            }
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
        case 37:
            td.prevAll('[data-edit="text"]:first').click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
        case 38:
            tr.prev().find('td').eq(td_index).click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
        case 40:
            tr.next().find('td').eq(td_index).click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
    }

});
layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.33'
}).extend({                         // 模块别名
    soulTable: 'soulTable'
});
var clothesDevTable,fabricTable,accessoryTable;
layui.use(['form', 'soulTable', 'table', 'upload','yutons_sug', 'element'], function () {

    var table = layui.table,
        soulTable = layui.soulTable,
        element = layui.element,
        form = layui.form,
        $ = layui.$;

    clothesDevTable = table.render({
        elem: '#clothesDevTable'
        ,cols: [[]]
        ,loading:true
        ,data:[]
        ,height: 'full-80'
        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
        ,defaultToolbar: ['filter', 'print']
        ,title: '开发总表'
        ,totalRow: true
        ,page: true
        ,even: true
        ,overflow: 'tips'
        ,limits: [50, 100, 200]
        ,limit: 50 //每页默认显示的数量
        ,done: function (res, curr, count) {
            soulTable.render(this);
            var divArr = $(".layui-table-total div.layui-table-cell");
            $.each(divArr,function (index,item) {
                var _div = $(item);
                var content = _div.html();
                content = content.replace(".00","");
                _div.html(content);
            });
        }
        ,filter: {
            bottom: true,
            items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
        }
    });

    initTable('','');

    function initTable(clothesDevNumber, orderName){
        var param = {};
        if (orderName != null && orderName != ''){
            param.orderName = orderName;
        }
        if (clothesDevNumber != null && clothesDevNumber != ''){
            param.clothesDevNumber = clothesDevNumber;
        }
        $.ajax({
            url: "/erp/getclothesdevinfobymap",
            type: 'GET',
            data: param,
            success: function (res) {
                if (res.data) {
                    var reportData = res.data;
                    table.render({
                        elem: '#clothesDevTable'
                        ,cols:[[
                            {type:'radio'}
                            ,{type:'numbers', align:'center', title:'序号', width:60}
                            ,{field:'clothesDevNumber', title:'开发号', align:'center', width:120, sort: true, filter: true, totalRowText: '合计'}
                            ,{field:'orderName', title:'款号', align:'center', width:120, sort: true, filter: true, totalRowText: '合计'}
                            ,{field:'clothesYear', title:'年份', align:'center', width:90, sort: true, filter: true}
                            ,{field:'season', title:'季度', align:'center', width:90, sort: true, filter: true, totalRow: true}
                            ,{field:'styleCategory', title:'类别', align:'center',width:120, sort: true, filter: true}
                            ,{field:'styleName', title:'款式', align:'center',width:180, sort: true, filter: true}
                            ,{field:'designer', title:'设计师', align:'center', width:90, sort: true, filter: true}
                            ,{field:'designDepartName', title:'设计部', align:'center', width:120, sort: true, filter: true}
                            ,{field:'stylePattern', title:'版类', align:'center', width:120, sort: true, filter: true}
                            ,{field:'updateTime', title:'最后修改', align:'center', width:120, sort: true, filter: true, templet:function (d) {
                                    return moment(d.updateTime).format("YYYY-MM-DD");
                                }}
                            ,{fixed: 'right', title:'操作', align:'center', toolbar: '#barTop', width:240}
                        ]]
                        ,data:reportData
                        ,height: 'full-80'
                        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                        ,title: '制单总表'
                        ,totalRow: true
                        ,page: true
                        ,even: true
                        ,overflow: 'tips'
                        ,limits: [50, 100, 200]
                        ,limit: 50 //每页默认显示的数量
                        ,done: function (res, curr, count) {
                            soulTable.render(this);
                            var divArr = $(".layui-table-total div.layui-table-cell");
                            $.each(divArr,function (index,item) {
                                var _div = $(item);
                                var content = _div.html();
                                content = content.replace(".00","");
                                _div.html(content);
                            });
                        }
                    });
                }
            }
        })
    }

    form.on('submit(searchBeat)', function(data){
        var filterClothesDevNumber = $("#searchClothesDevNumber").val();
        var filterOrderName = $("#searchOrderName").val();
        initTable(filterClothesDevNumber, filterOrderName);
        return false;
    });

    table.on('toolbar(clothesDevTable)', function(obj){
        if (obj.event === 'clearFilter') {
            // 清除所有筛选条件并重载表格
            // 参数: tableId
            soulTable.clearFilter('clothesDevTable');
        }else if (obj.event === 'refresh') {
            initTable('', '', '', 0);
        } else if (obj.event === 'exportExcel') {
            soulTable.export('processOrderTable');
        }else if(obj.event === 'addNew') {  //录入基础信息
            basicOrderInfo(0, '', '');
        }
    });

    //监听面料信息按钮
    table.on('tool(fabricTable)', function(obj){
        var data = table.cache.fabricTable;
        if(obj.event === 'del'){
            obj.del();
            $.each(data,function (index,item) {
                if(item instanceof Array) {
                    data.splice(index,1);
                }
            });
            table.reload("fabricTable",{
                data:data   // 将新数据重新载入表格
            })
        } else if(obj.event === 'add') {
            data.push({
                "devFabricName": ""
            });

            table.reload("fabricTable", {
                data: data   // 将新数据重新载入表格
            });
        }
    });

    //监听行工具事件
    table.on('tool(clothesDevTable)', function(obj){
        var data = obj.data;
        var thisOrderName = data.orderName;
        var thisClothesDevNumber = data.clothesDevNumber;
        if(obj.event === 'update'){
            basicOrderInfo(1, data.clothesDevNumber, data.orderName);
        } else if (obj.event === 'detail'){
            processOrderDetail(data.clothesDevNumber);
        } else if (obj.event === 'change'){
            var changeColorSel,changeSizeSel,changeSizeNameDate,changeColorNameData,clothesDevInfo;
            var changeSelectSizeData = [];
            var changeSelectColorData = [];
            var index = layer.open({
                type: 1 //Page层类型
                , title: "转生产"
                , btn: ['保存','汇总']
                , shade: 0.6 //遮罩透明度
                , area: '1000px'
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , content: $("#clothesDevChange")
                ,yes: function(i, layero){
                    var customerName = $("#changeCustomerName").val();
                    var orderName = $("#changeOrderName").val();
                    var deliveryDate = $("#changeDelivery").val();
                    var serialNumber = "001";
                    var brandName = customerName;
                    if (customerName == null || customerName == '' || brandName == null || brandName == '' || orderName == null || orderName == '' || deliveryDate == null || deliveryDate == ''){
                        layer.msg("客户-客户款号-商标-交期必须~");
                        return false;
                    }
                    var colorSizeData = table.cache.colorSizeTable;
                    if (!colorSizeData || colorSizeData.length == 0){
                        layer.msg("请填写下单数据~~");
                        return false;
                    }
                    var colorSizeList = [];
                    var changeSelectSizeData = changeSizeSel.getValue('name');
                    var changeSelectColorData = changeColorSel.getValue('name');
                    var selectColorList = [];
                    var selectSizeList = [];
                    $.each(changeSelectSizeData,function (index, item) {
                        selectSizeList.push(item);
                    });
                    $.each(changeSelectColorData,function (index, item) {
                        selectColorList.push(item.split("/")[1]);
                    });
                    var colorInfo = selectColorList.join(",");
                    var sizeInfo = selectSizeList.join(",");
                    $.each(colorSizeData,function (index, item) {
                        $.each(changeSelectSizeData,function (s_index,size) {
                            var orderClothes = {};
                            if (item.color != "合计"){
                                orderClothes.colorName = item.color.split("/")[1];
                                orderClothes.colorNumber = item.color.split("/")[0];
                                orderClothes.userName = userName;
                                orderClothes.orderName = orderName;
                                orderClothes.serialNumber = serialNumber;
                                orderClothes.clothesVersionNumber = thisOrderName;
                                orderClothes.styleDescription = clothesDevInfo.styleName;
                                orderClothes.season = clothesDevInfo.season;
                                orderClothes.deadLine = moment(new Date()).format("YYYY-MM-DD");
                                orderClothes.deliveryDate = deliveryDate;
                                orderClothes.sizeName = size;
                                orderClothes.customerName = customerName;
                                orderClothes.purchaseMethod = "FOB";
                                orderClothes.count = item[size];
                                orderClothes.orderState = 0;
                                colorSizeList.push(orderClothes);
                            }
                        })
                    });
                    var load = layer.load();
                    $.ajax({
                        url: "/erp/changeclothesdevtoorder",
                        type: 'POST',
                        data: {
                            clothesDevNumber: thisOrderName,
                            orderName: orderName,
                            serialNumber: serialNumber,
                            brandName: brandName,
                            customerName: customerName,
                            initClothesDevNumber: thisClothesDevNumber,
                            orderClothesJson: JSON.stringify(colorSizeList),
                            colorJson: JSON.stringify(selectColorList),
                            sizeJson: JSON.stringify(selectSizeList),
                            colorInfo: colorInfo,
                            sizeInfo: sizeInfo,
                            deliveryDate: deliveryDate,
                            buyer: userName
                        },
                        success: function (res) {
                            if (res.result == 0) {
                                layer.close(load);
                                layer.msg("提交成功,详情信息请在生产制单模块查看！", { icon: 1 });
                                layer.close(index);
                            } else {
                                layer.close(load);
                                layer.msg("提交失败！", { icon: 2 });
                            }
                        },
                        error: function () {
                            layer.close(load);
                            layer.msg("提交失败！", { icon: 2 });
                        }
                    });
                    return false;
                }
                ,btn2: function (i, layero) {
                    var tmpData = table.cache.colorSizeTable;
                    // 构造一个空的含有尺码的对象
                    var sizeTmp = {color:'合计'};
                    $.each(changeSelectSizeData,function(index,value){
                        sizeTmp[value] = 0;
                    });
                    sizeTmp.colorSum = 0;
                    // 遍历不是合计行累加
                    $.each(tmpData,function(index,item){
                        if (item['color'] != '合计'){
                            $.each(changeSelectSizeData,function(s_i, s_item){
                                if (item[s_item]){
                                    sizeTmp[s_item] += Number(item[s_item]);
                                }
                            });
                        }
                    });
                    // 把累加的赋值到合计
                    $.each(tmpData,function(index,item){
                        if (item['color'] == '合计'){
                            $.each(changeSelectSizeData,function(s_i, s_item){
                                item[s_item] = sizeTmp[s_item];
                            });
                        }
                    });
                    $.each(tmpData,function(index,item){
                        var colorSum = 0;
                        for (var i = 0; i < changeSelectSizeData.length; i ++){
                            var sizeName = changeSelectSizeData[i];
                            if (item[sizeName]){
                                colorSum += Number(item[sizeName]);
                            }
                        }
                        item.colorSum = colorSum;
                    });
                    table.reload("colorSizeTable",{
                        data:tmpData   // 将新数据重新载入表格
                    });
                    return false;
                }
            });
            layer.full(index);
            form.render('select');
            setTimeout(function () {
                $.ajax({
                    url: "/erp/getallsizenamebygroup",
                    type: 'GET',
                    data: {
                    },
                    success: function (res) {
                        if (res.size) {
                            changeSizeNameDate = res.size;
                            var data=[];
                            for(var key in res.size) {
                                var parent = {};
                                parent.name = key;
                                parent.value = key;
                                var children=[];
                                $.each(res.size[key],function (index,item) {
                                    children.push({
                                        name:item,
                                        value:item
                                    })
                                });
                                parent.children = children;
                                data.push(parent);
                            }
                            changeSizeSel = xmSelect.render({
                                el: '#changeSizeInfo',
                                autoRow: true,
                                cascader: {
                                    show: true,
                                    indent: 150
                                },
                                height: '200px',
                                data:data
                            });
                        }
                    },
                    error: function () {
                        layer.msg("获取尺码信息失败", { icon: 2 });
                    }
                });
                $.ajax({
                    url: "/erp/getallcolormanage",
                    type: 'GET',
                    data: {},
                    success: function (res) {
                        if (res.data) {
                            changeColorNameData = res.data;
                            var thisData = [];
                            $.each(res.data, function(index,element){
                                thisData.push({name:element.colorNumber + "/" + element.colorName, value: element.id});
                            });
                            changeColorSel = xmSelect.render({
                                el: '#changeColorInfo',
                                filterable: true,
                                height: '200px',
                                data: thisData
                            });
                            $(".xm-body-cascader").css("overflow-y", "auto");
                        }
                    },
                    error: function () {
                        layer.msg("获取版单失败", { icon: 2 });
                    }
                });
                $.ajax({
                    url: "/erp/getclothesdevtotalbyclothesdevnumber",
                    type: 'GET',
                    data: {
                        clothesDevNumber: thisClothesDevNumber
                    },
                    success: function (res) {
                        if (res.clothesDevImage) {
                            var clothesDevSizeList = res.clothesDevSizeList;
                            var clothesDevColorList = res.clothesDevColorList;
                            clothesDevInfo = res.clothesDevInfo;
                            // 赋值size
                            var sizeData=[];
                            for(var key in sizeNameData) {
                                var parent = {};
                                parent.name = key;
                                parent.value = key;
                                var children=[];
                                $.each(sizeNameData[key],function (index,item) {
                                    var tmp = {name: item, value: item};
                                    $.each(clothesDevSizeList,function (index_s,item_s) {
                                        if (item == item_s.sizeName){
                                            tmp.selected = true;
                                        }
                                    });
                                    children.push(tmp);
                                });
                                parent.children = children;
                                sizeData.push(parent);
                            }
                            changeSizeSel.update({
                                data: sizeData
                            });
                            // 赋值color
                            var thisData = [];
                            $.each(changeColorNameData, function(index,element){
                                var tmp= {name:element.colorNumber + "/" + element.colorName, value: element.id};
                                $.each(clothesDevColorList, function(index_c,element_c){
                                    if (element.colorNumber == element_c.colorNumber && element.colorName == element_c.colorName){
                                        tmp.selected = true;
                                    }
                                });
                                thisData.push(tmp);
                            });
                            changeColorSel.update({
                                data: thisData
                            });
                            form.render();
                            $("#thisDevOrderName").val(res.clothesDevInfo.orderName);
                        }
                    },
                    error: function () {
                        layer.msg("获取版单失败", { icon: 2 });
                    }
                });
                layui.use(['yutons_sug'], function () {
                    layui.yutons_sug.render({
                        id: "changeCustomerCode", //设置容器唯一id
                        height: "300",
                        width: "550",
                        limit:"10",            limits:[10,20,50,100],
                        cols: [
                            [{
                                field: 'changeCustomerCode',
                                title: '客户编号',
                                align: 'left'
                            }, {
                                field: 'changeCustomerName',
                                title: '客户名',
                                align: 'left'
                            }]
                        ], //设置表头
                        params: [
                            {
                                name: 'changeCustomerCode',
                                field: 'changeCustomerCode'
                            }, {
                                name: 'changeCustomerName',
                                field: 'changeCustomerName'
                            }],//设置字段映射，适用于输入一个字段，回显多个字段
                        type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
                        url: "/erp/getcustomerhint?keyWord=" //设置异步数据接口,url为必填项,params为字段名
                    });

                    layui.yutons_sug.render({
                        id: "changeCustomerName", //设置容器唯一id
                        height: "300",
                        width: "550",
                        limit:"10",            limits:[10,20,50,100],
                        cols: [
                            [{
                                field: 'changeCustomerCode',
                                title: '客户编号',
                                align: 'left'
                            }, {
                                field: 'changeCustomerName',
                                title: '客户名',
                                align: 'left'
                            }]
                        ], //设置表头
                        params: [
                            {
                                name: 'changeCustomerCode',
                                field: 'changeCustomerCode'
                            }, {
                                name: 'changeCustomerName',
                                field: 'changeCustomerName'
                            }],//设置字段映射，适用于输入一个字段，回显多个字段
                        type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
                        url: "/erp/getcustomerhint?keyWord=" //设置异步数据接口,url为必填项,params为字段名
                    });
                });
                layui.laydate.render({
                    elem: '#changeDelivery',
                    trigger: 'click'
                });

                // $.ajax({
                //     url: "/erp/getserialnumberbydate",
                //     type: 'GET',
                //     data: {},
                //     success: function (res) {
                //         if (res.serialNumber) {
                //             $("#serialNumber").val(res.serialNumber);
                //         }
                //     },
                //     error: function () {
                //         layer.msg("获取版单失败", { icon: 2 });
                //     }
                // });

                // $("#changeCustomerCode").bind('blur change',function(){
                //     var changeCustomerCode = $("#changeCustomerCode").val();
                //     $("#changeOrderName").val(changeCustomerCode + $("#thisDevOrderName").val());
                // });
                //
                // $("#changeCustomerName").bind('blur change',function(){
                //     var changeCustomerCode = $("#changeCustomerCode").val();
                //     $("#changeOrderName").val(changeCustomerCode + $("#thisDevOrderName").val());
                // });
            }, 100);
            form.on('submit(searchBeatOrder)', function(data){
                changeSelectSizeData = changeSizeSel.getValue('name');
                changeSelectColorData = changeColorSel.getValue('name');
                var title = [
                    {field: 'color', title: '颜色/尺码',minWidth:100, fixed: true}
                ];
                $.each(changeSelectSizeData,function(index,value){
                    var field = {};
                    field.field = value;
                    field.title = value;
                    field.minWidth = 80;
                    field.edit = 'text';
                    title.push(field);
                });
                title.push({field: 'colorSum', title: '颜色合计',minWidth:100, fixed: 'right'});
                var colorSizeData = [];
                $.each(changeSelectColorData,function(index,value){
                    var tmp = {};
                    $.each(changeSelectSizeData,function(s_index, s_value){
                        tmp.color = value;
                        tmp.colorSum = 0;
                        tmp[s_value] = '';
                    });
                    colorSizeData.push(tmp);
                });
                var sizeTmp = {color:'合计'};
                $.each(changeSelectSizeData,function(index,value){
                    sizeTmp[value] = 0;
                });
                sizeTmp.colorSum = 0;
                colorSizeData.push(sizeTmp);
                colorSizeTable = table.render({
                    elem: '#colorSizeTable'
                    ,cols: [title]
                    ,data: colorSizeData
                    ,done:function () {

                    }
                });
                return false;
            });
        } else if(obj.event === 'del'){
            layer.confirm('确认删除吗?', function(index){
                $.ajax({
                    url: "/erp/deleteclothesdevtotal",
                    type: 'POST',
                    data: {
                        clothesDevNumber: thisClothesDevNumber
                    },
                    success: function (res) {
                        if (res.result == 0) {
                            layer.close(index);
                            layer.msg("删除成功！", {icon: 1});
                            initTable('','');
                        } else {
                            layer.msg("删除失败！", {icon: 2});
                        }
                    }, error: function () {
                        layer.msg("删除失败！", {icon: 2});
                    }
                })
            });
        }
    });

    function basicOrderInfo(type, clothesDevNumber, orderName) {
        var designDepartList = [];
        $.ajax({
            url: "/erp/getallcolormanage",
            type: 'GET',
            data: {},
            success: function (res) {
                if (res.data) {
                    colorNameData = res.data;
                    var thisData = [];
                    $.each(res.data, function(index,element){
                        thisData.push({name:element.colorNumber + "/" + element.colorName, value: element.id});
                    });
                    colorSelect = xmSelect.render({
                        el: '#colorInfo',
                        filterable: true,
                        height: '200px',
                        data: thisData,
                        on: function(data){
                            colorSelectValue = data.arr;
                        }
                    });
                    $(".xm-body-cascader").css("overflow-y", "auto");
                }
            },
            error: function () {
                layer.msg("获取版单失败", { icon: 2 });
            }
        });
        $.ajax({
            url: "/erp/getallstylemanage",
            type: 'GET',
            data: {},
            success: function (res) {
                if (res.data) {
                    $("#sizeSizeManage").empty();
                    $("#processSizeManage").empty();
                    $("#sizeSizeManage").append("<option value='0'>选择款式</option>");
                    $("#processSizeManage").append("<option value='0'>选择款式</option>");
                    $.each(res.data, function(index,element){
                        $("#sizeSizeManage").append("<option value='"+element.id+"'>"+element.styleNumber + "/" + element.styleName +"</option>");
                        $("#processSizeManage").append("<option value='"+element.id+"'>"+element.styleNumber + "/" + element.styleName +"</option>");
                    });
                    form.render();
                }
            },
            error: function () {
                layer.msg("获取版单失败", { icon: 2 });
            }
        });
        $.ajax({
            url: "/erp/getalldesigndepart",
            type: 'GET',
            data: {
            },
            success: function (res) {
                if (res.data) {
                    designDepartList = res.data;
                    $("#designDepart").empty();
                    $.each(res.data, function(index,element){
                        $("#designDepart").append("<option value='"+element.designDepartCode+"'>"+ element.designDepartName +"</option>");
                    });
                    form.render();
                }
            },
            error: function () {
                layer.msg("获取版单失败", { icon: 2 });
            }
        });
        setTimeout(function () {
            tinymce.init({
                selector: '#clothesImageTextarea',
                language: 'zh_CN',//中文
                placeholder: '试试复制好款式图直接在这里粘贴~~',
                directionality: 'ltr',  //文字方向,ltr文字方向从左到右，rtl从右到左
                browser_spellcheck: true,
                contextmenu: false,
                branding: false, //技术支持(Powered by Tiny || 由Tiny驱动)
                menubar: false, //菜单栏
                plugins: [
                    "print image autoresize table powerpaste"
                ],
                powerpaste_word_import: 'propmt',// 参数可以是propmt, merge, clear，效果自行切换对比
                powerpaste_html_import: 'propmt',// propmt, merge, clear
                powerpaste_allow_local_images: true,
                paste_data_images: true,
                relative_urls : false,
                remove_script_host : false,
                convert_urls : true,
                toolbar: "print | undo redo | bold | alignleft aligncenter alignright alignjustify | fontselect | fontsizeselect | image table",
                images_upload_handler: function(blobInfo, success, failure){
                    handleImgUpload(blobInfo, success, failure)
                }
            });
        },100);
        setTimeout(function () {
            tinymce.init({
                selector: '#sizeImageTextarea',
                language: 'zh_CN',//中文
                height: '800',
                placeholder: '试试在上方根据款式选择尺寸模板,在模板的基础上进行微调~~',
                directionality: 'ltr',  //文字方向,ltr文字方向从左到右，rtl从右到左
                browser_spellcheck: true,
                contextmenu: false,
                branding: false, //技术支持(Powered by Tiny || 由Tiny驱动)
                menubar: false, //菜单栏
                plugins: [
                    "print image autoresize table powerpaste"
                ],
                powerpaste_word_import: 'propmt',// 参数可以是propmt, merge, clear，效果自行切换对比
                powerpaste_html_import: 'propmt',// propmt, merge, clear
                powerpaste_allow_local_images: true,
                paste_data_images: true,
                relative_urls : false,
                remove_script_host : false,
                convert_urls : true,
                toolbar: "print | undo redo | bold | alignleft aligncenter alignright alignjustify | fontselect | fontsizeselect | image table",
                images_upload_handler: function(blobInfo, success, failure){
                    handleImgUpload(blobInfo, success, failure)
                }
            });
        },100);
        setTimeout(function () {
            tinymce.init({
                selector: '#processImageTextarea',
                language: 'zh_CN',//中文
                height: '800',
                placeholder: '试试在上方根据款式选择工艺模板,在模板的基础上进行微调~~',
                directionality: 'ltr',  //文字方向,ltr文字方向从左到右，rtl从右到左
                browser_spellcheck: true,
                contextmenu: false,
                branding: false, //技术支持(Powered by Tiny || 由Tiny驱动)
                menubar: false, //菜单栏
                plugins: [
                    "print image autoresize table powerpaste"
                ],
                powerpaste_word_import: 'propmt',// 参数可以是propmt, merge, clear，效果自行切换对比
                powerpaste_html_import: 'propmt',// propmt, merge, clear
                powerpaste_allow_local_images: true,
                paste_data_images: true,
                relative_urls : false,
                remove_script_host : false,
                convert_urls : true,
                toolbar: "print | undo redo | bold | alignleft aligncenter alignright alignjustify | fontselect | fontsizeselect | image table",
                images_upload_handler: function(blobInfo, success, failure){
                    handleImgUpload(blobInfo, success, failure)
                }
            });
        },100);
        layui.use(['yutons_sug'], function () {
            layui.yutons_sug.render({
                id: "customerCode", //设置容器唯一id
                height: "300",
                width: "550",
                limit:"10",            limits:[10,20,50,100],
                cols: [
                    [{
                        field: 'customerCode',
                        title: '客户编号',
                        align: 'left'
                    }, {
                        field: 'customerName',
                        title: '客户名',
                        align: 'left'
                    }]
                ], //设置表头
                params: [
                    {
                        name: 'customerCode',
                        field: 'customerCode'
                    }, {
                        name: 'customerName',
                        field: 'customerName'
                    }],//设置字段映射，适用于输入一个字段，回显多个字段
                type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
                url: "/erp/getcustomerhint?keyWord=" //设置异步数据接口,url为必填项,params为字段名
            });

            layui.yutons_sug.render({
                id: "customerName", //设置容器唯一id
                height: "300",
                width: "550",
                limit:"10",            limits:[10,20,50,100],
                cols: [
                    [{
                        field: 'customerCode',
                        title: '客户编号',
                        align: 'left'
                    }, {
                        field: 'customerName',
                        title: '客户名',
                        align: 'left'
                    }]
                ], //设置表头
                params: [
                    {
                        name: 'customerCode',
                        field: 'customerCode'
                    }, {
                        name: 'customerName',
                        field: 'customerName'
                    }],//设置字段映射，适用于输入一个字段，回显多个字段
                type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
                url: "/erp/getcustomerhint?keyWord=" //设置异步数据接口,url为必填项,params为字段名
            });
        });
        layui.use(['yutons_sug'], function () {
            layui.yutons_sug.render({
                id: "styleCategory", //设置容器唯一id
                height: "300",
                width: "550",
                limit:"10",            limits:[10,20,50,100],
                cols: [
                    [{
                        field: 'styleCategory',
                        title: '款式类别',
                        align: 'center'
                    }, {
                        field: 'categoryCode',
                        title: '类别代码',
                        align: 'center'
                    },{
                        field: 'styleName',
                        title: '款式名称',
                        align: 'center'
                    }, {
                        field: 'styleNumber',
                        title: '款式编号',
                        align: 'center'
                    }]
                ], //设置表头
                params: [
                    {
                        name: 'styleCategory',
                        field: 'styleCategory'
                    }, {
                        name: 'categoryCode',
                        field: 'categoryCode'
                    },{
                        name: 'styleName',
                        field: 'styleName'
                    }, {
                        name: 'styleNumber',
                        field: 'styleNumber'
                    }],//设置字段映射，适用于输入一个字段，回显多个字段
                type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
                url: "/erp/getstylemanagehint?keyWord=" //设置异步数据接口,url为必填项,params为字段名
            });

            layui.yutons_sug.render({
                id: "categoryCode", //设置容器唯一id
                height: "300",
                width: "550",
                limit:"10",            limits:[10,20,50,100],
                cols: [
                    [{
                        field: 'styleCategory',
                        title: '款式类别',
                        align: 'center'
                    }, {
                        field: 'categoryCode',
                        title: '类别代码',
                        align: 'center'
                    },{
                        field: 'styleName',
                        title: '款式名称',
                        align: 'center'
                    }, {
                        field: 'styleNumber',
                        title: '款式编号',
                        align: 'center'
                    }]
                ], //设置表头
                params: [
                    {
                        name: 'styleCategory',
                        field: 'styleCategory'
                    }, {
                        name: 'categoryCode',
                        field: 'categoryCode'
                    },{
                        name: 'styleName',
                        field: 'styleName'
                    }, {
                        name: 'styleNumber',
                        field: 'styleNumber'
                    }],//设置字段映射，适用于输入一个字段，回显多个字段
                type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
                url: "/erp/getstylemanagehint?keyWord=" //设置异步数据接口,url为必填项,params为字段名
            });

            layui.yutons_sug.render({
                id: "styleName", //设置容器唯一id
                height: "300",
                width: "550",
                limit:"10",            limits:[10,20,50,100],
                cols: [
                    [{
                        field: 'styleCategory',
                        title: '款式类别',
                        align: 'center'
                    }, {
                        field: 'categoryCode',
                        title: '类别代码',
                        align: 'center'
                    },{
                        field: 'styleName',
                        title: '款式名称',
                        align: 'center'
                    }, {
                        field: 'styleNumber',
                        title: '款式编号',
                        align: 'center'
                    }]
                ], //设置表头
                params: [
                    {
                        name: 'styleCategory',
                        field: 'styleCategory'
                    }, {
                        name: 'categoryCode',
                        field: 'categoryCode'
                    },{
                        name: 'styleName',
                        field: 'styleName'
                    }, {
                        name: 'styleNumber',
                        field: 'styleNumber'
                    }],//设置字段映射，适用于输入一个字段，回显多个字段
                type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
                url: "/erp/getstylemanagehint?keyWord=" //设置异步数据接口,url为必填项,params为字段名
            });

            layui.yutons_sug.render({
                id: "styleNumber", //设置容器唯一id
                height: "300",
                width: "550",
                limit:"10",            limits:[10,20,50,100],
                cols: [
                    [{
                        field: 'styleCategory',
                        title: '款式类别',
                        align: 'center'
                    }, {
                        field: 'categoryCode',
                        title: '类别代码',
                        align: 'center'
                    },{
                        field: 'styleName',
                        title: '款式名称',
                        align: 'center'
                    }, {
                        field: 'styleNumber',
                        title: '款式编号',
                        align: 'center'
                    }]
                ], //设置表头
                params: [
                    {
                        name: 'styleCategory',
                        field: 'styleCategory'
                    }, {
                        name: 'categoryCode',
                        field: 'categoryCode'
                    },{
                        name: 'styleName',
                        field: 'styleName'
                    }, {
                        name: 'styleNumber',
                        field: 'styleNumber'
                    }],//设置字段映射，适用于输入一个字段，回显多个字段
                type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
                url: "/erp/getstylemanagehint?keyWord=" //设置异步数据接口,url为必填项,params为字段名
            });
        });
        layui.laydate.render({
            elem: '#requireDate',
            trigger: 'click'
        });
        layui.laydate.render({
            elem: '#expectDate',
            trigger: 'click'
        });
        layui.laydate.render({
            elem: '#clothesYear',
            trigger: 'click',
            type: 'year'
        });
        form.on('submit(searchBeatSize)', function(data){
            var thisId = $("#sizeSizeManage").val();
            $.ajax({
                url: "/erp/getstylemanagebyid",
                type: 'GET',
                data: {
                    id: thisId
                },
                success: function (res) {
                    if (res.data){
                        tinymce.get('sizeImageTextarea').setContent(res.data.sizeText);
                    }
                },
                error: function () {
                    layer.msg("获取尺寸模板失败！", {icon: 2});
                }
            });
            return false;
        });
        form.on('submit(searchBeatProcess)', function(data){
            var thisId = $("#processSizeManage").val();
            $.ajax({
                url: "/erp/getstylemanagebyid",
                type: 'GET',
                data: {
                    id: thisId
                },
                success: function (res) {
                    if (res.data){
                        tinymce.get('processImageTextarea').setContent(res.data.processText);
                    }
                },
                error: function () {
                    layer.msg("获取工艺模板失败！", {icon: 2});
                }
            });
            return false;
        });
        if (type == 0){
            var index = layer.open({
                type: 1 //Page层类型
                , title: "新增&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;面料单耗是指单位1的单耗,保存时会根据换算系数自动计算单位2的单耗~~"
                , btn: ['保存','获取款号']
                , shade: 0.6 //遮罩透明度
                , area: '1000px'
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , content: $("#clothesDevAdd")
                ,yes: function(i, layero){
                    var colorCombineList = [];
                    var colorList = [];
                    $.each(colorSelectValue,function (index, item) {
                        colorCombineList.push(item.name);
                    });
                    $.each(colorCombineList, function (c_index, c_item) {
                        var colorNumber = c_item.split('/')[0];
                        var colorName = c_item.split('/')[1];
                        colorList.push({colorNumber: colorNumber, colorName: colorName});
                    });
                    // 基础信息
                    var params = form.val("formClothsDevInfo");
                    params.orderName = params.devOrderName;
                    params.season = params.devSeason;
                    params.designDepartCode = params.designDepart;
                    params.designDepartName = $("#designDepart option:selected").text();
                    var infoFlag = false;
                    for (var key in params){
                        if (key != 'select' && params[key] == ''){
                            infoFlag = true;
                        }
                    }
                    if (infoFlag){
                        layer.msg("请输入完整基本信息");
                        return false;
                    }
                    // 颜色
                    var clothesDevColorList = colorList;
                    $.each(clothesDevColorList,function (index, item) {
                        item.clothesDevNumber = params.clothesDevNumber;
                        item.orderName = params.devOrderName;
                    });
                    if (clothesDevColorList.length == 0){
                        layer.msg("请选择颜色");
                        return false;
                    }
                    // 尺码
                    var sizeSelValue = sizeSelect.getValue('value');
                    var clothesDevSizeList = [];
                    $.each(sizeSelValue,function (index, item) {
                        var tmp = {};
                        tmp.clothesDevNumber = params.clothesDevNumber;
                        tmp.orderName = params.devOrderName;
                        tmp.sizeName = item;
                        clothesDevSizeList.push(tmp);
                    });
                    if (clothesDevSizeList.length == 0){
                        layer.msg("请选择尺码");
                        return false;
                    }
                    // 公仔图
                    var devImageOne = tinymce.get('clothesImageTextarea').getContent();
                    var clothesDevImage = {};
                    clothesDevImage.clothesDevNumber = params.clothesDevNumber;
                    clothesDevImage.orderName = params.devOrderName;
                    clothesDevImage.devImageOne = devImageOne;
                    // 工艺
                    var processImg = tinymce.get('processImageTextarea').getContent();
                    var clothesDevProcess = {};
                    clothesDevProcess.clothesDevNumber = params.clothesDevNumber;
                    clothesDevProcess.orderName = params.devOrderName;
                    clothesDevProcess.processImg = processImg;
                    // 尺寸
                    var measureImg = tinymce.get('sizeImageTextarea').getContent();
                    var clothesDevMeasure = {};
                    clothesDevMeasure.clothesDevNumber = params.clothesDevNumber;
                    clothesDevMeasure.orderName = params.devOrderName;
                    clothesDevMeasure.measureImg = measureImg;
                    // 面料
                    var fabricData = table.cache.fabricTable;
                    if (fabricData == null || fabricData.length == 0){
                        layer.msg("请输入面料信息");
                        return false;
                    }
                    var isHits = [];
                    var fabricColors = [];
                    var fabricColorNumberList = [];
                    $.each(fabricData,function (index,item) {
                        item.fabricName = item.devFabricName;
                        var selects = $("div[lay-id='fabricTable'] tr[data-index="+index+"] select[name='isHit']");
                        var inputs = $("div[lay-id='fabricTable'] tr[data-index="+index+"] input[name='fabricColor']");
                        var fabricColorNumbers = $("div[lay-id='fabricTable'] tr[data-index="+index+"] input[name='fabricColorNumber']");
                        var tmp = [];
                        var itmp = [];
                        var ctmp = [];
                        $.each(selects,function (s_index,s_item) {
                            tmp.push(selects.eq(s_index).val());
                            if(inputs.eq(s_index).val()!="") {
                                itmp.push(inputs.eq(s_index).val())
                            } else {
                                itmp.push("未输");
                            }
                            if(fabricColorNumbers.eq(s_index).val()!="") {
                                ctmp.push(fabricColorNumbers.eq(s_index).val())
                            }else{
                                ctmp.push('未输');
                            }
                        });
                        isHits.push(tmp);
                        fabricColors.push(itmp);
                        fabricColorNumberList.push(ctmp);
                    });
                    var clothesDevFabricList = [];
                    var isInput = true;
                    var pieceFlag = true;
                    var ratioFlag = true;
                    var unitFlag = true;
                    $.each(fabricColors,function (i,item) {
                        if(item.length<isHits[0].length) {
                            isInput = false;
                            return false;
                        }
                    });
                    if(isInput) {
                        $.each(fabricData, function (i, item) {
                            if (item.fabricName == null || item.unit == null || item.partName == null || item.pieceUsage == null || item.ratio == null || item.fabricName == '' || item.unit == '' || item.partName == '' || item.pieceUsage == '' || item.ratio == '') {
                                isInput = false;
                                return false;
                            }
                            var pieceUsage = 0;
                            var pieceUsageTwo = 0;
                            if (item.pieceUsage != ''){
                                if (!isNumber(item.pieceUsage)){
                                    pieceFlag = false;
                                } else {
                                    pieceUsage = item.pieceUsage;
                                }
                            }
                            if (item.ratio != ''){
                                if (!isNumber(item.ratio) || Number(item.ratio) === 0){
                                    ratioFlag = false;
                                } else {
                                    pieceUsageTwo = item.pieceUsage / Number(item.ratio);
                                }
                            }
                            $.each(colorList,function(j,value) {
                                var manufactureFabric = {};
                                manufactureFabric.fabricKey = i;
                                manufactureFabric.orderName = params.devOrderName;
                                manufactureFabric.clothesDevNumber = params.clothesDevNumber;
                                manufactureFabric.fabricNumber = item.fabricNumber;
                                manufactureFabric.fabricName = item.fabricName;
                                manufactureFabric.supplier = item.supplier;
                                manufactureFabric.fabricWidth = item.fabricWidth;
                                manufactureFabric.fabricWeight = item.fabricWeight;
                                manufactureFabric.fabricContent = item.fabricContent;
                                var tmpUnit = item.unit;
                                tmpUnit = tmpUnit.replace(/^\s*|\s*$/g,"");
                                manufactureFabric.unit = tmpUnit;
                                var tmpUnitTwo = item.unitTwo;
                                tmpUnitTwo = tmpUnitTwo.replace(/^\s*|\s*$/g,"");
                                manufactureFabric.unitTwo = tmpUnitTwo;
                                manufactureFabric.ratio = item.ratio;
                                manufactureFabric.partName = item.partName;
                                manufactureFabric.pieceUsage = pieceUsage;
                                manufactureFabric.pieceUsageTwo = pieceUsageTwo;
                                manufactureFabric.colorName = value.colorName;
                                manufactureFabric.colorNumber = value.colorNumber;
                                manufactureFabric.isHit = isHits[i][j];
                                manufactureFabric.fabricColor = fabricColors[i][j];
                                manufactureFabric.fabricColorNumber = fabricColorNumberList[i][j];
                                if (manufactureFabric.fabricColor != "未输"){
                                    clothesDevFabricList.push(manufactureFabric);
                                }
                            })
                        });
                    }
                    if (!ratioFlag){
                        layer.msg("换算有误");
                        return false;
                    }
                    if (clothesDevFabricList.length == 0){
                        layer.msg("请输入面料信息");
                        return false;
                    }
                    // 辅料
                    var accessoryData = table.cache.accessoryTable;
                    if (accessoryData == null || accessoryData.length == 0){
                        layer.msg("请输入辅料信息");
                        return false;
                    }
                    var clothesDevAccessoryList = [];
                    $.each(accessoryData,function (index,item) {
                        var accessory = item;
                        accessory.accessoryName = accessory.devAccessoryName;
                        accessory.clothesDevNumber = params.clothesDevNumber;
                        clothesDevAccessoryList.push(accessory);
                    });
                    if (clothesDevAccessoryList.length == 0){
                        layer.msg("请输入辅料信息");
                        return false;
                    }
                    $.ajax({
                        url: "/erp/addclothesdevtotal",
                        type: 'POST',
                        data: {
                            clothesDevInfoJson: JSON.stringify(params),
                            clothesDevImageJson: JSON.stringify(clothesDevImage),
                            clothesDevProcessJson: JSON.stringify(clothesDevProcess),
                            clothesDevMeasureJson: JSON.stringify(clothesDevMeasure),
                            clothesDevColorListJson: JSON.stringify(clothesDevColorList),
                            clothesDevSizeListJson: JSON.stringify(clothesDevSizeList),
                            clothesDevFabricListJson: JSON.stringify(clothesDevFabricList),
                            clothesDevAccessoryListJson: JSON.stringify(clothesDevAccessoryList)
                        },
                        success: function (res) {
                            if (res.result == 0) {
                                layer.msg("保存成功！", { icon: 1 });
                                layer.close(index);
                            } else {
                                layer.msg("保存失败！", { icon: 2 });
                            }
                        },
                        error: function () {
                            layer.msg("发生异常~~~", { icon: 2 });
                        }
                    });
                    return false;
                }
                ,btn2: function(index, layero){
                    var designDepart = $("#designDepart").val();
                    var styleNumber = $("#styleNumber").val();
                    var categoryCode = $("#categoryCode").val();
                    var season = $("#devSeason").val();
                    var year = $("#clothesYear").val();
                    var seasonCode;
                    if (designDepart == null || designDepart == '' || categoryCode == null || categoryCode == '' || styleNumber == null || styleNumber == '' || season == null || season == '' || year == null || year == ''){
                        layer.msg("设计部-款式-年份-季度必须~~~~");
                        return false;
                    }
                    if (season === '春夏'){
                        seasonCode = 2;
                    } else {
                        seasonCode = 3;
                    }
                    var preName = designDepart + categoryCode.slice(0,1) + year.slice(3) + seasonCode + styleNumber.slice(0,1);
                    $.ajax({
                        url: "/erp/getclothesdevordernamebyprefix",
                        type: 'GET',
                        data: {
                            preFix: preName
                        },
                        success: function (res) {
                            if (res.orderName) {
                                $("#devOrderName").val(res.orderName);
                            }
                        },
                        error: function () {
                            layer.msg("获取款号序列失败", { icon: 2 });
                        }
                    });
                    return false;
                }
                ,cancel: function(index, layero){
                    tinymce.remove("#clothesImageTextarea");
                    tinymce.remove("#sizeImageTextarea");
                    tinymce.remove("#processImageTextarea");
                    colorSelectValue = [];
                    table.render({
                        elem: '#fabricTable'
                        ,cols: [[]]
                        ,height: 'full-200'
                        ,width: pageWidth
                        ,data: []
                        ,limit:Number.MAX_VALUE
                        ,done:function (res) {}
                    });
                    table.render({
                        elem: '#accessoryTable'
                        ,cols: [[]]
                        ,height: 'full-200'
                        ,width: pageWidth
                        ,data: []
                        ,limit:Number.MAX_VALUE
                        ,done:function (res) {}
                    });
                    element.tabChange('docDemoTabBrief', '1');
                }
            });
            layer.full(index);
            form.render('select');
            setTimeout(function () {
                $.ajax({
                    url: "/erp/getpreclothesversionnumber",
                    type: 'GET',
                    data: {
                    },
                    success: function (res) {
                        if (res.clothesDevNumber) {
                            $("#clothesDevNumber").val(res.clothesDevNumber);
                        }
                    },
                    error: function () {
                        layer.msg("获取版单失败", { icon: 2 });
                    }
                });
                // 辅料
                var title = [
                    {field: 'devAccessoryName', title: '名称',width:'20%',templet: function(d){
                            return '<input class="layui-input" autocomplete="off" type="text" id="devAccessoryName-'+d.LAY_TABLE_INDEX+'"/>'
                        }},
                    {field: 'accessoryNumber', title: '编号',width:'20%', align:'center',edit:'text'},
                    {field: 'specification', title: '规格',align:'center',width:'10%',edit:'text'},
                    {field: 'accessoryColor', title: '颜色',align:'center',width:'10%',edit:'text'},
                    {field: 'accessoryUnit', title: '单位',align:'center',width:'8%',edit:'text'},
                    {field: 'accessoryType', title: '类别',align:'center',width:'8%',edit:'text'},
                    {field: 'supplier', title: '供应商',align:'center',width:'8%',edit:'text'},
                    {field: 'pieceUsage', title: '单耗',align:'center',width:'8%',edit:'text'},
                    {field: 'operation', title: '操作',width:'8%',align:'center', templet: function(d){
                            return '<a class="layui-btn layui-btn-xs" lay-event="add"><i class="layui-icon layui-icon-addition" style="margin-right:0"></i></a><a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del"><i class="layui-icon layui-icon-subtraction" style="margin-right:0"></i></a>';
                        }}
                ];
                table.render({
                    elem: '#accessoryTable'
                    ,cols: [title]
                    ,height: 'full-200'
                    ,width: pageWidth
                    ,data: [
                        {
                            "devAccessoryName": ""
                            ,"accessoryNumber": ""
                            ,"specification": ""
                            ,"accessoryColor": ""
                            ,"accessoryUnit": ""
                            ,"supplier": ""
                            ,"pieceUsage": ""
                            ,"accessoryType":""
                        }
                    ]
                    ,limit:Number.MAX_VALUE
                    ,done:function (res) {
                        var data = table.cache.accessoryTable;
                        //渲染多选
                        $.each(res.data,function (index, item) {
                            $("#devAccessoryName-"+item.LAY_TABLE_INDEX).parent().css("overflow","unset");
                            $("#devAccessoryName-"+item.LAY_TABLE_INDEX).parent().css("height","auto");

                            $("#devAccessoryName-"+item.LAY_TABLE_INDEX).val(item.devAccessoryName);

                            $("#devAccessoryName-"+item.LAY_TABLE_INDEX).bind("input propertychange",function(event){
                                data[item.LAY_TABLE_INDEX].devAccessoryName = $("#devAccessoryName-"+item.LAY_TABLE_INDEX).val();
                            });
                            layui.yutons_sug.render({
                                id: "devAccessoryName-" + item.LAY_TABLE_INDEX, //设置容器唯一id
                                height: "300",
                                width: "1000",
                                limit:"50",
                                limits:[10,20,50,100],
                                cols: [
                                    [{
                                        field: 'devAccessoryName',
                                        title: '名称',
                                        width: '20%'
                                    }, {
                                        field: 'accessoryNumber',
                                        title: '编号',
                                        width: '20%'
                                    }, {
                                        field: 'specification',
                                        title: '规格',
                                        width: '10%'
                                    }, {
                                        field: 'accessoryColor',
                                        title: '颜色',
                                        width: '10%'
                                    }, {
                                        field: 'accessoryUnit',
                                        title: '单位',
                                        width: '10%'
                                    }, {
                                        field: 'accessoryType',
                                        title: '类别',
                                        width: '10%'
                                    }, {
                                        field: 'supplier',
                                        title: '供应商',
                                        width: '10%'
                                    }, {
                                        field: 'pieceUsage',
                                        title: '单耗',
                                        width: '10%'
                                    }]
                                ], //设置表头
                                params: [
                                    {
                                        name: 'subAccessoryName',
                                        field: 'devAccessoryName'
                                    }],//设置字段映射，适用于输入一个字段，回显多个字段
                                type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
                                url: "/erp/getclothesdevaccessorymanagehint?subAccessoryName=" //设置异步数据接口,url为必填项,params为字段名
                            });
                        });
                        soulTable.render(this);
                        form.render();
                    }
                });
                table.on('tool(accessoryTable)', function(obj){
                    var data = table.cache.accessoryTable;
                    if(obj.event === 'del'){
                        obj.del();
                        $.each(data,function (index,item) {
                            if(item instanceof Array) {
                                data.splice(index,1)
                            }
                        });
                        if (!data || data.length == 0){
                            data.push({
                                "devAccessoryName": ""
                                ,"accessoryNumber": ""
                                ,"specification": ""
                                ,"accessoryColor": ""
                                ,"accessoryUnit": ""
                                ,"supplier": ""
                                ,"pieceUsage": ""
                                ,"accessoryType":""
                            });
                        }
                        table.reload("accessoryTable",{
                            data:data   // 将新数据重新载入表格
                        })
                    } else if(obj.event === 'add'){
                        data.push({
                            "devAccessoryName": ""
                            ,"accessoryNumber": ""
                            ,"specification": ""
                            ,"accessoryColor": ""
                            ,"accessoryUnit": ""
                            ,"supplier": ""
                            ,"pieceUsage": ""
                            ,"accessoryType":""
                        });
                        table.reload("accessoryTable",{
                            data:data   // 将新数据重新载入表格
                        })
                    }
                });

                // 面料
                element.on('tab(docDemoTabBrief)', function(elem){
                    var elementId = $(this).attr('lay-id');
                    if (elementId === '3'){
                        var title = [
                            {field: 'devFabricName', title: '面料名称',align:'center',minWidth:220,templet: function(d){
                                    return '<input class="layui-input" autocomplete="off" type="text" id="devFabricName-'+d.LAY_TABLE_INDEX+'"/>'
                                }},
                            {field: 'fabricNumber', title: '面料号',align:'center',minWidth:150,edit:'text'},
                            {field: 'supplier', title: '供应商',align:'center',minWidth:90,edit:'text'},
                            {field: 'fabricWidth', title: '布封',align:'center',minWidth:90,edit:'text'},
                            {field: 'fabricWeight', title: '克重',align:'center',minWidth:90,edit:'text'},
                            {field: 'fabricContent', title: '成分',align:'center',minWidth:90,edit:'text'},
                            {field: 'unit', title: '单位',align:'center',minWidth:90,edit:'text'},
                            {field: 'unitTwo', title: '单位2',align:'center',minWidth:90,edit:'text'},
                            {field: 'ratio', title: '换算',align:'center',minWidth:90,edit:'text'},
                            {field: 'pieceUsage', title: '单耗',align:'center',minWidth:90,edit:'text'},
                            {field: 'partName', title: '部位',align:'center',minWidth:150,edit:'text'}
                        ];
                        var colorList = [];
                        if (colorSelectValue.length == 0){
                            layer.msg("还没选择颜色,请先选择~~~");
                        } else {
                            var catchData = table.cache.fabricTable;
                            if (!catchData || catchData.length == 0){
                                catchData = [];
                                catchData.push({"devFabricName": ""});
                            }
                            $.each(colorSelectValue,function (index, item) {
                                colorList.push(item.name);
                            });
                            $.each(colorList, function (index, item) {
                                var colorNumber = item.split('/')[0];
                                var colorName = item.split('/')[1];
                                title.push({
                                    field:item,title:item,align:'center',minWidth:150,templet:function (d) {
                                        return '<div class="layui-input-inline">' +
                                            '<select name="isHit">' +
                                            '<option value="否">不撞色</option>'+
                                            '<option value="是">撞色</option>'+
                                            '</select>' +
                                            '<input name="fabricColor" type="text" autocomplete="off" class="layui-input" value="'+colorName+'">' +
                                            '<input name="fabricColorNumber" type="text" autocomplete="off" class="layui-input" value="'+ colorNumber +'" placeholder="色号">' +
                                            '</div>'
                                    }
                                })
                            });
                            layui.form.render("select");
                            title.push( {field: 'operation', title: '操作',minWidth:70,align:'center' ,templet: function(d){
                                    if(d.LAY_INDEX == 1)
                                        return '<a class="layui-btn layui-btn-xs" lay-event="add"><i class="layui-icon layui-icon-addition" style="margin-right:0"></i></a>'
                                    else
                                        return '<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del"><i class="layui-icon layui-icon-subtraction" style="margin-right:0"></i></a>'
                                }});
                            table.render({
                                elem: '#fabricTable'
                                ,cols: [title]
                                ,height: 'full-200'
                                ,width: pageWidth
                                ,data: catchData
                                ,limit:Number.MAX_VALUE
                                ,done:function (res) {
                                    var data = table.cache.fabricTable;
                                    $.each(res.data,function (index,item) {
                                        $("#devFabricName-"+item.LAY_TABLE_INDEX).parent().css("overflow","unset");
                                        $("#devFabricName-"+item.LAY_TABLE_INDEX).parent().css("height","auto");

                                        $("#devFabricName-"+item.LAY_TABLE_INDEX).val(item.devFabricName);

                                        $("#devFabricName-"+item.LAY_TABLE_INDEX).bind("input propertychange",function(event){
                                            data[item.LAY_TABLE_INDEX].devFabricName = $("#devFabricName-"+item.LAY_TABLE_INDEX).val();
                                        });

                                        if(data[index].isHits) {
                                            $.each(data[index].isHits, function (h_index, h_item) {
                                                $("div[lay-id='fabricTable'] tr[data-index=" + index + "] select[name='isHit']").eq(h_index).val(h_item.val());
                                                $("div[lay-id='fabricTable'] tr[data-index=" + index + "] input[name='fabricColor']").eq(h_index).val(data[index].fabricColors[h_index].val());
                                                $("div[lay-id='fabricTable'] tr[data-index=" + index + "] input[name='fabricColorNumber']").eq(h_index).val(data[index].fabricColorNumbers[h_index].val());
                                            });
                                        }
                                        var selects = $("div[lay-id='fabricTable'] tr[data-index="+index+"] select[name='isHit']");
                                        var inputs = $("div[lay-id='fabricTable'] tr[data-index="+index+"] input[name='fabricColor']");
                                        var numberInputs = $("div[lay-id='fabricTable'] tr[data-index="+index+"] input[name='fabricColorNumber']");
                                        var isHits = [];
                                        var fabricColors = [];
                                        var fabricColorNumbers = [];
                                        $.each(selects,function (s_index,s_item) {
                                            isHits.push(selects.eq(s_index));
                                            fabricColors.push(inputs.eq(s_index));
                                            fabricColorNumbers.push(numberInputs.eq(s_index));
                                        });
                                        item.isHits = isHits;
                                        item.fabricColors = fabricColors;
                                        item.fabricColorNumbers = fabricColorNumbers;

                                        layui.yutons_sug.render({
                                            id: "devFabricName-" + item.LAY_TABLE_INDEX, //设置容器唯一id
                                            height: "300",
                                            width: "1200",
                                            limit:"10",
                                            limits:[10,20,50,100],
                                            cols: [
                                                [{
                                                    field: 'devFabricName',
                                                    title: '名称',
                                                    width: '15%'
                                                }, {
                                                    field: 'fabricNumber',
                                                    title: '面料号',
                                                    width: '15%'
                                                }, {
                                                    field: 'supplier',
                                                    title: '供应商',
                                                    width: '10%'
                                                }, {
                                                    field: 'unit',
                                                    title: '单位',
                                                    width: '10%'
                                                }, {
                                                    field: 'unitTwo',
                                                    title: '单位2',
                                                    width: '10%'
                                                }, {
                                                    field: 'ratio',
                                                    title: '换算',
                                                    width: '10%'
                                                }, {
                                                    field: 'fabricWidth',
                                                    title: '布封',
                                                    width: '10%'
                                                }, {
                                                    field: 'fabricWeight',
                                                    title: '克重',
                                                    width: '10%'
                                                }, {
                                                    field: 'fabricContent',
                                                    title: '成分',
                                                    width: '20%'
                                                }]
                                            ], //设置表头
                                            params: [
                                                {
                                                    name: 'subFabricName',
                                                    field: 'devFabricName'
                                                }],//设置字段映射，适用于输入一个字段，回显多个字段
                                            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
                                            url: "/erp/getclothesdevfabricmanagehint?subFabricName=" //设置异步数据接口,url为必填项,params为字段名
                                        });
                                    });

                                    layui.form.render("select");

                                    $("select[name='isHit']").parent().parent().css("overflow","unset");
                                    $("select[name='isHit']").parent().parent().css("height","auto");
                                }
                            });
                        }
                    }
                });
                form.render();
            },100);
        } else if (type == 1){
            var index = layer.open({
                type: 1 //Page层类型
                , title: "详情&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;面料单耗是指单位1的单耗,保存时会根据换算系数自动计算单位2的单耗~~"
                , btn: ['修改']
                , shade: 0.6 //遮罩透明度
                , area: '1000px'
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , content: $("#clothesDevAdd")
                , yes: function(i, layero){
                    var colorCombineList = [];
                    var colorList = [];
                    $.each(colorSelectValue,function (index, item) {
                        colorCombineList.push(item.name);
                    });
                    $.each(colorCombineList, function (c_index, c_item) {
                        var colorNumber = c_item.split('/')[0];
                        var colorName = c_item.split('/')[1];
                        colorList.push({colorNumber: colorNumber, colorName: colorName});
                    });
                    // 基础信息
                    var params = form.val("formClothsDevInfo");
                    params.orderName = params.devOrderName;
                    params.season = params.devSeason;
                    params.designDepartCode = params.designDepart;
                    var infoFlag = false;
                    for (var key in params){
                        if (key != 'select' && params[key] == ''){
                            infoFlag = true;
                        }
                    }
                    if (infoFlag){
                        layer.msg("请输入完整基本信息");
                        return false;
                    }
                    // 颜色
                    var clothesDevColorList = colorList;
                    $.each(clothesDevColorList,function (index, item) {
                        item.clothesDevNumber = params.clothesDevNumber;
                        item.orderName = params.devOrderName;
                    });
                    if (clothesDevColorList.length == 0){
                        layer.msg("请选择颜色");
                        return false;
                    }
                    // 尺码
                    var sizeSelValue = sizeSelect.getValue('value');
                    var clothesDevSizeList = [];
                    $.each(sizeSelValue,function (index, item) {
                        var tmp = {};
                        tmp.clothesDevNumber = params.clothesDevNumber;
                        tmp.orderName = params.devOrderName;
                        tmp.sizeName = item;
                        clothesDevSizeList.push(tmp);
                    });
                    if (clothesDevSizeList.length == 0){
                        layer.msg("请选择尺码");
                        return false;
                    }
                    // 公仔图
                    var devImageOne = tinymce.get('clothesImageTextarea').getContent();
                    var clothesDevImage = {};
                    clothesDevImage.clothesDevNumber = params.clothesDevNumber;
                    clothesDevImage.orderName = params.devOrderName;
                    clothesDevImage.devImageOne = devImageOne;
                    // 工艺
                    var processImg = tinymce.get('processImageTextarea').getContent();
                    var clothesDevProcess = {};
                    clothesDevProcess.clothesDevNumber = params.clothesDevNumber;
                    clothesDevProcess.orderName = params.devOrderName;
                    clothesDevProcess.processImg = processImg;
                    // 尺寸
                    var measureImg = tinymce.get('sizeImageTextarea').getContent();
                    var clothesDevMeasure = {};
                    clothesDevMeasure.clothesDevNumber = params.clothesDevNumber;
                    clothesDevMeasure.orderName = params.devOrderName;
                    clothesDevMeasure.measureImg = measureImg;
                    // 面料
                    var fabricData = table.cache.fabricTable;
                    if (fabricData == null || fabricData.length == 0){
                        layer.msg("请输入面料信息");
                        return false;
                    }
                    var isHits = [];
                    var fabricColors = [];
                    var fabricColorNumberList = [];
                    $.each(fabricData,function (index,item) {
                        item.fabricName = item.devFabricName;
                        var selects = $("div[lay-id='fabricTable'] tr[data-index="+index+"] select[name='isHit']");
                        var inputs = $("div[lay-id='fabricTable'] tr[data-index="+index+"] input[name='fabricColor']");
                        var fabricColorNumbers = $("div[lay-id='fabricTable'] tr[data-index="+index+"] input[name='fabricColorNumber']");
                        var tmp = [];
                        var itmp = [];
                        var ctmp = [];
                        $.each(selects,function (s_index,s_item) {
                            tmp.push(selects.eq(s_index).val());
                            if(inputs.eq(s_index).val()!="") {
                                itmp.push(inputs.eq(s_index).val())
                            } else {
                                itmp.push("未输");
                            }
                            if(fabricColorNumbers.eq(s_index).val()!="") {
                                ctmp.push(fabricColorNumbers.eq(s_index).val())
                            }else{
                                ctmp.push('未输');
                            }
                        });
                        isHits.push(tmp);
                        fabricColors.push(itmp);
                        fabricColorNumberList.push(ctmp);
                    });
                    var clothesDevFabricList = [];
                    var isInput = true;
                    var pieceFlag = true;
                    var ratioFlag = true;
                    var unitFlag = true;
                    $.each(fabricColors,function (i,item) {
                        if(item.length<isHits[0].length) {
                            isInput = false;
                            return false;
                        }
                    });
                    if(isInput) {
                        $.each(fabricData, function (i, item) {
                            if (item.fabricName == null || item.unit == null || item.partName == null || item.pieceUsage == null || item.ratio == null || item.fabricName == '' || item.unit == '' || item.partName == '' || item.pieceUsage == '' || item.ratio == '') {
                                isInput = false;
                                return false;
                            }
                            var pieceUsage = 0;
                            var pieceUsageTwo = 0;
                            if (item.pieceUsage != ''){
                                if (!isNumber(item.pieceUsage)){
                                    pieceFlag = false;
                                } else {
                                    pieceUsage = item.pieceUsage;
                                }
                            }
                            if (item.ratio != ''){
                                if (!isNumber(item.ratio) || Number(item.ratio) === 0){
                                    ratioFlag = false;
                                } else {
                                    pieceUsageTwo = item.pieceUsage / Number(item.ratio);
                                }
                            }
                            $.each(colorList,function(j,value) {
                                var manufactureFabric = {};
                                manufactureFabric.fabricKey = i;
                                manufactureFabric.orderName = params.devOrderName;
                                manufactureFabric.clothesDevNumber = params.clothesDevNumber;
                                manufactureFabric.fabricNumber = item.fabricNumber;
                                manufactureFabric.fabricName = item.fabricName;
                                manufactureFabric.supplier = item.supplier;
                                manufactureFabric.fabricWidth = item.fabricWidth;
                                manufactureFabric.fabricWeight = item.fabricWeight;
                                manufactureFabric.fabricContent = item.fabricContent;
                                var tmpUnit = item.unit;
                                tmpUnit = tmpUnit.replace(/^\s*|\s*$/g,"");
                                manufactureFabric.unit = tmpUnit;
                                var tmpUnitTwo = item.unitTwo;
                                tmpUnitTwo = tmpUnitTwo.replace(/^\s*|\s*$/g,"");
                                manufactureFabric.unitTwo = tmpUnitTwo;
                                manufactureFabric.ratio = item.ratio;
                                manufactureFabric.partName = item.partName;
                                manufactureFabric.pieceUsage = pieceUsage;
                                manufactureFabric.pieceUsageTwo = pieceUsageTwo;
                                manufactureFabric.colorName = value.colorName;
                                manufactureFabric.colorNumber = value.colorNumber;
                                manufactureFabric.isHit = isHits[i][j];
                                manufactureFabric.fabricColor = fabricColors[i][j];
                                manufactureFabric.fabricColorNumber = fabricColorNumberList[i][j];
                                if (manufactureFabric.fabricColor != "未输"){
                                    clothesDevFabricList.push(manufactureFabric);
                                }
                            })
                        });
                    }
                    if (!ratioFlag){
                        layer.msg("换算有误");
                        return false;
                    }
                    if (clothesDevFabricList.length == 0){
                        layer.msg("请输入面料信息");
                        return false;
                    }
                    // 辅料
                    var accessoryData = table.cache.accessoryTable;
                    console.log(accessoryData);
                    if (accessoryData == null || accessoryData.length == 0){
                        layer.msg("请输入辅料信息");
                        return false;
                    }
                    var clothesDevAccessoryList = [];
                    $.each(accessoryData,function (index,item) {
                        var accessory = item;
                        accessory.accessoryName = accessory.devAccessoryName;
                        accessory.clothesDevNumber = params.clothesDevNumber;
                        clothesDevAccessoryList.push(accessory);
                    });
                    console.log(clothesDevAccessoryList);
                    if (clothesDevAccessoryList.length == 0){
                        layer.msg("请输入辅料信息");
                        return false;
                    }
                    $.ajax({
                        url: "/erp/updateclothesdevtotal",
                        type: 'POST',
                        data: {
                            clothesDevNumber: params.clothesDevNumber,
                            clothesDevInfoJson: JSON.stringify(params),
                            clothesDevImageJson: JSON.stringify(clothesDevImage),
                            clothesDevProcessJson: JSON.stringify(clothesDevProcess),
                            clothesDevMeasureJson: JSON.stringify(clothesDevMeasure),
                            clothesDevColorListJson: JSON.stringify(clothesDevColorList),
                            clothesDevSizeListJson: JSON.stringify(clothesDevSizeList),
                            clothesDevFabricListJson: JSON.stringify(clothesDevFabricList),
                            clothesDevAccessoryListJson: JSON.stringify(clothesDevAccessoryList)
                        },
                        success: function (res) {
                            if (res.result == 0) {
                                layer.msg("保存成功！", { icon: 1 });
                                layer.close(index);
                            } else {
                                layer.msg("保存失败！", { icon: 2 });
                            }
                        },
                        error: function () {
                            layer.msg("发生异常~~~", { icon: 2 });
                        }
                    });
                    return false;
                }
                , cancel: function(index, layero){
                    tinymce.remove("#clothesImageTextarea");
                    tinymce.remove("#sizeImageTextarea");
                    tinymce.remove("#processImageTextarea");
                    colorSelectValue = [];
                    table.render({
                        elem: '#fabricTable'
                        ,cols: [[]]
                        ,height: 'full-200'
                        ,width: pageWidth
                        ,data: []
                        ,limit:Number.MAX_VALUE
                        ,done:function (res) {}
                    });
                    table.render({
                        elem: '#accessoryTable'
                        ,cols: [[]]
                        ,height: 'full-200'
                        ,width: pageWidth
                        ,data: []
                        ,limit:Number.MAX_VALUE
                        ,done:function (res) {}
                    });
                    element.tabChange('docDemoTabBrief', '1');
                }
            });
            layer.full(index);
            form.render('select');
            setTimeout(function () {
                $.ajax({
                    url: "/erp/getclothesdevtotalbyclothesdevnumber",
                    type: 'GET',
                    data: {
                        clothesDevNumber: clothesDevNumber
                    },
                    success: function (res) {
                        if (res.clothesDevImage) {
                            var clothesDevInfo = res.clothesDevInfo;
                            var clothesDevSizeList = res.clothesDevSizeList;
                            var clothesDevColorList = res.clothesDevColorList;
                            var clothesDevAccessoryList = res.clothesDevAccessoryList;
                            var clothesDevFabricList = res.clothesDevFabricList;
                            var uniqueClothesDevFabricList = res.uniqueClothesDevFabricList;
                            // 通过form批量赋值
                            form.val("formClothsDevInfo", { //formTest 即 class="layui-form" 所在元素属性 lay-filter="" 对应的值
                                "clothesDevNumber": clothesDevInfo.clothesDevNumber // "name": "value"
                                ,"customerCode": clothesDevInfo.customerCode
                                ,"customerName": clothesDevInfo.customerName
                                ,"stylePattern": clothesDevInfo.stylePattern
                                ,"clothesYear": clothesDevInfo.clothesYear
                                ,"devSeason": clothesDevInfo.season
                                ,"requireDate": moment(clothesDevInfo.requireDate).format("YYYY-MM-DD")
                                ,"expectDate": moment(clothesDevInfo.expectDate).format("YYYY-MM-DD")
                                ,"styleCategory": clothesDevInfo.styleCategory
                                ,"categoryCode": clothesDevInfo.categoryCode
                                ,"styleName": clothesDevInfo.styleName
                                ,"styleNumber": clothesDevInfo.styleNumber
                                ,"designDepart": clothesDevInfo.designDepartCode
                                ,"designer": clothesDevInfo.designer
                                ,"devOrderName": clothesDevInfo.orderName
                                ,"clothesCount": clothesDevInfo.clothesCount
                            });
                            // 赋值size
                            var sizeData=[];
                            for(var key in sizeNameData) {
                                var parent = {};
                                parent.name = key;
                                parent.value = key;
                                var children=[];
                                $.each(sizeNameData[key],function (index,item) {
                                    var tmp = {name: item, value: item};
                                    $.each(clothesDevSizeList,function (index_s,item_s) {
                                        if (item == item_s.sizeName){
                                            tmp.selected = true;
                                        }
                                    });
                                    children.push(tmp);
                                });
                                parent.children = children;
                                sizeData.push(parent);
                            }
                            sizeSelect.update({
                                data: sizeData
                            });
                            // 赋值color
                            var thisData = [];
                            $.each(colorNameData, function(index,element){
                                var tmp= {name:element.colorNumber + "/" + element.colorName, value: element.id};
                                $.each(clothesDevColorList, function(index_c,element_c){
                                    if (element.colorNumber == element_c.colorNumber && element.colorName == element_c.colorName){
                                        tmp.selected = true;
                                        colorSelectValue.push(tmp);
                                    }
                                });
                                thisData.push(tmp);
                            });
                            colorSelect.update({
                                data: thisData
                            });
                            setTimeout(function () {
                                tinymce.get('clothesImageTextarea').setContent(res.clothesDevImage.devImageOne);
                            }, 100);
                            setTimeout(function () {
                                tinymce.get('sizeImageTextarea').setContent(res.clothesDevMeasure.measureImg);
                            }, 100);
                            setTimeout(function () {
                                tinymce.get('processImageTextarea').setContent(res.clothesDevProcess.processImg);
                            }, 100);

                            // 辅料
                            var title = [
                                {field: 'devAccessoryName', title: '名称',width:'20%',templet: function(d){
                                        return '<input class="layui-input" autocomplete="off" type="text" id="devAccessoryName-'+d.LAY_TABLE_INDEX+'"/>'
                                    }},
                                {field: 'accessoryNumber', title: '编号',width:'20%', align:'center',edit:'text'},
                                {field: 'specification', title: '规格',align:'center',width:'10%',edit:'text'},
                                {field: 'accessoryColor', title: '颜色',align:'center',width:'10%',edit:'text'},
                                {field: 'accessoryUnit', title: '单位',align:'center',width:'8%',edit:'text'},
                                {field: 'accessoryType', title: '类别',align:'center',width:'8%',edit:'text'},
                                {field: 'supplier', title: '供应商',align:'center',width:'8%',edit:'text'},
                                {field: 'pieceUsage', title: '单耗',align:'center',width:'8%',edit:'text'},
                                {field: 'operation', title: '操作',width:'8%',align:'center', templet: function(d){
                                        return '<a class="layui-btn layui-btn-xs" lay-event="add"><i class="layui-icon layui-icon-addition" style="margin-right:0"></i></a><a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del"><i class="layui-icon layui-icon-subtraction" style="margin-right:0"></i></a>';
                                    }}
                            ];

                            $.each(clothesDevAccessoryList,function (index, item) {
                                item.devAccessoryName = item.accessoryName;
                            });

                            table.render({
                                elem: '#accessoryTable'
                                ,cols: [title]
                                ,height: 'full-200'
                                ,width: pageWidth
                                ,data: clothesDevAccessoryList
                                ,limit:Number.MAX_VALUE
                                ,done:function (res) {
                                    var data = table.cache.accessoryTable;
                                    //渲染多选
                                    $.each(res.data,function (index, item) {
                                        $("#devAccessoryName-"+item.LAY_TABLE_INDEX).parent().css("overflow","unset");
                                        $("#devAccessoryName-"+item.LAY_TABLE_INDEX).parent().css("height","auto");

                                        $("#devAccessoryName-"+item.LAY_TABLE_INDEX).val(item.devAccessoryName);

                                        $("#devAccessoryName-"+item.LAY_TABLE_INDEX).bind("input propertychange",function(event){
                                            data[item.LAY_TABLE_INDEX].devAccessoryName = $("#devAccessoryName-"+item.LAY_TABLE_INDEX).val();
                                        });
                                        layui.yutons_sug.render({
                                            id: "devAccessoryName-" + item.LAY_TABLE_INDEX, //设置容器唯一id
                                            height: "300",
                                            width: "1000",
                                            limit:"50",
                                            limits:[10,20,50,100],
                                            cols: [
                                                [{
                                                    field: 'devAccessoryName',
                                                    title: '名称',
                                                    width: '20%'
                                                }, {
                                                    field: 'accessoryNumber',
                                                    title: '编号',
                                                    width: '20%'
                                                }, {
                                                    field: 'specification',
                                                    title: '规格',
                                                    width: '10%'
                                                }, {
                                                    field: 'accessoryColor',
                                                    title: '颜色',
                                                    width: '10%'
                                                }, {
                                                    field: 'accessoryUnit',
                                                    title: '单位',
                                                    width: '10%'
                                                }, {
                                                    field: 'accessoryType',
                                                    title: '类别',
                                                    width: '10%'
                                                }, {
                                                    field: 'supplier',
                                                    title: '供应商',
                                                    width: '10%'
                                                }, {
                                                    field: 'pieceUsage',
                                                    title: '单耗',
                                                    width: '10%'
                                                }]
                                            ], //设置表头
                                            params: [
                                                {
                                                    name: 'subAccessoryName',
                                                    field: 'devAccessoryName'
                                                }],//设置字段映射，适用于输入一个字段，回显多个字段
                                            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
                                            url: "/erp/getclothesdevaccessorymanagehint?subAccessoryName=" //设置异步数据接口,url为必填项,params为字段名
                                        });
                                    });
                                    soulTable.render(this);
                                    form.render();
                                }
                            });
                            table.on('tool(accessoryTable)', function(obj){
                                var data = table.cache.accessoryTable;
                                if(obj.event === 'del'){
                                    obj.del();
                                    $.each(data,function (index,item) {
                                        if(item instanceof Array) {
                                            data.splice(index,1)
                                        }
                                    });
                                    if (!data || data.length == 0){
                                        data.push({
                                            "devAccessoryName": ""
                                            ,"accessoryNumber": ""
                                            ,"specification": ""
                                            ,"accessoryColor": ""
                                            ,"accessoryUnit": ""
                                            ,"supplier": ""
                                            ,"pieceUsage": ""
                                            ,"accessoryType":""
                                        });
                                    }
                                    table.reload("accessoryTable",{
                                        data:data   // 将新数据重新载入表格
                                    })
                                } else if(obj.event === 'add'){
                                    data.push({
                                        "devAccessoryName": ""
                                        ,"accessoryNumber": ""
                                        ,"specification": ""
                                        ,"accessoryColor": ""
                                        ,"accessoryUnit": ""
                                        ,"supplier": ""
                                        ,"pieceUsage": ""
                                        ,"accessoryType":""
                                    });
                                    table.reload("accessoryTable",{
                                        data:data   // 将新数据重新载入表格
                                    })
                                }
                            });
                            element.on('tab(docDemoTabBrief)', function(elem){
                                var elementId = $(this).attr('lay-id');
                                if (elementId === '3'){
                                    var title = [
                                        {field: 'devFabricName', title: '面料名称',align:'center',minWidth:220,templet: function(d){
                                                return '<input class="layui-input" autocomplete="off" type="text" id="devFabricName-'+d.LAY_TABLE_INDEX+'"/>'
                                            }},
                                        {field: 'fabricNumber', title: '面料号',align:'center',minWidth:150,edit:'text'},
                                        {field: 'supplier', title: '供应商',align:'center',minWidth:90,edit:'text'},
                                        {field: 'fabricWidth', title: '布封',align:'center',minWidth:90,edit:'text'},
                                        {field: 'fabricWeight', title: '克重',align:'center',minWidth:90,edit:'text'},
                                        {field: 'fabricContent', title: '成分',align:'center',minWidth:90,edit:'text'},
                                        {field: 'unit', title: '单位',align:'center',minWidth:90,edit:'text'},
                                        {field: 'unitTwo', title: '单位2',align:'center',minWidth:90,edit:'text'},
                                        {field: 'ratio', title: '换算',align:'center',minWidth:90,edit:'text'},
                                        {field: 'pieceUsage', title: '单耗',align:'center',minWidth:90,edit:'text'},
                                        {field: 'partName', title: '部位',align:'center',minWidth:150,edit:'text'}
                                    ];
                                    var colorList = [];
                                    if (colorSelectValue.length == 0){
                                        layer.msg("还没选择颜色,请先选择~~~");
                                    } else {
                                        $.each(uniqueClothesDevFabricList,function (index, item) {
                                            item.devFabricName = item.fabricName;
                                        });
                                        $.each(colorSelectValue,function (index, item) {
                                            colorList.push(item.name);
                                        });
                                        console.log(colorSelectValue);
                                        console.log(colorList);
                                        $.each(colorList, function (index, item) {
                                            var colorNumber = item.split('/')[0];
                                            var colorName = item.split('/')[1];
                                            title.push({
                                                field:item,title:item,align:'center',minWidth:150,templet:function (d) {
                                                    return '<div class="layui-input-inline">' +
                                                        '<select name="isHit">' +
                                                        '<option value="否">不撞色</option>'+
                                                        '<option value="是">撞色</option>'+
                                                        '</select>' +
                                                        '<input name="fabricColor" type="text" autocomplete="off" class="layui-input" value="'+colorName+'">' +
                                                        '<input name="fabricColorNumber" type="text" autocomplete="off" class="layui-input" value="'+ colorNumber +'" placeholder="色号">' +
                                                        '</div>'
                                                }
                                            })
                                        });
                                        layui.form.render("select");
                                        title.push( {field: 'operation', title: '操作',minWidth:70,align:'center' ,templet: function(d){
                                                if(d.LAY_INDEX == 1)
                                                    return '<a class="layui-btn layui-btn-xs" lay-event="add"><i class="layui-icon layui-icon-addition" style="margin-right:0"></i></a>'
                                                else
                                                    return '<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del"><i class="layui-icon layui-icon-subtraction" style="margin-right:0"></i></a>'
                                            }});
                                        table.render({
                                            elem: '#fabricTable'
                                            ,cols: [title]
                                            ,height: 'full-200'
                                            ,width: pageWidth
                                            ,data: uniqueClothesDevFabricList
                                            ,limit:Number.MAX_VALUE
                                            ,done:function (res) {
                                                var data = table.cache.fabricTable;
                                                $.each(res.data,function (index,item) {
                                                    $("#devFabricName-"+item.LAY_TABLE_INDEX).parent().css("overflow","unset");
                                                    $("#devFabricName-"+item.LAY_TABLE_INDEX).parent().css("height","auto");

                                                    $("#devFabricName-"+item.LAY_TABLE_INDEX).val(item.devFabricName);

                                                    $("#devFabricName-"+item.LAY_TABLE_INDEX).bind("input propertychange",function(event){
                                                        data[item.LAY_TABLE_INDEX].devFabricName = $("#devFabricName-"+item.LAY_TABLE_INDEX).val();
                                                    });
                                                    if(data[index].isHits) {
                                                        $.each(data[index].isHits, function (h_index, h_item) {
                                                            $("div[lay-id='fabricTable'] tr[data-index=" + index + "] select[name='isHit']").eq(h_index).val(h_item.val());
                                                            $("div[lay-id='fabricTable'] tr[data-index=" + index + "] input[name='fabricColor']").eq(h_index).val(data[index].fabricColors[h_index].val());
                                                            $("div[lay-id='fabricTable'] tr[data-index=" + index + "] input[name='fabricColorNumber']").eq(h_index).val(data[index].fabricColorNumbers[h_index].val());
                                                        });
                                                    }
                                                    var selects = $("div[lay-id='fabricTable'] tr[data-index="+index+"] select[name='isHit']");
                                                    var inputs = $("div[lay-id='fabricTable'] tr[data-index="+index+"] input[name='fabricColor']");
                                                    var numberInputs = $("div[lay-id='fabricTable'] tr[data-index="+index+"] input[name='fabricColorNumber']");
                                                    var isHits = [];
                                                    var fabricColors = [];
                                                    var fabricColorNumbers = [];
                                                    $.each(selects,function (s_index,s_item) {
                                                        isHits.push(selects.eq(s_index));
                                                        fabricColors.push(inputs.eq(s_index));
                                                        fabricColorNumbers.push(numberInputs.eq(s_index));
                                                    });
                                                    item.isHits = isHits;
                                                    item.fabricColors = fabricColors;
                                                    item.fabricColorNumbers = fabricColorNumbers;

                                                    layui.yutons_sug.render({
                                                        id: "devFabricName-" + item.LAY_TABLE_INDEX, //设置容器唯一id
                                                        height: "300",
                                                        width: "1200",
                                                        limit:"10",
                                                        limits:[10,20,50,100],
                                                        cols: [
                                                            [{
                                                                field: 'devFabricName',
                                                                title: '名称',
                                                                width: '20%'
                                                            }, {
                                                                field: 'fabricNumber',
                                                                title: '面料号',
                                                                width: '20%'
                                                            }, {
                                                                field: 'supplier',
                                                                title: '供应商',
                                                                width: '10%'
                                                            }, {
                                                                field: 'unit',
                                                                title: '单位',
                                                                width: '10%'
                                                            }, {
                                                                field: 'unitTwo',
                                                                title: '单位2',
                                                                width: '10%'
                                                            }, {
                                                                field: 'ratio',
                                                                title: '换算',
                                                                width: '10%'
                                                            }, {
                                                                field: 'fabricWidth',
                                                                title: '布封',
                                                                width: '10%'
                                                            }, {
                                                                field: 'fabricWeight',
                                                                title: '克重',
                                                                width: '10%'
                                                            }, {
                                                                field: 'fabricContent',
                                                                title: '成分',
                                                                width: '20%'
                                                            }]
                                                        ], //设置表头
                                                        params: [
                                                            {
                                                                name: 'subFabricName',
                                                                field: 'devFabricName'
                                                            }],//设置字段映射，适用于输入一个字段，回显多个字段
                                                        type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
                                                        url: "/erp/getclothesdevfabricmanagehint?subFabricName=" //设置异步数据接口,url为必填项,params为字段名
                                                    });
                                                });
                                                layui.form.render("select");
                                                $("select[name='isHit']").parent().parent().css("overflow","unset");
                                                $("select[name='isHit']").parent().parent().css("height","auto");
                                            }
                                        });
                                    }
                                }
                            });
                            form.render();
                        }
                    },
                    error: function () {
                        layer.msg("获取版单失败", { icon: 2 });
                    }
                });

                form.render();
            },100);

        }

    }
});
function processOrderDetail(clothesDevNumber) {
    var pdfName = clothesDevNumber + '--制单.pdf';
    var index = layer.open({
        type: 1 //Page层类型
        , title: '开发版制单'
        , btn: ['下载']
        , shade: 0.6 //遮罩透明度
        , maxmin: false //允许全屏最小化
        , anim: 0 //0-6的动画形式，-1不开启
        , content: "<div id=\"processDetail\" name=\"processDetail\" style=\"background-color: white; color: black;font-size: 20px;\">\n" +
            "    <div id=\"contentPage\" style=\"background-color: white\">\n" +
            "        <h1 style=\"text-align: center;font-size: 24px; font-weight: 800; margin-top: 0px\">德悦服饰制单(开发版)</h1>\n" +
            "        <div class=\"row\" style=\"margin:0\">\n" +
            "            <div class=\"col-md-12\">\n" +
            "                <section class=\"panel panel-default\">\n" +
            "                    <header class=\"panel-heading font-bold\" style=\"font-size: 16px;background-color: #dcd9d9;border-color: black;\">\n" +
            "                        <span class=\"label bg-success pull-right\"></span>基本信息\n" +
            "                    </header>\n" +
            "                    <div id=\"basicDetailInfo\"></div>\n" +
            "                </section>\n" +
            "            </div>\n" +
            "            <div class=\"row\" style=\"margin:0\">\n" +
            "                <div class=\"col-md-12\">\n" +
            "                    <section class=\"panel panel-default\">\n" +
            "                        <header class=\"panel-heading font-bold\" style=\"font-size: 16px;background-color: #dcd9d9;border-color: black;\">\n" +
            "                            <span class=\"label bg-success pull-right\"></span>款式图\n" +
            "                        </header>\n" +
            "                        <div id=\"styleImgDiv\" style=\"text-align: center; height: 300px\"></div>\n" +
            "                    </section>\n" +
            "                </div>\n" +
            "            </div>\n" +
            "        </div>\n" +
            "        <div class=\"row\" style=\"margin:0\">\n" +
            "            <div class=\"col-md-12\">\n" +
            "                <section class=\"panel panel-default\">\n" +
            "                    <header class=\"panel-heading font-bold\" style=\"font-size: 16px;background-color:#dcd9d9;border-color: black;\">\n" +
            "                        <span class=\"label bg-success pull-right\"></span>面料信息\n" +
            "                    </header>\n" +
            "                    <div id=\"manufactureFabricTable\"></div>\n" +
            "                </section>\n" +
            "            </div>\n" +
            "        </div>\n" +
            "        <div class=\"row\" style=\"margin:0\">\n" +
            "            <div class=\"col-md-12\">\n" +
            "                <section class=\"panel panel-default\">\n" +
            "                    <header class=\"panel-heading font-bold\" style=\"font-size: 16px;background-color: #dcd9d9;border-color: black;\">\n" +
            "                        <span class=\"label bg-success pull-right\"></span>辅料信息\n" +
            "                    </header>\n" +
            "                    <div id=\"manufactureAccessoryTable\"></div>\n" +
            "                </section>\n" +
            "            </div>\n" +
            "        </div>\n" +
            "        <div style='page-break-after:always;text-align:center;'></div>\n"+
            "        <div class=\"row\" style=\"margin:0\">\n" +
            "            <div class=\"col-md-12\">\n" +
            "                <section class=\"panel panel-default\">\n" +
            "                    <header class=\"panel-heading font-bold\" style=\"font-size: 16px;background-color: #dcd9d9;border-color: black;\">\n" +
            "                        <span class=\"label bg-success pull-right\"></span>尺寸\n" +
            "                    </header>\n" +
            "                    <div id=\"measureDiv\"></div>\n" +
            "                </section>\n" +
            "            </div>\n" +
            "        </div>\n" +
            "        <div class=\"row\" style=\"margin:0\">\n" +
            "            <div class=\"col-md-12\">\n" +
            "                <section class=\"panel panel-default\">\n" +
            "                    <header class=\"panel-heading font-bold\" style=\"font-size: 16px;background-color: #dcd9d9;border-color: black;\">\n" +
            "                        <span class=\"label bg-success pull-right\"></span>工艺\n" +
            "                    </header>\n" +
            "                    <div id=\"processDiv\"></div>\n" +
            "                </section>\n" +
            "            </div>\n" +
            "        </div>\n" +
            "    </div>\n" +
            "</div>\n" +
            "    <style>\n" +
            "        section {\n" +
            "            border-color: #000000 !important;\n" +
            "        }\n" +
            "    </style>"
        , yes: function () {
            var newWin=window.open('about:blank', '', '');
            var html = '<html><head><title>'+pdfName+'</title></head><body><div>'+$("#contentPage").html()+'</div></body>' +
                '<style>' +
                'section {border:1px solid #000} .col-md-6,.row{margin: 0 0 20px 0 !important;}' +
                '</style></html>';
            newWin.document.write(html);
            newWin.document.close();
            newWin.print();

        }
        , cancel : function (i,layero) {
            $("#basicDetailInfo").empty();
            $("#styleImgDiv").empty();
            $("#manufactureFabricTable").empty();
            $("#manufactureAccessoryTable").empty();
            $("#measureDiv").empty();
            $("#processDiv").empty();
        }
    });
    layer.full(index);
    setTimeout(function () {
        $("#basicDetailInfo").empty();
        $("#styleImgDiv").empty();
        $("#manufactureFabricTable").empty();
        $("#manufactureAccessoryTable").empty();
        $("#measureDiv").empty();
        $("#processDiv").empty();
        $.ajax({
            url: "/erp/getclothesdevtotalbyclothesdevnumber",
            type: 'GET',
            data: {
                clothesDevNumber: clothesDevNumber
            },
            success: function (res) {
                if(res.clothesDevInfo) {
                    var data = res.clothesDevInfo;
                    var tableHtml = "<table style='width: 100%; font-size: 16px; border: 1px solid black;margin-bottom: 0px;word-wrap:break-word;border-spacing:0 0' class='table'>";
                    tableHtml += "<tr><td style='font-weight: bolder; border: 1px solid black;'>开发号:</td><td style='border: 1px solid black;'>"+ data.clothesDevNumber +"</td><td style='font-weight: bolder; border: 1px solid black;'>款号:</td><td style='border: 1px solid black;'>"+ data.orderName +"</td><td style='font-weight: bolder; border: 1px solid black;'>客户名称:</td><td style='border: 1px solid black;'>"+ data.customerName +"</td><td style='font-weight: bolder; border: 1px solid black;'>版类:</td><td style='border: 1px solid black;'>"+ data.stylePattern +"</td></tr>";
                    tableHtml += "<tr><td style='font-weight: bolder; border: 1px solid black;'>年份:</td><td style='border: 1px solid black;'>"+ data.clothesYear +"</td><td style='font-weight: bolder; border: 1px solid black;'>季度:</td><td style='border: 1px solid black;'>"+ data.season +"</td><td style='font-weight: bolder; border: 1px solid black;'>要求版期:</td><td style='border: 1px solid black;'>"+ moment(data.requireDate).format("YYYY-MM-DD") +"</td><td style='font-weight: bolder; border: 1px solid black;'>预计版期:</td><td style='border: 1px solid black;'>"+ moment(data.expectDate).format("YYYY-MM-DD") +"</td></tr>";
                    tableHtml += "<tr><td style='font-weight: bolder; border: 1px solid black;'>款式类别:</td><td style='border: 1px solid black;'>"+ data.styleCategory +"</td><td style='font-weight: bolder; border: 1px solid black;'>类别代码:</td><td style='border: 1px solid black;'>"+ data.categoryCode +"</td><td style='font-weight: bolder; border: 1px solid black;'>款式名称:</td><td style='border: 1px solid black;'>"+ data.styleName +"</td><td style='font-weight: bolder; border: 1px solid black;'>款式代码:</td><td style='border: 1px solid black;'>"+ data.styleNumber +"</td></tr>";
                    tableHtml += "<tr><td style='font-weight: bolder; border: 1px solid black;'>设计部:</td><td style='border: 1px solid black;'>"+ data.designDepartName +"</td><td style='font-weight: bolder; border: 1px solid black;'>设计师:</td><td style='border: 1px solid black;'>"+ data.designer +"</td><td style='font-weight: bolder; border: 1px solid black;'>数量:</td><td style='border: 1px solid black;'>"+ data.clothesCount +"</td><td style='font-weight: bolder; border: 1px solid black;'></td><td style='border: 1px solid black;'></td></tr>";
                    tableHtml += "<tr><td style='font-weight: bolder; border: 1px solid black;'>颜色:</td><td colspan='3' style='border: 1px solid black;'>"+ res.colorInfo +"</td><td style='font-weight: bolder; border: 1px solid black;'>尺码:</td><td colspan='3' style='border: 1px solid black;'>"+ res.sizeInfo +"</td></tr></table>";
                    $("#basicDetailInfo").append(tableHtml);
                }
                $("#styleImgDiv").empty();
                if(res.clothesDevImage) {
                    $("#styleImgDiv").html(res.clothesDevImage.devImageOne);
                    $("#styleImgDiv img").each(function () {
                        var heightStr = $(this).css("height");
                        var weightStr = $(this).css("width");
                        var hl = heightStr.indexOf('p');
                        var wl = weightStr.indexOf('p');
                        var initHeight = heightStr.substring(0, hl);
                        var initWidth = heightStr.substring(0, wl);
                        $(this).css("height", 290);
                        $(this).css("width", initWidth/initHeight * 290 - 30);
                    })
                } else {
                    $("#styleImgDiv").html("<h3>暂无款式图</h3>");
                }
                if(res.clothesDevFabricList) {
                    var data = res.clothesDevFabricList;
                    var fabricHtml = "<table style='width: 100%; font-size: 16px; border: 1px solid black;margin-bottom: 0px;word-wrap:break-word;border-spacing:0 0' class='table'>";
                    fabricHtml += "<tr><td style='font-weight: bolder; border: 1px solid black;'>部位</td><td style='font-weight: bolder; border: 1px solid black;width: '20%'>名称</td><td style='font-weight: bolder; border: 1px solid black;'>编号</td><td style='font-weight: bolder; border: 1px solid black;'>布封</td><td style='font-weight: bolder; border: 1px solid black;'>克重</td><td style='font-weight: bolder; border: 1px solid black;'>成分</td><td style='font-weight: bolder; border: 1px solid black;'>单位</td><td style='font-weight: bolder; border: 1px solid black;'>单耗</td><td style='font-weight: bolder; border: 1px solid black;'>单位2</td><td style='font-weight: bolder; border: 1px solid black;'>单耗2</td><td style='font-weight: bolder; border: 1px solid black;'>色组</td><td style='font-weight: bolder; border: 1px solid black;'>色号</td><td style='font-weight: bolder; border: 1px solid black;'>布色</td><td style='font-weight: bolder; border: 1px solid black;'>色号</td></tr>";
                    for (var i=0; i < data.length; i ++){
                        fabricHtml += "<tr><td style='border: 1px solid black;'>"+data[i].partName+"</td><td style='border: 1px solid black;'>"+data[i].fabricName+"</td><td style='border: 1px solid black;'>"+data[i].fabricNumber+"</td><td style='border: 1px solid black;'>"+data[i].fabricWidth+"</td><td style='border: 1px solid black;'>"+data[i].fabricWeight+"</td><td style='border: 1px solid black;'>"+data[i].fabricContent+"</td><td style='border: 1px solid black;'>"+data[i].unit+"</td><td style='border: 1px solid black;'>"+ toDecimal(data[i].pieceUsage) +"</td><td style='border: 1px solid black;'>"+data[i].unitTwo+"</td><td style='border: 1px solid black;'>"+ toDecimal(data[i].pieceUsageTwo) +"</td><td style='border: 1px solid black;'>"+data[i].colorName+"</td><td style='border: 1px solid black;'>"+data[i].colorNumber+"</td><td style='border: 1px solid black;'>"+ data[i].fabricColor +"</td><td style='border: 1px solid black;'>"+ data[i].fabricColorNumber +"</td></tr>";
                    }
                    fabricHtml += "</table>";
                    $("#manufactureFabricTable").append(fabricHtml);
                } else {
                    $("#manufactureFabricTable").append("面料未入");
                }
                if(res.clothesDevAccessoryList) {
                    var data = res.clothesDevAccessoryList;
                    var accessoryHtml = "<table style='width: 100%; font-size: 16px; border: 1px solid black;margin-bottom: 0px;word-wrap:break-word;border-spacing:0 0' class='table'>";
                    accessoryHtml += "<tr><td style='font-weight: bolder; border: 1px solid black;'>名称</td><td style='font-weight: bolder; border: 1px solid black;'>编号</td><td style='font-weight: bolder; border: 1px solid black;'>规格</td><td style='font-weight: bolder; border: 1px solid black;'>颜色</td><td style='font-weight: bolder; border: 1px solid black;'>单位</td><td style='font-weight: bolder; border: 1px solid black;'>单耗</td><td style='font-weight: bolder; border: 1px solid black;'>供应商</td><td style='font-weight: bolder; border: 1px solid black;'>类别</td></tr>";
                    for (var i=0; i < data.length; i ++){
                        accessoryHtml += "<tr><td style='border: 1px solid black;'>"+data[i].accessoryName+"</td><td style='border: 1px solid black;'>"+data[i].accessoryNumber+"</td><td style='border: 1px solid black;'>"+data[i].specification+"</td><td style='border: 1px solid black;'>"+data[i].accessoryColor+"</td><td style='border: 1px solid black;'>"+data[i].accessoryUnit+"</td><td style='border: 1px solid black;'>"+data[i].pieceUsage+"</td><td style='border: 1px solid black;'>"+data[i].supplier+"</td><td style='border: 1px solid black;'>"+data[i].accessoryType+"</td></tr>";
                    }
                    accessoryHtml += "</table>";
                    $("#manufactureAccessoryTable").append(accessoryHtml);
                }else {
                    $("#manufactureAccessoryTable").append("辅料未入");
                }
                $("#measureDiv").empty();
                if(res.clothesDevMeasure) {
                    $("#measureDiv").html(res.clothesDevMeasure.measureImg);
                } else {
                    $("#measureDiv").html("<h3>尺寸未录入</h3>");
                }
                $("#processDiv").empty();
                if(res.clothesDevProcess) {
                    $("#processDiv").html(res.clothesDevProcess.processImg);
                } else {
                    $("#processDiv").html("<h3>工艺未录入</h3>");
                }
            },
            error: function () {
                layer.msg("获取详情信息失败！", {icon: 2});
                layer.close(index);
            }
        })
    },50)
}
function handleImgUpload(blobInfo, success, failure){
    var formdata = new FormData();
    // append 方法中的第一个参数就是 我们要上传文件 在后台接收的文件名
    // 这个值要根据后台来定义
    // 第二个参数是我们上传的文件
    formdata.append('file', blobInfo.blob());
    $.ajax({
        url: "/erp/wangEditorUploadImg",
        type: 'post',
        dataType:'json',
        contentType:false,//ajax上传图片需要添加
        processData:false,//ajax上传图片需要添加
        data: formdata,
        success: function (res) {
            if(res.data) {
                console.log(res)
                success(res.data[0]);
            }else {
                failure("上传图片失败");
            }
        },
        error: function () {
            layer.msg("上传图片失败", { icon: 2 });
        }
    });

}
function renderSeasonDate(ohd, sgl) {
    var ele = $(ohd);
    $Date.render({
        elem: ohd,
        type: 'month',
        format: 'yyyy年M季度',
        range: sgl ? null : '~',
        min: "1900-1-1",
        max: "2099-12-31",
        btns: ['clear', 'confirm'],
        ready: function (value, date, endDate) {
            var hd = $("#layui-laydate" + ele.attr("lay-key"));
            if (hd.length > 0) {
                hd.click(function () {
                    ren($(this));
                });
            }
            ren(hd);
        },
        done: function (value, date, endDate) {
            if (!isNull(date) && date.month > 0 && date.month < 5) {
                ele.attr("startDate", date.year + "-" + date.month);
            } else {
                ele.attr("startDate", "");
            }
            if (!isNull(endDate) && endDate.month > 0 && endDate.month < 5) {
                ele.attr("endDate", endDate.year + "-" + endDate.month)
            } else {
                ele.attr("endDate", "");
            }
        }
    });
    var ren = function (thiz) {
        var mls = thiz.find(".laydate-month-list");
        mls.each(function (i, e) {
            $(this).find("li").each(function (inx, ele) {
                var cx = ele.innerHTML;
                if (inx < 4) {
                    ele.innerHTML = cx.replace(/月/g, "季度");
                } else {
                    ele.style.display = "none";
                }
            });
        });
    }
}
function initDateForm(sgl, form) {
    if (isNull(form)) form = $(document.body);
    var ltm = function (tar, tars, tva) {
        tars.each(function () {
            $(this).removeAttr("lay-key");
            this.outerHTML = this.outerHTML;
        });
        tars = form.find(".dateTarget" + tar);
        tars.each(function () {
            var ele = $(this);
            if ("y" == tva) {
                $Date.render({
                    elem: this,
                    type: 'year',
                    range: '~',
                    format: 'yyyy年',
                    range: sgl ? null : '~',
                    done: function (value, date, endDate) {
                        if (typeof (date.year) == "number") ele.attr("startDate", date.year);
                        else ele.attr("startDate", "");
                        if (typeof (endDate.year) == "number") ele.attr("endDate", endDate.year);
                        else ele.attr("endDate", "");
                    }
                });

            } else if ("s" == tva) {
                ele.attr("startDate", "");
                ele.attr("endDate", "");
                renderSeasonDate(this, sgl);
            } else if ("m" == tva) {
                ele.attr("startDate", "");
                ele.attr("endDate", "");
                $Date.render({
                    elem: this,
                    type: 'month',
                    range: '~',
                    format: 'yyyy年MM月',
                    range: sgl ? null : '~',
                    done: function (value, date, endDate) {
                        if (typeof (date.month) == "number") ele.attr("startDate", date.year + "-" + date.month);
                        else ele.attr("startDate", "");
                        if (typeof (endDate.month) == "number") ele.attr("endDate", endDate.year + "-" + endDate.month);
                        else ele.attr("endDate", "");
                    }
                });
            } else if ("d" == tva) {
                ele.attr("startDate", "");
                ele.attr("endDate", "");
                $Date.render({
                    elem: this,
                    range: '~',
                    format: 'yyyy年MM月dd日',
                    range: sgl ? null : '~',
                    done: function (value, date, endDate) {
                        if (typeof (date.date) == "number") ele.attr("startDate", date.year + "-" + date.month + "-" + date.date);
                        else ele.attr("startDate", "");
                        if (typeof (endDate.date) == "number") ele.attr("endDate", endDate.year + "-" + endDate.month + "-" +
                            endDate.date);
                        else ele.attr("endDate", "");
                    }
                });
            }
        });
    }
    var sels = form.find(".dateSelector");
    sels.each(function (i, e) {
        var ths = this;
        var thiz = $(e);
        var tar = thiz.attr("date-target");
        thiz.next().find("dd").click(function () {
            var tva = thiz.val();
            var tars = form.find(".dateTarget" + tar);
            ltm(tar, tars, tva);
        });
        thiz.change(function () {
            var tva = $(this).val();
            var tars = form.find(".dateTarget" + tar);
            ltm(tar, tars, tva);
        });
        var tars = form.find(".dateTarget" + tar);
        ltm(tar, tars, thiz.val());
    });
}
function isNull(s) {
    if (s == null || typeof (s) == "undefined" || s == "") return true;
    return false;
}
function printDeal(){
    $("#printf").empty();
    var printBoxs = document.getElementsByName('printTable');
    var newContent = '';
    for(var i=0;i<printBoxs.length;i++) {
        newContent += printBoxs[i].innerHTML;
    }
    var f = document.getElementById('printf');
    // f.contentDocument.write('<link rel="stylesheet" type="text/css" href="/css/app.v2.css">');
    f.contentDocument.write('<style>' +
        'table {' +
        'border-collapse:collapse;' +
        '}' +
        'td {\n' +
        '        border: 1px solid #000000;\n' +
        '    }'+
        '</style>');
    f.contentDocument.write(newContent);
    f.contentDocument.close();
    window.frames['printf'].focus();
    try{
        window.frames['printf'].print();
    }catch(err){
        f.contentWindow.print();
    }
}
function unique(arr) {
    return arr.filter(function(item, index, arr) {
        //当前元素，在原始数组中的第一个索引==当前索引值，否则返回当前元素
        return arr.indexOf(item, 0) === index;
    });
}
function isNumber(val){

    var regPos = /^\d+(\.\d+)?$/; //非负浮点数
    var regNeg = /^(-(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*)))$/; //负浮点数
    if (regPos.test(val) || regNeg.test(val)){
        return true;
    }else{
        return false;
    }

}
function toDecimal(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*100)/100;
    return f;
}
function dateFormat(fmt, date) {
    var ret;
    var opt = {
        "Y+": date.getFullYear().toString(),        // 年
        "m+": (date.getMonth() + 1).toString(),     // 月
        "d+": date.getDate().toString(),            // 日
        "H+": date.getHours().toString(),           // 时
        "M+": date.getMinutes().toString(),         // 分
        "S+": date.getSeconds().toString()          // 秒
        // 有其他格式化字符需求可以继续添加，必须转化成字符串
    };
    for (var k in opt) {
        ret = new RegExp("(" + k + ")").exec(fmt);
        if (ret) {
            fmt = fmt.replace(ret[1], (ret[1].length == 1) ? (opt[k]) : (opt[k].padStart(ret[1].length, "0")))
        }
    }
    return fmt;
}
function moveCursor(event,obj) {
    if(event.keyCode==38){
        var name = $(obj).attr("name");
        var index = $("input[name='"+name+"']").index(obj);
        $("input[name='"+name+"']").eq(index-1).focus();
    }else if(event.keyCode==40){
        var name = $(obj).attr("name");
        var index = $("input[name='"+name+"']").index(obj);
        $("input[name='"+name+"']").eq(index+1).focus();
    }
}
