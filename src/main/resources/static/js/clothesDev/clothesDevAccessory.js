var userName = $("#userName").val();
var userRole = $("#userRole").val();
var form;
var supplierDemo;
layui.use('form', function(){
    form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
    form.render();
});

$(document).ready(function () {

    layui.laydate.render({
        elem: '#deadLine',
        trigger: 'click'
    });
    layui.laydate.render({
        elem: '#deliveryDate',
        trigger: 'click'
    });

    layui.use(['autocomplete'], function () {
        layui.autocomplete.render({
            elem: $('#accessoryName')[0],
            url: 'getclothesdevaccessorynamehint',
            template_val: '{{d}}',
            template_txt: '{{d}}'
        })
    });
    layui.use(['autocomplete'], function () {
        layui.autocomplete.render({
            elem: $('#accessoryNumber')[0],
            url: 'getclothesdevaccessorynumberhint',
            template_val: '{{d}}',
            template_txt: '{{d}}'
        })
    });

});
$(document).on('keydown', '.layui-table-edit', function (e) {
    var td = $(this).parent('td')
    ,tr = td.parent('tr')
    ,trs = tr.parent().parent().find('tr')
    ,tr_index = tr.index()
    ,td_index = td.index()
    ,td_last_index = tr.find('[data-edit="text"]:last').index()
    ,td_first_index = tr.find('[data-edit="text"]:first').index();
    switch (e.keyCode) {
        case 13:
            tr.next().find('td').eq(td_index).click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            return false;
        case 39:
            td.nextAll('[data-edit="text"]:first').click();
            if (td_index == td_last_index){
                tr.next().find('td').eq(td_first_index).click();
                if (tr_index == trs.length - 1){
                    trs.eq(0).find('td').eq(td_first_index).click();
                }
            }
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
        case 37:
            td.prevAll('[data-edit="text"]:first').click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
        case 38:
            tr.prev().find('td').eq(td_index).click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
        case 40:
            tr.next().find('td').eq(td_index).click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
    }

});
layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.26'
}).extend({                         // 模块别名
    soulTable: 'soulTable'
});
var dataTable;
layui.use(['form', 'soulTable', 'table', 'upload','yutons_sug', 'element'], function () {

    var table = layui.table,
        soulTable = layui.soulTable,
        element = layui.element,
        form = layui.form,
        $ = layui.$;

    dataTable = table.render({
        elem: '#dataTable'
        ,cols: [[]]
        ,loading:true
        ,data:[]
        ,height: 'full-80'
        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
        ,title: '开发辅料'
        ,totalRow: true
        ,page: true
        ,even: true
        ,overflow: 'tips'
        ,limits: [50, 100, 200]
        ,limit: 50 //每页默认显示的数量
        ,done: function (res, curr, count) {
            soulTable.render(this);
        }
    });

    initTable('','');

    function initTable(accessoryName, accessoryNumber){
        var param = {};
        if (accessoryName != null && accessoryName != ''){
            param.accessoryName = accessoryName;
        }
        if (accessoryNumber != null && accessoryNumber != ''){
            param.accessoryNumber = accessoryNumber;
        }
        param.storageType = "index";
        $.ajax({
            url: "/erp/getclothesdevaccessorymanagebyinfo",
            type: 'GET',
            data: param,
            success: function (res) {
                if (res.data) {
                    var reportData = res.data;
                    table.render({
                        elem: '#dataTable'
                        ,cols:[[
                            {title: '#', width: 50, collapse: true,lazy: true, icon: ['layui-icon layui-icon-triangle-r', 'layui-icon layui-icon-triangle-d'], children:[
                                    {
                                        title: '入库信息'
                                        ,url: 'getclothesdevaccessorymanagebyinfo'
                                        ,where: function(row){
                                            return {
                                                accessoryID: row.id,
                                                storageType: "in"
                                            }
                                        }
                                        ,height: 300
                                        ,loading:true
                                        ,page: false
                                        ,totalRow: true
                                        ,toolbar: '<div><a class="layui-btn layui-btn-danger layui-btn-sm" lay-event="batchDelete">删除</a></div>'
                                        ,cols: [[
                                            {type:'checkbox'}
                                            ,{field:'accessoryName', title:'名称', align:'center', width:170, sort: true, filter: true}
                                            ,{field:'accessoryNumber', title:'编号', align:'center', width:120, sort: true, filter: true}
                                            ,{field:'specification', title:'规格', align:'center', width:120, sort: true, filter: true}
                                            ,{field:'accessoryColor', title:'颜色', align:'center', width:120, sort: true, filter: true}
                                            ,{field:'accessoryColorNumber', title:'色号', align:'center',width:200, sort: true, filter: true}
                                            ,{field:'unit', title:'单位', align:'center', width:90, sort: true, filter: true}
                                            ,{field:'inStoreCount', title:'入库数量', align:'center', width:120, sort: true, filter: true}
                                            ,{field:'price', title:'单价', align:'center', width:90, sort: true, filter: true}
                                            ,{field:'supplier', title:'供应商', align:'center', width:120, sort: true, filter: true}
                                            ,{field:'operateDate', title:'日期', align:'center', width:120, sort: true, filter: true, templet:function (d) {
                                                    return moment(d.operateDate).format("YYYY-MM-DD");
                                                }}
                                            ,{field:'location', title:'位置', align:'center', width:120, sort: true, filter: true}
                                            ,{field:'remark', title:'备注', align:'center', width:120, sort: true, filter: true}
                                            ,{field:'id', hide: true}
                                            ,{field:'accessoryID', hide: true}
                                        ]]
                                        ,toolEvent: function (obj, pobj) {

                                        }
                                        ,toolbarEvent: function (obj, pobj) {
                                            // obj 子表当前行对象
                                            // pobj 父表当前行对象
                                            var objData = obj.data;
                                            var childId = this.id;
                                            var accessoryID;
                                            if (obj.event === 'batchDelete') {
                                                var checkStatus = table.checkStatus(obj.config.id);
                                                if(checkStatus.data.length == 0) {
                                                    layer.msg('请选择数据');
                                                }else {
                                                    var idList = [];
                                                    var inStoreCount = 0;
                                                    $.each(checkStatus.data, function (index, item) {
                                                        idList.push(item.id);
                                                        inStoreCount += item.inStoreCount;
                                                        accessoryID = item.accessoryID
                                                    });
                                                    layer.confirm('确认全部删除吗?', function(index){
                                                        $.ajax({
                                                            url: "/erp/deleteclothesdevaccessorymanageinbatch",
                                                            type: 'POST',
                                                            data: {
                                                                idList: idList,
                                                                id: accessoryID,
                                                                inStoreCount: inStoreCount
                                                            },
                                                            traditional: true,
                                                            success: function (res) {
                                                                if (res.result == 0) {
                                                                    layer.close(index);
                                                                    layer.msg("删除成功！", {icon: 1});
                                                                    table.reload(childId);
                                                                } else if (res.result == 4) {
                                                                    layer.msg("库存不足！", {icon: 2});
                                                                } else {
                                                                    layer.msg("删除失败！", {icon: 2});
                                                                }
                                                            }, error: function () {
                                                                layer.msg("删除失败！", {icon: 2});
                                                            }
                                                        })
                                                    });
                                                }
                                            }
                                        }
                                        ,done: function () {
                                            soulTable.render(this);
                                        }
                                        ,filter: {
                                            bottom: false,
                                            items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                                        }
                                    }, {
                                        title: '库存信息'
                                        ,url: 'getclothesdevaccessorymanagebyinfo'
                                        ,where: function(row){
                                            return {
                                                accessoryID: row.id,
                                                storageType: "storage"
                                            }
                                        }
                                        ,height: 300
                                        ,loading:true
                                        ,page: false
                                        ,totalRow: true
                                        ,cols: [[
                                            {field:'accessoryName', title:'名称', align:'center', width:170, sort: true, filter: true}
                                            ,{field:'accessoryNumber', title:'编号', align:'center', width:120, sort: true, filter: true}
                                            ,{field:'specification', title:'规格', align:'center', width:120, sort: true, filter: true}
                                            ,{field:'accessoryColor', title:'颜色', align:'center', width:120, sort: true, filter: true}
                                            ,{field:'accessoryColorNumber', title:'色号', align:'center',width:200, sort: true, filter: true}
                                            ,{field:'unit', title:'单位', align:'center', width:90, sort: true, filter: true}
                                            ,{field:'storageCount', title:'库存数量', align:'center', width:120, sort: true, filter: true}
                                            ,{field:'price', title:'单价', align:'center', width:90, sort: true, filter: true}
                                            ,{field:'supplier', title:'供应商', align:'center', width:120, sort: true, filter: true}
                                            ,{field:'operateDate', title:'日期', align:'center', width:120, sort: true, filter: true, templet:function (d) {
                                                    return moment(d.operateDate).format("YYYY-MM-DD");
                                                }}
                                            ,{field:'location', title:'位置', align:'center', width:120, sort: true, filter: true}
                                            ,{field:'remark', title:'备注', align:'center', width:120, sort: true, filter: true}
                                            ,{field:'id', hide: true}
                                        ]]
                                        ,done: function () {
                                            soulTable.render(this);
                                        }
                                        ,filter: {
                                            bottom: false,
                                            items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                                        }
                                    },{
                                        title: '出库信息'
                                        ,url: 'getclothesdevaccessorymanagebyinfo'
                                        ,where: function(row){
                                            return {
                                                accessoryID: row.id,
                                                storageType: "out"
                                            }
                                        }
                                        ,height: 300
                                        ,loading:true
                                        ,page: false
                                        ,totalRow: true
                                        ,toolbar: '<div><a class="layui-btn layui-btn-danger layui-btn-sm" lay-event="batchDelete">删除</a></div>'
                                        ,cols: [[
                                            {type:'checkbox'}
                                            ,{field:'accessoryName', title:'名称', align:'center', width:170, sort: true, filter: true}
                                            ,{field:'accessoryNumber', title:'编号', align:'center', width:120, sort: true, filter: true}
                                            ,{field:'specification', title:'规格', align:'center', width:120, sort: true, filter: true}
                                            ,{field:'accessoryColor', title:'颜色', align:'center', width:120, sort: true, filter: true}
                                            ,{field:'accessoryColorNumber', title:'色号', align:'center',width:200, sort: true, filter: true}
                                            ,{field:'unit', title:'单位', align:'center', width:90, sort: true, filter: true}
                                            ,{field:'outStoreCount', title:'出库数量', align:'center', width:120, sort: true, filter: true}
                                            ,{field:'price', title:'单价', align:'center', width:90, sort: true, filter: true}
                                            ,{field:'supplier', title:'供应商', align:'center', width:120, sort: true, filter: true}
                                            ,{field:'operateDate', title:'日期', align:'center', width:120, sort: true, filter: true, templet:function (d) {
                                                    return moment(d.operateDate).format("YYYY-MM-DD");
                                                }}
                                            // ,{field:'location', title:'位置', align:'center', width:120, sort: true, filter: true}
                                            ,{field:'remark', title:'备注', align:'center', width:120, sort: true, filter: true}
                                            ,{field:'id', hide: true}
                                            ,{field:'accessoryID', hide: true}
                                        ]]
                                        ,toolbarEvent: function (obj, pobj) {
                                            // obj 子表当前行对象
                                            // pobj 父表当前行对象
                                            var objData = obj.data;
                                            var childId = this.id;
                                            if (obj.event === 'batchDelete') {
                                                var checkStatus = table.checkStatus(obj.config.id);
                                                if(checkStatus.data.length == 0) {
                                                    layer.msg('请选择数据');
                                                }else {
                                                    var idList = [];
                                                    var outStoreCount = 0;
                                                    var accessoryID;
                                                    $.each(checkStatus.data, function (index, item) {
                                                        idList.push(item.id);
                                                        accessoryID = item.accessoryID;
                                                        outStoreCount += item.outStoreCount;
                                                    });
                                                    layer.confirm('确认全部删除吗?', function(index){
                                                        $.ajax({
                                                            url: "/erp/deleteclothesdevaccessorymanageoutbatch",
                                                            type: 'POST',
                                                            data: {
                                                                idList: idList,
                                                                id: accessoryID,
                                                                outStoreCount: outStoreCount
                                                            },
                                                            traditional: true,
                                                            success: function (res) {
                                                                if (res.result == 0) {
                                                                    layer.close(index);
                                                                    layer.msg("删除成功！", {icon: 1});
                                                                    table.reload(childId);
                                                                } else if (res.result == 4) {
                                                                    layer.msg("库存异常！", {icon: 2});
                                                                } else {
                                                                    layer.msg("删除失败！", {icon: 2});
                                                                }
                                                            }, error: function () {
                                                                layer.msg("删除失败！", {icon: 2});
                                                            }
                                                        })
                                                    });
                                                }
                                            }
                                        }
                                        ,done: function () {
                                            soulTable.render(this);
                                        }
                                        ,filter: {
                                            bottom: false,
                                            items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                                        }
                                    }
                                ]}
                            ,{type:'numbers', align:'center', title:'序号', width:60}
                            ,{field:'accessoryName', title:'名称', align:'center', width:170, sort: true, filter: true}
                            ,{field:'accessoryNumber', title:'编号', align:'center', width:120, sort: true, filter: true}
                            ,{field:'specification', title:'规格', align:'center', width:120, sort: true, filter: true}
                            ,{field:'accessoryColor', title:'颜色', align:'center', width:120, sort: true, filter: true}
                            ,{field:'accessoryColorNumber', title:'色号', align:'center',width:200, sort: true, filter: true}
                            ,{field:'unit', title:'单位', align:'center', width:90, sort: true, filter: true}
                            ,{field:'price', title:'单价', align:'center', width:90, sort: true, filter: true}
                            ,{field:'supplier', title:'供应商', align:'center', width:120, sort: true, filter: true}
                            ,{field:'remark', title:'备注', align:'center', width:120, sort: true, filter: true}
                            ,{field:'id', fixed: 'right', title:'操作', align:'center', toolbar: '#barTop', width:280}
                        ]]
                        ,data:reportData
                        ,height: 'full-80'
                        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                        ,title: '开发面料'
                        ,totalRow: true
                        ,page: true
                        ,even: true
                        ,overflow: 'tips'
                        ,limits: [50, 100, 200]
                        ,limit: 50 //每页默认显示的数量
                        ,done: function (res, curr, count) {
                            soulTable.render(this);
                        }
                    });
                }
            }
        })
    }

    form.on('submit(searchBeat)', function(data){
        var filterAccessoryName = $("#accessoryName").val();
        var filterAccessoryNumber = $("#accessoryNumber").val();
        initTable(filterAccessoryName, filterAccessoryNumber);
        return false;
    });

    table.on('toolbar(dataTable)', function(obj){
        if (obj.event === 'clearFilter') {
            // 清除所有筛选条件并重载表格
            // 参数: tableId
            soulTable.clearFilter('dataTable');
        }else if (obj.event === 'refresh') {
            initTable('', '');
        } else if (obj.event === 'exportExcel') {
            soulTable.export('dataTable');
        }else if(obj.event === 'addNew') {  //录入基础信息
            var index = layer.open({
                type: 1 //Page层类型
                , title: "新增"
                , btn: ['保存']
                , area: ['1000px','500px']
                , shade: 0.6 //遮罩透明度
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , content: $("#accessoryOperate")
                ,yes: function(i, layero){
                    var param = {};
                    param.accessoryName = $("#accessoryNameAdd").val();
                    param.accessoryNumber = $("#accessoryNumberAdd").val() + $("#accessoryNumberOrderAdd").val();
                    param.specification = $("#specification").val();
                    param.unit = $("#unit").val();
                    var accessoryColor = colorSelect.getValue('name')[0];
                    param.accessoryColorNumber = accessoryColor.split("/")[0];
                    param.accessoryColor = accessoryColor.split("/")[1];
                    param.price = $("#price").val();
                    param.supplier = supplierDemo.getValue('nameStr');
                    param.remark = $("#remark").val();
                    param.storageType = "index";
                    if (param.accessoryName == null || param.accessoryName == '' || param.accessoryNumber == null || param.accessoryNumber == '' || param.unit == null || param.unit == '' || param.price == null || param.price == '' || param.accessoryColorNumber == null || param.accessoryColorNumber == '' || param.accessoryColor == null || param.accessoryColor == ''){
                        layer.msg("请输入完整必要信息~~~");
                        return false;
                    }
                    $.ajax({
                        url: "/erp/addclothesdevaccessorymanage",
                        type: 'POST',
                        data: {
                            clothesDevAccessoryManageJson: JSON.stringify(param)
                        },
                        success: function (res) {
                            if (res.result == 0) {
                                layer.close(index);
                                layer.msg("添加成功！", {icon: 1});
                                initTable('', '');
                                form.val("accessoryAdd", { //formTest 即 class="layui-form" 所在元素属性 lay-filter="" 对应的值
                                    "accessoryNameAdd": '' // "name": "value"
                                    ,"accessoryNumberAdd": ''
                                    ,"accessoryNumberOrderAdd": ''
                                    ,"specification": ''
                                    ,"unit": ''
                                    ,"price": ''
                                    // ,"supplier": ''
                                    ,"remark": ''
                                });
                            } else {
                                layer.msg("添加失败！", {icon: 2});
                            }
                        },
                        error: function () {
                            layer.msg("添加失败", { icon: 2 });
                        }
                    });
                }
                ,cancel: function () {
                    form.val("accessoryAdd", { //formTest 即 class="layui-form" 所在元素属性 lay-filter="" 对应的值
                        "accessoryNameAdd": '' // "name": "value"
                        ,"accessoryNumberAdd": ''
                        ,"accessoryNumberOrderAdd": ''
                        ,"specification": ''
                        ,"unit": ''
                        ,"price": ''
                        // ,"supplier": ''
                        ,"remark": ''
                    });
                }
            });
            form.render('select');
            $.ajax({
                url: "/erp/getallcolormanage",
                type: 'GET',
                data: {},
                success: function (res) {
                    if (res.data) {
                        colorNameData = res.data;
                        var thisData = [];
                        $.each(res.data, function(index,element){
                            thisData.push({name:element.colorNumber + "/" + element.colorName, value: element.id});
                        });
                        colorSelect = xmSelect.render({
                            el: '#colorInfo',
                            radio: true,
                            filterable: true,
                            height: '200px',
                            data: thisData
                        });
                    }
                },
                error: function () {
                    layer.msg("获取颜色失败", { icon: 2 });
                }
            });
            $.ajax({
                url: "/erp/getallsupplierinfo",
                data: {
                    supplierType: "辅料"
                },
                async:false,
                success:function(res){
                    if (res.data){
                        var resultData = [];
                        $.each(res.data, function(index,element){
                            if (element != null && element != ''){
                                resultData.push({name:element.supplierName, value:element.supplierName});
                            }
                        });
                        supplierDemo = xmSelect.render({
                            el: '#supplier',
                            radio: true,
                            filterable: true,
                            data: resultData
                        });
                    }
                }, error:function(){
                }
            });
            $.ajax({
                url: "/erp/getallclothesdevaccessorynumber",
                type: 'GET',
                data: {},
                success: function (res) {
                    if (res.data) {
                        $("#accessoryNumberAdd").empty();
                        $("#accessoryNumberAdd").append("<option value=''>选择代码</option>");
                        $.each(res.data, function(index,element){
                            $("#accessoryNumberAdd").append("<option value='"+element.accessoryNumberCode+"'>"+ element.accessoryNumberCode + "/" + element.accessoryNumberName +"</option>")
                        });
                        form.render();
                    }
                },
                error: function () {
                    layer.msg("获取数据失败", { icon: 2 });
                }
            });
            form.on('select(demo)', function(data){
                var accessoryNumberCode = $("#accessoryNumberAdd").val();
                if (accessoryNumberCode != null && accessoryNumberCode != ''){
                    $.ajax({
                        url: "/erp/getaccessorynameorderbyprefix",
                        data: {
                            accessoryNumberCode: accessoryNumberCode
                        },
                        success:function(data){
                            if (data.accessoryOrder){
                                $("#accessoryNumberOrderAdd").val(data.accessoryOrder);
                            }
                        }, error:function(){
                        }
                    });
                    form.render('select');
                }
            });
        } else if(obj.event === 'storage') {  //录入基础信息
            var index = layer.open({
                type: 1 //Page层类型
                , title: "共用辅料库存"
                , btn: []
                , shade: 0.6 //遮罩透明度
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , content: "<table id='storageTable' lay-filter='storageTable'></table>"
            });
            layer.full(index);
            setTimeout(function () {
                $.ajax({
                    url: "/erp/getshareaccessorystorage",
                    type: 'GET',
                    data: {
                        storageType: 'storage'
                    },
                    success: function (res) {
                        if (res.data) {
                            var reportData = res.data;
                            table.render({
                                elem: '#storageTable'
                                ,cols:[[
                                    {type:'numbers', align:'center', title:'序号', width:60}
                                    ,{field:'accessoryName', title:'名称', align:'center', width:170, sort: true, filter: true}
                                    ,{field:'accessoryNumber', title:'编号', align:'center', width:120, sort: true, filter: true}
                                    ,{field:'specification', title:'规格', align:'center', width:120, sort: true, filter: true}
                                    ,{field:'accessoryColor', title:'颜色', align:'center', width:120, sort: true, filter: true}
                                    ,{field:'accessoryColorNumber', title:'色号', align:'center',width:200, sort: true, filter: true}
                                    ,{field:'unit', title:'单位', align:'center', width:90, sort: true, filter: true}
                                    ,{field:'storageCount', title:'库存数量', align:'center', width:120, sort: true, filter: true}
                                    ,{field:'price', title:'单价', align:'center', width:90, sort: true, filter: true}
                                    ,{field:'supplier', title:'供应商', align:'center', width:120, sort: true, filter: true}
                                    ,{field:'operateDate', title:'日期', align:'center', width:120, sort: true, filter: true, templet:function (d) {
                                            return moment(d.operateDate).format("YYYY-MM-DD");
                                        }}
                                    ,{field:'location', title:'位置', align:'center', width:120, sort: true, filter: true}
                                    ,{field:'remark', title:'备注', align:'center', width:120, sort: true, filter: true}
                                    ,{field:'id', hide: true}
                                ]]
                                ,data:reportData
                                ,height: 'full-130'
                                ,title: '辅料库存'
                                ,totalRow: true
                                ,page: true
                                ,even: true
                                ,overflow: 'tips'
                                ,limits: [50, 100, 200]
                                ,limit: 50 //每页默认显示的数量
                                ,done: function (res, curr, count) {
                                    soulTable.render(this);
                                }
                            });
                        }
                    }
                })
            },100)
        }
    });

    //监听行工具事件
    table.on('tool(dataTable)', function(obj){
        var data = obj.data;
        if(obj.event === 'del'){
            layer.confirm('该辅料删除后所有的出入库记录都会删除,确认删除吗', function(index){
                $.ajax({
                    url: "/erp/deleteclothesdevaccessorymanagealltype",
                    type: 'POST',
                    data: {
                        id:data.id
                    },
                    success: function (res) {
                        if (res.result == 0) {
                            initTable('', '');
                            layer.close(index);
                            layer.msg("删除成功！", {icon: 1});
                        } else {
                            layer.msg("删除失败！", {icon: 2});
                        }
                    },
                    error: function () {
                        layer.msg("删除失败！", {icon: 2});
                    }
                })

            });
        } else if(obj.event === 'inStore'){
            var index = layer.open({
                type: 1 //Page层类型
                , title: "入库"
                , btn: ['保存']
                , area: ['850px','300px']
                , shade: 0.6 //遮罩透明度
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , content: $("#inStoreOperate")
                ,yes: function(i, layero){
                    var param = {};
                    param.inStoreCount = $("#inStoreCount").val();
                    param.price = $("#inStorePrice").val();
                    param.payState = $("#inStorePayState").val();
                    param.operateDate = $("#inStoreDate").val();
                    param.location = $("#inStoreLocation").val();
                    if (param.inStoreCount == null || param.inStoreCount == '' || param.price == null || param.price == '' || param.location == null || param.location == '' || param.operateDate == null || param.operateDate == ''){
                        layer.msg("请输入完整必要信息~~~");
                        return false;
                    }
                    $.ajax({
                        url: "/erp/clothesdevaccessorymanageinstore",
                        type: 'POST',
                        data: {
                            id: data.id,
                            inStoreCount: param.inStoreCount,
                            price: param.price,
                            payState: param.payState,
                            operateDate: param.operateDate,
                            location: param.location
                        },
                        success: function (res) {
                            if (res.result == 0) {
                                layer.close(index);
                                layer.msg("添加成功！", {icon: 1});
                                initTable('', '');
                                $("#inStoreCount").val('');
                                $("#inStorePrice").val('');
                                $("#inStoreUnit").val('');
                                $("#inStoreLocation").val('');
                                $("#inStoreDate").val('');
                            } else {
                                layer.msg("添加失败！", {icon: 2});
                            }
                        },
                        error: function () {
                            layer.msg("添加失败", { icon: 2 });
                        }
                    });
                }
                ,cancel:function () {
                    $("#inStoreCount").val('');
                    $("#inStorePrice").val('');
                    $("#inStoreUnit").val('');
                    $("#inStoreLocation").val('');
                    $("#inStoreDate").val('');
                }
            });
            $("#inStoreUnit").val(data.unit);
            $("#inStorePrice").val(data.price);
            layui.laydate.render({
                elem: '#inStoreDate',
                trigger: 'click'
            });
        } else if(obj.event === 'add'){
            var index = layer.open({
                type: 1 //Page层类型
                , title: "新增"
                , btn: ['保存']
                , area: ['1000px','500px']
                , shade: 0.6 //遮罩透明度
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , content: $("#accessoryOperate")
                ,yes: function(i, layero){
                    var param = {};
                    param.accessoryName = $("#accessoryNameAdd").val();
                    param.accessoryNumber = $("#accessoryNumberAdd").val() + $("#accessoryNumberOrderAdd").val();
                    param.specification = $("#specification").val();
                    param.unit = $("#unit").val();
                    var accessoryColor = colorSelect.getValue('name')[0];
                    param.accessoryColorNumber = accessoryColor.split("/")[0];
                    param.accessoryColor = accessoryColor.split("/")[1];
                    param.price = $("#price").val();
                    param.supplier = supplierDemo.getValue('nameStr');
                    param.remark = $("#remark").val();
                    param.storageType = "index";
                    if (param.accessoryName == null || param.accessoryName == '' || param.accessoryNumber == null || param.accessoryNumber == '' || param.unit == null || param.unit == '' || param.price == null || param.price == '' || param.accessoryColorNumber == null || param.accessoryColorNumber == '' || param.accessoryColor == null || param.accessoryColor == ''){
                        layer.msg("请输入完整必要信息~~~");
                        return false;
                    }
                    $.ajax({
                        url: "/erp/addclothesdevaccessorymanage",
                        type: 'POST',
                        data: {
                            clothesDevAccessoryManageJson: JSON.stringify(param)
                        },
                        success: function (res) {
                            if (res.result == 0) {
                                layer.close(index);
                                layer.msg("添加成功！", {icon: 1});
                                initTable('', '');
                                form.val("accessoryAdd", { //formTest 即 class="layui-form" 所在元素属性 lay-filter="" 对应的值
                                    "accessoryNameAdd": '' // "name": "value"
                                    ,"accessoryNumberAdd": ''
                                    ,"accessoryNumberOrderAdd": ''
                                    ,"specification": ''
                                    ,"unit": ''
                                    ,"price": ''
                                    ,"supplier": ''
                                    ,"remark": ''
                                });
                            } else {
                                layer.msg("添加失败！", {icon: 2});
                            }
                        },
                        error: function () {
                            layer.msg("添加失败", { icon: 2 });
                        }
                    });
                }
                ,cancel: function () {
                    form.val("accessoryAdd", { //formTest 即 class="layui-form" 所在元素属性 lay-filter="" 对应的值
                        "accessoryNameAdd": '' // "name": "value"
                        ,"accessoryNumberAdd": ''
                        ,"accessoryNumberOrderAdd": ''
                        ,"specification": ''
                        ,"unit": ''
                        ,"price": ''
                        ,"supplier": ''
                        ,"remark": ''
                    });
                }
            });
            form.render('select');
            $.ajax({
                url: "/erp/getallcolormanage",
                type: 'GET',
                data: {},
                success: function (res) {
                    if (res.data) {
                        colorNameData = res.data;
                        var thisData = [];
                        $.each(res.data, function(index,element){
                            if (data.accessoryColor == element.colorName && data.accessoryColorNumber == element.colorNumber){
                                thisData.push({name:element.colorNumber + "/" + element.colorName, value: element.id, selected: true});
                            } else {
                                thisData.push({name:element.colorNumber + "/" + element.colorName, value: element.id});
                            }
                        });
                        colorSelect = xmSelect.render({
                            el: '#colorInfo',
                            radio: true,
                            filterable: true,
                            height: '200px',
                            data: thisData
                        });
                    }
                },
                error: function () {
                    layer.msg("获取颜色失败", { icon: 2 });
                }
            });
            $.ajax({
                url: "/erp/getallsupplierinfo",
                data: {
                    supplierType: "辅料"
                },
                async:false,
                success:function(res){
                    if (res.data){
                        var resultData = [];
                        $.each(res.data, function(index,element){
                            if (element != null && element != ''){
                                if (element.supplierName === data.supplier){
                                    resultData.push({name:element.supplierName, selected: true, value:element.supplierName});
                                } else {
                                    resultData.push({name:element.supplierName, value:element.supplierName});
                                }
                            }
                        });
                        supplierDemo = xmSelect.render({
                            el: '#supplier',
                            radio: true,
                            filterable: true,
                            data: resultData
                        });
                    }
                }, error:function(){
                }
            });
            $.ajax({
                url: "/erp/getallclothesdevaccessorynumber",
                type: 'GET',
                data: {},
                success: function (res) {
                    if (res.data) {
                        $("#accessoryNumberAdd").empty();
                        $("#accessoryNumberAdd").append("<option value=''>选择代码</option>");
                        $.each(res.data, function(index,element){
                            if (element.accessoryNumberCode == data.accessoryNumber.slice(0,2)){
                                $("#accessoryNumberAdd").append("<option value='"+element.accessoryNumberCode+"' selected>"+ element.accessoryNumberCode + "/" + element.accessoryNumberName +"</option>");
                            } else {
                                $("#accessoryNumberAdd").append("<option value='"+element.accessoryNumberCode+"'>"+ element.accessoryNumberCode + "/" + element.accessoryNumberName +"</option>");
                            }

                        });
                        form.render();
                    }
                },
                error: function () {
                    layer.msg("获取数据失败", { icon: 2 });
                }
            });
            form.on('select(demo)', function(data){
                var accessoryNumberCode = $("#accessoryNumberAdd").val();
                if (accessoryNumberCode != null && accessoryNumberCode != ''){
                    $.ajax({
                        url: "/erp/getaccessorynameorderbyprefix",
                        data: {
                            accessoryNumberCode: accessoryNumberCode
                        },
                        success:function(data){
                            if (data.accessoryOrder){
                                $("#accessoryNumberOrderAdd").val(data.accessoryOrder);
                            }
                        }, error:function(){
                        }
                    });
                    form.render('select');
                }
            });
            form.val("accessoryAdd", { //formTest 即 class="layui-form" 所在元素属性 lay-filter="" 对应的值
                "accessoryNameAdd": data.accessoryName // "name": "value"
                // ,"accessoryNumberAdd": data.accessoryNumber.slice(0,2)
                ,"accessoryNumberOrderAdd": data.accessoryNumber.slice(2)
                ,"specification": data.specification
                ,"unit": data.unit
                ,"price": data.price
                // ,"supplier": data.supplier
                ,"remark": data.remark
            });
        } else if(obj.event === 'outStore'){
            var index = layer.open({
                type: 1 //Page层类型
                , title: "出库"
                , btn: ['保存']
                , area: ['600px','300px']
                , shade: 0.6 //遮罩透明度
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , content: $("#outStoreOperate")
                ,yes: function(i, layero){
                    var param = {};
                    param.outStoreCount = $("#outStoreCount").val();
                    if (param.outStoreCount == null || param.outStoreCount == ''){
                        layer.msg("请输入完整必要信息~~~");
                        return false;
                    }
                    $.ajax({
                        url: "/erp/clothesdevaccessorymanageoutstore",
                        type: 'POST',
                        data: {
                            id: data.id,
                            outStoreCount: param.outStoreCount
                        },
                        success: function (res) {
                            if (res.result == 0) {
                                layer.close(index);
                                layer.msg("添加成功！", {icon: 1});
                                initTable('', '');
                            } else if (res.result == 4) {
                                layer.msg("库存不足！", {icon: 2});
                            } else {
                                layer.msg("添加失败！", {icon: 2});
                            }
                        },
                        error: function () {
                            layer.msg("添加失败", { icon: 2 });
                        }
                    });
                }
            });
        } else if(obj.event === 'update'){
            var index = layer.open({
                type: 1 //Page层类型
                , title: "修改"
                , btn: ['保存']
                , area: ['1000px','500px']
                , shade: 0.6 //遮罩透明度
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , content: $("#accessoryOperate")
                ,yes: function(i, layero){
                    var param = {};
                    param.accessoryName = $("#accessoryNameAdd").val();
                    param.accessoryNumber = $("#accessoryNumberAdd").val() + $("#accessoryNumberOrderAdd").val();
                    param.specification = $("#specification").val();
                    param.unit = $("#unit").val();
                    var accessoryColor = colorSelect.getValue('name')[0];
                    param.accessoryColorNumber = accessoryColor.split("/")[0];
                    param.accessoryColor = accessoryColor.split("/")[1];
                    param.price = $("#price").val();
                    param.supplier = supplierDemo.getValue('nameStr');
                    param.remark = $("#remark").val();
                    param.storageType = "index";
                    param.id = data.id;
                    if (param.accessoryName == null || param.accessoryName == '' || param.accessoryNumber == null || param.accessoryNumber == '' || param.unit == null || param.unit == '' || param.price == null || param.price == '' || param.accessoryColorNumber == null || param.accessoryColorNumber == '' || param.accessoryColor == null || param.accessoryColor == ''){
                        layer.msg("请输入完整必要信息~~~");
                        return false;
                    }
                    $.ajax({
                        url: "/erp/updateclothesdevaccessorymanagetotal",
                        type: 'POST',
                        data: {
                            clothesDevAccessoryManageJson: JSON.stringify(param),
                            id: data.id
                        },
                        success: function (res) {
                            if (res.result == 0) {
                                layer.close(index);
                                layer.msg("添加成功！", {icon: 1});
                                initTable('', '');
                            } else {
                                layer.msg("添加失败！", {icon: 2});
                            }
                        },
                        error: function () {
                            layer.msg("添加失败", { icon: 2 });
                        }
                    });
                }
            });
            form.render('select');
            $.ajax({
                url: "/erp/getallcolormanage",
                type: 'GET',
                data: {},
                success: function (res) {
                    if (res.data) {
                        colorNameData = res.data;
                        var thisData = [];
                        $.each(res.data, function(index,element){
                            if (data.accessoryColor == element.colorName && data.accessoryColorNumber == element.colorNumber){
                                thisData.push({name:element.colorNumber + "/" + element.colorName, value: element.id, selected: true});
                            } else {
                                thisData.push({name:element.colorNumber + "/" + element.colorName, value: element.id});
                            }
                        });
                        colorSelect = xmSelect.render({
                            el: '#colorInfo',
                            radio: true,
                            filterable: true,
                            height: '200px',
                            data: thisData
                        });
                    }
                },
                error: function () {
                    layer.msg("获取颜色失败", { icon: 2 });
                }
            });
            $.ajax({
                url: "/erp/getallsupplierinfo",
                data: {
                    supplierType: "辅料"
                },
                async:false,
                success:function(res){
                    if (res.data){
                        var resultData = [];
                        $.each(res.data, function(index,element){
                            if (element != null && element != ''){
                                if (element.supplierName === data.supplier){
                                    resultData.push({name:element.supplierName, selected: true, value:element.supplierName});
                                } else {
                                    resultData.push({name:element.supplierName, value:element.supplierName});
                                }
                            }
                        });
                        supplierDemo = xmSelect.render({
                            el: '#supplier',
                            radio: true,
                            filterable: true,
                            data: resultData
                        });
                    }
                }, error:function(){
                }
            });
            $.ajax({
                url: "/erp/getallclothesdevaccessorynumber",
                type: 'GET',
                data: {},
                success: function (res) {
                    if (res.data) {
                        $("#accessoryNumberAdd").empty();
                        $("#accessoryNumberAdd").append("<option value=''>选择代码</option>");
                        $.each(res.data, function(index,element){
                            if (element.accessoryNumberCode == data.accessoryNumber.slice(0,2)){
                                $("#accessoryNumberAdd").append("<option value='"+element.accessoryNumberCode+"' selected>"+ element.accessoryNumberCode + "/" + element.accessoryNumberName +"</option>");
                            } else {
                                $("#accessoryNumberAdd").append("<option value='"+element.accessoryNumberCode+"'>"+ element.accessoryNumberCode + "/" + element.accessoryNumberName +"</option>");
                            }

                        });
                        form.render();
                    }
                },
                error: function () {
                    layer.msg("获取数据失败", { icon: 2 });
                }
            });
            form.on('select(demo)', function(data){
                var accessoryNumberCode = $("#accessoryNumberAdd").val();
                if (accessoryNumberCode != null && accessoryNumberCode != ''){
                    $.ajax({
                        url: "/erp/getaccessorynameorderbyprefix",
                        data: {
                            accessoryNumberCode: accessoryNumberCode
                        },
                        success:function(data){
                            if (data.accessoryOrder){
                                $("#accessoryNumberOrderAdd").val(data.accessoryOrder);
                            }
                        }, error:function(){
                        }
                    });
                    form.render('select');
                }
            });
            form.val("accessoryAdd", { //formTest 即 class="layui-form" 所在元素属性 lay-filter="" 对应的值
                "accessoryNameAdd": data.accessoryName // "name": "value"
                ,"accessoryNumberAdd": data.accessoryNumber.slice(0,3)
                ,"accessoryNumberOrderAdd": data.accessoryNumber.slice(3)
                ,"specification": data.specification
                ,"unit": data.unit
                ,"price": data.price
                ,"supplier": data.supplier
                ,"remark": data.remark
            });
        }
    });
});

function toDecimal(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*100)/100;
    return f;
}

function moveCursor(event,obj) {
    if(event.keyCode==38){
        var name = $(obj).attr("name");
        var index = $("input[name='"+name+"']").index(obj);
        $("input[name='"+name+"']").eq(index-1).focus();
    }else if(event.keyCode==40){
        var name = $(obj).attr("name");
        var index = $("input[name='"+name+"']").index(obj);
        $("input[name='"+name+"']").eq(index+1).focus();
    }
}
