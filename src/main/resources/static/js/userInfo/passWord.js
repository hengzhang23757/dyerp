var form;
layui.use('form', function(){
    form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
    form.render();
});
layui.config({
    base: '/layuiadmin/' //静态资源所在路径
}).extend({
    index: 'lib/index' //主入口模块
}).use(['index', 'set']);

$(document).ready(function () {
    var userName = $("#loginUserName").val();
    $.ajax({
        url: "/erp/checkmessagedot",
        type: 'GET',
        data: {
            receiveUser: userName
        },
        success: function (data) {
            if(data.messageCount == 0) {
                $("#noticeDot").hide();
            }else {
                if(data.messageCount < 10){
                    $("#noticeDot").text(data.messageCount);
                }else{
                    $("#noticeDot").text("...");
                }
            }
        },
        error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }
    });

});
layui.use(['form'], function () {
    var form = layui.form;
        $ = layui.$;
    form.on('submit(updatePassWord)', function(data){
        var userName = $("#loginUserName").val();
        console.log(userName);
        var newPassWord = $("#password").val();
        var repassword = $("#repassword").val();
        if (newPassWord != repassword){
            layer.msg("两次输入不一致");
        } else {
            $.ajax({
                url: "/erp/updateUser",
                type: 'GET',
                data: {
                    originPassWord:$("#oldPassword").val(),
                    newPassWord:$("#password").val(),
                    userName:userName
                },
                success: function (data) {
                    if(data == 1) {
                        $("#oldPassword").val("");
                        $("#password").val("");
                        $("#repassword").val("");
                        layer.msg("修改成功");
                    }else if(data == -1){
                        layer.msg("原始密码错误");
                    }else {
                        layer.msg("修改失败");
                    }
                },
                error: function () {
                    layer.msg("网络异常");
                }
            });
        }
        return false
    })
});


function loginOut() {
    layer.confirm('真的退出？', {
        btn: ['真的','取消'] //按钮
        ,title:"提示"
    }, function(){
        window.location.href = "/";
    });
}

