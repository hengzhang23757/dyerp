var $colorSel,sizeSelect,colorRender;
var userName = $("#userName").val();
var userRole = $("#userRole").val();
var form;
layui.use('form', function(){
    form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
    form.render();
});
console.log(userName)
var pageWidth = 1500,customerNameDemo,customerNameAddDemo;
var sizeNameData = [];
$(document).ready(function () {
    $.ajax({
        url: "/erp/gethistoryseason",
        data: {},
        success:function(data){
            if (data.seasonList){
                $("#seasonName").empty();
                if (data.seasonList) {
                    $("#seasonName").append("<option value=''>请选择季度</option>");
                    $.each(data.seasonList, function(index,element){
                        $("#seasonName").append("<option value="+element+">"+element+"</option>");
                    });
                }
                form.render();
            }
        }, error:function(){
        }
    });
    $.ajax({
        url: "/erp/getallcustomer",
        data: {},
        success:function(res){
            if (res.data){
                // $("#customerNameAdd").empty();
                if (res.data) {
                    // $.each(res.data, function(index,element){
                    //     $("#customerNameAdd").append("<option value='"+element.customerName+"'>"+element.customerName+"</option>");
                    // });
                    // layui.form.render("select");
                    var resultData = [];
                    $.each(res.data, function(index,element){
                        if (element != null && element != ''){
                            resultData.push({name:element.customerName, value:element.customerName});
                        }
                    });
                    customerNameDemo = xmSelect.render({
                        el: '#customerName',
                        radio: true,
                        filterable: true,
                        data: resultData
                    });
                    customerNameAddDemo = xmSelect.render({
                        el: '#customerNameAdd',
                        radio: true,
                        filterable: true,
                        data: resultData
                    });
                }

            }
        }, error:function(){
        }
    });
    // $.ajax({
    //     url: "/erp/getallcustomer",
    //     data: {},
    //     success:function(res){
    //         if (res.data){
    //             $("#customerName").empty();
    //             if (res.data) {
    //                 $("#customerName").append("<option value=''>选择客户</option>");
    //                 $.each(res.data, function(index,element){
    //                     $("#customerName").append("<option value='"+element.customerName+"'>"+element.customerName+"</option>");
    //                 });
    //                 layui.form.render("select");
    //             }
    //
    //         }
    //     }, error:function(){
    //     }
    // });
    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumber", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });
    pageWidth = $(document.body).width();
    $colorSel = $('#colorSelect').selectize({
        plugins: ['remove_button'],
        maxItems: null,
        persist: false,
        create: true,
        valueField: 'id',
        labelField: 'title',
        searchField: ['title']
    });
    layui.laydate.render({
        elem: '#deadLine',
        trigger: 'click',
        min: getNowFormatDate()
    });
    layui.laydate.render({
        elem: '#deliveryDate',
        trigger: 'click'
    });
    lay('#version').html('-v' + layui.laydate.v);
    $Date = layui.laydate;
    renderSeasonDate(document.getElementById('season'), 1);
    initDateForm();
    $.ajax({
        url: "/erp/getallsizenamebygroup",
        type: 'GET',
        data: {
        },
        success: function (res) {
            if (res.size) {
                sizeNameData = res.size;
                var data=[];
                for(var key in res.size) {
                    var parent = {};
                    parent.name = key;
                    parent.value = key;
                    var children=[];
                    $.each(res.size[key],function (index,item) {
                        children.push({
                            name:item,
                            value:item
                        })
                    });
                    parent.children = children;
                    data.push(parent);
                }
                sizeSelect = xmSelect.render({
                    el: '#sizeInfo',
                    autoRow: true,
                    cascader: {
                        show: true,
                        indent: 150
                    },
                    height: '200px',
                    data:data
                });
                $(".xm-body-cascader").css("overflow-y", "auto");
            }
        },
        error: function () {
            layer.msg("获取尺码信息失败", { icon: 2 });
        }
    });
    $("#stepForm").hide();
});
$(document).on('keydown', '.layui-table-edit', function (e) {
    var td = $(this).parent('td')
        ,tr = td.parent('tr')
        ,trs = tr.parent().parent().find('tr')
        ,tr_index = tr.index()
        ,td_index = td.index()
        ,td_last_index = tr.find('[data-edit="text"]:last').index()
        ,td_first_index = tr.find('[data-edit="text"]:first').index();
    switch (e.keyCode) {
        case 13:
            tr.next().find('td').eq(td_index).click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            return false;
        case 39:
            td.nextAll('[data-edit="text"]:first').click();
            if (td_index == td_last_index){
                tr.next().find('td').eq(td_first_index).click();
                if (tr_index == trs.length - 1){
                    trs.eq(0).find('td').eq(td_first_index).click();
                }
            }
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
        case 37:
            td.prevAll('[data-edit="text"]:first').click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
        case 38:
            tr.prev().find('td').eq(td_index).click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
        case 40:
            tr.next().find('td').eq(td_index).click();
            setTimeout(function () {
                $('.layui-table-edit').select()
            }, 0);
            break;
    }

});
layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.26'
}).extend({                         // 模块别名
    soulTable: 'soulTable'
});
var processOrderTable;
layui.use(['form', 'soulTable', 'table', 'upload','yutons_sug'], function () {

    var table = layui.table,
        soulTable = layui.soulTable,
        upload = layui.upload,
        form = layui.form,
        $ = layui.$;

    processOrderTable = table.render({
        elem: '#processOrderTable'
        ,cols: [[]]
        ,loading:true
        ,data:[]
        ,height: 'full-90'
        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
        ,defaultToolbar: ['filter', 'print']
        ,title: '制单总表'
        ,totalRow: true
        ,page: true
        ,even: true
        ,overflow: 'tips'
        ,limits: [50, 100, 200]
        ,limit: 50 //每页默认显示的数量
        ,done: function (res, curr, count) {
            soulTable.render(this);
            var divArr = $(".layui-table-total div.layui-table-cell");
            $.each(divArr,function (index,item) {
                var _div = $(item);
                var content = _div.html();
                content = content.replace(".00","");
                _div.html(content);
            });
        }
        ,filter: {
            bottom: true,
            items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
        }
    });

    initTable('','','',0);

    function initTable(orderName, season, customerName, orderState){
        var param = {};
        if (orderName != null && orderName !== ''){
            param.orderName = orderName;
        }
        if (season != null && season !== ''){
            param.season = season;
        }
        if (customerName != null && customerName !== ''){
            param.customerName = customerName;
        }
        if (orderState != null && orderState !== ''){
            param.orderState = orderState;
        }
        $.ajax({
            url: "/erp/getoneyearordersummarylay",
            type: 'POST',
            data: param,
            success: function (res) {
                if (res.data) {
                    var reportData = res.data;
                    table.render({
                        elem: '#processOrderTable'
                        ,cols:[[
                            {title: '#', width: 50, collapse: true,lazy: true, icon: ['layui-icon layui-icon-triangle-r', 'layui-icon layui-icon-triangle-d'], children:[
                                    {
                                        title: '基础信息'
                                        ,url: 'getmanufactureorderinfobyordername'
                                        ,where: function(row){
                                            return {orderName: row.orderName}
                                        }
                                        ,height: 100
                                        ,loading:true
                                        ,page: false
                                        ,cols: [[
                                            {field: 'customerName', title: '品牌',align:'center', minWidth: 100},
                                            {field: 'purchaseMethod', title: '订购方式',align:'center', minWidth: 100},
                                            {field: 'styleName', title: '款名',align:'center', minWidth: 165 },
                                            {field: 'clothesVersionNumber', title: '单号',align:'center', minWidth: 123},
                                            {field: 'orderName', title: '款号',align:'center', minWidth: 123},
                                            {field: 'season', title: '季度',align:'center', minWidth: 123},
                                            {field: 'deadLine', title: '收单日期',align:'center', minWidth: 123, templet:function (d) {
                                                    return moment(d.deadLine).format("YYYY-MM-DD");
                                                }},
                                            {field: 'buyer', title: '跟单',align:'center', minWidth: 123},
                                            {field: 'modelType', title: '分组系列',align:'center', minWidth: 123},
                                            {field: 'deliveryDate', title: '交期',align:'center', minWidth: 123, templet:function (d) {
                                                    return moment(d.deliveryDate).format("YYYY-MM-DD");
                                                }},
                                            {field: 'additional', title: '允许加裁%',align:'center', minWidth: 123},
                                            {field: 'sizeInfo', title: '尺码',align:'center', minWidth: 123},
                                            {field: 'colorInfo', title: '颜色',align:'center', minWidth: 123},
                                            {field: 'price', title: '单价',align:'center', minWidth: 123},
                                            {field: 'printPrice', title: '绣印花单价',align:'center', minWidth: 123},
                                            {field: 'washPrice', title: '洗水单价',align:'center', minWidth: 123},
                                            {field: 'remark', title: '备注',align:'center', minWidth: 123},
                                            {field: 'modifyState', title: '修改',align:'center', minWidth: 123},
                                            {field: 'manufactureOrderID', title: '操作', minWidth: 380,align:'center', templet: function(row) {
                                                    return '<a class="layui-btn layui-btn-xs" lay-event="childEdit">修改</a><a class="layui-btn layui-btn-xs" lay-event="updateOrderName">改款号</a><a class="layui-btn layui-btn-xs" lay-event="updateDeadLine">改交期</a><a class="layui-btn layui-btn-xs" lay-event="updateSeason">改季度</a><a class="layui-btn layui-btn-xs" lay-event="updateColor">改颜色</a><a class="layui-btn layui-btn-xs" lay-event="updateSize">改尺码</a>';
                                                }}
                                        ]]
                                        ,toolEvent: function (obj, pobj) {
                                            // obj 子表当前行对象
                                            // pobj 父表当前行对象
                                            var rowData = obj.data;
                                            var childId = this.id; // 通过 this 对象获取当前子表的id
                                            if (obj.event === 'childEdit') {
                                                var index = layer.open({
                                                    type: 1 //Page层类型
                                                    , title: '修改'
                                                    , btn: ['保存','取消']
                                                    , shade: 0.6 //遮罩透明度
                                                    , maxmin: false //允许全屏最小化
                                                    , anim: 0 //0-6的动画形式，-1不开启
                                                    , zIndex:1 //层优先级
                                                    , content: $('#basicOrderInfoUpdate')
                                                    ,yes: function(index, layero){
                                                        var params = {};
                                                        params.customerName = $("#basicOrderInfoUpdate input[name='customerName']").val();
                                                        params.purchaseMethod = $("#basicOrderInfoUpdate input[name='purchaseMethod']").val();
                                                        params.styleName = $("#basicOrderInfoUpdate input[name='styleName']").val();
                                                        params.orderName = $("#basicOrderInfoUpdate input[name='orderName']").val();
                                                        params.clothesVersionNumber = $("#basicOrderInfoUpdate input[name='clothesVersionNumber']").val();
                                                        params.season = $("#basicOrderInfoUpdate input[name='season']").val();
                                                        params.buyer = $("#basicOrderInfoUpdate input[name='buyer']").val();
                                                        params.modelType = $("#basicOrderInfoUpdate input[name='modelType']").val();
                                                        params.deliveryDate = $("#basicOrderInfoUpdate input[name='deliveryDate']").val();
                                                        params.deadLine = $("#basicOrderInfoUpdate input[name='deadLine']").val();
                                                        params.additional = $("#basicOrderInfoUpdate input[name='additional']").val();
                                                        params.price = $("#basicOrderInfoUpdate input[name='price']").val();
                                                        params.printPrice = $("#basicOrderInfoUpdate input[name='printPrice']").val();
                                                        params.washPrice = $("#basicOrderInfoUpdate input[name='washPrice']").val();
                                                        params.colorInfo = $("#basicOrderInfoUpdate input[name='colorInfo']").val();
                                                        params.sizeInfo = $("#basicOrderInfoUpdate input[name='sizeInfo']").val();
                                                        params.remark = $("#basicOrderInfoUpdate input[name='remark']").val();
                                                        params.manufactureOrderID = rowData.manufactureOrderID;
                                                        $.ajax({
                                                            url: "/erp/updatemanufactureorderbasicinfo",
                                                            type: 'POST',
                                                            data: {
                                                                manufactureOrderJson:JSON.stringify(params)
                                                            },
                                                            success: function (res) {
                                                                if (res.result == 0) {
                                                                    layer.close(index);
                                                                    layer.msg("保存成功！", {icon: 1});
                                                                    table.reload(childId);
                                                                    $("#basicOrderInfoUpdate").find("input").val("");
                                                                } else {
                                                                    layer.msg("保存失败！", {icon: 2});
                                                                }
                                                            },
                                                            error: function () {
                                                                layer.msg("保存失败！", {icon: 2});
                                                            }
                                                        })
                                                    }
                                                    , cancel : function (i,layero) {
                                                        $("#basicOrderInfoUpdate").find("input").val("");
                                                    }
                                                });
                                                layer.full(index);
                                                $("#basicOrderInfoUpdate input[name='customerName']").val(rowData.customerName);
                                                $("#basicOrderInfoUpdate input[name='purchaseMethod']").val(rowData.purchaseMethod);
                                                $("#basicOrderInfoUpdate input[name='styleName']").val(rowData.styleName);
                                                $("#basicOrderInfoUpdate input[name='clothesVersionNumber']").val(rowData.clothesVersionNumber);
                                                $("#basicOrderInfoUpdate input[name='orderName']").val(rowData.orderName);
                                                $("#basicOrderInfoUpdate input[name='season']").val(rowData.season);
                                                $("#basicOrderInfoUpdate input[name='deadLine']").val(moment(rowData.deadLine).format("YYYY-MM-DD"));
                                                $("#basicOrderInfoUpdate input[name='buyer']").val(rowData.buyer);
                                                $("#basicOrderInfoUpdate input[name='modelType']").val(rowData.modelType);
                                                $("#basicOrderInfoUpdate input[name='deliveryDate']").val(moment(rowData.deliveryDate).format("YYYY-MM-DD"));
                                                $("#basicOrderInfoUpdate input[name='additional']").val(rowData.additional);
                                                $("#basicOrderInfoUpdate input[name='price']").val(rowData.price);
                                                $("#basicOrderInfoUpdate input[name='printPrice']").val(rowData.printPrice);
                                                $("#basicOrderInfoUpdate input[name='washPrice']").val(rowData.washPrice);
                                                $("#basicOrderInfoUpdate input[name='sizeInfo']").val(rowData.sizeInfo);
                                                $("#basicOrderInfoUpdate input[name='colorInfo']").val(rowData.colorInfo);
                                                $("#basicOrderInfoUpdate input[name='remark']").val(rowData.remark);
                                            }
                                            else if (obj.event === 'updateDeadLine') {
                                                var index = layer.open({
                                                    type: 1 //Page层类型
                                                    , title: '修改交期'
                                                    , btn: ['保存','取消']
                                                    , shade: 0.6 //遮罩透明度
                                                    , maxmin: false //允许全屏最小化
                                                    , anim: 0 //0-6的动画形式，-1不开启
                                                    , zIndex:1 //层优先级
                                                    , area: ['300px', '220px']
                                                    , content: "<div><table><tr><td><label class='layui-form-label'>原交期:</label></td><td><input type='text' id='initDeadLine' autocomplete='off' class='layui-input' readonly></td></tr><tr><td><label class='layui-form-label'>修改成:</label></td><td><input type='text' id='toDeadLine' autocomplete='off' class='layui-input'></td></tr></table></div>"
                                                    ,yes: function(index, layero){
                                                        var params = {};
                                                        params.orderName = rowData.orderName;
                                                        params.toDeadLine = $("#toDeadLine").val();
                                                        if (params.toDeadLine == null || params.toDeadLine == ''){
                                                            layer.msg("不能为空~~~");
                                                            return false;
                                                        }
                                                        $.ajax({
                                                            url: "/erp/updatedeadlinebyorder",
                                                            type: 'POST',
                                                            data: params,
                                                            success: function (res) {
                                                                if (res.result == 0) {
                                                                    layer.close(index);
                                                                    layer.msg("保存成功！", {icon: 1});
                                                                    table.reload(childId);
                                                                } else {
                                                                    layer.msg("保存失败！", {icon: 2});
                                                                }
                                                            },
                                                            error: function () {
                                                                layer.msg("保存失败！", {icon: 2});
                                                            }
                                                        })
                                                    }
                                                    , cancel : function (i,layero) {
                                                        $("#toDeadLine").val("");
                                                        $("#initDeadLine").val("");
                                                    }
                                                });
                                                $("#toDeadLine").val("");
                                                $("#initDeadLine").val(moment(rowData.deliveryDate).format("YYYY-MM-DD"));
                                                layui.laydate.render({
                                                    elem: '#toDeadLine',
                                                    trigger: 'click'
                                                });
                                            }
                                            else if (obj.event === 'updateSeason') {
                                                var index = layer.open({
                                                    type: 1 //Page层类型
                                                    , title: '修改季度'
                                                    , btn: ['保存','取消']
                                                    , shade: 0.6 //遮罩透明度
                                                    , maxmin: false //允许全屏最小化
                                                    , anim: 0 //0-6的动画形式，-1不开启
                                                    , zIndex:1 //层优先级
                                                    , area: ['300px', '220px']
                                                    , content: "<div><table><tr><td><label class='layui-form-label'>原季度:</label></td><td><input type='text' id='initSeason' autocomplete='off' class='layui-input' readonly></td></tr><tr><td><label class='layui-form-label'>修改成:</label></td><td><input type='text' id='toSeason' autocomplete='off' class='layui-input'></td></tr></table></div>"
                                                    ,yes: function(index, layero){
                                                        var params = {};
                                                        params.orderName = rowData.orderName;
                                                        params.toSeason = $("#toSeason").val();
                                                        if (params.toSeason == null || params.toSeason == ''){
                                                            layer.msg("不能为空~~~");
                                                            return false;
                                                        }
                                                        $.ajax({
                                                            url: "/erp/updateseasonbyorder",
                                                            type: 'POST',
                                                            data: params,
                                                            success: function (res) {
                                                                if (res.result == 0) {
                                                                    layer.close(index);
                                                                    layer.msg("保存成功！", {icon: 1});
                                                                    table.reload(childId);
                                                                } else {
                                                                    layer.msg("保存失败！", {icon: 2});
                                                                }
                                                            },
                                                            error: function () {
                                                                layer.msg("保存失败！", {icon: 2});
                                                            }
                                                        })
                                                    }
                                                    , cancel : function (i,layero) {
                                                        $("#toSeason").val("");
                                                        $("#initSeason").val("");
                                                    }
                                                });
                                                $("#toSeason").val("");
                                                $("#initSeason").val(rowData.season);
                                                $Date = layui.laydate;
                                                renderSeasonDate(document.getElementById('toSeason'), 1);
                                                initDateForm();
                                            }
                                            else if (obj.event === 'updateColor') {
                                                var readyOrderClothesList = [];
                                                var sizeNameList = [];
                                                var colorNameList = [];
                                                var orderName = rowData.orderName;
                                                var index = layer.open({
                                                    type: 1 //Page层类型
                                                    , title: '修改制单颜色信息'
                                                    , btn: ['保存']
                                                    , shade: 0.6 //遮罩透明度
                                                    , maxmin: false //允许全屏最小化
                                                    , zIndex: 1
                                                    , anim: 0 //0-6的动画形式，-1不开启
                                                    , content: "<div id='orderColorUpdate'><table id='orderColorUpdateTable' name ='orderColorUpdateTable' style='background-color: white; color: black'>" +
                                                        "        </table></div>"
                                                    , yes: function (index, layero) {
                                                        var orderColorData = layui.table.cache.orderColorUpdateTable;
                                                        var isInput = true;
                                                        $.each(orderColorData,function(i,item){
                                                            if (item.toColor == null || item.toColor == ''){
                                                                isInput = false;
                                                            }
                                                        });

                                                        if(!isInput) {
                                                            layer.msg("请输入完所有数量", { icon: 2 });
                                                            return false;
                                                        }else {
                                                            $.ajax({
                                                                url: "/erp/updateordercolordata",
                                                                type: 'POST',
                                                                data: {
                                                                    orderColorJson:JSON.stringify(orderColorData),
                                                                    orderName: orderName
                                                                },
                                                                success: function (res) {
                                                                    if (res.result == 0) {
                                                                        layer.close(index);
                                                                        layer.msg("保存成功！", {icon: 1});
                                                                        table.reload(childId);
                                                                    } else if (res.result == 3) {
                                                                        layer.msg("已开裁, 无法修改！", {icon: 2});
                                                                    } else {
                                                                        layer.msg("保存失败！", {icon: 2});
                                                                    }
                                                                },
                                                                error: function () {
                                                                    layer.msg("保存失败！", {icon: 2});
                                                                }
                                                            });
                                                            return false;
                                                        }
                                                    }
                                                    , cancel: function (i, layero) {

                                                    }
                                                });
                                                layer.full(index);
                                                setTimeout(function () {
                                                    $.ajax({
                                                        url: "/erp/getordercolornamesbyorder",
                                                        type: 'GET',
                                                        data: {
                                                            orderName: orderName
                                                        },
                                                        success: function (res) {
                                                            colorNameList = res.colorNameList;
                                                            var title = [
                                                                {field: 'color', title: '原颜色',minWidth:100, fixed: true},
                                                                {field: 'toColor', title: '修改成',minWidth:100, fixed: true, edit:'text'}
                                                            ];
                                                            var colorSizeData = [];
                                                            $.each(colorNameList,function(index,value){
                                                                var tmp = {};
                                                                tmp.color = value;
                                                                tmp.toColor = value;
                                                                colorSizeData.push(tmp);
                                                            });
                                                            var orderColorUpdateTable = table.render({
                                                                elem: '#orderColorUpdateTable'
                                                                ,cols: [title]
                                                                ,data: colorSizeData
                                                                ,limit: 500 //每页默认显示的数量
                                                                ,done:function () {}
                                                            });
                                                            return false;
                                                        },
                                                        error: function () {
                                                            layer.msg("获取数据失败！", {icon: 2});
                                                        }
                                                    })
                                                }, 100);
                                            }
                                            else if (obj.event === 'updateSize') {
                                                var sizeNameList = [];
                                                var orderName = rowData.orderName;
                                                var index = layer.open({
                                                    type: 1 //Page层类型
                                                    , title: '修改制单尺码信息'
                                                    , btn: ['保存']
                                                    , shade: 0.6 //遮罩透明度
                                                    , maxmin: false //允许全屏最小化
                                                    , zIndex: 1
                                                    , anim: 0 //0-6的动画形式，-1不开启
                                                    , content: "<div id='orderSizeUpdate'><table id='orderSizeUpdateTable' name ='orderSizeUpdateTable' style='background-color: white; color: black'>" +
                                                        "        </table></div>"
                                                    , yes: function (index, layero) {
                                                        var orderSizeData = layui.table.cache.orderSizeUpdateTable;
                                                        var isInput = true;
                                                        $.each(orderSizeData,function(i,item){
                                                            if (item.toSize == null || item.toSize == ''){
                                                                isInput = false;
                                                            }
                                                        });

                                                        if(!isInput) {
                                                            layer.msg("请输入完所有数量", { icon: 2 });
                                                            return false;
                                                        }else {
                                                            $.ajax({
                                                                url: "/erp/updateordersizedata",
                                                                type: 'POST',
                                                                data: {
                                                                    orderSizeJson:JSON.stringify(orderSizeData),
                                                                    orderName: orderName
                                                                },
                                                                success: function (res) {
                                                                    if (res.result == 0) {
                                                                        layer.close(index);
                                                                        layer.msg("保存成功！", {icon: 1});
                                                                        table.reload(childId);
                                                                    } else if (res.result == 3) {
                                                                        layer.msg("已开裁, 无法修改！", {icon: 2});
                                                                    } else {
                                                                        layer.msg("保存失败！", {icon: 2});
                                                                    }
                                                                },
                                                                error: function () {
                                                                    layer.msg("保存失败！", {icon: 2});
                                                                }
                                                            });
                                                            return false;
                                                        }
                                                    }
                                                    , cancel: function (i, layero) {

                                                    }
                                                });
                                                layer.full(index);
                                                setTimeout(function () {
                                                    $.ajax({
                                                        url: "/erp/getordersizenamesbyorder",
                                                        type: 'GET',
                                                        data: {
                                                            orderName: orderName
                                                        },
                                                        success: function (res) {
                                                            sizeNameList = res.sizeNameList;
                                                            var title = [
                                                                {field: 'size', title: '原尺码',minWidth:100, fixed: true},
                                                                {field: 'toSize', title: '修改成',minWidth:100, fixed: true, edit:'text'}
                                                            ];
                                                            var colorSizeData = [];
                                                            $.each(sizeNameList,function(index,value){
                                                                var tmp = {};
                                                                tmp.size = value;
                                                                tmp.toSize = value;
                                                                colorSizeData.push(tmp);
                                                            });
                                                            var orderColorUpdateTable = table.render({
                                                                elem: '#orderSizeUpdateTable'
                                                                ,cols: [title]
                                                                ,data: colorSizeData
                                                                ,limit: 500 //每页默认显示的数量
                                                                ,done:function () {}
                                                            });
                                                            return false;
                                                        },
                                                        error: function () {
                                                            layer.msg("获取数据失败！", {icon: 2});
                                                        }
                                                    })
                                                }, 100);
                                            }
                                            else if (obj.event === 'updateOrderName') {
                                                var index = layer.open({
                                                    type: 1 //Page层类型
                                                    , title: '修改款号'
                                                    , btn: ['保存','取消']
                                                    , shade: 0.6 //遮罩透明度
                                                    , maxmin: false //允许全屏最小化
                                                    , anim: 0 //0-6的动画形式，-1不开启
                                                    , zIndex:1 //层优先级
                                                    , area: ['300px', '220px']
                                                    , content: "<div><table><tr><td><label class='layui-form-label'>原款号:</label></td><td><input type='text' id='initOrderName' autocomplete='off' class='layui-input' readonly></td></tr><tr><td><label class='layui-form-label'>修改成:</label></td><td><input type='text' id='toOrderName' autocomplete='off' class='layui-input'></td></tr></table></div>"
                                                    ,yes: function(index, layero){
                                                        var params = {};
                                                        params.orderName = rowData.orderName;
                                                        params.toOrderName = $("#toOrderName").val();
                                                        if (params.toOrderName == null || params.toOrderName === ''){
                                                            layer.msg("不能为空~~~");
                                                            return false;
                                                        }
                                                        $.ajax({
                                                            url: "/erp/changeordername",
                                                            type: 'POST',
                                                            data: params,
                                                            success: function (res) {
                                                                if (res.result == 0) {
                                                                    layer.close(index);
                                                                    layer.msg("保存成功！", {icon: 1});
                                                                    table.reload(childId);
                                                                } else {
                                                                    layer.msg("保存失败！", {icon: 2});
                                                                }
                                                            },
                                                            error: function () {
                                                                layer.msg("保存失败！", {icon: 2});
                                                            }
                                                        })
                                                    }
                                                    , cancel : function (i,layero) {
                                                        $("#toOrderName").val("");
                                                        $("#initOrderName").val("");
                                                    }
                                                });
                                                $("#toOrderName").val("");
                                                $("#initOrderName").val(rowData.orderName);
                                            }
                                        }
                                        ,done: function () {
                                            soulTable.render(this);
                                        }
                                        ,filter: {
                                            bottom: false,
                                            items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                                        }
                                    },
                                    {
                                        title: '制单数据'
                                        ,url: 'getorderclothesbyordername'
                                        ,where: function(row){
                                            return {orderName: row.orderName}
                                        }
                                        ,height: 300
                                        ,loading:true
                                        ,page: false
                                        ,totalRow: true
                                        ,toolbar: '<div><a class="layui-btn layui-btn-danger layui-btn-sm" lay-event="batchDelete">批量删除</a><a class="layui-btn layui-btn-sm" lay-event="orderClothesReturn">反审</a></div>'
                                        ,cols: [[
                                            {type: 'checkbox', fixed: 'left'},
                                            {field: 'orderName', title: '款号', minWidth: 100, sort: true, filter: true},
                                            {field: 'clothesVersionNumber', title: '单号', minWidth: 100, sort: true, filter: true},
                                            {field: 'customerName', title: '品牌', minWidth: 100, sort: true, filter: true},
                                            {field: 'purchaseMethod', title: '订购方式', minWidth: 120, sort: true, filter: true},
                                            {field: 'styleDescription', title: '款式描述', filter: true, totalRowText: '合计'},
                                            {field: 'colorName', title: '颜色', minWidth: 100, filter: true},
                                            {field: 'sizeName', title: '尺码', minWidth: 100, filter: true},
                                            {field: 'count', title: '数量', minWidth: 100, filter: true, totalRow: true},
                                            {field: 'season', title: '季度', minWidth: 100, filter: true},
                                            {field: 'deadLine', title: '收单日期', minWidth: 100, filter: true, templet:function (d) {
                                                    return moment(d.deadLine).format("YYYY-MM-DD");
                                                }},
                                            {field: 'orderClothesID', title: '操作', minWidth: 120, templet: function(row) {
                                                    return '<a class="layui-btn layui-btn-xs" lay-event="childEdit">修改</a>';
                                                }}
                                        ]]
                                        ,toolEvent: function (obj, pobj) {
                                            // obj 子表当前行对象
                                            // pobj 父表当前行对象
                                            var rowData = obj.data;
                                            var childId = this.id; // 通过 this 对象获取当前子表的id
                                            if (obj.event === 'childEdit') {
                                                var modify = "yes";
                                                $.ajax({
                                                    url: "/erp/decideorderclothescountupdatestate",
                                                    type: 'GET',
                                                    async: false,
                                                    data: {
                                                        orderName: rowData.orderName
                                                    },
                                                    success: function (res) {
                                                        if (res.state == "no"){
                                                            modify = "no";
                                                        }
                                                    },
                                                    error: function () {
                                                        layer.msg("获取数据失败！", {icon: 2});
                                                    }
                                                })
                                                if (modify == "no"){
                                                    layer.msg("订单锁定,无法修改,若要修改请提交审核通过后修改");
                                                    return false;
                                                }
                                                var readyOrderClothesList = [];
                                                var sizeNameList = [];
                                                var colorNameList = [];
                                                var orderName = rowData.orderName;
                                                var index = layer.open({
                                                    type: 1 //Page层类型
                                                    , title: '修改制单数据信息'
                                                    , btn: ['保存']
                                                    , shade: 0.6 //遮罩透明度
                                                    , maxmin: false //允许全屏最小化
                                                    , zIndex: 1
                                                    , anim: 0 //0-6的动画形式，-1不开启
                                                    , content: "<div id='basicOrderClothesUpdate'><table id='basicOrderClothesTable' name ='basicOrderClothesTable' style='background-color: white; color: black'>" +
                                                        "        </table></div>"
                                                    , yes: function (index, layero) {
                                                        var colorSizeNums = layui.table.cache.basicOrderClothesTable;
                                                        var isInput = true;
                                                        $.each(colorSizeNums,function(i,item){
                                                            $.each(sizeNameList,function(j,value){
                                                                if(item[value] === '') {
                                                                    isInput = false;
                                                                    return false;
                                                                }
                                                            });
                                                        });

                                                        if(!isInput) {
                                                            layer.msg("请输入完所有数量", { icon: 2 });
                                                            return false;
                                                        }else {
                                                            $.each(readyOrderClothesList,function (index1, item1) {
                                                                $.each(colorSizeNums, function (index, item) {
                                                                    $.each(sizeNameList, function (s_index, size) {
                                                                        if (item1.colorName === item.color && item1.sizeName === size){
                                                                            item1.count = item[size];
                                                                        }
                                                                    })
                                                                });
                                                            });
                                                            $.ajax({
                                                                url: "/erp/updateorderclothesbatch",
                                                                type: 'POST',
                                                                data: {
                                                                    orderClothesJson:JSON.stringify(readyOrderClothesList)
                                                                },
                                                                success: function (res) {
                                                                    if (res == 0) {
                                                                        layer.close(index);
                                                                        layer.msg("保存成功！", {icon: 1});
                                                                        initTable(rowData.orderName, '', '','');
                                                                    } else {
                                                                        layer.msg("保存失败！", {icon: 2});
                                                                    }
                                                                },
                                                                error: function () {
                                                                    layer.msg("保存失败！", {icon: 2});
                                                                }
                                                            });
                                                            return false;
                                                        }
                                                    }
                                                    , cancel: function (i, layero) {

                                                    }
                                                });
                                                layer.full(index);
                                                setTimeout(function () {
                                                    $.ajax({
                                                        url: "/erp/getorderclothesandcolorsizebyordername",
                                                        type: 'GET',
                                                        data: {
                                                            orderName: orderName
                                                        },
                                                        success: function (res) {
                                                            readyOrderClothesList = res.data;
                                                            var orderClothesData = res.data;
                                                            colorNameList = res.colorNameList;
                                                            sizeNameList = res.sizeNameList;
                                                            sizeNameList = globalSizeSort(sizeNameList);
                                                            var title = [
                                                                {field: 'color', title: '颜色/尺码',minWidth:100, fixed: true}
                                                            ];
                                                            $.each(sizeNameList,function(index,value){
                                                                var field = {};
                                                                field.field = value;
                                                                field.title = value;
                                                                field.minWidth = 80;
                                                                field.edit = 'text';
                                                                title.push(field);
                                                            });
                                                            var colorSizeData = [];
                                                            $.each(colorNameList,function(index,value){
                                                                var tmp = {};
                                                                tmp.color = value;
                                                                $.each(sizeNameList,function(index2,value2){
                                                                    $.each(orderClothesData,function(index3,value3){
                                                                        if (value3.colorName === value && value3.sizeName === value2){
                                                                            tmp[value2] = value3.count;
                                                                        }
                                                                    })
                                                                });
                                                                colorSizeData.push(tmp);
                                                            });
                                                            var basicOrderClothesTable = table.render({
                                                                elem: '#basicOrderClothesTable'
                                                                ,cols: [title]
                                                                ,data: colorSizeData
                                                                ,limit: 500 //每页默认显示的数量
                                                                ,done:function () {}
                                                            });
                                                            return false;
                                                        },
                                                        error: function () {
                                                            layer.msg("获取数据失败！", {icon: 2});
                                                        }
                                                    })
                                                }, 100);
                                            }
                                        }
                                        ,toolbarEvent: function (obj, pobj) {
                                            // obj 子表当前行对象
                                            // pobj 父表当前行对象
                                            var objData = obj.data;
                                            var pData = pobj.data;
                                            var childId = this.id;
                                            if (obj.event === 'batchDelete') {
                                                if (pData.modifyState === '锁定'){
                                                    layer.msg('删除请先反审~~');
                                                    return false;
                                                }
                                                var checkStatus = table.checkStatus(obj.config.id);
                                                if(checkStatus.data.length == 0) {
                                                    layer.msg('请选择数据');
                                                }else {
                                                    var idList = [];
                                                    $.each(checkStatus.data, function (index, item) {
                                                        idList.push(item.orderClothesID);
                                                    });
                                                    layer.confirm('确认全部删除吗?', function(index){
                                                        $.ajax({
                                                            url: "/erp/deleteorderclothesbatch",
                                                            type: 'POST',
                                                            data: {
                                                                orderClothesIDList: idList
                                                            },
                                                            traditional: true,
                                                            success: function (res) {
                                                                if (res.result == 0) {
                                                                    layer.close(index);
                                                                    layer.msg("删除成功！", {icon: 1});
                                                                    table.reload(childId);
                                                                } else {
                                                                    layer.msg("删除失败！", {icon: 2});
                                                                }
                                                            }, error: function () {
                                                                layer.msg("删除失败！", {icon: 2});
                                                            }
                                                        })
                                                    });
                                                }
                                            } else if (obj.event === 'orderClothesReturn'){
                                                layer.confirm('确认提交反审吗?', function(index){
                                                    $.ajax({
                                                        url: "/erp/updatemanufactureordermodifystate",
                                                        type: 'POST',
                                                        data: {
                                                            orderName: pData.orderName,
                                                            modifyState: '反审'
                                                        },
                                                        success: function (res) {
                                                            if (res.result == 0) {
                                                                layer.close(index);
                                                                layer.msg("提交成功！", {icon: 1});
                                                                table.reload(childId);
                                                            } else {
                                                                layer.msg("提交失败！", {icon: 2});
                                                            }
                                                        }, error: function () {
                                                            layer.msg("提交失败！", {icon: 2});
                                                        }
                                                    })
                                                });
                                            }
                                        }
                                        ,done: function () {
                                            soulTable.render(this);
                                        }
                                        ,filter: {
                                            bottom: false,
                                            items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                                        }
                                    },
                                    {
                                        title: '面料信息'
                                        ,url: 'getmanufacturefabricbyorder'
                                        ,where: function(row){
                                            return {orderName: row.orderName}
                                        }
                                        ,height: 300
                                        ,page: false
                                        ,toolbar: '<div><a class="layui-btn layui-btn-danger layui-btn-sm" lay-event="batchDelete">批量删除</a></div>'
                                        ,cols: [[
                                            {type: 'checkbox', fixed: 'left'},
                                            {field: 'orderName', title: '款号', minWidth: 130, sort: true, filter: true},
                                            {field: 'clothesVersionNumber', title: '单号', minWidth: 130, sort: true, filter: true},
                                            {field: 'fabricName', title: '面料名称', minWidth: 130, sort: true, filter: true},
                                            {field: 'fabricNumber', title: '面料号', minWidth: 130, sort: true, filter: true},
                                            {field: 'supplier', title: '供应商', minWidth: 100, sort: true, filter: true},
                                            {field: 'unit', title: '单位', minWidth: 100, sort: true, filter: true},
                                            {field: 'fabricWidth', title: '布封', minWidth: 165 , filter: true},
                                            {field: 'fabricWeight', title: '克重', minWidth: 123, filter: true},
                                            {field: 'partName', title: '部位', minWidth: 123, filter: true},
                                            {field: 'fabricAdd', title: '损耗(%)', minWidth: 123, filter: true},
                                            {field: 'orderPieceUsage', title: '客供', minWidth: 123, filter: true},
                                            {field: 'pieceUsage', title: '单件用量', minWidth: 123, filter: true},
                                            {field: 'colorName', title: '订单颜色', minWidth: 123, filter: true},
                                            {field: 'isHit', title: '是否撞色', minWidth: 123, filter: true},
                                            {field: 'fabricColor', title: '面料颜色', minWidth: 123, filter: true},
                                            {field: 'fabricColorNumber', title: '面料色号', minWidth: 123, filter: true},
                                            {field: 'checkState', title: '审核', minWidth: 123, filter: true},
                                            {field: 'remark', title: '备注', minWidth: 123, filter: true},
                                            {field: 'fabricID',title: '操作', minWidth: 200, templet: function(row) {
                                                    return '<a class="layui-btn layui-btn-xs" lay-event="childEdit">修改</a><a class="layui-btn layui-btn-xs layui-btn-warm" lay-event="reAdd">补料</a>';
                                                }}
                                        ]]
                                        ,toolEvent: function (obj, pobj) {
                                            // obj 子表当前行对象
                                            // pobj 父表当前行对象
                                            var rowData = obj.data;
                                            var childId = this.id; // 通过 this 对象获取当前子表的id
                                            if (obj.event === 'childEdit') {
                                                var orderName = rowData.orderName;
                                                var index = layer.open({
                                                    type: 1 //Page层类型
                                                    , title: '修改面料信息'
                                                    , btn: ['保存']
                                                    , shade: 0.6 //遮罩透明度
                                                    , maxmin: false //允许全屏最小化
                                                    , anim: 0 //0-6的动画形式，-1不开启
                                                    , content: "<div id=\"fabricDetailUpdate\" style=\"cursor:default;\">\n" +
                                                        "        <table id=\"fabricDetailUpdateTable\" name = \"fabricDetailUpdateTable\" style=\"background-color: white; color: black\">\n" +
                                                        "        </table>\n" +
                                                        "    </div>"
                                                    , yes: function (index, layero) {
                                                        var fabricDataList = table.cache.fabricDetailUpdateTable;
                                                        $.each(fabricDataList,function (index,item) {
                                                            if (item.fabricAdd == ''){
                                                                item.fabricAdd = 0;
                                                            }
                                                            if (item.orderPieceUsage == ''){
                                                                item.orderPieceUsage = 0;
                                                            }
                                                            if (item.pieceUsage == ''){
                                                                item.pieceUsage = 0;
                                                            }
                                                            item.checkState = '未提交';
                                                        });
                                                        $.ajax({
                                                            url: "/erp/updatemanufacturefabricbatch",
                                                            type: 'POST',
                                                            data: {
                                                                manufactureFabricJson:JSON.stringify(fabricDataList)
                                                            },
                                                            success: function (res) {
                                                                if (res.result == 0) {
                                                                    layer.close(index);
                                                                    layer.msg("保存成功！", {icon: 1});
                                                                    initTable(orderName, '', '','');
                                                                } else {
                                                                    layer.msg("保存失败！", {icon: 2});
                                                                }
                                                            },
                                                            error: function () {
                                                                layer.msg("保存失败！", {icon: 2});
                                                            }
                                                        })
                                                    }
                                                    , cancel: function (i, layero) {
                                                        // $("#fabricDetailUpdate").find("input").val("");
                                                    }
                                                });
                                                layer.full(index);
                                                setTimeout(function () {
                                                    $.ajax({
                                                        url: "/erp/getmanufacturefabricbyorder",
                                                        type: 'GET',
                                                        data: {
                                                            orderName: orderName
                                                        },
                                                        success: function (res) {
                                                            var unCheckedData = [];
                                                            $.each(res.data,function (index, item) {
                                                                if (item.checkState == '未提交' || item.checkState == '不通过'){
                                                                    unCheckedData.push(item);
                                                                }
                                                            });
                                                            if (unCheckedData.length == 0){
                                                                layer.close(index);
                                                                layer.msg("已提交审核，不能修改！", {icon: 2});
                                                            }
                                                            var title = [
                                                                {field: 'orderName', title: '款号', align: 'center', minWidth: 150},
                                                                {field: 'clothesVersionNumber', title: '版单', align: 'center', minWidth: 100},
                                                                {field: 'fabricName', title: '名称', align: 'center', minWidth: 150,edit:'text'},
                                                                {field: 'fabricNumber', title: '面料号', align: 'center', minWidth: 150,edit:'text'},
                                                                {field: 'supplier', title: '供应商', align: 'center', minWidth: 120,edit:'text'},
                                                                {field: 'fabricWidth', title: '布封', align: 'center', minWidth: 90,edit:'text'},
                                                                {field: 'fabricWeight', title: '克重', align: 'center', minWidth: 90,edit:'text'},
                                                                {field: 'unit', title: '单位', align: 'center', minWidth: 80,edit:'text'},
                                                                {field: 'partName', title: '部位', align: 'center', minWidth: 100,edit:'text'},
                                                                {field: 'fabricAdd', title: '损耗(%)', align: 'center', minWidth: 90,edit:'text'},
                                                                {field: 'orderPieceUsage', title: '客供', align: 'center', minWidth: 100,edit:'text'},
                                                                {field: 'pieceUsage', title: '单件用量', align: 'center', minWidth: 100,edit:'text'},
                                                                {field: 'colorName', title: '颜色', align: 'center', minWidth: 100,edit:'text'},
                                                                {field: 'isHit', title: '撞色', align: 'center', minWidth: 90, edit:'text'},
                                                                {field: 'fabricColor', title: '面料颜色', align: 'center', minWidth: 100,edit:'text'},
                                                                {field: 'fabricColorNumber', title: '面料色号', align: 'center', minWidth: 100,edit:'text'},
                                                                {field: 'fabricID', title: '', align: 'center', minWidth: 100, hide: true}
                                                            ];
                                                            table.render({
                                                                elem: '#fabricDetailUpdateTable'
                                                                , cols: [title]
                                                                , page: false
                                                                , height: 'full-100'
                                                                , limit: Number.MAX_VALUE
                                                                , data: unCheckedData
                                                                , done: function () {
                                                                    soulTable.render(this);
                                                                }
                                                            });
                                                        },
                                                        error: function () {
                                                            layer.msg("获取数据失败！", {icon: 2});
                                                        }
                                                    })
                                                }, 100);
                                                layui.form.render("select");
                                            }
                                            else if (obj.event === 'reAdd') {
                                                var fabricID = rowData.fabricID;
                                                var index = layer.open({
                                                    type: 1 //Page层类型
                                                    , title: '补料'
                                                    , btn: ['保存']
                                                    , shade: 0.6 //遮罩透明度
                                                    , maxmin: false //允许全屏最小化
                                                    , anim: 0 //0-6的动画形式，-1不开启
                                                    , area: ['300px', '220px']
                                                    , content: "<div><table><tr><td><label class='layui-form-label'>数量:</label></td><td><input type='text' id='fabricReAddWeight' autocomplete='off' class='layui-input'></td></tr><tr><td><label class='layui-form-label'>备注:</label></td><td><input type='text' id='fabricReAddRemark' autocomplete='off' class='layui-input'></td></tr></table></div>"
                                                    ,yes: function(index, layero){
                                                        var weight = $("#fabricReAddWeight").val();
                                                        var remark = $("#fabricReAddRemark").val();
                                                        $.ajax({
                                                            url: "/erp/fabricreadd",
                                                            type: 'POST',
                                                            data: {
                                                                fabricID: fabricID,
                                                                weight: weight,
                                                                remark: remark
                                                            },
                                                            success: function (res) {
                                                                if (res.result == 0) {
                                                                    layer.close(index);
                                                                    layer.msg("提交成功！", {icon: 1});
                                                                    table.reload(childId);
                                                                    $("#fabricReAddWeight").val('');
                                                                    $("#fabricReAddRemark").val('');
                                                                } else {
                                                                    layer.msg("提交失败！", {icon: 2});
                                                                }
                                                            }, error: function () {
                                                                layer.msg("提交失败！", {icon: 2});
                                                            }
                                                        })
                                                    }
                                                    , cancel : function (i,layero) {
                                                        $("#fabricReAddWeight").val('');
                                                        $("#fabricReAddRemark").val('');
                                                    }
                                                });
                                            }
                                        }
                                        ,toolbarEvent: function (obj, pobj) {
                                            // obj 子表当前行对象
                                            // pobj 父表当前行对象
                                            var objData = obj.data;
                                            var childId = this.id;
                                            if (obj.event === 'batchDelete') {
                                                var checkStatus = table.checkStatus(obj.config.id);
                                                if(checkStatus.data.length == 0) {
                                                    layer.msg('请选择数据');
                                                }else {
                                                    var idList = [];
                                                    $.each(checkStatus.data, function (index, item) {
                                                        idList.push(item.fabricID);
                                                    });
                                                    layer.confirm('确认全部删除吗?', function(index){
                                                        $.ajax({
                                                            url: "/erp/deletemanufacturefabricbatch",
                                                            type: 'POST',
                                                            data: {
                                                                fabricIDList: idList
                                                            },
                                                            traditional: true,
                                                            success: function (res) {
                                                                if (res.result == 0) {
                                                                    layer.close(index);
                                                                    layer.msg("删除成功！", {icon: 1});
                                                                    table.reload(childId);
                                                                } else if (res.result == 3) {
                                                                    layer.msg("所选面料已入库,无法删除！", {icon: 2});
                                                                } else {
                                                                    layer.msg("删除失败！", {icon: 2});
                                                                }
                                                            }, error: function () {
                                                                layer.msg("删除失败！", {icon: 2});
                                                            }
                                                        })
                                                    });
                                                }
                                            }
                                        }
                                        ,done: function () {
                                            soulTable.render(this);
                                        }
                                        ,filter: {
                                            bottom: false,
                                            items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                                        }
                                    },
                                    {
                                        title: '扁机信息'
                                        ,url: 'getbjbyorder'
                                        ,where: function(row){
                                            return {orderName: row.orderName}
                                        }
                                        ,height: 300
                                        ,loading:true
                                        ,toolbar: '<div><a class="layui-btn layui-btn-danger layui-btn-sm" lay-event="batchDelete">批量删除</a></div>'
                                        ,page: false
                                        ,cols: [[
                                            {type: 'checkbox', fixed: 'left'},
                                            {field: 'orderName', title: '款号', minWidth: 130, sort: true, filter: true},
                                            {field: 'clothesVersionNumber', title: '单号', minWidth: 130, sort: true, filter: true},
                                            {field: 'accessoryName', title: '名称', width: 150, sort: true, filter: true},
                                            {field: 'accessoryNumber', title: '编号', width: 150, sort: true, filter: true},
                                            {field: 'specification', title: '规格', width: 150, sort: true, filter: true},
                                            {field: 'accessoryUnit', title: '单位', width: 150 , filter: true},
                                            {field: 'accessoryColor', title: '颜色', width: 150, filter: true},
                                            {field: 'colorName', title: '色组', width: 150, filter: true},
                                            {field: 'sizeName', title: '尺码', width: 150, filter: true},
                                            {field: 'sizeProperty', title: '尺码属性', width: 150, filter: true},
                                            {field: 'colorProperty', title: '颜色属性', width: 150, filter: true},
                                            {field: 'wellCount', title: '好片', width: 90, filter: true},
                                            {field: 'orderPieceUsage', title: '客供用量', width: 150, filter: true},
                                            {field: 'pieceUsage', title: '用量', width: 90, filter: true},
                                            {field: 'accessoryAdd', title: '损耗(%)', width: 90, filter: true},
                                            {field: 'accessoryOrderCount', title: '客供总量', width: 90, filter: true},
                                            {field: 'accessoryCount', title: '订购量', width: 90, filter: true},
                                            {field: 'orderPrice', title: '客供单价', width: 90, filter: true},
                                            {field: 'price', title: '单价', width: 90, filter: true},
                                            {field: 'orderSumMoney', title: '客供总价', width: 90, filter: true},
                                            {field: 'sumMoney', title: '单价', width: 90, filter: true},
                                            {field: 'checkState', title: '审核', width: 123, filter: true},
                                            {field: 'remark', title: '备注', width: 123, filter: true},
                                            {field: 'accessoryID',title: '操作', width: 156, templet: function(row) {
                                                    return '<a class="layui-btn layui-btn-xs" lay-event="childEdit">修改</a><a class="layui-btn layui-btn-xs" lay-event="feed">补料</a>';
                                                }}
                                        ]]
                                        ,toolEvent: function (obj, pobj) {
                                            // obj 子表当前行对象
                                            // pobj 父表当前行对象
                                            var rowData = obj.data;
                                            var childId = this.id; // 通过 this 对象获取当前子表的id
                                            if (obj.event === 'childEdit') {
                                                var orderName = rowData.orderName;
                                                var index = layer.open({
                                                    type: 1 //Page层类型
                                                    , title: '修改辅料信息'
                                                    , btn: ['保存']
                                                    , shade: 0.6 //遮罩透明度
                                                    , maxmin: false //允许全屏最小化
                                                    , anim: 0 //0-6的动画形式，-1不开启
                                                    , content: "<div id=\"udpateAccessoryData\" style=\"cursor:default;\">\n" +
                                                        "        <table id=\"accessoryDataUpdateTable\" name = \"accessoryDataUpdateTable\" style=\"background-color: white; color: black\">\n" +
                                                        "        </table>\n" +
                                                        "    </div>"
                                                    , yes: function (index, layero) {
                                                        var accessoryDataList = table.cache.accessoryDataUpdateTable;
                                                        $.each(accessoryDataList,function (index,item) {
                                                            if (item.orderPieceUsage == ''){
                                                                item.orderPieceUsage = 0;
                                                            }
                                                            if (item.pieceUsage == ''){
                                                                item.pieceUsage = 0;
                                                            }
                                                            if (item.price == ''){
                                                                item.price = 0;
                                                            }
                                                            if (item.orderPrice == ''){
                                                                item.orderPrice = 0;
                                                            }
                                                            item.checkState = '未提交';
                                                        });
                                                        $.ajax({
                                                            url: "/erp/updatemanufactureaccessorybatch",
                                                            type: 'POST',
                                                            data: {
                                                                manufactureAccessoryJson:JSON.stringify(accessoryDataList)
                                                            },
                                                            success: function (res) {
                                                                if (res.result == 0) {
                                                                    layer.close(index);
                                                                    layer.msg("保存成功！", {icon: 1});
                                                                    initTable(rowData.orderName, '', '','');
                                                                } else {
                                                                    layer.msg("保存失败！", {icon: 2});
                                                                }
                                                            },
                                                            error: function () {
                                                                layer.msg("保存失败！", {icon: 2});
                                                            }
                                                        })
                                                    }
                                                    , cancel: function (i, layero) {
                                                        // $("#fabricDetailUpdate").find("input").val("");
                                                    }
                                                });
                                                layer.full(index);
                                                setTimeout(function () {
                                                    $.ajax({
                                                        url: "/erp/getmanufactureaccessorybyorder",
                                                        type: 'GET',
                                                        data: {
                                                            orderName: orderName
                                                        },
                                                        success: function (res) {
                                                            var unCheckedData = [];
                                                            $.each(res.data,function (index, item) {
                                                                if (item.checkState == '未提交' || item.checkState == '不通过'){
                                                                    unCheckedData.push(item);
                                                                }
                                                            });
                                                            if (unCheckedData.length == 0){
                                                                layer.close(index);
                                                                layer.msg("已提交审核，不能修改！", {icon: 2});
                                                            }
                                                            var title = [
                                                                {field: 'orderName', title: '款号', align: 'center', minWidth: 150},
                                                                {field: 'clothesVersionNumber', title: '版单', align: 'center', minWidth: 100},
                                                                {field: 'accessoryName', title: '名称', align: 'center', minWidth: 150,edit:'text'},
                                                                {field: 'accessoryNumber', title: '编号', align: 'center', minWidth: 120,edit:'text'},
                                                                {field: 'accessoryUnit', title: '单位', align: 'center', minWidth: 100,edit:'text'},
                                                                {field: 'colorProperty', title: '颜色属性', align: 'center', minWidth: 100,edit:'text'},
                                                                {field: 'sizeProperty', title: '尺码属性', align: 'center', minWidth: 100,edit:'text'},
                                                                {field: 'pieceUsage', title: '单件用量', align: 'center', minWidth: 100,edit:'text'},
                                                                {field: 'orderPieceUsage', title: '客供用量', align: 'center', minWidth: 100,edit:'text'},
                                                                {field: 'price', title: '单价', align: 'center', minWidth: 100,edit:'text'},
                                                                {field: 'orderPrice', title: '客供单价', align: 'center', minWidth: 100,edit:'text'},
                                                                {field: 'colorName', title: '色组', align: 'center', minWidth: 100},
                                                                {field: 'sizeName', title: '尺码组', align: 'center', minWidth: 100},
                                                                {field: 'accessoryID', title: '', align: 'center', minWidth: 100, hide: true}
                                                            ];
                                                            table.render({
                                                                elem: '#accessoryDataUpdateTable'
                                                                , cols: [title]
                                                                , page: false
                                                                , height: 'full-100'
                                                                , limit: Number.MAX_VALUE
                                                                , data: unCheckedData
                                                                , done: function (res, curr, count) {

                                                                    var that = this.elem.next();
                                                                    res.data.forEach(function (item, index) {
                                                                        if (index % 2 == 0) {
                                                                            var tr = that.find(".layui-table-box tbody tr[data-index='" + index + "']").css("background-color", "#fff");
                                                                        } else {
                                                                            var tr = that.find(".layui-table-box tbody tr[data-index='" + index + "']").css("background-color", "#c2c2c2");
                                                                        }
                                                                    });

                                                                    soulTable.render(this);
                                                                }
                                                            });
                                                        },
                                                        error: function () {
                                                            layer.msg("获取数据失败！", {icon: 2});
                                                        }
                                                    })
                                                }, 100);
                                                layui.form.render("select");
                                            } else if (obj.event === 'feed') {
                                                var orderName = rowData.orderName;
                                                var index = layer.open({
                                                    type: 1 //Page层类型
                                                    , title: '辅料补充'
                                                    , btn: ['保存']
                                                    , shade: 0.6 //遮罩透明度
                                                    , maxmin: false //允许全屏最小化
                                                    , anim: 0 //0-6的动画形式，-1不开启
                                                    , content: "<div id=\"feedAccessoryData\" style=\"cursor:default;\">\n" +
                                                        "        <table id=\"accessoryDataFeedTable\" name = \"accessoryDataFeedTable\" style=\"background-color: white; color: black\">\n" +
                                                        "        </table>\n" +
                                                        "    </div>"
                                                    , yes: function (index, layero) {
                                                        var accessoryDataList = table.cache.accessoryDataFeedTable;
                                                        var nowDate = dateFormat("YYYY-mm-dd HH:MM:SS", new Date()).replace(" ", "").replace(/\:/g,'').replace(/\-/g,'');
                                                        var addAccessoryList = [];
                                                        var keyIndex = 0;
                                                        $.each(accessoryDataList,function (index,item) {
                                                            if (item.accessoryCount != null && item.accessoryCount != '' && Number(item.accessoryCount) > 0){
                                                                item.orderState = '未下单';
                                                                item.checkState = '已提交';
                                                                item.sumMoney = Number(item.price) * Number(item.accessoryCount);
                                                                item.checkNumber = nowDate;
                                                                item.accessoryKey = keyIndex ++;
                                                                item.batchOrder = 1;
                                                                addAccessoryList.push(item);
                                                            }
                                                        });
                                                        if (addAccessoryList.length == 0){
                                                            layer.msg("没有输入辅料数量,请核实！", {icon: 2});
                                                            return false;
                                                        }
                                                        $.ajax({
                                                            url: "/erp/addmanufactureaccessorybatch",
                                                            type: 'POST',
                                                            data: {
                                                                manufactureAccessoryJson:JSON.stringify(addAccessoryList),
                                                                orderName: rowData.orderName,
                                                                userName: userName
                                                            },
                                                            success: function (res) {
                                                                if (res.result == 0) {
                                                                    layer.close(index);
                                                                    layer.msg("保存成功！", {icon: 1});
                                                                    initTable(rowData.orderName, '', '','');
                                                                } else {
                                                                    layer.msg("保存失败！", {icon: 2});
                                                                }
                                                            },
                                                            error: function () {
                                                                layer.msg("保存失败！", {icon: 2});
                                                            }
                                                        })
                                                    }
                                                    , cancel: function (i, layero) {}
                                                });
                                                layer.full(index);
                                                setTimeout(function () {
                                                    $.ajax({
                                                        url: "/erp/getmanufactureaccessorybyorder",
                                                        type: 'GET',
                                                        data: {
                                                            orderName: orderName
                                                        },
                                                        success: function (res) {
                                                            var unCheckedData = res.data;
                                                            $.each(unCheckedData, function (index, item) {
                                                                item.accessoryCount = '';
                                                                item.remark = '补料';
                                                            });
                                                            var title = [
                                                                {field: 'orderName', title: '款号', align: 'center', width: '8%', filter: true},
                                                                {field: 'clothesVersionNumber', title: '版单', align: 'center', width: '8%', filter: true},
                                                                {field: 'accessoryName', title: '名称', align: 'center', width: '6%', filter: true},
                                                                {field: 'accessoryNumber', title: '编号', align: 'center', width: '6%', filter: true},
                                                                {field: 'specification', title: '规格', align: 'center', width: '8%', filter: true},
                                                                {field: 'accessoryUnit', title: '单位', align: 'center', width: '5%', filter: true},
                                                                {field: 'accessoryColor', title: '颜色', align: 'center', width: '8%', filter: true},
                                                                {field: 'pieceUsage', title: '单件用量', align: 'center', width: '8%', filter: true},
                                                                {field: 'colorName', title: '色组', align: 'center', width: '8%', filter: true},
                                                                {field: 'sizeName', title: '尺码组', align: 'center', width: '8%', filter: true},
                                                                {field: 'supplier', title: '供应商', align: 'center', width: '6%', filter: true},
                                                                {field: 'price', title: '单价', align: 'center', width: '5%', filter: true},
                                                                {field: 'accessoryCount', title: '数量', align: 'center', width: '8%',edit:'text', filter: true},
                                                                {field: 'remark', title: '备注', align: 'center', width: '8%',edit:'text', filter: true},
                                                                {field: 'accessoryID', hide: true}
                                                            ];
                                                            table.render({
                                                                elem: '#accessoryDataFeedTable'
                                                                , cols: [title]
                                                                , page: false
                                                                , height: 'full-100'
                                                                , limit: 10000
                                                                , overflow:'tips'
                                                                , data: unCheckedData
                                                                , done: function (res, curr, count) {
                                                                    var that = this.elem.next();
                                                                    res.data.forEach(function (item, index) {
                                                                        if (index % 2 == 0) {
                                                                            var tr = that.find(".layui-table-box tbody tr[data-index='" + index + "']").css("background-color", "#fff");
                                                                        } else {
                                                                            var tr = that.find(".layui-table-box tbody tr[data-index='" + index + "']").css("background-color", "#c2c2c2");
                                                                        }
                                                                    });
                                                                    soulTable.render(this);
                                                                }
                                                            });
                                                        },
                                                        error: function () {
                                                            layer.msg("获取数据失败！", {icon: 2});
                                                        }
                                                    })
                                                }, 100);
                                                layui.form.render("select");
                                            }
                                        }
                                        ,toolbarEvent: function (obj, pobj) {
                                            // obj 子表当前行对象
                                            // pobj 父表当前行对象
                                            var objData = obj.data;
                                            var childId = this.id;
                                            if (obj.event === 'batchDelete') {
                                                var checkStatus = table.checkStatus(obj.config.id);
                                                if(checkStatus.data.length == 0) {
                                                    layer.msg('请选择数据');
                                                }else {
                                                    var accessoryIDList = [];
                                                    var flag = false;
                                                    $.each(checkStatus.data, function (index, item) {
                                                        if (item.checkState != '未提交'){
                                                            flag = true
                                                        }
                                                        accessoryIDList.push(item.accessoryID);
                                                    });
                                                    if (flag) {
                                                        layer.msg('已提交审核,无法删除...');
                                                        return false;
                                                    }
                                                    layer.confirm('确认全部删除吗?', function(index){
                                                        $.ajax({
                                                            url: "/erp/deletemanufactureaccessorybatch",
                                                            type: 'POST',
                                                            data: {
                                                                accessoryIDList: accessoryIDList
                                                            },
                                                            traditional: true,
                                                            success: function (res) {
                                                                if (res.result == 0) {
                                                                    layer.close(index);
                                                                    layer.msg("删除成功！", {icon: 1});
                                                                    table.reload(childId);
                                                                } else {
                                                                    layer.msg("删除失败！", {icon: 2});
                                                                }
                                                            }, error: function () {
                                                                layer.msg("删除失败！", {icon: 2});
                                                            }
                                                        })
                                                    });
                                                }
                                            }
                                        }
                                        ,done: function () {
                                            soulTable.render(this);
                                        }
                                    } ,
                                    {
                                        title: '辅料信息'
                                        ,url: 'getmanufactureaccessorybyorder'
                                        ,where: function(row){
                                            return {orderName: row.orderName}
                                        }
                                        ,height: 300
                                        ,loading:true
                                        ,toolbar: '<div><a class="layui-btn layui-btn-danger layui-btn-sm" lay-event="batchDelete">批量删除</a><a class="layui-btn layui-btn-sm" lay-event="customerUsage">客供</a></div>'
                                        ,page: false
                                        ,cols: [[
                                            {type: 'checkbox', fixed: 'left'},
                                            {field: 'orderName', title: '款号', minWidth: 130, sort: true, filter: true},
                                            {field: 'clothesVersionNumber', title: '单号', minWidth: 130, sort: true, filter: true},
                                            {field: 'accessoryName', title: '名称', width: 150, sort: true, filter: true},
                                            {field: 'accessoryNumber', title: '编号', width: 150, sort: true, filter: true},
                                            {field: 'specification', title: '规格', width: 150, sort: true, filter: true},
                                            {field: 'accessoryUnit', title: '单位', width: 150 , filter: true},
                                            {field: 'accessoryColor', title: '颜色', width: 150, filter: true},
                                            {field: 'colorName', title: '色组', width: 150, filter: true},
                                            {field: 'sizeName', title: '尺码', width: 150, filter: true},
                                            {field: 'sizeProperty', title: '尺码属性', width: 150, filter: true},
                                            {field: 'colorProperty', title: '颜色属性', width: 150, filter: true},
                                            {field: 'wellCount', title: '好片', width: 90, filter: true},
                                            {field: 'orderPieceUsage', title: '客供用量', width: 150, filter: true},
                                            {field: 'pieceUsage', title: '用量', width: 90, filter: true},
                                            {field: 'accessoryAdd', title: '损耗(%)', width: 90, filter: true},
                                            {field: 'accessoryOrderCount', title: '客供总量', width: 90, filter: true},
                                            {field: 'accessoryCount', title: '订购量', width: 90, filter: true},
                                            {field: 'orderPrice', title: '客供单价', width: 90, filter: true},
                                            {field: 'price', title: '单价', width: 90, filter: true},
                                            {field: 'orderSumMoney', title: '客供总价', width: 90, filter: true},
                                            {field: 'sumMoney', title: '单价', width: 90, filter: true},
                                            {field: 'checkState', title: '审核', width: 123, filter: true},
                                            {field: 'remark', title: '备注', width: 123, filter: true},
                                            {field: 'accessoryID',title: '操作', width: 156, templet: function(row) {
                                                    return '<a class="layui-btn layui-btn-xs" lay-event="childEdit">修改</a><a class="layui-btn layui-btn-xs" lay-event="feed">补料</a>';
                                                }}
                                        ]]
                                        ,toolEvent: function (obj, pobj) {
                                            // obj 子表当前行对象
                                            // pobj 父表当前行对象
                                            var rowData = obj.data;
                                            var childId = this.id; // 通过 this 对象获取当前子表的id
                                            if (obj.event === 'childEdit') {
                                                var orderName = rowData.orderName;
                                                var index = layer.open({
                                                    type: 1 //Page层类型
                                                    , title: '修改辅料信息'
                                                    , btn: ['保存']
                                                    , shade: 0.6 //遮罩透明度
                                                    , maxmin: false //允许全屏最小化
                                                    , anim: 0 //0-6的动画形式，-1不开启
                                                    , content: "<div id=\"udpateAccessoryData\" style=\"cursor:default;\">\n" +
                                                        "        <table id=\"accessoryDataUpdateTable\" name = \"accessoryDataUpdateTable\" style=\"background-color: white; color: black\">\n" +
                                                        "        </table>\n" +
                                                        "    </div>"
                                                    , yes: function (index, layero) {
                                                        var accessoryDataList = table.cache.accessoryDataUpdateTable;
                                                        $.each(accessoryDataList,function (index,item) {
                                                            if (item.orderPieceUsage == ''){
                                                                item.orderPieceUsage = 0;
                                                            }
                                                            if (item.pieceUsage == ''){
                                                                item.pieceUsage = 0;
                                                            }
                                                            if (item.price == ''){
                                                                item.price = 0;
                                                            }
                                                            if (item.orderPrice == ''){
                                                                item.orderPrice = 0;
                                                            }
                                                            item.checkState = '未提交';
                                                        });
                                                        $.ajax({
                                                            url: "/erp/updatemanufactureaccessorybatch",
                                                            type: 'POST',
                                                            data: {
                                                                manufactureAccessoryJson:JSON.stringify(accessoryDataList)
                                                            },
                                                            success: function (res) {
                                                                if (res.result == 0) {
                                                                    layer.close(index);
                                                                    layer.msg("保存成功！", {icon: 1});
                                                                    initTable(rowData.orderName, '', '','');
                                                                } else {
                                                                    layer.msg("保存失败！", {icon: 2});
                                                                }
                                                            },
                                                            error: function () {
                                                                layer.msg("保存失败！", {icon: 2});
                                                            }
                                                        })
                                                    }
                                                    , cancel: function (i, layero) {
                                                        // $("#fabricDetailUpdate").find("input").val("");
                                                    }
                                                });
                                                layer.full(index);
                                                setTimeout(function () {
                                                    $.ajax({
                                                        url: "/erp/getmanufactureaccessorybyorder",
                                                        type: 'GET',
                                                        data: {
                                                            orderName: orderName
                                                        },
                                                        success: function (res) {
                                                            var unCheckedData = [];
                                                            $.each(res.data,function (index, item) {
                                                                if (item.checkState == '未提交' || item.checkState == '不通过'){
                                                                    unCheckedData.push(item);
                                                                }
                                                            });
                                                            if (unCheckedData.length == 0){
                                                                layer.close(index);
                                                                layer.msg("已提交审核，不能修改！", {icon: 2});
                                                            }
                                                            var title = [
                                                                {field: 'orderName', title: '款号', align: 'center', minWidth: 150},
                                                                {field: 'clothesVersionNumber', title: '版单', align: 'center', minWidth: 100},
                                                                {field: 'accessoryName', title: '名称', align: 'center', minWidth: 150,edit:'text'},
                                                                {field: 'accessoryNumber', title: '编号', align: 'center', minWidth: 120,edit:'text'},
                                                                {field: 'accessoryUnit', title: '单位', align: 'center', minWidth: 100,edit:'text'},
                                                                {field: 'colorProperty', title: '颜色属性', align: 'center', minWidth: 100,edit:'text'},
                                                                {field: 'sizeProperty', title: '尺码属性', align: 'center', minWidth: 100,edit:'text'},
                                                                {field: 'pieceUsage', title: '单件用量', align: 'center', minWidth: 100,edit:'text'},
                                                                {field: 'orderPieceUsage', title: '客供用量', align: 'center', minWidth: 100,edit:'text'},
                                                                {field: 'price', title: '单价', align: 'center', minWidth: 100,edit:'text'},
                                                                {field: 'orderPrice', title: '客供单价', align: 'center', minWidth: 100,edit:'text'},
                                                                {field: 'colorName', title: '色组', align: 'center', minWidth: 100},
                                                                {field: 'sizeName', title: '尺码组', align: 'center', minWidth: 100},
                                                                {field: 'accessoryID', title: '', align: 'center', minWidth: 100, hide: true}
                                                            ];
                                                            table.render({
                                                                elem: '#accessoryDataUpdateTable'
                                                                , cols: [title]
                                                                , page: false
                                                                , height: 'full-100'
                                                                , limit: Number.MAX_VALUE
                                                                , data: unCheckedData
                                                                , done: function (res, curr, count) {

                                                                    var that = this.elem.next();
                                                                    res.data.forEach(function (item, index) {
                                                                        if (index % 2 == 0) {
                                                                            var tr = that.find(".layui-table-box tbody tr[data-index='" + index + "']").css("background-color", "#fff");
                                                                        } else {
                                                                            var tr = that.find(".layui-table-box tbody tr[data-index='" + index + "']").css("background-color", "#c2c2c2");
                                                                        }
                                                                    });

                                                                    soulTable.render(this);
                                                                }
                                                            });
                                                        },
                                                        error: function () {
                                                            layer.msg("获取数据失败！", {icon: 2});
                                                        }
                                                    })
                                                }, 100);
                                                layui.form.render("select");
                                            }
                                            else if (obj.event === 'feed') {
                                                var orderName = rowData.orderName;
                                                var index = layer.open({
                                                    type: 1 //Page层类型
                                                    , title: '辅料补充'
                                                    , btn: ['保存']
                                                    , shade: 0.6 //遮罩透明度
                                                    , maxmin: false //允许全屏最小化
                                                    , anim: 0 //0-6的动画形式，-1不开启
                                                    , content: "<div id=\"feedAccessoryData\" style=\"cursor:default;\">\n" +
                                                        "        <table id=\"accessoryDataFeedTable\" name = \"accessoryDataFeedTable\" style=\"background-color: white; color: black\">\n" +
                                                        "        </table>\n" +
                                                        "    </div>"
                                                    , yes: function (index, layero) {
                                                        var accessoryDataList = table.cache.accessoryDataFeedTable;
                                                        var nowDate = dateFormat("YYYY-mm-dd HH:MM:SS", new Date()).replace(" ", "").replace(/\:/g,'').replace(/\-/g,'');
                                                        var addAccessoryList = [];
                                                        var keyIndex = 0;
                                                        $.each(accessoryDataList,function (index,item) {
                                                            if (item.accessoryCount != null && item.accessoryCount != '' && Number(item.accessoryCount) > 0){
                                                                item.orderState = '未下单';
                                                                item.preCheck = '已提交';
                                                                item.checkState = '未提交';
                                                                item.sumMoney = Number(item.price) * Number(item.accessoryCount);
                                                                item.checkNumber = nowDate;
                                                                item.accessoryKey = keyIndex ++;
                                                                item.batchOrder = 1;
                                                                addAccessoryList.push(item);
                                                            }
                                                        });
                                                        if (addAccessoryList.length == 0){
                                                            layer.msg("没有输入辅料数量,请核实！", {icon: 2});
                                                            return false;
                                                        }
                                                        $.ajax({
                                                            url: "/erp/addmanufactureaccessorybatch",
                                                            type: 'POST',
                                                            data: {
                                                                manufactureAccessoryJson:JSON.stringify(addAccessoryList),
                                                                orderName: rowData.orderName,
                                                                userName: userName
                                                            },
                                                            success: function (res) {
                                                                if (res.result == 0) {
                                                                    layer.close(index);
                                                                    layer.msg("保存成功！", {icon: 1});
                                                                    initTable(rowData.orderName, '', '','');
                                                                } else {
                                                                    layer.msg("保存失败！", {icon: 2});
                                                                }
                                                            },
                                                            error: function () {
                                                                layer.msg("保存失败！", {icon: 2});
                                                            }
                                                        })
                                                    }
                                                    , cancel: function (i, layero) {}
                                                });
                                                layer.full(index);
                                                setTimeout(function () {
                                                    $.ajax({
                                                        url: "/erp/getmanufactureaccessorybyorder",
                                                        type: 'GET',
                                                        data: {
                                                            orderName: orderName
                                                        },
                                                        success: function (res) {
                                                            var unCheckedData = res.data;
                                                            $.each(unCheckedData, function (index, item) {
                                                                item.accessoryCount = '';
                                                                item.remark = '补料';
                                                            });
                                                            var title = [
                                                                {field: 'orderName', title: '款号', align: 'center', width: '8%', filter: true},
                                                                {field: 'clothesVersionNumber', title: '版单', align: 'center', width: '8%', filter: true},
                                                                {field: 'accessoryName', title: '名称', align: 'center', width: '6%', filter: true},
                                                                {field: 'accessoryNumber', title: '编号', align: 'center', width: '6%', filter: true},
                                                                {field: 'specification', title: '规格', align: 'center', width: '8%', filter: true},
                                                                {field: 'accessoryUnit', title: '单位', align: 'center', width: '5%', filter: true},
                                                                {field: 'accessoryColor', title: '颜色', align: 'center', width: '8%', filter: true},
                                                                {field: 'pieceUsage', title: '单件用量', align: 'center', width: '8%', filter: true},
                                                                {field: 'colorName', title: '色组', align: 'center', width: '8%', filter: true},
                                                                {field: 'sizeName', title: '尺码组', align: 'center', width: '8%', filter: true},
                                                                {field: 'supplier', title: '供应商', align: 'center', width: '6%', filter: true},
                                                                {field: 'price', title: '单价', align: 'center', width: '5%', filter: true},
                                                                {field: 'accessoryCount', title: '数量', align: 'center', width: '8%',edit:'text', filter: true},
                                                                {field: 'remark', title: '备注', align: 'center', width: '8%',edit:'text', filter: true},
                                                                {field: 'accessoryID', hide: true}
                                                            ];
                                                            table.render({
                                                                elem: '#accessoryDataFeedTable'
                                                                , cols: [title]
                                                                , page: false
                                                                , height: 'full-100'
                                                                , limit: 10000
                                                                , overflow:'tips'
                                                                , data: unCheckedData
                                                                , done: function (res, curr, count) {
                                                                    var that = this.elem.next();
                                                                    res.data.forEach(function (item, index) {
                                                                        if (index % 2 == 0) {
                                                                            var tr = that.find(".layui-table-box tbody tr[data-index='" + index + "']").css("background-color", "#fff");
                                                                        } else {
                                                                            var tr = that.find(".layui-table-box tbody tr[data-index='" + index + "']").css("background-color", "#c2c2c2");
                                                                        }
                                                                    });
                                                                    soulTable.render(this);
                                                                }
                                                            });
                                                        },
                                                        error: function () {
                                                            layer.msg("获取数据失败！", {icon: 2});
                                                        }
                                                    })
                                                }, 100);
                                                layui.form.render("select");
                                            }
                                        }
                                        ,toolbarEvent: function (obj, pobj) {
                                            // obj 子表当前行对象
                                            // pobj 父表当前行对象
                                            var objData = obj.data;
                                            var childId = this.id;
                                            var pData = pobj.data;
                                            if (obj.event === 'batchDelete') {
                                                var checkStatus = table.checkStatus(obj.config.id);
                                                if(checkStatus.data.length == 0) {
                                                    layer.msg('请选择数据');
                                                }else {
                                                    var accessoryIDList = [];
                                                    var flag = false;
                                                    $.each(checkStatus.data, function (index, item) {
                                                        if (item.checkState != '未提交'){
                                                            flag = true
                                                        }
                                                        accessoryIDList.push(item.accessoryID);
                                                    });
                                                    if (flag) {
                                                        layer.msg('已提交审核,无法删除...');
                                                        return false;
                                                    }
                                                    layer.confirm('确认全部删除吗?', function(index){
                                                        $.ajax({
                                                            url: "/erp/deletemanufactureaccessorybatch",
                                                            type: 'POST',
                                                            data: {
                                                                accessoryIDList: accessoryIDList
                                                            },
                                                            traditional: true,
                                                            success: function (res) {
                                                                if (res.result == 0) {
                                                                    layer.close(index);
                                                                    layer.msg("删除成功！", {icon: 1});
                                                                    table.reload(childId);
                                                                } else {
                                                                    layer.msg("删除失败！", {icon: 2});
                                                                }
                                                            }, error: function () {
                                                                layer.msg("删除失败！", {icon: 2});
                                                            }
                                                        })
                                                    });
                                                }
                                            }
                                            else if (obj.event === 'customerUsage') {
                                                var orderName = pData.orderName;
                                                var index = layer.open({
                                                    type: 1 //Page层类型
                                                    , title: '修改客供用量/单价'
                                                    , btn: ['保存']
                                                    , shade: 0.6 //遮罩透明度
                                                    , maxmin: false //允许全屏最小化
                                                    , anim: 0 //0-6的动画形式，-1不开启
                                                    , content: "<div id=\"udpateAccessoryData\" style=\"cursor:default;\">\n" +
                                                        "        <table id=\"accessoryDataUpdateTable\" name = \"accessoryDataUpdateTable\" style=\"background-color: white; color: black\">\n" +
                                                        "        </table>\n" +
                                                        "    </div>"
                                                    , yes: function (index, layero) {
                                                        var accessoryDataList = table.cache.accessoryDataUpdateTable;
                                                        $.each(accessoryDataList,function (index,item) {
                                                            if (item.orderPieceUsage == ''){
                                                                item.orderPieceUsage = 0;
                                                            }
                                                            if (item.orderPrice == ''){
                                                                item.orderPrice = 0;
                                                            }
                                                        });
                                                        $.ajax({
                                                            url: "/erp/updateaccessorycustomerusagebatch",
                                                            type: 'POST',
                                                            data: {
                                                                manufactureAccessoryJson:JSON.stringify(accessoryDataList)
                                                            },
                                                            success: function (res) {
                                                                if (res.result == 0) {
                                                                    layer.close(index);
                                                                    layer.msg("保存成功！", {icon: 1});
                                                                    initTable(orderName, '', '','');
                                                                } else {
                                                                    layer.msg("保存失败！", {icon: 2});
                                                                }
                                                            },
                                                            error: function () {
                                                                layer.msg("保存失败！", {icon: 2});
                                                            }
                                                        })
                                                    }
                                                    , cancel: function (i, layero) {}
                                                });
                                                layer.full(index);
                                                setTimeout(function () {
                                                    $.ajax({
                                                        url: "/erp/getmanufactureaccessorybyorder",
                                                        type: 'GET',
                                                        data: {
                                                            orderName: orderName
                                                        },
                                                        success: function (res) {
                                                            var unCheckedData = res.data;
                                                            if (unCheckedData.length === 0){
                                                                layer.close(index);
                                                                layer.msg("已提交审核，不能修改！", {icon: 2});
                                                            }
                                                            var title = [
                                                                {field: 'orderName', title: '款号', align: 'center', minWidth: 150},
                                                                {field: 'clothesVersionNumber', title: '版单', align: 'center', minWidth: 100},
                                                                {field: 'accessoryName', title: '名称', align: 'center', minWidth: 150},
                                                                {field: 'accessoryNumber', title: '编号', align: 'center', minWidth: 120},
                                                                {field: 'accessoryUnit', title: '单位', align: 'center', minWidth: 100},
                                                                {field: 'colorProperty', title: '颜色属性', align: 'center', minWidth: 100},
                                                                {field: 'sizeProperty', title: '尺码属性', align: 'center', minWidth: 100},
                                                                {field: 'pieceUsage', title: '单件用量', align: 'center', minWidth: 100},
                                                                {field: 'price', title: '单价', align: 'center', minWidth: 100},
                                                                {field: 'orderPieceUsage', title: '客供用量', align: 'center', minWidth: 100,edit:'text'},
                                                                {field: 'orderPrice', title: '客供单价', align: 'center', minWidth: 100,edit:'text'},
                                                                {field: 'colorName', title: '色组', align: 'center', minWidth: 100},
                                                                {field: 'sizeName', title: '尺码组', align: 'center', minWidth: 100},
                                                                {field: 'accessoryID', hide: true}
                                                            ];
                                                            table.render({
                                                                elem: '#accessoryDataUpdateTable'
                                                                , cols: [title]
                                                                , page: false
                                                                , height: 'full-150'
                                                                , limit: Number.MAX_VALUE
                                                                , data: unCheckedData
                                                                , done: function (res, curr, count) {
                                                                    var that = this.elem.next();
                                                                    res.data.forEach(function (item, index) {
                                                                        if (index % 2 == 0) {
                                                                            var tr = that.find(".layui-table-box tbody tr[data-index='" + index + "']").css("background-color", "#fff");
                                                                        } else {
                                                                            var tr = that.find(".layui-table-box tbody tr[data-index='" + index + "']").css("background-color", "#c2c2c2");
                                                                        }
                                                                    });
                                                                    soulTable.render(this);
                                                                }
                                                            });
                                                        },
                                                        error: function () {
                                                            layer.msg("获取数据失败！", {icon: 2});
                                                        }
                                                    })
                                                }, 100);
                                                layui.form.render("select");
                                            }
                                        }
                                        ,done: function () {
                                            soulTable.render(this);
                                        }
                                        ,filter: {
                                            bottom: false,
                                            items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                                        }
                                    } ,
                                    {
                                        title: '价格'
                                        ,url: 'getclothespricebyorder'
                                        ,where: function(row){
                                            return {orderName: row.orderName}
                                        }
                                        ,height: 300
                                        ,loading:true
                                        ,toolbar: '<div><a class="layui-btn layui-btn-danger layui-btn-sm" lay-event="batchDelete">批量删除</a></div>'
                                        ,page: false
                                        ,cols: [[
                                            {type: 'checkbox', fixed: 'left'},
                                            {field: 'orderName', title: '款号', minWidth: 130, sort: true, filter: true},
                                            {field: 'clothesVersionNumber', title: '单号', minWidth: 130, sort: true, filter: true},
                                            {field: 'colorName', title: '颜色', width: 150, sort: true, filter: true},
                                            {field: 'clothesPrice', title: '衣服单价', width: 150, sort: true, filter: true},
                                            {field: 'printPrice', title: '印花单价', width: 150, sort: true, filter: true},
                                            {field: 'sewPrice', title: '绣花单价', width: 150 , filter: true},
                                            {field: 'id',title: '操作', width: 156, templet: function(row) {
                                                    return '<a class="layui-btn layui-btn-xs" lay-event="childEdit">修改</a>';
                                                }}
                                        ]]
                                        ,toolEvent: function (obj, pobj) {
                                            // obj 子表当前行对象
                                            // pobj 父表当前行对象
                                            var rowData = obj.data;
                                            var childId = this.id; // 通过 this 对象获取当前子表的id
                                            if (obj.event === 'childEdit') {
                                                var orderName = rowData.orderName;
                                                var index = layer.open({
                                                    type: 1 //Page层类型
                                                    , title: '修改价格信息'
                                                    , btn: ['保存']
                                                    , shade: 0.6 //遮罩透明度
                                                    , maxmin: false //允许全屏最小化
                                                    , anim: 0 //0-6的动画形式，-1不开启
                                                    , content: "<div id=\"addClothesPrice\">\n" +
                                                        "        <form class=\"layui-form\" style=\"margin: 0 auto;max-width:100%;padding-top: 40px;\">\n" +
                                                        "            <table class=\"layui-hide\" id=\"clothesPriceTable\" lay-filter=\"clothesPriceTable\"></table>\n" +
                                                        "        </form>\n" +
                                                        "    </div>"
                                                    , yes: function (index, layero) {
                                                        var priceData = table.cache.clothesPriceTable;
                                                        $.ajax({
                                                            url: "/erp/updateclothespricebatch",
                                                            type: 'POST',
                                                            data: {
                                                                clothesPriceJson:JSON.stringify(priceData)
                                                            },
                                                            success: function (res) {
                                                                if (res.result == 0) {
                                                                    layer.close(index);
                                                                    layer.msg("保存成功！", {icon: 1});
                                                                    initTable(rowData.orderName, '', '','');
                                                                } else {
                                                                    layer.msg("保存失败！", {icon: 2});
                                                                }
                                                            },
                                                            error: function () {
                                                                layer.msg("保存失败！", {icon: 2});
                                                            }
                                                        })
                                                    }
                                                    , cancel: function (i, layero) {
                                                        // $("#fabricDetailUpdate").find("input").val("");
                                                    }
                                                });
                                                layer.full(index);
                                                setTimeout(function () {
                                                    $.ajax({
                                                        url: "/erp/getclothespricebyorder",
                                                        type: 'GET',
                                                        data: {
                                                            orderName: orderName
                                                        },
                                                        success: function (res) {
                                                            var title = [
                                                                {field: 'orderName', title: '款号', align: 'center', minWidth: 150},
                                                                {field: 'clothesVersionNumber', title: '版单', align: 'center', minWidth: 100},
                                                                {field: 'colorName', title: '颜色', align: 'center', minWidth: 150},
                                                                {field: 'clothesPrice', title: '衣服单价', align: 'center', minWidth: 120,edit:'text'},
                                                                {field: 'printPrice', title: '印花单价', align: 'center', minWidth: 120,edit:'text'},
                                                                {field: 'sewPrice', title: '绣花单价', align: 'center', minWidth: 100,edit:'text'},
                                                                {field: 'id', title: '', align: 'center', minWidth: 100, hide: true}
                                                            ];
                                                            table.render({
                                                                elem: '#clothesPriceTable'
                                                                , cols: [title]
                                                                , page: false
                                                                , height: 'full-100'
                                                                , limit: Number.MAX_VALUE
                                                                , data: res.data
                                                                , done: function (res, curr, count) {
                                                                    soulTable.render(this);
                                                                }
                                                            });
                                                        },
                                                        error: function () {
                                                            layer.msg("获取数据失败！", {icon: 2});
                                                        }
                                                    })
                                                }, 100);
                                                layui.form.render("select");
                                            }
                                        }
                                        ,toolbarEvent: function (obj, pobj) {
                                            // obj 子表当前行对象
                                            // pobj 父表当前行对象
                                            var objData = obj.data;
                                            var childId = this.id;
                                            if (obj.event === 'batchDelete') {
                                                var checkStatus = table.checkStatus(obj.config.id);
                                                if(checkStatus.data.length == 0) {
                                                    layer.msg('请选择数据');
                                                }else {
                                                    var idList = [];
                                                    $.each(checkStatus.data, function (index, item) {
                                                        idList.push(item.id);
                                                    });
                                                    layer.confirm('确认全部删除吗?', function(index){
                                                        $.ajax({
                                                            url: "/erp/deleteclothespricebatch",
                                                            type: 'POST',
                                                            data: {
                                                                idList: idList
                                                            },
                                                            traditional: true,
                                                            success: function (res) {
                                                                if (res.result == 0) {
                                                                    layer.close(index);
                                                                    layer.msg("删除成功！", {icon: 1});
                                                                    table.reload(childId);
                                                                } else {
                                                                    layer.msg("删除失败！", {icon: 2});
                                                                }
                                                            }, error: function () {
                                                                layer.msg("删除失败！", {icon: 2});
                                                            }
                                                        })
                                                    });
                                                }
                                            }
                                        }
                                        ,done: function () {
                                            soulTable.render(this);
                                        }
                                        ,filter: {
                                            bottom: false,
                                            items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                                        }
                                    }
                                ]}
                            ,{type:'radio'}
                            ,{type:'numbers', align:'center', title:'序号', width:60}
                            ,{field:'orderName', title:'款号', align:'center', width:170, sort: true, filter: true, totalRowText: '合计'}
                            ,{field:'clothesVersionNumber', title:'版单号', align:'center', width:170, sort: true, filter: true}
                            ,{field:'customerName', title:'品牌', align:'center', width:120, sort: true, filter: true}
                            ,{field:'orderSum', title:'订单量', align:'center', width:120, sort: true, filter: true, totalRow: true}
                            ,{field:'styleDescription', title:'款式描述', align:'center',width:200, sort: true, filter: true}
                            ,{field:'season', title:'季度', align:'center', width:120, sort: true, filter: true}
                            ,{field:'deadLine', title:'收单日期', align:'center', width:120, sort: true, filter: true, templet:function (d) {
                                    return moment(d.deadLine).format("YYYY-MM-DD");
                                }}
                            ,{field:'deliveryDate', title:'交期', align:'center', width:120, sort: true, filter: true, templet:function (d) {
                                    return moment(d.deliveryDate).format("YYYY-MM-DD");
                                }}
                            ,{field:'userName', title:'跟单', align:'center', width:120, sort: true, filter: true}
                            ,{field:'modifyState', title:'状态', align:'center', width:70, sort: true, filter: true}
                            ,{fixed: 'right', title:'操作', align:'center', toolbar: '#barTop', width:330}
                        ]]
                        ,data:reportData
                        ,height: 'full-90'
                        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                        ,defaultToolbar: ['filter', 'print']
                        ,title: '制单总表'
                        ,totalRow: true
                        ,page: true
                        ,even: true
                        ,overflow: 'tips'
                        ,limits: [50, 100, 200]
                        ,limit: 50 //每页默认显示的数量
                        ,done: function (res, curr, count) {
                            soulTable.render(this);
                            var divArr = $(".layui-table-total div.layui-table-cell");
                            $.each(divArr,function (index,item) {
                                var _div = $(item);
                                var content = _div.html();
                                content = content.replace(".00","");
                                _div.html(content);
                            });
                        }
                        ,filter: {
                            bottom: true,
                            items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                        }
                    });
                }
            }
        })
    }

    form.on('submit(searchBeat)', function(data){
        var filterOrderName = $("#orderName").val();
        // var filterCustomerName = $("#customerName").val();
        var filterCustomerName = customerNameDemo.getValue('nameStr');
        var filterSeason = $("#seasonName").val();
        initTable(filterOrderName, filterSeason, filterCustomerName,'');
        return false;
    });

    table.on('toolbar(processOrderTable)', function(obj){
        if (obj.event === 'clearFilter') {
            // 清除所有筛选条件并重载表格
            // 参数: tableId
            soulTable.clearFilter('processOrderTable')
        }
        else if (obj.event === 'refresh') {
            initTable('', '', '', 0);
        }
        else if (obj.event === 'exportExcel') {
            soulTable.export('processOrderTable');
        }
        else if(obj.event === 'addBasicOrderData') {  //录入基础信息
            basicOrderInfo(0, '', '');
        }
        else if (obj.event === "addAccessoryData") {   //录入辅料信息
            if (userRole != "role17" && userRole != "root" && userRole != "role2"){
                layer.msg("您没有权限！", {icon: 2});
                return false;
            }
            var checkStatus = table.checkStatus(obj.config.id); //获取选中行状态
            if(checkStatus.data.length == 0) {
                layer.msg('请先选择订单');
            }else {
                var colorProperty = {};
                var sizeProperty = {};
                var colorCheckProperty = {};
                var sizeCheckProperty = {};
                var colorData=[];
                var sizeData=[];
                var thisSizeNameList;
                var thisColorNameList;
                var orderName = checkStatus.data[0].orderName;
                var clothesVersionNumber = checkStatus.data[0].clothesVersionNumber;
                var label = "录入辅料信息&emsp;&emsp;&emsp;&emsp;" + "单号："+clothesVersionNumber+"&emsp;&emsp;&emsp;&emsp;款号："+orderName + "名称,单位,色组,尺码必须" +"&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;试试在下面直接输入相似款进行复制~~~";
                var index = layer.open({
                    type: 1 //Page层类型
                    , title: label
                    , btn: ['保存','添加一行','制单数据']
                    , shade: 0.6 //遮罩透明度
                    , area: '1000px'
                    , maxmin: false //允许全屏最小化
                    , anim: 0 //0-6的动画形式，-1不开启
                    , content: "<div id=\"addAccessoryData\">\n" +
                        "             <div class=\"layui-row\">\n" +
                        "                <form class=\"layui-form\" style=\"margin: 0 auto;padding-top: 3px;\" lay-filter=\"formCopyInfo\">\n" +
                        "                    <table>\n" +
                        "                    <tr>\n" +
                        "                        <td style=\"text-align: right;margin-bottom: 15px;\">\n" +
                        "                            <label class=\"layui-form-label\" style=\"width: 100px\">单号</label>\n" +
                        "                        </td>\n" +
                        "                        <td style=\"margin-bottom: 15px;\">\n" +
                        "                            <input type=\"text\" id=\"clothesVersionNumberTwo\" autocomplete=\"off\" class=\"layui-input\">\n" +
                        "                        </td>\n" +
                        "                        <td style=\"text-align: right;margin-bottom: 15px;\">\n" +
                        "                            <label class=\"layui-form-label\" style=\"width: 100px\">款号</label>\n" +
                        "                        </td>\n" +
                        "                        <td style=\"margin-bottom: 15px;\">\n" +
                        "                            <input type=\"text\" id=\"orderNameTwo\" autocomplete=\"off\" class=\"layui-input\">\n" +
                        "                        </td>\n" +
                        "                        <td style=\"text-align: right;margin-bottom: 15px;\">\n" +
                        "                            <label class=\"layui-form-label\"></label>\n" +
                        "                        </td>\n" +
                        "                        <td style=\"margin-bottom: 15px;\">\n" +
                        "                            <div class=\"layui-input-inline\">\n" +
                        "                                <button class=\"layui-btn\" lay-submit lay-filter=\"searchCopyBeat\">搜索</button>\n" +
                        "                            </div>\n" +
                        "                        </td>\n" +
                        "                    </tr>\n" +
                        "                </table>\n" +
                        "                </form>\n" +
                        "            </div>\n"+
                        "        <form class=\"layui-form\" style=\"margin: 0 auto;width:100%; min-height:400px; padding-top: 5px;\">\n" +
                        "            <table class=\"layui-hide\" id=\"accessoryDataTable\" lay-filter=\"accessoryDataTable\"></table>\n" +
                        "        </form>\n" +
                        "    </div>"
                    ,yes: function(i, layero){
                        var data = table.cache.accessoryDataTable;
                        var accessoryDataList = [];
                        var isInput = true;
                        var pieceFlag = true;
                        $.each(data,function (index, item) {
                            if(item.accessoryName=='' || item.accessoryUnit=='' || item.supplier=='' || item.accessoryType=='') {
                                isInput = false;
                                return false;
                            }
                            var pieceUsage = 0;
                            var orderPieceUsage = 0;
                            if (Number(item.orderPieceUsage) > 0){
                                orderPieceUsage = Number(item.orderPieceUsage);
                            }
                            if (item.pieceUsage != ''){
                                if (!isNumber(item.pieceUsage)){
                                    pieceFlag = false;
                                    return false;
                                } else {
                                    pieceUsage = item.pieceUsage;
                                }
                            }
                            var colorEditId = 'colorEdit' + index;
                            var sizeEditId = 'sizeEdit' + index;
                            if (item.colorChild === '1' && item.sizeChild === '1'){
                                if (colorCheckProperty[colorEditId] && sizeCheckProperty[sizeEditId]){
                                    var checkColorList = colorCheckProperty[colorEditId];
                                    var checkSizeList = sizeCheckProperty[sizeEditId];
                                    var colorGroup = "";
                                    var sizeGroup = "";
                                    $.each(checkColorList,function (i_c, color) {
                                        colorGroup += color + ",";
                                    });
                                    $.each(checkSizeList,function (i_s, size) {
                                        sizeGroup += size + ",";
                                    });
                                    colorGroup = colorGroup.substr(0,colorGroup.length - 1);
                                    sizeGroup = sizeGroup.substr(0,sizeGroup.length - 1);
                                    var accessory = {orderPieceUsage: orderPieceUsage,accessoryKey: index, batchOrder: 0, colorChild: 2, sizeChild: 2, colorName: colorGroup, sizeName: sizeGroup, orderName:orderName, clothesVersionNumber:clothesVersionNumber, accessoryName:item.accessoryName, accessoryNumber:item.accessoryNumber, accessoryColor:item.accessoryColor, specification:item.specification, accessoryUnit:item.accessoryUnit, accessoryType:item.accessoryType, supplier:item.supplier, pieceUsage:pieceUsage, colorProperty: '', sizeProperty: '', orderState:'未下单', checkState:'未提交', accessoryAdd:item.accessoryAdd};
                                    accessoryDataList.push(accessory);
                                } else if (colorCheckProperty[colorEditId]){
                                    var checkColorList = colorCheckProperty[colorEditId];
                                    var colorGroup = "";
                                    $.each(checkColorList,function (i_c, color) {
                                        colorGroup += color + ",";
                                    });
                                    colorGroup = colorGroup.substr(0,colorGroup.length - 1);

                                    $.each(thisSizeNameList,function (i_s, size) {
                                        var thisSizeProperty = '';
                                        if (sizeProperty[sizeEditId]){
                                            var tmpSizeProperty = sizeProperty[sizeEditId];
                                            thisSizeProperty = tmpSizeProperty[size];
                                        }
                                        var accessory = {orderPieceUsage: orderPieceUsage,accessoryKey: index, batchOrder: 0, colorChild: 2, sizeChild: 1, colorName: colorGroup, sizeName: size, orderName:orderName, clothesVersionNumber:clothesVersionNumber, accessoryName:item.accessoryName, accessoryNumber:item.accessoryNumber, accessoryColor:item.accessoryColor, specification:thisSizeProperty, accessoryUnit:item.accessoryUnit, accessoryType:item.accessoryType, supplier:item.supplier, pieceUsage:pieceUsage, colorProperty: '', sizeProperty: thisSizeProperty, orderState:'未下单', checkState:'未提交', accessoryAdd:item.accessoryAdd};
                                        accessoryDataList.push(accessory);
                                    });
                                } else if (sizeCheckProperty[sizeEditId]){
                                    var checkSizeList = sizeCheckProperty[sizeEditId];
                                    var sizeGroup = "";
                                    $.each(checkSizeList,function (i_s, size) {
                                        sizeGroup += size + ",";
                                    });
                                    sizeGroup = sizeGroup.substr(0,sizeGroup.length - 1);
                                    $.each(thisColorNameList,function (i_c, color) {
                                        var thisColorProperty = '';
                                        if (colorProperty[colorEditId]){
                                            var tmpColorProperty = colorProperty[colorEditId];
                                            thisColorProperty = tmpColorProperty[color];
                                        }
                                        var accessory = {orderPieceUsage: orderPieceUsage,accessoryKey: index, batchOrder: 0, colorChild: 1, sizeChild: 2, colorName: color, sizeName: sizeGroup, orderName:orderName, clothesVersionNumber:clothesVersionNumber, accessoryName:item.accessoryName, accessoryNumber:item.accessoryNumber, accessoryColor:thisColorProperty, specification:item.specification, accessoryUnit:item.accessoryUnit, accessoryType:item.accessoryType, supplier:item.supplier, pieceUsage:pieceUsage, colorProperty: thisColorProperty, sizeProperty: '', orderState:'未下单', checkState:'未提交', accessoryAdd:item.accessoryAdd};
                                        accessoryDataList.push(accessory);
                                    });
                                } else {
                                    $.each(thisColorNameList,function (i_c, color) {
                                        var thisColorProperty = '';
                                        if (colorProperty[colorEditId]){
                                            var tmpColorProperty = colorProperty[colorEditId];
                                            thisColorProperty = tmpColorProperty[color];
                                        }
                                        $.each(thisSizeNameList,function (i_s, size) {
                                            var thisSizeProperty = '';
                                            if (sizeProperty[sizeEditId]){
                                                var tmpSizeProperty = sizeProperty[sizeEditId];
                                                thisSizeProperty = tmpSizeProperty[size];
                                            }
                                            var accessory = {orderPieceUsage: orderPieceUsage,accessoryKey: index, batchOrder: 0, colorChild: 1, sizeChild: 1, colorName: color, sizeName: size, orderName:orderName, clothesVersionNumber:clothesVersionNumber, accessoryName:item.accessoryName, accessoryNumber:item.accessoryNumber, accessoryColor:thisColorProperty, specification:thisSizeProperty, accessoryUnit:item.accessoryUnit, accessoryType:item.accessoryType, supplier:item.supplier, pieceUsage:pieceUsage, colorProperty: thisColorProperty, sizeProperty: thisSizeProperty, orderState:'未下单', checkState:'未提交', accessoryAdd:item.accessoryAdd};
                                            accessoryDataList.push(accessory);
                                        })
                                    })
                                }
                            }
                            if (item.colorChild === '1' && item.sizeChild === '0'){
                                if (colorCheckProperty[colorEditId]){
                                    var checkColorList = colorCheckProperty[colorEditId];
                                    var colorGroup = "";
                                    $.each(checkColorList,function (i_c, color) {
                                        colorGroup += color + ",";
                                    });
                                    colorGroup = colorGroup.substr(0,colorGroup.length - 1);
                                    var accessory = {orderPieceUsage: orderPieceUsage,accessoryKey: index, batchOrder: 0, colorChild: 2, sizeChild: 0, colorName: colorGroup, sizeName: "通用", orderName:orderName, clothesVersionNumber:clothesVersionNumber, accessoryName:item.accessoryName, accessoryNumber:item.accessoryNumber, accessoryColor:item.accessoryColor, specification:item.specification, accessoryUnit:item.accessoryUnit, accessoryType:item.accessoryType, supplier:item.supplier, pieceUsage:pieceUsage, colorProperty: '', sizeProperty: '', orderState:'未下单', checkState:'未提交', accessoryAdd:item.accessoryAdd};
                                    accessoryDataList.push(accessory);
                                } else {
                                    $.each(thisColorNameList,function (i_c, color) {
                                        var thisColorProperty = '';
                                        if (colorProperty[colorEditId]){
                                            var tmpColorProperty = colorProperty[colorEditId];
                                            thisColorProperty = tmpColorProperty[color];
                                        }
                                        var accessory = {orderPieceUsage: orderPieceUsage,accessoryKey: index, batchOrder: 0, colorChild: 1, sizeChild: 0, colorName: color, sizeName: '通用', orderName:orderName, clothesVersionNumber:clothesVersionNumber, accessoryName:item.accessoryName, accessoryNumber:item.accessoryNumber, accessoryColor:thisColorProperty, specification:item.specification, accessoryUnit:item.accessoryUnit, accessoryType:item.accessoryType, supplier:item.supplier, pieceUsage:pieceUsage, colorProperty: thisColorProperty, sizeProperty: '', orderState:'未下单', checkState:'未提交', accessoryAdd:item.accessoryAdd};
                                        accessoryDataList.push(accessory);
                                    })
                                }
                            }
                            if (item.colorChild === '0' && item.sizeChild === '1'){
                                if (sizeCheckProperty[sizeEditId]){
                                    var checkSizeList = sizeCheckProperty[sizeEditId];
                                    var sizeGroup = "";
                                    $.each(checkSizeList,function (i_s, size) {
                                        sizeGroup += size + ",";
                                    });
                                    sizeGroup = sizeGroup.substr(0,sizeGroup.length - 1);
                                    var accessory = {orderPieceUsage: orderPieceUsage,accessoryKey: index, batchOrder: 0, colorChild: 0, sizeChild: 2, colorName: '通用', sizeName: sizeGroup, orderName:orderName, clothesVersionNumber:clothesVersionNumber, accessoryName:item.accessoryName, accessoryNumber:item.accessoryNumber, accessoryColor:item.accessoryColor, specification:item.specification, accessoryUnit:item.accessoryUnit, accessoryType:item.accessoryType, supplier:item.supplier, pieceUsage:pieceUsage, colorProperty: '', sizeProperty: '', orderState:'未下单', checkState:'未提交', accessoryAdd:item.accessoryAdd};
                                    accessoryDataList.push(accessory);
                                } else {
                                    $.each(thisSizeNameList,function (i_s, size) {
                                        var thisSizeProperty = '';
                                        if (sizeProperty[sizeEditId]){
                                            var tmpSizeProperty = sizeProperty[sizeEditId];
                                            thisSizeProperty = tmpSizeProperty[size];
                                        }
                                        var accessory = {orderPieceUsage: orderPieceUsage,accessoryKey: index, batchOrder: 0, colorChild: 0, sizeChild: 1, colorName: '通用', sizeName: size, orderName:orderName, clothesVersionNumber:clothesVersionNumber, accessoryName:item.accessoryName, accessoryNumber:item.accessoryNumber, accessoryColor:item.accessoryColor, specification:thisSizeProperty, accessoryUnit:item.accessoryUnit, accessoryType:item.accessoryType, supplier:item.supplier, pieceUsage:pieceUsage, colorProperty: '', sizeProperty: thisSizeProperty, orderState:'未下单', checkState:'未提交', accessoryAdd:item.accessoryAdd};
                                        accessoryDataList.push(accessory);
                                    })
                                }
                            }
                            if (item.colorChild === '0' && item.sizeChild === '0'){
                                var accessory = {orderPieceUsage: orderPieceUsage,accessoryKey: index, batchOrder: 0, colorChild: 0, sizeChild: 0, colorName: '通用', sizeName: '通用', orderName:orderName, clothesVersionNumber:clothesVersionNumber, accessoryName:item.accessoryName, accessoryNumber:item.accessoryNumber, accessoryColor:item.accessoryColor, specification:item.specification, accessoryUnit:item.accessoryUnit, accessoryType:item.accessoryType, supplier:item.supplier, pieceUsage:pieceUsage, colorProperty: '', sizeProperty: '', orderState:'未下单', checkState:'未提交', accessoryAdd:item.accessoryAdd};
                                accessoryDataList.push(accessory);
                            }
                        });
                        if(!isInput) {
                            layer.msg("请输入完所有内容", { icon: 2 });
                        } else if (!pieceFlag){
                            layer.msg("单件用量有误", { icon: 2 });
                        } else {
                            var load = layer.load();
                            $.ajax({
                                url: "/erp/addmanufactureaccessorybatch",
                                type: 'POST',
                                data: {
                                    manufactureAccessoryJson:JSON.stringify(accessoryDataList),
                                    orderName: orderName,
                                    userName: userName
                                },
                                success: function (res) {
                                    layer.close(load);
                                    if (res.result == 0) {
                                        initTable('', '', '', 0);
                                        layer.close(index);
                                        layer.msg("录入成功！", {icon: 1});
                                    } else if (res.result == 3){
                                        layer.msg(res.msg, {icon: 2});
                                    } else {
                                        layer.msg("录入失败！", {icon: 2});
                                    }
                                },
                                error: function () {
                                    layer.close(load);
                                    layer.msg("录入失败！", {icon: 2});
                                }
                            });
                        }
                    }
                    ,btn2: function(index, layero){
                        var data = table.cache.accessoryDataTable;
                        data.push({
                            "accessoryName": ""
                            ,"specification": ""
                            ,"accessoryUnit": ""
                            ,"accessoryType": ""
                            ,"pieceUsage": ""
                            ,"orderPieceUsage": ""
                            ,"colorName": ""
                            ,"sizeName": ""
                            ,"supplier": ""
                            ,"accessoryAdd": "0"
                            ,"colorChild":"0"
                            ,"sizeChild":"0"
                        });
                        table.reload("accessoryDataTable",{
                            data:data   // 将新数据重新载入表格
                        });
                        return false  //开启该代码可禁止点击该按钮关闭
                    }
                    ,btn3: function(index, layero){
                        var readyOrderClothesList = [];
                        var sizeNameList = [];
                        var colorNameList = [];
                        layui.use(['table', 'soulTable'], function () {
                            var table = layui.table,
                                soulTable = layui.soulTable;
                            var index = layer.open({
                                type: 1 //Page层类型
                                , title: '制单数据信息'
                                , shade: 0 //遮罩透明度
                                , maxmin: false //允许全屏最小化
                                , offset: 'auto'
                                , anim: 0 //0-6的动画形式，-1不开启
                                , content: "<div id='OrderClothesDetailDataDiv'><table id='OrderClothesDetailDataTable' name = 'OrderClothesDetailDataTable'></table></div>"
                                , cancel: function (i, layero) {

                                }
                            });
                            setTimeout(function () {
                                $.ajax({
                                    url: "/erp/getorderclothesandcolorsizebyordername",
                                    type: 'GET',
                                    data: {
                                        orderName: orderName
                                    },
                                    success: function (res) {
                                        readyOrderClothesList = res.data;
                                        var orderClothesData = res.data;
                                        colorNameList = res.colorNameList;
                                        sizeNameList = res.sizeNameList;
                                        sizeNameList = globalSizeSort(sizeNameList);
                                        var title = [
                                            {field: 'color', title: '颜色/尺码',minWidth:100}
                                        ];
                                        $.each(sizeNameList,function(index,value){
                                            var field = {};
                                            field.field = value;
                                            field.title = value;
                                            field.minWidth = 80;
                                            field.totalRow = true;
                                            title.push(field);
                                        });
                                        title.push({field: 'colorSum', title: '小计',minWidth:100, totalRow:true});
                                        var colorSizeData = [];
                                        $.each(colorNameList,function(index,value){
                                            var tmp = {};
                                            tmp.color = value;
                                            var colorSum = 0;
                                            $.each(sizeNameList,function(index2,value2){
                                                $.each(orderClothesData,function(index3,value3){
                                                    if (value3.colorName === value && value3.sizeName === value2){
                                                        tmp[value2] = value3.count;
                                                        colorSum += value3.count;
                                                    }
                                                })
                                            });
                                            tmp["colorSum"] = colorSum;
                                            colorSizeData.push(tmp);
                                        });
                                        var OrderClothesDetailDataTable = table.render({
                                            elem: '#OrderClothesDetailDataTable'
                                            ,cols: [title]
                                            ,data: colorSizeData
                                            ,totalRow:true
                                            ,limit: 500 //每页默认显示的数量
                                            ,done:function () {
                                            }
                                        });
                                        return false;
                                    },
                                    error: function () {
                                        layer.msg("获取数据失败！", {icon: 2});
                                    }
                                })
                            }, 100);
                        });
                        return false  //开启该代码可禁止点击该按钮关闭
                    }
                });
                layer.full(index);
                $.ajax({
                    url: "/erp/getsizeandcolorbyorder",
                    type: 'GET',
                    data: {
                        orderName:checkStatus.data[0].orderName
                    },
                    success: function (res) {
                        thisSizeNameList = res.sizeNameList;
                        thisSizeNameList = globalSizeSort(thisSizeNameList);
                        thisColorNameList = res.colorNameList;
                        var title = [
                            {field: 'accessoryName', title: '名称',width:'12%',templet: function(d){
                                    return '<input class="layui-input" autocomplete="off" type="text" id="accessoryName-'+d.LAY_TABLE_INDEX+'"/>'
                                }},
                            {field: 'accessoryNumber', title: '编号',width:'10%',templet: function(d){
                                    return '<input class="layui-input" autocomplete="off" type="text" id="accessoryNumber-'+d.LAY_TABLE_INDEX+'"/>'
                                }},
                            {field: 'specification', title: '规格',align:'center',width:'6%',edit:'text'},
                            {field: 'accessoryColor', title: '颜色',align:'center',width:'6%',edit:'text'},
                            {field: 'accessoryUnit', title: '单位',align:'center',width:'6%',edit:'text'},
                            {field: 'accessoryType', title: '类别',align:'center',width:'6%',edit:'text'},
                            {field: 'supplier', title: '供应商',align:'center',width:'6%',edit:'text'},
                            {field: 'orderPieceUsage', title: '客供',align:'center',width:'6%',edit:'text'},
                            {field: 'pieceUsage', title: '单耗',align:'center',width:'6%',edit:'text'},
                            {field: 'accessoryAdd', title: '损耗',align:'center',width:'6%',edit:'text'},
                            {field: 'colorName', title: '色组',align:'center',width:'6%',templet: function(d){
                                    if (d.colorChild === '0'){
                                        return "<input type='checkbox' name='colorSwitch' lay-filter='colorSwitch' checked='checked' lay-skin='switch' lay-text='合并|分开' >";
                                    } else {
                                        return "<input type='checkbox' name='colorSwitch' lay-filter='colorSwitch' lay-skin='switch' lay-text='合并|分开' >";
                                    }
                                }},
                            {title: '分色属性',align:'center', width:'6%', templet: function(d){
                                    if (d.colorChild === '1'){
                                        return '<a class="layui-btn layui-btn-xs" lay-event="editColor">编辑</a>';
                                    } else {
                                        return '';
                                    }
                                }},
                            {field: 'sizeName', title: '尺码',align:'center',width:'6%',templet: function(d){
                                    if (d.sizeChild === '0'){
                                        return "<input type='checkbox' name='sizeSwitch' lay-filter='sizeSwitch' checked='checked' lay-skin='switch' lay-text='合并|分开' >";
                                    } else {
                                        return "<input type='checkbox' name='sizeSwitch' lay-filter='sizeSwitch' lay-skin='switch' lay-text='合并|分开' >";
                                    }
                                }},
                            {title: '分码属性',align:'center',width:'6%', templet: function(d){
                                    if (d.sizeChild === '1'){
                                        return '<a class="layui-btn layui-btn-xs" lay-event="editSize">编辑</a>';
                                    } else {
                                        return '';
                                    }
                                }},
                            {field: 'operation', title: '操作',width:'7%',align:'center', templet: function(d){
                                    if(d.LAY_INDEX == 1)
                                        return '<a class="layui-btn layui-btn-xs" lay-event="add"><i class="layui-icon layui-icon-addition" style="margin-right:0"></i></a>'
                                    else
                                        return '<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del"><i class="layui-icon layui-icon-subtraction" style="margin-right:0"></i></a>'
                                }}
                        ];
                        table.render({
                            elem: '#accessoryDataTable'
                            ,cols: [title]
                            ,height: 'full-200'
                            ,width: pageWidth
                            ,data: [
                                {
                                    "accessoryName": ""
                                    ,"accessoryNumber": ""
                                    ,"specification": ""
                                    ,"accessoryColor": ""
                                    ,"accessoryUnit": ""
                                    ,"supplier": ""
                                    ,"orderPieceUsage": ""
                                    ,"pieceUsage": ""
                                    ,"accessoryType":""
                                    ,"colorName": ""
                                    ,"sizeName": ""
                                    ,"accessoryAdd": "0"
                                    ,"colorChild":"0"
                                    ,"sizeChild":"0"
                                }
                            ]
                            ,limit:Number.MAX_VALUE
                            ,done:function (res) {
                                var data = table.cache.accessoryDataTable;
                                //渲染多选
                                $.each(res.data,function (index, item) {
                                    $("#accessoryName-"+item.LAY_TABLE_INDEX).parent().css("overflow","unset");
                                    $("#accessoryName-"+item.LAY_TABLE_INDEX).parent().css("height","auto");

                                    $("#accessoryNumber-"+item.LAY_TABLE_INDEX).parent().css("overflow","unset");
                                    $("#accessoryNumber-"+item.LAY_TABLE_INDEX).parent().css("height","auto");

                                    $("#accessoryName-"+item.LAY_TABLE_INDEX).val(item.accessoryName);

                                    $("#accessoryName-"+item.LAY_TABLE_INDEX).bind("input propertychange",function(event){
                                        data[item.LAY_TABLE_INDEX].accessoryName = $("#accessoryName-"+item.LAY_TABLE_INDEX).val();
                                    });

                                    $("#accessoryNumber-"+item.LAY_TABLE_INDEX).val(item.accessoryNumber);

                                    $("#accessoryNumber-"+item.LAY_TABLE_INDEX).bind("input propertychange",function(event){
                                        data[item.LAY_TABLE_INDEX].accessoryNumber = $("#accessoryNumber-"+item.LAY_TABLE_INDEX).val();
                                    });
                                    layui.yutons_sug.render({
                                        id: "accessoryName-" + item.LAY_TABLE_INDEX, //设置容器唯一id
                                        height: "300",
                                        width: "1000",
                                        limit:"50",
                                        limits:[10,20,50,100],
                                        cols: [
                                            [{
                                                field: 'accessoryName',
                                                title: '名称',
                                                width: '15%'
                                            }, {
                                                field: 'accessoryNumber',
                                                title: '编号',
                                                width: '15%'
                                            }, {
                                                field: 'specification',
                                                title: '规格',
                                                width: '10%'
                                            }, {
                                                field: 'accessoryColor',
                                                title: '颜色',
                                                width: '10%'
                                            }, {
                                                field: 'accessoryUnit',
                                                title: '单位',
                                                width: '10%'
                                            }, {
                                                field: 'accessoryType',
                                                title: '类别',
                                                width: '10%'
                                            }, {
                                                field: 'supplier',
                                                title: '供应商',
                                                width: '10%'
                                            }, {
                                                field: 'orderPieceUsage',
                                                title: '客供用量',
                                                width: '10%'
                                            }, {
                                                field: 'pieceUsage',
                                                title: '单件用量',
                                                width: '10%'
                                            }]
                                        ], //设置表头
                                        params: [
                                            {
                                                name: 'subAccessoryName',
                                                field: 'accessoryName'
                                            }],//设置字段映射，适用于输入一个字段，回显多个字段
                                        type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
                                        url: "/erp/getmanufactureaccessoryhintbyname?subAccessoryName=" //设置异步数据接口,url为必填项,params为字段名
                                    });

                                    layui.yutons_sug.render({
                                        id: "accessoryNumber-" + item.LAY_TABLE_INDEX, //设置容器唯一id
                                        height: "300",
                                        width: "1000",
                                        limit:"50",
                                        limits:[10,20,50,100],
                                        cols: [
                                            [{
                                                field: 'accessoryName',
                                                title: '名称',
                                                width: '20%'
                                            }, {
                                                field: 'accessoryNumber',
                                                title: '编号',
                                                width: '20%'
                                            }, {
                                                field: 'specification',
                                                title: '规格',
                                                width: '10%'
                                            }, {
                                                field: 'accessoryColor',
                                                title: '颜色',
                                                width: '10%'
                                            }, {
                                                field: 'accessoryUnit',
                                                title: '单位',
                                                width: '10%'
                                            }, {
                                                field: 'accessoryType',
                                                title: '类别',
                                                width: '10%'
                                            }, {
                                                field: 'supplier',
                                                title: '供应商',
                                                width: '10%'
                                            }, {
                                                field: 'orderPieceUsage',
                                                title: '客供用量',
                                                width: '10%'
                                            }, {
                                                field: 'pieceUsage',
                                                title: '单件用量',
                                                width: '10%'
                                            }]
                                        ], //设置表头
                                        params: [
                                            {
                                                name: 'subAccessoryNumber',
                                                field: 'accessoryNumber'
                                            }],//设置字段映射，适用于输入一个字段，回显多个字段
                                        type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
                                        url: "/erp/getmanufactureaccessoryhintbynumber?subAccessoryNumber=" //设置异步数据接口,url为必填项,params为字段名
                                    });
                                });
                                soulTable.render(this);
                                form.render();
                            }
                        });
                        layui.use(['yutons_sug'], function () {
                            layui.yutons_sug.render({
                                id: "clothesVersionNumberTwo", //设置容器唯一id
                                height: "300",
                                width: "550",
                                limit:"10",            limits:[10,20,50,100],
                                cols: [
                                    [{
                                        field: 'clothesVersionNumberTwo',
                                        title: '单号',
                                        align: 'left'
                                    }, {
                                        field: 'orderNameTwo',
                                        title: '款号',
                                        align: 'left'
                                    }]
                                ], //设置表头
                                params: [
                                    {
                                        name: 'clothesVersionNumberTwo',
                                        field: 'clothesVersionNumberTwo'
                                    }, {
                                        name: 'orderNameTwo',
                                        field: 'orderNameTwo'
                                    }],//设置字段映射，适用于输入一个字段，回显多个字段
                                type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
                                url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
                            });

                            layui.yutons_sug.render({
                                id: "orderNameTwo", //设置容器唯一id
                                height: "300",
                                width: "550",
                                limit:"10",            limits:[10,20,50,100],
                                cols: [
                                    [{
                                        field: 'clothesVersionNumberTwo',
                                        title: '单号',
                                        align: 'left'
                                    }, {
                                        field: 'orderNameTwo',
                                        title: '款号',
                                        align: 'left'
                                    }]
                                ], //设置表头
                                params: [
                                    {
                                        name: 'clothesVersionNumberTwo',
                                        field: 'clothesVersionNumberTwo'
                                    }, {
                                        name: 'orderNameTwo',
                                        field: 'orderNameTwo'
                                    }],//设置字段映射，适用于输入一个字段，回显多个字段
                                type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
                                url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
                            });
                        });

                        form.on('submit(searchCopyBeat)', function(data){
                            var initOrderName = $("#orderNameTwo").val();
                            $.ajax({
                                url: "/erp/getmanufactureaccessorykeydata",
                                type: 'GET',
                                data: {
                                    orderName: initOrderName
                                },
                                success: function (res) {
                                    var readyAccessoryData = res.data;
                                    var tableData = [];
                                    $.each(readyAccessoryData,function (index,item) {
                                        tableData.push({
                                            "accessoryName": item.accessoryName
                                            ,"accessoryNumber": item.accessoryNumber
                                            ,"specification": item.specification
                                            ,"accessoryColor": item.accessoryColor
                                            ,"accessoryUnit": item.accessoryUnit
                                            ,"supplier": item.supplier
                                            ,"orderPieceUsage": item.orderPieceUsage
                                            ,"pieceUsage": item.pieceUsage
                                            ,"accessoryType": item.accessoryType
                                            ,"colorName": ""
                                            ,"sizeName": ""
                                            ,"accessoryAdd": item.accessoryAdd
                                            ,"colorChild":"0"
                                            ,"sizeChild":"0"
                                        });
                                    });

                                    table.reload("accessoryDataTable",{
                                        data:tableData   // 将新数据重新载入表格
                                    });
                                    return false;
                                },
                                error: function () {
                                    layer.msg("获取数据失败！", {icon: 2});
                                }
                            });
                            return false;
                        });

                        //监听辅料信息按钮
                        table.on('tool(accessoryDataTable)', function(obj){
                            var data = table.cache.accessoryDataTable;
                            if(obj.event === 'del'){
                                obj.del();
                                $.each(data,function (index,item) {
                                    if(item instanceof Array) {
                                        data.splice(index,1)
                                    }
                                });
                                table.reload("accessoryDataTable",{
                                    data:data   // 将新数据重新载入表格
                                })
                            } else if(obj.event === 'add'){
                                data.push({
                                    "accessoryName": ""
                                    ,"accessoryNumber": ""
                                    ,"specification": ""
                                    ,"accessoryColor": ""
                                    ,"accessoryUnit": ""
                                    ,"supplier": ""
                                    ,"orderPieceUsage": ""
                                    ,"pieceUsage": ""
                                    ,"accessoryType":""
                                    ,"colorName": ""
                                    ,"sizeName": ""
                                    ,"accessoryAdd": "0"
                                    ,"colorChild":"0"
                                    ,"sizeChild":"0"
                                });
                                table.reload("accessoryDataTable",{
                                    data:data   // 将新数据重新载入表格
                                })
                            } else if(obj.event === 'editColor'){
                                var index = $(this).parents('tr').data('index');
                                var colorEditId = 'colorEdit' + index;
                                var colorEditHtml = "<div id="+ colorEditId +" class='layui-row'><table>";
                                $.each(thisColorNameList,function (index, item) {
                                    var checkValue = item + "_check";
                                    colorEditHtml += "<tr><td style='text-align: right margin-bottom: 15px; padding-left: 15px'><input type='checkbox' name='"+ checkValue +"' value='"+ checkValue +"'></td><td style='text-align: right margin-bottom: 15px'><label class='layui-form-label' style='width:100px'>" + item + "</label></td><td style='margin-bottom: 15px;'><input type='text' name='"+ item +"' autocomplete='off' class='layui-input'></td></tr>";
                                });
                                colorEditHtml += "</table></div>";
                                var index = layer.open({
                                    type: 1 //Page层类型
                                    , title: "<div style='color: #00c7f7; font-weight: bolder; font-size: large'>编辑(左侧选择框在部分颜色合并时进行选择,完全分色请不要选择)</div>"
                                    , btn: ['保存','清空']
                                    , shade: 0.6 //遮罩透明度
                                    , maxmin: false //允许全屏最小化
                                    , anim: 0 //0-6的动画形式，-1不开启
                                    , area: ['600px', '500px']
                                    , content: colorEditHtml
                                    , yes: function (index, layero) {
                                        var tmpInput = {};
                                        $.each(thisColorNameList,function (i, color) {
                                            var tmpInputName = "#" + colorEditId + " input[name='" + color + "']";
                                            tmpInput[color] = $(tmpInputName).val();
                                        });
                                        colorProperty[colorEditId] = tmpInput;
                                        var checkDivName = "#" + colorEditId + " input[type='checkBox']:checked";
                                        var checkList = [];
                                        $(checkDivName).each(function () {
                                            var checkedValue = $(this).val();
                                            var location = checkedValue.lastIndexOf("_");
                                            checkList.push(checkedValue.substring(0, location));
                                        });
                                        if (checkList.length > 0){
                                            colorCheckProperty[colorEditId] = checkList;
                                        }
                                        layer.close(index);
                                    }
                                    , btn2: function (index, layero) {
                                        $.each(thisColorNameList,function (i, color) {
                                            var tmpInputName = "#" + colorEditId + " input[name='" + color + "']";
                                            $(tmpInputName).val('');
                                        });
                                        return false;
                                    }
                                    , cancel: function (i, layero) {
                                        var tmpInput = {};
                                        $.each(thisColorNameList,function (i, color) {
                                            var tmpInputName = "#" + colorEditId + " input[name='" + color + "']";
                                            tmpInput[color] = $(tmpInputName).val();
                                        });
                                        colorProperty[colorEditId] = tmpInput;
                                        var checkDivName = "#" + colorEditId + " input[type='checkBox']:checked";
                                        var checkList = [];
                                        $(checkDivName).each(function () {
                                            var checkedValue = $(this).val();
                                            var location = checkedValue.lastIndexOf("_");
                                            checkList.push(checkedValue.substring(0, location));
                                        });
                                        if (checkList.length > 0){
                                            colorCheckProperty[colorEditId] = checkList;
                                        }
                                    }
                                });

                                setTimeout(function () {

                                    $.each(thisColorNameList,function (i, color) {
                                        var tmpInputName = "#" + colorEditId + " input[name='" + color + "']";
                                        $(tmpInputName).keydown(function (event) {
                                            if(event.keyCode==38){
                                                var name = $(obj).attr("name");
                                                var colorLength = thisColorNameList.length;
                                                var lastIndex = i == 0 ? colorLength - 1 : i - 1;
                                                var tmpCheckName = "#" + colorEditId + " input[name='" + thisColorNameList[lastIndex] + "']";
                                                $(tmpCheckName).focus();
                                            }else if(event.keyCode==40){
                                                var name = $(obj).attr("name");
                                                var colorLength = thisColorNameList.length;
                                                var nextIndex = i == colorLength - 1 ? 0 : i + 1;
                                                var tmpCheckName = "#" + colorEditId + " input[name='" + thisColorNameList[nextIndex] + "']";
                                                $(tmpCheckName).focus();
                                            }
                                        });
                                    });

                                    if (colorProperty[colorEditId]){
                                        var thisInputValue = colorProperty[colorEditId];
                                        $.each(thisColorNameList,function (i, color) {
                                            var tmpInputName = "#" + colorEditId + " input[name='" + color + "']";
                                            $(tmpInputName).val(thisInputValue[color]);
                                        });
                                    }
                                    if (colorCheckProperty[colorEditId]){
                                        var checkList = colorCheckProperty[colorEditId];
                                        if (checkList){
                                            $.each(checkList,function (i, color) {
                                                var checkBoxName = color + "_check";
                                                var tmpCheckName = "#" + colorEditId + " input[name='" + checkBoxName + "']:checkbox";
                                                $(tmpCheckName).attr("checked", true);
                                            });
                                        }

                                    }

                                }, 100);
                            } else if(obj.event === 'editSize'){
                                var index = $(this).parents('tr').data('index');
                                var sizeEditId = 'sizeEdit' + index;
                                var sizeEditHtml = "<div id="+ sizeEditId +" class='layui-row'><table>";
                                $.each(thisSizeNameList,function (index, item) {
                                    var checkValue = item + "_check";
                                    sizeEditHtml += "<tr><td style='text-align: right margin-bottom: 15px; padding-left: 15px'><input type='checkbox' name='"+ checkValue +"' value='"+ checkValue +"'></td><td style='text-align: right margin-bottom: 15px'><label class='layui-form-label' style='width:100px'>" + item + "</label></td><td style='margin-bottom: 15px;'><input type='text' name='"+ item +"' autocomplete='off' class='layui-input' onkeydown='moveCursorSize(event,this)'></td></tr>";
                                });
                                sizeEditHtml += "</table></div>";
                                var index = layer.open({
                                    type: 1 //Page层类型
                                    , title: "<div style='color: #00c7f7; font-weight: bolder; font-size: large'>编辑(左侧选择框在部分尺码合并时进行选择,完全分码请不要选择)</div>"
                                    , btn: ['保存','清空']
                                    , shade: 0.6 //遮罩透明度
                                    , maxmin: false //允许全屏最小化
                                    , anim: 0 //0-6的动画形式，-1不开启
                                    , area: ['600px', '500px']
                                    , content: sizeEditHtml
                                    , yes: function (index, layero) {
                                        var tmpInput = {};
                                        $.each(thisSizeNameList,function (i, size) {
                                            var tmpInputName = "#" + sizeEditId + " input[name='" + size + "']";
                                            tmpInput[size] = $(tmpInputName).val();
                                        });
                                        sizeProperty[sizeEditId] = tmpInput;
                                        var checkDivName = "#" + sizeEditId + " input[type='checkBox']:checked";
                                        var checkList = [];
                                        $(checkDivName).each(function () {
                                            var checkedValue = $(this).val();
                                            var location = checkedValue.lastIndexOf("_");
                                            checkList.push(checkedValue.substring(0, location));
                                        });
                                        if (checkList.length > 0){
                                            sizeCheckProperty[sizeEditId] = checkList;
                                        }
                                        layer.close(index);
                                    }
                                    , btn2: function (index, layero) {
                                        $.each(thisSizeNameList,function (i, size) {
                                            var tmpInputName = "#" + sizeEditId + " input[name='" + size + "']";
                                            $(tmpInputName).val('');
                                        });
                                        return false;
                                    }
                                    , cancel: function (i, layero) {
                                        var tmpInput = {};
                                        $.each(thisSizeNameList,function (i, size) {
                                            var tmpInputName = "#" + sizeEditId + " input[name='" + size + "']";
                                            tmpInput[size] = $(tmpInputName).val();
                                        });
                                        sizeProperty[sizeEditId] = tmpInput;
                                        var checkDivName = "#" + sizeEditId + " input[type='checkBox']:checked";
                                        var checkList = [];
                                        $(checkDivName).each(function () {
                                            var checkedValue = $(this).val();
                                            var location = checkedValue.lastIndexOf("_");
                                            checkList.push(checkedValue.substring(0, location));
                                        });
                                        if (checkList.length > 0){
                                            sizeCheckProperty[sizeEditId] = checkList;
                                        }
                                    }
                                });
                                setTimeout(function () {
                                    $.each(thisSizeNameList,function (i, size) {
                                        var tmpInputName = "#" + sizeEditId + " input[name='" + size + "']";
                                        $(tmpInputName).keydown(function (event) {
                                            if(event.keyCode==38){
                                                var name = $(obj).attr("name");
                                                var sizeLength = thisSizeNameList.length;
                                                var lastIndex = i == 0 ? sizeLength - 1 : i - 1;
                                                var tmpCheckName = "#" + sizeEditId + " input[name='" + thisSizeNameList[lastIndex] + "']";
                                                $(tmpCheckName).focus();
                                            }else if(event.keyCode==40){
                                                var name = $(obj).attr("name");
                                                var sizeLength = thisSizeNameList.length;
                                                var nextIndex = i == sizeLength - 1 ? 0 : i + 1;
                                                var tmpCheckName = "#" + sizeEditId + " input[name='" + thisSizeNameList[nextIndex] + "']";
                                                $(tmpCheckName).focus();
                                            }
                                        });
                                    });

                                    if (sizeProperty[sizeEditId]){
                                        var thisInputValue = sizeProperty[sizeEditId];
                                        $.each(thisSizeNameList,function (i, size) {
                                            var tmpInputName = "#" + sizeEditId + " input[name='" + size + "']";
                                            $(tmpInputName).val(thisInputValue[size]);
                                        });
                                    }
                                    if (sizeCheckProperty[sizeEditId]){
                                        var checkList = sizeCheckProperty[sizeEditId];
                                        if (checkList){
                                            $.each(checkList,function (i, size) {
                                                var checkBoxName = size + "_check";
                                                var tmpCheckName = "#" + sizeEditId + " input[name='" + checkBoxName + "']:checkbox";
                                                $(tmpCheckName).attr("checked", true);
                                            });
                                        }

                                    }
                                }, 100);
                            }
                        });
                    },
                    error: function () {
                        layer.msg("获取色组、尺码信息失败！", {icon: 2});
                    }
                })
            }
        }
        else if (obj.event === "addFabricData") { //录入面料信息
            var checkStatus = table.checkStatus(obj.config.id); //获取选中行状态
            if(checkStatus.data.length == 0) {
                layer.msg('请先选择订单');
            }
            else {
                var colorList = [];
                var sizeList = [];
                var orderClothesList = [];
                var orderName = checkStatus.data[0].orderName;
                var clothesVersionNumber = checkStatus.data[0].clothesVersionNumber;
                var label = "<p style='color: red; font-weight: bold'>录入面料信息&emsp;&emsp;&emsp;&emsp;" + "单号："+clothesVersionNumber+"&emsp;&emsp;&emsp;&emsp;款号："+orderName + "&emsp;&emsp;&emsp;&emsp;除了面料号其他都必填&emsp;&emsp;&emsp;&emsp;" + "如果某个色组没有用到该面料,颜色输入框为空即可</p>";
                var index = layer.open({
                    type: 1 //Page层类型
                    , title: label
                    , btn: ['保存']
                    , shade: 0.6 //遮罩透明度
                    , area: '1000px'
                    , maxmin: false //允许全屏最小化
                    , anim: 0 //0-6的动画形式，-1不开启
                    , content: "<div id=\"addFabricData\">\n" +
                        "        <form class=\"layui-form\" style=\"margin: 0 auto;max-width:100%;padding-top: 5px;\">\n" +
                        "            <table class=\"layui-hide\" id=\"fabricDataTable\" lay-filter=\"fabricDataTable\"></table>\n" +
                        "            <table class=\"layui-hide\" id=\"bjDataTable\" lay-filter=\"bjDataTable\"></table>\n" +
                        "        </form>\n" +
                        "    </div>"
                    ,yes: function(i, layero){
                        var fabricData = table.cache.fabricDataTable;
                        var bjData = table.cache.bjDataTable;
                        var manufactureFabrics = [];
                        var isInput = true;
                        var pieceFlag = true;
                        var orderPieceFlag = true;
                        var lossFlag = true;
                        $.each(fabricData, function (i, item) {
                            if (item.fabricName == null || item.fabricWidth == null ||
                                item.fabricWeight == null || item.partName == null ||
                                item.orderPieceUsage == null || item.pieceUsage == null ||
                                item.fabricName === '' || item.fabricWidth === '' ||
                                item.fabricWeight === '' || item.partName === '' ||
                                item.orderPieceUsage === '' || item.pieceUsage === '') {
                                isInput = false;
                                return false;
                            }
                            var orderPieceUsage = 0;
                            var pieceUsage = 0;
                            if (item.orderPieceUsage !== ''){
                                if (Number(item.orderPieceUsage)  > 0){
                                    orderPieceUsage = Number(item.orderPieceUsage);
                                }
                            }
                            if (item.pieceUsage !==''){
                                if (Number(item.pieceUsage)){
                                    pieceUsage = Number(item.pieceUsage);
                                }
                            }
                            $.each(colorList,function(c_index, c_item) {
                                var manufactureFabric = {};
                                manufactureFabric.orderName = orderName;
                                manufactureFabric.clothesVersionNumber = clothesVersionNumber;
                                manufactureFabric.fabricNumber = item.fabricNumber;
                                manufactureFabric.fabricName = item.fabricName;
                                manufactureFabric.supplier = item.supplier;
                                manufactureFabric.fabricWidth = item.fabricWidth;
                                manufactureFabric.unit = item.units[0].val();
                                manufactureFabric.fabricWeight = item.fabricWeight;
                                manufactureFabric.partName = item.partName;
                                if (item.fabricAdd == null || item.fabricAdd === ""){
                                    manufactureFabric.fabricAdd = 0;
                                }else {
                                    if (!isNumber(item.fabricAdd)){
                                        lossFlag = false;
                                        return false;
                                    } else {
                                        manufactureFabric.fabricAdd = item.fabricAdd;
                                    }
                                }
                                manufactureFabric.orderPieceUsage = orderPieceUsage;
                                manufactureFabric.pieceUsage = pieceUsage;
                                manufactureFabric.colorName = c_item;
                                manufactureFabric.isHit = "";
                                manufactureFabric.fabricColor = item[c_item];
                                manufactureFabric.fabricColorNumber = "";
                                manufactureFabric.orderState = '未下单';
                                manufactureFabric.checkState = '未提交';
                                if (manufactureFabric.fabricColor != null && manufactureFabric.fabricColor !== ''){
                                    manufactureFabrics.push(manufactureFabric);
                                }
                            })
                        });
                        //  检测扁机数据
                        var manufactureAccessoryList = [];
                        $.each(bjData, function (b_index, b_item){
                            if (b_item.accessoryName != null && b_item.accessoryName !== ''
                                && b_item.pieceUsage != null && Number(b_item.pieceUsage) > 0){
                                b_item.orderState = '未下单';
                                b_item.checkState = '未提交';
                                b_item.accessoryType = '扁机';
                                b_item.batchOrder = 0;
                                b_item.colorChild = 0;
                                b_item.sizeChild = 1;
                                b_item.sizeName = '通用';
                                b_item.accessoryKey = b_item.hideKey;
                                b_item.colorProperty = b_item.accessoryColor;
                                b_item.sizeProperty = b_item.specification;
                                b_item.accessoryCount = Number(b_item.pieceUsage) * Number(b_item.orderCount);
                                manufactureAccessoryList.push(b_item);
                            }
                        });
                        if (manufactureFabrics.length === 0 && manufactureAccessoryList.length === 0){
                            layer.msg("数据不能全部为空~~~~");
                            return false;
                        }
                        var param = {};
                        param.orderName = orderName;
                        if (manufactureFabrics.length > 0){
                            param.manufactureFabricJson = JSON.stringify(manufactureFabrics);
                        }
                        if (manufactureAccessoryList.length > 0){
                            param.manufactureAccessoryJson = JSON.stringify(manufactureAccessoryList);
                        }
                        if (manufactureFabrics.length > 0){
                            if(!isInput) {
                                layer.msg("请输入完所有内容(除了面料号其他都要填写)", { icon: 2 });
                                return false;
                            } else if(!pieceFlag) {
                                layer.msg("单件用量有误", { icon: 2 });
                                return false;
                            } else if(!orderPieceFlag) {
                                layer.msg("接单用量有误", { icon: 2 });
                                return false;
                            } else if(!lossFlag) {
                                layer.msg("损耗有误", { icon: 2 });
                                return false;
                            }
                        }
                        $.ajax({
                            url: "/erp/addmanufacturefabricbatch",
                            type: 'POST',
                            data: param,
                            success: function (res) {
                                if (res.result == 0) {
                                    initTable('', '', '', 0);
                                    layer.close(index);
                                    layer.msg("录入成功！", {icon: 1});
                                } else {
                                    layer.msg("录入失败！", {icon: 2});
                                }
                            },
                            error: function () {
                                layer.msg("录入失败！", {icon: 2});
                            }
                        })
                    }
                    ,btn2: function(index, layero){
                        var data = table.cache.fabricDataTable;
                        data.push({
                            "fabricName": ""
                        });
                        table.reload("fabricDataTable",{
                            data:data   // 将新数据重新载入表格
                        });
                        return false  //开启该代码可禁止点击该按钮关闭
                    }
                });
                layer.full(index);
                setTimeout(function (){
                    var bjReportData = [];
                    var orderCount = checkStatus.data[0].orderSum;
                    $.ajax({
                        url: "/erp/getorderclothesandcolorsizebyordername",
                        type: 'GET',
                        async:false,
                        data: {
                            orderName:orderName
                        },
                        success: function (res) {
                            colorList = res.colorNameList;
                            sizeList = res.sizeNameList;
                            orderClothesList = res.data;
                        },
                        error: function () {
                            layer.msg("获取订单颜色失败！", {icon: 2});
                        }
                    })
                    var title = [
                        {field: 'fabricName', title: '面料名称',align:'center',minWidth:220,templet: function(d){
                                return '<input class="layui-input" autocomplete="off" type="text" id="fabricName-'+d.LAY_TABLE_INDEX+'"/>'
                            }},
                        {field: 'fabricNumber', title: '面料号',align:'center',minWidth:120,edit:'text'},
                        {field: 'supplier', title: '供应商',align:'center',minWidth:90,edit:'text'},
                        {field: 'fabricWidth', title: '布封',align:'center',minWidth:90,edit:'text'},
                        {field: 'fabricWeight', title: '克重',align:'center',minWidth:90,edit:'text'},
                        {field: 'partName', title: '部位',align:'center',minWidth:120,edit:'text'},
                        {field: 'fabricAdd', title: '损耗(%)',align:'center',minWidth:90,edit:'text'},
                        {field: 'orderPieceUsage', title: '接单用量',align:'center',minWidth:100,edit:'text'},
                        {field: 'pieceUsage', title: '单件用量',align:'center',minWidth:100,edit:'text'},
                        {field: 'unit', title: '单位',align:'center',minWidth:90, templet: function (d) {
                                return '<select name="unit" class="layui-input">' +
                                    '         <option value="KG">KG</option>' +
                                    '         <option value="磅">磅</option>' +
                                    '         <option value="米">米</option>' +
                                    '         <option value="码">码</option>' +
                                    '    </select>';
                            }},
                    ];
                    var headLine = {fabricName: ""};
                    $.each(colorList,function (index, item) {
                        headLine[item] = item;
                        title.push({field:item,title:item,align:'center',minWidth:100,edit:'text'})
                    });
                    title.push( {field: 'operation', title: '操作',minWidth:90,align:'center' ,templet: function(d){
                            return '<a class="layui-btn layui-btn-xs" lay-event="add"><i class="layui-icon layui-icon-addition" style="margin-right:0"></i></a><a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del"><i class="layui-icon layui-icon-subtraction" style="margin-right:0"></i></a>';
                        }});
                    table.render({
                        elem: '#fabricDataTable'
                        ,cols: [title]
                        ,height: '350'
                        ,width: pageWidth
                        ,data: [headLine]
                        ,limit:Number.MAX_VALUE
                        ,done:function (res) {
                            var data = table.cache.fabricDataTable;
                            $.each(res.data,function (index,item) {
                                $("#fabricName-"+item.LAY_TABLE_INDEX).parent().css("overflow","unset");
                                $("#fabricName-"+item.LAY_TABLE_INDEX).parent().css("height","auto");
                                $("#fabricName-"+item.LAY_TABLE_INDEX).val(item.fabricName);
                                $("#fabricName-"+item.LAY_TABLE_INDEX).bind("input propertychange",function(event){
                                    data[item.LAY_TABLE_INDEX].fabricName = $("#fabricName-"+item.LAY_TABLE_INDEX).val();
                                });
                                layui.yutons_sug.render({
                                    id: "fabricName-" + item.LAY_TABLE_INDEX, //设置容器唯一id
                                    height: "300",
                                    width: "1400",
                                    limit:"10",
                                    limits:[10,20,50,100],
                                    cols: [
                                        [{
                                            field: 'fabricName',
                                            title: '名称',
                                            width: '15%'
                                        }, {
                                            field: 'fabricNumber',
                                            title: '面料号',
                                            width: '15%'
                                        }, {
                                            field: 'supplier',
                                            title: '供应商',
                                            width: '10%'
                                        }, {
                                            field: 'fabricWidth',
                                            title: '布封',
                                            width: '10%'
                                        }, {
                                            field: 'fabricWeight',
                                            title: '克重',
                                            width: '10%'
                                        }, {
                                            field: 'partName',
                                            title: '部位',
                                            width: '10%'
                                        }, {
                                            field: 'fabricAdd',
                                            title: '损耗(%)',
                                            width: '10%'
                                        }, {
                                            field: 'orderPieceUsage',
                                            title: '接单用量',
                                            width: '10%'
                                        }, {
                                            field: 'pieceUsage',
                                            title: '单位用量',
                                            width: '10%'
                                        }]
                                    ], //设置表头
                                    params: [
                                        {
                                            name: 'subFabricName',
                                            field: 'fabricName'
                                        }],//设置字段映射，适用于输入一个字段，回显多个字段
                                    type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
                                    url: "/erp/getmanufacturefabrichintbyfabricname?subFabricName=" //设置异步数据接口,url为必填项,params为字段名
                                });
                            });
                            // 选择操作
                            $.each(data,function (index,item) {
                                $("select[name='unit']").parent().css("overflow","unset");
                                $("select[name='unit']").parent().css("height","auto");
                                if(data[index].unit) {
                                    $("div[lay-id='fabricDataTable'] tr[data-index=" + index + "] select[name='unit']").val(data[index].unit);
                                }
                                var unitSelects = $("div[lay-id='fabricDataTable'] tr[data-index="+index+"] select[name='unit']");
                                var units = [];
                                $.each(unitSelects,function (s_index,s_item) {
                                    units.push(unitSelects.eq(s_index));
                                });
                                item.units = units;
                            });
                            soulTable.render(this);
                            form.render();
                        }
                    });
                    //监听面料信息按钮
                    table.on('tool(fabricDataTable)', function(obj){
                        var data = table.cache.fabricDataTable;
                        if(obj.event === 'del'){
                            if (data.length > 1){
                                obj.del();
                                $.each(data,function (index,item) {
                                    if(item instanceof Array) {
                                        data.splice(index,1);
                                    }
                                })
                                table.reload("fabricDataTable",{
                                    data:data   // 将新数据重新载入表格
                                })
                            }
                        }
                        else if(obj.event === 'add') {
                            var headLine = {fabricName: ""};
                            $.each(colorList,function (index, item) {
                                headLine[item] = item;
                            });
                            data.push(headLine);
                            table.reload("fabricDataTable", {
                                data: data   // 将新数据重新载入表格
                            });
                        }
                    });

                    var bjTitle = [
                        {field: 'clothesVersionNumber',hide: true},
                        {field: 'orderName',hide: true},
                        {field: 'keyName', title: '次序', align: 'center', minWidth: 100},
                        {field: 'colorName', title: '单色', align: 'center', minWidth: 120},
                        {field: 'accessoryName', title: '名称', align: 'center', minWidth: 120, edit: 'text'},
                        {field: 'specification', title: '规格', align: 'center', minWidth: 100, edit: 'text'},
                        {field: 'accessoryColor', title: '物色', align: 'center', minWidth: 100, edit: 'text'},
                        {field: 'partName', title: '部位', align: 'center', minWidth: 120, edit: 'text'},
                        {field: 'accessoryUnit', title: '单位', align: 'center', minWidth: 100, edit: 'text'},
                        {field: 'pieceUsage', title: '单件用量', align: 'center', minWidth: 100,edit:'text'},
                        {field: 'remark', title: '备注', align: 'center', minWidth: 100,edit:'text'},
                        {field: 'orderCount', title: '订单量', align: 'center', minWidth: 100},
                        {field: 'accessoryID', hide: true},
                        {field: 'hideKey', hide: true}
                    ];
                    $.each(colorList, function (c_index, c_item){
                        var colorCount = 0;
                        $.each(orderClothesList, function (o_index, o_item){
                            if (o_item.colorName === c_item){
                                colorCount += o_item.count;
                            }
                        });
                        bjReportData.push({
                            clothesVersionNumber: clothesVersionNumber, orderName: orderName, partName: '',
                            accessoryName: '', specification: '', accessoryColor: '', accessoryUnit:'条',
                            orderCount: colorCount, pieceUsage: '', hideKey: 0,
                            keyName: '第1组', colorName: c_item
                        })
                    });
                    table.render({
                        elem: '#bjDataTable'
                        ,cols: [bjTitle]
                        ,height: 'full-500'
                        ,width: pageWidth
                        ,toolbar: '#toolbarTopFabric' //开启头部工具栏，并为其绑定左侧模板
                        ,data: bjReportData
                        ,limit:Number.MAX_VALUE
                        ,done:function (res) {}
                    });
                    table.on('toolbar(bjDataTable)', function(obj){
                        if (obj.event === "addGroup"){
                            var fabricData = table.cache.bjDataTable;
                            var hideKey = 0;
                            $.each(fabricData,function (f_index, f_item) {
                                hideKey = f_item.hideKey > hideKey ? f_item.hideKey:hideKey;
                            })
                            hideKey ++;
                            $.each(colorList, function (c_index, c_item){
                                var colorCount = 0;
                                $.each(orderClothesList, function (o_index, o_item){
                                    if (o_item.colorName === c_item){
                                        colorCount += o_item.count;
                                    }
                                });
                                fabricData.push({
                                    clothesVersionNumber: clothesVersionNumber, orderName: orderName, partName: '',
                                    accessoryName: '', specification: '', accessoryColor: '', accessoryUnit:'条',
                                    orderCount: colorCount, pieceUsage: '', hideKey: hideKey,
                                    keyName: '第' + (hideKey + 1) + '组', colorName: c_item
                                })
                            });
                            table.reload("bjDataTable",{
                                data:fabricData   // 将新数据重新载入表格
                            })
                            return false;
                        }
                        else if (obj.event === "minusGroup"){
                            var fabricData = table.cache.bjDataTable;
                            var hideKey = 0;
                            $.each(fabricData,function (f_index, f_item) {
                                hideKey = f_item.hideKey > hideKey ? f_item.hideKey:hideKey;
                            })
                            var fabricList = [];
                            $.each(fabricData,function (f_index, f_item) {
                                if (f_item.hideKey < hideKey){
                                    fabricList.push(f_item);
                                }
                            })
                            if (fabricList.length > 0){
                                table.reload("bjDataTable",{
                                    data:fabricList   // 将新数据重新载入表格
                                })
                            }
                            return false;
                        }
                    });
                }, 1000);
            }
        }
        else if (obj.event === "copyOrder") {   //录入工艺信息
            var checkStatus = table.checkStatus(obj.config.id); //获取选中行状态
            if (checkStatus.data.length == 0) {
                layer.msg('请先选择订单');
            } else {
                var orderName = checkStatus.data[0].orderName;
                var clothesVersionNumber = checkStatus.data[0].clothesVersionNumber;
                basicOrderInfo(1, orderName, clothesVersionNumber);
            }
        }
        else if (obj.event === "addColorData") {   //录入工艺信息
            var checkStatus = table.checkStatus(obj.config.id); //获取选中行状态
            if (checkStatus.data.length == 0) {
                layer.msg('请先选择订单');
            } else {
                var orderName = checkStatus.data[0].orderName;
                var clothesVersionNumber = checkStatus.data[0].clothesVersionNumber;
                var index = layer.open({
                    type: 1 //Page层类型
                    , title: '添加颜色'
                    , btn: ['保存']
                    , shade: 0.6 //遮罩透明度
                    , anim: 0 //0-6的动画形式，-1不开启
                    , content: '<div style="padding-top: 20px;margin-bottom: 15px"><label class="control-label" style="margin-left: 20px; margin-right: 20px">颜色名称</label><input id="addColorData" class="form-control" placeholder="请输入颜色" autocomplete="off" style="width: 200px;"></div><table id="addColorDataTable" lay-filter="addColorDataTable"></table>'
                    ,yes: function(index, layero){
                        var orderClothesList = [];
                        var commitAddColorData = table.cache.addColorDataTable;
                        if ($("#addColorData").val() == null || $("#addColorData").val() == ''){
                            layer.msg('颜色输入不能为空');
                            return false;
                        }
                        $.each(commitAddColorData,function (index,item) {
                            orderClothesList.push({orderName: orderName, clothesVersionNumber: clothesVersionNumber, sizeName: item.sizeName, count: item.count});
                        });
                        $.ajax({
                            url: "/erp/addadditionalcolorname",
                            type: 'POST',
                            data: {
                                orderName: orderName,
                                colorName: $("#addColorData").val(),
                                orderClothesListJson:JSON.stringify(orderClothesList)
                            },
                            success: function (res) {
                                if (res == 0) {
                                    layer.close(index);
                                    layer.msg("添加成功！", {icon: 1});
                                    initTable('', '', '', 0);
                                } else {
                                    layer.msg("添加失败！", {icon: 2});
                                }
                            }, error: function () {
                                layer.msg("出库失败！", {icon: 2});
                            }
                        })
                    }, cancel : function (i,layero) {
                    }
                });
                layer.full(index);
                setTimeout(function () {
                    $.ajax({
                        url: "/erp/getordersizenamesbyorder",
                        type: 'GET',
                        data: {
                            orderName:orderName
                        },
                        success: function (res) {
                            var title = [
                                {field: 'sizeName', title: '尺码',align:'center',minWidth:220},
                                {field: 'count', title: '数量',align:'center',minWidth:220,edit:'text'}
                            ];
                            var addColorSizeData = [];
                            var tmpSizeNameList = res.sizeNameList;
                            tmpSizeNameList = globalSizeSort(tmpSizeNameList);
                            $.each(tmpSizeNameList,function (index, item) {
                                addColorSizeData.push({sizeName: item, count: 0})
                            });
                            table.render({
                                elem: '#addColorDataTable'
                                ,cols: [title]
                                ,height: 'full-200'
                                ,data: addColorSizeData
                                ,limit:Number.MAX_VALUE
                                ,done:function (res) {}
                            });
                        }, error: function () {
                            layer.msg("获取订单尺码失败！", {icon: 2});
                        }
                    })
                }, 100);
            }
        }
        else if (obj.event === "addSizeData") {   //录入工艺信息
            var checkStatus = table.checkStatus(obj.config.id); //获取选中行状态
            if (checkStatus.data.length == 0) {
                layer.msg('请先选择订单');
            } else {
                var orderName = checkStatus.data[0].orderName;
                var clothesVersionNumber = checkStatus.data[0].clothesVersionNumber;
                var index = layer.open({
                    type: 1 //Page层类型
                    , title: '添加尺码'
                    , btn: ['保存']
                    , shade: 0.6 //遮罩透明度
                    , anim: 0 //0-6的动画形式，-1不开启
                    , content: '<div style="padding-top: 20px;margin-bottom: 15px"><label class="control-label" style="margin-left: 20px; margin-right: 20px">尺码名称</label><select id="addSizeData" class="form-control" autocomplete="off" style="width: 200px;"></select></div><table id="addSizeDataTable" lay-filter="addSizeDataTable"></table>'
                    ,yes: function(index, layero){
                        var orderClothesList = [];
                        var commitAddSizeData = table.cache.addSizeDataTable;
                        if ($("#addSizeData").val() == null || $("#addSizeData").val() == ''){
                            layer.msg('尺码选择不能为空');
                            return false;
                        }
                        $.each(commitAddSizeData,function (index,item) {
                            orderClothesList.push({orderName: orderName, clothesVersionNumber: clothesVersionNumber, colorName: item.colorName, count: item.count});
                        });
                        $.ajax({
                            url: "/erp/addadditionalsizename",
                            type: 'POST',
                            data: {
                                orderName: orderName,
                                sizeName: $("#addSizeData").val(),
                                orderClothesListJson:JSON.stringify(orderClothesList)
                            },
                            success: function (res) {
                                if (res == 0) {
                                    layer.close(index);
                                    layer.msg("添加成功！", {icon: 1});
                                    initTable('', '', '', 0);
                                } else {
                                    layer.msg("添加失败！", {icon: 2});
                                }
                            }, error: function () {
                                layer.msg("出库失败！", {icon: 2});
                            }
                        })
                    }, cancel : function (i,layero) {
                    }
                });
                layer.full(index);
                setTimeout(function () {
                    $.ajax({
                        url: "/erp/getordercolornamesbyorder",
                        type: 'GET',
                        data: {
                            orderName:orderName
                        },
                        success: function (res) {
                            var title = [
                                {field: 'colorName', title: '颜色',align:'center',minWidth:220},
                                {field: 'count', title: '数量',align:'center',minWidth:220,edit:'text'}
                            ];
                            var addColorSizeData = [];
                            $.each(res.colorNameList,function (index, item) {
                                addColorSizeData.push({colorName: item, count: 0})
                            });
                            table.render({
                                elem: '#addSizeDataTable'
                                ,cols: [title]
                                ,height: 'full-200'
                                ,data: addColorSizeData
                                ,limit:Number.MAX_VALUE
                                ,done:function (res) {}
                            });
                        }, error: function () {
                            layer.msg("获取订单颜色失败！", {icon: 2});
                        }
                    });
                    $.ajax({
                        url: "/erp/getallsizenamefromsizemanage",
                        type: 'GET',
                        data: {},
                        success: function (res) {
                            $("#addSizeData").empty();
                            $("#addSizeData").append("<option value=''>选择尺码</option>");
                            $.each(res.sizeNameList, function(index,element){
                                $("#addSizeData").append("<option value='"+element+"'>"+element+"</option>");
                            })
                        }, error: function () {
                            layer.msg("获取尺码数据失败！", {icon: 2});
                        }
                    })
                }, 100);
            }
        }
        else if (obj.event === "normalOrder") {
            initTable('', '', '', 0);
        }
        else if (obj.event === "closedOrder") {
            initTable('', '', '', 1);
        }
        else if (obj.event === "addPrice") {
            var checkStatus = table.checkStatus(obj.config.id); //获取选中行状态
            if(checkStatus.data.length == 0) {
                layer.msg('请先选择订单');
            }else {
                var colorList = [];
                var orderName = checkStatus.data[0].orderName;
                var clothesVersionNumber = checkStatus.data[0].clothesVersionNumber;
                var label = "录入价格&emsp;&emsp;&emsp;&emsp;" + "单号："+clothesVersionNumber+"&emsp;&emsp;&emsp;&emsp;款号："+orderName ;
                var index = layer.open({
                    type: 1 //Page层类型
                    , title: label
                    , btn: ['保存','填充']
                    , shade: 0.6 //遮罩透明度
                    , area: '1000px'
                    , maxmin: false //允许全屏最小化
                    , anim: 0 //0-6的动画形式，-1不开启
                    , content: "<div id=\"addClothesPrice\">\n" +
                        "        <form class=\"layui-form\" style=\"margin: 0 auto;max-width:100%;padding-top: 40px;\">\n" +
                        "            <table class=\"layui-hide\" id=\"clothesPriceTable\" lay-filter=\"clothesPriceTable\"></table>\n" +
                        "        </form>\n" +
                        "    </div>"
                    ,yes: function(i, layero){
                        var priceData = table.cache.clothesPriceTable;
                        var priceList = [];
                        $.each(priceData,function (index,item) {
                            priceList.push({orderName:orderName, clothesVersionNumber: clothesVersionNumber, colorName: item.colorName, clothesPrice: item.clothesPrice, printPrice: item.printPrice, sewPrice: item.sewPrice});
                        });
                        $.ajax({
                            url: "/erp/addclothespricebatch",
                            type: 'POST',
                            data: {
                                clothesPriceJson:JSON.stringify(priceList)
                            },
                            success: function (res) {
                                if (res.result == 0) {
                                    initTable('', '', '', 0);
                                    layer.close(index);
                                    layer.msg("录入成功！", {icon: 1});
                                } else {
                                    layer.msg("录入失败！", {icon: 2});
                                }
                            },
                            error: function () {
                                layer.msg("录入失败！", {icon: 2});
                            }
                        })
                    }
                    ,btn2: function(index, layero){
                        var data = table.cache.clothesPriceTable;
                        var clothesPrice = 0,printPrice = 0,sewPrice = 0;
                        $.each(data,function (index,item) {
                            if (item.clothesPrice != null && item.clothesPrice != '' && item.clothesPrice != 0){
                                clothesPrice = item.clothesPrice;
                            }
                            if (item.printPrice != null && item.printPrice != '' && item.printPrice != 0){
                                printPrice = item.printPrice;
                            }
                            if (item.sewPrice != null && item.sewPrice != '' && item.sewPrice != 0){
                                sewPrice = item.sewPrice;
                            }
                        });
                        $.each(data,function (index,item) {
                            item.clothesPrice = clothesPrice;
                            item.printPrice = printPrice;
                            item.sewPrice = sewPrice;
                        });
                        table.reload("clothesPriceTable",{
                            data:data   // 将新数据重新载入表格
                        });
                        return false  //开启该代码可禁止点击该按钮关闭
                    }
                });
                layer.full(index);
                $.ajax({
                    url: "/erp/getordercolornamesbyorder",
                    type: 'GET',
                    data: {
                        orderName:orderName
                    },
                    success: function (res) {
                        var title = [
                            {field: 'colorName', title: '颜色',align:'center',minWidth:150},
                            {field: 'clothesPrice', title: '衣服单价',align:'center',minWidth:90,edit:'text'},
                            {field: 'printPrice', title: '印花单价',align:'center',minWidth:90,edit:'text'},
                            {field: 'sewPrice', title: '绣花单价',align:'center',minWidth:90,edit:'text'}
                        ];
                        colorList = res.colorNameList;
                        var data = [];
                        $.each(colorList,function (index, item) {
                            data.push({colorName: item, clothesPrice: 0, printPrice: 0, sewPrice: 0});
                        });
                        table.render({
                            elem: '#clothesPriceTable'
                            ,cols: [title]
                            ,height: 'full-200'
                            ,width: pageWidth
                            ,data: data
                            ,limit:Number.MAX_VALUE
                            ,done:function (res) {}
                        });
                    },
                    error: function () {
                        layer.msg("获取订单颜色失败！", {icon: 2});
                    }
                })
            }
        }
    });

    form.on('switch(sizeSwitch)', function(obj){
        var tableData = table.cache.accessoryDataTable;
        var index  = obj.othis.parents('tr').attr("data-index");
        if (tableData[index].sizeChild === '0'){
            tableData[index].sizeChild = '1';
        } else {
            tableData[index].sizeChild = '0';
        }
        setTimeout(function () {
            table.reload("accessoryDataTable",{
                data:tableData   // 将新数据重新载入表格
            })
        }, 200)
    });

    form.on('switch(colorSwitch)', function(obj){
        var tableData = table.cache.accessoryDataTable;
        var index  = obj.othis.parents('tr').attr("data-index");
        if (tableData[index].colorChild === '0'){
            tableData[index].colorChild = '1';
        } else {
            tableData[index].colorChild = '0';
        }
        setTimeout(function () {
            table.reload("accessoryDataTable",{
                data:tableData   // 将新数据重新载入表格
            })
        }, 200)
    });
    //监听行工具事件
    table.on('tool(processOrderTable)', function(obj){
        var data = obj.data;
        var gd = data.userName;
        var thisOrderName = data.orderName;
        if(obj.event === 'del'){
            if (userName != gd){
                layer.msg("您没有权限！", {icon: 2});
                return false;
            }
            layer.confirm('真的删除吗', function(index){
                $.ajax({
                    url: "/erp/deleteallmanufactureorderinfobyordername",
                    type: 'GET',
                    data: {
                        orderName:data.orderName
                    },
                    success: function (res) {
                        if (res.result == 0) {
                            initTable('', '', '', 0);
                            layer.close(index);
                            layer.msg("删除成功！", {icon: 1});
                        } else if (res.result == 4) {
                            layer.msg("已经开裁,无法删除 ！", {icon: 2});
                        } else {
                            layer.msg("删除失败！", {icon: 2});
                        }
                    },
                    error: function () {
                        layer.msg("删除失败！", {icon: 2});
                    }
                })

            });
        }
        else if(obj.event === 'detail'){
            processOrderDetail(data.orderName);
        }
        else if(obj.event === 'changeImage') {
            var orderName = data.orderName;
            var clothesVersionNumber = data.clothesVersionNumber;
            var label = "<span style='font-size: larger; font-weight: bolder'>公仔图添加/修改&nbsp;&nbsp;单号:"+ clothesVersionNumber + "&nbsp;&nbsp;款号:" + orderName + "</span>";
            var index = layer.open({
                type: 1 //Page层类型
                , title: label
                , btn: ['保存']
                , shade: 0.6 //遮罩透明度
                , area: '1000px'
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , content: "<div id=\"changeClothesImage\">\n" +
                    "        <form class=\"layui-form\" style=\"margin: 0 auto;max-width:90%;padding-top: 10px;\">\n" +
                    "            <div class=\"layui-row layui-col-space10\">\n" +
                    "                <div class=\"layui-col-md12\" id=\"clothesImageOperate\">\n" +
                    "                    <div class=\"layui-tab layui-tab-card\" style=\"width: 90%; height:500px\">\n" +
                    "                        <textarea id=\"clothesImageTextarea\"></textarea>\n" +
                    "                    </div>\n" +
                    "                </div>\n" +
                    "            </div>\n" +
                    "        </form>\n" +
                    "    </div>"
                ,yes: function(i, layero){
                    $.ajax({
                        url: "/erp/addstyleimage",
                        type: 'POST',
                        data: {
                            orderName: orderName,
                            clothesVersionNumber: clothesVersionNumber,
                            imageText: tinymce.activeEditor.getContent()
                        },
                        success: function (res) {
                            if (res.result == 0) {
                                tinymce.activeEditor.setContent("");
                                tinymce.remove('#clothesImageTextarea');
                                layer.msg("录入成功！", {icon: 1});
                                layer.close(index);
                            } else {
                                layer.msg("录入失败！", {icon: 2});
                            }
                        },
                        error: function () {
                            layer.msg("录入失败！", {icon: 2});
                        }
                    });
                    return false;
                }
                , cancel : function (i,layero) {
                    tinymce.activeEditor.setContent("");
                    tinymce.remove('#clothesImageTextarea');
                }
            });
            layer.full(index);
            setTimeout(function () {
                $.ajax({
                    url: "/erp/getstyleimagebyordername",
                    type: 'GET',
                    data: {
                        orderName:orderName
                    },
                    success: function (res) {
                        tinymce.init({
                            selector: '#clothesImageTextarea',
                            language: 'zh_CN',//中文
                            directionality: 'ltr',  //文字方向,ltr文字方向从左到右，rtl从右到左
                            browser_spellcheck: true,
                            contextmenu: false,
                            branding: false, //技术支持(Powered by Tiny || 由Tiny驱动)
                            menubar: false, //菜单栏
                            plugins: [
                                "print image autoresize table powerpaste"
                            ],
                            powerpaste_word_import: 'propmt',// 参数可以是propmt, merge, clear，效果自行切换对比
                            powerpaste_html_import: 'propmt',// propmt, merge, clear
                            powerpaste_allow_local_images: true,
                            paste_data_images: true,
                            relative_urls : false,
                            remove_script_host : false,
                            convert_urls : true,
                            toolbar: "print | undo redo | bold | alignleft aligncenter alignright alignjustify | fontselect | fontsizeselect | image table",
                            images_upload_handler: function(blobInfo, success, failure){
                                handleImgUpload(blobInfo, success, failure)
                            }
                        });
                        if (res.styleImage){
                            setTimeout(function () {
                                tinymce.activeEditor.setContent(res.styleImage.imageText);
                            }, 1000);
                        }
                    }, error: function () {
                        tinymce.init({
                            selector: '#clothesImageTextarea',
                            language: 'zh_CN',//中文
                            directionality: 'ltr',  //文字方向,ltr文字方向从左到右，rtl从右到左
                            browser_spellcheck: true,
                            contextmenu: false,
                            branding: false, //技术支持(Powered by Tiny || 由Tiny驱动)
                            menubar: false, //菜单栏
                            plugins: [
                                "print image autoresize table powerpaste"
                            ],
                            powerpaste_word_import: 'propmt',// 参数可以是propmt, merge, clear，效果自行切换对比
                            powerpaste_html_import: 'propmt',// propmt, merge, clear
                            powerpaste_allow_local_images: true,
                            paste_data_images: true,
                            relative_urls : false,
                            remove_script_host : false,
                            convert_urls : true,
                            toolbar: "print | undo redo | bold | alignleft aligncenter alignright alignjustify | fontselect | fontsizeselect | image table",
                            images_upload_handler: function(blobInfo, success, failure){
                                handleImgUpload(blobInfo, success, failure)
                            }
                        });
                        layer.msg("获取失败");
                    }
                });
            },100);
        }
        else if(obj.event === 'cutFabric') {
            var tableHtml = "";
            var index = layer.open({
                type: 1 //Page层类型
                , title: '裁剪单'
                , btn: ['下载/打印']
                , shade: 0.6 //遮罩透明度
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , content: "<div><div class='row'>" +
                    "        <div id='cutFabricExport'>" +
                    "        </div>" +
                    "    </div>" +
                    "    <iframe id='printf' src='' width='0' height='0' frameborder='0'></iframe></div>"
                , yes: function () {
                    printDeal();
                }
                , cancel : function (i,layero) {
                    $("#cutFabricExport").empty();
                }
            });
            layer.full(index);
            setTimeout(function () {
                $.ajax({
                    url: "/erp/getcutfabricbyordername",
                    type: 'GET',
                    async: false,
                    data: {
                        orderName:data.orderName
                    },
                    success: function (res) {
                        if(res.manufactureFabricList) {
                            var manufactureOrder = res.manufactureOrder;
                            var colorList = res.colorList;
                            var sizeList = res.sizeList;
                            var fabricNameList = res.fabricNameList;
                            var manufactureFabricList = res.manufactureFabricList;
                            var orderClothesList = res.orderClothesList;
                            sizeList = globalSizeSort(sizeList);
                            tableHtml = "<div class=\"col-md-12\" name='printTable'>\n" +
                                "               <section class=\"panel panel-default\" style='page-break-after:always;'>\n" +
                                "                   <header class=\"panel-heading font-bold\" style=\"text-align: center\">\n" +
                                "                       <div style=\"font-size: 20px;font-weight: 700\">中山市德悦服饰有限公司裁剪单</div><br>\n" +
                                "                       <div style=\"font-size: 16px;font-weight: 700\">客户:" + manufactureOrder.customerName + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;单号:" + manufactureOrder.clothesVersionNumber + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;款号:" + manufactureOrder.orderName +"</div><br>\n" +
                                "                   </header>\n" +
                                "                   <table class=\"table table-bordered\" style='width: 100%'>\n" +
                                "                       <tbody align='center'>";

                            if (fabricNameList.length <= 4){
                                tableHtml += "<tr><td style='font-weight: 700;color: black;width: 10%'>色组</td><td style='font-weight: 700; color: black;width: 10%'>属性</td><td colspan='2' style='font-weight: 700; color: black;width: 20%'>面料A</td><td colspan='2' style='font-weight: 700; color: black;width: 20%'>面料B</td><td colspan='2' style='font-weight: 700; color: black;width: 20%'>面料C</td><td colspan='2' style='font-weight: 700; color: black;width: 20%'>面料D</td></tr>";
                                for (var i = 0; i < colorList.length; i ++){
                                    var tmpColor = colorList[i];
                                    tableHtml += "<tr><td rowspan='8' style='font-size: large; font-weight: bolder;color: black'>"+ tmpColor +"</td>";
                                    var lineOne = "<td style='font-weight: bolder;color: black'>部    位:</td>";
                                    var lineTwo = "<tr><td style='font-weight: bolder;color: black'>颜    色:</td>";
                                    var lineThree = "<tr><td style='font-weight: bolder;color: black'>名    称:</td>";
                                    var lineFour = "<tr><td style='font-weight: bolder;color: black'>布    封:</td>";
                                    var lineFive = "<tr><td style='font-weight: bolder;color: black'>克    重:</td>";
                                    var lineSix = "<tr><td style='font-weight: bolder;color: black'>码    长:</td>";
                                    var lineSeven = "<tr><td style='font-weight: bolder;color: black'>单件用量:</td>";
                                    var lineEight = "<tr><td style='font-weight: bolder;color: black'>实际用量:</td>";
                                    for (var j = 0; j < fabricNameList.length; j ++){
                                        var tmpFabricName = fabricNameList[j];
                                        for (var k = 0; k < manufactureFabricList.length; k ++){
                                            var tmpFabric = manufactureFabricList[k];
                                            if (tmpFabric.colorName === tmpColor && tmpFabric.fabricName === tmpFabricName){
                                                lineOne += "<td>" + tmpFabric.partName + "</td><td rowspan='8' style='width: 10%'></td>";
                                                lineTwo += "<td>" + tmpFabric.fabricColor + "</td>";
                                                lineThree += "<td>" + tmpFabric.fabricName + "</td>";
                                                lineFour += "<td>" + tmpFabric.fabricWidth + "</td>";
                                                lineFive += "<td>" + tmpFabric.fabricWeight + "</td>";
                                                lineSix += "<td></td>";
                                                lineSeven += "<td>" + tmpFabric.pieceUsage + tmpFabric.unit + "</td>";
                                                lineEight += "<td></td>";
                                            }
                                        }
                                    }
                                    if (fabricNameList.length === 0){
                                        console.log("面料长度是0");
                                        lineOne += "<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>";
                                        lineTwo += "<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>";
                                        lineThree += "<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>";
                                        lineFour += "<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>";
                                        lineFive += "<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>";
                                        lineSix += "<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>";
                                        lineSeven += "<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>";
                                        lineEight += "<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>";
                                    } else if (fabricNameList.length === 1){
                                        console.log("面料长度是1");
                                        lineOne += "<td></td><td></td><td></td><td></td><td></td><td></td>";
                                        lineTwo += "<td></td><td></td><td></td><td></td><td></td><td></td>";
                                        lineThree += "<td></td><td></td><td></td><td></td><td></td><td></td>";
                                        lineFour += "<td></td><td></td><td></td><td></td><td></td><td></td>";
                                        lineFive += "<td></td><td></td><td></td><td></td><td></td><td></td>";
                                        lineSix += "<td></td><td></td><td></td><td></td><td></td><td></td>";
                                        lineSeven += "<td></td><td></td><td></td><td></td><td></td><td></td>";
                                        lineEight += "<td></td><td></td><td></td><td></td><td></td><td></td>";
                                    }  else if (fabricNameList.length === 2){
                                        console.log("面料长度是2");
                                        lineOne += "<td></td><td></td><td></td><td></td>";
                                        lineTwo += "<td></td><td></td><td></td><td></td>";
                                        lineThree += "<td></td><td></td><td></td><td></td>";
                                        lineFour += "<td></td><td></td><td></td><td></td>";
                                        lineFive += "<td></td><td></td><td></td><td></td>";
                                        lineSix += "<td></td><td></td><td></td><td></td>";
                                        lineSeven += "<td></td><td></td><td></td><td></td>";
                                        lineEight += "<td></td><td></td><td></td><td></td>";
                                    } else if (fabricNameList.length === 3){
                                        console.log("面料长度是3");
                                        lineOne += "<td></td><td></td>";
                                        lineTwo += "<td></td><td></td>";
                                        lineThree += "<td></td><td></td>";
                                        lineFour += "<td></td><td></td>";
                                        lineFive += "<td></td><td></td>";
                                        lineSix += "<td></td><td></td>";
                                        lineSeven += "<td></td><td></td>";
                                        lineEight += "<td></td><td></td>";
                                    }
                                    lineOne += "</tr>";
                                    lineTwo += "</tr>";
                                    lineThree += "</tr>";
                                    lineFour += "</tr>";
                                    lineFive += "</tr>";
                                    lineSix += "</tr>";
                                    lineSeven += "</tr>";
                                    lineEight += "</tr>";
                                    tableHtml += lineOne;
                                    tableHtml += lineTwo;
                                    tableHtml += lineThree;
                                    tableHtml += lineFour;
                                    tableHtml += lineFive;
                                    tableHtml += lineSix;
                                    tableHtml += lineSeven;
                                    tableHtml += lineEight;
                                }
                            }
                            else {
                                tableHtml += "<tr><td style='font-weight: 700;color: black;width: 5%'>色组</td><td style='font-weight: 700; color: black;width: 5%'>属性</td><td colspan='2' style='font-weight: 700; color: black;width: 15%'>面料A</td><td colspan='2' style='font-weight: 700; color: black;width: 15%'>面料B</td><td colspan='2' style='font-weight: 700; color: black;width: 15%'>面料C</td><td colspan='2' style='font-weight: 700; color: black;width: 15%'>面料D</td><td colspan='2' style='font-weight: 700; color: black;width: 15%'>面料E</td><td colspan='2' style='font-weight: 700; color: black;width: 15%'>面料F</td></tr>";
                                for (var i = 0; i < colorList.length; i ++){
                                    var tmpColor = colorList[i];
                                    tableHtml += "<tr><td rowspan='8' style='font-size: large; font-weight: bolder;color: black'>"+ tmpColor +"</td>";
                                    var lineOne = "<td style='font-weight: bolder;color: black'>部    位:</td>";
                                    var lineTwo = "<tr><td style='font-weight: bolder;color: black'>颜    色:</td>";
                                    var lineThree = "<tr><td style='font-weight: bolder;color: black'>名    称:</td>";
                                    var lineFour = "<tr><td style='font-weight: bolder;color: black'>布    封:</td>";
                                    var lineFive = "<tr><td style='font-weight: bolder;color: black'>克    重:</td>";
                                    var lineSix = "<tr><td style='font-weight: bolder;color: black'>码    长:</td>";
                                    var lineSeven = "<tr><td style='font-weight: bolder;color: black'>单件用量:</td>";
                                    var lineEight = "<tr><td style='font-weight: bolder;color: black'>实际用量:</td>";
                                    var j = 0;
                                    for (j = 0; j < fabricNameList.length; j ++){
                                        var tmpFabricName = fabricNameList[j];
                                        for (var k = 0; k < manufactureFabricList.length; k ++){
                                            var tmpFabric = manufactureFabricList[k];
                                            if (tmpFabric.colorName === tmpColor && tmpFabric.fabricName === tmpFabricName){
                                                lineOne += "<td>" + tmpFabric.partName + "</td><td rowspan='8' style='width: 10%'></td>";
                                                lineTwo += "<td>" + tmpFabric.fabricColor + "</td>";
                                                lineThree += "<td>" + tmpFabric.fabricName + "</td>";
                                                lineFour += "<td>" + tmpFabric.fabricWidth + "</td>";
                                                lineFive += "<td>" + tmpFabric.fabricWeight + "</td>";
                                                lineSix += "<td></td>";
                                                lineSeven += "<td>" + tmpFabric.pieceUsage + tmpFabric.unit + "</td>";
                                                lineEight += "<td></td>";
                                            }
                                        }
                                    }
                                    if (fabricNameList.length === 5){
                                        lineOne += "<td></td><td></td>";
                                        lineTwo += "<td></td><td></td>";
                                        lineThree += "<td></td><td></td>";
                                        lineFour += "<td></td><td></td>";
                                        lineFive += "<td></td><td></td>";
                                        lineSix += "<td></td><td></td>";
                                        lineSeven += "<td></td><td></td>";
                                        lineEight += "<td></td><td></td>";
                                    }
                                    lineOne += "</tr>";
                                    lineTwo += "</tr>";
                                    lineThree += "</tr>";
                                    lineFour += "</tr>";
                                    lineFive += "</tr>";
                                    lineSix += "</tr>";
                                    lineSeven += "</tr>";
                                    lineEight += "</tr>";
                                    tableHtml += lineOne;
                                    tableHtml += lineTwo;
                                    tableHtml += lineThree;
                                    tableHtml += lineFour;
                                    tableHtml += lineFive;
                                    tableHtml += lineSix;
                                    tableHtml += lineSeven;
                                    tableHtml += lineEight;
                                }
                            }
                            tableHtml += "</tbody></table>";
                            tableHtml += "<hr/><br>";
                            tableHtml += "<div class='col-md-12'><div style='font-weight: bolder;font-size: large;color: black'>裁剪备注:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1、裁床不接受色差&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2、分缸裁&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3、印绣花部位:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4、其他:</div></div>";
                            tableHtml += "<br><hr/><br>";
                            tableHtml += "<div class='col-md-12'><div style='font-weight: bolder;font-size: large;color: black' align='center'>订单详情</div></div><br>";

                            tableHtml += "                   <table class=\"table table-bordered\" style='width: 100%'>\n" +
                                "                       <tbody align='center'>";
                            // 加载表头
                            tableHtml += "<tr><td style='font-weight: bolder;color: black'>颜色</td>";
                            for (var i = 0; i < sizeList.length; i ++){
                                tableHtml += "<td style='font-weight: bolder;color: black'>" + sizeList[i] + "</td>";
                            }
                            tableHtml += "<td>合计</td></tr>";
                            var orderSum = 0;
                            for (var i = 0; i < colorList.length; i ++){
                                var tmpColor = colorList[i];
                                var colorSum = 0;
                                var tmpLine = "<tr><td style='font-weight: bolder;color: black'>" + tmpColor + "</td>";
                                for (var j = 0; j < sizeList.length; j ++){
                                    var tmpSize = sizeList[j];
                                    var flag = true;
                                    for (var k = 0; k < orderClothesList.length; k ++){
                                        var tmpOrderClothes = orderClothesList[k];
                                        if (tmpOrderClothes.colorName === tmpColor && tmpOrderClothes.sizeName === tmpSize){
                                            tmpLine += "<td>" + tmpOrderClothes.count + "</td>";
                                            flag = false;
                                            colorSum += tmpOrderClothes.count;
                                        }
                                    }
                                    if (flag){
                                        tmpLine += "<td>0</td>";
                                    }
                                }
                                orderSum += colorSum;
                                tmpLine += "<td>" + colorSum + "</td></tr>";
                                tableHtml += tmpLine;
                            }
                            tableHtml += "</tbody></table>";
                            tableHtml += "<br>";
                            tableHtml += "<div class='col-md-12'><div align='center' style='font-weight: bolder;font-size: large;color: black'>订单总数:" + orderSum + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;允许加裁:"+ manufactureOrder.additional +"%&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;裁剪数量:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;业务跟单:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div></div>";
                            tableHtml += "<br>";
                            $("#cutFabricExport").append(tableHtml);
                        }
                    },
                    error: function () {
                        layer.msg("获取详情信息失败！", {icon: 2});
                        layer.close(index);
                    }
                })
            },500)
        }
        else if(obj.event === 'materialCard') {
            var tableHtml = "";
            var index = layer.open({
                type: 1 //Page层类型
                , title: '物料卡'
                , btn: ['下载/打印']
                , shade: 0.6 //遮罩透明度
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , content: "<div><div class='row'>" +
                    "        <div id='materialCardExport'>" +
                    "        </div>" +
                    "    </div>" +
                    "    <iframe id='printf' src='' width='0' height='0' frameborder='0'></iframe></div>"
                , yes: function () {
                    printDeal();
                }
                , cancel : function (i,layero) {
                    $("#cutFabricExport").empty();
                }
            });
            layer.full(index);
            setTimeout(function () {
                $.ajax({
                    url: "/erp/getaccessorycardinfobyordername",
                    type: 'GET',
                    data: {
                        orderName:data.orderName
                    },
                    success: function (res) {
                        if(res.code == 0) {
                            var manufactureOrder = res.manufactureOrder;
                            var colorList = res.colorList;
                            var sizeList = res.sizeList;
                            var fabricNameList = res.fabricNameList;
                            var accessoryNameList = res.accessoryNameList;
                            var manufactureAccessoryList1 = res.manufactureAccessoryList1;
                            var manufactureAccessoryList2 = res.manufactureAccessoryList2;
                            var manufactureAccessoryList3 = res.manufactureAccessoryList3;
                            var manufactureAccessoryList4 = res.manufactureAccessoryList4;
                            var orderSum = res.orderSum;
                            sizeList = globalSizeSort(sizeList);
                            tableHtml = "<div class=\"col-md-12\" name='printTable'>\n" +
                                "               <section class=\"panel panel-default\" style='page-break-after:always;'>\n" +
                                "                   <header class=\"panel-heading font-bold\" style=\"text-align: center\">\n" +
                                "                       <div style=\"font-size: 20px;font-weight: 700\">中山市德悦服饰有限公司物料卡</div><br>\n" +
                                "                       <div style=\"font-size: 16px;font-weight: 700\">客户:" + manufactureOrder.customerName + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;单号:" + manufactureOrder.clothesVersionNumber + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;款号:" + manufactureOrder.orderName + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;订单总数:" + orderSum +"</div><br>\n" +
                                "                   </header>\n" +
                                "                   <table class=\"table table-bordered\" style='width: 100%'>\n" +
                                "                       <tbody align='center'>";
                            tableHtml += "<tr>";
                            // 如果面料、通用辅料、颜色通用辅料加一起不到10 就把三者放在一行展示
                            if (fabricNameList.length + manufactureAccessoryList2.length + manufactureAccessoryList1.length <= 10){
                                for (var i = 0; i < fabricNameList.length; i ++){
                                    tableHtml += "<td style='width: 10%'>" + fabricNameList[i] + "</td>"
                                }
                                for (var i = 0; i < manufactureAccessoryList2.length; i ++){
                                    tableHtml += "<td style='width: 10%'>" + manufactureAccessoryList2[i] + "</td>"
                                }
                                for (var i = 0; i < manufactureAccessoryList1.length; i ++){
                                    tableHtml += "<td style='width: 10%'>" + manufactureAccessoryList1[i] + "</td>"
                                }
                                for (var i = 0; i < (10 - fabricNameList.length - manufactureAccessoryList2.length - manufactureAccessoryList1.length); i++){
                                    tableHtml += "<td style='width: 10%'></td>";
                                }
                                tableHtml += "</tr>";
                                for (var i = 0; i < colorList.length; i ++){

                                    var rowCount = colorList.length;
                                    tableHtml += "<tr style='height: 100px'>";
                                    if (i === 0){
                                        for (var j = 0; j < 10; j ++){
                                            if (j >= (fabricNameList.length + manufactureAccessoryList2.length)){
                                                tableHtml += "<td rowspan="+ rowCount +"></td>";
                                            } else {
                                                tableHtml += "<td>" + colorList[i] + "</td>";
                                            }
                                        }
                                    } else {
                                        for (var j = 0; j < 10; j ++){
                                            if (j < (fabricNameList.length + manufactureAccessoryList2.length)){
                                                tableHtml += "<td>" + colorList[i] + "</td>";
                                            }
                                        }
                                    }
                                    tableHtml += "</tr>";
                                }

                                // 拼接分码辅料  另起一行
                                tableHtml += "<tr>";
                                var sizeLine = "<tr>";
                                for (var i = 0; i < 10; i ++){
                                    if (manufactureAccessoryList3[i]){
                                        tableHtml += "<td>" + manufactureAccessoryList3[i] + "</td>";
                                        sizeLine += "<td><table>";
                                        for (var k = 0; k < sizeList.length; k ++){
                                            sizeLine += "<tr><td>" + sizeList[k] + "</td></tr>";
                                        }
                                        sizeLine += "</table></td>";
                                    } else {
                                        tableHtml += "<td></td>";
                                        sizeLine += "<td></td>";
                                    }
                                }
                                tableHtml += "</tr>";
                                sizeLine += "</tr>";
                                tableHtml += sizeLine;
                            } else {
                                // 如果面料和分色辅料 > 10
                                if (fabricNameList.length + manufactureAccessoryList2.length > 10){
                                    for (var i = 0; i < fabricNameList.length; i ++){
                                        tableHtml += "<td style='width: 10%'>" + fabricNameList[i] + "</td>"
                                    }
                                    for (var i = fabricNameList.length; i < 10; i ++){
                                        tableHtml += "<td style='width: 10%'>" + manufactureAccessoryList2[i - fabricNameList.length] + "</td>"
                                    }
                                    tableHtml += "</tr>";
                                    // 第一行颜色全部区分
                                    for (var i = 0; i < colorList.length; i ++){
                                        tableHtml += "<tr style='height: 100px'><td>"+ colorList[i] + "</td><td>" +  colorList[i] + "</td><td>" +  colorList[i] + "</td><td>" +  colorList[i] + "</td><td>" +  colorList[i] + "</td><td>" +  colorList[i] + "</td><td>" +  colorList[i] + "</td><td>" +  colorList[i]+ "</td><td>" +  colorList[i]+ "</td><td>" +  colorList[i] + "</td></tr>";
                                    }
                                    tableHtml += "<tr>";
                                    // 第二行先拼接剩下的分色辅料 然后 分情况讨论 通用辅料是否能放下第二行剩下的
                                    if (fabricNameList.length + manufactureAccessoryList2.length + manufactureAccessoryList1.length <= 20){
                                        // 这里只拼接分色和通用  分码另起一行
                                        for (var i = 0; i < 10; i ++){
                                            if (manufactureAccessoryList2[10 - fabricNameList.length + i]){
                                                tableHtml += "<td>" + manufactureAccessoryList2[10 - fabricNameList.length + i] + "</td>";
                                            } else if (manufactureAccessoryList1[i - fabricNameList.length - manufactureAccessoryList2.length - 10]) {
                                                tableHtml += "<td>" + manufactureAccessoryList1[i - fabricNameList.length - manufactureAccessoryList2.length - 10] + "</td>";
                                            } else {
                                                tableHtml += "<td></td>";
                                            }
                                        }
                                        for (var i = 0; i < colorList.length; i ++){
                                            var rowCount = colorList.length;
                                            tableHtml += "<tr style='height: 100px'>";
                                            if (i === 0){
                                                for (var j = 0; j < 10; j ++){
                                                    if (j < (fabricNameList.length + manufactureAccessoryList2.length - 10)){
                                                        tableHtml += "<td rowspan="+ rowCount +"></td>";
                                                    } else {
                                                        tableHtml += "<td>" + colorList[i] + "</td>";
                                                    }
                                                }
                                            } else {
                                                for (var j = 0; j < 10; j ++){
                                                    if (j < (fabricNameList.length + manufactureAccessoryList2.length)){
                                                        tableHtml += "<td>" + colorList[i] + "</td>";
                                                    }
                                                }
                                            }
                                            tableHtml += "</tr>";
                                        }
                                        // 拼接分码辅料
                                        tableHtml += "<tr>";
                                        var sizeLine = "<tr>";
                                        for (var i = 0; i < 10; i ++){
                                            if (manufactureAccessoryList3[i]){
                                                tableHtml += "<td>" + manufactureAccessoryList3[i] + "</td>";
                                                sizeLine += "<td><table>";
                                                for (var k = 0; k < sizeList.length; k ++){
                                                    sizeLine += "<tr><td>" + sizeList[k] + "</td></tr>";
                                                }
                                                sizeLine += "</table></td>";
                                            } else {
                                                tableHtml += "<td></td>";
                                                sizeLine += "<td></td>";
                                            }
                                        }
                                        tableHtml += "</tr>";
                                        sizeLine += "</tr>";
                                        tableHtml += sizeLine;
                                    } else {
                                        // 面料+分色+通用 > 20 这里只拼接分色和通用在第二行
                                        for (var i = 0; i < 10; i ++){
                                            if (manufactureAccessoryList2[10 - fabricNameList.length + i]){
                                                tableHtml += "<td>" + manufactureAccessoryList2[10 - fabricNameList.length + i] + "</td>";
                                            } else {
                                                tableHtml += "<td>" + manufactureAccessoryList1[i - fabricNameList.length - manufactureAccessoryList2.length - 10] + "</td>";
                                            }
                                        }
                                        // 拼接颜色行
                                        for (var i = 0; i < colorList.length; i ++){
                                            var rowCount = colorList.length;
                                            tableHtml += "<tr style='height: 100px'>";
                                            if (i === 0){
                                                for (var j = 0; j < 10; j ++){
                                                    if (j < (fabricNameList.length + manufactureAccessoryList2.length - 10)){
                                                        tableHtml += "<td rowspan="+ rowCount +"></td>";
                                                    } else {
                                                        tableHtml += "<td>" + colorList[i] + "</td>";
                                                    }
                                                }
                                            } else {
                                                for (var j = 0; j < 10; j ++){
                                                    if (j < (fabricNameList.length + manufactureAccessoryList2.length)){
                                                        tableHtml += "<td>" + colorList[i] + "</td>";
                                                    }
                                                }
                                            }
                                            tableHtml += "</tr>";
                                        }
                                        // 通用剩下的和分码另起一行
                                        tableHtml += "<tr>";
                                        var colorSizeLine = "<tr>";
                                        for (var i = 0; i < 10; i ++){
                                            if (i < (fabricNameList.length + manufactureAccessoryList2.length + manufactureAccessoryList1.length - 20)){
                                                tableHtml += "<td>" + manufactureAccessoryList1[10 - fabricNameList.length - manufactureAccessoryList2.length + i] + "</td>";
                                                colorSizeLine += "<td></td>"
                                            } else if (manufactureAccessoryList3[10 - fabricNameList.length - manufactureAccessoryList2.length - manufactureAccessoryList1.length + i]) {
                                                tableHtml += "<td>" + manufactureAccessoryList3[10 - fabricNameList.length - manufactureAccessoryList2.length - manufactureAccessoryList1.length + i] + "</td>";
                                                colorSizeLine += "<td><table>";
                                                for (var k = 0; k < sizeList.length; k ++){
                                                    colorSizeLine += "<tr><td>" + sizeList[k] + "</td></tr>";
                                                }
                                                colorSizeLine += "</table></td>";
                                            } else {
                                                tableHtml += "<td></td>";
                                                colorSizeLine += "<td></td>";
                                            }
                                        }
                                    }
                                } else {

                                    // 如果面料 + 分色辅料 <= 10  第一行拼接三种（面料 分色辅料  通用辅料）
                                    // 注意: 这里肯定是 面料 + 分色辅料 + 通用辅料 > 10 因为如果 <= 10在第一个if 就被过滤掉了

                                    for (var i = 0; i < fabricNameList.length; i ++){
                                        tableHtml += "<td width='10%'>" + fabricNameList[i] + "</td>"
                                    }
                                    for (var i = 0; i < manufactureAccessoryList2.length; i ++){
                                        tableHtml += "<td width='10%'>" + manufactureAccessoryList2[i] + "</td>"
                                    }
                                    for (var i = (fabricNameList.length + manufactureAccessoryList2.length); i < 10; i ++){
                                        tableHtml += "<td width='10%'>" + manufactureAccessoryList1[i - (fabricNameList.length + manufactureAccessoryList2.length)] + "</td>"
                                    }
                                    tableHtml += "</tr>";
                                    for (var i = 0; i < colorList.length; i ++){
                                        var rowCount = colorList.length;
                                        tableHtml += "<tr style='height: 100px'>";
                                        if (i === 0){
                                            for (var j = 0; j < 10; j ++){
                                                if (j >= (fabricNameList.length + manufactureAccessoryList2.length)){
                                                    tableHtml += "<td rowspan="+ rowCount +"></td>";
                                                } else {
                                                    tableHtml += "<td>" + colorList[i] + "</td>";
                                                }
                                            }
                                        } else {
                                            for (var j = 0; j < 10; j ++){
                                                if (j < (fabricNameList.length + manufactureAccessoryList2.length)){
                                                    tableHtml += "<td>" + colorList[i] + "</td>";
                                                }
                                            }
                                        }
                                        tableHtml += "</tr>";
                                    }
                                    var colorSizeLine = "<tr style='height: 100px'>";

                                    // 这里默认通用辅料第二行全部放完 接着放分码辅料
                                    // 如果分码辅料第二行可以放完 和 不能放完  进行讨论

                                    if ((20 - fabricNameList.length - manufactureAccessoryList2.length - manufactureAccessoryList1.length) >= manufactureAccessoryList3.length){
                                        tableHtml += "<tr>";
                                        for (var i = 0; i < 10; i ++){
                                            if (i < (fabricNameList.length + manufactureAccessoryList2.length + manufactureAccessoryList1.length - 10)){
                                                tableHtml += "<td>" + manufactureAccessoryList1[10 - fabricNameList.length - manufactureAccessoryList2.length + i] + "</td>";
                                                colorSizeLine += "<td></td>"
                                            } else if (manufactureAccessoryList3[10 - fabricNameList.length - manufactureAccessoryList2.length - manufactureAccessoryList1.length + i]) {
                                                tableHtml += "<td>" + manufactureAccessoryList3[10 - fabricNameList.length - manufactureAccessoryList2.length - manufactureAccessoryList1.length + i] + "</td>";
                                                colorSizeLine += "<td><table>";
                                                for (var k = 0; k < sizeList.length; k ++){
                                                    colorSizeLine += "<tr><td>" + sizeList[k] + "</td></tr>";
                                                }
                                                colorSizeLine += "</table></td>";
                                            } else {
                                                tableHtml += "<td></td>";
                                                colorSizeLine += "<td></td>";
                                            }
                                        }
                                        tableHtml += "</tr>";
                                        colorSizeLine += "</tr>";
                                        tableHtml += colorSizeLine;
                                    } else {
                                        tableHtml += "<tr>";
                                        for (var i = 0; i < 10; i ++){
                                            if (i < (fabricNameList.length + manufactureAccessoryList2.length + manufactureAccessoryList1.length - 10)){
                                                tableHtml += "<td>" + manufactureAccessoryList1[10 - fabricNameList.length - manufactureAccessoryList2.length + i] + "</td>";
                                                colorSizeLine += "<td></td>"
                                            } else {
                                                tableHtml += "<td>" + manufactureAccessoryList3[10 - fabricNameList.length - manufactureAccessoryList2.length - manufactureAccessoryList1.length + i] + "</td>";
                                                colorSizeLine += "<td><table>";
                                                for (var k = 0; k < sizeList.length; k ++){
                                                    colorSizeLine += "<tr><td>" + sizeList[k] + "</td></tr>";
                                                }
                                                colorSizeLine += "</table></td>";
                                            }
                                        }
                                        tableHtml += "</tr>";
                                        colorSizeLine += "</tr>";
                                        tableHtml += colorSizeLine;
                                        tableHtml += "<tr>";
                                        var colorSizeLineTwo = "<tr>";
                                        for (var i = 0; i < 10; i ++){
                                            if (i < (fabricNameList.length + manufactureAccessoryList2.length + manufactureAccessoryList1.length + manufactureAccessoryList3.length - 20)){
                                                tableHtml += "<td>" + manufactureAccessoryList3[20 - fabricNameList.length - manufactureAccessoryList2.length - manufactureAccessoryList1.length + i] + "</td>";
                                                colorSizeLineTwo += "<td><table>";
                                                for (var k = 0; k < sizeList.length; k ++){
                                                    colorSizeLineTwo += "<tr><td>" + sizeList[k] + "</td></tr>";
                                                }
                                                colorSizeLineTwo += "</td></table>";
                                            } else {
                                                tableHtml += "<td></td>";
                                                colorSizeLineTwo += "<td></td>"
                                            }
                                        }
                                        colorSizeLineTwo += "</tr>";
                                        tableHtml += "</tr>";
                                        tableHtml += colorSizeLineTwo;
                                    }

                                }
                            }
                            tableHtml += "</tbody></table>";
                            tableHtml += "<hr/><br>";
                            tableHtml += "<div class='col-md-12'><div style='font-weight: bolder;font-size: large;color: black'>跟单:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;QC:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div></div>";
                            tableHtml += "<br><hr/><br>";
                            $("#materialCardExport").append(tableHtml);
                        }
                    }, error: function () {
                        layer.msg("获取详情信息失败！", {icon: 2});
                        layer.close(index);
                    }
                })
            },500)
        }
        else if(obj.event === 'closeOrder') {
            if (userName != gd){
                layer.msg("您没有权限！", {icon: 2});
                return false;
            }
            layer.confirm('确认关单吗?', function(index){
                $.ajax({
                    url: "/erp/changeorderstate",
                    type: 'POST',
                    data: {
                        orderName:data.orderName,
                        orderState:1
                    },
                    success: function (res) {
                        if (res.result == 0) {
                            initTable('', '', '', 0);
                            layer.close(index);
                            layer.msg("删除成功！", {icon: 1});
                        } else {
                            layer.msg("删除失败！", {icon: 2});
                        }
                    },
                    error: function () {
                        layer.msg("删除失败！", {icon: 2});
                    }
                })

            });
        }
        else if (obj.event === "send") {
            layer.confirm("确认发送到阳春德悦?", function(index){
                $.ajax({
                    url: "checkorderclothesexist",
                    type:'POST',
                    data: {
                        orderName: thisOrderName
                    },
                    success: function (data) {
                        if(data.result == 0) {
                            $.ajax({
                                url: "/erp/syncorderclothestoyc",
                                type: 'POST',
                                data: {
                                    orderName: thisOrderName
                                },
                                success: function (res) {
                                    if (res.result == 0) {
                                        layer.msg("发送成功！", {icon: 1});
                                    } else {
                                        layer.msg("发送失败！", {icon: 2});
                                    }
                                }, error: function () {
                                    layer.msg("发送失败！", {icon: 2});
                                }
                            });
                            return false;
                        }else {
                            layer.msg("已经存在,请确认！",{icon: 2});
                        }
                    }, error: function () {
                        layer.msg("发送失败！",{icon: 2});
                    }
                });
            });
        }
        else if (obj.event === "size"){
            var orderName = data.orderName;
            var clothesVersionNumber = data.clothesVersionNumber;
            var label = "<span style='font-size: larger; font-weight: bolder'>尺寸工艺&nbsp;&nbsp;单号:"+ clothesVersionNumber + "&nbsp;&nbsp;款号:" + orderName + "&nbsp;&nbsp;&nbsp;&nbsp;</span>";
            var index = layer.open({
                type: 1 //Page层类型
                , title: label
                , btn: ['保存']
                , shade: 0.6 //遮罩透明度
                , area: '1000px'
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , content: "<div>\n" +
                    "        <div id=\"addSizeDetail\">\n" +
                    "            <form class=\"layui-form\" style=\"margin: 0 auto;max-width:100%;padding-top: 10px;\">\n" +
                    "                <div class=\"layui-col-md12\" style=\"width: 100%;padding-top: 5px\">\n" +
                    "                    <div class=\"layui-tab layui-tab-card\">\n" +
                    "                        <textarea id=\"sizeDetailTextarea\"></textarea>\n" +
                    "                    </div>\n" +
                    "                </div>\n" +
                    "            </form>\n" +
                    "        </div>\n" +
                    "        <style>\n" +
                    "            .tox-tinymce-aux {\n" +
                    "                z-index:199910170\n" +
                    "            }\n" +
                    "        </style>\n" +
                    "    </div>"
                ,yes: function(i, layero){
                    $.ajax({
                        url: "/erp/addordermeasuretext",
                        type: 'POST',
                        data: {
                            orderName: orderName,
                            clothesVersionNumber: clothesVersionNumber,
                            orderMeasureImg: tinymce.activeEditor.getContent()
                        },
                        success: function (res) {
                            if (res.result == 0) {
                                tinymce.activeEditor.setContent("");
                                tinymce.remove('#sizeDetailTextarea');
                                layer.msg("录入成功！", {icon: 1});
                                layer.close(index);
                            } else {
                                layer.msg("录入失败！", {icon: 2});
                            }
                        },
                        error: function () {
                            layer.msg("录入失败！", {icon: 2});
                        }
                    });
                    return false;
                }
                , cancel : function (i,layero) {
                    tinymce.activeEditor.setContent("");
                    tinymce.remove('#sizeDetailTextarea');
                }
            });
            layer.full(index);
            setTimeout(function () {
                $.ajax({
                    url: "/erp/getordermeasurebyorder",
                    type: 'GET',
                    data: {
                        orderName:orderName
                    },
                    async:false,
                    success: function (res) {
                        tinymce.init({
                            selector: '#sizeDetailTextarea',
                            language: 'zh_CN',//中文
                            directionality: 'ltr',  //文字方向,ltr文字方向从左到右，rtl从右到左
                            browser_spellcheck: true,
                            contextmenu: false,
                            branding: false, //技术支持(Powered by Tiny || 由Tiny驱动)
                            menubar: false, //菜单栏
                            plugins: [
                                "print image autoresize table powerpaste"
                            ],
                            powerpaste_word_import: 'propmt',// 参数可以是propmt, merge, clear，效果自行切换对比
                            powerpaste_html_import: 'propmt',// propmt, merge, clear
                            powerpaste_allow_local_images: true,
                            paste_data_images: true,
                            toolbar: "print | undo redo | bold | alignleft aligncenter alignright alignjustify | fontselect | fontsizeselect | image table",
                            images_upload_handler: function (blobInfo, success, failure) {
                                handleImgUpload(blobInfo, success, failure)
                            }
                        });
                        if (res.orderMeasure) {
                            setTimeout(function () {
                                tinymce.activeEditor.setContent(res.orderMeasure.orderMeasureImg);
                            }, 1000);
                        } else {
                            setTimeout(function () {
                                var initText = "<p style='text-align: center;'><span style='font-size: 18pt;'><strong>中山德悦服饰尺寸工艺单</strong><br>单号:" + clothesVersionNumber + "&nbsp; &nbsp;&nbsp; &nbsp;款号:" + orderName + "</span></p>&nbsp;<p style='text-align: center;'></p>&nbsp;<p style='text-align: center;'></p>&nbsp;<p style='text-align: center;'></p>&nbsp;<p style='text-align: center;'></p>&nbsp;<p style='text-align: center;'></p>";
                                tinymce.activeEditor.setContent(initText);
                            }, 1000);
                        }
                    }, error: function () {
                        tinymce.init({
                            selector: '#sizeDetailTextarea',
                            language: 'zh_CN',//中文
                            directionality: 'ltr',  //文字方向,ltr文字方向从左到右，rtl从右到左
                            browser_spellcheck: true,
                            contextmenu: false,
                            branding: false, //技术支持(Powered by Tiny || 由Tiny驱动)
                            menubar: false, //菜单栏
                            plugins: [
                                "print image autoresize table powerpaste"
                            ],
                            powerpaste_word_import: 'propmt',// 参数可以是propmt, merge, clear，效果自行切换对比
                            powerpaste_html_import: 'propmt',// propmt, merge, clear
                            powerpaste_allow_local_images: true,
                            paste_data_images: true,
                            toolbar: "print | undo redo | bold | alignleft aligncenter alignright alignjustify | fontselect | fontsizeselect | image table",
                            images_upload_handler: function(blobInfo, success, failure){
                                handleImgUpload(blobInfo, success, failure)
                            }
                        });
                        layer.msg("获取失败");
                    }
                });
            },100);
        }
    });

    function basicOrderInfo(type, orderName, clothesVersionNumber) {
        var colorSizeTable,colorSelValue,sizeSelValue;
        layui.use([ 'form', 'step','table'], function () {
            var $ = layui.$
                , form = layui.form
                , step = layui.step
                , table = layui.table;

            var titleHtml;
            if (type === 1){
                titleHtml = "<span style='font-weight: bolder;font-size: larger;color:#ff0000'>复制制单 单号:"+ clothesVersionNumber + "款号:" + orderName + "若存在撞色请再颜色输入时把几个颜色用\'/\'隔开,如:红色和白色,颜色录入'红/白',否则打菲会混哦~~~</span>";
            } else {
                titleHtml = "<span style='font-weight: bolder;font-size: larger;color:#ff0000'>录入基本信息  若存在撞色请再颜色输入时把几个颜色用\'/\'隔开,如:红色和白色,颜色录入'红/白',否则打菲会混哦~~~</span>";
            }
            var index = layer.open({
                type: 1 //Page层类型
                , title: titleHtml
                , shade: 0.6 //遮罩透明度
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , content: $('#stepForm')
                , cancel : function (i,layero) {
                    $("#stepForm").find("input").val("");
                    sizeSelect.update({
                        data: []
                    });
                    $('#colorSelect')[0].selectize.clear();
                    $("#stepForm").hide();
                    $("#colorSizeTableDiv").hide();
                    $("#controlStep").hide();
                    $("#lay_step").hide();
                    window.location.reload();
                }
            });
            $(".layui-form-select,.layui-input").css("padding-right",0);
            layer.full(index);
            step.render({
                elem: '#stepForm',
                filter: 'stepForm',
                width: '100%', //设置容器宽度
                stepWidth: '100%',
                height: '600px',
                stepItems: [{
                    title: '基础信息'
                }, {
                    title: '订单数量'
                }, {
                    title: '完成'
                }]
            });
            if (type === 1){
                $.ajax({
                    url: "/erp/getcopymanufactureorderinfo",
                    type: 'GET',
                    data: {
                        orderName: orderName
                    },
                    success: function (res) {
                        if (res.code == 0) {
                            var manufactureOrder = res.manufactureOrder;
                            var colorNameList = res.colorNameList;
                            var sizeNameList = res.sizeNameList;
                            sizeNameList = globalSizeSort(sizeNameList);
                            console.log(sizeNameList);
                            $.each(colorNameList, function(index,element){
                                $('#colorSelect')[0].selectize.addOption({id:element, value:element, text:element, title:element});
                                $('#colorSelect')[0].selectize.addItem(element);
                            });
                            customerNameAddDemo.setValue([{ name: manufactureOrder.customerName, value:manufactureOrder.customerName}])
                            form.val("baseInfo", { //formTest 即 class="layui-form" 所在元素属性 lay-filter="" 对应的值
                                // "customerNameAdd": manufactureOrder.customerName // "name": "value"
                                "purchaseMethod": manufactureOrder.purchaseMethod
                                ,"orderName": manufactureOrder.orderName
                                ,"clothesVersionNumber": manufactureOrder.clothesVersionNumber
                                ,"styleName": manufactureOrder.styleName
                                ,"season": manufactureOrder.season
                                ,"modelType": manufactureOrder.modelType
                                ,"deadLine": moment(manufactureOrder.deadLine).format("YYYY-MM-DD")
                                ,"deliveryDate": moment(manufactureOrder.deliveryDate).format("YYYY-MM-DD")
                                ,"additional": manufactureOrder.additional
                                ,"remark": manufactureOrder.remark + "(由款号" + orderName + "复制)"
                            });

                            var sizeData=[];
                            for(var key in sizeNameData) {
                                var parent = {};
                                parent.name = key;
                                parent.value = key;
                                var children=[];
                                var tmpList = sizeNameData[key];
                                tmpList = globalSizeSort(tmpList);
                                $.each(tmpList, function (index,item) {
                                    var tmp = {name: item, value: item};
                                    $.each(sizeNameList,function (index_s,item_s) {
                                        if (item == item_s){
                                            tmp.selected = true;
                                        }
                                    });
                                    children.push(tmp);
                                });
                                children = globalSizeNameSort(children);
                                console.log(children);
                                parent.children = children;
                                sizeData.push(parent);
                            }
                            sizeSelect.update({
                                data: sizeData
                            })
                            form.render();
                        } else {
                            layer.msg("获取失败！", {icon: 2});
                        }
                    },
                    error: function () {
                        layer.msg("获取失败！", {icon: 2});
                    }
                });
            }

            form.on('submit(formStep)', function (data) {
                $(".layui-layer-content").scrollTop(0);
                colorSelValue = $("#colorSelect").val();
                sizeSelValue = sizeSelect.getValue('value');

                if (type == 1){
                    if ($("input[name='orderName']").val() == orderName){
                        layer.msg("复制制单款号不能与原制单相同~~~", { icon: 2 });
                        return false;
                    }
                }
                if($("input[name='orderName']").val()=="") {
                    layer.msg("款号必须", { icon: 2 });
                    return false;
                }
                // if($("#customerNameAdd").val()=="") {
                if(customerNameAddDemo.getValue('nameStr')=="") {
                    layer.msg("品牌必须", { icon: 2 });
                    return false;
                }
                if($("input[name='clothesVersionNumber']").val()=="") {
                    layer.msg("版单必须，如果没有则和款号相同", { icon: 2 });
                    return false;
                }
                if($("input[id='season']").val()=="") {
                    layer.msg("请选择季度", { icon: 2 });
                    return false;
                }
                if($("input[id='styleName']").val()=="") {
                    layer.msg("款式描述必须", { icon: 2 });
                    return false;
                }
                if($("input[id='additional']").val()=="") {
                    layer.msg("允许加裁必须", { icon: 2 });
                    return false;
                }
                if(!isNumber($("#additional").val())) {
                    layer.msg("允许加裁是数字", { icon: 2 });
                    return false;
                }
                if($("input[id='deadLine']").val()=="") {
                    layer.msg("请输入收单日期", { icon: 2 });
                    return false;
                }

                if($("input[id='deliveryDate']").val()=="") {
                    layer.msg("请输入交期", { icon: 2 });
                    return false;
                }

                if(sizeSelValue.length==0) {
                    layer.msg("请选择尺码", { icon: 2 });
                    return false;
                }
                if(colorSelValue==null) {
                    layer.msg("请输入颜色", { icon: 2 });
                    return false;
                }

                var title = [
                    {field: 'color', title: '颜色/尺码',minWidth:100, fixed: true}
                ];
                $.each(sizeSelValue,function(index,value){
                    var field = {};
                    field.field = value;
                    field.title = value;
                    field.minWidth = 80;
                    field.edit = 'text';
                    title.push(field);
                });
                var colorSizeData = [];
                $.each(colorSelValue,function(index,value){
                    var tmp = {};
                    tmp.color = value;
                    colorSizeData.push(tmp);
                });
                step.next('#stepForm');
                $("#colorSizeTableDiv").show();
                $("#controlStep").show();
                colorSizeTable = table.render({
                    elem: '#colorSizeTable'
                    ,cols: [title]
                    ,data: colorSizeData
                    ,limit: 100000
                    ,done:function () {
                        $("div[lay-id='colorSizeTable'] .layui-table-body tr:first td:eq(1)").bind('paste', function (e) {
                            e.preventDefault(); //消除默认粘贴
                            //获取粘贴板数据
                            var clipboardData = window.clipboardData || e.originalEvent.clipboardData, // IE || chrome
                                data = clipboardData.getData('Text'),
                                //判断表格数据是使用\n还是\r分行，解析成行数组
                                rowArr = (data.indexOf(String.fromCharCode(10)) > -1) ?
                                    data.split(String.fromCharCode(10)) :
                                    data.split(String.fromCharCode(13)),
                                //根据\t解析单元格
                                cellArr = rowArr.filter(function(item) { //兼容Excel行末\n，防止粘贴出多余空行
                                    return (item !== "")
                                }).map(function(item) {
                                    return item. split(String.fromCharCode(9));
                                });
                            $.each(colorSizeData,function(d_index,row){
                                $.each(sizeSelValue,function(s_index,size){
                                    if(cellArr.length>d_index && cellArr[0].length>s_index) {
                                        colorSizeData[d_index][size] = cellArr[d_index][s_index];
                                    }
                                });
                            });
                            table.reload("colorSizeTable",{
                                data:colorSizeData   // 将新数据重新载入表格
                            })
                        });
                    }
                });
                return false;
            });

            form.on('submit(formStep2)', function (data) {
                var colorSizeNums = layui.table.cache.colorSizeTable;
                var isInput = true;
                $.each(colorSizeNums,function(i,item){
                    $.each(sizeSelValue,function(j,value){
                        if(!item[value]) {
                            isInput = false;
                            return false;
                        }
                    });
                });
                if(!isInput) {
                    layer.msg("请输入完所有数量", { icon: 2 });
                    return false;
                }else {
                    var params = form.val("baseInfo");
                    params.sizeInfo = sizeSelect.getValue('valueStr');
                    params.season = $("#season").val();
                    params.colorInfo = colorSelValue.join(",");
                    params.buyer = userName;
                    // params.customerName = params.customerNameAdd;
                    params.customerName = customerNameAddDemo.getValue('nameStr');
                    var orderClothesList = [];
                    $.each(colorSizeNums,function (index, item) {
                        $.each(sizeSelValue,function (s_index,size) {
                            var orderClothes = {};
                            orderClothes.colorName = item.color.replace(/\ +/g,"").replace(/[\r\n]/g,"");
                            orderClothes.userName = userName;
                            orderClothes.orderName = params.orderName.replace(/\s/g,"");
                            orderClothes.clothesVersionNumber = params.clothesVersionNumber.replace(/\s/g,"");
                            orderClothes.styleDescription = params.styleName;
                            orderClothes.season = params.season;
                            orderClothes.deadLine = params.deadLine;
                            orderClothes.deliveryDate = params.deliveryDate;
                            orderClothes.sizeName = size.replace(/\ +/g,"").replace(/[\r\n]/g,"");
                            orderClothes.customerName = params.customerName;
                            orderClothes.purchaseMethod = params.purchaseMethod;
                            orderClothes.count = item[size];
                            orderClothes.orderState = 0;
                            orderClothesList.push(orderClothes);
                        })
                    });
                    if (type === 1){
                        $.ajax({
                            url: "/erp/copymanufactureorder",
                            type: 'POST',
                            data: {
                                manufactureOrderJson:JSON.stringify(params),
                                orderClothesJson:JSON.stringify(orderClothesList),
                                initOrderName: orderName,
                                toOrderName: params.orderName
                            },
                            success: function (res) {
                                if (res.error){
                                    layer.msg(res.error, {icon: 2});
                                } else if (res.result == 4) {
                                    layer.msg("该款号已经录入,款号不能重复,请检查", {icon: 2});
                                } else if (res.result == 0) {
                                    step.next('#stepForm');
                                    $(".layui-layer-content").scrollTop(0);
                                    initTable(params.orderName, '', '', 0);
                                } else {
                                    layer.msg("复制失败！", {icon: 2});
                                }
                            },
                            error: function () {
                                layer.msg("复制失败！", {icon: 2});
                            }
                        });
                    } else {
                        $.ajax({
                            url: "/erp/addmanufactureorder",
                            type: 'POST',
                            data: {
                                manufactureOrderJson:JSON.stringify(params),
                                orderClothesJson:JSON.stringify(orderClothesList)
                            },
                            success: function (res) {
                                if (res.error){
                                    layer.msg(res.error, {icon: 2});
                                } else if (res.result == 4) {
                                    layer.msg("该款号已经录入,款号不能重复,请检查", {icon: 2});
                                } else if (res.result == 0) {
                                    step.next('#stepForm');
                                    $(".layui-layer-content").scrollTop(0);
                                    initTable(params.orderName, '', '', 0);
                                } else {
                                    layer.msg("录入失败！", {icon: 2});
                                }
                            },
                            error: function () {
                                layer.msg("录入失败！", {icon: 2});
                            }
                        });
                    }
                    return false;
                }
            });

            $('.pre').click(function () {
                $(".layui-layer-content").scrollTop(0);
                step.pre('#stepForm');
            });

            $('.next').click(function () {
                $(".layui-layer-content").scrollTop(0);
                $("#stepForm").find("input").val("");
                sizeSelect.update({
                    data: []
                });
                $('#colorSelect')[0].selectize.clear();
                $("#colorSizeTableDiv").hide();
                $("#controlStep").hide();
                $("#lay_step").hide();
                step.next('#stepForm');
            });

            $('.detail').click(function () {
                var orderName = $("input[name='orderName']").val().replace(/\s/g,"");
                $("#stepForm").find("input").val("");
                sizeSelect.update({
                    data: []
                });
                $('#colorSelect')[0].selectize.clear();
                $("#stepForm").hide();
                $("#colorSizeTableDiv").hide();
                $("#controlStep").hide();
                $("#lay_step").hide();
                layer.closeAll();
                processOrderDetail(orderName);
                return false;
            });
        });
    }
});
function processOrderDetail(orderName) {
    var pdfName = orderName + '--制单.pdf';
    var index = layer.open({
        type: 1 //Page层类型
        , title: '制单工艺表'
        , btn: ['下载']
        , shade: 0.6 //遮罩透明度
        , maxmin: false //允许全屏最小化
        , anim: 0 //0-6的动画形式，-1不开启
        , content: "<div id=\"processDetail\" name=\"processDetail\" style=\"background-color: white; color: black;font-size: 20px;\">\n" +
            "    <div id=\"contentPage\" style=\"background-color: white\">\n" +
            "        <h1 style=\"text-align: center;font-size: 20px; font-weight: 800; margin-top: 0px\">中山德悦服饰生产制单</h1>\n" +
            "        <div class=\"row\" style=\"margin:0\">\n" +
            "            <div class=\"col-md-12\">\n" +
            "                <section class=\"panel panel-default\">\n" +
            "                    <header class=\"panel-heading font-bold\" style=\"font-size: 16px;background-color: #dcd9d9;border-color: black;\">\n" +
            "                        <span class=\"label bg-success pull-right\"></span>基本信息\n" +
            "                    </header>\n" +
            "                    <div id=\"basicDetailInfo\"></div>\n" +
            "                </section>\n" +
            "            </div>\n" +
            "            <div class=\"row\" style=\"margin:0\">\n" +
            "                <div class=\"col-md-12\">\n" +
            "                    <section class=\"panel panel-default\">\n" +
            "                        <header class=\"panel-heading font-bold\" style=\"font-size: 16px;background-color: #dcd9d9;border-color: black;\">\n" +
            "                            <span class=\"label bg-success pull-right\"></span>款式图\n" +
            "                        </header>\n" +
            "                        <div id=\"styleImgDiv\" style=\"text-align: center; height: 300px\"></div>\n" +
            "                    </section>\n" +
            "                </div>\n" +
            "            </div>\n" +
            "        </div>\n" +
            "        <div class=\"row\" style=\"margin:0\">\n" +
            "            <div class=\"col-md-12\">\n" +
            "                <section class=\"panel panel-default\">\n" +
            "                    <header class=\"panel-heading font-bold table-head\" style=\"font-size: 16px;background-color: #dcd9d9;border-color: black;\">\n" +
            "                        <span class=\"label bg-success pull-right\"></span>下单细数\n" +
            "                    </header>\n" +
            "                    <div id=\"orderClothesTable\"></div>\n" +
            "                </section>\n" +
            "            </div>\n" +
            "        </div>\n" +
            "        <div class=\"row\" style=\"margin:0\">\n" +
            "            <div class=\"col-md-12\">\n" +
            "                <section class=\"panel panel-default\">\n" +
            "                    <header class=\"panel-heading font-bold\" style=\"font-size: 16px;background-color:#dcd9d9;border-color: black;\">\n" +
            "                        <span class=\"label bg-success pull-right\"></span>面料信息\n" +
            "                    </header>\n" +
            "                    <div id=\"manufactureFabricTable\"></div>\n" +
            "                </section>\n" +
            "            </div>\n" +
            "        </div>\n" +
            "        <div class=\"row\" style=\"margin:0\">\n" +
            "            <div class=\"col-md-12\">\n" +
            "                <section class=\"panel panel-default\">\n" +
            "                    <header class=\"panel-heading font-bold\" style=\"font-size: 16px;background-color: #dcd9d9;border-color: black;\">\n" +
            "                        <span class=\"label bg-success pull-right\"></span>辅料信息\n" +
            "                    </header>\n" +
            "                    <div id=\"manufactureAccessoryTable\"></div>\n" +
            "                </section>\n" +
            "            </div>\n" +
            "        </div>\n" +
            "        <div style='page-break-after:always;text-align:center;'></div>\n"+
            "        <div class=\"row\" style=\"margin:0\">\n" +
            "            <div class=\"col-md-12\">\n" +
            "                <section class=\"panel panel-default\">\n" +
            "                    <header class=\"panel-heading font-bold\" style=\"font-size: 16px;background-color: #dcd9d9;border-color: black;\">\n" +
            "                        <span class=\"label bg-success pull-right\"></span>工艺尺寸\n" +
            "                    </header>\n" +
            "                    <div id=\"measureProcess\"></div>\n" +
            "                </section>\n" +
            "            </div>\n" +
            "        </div>\n" +
            "    </div>\n" +
            "</div>\n" +
            "    <style>\n" +
            "        section {\n" +
            "            border-color: #000000 !important;\n" +
            "        }\n" +
            "    </style>"
        , yes: function () {
            var newWin=window.open('about:blank', '', '');
            var html = '<html><head><title>'+pdfName+'</title></head><body><div>'+$("#contentPage").html()+'</div></body>' +
                '<style>' +
                'section {border:1px solid #000} .col-md-6,.row{margin: 0 0 20px 0 !important;}' +
                '</style></html>';
            newWin.document.write(html);
            newWin.document.close();
            newWin.print();

        }
        , cancel : function (i,layero) {
            $("#basicDetailInfo").empty();
            $("#styleImgDiv").empty();
            $("#orderClothesTable").empty();
            $("#manufactureFabricTable").empty();
            $("#manufactureAccessoryTable").empty();
            $("#measureProcess").empty();
            $("#stepForm").hide();
        }
    });
    layer.full(index);
    setTimeout(function () {
        $("#basicDetailInfo").empty();
        $("#styleImgDiv").empty();
        $("#orderClothesTable").empty();
        $("#manufactureFabricTable").empty();
        $("#manufactureAccessoryTable").empty();
        $("#measureProcess").empty();
        $.ajax({
            url: "/erp/getAllProcessDetail",
            type: 'GET',
            data: {
                orderName:orderName
            },
            success: function (res) {
                if(res.manufactureOrder) {
                    var tableHtml = "<table style='width: 100%; font-size: 13px; border: 1px solid black;margin-bottom: 0px;word-wrap:break-word;border-spacing:0 0' class='table'>";
                    tableHtml += "<tr><td style='font-weight: bolder; border: 1px solid black;'>名称:</td><td style='border: 1px solid black;'>"+ res.manufactureOrder.styleName +"</td><td style='font-weight: bolder; border: 1px solid black;'>品牌:</td><td style='border: 1px solid black;'>"+ res.manufactureOrder.customerName +"</td><td style='font-weight: bolder; border: 1px solid black;'>订购方式:</td><td style='border: 1px solid black;'>"+ res.manufactureOrder.purchaseMethod +"</td><td style='font-weight: bolder; border: 1px solid black;'>季度:</td><td style='border: 1px solid black;'>"+ res.manufactureOrder.season +"</td></tr>";
                    tableHtml += "<tr><td style='font-weight: bolder; border: 1px solid black;'>单号:</td><td style='border: 1px solid black;'>"+ res.manufactureOrder.clothesVersionNumber +"</td><td style='font-weight: bolder; border: 1px solid black;'>款号:</td><td style='border: 1px solid black;'>"+ res.manufactureOrder.orderName +"</td><td style='font-weight: bolder; border: 1px solid black;'>分组系列:</td><td style='border: 1px solid black;'>"+ res.manufactureOrder.modelType +"</td><td style='font-weight: bolder; border: 1px solid black;'>跟单:</td><td style='border: 1px solid black;'>"+ res.manufactureOrder.buyer +"</td></tr>";
                    tableHtml += "<tr><td style='font-weight: bolder; border: 1px solid black;'>收单日期:</td><td style='border: 1px solid black;'>"+ moment(res.manufactureOrder.deadLine).format("YYYY-MM-DD") +"</td><td style='font-weight: bolder; border: 1px solid black;'>交货日期:</td><td style='border: 1px solid black;'>"+ moment(res.manufactureOrder.deliveryDate).format("YYYY-MM-DD") +"</td><td style='font-weight: bolder; border: 1px solid black;'>允许加裁:</td><td style='border: 1px solid black;'>"+ res.manufactureOrder.additional +"%</td><td style='font-weight: bolder; border: 1px solid black;'>订单量:</td><td style='border: 1px solid black;'>"+ res.orderCount +"件</td></tr>";
                    tableHtml += "<tr><td style='font-weight: bolder; border: 1px solid black;'>颜色:</td><td colspan='3' style='border: 1px solid black;'>"+ res.manufactureOrder.colorInfo +"</td><td style='font-weight: bolder; border: 1px solid black;'>尺码:</td><td colspan='3' style='border: 1px solid black;'>"+ res.manufactureOrder.sizeInfo +"</td></tr>";
                    tableHtml += "<tr><td style='font-weight: bolder; border: 1px solid black;'>备注:</td><td colspan='7' style='border: 1px solid black;'>"+ res.manufactureOrder.remark +"</td></tr></table>";
                    $("#basicDetailInfo").append(tableHtml);
                }
                $("#styleImgDiv").empty();
                if(res.styleImage) {
                    $("#styleImgDiv").html(res.styleImage.imageText);
                    $("#styleImgDiv img").each(function () {
                        var heightStr = $(this).css("height");
                        var weightStr = $(this).css("width");
                        var hl = heightStr.indexOf('p');
                        var wl = weightStr.indexOf('p');
                        var initHeight = heightStr.substring(0, hl);
                        var initWidth = heightStr.substring(0, wl);
                        $(this).css("height", 290);
                        $(this).css("width", initWidth/initHeight * 290 - 30);
                    })
                }
                else {
                    $("#styleImgDiv").html("<h3>暂无款式图</h3>");
                }
                var sizeList = [];
                var colorList = [];
                var sizeSum = [];
                var totalWidth = $("#contentPage").width() - 60;
                var colorCount;
                if(res.orderClothesList) {
                    $.each(res.orderClothesList,function (index,item) {
                        if (sizeList.indexOf(item.sizeName) == -1){
                            sizeList.push(item.sizeName);
                        }
                        if (colorList.indexOf(item.colorName) == -1){
                            colorList.push(item.colorName);
                        }
                    });
                    sizeList = globalSizeSort(sizeList);
                    var widthNumber = Number(totalWidth/(sizeList.length + 2));
                    colorCount = colorList.length;
                    var title = [
                        {field: 'colorName', title: '', width: widthNumber, align: 'center',totalRowText: '合计'}
                    ];
                    $.each(sizeList,function (index,item) {
                        title.push({
                            field: item, title: item, width: widthNumber, align: 'center', totalRow: true
                        });
                        sizeSum.push(0);
                    });
                    title.push({
                        field: 'colorSum', title: '合计', width: widthNumber, align: 'center', totalRow: true
                    });
                    var orderClothesData = [];
                    $.each(colorList,function (index2,item2) {
                        var dataLine = {};
                        var colorSum = 0;
                        dataLine.colorName = item2;
                        $.each(sizeList,function (index3,item3) {
                            var flag = true;
                            $.each(res.orderClothesList,function (index1,item1) {
                                if (item1.colorName == item2 && item1.sizeName == item3){
                                    dataLine[item3] = item1.count;
                                    flag = false;
                                    colorSum += item1.count;
                                }
                                if (flag){
                                    dataLine[item3] = 0;
                                }
                            });
                        });
                        dataLine.colorSum = colorSum;
                        orderClothesData.push(dataLine);
                    });
                    var tableHtml = "<table style='width: 100%; font-size: 13px; border: 1px solid black;margin-bottom: 0px;word-wrap:break-word;border-spacing:0 0' class='table'>";
                    tableHtml += "<tr><td style='font-weight: bolder; border: 1px solid black;'>颜色</td>";
                    $.each(sizeList,function (index,item) {
                        tableHtml += "<td style='font-weight: bolder; border: 1px solid black;'>"+item+"</td>";
                    });
                    tableHtml += "<td style='font-weight: bolder; border: 1px solid black;'>合计</td></tr>";
                    $.each(orderClothesData,function (index,item) {
                        tableHtml += "<tr><td style='border: 1px solid black;'>"+item.colorName+"</td>";
                        $.each(sizeList,function(sIndex, sItem) {
                            tableHtml += "<td style='border: 1px solid black;'>"+item[sItem]+"</td>";
                            sizeSum[sIndex] += item[sItem];
                        });
                        tableHtml += "<td style='border: 1px solid black;'>"+item.colorSum+"</td></tr>";
                    });
                    tableHtml += "<tr><td style='border: 1px solid black;'>合计</td>";
                    var totalSum = 0;
                    for (var m = 0; m < sizeSum.length; m ++){
                        tableHtml += "<td style='border: 1px solid black;'>"+ sizeSum[m] +"</td>";
                        totalSum += sizeSum[m];
                    }
                    tableHtml += "<td style='border: 1px solid black;'>" + totalSum +"</td></tr>";
                    $("#orderClothesTable").append(tableHtml);

                }
                else {
                    $("#orderClothesTable").append("无制单数据");
                }
                if(res.manufactureFabricList) {
                    var data = res.manufactureFabricList;
                    var fabricHtml = "<table style='width: 100%; font-size: 9px; border: 1px solid black;margin-bottom: 0px;word-wrap:break-word;border-spacing:0 0' class='table'>";
                    fabricHtml += "<tr><td style='font-weight: bolder; border: 1px solid black;'>部位</td><td style='font-weight: bolder; border: 1px solid black;width: '20%'>名称</td><td style='font-weight: bolder; border: 1px solid black;'>编号</td><td style='font-weight: bolder; border: 1px solid black;'>布封(cm)</td><td style='font-weight: bolder; border: 1px solid black;'>克重(g)</td><td style='font-weight: bolder; border: 1px solid black;'>单位</td><td style='font-weight: bolder; border: 1px solid black;'>色组</td><td style='font-weight: bolder; border: 1px solid black;'>面料颜色</td><td style='font-weight: bolder; border: 1px solid black;'>单耗(米)</td><td style='font-weight: bolder; border: 1px solid black;'>损耗</td><td style='font-weight: bolder; border: 1px solid black;'>订单量</td><td style='font-weight: bolder; border: 1px solid black;'>总用量</td></tr>";
                    for (var i=0; i < data.length; i ++){
                        fabricHtml += "<tr><td style='border: 1px solid black;'>"+data[i].partName+"</td><td style='border: 1px solid black;'>"+data[i].fabricName+"</td><td style='border: 1px solid black;'>"+data[i].fabricNumber+"</td><td style='border: 1px solid black;'>"+data[i].fabricWidth+"</td><td style='border: 1px solid black;'>"+data[i].fabricWeight+"</td><td style='border: 1px solid black;'>"+data[i].unit+"</td><td style='border: 1px solid black;'>"+data[i].colorName+"</td><td style='border: 1px solid black;'>"+data[i].fabricColor+"</td><td style='border: 1px solid black;'>"+data[i].pieceUsage+"</td><td style='border: 1px solid black;'>"+data[i].fabricLoss+"%</td><td style='border: 1px solid black;'>"+data[i].orderCount+"</td><td style='border: 1px solid black;'>"+ toDecimal(data[i].fabricActCount)+"</td></tr>";
                    }
                    fabricHtml += "</table>";
                    $("#manufactureFabricTable").append(fabricHtml);
                }
                else {
                    $("#manufactureFabricTable").append("面料未入");
                }
                if(res.manufactureAccessoryList) {
                    var data = res.manufactureAccessoryList;
                    var accessoryHtml = "<table style='width: 100%; font-size: 9px; border: 1px solid black;margin-bottom: 0px;word-wrap:break-word;border-spacing:0 0' class='table'>";
                    accessoryHtml += "<tr><td style='font-weight: bolder; border: 1px solid black;'>名称</td><td style='font-weight: bolder; border: 1px solid black;'>编号</td><td style='font-weight: bolder; border: 1px solid black;'>规格</td><td style='font-weight: bolder; border: 1px solid black;'>颜色</td><td style='font-weight: bolder; border: 1px solid black;'>单位</td><td style='font-weight: bolder; border: 1px solid black;'>单耗</td><td style='font-weight: bolder; border: 1px solid black;'>损耗</td><td style='font-weight: bolder; border: 1px solid black;'>色组</td><td style='font-weight: bolder; border: 1px solid black;'>尺码</td><td style='font-weight: bolder; border: 1px solid black;'>订单量</td><td style='font-weight: bolder; border: 1px solid black;'>总用量</td></tr>";
                    for (var i=0; i < data.length; i ++){
                        accessoryHtml += "<tr><td style='border: 1px solid black;'>"+data[i].accessoryName+"</td><td style='border: 1px solid black;'>"+data[i].accessoryNumber+"</td><td style='border: 1px solid black;'>"+data[i].specification+"</td><td style='border: 1px solid black;'>"+data[i].accessoryColor+"</td><td style='border: 1px solid black;'>"+data[i].accessoryUnit+"</td><td style='border: 1px solid black;'>"+data[i].pieceUsage+"</td><td style='border: 1px solid black;'>"+data[i].accessoryAdd+"%</td><td style='border: 1px solid black;'>"+data[i].colorName+"</td><td style='border: 1px solid black;'>"+data[i].sizeName+"</td><td style='border: 1px solid black;'>"+data[i].orderCount+"</td><td style='border: 1px solid black;'>"+toDecimal(data[i].accessoryCount)+"</td></tr>";
                    }
                    accessoryHtml += "</table>";
                    $("#manufactureAccessoryTable").append(accessoryHtml);

                    // var data = res.manufactureAccessoryList;
                    // var accessoryHtml = "<table style='width: 100%; font-size: 13px; border: 1px solid black;margin-bottom: 0px;word-wrap:break-word;border-spacing:0 0' class='table'>";
                    // var widthNumber = 6 + sizeList.length;
                    // if (res.sewAccessoryNameList){
                    //     var sewAccessoryNameList = res.sewAccessoryNameList;
                    //     accessoryHtml += "<tr><td style='font-weight: bolder; border: 1px solid black; text-align: center' colspan='"+ widthNumber +"'>车缝辅料</td>";
                    //     accessoryHtml += "<tr><td style='font-weight: bolder; border: 1px solid black;'>名称</td><td style='font-weight: bolder; border: 1px solid black;'>编号</td><td style='font-weight: bolder; border: 1px solid black;'>单位</td><td style='font-weight: bolder; border: 1px solid black;'>单耗</td><td style='font-weight: bolder; border: 1px solid black;'>色组</td><td style='font-weight: bolder; border: 1px solid black;'>颜色属性</td>";
                    //     $.each(sizeList,function (index,item) {
                    //         accessoryHtml += "<td style='font-weight: bolder; border: 1px solid black;'>" + item + "</td>";
                    //     });
                    //     accessoryHtml += "</tr>";
                    //     $.each(sewAccessoryNameList,function (n_index, n_item) {
                    //         if (n_item.colorChild === 1 && n_item.sizeChild === 1){
                    //             var line = "";
                    //             var colorFlag = false;
                    //             $.each(colorList,function (c_index,c_item) {
                    //                 line += "<tr><td style='font-weight: bolder; border: 1px solid black;'>"+ n_item.accessoryName + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.accessoryNumber + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.accessoryUnit + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + toDecimal(n_item.pieceUsage) + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + c_item + "</td>";
                    //                 var cpFlag = false;
                    //                 $.each(data, function (d_index, d_item) {
                    //                     if (d_item.accessoryName == n_item.accessoryName && d_item.colorName == c_item && cpFlag == false){
                    //                         cpFlag = true;
                    //                         line += "<td style='font-weight: bolder; border: 1px solid black;'>"+ d_item.colorProperty + "</td>";
                    //                     }
                    //                 });
                    //                 if (!cpFlag){
                    //                     line += "<td style='font-weight: bolder; border: 1px solid black;'></td>";
                    //                 }
                    //                 $.each(sizeList,function (s_index,s_item) {
                    //                     var sizeFlag = false;
                    //                     $.each(data, function (d_index, d_item) {
                    //                         if (d_item.accessoryName == n_item.accessoryName && d_item.colorName == c_item && d_item.sizeName == s_item){
                    //                             colorFlag = true;
                    //                             sizeFlag = true;
                    //                             line += "<td style='font-weight: bolder; border: 1px solid black;'>"+ d_item.sizeProperty + "</td>";
                    //                         }
                    //                     });
                    //                     if (!sizeFlag){
                    //                         line += "<td style='font-weight: bolder; border: 1px solid black;'>/</td>";
                    //                     }
                    //                 });
                    //                 if (colorFlag){
                    //                     line += "</tr>";
                    //                 }
                    //             });
                    //             accessoryHtml += line;
                    //         }
                    //         if (n_item.colorChild === 1 && n_item.sizeChild === 0){
                    //             var line = "";
                    //             var colorFlag = false;
                    //             $.each(colorList,function (c_index,c_item) {
                    //                 line += "<tr><td style='font-weight: bolder; border: 1px solid black;'>"+ n_item.accessoryName + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.accessoryNumber + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.accessoryUnit + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + toDecimal(n_item.pieceUsage) + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + c_item + "</td>";
                    //                 var cpFlag = false;
                    //                 $.each(data, function (d_index, d_item) {
                    //                     if (d_item.accessoryName == n_item.accessoryName && d_item.colorName == c_item && cpFlag == false){
                    //                         cpFlag = true;
                    //                         line += "<td style='font-weight: bolder; border: 1px solid black;'>"+ d_item.colorProperty + "</td>";
                    //                     }
                    //                 });
                    //                 if (!cpFlag){
                    //                     line += "<td style='font-weight: bolder; border: 1px solid black;'></td>";
                    //                 }
                    //                 $.each(sizeList,function (s_index,s_item) {
                    //                     var sizeFlag = false;
                    //                     $.each(data, function (d_index, d_item) {
                    //                         if (d_item.accessoryName == n_item.accessoryName && d_item.colorName == c_item && d_item.sizeName == s_item){
                    //                             colorFlag = true;
                    //                             sizeFlag = true;
                    //                             line += "<td style='font-weight: bolder; border: 1px solid black;'>通用</td>";
                    //                         }
                    //                     });
                    //                     if (!sizeFlag){
                    //                         line += "<td style='font-weight: bolder; border: 1px solid black;'>/</td>";
                    //                     }
                    //                 });
                    //                 if (colorFlag){
                    //                     line += "</tr>";
                    //                 }
                    //             });
                    //             accessoryHtml += line;
                    //         }
                    //         if (n_item.colorChild === 0 && n_item.sizeChild === 1){
                    //             var line = "<tr><td style='font-weight: bolder; border: 1px solid black;'>"+ n_item.accessoryName + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.accessoryNumber + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.accessoryUnit + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + toDecimal(n_item.pieceUsage) + "</td><td style='font-weight: bolder; border: 1px solid black;'>通用</td>";
                    //             var cpFlag = false;
                    //             $.each(data, function (d_index, d_item) {
                    //                 if (d_item.accessoryName == n_item.accessoryName && cpFlag == false){
                    //                     cpFlag = true;
                    //                     line += "<td style='font-weight: bolder; border: 1px solid black;'>"+ d_item.colorProperty + "</td>";
                    //                 }
                    //             });
                    //             if (!cpFlag){
                    //                 line += "<td style='font-weight: bolder; border: 1px solid black;'>/</td>";
                    //             }
                    //             $.each(sizeList,function (s_index,s_item) {
                    //                 var sizeFlag = false;
                    //                 $.each(data, function (d_index, d_item) {
                    //                     if (d_item.accessoryName == n_item.accessoryName  && d_item.sizeName == s_item){
                    //                         sizeFlag = true;
                    //                         line += "<td style='font-weight: bolder; border: 1px solid black;'>"+ d_item.sizeProperty + "</td>";
                    //                     }
                    //                 });
                    //                 if (!sizeFlag){
                    //                     line += "<td style='font-weight: bolder; border: 1px solid black;'>/</td>";
                    //                 }
                    //             });
                    //             line += "</tr>";
                    //             accessoryHtml += line;
                    //         }
                    //         if (n_item.colorChild === 0 && n_item.sizeChild === 0){
                    //             var line = "<tr><td style='font-weight: bolder; border: 1px solid black;'>"+ n_item.accessoryName + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.accessoryNumber + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.accessoryUnit + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + toDecimal(n_item.pieceUsage) + "</td><td style='font-weight: bolder; border: 1px solid black;'>通用</td>";
                    //             var cpFlag = false;
                    //             $.each(data, function (d_index, d_item) {
                    //                 if (d_item.accessoryName == n_item.accessoryName && cpFlag == false){
                    //                     cpFlag = true;
                    //                     line += "<td style='font-weight: bolder; border: 1px solid black;'>"+ d_item.colorProperty + "</td>";
                    //                 }
                    //             });
                    //             if (!cpFlag){
                    //                 line += "<td style='font-weight: bolder; border: 1px solid black;'>/</td>";
                    //             }
                    //             $.each(sizeList,function (s_index,s_item) {
                    //                 var sizeFlag = false;
                    //                 $.each(data, function (d_index, d_item) {
                    //                     if (d_item.accessoryName == n_item.accessoryName && d_item.sizeName == s_item){
                    //                         sizeFlag = true;
                    //                         line += "<td style='font-weight: bolder; border: 1px solid black;'>通用</td>";
                    //                     }
                    //                 });
                    //                 if (!sizeFlag){
                    //                     line += "<td style='font-weight: bolder; border: 1px solid black;'>/</td>";
                    //                 }
                    //             });
                    //             line += "</tr>";
                    //             accessoryHtml += line;
                    //         }
                    //     });
                    // }
                    // if (res.packAccessoryNameList){
                    //     var packAccessoryNameList = res.packAccessoryNameList;
                    //     accessoryHtml += "<tr><td style='font-weight: bolder; border: 1px solid black; text-align: center' colspan='"+ widthNumber +"'>包装辅料</td>";
                    //     accessoryHtml += "<tr><td style='font-weight: bolder; border: 1px solid black;'>名称</td><td style='font-weight: bolder; border: 1px solid black;'>编号</td><td style='font-weight: bolder; border: 1px solid black;'>单位</td><td style='font-weight: bolder; border: 1px solid black;'>单耗</td><td style='font-weight: bolder; border: 1px solid black;'>色组</td><td style='font-weight: bolder; border: 1px solid black;'>颜色属性</td>";
                    //     $.each(sizeList,function (index,item) {
                    //         accessoryHtml += "<td style='font-weight: bolder; border: 1px solid black;'>" + item + "</td>";
                    //     });
                    //     accessoryHtml += "</tr>";
                    //     $.each(packAccessoryNameList,function (n_index, n_item) {
                    //         if (n_item.colorChild === 1 && n_item.sizeChild === 1){
                    //             var line = "";
                    //             var colorFlag = false;
                    //             $.each(colorList,function (c_index,c_item) {
                    //                 line += "<tr><td style='font-weight: bolder; border: 1px solid black;'>"+ n_item.accessoryName + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.accessoryNumber + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.accessoryUnit + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + toDecimal(n_item.pieceUsage) + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + c_item + "</td>";
                    //                 var cpFlag = false;
                    //                 $.each(data, function (d_index, d_item) {
                    //                     if (d_item.accessoryName == n_item.accessoryName && d_item.colorName == c_item && cpFlag == false){
                    //                         cpFlag = true;
                    //                         line += "<td style='font-weight: bolder; border: 1px solid black;'>"+ d_item.colorProperty + "</td>";
                    //                     }
                    //                 });
                    //                 if (!cpFlag){
                    //                     line += "<td style='font-weight: bolder; border: 1px solid black;'></td>";
                    //                 }
                    //                 $.each(sizeList,function (s_index,s_item) {
                    //                     var sizeFlag = false;
                    //                     $.each(data, function (d_index, d_item) {
                    //                         if (d_item.accessoryName == n_item.accessoryName && d_item.colorName == c_item && d_item.sizeName == s_item){
                    //                             colorFlag = true;
                    //                             sizeFlag = true;
                    //                             line += "<td style='font-weight: bolder; border: 1px solid black;'>"+ d_item.sizeProperty + "</td>";
                    //                         }
                    //                     });
                    //                     if (!sizeFlag){
                    //                         line += "<td style='font-weight: bolder; border: 1px solid black;'>/</td>";
                    //                     }
                    //                 });
                    //                 if (colorFlag){
                    //                     line += "</tr>";
                    //                 }
                    //             });
                    //             accessoryHtml += line;
                    //         }
                    //         if (n_item.colorChild === 1 && n_item.sizeChild === 0){
                    //             var line = "";
                    //             var colorFlag = false;
                    //             $.each(colorList,function (c_index,c_item) {
                    //                 line += "<tr><td style='font-weight: bolder; border: 1px solid black;'>"+ n_item.accessoryName + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.accessoryNumber + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.accessoryUnit + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + toDecimal(n_item.pieceUsage) + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + c_item + "</td>";
                    //                 var cpFlag = false;
                    //                 $.each(data, function (d_index, d_item) {
                    //                     if (d_item.accessoryName == n_item.accessoryName && d_item.colorName == c_item && cpFlag == false){
                    //                         cpFlag = true;
                    //                         line += "<td style='font-weight: bolder; border: 1px solid black;'>"+ d_item.colorProperty + "</td>";
                    //                     }
                    //                 });
                    //                 if (!cpFlag){
                    //                     line += "<td style='font-weight: bolder; border: 1px solid black;'></td>";
                    //                 }
                    //                 $.each(sizeList,function (s_index,s_item) {
                    //                     var sizeFlag = false;
                    //                     $.each(data, function (d_index, d_item) {
                    //                         if (d_item.accessoryName == n_item.accessoryName && d_item.colorName == c_item && d_item.sizeName == s_item){
                    //                             colorFlag = true;
                    //                             sizeFlag = true;
                    //                             line += "<td style='font-weight: bolder; border: 1px solid black;'>通用</td>";
                    //                         }
                    //                     });
                    //                     if (!sizeFlag){
                    //                         line += "<td style='font-weight: bolder; border: 1px solid black;'>/</td>";
                    //                     }
                    //                 });
                    //                 if (colorFlag){
                    //                     line += "</tr>";
                    //                 }
                    //             });
                    //             accessoryHtml += line;
                    //         }
                    //         if (n_item.colorChild === 0 && n_item.sizeChild === 1){
                    //             var line = "<tr><td style='font-weight: bolder; border: 1px solid black;'>"+ n_item.accessoryName + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.accessoryNumber + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.accessoryUnit + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + toDecimal(n_item.pieceUsage) + "</td><td style='font-weight: bolder; border: 1px solid black;'>通用</td>";
                    //             var cpFlag = false;
                    //             $.each(data, function (d_index, d_item) {
                    //                 if (d_item.accessoryName == n_item.accessoryName && cpFlag == false){
                    //                     cpFlag = true;
                    //                     line += "<td style='font-weight: bolder; border: 1px solid black;'>"+ d_item.colorProperty + "</td>";
                    //                 }
                    //             });
                    //             if (!cpFlag){
                    //                 line += "<td style='font-weight: bolder; border: 1px solid black;'>/</td>";
                    //             }
                    //             $.each(sizeList,function (s_index,s_item) {
                    //                 var sizeFlag = false;
                    //                 $.each(data, function (d_index, d_item) {
                    //                     if (d_item.accessoryName == n_item.accessoryName  && d_item.sizeName == s_item){
                    //                         sizeFlag = true;
                    //                         line += "<td style='font-weight: bolder; border: 1px solid black;'>"+ d_item.sizeProperty + "</td>";
                    //                     }
                    //                 });
                    //                 if (!sizeFlag){
                    //                     line += "<td style='font-weight: bolder; border: 1px solid black;'>/</td>";
                    //                 }
                    //             });
                    //             line += "</tr>";
                    //             accessoryHtml += line;
                    //         }
                    //         if (n_item.colorChild === 0 && n_item.sizeChild === 0){
                    //             var line = "<tr><td style='font-weight: bolder; border: 1px solid black;'>"+ n_item.accessoryName + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.accessoryNumber + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + n_item.accessoryUnit + "</td><td style='font-weight: bolder; border: 1px solid black;'>" + toDecimal(n_item.pieceUsage) + "</td><td style='font-weight: bolder; border: 1px solid black;'>通用</td>";
                    //             var cpFlag = false;
                    //             $.each(data, function (d_index, d_item) {
                    //                 if (d_item.accessoryName == n_item.accessoryName && cpFlag == false){
                    //                     cpFlag = true;
                    //                     line += "<td style='font-weight: bolder; border: 1px solid black;'>"+ d_item.colorProperty + "</td>";
                    //                 }
                    //             });
                    //             if (!cpFlag){
                    //                 line += "<td style='font-weight: bolder; border: 1px solid black;'>/</td>";
                    //             }
                    //             $.each(sizeList,function (s_index,s_item) {
                    //                 var sizeFlag = false;
                    //                 $.each(data, function (d_index, d_item) {
                    //                     if (d_item.accessoryName == n_item.accessoryName && d_item.sizeName == s_item){
                    //                         sizeFlag = true;
                    //                         line += "<td style='font-weight: bolder; border: 1px solid black;'>通用</td>";
                    //                     }
                    //                 });
                    //                 if (!sizeFlag){
                    //                     line += "<td style='font-weight: bolder; border: 1px solid black;'>/</td>";
                    //                 }
                    //             });
                    //             line += "</tr>";
                    //             accessoryHtml += line;
                    //         }
                    //     });
                    // }
                    // accessoryHtml += "</table>";
                    // $("#manufactureAccessoryTable").append(accessoryHtml);
                }else {
                    $("#manufactureAccessoryTable").append("辅料未入");
                }
                $("#measureProcess").empty();
                if(res.orderMeasure) {
                    $("#measureProcess").html(res.orderMeasure.orderMeasureImg);
                } else {
                    $("#measureProcess").html("<h3>尺寸工艺未录入</h3>");
                }
            },
            error: function () {
                layer.msg("获取详情信息失败！", {icon: 2});
                layer.close(index);
            }
        })
    },50)
}
function handleImgUpload(blobInfo, success, failure){
    var formdata = new FormData();
    // append 方法中的第一个参数就是 我们要上传文件 在后台接收的文件名
    // 这个值要根据后台来定义
    // 第二个参数是我们上传的文件
    formdata.append('file', blobInfo.blob());
    $.ajax({
        url: "/erp/wangEditorUploadImg",
        type: 'post',
        dataType:'json',
        contentType:false,//ajax上传图片需要添加
        processData:false,//ajax上传图片需要添加
        data: formdata,
        success: function (res) {
            if(res.data) {
                console.log(res)
                success(res.data[0]);
            }else {
                failure("上传图片失败");
            }
        },
        error: function () {
            layer.msg("上传图片失败", { icon: 2 });
        }
    });

}
function renderSeasonDate(ohd, sgl) {
    var ele = $(ohd);
    $Date.render({
        elem: ohd,
        type: 'month',
        format: 'yyyy年M季度',
        range: sgl ? null : '~',
        min: "1900-1-1",
        max: "2099-12-31",
        btns: ['clear', 'confirm'],
        ready: function (value, date, endDate) {
            var hd = $("#layui-laydate" + ele.attr("lay-key"));
            if (hd.length > 0) {
                hd.click(function () {
                    ren($(this));
                });
            }
            ren(hd);
        },
        done: function (value, date, endDate) {
            if (!isNull(date) && date.month > 0 && date.month < 5) {
                ele.attr("startDate", date.year + "-" + date.month);
            } else {
                ele.attr("startDate", "");
            }
            if (!isNull(endDate) && endDate.month > 0 && endDate.month < 5) {
                ele.attr("endDate", endDate.year + "-" + endDate.month)
            } else {
                ele.attr("endDate", "");
            }
        }
    });
    var ren = function (thiz) {
        var mls = thiz.find(".laydate-month-list");
        mls.each(function (i, e) {
            $(this).find("li").each(function (inx, ele) {
                var cx = ele.innerHTML;
                if (inx < 4) {
                    ele.innerHTML = cx.replace(/月/g, "季度");
                } else {
                    ele.style.display = "none";
                }
            });
        });
    }
}
function initDateForm(sgl, form) {
    if (isNull(form)) form = $(document.body);
    var ltm = function (tar, tars, tva) {
        tars.each(function () {
            $(this).removeAttr("lay-key");
            this.outerHTML = this.outerHTML;
        });
        tars = form.find(".dateTarget" + tar);
        tars.each(function () {
            var ele = $(this);
            if ("y" == tva) {
                $Date.render({
                    elem: this,
                    type: 'year',
                    range: '~',
                    format: 'yyyy年',
                    range: sgl ? null : '~',
                    done: function (value, date, endDate) {
                        if (typeof (date.year) == "number") ele.attr("startDate", date.year);
                        else ele.attr("startDate", "");
                        if (typeof (endDate.year) == "number") ele.attr("endDate", endDate.year);
                        else ele.attr("endDate", "");
                    }
                });

            } else if ("s" == tva) {
                ele.attr("startDate", "");
                ele.attr("endDate", "");
                renderSeasonDate(this, sgl);
            } else if ("m" == tva) {
                ele.attr("startDate", "");
                ele.attr("endDate", "");
                $Date.render({
                    elem: this,
                    type: 'month',
                    range: '~',
                    format: 'yyyy年MM月',
                    range: sgl ? null : '~',
                    done: function (value, date, endDate) {
                        if (typeof (date.month) == "number") ele.attr("startDate", date.year + "-" + date.month);
                        else ele.attr("startDate", "");
                        if (typeof (endDate.month) == "number") ele.attr("endDate", endDate.year + "-" + endDate.month);
                        else ele.attr("endDate", "");
                    }
                });
            } else if ("d" == tva) {
                ele.attr("startDate", "");
                ele.attr("endDate", "");
                $Date.render({
                    elem: this,
                    range: '~',
                    format: 'yyyy年MM月dd日',
                    range: sgl ? null : '~',
                    done: function (value, date, endDate) {
                        if (typeof (date.date) == "number") ele.attr("startDate", date.year + "-" + date.month + "-" + date.date);
                        else ele.attr("startDate", "");
                        if (typeof (endDate.date) == "number") ele.attr("endDate", endDate.year + "-" + endDate.month + "-" +
                            endDate.date);
                        else ele.attr("endDate", "");
                    }
                });
            }
        });
    }
    var sels = form.find(".dateSelector");
    sels.each(function (i, e) {
        var ths = this;
        var thiz = $(e);
        var tar = thiz.attr("date-target");
        thiz.next().find("dd").click(function () {
            var tva = thiz.val();
            var tars = form.find(".dateTarget" + tar);
            ltm(tar, tars, tva);
        });
        thiz.change(function () {
            var tva = $(this).val();
            var tars = form.find(".dateTarget" + tar);
            ltm(tar, tars, tva);
        });
        var tars = form.find(".dateTarget" + tar);
        ltm(tar, tars, thiz.val());
    });
}
function isNull(s) {
    if (s == null || typeof (s) == "undefined" || s == "") return true;
    return false;
}
function printDeal(){
    $("#printf").empty();
    var printBoxs = document.getElementsByName('printTable');
    var newContent = '';
    for(var i=0;i<printBoxs.length;i++) {
        newContent += printBoxs[i].innerHTML;
    }
    var f = document.getElementById('printf');
    // f.contentDocument.write('<link rel="stylesheet" type="text/css" href="/css/app.v2.css">');
    f.contentDocument.write('<style>' +
        'table {' +
        'border-collapse:collapse;' +
        '}' +
        'td {\n' +
        '        border: 1px solid #000000;\n' +
        '    }'+
        '</style>');
    f.contentDocument.write(newContent);
    f.contentDocument.close();
    window.frames['printf'].focus();
    try{
        window.frames['printf'].print();
    }catch(err){
        f.contentWindow.print();
    }
}
function unique(arr) {
    return arr.filter(function(item, index, arr) {
        //当前元素，在原始数组中的第一个索引==当前索引值，否则返回当前元素
        return arr.indexOf(item, 0) === index;
    });
}
function isNumber(val){

    var regPos = /^\d+(\.\d+)?$/; //非负浮点数
    var regNeg = /^(-(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*)))$/; //负浮点数
    if (regPos.test(val) || regNeg.test(val)){
        return true;
    }else{
        return false;
    }

}
function toDecimal(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*100)/100;
    return f;
}
function dateFormat(fmt, date) {
    var ret;
    var opt = {
        "Y+": date.getFullYear().toString(),        // 年
        "m+": (date.getMonth() + 1).toString(),     // 月
        "d+": date.getDate().toString(),            // 日
        "H+": date.getHours().toString(),           // 时
        "M+": date.getMinutes().toString(),         // 分
        "S+": date.getSeconds().toString()          // 秒
        // 有其他格式化字符需求可以继续添加，必须转化成字符串
    };
    for (var k in opt) {
        ret = new RegExp("(" + k + ")").exec(fmt);
        if (ret) {
            fmt = fmt.replace(ret[1], (ret[1].length == 1) ? (opt[k]) : (opt[k].padStart(ret[1].length, "0")))
        }
    }
    return fmt;
}
function getNowFormatDate() {

    var date = new Date( );
    var seperator1 = "-";
    var seperator2 = ":";
    var month = date.getMonth() + 1;
    var strDate = date.getDate();
    if (month >= 1 && month <= 9) {
        month = "0" + month;
    }
    if (strDate >= 0 && strDate <= 9) {
        strDate = "0" + strDate;
    }
    var currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate
        + " " + date.getHours() + seperator2 + date.getMinutes()
        + seperator2 + date.getSeconds();
    return currentdate;
}
function moveCursorColor(event,obj) {
    if(event.keyCode==38){
        var name = $(obj).attr("name");
        var colorLength = thisColorNameList.length;
        $.each(thisColorNameList,function (index, item) {
            if (item === name){
                var lastIndex = index == 0 ? colorLength - 1 : index - 1;
                var tmpCheckName = "#" + colorEditId + " input[name='" + thisColorNameList[lastIndex] + "']";
                $(tmpCheckName).focus();
            }
        });
    }else if(event.keyCode==40){
        var name = $(obj).attr("name");
        var colorLength = thisColorNameList.length;
        $.each(thisColorNameList,function (index, item) {
            if (item === name){
                var nextIndex = index == colorLength - 1 ? 0 : index + 1;
                var tmpCheckName = "#" + colorEditId + " input[name='" + thisColorNameList[nextIndex] + "']";
                $(tmpCheckName).focus();
            }
        });
    }
}