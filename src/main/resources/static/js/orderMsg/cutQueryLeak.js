var hot;
var basePath=$("#basePath").val();

layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.21'
}).extend();

$(document).ready(function () {
    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumber", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });

    layui.laydate.render({
        elem: '#from',
        trigger: 'click'
    });
    layui.laydate.render({
        elem: '#to',
        trigger: 'click'
    });


    $("#orderName").bind('blur change',function(){
        var orderName = $("#orderName").val();
        $("#colorName").empty();
        $.ajax({
            url: "/erp/getordercolornamesbyorder",
            data: {"orderName": orderName},
            success:function(data){
                $("#colorName").empty();
                if (data.colorNameList) {
                    console.log(data.colorNameList);
                    $("#colorName").empty();
                    $("#colorName").append("<option value=''>选择颜色</option>");
                    $.each(data.colorNameList, function(index,element){
                        $("#colorName").append("<option value='"+element+"'>"+element+"</option>");
                    })
                    form.render();
                }
            },
            error:function(){
            }
        });
    });

    $("#clothesVersionNumber").bind('blur change',function(){
        var orderName = $("#orderName").val();
        $("#colorName").empty();
        $.ajax({
            url: "/erp/getordercolornamesbyorder",
            data: {"orderName": orderName},
            success:function(data){
                $("#colorName").empty();
                if (data.colorNameList) {
                    $("#colorName").empty();
                    $("#colorName").append("<option value=''>选择颜色</option>");
                    $.each(data.colorNameList, function(index,element){
                        $("#colorName").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });
    });

    var detailOrderName = $("#detailOrderName").val();
    var detailClothesVersionNumber = $("#detailClothesVersionNumber").val();
    if (detailOrderName != null && detailOrderName != ""){
        $("#orderName").val(detailOrderName);
        $("#clothesVersionNumber").val(detailClothesVersionNumber);
        search();
    }
});

function search() {
    var container = document.getElementById('addOrderExcel');
    if(hot==undefined) {
        hot = new Handsontable(container, {
            // data: data,
            rowHeaders: true,
            colHeaders: true,
            autoColumnSize: true,
            dropdownMenu: true,
            contextMenu: true,
            stretchH: 'all',
            autoWrapRow: true,
            height: $(document.body).height() - 200,
            manualRowResize: true,
            manualColumnResize: true,
            // minRows: 20,
            minCols: 22,
            colWidths:[100,100,60,60,60,60,60,60,60,60,60,60,60,60,60,60,60,60,60,60,60,60],
            language: 'zh-CN',
            licenseKey: 'non-commercial-and-evaluation'
        });
    }
    if($("#orderName").val().trim()=="") {
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入要查询的订单号！</span>",html: true});
        return false;
    }
    var param = {};
    param.orderName = $("#orderName").val();
    if ($("#colorName").val() != null && $("#colorName").val() != ''){
        param.colorName = $("#colorName").val()
    }
    var from = $("#from").val();
    var to = $("#to").val();
    if (from != null && from != ''){
        param.from = from;
    }
    if (to != null && to != ''){
        to.from = to;
    }
    var load = layer.load();
    $.ajax({
        url: basePath+"erp/searchCutQueryLeak",
        type:'GET',
        data: param,
        success: function (data) {
            $("#exportDiv").show();
            var hotData = [];
            if(data) {
                var dataOne = data["part"];
                var dataTwo = data["partTime"];
                for(var partKey in dataOne) {
                    if (partKey === '主身布'){
                        var partIndex = 0;
                        var partRow = ["部位名称",partKey];
                        hotData.push(partRow);
                        var colorData = dataOne[partKey];
                        var partPlanSum = 0;
                        var partActualSum = 0;
                        var partWellSum = 0;
                        var partDifferenceSum = 0;
                        var sizeSummary1 = [];
                        sizeSummary1[0] = [];
                        sizeSummary1[1] = ["订单尺码小计"];
                        var sizeSummary2 = [];
                        sizeSummary2[0] = [];
                        sizeSummary2[1] = ["裁剪尺码小计"];
                        var sizeSummary3 = [];
                        sizeSummary3[0] = [];
                        sizeSummary3[1] = ["好片尺码小计"];
                        var sizeSummaryIndex = 0;
                        for (var colorKey in colorData){
                            var colorRow1 = [];
                            var colorRow2 = [];
                            var colorRow3 = [];
                            var colorRow4 = [];
                            var colorRow5 = [];
                            colorRow2[0] = [];
                            colorRow3[0] = [];
                            colorRow4[0] = [];
                            colorRow5[0] = [];
                            colorRow1.push(colorKey);
                            colorRow1.push("订单量");
                            colorRow2.push("裁剪数量");
                            colorRow3.push("好片数量");
                            colorRow4.push("对比");
                            colorRow5.push("对比百分比");
                            var sizeData = colorData[colorKey];
                            var sizeRow = [];
                            var colorPlanSum = 0;
                            var colorActualSum = 0;
                            var colorWellSum = 0;
                            var colorDifferenceSum = 0;

                            var sizeSumIndex = 2;

                            var sizeDataList = [];
                            for (var i in sizeData){
                                var sizeDataItem = {};
                                sizeDataItem.sizeName = i;
                                sizeDataItem.sizeDataDetail = sizeData[i];
                                sizeDataList.push(sizeDataItem);
                            }
                            sizeDataList = globalSizeDataSort(sizeDataList);
                            for (var i in sizeDataList) {
                                if (partIndex == 0) {
                                    sizeRow[0] = [];
                                    sizeRow[1] = [];
                                    sizeRow.push(sizeDataList[i].sizeName);
                                }
                                colorRow1.push(sizeDataList[i].sizeDataDetail.orderCount);
                                colorPlanSum += sizeDataList[i].sizeDataDetail.orderCount;
                                colorRow2.push(sizeDataList[i].sizeDataDetail.cutCount);
                                colorActualSum += sizeDataList[i].sizeDataDetail.cutCount;
                                colorRow3.push(sizeDataList[i].sizeDataDetail.wellCount);
                                colorWellSum += sizeDataList[i].sizeDataDetail.wellCount;
                                colorRow4.push(sizeDataList[i].sizeDataDetail.diffCount);
                                colorDifferenceSum += sizeDataList[i].sizeDataDetail.diffCount;

                                if (sizeSummaryIndex == 0) {
                                    sizeSummary1.push(sizeDataList[i].sizeDataDetail.orderCount);
                                    sizeSummary2.push(sizeDataList[i].sizeDataDetail.cutCount);
                                    sizeSummary3.push(sizeDataList[i].sizeDataDetail.wellCount);
                                } else {
                                    sizeSummary1[sizeSumIndex] += sizeDataList[i].sizeDataDetail.orderCount;
                                    sizeSummary2[sizeSumIndex] += sizeDataList[i].sizeDataDetail.cutCount;
                                    sizeSummary3[sizeSumIndex] += sizeDataList[i].sizeDataDetail.wellCount;
                                }

                                sizeSumIndex++;

                                if (sizeDataList[i].sizeDataDetail.orderCount == 0) {
                                    colorRow5.push(toPercent(0));
                                } else {
                                    colorRow5.push(toPercent(sizeDataList[i].sizeDataDetail.diffCount / sizeDataList[i].sizeDataDetail.orderCount));
                                }
                            }
                            if (partIndex == 0){
                                sizeRow.push("合计");
                                hotData.push(sizeRow);
                            }
                            sizeSummaryIndex ++;
                            partPlanSum += colorPlanSum;
                            partActualSum += colorActualSum;
                            partWellSum += colorWellSum;
                            partDifferenceSum += colorDifferenceSum;
                            colorRow1.push(colorPlanSum);
                            colorRow2.push(colorActualSum);
                            colorRow3.push(colorWellSum);
                            colorRow4.push(colorDifferenceSum);
                            if (colorPlanSum == 0){
                                colorRow5.push(toPercent(0));
                            }else{
                                colorRow5.push(toPercent(colorDifferenceSum/colorPlanSum));
                            }
                            hotData.push(colorRow1);
                            hotData.push(colorRow2);
                            hotData.push(colorRow3);
                            hotData.push(colorRow4);
                            hotData.push(colorRow5);

                            partIndex ++;

                        }
                        hotData.push([]);
                        hotData.push(sizeSummary1);
                        hotData.push(sizeSummary2);
                        hotData.push(sizeSummary3);
                        hotData.push([["合计"],["订单总数"],partPlanSum,["总裁数"],partActualSum,["好片数"],partWellSum,["对比"],partDifferenceSum,["百分比"],toPercent(partDifferenceSum/partPlanSum)]);
                        hotData.push([]);
                    } else {
                        var partIndex = 0;
                        var partRow = ["部位名称",partKey];
                        hotData.push(partRow);
                        var colorData = dataOne[partKey];
                        var partPlanSum = 0;
                        var partActualSum = 0;
                        var partWellSum = 0;
                        var partDifferenceSum = 0;
                        var sizeSummary1 = [];
                        sizeSummary1[0] = [];
                        sizeSummary1[1] = ["订单尺码小计"];
                        var sizeSummary2 = [];
                        sizeSummary2[0] = [];
                        sizeSummary2[1] = ["主身尺码小计"];
                        var sizeSummary3 = [];
                        sizeSummary3[0] = [];
                        sizeSummary3[1] = ["好片尺码小计"];
                        var sizeSummaryIndex = 0;
                        for (var colorKey in colorData){
                            var colorRow1 = [];
                            var colorRow2 = [];
                            var colorRow3 = [];
                            var colorRow4 = [];
                            var colorRow5 = [];
                            colorRow2[0] = [];
                            colorRow3[0] = [];
                            colorRow4[0] = [];
                            colorRow5[0] = [];
                            colorRow1.push(colorKey);
                            colorRow1.push("订单量");
                            colorRow2.push("主身好片");
                            colorRow3.push("部位好片");
                            colorRow4.push("对比");
                            colorRow5.push("对比百分比");
                            var sizeData = colorData[colorKey];
                            var sizeRow = [];
                            var colorPlanSum = 0;
                            var colorActualSum = 0;
                            var colorWellSum = 0;
                            var colorDifferenceSum = 0;

                            var sizeSumIndex = 2;

                            var sizeDataList = [];
                            for (var i in sizeData){
                                var sizeDataItem = {};
                                sizeDataItem.sizeName = i;
                                sizeDataItem.sizeDataDetail = sizeData[i];
                                sizeDataList.push(sizeDataItem);
                            }
                            sizeDataList = globalSizeDataSort(sizeDataList);
                            for (var i in sizeDataList) {
                                if (partIndex == 0) {
                                    sizeRow[0] = [];
                                    sizeRow[1] = [];
                                    sizeRow.push(sizeDataList[i].sizeName);
                                }
                                colorRow1.push(sizeDataList[i].sizeDataDetail.orderCount);
                                colorPlanSum += sizeDataList[i].sizeDataDetail.orderCount;
                                colorRow2.push(sizeDataList[i].sizeDataDetail.cutCount);
                                colorActualSum += sizeDataList[i].sizeDataDetail.cutCount;
                                colorRow3.push(sizeDataList[i].sizeDataDetail.wellCount);
                                colorWellSum += sizeDataList[i].sizeDataDetail.wellCount;
                                colorRow4.push(sizeDataList[i].sizeDataDetail.diffCount);
                                colorDifferenceSum += sizeDataList[i].sizeDataDetail.diffCount;

                                if (sizeSummaryIndex == 0) {
                                    sizeSummary1.push(sizeDataList[i].sizeDataDetail.orderCount);
                                    sizeSummary2.push(sizeDataList[i].sizeDataDetail.cutCount);
                                    sizeSummary3.push(sizeDataList[i].sizeDataDetail.wellCount);
                                } else {
                                    sizeSummary1[sizeSumIndex] += sizeDataList[i].sizeDataDetail.orderCount;
                                    sizeSummary2[sizeSumIndex] += sizeDataList[i].sizeDataDetail.cutCount;
                                    sizeSummary3[sizeSumIndex] += sizeDataList[i].sizeDataDetail.wellCount;
                                }

                                sizeSumIndex++;

                                if (sizeDataList[i].sizeDataDetail.orderCount == 0) {
                                    colorRow5.push(toPercent(0));
                                } else {
                                    colorRow5.push(toPercent(sizeDataList[i].sizeDataDetail.diffCount / sizeDataList[i].sizeDataDetail.cutCount));
                                }
                            }
                            if (partIndex == 0){
                                sizeRow.push("合计");
                                hotData.push(sizeRow);
                            }
                            sizeSummaryIndex ++;
                            partPlanSum += colorPlanSum;
                            partActualSum += colorActualSum;
                            partWellSum += colorWellSum;
                            partDifferenceSum += colorDifferenceSum;
                            colorRow1.push(colorPlanSum);
                            colorRow2.push(colorActualSum);
                            colorRow3.push(colorWellSum);
                            colorRow4.push(colorDifferenceSum);
                            if (colorPlanSum == 0){
                                colorRow5.push(toPercent(0));
                            }else{
                                colorRow5.push(toPercent(colorDifferenceSum/colorActualSum));
                            }
                            hotData.push(colorRow1);
                            hotData.push(colorRow2);
                            hotData.push(colorRow3);
                            hotData.push(colorRow4);
                            hotData.push(colorRow5);

                            partIndex ++;

                        }
                        hotData.push([]);
                        hotData.push(sizeSummary1);
                        hotData.push(sizeSummary2);
                        hotData.push(sizeSummary3);
                        hotData.push([["合计"],["订单总数"],partPlanSum,["主身好片"],partActualSum,["部位好片"],partWellSum,["对比"],partDifferenceSum,["百分比"],toPercent(partDifferenceSum/partPlanSum)]);
                        hotData.push([]);
                    }

                }
                if (dataTwo){
                    hotData.push([["分组明细"]]);
                    var groupPartIndex = 0;
                    for (var partKey in dataTwo){
                        var partIndex = 0;
                        var partRow = ["部位名称",partKey];
                        hotData.push(partRow);
                        var colorData = dataTwo[partKey];
                        var sizeRow = [];
                        for (var colorKey in colorData){
                            var bedData = colorData[colorKey];
                            if (!isEmpty(bedData)){
                                var bedColorSum = 0;
                                var bedColorPackageSum = 0;
                                var bedColorSummary = [];
                                var bedColorIndex = 0;
                                if (partIndex == 0){
                                    sizeRow.push(colorKey);
                                    sizeRow.push("");
                                }else{
                                    hotData.push([colorKey])
                                }
                                for (var bedKey in bedData){
                                    var sizeData = bedData[bedKey];
                                    var bedTime = bedKey.split("#");
                                    var bed = bedTime[0]+"床";
                                    var time = bedTime[1];
                                    var groupName = bedTime[2];
                                    var bedTimeRow = [];
                                    bedTimeRow[0] = [bed];
                                    bedTimeRow[1] = [time];
                                    var bedSizeSum = 0;

                                    var sizeDataList = [];
                                    for (var i in sizeData){
                                        if (i != "packageCount"){
                                            var sizeDataItem = {};
                                            sizeDataItem.sizeName = i;
                                            sizeDataItem.sizeDataDetail = sizeData[i];
                                            sizeDataList.push(sizeDataItem);
                                        }
                                    }
                                    sizeDataList = globalSizeDataSort(sizeDataList);
                                    for (var i in sizeDataList){
                                        if (partIndex == 0){
                                            sizeRow.push(sizeDataList[i].sizeName);
                                        }
                                        if (bedColorIndex == 0){
                                            bedColorSummary.push("");
                                        }
                                        bedSizeSum += sizeDataList[i].sizeDataDetail;
                                        bedTimeRow.push(sizeDataList[i].sizeDataDetail);
                                    }
                                    if (partIndex == 0){
                                        sizeRow.push("合计");
                                        sizeRow.push("扎数");
                                        sizeRow.push("组名");
                                        hotData.push(sizeRow);
                                    }
                                    if (bedColorIndex == 0){
                                        bedColorSummary.push("");
                                        bedColorSummary.push("合计");
                                    }
                                    bedColorIndex ++;
                                    partIndex ++;
                                    bedColorSum += bedSizeSum;
                                    bedTimeRow.push(bedSizeSum);
                                    bedTimeRow.push(sizeData["packageCount"]);
                                    bedColorPackageSum += sizeData["packageCount"];
                                    bedTimeRow.push(groupName);
                                    hotData.push(bedTimeRow);
                                }
                                bedColorSummary.push(bedColorSum);
                                bedColorSummary.push(bedColorPackageSum);
                                hotData.push(bedColorSummary);
                                hotData.push();
                            }
                        }
                    }
                }
                layer.close(load);
            }
            hot.loadData(hotData);
            // var rows=hot.countRows();  // get the count of the rows in the table
            // var cols=hot.countCols();  // get the count of the columns in the table.
            // for(var row=0; row<rows; row++){  // go through each row of the table
            //     if (hotData[row][0] === '部位名称'){
            //         for(var col=0; col<cols; col++){
            //             var rowCell = hot.getCell(row,col);
            //             rowCell.style.background = '#2DCA93';
            //         }
            //     }
            // }
            // hot.render();
        },
        error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }
    })
}


function exportData() {
    var data = hot.getData();
    var result = [];
    var col = 0;
    $.each(data,function (index, item) {
        // if(item[0]!=null) {
        for(var i=0;i<item.length;i++) {
            if(item[i]==null) {
                item[i] = '';
            }else if(index==1) {
                col++;
            }
        }
        result.push(item);
    });
    var orderName = $("#orderName").val();
    var clothesVersionNumber = $("#clothesVersionNumber").val();
    var myDate = new Date();
    export2Excel([orderName+"-"+clothesVersionNumber+'-裁床汇总-'+myDate.toLocaleString()],col, result, orderName+"-"+clothesVersionNumber+'-裁床汇总-'+myDate.toLocaleString()+".xls")
}


function toPercent(point){
    var str=Number(point*100).toFixed(1);
    str+="%";
    return str;
}

function isEmpty(v) {
    switch (typeof v) {
        case 'undefined':
            return true;
        case 'string':
            if (v.replace(/(^[ \t\n\r]*)|([ \t\n\r]*$)/g, '').length == 0) return true;
            break;
        case 'boolean':
            if (!v) return true;
            break;
        case 'number':
            if (0 === v || isNaN(v)) return true;
            break;
        case 'object':
            if (null === v || v.length === 0) return true;
            for (var i in v) {
                return false;
            }
            return true;
    }
    return false;
}