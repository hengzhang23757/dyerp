var hot;
var basePath=$("#basePath").val();
layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.21'
}).extend();
$(document).ready(function () {

    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumber", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });

    $("#orderName").bind('blur change',function(){
        var orderName = $("#orderName").val();
        $.ajax({
            url: "/erp/getotherprintpartnamesbyorder",
            data: {"orderName": orderName},
            success:function(data){
                $("#partName").empty();
                if (data.printPartNameList) {
                    $("#partName").append("<option value=''>选择部位</option>");
                    $("#partName").append("<option value='主身布'>主身布</option>");
                    $.each(data.printPartNameList, function(index,element){
                        $("#partName").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });
    });

    $("#clothesVersionNumber").bind('blur change',function(){
        var orderName = $("#orderName").val();
        $.ajax({
            url: "/erp/getotherprintpartnamesbyorder",
            data: {"orderName": orderName},
            success:function(data){
                $("#partName").empty();
                if (data.printPartNameList) {
                    $("#partName").append("<option value=''>选择部位</option>");
                    $("#partName").append("<option value='主身布'>主身布</option>");
                    $.each(data.printPartNameList, function(index,element){
                        $("#partName").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });
    });

    $("#partName").change(function () {
        var partName = $("#partName").val();
        var orderName = $("#orderName").val();
        $.ajax({
            url: "/erp/getbednumbersbyorderparttype",
            data: {
                "orderName": orderName,
                "partName": partName
            },
            success:function(data){
                $("#bedNumber").empty();
                if (data.bedNumList) {
                    $("#bedNumber").append("<option value=''>选择床号</option>");
                    $.each(data.bedNumList, function(index,element){
                        $("#bedNumber").append("<option value="+element+">"+element+"</option>");
                    })
                }
            }, error:function(){
            }
        });

    });
});



var orderName = $("#orderName").val();
var basePath=$("#basePath").val();
var colNum = 0;

function reportCut() {
    if($("#orderName").val().trim()=="") {
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入要查询的订单号！</span>",html: true});
        return false;
    }
    if($("#partName").val().trim()=="") {
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请选择要查询的部位！</span>",html: true});
        return false;
    }
    hot = null;
    $("#reportExcel").empty();
    orderName = $("#orderName").val();
    var clothesVersionNumber = $("#clothesVersionNumber").val();
    var bedNumber = $("#bedNumber").val();
    var data = {};
    data.orderName = orderName;
    data.partName = $("#partName").val();
    var printPartName = $("#partName").val();
    if (bedNumber != "" && bedNumber != null){
        data.bedNumber = bedNumber;
    }
    $.ajax({
        url: basePath + "erp/tailorreport",
        type:'GET',
        data: data,
        success: function (data) {
            $("#exportDiv").show();
            var hotData = [];
            var tableHtml = "";
            $("#entities").empty();
            if(data) {
                tableHtml = "<div class=\"col-md-12\" name='printTable'>\n" +
                    "               <section class=\"panel panel-default\" style='page-break-after:always;'>\n" +
                    "                   <header class=\"panel-heading font-bold\" style=\"text-align: center\">\n" +
                    "                       <div style=\"font-size: 20px;font-weight: 700\">德悦服饰裁床单</div><br>\n" +
                    "                       <div style=\"font-size: 14px;float: center;font-weight: 700\">版单："+clothesVersionNumber+" 订单："+orderName+" 床号:"+ bedNumber + "部位:" + printPartName +" 裁剪日期:"+ moment(data.cutDate).format("YYYY-MM-DD") +"</div>\n"+
                    "                   </header>\n" +
                    "                   <table class=\"table table-bordered\" style='width: 100%'>\n" +
                    "                       <tbody>";
                tableHtml += "<tr style='font-weight: 700'><td style='background-color: #0f9d58'>扎号</td><td>床号</td><td>颜色</td><td>尺码</td><td>数量</td><td>缸号</td><td style='background-color: #0f9d58'>扎号</td><td>床号</td><td>颜色</td><td>尺码</td><td>数量</td><td>缸号</td><td style='background-color: #0f9d58'>扎号</td><td>床号</td><td>颜色</td><td>尺码</td><td>数量</td><td>缸号</td></tr>";
                var title = [["扎号"],["床号"],["颜色"],["尺码"],["数量"],["缸号"],["扎号"],["床号"],["颜色"],["尺码"],["数量"],["缸号"],["扎号"],["床号"],["颜色"],["尺码"],["数量"],["缸号"]];
                var detail = data.detail;
                var summary = data.color;
                var jarData = data.jar;
                var sizeHeadList = data.size;
                var oneLength = data.oneLength;
                var twoLength = data.twoLength;
                var threeLength = data.threeLength;
                var hotData = [];
                var index_num = 1;
                console.log(detail);
                sizeHeadList = globalSizeSort(sizeHeadList);
                if (detail){
                    hotData.push(title);
                    // var height = Math.floor(detail.length/3);
                    // var modHeight = detail.length % 3;
                    if (oneLength > 0){
                        if (oneLength == twoLength && oneLength == threeLength){
                            for (var i = 0; i < oneLength; i++ ){
                                tableHtml += "<tr><td style='background-color: #0f9d58'>" + detail[i].packageNumber+ "</td><td>" + detail[i].bedNumber + "</td><td>" + detail[i].colorName + "</td><td>" + detail[i].sizeName + "</td><td>" + detail[i].layerCount + "</td><td>" + detail[i].jarName + "</td><td style='background-color: #0f9d58'>" + detail[i+oneLength].packageNumber + "</td><td>" + detail[i+oneLength].bedNumber + "</td><td>" + detail[i+oneLength].colorName + "</td><td>" + detail[i+oneLength].sizeName + "</td><td>" + detail[i+oneLength].layerCount + "</td><td>" + detail[i+oneLength].jarName+ "</td><td style='background-color: #0f9d58'>" + detail[i+oneLength+twoLength].packageNumber + "</td><td>" + detail[i+oneLength+twoLength].bedNumber  + "</td><td>" + detail[i+oneLength+twoLength].colorName + "</td><td>" + detail[i+oneLength+twoLength].sizeName + "</td><td>" + detail[i+oneLength+twoLength].layerCount + "</td><td>" + detail[i+oneLength+twoLength].jarName + "</td></tr>";
                                var rowData = [];
                                rowData[0] = detail[i].packageNumber;
                                rowData[1] = detail[i].bedNumber;
                                rowData[2] = detail[i].colorName;
                                rowData[3] = detail[i].sizeName;
                                rowData[4] = detail[i].layerCount;
                                rowData[5] = detail[i].jarName;
                                rowData[6] = detail[i + oneLength].packageNumber;
                                rowData[7] = detail[i + oneLength].bedNumber;
                                rowData[8] = detail[i + oneLength].colorName;
                                rowData[9] = detail[i + oneLength].sizeName;
                                rowData[10] = detail[i + oneLength].layerCount;
                                rowData[11] = detail[i + oneLength].jarName;
                                rowData[12] = detail[i + oneLength + twoLength].packageNumber;
                                rowData[13] = detail[i + oneLength + twoLength].bedNumber;
                                rowData[14] = detail[i + oneLength + twoLength].colorName;
                                rowData[15] = detail[i + oneLength + twoLength].sizeName;
                                rowData[16] = detail[i + oneLength + twoLength].layerCount;
                                rowData[17] = detail[i + oneLength + twoLength].jarName;
                                hotData[index_num] = rowData;
                                index_num ++;
                            }
                        } else if (oneLength == twoLength && oneLength > threeLength){
                            for (var i = 0; i < oneLength - 1; i++ ){
                                tableHtml += "<tr><td style='background-color: #0f9d58'>" + detail[i].packageNumber + "</td><td>"+ detail[i].bedNumber + "</td><td>" + detail[i].colorName + "</td><td>" + detail[i].sizeName + "</td><td>" + detail[i].layerCount + "</td><td>" + detail[i].jarName + "</td><td style='background-color: #0f9d58'>" + detail[i+oneLength].packageNumber + "</td><td>" + detail[i+oneLength].bedNumber + "</td><td>" + detail[i+oneLength].colorName + "</td><td>" + detail[i+oneLength].sizeName + "</td><td>" + detail[i+oneLength].layerCount + "</td><td>" + detail[i+oneLength].jarName+ "</td><td style='background-color: #0f9d58'>" + detail[i+oneLength+twoLength].packageNumber + "</td><td>" + detail[i+oneLength+twoLength].bedNumber + "</td><td>" + detail[i+oneLength+twoLength].colorName + "</td><td>" + detail[i+oneLength+twoLength].sizeName + "</td><td>" + detail[i+oneLength+twoLength].layerCount + "</td><td>" + detail[i+oneLength+twoLength].jarName + "</td></tr>";
                                var rowData = [];
                                rowData[0] = detail[i].packageNumber;
                                rowData[1] = detail[i].bedNumber;
                                rowData[2] = detail[i].colorName;
                                rowData[3] = detail[i].sizeName;
                                rowData[4] = detail[i].layerCount;
                                rowData[5] = detail[i].jarName;
                                rowData[6] = detail[i + oneLength].packageNumber;
                                rowData[7] = detail[i + oneLength].bedNumber;
                                rowData[8] = detail[i + oneLength].colorName;
                                rowData[9] = detail[i + oneLength].sizeName;
                                rowData[10] = detail[i + oneLength].layerCount;
                                rowData[11] = detail[i + oneLength].jarName;
                                rowData[12] = detail[i + oneLength + twoLength].packageNumber;
                                rowData[13] = detail[i + oneLength + twoLength].bedNumber;
                                rowData[14] = detail[i + oneLength + twoLength].colorName;
                                rowData[15] = detail[i + oneLength + twoLength].sizeName;
                                rowData[16] = detail[i + oneLength + twoLength].layerCount;
                                rowData[17] = detail[i + oneLength + twoLength].jarName;
                                hotData[index_num] = rowData;
                                index_num ++;
                            }
                            tableHtml += "<tr><td style='background-color: #0f9d58'>" + detail[oneLength - 1].packageNumber+ "</td><td>" +detail[oneLength - 1].bedNumber + "</td><td>" + detail[oneLength - 1].colorName + "</td><td>" + detail[oneLength - 1].sizeName + "</td><td>" + detail[oneLength - 1].layerCount + "</td><td>" + detail[oneLength - 1].jarName + "</td><td style='background-color: #0f9d58'>" + detail[oneLength + twoLength - 1].packageNumber + "</td><td>"+ detail[oneLength + twoLength - 1].bedNumber + "</td><td>" + detail[oneLength + twoLength - 1].colorName + "</td><td>" + detail[oneLength + twoLength - 1].sizeName + "</td><td>" + detail[oneLength + twoLength - 1].layerCount + "</td><td>" + detail[oneLength + twoLength - 1].jarName+ "</td><td></td><td></td><td></td><td></td><td></td></tr>";
                            var rowData = [];
                            rowData[0] = detail[oneLength - 1].packageNumber;
                            rowData[1] = detail[oneLength - 1].bedNumber;
                            rowData[2] = detail[oneLength - 1].colorName;
                            rowData[3] = detail[oneLength - 1].sizeName;
                            rowData[4] = detail[oneLength - 1].layerCount;
                            rowData[5] = detail[oneLength - 1].jarName;
                            rowData[6] = detail[oneLength + twoLength - 1].packageNumber;
                            rowData[7] = detail[oneLength + twoLength - 1].bedNumber;
                            rowData[8] = detail[oneLength + twoLength - 1].colorName;
                            rowData[9] = detail[oneLength + twoLength - 1].sizeName;
                            rowData[10] = detail[oneLength + twoLength - 1].layerCount;
                            rowData[11] = detail[oneLength + twoLength - 1].jarName;
                            hotData[index_num] = rowData;
                            index_num ++;
                        } else if (oneLength > twoLength && oneLength > threeLength){
                            for (var i = 0; i < oneLength - 1; i++ ){
                                tableHtml += "<tr><td style='background-color: #0f9d58'>" + detail[i].packageNumber + "</td><td>" + detail[i].bedNumber + "</td><td>" + detail[i].colorName + "</td><td>" + detail[i].sizeName + "</td><td>" + detail[i].layerCount + "</td><td>" + detail[i].jarName + "</td><td style='background-color: #0f9d58'>" + detail[i+oneLength].packageNumber + "</td><td>" + detail[i+oneLength].bedNumber + "</td><td>" + detail[i+oneLength].colorName + "</td><td>" + detail[i+oneLength].sizeName + "</td><td>" + detail[i+oneLength].layerCount + "</td><td>" + detail[i+oneLength].jarName+ "</td><td style='background-color: #0f9d58'>" + detail[i+oneLength+twoLength].packageNumber + "</td><td>" + detail[i+oneLength+twoLength].bedNumber + "</td><td>" + detail[i+oneLength+twoLength].colorName + "</td><td>" + detail[i+oneLength+twoLength].sizeName + "</td><td>" + detail[i+oneLength+twoLength].layerCount + "</td><td>" + detail[i+oneLength+twoLength].jarName + "</td></tr>";
                                var rowData = [];
                                rowData[0] = detail[i].packageNumber;
                                rowData[1] = detail[i].bedNumber;
                                rowData[2] = detail[i].colorName;
                                rowData[3] = detail[i].sizeName;
                                rowData[4] = detail[i].layerCount;
                                rowData[5] = detail[i].jarName;
                                rowData[6] = detail[i + oneLength].packageNumber;
                                rowData[7] = detail[i + oneLength].bedNumber;
                                rowData[8] = detail[i + oneLength].colorName;
                                rowData[9] = detail[i + oneLength].sizeName;
                                rowData[10] = detail[i + oneLength].layerCount;
                                rowData[11] = detail[i + oneLength].jarName;
                                rowData[12] = detail[i + oneLength + twoLength].packageNumber;
                                rowData[13] = detail[i + oneLength + twoLength].bedNumber;
                                rowData[14] = detail[i + oneLength + twoLength].colorName;
                                rowData[15] = detail[i + oneLength + twoLength].sizeName;
                                rowData[16] = detail[i + oneLength + twoLength].layerCount;
                                rowData[17] = detail[i + oneLength + twoLength].jarName;
                                hotData[index_num] = rowData;
                                index_num ++;
                            }
                            tableHtml += "<tr><td style='background-color: #0f9d58'>" + detail[oneLength - 1].packageNumber + "</td><td>" + detail[oneLength - 1].bedNumber + "</td><td>" + detail[oneLength - 1].colorName + "</td><td>" + detail[oneLength - 1].sizeName + "</td><td>" + detail[oneLength - 1].layerCount + "</td><td>" + detail[oneLength - 1].jarName + "</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
                            var rowData = [];
                            rowData[0] = detail[oneLength - 1].packageNumber;
                            rowData[1] = detail[oneLength - 1].bedNumber;
                            rowData[2] = detail[oneLength - 1].colorName;
                            rowData[3] = detail[oneLength - 1].sizeName;
                            rowData[4] = detail[oneLength - 1].layerCount;
                            rowData[5] = detail[oneLength - 1].jarName;
                            hotData[index_num] = rowData;
                            index_num ++;
                        }

                    }
                }
                tableHtml +=  "                       </tbody>" +
                    "                   </table>";

                if (summary) {
                    hotData.push([]);
                    hotData.push(["汇总"]);
                    tableHtml += "                   <table class=\"table table-bordered\" style='width: 100%'>\n" +
                        "                       <tbody>";
                    var sizeRow = [];
                    sizeRow[0] = [];
                    var sizeSumRow = [];
                    sizeSumRow[0] = ["小计"];
                    var colorIndex = 0;
                    var totalSum = 0;

                    if (sizeHeadList){
                        var widthSummary = sizeHeadList.length + 2;
                        tableHtml += "<tr><td colspan='"+ widthSummary +"' style='text-align:center; font-size: 15px; font-weight: 700'></td></tr>";
                        tableHtml += "<tr><td colspan='"+ widthSummary +"' style='text-align:center; font-size: 15px; font-weight: 700'>汇总</td></tr>";
                        tableHtml += "<tr style='font-weight: 700'><td></td>";
                        var flag = true;
                        for (var j = 0; j < sizeHeadList.length; j++){
                            tableHtml += "<td>" + sizeHeadList[j]+"</td>";
                            sizeSumRow[j+1] = 0;
                        }
                        tableHtml += "<td>合计</td></tr>";
                    }

                    for (var colorKey in summary){
                        var sizeData = summary[colorKey];
                        var tmpRow = [];
                        var colorSum = 0;
                        tmpRow.push(colorKey);
                        tableHtml += "<tr><td>" + colorKey + "</td>";
                        for (var m = 0; m < sizeHeadList.length; m ++){
                            if (sizeData[sizeHeadList[m]]){
                                tmpRow.push(sizeData[sizeHeadList[m]]);
                                colorSum += sizeData[sizeHeadList[m]];
                                tableHtml += "<td>" + sizeData[sizeHeadList[m]] + "</td>"
                                sizeSumRow[m+1] += sizeData[sizeHeadList[m]];
                            }else {
                                tmpRow.push([0]);
                                tableHtml += "<td>0</td>";
                            }
                        }
                        tableHtml += "<td>" + colorSum + "</td></tr>";
                        totalSum += colorSum;
                        tableHtml += "<tr><td>小计</td>";
                        hotData[index_num] = tmpRow;
                        index_num ++;
                    }

                    for (var n = 1; n < sizeSumRow.length; n++){
                        tableHtml += "<td>" + sizeSumRow[n] + "</td>";
                    }
                    tableHtml += "<td>" + totalSum + "</td></tr>";

                    hotData[index_num] = sizeSumRow;
                    index_num ++;
                }

                tableHtml +=  "                       </tbody>" +
                    "                   </table>";
                if (jarData){
                    hotData.push([]);
                    hotData.push(["缸号汇总"]);
                    tableHtml += "                   <table class=\"table table-bordered\" style='width: 100%'>\n" +
                        "                       <tbody>";

                    var jarHead = [["颜色"],["LO色"],["缸号"]];

                    if (sizeHeadList){
                        var widthSummary = sizeHeadList.length + 4;
                        tableHtml += "<tr><td colspan='"+ widthSummary +"' style='text-align:center; font-size: 15px; font-weight: 700'></td></tr>";
                        tableHtml += "<tr><td colspan='"+ widthSummary +"' style='text-align:center; font-size: 15px; font-weight: 700'>缸号汇总</td></tr>";
                        tableHtml += "<tr style='font-weight: 700'><td>颜色</td><td>LO色</td><td>缸号</td>";
                        var flag = true;
                        for (var j = 0; j < sizeHeadList.length; j++){
                            tableHtml += "<td>" + sizeHeadList[j]+"</td>";
                            jarHead.push([sizeHeadList[j]]);
                        }
                        tableHtml += "<td>合计</td></tr>";
                        jarHead.push(["合计"]);
                    }
                    hotData.push(jarHead);
                    for (var loColor in jarData){
                        var loData = jarData[loColor];
                        for (var lo in loData){
                            var loSum = 0;
                            var loSizeSum = [];
                            loSizeSum[0] = [];
                            loSizeSum[1] = [];
                            loSizeSum[2] = [];
                            $.each(sizeHeadList,function (index, item) {
                                loSizeSum[index + 3] = 0;
                            });
                            loSizeSum[sizeHeadList.length + 3] = 0;


                            var loJarData = loData[lo];
                            for (var jar in loJarData){
                                var dataLine = [];
                                tableHtml += "<tr><td>" + loColor + "</td><td>" + lo +"</td><td>"+ jar +"</td>";
                                var loSizeData = loJarData[jar];
                                var jarSum = 0;
                                $.each(sizeHeadList,function (index, item) {
                                    if (loSizeData[item]){
                                        tableHtml += "<td>" + loSizeData[item] + "</td>";
                                        jarSum += loSizeData[item];
                                        dataLine[index + 3] = loSizeData[item];
                                        loSizeSum[index + 3] += loSizeData[item]
                                    } else {
                                        tableHtml += "<td>0</td>";
                                        dataLine[index + 3] = 0;
                                    }
                                });
                                dataLine.push([jarSum]);
                                tableHtml += "<td>" + jarSum + "</td></tr>";
                                loSum += jarSum;
                                loSizeSum[sizeHeadList.length + 3] += jarSum;
                                hotData.push(dataLine);
                            }
                            tableHtml += "<tr><td></td><td></td><td>小计</td>";
                            for (var a = 3; a < loSizeSum.length; a ++){
                                tableHtml += "<td>" + loSizeSum[a] + "</td>";
                            }
                            tableHtml += "</tr>";
                            hotData.push(loSizeSum);
                            hotData.push([]);
                        }
                    }

                }
                colNum = title.length;
                if(hotData.length == 0) {
                    hotData = [[]];
                }
                var container = document.getElementById('reportExcel');
                if (hot == undefined) {
                    hot = new Handsontable(container, {
                        data: hotData,
                        rowHeaders: true,
                        colHeaders: true,
                        autoColumnSize: true,
                        dropdownMenu: true,
                        contextMenu: true,
                        autoWrapRow: true,
                        manualColumnResize:true,
                        height: $(document.body).height() - 200,
                        // minRows: 35,
                        minCols: 20,
                        colWidths: [80,80,80,80,80,80,80,80,80,80,80,80,80,80,80,50,50,50,50,50],
                        stretchH: "all",
                        language: 'zh-CN',
                        licenseKey: 'non-commercial-and-evaluation'

                    });
                }else {
                    hot.loadData(hotData);
                }
                tableHtml +=  "                       </tbody>" +
                    "                   </table>" +
                    "               </section>" +
                    "              </div>";
                $("#entities").append(tableHtml);
            }else {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，没有查到相关信息！</span>",html: true});
            }
        },
        error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }
    })

}


function exportData() {
    var data = hot.getData();
    var result = [];
    $.each(data,function (index, item) {
        // if(item[0]!=null) {
            for(var i=0;i<item.length;i++) {
                if(item[i]==null) {
                    item[i] = '';
                }
            }
            result.push(item);
        // }else {
        //     return false;
        // }
    });
    var orderName = $("#orderName").val();
    var clothesVersionNumber = $("#clothesVersionNumber").val();
    var bedNumber = $("#bedNumber").val();
    var myDate = new Date();
    export2Excel([orderName+"-"+clothesVersionNumber+"-第"+bedNumber+'床-裁床报表-'+myDate.toLocaleString()],colNum, result, orderName+"-"+clothesVersionNumber+"-第"+bedNumber+'床-裁床报表-'+myDate.toLocaleString()+".xls")
}

function printDeal(){
    $("#printf").empty();
    var printBoxs = document.getElementsByName('printTable');
    var newContent = '';
    for(var i=0;i<printBoxs.length;i++) {
        newContent += printBoxs[i].innerHTML;
    }
    var f = document.getElementById('printf');
    // f.contentDocument.write('<link rel="stylesheet" type="text/css" href="/css/app.v2.css">');
    f.contentDocument.write('<style>' +
        'table {' +
        'border-collapse:collapse;' +
        '}' +
        'td {\n' +
        '        border: 1px solid #000000;\n' +
        '    }'+
        '</style>');
    f.contentDocument.write(newContent);
    f.contentDocument.close();
    window.frames['printf'].focus();
    try{
        window.frames['printf'].print();
    }catch(err){
        f.contentWindow.print();
    }
}