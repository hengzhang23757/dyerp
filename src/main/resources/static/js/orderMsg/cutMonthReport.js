var form;
layui.use('form', function(){
    form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
    form.render();
});
layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.25'
}).extend({                         // 模块别名
    soulTable: 'soulTable'
});

$(document).ready(function () {
    // $("#groupName").next().find("select")[0].style.width = '150px';
    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumber", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });

});

var cutMonthTable;

layui.use(['form', 'table', 'soulTable'], function () {
    var table = layui.table,
        soulTable = layui.soulTable,
        form = layui.form,
        $ = layui.$;

    layui.laydate.render({
        elem: '#from',
        trigger: 'click',
        value: new Date()
    });
    layui.laydate.render({
        elem: '#to',
        trigger: 'click',
        value: new Date()
    });

    cutMonthTable = table.render({
        elem: '#cutMonthTable'
        ,cols:[[]]
        ,loading:true
        ,data:[]
        ,height: 'full-130'
        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
        ,defaultToolbar: ['filter', 'print']
        ,title: '裁床月度报表'
        ,totalRow: true
        ,page: true
        ,limits: [500, 1000, 2000]
        ,limit: 1000 //每页默认显示的数量
        ,done: function () {
            soulTable.render(this);
        }
        ,filter: {
            bottom: true,
            items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
        }
    });

    var from = $("#from").val();
    var to = $("#to").val();
    var groupName = $("#groupName").val();
    var orderName = $("#orderName").val();


    initTable(from, to, '', '', '分床汇总');

    function initTable(from, to, groupName, orderName, type){
        var data = {};
        data.from = from;
        data.to = to;
        if (groupName != null && groupName != ''){
            data.groupName = groupName;
        }
        if (orderName != null && orderName != ''){
            data.orderName = orderName;
        }
        data.type = type;
        var load = layer.load();
        if (type == "分床汇总"){
            $.ajax({
                url: "/erp/monthtailorreportgroupbybednumber",
                type: 'GET',
                data: data,
                success: function (res) {
                    if (res.tailorList) {
                        var reportData = res.tailorList;
                        table.render({
                            elem:"#cutMonthTable"
                            ,cols:[[
                                {type:'numbers', align:'center', title:'序号', width:90}
                                ,{field:'clothesVersionNumber', title:'单号', align:'center', width:200, sort: true, filter: true, totalRowText: '合计'}
                                ,{field:'orderName', title:'款号', align:'center', width:200, sort: true, filter: true}
                                ,{field:'partName', title:'部位', align:'center', width:120, sort: true, filter: true}
                                ,{field:'bedNumber', title:'床号', align:'center', width:120, sort: true, filter: true}
                                ,{field:'groupName', title:'组名', align:'center', width:120, sort: true, filter: true}
                                ,{field:'initCount', title:'裁数', align:'center', width:120, sort: true, filter: true, totalRow: true,excel: {cellType: 'n'}}
                                ,{fixed: 'right', title:'操作', align:'center', toolbar: '#barTop', width:180}
                            ]]
                            ,excel: {
                                filename: '裁床区间汇总.xlsx',
                                add: {
                                    top: {
                                        data: [
                                            ['裁床按天汇总报表'], //头部第一行数据，由于我设置了后面的数据merge了，就只写一个
                                            ["从"+from + "到"+to]
                                        ],
                                        heights: [30,15],
                                        merge: [['1,1','1,10'],['2,1','2,3']]
                                    }
                                }
                            }
                            ,data: reportData   // 将新数据重新载入表格
                            ,overflow: 'tips'
                            ,loading:true
                            ,height: 'full-130'
                            ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                            ,title: '裁床月度报表'
                            ,totalRow: true
                            ,page: true
                            ,limit: 1000
                            ,limits: [1000, 2000, 3000]
                            ,done: function () {
                                soulTable.render(this);
                                layer.close(load);
                            }
                        });

                    } else {
                        layer.msg("获取失败！", {icon: 2});
                    }
                }, error: function () {
                    layer.msg("获取失败！", {icon: 2});
                }
            });
        } else if (type == "主身布"){
            $.ajax({
                url: "/erp/tailormonthreport",
                type: 'GET',
                data: data,
                success: function (res) {
                    if (res.tailorMonthReportList) {
                        var reportData = res.tailorMonthReportList;
                        table.render({
                            elem:"#cutMonthTable"
                            ,cols:[[
                                {type:'numbers', align:'center', title:'序号', width:90}
                                ,{field:'clothesVersionNumber', title:'单号', align:'center', width:130, sort: true, filter: true, totalRowText: '合计'}
                                ,{field:'orderName', title:'款号', align:'center', width:130, sort: true, filter: true}
                                ,{field:'customerName', title:'客户', align:'center', width:100, sort: true, filter: true}
                                ,{field:'partName', title:'部位', align:'center', width:120, sort: true, filter: true,templet: function (d) {
                                        return "主身布";
                                    }}
                                ,{field:'orderCount', title:'订单总数', align:'center', width:120, sort: true, filter: true, totalRow: true,excel: {cellType: 'n'}}
                                ,{field:'cutCount', title:'总裁数', align:'center', width:120, sort: true, filter: true, totalRow: true,excel: {cellType: 'n'}}
                                ,{field:'differenceCount', title:'对比', align:'center', width:150, sort: true, filter: true, totalRow: true,excel: {cellType: 'n'}}
                                ,{field:'bedNumberCount', title:'区间床数', align:'center',width:120, sort: true, filter: true, totalRow: true,excel: {cellType: 'n'}}
                                ,{field:'pieceCount', title:'区间裁数', align:'center', width:150, sort: true, filter: true, totalRow: true,excel: {cellType: 'n'}}
                                ,{field:'groupName', title:'组名', align:'center', width:150, sort: true, filter: true, totalRow: true,excel: {cellType: 'n'}}
                                ,{field:'price', title:'单价', align:'center', width:90, sort: true, filter: true, totalRow: true, templet: function (d) {
                                        return toDecimal(d.price);
                                    }}
                                ,{field:'priceTwo', title:'浮动', align:'center', width:90, sort: true, filter: true, totalRow: true, templet: function (d) {
                                        return toDecimal(d.priceTwo);
                                    }}
                                ,{field:'sumMoney', title:'工资', align:'center', width:90, sort: true, filter: true, totalRow: true, templet: function (d) {
                                        return toDecimal(d.sumMoney);
                                    }}
                            ]]
                            ,excel: {
                                filename: '裁床主身布月度报表.xlsx',
                                add: {
                                    top: {
                                        data: [
                                            ['裁床主身布月度报表'], //头部第一行数据，由于我设置了后面的数据merge了，就只写一个
                                            ["从"+from + "到"+to]
                                        ],
                                        heights: [30,15],
                                        merge: [['1,1','1,10'],['2,1','2,3']]
                                    }
                                }
                            }
                            ,data: reportData   // 将新数据重新载入表格
                            ,overflow: 'tips'
                            ,loading:true
                            ,height: 'full-130'
                            ,page: true
                            ,limit: 1000
                            ,limits: [1000, 2000, 3000]
                            ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                            ,defaultToolbar: ['filter', 'print']
                            ,totalRow: true
                            ,done: function () {
                                soulTable.render(this);
                                layer.close(load);
                            }
                        });

                    } else {
                        layer.msg("获取失败！", {icon: 2});
                    }
                }, error: function () {
                    layer.msg("获取失败！", {icon: 2});
                }
            });
        } else if (type == "汇总"){
            $.ajax({
                url: "/erp/tailorsummarymonthreport",
                type: 'GET',
                data: data,
                success: function (res) {
                    if (res.summaryTailorMonthReportList) {
                        var reportData = res.summaryTailorMonthReportList;
                        table.render({
                            elem:"#cutMonthTable"
                            ,cols:[[
                                {type:'numbers', align:'center', title:'序号', width:90}
                                ,{field:'clothesVersionNumber', title:'单号', align:'center', width:130, sort: true, filter: true, totalRowText: '合计'}
                                ,{field:'orderName', title:'款号', align:'center', width:130, sort: true, filter: true}
                                ,{field:'customerName', title:'客户', align:'center', width:100, sort: true, filter: true}
                                ,{field:'partName', title:'部位', align:'center', width:120, sort: true, filter: true}
                                ,{field:'orderCount', title:'订单总数', align:'center', width:120, sort: true, filter: true, totalRow: true,excel: {cellType: 'n'}}
                                ,{field:'cutCount', title:'总裁数', align:'center', width:120, sort: true, filter: true, totalRow: true,excel: {cellType: 'n'}}
                                ,{field:'differenceCount', title:'对比', align:'center', width:150, sort: true, filter: true, totalRow: true,excel: {cellType: 'n'}}
                                ,{field:'bedNumberCount', title:'区间床数', align:'center',width:120, sort: true, filter: true, totalRow: true,excel: {cellType: 'n'}}
                                ,{field:'pieceCount', title:'区间裁数', align:'center', width:150, sort: true, filter: true, totalRow: true,excel: {cellType: 'n'}}
                                ,{field:'groupName', title:'组别', align:'center', width:150, sort: true, filter: true}
                            ]]
                            ,excel: {
                                filename: '裁床汇总月度报表.xlsx',
                                add: {
                                    top: {
                                        data: [
                                            ['裁床汇总月度报表'], //头部第一行数据，由于我设置了后面的数据merge了，就只写一个
                                            ["从"+from + "到"+to]
                                        ],
                                        heights: [30,15],
                                        merge: [['1,1','1,10'],['2,1','2,3']]
                                    }
                                }
                            }
                            ,data: reportData   // 将新数据重新载入表格
                            ,loading:true
                            ,height: 'full-130'
                            ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                            ,defaultToolbar: ['filter', 'print']
                            ,totalRow: true
                            ,overflow: 'tips'
                            ,page: true
                            ,limit: 1000
                            ,limits: [1000, 2000, 3000]
                            ,done: function () {
                                soulTable.render(this);
                                layer.close(load);
                            }
                        });

                    } else {
                        layer.msg("获取失败！", {icon: 2});
                    }
                }, error: function () {
                    layer.msg("获取失败！", {icon: 2});
                }
            });
        }else if (type == "按天主身"){
            $.ajax({
                url: "/erp/tailormonthreportofeachday",
                type: 'GET',
                data: data,
                success: function (res) {
                    if (res.summaryTailorMonthReportList) {
                        var reportData = res.summaryTailorMonthReportList;
                        table.render({
                            elem:"#cutMonthTable"
                            ,cols:[[
                                {type:'numbers', align:'center', title:'序号', width:90}
                                ,{field:'cutDate', title:'日期', align:'center', width:150, sort: true, filter: true,templet: function (d) {
                                        return moment(d.cutDate).format("YYYY-MM-DD");
                                    }}
                                ,{field:'clothesVersionNumber', title:'单号', align:'center', width:130, sort: true, filter: true, totalRowText: '合计'}
                                ,{field:'orderName', title:'款号', align:'center', width:130, sort: true, filter: true}
                                ,{field:'customerName', title:'客户', align:'center', width:100, sort: true, filter: true}
                                ,{field:'partName', title:'部位', align:'center', width:120, sort: true, filter: true,templet: function (d) {
                                        return "主身布";
                                    }}
                                ,{field:'orderCount', title:'订单总数', align:'center', width:120, sort: true, filter: true, totalRow: true,excel: {cellType: 'n'}}
                                ,{field:'cutCount', title:'总裁数', align:'center', width:120, sort: true, filter: true, totalRow: true,excel: {cellType: 'n'}}
                                ,{field:'differenceCount', title:'对比', align:'center', width:150, sort: true, filter: true, totalRow: true,excel: {cellType: 'n'}}
                                ,{field:'bedNumberCount', title:'区间床数', align:'center',width:120, sort: true, filter: true, totalRow: true,excel: {cellType: 'n'}}
                                ,{field:'pieceCount', title:'区间裁数', align:'center', width:150, sort: true, filter: true, totalRow: true,excel: {cellType: 'n'}}
                                ,{field:'groupName', title:'组别', align:'center', width:150, sort: true, filter: true}
                            ]]
                            ,excel: {
                                filename: '裁床主身布按天汇总报表.xlsx',
                                add: {
                                    top: {
                                        data: [
                                            ['裁床主身布按天汇总报表'], //头部第一行数据，由于我设置了后面的数据merge了，就只写一个
                                            ["从"+from + "到"+to]
                                        ],
                                        heights: [30,15],
                                        merge: [['1,1','1,10'],['2,1','2,3']]
                                    }
                                }
                            }
                            ,data: reportData   // 将新数据重新载入表格
                            ,loading:true
                            ,height: 'full-130'
                            ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                            ,defaultToolbar: ['filter', 'print']
                            ,totalRow: true
                            ,page: true
                            ,limit: 1000
                            ,limits: [1000, 2000, 3000]
                            ,overflow: 'tips'
                            ,done: function () {
                                soulTable.render(this);
                                layer.close(load);
                            }
                        });

                    } else {
                        layer.msg("获取失败！", {icon: 2});
                    }
                }, error: function () {
                    layer.msg("获取失败！", {icon: 2});
                }
            });
        }else if (type == "按天汇总"){
            $.ajax({
                url: "/erp/tailorsummarymonthreportofeachday",
                type: 'GET',
                data: data,
                success: function (res) {
                    if (res.summaryTailorMonthReportList) {
                        var reportData = res.summaryTailorMonthReportList;
                        table.render({
                            elem:"#cutMonthTable"
                            ,cols:[[
                                {type:'numbers', align:'center', title:'序号', width:90}
                                ,{field:'cutDate', title:'日期', align:'center', width:150, sort: true, filter: true,templet: function (d) {
                                        return moment(d.cutDate).format("YYYY-MM-DD");
                                    }}
                                ,{field:'clothesVersionNumber', title:'单号', align:'center', width:130, sort: true, filter: true, totalRowText: '合计'}
                                ,{field:'orderName', title:'款号', align:'center', width:130, sort: true, filter: true}
                                ,{field:'customerName', title:'客户', align:'center', width:100, sort: true, filter: true}
                                ,{field:'partName', title:'部位', align:'center', width:120, sort: true, filter: true}
                                ,{field:'orderCount', title:'订单总数', align:'center', width:120, sort: true, filter: true, totalRow: true,excel: {cellType: 'n'}}
                                ,{field:'cutCount', title:'总裁数', align:'center', width:120, sort: true, filter: true, totalRow: true,excel: {cellType: 'n'}}
                                ,{field:'differenceCount', title:'对比', align:'center', width:150, sort: true, filter: true, totalRow: true,excel: {cellType: 'n'}}
                                ,{field:'bedNumberCount', title:'区间床数', align:'center',width:120, sort: true, filter: true, totalRow: true,excel: {cellType: 'n'}}
                                ,{field:'pieceCount', title:'区间裁数', align:'center', width:150, sort: true, filter: true, totalRow: true,excel: {cellType: 'n'}}
                                ,{field:'groupName', title:'组别', align:'center', width:150, sort: true, filter: true}
                            ]]
                            ,excel: {
                                filename: '裁床按天汇总报表.xlsx',
                                add: {
                                    top: {
                                        data: [
                                            ['裁床按天汇总报表'], //头部第一行数据，由于我设置了后面的数据merge了，就只写一个
                                            ["从"+from + "到"+to]
                                        ],
                                        heights: [30,15],
                                        merge: [['1,1','1,10'],['2,1','2,3']]
                                    }
                                }
                            }
                            ,data: reportData   // 将新数据重新载入表格
                            ,loading:true
                            ,height: 'full-130'
                            ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                            ,defaultToolbar: ['filter', 'print']
                            ,totalRow: true
                            ,page: true
                            ,limit: 1000
                            ,limits: [1000, 2000, 3000]
                            ,overflow: 'tips'
                            ,done: function () {
                                soulTable.render(this);
                                layer.close(load);
                            }
                        });

                    } else {
                        layer.msg("获取失败！", {icon: 2});
                    }
                }, error: function () {
                    layer.msg("获取失败！", {icon: 2});
                }
            });
        }
        return false;
    }


    //监听提交
    form.on('submit(searchBeat)', function(data){
        var from = $("#from").val();
        var to = $("#to").val();
        var groupName = $("#groupName").val();
        var orderName = $("#orderName").val();
        var type = $("#type").val();
        if ((from == null || from === "") && (to == null || to === "")){
            layer.msg("时间段必填", { icon: 2 });
            return false;
        }
        initTable(from, to, groupName, orderName, type);
        return false;
    });

    table.on('toolbar(cutMonthTable)', function(obj) {
        if (obj.event === 'clearFilter') {
            // 清除所有筛选条件并重载表格
            // 参数: tableId
            soulTable.clearFilter('cutMonthTable')
        } else if (obj.event === 'exportExcel') {
            soulTable.export('cutMonthTable');
        } else if (obj.event === 'init') {
            $.ajax({
                url: "/erp/getrecentweekcutinfo",
                type: 'GET',
                data: {},
                success: function (res) {
                    if (res.tailorMatchList) {
                        var reportData = res.tailorMatchList;
                        table.reload("cutMonthTable", {
                            cols: [[
                                {type: 'numbers', align: 'center', title: '序号', width: 90}
                                , {
                                    field: 'clothesVersionNumber',
                                    title: '单号',
                                    align: 'center',
                                    width: 180,
                                    sort: true,
                                    filter: true,
                                    totalRowText: '合计'
                                }
                                , {field: 'orderName', title: '款号', align: 'center', width: 180, sort: true, filter: true}
                                , {field: 'beginCutDate', title: '开裁日期', align: 'center', width: 150, sort: true, filter: true}
                                , {field: 'lastCutDate', title: '最后裁剪日期', align: 'center', width: 180, sort: true, filter: true}
                                , {field: 'colorName', title: '颜色', align: 'center', width: 180, sort: true, filter: true}
                                , {
                                    field: 'orderCount',
                                    title: '订单数',
                                    align: 'center',
                                    width: 120,
                                    sort: true,
                                    filter: true,
                                    totalRow: true,
                                    excel: {cellType: 'n'}
                                }
                                , {
                                    field: 'mainCount',
                                    title: '主身裁数',
                                    align: 'center',
                                    width: 120,
                                    sort: true,
                                    filter: true,
                                    totalRow: true,
                                    excel: {cellType: 'n'}
                                }
                                , {
                                    field: 'otherCount',
                                    title: '下栏最小裁数',
                                    align: 'center',
                                    width: 180,
                                    sort: true,
                                    filter: true,
                                    totalRow: true,
                                    excel: {cellType: 'n'}
                                }
                                , {
                                    field: 'matchCount',
                                    title: '齐套数',
                                    align: 'center',
                                    width: 120,
                                    sort: true,
                                    filter: true,
                                    totalRow: true,
                                    excel: {cellType: 'n'}
                                }
                                , {title: '操作', align: 'center', width: 150, templet: function (d) {
                                        return "<a class='layui-btn layui-btn-xs' lay-href=\"/erp/cutQueryLeakStart?orderName="+d.orderName +"&clothesVersionNumber="+d.clothesVersionNumber+"&type=detail\">\n" +
                                            "                            <cite>详情</cite>\n" +
                                            "                        </a>";
                                    }}
                            ]]
                            , excel: {
                                filename: '裁床主身布月度报表.xlsx',
                                add: {
                                    top: {
                                        data: [
                                            ['裁床主身布月度报表'], //头部第一行数据，由于我设置了后面的数据merge了，就只写一个
                                            ["从" + from + "到" + to]
                                        ],
                                        heights: [30, 15],
                                        merge: [['1,1', '1,10'], ['2,1', '2,3']]
                                    }
                                }
                            }
                            , data: reportData   // 将新数据重新载入表格
                            , overflow: 'tips'
                            ,loading:true
                            , done: function () {
                                soulTable.render(this);
                                layer.close(load);
                            }
                            , filter: {
                                bottom: true,
                                clearFilter: false,
                                items: ['column', 'data', 'condition'] // 只显示表格列和导出excel两个菜单项
                            }
                        });
                    }
                }, error: function () {
                    layer.msg("获取失败！", {icon: 2});
                }
            })
        }
    });

    table.on('tool(cutMonthTable)', function(obj){
        var data = obj.data;
        var orderName = data.orderName;
        var clothesVersionNumber = data.clothesVersionNumber;
        var bedNumber = data.bedNumber;
        var partName = data.partName;
        if(obj.event === 'detail') {
            var tableHtml = "";
            var index = layer.open({
                type: 1 //Page层类型
                , title: '裁床单'
                , btn: ['下载/打印']
                , shade: 0.6 //遮罩透明度
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , content: "<div><div class='row'>" +
                    "        <div id='cutReport'>" +
                    "        </div>" +
                    "    </div>" +
                    "    <iframe id='printf' src='' width='0' height='0' frameborder='0'></iframe></div>"
                , yes: function () {
                    printDeal();
                }
                , cancel : function (i,layero) {
                    $("#cutReport").empty();
                }
            });
            layer.full(index);
            setTimeout(function () {
                $.ajax({
                    url: "tailorreport",
                    type:'GET',
                    data: {
                        orderName: orderName,
                        partName: partName,
                        bedNumber: bedNumber
                    },
                    success: function (res) {
                        $("#cutReport").empty();
                        if(res) {
                            tableHtml = "<div class=\"col-md-12\" name='printTable'>\n" +
                                "               <section class=\"panel panel-default\" style='page-break-after:always;'>\n" +
                                "                   <header class=\"panel-heading font-bold\" style=\"text-align: center\">\n" +
                                "                       <div style=\"font-size: 20px;font-weight: 700\">德悦服饰裁床单</div><br>\n" +
                                "                       <div style=\"font-size: 14px;float: center;font-weight: 700\">版单："+clothesVersionNumber+"&nbsp;&nbsp;&nbsp;&nbsp;订单："+orderName +"&nbsp;&nbsp;&nbsp;&nbsp;部位:"+ partName + "&nbsp;&nbsp;&nbsp;&nbsp;床号:"+ bedNumber +"&nbsp;&nbsp;&nbsp;&nbsp;"+ moment(res.cutDate).format("YYYY-MM-DD") +"</div>\n"+
                                "                   </header>\n" +
                                "                   <table class=\"table table-bordered\" style='width: 100%'>\n" +
                                "                       <tbody>";
                            tableHtml += "<tr style='font-weight: 700'><td style='background-color: #0f9d58'>扎号</td><td>床号</td><td>颜色</td><td>尺码</td><td>数量</td><td>缸号</td><td style='background-color: #0f9d58'>扎号</td><td>床号</td><td>颜色</td><td>尺码</td><td>数量</td><td>缸号</td><td style='background-color: #0f9d58'>扎号</td><td>床号</td><td>颜色</td><td>尺码</td><td>数量</td><td>缸号</td></tr>";
                            var detail = res.detail;
                            var summary = res.color;
                            var jarData = res.jar;
                            var sizeHeadList = res.size;
                            var oneLength = res.oneLength;
                            var twoLength = res.twoLength;
                            var threeLength = res.threeLength;
                            var index_num = 1;
                            sizeHeadList = globalSizeSort(sizeHeadList);
                            if (detail){
                                // var height = Math.floor(detail.length/3);
                                // var modHeight = detail.length % 3;
                                if (oneLength > 0){
                                    if (oneLength == twoLength && oneLength == threeLength){
                                        for (var i = 0; i < oneLength; i++ ){
                                            tableHtml += "<tr><td style='background-color: #0f9d58'>" + detail[i].packageNumber+ "</td><td>" + detail[i].bedNumber + "</td><td>" + detail[i].colorName + "</td><td>" + detail[i].sizeName + "</td><td>" + detail[i].layerCount + "</td><td>" + detail[i].jarName + "</td><td style='background-color: #0f9d58'>" + detail[i+oneLength].packageNumber + "</td><td>" + detail[i+oneLength].bedNumber + "</td><td>" + detail[i+oneLength].colorName + "</td><td>" + detail[i+oneLength].sizeName + "</td><td>" + detail[i+oneLength].layerCount + "</td><td>" + detail[i+oneLength].jarName+ "</td><td style='background-color: #0f9d58'>" + detail[i+oneLength+twoLength].packageNumber + "</td><td>" + detail[i+oneLength+twoLength].bedNumber  + "</td><td>" + detail[i+oneLength+twoLength].colorName + "</td><td>" + detail[i+oneLength+twoLength].sizeName + "</td><td>" + detail[i+oneLength+twoLength].layerCount + "</td><td>" + detail[i+oneLength+twoLength].jarName + "</td></tr>";
                                            index_num ++;
                                        }
                                    } else if (oneLength == twoLength && oneLength > threeLength){
                                        for (var i = 0; i < oneLength - 1; i++ ){
                                            tableHtml += "<tr><td style='background-color: #0f9d58'>" + detail[i].packageNumber + "</td><td>"+ detail[i].bedNumber + "</td><td>" + detail[i].colorName + "</td><td>" + detail[i].sizeName + "</td><td>" + detail[i].layerCount + "</td><td>" + detail[i].jarName + "</td><td style='background-color: #0f9d58'>" + detail[i+oneLength].packageNumber + "</td><td>" + detail[i+oneLength].bedNumber + "</td><td>" + detail[i+oneLength].colorName + "</td><td>" + detail[i+oneLength].sizeName + "</td><td>" + detail[i+oneLength].layerCount + "</td><td>" + detail[i+oneLength].jarName+ "</td><td style='background-color: #0f9d58'>" + detail[i+oneLength+twoLength].packageNumber + "</td><td>" + detail[i+oneLength+twoLength].bedNumber + "</td><td>" + detail[i+oneLength+twoLength].colorName + "</td><td>" + detail[i+oneLength+twoLength].sizeName + "</td><td>" + detail[i+oneLength+twoLength].layerCount + "</td><td>" + detail[i+oneLength+twoLength].jarName + "</td></tr>";
                                            index_num ++;
                                        }
                                        tableHtml += "<tr><td style='background-color: #0f9d58'>" + detail[oneLength - 1].packageNumber+ "</td><td>" +detail[oneLength - 1].bedNumber + "</td><td>" + detail[oneLength - 1].colorName + "</td><td>" + detail[oneLength - 1].sizeName + "</td><td>" + detail[oneLength - 1].layerCount + "</td><td>" + detail[oneLength - 1].jarName + "</td><td style='background-color: #0f9d58'>" + detail[oneLength + twoLength - 1].packageNumber + "</td><td>"+ detail[oneLength + twoLength - 1].bedNumber + "</td><td>" + detail[oneLength + twoLength - 1].colorName + "</td><td>" + detail[oneLength + twoLength - 1].sizeName + "</td><td>" + detail[oneLength + twoLength - 1].layerCount + "</td><td>" + detail[oneLength + twoLength - 1].jarName+ "</td><td></td><td></td><td></td><td></td><td></td></tr>";
                                        index_num ++;
                                    } else if (oneLength > twoLength && oneLength > threeLength){
                                        for (var i = 0; i < oneLength - 1; i++ ){
                                            tableHtml += "<tr><td style='background-color: #0f9d58'>" + detail[i].packageNumber + "</td><td>" + detail[i].bedNumber + "</td><td>" + detail[i].colorName + "</td><td>" + detail[i].sizeName + "</td><td>" + detail[i].layerCount + "</td><td>" + detail[i].jarName + "</td><td style='background-color: #0f9d58'>" + detail[i+oneLength].packageNumber + "</td><td>" + detail[i+oneLength].bedNumber + "</td><td>" + detail[i+oneLength].colorName + "</td><td>" + detail[i+oneLength].sizeName + "</td><td>" + detail[i+oneLength].layerCount + "</td><td>" + detail[i+oneLength].jarName+ "</td><td style='background-color: #0f9d58'>" + detail[i+oneLength+twoLength].packageNumber + "</td><td>" + detail[i+oneLength+twoLength].bedNumber + "</td><td>" + detail[i+oneLength+twoLength].colorName + "</td><td>" + detail[i+oneLength+twoLength].sizeName + "</td><td>" + detail[i+oneLength+twoLength].layerCount + "</td><td>" + detail[i+oneLength+twoLength].jarName + "</td></tr>";
                                            index_num ++;
                                        }
                                        tableHtml += "<tr><td style='background-color: #0f9d58'>" + detail[oneLength - 1].packageNumber + "</td><td>" + detail[oneLength - 1].bedNumber + "</td><td>" + detail[oneLength - 1].colorName + "</td><td>" + detail[oneLength - 1].sizeName + "</td><td>" + detail[oneLength - 1].layerCount + "</td><td>" + detail[oneLength - 1].jarName + "</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
                                        index_num ++;
                                    }

                                }
                            }
                            tableHtml +=  "                       </tbody>" +
                                "                   </table>";

                            if (summary) {
                                tableHtml += "                   <table class=\"table table-bordered\" style='width: 100%'>\n" +
                                    "                       <tbody>";
                                var sizeRow = [];
                                sizeRow[0] = [];
                                var sizeSumRow = [];
                                sizeSumRow[0] = ["小计"];
                                var colorIndex = 0;
                                var totalSum = 0;

                                if (sizeHeadList){
                                    var widthSummary = sizeHeadList.length + 2;
                                    tableHtml += "<tr><td colspan='"+ widthSummary +"' style='text-align:center; font-size: 15px; font-weight: 700'></td></tr>";
                                    tableHtml += "<tr><td colspan='"+ widthSummary +"' style='text-align:center; font-size: 15px; font-weight: 700'>汇总</td></tr>";
                                    tableHtml += "<tr style='font-weight: 700'><td></td>";
                                    var flag = true;
                                    for (var j = 0; j < sizeHeadList.length; j++){
                                        tableHtml += "<td>" + sizeHeadList[j]+"</td>";
                                        sizeSumRow[j+1] = 0;
                                    }
                                    tableHtml += "<td>合计</td></tr>";
                                }

                                for (var colorKey in summary){
                                    var sizeData = summary[colorKey];
                                    var colorSum = 0;
                                    tableHtml += "<tr><td>" + colorKey + "</td>";
                                    for (var m = 0; m < sizeHeadList.length; m ++){
                                        if (sizeData[sizeHeadList[m]]){
                                            colorSum += sizeData[sizeHeadList[m]];
                                            tableHtml += "<td>" + sizeData[sizeHeadList[m]] + "</td>"
                                            sizeSumRow[m+1] += sizeData[sizeHeadList[m]];
                                        }else {
                                            tableHtml += "<td>0</td>";
                                        }
                                    }
                                    tableHtml += "<td>" + colorSum + "</td></tr>";
                                    totalSum += colorSum;
                                    tableHtml += "<tr><td>小计</td>";
                                    index_num ++;
                                }

                                for (var n = 1; n < sizeSumRow.length; n++){
                                    tableHtml += "<td>" + sizeSumRow[n] + "</td>";
                                }
                                tableHtml += "<td>" + totalSum + "</td></tr>";
                                index_num ++;
                            }

                            tableHtml +=  "                       </tbody>" +
                                "                   </table>";
                            if (jarData){
                                tableHtml += "                   <table class=\"table table-bordered\" style='width: 100%'>\n" +
                                    "                       <tbody>";


                                if (sizeHeadList){
                                    var widthSummary = sizeHeadList.length + 4;
                                    tableHtml += "<tr><td colspan='"+ widthSummary +"' style='text-align:center; font-size: 15px; font-weight: 700'></td></tr>";
                                    tableHtml += "<tr><td colspan='"+ widthSummary +"' style='text-align:center; font-size: 15px; font-weight: 700'>缸号汇总</td></tr>";
                                    tableHtml += "<tr style='font-weight: 700'><td>颜色</td><td>LO色</td><td>缸号</td>";
                                    var flag = true;
                                    for (var j = 0; j < sizeHeadList.length; j++){
                                        tableHtml += "<td>" + sizeHeadList[j]+"</td>";
                                    }
                                    tableHtml += "<td>合计</td></tr>";
                                }
                                for (var loColor in jarData){
                                    var loData = jarData[loColor];
                                    for (var lo in loData){
                                        var loSum = 0;
                                        var loSizeSum = [];
                                        loSizeSum[0] = [];
                                        loSizeSum[1] = [];
                                        loSizeSum[2] = [];
                                        $.each(sizeHeadList,function (index, item) {
                                            loSizeSum[index + 3] = 0;
                                        });
                                        loSizeSum[sizeHeadList.length + 3] = 0;


                                        var loJarData = loData[lo];
                                        for (var jar in loJarData){
                                            tableHtml += "<tr><td>" + loColor + "</td><td>" + lo +"</td><td>"+ jar +"</td>";
                                            var loSizeData = loJarData[jar];
                                            var jarSum = 0;
                                            $.each(sizeHeadList,function (index, item) {
                                                if (loSizeData[item]){
                                                    tableHtml += "<td>" + loSizeData[item] + "</td>";
                                                    jarSum += loSizeData[item];
                                                    loSizeSum[index + 3] += loSizeData[item]
                                                } else {
                                                    tableHtml += "<td>0</td>";
                                                }
                                            });
                                            tableHtml += "<td>" + jarSum + "</td></tr>";
                                            loSum += jarSum;
                                            loSizeSum[sizeHeadList.length + 3] += jarSum;
                                        }
                                        tableHtml += "<tr><td></td><td></td><td>小计</td>";
                                        for (var a = 3; a < loSizeSum.length; a ++){
                                            tableHtml += "<td>" + loSizeSum[a] + "</td>";
                                        }
                                        tableHtml += "</tr>";
                                    }
                                }

                            }
                            tableHtml +=  "                       </tbody>" +
                                "                   </table>" +
                                "               </section>" +
                                "              </div>";
                            $("#cutReport").append(tableHtml);
                        }else {
                            layer.msg("未查到相关信息。。。")
                        }
                    },
                    error: function () {
                        layer.msg("查询出错。。。")
                    }
                })
            },500)
        }
    });

});

function toDecimal(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*100)/100;
    return f;
}