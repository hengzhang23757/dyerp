var tailorQcodeID = [];
var basePath=$("#basePath").val();
var employeeNumber = $("#userName").val();
var employeeName = $("#employeeName").val();
var groupName = $("#groupName").val();
var isOutBound = false;
var spn;
layui.use('form', function(){
    var form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功


    form.on('switch(outBound)', function(data){
        isOutBound = data.elem.checked; //开关是否开启，true或者false
        if (data.elem.checked){
            $("#groupLable").show();
            $("#groupNameSelectTd").show();
        } else {
            $("#groupLable").hide();
            $("#groupNameSelectTd").hide();
        }
    });
    //……

    //但是，如果你的HTML是动态生成的，自动渲染就会失效
    //因此你需要在相应的地方，执行下述方法来手动渲染，跟这类似的还有 element.init();
    form.render();
})
$(document).ready(function () {

    layui.use(['autocomplete'], function () {
        layui.autocomplete.render({
            elem: $('#clothesVersionNumber')[0],
            url: 'getversionhint',
            template_val: '{{d}}',
            template_txt: '{{d}}',
            onselect: function (resp) {
                autoComplete(resp)
            }
        })
    });


    $("#selectOrderName").change(function () {
        var selectOrderName = $("#selectOrderName").val();
        $.ajax({
            url: "/erp/getdispatchbyordernameemployeenumber",
            data: {
                "orderName": selectOrderName,
                "employeeNumber":employeeNumber
            },
            success:function(data){
                $("#procedureInfo").append("<option value=''>全部</option>");
                $.each(data.dispatchList, function(index,element){
                    $("#procedureInfo").append("<option value='"+element.procedureNumber+"'>"+element.procedureName+"</option>");
                })
            },
            error:function(){
            }
        });
    });


    // $("#tailorQcode")[0].focus();
    document.onkeyup = function (event) {
        e = event ? event : (window.event ? window.event : null);
        if(e.keyCode==13 && e.target.getAttribute("id")=="tailorQcode") {
            var areaValue = $("#tailorQcode").val().split("\n");
            if(areaValue[0] == "") {
                Swal.fire({
                    type:"warning",
                    title:"<span style=\"font-weight:bolder;font-size: 20px\">二维码内容为空，请重新扫描！</span>",
                    timer: 1000,
                    width: 500,
                    showConfirmButton: false
                });
                $("#tailorQcode").val("");
                return false;
            }
            var qrCode = [];
            qrCode.push(areaValue[0]);
            $("#scanNum").html("已扫描1扎");
            var paramData = {};
            if (spn == null || spn == ''){
                Swal.fire({
                    type:"warning",
                    title:"<span style=\"font-weight:bolder;font-size: 20px\">没有选中工序,请扫描工序二维码！</span>",
                    timer: 1000,
                    width: 500,
                    showConfirmButton: false
                });
                $("#tailorQcode").val("");
                return false;
            }
            paramData.procedureNumber = spn;
            paramData.tailorQcodeID = qrCode[0];
            paramData.employeeNumber = employeeNumber;
            paramData.employeeName = employeeName;
            paramData.groupName = groupName;
            if (isOutBound){
                paramData.isOutBound = isOutBound;
                paramData.outBoundGroupName = $("#groupNameSelect").val();
            }
            $.ajax({
                url: basePath + "erp/addotherpieceworkbatch",
                type:'POST',
                data: paramData,
                success: function (data) {
                    var packageCount = 0;
                    var sumLayerCount = 0;
                    $("#pieceWorkDetailDiv").empty();
                    var tableHtml = "<table class=\"table table-striped table-bordered\">\n" +
                        "                    <thead style=\"font-size: 16px;font-weight: bold; color: rgb(45, 202, 147)\">\n" +
                        "                    <tr>\n" +
                        "                        <th style=\"text-align: center\">款号</th>\n" +
                        "                        <th style=\"text-align: center\">床号</th>\n" +
                        "                        <th style=\"text-align: center\">颜色</th>\n" +
                        "                        <th style=\"text-align: center\">尺码</th>\n" +
                        "                        <th style=\"text-align: center\">扎号</th>\n" +
                        "                        <th style=\"text-align: center\">数量</th>\n" +
                        "                        <th style=\"text-align: center\">工序号</th>\n" +
                        "                        <th style=\"text-align: center\">工序名</th>\n" +
                        "</tr>\n"+
                        "</thead>";

                    if (data.packageCount && data.totalLayer){
                        tableHtml += "<tr><td colspan='4'></td><td style='text-align: center'>" + data.packageCount + "扎</td><td style='text-align: center'>" + data.totalLayer + "件</td><td></td><td></td></tr>";
                    }


                    if (data.pieceWorkEmpToday) {
                        $.each(data.pieceWorkEmpToday, function(index,element){
                            packageCount += 1;
                            sumLayerCount += element.layerCount;
                            tableHtml += "<tr><td style='text-align: center'>"+element.orderName+"</td><td style='text-align: center'>"+element.bedNumber+"</td><td style='text-align: center'>"+element.colorName+"</td><td style='text-align: center'>"+element.sizeName+"</td><td style='text-align: center'>"+element.packageNumber+"</td><td style='text-align: center'>"+element.layerCount+"</td><td style='text-align: center'>"+element.procedureNumber+"</td><td style='text-align: center'>"+element.procedureName+"</td>";
                            tableHtml += "<tr>\n"
                        });
                    }
                    // tableHtml += "<tr><td style=\"text-align: center\" colspan='4'>小计</td><td style=\"text-align: center\">"+packageCount+"扎</td><td style=\"text-align: center\">"+sumLayerCount+"件</td><td></td><td></td>";
                    // tableHtml += "</tr>\n";
                    tableHtml += "</table>";
                    $("#pieceWorkDetailDiv").append(tableHtml);
                    if(data.code == 0) {
                        $.unblockUI();
                        var content = "无记录";
                        var tailor = data.tailor;
                        $("#customerName").val(tailor.customerName);
                        $("#orderName").val(tailor.orderName);
                        $("#packageNumber").val(tailor.packageNumber);
                        $("#colorName").val(tailor.colorName);
                        $("#sizeName").val(tailor.sizeName);
                        $("#thisLayerCount").val(tailor.layerCount);
                        if (data.pieceWorkSummary){
                            content = "<div style='text-align: center'><h3 style='font-weight: bold'><span style='text-align: center'>"+tailor.orderName+"  "+tailor.colorName+"  "+tailor.sizeName+"  "+tailor.bedNumber+"床  "+tailor.packageNumber+"扎  "+tailor.partName+"  "+tailor.layerCount+"件  "+"</span></h3>"
                                + "<p style='color: #F00; text-align: center'>0.5秒后关闭！历史计件信息</p>"
                                + "<table class='table table-striped table-bordered' style='text-align: center; font-size: 15px'>"
                                + "<tr style='text-align: center; color: #00FFFF'>"
                                + "     <th style='text-align: center'>工号</th><th style='text-align: center'>姓名</th><th style='text-align: center'>工序号</th><th style='text-align: center'>工序名</th><th style='text-align: center'>时间</th>"
                                + "</tr>";
                            $.each(data.pieceWorkSummary,function (index, item) {
                                content += "<tr><td>"+item.employeeNumber+"</td><td>"+item.employeeName+"</td><td>"+item.procedureNumber+"</td><td>"+item.procedureName+"</td><td>"+item.pieceTime+"</td></tr>";
                            });
                            content += "</table></div>";
                        }
                        Swal.fire({
                            type:"success",
                            title:"<span style=\"font-weight:bolder;color:#F00;font-size: 30px\">"+data.msg+"</span>",
                            html: content,
                            timer: 1000,
                            width: 800,
                            showConfirmButton: false
                        });
                        var url1 = "http://tts.baidu.com/text2audio?lan=zh&ie=UTF-8&text=" + encodeURI(data.msg);// baidu
                        var n1 = new Audio(url1);
                        n1.src = url1;
                        n1.play();
                        setTimeout (function(){
                            $("#tailorQcode").val("");
                            $("#scanNum").html("已扫描0扎");
                        },1000);
                    }else if(data.code == 1) {
                        var content = "无记录";
                        var tailor = data.tailor;
                        $("#customerName").val(tailor.customerName);
                        $("#orderName").val(tailor.orderName);
                        $("#packageNumber").val(tailor.packageNumber);
                        $("#colorName").val(tailor.colorName);
                        $("#sizeName").val(tailor.sizeName);
                        $("#thisLayerCount").val(tailor.layerCount);
                        if (data.pieceWorkSummary){
                            content = "<div style='text-align: center'><h3 style='font-weight: bold'><span style='text-align: center'>"+tailor.orderName+"  "+tailor.colorName+"  "+tailor.sizeName+"  "+tailor.bedNumber+"床  "+tailor.packageNumber+"扎  "+tailor.partName+"  "+tailor.layerCount+"件  "+"</span></h3>"
                                + "<p style='color: #F00; text-align: center'>0.5秒后关闭！历史计件信息</p>"
                                + "<table class='table table-striped table-bordered' style='text-align: center; font-size: 15px'>"
                                + "<tr style='text-align: center; color: #00FFFF'>"
                                + "     <th style='text-align: center'>工号</th><th style='text-align: center'>姓名</th><th style='text-align: center'>工序号</th><th style='text-align: center'>工序名</th><th style='text-align: center'>时间</th>"
                                + "</tr>";
                            $.each(data.pieceWorkSummary,function (index, item) {
                                content += "<tr><td>"+item.employeeNumber+"</td><td>"+item.employeeName+"</td><td>"+item.procedureNumber+"</td><td>"+item.procedureName+"</td><td>"+item.pieceTime+"</td></tr>";
                            });
                            content += "</table></div>";
                        }
                        Swal.fire({
                            type:"error",
                            title: "<span style=\"font-weight:bolder;color:#F00;font-size: 30px\">"+data.msg+"</span>",
                            html: content,
                            width: 800
                        })
                        var url2 = "http://tts.baidu.com/text2audio?lan=zh&ie=UTF-8&text=" + encodeURI(data.msg);// baidu
                        var n2 = new Audio(url2);
                        n2.src = url2;
                        n2.play();
                        setTimeout (function(){
                            $("#tailorQcode").val("");
                            $("#scanNum").html("已扫描0扎");
                        },1000);
                    }else if(data.code == 2) {
                        Swal.fire({
                            type:"warning",
                            width: 500,
                            timer: 1000,
                            title: "<span style=\"font-weight:bolder;font-size: 30px;color: #F00\">二维码信息不存在！</span>",
                            showConfirmButton: false
                        });
                        var url3 = "http://tts.baidu.com/text2audio?lan=zh&ie=UTF-8&text=" + encodeURI("二维码信息不存在");// baidu
                        var n3 = new Audio(url3);
                        n3.src = url3;
                        n3.play();
                        setTimeout (function(){
                            $("#tailorQcode").val("");
                            $("#scanNum").html("已扫描0扎");
                        },1000);
                    }else if(data.code == 3) {
                        var content = "无记录";
                        var tailor = data.tailor;
                        $("#customerName").val(tailor.customerName);
                        $("#orderName").val(tailor.orderName);
                        $("#packageNumber").val(tailor.packageNumber);
                        $("#colorName").val(tailor.colorName);
                        $("#sizeName").val(tailor.sizeName);
                        $("#thisLayerCount").val(tailor.layerCount);
                        if (data.pieceWorkSummary){
                            content = "<div style='text-align: center'><h3 style='font-weight: bold'><span style='text-align: center'>"+tailor.orderName+"  "+tailor.colorName+"  "+tailor.sizeName+"  "+tailor.bedNumber+"床  "+tailor.packageNumber+"扎  "+tailor.partName+"  "+tailor.layerCount+"件  "+"</span></h3>"
                                + "<p style='color: #F00; text-align: center'>历史计件信息</p>"
                                + "<table class='table table-striped table-bordered' style='text-align: center; font-size: 15px'>"
                                + "<tr style='text-align: center; color: #00FFFF'>"
                                + "     <th style='text-align: center'>工号</th><th style='text-align: center'>姓名</th><th style='text-align: center'>工序号</th><th style='text-align: center'>工序名</th><th style='text-align: center'>时间</th>"
                                + "</tr>";
                            $.each(data.pieceWorkSummary,function (index, item) {
                                content += "<tr><td>"+item.employeeNumber+"</td><td>"+item.employeeName+"</td><td>"+item.procedureNumber+"</td><td>"+item.procedureName+"</td><td>"+item.pieceTime+"</td></tr>";
                            });
                            content += "</table></div>";
                        }
                        Swal.fire({
                            type: "info",
                            width: 800,
                            title: "<span style=\"font-weight:bolder;font-size: 30px;color: #F00\">"+ data.msg +"</span>",
                            html: content
                        })
                        var url4 = "http://tts.baidu.com/text2audio?lan=zh&ie=UTF-8&text=" + encodeURI(data.msg);// baidu
                        var n4 = new Audio(url4);
                        n4.src = url4;
                        n4.play();
                        setTimeout (function(){
                            $("#tailorQcode").val("");
                            $("#scanNum").html("已扫描0扎");
                        },1000);
                    }else if(data.code == 4) {
                        var content = "无记录";
                        var tailor = data.tailor;
                        $("#customerName").val(tailor.customerName);
                        $("#orderName").val(tailor.orderName);
                        $("#packageNumber").val(tailor.packageNumber);
                        $("#colorName").val(tailor.colorName);
                        $("#sizeName").val(tailor.sizeName);
                        $("#thisLayerCount").val(tailor.layerCount);
                        if (data.pieceWorkSummary){
                            content = "<div style='text-align: center'><h3 style='font-weight: bold'><span style='text-align: center'>"+tailor.orderName+"  "+tailor.colorName+"  "+tailor.sizeName+"  "+tailor.bedNumber+"床  "+tailor.packageNumber+"扎  "+tailor.partName+"  "+tailor.layerCount+"件  "+"</span></h3>"
                                + "<p style='color: #F00; text-align: center'>2秒后关闭！历史计件信息</p>"
                                + "<table class='table table-striped table-bordered' style='text-align: center; font-size: 15px'>"
                                + "<tr style='text-align: center; color: #00FFFF'>"
                                + "     <th style='text-align: center'>工号</th><th style='text-align: center'>姓名</th><th style='text-align: center'>工序号</th><th style='text-align: center'>工序名</th><th style='text-align: center'>时间</th>"
                                + "</tr>";
                            $.each(data.pieceWorkSummary,function (index, item) {
                                content += "<tr><td>"+item.employeeNumber+"</td><td>"+item.employeeName+"</td><td>"+item.procedureNumber+"</td><td>"+item.procedureName+"</td><td>"+item.pieceTime+"</td></tr>";
                            });
                            content += "</table></div>";
                        }
                        Swal.fire({
                            type:"warning",
                            title:"<span style=\"font-weight:bolder;color:#F00;font-size: 30px\">重复计件！</span>",
                            html: content,
                            timer: 2000,
                            width: 800,
                            showConfirmButton: false
                        })
                        var url5 = "http://tts.baidu.com/text2audio?lan=zh&ie=UTF-8&text=" + encodeURI("重复计件");// baidu
                        var n5 = new Audio(url5);
                        n5.src = url5;
                        n5.play();
                        setTimeout (function(){
                            $("#tailorQcode").val("");
                            $("#scanNum").html("已扫描0扎");
                        },2000);
                    }else if(data.code == 5) {
                        var content = "无记录";
                        var tailor = data.tailor;
                        $("#customerName").val(tailor.customerName);
                        $("#orderName").val(tailor.orderName);
                        $("#packageNumber").val(tailor.packageNumber);
                        $("#colorName").val(tailor.colorName);
                        $("#sizeName").val(tailor.sizeName);
                        $("#thisLayerCount").val(tailor.layerCount);
                        if (data.pieceWorkSummary){
                            content = "<div style='text-align: center'><h3 style='font-weight: bold'><span style='text-align: center'>"+tailor.orderName+"  "+tailor.colorName+"  "+tailor.sizeName+"  "+tailor.bedNumber+"床  "+tailor.packageNumber+"扎  "+tailor.partName+"  "+tailor.layerCount+"件  "+"</span></h3>"
                                + "<p style='color: #F00; text-align: center'>2秒后关闭！历史计件信息</p>"
                                + "<table class='table table-striped table-bordered' style='text-align: center; font-size: 15px'>"
                                + "<tr style='text-align: center; color: #00FFFF'>"
                                + "     <th style='text-align: center'>工号</th><th style='text-align: center'>姓名</th><th style='text-align: center'>工序号</th><th style='text-align: center'>工序名</th><th style='text-align: center'>时间</th>"
                                + "</tr>";
                            $.each(data.pieceWorkSummary,function (index, item) {
                                content += "<tr><td>"+item.employeeNumber+"</td><td>"+item.employeeName+"</td><td>"+item.procedureNumber+"</td><td>"+item.procedureName+"</td><td>"+item.pieceTime+"</td></tr>";
                            });
                            content += "</table></div>";
                        }
                        Swal.fire({
                            type:"warning",
                            title:"<span style=\"font-weight:bolder;color:#F00;font-size: 30px\">部分工序重复计件！</span>",
                            html: content,
                            timer: 2000,
                            width: 800,
                            showConfirmButton: false
                        })
                        var url5 = "http://tts.baidu.com/text2audio?lan=zh&ie=UTF-8&text=" + encodeURI("部分重复计件");// baidu
                        var n5 = new Audio(url5);
                        n5.src = url5;
                        n5.play();
                        setTimeout (function(){
                            $("#tailorQcode").val("");
                            $("#scanNum").html("已扫描0扎");
                        },2000);
                    }else if(data.code == 6) {
                        var content = "无记录";
                        var tailor = data.tailor;
                        $("#customerName").val(tailor.customerName);
                        $("#orderName").val(tailor.orderName);
                        $("#packageNumber").val(tailor.packageNumber);
                        $("#colorName").val(tailor.colorName);
                        $("#sizeName").val(tailor.sizeName);
                        $("#thisLayerCount").val(tailor.layerCount);
                        if (data.pieceWorkSummary){
                            content = "<div style='text-align: center'><h3 style='font-weight: bold'><span style='text-align: center'>"+tailor.orderName+"  "+tailor.colorName+"  "+tailor.sizeName+"  "+tailor.bedNumber+"床  "+tailor.packageNumber+"扎  "+tailor.partName+"  "+tailor.layerCount+"件  "+"</span></h3>"
                                + "<p style='color: #F00; text-align: center'>2秒后关闭！历史计件信息</p>"
                                + "<table class='table table-striped table-bordered' style='text-align: center; font-size: 15px'>"
                                + "<tr style='text-align: center; color: #00FFFF'>"
                                + "     <th style='text-align: center'>工号</th><th style='text-align: center'>姓名</th><th style='text-align: center'>工序号</th><th style='text-align: center'>工序名</th><th style='text-align: center'>时间</th>"
                                + "</tr>";
                            $.each(data.pieceWorkSummary,function (index, item) {
                                content += "<tr><td>"+item.employeeNumber+"</td><td>"+item.employeeName+"</td><td>"+item.procedureNumber+"</td><td>"+item.procedureName+"</td><td>"+item.pieceTime+"</td></tr>";
                            });
                            content += "</table></div>";
                        }
                        Swal.fire({
                            type:"warning",
                            title:"<span style=\"font-weight:bolder;color:#F00;font-size: 30px\">扫描部位不正确！</span>",
                            html: content,
                            timer: 2000,
                            width: 800,
                            showConfirmButton: false
                        })
                        var url5 = "http://tts.baidu.com/text2audio?lan=zh&ie=UTF-8&text=" + encodeURI("扫描部位不正确");// baidu
                        var n5 = new Audio(url5);
                        n5.src = url5;
                        n5.play();
                        setTimeout (function(){
                            $("#tailorQcode").val("");
                            $("#scanNum").html("已扫描0扎");
                        },2000);
                    }else if(data.code == 7) {
                        Swal.fire({
                            type: "warning",
                            width: 1000,
                            timer: 500,
                            title: "<span style=\"font-weight:bolder;font-size: 30px;color: #F00\">配扎工序请扫描主身！</span>",
                            showConfirmButton: false
                        });
                        var url3 = "http://tts.baidu.com/text2audio?lan=zh&ie=UTF-8&text=" + encodeURI("配扎工序请扫描主身");// baidu
                        var n3 = new Audio(url3);
                        n3.src = url3;
                        n3.play();
                        setTimeout(function () {
                            $("#tailorQcode").val("");
                            $("#scanNum").html("已扫描0扎");
                        }, 1000);
                    }
                }, error: function () {
                    Swal.fire({type:"error",width: 500,title: "<span style=\"font-weight:bolder;font-size: 30px;color: #F00\">服务发生未知错误～</span>"});
                }
            })

        }
    }

});


function autoComplete(keywords) {
    $("#selectOrderName").empty();
    $.ajax({
        url: "/erp/getorderbyversion",
        data: {"clothesVersionNumber": keywords},
        success:function(data){
            $("#selectOrderName").empty();
            if (data.orderList) {
                $("#selectOrderName").append("<option value=''>订单号</option>");
                $.each(data.orderList, function(index,element){
                    $("#selectOrderName").append("<option value='"+element+"'>"+element+"</option>");
                })
            }
        },
        error:function(){
        }
    });
}


function procedureSelect(obj) {
    $.blockUI({
        css: {
            top: '30%',
            border: 'none',
            padding: '15px',
            backgroundColor: '#fff',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: 1,
            color: '#000'
        },
        message: $('#chooseDiv')
    });
    $("#procedureNumberCode").focus();
    $("#procedureNumberCode").val("");
    $('#procedureNumberCode').keydown(function(e){
        if(e.keyCode == 13) {
            var scanCode = $("#procedureNumberCode").val();
            if(scanCode == "") {
                Swal.fire({
                    type:"warning",
                    title:"<span style=\"font-weight:bolder;font-size: 20px\">工序选择为空！</span>",
                    timer: 1000,
                    width: 500,
                    showConfirmButton: false
                });
                $("#procedureNumberCode").val("");
                return false;
            }
            spn = scanCode;
            $("#selectProcedureNumber").text("点击扫描工序 已选工序号:" + scanCode);
            $.unblockUI();
            $("#tailorQcode").empty();
        }
    });

    $("#chooseNo").unbind("click").bind("click", function () {
        $("#procedureNumberCode").val("");
        $.unblockUI();
    });
}

