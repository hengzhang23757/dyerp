var hot;
var basePath=$("#basePath").val();

$(document).ready(function () {
    var container = document.getElementById('addInnerBoardExcel');
    hot = new Handsontable(container, {
        // data: data,
        rowHeaders: true,
        // colHeaders: true,
        colHeaders:['订单信息','季节','版单号','面料名称','面料编号','面料颜色','面料色号','成衣工厂','合作模式','面料开发工厂','交期','打板数量','单位','回布时间','缸号','回布数量','内评板发布数量','内评板后剩余数量','订货板发布数量','订货板后剩余数量'],
        autoColumnSize:true,
        dropdownMenu: true,
        contextMenu:true,
        minRows:20,
        minCols:20,
        // colWidths:70,
        language:'zh-CN',
        licenseKey: 'non-commercial-and-evaluation'
    });
});

function add() {
    swal({
        title: "",
        text: "<span style=\"font-weight:bolder;font-size: 20px\">您确定要提交输入的内板信息吗？</span>",
        type: "warning",
        html:true,
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: "确定",
        cancelButtonText:"我再想想",
        confirmButtonColor: "#ec6c62",
        showLoaderOnConfirm: false
    }, function() {
        var innerBoardJson = [];
        var data = hot.getData();
        // console.log(data);
        // var titleRow = data[0];
        // var endIndex = titleRow.length;
        // $.each(titleRow,function (index, item) {
        //     if(item==null || item=="") {
        //         endIndex = index;
        //         return false;
        //     }
        // });
        // titleRow.splice(endIndex,titleRow.length-endIndex);
        $.each(data,function (index, item) {
            if(item[0]!=null && item[0]!="") {
                var innerBoard = {};
                innerBoard.orderInfo = item[0].trim();
                innerBoard.season = item[1].trim();
                innerBoard.versionNumber = item[2].trim();
                innerBoard.fabricName = item[3].trim();
                innerBoard.fabricNumber = item[4].trim();
                innerBoard.fabricColor = item[5].trim();
                innerBoard.fabricColorNumber = item[6].trim();
                innerBoard.clothingFactory = item[7].trim();
                innerBoard.cooperateMode = item[8].trim();
                innerBoard.fabricFactory = item[9].trim();
                innerBoard.deliveryDate = item[10].trim();
                innerBoard.innerBoardCount = item[11].trim();
                innerBoard.unit = item[12].trim();
                innerBoard.returnFabricDate = item[13].trim();
                innerBoard.jarNumber = item[14].trim();
                innerBoard.returnFabricCount = item[15].trim();
                innerBoard.innerBoardPublishCount = item[16].trim();
                innerBoard.innerBoardRemaindCount = item[17].trim();
                innerBoard.orderBoardPublishCount = item[18].trim();
                innerBoard.orderBoardRemaindCount = item[19].trim();
                innerBoardJson.push(innerBoard);
            }
        });
        $.ajax({
            url: "/erp/addinnerboardbatch",
            type:'POST',
            data: {
                innerBoardJson:JSON.stringify(innerBoardJson)
            },
            success: function (data) {
                if(data == 0) {
                    swal({type:"success",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">恭喜你，录入成功！</span>",html: true},function (){
                        window.parent.document.getElementById("innerBoardA").click();
                    });
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，录入失败！</span>",html: true});
                }
            },
            error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        })
    })
}