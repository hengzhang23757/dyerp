layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.21'
}).extend({                         // 模块别名
    soulTable: 'soulTable',
});
var fabricOutRecordTable;
layui.use(['form', 'soulTable', 'table', 'upload','yutons_sug'], function () {
    var table = layui.table,
        soulTable = layui.soulTable
    var load = layer.load();
    fabricOutRecordTable = table.render({
        elem: '#fabricOutRecordTable'
        ,url:'gettodayfabricoutrecord'
        ,method:'post'
        ,cols: [[]]
        ,loading:true
        ,done: function (res, curr, count) {
            table.init('fabricOutRecordTable',{//转换成静态表格
                cols:[[
                    {type:'numbers', align:'center', title:'序号', width:60}
                    ,{field:'orderName', title:'款号', align:'center', width:130, sort: true, filter: true, totalRowText: '合计'}
                    ,{field:'clothesVersionNumber', title:'版单号', align:'center', width:130, sort: true, filter: true}
                    ,{field:'location', title:'仓位', align:'center', width:120, sort: true, filter: true}
                    ,{field:'fabricName', title:'面料', align:'center', width:220, sort: true, filter: true}
                    ,{field:'colorName', title:'色组', align:'center', width:100, sort: true, filter: true}
                    ,{field:'fabricColor', title:'面料颜色', align:'center',width:100, sort: true, filter: true}
                    ,{field:'jarName', title:'缸号', align:'center', width:150, sort: true, filter: true}
                    ,{field:'weight', title:'数量', align:'center', width:90, sort: true, filter: true, totalRow: true}
                    ,{field:'batchNumber', title:'卷数', align:'center', width:90, sort: true, filter: true, totalRow: true}
                    ,{field:'operateType', title:'出库类型', align:'center', width:100, sort: true, filter: true}
                    ,{field:'returnTime', title:'时间', align:'center', width:120, sort: true, filter: true,templet: function (d) {
                            return layui.util.toDateString(d.returnTime, 'yyyy-MM-dd');
                        }}
                ]]
                ,data:res.data
                ,height: 'full-50'
                ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                ,defaultToolbar: ['filter', 'print']
                ,title: '面料出库表'
                ,totalRow: true
                ,page: true
                ,limits: [50, 100, 200]
                ,limit: 100 //每页默认显示的数量
                ,overflow: 'tips'
                ,done: function () {
                    soulTable.render(this);
                    layer.close(load);
                }
                ,filter: {
                    bottom: true,
                    items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                }
            });
        }
    });


    table.on('toolbar(fabricOutRecordTable)', function(obj) {
        if (obj.event === 'clearFilter') {
            // 清除所有筛选条件并重载表格
            // 参数: tableId
            soulTable.clearFilter('fabricOutRecordTable')
        } else if (obj.event === 'refresh') {
            fabricOutRecordTable.reload();
        } else if (obj.event === 'exportExcel') {
            soulTable.export('fabricOutRecordTable');
        }
    });


    // //监听行工具事件
    // table.on('tool(fabricOutRecordTable)', function(obj){
    //     var data = obj.data;
    //     if(obj.event === 'del'){
    //         layer.confirm('真的删除吗', function(index){
    //             $.ajax({
    //                 url: "/erp/deletefabricstorage",
    //                 type: 'POST',
    //                 data: {
    //                     fabricStorageID:data.fabricStorageID
    //                 },
    //                 success: function (res) {
    //                     if (res.error) {
    //                         layer.msg("删除失败！", {icon: 2});
    //                     } else if (res.info) {
    //                         fabricQueryTable.reload();
    //                         layer.close(index);
    //                         layer.msg(res.info, {icon: 1});
    //                     }
    //                 }, error: function () {
    //                     layer.msg("删除失败！", {icon: 2});
    //                 }
    //             })
    //
    //         });
    //     }
    // })

});