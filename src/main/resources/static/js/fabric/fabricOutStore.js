$(document).ready(function () {

    layui.use(['autocomplete'], function () {
        layui.autocomplete.render({
            elem: $('#clothesVersionNumber')[0],
            url: 'getversionhint',
            template_val: '{{d}}',
            template_txt: '{{d}}',
            onselect: function (resp) {
                autoComplete(resp)
            }
        })
    });

    layui.use(['autocomplete'], function () {
        layui.autocomplete.render({
            elem: $('#toClothesVersionNumber')[0],
            url: 'getversionhint',
            template_val: '{{d}}',
            template_txt: '{{d}}',
            onselect: function (resp) {
                autoCompleteOther(resp)
            }
        })
    });

    $('#clothesVersionNumber').keyup(function() {
        $("#fabricName").empty();
        $("#colorName").empty();
        $("#fabricColor").empty();
        $("#jarName").empty();
        $("#toClothesVersionNumber").empty();
        $("#toColorName").empty();
        $("#toOrderName").empty();
        $("#weight").val("");
        $("#batchNumber").val("");
    });

    $("#orderName").change(function () {
        var orderName = $(this).val();
        $("#fabricName").empty();
        $("#colorName").empty();
        $("#fabricColor").empty();
        $("#jarName").empty();
        $("#toClothesVersionNumber").empty();
        $("#toColorName").empty();
        $("#toOrderName").empty();
        $("#weight").val("");
        $("#batch").val("");
        $.ajax({
            url: "/erp/getloosefabriccolornamehint",
            data: {"orderName": orderName},
            success:function(data){
                $("#colorName").empty();
                if (data.colorNameList) {
                    $("#colorName").append("<option value=''>色组</option>");
                    $.each(data.colorNameList, function(index,element){
                        $("#colorName").append("<option value="+element+">"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });
    });

    $("#colorName").change(function () {
        var orderName = $("#orderName").val();
        var colorName = $(this).val();
        $("#fabricName").empty();
        $("#fabricColor").empty();
        $("#jarName").empty();
        $("#toClothesVersionNumber").empty();
        $("#toColorName").empty();
        $("#toOrderName").empty();
        $("#weight").val("");
        $("#batch").val("");
        $.ajax({
            url: "/erp/getloosefabricfabricnamehint",
            data: {
                "orderName": orderName,
                "colorName": colorName
            },
            success:function(data){
                $("#fabricName").empty();
                if (data.fabricNameList) {
                    $("#fabricName").append("<option value=''>面料</option>");
                    $.each(data.fabricNameList, function(index,element){
                        $("#fabricName").append("<option value="+element+">"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });
    });

    $("#toOrderName").change(function () {
        var toOrderName = $(this).val();
        $.ajax({
            url: "/erp/getordercolorhint",
            data: {"orderName": toOrderName},
            success:function(data){
                $("#toColorName").empty();
                if (data.colorList) {
                    $("#toColorName").append("<option value=''>选择色组</option>");
                    $.each(data.colorList, function(index,element){
                        $("#toColorName").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });
    });

    $("#fabricName").change(function () {
        var fabricName = $(this).val();
        var orderName = $("#orderName").val();
        var colorName = $("#colorName").val();
        $("#fabricColor").empty();
        $("#jarName").empty();
        $("#toClothesVersionNumber").empty();
        $("#toColorName").empty();
        $("#toOrderName").empty();
        $("#weight").val("");
        $("#batch").val("");
        $.ajax({
            url: "/erp/getloosefabriccolorhint",
            data: {
                "orderName": orderName,
                "colorName": colorName,
                "fabricName": fabricName
            },
            success:function(data){
                $("#fabricColor").empty();
                if (data.fabricColorList) {
                    $("#fabricColor").append("<option value=''>颜色</option>");
                    $.each(data.fabricColorList, function(index,element){
                        $("#fabricColor").append("<option value="+element+">"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });
    });

    $("#fabricColor").change(function () {
        var fabricColor = $(this).val();
        var fabricName = $("#fabricName").val();
        var colorName = $("#colorName").val();
        var orderName = $("#orderName").val();
        $("#jarName").empty();
        $("#toClothesVersionNumber").empty();
        $("#toColorName").empty();
        $("#toOrderName").empty();
        $("#weight").val("");
        $("#batch").val("");
        $.ajax({
            url: "/erp/getloosefabricjarnameofunprint",
            data: {
                "orderName": orderName,
                "colorName": colorName,
                "fabricName": fabricName,
                "fabricColor": fabricColor
            },
            success:function(data){
                $("#jarName").empty();
                if (data.jarNumberList) {
                    $("#jarName").append("<option value=''>缸号</option>");
                    $.each(data.jarNumberList, function(index,element){
                        $("#jarName").append("<option value="+element+">"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });
    });

    $("#operateType").change(function () {
        var operateType = $(this).val();
        if (operateType === '换款'){
            $("#transOrder").show();
        }else{
            $("#transOrder").hide();
        }
    });

    $("#jarName").change(function () {
        var jarName = $(this).val();
        var fabricColor = $("#fabricColor").val();
        var fabricName = $("#fabricName").val();
        var orderName = $("#orderName").val();
        var colorName = $("#colorName").val();
        $("#toClothesVersionNumber").empty();
        $("#toColorName").empty();
        $("#toOrderName").empty();
        $("#weight").val("");
        $("#batchNumber").val("");
        $.ajax({
            url: "/erp/getfabriclocationhint",
            data: {
                "orderName": orderName,
                "colorName": colorName,
                "fabricName": fabricName,
                "fabricColor": fabricColor,
                "jarName": jarName
            },
            success:function(data){
                if (data.fabricLocationList) {
                    $("#location").append("<option value=''>选择出库货架</option>");
                    $.each(data.fabricLocationList, function(index,element){
                        $("#location").append("<option value="+element+">"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });
    });

    $("#location").change(function () {
        var jarName = $("#jarName").val();
        var fabricColor = $("#fabricColor").val();
        var fabricName = $("#fabricName").val();
        var orderName = $("#orderName").val();
        var colorName = $("#colorName").val();
        var location = $(this).val();
        $("#toClothesVersionNumber").empty();
        $("#toColorName").empty();
        $("#toOrderName").empty();
        $.ajax({
            url: "/erp/getweightbatchinout",
            data: {
                "orderName": orderName,
                "colorName": colorName,
                "fabricName": fabricName,
                "fabricColor": fabricColor,
                "jarName": jarName,
                "location": location
            },
            success:function(data){
                if (data.weight) {
                    $("#weight").val(data.weight);
                }
                if (data.batchNumber){
                    $("#batchNumber").val(data.batchNumber);
                }
            },
            error:function(){
            }
        });
    });
});

function autoComplete(keywords) {
    $("#orderName").empty();
    $.ajax({
        url: "/erp/getorderbyversion",
        data: {"clothesVersionNumber": keywords},
        success:function(data){
            $("#orderName").empty();
            if (data.orderList) {
                $("#orderName").append("<option value=''>选择订单号</option>");
                $.each(data.orderList, function(index,element){
                    $("#orderName").append("<option value='"+element+"'>"+element+"</option>");
                })
            }
        },
        error:function(){
        }
    });
}

function autoCompleteOther(keywords) {
    $("#toOrderName").empty();
    $.ajax({
        url: "/erp/getorderbyversion",
        data: {"clothesVersionNumber": keywords},
        success:function(data){
            $("#toOrderName").empty();
            if (data.orderList) {
                $("#toOrderName").append("<option value=''>选择订单号</option>");
                $.each(data.orderList, function(index,element){
                    $("#toOrderName").append("<option value='"+element+"'>"+element+"</option>");
                })
            }
        },
        error:function(){
        }
    });
}

var basePath=$("#basePath").val();
var userRole=$("#userRole").val();

function outStore() {
    if(userRole!='root' && userRole!='role11'){
        swal("SORRY!", "对不起，您没有操作权限！", "warning");
        return;
    }
    var flag = false;
    $("#baseInfo :input").each(function (index,item) {
        if($(item).val()==null || $(item).val().trim()=="") {
            flag = true;
            return false;
        }
    });
    if ($("#operateType").val() === "换款"){
        $(".panel-body :input").each(function (index,item) {
            if($(item).val()==null || $(item).val().trim()=="") {
                flag = true;
                return false;
            }
        });
    }
    if(flag) {
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请填写完所有字段信息！</span>",html: true});
        return false;
    }
    var fabricOutRecord = {};
    fabricOutRecord.orderName = $("#orderName").val();
    fabricOutRecord.colorName = $("#colorName").val();
    fabricOutRecord.clothesVersionNumber = $("#clothesVersionNumber").val();
    fabricOutRecord.fabricName = $("#fabricName").val();
    fabricOutRecord.fabricColor = $("#fabricColor").val();
    fabricOutRecord.jarName = $("#jarName").val();
    fabricOutRecord.weight = $("#weight").val();
    fabricOutRecord.batchNumber = $("#batchNumber").val();
    fabricOutRecord.operateType = $("#operateType").val();
    fabricOutRecord.returnTime = new Date();
    fabricOutRecord.location = $("#location").val();
    swal({
        title: "",
        text: "<span style=\"font-weight:bolder;font-size: 20px\">您确定要将该面料出库吗？</span>",
        type: "warning",
        html:true,
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: "确定",
        cancelButtonText:"我再想想",
        confirmButtonColor: "#ec6c62",
        showLoaderOnConfirm: true
    }, function() {
        $.ajax({
            url: "/erp/addfabricoutrecord",
            data: {
                fabricOutRecordJson:JSON.stringify(fabricOutRecord),
                toClothesVersionNumber:$("#toClothesVersionNumber").val(),
                toColorName:$("#toColorName").val(),
                toOrderName:$("#toOrderName").val()
            },
            type:"POST",
            success: function (data) {
                if (data.error) {
                    swal({
                        type: "error",
                        title: "",
                        text: "<span style=\"font-weight:bolder;font-size: 20px\">"+data.error+"</span>",
                        html: true
                    },
                    function(){
                        location.href=basePath+"erp/fabricOutStoreStart";
                    });
                } else if(data.trans){
                    swal({
                        type: "success",
                        title: "",
                        text: "<span style=\"font-weight:bolder;font-size: 20px\">"+data.trans+"</span>",
                        html: true
                    }, function(){
                            location.href=basePath+"erp/fabricOutStoreStart";
                        });
                }else if(data.out){
                    swal({
                        type: "success",
                        title: "",
                        text: "<span style=\"font-weight:bolder;font-size: 20px\">"+data.out+"</span>",
                        html: true
                    }, function(){
                            location.href=basePath+"erp/fabricOutStoreStart";
                        });
                }else {
                    swal({
                        type: "error",
                        title: "",
                        text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，出库失败！</span>",
                        html: true
                    });
                }
            }, error: function () {
                swal({
                    type: "error",
                    title: "",
                    text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",
                    html: true
                });
            }
        });
    });

}
