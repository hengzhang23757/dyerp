var userName=$("#userName").val();
var userRole=$("#userRole").val();
$(document).ready(function () {
    $.ajax({
        url: basePath + "erp/gettodayfabricorder",
        type:'GET',
        data: {},
        success: function (data) {
            if(data) {
                createFabricOrderTable(data.fabricOrderList);
            }else {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，没有查到相关信息！</span>",html: true});
            }
        },
        error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }
    });
    $('#mainFrameTabs').bTabs();

    $('#orderListTab').click(function(){
        $.ajax({
            url: basePath + "erp/gettodayfabricorder",
            type:'GET',
            data: {},
            success: function (data) {
                if(data) {
                    createFabricOrderTable(data.fabricOrderList);
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，没有查到相关信息！</span>",html: true});
                }
            },
            error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        });
    });

});


var basePath=$("#basePath").val();
var myDate = new Date();
var fabricOrderTable;
function createFabricOrderTable(data) {
    if (fabricOrderTable != undefined) {
        fabricOrderTable.clear(); //清空一下table
        fabricOrderTable.destroy(); //还原初始化了的datatable
    }

    fabricOrderTable = $('#fabricOrderTable').DataTable({
        "data":data,
        //"retrieve": true,
        language : {
            processing : "载入中",//处理页面数据的时候的显示
            paginate : {//分页的样式文本内容。
                previous : "上一页",
                next : "下一页",
                first : "第一页",
                last : "最后一页"
            },
            search:"搜索：",
            zeroRecords : "没有内容",//table tbody内容为空时，tbody的内容。
            //下面三者构成了总体的左下角的内容。
            info : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//左下角的信息显示，大写的词为关键字。
            infoEmpty : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//筛选为空时左下角的显示。
            infoFiltered : ""//筛选之后的左下角筛选提示(另一个是分页信息显示，在上面的info中已经设置，所以可以不显示)，
        },
        dom: 'Bfrtip',
        "buttons": [{
            'extend': 'excel',
            'text': '导出',//定义导出excel按钮的文字
            'className': 'btn btn-success', //按钮的class样式
            'title': '面料订单'+myDate.toLocaleString( ),
            'exportOptions': {
                'modifier': {
                    'page': 'all'
                }
            }
        }],
        pageLength : 50,// 每页显示10条数据
        pagingType : "full_numbers", // 分页样式：simple,simple_numbers,full,full_numbers，
        "paging": true,
        // "ordering" : false,
        "info": true,
        searching:true,
        lengthChange:false,
        scrollX: 2000,
        scrollCollapse: false,
        // scroller:       true,
        "columns": [
            {
                "data": "fabricOrderID",
                "title":"选择",
                "width":"45px",
                "defaultContent": "",
                "sClass": "text-center"
            },
            {
                "data": "stateType",
                "title":"状态",
                "width":"30px",
                "defaultContent": "",
                "sClass": "text-center",
                "searchable":false,
                render: function (data, type, row, meta) {
                    if (data == 0){
                        return "未提交";
                    }else{
                        return "已提交";
                    }
                }
            },{
                "data": "fabricOrderName",
                "title":"订单编号",
                "width":"90px",
                "defaultContent": "",
                "sClass": "text-center"
            },{
                "data": "clothesVersionNumber",
                "title":"版单号",
                "width":"90px",
                "defaultContent": "",
                "sClass": "text-center"
            },{
                "data": "orderName",
                "title":"款号",
                "width":"60px",
                "defaultContent": "",
                "sClass": "text-center"
            },{
                "data": "fabricName",
                "title":"面料名称",
                "width":"90px",
                "defaultContent": "",
                "sClass": "text-center"
            }, {
                "data": "fabricTypeNumber",
                "title":"布种",
                "width":"45px",
                "defaultContent": "",
                "sClass": "text-center"
            }, {
                "data": "fabricNumber",
                "title":"面料号",
                "width":"90px",
                "defaultContent": "",
                "sClass": "text-center"
            }, {
                "data": "fabricColor",
                "title":"面料颜色",
                "width":"90px",
                "defaultContent": "",
                "sClass": "text-center"
            }, {
                "data": "fabricColorNumber",
                "title":"面料色号",
                "width":"90px",
                "defaultContent": "",
                "sClass": "text-center"
            }, {
                "data": "skcColor",
                "title":"skc颜色",
                "width":"60px",
                "defaultContent": "",
                "sClass": "text-center"
            }, {
                "data": "fabricUse",
                "title":"面料用途",
                "width":"60px",
                "defaultContent": "",
                "sClass": "text-center"
            }, {
                "data": "singleCount",
                "title":"单件用量",
                "width":"60px",
                "defaultContent": "",
                "sClass": "text-center"
            }, {
                "data": "fabricSupplier",
                "title": "供应商",
                "width":"60px",
                "defaultContent": "",
                "sClass": "text-center"
            }, {
                "data": "orderCount",
                "title": "订单量",
                "width":"90px",
                "defaultContent": "",
                "sClass": "text-center"
            }, {
                "data": "fabricSumCount",
                "title": "面料总量",
                "width":"90px",
                "defaultContent": "",
                "sClass": "text-center"
            }, {
                "data": "price",
                "title": "单价",
                "width":"90px",
                "defaultContent": "",
                "sClass": "text-center"
            }, {
                "data": "total",
                "title": "总价",
                "width":"90px",
                "defaultContent": "",
                "sClass": "text-center"
            }, {
                "data": "deliveryDate",
                "title": "交期",
                "width":"90px",
                "defaultContent": "",
                "sClass": "text-center"
            }, {
                "data": "width",
                "title": "门幅",
                "width":"90px",
                "defaultContent": "",
                "sClass": "text-center"
            }, {
                "data": "weight",
                "title": "克重",
                "width":"90px",
                "defaultContent": "",
                "sClass": "text-center"
            }, {
                "data": "filling",
                "title": "纬斜",
                "width":"90px",
                "defaultContent": "",
                "sClass": "text-center"
            }, {
                "data": "twist",
                "title": "扭度",
                "width":"90px",
                "defaultContent": "",
                "sClass": "text-center"
            }, {
                "data": "standard",
                "title": "执行标准",
                "width":"90px",
                "defaultContent": "",
                "sClass": "text-center"
            }, {
                "data": "remark",
                "title": "备注",
                "width":"200px",
                "defaultContent": "",
                "sClass": "text-center"
            }
        ],
        "columnDefs" :
            [{
                "orderable" : false, // 禁用排序
                "targets" : [0], // 指定的列
                "render" : function(data, type, full, meta) {
                    return "<input name='checkBtn' type='checkbox' value='"+data+"'>";
                }
            },{
                "orderable" : false, // 禁用排序
                "targets" : [25], // 指定的列
                "data" : "fabricOrderID",
                "width":"70px",
                "render" : function(data, type, full, meta) {
                    return "<a href='#' style='color:#ff0000' onclick='deleteFabricOrder("+data+")'>删除</a>&nbsp;&nbsp;&nbsp;<a href='#' style='color:#3e8eea' onclick='updateFabricOrder(this,"+data+")'>修改</a>";
                }
            },{
                "targets": [1],
                "createdCell": function (td, data, rowData, row, col) {
                    if (data == 0) {
                        $(td).css('color', 'red')
                    }else{
                        $(td).css('color', 'green')
                    }
                }
            }]

    });
    $('#fabricOrderTable_wrapper .dt-buttons').append("<button class=\"btn btn-success\" style=\"outline:none;margin-left: 10px\" onclick=\"addFabricOrder()\">添加面料订单</button>");

}


function deleteFabricOrder(fabricOrderID) {
    if(userRole!='root' && userRole!='role3'){
        swal("SORRY!", "对不起，您没有操作权限！", "warning");
        return;
    }
    swal({
        title: "",
        text: "<span style=\"font-weight:bolder;font-size: 20px\">您确定要删除该面料订单信息吗？</span>",
        type: "warning",
        html:true,
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: "确定",
        cancelButtonText:"我再想想",
        confirmButtonColor: "#ec6c62",
        showLoaderOnConfirm: true
    }, function() {
        $.ajax({
            url: basePath + "erp/deletefabricorder",
            type:'POST',
            data: {
                fabricOrderID:fabricOrderID,
                userName:userName
            },
            success: function (data) {
                if(data == 0) {
                    swal({type:"success",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">恭喜你，删除成功！</span>",html: true});
                    createFabricOrderTable();
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，删除失败！</span>",html: true});
                }
            },
            error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        })
    });
}

var tabId = 1;
function addFabricOrder() {
    if(userRole!='root' && userRole!='role3'){
        swal("SORRY!", "对不起，您没有操作权限！", "warning");
        return;
    }
    $('#mainFrameTabs').bTabsAdd("tabId" + tabId, "面料订单录入", "/erp/addFabricOrderStart");
    tabId++;
}

function updateFabricOrder(obj,fabricOrderID){
    if(userRole!='root' && userRole!='role3'){
        swal("SORRY!", "对不起，您没有操作权限！", "warning");
        return;
    }
    $("#fabricOrderName").val($(obj).parent().parent().find("td").eq(1).text());
    $("#clothesVersionNumber").val($(obj).parent().parent().find("td").eq(2).text());
    $("#orderName").val($(obj).parent().parent().find("td").eq(3).text());
    $("#fabricName").val($(obj).parent().parent().find("td").eq(4).text());
    $("#fabricNumber").val($(obj).parent().parent().find("td").eq(5).text());
    $("#fabricColor").val($(obj).parent().parent().find("td").eq(6).text());
    $("#fabricColorNumber").val($(obj).parent().parent().find("td").eq(7).text());
    $("#skcColor").val($(obj).parent().parent().find("td").eq(8).text());
    $("#fabricUse").val($(obj).parent().parent().find("td").eq(9).text());
    $("#singleCount").val($(obj).parent().parent().find("td").eq(10).text());
    $("#orderCount").val($(obj).parent().parent().find("td").eq(11).text());
    $("#fabricSumCount").val($(obj).parent().parent().find("td").eq(12).text());
    $("#fabricSupplier").val($(obj).parent().parent().find("td").eq(13).text());
    $("#price").val($(obj).parent().parent().find("td").eq(14).text());
    $("#total").val($(obj).parent().parent().find("td").eq(15).text());
    $("#deliveryDate").val($(obj).parent().parent().find("td").eq(16).text());
    $("#width").val($(obj).parent().parent().find("td").eq(17).text());
    $("#weight").val($(obj).parent().parent().find("td").eq(18).text());
    $("#filling").val($(obj).parent().parent().find("td").eq(19).text());
    $("#twist").val($(obj).parent().parent().find("td").eq(20).text());
    $("#standard").val($(obj).parent().parent().find("td").eq(21).text());
    $("#remark").val($(obj).parent().parent().find("td").eq(22).text());
    $.blockUI({
        css: {
            width: '90%',
            top: '5%',
            left:'5%',
            border: 'none',
            padding: '15px',
            backgroundColor: '#fff',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: 1,
            color: '#000'
        },
        message: $('#editFabricOrder')
    });

    $("#editFabricOrderYes").unbind("click").bind("click", function () {
        var flag = false;
        $("#editFabricOrder input").each(function (index, item) {
            if($(this).val() == "") {
                flag = true;
                return false;
            }
        });
        if(flag) {
            swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入完全部信息！</span>",html: true});
            return false;
        }
        $.ajax({
            url: basePath + "erp/updatefabricorder",
            type:'POST',
            data: {
                fabricOrderID:fabricOrderID,
                fabricOrderName:$("#fabricOrderName").val(),
                clothesVersionNumber:$("#clothesVersionNumber").val(),
                orderName:$("#orderName").val(),
                fabricName:$("#fabricName").val(),
                fabricNumber:$("#fabricNumber").val(),
                fabricColor:$("#fabricColor").val(),
                fabricColorNumber:$("#fabricColorNumber").val(),
                skcColor:$("#skcColor").val(),
                fabricUse:$("#fabricUse").val(),
                singleCount:$("#singleCount").val(),
                orderCount:$("#orderCount").val(),
                fabricSumCount:$("#fabricSumCount").val(),
                fabricSupplier:$("#fabricSupplier").val(),
                price:$("#price").val(),
                total:$("#total").val(),
                deliveryDate:$("#deliveryDate").val(),
                width:$("#width").val(),
                weight:$("#weight").val(),
                filling:$("#filling").val,
                twist:$("#twist").val(),
                standard:$("#standard").val(),
                remark:$("#remark").val()
            },
            success: function (data) {
                if(data == 0) {
                    $.unblockUI();
                    $("#editFabricOrder input").val("");
                    swal({type:"success",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">恭喜你，保存成功！</span>",html: true});
                    createFabricOrderTable();
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，保存失败！</span>",html: true});
                }
            },
            error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        })
    });
    $("#editFabricOrderNo").unbind("click").bind("click", function () {
        $.unblockUI();
        $("#editFabricOrder input").val("");
    });
}
var xUserID="Z146I01";
var xPwd="Z146I01";
function commitFabricOrder() {
    if(userRole!='root' && userRole!='role3'){
        swal("SORRY!", "对不起，您没有操作权限！", "warning");
        return;
    }
    var orderNameList = [];
    $("#fabricOrderTable tbody input[name='checkBtn']").each(function(){
        if($(this).is(':checked')) {
            orderNameList.push($(this).parent().parent().find("td").eq(4).text());
        }
    });
    orderNameList = uniqArr(orderNameList);
    if(orderNameList.length == 0) {
        swal("SORRY!", "请先选择要提交的数据！", "error");
        return;
    }
    if(orderNameList.length > 1) {
        swal("SORRY!", "一次只能提交一个款！", "error");
        return;
    }

    swal({
        title: "",
        text: "<span style=\"font-weight:bolder;font-size: 20px\">您确定要提交到供应商系统吗？</span>",
        type: "warning",
        html:true,
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: "确定",
        cancelButtonText:"我再想想",
        confirmButtonColor: "#ec6c62",
        showLoaderOnConfirm: true
    }, function() {
        var SKey = "" ;
        var argParam = {};
        var commitResult = false;
        argParam["UserID"] =xUserID ;                              	//模擬傳入參數
        argParam["Pwd"] = xPwd;				//模擬傳入參數
        $.ajax({
            type: "POST",
            url : "https://i-tex.texwinca.com:6633/wCRM/ctlESGTKN",   //服務器主機
            dataType: "json",
            data : argParam,
            async: false,
            beforeSend: function(){},
            complete: function(){},
            success : function(saveRTN) {
                console.log(saveRTN);
                if (saveRTN.Status) {
                    SKey=saveRTN.SKey ;			// 成功取SKey
                } else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">"+saveRTN.Err+"</span>",html: true});
                }
            },
            error: function() {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">取getwebkey傳送失敗!</span>",html: true});
            }
        });

        if (SKey!="") {
            var argPara = {};
            var dataJSON  = [];
            var dataJSON2 = [];
            var dataJSON3 = [];
            var checkIndex = 0;
            $("#fabricOrderTable tbody input[name='checkBtn']").each(function(){
                if($(this).is(':checked')) {
                    if (checkIndex == 0){
                        var dataJsonTmp = {};
                        dataJsonTmp.CUST_REF = $(this).parent().parent().find("td").eq(2).text();
                        dataJsonTmp.SO_NO = $(this).parent().parent().find("td").eq(2).text();
                        dataJsonTmp.RV_NO = "";
                        dataJsonTmp.ODATE = $(this).parent().parent().find("td").eq(18).text();
                        dataJsonTmp.TYPE = "N";
                        dataJsonTmp.IS_SAMP = "Y";
                        dataJsonTmp.SMAN_CODE = "";
                        dataJsonTmp.CUST_NO = "";
                        dataJsonTmp.CUST_NAME1 = "";
                        dataJsonTmp.CUST_NAME2 = "";
                        dataJsonTmp.STYLE_NO = $(this).parent().parent().find("td").eq(4).text();
                        dataJsonTmp.LABEL = "";
                        dataJsonTmp.LABEL_S = "";
                        dataJsonTmp.LABEL_KIND = "";
                        dataJsonTmp.AGENT = "";
                        dataJsonTmp.AGENT_S = "";
                        dataJsonTmp.AGENT_USER = "";
                        dataJsonTmp.AGENT_CTRY = "";
                        dataJsonTmp.COUNTRY = "";
                        dataJsonTmp.COUNTRY_C = "";
                        dataJsonTmp.SEASON = "Q4";
                        dataJsonTmp.SEASON_YEAR = "2019";
                        dataJsonTmp.GRTNAME = "";
                        dataJsonTmp.GRTNAME2 = "";   //暂时置空
                        dataJsonTmp.AGE = ""; //暂时置空
                        dataJsonTmp.ETD = ""; //暂时置空
                        dataJsonTmp.SHIP_ADDR = "";
                        dataJsonTmp.PAYMENT = "";
                        dataJsonTmp.DLYMENT = "";
                        dataJsonTmp.PORT = "";
                        dataJsonTmp.TERMS = "";
                        dataJsonTmp.VENDOR = "";
                        dataJsonTmp.ATTN = "";
                        dataJsonTmp.SO_RECNO = "000025";
                        dataJSON.push(dataJsonTmp);
                    }
                    var dataJsonTmp2= {};
                    dataJsonTmp2.CUST_REF = $(this).parent().parent().find("td").eq(2).text();
                    dataJsonTmp2.SO_NO = $(this).parent().parent().find("td").eq(2).text();
                    dataJsonTmp2.SO_SEQ = $(this).parent().parent().find("td").eq(6).text();
                    dataJsonTmp2.STYLE_NO = $(this).parent().parent().find("td").eq(4).text();
                    dataJsonTmp2.STYLE_NO2 = $(this).parent().parent().find("td").eq(4).text();
                    dataJsonTmp2.BD_NO = $(this).parent().parent().find("td").eq(7).text();
                    dataJsonTmp2.PROD_NO_C = $(this).parent().parent().find("td").eq(7).text();
                    dataJsonTmp2.PROD_NO = "";
                    dataJsonTmp2.PROD_NAME1 = "";
                    dataJsonTmp2.PROD_NAME2 = "";
                    dataJsonTmp2.PROD_NAME_C = $(this).parent().parent().find("td").eq(5).text();
                    dataJsonTmp2.QO_NO = "";
                    dataJsonTmp2.QO_RV_NO = "";
                    dataJsonTmp2.QO_ITEM_C = "";
                    dataJsonTmp2.QO_COL_SEQ = "";
                    dataJsonTmp2.QO_COL_DESC = "";
                    dataJsonTmp2.QO_COL_DESC_E = "";
                    dataJsonTmp2.CQTY = $(this).parent().parent().find("td").eq(15).text();
                    dataJsonTmp2.UNIT = "LBS";
                    dataJsonTmp2.CUR = "RMB";
                    dataJsonTmp2.OUP = $(this).parent().parent().find("td").eq(16).text();
                    dataJsonTmp2.OAMT = $(this).parent().parent().find("td").eq(17).text();
                    dataJsonTmp2.PROD_ZG = "";
                    dataJsonTmp2.KNIT_INCH = "";
                    dataJsonTmp2.KNIT_PRESS = "";
                    dataJsonTmp2.KNIT_PART = "";
                    dataJsonTmp2.PRESS = "";
                    dataJsonTmp2.COMP = "";
                    dataJsonTmp2.COMP_OQTY = "";
                    dataJsonTmp2.PRD_WDQTY = $(this).parent().parent().find("td").eq(19).text();
                    dataJsonTmp2.PRD_WDTYPE = "实用";
                    dataJsonTmp2.PRD_WDUNIT = "\"";
                    dataJsonTmp2.PRD_WDTOL1 = "1";
                    dataJsonTmp2.PRD_WD_TOL2 = "1";
                    dataJsonTmp2.PRD_WDTOLUNIT = "\%";
                    dataJsonTmp2.PRD_WTQTY = $(this).parent().parent().find("td").eq(20).text();
                    dataJsonTmp2.PRD_WTUNIT = "G/M2";
                    dataJsonTmp2.PRD_WTTOL1 = "5";
                    dataJsonTmp2.PRD_WTTOL2 = "5";
                    dataJsonTmp2.PRD_WTTOLUNIT = "\%";
                    dataJsonTmp2.SHK_W = "";
                    dataJsonTmp2.PRD_SHK_W = "";
                    dataJsonTmp2.SHK_L = "";
                    dataJsonTmp2.PRD_SHK_L = "";
                    dataJsonTmp2.PRD_WASH = "";
                    dataJsonTmp2.SO_RECNO = "000025";
                    dataJSON2.push(dataJsonTmp2);
                    var dataJsonTmp3 = {};
                    dataJsonTmp3.CUST_REF = $(this).parent().parent().find("td").eq(2).text();
                    dataJsonTmp3.SO_NO = $(this).parent().parent().find("td").eq(2).text();
                    dataJsonTmp3.SO_SEQ = $(this).parent().parent().find("td").eq(6).text();
                    dataJsonTmp3.STYLE_NO = $(this).parent().parent().find("td").eq(4).text();
                    dataJsonTmp3.COMP = "";
                    dataJsonTmp3.COL_NO_C = $(this).parent().parent().find("td").eq(9).text();
                    dataJsonTmp3.COL_NAME_C = $(this).parent().parent().find("td").eq(8).text();
                    dataJsonTmp3.COL_NO = "";
                    dataJsonTmp3.COL_NAME = "";
                    dataJsonTmp3.CHK_COLOR = "";
                    dataJsonTmp3.PRD_DYE = "";
                    dataJsonTmp3.COL_OQTY = $(this).parent().parent().find("td").eq(15).text();
                    dataJsonTmp3.COL_UNIT = "LBS";
                    dataJsonTmp3.PER_TOL1 = "2";
                    dataJsonTmp3.PER_TOL2 = "2";
                    dataJsonTmp3.TOL_UNIT = "\%";
                    dataJsonTmp3.COL_NO_OK = "";
                    dataJsonTmp3.CHK_COLOR = "";
                    dataJsonTmp3.PRD_PZ = "";
                    dataJsonTmp3.PRD_ZF = "";
                    dataJsonTmp3.PRD_DTLS = "";
                    dataJsonTmp3.PRD_DTL_REM = "";
                    dataJsonTmp3.SIZE = "";
                    dataJsonTmp3.SIZE_DESCR = "";
                    dataJsonTmp3.SIZE_OQTY = "";
                    dataJsonTmp3.SIZE_UNIT = "";
                    dataJsonTmp3.RECNO_COL = "";
                    dataJsonTmp3.SO_RECNO = "000025";
                    dataJsonTmp3.OUP = $(this).parent().parent().find("td").eq(16).text();
                    dataJsonTmp3.CUR = "RMB";
                    dataJsonTmp3.OAMT = $(this).parent().parent().find("td").eq(17).text();
                    dataJSON3.push(dataJsonTmp3);
                    checkIndex ++;
                }
            });
            console.log(dataJSON);
            console.log(dataJSON2);
            console.log(dataJSON3);
            argPara["action"] ="AddSO" ;                                  	//模擬傳入參數
            argPara["pmUserID"] =xUserID ;                                //模擬傳入參數
            argPara["pmSKey"] =SKey ;                                     	//模擬傳入參數
            argPara["dataJSON"] =encodeURI(JSON.stringify(dataJSON));       //模擬傳入參數, http encode
            argPara["dataJSON2"] = encodeURI(JSON.stringify(dataJSON2));  //模擬傳入參數, http encode
            argPara["dataJSON3"] = encodeURI(JSON.stringify(dataJSON3));  //模擬傳入參數,http encode
            $.ajax( {
                url :" https://i-tex.texwinca.com:6633/wCRM/ctlESGAPI",   //服務器主機(內部測試用)
                type : "POST",
                dataType: "json",
                async : false,
                data : argPara,                // json data arrary
                beforeSend: function(){},
                complete: function(){},
                success : function(saveRTN) {
                    commitResult = true;
                    console.log(saveRTN);
                    var saveMsg = saveRTN.Err;
                    console.log(saveMsg);
                    if(saveRTN.SaveOK) {
                        swal({type:"success",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">存檔生成訂單號:" + saveRTN.ReturnNO + "</span>",timer:2000,html: true});
                    } else {
                        commitResult = false;
                        swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">"+saveMsg+"</span>",html: true,closeOnConfirm: false,});
                    }
                },
                error: function() {
                    commitResult = false;
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">傳送json數據失敗!</span>",html: true});
                }
            });
        }

        if (commitResult){
            var fabricOrderIDList = [];
            $("#fabricOrderTable tbody input[name='checkBtn']").each(function(){
                if($(this).is(':checked')) {
                    fabricOrderIDList.push($(this).val());
                }
            });
            console.log(fabricOrderIDList);
            $.ajax({
                url: basePath + "erp/commitfabricorder",
                type:'POST',
                data: {
                    fabricOrderIDJson:JSON.stringify(fabricOrderIDList)
                },
                success: function (data) {
                    if(data == 0) {
                        swal({type:"success",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">恭喜你，提交成功！</span>",html: true});
                        $.ajax({
                            url: basePath + "erp/gettodayfabricorder",
                            type:'GET',
                            data: {},
                            success: function (data) {
                                if(data) {
                                    createFabricOrderTable(data.fabricOrderList);
                                }else {
                                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，没有查到相关信息！</span>",html: true});
                                }
                            },
                            error: function () {
                                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
                            }
                        });
                    }else {
                        swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，删除失败！</span>",html: true});
                    }
                },
                error: function () {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
                }
            })
        }
        $("input[name='checkBtn']").prop("checked",false);


    });
}


function changeTable(obj){
    var opt = obj.options[obj.selectedIndex];
    if (opt.value == "today"){
        $.blockUI({ css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            },
            message: "<h3>稍等一下... 奋力搜索中</h3>"
        });
        $.ajax({
            url: basePath + "erp/gettodayfabricorder",
            type:'GET',
            data: {},
            success: function (data) {
                if(data) {
                    createFabricOrderTable(data.fabricOrderList);
                    $.unblockUI();
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，没有查到相关信息！</span>",html: true});
                }
            },
            error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        });
    }
    if (opt.value == "oneMonth"){
        $.blockUI({ css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            },
            message: "<h3>稍等一下... 奋力搜索中</h3>"
        });
        $.ajax({
            url: basePath + "erp/getonemonthfabricorder",
            type:'GET',
            data: {},
            success: function (data) {
                if(data) {
                    createFabricOrderTable(data.fabricOrderList);
                    $.unblockUI();
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，没有查到相关信息！</span>",html: true});
                }
            },
            error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        });
    }
    if (opt.value == "threeMonths"){
        $.blockUI({ css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            },
            message: "<h3>稍等一下... 奋力搜索中</h3>"
        });
        $.ajax({
            url: basePath + "erp/getthreemonthsfabricorder",
            type:'GET',
            data: {},
            success: function (data) {
                if(data) {
                    createFabricOrderTable(data.fabricOrderList);
                    $.unblockUI();
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，没有查到相关信息！</span>",html: true});
                }
            },
            error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        });
    }
    if (opt.value == "all"){
        $.blockUI({ css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            },
            message: "<h3>稍等一下... 奋力搜索中</h3>"
        });
        $.ajax({
            url: basePath + "erp/getallfabricorder",
            type:'GET',
            data: {},
            success: function (data) {
                if(data) {
                    createFabricOrderTable(data.fabricOrderList);
                    $.unblockUI();
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，没有查到相关信息！</span>",html: true});
                }
            },
            error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        });
    }
}



function uniqArr(array){
    var temp = []; //一个新的临时数组
    for(var i = 0; i < array.length; i++){
        if(temp.indexOf(array[i]) == -1){
            temp.push(array[i]);
        }
    }
    return temp;
}
