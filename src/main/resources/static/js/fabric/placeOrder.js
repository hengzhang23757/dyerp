var form;
layui.use('form', function(){
    form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
    form.render();
});

var userRole = $("#userRole").val();
layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.21'
}).extend({                         // 模块别名
    soulTable: 'soulTable'
});
$(document).ready(function () {

    form.render('select');


    lay('#version').html('-v' + layui.laydate.v);
    $Date = layui.laydate;
    renderSeasonDate(document.getElementById('season'), 1);
    initDateForm();



    $.ajax({
        url: "/erp/getdistinctfabricsupplier",
        type: 'GET',
        data: {
        },
        success:function(data){
            if (data.supplierList) {
                $("#supplier").append("<option value=''>选择供应商</option>");
                $.each(data.supplierList, function(index,element){
                    $("#supplier").append("<option value='"+element+"'>"+element+"</option>");
                });
                form.render('select');
            }
        }, error: function () {
            layer.msg("获取供应商信息失败", { icon: 2 });
        }
    });

    $.ajax({
        url: "/erp/getallcustomername",
        type: 'GET',
        data: {
        },
        success:function(data){
            if (data.customerNameList) {
                $("#customerName").append("<option value=''>选择品牌</option>");
                $.each(data.customerNameList, function(index,element){
                    $("#customerName").append("<option value='"+element+"'>"+element+"</option>");
                });
                form.render('select');
            }
        }, error: function () {
            layer.msg("获取供应商信息失败", { icon: 2 });
        }
    });

    layui.use(['autocomplete'], function () {
        layui.autocomplete.render({
            elem: $('#materielName')[0],
            url: 'getfabricnamehint',
            template_val: '{{d}}',
            template_txt: '{{d}}',
            onselect: function (resp) {
                // autoComplete(resp)
            }
        })
    });

    form.on('select(test)', function(data){
        if (data.value === '面料'){
            $("#supplier").empty();
            $.ajax({
                url: "/erp/getdistinctfabricsupplier",
                type: 'GET',
                data: {
                },
                success:function(data){
                    $("#supplier").empty();
                    if (data.supplierList) {
                        $("#supplier").append("<option value=''>选择供应商</option>");
                        $.each(data.supplierList, function(index,element){
                            $("#supplier").append("<option value='"+element+"'>"+element+"</option>");
                        });
                        form.render('select');
                    }
                }, error: function () {
                    layer.msg("获取供应商信息失败", { icon: 2 });
                }
            });


            layui.use(['autocomplete'], function () {
                layui.autocomplete.render({
                    elem: $('#materielName')[0],
                    url: 'getfabricnamehint',
                    template_val: '{{d}}',
                    template_txt: '{{d}}',
                    onselect: function (resp) {
                        // autoComplete(resp)
                    }
                })
            });


        } else if (data.value === '辅料'){
            $("#supplier").empty();
            $.ajax({
                url: "/erp/getdistinctaccessorysupplier",
                type: 'GET',
                data: {
                },
                success:function(data){
                    $("#supplier").empty();
                    if (data.supplierList) {
                        $("#supplier").append("<option value=''>选择供应商</option>");
                        $.each(data.supplierList, function(index,element){
                            $("#supplier").append("<option value='"+element+"'>"+element+"</option>");
                        });
                        form.render('select');
                    }
                }, error: function () {
                    layer.msg("获取供应商信息失败", { icon: 2 });
                }
            });

            layui.use(['autocomplete'], function () {
                layui.autocomplete.render({
                    elem: $('#materielName')[0],
                    url: 'getaccessorynamehint',
                    template_val: '{{d}}',
                    template_txt: '{{d}}',
                    onselect: function (resp) {
                        // autoComplete(resp)
                    }
                })
            });
        }
    });

    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumber", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });

});

var placeOrderTable;

layui.use(['form', 'table', 'soulTable'], function () {
    var table = layui.table,
        soulTable = layui.soulTable,
        $ = layui.$;
    var load = layer.load();

    // $(".layui-form-select,.layui-input").css("padding-right",0);

    placeOrderTable = table.render({
        elem: '#placeOrderTable'
        ,cols:[[]]
        ,loading:true
        ,data:[]
        ,height: 'full-130'
        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
        ,defaultToolbar: ['filter', 'print']
        ,title: '面辅料下单表'
        ,totalRow: true
        ,limit: Number.MAX_VALUE //每页默认显示的数量
        ,done: function () {
            soulTable.render(this);
            layer.close(load);
        }
        ,filter: {
            bottom: true,
            items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
        }
    });

    //监听提交
    form.on('submit(searchPlaceOrder)', function(data){
        var data = {};
        var orderName = $("#orderName").val();
        var supplier = $("#supplier").val();
        if ((orderName == null || orderName === "") && (supplier == null || supplier === "")){
            layer.msg("款号-供应商必须填一个", { icon: 2 });
            return false;
        }
        data.searchType = $("#searchType").val();
        if (supplier != null && supplier != ""){
            data.supplier = supplier;
        }
        if (orderName != null && orderName != ""){
            data.orderName = orderName;
        }
        data.checkState = $("#checkState").val();
        var load = layer.load(2);
        $.ajax({
            url: "/erp/getplaceorderbyinfo",
            type: 'GET',
            data: data,
            success: function (res) {
                if (res.data) {
                    if (data.searchType === '面料'){
                        table.render({
                            elem: '#placeOrderTable'
                            ,cols:[[
                                {type:'checkbox'},
                                {field: 'fabricID', title: 'fabricID', hide:true},
                                {field: 'orderName', title: '款号', width: 130, sort: true, filter: true},
                                {field: 'supplier', title: '供应商', width: 123, filter: true},
                                {field: 'colorName', title: '订单颜色', width: 123, filter: true},
                                {field: 'partName', title: '部位', width: 123, filter: true},
                                {field: 'isHit', title: '是否撞色', width: 123, filter: true},
                                {field: 'fabricColor', title: '面料颜色', width: 123, filter: true},
                                {field: 'fabricColorNumber', title: '面料色号', width: 123, filter: true},
                                {field: 'fabricName', title: '面料名称', width: 130, sort: true, filter: true, totalRowText: '合计'},
                                {field: 'fabricNumber', title: '面料号', width: 130, sort: true, filter: true},
                                {field: 'unit', title: '单位', width: 100, sort: true, filter: true},
                                {field: 'fabricWidth', title: '布封', width: 165 , filter: true},
                                {field: 'fabricWeight', title: '克重', width: 123, filter: true},
                                {field: 'orderPieceUsage', title: '接单用量', width: 123, filter: true},
                                {field: 'pieceUsage', title: '单件用量', width: 123, filter: true},
                                {field: 'orderCount', title: '件数', width: 123, filter: true},
                                {field: 'fabricAdd', title: '上浮(%)', width: 123, filter: true},
                                {field: 'fabricCount', title: '订布量', width: 123, filter: true, totalRow: true},
                                {field: 'fabricActCount', title: '实际订布', width: 123, filter: true, totalRow: true},
                                {field: 'price', title: '单价', width: 123, filter: true},
                                {field: 'sumMoney', title: '总价', width: 123, filter: true},
                                {field: 'checkState', title: '审核状态', width: 123, filter: true}
                            ]]
                            ,excel:{ // 导出excel配置, （以下值均为默认值）
                                on: true, //是否启用, 默认开启
                                filename: '面料数据表.xlsx', // 文件名
                                checked: true
                            }
                            ,height: 'full-130'
                            ,limit: Number.MAX_VALUE //每页默认显示的数量
                            ,overflow: 'tips'
                            ,even: true
                            ,data: res.data   // 将新数据重新载入表格
                            ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                            ,done: function () {
                                soulTable.render(this);
                                layer.close(load);
                            }
                            ,filter: {
                                bottom: true,
                                clearFilter: false,
                                items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                            }
                        });

                    } else if (data.searchType === '辅料'){

                        table.render({
                            elem: '#placeOrderTable'
                            ,cols:[[
                                {type:'checkbox'},
                                {field: 'accessoryID', title: 'ID', hide:true},
                                {field: 'orderName', title: '款号', width: 120, sort: true, filter: true},
                                {field: 'accessoryName', title: '名称', width: 120, sort: true, filter: true, totalRowText: '合计'},
                                {field: 'accessoryNumber', title: '编号', width: 100, sort: true, filter: true},
                                {field: 'specification', title: '规格', width: 100, sort: true, filter: true},
                                {field: 'accessoryUnit', title: '单位', width: 100 , filter: true},
                                {field: 'accessoryColor', title: '颜色', width: 90, filter: true},
                                {field: 'orderPieceUsage', title: '接单用量', width: 90, filter: true},
                                {field: 'pieceUsage', title: '单件用量', width: 90, filter: true},
                                {field: 'colorName', title: '色组', width: 150, filter: true},
                                {field: 'sizeName', title: '尺码', width: 150, filter: true},
                                {field: 'accessoryCount', title: '订购数量', width: 100, filter: true, totalRow: true},
                                {field: 'checkState', title: '审核状态', width: 100, filter: true}
                            ]]
                            ,excel:{ // 导出excel配置, （以下值均为默认值）
                                on: true, //是否启用, 默认开启
                                filename: '辅料数据表.xlsx', // 文件名
                                checked: true
                            }
                            ,height: 'full-130'
                            ,limit: Number.MAX_VALUE //每页默认显示的数量
                            ,overflow: 'tips'
                            ,even: true
                            ,data: res.data   // 将新数据重新载入表格
                            ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
                            ,done: function () {
                                soulTable.render(this);
                                layer.close(load);
                            }
                            ,filter: {
                                bottom: true,
                                clearFilter: false,
                                items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                            }
                        });
                    }
                } else {
                    layer.msg("获取失败！", {icon: 2});
                }
            }, error: function () {
                layer.msg("获取失败！", {icon: 2});
            }
        });
        return false;
    });

    table.on('toolbar(placeOrderTable)', function(obj) {
        if (obj.event === 'clearFilter') {
            // 清除所有筛选条件并重载表格
            // 参数: tableId
            soulTable.clearFilter('placeOrderTable')
        } else if (obj.event === 'exportExcel') {
            var exportData = table.cache.placeOrderTable;
            var checkFlag = false;
            // $.each(exportData, function(index,element){
            //     if (element.checkState != "通过"){
            //         checkFlag = true;
            //     }
            // });
            if (checkFlag){
                layer.msg("未审核,请先审核！", { icon: 2 });
                return false;
            }
            soulTable.export('placeOrderTable');
        } else if (obj.event === 'pass') {
            if (userRole != 'role10' && userRole != 'root'){
                layer.msg("您没有权限！", {icon: 2});
                return false;
            }
            var checkStatus = table.checkStatus('placeOrderTable');
            var data = checkStatus.data;
            if ($("#searchType").val() === '面料'){
                var fabricIDList = [];
                for (var i = 0; i < data.length; i++){
                    fabricIDList.push(data[i].fabricID);
                }
                $.ajax({
                    url: "/erp/changefabriccheckstateinplaceorder",
                    type: 'POST',
                    traditional: true,
                    data: {
                        fabricIDList: fabricIDList,
                        checkState: "通过"
                    },
                    success:function(data){
                        if (data == 0){
                            layer.msg("提交成功！", { icon: 1 });
                            var load = layer.load(2);
                            var beforeData = table.cache.placeOrderTable;
                            $.each(beforeData, function(index,element){
                                if (element.LAY_CHECKED){
                                    element.checkState = '通过';
                                }
                            });
                            layui.table.reload("placeOrderTable", {
                                data: beforeData   // 将新数据重新载入表格
                                ,overflow: 'tips'
                                ,done: function () {
                                    soulTable.render(this);
                                    layer.close(load);
                                }
                                ,filter: {
                                    bottom: true,
                                    clearFilter: false,
                                    items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                                }
                            });
                        } else {
                            layer.msg("提交失败！", { icon: 2 });
                        }
                    }, error: function () {
                        layer.msg("提交失败", { icon: 2 });
                    }
                });
            }else if ($("#searchType").val() === '辅料'){
                var accessoryIDList = [];
                for (var i = 0; i < data.length; i++){
                    accessoryIDList.push(data[i].accessoryID);
                }
                $.ajax({
                    url: "/erp/changeaccessorycheckstateinplaceorder",
                    type: 'POST',
                    data: {
                        accessoryIDList: accessoryIDList,
                        checkState: "通过"
                    },
                    traditional: true,
                    success:function(data){
                        if (data == 0){
                            layer.msg("提交成功！", { icon: 1 });
                            var load = layer.load(2);
                            var beforeData = table.cache.placeOrderTable;
                            $.each(beforeData, function(index,element){
                                if (element.LAY_CHECKED){
                                    element.checkState = '通过';
                                }
                            });
                            layui.table.reload("placeOrderTable", {
                                data: beforeData   // 将新数据重新载入表格
                                ,overflow: 'tips'
                                ,done: function () {
                                    soulTable.render(this);
                                    layer.close(load);
                                }
                                ,filter: {
                                    bottom: true,
                                    clearFilter: false,
                                    items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                                }
                            });
                        } else {
                            layer.msg("提交失败！", { icon: 2 });
                        }
                    }, error: function () {
                        layer.msg("提交失败", { icon: 2 });
                    }
                });
            }
        }else if (obj.event === 'unPass') {
            if (userRole != 'role10' && userRole != 'root'){
                layer.msg("您没有权限！", {icon: 2});
                return false;
            }
            var checkStatus = table.checkStatus('placeOrderTable');
            var data = checkStatus.data;
            if ($("#searchType").val() === '面料'){
                var fabricIDList = [];
                for (var i = 0; i < data.length; i++){
                    fabricIDList.push(data[i].fabricID);
                }
                $.ajax({
                    url: "/erp/changefabriccheckstateinplaceorder",
                    type: 'POST',
                    traditional: true,
                    data: {
                        fabricIDList: fabricIDList,
                        checkState: "不通过"
                    },
                    success:function(data){
                        if (data == 0){
                            layer.msg("提交成功！", { icon: 1 });
                            var load = layer.load(2);
                            var beforeData = table.cache.placeOrderTable;
                            $.each(beforeData, function(index,element){
                                if (element.LAY_CHECKED){
                                    element.checkState = '不通过';
                                }
                            });
                            layui.table.reload("placeOrderTable", {
                                data: beforeData   // 将新数据重新载入表格
                                ,overflow: 'tips'
                                ,done: function () {
                                    soulTable.render(this);
                                    layer.close(load);
                                }
                                ,filter: {
                                    bottom: true,
                                    clearFilter: false,
                                    items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                                }
                            });
                        } else {
                            layer.msg("提交失败！", { icon: 2 });
                        }
                    }, error: function () {
                        layer.msg("提交失败", { icon: 2 });
                    }
                });
            }else if ($("#searchType").val() === '辅料'){
                var accessoryIDList = [];
                for (var i = 0; i < data.length; i++){
                    accessoryIDList.push(data[i].accessoryID);
                }
                $.ajax({
                    url: "/erp/changeaccessorycheckstateinplaceorder",
                    type: 'POST',
                    data: {
                        accessoryIDList: accessoryIDList,
                        checkState: "不通过"
                    },
                    traditional: true,
                    success:function(data){
                        if (data == 0){
                            layer.msg("提交成功！", { icon: 1 });
                            var load = layer.load(2);
                            var beforeData = table.cache.placeOrderTable;
                            $.each(beforeData, function(index,element){
                                if (element.LAY_CHECKED){
                                    element.checkState = '不通过';
                                }
                            });
                            layui.table.reload("placeOrderTable", {
                                data: beforeData   // 将新数据重新载入表格
                                ,overflow: 'tips'
                                ,done: function () {
                                    soulTable.render(this);
                                    layer.close(load);
                                }
                                ,filter: {
                                    bottom: true,
                                    clearFilter: false,
                                    items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                                }
                            });
                        } else {
                            layer.msg("提交失败！", { icon: 2 });
                        }
                    }, error: function () {
                        layer.msg("提交失败", { icon: 2 });
                    }
                });
            }
        }else if (obj.event === 'returnReview') {
            if (userRole != 'role2' && userRole != 'root'){
                layer.msg("您没有权限！", {icon: 2});
                return false;
            }
            var checkStatus = table.checkStatus('placeOrderTable');
            var data = checkStatus.data;
            if ($("#searchType").val() === '面料'){
                var fabricIDList = [];
                for (var i = 0; i < data.length; i++){
                    fabricIDList.push(data[i].fabricID);
                }
                $.ajax({
                    url: "/erp/changefabriccheckstateinplaceorder",
                    type: 'POST',
                    traditional: true,
                    data: {
                        fabricIDList: fabricIDList,
                        checkState: "未提交"
                    },
                    success:function(data){
                        if (data == 0){
                            layer.msg("提交成功！", { icon: 1 });
                            var load = layer.load(2);
                            var beforeData = table.cache.placeOrderTable;
                            $.each(beforeData, function(index,element){
                                if (element.LAY_CHECKED){
                                    element.checkState = '未提交';
                                }
                            });
                            layui.table.reload("placeOrderTable", {
                                data: beforeData   // 将新数据重新载入表格
                                ,overflow: 'tips'
                                ,done: function () {
                                    soulTable.render(this);
                                    layer.close(load);
                                }
                                ,filter: {
                                    bottom: true,
                                    clearFilter: false,
                                    items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                                }
                            });
                        } else {
                            layer.msg("提交失败！", { icon: 2 });
                        }
                    }, error: function () {
                        layer.msg("提交失败", { icon: 2 });
                    }
                });
            }else if ($("#searchType").val() === '辅料'){
                var accessoryIDList = [];
                for (var i = 0; i < data.length; i++){
                    accessoryIDList.push(data[i].accessoryID);
                }
                $.ajax({
                    url: "/erp/changeaccessorycheckstateinplaceorder",
                    type: 'POST',
                    data: {
                        accessoryIDList: accessoryIDList,
                        checkState: "未提交"
                    },
                    traditional: true,
                    success:function(data){
                        if (data == 0){
                            layer.msg("提交成功！", { icon: 1 });
                            var load = layer.load(2);
                            var beforeData = table.cache.placeOrderTable;
                            $.each(beforeData, function(index,element){
                                if (element.LAY_CHECKED){
                                    element.checkState = '未提交';
                                }
                            });
                            layui.table.reload("placeOrderTable", {
                                data: beforeData   // 将新数据重新载入表格
                                ,overflow: 'tips'
                                ,done: function () {
                                    soulTable.render(this);
                                    layer.close(load);
                                }
                                ,filter: {
                                    bottom: true,
                                    clearFilter: false,
                                    items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                                }
                            });
                        } else {
                            layer.msg("提交失败！", { icon: 2 });
                        }
                    }, error: function () {
                        layer.msg("提交失败", { icon: 2 });
                    }
                });
            }
        }
    });

});





function renderSeasonDate(ohd, sgl) {
    var ele = $(ohd);
    $Date.render({
        elem: ohd,
        type: 'month',
        format: 'yyyy年M季度',
        range: sgl ? null : '~',
        min: "1900-1-1",
        max: "2099-12-31",
        btns: ['clear', 'confirm'],
        ready: function (value, date, endDate) {
            var hd = $("#layui-laydate" + ele.attr("lay-key"));
            if (hd.length > 0) {
                hd.click(function () {
                    ren($(this));
                });
            }
            ren(hd);
        },
        done: function (value, date, endDate) {
            if (!isNull(date) && date.month > 0 && date.month < 5) {
                ele.attr("startDate", date.year + "-" + date.month);
            } else {
                ele.attr("startDate", "");
            }
            if (!isNull(endDate) && endDate.month > 0 && endDate.month < 5) {
                ele.attr("endDate", endDate.year + "-" + endDate.month)
            } else {
                ele.attr("endDate", "");
            }
        }
    });
    var ren = function (thiz) {
        var mls = thiz.find(".laydate-month-list");
        mls.each(function (i, e) {
            $(this).find("li").each(function (inx, ele) {
                var cx = ele.innerHTML;
                if (inx < 4) {
                    ele.innerHTML = cx.replace(/月/g, "季度");
                } else {
                    ele.style.display = "none";
                }
            });
        });
    }
}
function initDateForm(sgl, form) {
    if (isNull(form)) form = $(document.body);
    var ltm = function (tar, tars, tva) {
        tars.each(function () {
            $(this).removeAttr("lay-key");
            this.outerHTML = this.outerHTML;
        });
        tars = form.find(".dateTarget" + tar);
        tars.each(function () {
            var ele = $(this);
            if ("y" == tva) {
                $Date.render({
                    elem: this,
                    type: 'year',
                    range: '~',
                    format: 'yyyy年',
                    range: sgl ? null : '~',
                    done: function (value, date, endDate) {
                        if (typeof (date.year) == "number") ele.attr("startDate", date.year);
                        else ele.attr("startDate", "");
                        if (typeof (endDate.year) == "number") ele.attr("endDate", endDate.year);
                        else ele.attr("endDate", "");
                    }
                });

            } else if ("s" == tva) {
                ele.attr("startDate", "");
                ele.attr("endDate", "");
                renderSeasonDate(this, sgl);
            } else if ("m" == tva) {
                ele.attr("startDate", "");
                ele.attr("endDate", "");
                $Date.render({
                    elem: this,
                    type: 'month',
                    range: '~',
                    format: 'yyyy年MM月',
                    range: sgl ? null : '~',
                    done: function (value, date, endDate) {
                        if (typeof (date.month) == "number") ele.attr("startDate", date.year + "-" + date.month);
                        else ele.attr("startDate", "");
                        if (typeof (endDate.month) == "number") ele.attr("endDate", endDate.year + "-" + endDate.month);
                        else ele.attr("endDate", "");
                    }
                });
            } else if ("d" == tva) {
                ele.attr("startDate", "");
                ele.attr("endDate", "");
                $Date.render({
                    elem: this,
                    range: '~',
                    format: 'yyyy年MM月dd日',
                    range: sgl ? null : '~',
                    done: function (value, date, endDate) {
                        if (typeof (date.date) == "number") ele.attr("startDate", date.year + "-" + date.month + "-" + date.date);
                        else ele.attr("startDate", "");
                        if (typeof (endDate.date) == "number") ele.attr("endDate", endDate.year + "-" + endDate.month + "-" +
                            endDate.date);
                        else ele.attr("endDate", "");
                    }
                });
            }
        });
    }
    var sels = form.find(".dateSelector");
    sels.each(function (i, e) {
        var ths = this;
        var thiz = $(e);
        var tar = thiz.attr("date-target");
        thiz.next().find("dd").click(function () {
            var tva = thiz.val();
            var tars = form.find(".dateTarget" + tar);
            ltm(tar, tars, tva);
        });
        thiz.change(function () {
            var tva = $(this).val();
            var tars = form.find(".dateTarget" + tar);
            ltm(tar, tars, tva);
        });
        var tars = form.find(".dateTarget" + tar);
        ltm(tar, tars, thiz.val());
    });
}

function isNull(s) {
    if (s == null || typeof (s) == "undefined" || s == "") return true;
    return false;
}