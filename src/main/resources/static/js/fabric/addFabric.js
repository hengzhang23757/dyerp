var hot;
var basePath=$("#basePath").val();
var userName=$("#userName").val();
console.log(userName);
$(document).ready(function () {
    var container = document.getElementById('addFabricExcel');
    hot = new Handsontable(container, {
        // data: data,
        rowHeaders: true,
        // colHeaders: true,
        colHeaders:['成衣客户','季度','版单号','大货款号','供应商','下单日期','面料交期','面料名称','面料编号','面料颜色','面料色号','面料用途','单位','单件用量','成衣数量','订布数量','单价','有效门幅','克重','实际交货日期','交货数量','卷数','缸号','备注'],
        autoColumnSize:true,
        dropdownMenu: true,
        contextMenu:true,
        minRows:20,
        minCols:24,
        // colWidths:70,
        language:'zh-CN',
        licenseKey: 'non-commercial-and-evaluation'
    });
});

function add() {
    swal({
        title: "",
        text: "<span style=\"font-weight:bolder;font-size: 20px\">您确定要提交输入的面料信息吗？</span>",
        type: "warning",
        html:true,
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: "确定",
        cancelButtonText:"我再想想",
        confirmButtonColor: "#ec6c62",
        showLoaderOnConfirm: false
    }, function() {
        var fabricJson = [];
        var data = hot.getData();
        // console.log(data);
        // var titleRow = data[0];
        // var endIndex = titleRow.length;
        // $.each(titleRow,function (index, item) {
        //     if(item==null || item=="") {
        //         endIndex = index;
        //         return false;
        //     }
        // });
        // titleRow.splice(endIndex,titleRow.length-endIndex);
        $.each(data,function (index, item) {
            if(item[0]!=null && item[0]!="") {
                var fabric = {};
                fabric.customerName = item[0].trim();
                fabric.season = item[1].trim();
                fabric.versionNumber = item[2].trim();
                fabric.styleNumber = item[3].trim();
                fabric.supplier = item[4].trim();
                fabric.orderDate = item[5].trim();
                fabric.deliveryDate = item[6].trim();
                fabric.fabricName = item[7].trim();
                fabric.fabricNumber = item[8].trim();
                fabric.fabricColor = item[9].trim();
                fabric.fabricColorNumber = item[10].trim();
                fabric.fabricUse = item[11].trim();
                fabric.unit = item[12].trim();
                fabric.pieceUsage = item[13].trim();
                fabric.clothesCount = item[14].trim();
                fabric.fabricCount = item[15].trim();
                fabric.price = item[16].trim();
                fabric.fabricWidth = item[17].trim();
                fabric.fabricWeight = item[18].trim();
                fabric.actualDeliveryDate = item[19].trim();
                fabric.deliveryQuantity = item[20].trim();
                fabric.batchNumber = item[21].trim();
                fabric.jarNumber = item[22].trim();
                fabric.remark = item[23];
                fabricJson.push(fabric);
            }
        });
        console.log(fabricJson);
        $.ajax({
            url: "/erp/addfabricbatch",
            type:'POST',
            data: {
                fabricJson:JSON.stringify(fabricJson),
                userName:userName
            },
            success: function (data) {
                if(data == 0) {
                    // swal({type:"success",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">恭喜你，录入成功！</span>",html: true});
                    swal({type:"success",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">恭喜你，录入成功！</span>",html: true},function (){
                        hot.clear();
                    });
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，录入失败！</span>",html: true});
                }
            },
            error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        })
    })
}