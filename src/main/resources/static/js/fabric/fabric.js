var userName=$("#userName").val();
var userRole=$("#userRole").val();
$(document).ready(function () {
    createOneMonthFabricTable();
    $('#mainFrameTabs').bTabs();

    $('#orderListTab').click(function(){
        createOneMonthFabricTable();
    });

    layui.laydate.render({
        elem: '#orderDate', //指定元素
        trigger: 'click'
    });
    layui.laydate.render({
        elem: '#deliveryDate', //指定元素
        trigger: 'click'
    });
    layui.laydate.render({
        elem: '#actualDeliveryDate', //指定元素
        trigger: 'click'
    });
});


//计算内容区域高度
function calcHeight(){
    var browserHeight = $(window).innerHeight();
    var topHeight = $('#mainFrameHeadBar').outerHeight(true);
    var tabMarginTop = parseInt($('#mainFrameTabs').css('margin-top'));//获取间距
    var tabHeadHeight = $('ul.nav-tabs',$('#mainFrameTabs')).outerHeight(true) + tabMarginTop;
    var contentMarginTop = parseInt($('div.tab-content',$('#mainFrameTabs')).css('margin-top'));//获取内容区间距
    var contentHeight = browserHeight - topHeight - tabHeadHeight - contentMarginTop;
    $('div.tab-content',$('#mainFrameTabs')).height(contentHeight);
};

var basePath=$("#basePath").val();

var fabricTable;
function createFabricTable() {
    if (fabricTable != undefined) {
        fabricTable.clear(); //清空一下table
        fabricTable.destroy(); //还原初始化了的datatable
    }
    $.ajax({
        url: "/erp/getallfabric",
        type:'GET',
        data: {
        },
        success: function (html) {
            $("#detailEntities").html(html);
            $('html, body').scrollTop($("#detailEntities").offset().top);

            var table = $('#fabricTable').DataTable({
                language : {
                    processing : "载入中",//处理页面数据的时候的显示
                    paginate : {//分页的样式文本内容。
                        previous : "上一页",
                        next : "下一页",
                        first : "第一页",
                        last : "最后一页"
                    },
                    search:"搜索：",
                    zeroRecords : "没有内容",//table tbody内容为空时，tbody的内容。
                    //下面三者构成了总体的左下角的内容。
                    info : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//左下角的信息显示，大写的词为关键字。
                    infoEmpty : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//筛选为空时左下角的显示。
                    infoFiltered : ""//筛选之后的左下角筛选提示(另一个是分页信息显示，在上面的info中已经设置，所以可以不显示)，
                },
                searching:true,
                lengthChange:true,
                pageLength : 50,// 每页显示10条数据
                pagingType : "full_numbers", // 分页样式：simple,simple_numbers,full,full_numbers，
                "paging" : true,
                "info": true,
                scrollX: 2000,
                scrollY: 400,
                scrollCollapse: true,
                scroller:       true,
                dom: 'Bfrtip',
                "buttons": [{
                    'extend': 'excel',
                    'text': '导出',//定义导出excel按钮的文字
                    'className': 'btn btn-success', //按钮的class样式
                    'title': "面料表",
                    'exportOptions': {
                        'modifier': {
                            'page': 'all'
                        }
                    }
                }],
                "footerCallback": function ( row, data, start, end, display ) {
                    var api = this.api(), data;

                    // Remove the formatting to get integer data for summation
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };

                    // Total over all pages
                    total = api
                        .column( 21 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    batchTotal = api
                        .column( 22 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Total over this page
                    pageTotal = api
                        .column( 21, { page: 'current'} )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    batchPageTotal = api
                        .column( 22, { page: 'current'} )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Update footer
                    $( api.column( 4 ).footer() ).html(
                        // "总计数量："+ total,
                        "当前页数量："+pageTotal.toFixed(2) +"，总计："+ total.toFixed(2)+ "<br>" +
                        "当前页卷数："+batchPageTotal +"，总计："+ batchTotal
                    );
                }
            });
            $('#fabricTable_wrapper .dt-buttons').append("<button class=\"btn btn-success\" style=\"outline:none;margin-left: 10px\" onclick=\"addFabric()\">添加面料</button>");
            new $.fn.dataTable.FixedColumns(table,{"iLeftColumns":5
            } );
            },
        error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }

    });
}


function createOneMonthFabricTable() {
    if (fabricTable != undefined) {
        fabricTable.clear(); //清空一下table
        fabricTable.destroy(); //还原初始化了的datatable
    }
    $.ajax({
        url: "/erp/getonemonthfabric",
        type:'GET',
        data: {
        },
        success: function (html) {
            $("#detailEntities").html(html);
            $('html, body').scrollTop($("#detailEntities").offset().top);

            var table = $('#fabricTable').DataTable({
                language : {
                    processing : "载入中",//处理页面数据的时候的显示
                    paginate : {//分页的样式文本内容。
                        previous : "上一页",
                        next : "下一页",
                        first : "第一页",
                        last : "最后一页"
                    },
                    search:"搜索：",
                    zeroRecords : "没有内容",//table tbody内容为空时，tbody的内容。
                    //下面三者构成了总体的左下角的内容。
                    info : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//左下角的信息显示，大写的词为关键字。
                    infoEmpty : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//筛选为空时左下角的显示。
                    infoFiltered : ""//筛选之后的左下角筛选提示(另一个是分页信息显示，在上面的info中已经设置，所以可以不显示)，
                },
                searching:true,
                lengthChange:true,
                pageLength : 50,// 每页显示10条数据
                pagingType : "full_numbers", // 分页样式：simple,simple_numbers,full,full_numbers，
                "paging" : true,
                "info": true,
                scrollX: 2000,
                scrollY: 400,
                scrollCollapse: true,
                scroller:       true,
                // ordering: false,
                dom: 'Bfrtip',
                "buttons": [{
                    'extend': 'excel',
                    'text': '导出',//定义导出excel按钮的文字
                    'className': 'btn btn-success', //按钮的class样式
                    'title': "面料表",
                    'exportOptions': {
                        'modifier': {
                            'page': 'all'
                        }
                    }
                }],
                "footerCallback": function ( row, data, start, end, display ) {
                    var api = this.api(), data;

                    // Remove the formatting to get integer data for summation
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };

                    // Total over all pages
                    total = api
                        .column( 21 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    batchTotal = api
                        .column( 22 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Total over this page
                    pageTotal = api
                        .column( 21, { page: 'current'} )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    batchPageTotal = api
                        .column( 22, { page: 'current'} )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Update footer
                    $( api.column( 4 ).footer() ).html(
                        // "总计数量："+ total,
                        "当前页数量："+pageTotal.toFixed(2) +"，总计："+ total.toFixed(2)+ "<br>" +
                        "当前页卷数："+batchPageTotal +"，总计："+ batchTotal
                    );
                }
            });
            $('#fabricTable_wrapper .dt-buttons').append("<button class=\"btn btn-success\" style=\"outline:none;margin-left: 10px\" onclick=\"addFabric()\">添加面料</button>");
            new $.fn.dataTable.FixedColumns(table,{"iLeftColumns":5
            } );
        },
        error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }

    });
}


function createThreeMonthsFabricTable() {
    if (fabricTable != undefined) {
        fabricTable.clear(); //清空一下table
        fabricTable.destroy(); //还原初始化了的datatable
    }
    $.ajax({
        url: "/erp/getthreemonthsfabric",
        type:'GET',
        data: {
        },
        success: function (html) {
            $("#detailEntities").html(html);
            $('html, body').scrollTop($("#detailEntities").offset().top);

            var table = $('#fabricTable').DataTable({
                language : {
                    processing : "载入中",//处理页面数据的时候的显示
                    paginate : {//分页的样式文本内容。
                        previous : "上一页",
                        next : "下一页",
                        first : "第一页",
                        last : "最后一页"
                    },
                    search:"搜索：",
                    zeroRecords : "没有内容",//table tbody内容为空时，tbody的内容。
                    //下面三者构成了总体的左下角的内容。
                    info : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//左下角的信息显示，大写的词为关键字。
                    infoEmpty : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//筛选为空时左下角的显示。
                    infoFiltered : ""//筛选之后的左下角筛选提示(另一个是分页信息显示，在上面的info中已经设置，所以可以不显示)，
                },
                searching:true,
                lengthChange:true,
                pageLength : 50,// 每页显示10条数据
                pagingType : "full_numbers", // 分页样式：simple,simple_numbers,full,full_numbers，
                "paging" : true,
                "info": true,
                scrollX: 2000,
                scrollY: 400,
                fixedColumns: {
                    leftColumns: 3
                },
                scrollCollapse: true,
                scroller:       true,
                dom: 'Bfrtip',
                "buttons": [{
                    'extend': 'excel',
                    'text': '导出',//定义导出excel按钮的文字
                    'className': 'btn btn-success', //按钮的class样式
                    'title': "面料表",
                    'exportOptions': {
                        'modifier': {
                            'page': 'all'
                        }
                    }
                }],
                "footerCallback": function ( row, data, start, end, display ) {
                    var api = this.api(), data;

                    // Remove the formatting to get integer data for summation
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };

                    // Total over all pages
                    total = api
                        .column( 21 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    batchTotal = api
                        .column( 22 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Total over this page
                    pageTotal = api
                        .column( 21, { page: 'current'} )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    batchPageTotal = api
                        .column( 22, { page: 'current'} )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Update footer
                    $( api.column( 4 ).footer() ).html(
                        // "总计数量："+ total,
                        "当前页数量："+pageTotal.toFixed(2) +"，总计："+ total.toFixed(2)+ "<br>" +
                        "当前页卷数："+batchPageTotal +"，总计："+ batchTotal
                    );
                }
            });
            $('#fabricTable_wrapper .dt-buttons').append("<button class=\"btn btn-success\" style=\"outline:none;margin-left: 10px\" onclick=\"addFabric()\">添加面料</button>");
            new $.fn.dataTable.FixedColumns(table,{"iLeftColumns":5
            } );
        },
        error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }

    });
}


function deleteFabric(fabricID) {
    if(userRole!='root' && userRole!='role3'){
        swal("SORRY!", "对不起，您没有操作权限！", "warning");
        return;
    }
    swal({
        title: "",
        text: "<span style=\"font-weight:bolder;font-size: 20px\">您确定要删除该面料信息吗？</span>",
        type: "warning",
        html:true,
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: "确定",
        cancelButtonText:"我再想想",
        confirmButtonColor: "#ec6c62",
        showLoaderOnConfirm: true
    }, function() {
        $.ajax({
            url: basePath + "erp/deletefabric",
            type:'POST',
            data: {
                fabricID:fabricID,
                userName:userName
            },
            success: function (data) {
                if(data == 0) {
                    swal({type:"success",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">恭喜你，删除成功！</span>",html: true});
                    createFabricTable();
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，删除失败！</span>",html: true});
                }
            },
            error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        })
    });
}

var tabId = 1;
function addFabric() {
    if(userRole!='root' && userRole!='role3'){
        swal("SORRY!", "对不起，您没有操作权限！", "warning");
        return;
    }
    $('#mainFrameTabs').bTabsAdd("tabId" + tabId, "面料录入", "/erp/addFabricStart");
    tabId++;
}

function updateFabric(obj,fabricID){
    if(userRole!='root' && userRole!='role3'){
        swal("SORRY!", "对不起，您没有操作权限！", "warning");
        return;
    }
    $("#customerName").val($(obj).parent().parent().find("td").eq(1).text());
    $("#season").val($(obj).parent().parent().find("td").eq(2).text());
    $("#versionNumber").val($(obj).parent().parent().find("td").eq(3).text());
    $("#styleNumber").val($(obj).parent().parent().find("td").eq(4).text());
    $("#supplier").val($(obj).parent().parent().find("td").eq(5).text());
    $("#orderDate").val($(obj).parent().parent().find("td").eq(6).text());
    $("#deliveryDate").val($(obj).parent().parent().find("td").eq(7).text());
    $("#fabricName").val($(obj).parent().parent().find("td").eq(8).text());
    $("#fabricNumber").val($(obj).parent().parent().find("td").eq(9).text());
    $("#fabricColor").val($(obj).parent().parent().find("td").eq(10).text());
    $("#fabricColorNumber").val($(obj).parent().parent().find("td").eq(11).text());
    $("#fabricUse").val($(obj).parent().parent().find("td").eq(12).text());
    $("#unit").val($(obj).parent().parent().find("td").eq(13).text());
    $("#pieceUsage").val($(obj).parent().parent().find("td").eq(14).text());
    $("#clothesCount").val($(obj).parent().parent().find("td").eq(15).text());
    $("#fabricCount").val($(obj).parent().parent().find("td").eq(16).text());
    $("#price").val($(obj).parent().parent().find("td").eq(17).text());
    $("#fabricWidth").val($(obj).parent().parent().find("td").eq(18).text());
    $("#fabricWeight").val($(obj).parent().parent().find("td").eq(19).text());
    $("#actualDeliveryDate").val($(obj).parent().parent().find("td").eq(20).text());
    $("#deliveryQuantity").val($(obj).parent().parent().find("td").eq(21).text());
    $("#batchNumber").val($(obj).parent().parent().find("td").eq(22).text());
    $("#jarNumber").val($(obj).parent().parent().find("td").eq(23).text());
    $("#remark").val($(obj).parent().parent().find("td").eq(24).text());
    $.blockUI({
        css: {
            width: '90%',
            top: '5%',
            left:'5%',
            border: 'none',
            padding: '15px',
            backgroundColor: '#fff',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: 1,
            color: '#000'
        },
        message: $('#editFabric')
    });

    $("#editFabricYes").unbind("click").bind("click", function () {
        var flag = false;
        if ($("#styleNumber").val().trim()==""){
            flag = true;
        }
        if ($("#versionNumber").val().trim() == ""){
            flag = true;
        }
        if ($("#fabricNumber").val().trim() == ""){
            flag = true;
        }
        if ($("#fabricColorNumber").val().trim() == ""){
            flag = true;
        }
        if ($("#deliveryQuantity").val().trim() == ""){
            flag = true;
        }
        if ($("#batchNumber").val().trim() == ""){
            flag = true;
        }
        if ($("#jarNumber").val().trim() == ""){
            flag = true;
        }
        if(flag) {
            swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请填写完所有字段信息！</span>",html: true});
            return false;
        }
        $.ajax({
            url: basePath + "erp/updatefabric",
            type:'POST',
            data: {
                fabricID:fabricID,
                customerName: $("#customerName").val(),
                season:$("#season").val(),
                versionNumber:$("#versionNumber").val(),
                styleNumber:$("#styleNumber").val(),
                supplier:$("#supplier").val(),
                orderDate:$("#orderDate").val(),
                deliveryDate:$("#deliveryDate").val(),
                fabricName:$("#fabricName").val(),
                fabricNumber:$("#fabricNumber").val(),
                fabricColor:$("#fabricColor").val(),
                fabricColorNumber:$("#fabricColorNumber").val(),
                fabricUse:$("#fabricUse").val(),
                unit:$("#unit").val(),
                pieceUsage:$("#pieceUsage").val(),
                clothesCount:$("#clothesCount").val(),
                fabricCount:$("#fabricCount").val(),
                price:$("#price").val(),
                fabricWidth:$("#fabricWidth").val(),
                fabricWeight:$("#fabricWeight").val(),
                actualDeliveryDate:$("#actualDeliveryDate").val(),
                deliveryQuantity:$("#deliveryQuantity").val(),
                batchNumber:$("#batchNumber").val(),
                jarNumber:$("#jarNumber").val(),
                remark:$("#remark").val()
            },
            success: function (data) {
                if(data == 0) {
                    $.unblockUI();
                    $("#editFabric input").val("");
                    swal({type:"success",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">恭喜你，保存成功！</span>",html: true});
                    createFabricTable();
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，保存失败！</span>",html: true});
                }
            },
            error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        })
    });
    $("#editFabricNo").unbind("click").bind("click", function () {
        $.unblockUI();
        $("#editFabric input").val("");
    });
}

function changeTable(obj){
    var opt = obj.options[obj.selectedIndex];
    if (opt.value == "oneMonth"){
        $.blockUI({ css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            },
            message: "<h3>稍等一下... 奋力搜索中</h3>"
        });
        createOneMonthFabricTable();
        $.unblockUI();

    }
    if (opt.value == "threeMonths"){
        $.blockUI({ css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            },
            message: "<h3>稍等一下... 奋力搜索中</h3>"
        });
        createThreeMonthsFabricTable();
        $.unblockUI();
    }
    if (opt.value == "all"){
        $.blockUI({ css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            },
            message: "<h3>稍等一下... 奋力搜索中</h3>"
        });
        createFabricTable();
        $.unblockUI();

    }
}
