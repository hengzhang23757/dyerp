var hot;
var basePath=$("#basePath").val();
layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.21'
}).extend();
$(document).ready(function () {
    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumber", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });

    $("#orderName").bind('blur change',function(){
        var orderName = $("#orderName").val();
        $("#colorName").empty();
        $.ajax({
            url: "/erp/getcolorhint",
            data: {"orderName": orderName},
            success:function(data){
                $("#colorName").empty();
                if (data.colorList) {
                    $("#colorName").empty();
                    $("#colorName").append("<option value=''>选择颜色</option>");
                    $.each(data.colorList, function(index,element){
                        $("#colorName").append("<option value='"+element.colorName+"'>"+element.colorName+"</option>");
                    })
                }
            },
            error:function(){
            }
        });
    });

    $("#clothesVersionNumber").bind('blur change',function(){
        var orderName = $("#orderName").val();
        $("#colorName").empty();
        $.ajax({
            url: "/erp/getcolorhint",
            data: {"orderName": orderName},
            success:function(data){
                $("#colorName").empty();
                if (data.colorList) {
                    $("#colorName").empty();
                    $("#colorName").append("<option value=''>选择颜色</option>");
                    $.each(data.colorList, function(index,element){
                        $("#colorName").append("<option value='"+element.colorName+"'>"+element.colorName+"</option>");
                    })
                }
            },
            error:function(){
            }
        });
    });

});


function search() {
    var container = document.getElementById('addOrderExcel');
    if(hot==undefined) {
        hot = new Handsontable(container, {
            // data: data,
            rowHeaders: true,
            colHeaders: true,
            autoColumnSize: true,
            dropdownMenu: true,
            contextMenu: true,
            // minRows: 20,
            minCols: 12,
            colWidths:[120,250,120,120,80,150,80,80,80,80,60],
            language: 'zh-CN',
            licenseKey: 'non-commercial-and-evaluation'
        });
    }
    var clothesVersionNumber = $("#clothesVersionNumber").val();
    var orderName = $("#orderName").val();

    if(orderName == "") {
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入款号！</span>",html: true});
        return false;
    }
    $.blockUI({ css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        },
        message: "<h3>稍等一下... 奋力搜索中</h3>"
    });
    if ($("#type").val() === "面料"){
        $.ajax({
            url: basePath+"erp/searchfabricleakbyorder",
            type:'GET',
            data: {
                orderName:orderName,
                colorName:$("#colorName").val()
            },
            success: function (data) {
                $("#exportDiv").show();
                var hotData = [];
                var tableHtml = "";
                $("#entities").empty();
                if(data) {
                    tableHtml = "<div class=\"col-md-12\" name='printTable'>\n" +
                        "               <section class=\"panel panel-default\" style='page-break-after:always;'>\n" +
                        "                   <header class=\"panel-heading font-bold\" style=\"text-align: center\">\n" +
                        "                       <div style=\"font-size: 20px;font-weight: 700\">德悦服饰单款面料报表</div><br>\n" +
                        "                       <div style=\"font-size: 14px;float: left;font-weight: 700\">版单："+clothesVersionNumber+" 订单："+orderName+"</div>\n"+
                        "                   </header>\n" +
                        "                   <table class=\"table table-bordered\" style='width: 100%'>\n" +
                        "                       <tbody>";

                    tableHtml += "<tr><td colspan='11' style='text-align: center; font-weight: 700; color: black'>面料回布汇总(回布数量=入库数量+超数暂存)</td></tr>";
                    tableHtml += "<tr><td style='width: 8%'>供应商</td><td style='width: 8%'>订单颜色</td><td style='width: 18%'>面料名称</td><td style='width: 10%'>面料颜色</td><td style='width: 10%'>面料用途</td><td style='width: 8%'>订布数量</td><td style='width: 8%'>单位</td><td style='width: 8%'>回布数量</td><td style='width: 8%'>差异</td><td style='width: 10%'>回布卷数</td><td style='width: 7%'>备注</td></tr>";
                    var orderSum = 0;
                    var actualSum = 0;
                    var differenceSum = 0;
                    var batchSum = 0;

                    if (data["fabricDifferenceList"]){
                        hotData.push([["总体对照"]]);
                        var differenceData = data["fabricDifferenceList"];
                        var differenceHead = [["面料名称"],["供应商"],["订单颜色"],["面料颜色"],["面料用途"],["订布数量"],["单位"],["回布数量"],["差异"],["回布卷数"]];
                        hotData.push(differenceHead);
                        for (var i=0;i<differenceData.length;i++){
                            tableHtml += "<tr><td>"+differenceData[i].supplier+"</td><td>"+differenceData[i].colorName+"</td><td>"+differenceData[i].fabricName+"</td><td>"+differenceData[i].fabricColor+"</td><td>"+differenceData[i].fabricUse+"</td><td>"+toDecimal(differenceData[i].orderCount)+"</td><td>"+differenceData[i].unit+"</td><td>"+toDecimal(differenceData[i].actualCount)+"</td><td>"+toDecimal(differenceData[i].differenceCount)+"</td><td>"+differenceData[i].batchNumber+"</td><td>"+differenceData[i].remark+"</td></tr>";
                            var differenceRow = [];
                            differenceRow.push(differenceData[i].fabricName);
                            differenceRow.push(differenceData[i].supplier);
                            differenceRow.push(differenceData[i].colorName);
                            differenceRow.push(differenceData[i].fabricColor);
                            differenceRow.push(differenceData[i].fabricUse);
                            differenceRow.push(toDecimal(differenceData[i].orderCount));
                            differenceRow.push(differenceData[i].unit);
                            differenceRow.push(toDecimal(differenceData[i].actualCount));
                            differenceRow.push(toDecimal(differenceData[i].differenceCount));
                            differenceRow.push(differenceData[i].batchNumber);
                            orderSum += differenceData[i].orderCount;
                            actualSum += differenceData[i].actualCount;
                            differenceSum += differenceData[i].differenceCount;
                            batchSum += differenceData[i].batchNumber;
                            hotData.push(differenceRow);
                        }
                        hotData.push([[],[],[],["小计"],[toDecimal(orderSum)],[],[toDecimal(actualSum)],[toDecimal(differenceSum)],[batchSum]]);
                        hotData.push([]);

                    }
                    tableHtml += "<tr><td colspan='5' style='text-align:center'>小计：</td><td>"+toDecimal(orderSum)+"</td><td></td><td>"+toDecimal(actualSum)+"</td><td>"+toDecimal(differenceSum)+"</td><td>"+batchSum+"</td><td></td></tr>";
                    var detailWeightSum = 0;
                    var detailBatchSum = 0;
                    tableHtml += "<tr><td colspan='11' style='text-align: center; font-weight: 700; color: black'>入库详情</td></tr>";
                    tableHtml += "<tr><td >日期</td><td>订单颜色</td><td>面料名称</td><td>面料颜色</td><td>回布数量</td><td>单位</td><td>回布卷数</td><td>缸号</td><td>位置</td><td>操作类型</td><td></td></tr>";
                    if (data["fabricDetailList"]){
                        hotData.push([["回布详情"]]);
                        var detailData = data["fabricDetailList"];
                        var detailHead = [["日期"],["订单颜色"],["面料名称"],["面料颜色"],["回布数量"],["单位"],["回布卷数"],["缸号"],["位置"],["操作类型"]];
                        hotData.push(detailHead);
                        for (var i=0;i<detailData.length;i++){
                            tableHtml += "<tr><td>"+moment(detailData[i].returnTime).format("YYYY-MM-DD")+"</td><td>"+detailData[i].colorName+"</td><td>"+detailData[i].fabricName+"</td><td>"+detailData[i].fabricColor+"</td><td>"+detailData[i].weight+"</td><td>"+detailData[i].unit+"</td><td>"+detailData[i].batchNumber+"</td><td>"+detailData[i].jarName+"</td><td>"+detailData[i].location+"</td><td>"+detailData[i].operateType+"</td><td></td></tr>";
                            var detailRow = [];
                            detailRow.push(moment(detailData[i].returnTime).format("YYYY-MM-DD"));
                            detailRow.push(detailData[i].colorName);
                            detailRow.push(detailData[i].fabricName);
                            detailRow.push(detailData[i].fabricColor);
                            detailRow.push(detailData[i].weight);
                            detailRow.push(detailData[i].unit);
                            detailRow.push(detailData[i].batchNumber);
                            detailRow.push(detailData[i].jarName);
                            detailRow.push(detailData[i].location);
                            detailRow.push(detailData[i].operateType);
                            detailWeightSum += detailData[i].weight;
                            detailBatchSum += detailData[i].batchNumber;
                            hotData.push(detailRow);
                        }
                        hotData.push([[],[],["小计"],[toDecimal(detailWeightSum)],[detailBatchSum],[],[],[]]);
                        hotData.push([]);
                    }
                    tableHtml += "<tr><td colspan='4' style='text-align:center'>小计：</td><td>"+toDecimal(detailWeightSum)+"</td><td></td><td>"+detailBatchSum+"</td><td></td><td></td><td></td><td></td></tr>";
                    var tmpWeightSum = 0;
                    var tmpBatchSum = 0;
                    tableHtml += "<tr><td colspan='11' style='text-align: center; font-weight: 700; color: black'>面料超数暂存</td></tr>";
                    tableHtml += "<tr><td >日期</td><td>订单颜色</td><td>面料名称</td><td>面料颜色</td><td>回布数量</td><td>单位</td><td>回布卷数</td><td>缸号</td><td>位置</td><td>操作类型</td><td></td></tr>";
                    if (data["fabricDetailTmpList"]){
                        hotData.push([["面料超数暂存"]]);
                        var detailData = data["fabricDetailTmpList"];
                        var detailHead = [["日期"],["订单颜色"],["面料名称"],["面料颜色"],["回布数量"],["单位"],["回布卷数"],["缸号"],["位置"],["操作类型"]];
                        hotData.push(detailHead);
                        for (var i=0;i<detailData.length;i++){
                            tableHtml += "<tr><td>"+moment(detailData[i].returnTime).format("YYYY-MM-DD")+"</td><td>"+detailData[i].colorName+"</td><td>"+detailData[i].fabricName+"</td><td>"+detailData[i].fabricColor+"</td><td>"+detailData[i].weight+"</td><td>"+detailData[i].unit+"</td><td>"+detailData[i].batchNumber+"</td><td>"+detailData[i].jarName+"</td><td>"+detailData[i].location+"</td><td>"+detailData[i].operateType+"</td><td></td></tr>";
                            var detailRow = [];
                            detailRow.push(moment(detailData[i].returnTime).format("YYYY-MM-DD"));
                            detailRow.push(detailData[i].colorName);
                            detailRow.push(detailData[i].fabricName);
                            detailRow.push(detailData[i].fabricColor);
                            detailRow.push(detailData[i].weight);
                            detailRow.push(detailData[i].unit);
                            detailRow.push(detailData[i].batchNumber);
                            detailRow.push(detailData[i].jarName);
                            detailRow.push(detailData[i].location);
                            detailRow.push(detailData[i].operateType);
                            tmpWeightSum += detailData[i].weight;
                            tmpBatchSum += detailData[i].batchNumber;
                            hotData.push(detailRow);
                        }
                        hotData.push([[],[],["小计"],[toDecimal(tmpWeightSum)],[tmpBatchSum],[],[],[]]);
                        hotData.push([]);
                    }
                    tableHtml += "<tr><td colspan='4' style='text-align:center'>小计：</td><td>"+toDecimal(tmpWeightSum)+"</td><td></td><td>"+tmpBatchSum+"</td><td></td><td></td><td></td><td></td></tr>";

                    tableHtml += "<tr><td colspan='11' style='text-align: center; font-weight: 700; color: black;'>出库汇总</td></tr>";
                    tableHtml += "<tr><td>出库日期</td><td>订单颜色</td><td>面料名称</td><td>面料颜色</td><td>出库数量</td><td>出库卷数</td><td>缸号</td><td>位置</td><td>出库类型</td><td>接收</td><td></td></tr>";
                    var outCountSum = 0;
                    var outBatchSum = 0;
                    if (data["fabricOutRecordList"]){
                        hotData.push([["出库汇总"]]);
                        var outData = data["fabricOutRecordList"];
                        console.log(outData);
                        var outHead=[["出库日期"],["订单颜色"]["面料名称"],["面料颜色"],["出库数量"],["出库卷数"],["缸号"],["位置"],["出库类型"],["接收"]];
                        hotData.push(outHead);
                        for (var j=0;j<outData.length;j++){
                            tableHtml += "<tr><td>"+moment(outData[j].returnTime).format("YYYY-MM-DD")+"</td><td>"+outData[j].colorName+"</td><td>"+outData[j].fabricName+"</td><td>"+outData[j].fabricColor+"</td><td>"+toDecimal(outData[j].weight)+"</td><td>"+outData[j].batchNumber+"</td><td>"+outData[j].jarName+"</td><td>"+outData[j].location+"</td><td>"+outData[j].operateType+"</td><td>"+outData[j].receiver+"</td><td></td></tr>";
                            var outRow = [];
                            outRow.push(moment(outData[j].returnTime).format("YYYY-MM-DD"));
                            outRow.push(outData[j].colorName);
                            outRow.push(outData[j].fabricName);
                            outRow.push(outData[j].fabricColor);
                            outRow.push(toDecimal(outData[j].weight));
                            outRow.push(outData[j].batchNumber);
                            outRow.push(outData[j].jarName);
                            outRow.push(outData[j].location);
                            outRow.push(outData[j].operateType);
                            outRow.push(outData[j].receiver);
                            outCountSum += outData[j].weight;
                            outBatchSum += outData[j].batchNumber;
                            hotData.push(outRow);
                        }
                        hotData.push([[],[],["小计"],[toDecimal(outCountSum)],[outBatchSum],[],[],[]]);
                        hotData.push([]);
                    }
                    tableHtml += "<tr><td colspan='4' style='text-align:center'>小计：</td><td>"+toDecimal(outCountSum)+"</td><td>"+outBatchSum+"</td><td></td><td></td><td></td><td></td></tr>";
                    tableHtml += "<tr><td colspan='11' style='text-align: center; font-weight: 700; color: black'>库存汇总</td></tr>";
                    tableHtml += "<tr><td>最后操作</td><td>订单颜色</td><td>面料名称</td><td>面料颜色</td><td>剩余数量</td><td>剩余卷数</td><td>缸号</td><td>位置</td><td></td><td></td><td></td></tr>";
                    var storageCountSum = 0;
                    var storageBatchSum = 0;
                    if (data["fabricStorageList"]){
                        hotData.push([["库存汇总"]]);
                        var storageData = data["fabricStorageList"];
                        var storageHead=[["最后操作"],["订单颜色"],["面料名称"],["面料颜色"],["剩余数量"],["剩余卷数"],["缸号"],["位置"]];
                        hotData.push(storageHead);
                        for (var k=0;k<storageData.length;k++){
                            tableHtml += "<tr><td>"+moment(storageData[k].returnTime).format("YYYY-MM-DD")+"</td><td>"+storageData[k].colorName+"</td><td>"+storageData[k].fabricName+"</td><td>"+storageData[k].fabricColor+"</td><td>"+toDecimal(storageData[k].weight)+"</td><td>"+storageData[k].batchNumber+"</td><td>"+storageData[k].jarName+"</td><td>"+storageData[k].location+"</td><td></td><td></td></tr>";
                            var storageRow = [];
                            storageRow.push(moment(storageData[k].returnTime).format("YYYY-MM-DD"));
                            storageRow.push(storageData[k].colorName);
                            storageRow.push(storageData[k].fabricName);
                            storageRow.push(storageData[k].fabricColor);
                            storageRow.push(toDecimal(storageData[k].weight));
                            storageRow.push(storageData[k].batchNumber);
                            storageRow.push(storageData[k].jarName);
                            storageRow.push(storageData[k].location);
                            storageCountSum += storageData[k].weight;
                            storageBatchSum += storageData[k].batchNumber;
                            hotData.push(storageRow);
                        }
                        hotData.push([[],[],["小计"],[toDecimal(storageCountSum)],[storageBatchSum]]);
                        hotData.push([]);
                    }
                    tableHtml += "<tr><td colspan='4' style='text-align:center'>小计：</td><td>"+toDecimal(storageCountSum)+"</td><td>"+storageBatchSum+"</td><td></td><td></td><td></td><td></td><td></td></tr>";
                    tableHtml +=  "                       </tbody>" +
                        "                   </table>" +
                        "               </section>" +
                        "              </div>";
                    $("#entities").append(tableHtml);
                }
                hot.loadData(hotData);
                $.unblockUI();
            }, error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        })
    } else if ($("#type").val() === "辅料"){
        $.ajax({
            url: basePath+"erp/searchaccessoryleakbyorder",
            type:'GET',
            data: {
                orderName:orderName
            },
            success: function (data) {
                $("#exportDiv").show();
                var hotData = [];
                var tableHtml = "";
                $("#entities").empty();
                if(data) {
                    tableHtml = "<div class=\"col-md-12\" name='printTable'>\n" +
                        "               <section class=\"panel panel-default\" style='page-break-after:always;'>\n" +
                        "                   <header class=\"panel-heading font-bold\" style=\"text-align: center\">\n" +
                        "                       <div style=\"font-size: 20px;font-weight: 700\">德悦服饰单款辅料报表</div><br>\n" +
                        "                       <div style=\"font-size: 14px;float: left;font-weight: 700\">版单："+clothesVersionNumber+" 订单："+orderName+"</div>\n"+
                        "                   </header>\n" +
                        "                   <table class=\"table table-bordered\" style='width: 100%; font-size: small;'>" +
                        "                       <tbody>";

                    tableHtml += "<tr><td colspan='14' style='text-align: center; font-weight: 700; color: black'>辅料汇总</td></tr>";

                    tableHtml += "<tr><th style='width: 15%'>辅料名称</th><th style='width: 10%'>辅料规格</th><th style='width: 6%'>单位</th><th style='width: 8%'>辅料颜色</th><th style='width: 10%'>色组</th><th style='width: 8%'>尺码</th><th style='width: 6%'>供应商</th><th style='width: 6%'>订购量</th><th style='width: 7%'>入库数量</th><th style='width: 6%'>订单数</th><th style='width: 5%'>单耗</th><th style='width: 5%'>理论</th><th style='width: 5%'>差异</th><th style='width: 5%'>好片</th></tr>";
                    var orderSum = 0;
                    var actualSum = 0;
                    var differenceSum = 0;
                    var planSum = 0;

                    if (data["plan"]){
                        hotData.push([["总体对照"]]);
                        var differenceData = data["plan"];
                        var differenceHead = [["辅料名称"],["辅料规格"],["单位"],["辅料颜色"],["色组"],["尺码"],["供应商"],["订购量"],["入库数量"],["订单数"],["单耗"],["理论"],["差异"],["好片数"]];
                        hotData.push(differenceHead);
                        for (var i=0;i<differenceData.length;i++){
                            tableHtml += "<tr><td>"+differenceData[i].accessoryName+"</td><td>"+differenceData[i].specification+"</td><td>"+differenceData[i].accessoryUnit+"</td><td>"+differenceData[i].accessoryColor+"</td><td>"+differenceData[i].colorName+"</td><td>"+differenceData[i].sizeName+"</td><td>"+differenceData[i].supplier+"</td><td>"+toDecimal(differenceData[i].accessoryCount)+"</td><td>"+toDecimal(differenceData[i].accessoryReturnCount)+"</td><td>"+differenceData[i].orderCount+"</td><td>"+differenceData[i].pieceUsage+"</td><td>"+differenceData[i].accessoryPlanCount+"</td>";
                            if (differenceData[i].accessoryReturnCount - differenceData[i].accessoryPlanCount < 0){
                                tableHtml += "<td style='color: red; font-weight: bolder'>"+toDecimal(differenceData[i].accessoryReturnCount - differenceData[i].accessoryPlanCount)+"</td><td>"+differenceData[i].wellCount+"</td></tr>";
                            } else {
                                tableHtml += "<td style='color: green; font-weight: bolder'>"+toDecimal(differenceData[i].accessoryReturnCount - differenceData[i].accessoryPlanCount)+"</td><td>"+differenceData[i].wellCount+"</td></tr>";
                            }
                            var differenceRow = [];
                            differenceRow.push(differenceData[i].accessoryName);
                            differenceRow.push(differenceData[i].specification);
                            differenceRow.push(differenceData[i].accessoryUnit);
                            differenceRow.push(differenceData[i].accessoryColor);
                            differenceRow.push(differenceData[i].colorName);
                            differenceRow.push(differenceData[i].sizeName);
                            differenceRow.push(differenceData[i].supplier);
                            differenceRow.push(toDecimal(differenceData[i].accessoryCount));
                            differenceRow.push(toDecimal(differenceData[i].accessoryReturnCount));
                            differenceRow.push(differenceData[i].orderCount);
                            differenceRow.push(differenceData[i].pieceUsage);
                            differenceRow.push(differenceData[i].accessoryPlanCount);
                            differenceRow.push(toDecimal(differenceData[i].accessoryReturnCount - differenceData[i].accessoryPlanCount));
                            differenceRow.push(differenceData[i].wellCount);
                            orderSum += differenceData[i].accessoryCount;
                            actualSum += differenceData[i].accessoryReturnCount;
                            planSum += differenceData[i].accessoryPlanCount;
                            differenceSum += toDecimal(differenceData[i].accessoryReturnCount - differenceData[i].accessoryPlanCount);
                            hotData.push(differenceRow);
                        }
                        hotData.push([[],[],[],["小计"],[toDecimal(orderSum)],[],[toDecimal(actualSum)],[toDecimal(planSum)],[toDecimal(differenceSum)]]);
                        hotData.push([]);

                    }
                    tableHtml += "<tr><td colspan='7' style='text-align:center'>小计：</td><td>"+toDecimal(orderSum)+"</td><td>"+toDecimal(actualSum)+"</td><td></td><td></td><td>"+ toDecimal(planSum) +"</td><td>"+toDecimal(differenceSum)+"</td><td></td></tr>";
                    var detailSum = 0;
                    tableHtml += "<tr><td colspan='14' style='text-align: center; font-weight: 700; color: black'>入库详情</td></tr>";
                    tableHtml += "<tr><td >辅料名称</td><td>辅料规格</td><td>单位</td><td>辅料颜色</td><td>色组</td><td>尺码</td><td>供应商</td><td>入库数量</td><td>日期</td><td>位置</td><td>订单数</td><td>好片数</td><td>色号</td><td></td></tr>";
                    if (data["inStore"]){
                        hotData.push([["入库详情"]]);
                        var detailData = data["inStore"];
                        var detailHead = [["辅料名称"],["辅料规格"],["单位"],["辅料颜色"],["色组"],["尺码"],["供应商"],["入库数量"],["日期"],["款号组"],["位置"]];
                        hotData.push(detailHead);
                        for (var i=0;i<detailData.length;i++){
                            tableHtml += "<tr><td>"+detailData[i].accessoryName+"</td><td>"+detailData[i].specification+"</td><td>"+detailData[i].accessoryUnit+"</td><td>"+detailData[i].accessoryColor+"</td><td>"+detailData[i].colorName+"</td><td>"+detailData[i].sizeName+"</td><td>"+detailData[i].supplier+"</td><td>"+toDecimal(detailData[i].inStoreCount)+"</td><td>"+moment(detailData[i].createTime).format("YYYY-MM-DD")+"</td><td>"+ detailData[i].accessoryLocation +"</td><td>"+ detailData[i].orderCount +"</td><td>"+ detailData[i].wellCount +"</td><td>"+ detailData[i].colorNumber +"</td><td></td></tr>";
                            var detailRow = [];
                            detailRow.push(detailData[i].accessoryName);
                            detailRow.push(detailData[i].specification);
                            detailRow.push(detailData[i].accessoryUnit);
                            detailRow.push(detailData[i].accessoryColor);
                            detailRow.push(detailData[i].colorName);
                            detailRow.push(detailData[i].sizeName);
                            detailRow.push(detailData[i].supplier);
                            detailRow.push(toDecimal(detailData[i].inStoreCount));
                            detailRow.push(moment(detailData[i].createTime).format("YYYY-MM-DD"));
                            detailRow.push(detailData[i].accessoryLocation);
                            detailRow.push(detailData[i].orderCount);
                            detailRow.push(detailData[i].wellCount);
                            detailRow.push(detailData[i].colorNumber);
                            detailSum += toDecimal(detailData[i].inStoreCount);
                            hotData.push(detailRow);
                        }
                        hotData.push([[],[],["小计"],[toDecimal(detailSum)],[],[],[],[]]);
                        hotData.push([]);
                    }
                    tableHtml += "<tr><td colspan='7' style='text-align:center'>小计：</td><td>"+toDecimal(detailSum)+"</td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
                    tableHtml += "<tr><td colspan='14' style='text-align: center; font-weight: 700; color: black;'>出库汇总</td></tr>";
                    tableHtml += "<tr><th>辅料名称</th><th>辅料规格</th><th>单位</th><th>辅料颜色</th><th>色组</th><th>尺码</th><th>供应商</th><th>入库数量</th><th>出库日期</th><th>位置</th><th>订单数</th><td>好片数</td><th>接收</th><th></th></tr>";
                    var outCountSum = 0;
                    if (data["out"]){
                        hotData.push([["出库汇总"]]);
                        var outData = data["out"];
                        var outHead= [["辅料名称"],["辅料规格"],["单位"],["辅料颜色"],["色组"],["尺码"],["供应商"],["入库数量"],["日期"],["位置"],["订单数"],["好片数"],["接收"]];
                        hotData.push(outHead);
                        for (var i=0;i<outData.length;i++){
                            tableHtml += "<tr><td>"+outData[i].accessoryName+"</td><td>"+outData[i].specification+"</td><td>"+outData[i].accessoryUnit+"</td><td>"+outData[i].accessoryColor+"</td><td>"+outData[i].colorName+"</td><td>"+outData[i].sizeName+"</td><td>"+outData[i].supplier+"</td><td>"+toDecimal(outData[i].outStoreCount)+"</td><td>"+moment(outData[i].createTime).format("YYYY-MM-DD")+"</td><td>"+ outData[i].accessoryLocation +"</td><td>"+ outData[i].orderCount +"</td><td>"+ outData[i].wellCount +"</td><td>"+ outData[i].receiver +"</td></tr>";
                            var detailRow = [];
                            detailRow.push(outData[i].accessoryName);
                            detailRow.push(outData[i].specification);
                            detailRow.push(outData[i].accessoryUnit);
                            detailRow.push(outData[i].accessoryColor);
                            detailRow.push(outData[i].colorName);
                            detailRow.push(outData[i].sizeName);
                            detailRow.push(outData[i].supplier);
                            detailRow.push(toDecimal(outData[i].outStoreCount));
                            detailRow.push(moment(outData[i].createTime).format("YYYY-MM-DD"));
                            detailRow.push(outData[i].accessoryLocation);
                            detailRow.push(outData[i].orderCount);
                            detailRow.push(outData[i].wellCount);
                            detailRow.push(outData[i].receiver);
                            outCountSum += toDecimal(outData[i].outStoreCount);
                            hotData.push(detailRow);
                        }
                        hotData.push([[],[],["小计"],[toDecimal(outCountSum)],[],[],[],[]]);
                        hotData.push([]);
                    }
                    tableHtml += "<tr><td colspan='7' style='text-align:center'>小计：</td><td>"+toDecimal(outCountSum)+"</td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
                    tableHtml += "<tr><td colspan='14' style='text-align: center; font-weight: 700; color: black'>库存汇总</td></tr>";
                    tableHtml += "<tr><th>辅料名称</th><th>辅料规格</th><th>单位</th><th>辅料颜色</th><th>色组</th><th>尺码</th><th>供应商</th><th>入库数量</th><th>最后操作</th><th>位置</th><th>订单数</th><th>好片数</th><th></th><th></th></tr>";
                    var storageSum = 0;
                    if (data["storage"]){
                        hotData.push([["库存汇总"]]);
                        var storageData = data["storage"];
                        var storageHead= [["辅料名称"],["辅料规格"],["单位"],["辅料颜色"],["色组"],["尺码"],["供应商"],["入库数量"],["日期"],["位置"],["订单数"],["好片数"]];
                        hotData.push(storageHead);
                        for (var i=0;i<storageData.length;i++){
                            tableHtml += "<tr><td>"+storageData[i].accessoryName+"</td><td>"+storageData[i].specification+"</td><td>"+storageData[i].accessoryUnit+"</td><td>"+storageData[i].accessoryColor+"</td><td>"+storageData[i].colorName+"</td><td>"+storageData[i].sizeName+"</td><td>"+storageData[i].supplier+"</td><td>"+toDecimal(storageData[i].storageCount)+"</td><td>"+moment(storageData[i].updateTime).format("YYYY-MM-DD")+"</td><td>"+ storageData[i].accessoryLocation +"</td><td>"+ storageData[i].orderCount +"</td><td>"+ storageData[i].wellCount +"</td><td></td><td></td></tr>";
                            var detailRow = [];
                            detailRow.push(storageData[i].accessoryName);
                            detailRow.push(storageData[i].specification);
                            detailRow.push(storageData[i].accessoryUnit);
                            detailRow.push(storageData[i].accessoryColor);
                            detailRow.push(storageData[i].colorName);
                            detailRow.push(storageData[i].sizeName);
                            detailRow.push(storageData[i].supplier);
                            detailRow.push(toDecimal(storageData[i].storageCount));
                            detailRow.push(moment(storageData[i].updateTime).format("YYYY-MM-DD"));
                            detailRow.push(storageData[i].accessoryLocation);
                            detailRow.push(storageData[i].orderCount);
                            detailRow.push(storageData[i].wellCount);
                            storageSum += toDecimal(storageData[i].storageCount);
                            hotData.push(detailRow);
                        }
                        hotData.push([[],[],["小计"],[toDecimal(storageSum)],[],[],[],[]]);
                        hotData.push([]);
                    }
                    tableHtml += "<tr><td colspan='7' style='text-align:center'>小计：</td><td>"+toDecimal(storageSum)+"</td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
                    tableHtml +=  "                       </tbody>" +
                        "                   </table>" +
                        "               </section>" +
                        "              </div>";
                    $("#entities").append(tableHtml);
                }
                hot.loadData(hotData);
                $.unblockUI();
            }, error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        })
    }else {
        var operateType = $("#type").val();
        $.ajax({
            url: basePath+"erp/getfabricoutrecordbyordertype",
            type:'GET',
            data: {
                orderName:orderName,
                operateType:operateType,
                colorName:$("#colorName").val()
            },
            success: function (data) {
                $("#exportDiv").show();
                var hotData = [];
                var tableHtml = "";
                $("#entities").empty();
                if(data) {
                    tableHtml = "<div class=\"col-md-12\" name='printTable'>\n" +
                        "               <section class=\"panel panel-default\" style='page-break-after:always;'>\n" +
                        "                   <header class=\"panel-heading font-bold\" style=\"text-align: center\">\n" +
                        "                       <div style=\"font-size: 20px;font-weight: 700\">德悦服饰"+operateType+"单</div><br>\n" +
                        "                       <div style=\"font-size: 14px;float: left;font-weight: 700\">版单："+clothesVersionNumber+" 订单："+orderName+"</div>\n"+
                        "                   </header>\n" +
                        "                   <table class=\"table table-bordered\" style='width: 100%'>\n" +
                        "                       <tbody>";

                    tableHtml += "<tr><td style='width: 8%'>日期</td><td style='width: 8%'>订单颜色</td><td style='width: 25%'>面料名称</td><td style='width: 10%'>面料颜色</td><td style='width: 10%'>卷数</td><td style='width: 10%'>重量(数量)</td><td style='width: 8%'>单位</td><td style='width: 10%'>缸号</td><td style='width: 10%'>位置</td></tr>";
                    var batchSum = 0;
                    var weightSum = 0;

                    if (data["fabricOutRecordList"]){
                        var fabricOutRecordData = data["fabricOutRecordList"];
                        var differenceHead = [["日期"],["订单颜色"],["面料名称"],["面料颜色"],["卷数"],["重量(数量)"],["单位"],["缸号"],["位置"]];
                        hotData.push(differenceHead);
                        for (var i=0;i<fabricOutRecordData.length;i++){
                            tableHtml += "<tr><td>"+moment(fabricOutRecordData[i].returnTime).format("YYYY-MM-DD") +"</td><td>"+fabricOutRecordData[i].colorName+"</td><td>"+fabricOutRecordData[i].fabricName+"</td><td>"+fabricOutRecordData[i].fabricColor+"</td><td>"+fabricOutRecordData[i].batchNumber+"</td><td>"+toDecimal(fabricOutRecordData[i].weight)+"</td><td>"+fabricOutRecordData[i].unit+"</td><td>"+fabricOutRecordData[i].jarName+"</td><td>"+fabricOutRecordData[i].location+"</td></tr>";
                            var differenceRow = [];
                            differenceRow.push(moment(fabricOutRecordData[i].returnTime).format("YYYY-MM-DD"));
                            differenceRow.push(fabricOutRecordData[i].colorName);
                            differenceRow.push(fabricOutRecordData[i].fabricName);
                            differenceRow.push(fabricOutRecordData[i].fabricColor);
                            differenceRow.push(fabricOutRecordData[i].batchNumber);
                            differenceRow.push(fabricOutRecordData[i].weight);
                            differenceRow.push(fabricOutRecordData[i].unit);
                            differenceRow.push(fabricOutRecordData[i].jarName);
                            differenceRow.push(fabricOutRecordData[i].location);
                            batchSum += fabricOutRecordData[i].batchNumber;
                            weightSum += fabricOutRecordData[i].weight;
                            hotData.push(differenceRow);
                        }
                        hotData.push([[],[],[],["小计"],[toDecimal(batchSum)],[],[toDecimal(weightSum)]]);
                        hotData.push([]);

                    }
                    tableHtml += "<tr><td colspan='4' style='text-align:center'>小计：</td><td>"+toDecimal(batchSum)+"</td><td>"+toDecimal(weightSum)+"</td><td></td><td></td><td></td></tr>";
                    tableHtml += "<tr><td colspan='5' style='text-align:center'>备注</td><td colspan='4' style='text-align:center; font-weight: 700; color: black'>确认签字:</td></tr>";

                    tableHtml +=  "                       </tbody>" +
                        "                   </table>" +
                        "               </section>" +
                        "              </div>";
                    $("#entities").append(tableHtml);
                }
                hot.loadData(hotData);
                $.unblockUI();
            }, error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        })

    }

}

function exportData() {
    var orderName = $("#styleNumber").val();
    var versionNumber = $("#clothesVersionNumber").val();
    var myDate = new Date();
    var title = orderName+'-'+versionNumber+'-面料详情-'+myDate.toLocaleString();
    var data = utl.Object.copyJson(hot.getData());//<=====读取handsontable的数据
    var result = [];
    var firstRow = new Array(data[0].length);
    firstRow[0] = title;
    for(var i=1;i<data[0].length;i++) {
        firstRow[i] = "";
    }
    result.push(firstRow);
    var col = 0;
    $.each(data,function (index, item) {
        for(var i=0;i<item.length;i++) {
            if(item[i]==null) {
                item[i] = '';
            }else if(index==1) {
                col++;
            }
        }
        result.push(item);
    });
    utl.XLSX.onExport(result,"Sheet1","xlsx",title+".xlsx");//<====导出
}

function toDecimal(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*100)/100;
    return f;
}

function printDeal(){
    $("#printf").empty();
    var printBoxs = document.getElementsByName('printTable');
    var newContent = '';
    for(var i=0;i<printBoxs.length;i++) {
        newContent += printBoxs[i].innerHTML;
    }
    var f = document.getElementById('printf');
    // f.contentDocument.write('<link rel="stylesheet" type="text/css" href="/css/app.v2.css">');
    f.contentDocument.write('<style>' +
        'table {' +
        'border-collapse:collapse;' +
        '}' +
        'td {\n' +
        '        border: 1px solid #000000;\n' +
        '    }'+
        '</style>');
    f.contentDocument.write(newContent);
    f.contentDocument.close();
    window.frames['printf'].focus();
    try{
        window.frames['printf'].print();
    }catch(err){
        f.contentWindow.print();
    }
}