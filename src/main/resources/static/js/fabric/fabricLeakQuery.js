var basePath=$("#basePath").val();
$(document).ready(function () {
    layui.use(['autocomplete'], function () {
        layui.autocomplete.render({
            elem: $('#season')[0],
            url: 'getseasonhint',
            template_val: '{{d}}',
            template_txt: '{{d}}',
            onselect: function (resp) {
                autoComplete(resp)
            }
        })
    });

    $('#season').keyup(function() {
        $("#versionNumber").empty();
        $("#versionNumber").append("<option value=''>版单号</option>");
    })

});

function autoComplete(season) {
    $("#versionNumber").empty();
    $.ajax({
        url: "/erp/getversionnumberbyseason",
        data: {"season": season},
        success:function(data){
            if (data.versionNumberList) {
                $("#versionNumber").append("<option value=''>版单号</option>");
                $.each(data.versionNumberList, function(index,element){
                    $("#versionNumber").append("<option value="+element+">"+element+"</option>");
                })
            }
        },
        error:function(){
        }
    });
}

var season = $("#season").val();
var versionNumber = $("#versionNumber").val();
var fabricLeakTable;
function searchFabricLeak() {
    if (fabricLeakTable != undefined) {
        fabricLeakTable.clear(); //清空一下table
        fabricLeakTable.destroy(); //还原初始化了的datatable
    }
    if($("#season").val().trim()=="") {
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入季度！</span>",html: true});
        return false;
    }
    if($("#versionNumber").val()==null) {
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入版单号！</span>",html: true});
        return false;
    }
    $.ajax({
        url: basePath + "erp/getfabricleakquery",
        type:'GET',
        data: {
            season:$("#season").val(),
            versionNumber:$("#versionNumber").val(),
        },
        success: function (html) {
            $("#entities").html(html);
            $('html, body').scrollTop($("#entities").offset().top);

            $('#fabricLeakTable').DataTable({
                language : {
                    processing : "载入中",//处理页面数据的时候的显示
                    paginate : {//分页的样式文本内容。
                        previous : "上一页",
                        next : "下一页",
                        first : "第一页",
                        last : "最后一页"
                    },
                    search:"搜索：",
                    zeroRecords : "没有内容",//table tbody内容为空时，tbody的内容。
                    //下面三者构成了总体的左下角的内容。
                    info : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//左下角的信息显示，大写的词为关键字。
                    infoEmpty : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//筛选为空时左下角的显示。
                    infoFiltered : ""//筛选之后的左下角筛选提示(另一个是分页信息显示，在上面的info中已经设置，所以可以不显示)，
                },
                searching:true,
                lengthChange:true,
                // pageLength : 15,// 每页显示10条数据
                // pagingType : "full_numbers",
                "paging" : false,
                "info": false,
                scrollX: 2000,
                scrollCollapse: true,
                scroller:       true,
                dom: 'Bfrtip',
                "buttons": [{
                    'extend': 'excel',
                    'text': '导出',//定义导出excel按钮的文字
                    'className': 'btn btn-success', //按钮的class样式
                    'title': season+"-"+versionNumber+"面料剩余",
                    'exportOptions': {
                        'modifier': {
                            'page': 'all'
                        }
                    }
                }],
                "footerCallback": function ( row, data, start, end, display ) {
                    var api = this.api(), data;

                    // Remove the formatting to get integer data for summation
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };

                    // Total over all pages
                    total = api
                        .column( 9 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    batchTotal = api
                        .column( 10 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Total over this page
                    pageTotal = api
                        .column( 9, { page: 'current'} )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    batchPageTotal = api
                        .column( 10, { page: 'current'} )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Update footer
                    $( api.column( 4 ).footer() ).html(
                        "当前页数量："+pageTotal.toFixed(2) +"，总计："+ total.toFixed(2) + "<br>" +
                        "当前页卷数："+batchPageTotal +"，总计："+ batchTotal
                    );
                }
            });
        },
        error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }
    })
}



// function searchFabricLeak() {
//
//     if($("#season").val().trim()=="") {
//         swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入季度！</span>",html: true});
//         return false;
//     }
//     if($("#versionNumber").val()==null) {
//         swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入版单号！</span>",html: true});
//         return false;
//     }
//     $.ajax({
//         url: basePath + "erp/getfabricleakquery",
//         type:'GET',
//         data: {
//             season:$("#season").val(),
//             versionNumber:$("#versionNumber").val(),
//         },
//         success: function (data) {
//             if(data) {
//                 // var json = JSON.parse(data);
//                 createFabricLeakTable(data.fabricLeakList);
//             }else {
//                 swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，没有查到相关信息！</span>",html: true});
//             }
//         },
//         error: function () {
//             swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
//         }
//     })
//
// }
//
//
//
// var fabricLeakTable;
// var myDate = new Date();
// function createFabricLeakTable(data) {
//     if (fabricLeakTable != undefined) {
//         fabricLeakTable.clear(); //清空一下table
//         fabricLeakTable.destroy(); //还原初始化了的datatable
//     }
//     fabricLeakTable = $('#fabricLeakTable').DataTable({
//         "data":data,
//         language : {
//             processing : "载入中",//处理页面数据的时候的显示
//             paginate : {//分页的样式文本内容。
//                 previous : "上一页",
//                 next : "下一页",
//                 first : "第一页",
//                 last : "最后一页"
//             },
//             search:"搜索：",
//             zeroRecords : "没有内容",//table tbody内容为空时，tbody的内容。
//             //下面三者构成了总体的左下角的内容。
//             info : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//左下角的信息显示，大写的词为关键字。
//             infoEmpty : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//筛选为空时左下角的显示。
//             infoFiltered : ""//筛选之后的左下角筛选提示(另一个是分页信息显示，在上面的info中已经设置，所以可以不显示)，
//         },
//         dom: 'Bfrtip',
//         "buttons": [{
//             'extend': 'excel',
//             'text': '导出',//定义导出excel按钮的文字
//             'className': 'btn', //按钮的class样式
//             'title': '面料查询'+myDate.toLocaleString( ),
//             'exportOptions': {
//                 'modifier': {
//                     'page': 'all'
//                 }
//             }
//         }],
//         pageLength : 15,// 每页显示10条数据
//         pagingType : "full_numbers", // 分页样式：simple,simple_numbers,full,full_numbers，
//         "paging": true,
//         "info": false,
//         searching:true,
//         lengthChange:false,
//         scrollX: 2000,
//         scrollCollapse: true,
//         scroller:       true,
//         "columns": [
//             {
//                 "data": null,
//                 "title":"序号",
//                 "width":"30px",
//                 "defaultContent": "",
//                 "sClass": "text-center",
//                 render: function (data, type, row, meta) {
//                     var no = meta.settings._iDisplayStart + meta.row + 1;
//                     return no;
//                 }
//             },{
//                 "data": "customerName",
//                 "title":"顾客",
//                 "width":"60px",
//                 "defaultContent": "",
//                 "sClass": "text-center",
//             },{
//                 "data": "season",
//                 "title":"季度",
//                 "width":"60px",
//                 "defaultContent": "",
//                 "sClass": "text-center",
//             }, {
//                 "data": "styleNumber",
//                 "title":"款号",
//                 "width":"60px",
//                 "defaultContent": "",
//                 "sClass": "text-center",
//             }, {
//                 "data": "versionNumber",
//                 "title":"版单号",
//                 "width":"60px",
//                 "defaultContent": "",
//                 "sClass": "text-center",
//             }, {
//                 "data": "fabricNumber",
//                 "title":"面料号",
//                 "width":"60px",
//                 "defaultContent": "",
//                 "sClass": "text-center",
//             }, {
//                 "data": "fabricColor",
//                 "title":"颜色",
//                 "width":"60px",
//                 "defaultContent": "",
//                 "sClass": "text-center",
//             }, {
//                 "data": "fabricColorNumber",
//                 "title":"色号",
//                 "width":"60px",
//                 "defaultContent": "",
//                 "sClass": "text-center",
//             }, {
//                 "data": "jarNumber",
//                 "title":"缸号",
//                 "width":"60px",
//                 "defaultContent": "",
//                 "sClass": "text-center",
//             }, {
//                 "data": "fabricCount",
//                 "title":"数量",
//                 "width":"60px",
//                 "defaultContent": "",
//                 "sClass": "text-center",
//             }, {
//                 "data": "batchNumber",
//                 "title":"卷数",
//                 "width":"60px",
//                 "defaultContent": "",
//                 "sClass": "text-center",
//             }, {
//                 "data": "location",
//                 "title":"位置",
//                 "width":"60px",
//                 "defaultContent": "",
//                 "sClass": "text-center",
//             }
//         ]
//     });
// }
