// var hot;
// var basePath=$("#basePath").val();
// layui.config({
//     base: '/js/ext/',   // 模块目录
//     version: 'v1.5.15'
// }).extend();
// $(document).ready(function () {
//
//     lay('#version').html('-v' + layui.laydate.v);
//     $Date = layui.laydate;
//     renderSeasonDate(document.getElementById('season'), 1);
//     initDateForm();
//
//     layui.laydate.render({
//         elem: '#orderDate',
//         trigger: 'click',
//         value: getFormatDate()
//     });
//
//     layui.laydate.render({
//         elem: '#deliveryDate',
//         trigger: 'click'
//     });
//
//     $.ajax({
//         url: "/erp/getdistinctfabricsupplier",
//         type: 'GET',
//         data: {
//         },
//         success:function(data){
//             if (data.supplierList) {
//                 $("#supplier").append("<option value=''>选择供应商</option>");
//                 $.each(data.supplierList, function(index,element){
//                     $("#supplier").append("<option value='"+element+"'>"+element+"</option>");
//                 });
//             }
//         }, error: function () {
//             layer.msg("获取供应商信息失败", { icon: 2 });
//         }
//     });
//
//     $.ajax({
//         url: "/erp/getallcustomername",
//         type: 'GET',
//         data: {
//         },
//         success:function(data){
//             if (data.customerNameList) {
//                 $("#customerName").append("<option value=''>选择品牌</option>");
//                 $.each(data.customerNameList, function(index,element){
//                     $("#customerName").append("<option value='"+element+"'>"+element+"</option>");
//                 });
//             }
//         }, error: function () {
//             layer.msg("获取供应商信息失败", { icon: 2 });
//         }
//     });
//
//     layui.use(['autocomplete'], function () {
//         layui.autocomplete.render({
//             elem: $('#materialName')[0],
//             url: 'getfabricnamehint',
//             template_val: '{{d}}',
//             template_txt: '{{d}}',
//             onselect: function (resp) {
//                 // autoComplete(resp)
//             }
//         })
//     });
//
//     $("#type").change(function () {
//         var type = $("#type").val();
//         if (type === "面料"){
//             layui.use(['autocomplete'], function () {
//                 layui.autocomplete.render({
//                     elem: $('#materialName')[0],
//                     url: 'getfabricnamehint',
//                     template_val: '{{d}}',
//                     template_txt: '{{d}}',
//                     onselect: function (resp) {
//                         // autoComplete(resp)
//                     }
//                 })
//             });
//             $("#supplier").empty();
//             $.ajax({
//                 url: "/erp/getdistinctfabricsupplier",
//                 type: 'GET',
//                 data: {
//                 },
//                 success:function(data){
//                     $("#supplier").empty();
//                     if (data.supplierList) {
//                         $("#supplier").append("<option value=''>选择供应商</option>");
//                         $.each(data.supplierList, function(index,element){
//                             $("#supplier").append("<option value='"+element+"'>"+element+"</option>");
//                         });
//                     }
//                 }, error: function () {
//                     layer.msg("获取供应商信息失败", { icon: 2 });
//                 }
//             });
//         } else {
//             layui.use(['autocomplete'], function () {
//                 layui.autocomplete.render({
//                     elem: $('#materialName')[0],
//                     url: 'getaccessorynamehint',
//                     template_val: '{{d}}',
//                     template_txt: '{{d}}',
//                     onselect: function (resp) {
//                         // autoComplete(resp)
//                     }
//                 })
//             });
//
//             $("#supplier").empty();
//             $.ajax({
//                 url: "/erp/getdistinctaccessorysupplier",
//                 type: 'GET',
//                 data: {
//                 },
//                 success:function(data){
//                     $("#supplier").empty();
//                     if (data.supplierList) {
//                         $("#supplier").append("<option value=''>选择供应商</option>");
//                         $.each(data.supplierList, function(index,element){
//                             $("#supplier").append("<option value='"+element+"'>"+element+"</option>");
//                         });
//                     }
//                 }, error: function () {
//                     layer.msg("获取供应商信息失败", { icon: 2 });
//                 }
//             });
//         }
//     });
//
//
//     layui.use(['yutons_sug'], function () {
//         layui.yutons_sug.render({
//             id: "clothesVersionNumber", //设置容器唯一id
//             height: "300",
//             width: "550",
//             limit:"10",            limits:[10,20,50,100],
//             cols: [
//                 [{
//                     field: 'clothesVersionNumber',
//                     title: '单号',
//                     align: 'left'
//                 }, {
//                     field: 'orderName',
//                     title: '款号',
//                     align: 'left'
//                 }]
//             ], //设置表头
//             params: [
//                 {
//                     name: 'clothesVersionNumber',
//                     field: 'clothesVersionNumber'
//                 }, {
//                     name: 'orderName',
//                     field: 'orderName'
//                 }],//设置字段映射，适用于输入一个字段，回显多个字段
//             type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
//             url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
//         });
//
//         layui.yutons_sug.render({
//             id: "orderName", //设置容器唯一id
//             height: "300",
//             width: "550",
//             limit:"10",            limits:[10,20,50,100],
//             cols: [
//                 [{
//                     field: 'clothesVersionNumber',
//                     title: '单号',
//                     align: 'left'
//                 }, {
//                     field: 'orderName',
//                     title: '款号',
//                     align: 'left'
//                 }]
//             ], //设置表头
//             params: [
//                 {
//                     name: 'clothesVersionNumber',
//                     field: 'clothesVersionNumber'
//                 }, {
//                     name: 'orderName',
//                     field: 'orderName'
//                 }],//设置字段映射，适用于输入一个字段，回显多个字段
//             type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
//             url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
//         });
//         layui.yutons_sug.render({
//             id: "supplierName", //设置容器唯一id
//             height: "300",
//             width: "550",
//             limit:"10",            limits:[10,20,50,100],
//             cols: [
//                 [{
//                     field: 'supplierName',
//                     title: '供应商',
//                     align: 'left'
//                 }, {
//                     field: 'supplierFullName',
//                     title: '供应商全称',
//                     align: 'left'
//                 }, {
//                     field: 'supplierAddress',
//                     title: '供应商地址',
//                     align: 'left'
//                 }]
//             ], //设置表头
//             params: [
//                 {
//                     name: 'supplierName',
//                     field: 'supplierName'
//                 }, {
//                     name: 'supplierFullName',
//                     field: 'supplierFullName'
//                 }, {
//                     name: 'supplierAddress',
//                     field: 'supplierAddress'
//                 }],//设置字段映射，适用于输入一个字段，回显多个字段
//             type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
//             url: "/erp/getsupplierinfohint?subSupplierName=" //设置异步数据接口,url为必填项,params为字段名
//         });
//     });
//
//     $("#orderName").bind('blur change',function(){
//         var orderName = $("#orderName").val();
//         $("#colorName").empty();
//         $.ajax({
//             url: "/erp/getcolorhint",
//             data: {"orderName": orderName},
//             success:function(data){
//                 $("#colorName").empty();
//                 if (data.colorList) {
//                     $("#colorName").empty();
//                     $("#colorName").append("<option value=''>选择颜色</option>");
//                     $.each(data.colorList, function(index,element){
//                         $("#colorName").append("<option value='"+element.colorName+"'>"+element.colorName+"</option>");
//                     })
//                 }
//             },
//             error:function(){
//             }
//         });
//     });
//
//     $("#clothesVersionNumber").bind('blur change',function(){
//         var orderName = $("#orderName").val();
//         $("#colorName").empty();
//         $.ajax({
//             url: "/erp/getcolorhint",
//             data: {"orderName": orderName},
//             success:function(data){
//                 $("#colorName").empty();
//                 if (data.colorList) {
//                     $("#colorName").empty();
//                     $("#colorName").append("<option value=''>选择颜色</option>");
//                     $.each(data.colorList, function(index,element){
//                         $("#colorName").append("<option value='"+element.colorName+"'>"+element.colorName+"</option>");
//                     })
//                 }
//             },
//             error:function(){
//             }
//         });
//     });
//
// });
//
//
// function search() {
//
//     var container = document.getElementById('addOrderExcel');
//     if(hot==undefined) {
//         hot = new Handsontable(container, {
//             // data: data,
//             rowHeaders: true,
//             colHeaders: true,
//             autoColumnSize: true,
//             dropdownMenu: true,
//             contextMenu: true,
//             // minRows: 20,
//             minCols: 12,
//             colWidths:[120,250,120,120,80,150,80,80,80,80,60],
//             language: 'zh-CN',
//             licenseKey: 'non-commercial-and-evaluation'
//         });
//     }
//     var clothesVersionNumber = $("#clothesVersionNumber").val();
//     var orderName = $("#orderName").val();
//     var searchType = $("#type").val();
//     var season = $("#season").val();
//     var materialName = $("#materialName").val();
//     var customerName = $("#customerName").val();
//     var supplier = $("#supplier").val();
//     var param = {};
//     param.searchType = searchType;
//     if (season != '' && season != null){
//         param.season = season;
//     }
//     if (supplier != '' && supplier != null){
//         param.supplier = supplier;
//     }
//     if (materialName != '' && materialName != null){
//         param.materialName = materialName;
//     }
//     if (customerName != '' && customerName != null){
//         param.customerName = customerName;
//     }
//     if (orderName != '' && orderName != null){
//         param.orderName = orderName;
//     }
//
//     if(orderName === '' && (customerName === '' || season === '')) {
//         swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">款号/品牌-季度不能同时为空！</span>",html: true});
//         return false;
//     }
//
//     var nowDate = dateFormat("YYYY-mm-dd HH:MM", new Date()).replace(" ", "").replace(":","").replace(/\-/g,'');
//     var orderDate = $("#orderDate").val();
//     var deliveryDate = $("#deliveryDate").val();
//     var partBName = $("#supplierFullName").val();
//     var partBAddress = $("#supplierAddress").val();
//     var supplierName = $("#supplierName").val();
//     if (supplierName != '' && supplierName != null){
//         param.supplier = supplierName;
//     }
//
//     $.ajax({
//         url: "/erp/addsupplierinfo",
//         type:'POST',
//         data: {
//             "supplierName": supplierName,
//             "supplierFullName": partBName,
//             "supplierAddress": partBAddress
//         },
//         success:function(data){
//         }, error:function(){
//         }
//     });
//
//     $.blockUI({ css: {
//             border: 'none',
//             padding: '15px',
//             backgroundColor: '#000',
//             '-webkit-border-radius': '10px',
//             '-moz-border-radius': '10px',
//             opacity: .5,
//             color: '#fff'
//         },
//         message: "<h3>稍等一下... 奋力搜索中</h3>"
//     });
//     if ($("#type").val() === "面料"){
//         $.ajax({
//             url: basePath+"erp/getordercheckbyinfo",
//             type:'GET',
//             data: param,
//             success: function (data) {
//                 $("#exportDiv").show();
//                 var hotData = [];
//                 var tableHtml = "";
//                 $("#entities").empty();
//                 if(data) {
//                     tableHtml = "<div class=\"col-md-12\" name='printTable'>\n" +
//                         "               <section class=\"panel panel-default\" style='page-break-after:always;'>\n" +
//                         "                   <header class=\"panel-heading font-bold\" style=\"text-align: center\">\n" +
//                         "                       <div style=\"font-size: 20px;font-weight: 700\">中山市德悦服饰有限公司</div><br>\n" +
//                         "                       <div style=\"font-size: 16px;font-weight: 700\">购销合同  合同编号: " + nowDate +"</div><br>\n" +
//                         "                   </header>\n" +
//                         "                   <table class=\"table table-bordered\" style='width: 100%'>\n" +
//                         "                       <tbody>";
//
//                     tableHtml += "<tr><td colspan='7' style='text-align: left; font-weight: 700; color: black'>甲方(需方):中山市德悦服饰有限公司</td><td colspan='7' style='text-align: left; font-weight: 700; color: black'>签约地点:广东省中山市</td></tr>";
//                     tableHtml += "<tr><td colspan='7' style='text-align: left; font-weight: 700; color: black'>乙方(供方):"+ partBName +"</td><td colspan='7' style='text-align: left; font-weight: 700; color: black'>签约时间:"+ orderDate +"</td></tr>";
//                     tableHtml += "<tr><td colspan='14' style='text-align: center; font-weight: 700; color: black'>甲乙双方协商一致,签订以下协议,双方共同遵守</td></tr>";
//                     tableHtml += "<tr><td colspan='14' style='text-align: left; font-weight: 700; color: black'>一、产品基本信息</td></tr>";
//                     tableHtml += "<tr><td>单号</td><td>款号</td><td>面料名称</td><td>面料号</td><td>布封</td><td>克重</td><td>部位</td><td>色组</td><td>单件用量</td><td>单位</td><td>件数</td><td>订购量</td><td>单价(含税)</td><td>金额</td></tr>";
//                     var pieceSum = 0;
//                     var moneySum = 0;
//
//                     if (data["manufactureFabricList"]){
//                         hotData.push([["总体对照"]]);
//                         var manufactureFabricList = data["manufactureFabricList"];
//                         var differenceHead = [["面料名称"],["供应商"],["订单颜色"],["面料颜色"],["面料用途"],["订布数量"],["单位"],["回布数量"],["差异"],["回布卷数"]];
//                         hotData.push(differenceHead);
//                         for (var i=0;i<manufactureFabricList.length;i++){
//                             tableHtml += "<tr><td>"+manufactureFabricList[i].clothesVersionNumber+"</td><td>"+manufactureFabricList[i].orderName+"</td><td>"+manufactureFabricList[i].fabricName+"</td><td>"+manufactureFabricList[i].fabricNumber+"</td><td>"+manufactureFabricList[i].fabricWidth+"</td><td>"+manufactureFabricList[i].fabricWeight+"</td><td>"+manufactureFabricList[i].partName+"</td><td>"+manufactureFabricList[i].colorName+"</td><td>"+manufactureFabricList[i].pieceUsage+"</td><td>"+manufactureFabricList[i].unit+"</td><td>"+manufactureFabricList[i].orderCount+"</td><td>"+manufactureFabricList[i].fabricActCount+"</td><td>"+toDecimal(manufactureFabricList[i].price)+"</td><td>"+toDecimal(manufactureFabricList[i].sumMoney)+"</td></tr>";
//                             pieceSum += manufactureFabricList[i].fabricActCount;
//                             moneySum += manufactureFabricList[i].sumMoney;
//                             // hotData.push(differenceRow);
//                         }
//                         // hotData.push([[],[],[],["小计"],[toDecimal(orderSum)],[],[toDecimal(actualSum)],[toDecimal(differenceSum)],[batchSum]]);
//                         hotData.push([]);
//
//                     }
//                     tableHtml += "<tr><td colspan='11' style='text-align:center'>小计：</td><td>"+toDecimal(pieceSum)+"</td><td></td><td>"+toDecimal(moneySum)+"</td></tr>";
//                     tableHtml += "<tr><td colspan='4' style='text-align:center'>交货日期：</td><td colspan='10' style='text-align:center'>"+ deliveryDate +"</td></tr>";
//                     tableHtml += "<tr><td colspan='4' style='text-align:center'>交货地址：</td><td colspan='10' style='text-align:center'>中山市沙溪镇康乐北路42号德悦服饰2楼辅料仓</td></tr>";
//                     tableHtml += "<tr><td colspan='2' rowspan='5' style='text-align:center'>备注</td><td colspan='12'>1.质量要求:质量要求：用料要环保，不能含致癌物质，品质跟样办。</td></tr>";
//                     tableHtml += "<tr><td colspan='12'>2.包装要求：请务必在每袋或每捆上注明款号、规格、数量。</td></tr>";
//                     tableHtml += "<tr><td colspan='12'>3.送货单：需注明采购单号及款号并注明产品名称、颜色、规格、数量。</td></tr>";
//                     tableHtml += "<tr><td colspan='12'>4.供应商收到订单应第一时间回复本司货期，否则作默认同意，供应商必须保证产品的质量和货期，若产品质量不对办，我司将要求无偿更换或退货处理，如不能如期交货，请提前回复我司。</td></tr>";
//                     tableHtml += "<tr><td colspan='12'>5.本合同需签字回传订单方可生效。。</td></tr>";
//                     tableHtml += "<tr><td colspan='14' style='text-align: left; font-weight: 700; color: black'>二、其他条款</td></tr>";
//                     tableHtml += "<tr><td colspan='14' style='text-align: left;'>1、乙方应严格按照《针织T恤衫》国家（一等品）标准，以及甲方订单要求进行加工生产，乙方交至甲方的产品，必须符合甲方的要求。</td></tr>";
//                     tableHtml += "<tr><td colspan='14' style='text-align: left;'>2、乙方应于订单规定的时间内把甲方采购的（物料/布料）交至甲方指定地点，乙方承担运费及运输过程中的一切风险和法律责任。</td></tr>";
//                     tableHtml += "<tr><td colspan='14' style='text-align: left;'>3、甲方对货物的验收仅是形式检验，其验收不及于物品的内在质量，乙方仍应对物品质量承担保证责任。如在生产或销售过程中，发现内在质量不合格，造成退货或其他损失，由乙方负全责。</td></tr>";
//                     tableHtml += "<tr><td colspan='14' style='text-align: left;'>4、如乙方货物出现短缺、规格不符或质量问题等不符合合同约定情形的，甲方有权拒收并视为未交货，乙方应负责于规定时间内，免费更换合格的产品并运送至甲方，如出现特殊情况，需双方协商一致后再另行处理。</td></tr>";
//                     tableHtml += "<tr><td colspan='14' style='text-align: left;'>5、乙方因生产进度、质量问题等导致交货延期或双方另行协商的货期内仍不能完成的,视为乙方违约,乙方应承担逾期交货违约责任，并按逾期交货金额1% 每日向甲方支付违约金，因此给甲方造成经济损失的，乙方还应予以赔偿。</td></tr>";
//                     tableHtml += "<tr><td colspan='14' style='text-align: left;'>6、结算方式：月结 _____ 天。（乙方每月需将采购货物签收单原件交付甲方，如乙方代发货至第三方的，同样需将签收单原件交付甲方。）付款期限内，乙方开具13%的增值税发票或相关票据后，甲方付给乙方货款。</td></tr>";
//                     tableHtml += "<tr><td colspan='14' style='text-align: left;'>7、在合同履行过程中，债权及债务不可转让给第三方。</td></tr>";
//                     tableHtml += "<tr><td colspan='14' style='text-align: left;'>8、本合同未尽事宜，或在合同执行过程中出现的争执和异议，双方本着友好协商的态度共同解决，协商不成的，任何一方可向甲方所在地人民法院起诉。</td></tr>";
//                     tableHtml += "<tr><td colspan='14' style='text-align: left;'>9、本合同一式两份，甲乙双方各执一份，具有同等法律效力。</td></tr>";
//                     tableHtml += "<tr><td colspan='7' style='text-align: left; font-weight: 700; color: black'>甲方:中山市徳悦服饰有限公司</td><td colspan='7' style='text-align: left; font-weight: 700; color: black'>乙方:"+ partBName +"</td></tr>";
//                     tableHtml += "<tr><td colspan='7' style='text-align: left; font-weight: 700; color: black'>单位地址:中山市沙溪镇康乐北路42号之1二楼</td><td colspan='7' style='text-align: left; font-weight: 700; color: black'>单位地址:"+ partBAddress +"</td></tr>";
//                     tableHtml += "<tr><td colspan='7' style='text-align: left; font-weight: 700; color: black'>授权代表:</td><td colspan='7' style='text-align: left; font-weight: 700; color: black'>授权代表:</td></tr>";
//                     tableHtml += "<tr><td colspan='7' style='text-align: left; font-weight: 700; color: black'>电话:</td><td colspan='7' style='text-align: left; font-weight: 700; color: black'>电话:</td></tr>";
//                     tableHtml +=  "                       </tbody>" +
//                         "                   </table>" +
//                         "               </section>" +
//                         "              </div>";
//                     $("#entities").append(tableHtml);
//                 }
//                 // hot.loadData(hotData);
//                 $.unblockUI();
//             }, error: function () {
//                 swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
//             }
//         })
//     } else if ($("#type").val() === "辅料"){
//         $.ajax({
//             url: basePath+"erp/getordercheckbyinfo",
//             type:'GET',
//             data: param,
//             success: function (data) {
//                 $("#exportDiv").show();
//                 var hotData = [];
//                 var tableHtml = "";
//                 $("#entities").empty();
//                 if(data) {
//                     tableHtml = "<div class=\"col-md-12\" name='printTable'>\n" +
//                         "               <section class=\"panel panel-default\" style='page-break-after:always;'>\n" +
//                         "                   <header class=\"panel-heading font-bold\" style=\"text-align: center\">\n" +
//                         "                       <div style=\"font-size: 20px;font-weight: 700\">中山市德悦服饰有限公司</div><br>\n" +
//                         "                       <div style=\"font-size: 16px;font-weight: 700\">购销合同  合同编号: " + nowDate +"</div><br>\n" +
//                         "                   </header>\n" +
//                         "                   <table class=\"table table-bordered\" style='width: 100%'>\n" +
//                         "                       <tbody>";
//
//                     tableHtml += "<tr><td colspan='5' style='text-align: left; font-weight: 700; color: black'>甲方(需方):中山市德悦服饰有限公司</td><td colspan='6' style='text-align: left; font-weight: 700; color: black'>签约地点:广东省中山市</td></tr>";
//                     tableHtml += "<tr><td colspan='5' style='text-align: left; font-weight: 700; color: black'>乙方(供方):"+ partBName +"</td><td colspan='6' style='text-align: left; font-weight: 700; color: black'>签约时间:"+ orderDate +"</td></tr>";
//                     tableHtml += "<tr><td colspan='10' style='text-align: center; font-weight: 700; color: black'>甲乙双方协商一致,签订以下协议,双方共同遵守</td></tr>";
//                     tableHtml += "<tr><td colspan='10' style='text-align: left; font-weight: 700; color: black'>一、产品基本信息</td></tr>";
//                     tableHtml += "<tr><td width='12%'>款号</td><td width='20%'>名称</td><td width='11%'>编号</td><td width='10%'>规格</td><td width='10%'>颜色</td><td width='8%'>尺码</td><td width='5%'>单位</td><td width='8%'>数量</td><td width='8%'>单价(含税)</td><td width='8%'>金额</td></tr>";
//                     var pieceSum = 0;
//                     var moneySum = 0;
//
//                     if (data["manufactureAccessoryList"]){
//                         hotData.push([["总体对照"]]);
//                         var manufactureAccessoryList = data["manufactureAccessoryList"];
//                         var differenceHead = [["面料名称"],["供应商"],["订单颜色"],["面料颜色"],["面料用途"],["订布数量"],["单位"],["回布数量"],["差异"],["回布卷数"]];
//                         hotData.push(differenceHead);
//                         for (var i=0;i<manufactureAccessoryList.length;i++){
//                             tableHtml += "<tr><td>"+manufactureAccessoryList[i].orderName+"</td><td>"+manufactureAccessoryList[i].accessoryName+"</td>";
//                             if (manufactureAccessoryList[i].accessoryNumber && manufactureAccessoryList[i].accessoryNumber != "null"){
//                                 tableHtml += "<td>"+manufactureAccessoryList[i].accessoryNumber+"</td>";
//                             } else {
//                                 tableHtml += "<td></td>";
//                             }
//                             if (manufactureAccessoryList[i].specification && manufactureAccessoryList[i].specification != "null"){
//                                 tableHtml += "<td>"+manufactureAccessoryList[i].specification+"</td>";
//                             }else {
//                                 tableHtml += "<td></td>";
//                             }
//                             if (manufactureAccessoryList[i].accessoryColor && manufactureAccessoryList[i].accessoryColor != "null"){
//                                 tableHtml += "<td>"+manufactureAccessoryList[i].accessoryColor+"</td>";
//                             }else {
//                                 tableHtml += "<td></td>";
//                             }
//                             if (manufactureAccessoryList[i].sizeName && manufactureAccessoryList[i].sizeName != "null"){
//                                 tableHtml += "<td>"+manufactureAccessoryList[i].sizeName+"</td>";
//                             }else {
//                                 tableHtml += "<td></td>";
//                             }
//                             if (manufactureAccessoryList[i].accessoryUnit && manufactureAccessoryList[i].accessoryUnit != "null"){
//                                 tableHtml += "<td>"+manufactureAccessoryList[i].accessoryUnit+"</td>";
//                             }else {
//                                 tableHtml += "<td></td>";
//                             }
//                             if (manufactureAccessoryList[i].accessoryCount && manufactureAccessoryList[i].accessoryCount != "null"){
//                                 tableHtml += "<td>"+manufactureAccessoryList[i].accessoryCount+"</td>";
//                             }else {
//                                 tableHtml += "<td></td>";
//                             }
//                             tableHtml += "<td>"+toDecimal(manufactureAccessoryList[i].price)+"</td><td>"+toDecimal(manufactureAccessoryList[i].sumMoney)+"</td></tr>";
//                             pieceSum += manufactureAccessoryList[i].accessoryCount;
//                             moneySum += manufactureAccessoryList[i].sumMoney;
//                             // hotData.push(differenceRow);
//                         }
//                         // hotData.push([[],[],[],["小计"],[toDecimal(orderSum)],[],[toDecimal(actualSum)],[toDecimal(differenceSum)],[batchSum]]);
//                         hotData.push([]);
//
//                     }
//                     tableHtml += "<tr><td colspan='7' style='text-align:center'>小计：</td><td>"+toDecimal(pieceSum)+"</td><td></td><td>"+toDecimal(moneySum)+"</td></tr>";
//                     tableHtml += "<tr><td colspan='3' style='text-align:center'>交货日期：</td><td colspan='7' style='text-align:center'>"+ deliveryDate +"</td></tr>";
//                     tableHtml += "<tr><td colspan='3' style='text-align:center'>交货地址：</td><td colspan='7' style='text-align:center'>中山市沙溪镇康乐北路42号德悦服饰2楼辅料仓</td></tr>";
//                     tableHtml += "<tr><td colspan='2' rowspan='5' style='text-align:center'>备注</td><td colspan='8'>1.质量要求:质量要求：用料要环保，不能含致癌物质，品质跟样办。</td></tr>";
//                     tableHtml += "<tr><td colspan='8'>2.包装要求：请务必在每袋或每捆上注明款号、规格、数量。</td></tr>";
//                     tableHtml += "<tr><td colspan='8'>3.送货单：需注明采购单号及款号并注明产品名称、颜色、规格、数量。</td></tr>";
//                     tableHtml += "<tr><td colspan='8'>4.供应商收到订单应第一时间回复本司货期，否则作默认同意，供应商必须保证产品的质量和货期，若产品质量不对办，我司将要求无偿更换或退货处理，如不能如期交货，请提前回复我司。</td></tr>";
//                     tableHtml += "<tr><td colspan='8'>5.本合同需签字回传订单方可生效。。</td></tr>";
//                     tableHtml += "<tr><td colspan='10' style='text-align: left; font-weight: 700; color: black'>二、其他条款</td></tr>";
//                     tableHtml += "<tr><td colspan='10' style='text-align: left;'>1、乙方应严格按照《针织T恤衫》国家（一等品）标准，以及甲方订单要求进行加工生产，乙方交至甲方的产品，必须符合甲方的要求。</td></tr>";
//                     tableHtml += "<tr><td colspan='10' style='text-align: left;'>2、乙方应于订单规定的时间内把甲方采购的（物料/布料）交至甲方指定地点，乙方承担运费及运输过程中的一切风险和法律责任。</td></tr>";
//                     tableHtml += "<tr><td colspan='10' style='text-align: left;'>3、甲方对货物的验收仅是形式检验，其验收不及于物品的内在质量，乙方仍应对物品质量承担保证责任。如在生产或销售过程中，发现内在质量不合格，造成退货或其他损失，由乙方负全责。</td></tr>";
//                     tableHtml += "<tr><td colspan='10' style='text-align: left;'>4、如乙方货物出现短缺、规格不符或质量问题等不符合合同约定情形的，甲方有权拒收并视为未交货，乙方应负责于规定时间内，免费更换合格的产品并运送至甲方，如出现特殊情况，需双方协商一致后再另行处理。</td></tr>";
//                     tableHtml += "<tr><td colspan='10' style='text-align: left;'>5、乙方因生产进度、质量问题等导致交货延期或双方另行协商的货期内仍不能完成的,视为乙方违约,乙方应承担逾期交货违约责任，并按逾期交货金额1% 每日向甲方支付违约金，因此给甲方造成经济损失的，乙方还应予以赔偿。</td></tr>";
//                     tableHtml += "<tr><td colspan='10' style='text-align: left;'>6、结算方式：月结 _____ 天。（乙方每月需将采购货物签收单原件交付甲方，如乙方代发货至第三方的，同样需将签收单原件交付甲方。）付款期限内，乙方开具13%的增值税发票或相关票据后，甲方付给乙方货款。</td></tr>";
//                     tableHtml += "<tr><td colspan='10' style='text-align: left;'>7、在合同履行过程中，债权及债务不可转让给第三方。</td></tr>";
//                     tableHtml += "<tr><td colspan='10' style='text-align: left;'>8、本合同未尽事宜，或在合同执行过程中出现的争执和异议，双方本着友好协商的态度共同解决，协商不成的，任何一方可向甲方所在地人民法院起诉。</td></tr>";
//                     tableHtml += "<tr><td colspan='10' style='text-align: left;'>9、本合同一式两份，甲乙双方各执一份，具有同等法律效力。</td></tr>";
//                     tableHtml += "<tr><td colspan='5' style='text-align: left; font-weight: 700; color: black'>甲方:中山市徳悦服饰有限公司</td><td colspan='5' style='text-align: left; font-weight: 700; color: black'>乙方:"+ partBName +"</td></tr>";
//                     tableHtml += "<tr><td colspan='5' style='text-align: left; font-weight: 700; color: black'>单位地址:中山市沙溪镇康乐北路42号之1二楼</td><td colspan='5' style='text-align: left; font-weight: 700; color: black'>单位地址:"+ partBAddress +"</td></tr>";
//                     tableHtml += "<tr><td colspan='5' style='text-align: left; font-weight: 700; color: black'>授权代表:</td><td colspan='5' style='text-align: left; font-weight: 700; color: black'>授权代表:</td></tr>";
//                     tableHtml += "<tr><td colspan='5' style='text-align: left; font-weight: 700; color: black'>电话:</td><td colspan='5' style='text-align: left; font-weight: 700; color: black'>电话:</td></tr>";
//                     tableHtml +=  "                       </tbody>" +
//                         "                   </table>" +
//                         "               </section>" +
//                         "              </div>";
//                     $("#entities").append(tableHtml);
//                 }
//                 // hot.loadData(hotData);
//                 $.unblockUI();
//             }, error: function () {
//                 swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
//             }
//         })
//     }
// }
//
// function exportData() {
//     var orderName = $("#styleNumber").val();
//     var versionNumber = $("#clothesVersionNumber").val();
//     var myDate = new Date();
//     var title = orderName+'-'+versionNumber+'-面料详情-'+myDate.toLocaleString();
//     var data = utl.Object.copyJson(hot.getData());//<=====读取handsontable的数据
//     var result = [];
//     var firstRow = new Array(data[0].length);
//     firstRow[0] = title;
//     for(var i=1;i<data[0].length;i++) {
//         firstRow[i] = "";
//     }
//     result.push(firstRow);
//     var col = 0;
//     $.each(data,function (index, item) {
//         for(var i=0;i<item.length;i++) {
//             if(item[i]==null) {
//                 item[i] = '';
//             }else if(index==1) {
//                 col++;
//             }
//         }
//         result.push(item);
//     });
//     utl.XLSX.onExport(result,"Sheet1","xlsx",title+".xlsx");//<====导出
// }
//
// function toDecimal(x) {
//     var f = parseFloat(x);
//     if (isNaN(f)) {
//         return;
//     }
//     f = Math.round(x*100)/100;
//     return f;
// }
//
// function printDeal(){
//     $("#printf").empty();
//     var printBoxs = document.getElementsByName('printTable');
//     var newContent = '';
//     for(var i=0;i<printBoxs.length;i++) {
//         newContent += printBoxs[i].innerHTML;
//     }
//     var f = document.getElementById('printf');
//     // f.contentDocument.write('<link rel="stylesheet" type="text/css" href="/css/app.v2.css">');
//     f.contentDocument.write('<style>' +
//         'table {' +
//         'border-collapse:collapse;' +
//         '}' +
//         'td {\n' +
//         '        border: 1px solid #000000;\n' +
//         '    }'+
//         '</style>');
//     f.contentDocument.write(newContent);
//     f.contentDocument.close();
//     window.frames['printf'].focus();
//     try{
//         window.frames['printf'].print();
//     }catch(err){
//         f.contentWindow.print();
//     }
// }
//
// function renderSeasonDate(ohd, sgl) {
//     var ele = $(ohd);
//     $Date.render({
//         elem: ohd,
//         type: 'month',
//         format: 'yyyy年M季度',
//         range: sgl ? null : '~',
//         min: "1900-1-1",
//         max: "2099-12-31",
//         btns: ['clear', 'confirm'],
//         ready: function (value, date, endDate) {
//             var hd = $("#layui-laydate" + ele.attr("lay-key"));
//             if (hd.length > 0) {
//                 hd.click(function () {
//                     ren($(this));
//                 });
//             }
//             ren(hd);
//         },
//         done: function (value, date, endDate) {
//             if (!isNull(date) && date.month > 0 && date.month < 5) {
//                 ele.attr("startDate", date.year + "-" + date.month);
//             } else {
//                 ele.attr("startDate", "");
//             }
//             if (!isNull(endDate) && endDate.month > 0 && endDate.month < 5) {
//                 ele.attr("endDate", endDate.year + "-" + endDate.month)
//             } else {
//                 ele.attr("endDate", "");
//             }
//         }
//     });
//     var ren = function (thiz) {
//         var mls = thiz.find(".laydate-month-list");
//         mls.each(function (i, e) {
//             $(this).find("li").each(function (inx, ele) {
//                 var cx = ele.innerHTML;
//                 if (inx < 4) {
//                     ele.innerHTML = cx.replace(/月/g, "季度");
//                 } else {
//                     ele.style.display = "none";
//                 }
//             });
//         });
//     }
// }
// function initDateForm(sgl, form) {
//     if (isNull(form)) form = $(document.body);
//     var ltm = function (tar, tars, tva) {
//         tars.each(function () {
//             $(this).removeAttr("lay-key");
//             this.outerHTML = this.outerHTML;
//         });
//         tars = form.find(".dateTarget" + tar);
//         tars.each(function () {
//             var ele = $(this);
//             if ("y" == tva) {
//                 $Date.render({
//                     elem: this,
//                     type: 'year',
//                     range: '~',
//                     format: 'yyyy年',
//                     range: sgl ? null : '~',
//                     done: function (value, date, endDate) {
//                         if (typeof (date.year) == "number") ele.attr("startDate", date.year);
//                         else ele.attr("startDate", "");
//                         if (typeof (endDate.year) == "number") ele.attr("endDate", endDate.year);
//                         else ele.attr("endDate", "");
//                     }
//                 });
//
//             } else if ("s" == tva) {
//                 ele.attr("startDate", "");
//                 ele.attr("endDate", "");
//                 renderSeasonDate(this, sgl);
//             } else if ("m" == tva) {
//                 ele.attr("startDate", "");
//                 ele.attr("endDate", "");
//                 $Date.render({
//                     elem: this,
//                     type: 'month',
//                     range: '~',
//                     format: 'yyyy年MM月',
//                     range: sgl ? null : '~',
//                     done: function (value, date, endDate) {
//                         if (typeof (date.month) == "number") ele.attr("startDate", date.year + "-" + date.month);
//                         else ele.attr("startDate", "");
//                         if (typeof (endDate.month) == "number") ele.attr("endDate", endDate.year + "-" + endDate.month);
//                         else ele.attr("endDate", "");
//                     }
//                 });
//             } else if ("d" == tva) {
//                 ele.attr("startDate", "");
//                 ele.attr("endDate", "");
//                 $Date.render({
//                     elem: this,
//                     range: '~',
//                     format: 'yyyy年MM月dd日',
//                     range: sgl ? null : '~',
//                     done: function (value, date, endDate) {
//                         if (typeof (date.date) == "number") ele.attr("startDate", date.year + "-" + date.month + "-" + date.date);
//                         else ele.attr("startDate", "");
//                         if (typeof (endDate.date) == "number") ele.attr("endDate", endDate.year + "-" + endDate.month + "-" +
//                             endDate.date);
//                         else ele.attr("endDate", "");
//                     }
//                 });
//             }
//         });
//     }
//     var sels = form.find(".dateSelector");
//     sels.each(function (i, e) {
//         var ths = this;
//         var thiz = $(e);
//         var tar = thiz.attr("date-target");
//         thiz.next().find("dd").click(function () {
//             var tva = thiz.val();
//             var tars = form.find(".dateTarget" + tar);
//             ltm(tar, tars, tva);
//         });
//         thiz.change(function () {
//             var tva = $(this).val();
//             var tars = form.find(".dateTarget" + tar);
//             ltm(tar, tars, tva);
//         });
//         var tars = form.find(".dateTarget" + tar);
//         ltm(tar, tars, thiz.val());
//     });
// }
//
// function isNull(s) {
//     if (s == null || typeof (s) == "undefined" || s == "") return true;
//     return false;
// }
//
//
// function dateFormat(fmt, date) {
//     let ret;
//     const opt = {
//         "Y+": date.getFullYear().toString(),        // 年
//         "m+": (date.getMonth() + 1).toString(),     // 月
//         "d+": date.getDate().toString(),            // 日
//         "H+": date.getHours().toString(),           // 时
//         "M+": date.getMinutes().toString(),         // 分
//         "S+": date.getSeconds().toString()          // 秒
//         // 有其他格式化字符需求可以继续添加，必须转化成字符串
//     };
//     for (let k in opt) {
//         ret = new RegExp("(" + k + ")").exec(fmt);
//         if (ret) {
//             fmt = fmt.replace(ret[1], (ret[1].length == 1) ? (opt[k]) : (opt[k].padStart(ret[1].length, "0")))
//         };
//     };
//     return fmt;
// }
//
// function getFormatDate() {
//     var date = new Date();
//     var month = date.getMonth() + 1;
//     var strDate = date.getDate();
//     if (month >= 1 && month <= 9) {
//         month = "0" + month;
//     }
//     if (strDate >= 0 && strDate <= 9) {
//         strDate = "0" + strDate;
//     }
//     var currentDate = date.getFullYear() + "-" + month + "-" + strDate;
//     return currentDate;
// }