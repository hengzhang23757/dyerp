var hot;
var basePath=$("#basePath").val();

$(document).ready(function () {
    layui.use(['autocomplete'], function () {
        layui.autocomplete.render({
            elem: $('#clothesVersionNumber')[0],
            url: 'getversionnumberbysubversion',
            template_val: '{{d}}',
            template_txt: '{{d}}',
            onselect: function (resp) {
                autoComplete(resp)
            }
        })
    });
});

function autoComplete(keywords) {
    $("#styleNumber").empty();
    $.ajax({
        url: "/erp/getstylenumbersbyversion",
        data: {"versionNumber": keywords},
        success:function(data){
            $("#styleNumber").empty();
            if (data.styleNumberList) {
                $("#styleNumber").append("<option value=''>订单号</option>");
                $.each(data.styleNumberList, function(index,element){
                    $("#styleNumber").append("<option value="+element+">"+element+"</option>");
                })
            }
        },
        error:function(){
        }
    });
}

function search() {
    var container = document.getElementById('addOrderExcel');
    if(hot==undefined) {
        hot = new Handsontable(container, {
            // data: data,
            rowHeaders: true,
            colHeaders: true,
            autoColumnSize: true,
            dropdownMenu: true,
            contextMenu: true,
            // minRows: 20,
            minCols: 12,
            colWidths:[100,150,120,120,80,80,80,80,80,80,60,60],
            language: 'zh-CN',
            licenseKey: 'non-commercial-and-evaluation'
        });
    }

    var versionNumber = $("#clothesVersionNumber").val();
    var styleNumber = $("#styleNumber").val();
    if($("#styleNumber").val().trim()=="") {
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入款号！</span>",html: true});
        return false;
    }
    $.ajax({
        url: basePath+"erp/getfabircdetail",
        type:'GET',
        data: {
            styleNumber:$("#styleNumber").val(),
        },
        success: function (data) {
            $("#exportDiv").show();
            var hotData = [];
            if(data) {
                var versionStyle = [["版单号"],[versionNumber],["款号"],[styleNumber]];
                hotData.push(versionStyle);
                if (data["difference"]){
                    hotData.push([["回布汇总"]]);
                    var differenceData = data["difference"];
                    var differenceHead = [["季度"],["面料名称"],["面料编号"],["面料用途"],["面料颜色"],["面料色号"],["订布数量"],["回布数量"],["差异"],["回布卷数"]];
                    hotData.push(differenceHead);
                    var orderSum = 0;
                    var actualSum = 0;
                    var differenceSum = 0;
                    var batchSum = 0;
                    for (var i=0;i<differenceData.length;i++){
                        var differenceRow = [];
                        differenceRow.push(differenceData[i].season);
                        differenceRow.push(differenceData[i].fabricName);
                        differenceRow.push(differenceData[i].fabricNumber);
                        differenceRow.push(differenceData[i].fabricUse);
                        differenceRow.push(differenceData[i].fabricColor);
                        differenceRow.push(differenceData[i].fabricColorNumber);
                        differenceRow.push(toDecimal(differenceData[i].orderCount));
                        differenceRow.push(toDecimal(differenceData[i].actualCount));
                        differenceRow.push(toDecimal(differenceData[i].differenceCount));
                        differenceRow.push(differenceData[i].batchNumber);
                        orderSum += differenceData[i].orderCount;
                        actualSum += differenceData[i].actualCount;
                        differenceSum += differenceData[i].differenceCount;
                        batchSum += differenceData[i].batchNumber;
                        hotData.push(differenceRow);
                    }
                    hotData.push([[],[],[],[],[],["小计"],[toDecimal(orderSum)],[toDecimal(actualSum)],[toDecimal(differenceSum)],[batchSum]]);
                    hotData.push([]);
                }
                if (data["out"]){
                    hotData.push([["出库汇总"]]);
                    var outData = data["out"];
                    var outHead=[["出库日期"],["位置"],["面料编号"],["面料颜色"],["面料色号"],["出库数量"],["出库卷数"],["缸号"],["出库类型"],["备注"]];
                    hotData.push(outHead);
                    var outCountSum = 0;
                    var outBatchSum = 0;
                    for (var j=0;j<outData.length;j++){
                        var outRow = [];
                        outRow.push(moment(outData[j].createTime).format("YYYY-MM-DD"));
                        outRow.push(outData[j].location);
                        outRow.push(outData[j].fabricNumber);
                        outRow.push(outData[j].colorName);
                        outRow.push(outData[j].colorNumber);
                        outRow.push(toDecimal(outData[j].fabricCount));
                        outRow.push(outData[j].batchNumber);
                        outRow.push(outData[j].jarNumber);
                        outRow.push(outData[j].operateType);
                        outRow.push(outData[j].remark);
                        outCountSum += outData[j].fabricCount;
                        outBatchSum += outData[j].batchNumber;
                        hotData.push(outRow);
                    }
                    hotData.push([[],[],[],[],["小计"],[toDecimal(outCountSum)],[outBatchSum]]);
                    hotData.push([]);
                }
                if (data["storage"]){
                    hotData.push([["库存汇总"]]);
                    var storageData = data["storage"];
                    var storageHead=[["面料编号"],["面料颜色"],["面料色号"],["剩余数量"],["剩余卷数"],["缸号"],["位置"]];
                    hotData.push(storageHead);
                    var storageCountSum = 0;
                    var storageBatchSum = 0;
                    for (var k=0;k<storageData.length;k++){
                        var storageRow = [];
                        storageRow.push(storageData[k].fabricNumber);
                        storageRow.push(storageData[k].colorName);
                        storageRow.push(storageData[k].colorNumber);
                        storageRow.push(toDecimal(storageData[k].fabricCount));
                        storageRow.push(storageData[k].batchNumber);
                        storageRow.push(storageData[k].jarNumber);
                        storageRow.push(storageData[k].location);
                        storageCountSum += storageData[k].fabricCount;
                        storageBatchSum += storageData[k].batchNumber;
                        hotData.push(storageRow);
                    }
                    hotData.push([[],[],["小计"],[toDecimal(storageCountSum)],[storageBatchSum]]);
                    hotData.push([]);
                }

            }
            hot.loadData(hotData);
        },
        error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }
    })
}

function exportData() {
    var orderName = $("#styleNumber").val();
    var versionNumber = $("#clothesVersionNumber").val();
    var myDate = new Date();
    var title = orderName+'-'+versionNumber+'-面料详情-'+myDate.toLocaleString();
    var data = utl.Object.copyJson(hot.getData());//<=====读取handsontable的数据
    var result = [];
    var firstRow = new Array(data[0].length);
    firstRow[0] = title;
    for(var i=1;i<data[0].length;i++) {
        firstRow[i] = "";
    }
    result.push(firstRow);
    var col = 0;
    $.each(data,function (index, item) {
        for(var i=0;i<item.length;i++) {
            if(item[i]==null) {
                item[i] = '';
            }else if(index==1) {
                col++;
            }
        }
        result.push(item);
    });
    utl.XLSX.onExport(result,"Sheet1","xlsx",title+".xlsx");//<====导出
}

function toDecimal(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*100)/100;
    return f;
}