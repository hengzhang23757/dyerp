var userName = $("#userName").val();
var userRole=$("#userRole").val();
$(document).ready(function () {
    layui.use(['autocomplete'], function () {
        layui.autocomplete.render({
            elem: $('#clothesVersionNumber')[0],
            url: 'getversionhint',
            template_val: '{{d}}',
            template_txt: '{{d}}',
            onselect: function (resp) {
                autoComplete(resp)
            }
        })
    });

    $('#clothesVersionNumber').keyup(function() {
        $("#fabricName").empty();
        $("#fabricColor").empty();
        $("#jarName").empty();
        $("#weight").val("");
        $("#batch").val("");
    });

    $("#orderName").change(function () {
        var orderName = $(this).val();
        $("#fabricName").empty();
        $("#fabricColor").empty();
        $("#jarName").empty();
        $("#weight").val("");
        $("#batch").val("");
        $.ajax({
            url: "/erp/getfabricnamehint",
            data: {"orderName": orderName},
            success:function(data){
                $("#fabricName").empty();
                if (data.fabricNameList) {
                    $("#fabricName").append("<option value=''>面料名</option>");
                    $.each(data.fabricNameList, function(index,element){
                        $("#fabricName").append("<option value="+element+">"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });
    });



    $("#fabricName").change(function () {
        var fabricName = $(this).val();
        var orderName = $("#orderName").val();
        $("#fabricColor").empty();
        $("#jarName").empty();
        $("#weight").val("");
        $("#batch").val("");
        $.ajax({
            url: "/erp/getfabriccolorbystylefabricname",
            data: {
                "orderName": orderName,
                "fabricName": fabricName
            },
            success:function(data){
                $("#fabricColor").empty();
                if (data.fabricColorList) {
                    $("#fabricColor").append("<option value=''>颜色</option>");
                    $.each(data.fabricColorList, function(index,element){
                        $("#fabricColor").append("<option value="+element+">"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });
    });

    $("#fabricColor").change(function () {
        var fabricColor = $(this).val();
        var fabricName = $("#fabricName").val();
        var orderName = $("#orderName").val();
        $("#jarName").empty();
        $("#weight").val("");
        $("#batch").val("");
        $.ajax({
            url: "/erp/getjarnumberbystylefabric",
            data: {
                "orderName": orderName,
                "fabricName": fabricName,
                "fabricColor": fabricColor
            },
            success:function(data){
                $("#jarName").empty();
                if (data.jarNumberList) {
                    $("#jarName").append("<option value=''>缸号</option>");
                    $.each(data.jarNumberList, function(index,element){
                        $("#jarName").append("<option value="+element+">"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });
    });

    $("#operateType").change(function () {
        var operateType = $("#operateType").val();
        $("#fromLocation").hide();
        if (operateType === "余布整合"){
            $("#fromLocation").show();
        }
    })

});

function autoComplete(keywords) {
    $("#orderName").empty();
    $.ajax({
        url: "/erp/getorderbyversion",
        data: {"clothesVersionNumber": keywords},
        success:function(data){
            $("#orderName").empty();
            if (data.orderList) {
                $("#orderName").append("<option value=''>选择订单号</option>");
                $.each(data.orderList, function(index,element){
                    $("#orderName").append("<option value='"+element+"'>"+element+"</option>");
                })
            }
        },
        error:function(){
        }
    });
}

var basePath=$("#basePath").val();

function inStore() {
    if(userRole!='root' && userRole!='role11'){
        swal("SORRY!", "对不起，您没有操作权限！", "warning");
        return;
    }
    var flag = false;
    $(".panel-body :input").each(function (index,item) {
        if($(item).val()==null || $(item).val().trim()=="") {
            flag = true;
            return false;
        }
    });
    if(flag) {
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请填写完所有字段信息！</span>",html: true});
        return false;
    }
    var fabricStorage = {};
    fabricStorage.orderName = $("#orderName").val();
    fabricStorage.clothesVersionNumber = $("#clothesVersionNumber").val();
    fabricStorage.fabricName = $("#fabricName").val();
    fabricStorage.fabricColor = $("#fabricColor").val();
    fabricStorage.jarName = $("#jarName").val();
    fabricStorage.weight = $("#weight").val();
    fabricStorage.batchNumber = $("#batch").val();
    fabricStorage.operateType = $("#operateType").val();
    fabricStorage.returnTime = new Date();
    fabricStorage.location = $("#storageRow").val() + "-" + $("#storageSite").val() + "-" + $("#storageStand").val();
    swal({
        title: "",
        text: "<span style=\"font-weight:bolder;font-size: 20px\">您确定要将该面料入库吗？</span>",
        type: "warning",
        html:true,
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: "确定",
        cancelButtonText:"我再想想",
        confirmButtonColor: "#ec6c62",
        showLoaderOnConfirm: true
    }, function() {
        $.ajax({
            url: "/erp/fabricinstore",
            data: {
                fabricStorageJson:JSON.stringify(fabricStorage)
            },
            type:"POST",
            success: function (data) {
                if (data == 0) {
                    swal({
                        type: "success",
                        title: "",
                        text: "<span style=\"font-weight:bolder;font-size: 20px\">恭喜你，操作成功！</span>",
                        html: true
                    },
                    function(){
                        location.href=basePath+"erp/fabricInStoreStart";
                    });
                } else {
                    swal({
                        type: "error",
                        title: "",
                        text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，操作失败！</span>",
                        html: true
                    });
                }
            },
            error: function () {
                swal({
                    type: "error",
                    title: "",
                    text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",
                    html: true
                });
            }
        });
    });

}
