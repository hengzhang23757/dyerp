var hot;
var basePath=$("#basePath").val();
var userName=$("#userName").val();
$(document).ready(function () {
    var container = document.getElementById('addFabricExcel');
    hot = new Handsontable(container, {
        // data: data,
        rowHeaders: true,
        // colHeaders: true,
        colHeaders:['订单编号','版单号','大货款号','面料名称','布种','面料编号','面料颜色','面料色号','SKC颜色','面料用途','单件用量','供应商','成衣数量','面料数量','单价','总价','交货日期','有效门幅','克重','纬斜','扭度','执行标准','备注'],
        autoColumnSize:true,
        dropdownMenu: true,
        contextMenu:true,
        minRows:20,
        minCols:23,
        // colWidths:70,
        language:'zh-CN',
        licenseKey: 'non-commercial-and-evaluation'
    });
});

function add() {
    swal({
        title: "",
        text: "<span style=\"font-weight:bolder;font-size: 20px\">您确定要提交输入的面料信息吗？</span>",
        type: "warning",
        html:true,
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: "确定",
        cancelButtonText:"我再想想",
        confirmButtonColor: "#ec6c62",
        showLoaderOnConfirm: false
    }, function() {
        var fabricOrderJson = [];
        var data = hot.getData();
        $.each(data,function (index, item) {
            if(item[0]!=null && item[0]!="") {
                var fabricOrder = {};
                fabricOrder.fabricOrderName = item[0].trim();
                fabricOrder.clothesVersionNumber = item[1].trim();
                fabricOrder.orderName = item[2].trim();
                fabricOrder.fabricName = item[3].trim();
                fabricOrder.fabricTypeNumber = item[4].trim();
                fabricOrder.fabricNumber = item[5].trim();
                fabricOrder.fabricColor = item[6].trim();
                fabricOrder.fabricColorNumber = item[7].trim();
                fabricOrder.skcColor = item[8].trim();
                fabricOrder.fabricUse = item[9].trim();
                fabricOrder.singleCount = item[10].trim();
                fabricOrder.fabricSupplier = item[11].trim();
                fabricOrder.orderCount = item[12].trim();
                fabricOrder.fabricSumCount = item[13].trim();
                fabricOrder.price = item[14].trim();
                fabricOrder.total = item[15].trim();
                fabricOrder.deliveryDate = item[16].trim();
                fabricOrder.width = item[17].trim();
                fabricOrder.weight = item[18].trim();
                fabricOrder.filling = item[19].trim();
                fabricOrder.twist = item[20].trim();
                fabricOrder.standard = item[21].trim();
                fabricOrder.remark = item[22].trim();
                fabricOrder.stateType = 0;
                fabricOrderJson.push(fabricOrder);
            }
        });
        $.ajax({
            url: "/erp/addfabricorderbatch",
            type:'POST',
            data: {
                fabricOrderJson:JSON.stringify(fabricOrderJson),
                userName:userName
            },
            success: function (data) {
                if(data == 0) {
                    // swal({type:"success",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">恭喜你，录入成功！</span>",html: true});
                    swal({type:"success",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">恭喜你，录入成功！</span>",html: true},function (){
                        hot.clear();
                    });
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，录入失败！</span>",html: true});
                }
            },
            error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        })
    })
}