var basePath=$("#basePath").val();
var globalTailorList;

layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.21'
}).extend();

$(document).ready(function () {

    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumber", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });

    $("#orderName").bind('blur change',function(){
        var orderName = $("#orderName").val();
        $.ajax({
            url: "/erp/getdeletedbednumbers",
            data: {"orderName": orderName},
            success:function(data){
                $("#bedNumber").empty();
                if (data.bedNumberList) {
                    $("#bedNumberList").append("<option value=''>选择床号</option>");
                    $.each(data.bedNumberList, function(index,element){
                        $("#bedNumber").append("<option value="+element+">"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });
    });

    $("#clothesVersionNumber").bind('blur change',function(){
        var orderName = $("#orderName").val();
        $.ajax({
            url: "/erp/getdeletedbednumbers",
            data: {"orderName": orderName},
            success:function(data){
                $("#bedNumber").empty();
                if (data.bedNumberList) {
                    $("#bedNumberList").append("<option value=''>选择床号</option>");
                    $.each(data.bedNumberList, function(index,element){
                        $("#bedNumber").append("<option value="+element+">"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });
    });
});

function search() {

    if($("#orderName").val().trim()=="") {
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">款号必须！</span>",html: true});
        return false;
    }
    if($("#bedNumber").val().trim()=="") {
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">床号必须！</span>",html: true});
        return false;
    }
    $.ajax({
        url: basePath + "erp/getdeletedtailorbyinfo",
        type:'GET',
        data: {
            orderName:$("#orderName").val(),
            bedNumber:$("#bedNumber").val()
        },
        success: function (data) {
            if(data) {
                createTailorInfoTable(data.tailorList);
            }else {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，没有查到相关信息！</span>",html: true});
            }
        },
        error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }
    })

}

var tailorInfoTable;

function createTailorInfoTable(data) {
    if (tailorInfoTable != undefined) {
        tailorInfoTable.clear(); //清空一下table
        tailorInfoTable.destroy(); //还原初始化了的datatable
    }
    tailorInfoTable = $('#tailorInfoTable').DataTable({
        "data":data,
        language : {
            processing : "载入中",//处理页面数据的时候的显示
            paginate : {//分页的样式文本内容。
                previous : "上一页",
                next : "下一页",
                first : "第一页",
                last : "最后一页"
            },
            search:"搜索：",
            zeroRecords : "没有内容",//table tbody内容为空时，tbody的内容。
            //下面三者构成了总体的左下角的内容。
            info : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//左下角的信息显示，大写的词为关键字。
            infoEmpty : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//筛选为空时左下角的显示。
            infoFiltered : ""//筛选之后的左下角筛选提示(另一个是分页信息显示，在上面的info中已经设置，所以可以不显示)，
        },
        pageLength : 100,// 每页显示10条数据
        pagingType : "full_numbers", // 分页样式：simple,simple_numbers,full,full_numbers，
        "paging": true,
        "info": true,
        searching:false,
        ordering:true,
        lengthChange:false,
        scrollX: 2000,
        fixedHeader: true,
        scrollY: $(document.body).height() - 250,
        scrollCollapse: true,
        scroller:       true,
        "columns": [
            {
                "data": "tailorID",
                "title":"<input type=\"checkbox\" onclick=\"checkAll(this)\">",
                "width":"20px",
                "defaultContent": "",
                "sClass": "text-center",
                "orderable":false,
                render: function (data, type, row, meta) {
                    return "<input type='checkbox' value="+ data +">";
                }
            },
            {
                "data": null,
                "title":"序号",
                "width":"30px",
                "defaultContent": "",
                "sClass": "text-center",
                render: function (data, type, row, meta) {
                    var no = meta.settings._iDisplayStart + meta.row + 1;
                    return no;
                }
            },{
                "data": "orderName",
                "title":"订单号",
                "width":"120px",
                "defaultContent": "",
                "sClass": "text-center",
            },{
                "data": "clothesVersionNumber",
                "title":"版单号",
                "width":"120px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "customerName",
                "title":"客户",
                "width":"90px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "bedNumber",
                "title":"床号",
                "width":"60px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "jarName",
                "title":"缸号",
                "width":"120px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "colorName",
                "title":"颜色",
                "width":"90px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "sizeName",
                "title":"尺码",
                "width":"90px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "partName",
                "title":"部位",
                "width":"90px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "layerCount",
                "title":"数量",
                "width":"45px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "initCount",
                "title":"原始数量",
                "width":"70px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "packageNumber",
                "title":"扎号",
                "width":"60px",
                "defaultContent": "",
                "sClass": "text-center",
            }
        ]
    });
}


function checkAll(obj) {
    if($(obj).is(':checked')) {
        $("#tailorInfoTable tbody input[type='checkbox']").prop("checked",true);
    }else {
        $("#tailorInfoTable tbody input[type='checkbox']").prop("checked",false);
    }
}


function tailorRecovery() {
    var tailorIDList = [];
    $("#tailorInfoTable tbody input[type='checkbox']:checked").each(function () {
        tailorIDList.push($(this).val());
    });
    if(tailorIDList.length<1){
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请选择要恢复的数据！</span>",html: true});
    }else{
        $.ajax({
            url: basePath + "erp/deletedtailorrecovery",
            type:'POST',
            data: {
                orderName:$("#orderName").val(),
                bedNumber:$("#bedNumber").val(),
                tailorIDList:tailorIDList
            },
            traditional: true,
            success: function (data) {
                if(data.msg) {
                    swal({type:"warning",title:"",text: "<span style='font-weight:bolder;font-size: 20px'>" + data.msg + "</span>",html: true});
                }else if (data.res == 0) {
                    swal({type:"success",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">恢复成功！</span>",html: true});
                    search();
                }else if (data.res == 1) {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">恢复失败！</span>",html: true});
                }
            }, error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        })
    }
}

function PrefixZero(num, n) {
    return (Array(n).join(0) + num).slice(-n);
}