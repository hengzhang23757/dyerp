var basePath=$("#basePath").val();
var partNameArray = [];
var jarNameArray = [];
var orderName,clothesVersionNumber,bedNumber,customerName,groupName,batchAndWeight=[],globalTailorList;
layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.21'
}).extend();
$(document).ready(function () {
    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumber", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });

    $("#orderName").bind('blur change',function(){
        var orderName = $("#orderName").val();
        $.ajax({
            url: "/erp/getbednumbersbyordername",
            data: {"orderName": orderName},
            success:function(data){
                $("#bedNumber").empty();
                if (data.bedNumList) {
                    $("#bedNumber").append("<option value=''>选择床号</option>");
                    $.each(data.bedNumList, function(index,element){
                        $("#bedNumber").append("<option value="+element+">"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });
    });

    $("#clothesVersionNumber").bind('blur change',function(){
        var orderName = $("#orderName").val();
        $.ajax({
            url: "/erp/getbednumbersbyordername",
            data: {"orderName": orderName},
            success:function(data){
                $("#bedNumber").empty();
                if (data.bedNumList) {
                    $("#bedNumber").append("<option value=''>选择床号</option>");
                    $.each(data.bedNumList, function(index,element){
                        $("#bedNumber").append("<option value="+element+">"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });
    });
});
function search() {
    partNameArray = [];
    jarNameArray = [];
    if(!$("#orderName").val() || $("#orderName").val()=="") {
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入要查询的订单号！</span>",html: true});
        return false;
    }
    if(!$("#bedNumber").val() || $("#bedNumber").val()=="") {
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请选择床号！</span>",html: true});
        return false;
    }
    $("#addBtn").show();
    $.ajax({
        url: basePath + "erp/getalltailorbyorderbed",
        type:'GET',
        data: {
            orderName:$("#orderName").val(),
            bedNumber:$("#bedNumber").val(),
            packageNumber:"",
        },
        success: function (data) {
            if(data) {
                // var json = JSON.parse(data);
                globalTailorList = data.tailorList;
                createTailorInfoTable(data.tailorList);
                $.each(data.tailorList,function (index,item) {
                    if(partNameArray.indexOf(item.partName) == -1) {
                        partNameArray.push(item.partName);
                    }
                    if(jarNameArray.indexOf(item.jarName) == -1) {
                        jarNameArray.push(item.jarName);
                    }
                    if(item.batch==null) {
                        item.batch = 0 ;
                    }
                    if(item.weight == null) {
                        item.weight = 0;
                    }
                    if(batchAndWeight.indexOf(item.batch+"-"+item.weight) == -1) {
                        batchAndWeight.push(item.batch+"-"+item.weight)
                    }
                });
                clothesVersionNumber = $("#clothesVersionNumber").val();
                orderName = $("#orderName").val();
                bedNumber = $("#bedNumber").val();
                customerName = data.tailorList[0].customerName;
                groupName = data.tailorList[0].groupName;
            }else {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，没有查到相关信息！</span>",html: true});
            }
        },
        error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }
    })

}

var tailorInfoTable;
var maxQrCode;

function createTailorInfoTable(data) {
    if (tailorInfoTable != undefined) {
        tailorInfoTable.clear(); //清空一下table
        tailorInfoTable.destroy(); //还原初始化了的datatable
    }
    tailorInfoTable = $('#tailorInfoTable').DataTable({
        "data":data,
        language : {
            processing : "载入中",//处理页面数据的时候的显示
            paginate : {//分页的样式文本内容。
                previous : "上一页",
                next : "下一页",
                first : "第一页",
                last : "最后一页"
            },
            search:"搜索：",
            zeroRecords : "没有内容",//table tbody内容为空时，tbody的内容。
            //下面三者构成了总体的左下角的内容。
            info : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//左下角的信息显示，大写的词为关键字。
            infoEmpty : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//筛选为空时左下角的显示。
            infoFiltered : ""//筛选之后的左下角筛选提示(另一个是分页信息显示，在上面的info中已经设置，所以可以不显示)，
        },
        pageLength : 100,// 每页显示10条数据
        pagingType : "full_numbers", // 分页样式：simple,simple_numbers,full,full_numbers，
        "paging": true,
        "info": true,
        searching:true,
        ordering:true,
        lengthChange:false,
        scrollX: 1500,
        fixedHeader: true,
        scrollY: $(document.body).height() - 260,
        scrollCollapse: true,
        scroller:       true,
        "columns": [
            {
                "data": null,
                "title":"序号",
                "width":"30px",
                "defaultContent": "",
                "sClass": "text-center",
                render: function (data, type, row, meta) {
                    var no = meta.settings._iDisplayStart + meta.row + 1;
                    return no;
                }
            },{
                "data": "orderName",
                "title":"订单号",
                "width":"120px",
                "defaultContent": "",
                "sClass": "text-center",
            },{
                "data": "clothesVersionNumber",
                "title":"版单号",
                "width":"120px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "customerName",
                "title":"客户",
                "width":"60px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "bedNumber",
                "title":"床号",
                "width":"45px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "jarName",
                "title":"缸号",
                "width":"60px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "colorName",
                "title":"颜色",
                "width":"60px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "sizeName",
                "title":"尺码",
                "width":"45px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "partName",
                "title":"部位",
                "width":"80px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "layerCount",
                "title":"数量",
                "width":"45px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "initCount",
                "title":"原始数量",
                "width":"70px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "packageNumber",
                "title":"扎号",
                "width":"60px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "weight",
                "title":"重量",
                "width":"60px",
                "defaultContent": "0",
                "sClass": "text-center",
            }, {
                "data": "batch",
                "title":"卷次",
                "width":"60px",
                "defaultContent": "0",
                "sClass": "text-center",
            }, {
                "data": "tailorQcodeID",
                "title":"二维码",
                "width":"90px",
                "defaultContent": "",
                "sClass": "text-center",
                render: function (data, type, row, meta) {
                    return PrefixZero(data,9);
                }
            }, {
                "data": "groupName",
                "title":"组名",
                "width":"60px",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "tailorID",
                "title":"操作",
                "width":"40px",
                "defaultContent": "",
                "sClass": "text-center",
            }
        ],
        "columnDefs" :
            [{
                "orderable" : false, // 禁用排序
                "targets" : [16], // 指定的列
                "data" : "tailorID",
                "width":"40px",
                "render" : function(data, type, full, meta) {
                    return "<a href='#' style='color:#3e8eea' onclick='updateTailor(this)' id='"+data+"'>改码</a>";
                }
            }],
    });
}

function PrefixZero(num, n) {
    return (Array(n).join(0) + num).slice(-n);
}

function updateTailor(obj) {
    $("#editTailor #orderName").val($(obj).parent().parent().find("td").eq(1).text());
    $("#editTailor #customerName").val($(obj).parent().parent().find("td").eq(3).text());
    var tmpColorName = $(obj).parent().parent().find("td").eq(6).text();
    $("#editTailor #jarName").val($(obj).parent().parent().find("td").eq(5).text());
    $("#editTailor #bedNumber").val($(obj).parent().parent().find("td").eq(4).text());
    $("#editTailor #layerCount").val($(obj).parent().parent().find("td").eq(9).text());
    $("#editTailor #packageNumber").val($(obj).parent().parent().find("td").eq(11).text());
    $("#editTailor #partName").val($(obj).parent().parent().find("td").eq(8).text());
    var tmpSizeName = $(obj).parent().parent().find("td").eq(7).text();
    $.ajax({
        url: "/erp/getordercolornamesbyorder",
        data: {"orderName": orderName},
        success:function(data){
            $("#editTailor #colorName").empty();
            if (data.colorNameList) {
                $.each(data.colorNameList, function(index,element){
                    $("#editTailor #colorName").append("<option value='"+element+"'>"+element+"</option>");
                });
                $("#editTailor #colorName option[value='"+tmpColorName+"']").prop("selected","selected");
            }
        },
        error:function(){
        }
    });

    $.ajax({
        url: "/erp/getordersizenamesbyorder",
        data: {"orderName": orderName},
        success:function(data){
            $("#editTailor #sizeName").empty();
            if (data.sizeNameList) {
                $.each(data.sizeNameList, function(index,element){
                    $("#editTailor #sizeName").append("<option value='"+element+"'>"+element+"</option>");
                });
                $("#editTailor #sizeName option[value='"+tmpSizeName+"']").prop("selected","selected");
            }
        },
        error:function(){
        }
    });
    $.blockUI({
        css: {
            width: '50%',
            top: '10%',
            left:'25%',
            border: 'none',
            padding: '15px',
            backgroundColor: '#fff',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: 1,
            color: '#000'
        },
        message: $('#editTailor')
    });

    $("#editTailorYes").unbind("click").bind("click", function () {
        var flag = false;
        $("#editTailor input").each(function () {
            if($(this).val().trim() == "") {
                flag = true;
                return false;
            }
        });
        if(flag) {
            swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请填写完所有字段信息！</span>",html: true});
            return false;
        }
        var tailorList = [];
        $(globalTailorList).each(function (index, item) {
            if (item.bedNumber == bedNumber && item.packageNumber == $("#editTailor #packageNumber").val()){
                var tailor = {};
                tailor.orderName = orderName;
                tailor.clothesVersionNumber = clothesVersionNumber;
                tailor.customerName = customerName;
                tailor.colorName = $("#editTailor #colorName").val();
                tailor.jarName = $("#editTailor #jarName").val();
                tailor.bedNumber = bedNumber;
                tailor.layerCount = $("#editTailor #layerCount").val();
                tailor.packageNumber = $("#editTailor #packageNumber").val();
                tailor.partName = item.partName;
                tailor.sizeName = $("#editTailor #sizeName").val();
                tailor.weight = $(obj).parent().parent().find("td").eq(12).text();
                tailor.batch = $(obj).parent().parent().find("td").eq(13).text();
                tailor.tailorQcode = tailor.orderName +"-"+tailor.customerName+"-"+tailor.bedNumber+"-"+tailor.jarName+"-"+tailor.colorName+"-"+tailor.sizeName+"-"+tailor.layerCount+"-"+tailor.packageNumber+"-"+item.partName;
                tailor.tailorQcodeID = item.tailorQcodeID;
                tailor.isDelete = 0;
                tailor.tailorType = item.tailorType;
                tailor.printTimes = 0;
                tailorList.push(tailor);
            }
        });


        $.ajax({
            url: "/erp/savetailordata",
            data: {
                tailorList: JSON.stringify(tailorList),
                groupName: groupName
            },
            success:function(data){
                if(data == 0) {
                    $.unblockUI();
                    $("#packageTableDiv").empty();
                    search();
                    swal({type:"success",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">恭喜您，添加成功！</span>",html: true});
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，添加失败！</span>",html: true});
                }
            },
            error:function(){
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务器发生了未知错误～！</span>",html: true});
            }
        });

        $.unblockUI();
        $("#editTailor input").val("");

    });
    $("#editTailorNo").unbind("click").bind("click", function () {
        $.unblockUI();
        $("#editTailor input").val("");
    });
}

function addPackage(obj) {
    if(partNameArray.length == 0) {
        return false;
    }
    var bedNumber = $("#bedNumber").val();
    var tailorType = 0;
    if (bedNumber > 1000){
        tailorType = 1;
    }
    var html =
        "<tr>\n" +
        "     <td>\n" +
        "         <label class=\"control-label\" style=\"margin-bottom: 15px;text-align: right;\">缸号</label>\n" +
        "     </td>\n" +
        "     <td>\n" +
        "         <select name=\"jarName\" class=\"form-control\" autocomplete=\"off\" style=\"width: 130px;margin-bottom: 10px;\"></select>\n" +
        "     </td>\n" +
        "     <td>\n" +
        "         <label class=\"control-label\" style=\"margin-bottom: 15px;text-align: right;\">尺码</label>\n" +
        "     </td>\n" +
        "     <td>\n" +
        "         <select name=\"sizeName\" class=\"form-control\" autocomplete=\"off\" style=\"width: 130px;margin-bottom: 10px;\"></select>\n" +
        "     </td>\n" +
        "     <td>\n" +
        "         <label class=\"control-label\" style=\"margin-bottom: 15px;text-align: right;\">颜色</label>\n" +
        "     </td>\n" +
        "     <td>\n" +
        "         <select name=\"colorName\" class=\"form-control\" autocomplete=\"off\" style=\"width: 130px;margin-bottom: 10px;\"></select>\n" +
        "     </td>\n" +
        " </tr>\n" +
        " <tr>\n" +
        "     <td>\n" +
        "         <label class=\"control-label\" style=\"margin-bottom: 15px;text-align: right;\">数量</label>\n" +
        "     </td>\n" +
        "     <td>\n" +
        "         <input name=\"layerCount\" class=\"form-control\" autocomplete=\"off\" style=\"width: 130px;margin-bottom: 10px;\" oninput=\"value=value.replace(/[^\\d]/g,'')\">\n" +
        "     </td>\n" +
        "     <td>\n" +
        "         <label class=\"control-label\" style=\"margin-bottom: 15px;text-align: right;\">卷次重量</label>\n" +
        "     </td>\n" +
        "     <td>\n" +
        "         <select name=\"batchWeight\" class=\"form-control\" autocomplete=\"off\" style=\"width: 130px;margin-bottom: 10px;\"></select>\n" +
        "     </td>\n" +
        "     <td>\n" +
        "         <label class=\"control-label\" style=\"margin-bottom: 15px;text-align: right;\">扎号</label>\n" +
        "     </td>\n" +
        "     <td>\n" +
        "         <input name=\"packageNumber\" class=\"form-control\" autocomplete=\"off\" style=\"width: 130px;margin-bottom: 10px;\" readonly>\n" +
        "     </td>\n" +
        " </tr>";
    $("#packageTableDiv").append(html);
    for(var i=0;i<jarNameArray.length;i++) {
        $("select[name='jarName']").append("<option value='"+jarNameArray[i]+"'>"+jarNameArray[i]+"</option>");
    }
    for(var i=0;i<batchAndWeight.length;i++) {
        $("select[name='batchWeight']").append("<option value='"+batchAndWeight[i]+"'>"+batchAndWeight[i]+"</option>");
    }
    $.blockUI({
        css: {
            width: '60%',
            top: '30%',
            left: '20%',
            border: 'none',
            padding: '15px',
            backgroundColor: '#fff',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: 1,
            color: '#000'
        },
        message: $('#addPackageWin')
    });
    $.ajax({
        url: "/erp/getordercolornamesbyorder",
        data: {"orderName": orderName},
        success:function(data){
            if (data.colorNameList) {
                $.each(data.colorNameList, function(index,element){
                    $("select[name='colorName']").append("<option value='"+element+"'>"+element+"</option>");
                })
            }
        },
        error:function(){
        }
    });
    $.ajax({
        url: "/erp/getordersizenamesbyorder",
        data: {"orderName": orderName},
        success:function(data){
            if (data.sizeNameList) {
                $.each(data.sizeNameList, function(index,element){
                    $("select[name='sizeName']").append("<option value='"+element+"'>"+element+"</option>");
                })
            }
        },
        error:function(){
        }
    });
    $.ajax({
        url: "/erp/getMaxPackageNumberByOrderType",
        data: {
            "orderName": orderName,
            "tailorType": tailorType
        },
        success:function(data){
            $("input[name='packageNumber']").each(function(index,item) {
                $(item).val(parseInt(data)+1);
            })
        },
        error:function(){
        }
    });
    $.ajax({
        url: "/erp/getMaxTailorQcodeId",
        data: {"number":partNameArray.length},
        success:function(data){
            maxQrCode = data;
        },
        error:function(){
        }
    });

    $("#addPackageWinYes").unbind("click").bind("click", function () {
        var flag = false;
        $("#packageTableDiv input").each(function () {
            if($(this).val().trim() == "") {
                flag = true;
                return false;
            }
        });
        if(flag) {
            swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请填写完所有字段信息！</span>",html: true});
            return false;
        }
        var tailorList = [];
        for(var i=0;i<partNameArray.length;i++) {
            var tailor = {};
            tailor.orderName = orderName;
            tailor.clothesVersionNumber = clothesVersionNumber;
            tailor.customerName = customerName;
            tailor.colorName = $("#packageTableDiv select[name='colorName']").eq(0).val();
            tailor.jarName = $("#packageTableDiv select[name='jarName']").eq(0).val();
            tailor.bedNumber = bedNumber;
            tailor.layerCount = $("#packageTableDiv input[name='layerCount']").eq(0).val();
            tailor.packageNumber = $("#packageTableDiv input[name='packageNumber']").eq(0).val();
            tailor.partName = partNameArray[i];
            tailor.sizeName = $("#packageTableDiv select[name='sizeName']").eq(0).val();
            tailor.weight = $("#packageTableDiv select[name='batchWeight']").eq(0).val().split('-')[1];
            tailor.batch = $("#packageTableDiv select[name='batchWeight']").eq(0).val().split('-')[0];
            tailor.tailorQcode = tailor.orderName +"-"+tailor.customerName+"-"+tailor.bedNumber+"-"+tailor.jarName+"-"+tailor.colorName+"-"+tailor.sizeName+"-"+tailor.layerCount+"-"+tailor.packageNumber+"-"+tailor.partName;
            tailor.tailorQcodeID = PrefixZero(maxQrCode + 1 + i);
            tailor.isDelete = 0;
            if (tailor.partName.startsWith("主身")){
                tailor.tailorType = 0;
            } else {
                tailor.tailorType = 1;
            }
            tailor.printTimes = 0;
            tailorList.push(tailor);
        }
        $.ajax({
            url: "/erp/savetailordata",
            data: {
                tailorList: JSON.stringify(tailorList),
                groupName: groupName,
            },
            success:function(data){
                if(data == 0) {
                    $.unblockUI();
                    $("#packageTableDiv").empty();
                    search();
                    swal({type:"success",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">恭喜您，添加成功！</span>",html: true});
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，添加失败！</span>",html: true});
                }
            },
            error:function(){
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务器发生了未知错误～！</span>",html: true});
            }
        });


    });
    $("#addPackageWinNo").unbind("click").bind("click", function () {
        $.unblockUI();
        $("#packageTableDiv").empty();
    });
}