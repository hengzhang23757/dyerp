var basePath=$("#basePath").val();
var userName=$("#userName").val();
layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.21'
}).extend();
$(document).ready(function () {
    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumber", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });
    $("#orderName").bind('blur change',function(){
        var orderName = $("#orderName").val();
        if ($("#operate").val() === "bed"){
            $("#bedNumber").empty();
        } else {
            $.ajax({
                url: "/erp/getbednumbersbyordername",
                data: {"orderName": orderName},
                success:function(data){
                    $("#bedNumber").empty();
                    if (data.bedNumList) {
                        $("#bedNumber").append("<option value=''>选择床号</option>");
                        $.each(data.bedNumList, function(index,element){
                            $("#bedNumber").append("<option value="+element+">"+element+"</option>");
                        })
                    }
                },
                error:function(){
                }
            });
        }

    });

    $("#clothesVersionNumber").bind('blur change',function(){
        var orderName = $("#orderName").val();
        if ($("#operate").val() === "bed"){
            $("#bedNumber").empty();
        } else {
            $.ajax({
                url: "/erp/getbednumbersbyordername",
                data: {"orderName": orderName},
                success:function(data){
                    $("#bedNumber").empty();
                    if (data.bedNumList) {
                        $("#bedNumber").append("<option value=''>选择床号</option>");
                        $.each(data.bedNumList, function(index,element){
                            $("#bedNumber").append("<option value="+element+">"+element+"</option>");
                        })
                    }
                },
                error:function(){
                }
            });
        }
    });

    $("#operate").change(function () {
        var orderName = $("#orderName").val();
        if ($("#operate").val() === "bed"){
            $("#delBtn").hide();
            $('#bedTableDiv').show();
            $('#packageTableDiv').hide();
            $("#bedNumber").empty();
            $("#packageTable").empty();
        } else {
            $('#bedTableDiv').hide();
            $('#packageTableDiv').show();
            $("#bedTable").empty();
            $.ajax({
                url: "/erp/getbednumbersbyordername",
                data: {"orderName": orderName},
                success:function(data){
                    $("#bedNumber").empty();
                    if (data.bedNumList) {
                        $("#bedNumber").append("<option value=''>选择床号</option>");
                        $.each(data.bedNumList, function(index,element){
                            $("#bedNumber").append("<option value="+element+">"+element+"</option>");
                        })
                    }
                },
                error:function(){
                }
            });
        }
    })
});
function checkAll(obj) {
    if($(obj).is(':checked')) {
        $("#packageTable tbody input[type='checkbox']").prop("checked",true);
    }else {
        $("#packageTable tbody input[type='checkbox']").prop("checked",false);
    }
}

var userRole=$("#userRole").val();

function searchTailorInfo() {

    if($("#orderName").val().trim()=="") {
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入要查询的订单号！</span>",html: true});
        return false;
    }
    if ($("#operate").val() === "bed"){
        $("#delBtn").hide();
        $('#bedTableDiv').show();
        $('#packageTableDiv').hide();
        $.ajax({
            url: basePath + "erp/getbedinfoindelete",
            type:'GET',
            data: {
                orderName:$("#orderName").val()
            },
            success: function (data) {
                if(data) {
                    createBedTailorInfoTable(data.tailorList);
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，没有查到相关信息！</span>",html: true});
                }
            },
            error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        })
    } else {
        if($("#bedNumber").val().trim()=="") {
            swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请选择床号！</span>",html: true});
            return false;
        }
        $("#delBtn").show();
        $('#bedTableDiv').hide();
        $('#packageTableDiv').show();
        $.ajax({
            url: basePath + "erp/getpackageinfoindelete",
            type:'GET',
            data: {
                orderName:$("#orderName").val(),
                bedNumber:$("#bedNumber").val()
            },
            success: function (data) {
                if(data) {
                    createPackageTailorInfoTable(data.tailorList);
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，没有查到相关信息！</span>",html: true});
                }
            },
            error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        })
    }


}

var bedTable,packageTable;
function createBedTailorInfoTable(data, orderName) {

    if (bedTable != undefined) {
        bedTable.clear(); //清空一下table
        bedTable.destroy(); //还原初始化了的datatable
    }
    bedTable = $('#bedTable').DataTable({
        "data":data,
        language : {
            processing : "载入中",//处理页面数据的时候的显示
            paginate : {//分页的样式文本内容。
                previous : "上一页",
                next : "下一页",
                first : "第一页",
                last : "最后一页"
            },
            search:"搜索：",
            zeroRecords : "没有内容",//table tbody内容为空时，tbody的内容。
            //下面三者构成了总体的左下角的内容。
            info : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//左下角的信息显示，大写的词为关键字。
            infoEmpty : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//筛选为空时左下角的显示。
            infoFiltered : ""//筛选之后的左下角筛选提示(另一个是分页信息显示，在上面的info中已经设置，所以可以不显示)，
        },
        pageLength : 100,// 每页显示10条数据
        pagingType : "full_numbers", // 分页样式：simple,simple_numbers,full,full_numbers，
        "paging": true,
        "info": false,
        searching:false,
        lengthChange:false,
        fixedHeader: true,
        scrollY: $(document.body).height() - 210,
        "columns": [
            {
                "data": null,
                "title":"序号",
                "width":"20%",
                "defaultContent": "",
                "sClass": "text-center",
                render: function (data, type, row, meta) {
                    var no = meta.settings._iDisplayStart + meta.row + 1;
                    return no;
                }
            },{
                "data": "clothesVersionNumber",
                "title":"单号",
                "width":"20%",
                "defaultContent": "",
                "sClass": "text-center",
            },{
                "data": "orderName",
                "title":"订单号",
                "width":"20%",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "bedNumber",
                "title":"床号",
                "width":"20%",
                "defaultContent": "",
                "sClass": "text-center",
            }, {
                "data": "",
                "title":"操作",
                "width":"20%",
                "defaultContent": "",
                "sClass": "text-center",
            }
        ],
        "columnDefs" :
            [{
                "orderable" : false, // 禁用排序
                "targets" : [4], // 指定的列
                "render" : function(data, type, full, meta) {
                    return "<a href='#' style='color:#ff0000' onclick=deleteTailor('"+full.orderName+"',"+full.bedNumber+")>删除该床</a>";
                }
            }],
    });
}


function deleteTailor(orderName,bedNumber) {
    if(userRole!='root' && userRole!='role1' && userRole!='role18'){
        swal("SORRY!", "对不起，您没有操作权限！", "warning");
        return;
    }
    swal({
        title: "",
        text: "<span style=\"font-weight:bolder;font-size: 20px\">您确定要删除吗？</span>",
        type: "warning",
        html:true,
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: "确定",
        cancelButtonText:"我再想想",
        confirmButtonColor: "#ec6c62",
        showLoaderOnConfirm: true
    }, function() {
        $.ajax({
            url: basePath + "erp/deletetailorbyorderbed",
            type:'POST',
            data: {
                orderName:orderName,
                bedNumber:bedNumber,
                userName:userName
            },
            success: function (data) {
                if(data == 0) {
                    $.unblockUI();
                    $("input").val("");
                    swal({
                            type:"success",
                            title:"",
                            text: "<span style=\"font-weight:bolder;font-size: 20px\">恭喜你，删除成功！</span>",
                            html: true
                        },
                        function(){
                            location.href=basePath+"erp/bedTailorInfoStart";
                        });
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，删除失败！</span>",html: true});
                }
            },
            error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        })
    });
}


function createPackageTailorInfoTable(data) {
    if (packageTable != undefined) {
        packageTable.clear(); //清空一下table
        packageTable.destroy(); //还原初始化了的datatable
    }
    packageTable = $('#packageTable').DataTable({
        "data":data,
        language : {
            processing : "载入中",//处理页面数据的时候的显示
            paginate : {//分页的样式文本内容。
                previous : "上一页",
                next : "下一页",
                first : "第一页",
                last : "最后一页"
            },
            search:"搜索：",
            zeroRecords : "没有内容",//table tbody内容为空时，tbody的内容。
            //下面三者构成了总体的左下角的内容。
            info : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//左下角的信息显示，大写的词为关键字。
            infoEmpty : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//筛选为空时左下角的显示。
            infoFiltered : ""//筛选之后的左下角筛选提示(另一个是分页信息显示，在上面的info中已经设置，所以可以不显示)，
        },
        pageLength : 100,// 每页显示10条数据
        pagingType : "full_numbers", // 分页样式：simple,simple_numbers,full,full_numbers，
        "paging": true,
        "info": false,
        searching:false,
        lengthChange:false,
        fixedHeader: true,
        scrollY: $(document.body).height() - 230,
        "columns": [
            {
                "data": "packageNumber",
                "title":"<input type='checkbox' onclick='checkAll(this)'>",
                "width":"8%",
                "defaultContent": "",
                "sClass": "text-center"
            },
            {
                "data": null,
                "title":"序号",
                "width":"10%",
                "defaultContent": "",
                "sClass": "text-center",
                render: function (data, type, row, meta) {
                    var no = meta.settings._iDisplayStart + meta.row + 1;
                    return no;
                }
            },{
                "data": "clothesVersionNumber",
                "title":"单号",
                "width":"15%",
                "defaultContent": "",
                "sClass": "text-center"
            },{
                "data": "orderName",
                "title":"款号",
                "width":"15%",
                "defaultContent": "",
                "sClass": "text-center"
            }, {
                "data": "bedNumber",
                "title":"床号",
                "width":"10%",
                "defaultContent": "",
                "sClass": "text-center"
            }, {
                "data": "colorName",
                "title":"颜色",
                "width":"10%",
                "defaultContent": "",
                "sClass": "text-center"
            }, {
                "data": "sizeName",
                "title":"尺码",
                "width":"10%",
                "defaultContent": "",
                "sClass": "text-center"
            }, {
                "data": "packageNumber",
                "title":"扎号",
                "width":"10%",
                "defaultContent": "",
                "sClass": "text-center"
            }, {
                "data": "layerCount",
                "title":"数量",
                "width":"10%",
                "defaultContent": "",
                "sClass": "text-center"
            }
        ],
        "columnDefs" :
            [{
                "orderable" : false, // 禁用排序
                "targets" : [0], // 指定的列
                "render" : function(data, type, full, meta) {
                    return "<input name='checkBtn' type='checkbox' value='"+data+"'>";
                }
            }],
    });
}

function batchDelete() {

    if(userRole!='root' && userRole!='role1' && userRole!='role18'){
        swal("SORRY!", "对不起，您没有操作权限！", "warning");
        return;
    }

    var packageNumberList = [];
    $("#packageTable tbody input[name='checkBtn']").each(function(){
        if($(this).is(':checked')) {
            packageNumberList.push($(this).val());
        }
    });
    if(packageNumberList.length == 0) {
        swal("SORRY!", "请先选择要删除的数据！", "error");
        return;
    }
    swal({
        title: "",
        text: "<span style=\"font-weight:bolder;font-size: 20px\">您确定要删除吗？</span>",
        type: "warning",
        html:true,
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: "确定",
        cancelButtonText:"我再想想",
        confirmButtonColor: "#ec6c62",
        showLoaderOnConfirm: true
    }, function() {
        $.ajax({
            url: basePath + "erp/deletetailorbyorderbedpack",
            type:'POST',
            data: {
                orderName:$("#orderName").val(),
                bedNumber:$("#bedNumber").val(),
                userName:userName,
                packageNumberList:packageNumberList
            },
            traditional:true,
            success: function (data) {
                if(data == 0) {
                    $.unblockUI();
                    $("input").val("");
                    swal({
                            type:"success",
                            title:"",
                            text: "<span style=\"font-weight:bolder;font-size: 20px\">恭喜你，删除成功！</span>",
                            html: true
                        },
                        function(){
                            location.href=basePath+"erp/bedTailorInfoStart";
                        });
                }else {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，删除失败！</span>",html: true});
                }
            },
            error: function () {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
            }
        })
    });
}

