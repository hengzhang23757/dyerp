var basePath=$("#basePath").val();
// var factoryName=$("#factoryName").val();
var $partNameSel;
layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.21'
}).extend();
$(document).ready(function () {
    var type=$("#type").val();

    $partNameSel = $('#partName').selectize({
        plugins: ['remove_button'],
        maxItems: null,
        persist: true,
        create: false,
        valueField: 'id',
        labelField: 'title',
        searchField: ['title']
    });

    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumber", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });

    $("#orderName").bind('blur change',function(){
        var orderName = $("#orderName").val();
        $.ajax({
            url: "/erp/getcustomernamebyordername",
            data: {"orderName": orderName},
            success:function(data){
                $("#customerName").empty();
                $("#customerName").val(data);
            },
            error:function(){
            }
        });
        $.ajax({
            url: "/erp/getmaxbednumberbyordertype",
            data: {
                "orderName": orderName,
                "tailorType": 0
            },
            success:function(data){
                if(data) {
                    $("#bedNumber").empty();
                    $("#bedNumber").val(data);
                }else {
                    $("#bedNumber").empty();
                    $("#bedNumber").val(1);
                }
            },
            error:function(){
            }
        });
        $("select[name='colorName']").empty();
        $.ajax({
            url: "/erp/getordercolornamesbyorder",
            data: {"orderName": orderName},
            success:function(data){
                if (data.colorNameList) {
                    $.each(data.colorNameList, function(index,element){
                        $("select[name='colorName']").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });

        $("select[name='size']").empty();
        $("select[name='size']").append("<option value=''>尺码</option>");
        $.ajax({
            url: "/erp/getordersizenamesbyorder",
            data: {"orderName": orderName},
            success:function(data){
                if (data.sizeNameList) {
                    $.each(data.sizeNameList, function(index,element){
                        $("select[name='size']").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });

        var control = $partNameSel[0].selectize;
        $.ajax({
            url: "/erp/getmainprintpartnamesbyorder",
            data: {"orderName": orderName},
            success: function (data) {
                control.clear();
                control.clearOptions();
                if(data.printPartNameList) {
                    $.each(data.printPartNameList,function (index,item) {
                        control.addOption({
                            id : item,
                            title : item
                        });
                    })
                }
            },
            error: function () {
                swal("OMG!", "发生了未知的错误！", "error");
            }
        });
    });

    $("#clothesVersionNumber").bind('blur change',function(){
        var orderName = $("#orderName").val();
        $.ajax({
            url: "/erp/getcustomernamebyordername",
            data: {"orderName": orderName},
            success:function(data){
                $("#customerName").empty();
                $("#customerName").val(data);
            },
            error:function(){
            }
        });
        $.ajax({
            url: "/erp/getmaxbednumberbyordertype",
            data: {
                "orderName": orderName,
                "tailorType": 0
            },
            success:function(data){
                if(data) {
                    $("#bedNumber").empty();
                    $("#bedNumber").val(data);
                }else {
                    $("#bedNumber").empty();
                    $("#bedNumber").val(1);
                }
            },
            error:function(){
            }
        });
        $("select[name='colorName']").empty();
        $.ajax({
            url: "/erp/getordercolornamesbyorder",
            data: {"orderName": orderName},
            success:function(data){
                if (data.colorNameList) {
                    $.each(data.colorNameList, function(index,element){
                        $("select[name='colorName']").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });

        $("select[name='sizeName']").empty();
        $.ajax({
            url: "/erp/getordersizenamesbyorder",
            data: {"orderName": orderName},
            success:function(data){
                if (data.sizeNameList) {
                    $.each(data.sizeNameList, function(index,element){
                        $("select[name='sizeName']").append("<option value='"+element+"'>"+element+"</option>");
                    })
                }
            },
            error:function(){
            }
        });

        var control = $partNameSel[0].selectize;
        $.ajax({
            url: "/erp/getmainprintpartnamesbyorder",
            data: {"orderName": orderName},
            success: function (data) {
                control.clear();
                control.clearOptions();
                if(data.printPartNameList) {
                    $.each(data.printPartNameList,function (index,item) {
                        control.addOption({
                            id : item,
                            title : item
                        });
                    })
                }
            },
            error: function () {
                swal("OMG!", "发生了未知的错误！", "error");
            }
        });
    });

});

var tailorTable;
function addOrder() {
    var flag = false;
    $("#baseInfo input").each(function () {
        if($(this).val().trim() == "") {
            flag = true;
            return false;
        }
    });
    $("#baseInfo select").each(function () {
        if($(this).val().trim() == "") {
            flag = true;
            return false;
        }
    });
    if(flag) {
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请填写完基本信息！</span>",html: true});
        return false;
    }
    var orderName = $("#orderName").val();
    var customerName = $("#customerName").val();
    var bedNumber = $("#bedNumber").val();
    var clothesVersionNumber = $("#clothesVersionNumber").val();
    var tailorListJson = [];
    var partNameList = [];
    var selPartName = $("#partName").val();
    $.each(selPartName,function (index,item) {
        partNameList.push(item);
    })
    $("select[name='sizeName']").each(function (sIndex,sItem) {
        $("select[name='colorName']").each(function (cIndex,cItem) {
            var tmp = {};
            tmp.orderName = orderName;
            tmp.clothesVersionNumber = clothesVersionNumber;
            tmp.customerName = customerName;
            tmp.bedNumber = bedNumber;
            tmp.isDelete = 0;
            tmp.tailorType = 0;
            tmp.printTimes = 0;
            tmp.weight = 1;
            tmp.batch = 1;
            tmp.colorName = $(cItem).val();
            tmp.sizeName = $(sItem).val();
            tmp.jarName = "000";
            var numberVal = $(cItem).parent().parent().find("input[name='number']").eq(sIndex).val();
            var r = /^\+?[1-9][0-9]*$/;
            if(r.test(numberVal)) {
                tmp.layerCount = numberVal;
                tmp.initCount = numberVal;
                tailorListJson.push(tmp);
            }
        })
    });
    if(partNameList.length==0) {
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请选择部位！</span>",html: true});
        return false;
    }
    if(tailorListJson.length==0) {
        swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入数量！</span>",html: true});
        return false;
    }
    $.ajax({
        url: "/erp/generatemainsmalltailor",
        type:'POST',
        data: {
            tailorListJson:JSON.stringify(tailorListJson),
            partNameListJson:JSON.stringify(partNameList),
            orderName: orderName
        },
        success: function (data) {
            if(data && data!= "null") {
                var json = data["tailorList"];
                swal(
                    {   type:"success",
                        title:"",
                        text: "<span style=\"font-weight:bolder;font-size: 20px\">恭喜你，生成成功！</span>",
                        html: true
                    },function () {
                        var href = $("li.active a[data-toggle='tab']",parent.document).attr("href");
                        var tabId = href.substr(7);
                        window.parent.document.getElementById("tailorA").click();
                        var numberId = window.parent.document.getElementById("numberID").value;
                        var list = "";
                        if ($('#tailorTable', window.parent.document).hasClass('dataTable')) {
                            $('#tailorTable_wrapper', window.parent.document).remove();
                            var $div = $("<table class=\"table table-striped\" id=\"tailorTable\" >\n" +
                                "             <thead>\n" +
                                "             <tr bgcolor=\"#ffcb99\" style=\"color: black;\">\n" +
                                "                 <input type=\"text\" hidden id=\"numberID\" value=\"1\">\n" +
                                "                 <th style=\"width: 30px;text-align:left;font-size:14px\"><input type=\"checkbox\" onclick=\"checkAll(this)\"></th>\n" +
                                "                 <th style=\"width: 30px;text-align:center;font-size:14px\">序号</th>\n" +
                                "                 <th style=\"width: 90px;text-align:center;font-size:14px\">订单号</th>\n" +
                                "                 <th style=\"width: 90px;text-align:center;font-size:14px\">版单号</th>\n" +
                                "                 <th style=\"width: 60px;text-align:center;font-size:14px\">客户</th>\n" +
                                "                 <th style=\"width: 60px;text-align:center;font-size:14px\">颜色</th>\n" +
                                "                 <th style=\"width: 45px;text-align:center;font-size:14px\">缸号</th>\n" +
                                "                 <th style=\"width: 45px;text-align:center;font-size:14px\">床号</th>\n" +
                                "                 <th style=\"width: 45px;text-align:center;font-size:14px\">数量</th>\n" +
                                "                 <th style=\"width: 45px;text-align:center;font-size:14px\">扎号</th>\n" +
                                "                 <th style=\"width: 60px;text-align:center;font-size:14px\">部位</th>\n" +
                                "                 <th style=\"width: 60px;text-align:center;font-size:14px\">尺码</th>\n" +
                                "                 <th style=\"width: 60px;text-align:center;font-size:14px\">重量</th>\n" +
                                "                 <th style=\"width: 60px;text-align:center;font-size:14px\">卷次</th>\n" +
                                "                 <th style=\"width: 90px;text-align:center;font-size:14px\">二维码</th>\n" +
                                "                 <th style=\"width: 120px;text-align:center;font-size:14px\">操作</th>\n" +
                                "             </tr>\n" +
                                "             </thead>\n" +
                                "             <tbody id=\"tailorBody\">\n" +
                                "             </tbody>\n" +
                                "         </table>");
                            $("#tailorTableDiv",window.parent.document).append($div);
                        }
                        var $tailorBody = window.parent.document.getElementById("tailorBody");
                        var $saveButton = window.parent.document.getElementById("saveButton");
                        $saveButton.style.display='block';
                        $.each(json,function (index,item) {
                            list +=  "<tr>" +
                                "<td><input type='checkbox' value='"+numberId+"'></td>" +
                                "<td>"+numberId+"</td>" +
                                "<td>"+item.orderName+"</td>" +
                                "<td>"+item.clothesVersionNumber+"</td>" +
                                "<td>"+item.customerName+"</td>" +
                                "<td>"+item.colorName+"</td>" +
                                "<td>"+item.jarName+"</td>" +
                                "<td>"+item.bedNumber+"</td>" +
                                "<td>"+item.layerCount+"</td>" +
                                "<td>"+item.packageNumber+"</td>" +
                                "<td>"+item.partName+"</td>" +
                                "<td>"+item.sizeName+"</td>" +
                                "<td>"+item.weight+"</td>" +
                                "<td>"+item.batch+"</td>" +
                                "<td>"+PrefixZero(item.tailorQcodeID,9)+"</td>" +
                                "<td><a href='#' style='color:#3e8eea' onclick='showQrCode(this)'>查看</a>&nbsp;&nbsp;&nbsp;<a href='#' style='color:#3e8eea' onclick='updateTailor(this)'>修改</a>&nbsp;&nbsp;&nbsp;<a href='#' style='color:#3e8eea' onclick='delTailor(this)'>删除</a></td>" +
                                "</tr>";
                            numberId++;
                        });
                        $tailorBody.innerHTML = list;
                        window.parent.document.getElementById("tailorBody").value=numberId;
                        tailorTable = $('#tailorTable',window.parent.document).DataTable({
                            language : {
                                processing : "载入中",//处理页面数据的时候的显示
                                paginate : {//分页的样式文本内容。
                                    previous : "上一页",
                                    next : "下一页",
                                    first : "第一页",
                                    last : "最后一页"
                                },
                                search:"搜索：",
                                lengthMenu:"显示 _MENU_ 条",
                                zeroRecords : "没有内容",//table tbody内容为空时，tbody的内容。
                                //下面三者构成了总体的左下角的内容。
                                info : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//左下角的信息显示，大写的词为关键字。
                                infoEmpty : "第 _PAGE_/_PAGES_页 共 _TOTAL_条记录",//筛选为空时左下角的显示。
                                infoFiltered : ""//筛选之后的左下角筛选提示(另一个是分页信息显示，在上面的info中已经设置，所以可以不显示)，
                            },
                            searching:false,
                            ordering:true,
                            "paging" : false,
                            "info": true,
                            "destroy":true,
                            pagingType : "full_numbers", // 分页样式：simple,simple_numbers,full,full_numbers，
                            scrollX: 1500,
                            scrollY: 650,
                            fixedHeader: true,
                            scrollCollapse: true,
                            scroller:       true,
                            lengthChange:false
                        });
                    });
            }else {
                swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，生成失败！</span>",html: true});
            }
        },
        error: function () {
            swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }
    })
}

function deleteRow(obj) {
    tailorTable.row($(obj).parents('tr')).remove().draw(false);
}

function loadPartName(){
    var control = $partNameSel[0].selectize;
    $.ajax({
        url: "/erp/getmainprintpartnamesbyorder",
        data: {
            "orderName": $("#orderName").val()
        },
        success: function (data) {
            control.clear();
            control.clearOptions();
            if(data.printPartNameList) {
                $.each(data.printPartNameList,function (index,item) {
                    control.addOption({
                        id : item,
                        title : item
                    });
                })
            }
        },
        error: function () {
            swal("OMG!", "发生了未知的错误！", "error");
        }
    });
}

function addSize(obj) {
    $("#numberTable").find("tr").eq(0).append($("#numberTable").find("tr").eq(0).find("td").last().clone());
    var index = $('[name="sizeName"]').length-1;
    $('[name="sizeName"]:last').attr("id","sizeName"+index);
    $("tr[name='numberTr']").each(function (i,tr) {
        $(tr).find("td").eq(index+1).after("<td class=\"tdWidth\">\n" +
            "                            <input name=\"number\" class=\"form-control\" autocomplete=\"off\" oninput=\"value=value.replace(/[^\\d]/g,'')\" onkeydown=\"moveCursor(event,this)\">\n" +
            "                        </td>");
    })
    $("button[name='addSize']:last").hide();
    $("button[name='addSize']:last").next().show();
}

function delSize(obj) {
    var index = $(obj).prev().prev().attr("id").replace("sizeName","");
    $(obj).parent().remove();
    $("tr[name='numberTr']").each(function (i,tr) {
        $(tr).find("input[name='number']").eq(index).parent().remove();
    })
    var sizeNameArr = $('[name="sizeName"]');
    for (var i = 0; i < sizeNameArr.length; i++){
        $(sizeNameArr[i]).attr("id","sizeName"+i);
    }

}

function delNumber(obj) {
    $(obj).parent().parent().remove();

    var orderOfLayerArr = $('[name="orderOfLayer"]');
    for (var i = 0; i < orderOfLayerArr.length; i++){
        $(orderOfLayerArr[i]).text(i + 1);
    }
}

function addNumber(obj) {

    $("#numberTable").append($("tr[name='numberTr']:last").clone());
    var orderOfLayerArr = $('[name="orderOfLayer"]');
    for (var i = 0; i < orderOfLayerArr.length; i++){
        $(orderOfLayerArr[i]).text(i + 1);
    }

    $("tr[name='numberTr']:last").find("input").val("");

    $("button[name='addNumber']:last").hide();
    $("button[name='addNumber']:last").next().show();

}

function moveCursor(event,obj) {
    if(event.keyCode==38){
        var name = $(obj).attr("name");
        var index = $("input[name='"+name+"']").index(obj);
        $("input[name='"+name+"']").eq(index-1).focus();
    }else if(event.keyCode==40){
        var name = $(obj).attr("name");
        var index = $("input[name='"+name+"']").index(obj);
        $("input[name='"+name+"']").eq(index+1).focus();
    }
}



function PrefixZero(num, n) {
    return (Array(n).join(0) + num).slice(-n);
}
