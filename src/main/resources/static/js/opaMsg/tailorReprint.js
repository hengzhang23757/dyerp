var basePath=$("#basePath").val();
var globalTailorList;
var form;
layui.use('form', function(){
    form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
    form.render();
});
layui.config({
    base: '/js/ext/',   // 模块目录
    version: 'v1.5.21'
}).extend({                         // 模块别名
    soulTable: 'soulTable',
});
var printTailorTable;
var printTailorList;
var LODOP;
$(document).ready(function () {

    layui.use(['yutons_sug'], function () {
        layui.yutons_sug.render({
            id: "clothesVersionNumber", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysubversion?subVersion=" //设置异步数据接口,url为必填项,params为字段名
        });

        layui.yutons_sug.render({
            id: "orderName", //设置容器唯一id
            height: "300",
            width: "550",
            limit:"10",            limits:[10,20,50,100],
            cols: [
                [{
                    field: 'clothesVersionNumber',
                    title: '单号',
                    align: 'left'
                }, {
                    field: 'orderName',
                    title: '款号',
                    align: 'left'
                }]
            ], //设置表头
            params: [
                {
                    name: 'clothesVersionNumber',
                    field: 'clothesVersionNumber'
                }, {
                    name: 'orderName',
                    field: 'orderName'
                }],//设置字段映射，适用于输入一个字段，回显多个字段
            type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
            url: "/erp/getorderandversionbysuborder?subOrderName=" //设置异步数据接口,url为必填项,params为字段名
        });
    });

    $("#orderName").bind('blur change',function(){
        var orderName = $("#orderName").val();
        $.ajax({
            url: "/erp/getbednumbersbyordername",
            data: {"orderName": orderName},
            success:function(data){
                $("#bedNumber").empty();
                if (data.bedNumList) {
                    $("#bedNumber").append("<option value=''>选择床号</option>");
                    $.each(data.bedNumList, function(index,element){
                        $("#bedNumber").append("<option value="+element+">"+element+"</option>");
                    });
                    form.render();
                }
            },
            error:function(){
            }
        });
    });

    $("#clothesVersionNumber").bind('blur change',function(){
        var orderName = $("#orderName").val();
        $.ajax({
            url: "/erp/getbednumbersbyordername",
            data: {"orderName": orderName},
            success:function(data){
                $("#bedNumber").empty();
                if (data.bedNumList) {
                    $("#bedNumber").append("<option value=''>选择床号</option>");
                    $.each(data.bedNumList, function(index,element){
                        $("#bedNumber").append("<option value="+element+">"+element+"</option>");
                    });
                    form.render();
                }
            },
            error:function(){
            }
        });
    });
});

layui.use(['form', 'soulTable', 'table'], function () {

    var table = layui.table,
        soulTable = layui.soulTable,
        $ = layui.$;
    var load = layer.load();

    printTailorTable = table.render({
        elem: '#printTailorTable'
        ,cols:[[]]
        ,loading:true
        ,data:[]
        ,height: 'full-130'
        ,toolbar: '#toolbarTop' //开启头部工具栏，并为其绑定左侧模板
        ,defaultToolbar: ['filter', 'print', 'export']
        ,title: '票菲数据'
        ,totalRow: true
        ,page: true
        ,limits: [100, 200, 500]
        ,limit: 200 //每页默认显示的数量
        ,done: function () {
            soulTable.render(this);
            layer.close(load);
        }
        ,filter: {
            bottom: true,
            items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
        }
    });

    //监听提交
    var param = {};
    form.on('submit(searchBeat)', function(data){
        var orderName = $("#orderName").val();
        var bedNumber = $("#bedNumber").val();
        var packageNumber = $("#packageNumber").val();
        if ((bedNumber == null || bedNumber === "") && (packageNumber == null || packageNumber === "")){
            layer.msg("床号和扎号不能同时为空", { icon: 2 });
            return false;
        }
        param.orderName = orderName;
        if (bedNumber != null && bedNumber != ""){
            param.bedNumber = bedNumber;
        }
        if (packageNumber != null && packageNumber != ""){
            param.packageNumber = packageNumber;
        }
        var load = layer.load(2);
        $.ajax({
            url: "/erp/getalltailorbyorderbed",
            type: 'GET',
            data: param,
            success: function (res) {
                if (res.tailorList) {
                    var reportData = res.tailorList;
                    globalTailorList = res.tailorList;
                    layui.table.reload("printTailorTable", {
                        cols:[[
                            {type:'checkbox'}
                            ,{type:'numbers', align:'center', title:'序号', width:50}
                            ,{field:'clothesVersionNumber', title:'单号', align:'center', width:120, sort: true, filter: true, totalRowText: '合计'}
                            ,{field:'orderName', title:'款号', align:'center', width:120, sort: true, filter: true}
                            ,{field:'customerName', title:'客户', align:'center', width:90, sort: true, filter: true}
                            ,{field:'bedNumber', title:'床号', align:'center', width:90, sort: true, filter: true}
                            ,{field:'jarName', title:'缸号', align:'center', width:90, sort: true, filter: true}
                            ,{field:'colorName', title:'颜色', align:'center', width:90, sort: true, filter: true}
                            ,{field:'sizeName', title:'尺码', align:'center', width:90, sort: true, filter: true}
                            ,{field:'partName', title:'部位', align:'center', width:100, sort: true, filter: true}
                            ,{field:'layerCount', title:'数量', align:'center', width:70, sort: true, filter: true}
                            ,{field:'initCount', title:'原始数量', align:'center', width:90, sort: true, filter: true}
                            ,{field:'packageNumber', title:'扎号', align:'center', width:90, sort: true, filter: true}
                            ,{field:'tailorQcodeID', title:'二维码', align:'center', width:120, sort: true, filter: true}
                            ,{field:'weight', title:'重量', align:'center', width:90, sort: true, filter: true}
                            ,{field:'batch', title:'卷次', align:'center', width:90, sort: true, filter: true}
                            ,{field:'groupName', title:'组名', align:'center', width:80, sort: true, filter: true}
                            ,{fixed: 'right', field: 'tailorID', title:'操作', align:'center', toolbar: '#barTop', width:220}

                        ]]
                        ,data: reportData   // 将新数据重新载入表格
                        ,overflow: 'tips'
                        ,done: function () {
                            soulTable.render(this);
                            layer.close(load);
                        }
                        ,filter: {
                            bottom: true,
                            clearFilter: false,
                            items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                        }
                    });

                } else {
                    layer.msg("获取失败！", {icon: 2});
                }
            }, error: function () {
                layer.msg("获取失败！", {icon: 2});
            }
        });
        return false;
    });


    table.on('toolbar(printTailorTable)', function(obj){
        if (obj.event === 'clearFilter') {
            soulTable.clearFilter('printTailorTable')
        } else if (obj.event === "printTailor") {   //面料下单
            var checkStatus = table.checkStatus(obj.config.id); //获取选中行状态
            if(checkStatus.data.length == 0) {
                layer.msg('请先选择要打印的数据...');
            }else {
                var tailorList = [];
                $.each(checkStatus.data, function (index, item) {
                    tailorList.push(item);
                });
                layer.msg("数据量较大,正在加载中,请不要重复点击按钮...");
                LODOP=getLodop();
                printTailorList = tailorList;
                LODOP.PRINT_INIT("");
                LODOP.SET_PRINT_PAGESIZE(1,"35mm","90mm","");
                LODOP.SET_PRINT_STYLE("FontSize",11);
                LODOP.SET_PRINT_STYLEA("FontName","黑体");
                CreateAllPages();
                LODOP.PREVIEW();
            }
        } else if (obj.event === "send") {
            var param = {};
            var orderName = $("#orderName").val();
            var bedNumber = $("#bedNumber").val();
            var packageNumber = $("#packageNumber").val();
            if (orderName == null || orderName == ''){
                layer.msg("款号不能为空~~~");
                return false;
            }
            param.orderName = orderName;
            var contentHtml = '确认把款号:' + orderName;
            if (bedNumber != null && bedNumber != ''){
                param.bedNumber = bedNumber;
                contentHtml += ',床号:' + bedNumber;
            }
            if (packageNumber != null && packageNumber != ''){
                param.packageNumber = packageNumber;
                contentHtml += ',扎号:' + packageNumber;
            }
            contentHtml += '发送到阳春德悦吗?';
            layer.confirm(contentHtml, function(index){
                $.ajax({
                    url: "checktailorexist",
                    type:'POST',
                    data: param,
                    success: function (data) {
                        if(data.result == 0 || data.result == 1) {
                            $.ajax({
                                url: "/erp/synctailortoyc",
                                type: 'POST',
                                data: param,
                                success: function (res) {
                                    if (res.result == 0) {
                                        layer.msg("发送成功！", {icon: 1});
                                    } else {
                                        layer.msg("发送失败！", {icon: 2});
                                    }
                                }, error: function () {
                                    layer.msg("发送失败！", {icon: 2});
                                }
                            });
                            return false;
                        }else {
                            layer.msg("已经存在,请确认！",{icon: 2});
                        }
                    }, error: function () {
                        layer.msg("发送失败！",{icon: 2});
                    }
                });
            });
        }
    });

    table.on('tool(printTailorTable)', function(obj){
        var data = obj.data;
        if(obj.event === 'layerCount'){
            var index = layer.open({
                type: 1 //Page层类型
                , title: "修改数量"
                , btn: ['保存']
                , shade: 0.6 //遮罩透明度
                , area: ['400px','250px']
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , content: "<div><table style='padding-top: 10px'><tr><td><label class='layui-form-label' style='width: 150px'>原始数量</label></td><td><input autocomplete='off' id='updateInitCount' class='layui-input'></td></tr>" +
                    "<tr><td><label class='layui-form-label' style='width: 150px'>修改成</label></td><td><input autocomplete='off' id='updateLayerCount' class='layui-input'></td></tr></table></div>"
                ,success: function(layero, index){
                    $("#updateInitCount").val(data.initCount);
                    $("#updateLayerCount").val(data.layerCount);
                }
                ,yes: function(index, layero){
                    var bedNumber = data.bedNumber;
                    var packageNumber = data.packageNumber;
                    var thisTailorList = [];
                    var thisLayerCount = $("#updateLayerCount").val();
                    if (thisLayerCount === '' || thisLayerCount == null){
                        layer.msg("请填写正确数量");
                        return false;
                    }
                    $(globalTailorList).each(function (index, item) {
                        if (item.bedNumber == bedNumber && item.packageNumber == packageNumber){
                            item.layerCount = thisLayerCount;
                            thisTailorList.push(item);
                        }
                    });
                    $.ajax({
                        url: "/erp/updatetailordata",
                        type:'POST',
                        data: {
                            tailorList: JSON.stringify(thisTailorList)
                        },
                        success:function(data){
                            if(data == 0) {
                                layer.msg('修改成功！');

                                $.ajax({
                                    url: "/erp/getalltailorbyorderbed",
                                    type: 'GET',
                                    data: param,
                                    success: function (res) {
                                        if (res.tailorList) {
                                            var reportData = res.tailorList;
                                            globalTailorList = res.tailorList;
                                            layui.table.reload("printTailorTable", {
                                                cols:[[
                                                    {type:'checkbox'}
                                                    ,{type:'numbers', align:'center', title:'序号', width:50}
                                                    ,{field:'clothesVersionNumber', title:'单号', align:'center', width:120, sort: true, filter: true, totalRowText: '合计'}
                                                    ,{field:'orderName', title:'款号', align:'center', width:120, sort: true, filter: true}
                                                    ,{field:'customerName', title:'客户', align:'center', width:90, sort: true, filter: true}
                                                    ,{field:'bedNumber', title:'床号', align:'center', width:90, sort: true, filter: true}
                                                    ,{field:'jarName', title:'缸号', align:'center', width:90, sort: true, filter: true}
                                                    ,{field:'colorName', title:'颜色', align:'center', width:90, sort: true, filter: true}
                                                    ,{field:'sizeName', title:'尺码', align:'center', width:90, sort: true, filter: true}
                                                    ,{field:'partName', title:'部位', align:'center', width:100, sort: true, filter: true}
                                                    ,{field:'layerCount', title:'数量', align:'center', width:70, sort: true, filter: true}
                                                    ,{field:'initCount', title:'原始数量', align:'center', width:90, sort: true, filter: true}
                                                    ,{field:'packageNumber', title:'扎号', align:'center', width:90, sort: true, filter: true}
                                                    ,{field:'tailorQcodeID', title:'二维码', align:'center', width:120, sort: true, filter: true}
                                                    ,{field:'weight', title:'重量', align:'center', width:90, sort: true, filter: true}
                                                    ,{field:'batch', title:'卷次', align:'center', width:90, sort: true, filter: true}
                                                    ,{field:'groupName', title:'组名', align:'center', width:80, sort: true, filter: true}
                                                    ,{fixed: 'right', field: 'tailorID', title:'操作', align:'center', toolbar: '#barTop', width:220}

                                                ]]
                                                ,data: reportData   // 将新数据重新载入表格
                                                ,overflow: 'tips'
                                                ,done: function () {
                                                    soulTable.render(this);
                                                    layer.close(load);
                                                }
                                                ,filter: {
                                                    bottom: true,
                                                    clearFilter: false,
                                                    items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                                                }
                                            });

                                        } else {
                                            layer.msg("获取失败！", {icon: 2});
                                        }
                                    }, error: function () {
                                        layer.msg("获取失败！", {icon: 2});
                                    }
                                });
                                return false;
                            }else {
                                layer.msg('修改失败！');
                            }
                        },
                        error:function(){
                            layer.msg('修改失败！');
                        }
                    });
                    layer.close(index);
                    $("#updateInitCount").val('');
                    $("#updateLayerCount").val('');
                }, cancel : function (i,layero) {
                    $("#updateInitCount").val('');
                    $("#updateLayerCount").val('');
                }
            });
        }
        else if (obj.event === 'weight'){
            var index = layer.open({
                type: 1 //Page层类型
                , title: "修改重量"
                , btn: ['保存']
                , shade: 0.6 //遮罩透明度
                , area: ['400px','250px']
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , content: "<div><table style='padding-top: 10px'><tr><td><label class='layui-form-label' style='width: 150px'>原始重量</label></td><td><input autocomplete='off' id='updateInitWeight' class='layui-input'></td></tr>" +
                    "<tr><td><label class='layui-form-label' style='width: 150px'>修改成</label></td><td><input autocomplete='off' id='updateWeight' class='layui-input'></td></tr></table></div>"
                ,success: function(layero, index){
                    $("#updateInitWeight").val(data.weight);
                    $("#updateWeight").val(data.weight);
                }
                ,yes: function(index, layero){
                    var bedNumber = data.bedNumber;
                    var batch = data.batch;
                    var thisTailorList = [];
                    var thisWeight = $("#updateWeight").val();
                    if (thisWeight === '' || thisWeight == null){
                        layer.msg("请填写正确数量");
                        return false;
                    }
                    $(globalTailorList).each(function (index, item) {
                        if (item.bedNumber == bedNumber && item.batch == batch){
                            item.weight = thisWeight;
                            thisTailorList.push(item);
                        }
                    });
                    $.ajax({
                        url: "/erp/updatetailordata",
                        type:'POST',
                        data: {
                            tailorList: JSON.stringify(thisTailorList)
                        },
                        success:function(data){
                            if(data == 0) {
                                layer.msg('修改成功！');
                                $.ajax({
                                    url: "/erp/getalltailorbyorderbed",
                                    type: 'GET',
                                    data: param,
                                    success: function (res) {
                                        if (res.tailorList) {
                                            var reportData = res.tailorList;
                                            globalTailorList = res.tailorList;
                                            layui.table.reload("printTailorTable", {
                                                cols:[[
                                                    {type:'checkbox'}
                                                    ,{type:'numbers', align:'center', title:'序号', width:50}
                                                    ,{field:'clothesVersionNumber', title:'单号', align:'center', width:120, sort: true, filter: true, totalRowText: '合计'}
                                                    ,{field:'orderName', title:'款号', align:'center', width:120, sort: true, filter: true}
                                                    ,{field:'customerName', title:'客户', align:'center', width:90, sort: true, filter: true}
                                                    ,{field:'bedNumber', title:'床号', align:'center', width:90, sort: true, filter: true}
                                                    ,{field:'jarName', title:'缸号', align:'center', width:90, sort: true, filter: true}
                                                    ,{field:'colorName', title:'颜色', align:'center', width:90, sort: true, filter: true}
                                                    ,{field:'sizeName', title:'尺码', align:'center', width:90, sort: true, filter: true}
                                                    ,{field:'partName', title:'部位', align:'center', width:100, sort: true, filter: true}
                                                    ,{field:'layerCount', title:'数量', align:'center', width:70, sort: true, filter: true}
                                                    ,{field:'initCount', title:'原始数量', align:'center', width:90, sort: true, filter: true}
                                                    ,{field:'packageNumber', title:'扎号', align:'center', width:90, sort: true, filter: true}
                                                    ,{field:'tailorQcodeID', title:'二维码', align:'center', width:120, sort: true, filter: true}
                                                    ,{field:'weight', title:'重量', align:'center', width:90, sort: true, filter: true}
                                                    ,{field:'batch', title:'卷次', align:'center', width:90, sort: true, filter: true}
                                                    ,{field:'groupName', title:'组名', align:'center', width:80, sort: true, filter: true}
                                                    ,{fixed: 'right', field: 'tailorID', title:'操作', align:'center', toolbar: '#barTop', width:220}

                                                ]]
                                                ,data: reportData   // 将新数据重新载入表格
                                                ,overflow: 'tips'
                                                ,done: function () {
                                                    soulTable.render(this);
                                                    layer.close(load);
                                                }
                                                ,filter: {
                                                    bottom: true,
                                                    clearFilter: false,
                                                    items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                                                }
                                            });

                                        } else {
                                            layer.msg("获取失败！", {icon: 2});
                                        }
                                    }, error: function () {
                                        layer.msg("获取失败！", {icon: 2});
                                    }
                                });
                                return false;
                            }else {
                                layer.msg('修改失败！');
                            }
                        },
                        error:function(){
                            layer.msg('修改失败！');
                        }
                    });
                    layer.close(index);
                    $("#updateInitWeight").val('');
                    $("#updateWeight").val('');
                }, cancel : function (i,layero) {
                    $("#updateInitWeight").val('');
                    $("#updateWeight").val('');
                }
            });
        }
        else if(obj.event === 'group'){
            var index = layer.open({
                type: 1 //Page层类型
                , title: "修改组名"
                , btn: ['保存']
                , shade: 0.6 //遮罩透明度
                , area: ['400px','550px']
                , maxmin: false //允许全屏最小化
                , anim: 0 //0-6的动画形式，-1不开启
                , content: "<div><table style='padding-top: 10px' class=\"layui-form\">" +
                    "<tr><td><label class='layui-form-label' style='width: 150px'>修改成</label></td><td><select autocomplete='off' id='updateGroup' class='layui-select'></select></td></tr></table></div>"
                ,success: function(layero, index){
                    $("#updateGroup").val(data.groupName);
                    form.render('select');
                }
                ,yes: function(index, layero){
                    var bedNumber = data.bedNumber;
                    var thisTailorList = [];
                    var thisGroupName = $("#updateGroup").val();
                    if (thisGroupName === '' || thisGroupName == null){
                        layer.msg("请选择组名");
                        return false;
                    }
                    $(globalTailorList).each(function (index, item) {
                        if (item.bedNumber == bedNumber){
                            item.groupName = thisGroupName;
                            thisTailorList.push(item);
                        }
                    });
                    $.ajax({
                        url: "/erp/updatetailordata",
                        type:'POST',
                        data: {
                            tailorList: JSON.stringify(thisTailorList)
                        },
                        success:function(data){
                            if(data == 0) {
                                layer.msg('修改成功！');

                                $.ajax({
                                    url: "/erp/getalltailorbyorderbed",
                                    type: 'GET',
                                    data: param,
                                    success: function (res) {
                                        if (res.tailorList) {
                                            var reportData = res.tailorList;
                                            globalTailorList = res.tailorList;
                                            layui.table.reload("printTailorTable", {
                                                cols:[[
                                                    {type:'checkbox'}
                                                    ,{type:'numbers', align:'center', title:'序号', width:50}
                                                    ,{field:'clothesVersionNumber', title:'单号', align:'center', width:120, sort: true, filter: true, totalRowText: '合计'}
                                                    ,{field:'orderName', title:'款号', align:'center', width:120, sort: true, filter: true}
                                                    ,{field:'customerName', title:'客户', align:'center', width:90, sort: true, filter: true}
                                                    ,{field:'bedNumber', title:'床号', align:'center', width:90, sort: true, filter: true}
                                                    ,{field:'jarName', title:'缸号', align:'center', width:90, sort: true, filter: true}
                                                    ,{field:'colorName', title:'颜色', align:'center', width:90, sort: true, filter: true}
                                                    ,{field:'sizeName', title:'尺码', align:'center', width:90, sort: true, filter: true}
                                                    ,{field:'partName', title:'部位', align:'center', width:100, sort: true, filter: true}
                                                    ,{field:'layerCount', title:'数量', align:'center', width:70, sort: true, filter: true}
                                                    ,{field:'initCount', title:'原始数量', align:'center', width:90, sort: true, filter: true}
                                                    ,{field:'packageNumber', title:'扎号', align:'center', width:90, sort: true, filter: true}
                                                    ,{field:'tailorQcodeID', title:'二维码', align:'center', width:120, sort: true, filter: true}
                                                    ,{field:'weight', title:'重量', align:'center', width:90, sort: true, filter: true}
                                                    ,{field:'batch', title:'卷次', align:'center', width:90, sort: true, filter: true}
                                                    ,{field:'groupName', title:'组名', align:'center', width:80, sort: true, filter: true}
                                                    ,{fixed: 'right', field: 'tailorID', title:'操作', align:'center', toolbar: '#barTop', width:220}

                                                ]]
                                                ,data: reportData   // 将新数据重新载入表格
                                                ,overflow: 'tips'
                                                ,done: function () {
                                                    soulTable.render(this);
                                                    layer.close(load);
                                                }
                                                ,filter: {
                                                    bottom: true,
                                                    clearFilter: false,
                                                    items:['column','data','condition'] // 只显示表格列和导出excel两个菜单项
                                                }
                                            });

                                        } else {
                                            layer.msg("获取失败！", {icon: 2});
                                        }
                                    }, error: function () {
                                        layer.msg("获取失败！", {icon: 2});
                                    }
                                });
                                return false;
                            }else {
                                layer.msg('修改失败！');
                            }
                        },
                        error:function(){
                            layer.msg('修改失败！');
                        }
                    });
                    layer.close(index);
                }
                , cancel : function (i,layero) {
                }
            });
            setTimeout(function (){
                $.ajax({
                    url: "/erp/listGroupNameByDepartment",
                    data: {"departmentName": "裁床"},
                    success:function(res){
                        if (res.data) {
                            $.each(res.data, function(index,element){
                                if (element === data.groupName){
                                    $("#updateGroup").append("<option selected value='"+element+"'>"+element+"</option>");
                                }
                                else {
                                    $("#updateGroup").append("<option value='"+element+"'>"+element+"</option>");
                                }

                            })
                            form.render('select');
                        }
                    },
                    error:function(){
                    }
                });
            }, 200);
        }
    });
});

function CreateAllPages(){
    for (var i = 0; i < printTailorList.length; i ++){
        var orderNameLine = printTailorList[i].orderName;
        var orderNameNew = orderNameLine.substring(orderNameLine.length-13);
        var clothesVersionNumberLine = printTailorList[i].clothesVersionNumber;
        var clothesVersionNumberNew = clothesVersionNumberLine.substring(clothesVersionNumberLine.length-13);
        LODOP.NewPage();
        // LODOP.ADD_PRINT_IMAGE("9mm","-3mm","40mm","15mm","<img border='0' src='/images/deyoo.png'>");
        // LODOP.SET_PRINT_STYLEA(0,"Stretch",1);//(可变形)扩展缩放模式
        LODOP.ADD_PRINT_TEXTA("text00","16mm","0mm","35mm","4mm",'德悦服饰');
        LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
        LODOP.ADD_PRINT_TEXTA("text01","22mm","0mm","35mm","4mm",orderNameNew);
        LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
        LODOP.ADD_PRINT_TEXTA("text01","26mm","0mm","35mm","4mm",clothesVersionNumberNew);
        LODOP.SET_PRINT_STYLEA(0,"Alignment",2);

        LODOP.ADD_PRINT_TEXTA("text03","30mm","0mm","35mm","4mm",printTailorList[i].jarName+"缸");
        LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
        //颜色  一行
        // LODOP.SET_PRINT_STYLE("Bold",1);
        LODOP.ADD_PRINT_TEXTA("text04","34mm","0mm","35mm","4mm",printTailorList[i].colorName);
        LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
        //尺码  一行
        // LODOP.SET_PRINT_STYLE("Bold",1);
        LODOP.ADD_PRINT_TEXTA("text05","38mm","0mm","35mm","4mm", printTailorList[i].sizeName);
        LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
        //床次
        // LODOP.SET_PRINT_STYLE("Bold",1);
        LODOP.ADD_PRINT_TEXTA("text06","42mm","0mm","35mm","4mm",printTailorList[i].bedNumber+"床");
        LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
        //扎号
        // LODOP.SET_PRINT_STYLE("Bold",1);
        LODOP.ADD_PRINT_TEXTA("text07","46mm","0mm","35mm","4mm",printTailorList[i].packageNumber+"扎");
        LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
        //件数
        // LODOP.SET_PRINT_STYLE("Bold",1);
        LODOP.ADD_PRINT_TEXTA("text08","50mm","0mm","16mm","4mm",printTailorList[i].layerCount+"件");
        LODOP.SET_PRINT_STYLEA(0,"Alignment",3);
        //组名
        // LODOP.SET_PRINT_STYLE("Bold",1);
        LODOP.ADD_PRINT_TEXTA("text09","50mm","18mm","16mm","4mm",printTailorList[i].groupName);
        LODOP.SET_PRINT_STYLEA(0,"Alignment",1);

        // LODOP.SET_PRINT_STYLE("Bold",0);
        LODOP.ADD_PRINT_TEXTA("text10","54mm","1mm","33mm","4mm",printTailorList[i].customerName);
        LODOP.SET_PRINT_STYLEA(0,"Alignment",2);

        // LODOP.SET_PRINT_STYLE("Bold",1);
        LODOP.ADD_PRINT_TEXTA("text11","58mm","1mm","33mm","4mm",printTailorList[i].partName);
        LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
        if (printTailorList[i].partName.indexOf("士啤") < 0){
            // LODOP.SET_PRINT_STYLE("Bold",0);
            LODOP.ADD_PRINT_BARCODE("64mm","11mm","18mm","18mm","QRCode",printTailorList[i].tailorQcodeID);
            LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
            LODOP.ADD_PRINT_TEXTA("text12","77mm","1mm","33mm","3mm",printTailorList[i].tailorQcodeID);
            LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
        }
    }

}

// function CreateOnePage(clothesVersionNumber, orderName, jarName, colorName, bedNumber, packageNumber, layerCount, sizeName, groupName, customerName, partName,qcodeid){
//     LODOP.NewPage();
//     LODOP.ADD_PRINT_IMAGE("9mm","-3mm","40mm","15mm","<img border='0' src='/images/deyoo.png'>");
//     LODOP.SET_PRINT_STYLEA(0,"Stretch",1);//(可变形)扩展缩放模式
//
//     LODOP.ADD_PRINT_TEXTA("text01","22mm","1mm","33mm","4mm",clothesVersionNumber);
//     LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
//     LODOP.ADD_PRINT_TEXTA("text02","26mm","1mm","33mm","4mm",orderName);
//     LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
//     LODOP.ADD_PRINT_TEXTA("text03","30mm","1mm","33mm","4mm",jarName+"缸");
//     LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
//     //颜色  一行
//     LODOP.SET_PRINT_STYLE("Bold",1);
//     LODOP.ADD_PRINT_TEXTA("text04","34mm","1mm","33mm","4mm",colorName);
//     LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
//     //尺码  一行
//     LODOP.SET_PRINT_STYLE("Bold",1);
//     LODOP.ADD_PRINT_TEXTA("text05","38mm","1mm","33mm","4mm", sizeName);
//     LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
//     //床次
//     LODOP.SET_PRINT_STYLE("Bold",1);
//     LODOP.ADD_PRINT_TEXTA("text06","42mm","0mm","16mm","4mm",bedNumber+"床");
//     LODOP.SET_PRINT_STYLEA(0,"Alignment",3);
//     //扎号
//     LODOP.SET_PRINT_STYLE("Bold",1);
//     LODOP.ADD_PRINT_TEXTA("text07","42mm","18mm","16mm","4mm",packageNumber+"扎");
//     LODOP.SET_PRINT_STYLEA(0,"Alignment",1);
//     //件数
//     LODOP.SET_PRINT_STYLE("Bold",1);
//     LODOP.ADD_PRINT_TEXTA("text08","46mm","0mm","16mm","4mm",layerCount+"件");
//     LODOP.SET_PRINT_STYLEA(0,"Alignment",3);
//     //组名
//     LODOP.SET_PRINT_STYLE("Bold",1);
//     LODOP.ADD_PRINT_TEXTA("text09","46mm","18mm","16mm","4mm",groupName);
//     LODOP.SET_PRINT_STYLEA(0,"Alignment",1);
//
//     LODOP.SET_PRINT_STYLE("Bold",0);
//     LODOP.ADD_PRINT_TEXTA("text10","50mm","1mm","33mm","4mm",customerName);
//     LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
//
//     LODOP.SET_PRINT_STYLE("Bold",1);
//     LODOP.ADD_PRINT_TEXTA("text11","54mm","1mm","33mm","4mm",partName);
//     LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
//     if (partName.indexOf("士啤") < 0){
//         LODOP.SET_PRINT_STYLE("Bold",0);
//         LODOP.ADD_PRINT_BARCODE("58mm","11mm","18mm","18mm","QRCode",qcodeid);
//         LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
//         LODOP.ADD_PRINT_TEXTA("text12","71mm","1mm","33mm","3mm",qcodeid);
//         LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
//     }
//     // LODOP.PREVIEW();
// }