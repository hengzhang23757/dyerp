/**

 @Name：layuiAdmin（iframe版） 消息中心
 @Author：贤心
 @Site：http://www.layui.com/admin/
 @License：LPPL
    
 */

var pageWidth;
$(document).ready(function () {
  var pageWidth = $(document.body).width();
});
layui.config({
  base: '/js/ext/',   // 模块目录
  version: 'v1.5.27'
}).extend({                         // 模块别名
  soulTable: 'soulTable'
});
layui.define(['admin', 'table', 'soulTable', 'util', 'yutons_sug'], function(exports){
  var $ = layui.$
  ,admin = layui.admin
  ,table = layui.table
  ,soulTable = layui.soulTable
  ,element = layui.element;
  
  var DISABLED = 'layui-btn-disabled'
  
  //区分各选项卡中的表格
  ,tabs = {
    all: {
      text: '全部消息'
      ,id: 'LAY-app-message-all'
    }
    ,send: {
      text: '已发'
      ,id: 'LAY-app-message-send'
    }
    ,notice: {
      text: '通知'
      ,id: 'LAY-app-message-notice'
    }
    ,direct: {
      text: '私信'
      ,id: 'LAY-app-message-direct'
    }
    ,order: {
      text: '跟单'
      ,id: 'LAY-app-message-order'
    }
    ,fabric: {
      text: '面料'
      ,id: 'LAY-app-message-fabric'
    }
    ,accessory: {
      text: '辅料'
      ,id: 'LAY-app-message-accessory'
    }
    ,IE: {
      text: 'IE'
      ,id: 'LAY-app-message-ie'
    }
    ,cut: {
      text: '裁床'
      ,id: 'LAY-app-message-cut'
    }
    ,sew: {
      text: '车缝'
      ,id: 'LAY-app-message-sew'
    }
    ,finish: {
      text: '后整'
      ,id: 'LAY-app-message-finish'
    }
    ,finance: {
      text: '财务'
      ,id: 'LAY-app-message-finance'
    }
  };
  
  //标题内容模板
  // var tplTitle = function(d){
  //   return '<a href="/erp/messageDetailStart?id='+ d.id +'">';
  // };
  var loginUserName = $("#loginUserName").val();




  //全部消息
  $.ajax({
    url: "/erp/getmessagebyinfo",
    type: 'GET',
    data: {},
    success: function (res) {
      if (res.data) {
        var reportData = res.data;
        table.render({
          elem: '#LAY-app-message-all'
          ,cols: [[
            {type: 'checkbox', fixed: 'left'}
            ,{field: 'messageType', title: '类型', minWidth: 60}
            ,{field: 'orderName', title: '款号', minWidth: 100}
            ,{field: 'clothesVersionNumber', title: '单号', minWidth: 100}
            ,{field: 'createUser', title: '发送者', minWidth: 80}
            ,{field: 'title', title: '标题', minWidth: 300}
            ,{field: 'readType', title: '状态', minWidth: 80, templet: function (d) {
                if (d.readType === '已完成'){
                  return "<p style='color: green'>已完成</p>";
                } else {
                  return "<p style='color: red'>未完成</p>";
                }
              }}
            ,{field: 'updateTime', title: '时间', minWidth: 90, templet: '<div>{{ layui.util.timeAgo(d.updateTime) }}</div>'}
            ,{field: 'id', title: '操作', minWidth: 60, toolbar: '#barDemo'}
          ]]
          ,loading:true
          ,data:reportData
          ,height: 'full-150'
          ,title: '全部消息'
          ,totalRow: true
          ,even: true
          ,page: true
          ,overflow: 'tips'
          ,limit:50
          ,limits:[50, 100, 200]
          ,done: function (res, curr, count) {
            soulTable.render(this);
          }
        });
      }
    }
  });

  table.on('tool(LAY-app-message-all)', function(obj){
    var data = obj.data;
    if(obj.event === 'detail'){
      return location.href="/erp/messageDetailStart?id=" + data.id;
    }
  });

  //通知
  $.ajax({
    url: "/erp/getmessagebyinfo",
    type: 'GET',
    data: {
      receiveUser: loginUserName,
      messageType: "通知"
    },
    success: function (res) {
      if (res.data) {
        var reportData = res.data;
        table.render({
          elem: '#LAY-app-message-notice'
          ,cols: [[
            {type: 'checkbox', fixed: 'left'}
            ,{field: 'messageType', title: '类型', minWidth: 60}
            ,{field: 'orderName', title: '款号', minWidth: 100}
            ,{field: 'clothesVersionNumber', title: '单号', minWidth: 100}
            ,{field: 'createUser', title: '发送者', minWidth: 80}
            ,{field: 'title', title: '标题', minWidth: 300}
            ,{field: 'readType', title: '状态', minWidth: 80, templet: function (d) {
                if (d.readType === '已完成'){
                  return "<p style='color: green'>已完成</p>";
                } else {
                  return "<p style='color: red'>未完成</p>";
                }
              }}
            ,{field: 'updateTime', title: '时间', minWidth: 90, templet: '<div>{{ layui.util.timeAgo(d.updateTime) }}</div>'}
            ,{field: 'id', title: '操作', minWidth: 60, toolbar: '#barDemo'}
          ]]
          ,loading:true
          ,data:reportData
          ,height: 'full-150'
          ,title: '通知消息'
          ,totalRow: true
          ,even: true
          ,page: true
          ,overflow: 'tips'
          ,limit:50
          ,limits:[50, 100, 200]
          ,done: function (res, curr, count) {
            soulTable.render(this);
          }
        });
      }
    }
  });

  table.on('tool(LAY-app-message-notice)', function(obj){
    var data = obj.data;
    if(obj.event === 'detail'){
      return location.href="/erp/messageDetailStart?id=" + data.id;
    }
  });

  //已发
  $.ajax({
    url: "/erp/getmessagebyinfo",
    type: 'GET',
    data: {
      sendUser: loginUserName
    },
    success: function (res) {
      if (res.data) {
        var reportData = res.data;
        table.render({
          elem: '#LAY-app-message-send'
          ,cols: [[
            {type: 'checkbox', fixed: 'left'}
            ,{field: 'messageType', title: '类型', minWidth: 60}
            ,{field: 'orderName', title: '款号', minWidth: 100}
            ,{field: 'clothesVersionNumber', title: '单号', minWidth: 100}
            ,{field: 'createUser', title: '发送者', minWidth: 80}
            ,{field: 'title', title: '标题', minWidth: 300}
            ,{field: 'readType', title: '状态', minWidth: 80, templet: function (d) {
                if (d.readType === '已完成'){
                  return "<p style='color: green'>已完成</p>";
                } else {
                  return "<p style='color: red'>未完成</p>";
                }
              }}
            ,{field: 'updateTime', title: '时间', minWidth: 90, templet: '<div>{{ layui.util.timeAgo(d.updateTime) }}</div>'}
            ,{field: 'id', title: '操作', minWidth: 60, toolbar: '#barDemo'}
          ]]
          ,loading:true
          ,data:reportData
          ,height: 'full-150'
          ,title: '已发消息'
          ,totalRow: true
          ,even: true
          ,page: true
          ,overflow: 'tips'
          ,limit:50
          ,limits:[50, 100, 200]
          ,done: function (res, curr, count) {
            soulTable.render(this);
          }
        });
      }
    }
  });

  table.on('tool(LAY-app-message-send)', function(obj){
    var data = obj.data;
    if(obj.event === 'detail'){
      return location.href="/erp/messageDetailStart?id=" + data.id;
    } else if(obj.event === 'direct'){
      var colorList = [];
      var orderName = data.orderName;
      var clothesVersionNumber = data.clothesVersionNumber;
      var label = "录入面料信息&emsp;&emsp;&emsp;&emsp;" + "单号："+clothesVersionNumber+"&emsp;&emsp;&emsp;&emsp;款号："+orderName + "名称,单位,门幅,克重,部位 必须"
      var index = layer.open({
        type: 1 //Page层类型
        , title: label
        , btn: ['保存','添加一行']
        , shade: 0.6 //遮罩透明度
        , area: '1000px'
        , maxmin: false //允许全屏最小化
        , anim: 0 //0-6的动画形式，-1不开启
        , content: "<div id=\"addFabricData\">\n" +
            "        <form class=\"layui-form\" style=\"padding-top: 40px;\">\n" +
            "            <table id=\"fabricDataTable\" lay-filter=\"fabricDataTable\"></table>\n" +
            "        </form>\n" +
            "    </div>"
        ,yes: function(i, layero){
          var fabricData = table.cache.fabricDataTable;
          var isHits = [];
          var fabricColors = [];
          var fabricColorNumberList = [];
          $.each(fabricData,function (index,item) {
            var selects = $("div[lay-id='fabricDataTable'] tr[data-index="+index+"] select[name='isHit']");
            var inputs = $("div[lay-id='fabricDataTable'] tr[data-index="+index+"] input[name='fabricColor']");
            var fabricColorNumbers = $("div[lay-id='fabricDataTable'] tr[data-index="+index+"] input[name='fabricColorNumber']");
            var tmp = [];
            var itmp = [];
            var ctmp = [];
            $.each(selects,function (s_index,s_item) {
              tmp.push(selects.eq(s_index).val());
              if(inputs.eq(s_index).val()!="") {
                itmp.push(inputs.eq(s_index).val())
              }
              if(fabricColorNumbers.eq(s_index).val()!="") {
                ctmp.push(fabricColorNumbers.eq(s_index).val())
              }else{
                ctmp.push('未输');
              }
            });
            isHits.push(tmp);
            fabricColors.push(itmp);
            fabricColorNumberList.push(ctmp);
          });

          var manufactureFabrics = [];
          var isInput = true;
          var pieceFlag = true;
          var orderPieceFlag = true;
          var lossFlag = true;
          $.each(fabricColors,function (i,item) {
            if(item.length<isHits[0].length) {
              isInput = false;
              return false;
            }
          });

          if(isInput) {
            $.each(fabricData, function (i, item) {
              if (item.fabricName == '' || item.unit == '' || item.fabricWidth == '' || item.fabricWeight == '' || item.partName == '') {
                isInput = false;
                return false;
              }
              var orderPieceUsage = 0;
              var pieceUsage = 0;
              if (item.orderPieceUsage != ''){
                if (!isNumber(item.orderPieceUsage)){
                  orderPieceFlag = false;
                  return false;
                } else {
                  orderPieceUsage = item.orderPieceUsage;
                }
              }
              if (item.pieceUsage != ''){
                if (!isNumber(item.pieceUsage)){
                  pieceFlag = false;
                  return false;
                } else {
                  pieceUsage = item.pieceUsage;
                }
              }
              $.each(colorList,function(j,value) {
                var manufactureFabric = {};
                manufactureFabric.orderName = orderName;
                manufactureFabric.clothesVersionNumber = clothesVersionNumber;
                manufactureFabric.fabricNumber = item.fabricNumber;
                manufactureFabric.fabricName = item.fabricName;
                manufactureFabric.supplier = item.supplier;
                manufactureFabric.fabricWidth = item.fabricWidth;
                manufactureFabric.unit = item.unit;
                manufactureFabric.fabricWeight = item.fabricWeight;
                manufactureFabric.partName = item.partName;
                if (item.fabricLoss == null || item.fabricLoss == ""){
                  manufactureFabric.fabricLoss = 0;
                }else {
                  if (!isNumber(item.fabricLoss)){
                    lossFlag = false;
                    return false;
                  } else {
                    manufactureFabric.fabricLoss = item.fabricLoss;
                  }
                }
                manufactureFabric.orderPieceUsage = orderPieceUsage;
                manufactureFabric.pieceUsage = pieceUsage;
                manufactureFabric.colorName = value;
                manufactureFabric.isHit = isHits[i][j];
                manufactureFabric.fabricColor = fabricColors[i][j];
                manufactureFabric.fabricColorNumber = fabricColorNumberList[i][j];
                manufactureFabric.orderState = '未下单';
                manufactureFabric.checkState = '未提交';
                manufactureFabrics.push(manufactureFabric);
              })
            });
          }

          if(!isInput) {
            layer.msg("请输入完所有内容", { icon: 2 });
          } else if(!pieceFlag) {
            layer.msg("单件用量有误", { icon: 2 });
          } else if(!orderPieceFlag) {
            layer.msg("接单用量有误", { icon: 2 });
          } else if(!lossFlag) {
            layer.msg("损耗有误", { icon: 2 });
          }else {

            $.ajax({
              url: "/erp/addmanufacturefabricbatch",
              type: 'POST',
              data: {
                manufactureFabricJson:JSON.stringify(manufactureFabrics),
              },
              success: function (res) {
                if (res.result == 0) {
                  processOrderTable.reload();
                  layer.close(index);
                  layer.msg("录入成功！", {icon: 1});
                } else {
                  layer.msg("录入失败！", {icon: 2});
                }
              },
              error: function () {
                layer.msg("录入失败！", {icon: 2});
              }
            })
          }
        }
        ,btn2: function(index, layero){
          var data = table.cache.fabricDataTable;
          data.push({
            "fabricName": ""
          });
          table.reload("fabricDataTable",{
            data:data   // 将新数据重新载入表格
          });
          return false  //开启该代码可禁止点击该按钮关闭
        }
      });
      layer.full(index);
      $.ajax({
        url: "/erp/getordercolornamesbyorder",
        type: 'GET',
        data: {
          orderName:orderName
        },
        success: function (res) {
          var title = [
            {field: 'fabricName', title: '面料名称',align:'center',minWidth:220,templet: function(d){
                return '<input class="layui-input" autocomplete="off" type="text" id="fabricName-'+d.LAY_TABLE_INDEX+'"/>'
              }},
            {field: 'fabricNumber', title: '面料号',align:'center',minWidth:150,edit:'text'},
            {field: 'supplier', title: '供应商',align:'center',minWidth:90,edit:'text'},
            {field: 'unit', title: '单位',align:'center',minWidth:90,edit:'text'},
            {field: 'fabricWidth', title: '布封',align:'center',minWidth:90,edit:'text'},
            {field: 'fabricWeight', title: '克重',align:'center',minWidth:90,edit:'text'},
            {field: 'partName', title: '部位',align:'center',minWidth:150,edit:'text'},
            {field: 'fabricLoss', title: '损耗',align:'center',minWidth:90,edit:'text'},
            {field: 'orderPieceUsage', title: '接单用量',align:'center',minWidth:120,edit:'text'},
            {field: 'pieceUsage', title: '单件用量',align:'center',minWidth:120,edit:'text'}
          ];
          colorList = res.colorNameList;
          $.each(res.colorNameList,function (index, item) {
            title.push({
              field:item,title:item,align:'center',minWidth:150,templet:function (d) {
                return '<div class="layui-input-inline">' +
                    '<select name="isHit">' +
                    '<option value="否">不撞色</option>'+
                    '<option value="是">撞色</option>'+
                    '</select>' +
                    '<input name="fabricColor" type="text" autocomplete="off" class="layui-input" value="'+item+'">' +
                    '<input name="fabricColorNumber" type="text" autocomplete="off" class="layui-input" placeholder="色号">' +
                    '</div>'
              }
            })
          });
          layui.form.render("select");
          title.push( {field: 'operation', title: '操作',minWidth:70,align:'center' ,templet: function(d){
              if(d.LAY_INDEX == 1)
                return '<a class="layui-btn layui-btn-xs" lay-event="add"><i class="layui-icon layui-icon-addition" style="margin-right:0"></i></a>'
              else
                return '<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del"><i class="layui-icon layui-icon-subtraction" style="margin-right:0"></i></a>'
            }});
          table.render({
            elem: '#fabricDataTable'
            ,cols: [title]
            ,height: 'full-100'
            ,width: pageWidth
            ,data: [
              {
                "fabricName": ""
                ,"fabricNumber": ""
                ,"supplier": ""
                ,"unit": ""
                ,"fabricWidth": ""
                ,"fabricWeight": ""
                ,"partName": ""
                ,"fabricAdd": ""
                ,"fabricLoss": ""
                ,"orderPieceUsage": ""
                ,"pieceUsage": ""
              }
            ]
            ,limit:Number.MAX_VALUE
            ,done:function (res) {
              var data = table.cache.fabricDataTable;

              $.each(res.data,function (index,item) {
                $("#fabricName-"+item.LAY_TABLE_INDEX).parent().css("overflow","unset");
                $("#fabricName-"+item.LAY_TABLE_INDEX).parent().css("height","auto");

                $("#fabricName-"+item.LAY_TABLE_INDEX).val(item.fabricName);

                $("#fabricName-"+item.LAY_TABLE_INDEX).bind("input propertychange",function(event){
                  data[item.LAY_TABLE_INDEX].fabricName = $("#fabricName-"+item.LAY_TABLE_INDEX).val();
                });

                if(data[index].isHits) {
                  $.each(data[index].isHits, function (h_index, h_item) {
                    $("div[lay-id='fabricDataTable'] tr[data-index=" + index + "] select[name='isHit']").eq(h_index).val(h_item.val());
                    $("div[lay-id='fabricDataTable'] tr[data-index=" + index + "] input[name='fabricColor']").eq(h_index).val(data[index].fabricColors[h_index].val());
                    $("div[lay-id='fabricDataTable'] tr[data-index=" + index + "] input[name='fabricColorNumber']").eq(h_index).val(data[index].fabricColorNumbers[h_index].val());
                  });
                }


                var selects = $("div[lay-id='fabricDataTable'] tr[data-index="+index+"] select[name='isHit']");
                var inputs = $("div[lay-id='fabricDataTable'] tr[data-index="+index+"] input[name='fabricColor']");
                var numberInputs = $("div[lay-id='fabricDataTable'] tr[data-index="+index+"] input[name='fabricColorNumber']");

                var isHits = [];
                var fabricColors = [];
                var fabricColorNumbers = [];

                $.each(selects,function (s_index,s_item) {
                  isHits.push(selects.eq(s_index));
                  fabricColors.push(inputs.eq(s_index));
                  fabricColorNumbers.push(numberInputs.eq(s_index));
                });
                item.isHits = isHits;
                item.fabricColors = fabricColors;
                item.fabricColorNumbers = fabricColorNumbers;

                layui.yutons_sug.render({
                  id: "fabricName-" + item.LAY_TABLE_INDEX, //设置容器唯一id
                  height: "300",
                  width: "1400",
                  limit:"10",
                  limits:[10,20,50,100],
                  cols: [
                    [{
                      field: 'fabricName',
                      title: '名称',
                      width: '15%'
                    }, {
                      field: 'fabricNumber',
                      title: '面料号',
                      width: '15%'
                    }, {
                      field: 'supplier',
                      title: '供应商',
                      width: '9%'
                    }, {
                      field: 'unit',
                      title: '单位',
                      width: '7%'
                    }, {
                      field: 'fabricWidth',
                      title: '布封',
                      width: '9%'
                    }, {
                      field: 'fabricWeight',
                      title: '克重',
                      width: '9%'
                    }, {
                      field: 'partName',
                      title: '部位',
                      width: '9%'
                    }, {
                      field: 'fabricLoss',
                      title: '损耗',
                      width: '9%'
                    }, {
                      field: 'orderPieceUsage',
                      title: '接单用量',
                      width: '9%'
                    }, {
                      field: 'pieceUsage',
                      title: '单位用量',
                      width: '9%'
                    }]
                  ], //设置表头
                  params: [
                    {
                      name: 'subFabricName',
                      field: 'fabricName'
                    }],//设置字段映射，适用于输入一个字段，回显多个字段
                  type: 'sugTable', //设置输入框提示类型：sug-下拉框，sugTable-下拉表格
                  url: "/erp/getmanufacturefabrichintbyfabricname?subFabricName=" //设置异步数据接口,url为必填项,params为字段名
                });
              });

              layui.form.render("select");


              $("select[name='isHit']").parent().parent().css("overflow","unset");
              $("select[name='isHit']").parent().parent().css("height","auto");

            }
          });
        },
        error: function () {
          layer.msg("获取订单颜色失败！", {icon: 2});
        }
      })
    }
  });
  
  //私信
  $.ajax({
    url: "/erp/getmessagebyinfo",
    type: 'GET',
    data: {
      receiveUser: loginUserName,
      messageType: "私信"
    },
    success: function (res) {
      if (res.data) {
        var reportData = res.data;
        table.render({
          elem: '#LAY-app-message-direct'
          ,cols: [[
            {type: 'checkbox', fixed: 'left'}
            ,{field: 'messageType', title: '类型', minWidth: 60}
            ,{field: 'orderName', title: '款号', minWidth: 100}
            ,{field: 'clothesVersionNumber', title: '单号', minWidth: 100}
            ,{field: 'createUser', title: '发送者', minWidth: 80}
            ,{field: 'title', title: '标题', minWidth: 300}
            ,{field: 'readType', title: '状态', minWidth: 80, templet: function (d) {
                if (d.readType === '已完成'){
                  return "<p style='color: green'>已完成</p>";
                } else {
                  return "<p style='color: red'>未完成</p>";
                }
              }}
            ,{field: 'updateTime', title: '时间', minWidth: 90, templet: '<div>{{ layui.util.timeAgo(d.updateTime) }}</div>'}
            ,{field: 'id', title: '操作', minWidth: 60, toolbar: '#barDemo'}
          ]]
          ,loading:true
          ,data:reportData
          ,height: 'full-150'
          ,title: '私信消息'
          ,totalRow: true
          ,even: true
          ,page: true
          ,overflow: 'tips'
          ,limit:50
          ,limits:[50, 100, 200]
          ,done: function (res, curr, count) {
            soulTable.render(this);
          }
        });
      }
    }
  });

  table.on('tool(LAY-app-message-direct)', function(obj){
    var data = obj.data;
    if(obj.event === 'detail'){
      return location.href="/erp/messageDetailStart?id=" + data.id;
    }
  });

  //跟单
  $.ajax({
    url: "/erp/getmessagebyinfo",
    type: 'GET',
    data: {
      receiveUser: "业务部",
      messageType: "跟单"
    },
    success: function (res) {
      if (res.data) {
        var reportData = res.data;
        table.render({
          elem: '#LAY-app-message-order'
          ,cols: [[
            {type: 'checkbox', fixed: 'left'}
            ,{field: 'messageType', title: '类型', minWidth: 60}
            ,{field: 'orderName', title: '款号', minWidth: 100}
            ,{field: 'clothesVersionNumber', title: '单号', minWidth: 100}
            ,{field: 'createUser', title: '发送者', minWidth: 80}
            ,{field: 'title', title: '标题', minWidth: 300}
            ,{field: 'readType', title: '状态', minWidth: 80, templet: function (d) {
                if (d.readType === '已完成'){
                  return "<p style='color: green'>已完成</p>";
                } else {
                  return "<p style='color: red'>未完成</p>";
                }
              }}
            ,{field: 'updateTime', title: '时间', minWidth: 90, templet: '<div>{{ layui.util.timeAgo(d.updateTime) }}</div>'}
            ,{field: 'id', title: '操作', minWidth: 60, toolbar: '#barDemo'}
          ]]
          ,loading:true
          ,data:reportData
          ,height: 'full-150'
          ,title: '跟单消息'
          ,totalRow: true
          ,even: true
          ,page: true
          ,overflow: 'tips'
          ,limit:50
          ,limits:[50, 100, 200]
          ,done: function (res, curr, count) {
            soulTable.render(this);
          }
        });
      }
    }
  });

  table.on('tool(LAY-app-message-order)', function(obj){
    var data = obj.data;
    if(obj.event === 'detail'){
      return location.href="/erp/messageDetailStart?id=" + data.id;
    }
  });

  //面料
  $.ajax({
    url: "/erp/getmessagebyinfo",
    type: 'GET',
    data: {
      receiveUser: "面料跟单",
      messageType: "面料"
    },
    success: function (res) {
      if (res.data) {
        var reportData = res.data;
        table.render({
          elem: '#LAY-app-message-fabric'
          ,cols: [[
            {type: 'checkbox', fixed: 'left'}
            ,{field: 'messageType', title: '类型', minWidth: 60}
            ,{field: 'orderName', title: '款号', minWidth: 100}
            ,{field: 'clothesVersionNumber', title: '单号', minWidth: 100}
            ,{field: 'createUser', title: '发送者', minWidth: 80}
            ,{field: 'title', title: '标题', minWidth: 300}
            ,{field: 'readType', title: '状态', minWidth: 80, templet: function (d) {
                if (d.readType === '已完成'){
                  return "<p style='color: green'>已完成</p>";
                } else {
                  return "<p style='color: red'>未完成</p>";
                }
              }}
            ,{field: 'updateTime', title: '时间', minWidth: 90, templet: '<div>{{ layui.util.timeAgo(d.updateTime) }}</div>'}
            ,{field: 'id', title: '操作', minWidth: 60, toolbar: '#barDemo'}
          ]]
          ,loading:true
          ,data:reportData
          ,height: 'full-150'
          ,title: '面料消息'
          ,totalRow: true
          ,even: true
          ,page: true
          ,overflow: 'tips'
          ,limit:50
          ,limits:[50, 100, 200]
          ,done: function (res, curr, count) {
            soulTable.render(this);
          }
        });
      }
    }
  });

  table.on('tool(LAY-app-message-fabric)', function(obj){
    var data = obj.data;
    if(obj.event === 'detail'){
      return location.href="/erp/messageDetailStart?id=" + data.id;
    }
  });

  //辅料
  $.ajax({
    url: "/erp/getmessagebyinfo",
    type: 'GET',
    data: {
      receiveUser: "辅料跟单",
      messageType: "辅料"
    },
    success: function (res) {
      if (res.data) {
        var reportData = res.data;
        table.render({
          elem: '#LAY-app-message-accessory'
          ,cols: [[
            {type: 'checkbox', fixed: 'left'}
            ,{field: 'messageType', title: '类型', minWidth: 60}
            ,{field: 'orderName', title: '款号', minWidth: 100}
            ,{field: 'clothesVersionNumber', title: '单号', minWidth: 100}
            ,{field: 'createUser', title: '发送者', minWidth: 80}
            ,{field: 'title', title: '标题', minWidth: 300}
            ,{field: 'readType', title: '状态', minWidth: 80, templet: function (d) {
                if (d.readType === '已完成'){
                  return "<p style='color: green'>已完成</p>";
                } else {
                  return "<p style='color: red'>未完成</p>";
                }
              }}
            ,{field: 'updateTime', title: '时间', minWidth: 90, templet: '<div>{{ layui.util.timeAgo(d.updateTime) }}</div>'}
            ,{field: 'id', title: '操作', minWidth: 60, toolbar: '#barDemo'}
          ]]
          ,loading:true
          ,data:reportData
          ,height: 'full-150'
          ,title: '辅料消息'
          ,totalRow: true
          ,even: true
          ,page: true
          ,overflow: 'tips'
          ,limit:50
          ,limits:[50, 100, 200]
          ,done: function (res, curr, count) {
            soulTable.render(this);
          }
        });
      }
    }
  });


  table.on('tool(LAY-app-message-accessory)', function(obj){
    var data = obj.data;
    if(obj.event === 'detail'){
      return location.href="/erp/messageDetailStart?id=" + data.id;
    }
  });


  //IE
  $.ajax({
    url: "/erp/getmessagebyinfo",
    type: 'GET',
    data: {
      receiveUser: "IE",
      messageType: "IE"
    },
    success: function (res) {
      if (res.data) {
        var reportData = res.data;
        table.render({
          elem: '#LAY-app-message-ie'
          ,cols: [[
            {type: 'checkbox', fixed: 'left'}
            ,{field: 'messageType', title: '类型', minWidth: 60}
            ,{field: 'orderName', title: '款号', minWidth: 100}
            ,{field: 'clothesVersionNumber', title: '单号', minWidth: 100}
            ,{field: 'createUser', title: '发送者', minWidth: 80}
            ,{field: 'title', title: '标题', minWidth: 300}
            ,{field: 'readType', title: '状态', minWidth: 80, templet: function (d) {
                if (d.readType === '已完成'){
                  return "<p style='color: green'>已完成</p>";
                } else {
                  return "<p style='color: red'>未完成</p>";
                }
              }}
            ,{field: 'updateTime', title: '时间', minWidth: 90, templet: '<div>{{ layui.util.timeAgo(d.updateTime) }}</div>'}
            ,{field: 'id', title: '操作', minWidth: 60, toolbar: '#barDemo'}
          ]]
          ,loading:true
          ,data:reportData
          ,height: 'full-150'
          ,title: 'IE消息'
          ,totalRow: true
          ,even: true
          ,page: true
          ,overflow: 'tips'
          ,limit:50
          ,limits:[50, 100, 200]
          ,done: function (res, curr, count) {
            soulTable.render(this);
          }
        });
      }
    }
  });

  table.on('tool(LAY-app-message-ie)', function(obj){
    var data = obj.data;
    if(obj.event === 'detail'){
      return location.href="/erp/messageDetailStart?id=" + data.id;
    }
  });

  //裁床
  $.ajax({
    url: "/erp/getmessagebyinfo",
    type: 'GET',
    data: {
      receiveUser: "裁床",
      messageType: "裁床"
    },
    success: function (res) {
      if (res.data) {
        var reportData = res.data;
        table.render({
          elem: '#LAY-app-message-cut'
          ,cols: [[
            {type: 'checkbox', fixed: 'left'}
            ,{field: 'messageType', title: '类型', minWidth: 60}
            ,{field: 'orderName', title: '款号', minWidth: 100}
            ,{field: 'clothesVersionNumber', title: '单号', minWidth: 100}
            ,{field: 'createUser', title: '发送者', minWidth: 80}
            ,{field: 'title', title: '标题', minWidth: 300}
            ,{field: 'readType', title: '状态', minWidth: 80, templet: function (d) {
                if (d.readType === '已完成'){
                  return "<p style='color: green'>已完成</p>";
                } else {
                  return "<p style='color: red'>未完成</p>";
                }
              }}
            ,{field: 'updateTime', title: '时间', minWidth: 90, templet: '<div>{{ layui.util.timeAgo(d.updateTime) }}</div>'}
            ,{field: 'id', title: '操作', minWidth: 60, toolbar: '#barDemo'}
          ]]
          ,loading:true
          ,data:reportData
          ,height: 'full-150'
          ,title: '裁床消息'
          ,totalRow: true
          ,even: true
          ,page: true
          ,overflow: 'tips'
          ,limit:50
          ,limits:[50, 100, 200]
          ,done: function (res, curr, count) {
            soulTable.render(this);
          }
        });
      }
    }
  });

  table.on('tool(LAY-app-message-cut)', function(obj){
    var data = obj.data;
    if(obj.event === 'detail'){
      return location.href="/erp/messageDetailStart?id=" + data.id;
    }
  });

  //车缝
  $.ajax({
    url: "/erp/getmessagebyinfo",
    type: 'GET',
    data: {
      receiveUser: "车缝",
      messageType: "车缝"
    },
    success: function (res) {
      if (res.data) {
        var reportData = res.data;
        table.render({
          elem: '#LAY-app-message-sew'
          ,cols: [[
            {type: 'checkbox', fixed: 'left'}
            ,{field: 'messageType', title: '类型', minWidth: 60}
            ,{field: 'orderName', title: '款号', minWidth: 100}
            ,{field: 'clothesVersionNumber', title: '单号', minWidth: 100}
            ,{field: 'createUser', title: '发送者', minWidth: 80}
            ,{field: 'title', title: '标题', minWidth: 300}
            ,{field: 'readType', title: '状态', minWidth: 80, templet: function (d) {
                if (d.readType === '已完成'){
                  return "<p style='color: green'>已完成</p>";
                } else {
                  return "<p style='color: red'>未完成</p>";
                }
              }}
            ,{field: 'updateTime', title: '时间', minWidth: 90, templet: '<div>{{ layui.util.timeAgo(d.updateTime) }}</div>'}
            ,{field: 'id', title: '操作', minWidth: 60, toolbar: '#barDemo'}
          ]]
          ,loading:true
          ,data:reportData
          ,height: 'full-150'
          ,title: '车缝消息'
          ,totalRow: true
          ,even: true
          ,page: true
          ,overflow: 'tips'
          ,limit:50
          ,limits:[50, 100, 200]
          ,done: function (res, curr, count) {
            soulTable.render(this);
          }
        });
      }
    }
  });

  table.on('tool(LAY-app-message-sew)', function(obj){
    var data = obj.data;
    if(obj.event === 'detail'){
      return location.href="/erp/messageDetailStart?id=" + data.id;
    }
  });

  //后整
  $.ajax({
    url: "/erp/getmessagebyinfo",
    type: 'GET',
    data: {
      receiveUser: "后整",
      messageType: "后整"
    },
    success: function (res) {
      if (res.data) {
        var reportData = res.data;
        table.render({
          elem: '#LAY-app-message-finish'
          ,cols: [[
            {type: 'checkbox', fixed: 'left'}
            ,{field: 'messageType', title: '类型', minWidth: 60}
            ,{field: 'orderName', title: '款号', minWidth: 100}
            ,{field: 'clothesVersionNumber', title: '单号', minWidth: 100}
            ,{field: 'createUser', title: '发送者', minWidth: 80}
            ,{field: 'title', title: '标题', minWidth: 300}
            ,{field: 'readType', title: '状态', minWidth: 80, templet: function (d) {
                if (d.readType === '已完成'){
                  return "<p style='color: green'>已完成</p>";
                } else {
                  return "<p style='color: red'>未完成</p>";
                }
              }}
            ,{field: 'updateTime', title: '时间', minWidth: 90, templet: '<div>{{ layui.util.timeAgo(d.updateTime) }}</div>'}
            ,{field: 'id', title: '操作', minWidth: 60, toolbar: '#barDemo'}
          ]]
          ,loading:true
          ,data:reportData
          ,height: 'full-150'
          ,title: '后整消息'
          ,totalRow: true
          ,even: true
          ,page: true
          ,overflow: 'tips'
          ,limit:50
          ,limits:[50, 100, 200]
          ,done: function (res, curr, count) {
            soulTable.render(this);
          }
        });
      }
    }
  });

  table.on('tool(LAY-app-message-finish)', function(obj){
    var data = obj.data;
    if(obj.event === 'detail'){
      return location.href="/erp/messageDetailStart?id=" + data.id;
    }
  });


  //财务
  $.ajax({
    url: "/erp/getmessagebyinfo",
    type: 'GET',
    data: {
      receiveUser: "财务",
      messageType: "财务"
    },
    success: function (res) {
      if (res.data) {
        var reportData = res.data;
        table.render({
          elem: '#LAY-app-message-finance'
          ,cols: [[
            {type: 'checkbox', fixed: 'left'}
            ,{field: 'messageType', title: '类型', minWidth: 60}
            ,{field: 'orderName', title: '款号', minWidth: 100}
            ,{field: 'clothesVersionNumber', title: '单号', minWidth: 100}
            ,{field: 'createUser', title: '发送者', minWidth: 80}
            ,{field: 'title', title: '标题', minWidth: 300}
            ,{field: 'readType', title: '状态', minWidth: 80, templet: function (d) {
                if (d.readType === '已完成'){
                  return "<p style='color: green'>已完成</p>";
                } else {
                  return "<p style='color: red'>未完成</p>";
                }
              }}
            ,{field: 'updateTime', title: '时间', minWidth: 90, templet: '<div>{{ layui.util.timeAgo(d.updateTime) }}</div>'}
            ,{field: 'id', title: '操作', minWidth: 60, toolbar: '#barDemo'}
          ]]
          ,loading:true
          ,data:reportData
          ,height: 'full-150'
          ,title: '后整消息'
          ,totalRow: true
          ,even: true
          ,page: true
          ,overflow: 'tips'
          ,limit:50
          ,limits:[50, 100, 200]
          ,done: function (res, curr, count) {
            soulTable.render(this);
          }
        });
      }
    }
  });

  table.on('tool(LAY-app-message-finance)', function(obj){
    var data = obj.data;
    if(obj.event === 'detail'){
      return location.href="/erp/messageDetailStart?id=" + data.id;
    }
  });

  
  //事件处理
  var events = {
    del: function(othis, type){
      var thisTabs = tabs[type]
      ,checkStatus = table.checkStatus(thisTabs.id)
      ,data = checkStatus.data; //获得选中的数据
      if(data.length === 0) return layer.msg('未选中行');

      layer.confirm('确定删除选中的数据吗？', function(){
        /*
        admin.req('url', {}, function(){ //请求接口
          //do somethin
        });
        */
        //此处只是演示，实际应用需把下述代码放入上述Ajax回调中
        layer.msg('删除成功', {
          icon: 1
        });
        table.reload(thisTabs.id); //刷新表格
      });
    }
    ,ready: function(othis, type){
      var thisTabs = tabs[type]
      ,checkStatus = table.checkStatus(thisTabs.id)
      ,data = checkStatus.data; //获得选中的数据
      if(data.length === 0) return layer.msg('未选中行');
      var idList = [];
      for (var i = 0; i < data.length; i ++){
        idList.push(data[i].id)
      }
      $.ajax({
        url: "/erp/updatemessagereadtype",
        type: 'POST',
        traditional: true,
        data: {
          idList: idList
        },
        success: function (data) {
          if(data.res == 0) {
            layer.msg('标记已读成功', {icon: 1});
          } else {
            layer.msg('标记已读失败');
          }
        }, error: function () {
          swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }
      });
      table.reload(thisTabs.id); //刷新表格
    }
    ,readyAll: function(othis, type){
      var thisTabs = tabs[type];
      var userName = $("#loginUserName").val();
      $.ajax({
        url: "/erp/updatemessagereadtypebyreceiveuser",
        type: 'POST',
        data: {
          receiveUser: userName
        },
        success: function (data) {
          if(data.res == 0) {
            layer.msg('全部标记已读成功', {icon: 1});
          } else {
            layer.msg('标记失败');
          }
        }, error: function () {
          swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
        }
      });
      table.reload(thisTabs.id); //刷新表格
    }
  };
  
  $('.LAY-app-message-btns .layui-btn').on('click', function(){
    var othis = $(this)
    ,thisEvent = othis.data('events')
    ,type = othis.data('type');
    events[thisEvent] && events[thisEvent].call(this, othis, type);
  });
  
  exports('message', {});
});
