<#macro search>
    <section id="content">
        <section class="vbox">
            <section class="scrollable">
                <div class="layui-row">
                    <form class="layui-form" style="margin: 0 auto;padding-top: 3px;" lay-filter="baseInfo">
                        <table>
                            <tr>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label">单号</label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <input type="text" name="clothesVersionNumber" id="clothesVersionNumber" placeholder="请输入单号" class="layui-input">
                                </td>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label">款号</label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <input type="text" name="orderName" id="orderName" placeholder="请输入款号" class="layui-input">
                                </td>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label">颜色</label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <select type="text" name="colorName" id="colorName"></select>
                                </td>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label">尺码</label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <select type="text" name="sizeName" id="sizeName"></select>
                                </td>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label"></label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <div class="layui-input-inline">
                                        <button class="layui-btn" lay-submit lay-filter="searchBeat">搜索</button>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
                <table id="cutStorageTable" lay-filter="cutStorageTable"></table>
                <script type="text/html" id="toolbarTop">
                    <div class="layui-btn-container">
                        <button class="layui-btn layui-btn-sm" lay-event="refresh">刷新</button>
                        <button class="layui-btn layui-btn-sm" lay-event="clearFilter">清除所有筛选</button>
                        <button class="layui-btn layui-btn-sm" lay-event="exportExcel">导出</button>
                    </div>
                </script>
                <script type="text/html" id="barTop">
                    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
                </script>
            </section>
        </section>
    </section>
    <link rel="stylesheet" href="/css/layui.css" type="text/css">
    <link rel="stylesheet" href="/css/soulTable.css" type="text/css">
    <script src="/js/cutMarket/allStorage.js?t=${currentDate?c}"></script>

</#macro>

<style>
    button {
        background:rgb(45, 202, 147);
        opacity:0.86;
        color: white;
        font-family: PingFangSC-Semibold, sans-serif;
    }
    ::-webkit-scrollbar-track-piece {

        background-color:#f8f8f8;

    }

    ::-webkit-scrollbar {

        width:15px;

        height:15px;

    }

    ::-webkit-scrollbar-thumb {

        background-color:#dddddd;

        background-clip:padding-box;

        min-height:28px;

    }

    ::-webkit-scrollbar-thumb:hover {

        background-color:#bbb;

    }
</style>