<section class="panel panel-default">

    <div id="tableDiv">

        <table class="table table-striped table-bordered " id="cutStorageTable">
            <#if cutStorageQueryList??>
                <thead>
                <tr bgcolor="#ffcb99" style="color: black;">
                    <th style="width: 8%;text-align:left;font-size:14px">序号</th>
                    <th style="width: 15%;text-align:left;font-size:14px">订单号</th>
                    <th style="width: 8%;text-align:left;font-size:14px">床号</th>
                    <th style="width: 12%;text-align:left;font-size:14px">颜色</th>
                    <th style="width: 12%;text-align:left;font-size:14px">尺码</th>
                    <th style="width: 12%;text-align:left;font-size:14px">数量</th>
                    <th style="width: 15%;text-align:left;font-size:14px">位置</th>
                    <th style="width: 10%;text-align:left;font-size:14px">操作</th>
                </tr>
                </thead>
                <tbody>
                <#list cutStorageQueryList as opa>
                    <tr>
                        <td>${opa_index+1}</td>
                        <td>${opa.orderName!}</td>
                        <td>${opa.bedNumber!}</td>
                        <td>${opa.colorName!}</td>
                        <td>${opa.sizeName!}</td>
                        <td>${opa.cutStorageCount!}</td>
                        <td>${opa.storehouseLocation!}</td>
                        <td><a href="#" style="color:red;" onclick="deleteStorage('${opa.orderName?string}',${opa.bedNumber?c},'${opa.colorName?string}','${opa.sizeName?string}','${opa.storehouseLocation?string}',this)">删除</a></td>
                    </tr>
                </#list>
                </tbody>
                <tfoot>
                <tr>
                    <th colspan="8" style="text-align:right"></th>
                </tr>
                </tfoot>
            <#else>
                <h4 style="text-align: center">暂无数据</h4>
            </#if>
        </table>
    </div>

</section>

<style>
    td {
        word-break:break-all;
    }

</style>