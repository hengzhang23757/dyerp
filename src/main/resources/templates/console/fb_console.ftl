<#macro search>
<div class="layui-fluid">
  <div class="layui-row layui-col-space15">
    <div class="layui-col-md8">
      <div class="layui-row layui-col-space15">
        <div class="layui-col-md6">
          <div class="layui-card">
            <div class="layui-card-header">快捷方式</div>
            <div class="layui-card-body">

              <div class="layui-carousel layadmin-carousel layadmin-shortcut">
                <div carousel-item>
                  <ul class="layui-row layui-col-space10">
                    <#if role! == 'root'><#--根-->
                      <li class="layui-col-xs3">
                        <a lay-href="/erp/processOrderStart">
                          <i class="layui-icon layui-icon-list"></i>
                          <cite>制单信息</cite>
                        </a>
                      </li>
                      <li class="layui-col-xs3">
                        <a lay-href="/erp/homepageNew">
                          <i class="layui-icon layui-icon-console"></i>
                          <cite>订单进度</cite>
                        </a>
                      </li>
                      <li class="layui-col-xs3">
                        <a lay-href="/erp/procedureBalanceNewStart">
                          <i class="layui-icon layui-icon-chart"></i>
                          <cite>工序平衡</cite>
                        </a>
                      </li>
                      <li class="layui-col-xs3">
                        <a lay-href="/erp/cutQueryLeakStart">
                          <i class="layui-icon layui-icon-table"></i>
                          <cite>单款报表</cite>
                        </a>
                      </li>
                      <li class="layui-col-xs3">
                        <a lay-href="/erp/fabricAccessoryCheckStart">
                        <#--<a onclick="fabricAccessoryOrder()">-->
                          <i class="layui-icon layui-icon-cart-simple"></i>
                          <cite>一站式下单</cite>
                        </a>
                      </li>
                      <li class="layui-col-xs3">
                        <a lay-href="/erp/prenatalProgressSummaryStart">
                          <i class="layui-icon layui-icon-slider"></i>
                          <cite>产前进度</cite>
                        </a>
                      </li>
                      <li class="layui-col-xs3">
                        <a lay-href="/erp/tailorReprintStart">
                          <i class="layui-icon layui-icon-print"></i>
                          <cite>票菲打印</cite>
                        </a>
                      </li>
                      <li class="layui-col-xs3">
                        <a lay-href="/erp/fabricLeakStart">
                          <i class="layui-icon layui-icon-app"></i>
                          <cite>单款面辅料</cite>
                        </a>
                      </li>
                    <#elseif role! == 'role1'>
                      <li class="layui-col-xs3">
                        <a lay-href="/erp/processOrderStart">
                          <i class="layui-icon layui-icon-list"></i>
                          <cite>制单信息</cite>
                        </a>
                      </li>
                      <li class="layui-col-xs3">
                        <a lay-href="/erp/homepageNew">
                          <i class="layui-icon layui-icon-console"></i>
                          <cite>订单进度</cite>
                        </a>
                      </li>
                      <li class="layui-col-xs3">
                        <a lay-href="/erp/procedureBalanceNewStart">
                          <i class="layui-icon layui-icon-chart"></i>
                          <cite>工序平衡</cite>
                        </a>
                      </li>
                      <li class="layui-col-xs3">
                        <a lay-href="/erp/cutQueryLeakStart">
                          <i class="layui-icon layui-icon-table"></i>
                          <cite>单款报表</cite>
                        </a>
                      </li>
                      <li class="layui-col-xs3">
                          <a lay-href="/erp/tailorReprintStart">
                              <i class="layui-icon layui-icon-print"></i>
                              <cite>票菲打印</cite>
                          </a>
                      </li>
                      <li class="layui-col-xs3">
                          <a lay-href="/erp/fabricLeakStart">
                              <i class="layui-icon layui-icon-app"></i>
                              <cite>单款面辅料</cite>
                          </a>
                      </li>
                    <#elseif role! == 'role2'>
                      <li class="layui-col-xs3">
                        <a lay-href="/erp/processOrderStart">
                          <i class="layui-icon layui-icon-list"></i>
                          <cite>制单信息</cite>
                        </a>
                      </li>
                      <li class="layui-col-xs3">
                        <a lay-href="/erp/homepageNew">
                          <i class="layui-icon layui-icon-console"></i>
                          <cite>订单进度</cite>
                        </a>
                      </li>
                      <li class="layui-col-xs3">
                        <a lay-href="/erp/procedureBalanceNewStart">
                          <i class="layui-icon layui-icon-chart"></i>
                          <cite>工序平衡</cite>
                        </a>
                      </li>
                      <li class="layui-col-xs3">
                        <a lay-href="/erp/cutQueryLeakStart">
                          <i class="layui-icon layui-icon-table"></i>
                          <cite>单款报表</cite>
                        </a>
                      </li>
                      <li class="layui-col-xs3">
                        <a lay-href="/erp/prenatalProgressSummaryStart">
                          <i class="layui-icon layui-icon-slider"></i>
                          <cite>产前进度</cite>
                        </a>
                      </li>
                      <li class="layui-col-xs3">
                        <a lay-href="/erp/fabricLeakStart">
                          <i class="layui-icon layui-icon-app"></i>
                          <cite>单款面辅料</cite>
                        </a>
                      </li>
                    <#elseif role! == 'role10'>
                      <li class="layui-col-xs3">
                        <a lay-href="/erp/processOrderStart">
                          <i class="layui-icon layui-icon-list"></i>
                          <cite>制单信息</cite>
                        </a>
                      </li>
                      <li class="layui-col-xs3">
                        <a lay-href="/erp/homepageNew">
                          <i class="layui-icon layui-icon-console"></i>
                          <cite>订单进度</cite>
                        </a>
                      </li>
                      <li class="layui-col-xs3">
                        <a lay-href="/erp/procedureBalanceNewStart">
                          <i class="layui-icon layui-icon-chart"></i>
                          <cite>工序平衡</cite>
                        </a>
                      </li>
                      <li class="layui-col-xs3">
                        <a lay-href="/erp/cutQueryLeakStart">
                          <i class="layui-icon layui-icon-table"></i>
                          <cite>单款报表</cite>
                        </a>
                      </li>
                      <li class="layui-col-xs3">
                        <a lay-href="/erp/fabricAccessoryCheckStart">
                          <i class="layui-icon layui-icon-cart-simple"></i>
                          <cite>一站式下单</cite>
                        </a>
                      </li>
                      <li class="layui-col-xs3">
                        <a lay-href="/erp/prenatalProgressSummaryStart">
                          <i class="layui-icon layui-icon-slider"></i>
                          <cite>产前进度</cite>
                        </a>
                      </li>
                      <li class="layui-col-xs3">
                        <a lay-href="/erp/fabricLeakStart">
                          <i class="layui-icon layui-icon-app"></i>
                          <cite>单款面辅料</cite>
                        </a>
                      </li>
                    <#elseif role! == 'role17'>
                      <li class="layui-col-xs3">
                        <a lay-href="/erp/processOrderStart">
                          <i class="layui-icon layui-icon-list"></i>
                          <cite>制单信息</cite>
                        </a>
                      </li>
                      <li class="layui-col-xs3">
                        <a lay-href="/erp/homepageNew">
                          <i class="layui-icon layui-icon-console"></i>
                          <cite>订单进度</cite>
                        </a>
                      </li>
                      <li class="layui-col-xs3">
                        <a lay-href="/erp/procedureBalanceNewStart">
                          <i class="layui-icon layui-icon-chart"></i>
                          <cite>工序平衡</cite>
                        </a>
                      </li>
                      <li class="layui-col-xs3">
                        <a lay-href="/erp/cutQueryLeakStart">
                          <i class="layui-icon layui-icon-table"></i>
                          <cite>单款报表</cite>
                        </a>
                      </li>
                      <li class="layui-col-xs3">
                        <a lay-href="/erp/fabricAccessoryCheckStart">
                          <i class="layui-icon layui-icon-cart-simple"></i>
                          <cite>一站式下单</cite>
                        </a>
                      </li>
                      <li class="layui-col-xs3">
                        <a lay-href="/erp/prenatalProgressSummaryStart">
                          <i class="layui-icon layui-icon-slider"></i>
                          <cite>产前进度</cite>
                        </a>
                      </li>
                      <li class="layui-col-xs3">
                        <a lay-href="/erp/tailorReprintStart">
                          <i class="layui-icon layui-icon-print"></i>
                          <cite>票菲打印</cite>
                        </a>
                      </li>
                      <li class="layui-col-xs3">
                        <a lay-href="/erp/fabricLeakStart">
                          <i class="layui-icon layui-icon-app"></i>
                          <cite>单款面辅料</cite>
                        </a>
                      </li>
                    <#else>
                      <li class="layui-col-xs3">
                        <a lay-href="/erp/processOrderStart">
                          <i class="layui-icon layui-icon-list"></i>
                          <cite>制单信息</cite>
                        </a>
                      </li>
                      <li class="layui-col-xs3">
                        <a lay-href="/erp/homepageNew">
                          <i class="layui-icon layui-icon-console"></i>
                          <cite>订单进度</cite>
                        </a>
                      </li>
                      <li class="layui-col-xs3">
                        <a lay-href="/erp/procedureBalanceNewStart">
                          <i class="layui-icon layui-icon-chart"></i>
                          <cite>工序平衡</cite>
                        </a>
                      </li>
                      <li class="layui-col-xs3">
                        <a lay-href="/erp/cutQueryLeakStart">
                          <i class="layui-icon layui-icon-table"></i>
                          <cite>单款报表</cite>
                        </a>
                      </li>
                      <li class="layui-col-xs3">
                        <a lay-href="/erp/prenatalProgressSummaryStart">
                          <i class="layui-icon layui-icon-slider"></i>
                          <cite>产前进度</cite>
                        </a>
                      </li>
                      <li class="layui-col-xs3">
                        <a lay-href="/erp/fabricLeakStart">
                          <i class="layui-icon layui-icon-app"></i>
                          <cite>单款面辅料</cite>
                        </a>
                      </li>
                    </#if>
                  </ul>
                </div>
              </div>

            </div>
          </div>
        </div>
        <div class="layui-col-md6">
          <div class="layui-card">
            <div class="layui-card-header">待办事项</div>
            <div class="layui-card-body">

              <div class="layui-carousel layadmin-carousel layadmin-backlog">
                <div carousel-item>
                  <ul class="layui-row layui-col-space10">
                    <#if role! == 'root'><#--根-->
                      <li class="layui-col-xs6">
                        <a lay-href="/erp/fabricAccessoryOnlyCheckStart" class="layadmin-backlog-body">
                          <h3>待审面辅料</h3>
                          <p><cite id="fabricAccessoryReviewCount"></cite></p>
                        </a>
                      </li>
                      <li class="layui-col-xs6">
                        <a lay-href="/erp/orderProcedureNewStart" class="layadmin-backlog-body">
                          <h3>待审工价</h3>
                          <p><cite id="pendingOrderProcedureCount">0</cite></p>
                        </a>
                      </li>
                      <li class="layui-col-xs6">
                        <a lay-href="/erp/message" class="layadmin-backlog-body">
                          <h3>待阅消息</h3>
                          <p><cite id="pendingMessageCount">0</cite></p>
                        </a>
                      </li>
                      <li class="layui-col-xs6">
                        <a lay-href="/erp/manufactureOrderModifyReviewStart" class="layadmin-backlog-body">
                            <h3>修改审核</h3>
                            <p><cite id="orderClothesModify">0</cite></p>
                        </a>
                      </li>
                    <#elseif role! == 'role5'><#--根-->
                      <li class="layui-col-xs6">
                        <a lay-href="/erp/orderProcedureNewStart" class="layadmin-backlog-body">
                          <h3>待审工价</h3>
                          <p><cite id="pendingOrderProcedureCount">0</cite></p>
                        </a>
                      </li>
                      <li class="layui-col-xs6">
                        <a lay-href="/erp/message" class="layadmin-backlog-body">
                          <h3>待阅消息</h3>
                          <p><cite id="pendingMessageCount">0</cite></p>
                        </a>
                      </li>
                    <#elseif role! == 'role10'><#--根-->
                      <li class="layui-col-xs6">
                        <a lay-href="/erp/fabricAccessoryOnlyCheckStart" class="layadmin-backlog-body">
                          <h3>待审面辅料</h3>
                          <p><cite id="fabricAccessoryReviewCount"></cite></p>
                        </a>
                      </li>
                      <li class="layui-col-xs6">
                        <a lay-href="/erp/orderProcedureNewStart" class="layadmin-backlog-body">
                          <h3>待审工价</h3>
                          <p><cite id="pendingOrderProcedureCount">0</cite></p>
                        </a>
                      </li>
                      <li class="layui-col-xs6">
                        <a lay-href="/erp/message" class="layadmin-backlog-body">
                          <h3>待阅消息</h3>
                          <p><cite id="pendingMessageCount">0</cite></p>
                        </a>
                      </li>
                      <li class="layui-col-xs6">
                          <a lay-href="/erp/manufactureOrderModifyReviewStart" class="layadmin-backlog-body">
                              <h3>修改审核</h3>
                              <p><cite id="orderClothesModify">0</cite></p>
                          </a>
                      </li>
                    <#else><#--根-->
                      <li class="layui-col-xs6">
                        <a lay-href="/erp/message" class="layadmin-backlog-body">
                          <h3>待阅消息</h3>
                          <p><cite id="pendingMessageCount">0</cite></p>
                        </a>
                      </li>
                    </#if><#--根-->
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="layui-col-md12">
          <div class="layui-card">
            <div class="layui-card-header">一周看板</div>
            <div class="layui-card-body">
              <div id="container" style="height: 430px;"></div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="layui-col-md4">
      <div class="layui-card" style="height: 505px">
        <div class="layui-card-header">松布裁剪计划</div>
        <div class="layui-card-body layui-text">
          <table class="layui-table" id="weekLooseTable" lay-filter="weekLooseTable"></table>
        </div>
      </div>

      <#--<div class="layui-card" style="height: 248px">-->
        <#--<div class="layui-card-header">占个位置</div>-->
        <#--<div class="layui-card-body layui-text">-->
          <#--<table class="layui-table" id="weekCutTable" lay-filter="weekCutTable"></table>-->
        <#--</div>-->
      <#--</div>-->

      <div class="layui-card">
        <div class="layui-card-header">
          微信小程序
          <i class="layui-icon layui-icon-tips" lay-tips="打开微信扫描一下试试" lay-offset="5"></i>
        </div>
        <div class="layui-card-body">
          <table class="layui-table">
            <tr><td align="center"><img src="/images/piece.jpg"></td><td align="center"><img src="/images/cutEmb.jpg"></td></tr>
            <tr><td align="center">计件小程序</td><td align="center">裁片超市小程序</td></tr>
          </table>
        </div>
      </div>
    </div>

  </div>
</div>
<style>

  #first i {
    display: none
  }

  ::-webkit-scrollbar-track-piece {

    background-color:#f8f8f8;

  }

  ::-webkit-scrollbar {

    width:15px;

    height:15px;

  }

  ::-webkit-scrollbar-thumb {

    background-color:#dddddd;

    background-clip:padding-box;

    min-height:28px;

  }

  ::-webkit-scrollbar-thumb:hover {

    background-color:#bbb;

  }

</style>
<link rel="stylesheet" href="/layuiadmin/layui/css/layui.css" media="all">
<link rel="stylesheet" href="/layuiadmin/style/admin.css" media="all">
<script src="/js/common/echarts.js" type="text/javascript"></script>
<script src="/layuiadmin/layui/layui.js?t=1"></script>
<script src="/js/console/console.js"></script>
<script src="/js/common/moment.min.js" type="text/javascript"></script>
<script src="/js/ext/soulTable.js" type="text/javascript"></script>
</#macro>
