<!DOCTYPE html>
<html lang="en" class="app">
<#include "../feedback/fb_script.ftl">
<@script> </@script>
<body>
<section class="vbox">
    <input  type="hidden" value="${basePath}"  id="basePath"/>
    <input  type="hidden" value="${role!?string}"  id="userRole"/>
    <input  type="hidden" value="${userName!?string}"  id="userName"/>
    <section>
        <section class="hbox stretch">
            <#include "fb_fabricAccessoryOverReview.ftl">
            <@search> </@search>
        </section>
    </section>
</section>
</body>
</html>