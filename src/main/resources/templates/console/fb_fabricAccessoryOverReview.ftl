<#macro search>
<section id="content">
    <section class="vbox">
        <section class="scrollable">
            <table id="dataTable" lay-filter="dataTable"></table>
            <script type="text/html" id="toolbarTop">
                <div class="layui-btn-container">
                    <button class="layui-btn layui-btn-sm" lay-event="clearFilter">清除所有筛选</button>
                </div>
            </script>
            <script type="text/html" id="barTopReview">
                <a class="layui-btn layui-btn-xs layui-btn-normal" lay-event="inStore">入库</a>
                <a class="layui-btn layui-btn-xs" lay-event="return">退面料厂</a>
                <a class="layui-btn layui-btn-xs layui-btn-warm" lay-event="discount">打折收货</a>
            </script>
        </section>
    </section>
</section>

    <link rel="stylesheet" href="/css/layui.css" type="text/css">
    <link rel="stylesheet" href="/css/soulTable.css" type="text/css">
    <script src="/js/common/layer.js"></script>
    <script src="/js/common/moment.min.js" type="text/javascript"></script>
    <script src="/js/console/fabricAccessoryOverReview.js?t=${currentDate?c}"></script>
</#macro>

<style>

    ::-webkit-scrollbar-track-piece {

        background-color:#f8f8f8;

    }

    ::-webkit-scrollbar {

        width:15px;

        height:15px;

    }

    ::-webkit-scrollbar-thumb {

        background-color:#dddddd;

        background-clip:padding-box;

        min-height:28px;

    }

    ::-webkit-scrollbar-thumb:hover {

        background-color:#bbb;

    }
</style>
