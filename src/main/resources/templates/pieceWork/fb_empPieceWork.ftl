<#macro search>
<section id="content">
    <section class="vbox">
        <section class="scrollable padder water-mark-visible-ff">
            <div class="col-md-12" style="padding-top: 20px;">
                <section class="panel panel-default">
                    <div class="panel-body">
                        <table class="row">
                            <tr class="col-md-12">
                                <td>
                                    <label class="control-label" style="margin-bottom: 15px;text-align: right;">从</label>
                                </td>
                                <td>
                                    <input id="from" class="form-control" autocomplete="off" style="width: 70%;">
                                </td>
                                <td>
                                    <label class="control-label" style="margin-bottom: 15px;text-align: right;">到</label>
                                </td>
                                <td>
                                    <input id="to" class="form-control" autocomplete="off" style="width: 70%;">
                                </td>
                                <td>
                                    <label class="control-label" style="margin-bottom: 15px;text-align: right;">组名</label>
                                </td>
                                <td>
                                    <select id="groupName" class="form-control" autocomplete="off" style="width: 70%;">
                                        <option value="">请选择组名</option>
                                        <option value="车缝A组">车缝A组</option>
                                        <option value="车缝B组">车缝B组</option>
                                        <option value="车缝C组">车缝C组</option>
                                        <option value="车缝D组">车缝D组</option>
                                        <option value="车缝E组">车缝E组</option>
                                        <option value="车缝F组">车缝F组</option>
                                        <option value="车缝G组">车缝G组</option>
                                        <option value="车缝H组">车缝H组</option>
                                        <option value="后整I组">后整I组</option>
                                        <option value="后整J组">后整J组</option>
                                        <option value="品控部">品控部</option>
                                        <option value="车间">车间</option>
                                        <option value="裁床">裁床</option>
                                    </select>
                                </td>
                                <td>
                                    <label class="control-label" style="margin-bottom: 15px;text-align: right;">姓名</label>
                                </td>
                                <td>
                                    <select id="employeeNumber" class="form-control" autocomplete="off" style="width: 70%;">
                                    </select>
                                </td>
                                <td>
                                    <button  class="btn" style="border-radius: 5px;color:white;font-family: PingFangSC-Semibold, sans-serif;width:60%;"  onclick="search()">查找</button>
                                </td>
                            </tr>
                        </table>
                        <div class="col-md-12" style="padding-top: 20px;padding-bottom:10px" id="pieceWorkDiv" hidden>
                            <table class="table table-striped" id="pieceWorkTable" ></table>
                        </div>
                    </div>
                </section>
            </div>
            <div class="row">
                <div id="entities">
                </div>
            </div>
        </section>
    </section>
    <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen"
       data-target="#nav"></a>
</section>
<aside class="bg-light lter b-l aside-md hide" id="notes">
    <div class="wrapper">Notification</div>
</aside>
</section>
<#--<link rel="stylesheet" href="/css/bootstrap.min.css" type="text/css">-->
<link rel="stylesheet" href="/css/bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="/css/b.tabs.css" type="text/css">
<script src="/js/common/laydate.js" type="text/javascript" ></script>
<script src="/js/common/jquery.blockUI.js" type="text/javascript" ></script>
<script src="/js/common/b.tabs.js?t=${currentDate?c}"></script>
<link rel="stylesheet" href="/css/selectize.bootstrap3.css" type="text/css">
<script src="/js/common/selectize.js" type="text/javascript"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.bootstrap.min.css">
<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="/js/pieceWork/empPieceWork.js?t=${currentDate?c}"></script>


</#macro>

<style>
    button {
        background:rgb(45, 202, 147);
        opacity:0.86;
        color: white;
        font-family: PingFangSC-Semibold, sans-serif;
    }
</style>
