<#macro search>
<section id="content">
    <section class="vbox">
        <section class="scrollable">
            <div class="layui-row">
                <form class="layui-form" style="margin: 0 auto;padding-top: 3px;" lay-filter="baseInfo">
                    <table>
                        <tr>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label">组别</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <select type="text" name="groupName" id="groupName" autocomplete="off" class="layui-input"></select>
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label">工号</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" name="employeeNumber" id="employeeNumber" autocomplete="off" class="layui-input">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label">姓名</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" name="employeeName" id="employeeName" autocomplete="off" class="layui-input">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label">开始</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" name="from" id="from" autocomplete="off" class="layui-input">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label">结束</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" name="to" id="to" autocomplete="off" class="layui-input">
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label">单号</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" name="clothesVersionNumber" id="clothesVersionNumber" autocomplete="off" placeholder="请输入单号" class="layui-input">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label">款号</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" name="orderName" id="orderName" placeholder="请输入款号" autocomplete="off" class="layui-input">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label">工序</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" name="procedureNumber" id="procedureNumber" placeholder="请输入工序号" autocomplete="off" class="layui-input">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label">工序状态</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <select type="text" name="procedureState" id="procedureState">
                                    <option value="">全部</option>
                                    <option value="2">审核通过</option>
                                </select>
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label">类别</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <select type="text" name="searchType" autocomplete="off" id="searchType">
                                    <option value="产能详情">产能详情</option>
                                    <option value="产能汇总">产能汇总</option>
                                    <option value="金额详情">金额详情</option>
                                    <option value="金额汇总">金额汇总</option>
                                </select>
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;padding-left: 20px;">
                                <div class="layui-input-inline">
                                    <button class="layui-btn" lay-submit lay-filter="searchBeat">搜索</button>
                                </div>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
            <table id="reportTable" lay-filter="reportTable"></table>

            <script type="text/html" id="toolbarTop">
                <div class="layui-btn-container">
                    <button class="layui-btn layui-btn-sm" lay-event="refresh">刷新</button>
                    <button class="layui-btn layui-btn-sm" lay-event="clearFilter">清除所有筛选</button>
                    <button class="layui-btn layui-btn-sm" lay-event="exportExcel">导出</button>
                </div>
            </script>
        </section>
    </section>
</section>
    <link rel="stylesheet" href="/css/layui.css" type="text/css">
    <link rel="stylesheet" href="/css/soulTable.css" type="text/css">
    <script src="/js/common/xm-select.js?t=${currentDate?c}" type="text/javascript"></script>
    <script src="/js/common/moment.min.js" type="text/javascript"></script>
    <script src="/js/pieceWork/gatherPieceWorkReport.js?t=${currentDate?c}"></script>
</#macro>
<style>
    .layui-form-label {
        width:100px !important;
    }

    ::-webkit-scrollbar-track-piece {

        background-color:#f8f8f8;

    }

    ::-webkit-scrollbar {

        width:15px;

        height:15px;

    }

    ::-webkit-scrollbar-thumb {

        background-color:#dddddd;

        background-clip:padding-box;

        min-height:28px;

    }

    ::-webkit-scrollbar-thumb:hover {

        background-color:#bbb;

    }
</style>
