<#macro search>
<section id="content">
    <section class="vbox">
        <section class="scrollable">

            <div class="layui-row">
                <form class="layui-form" style="margin: 0 auto;padding-top: 3px;" lay-filter="baseInfo">
                    <table>
                        <tr>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label">单号</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" name="clothesVersionNumber" id="clothesVersionNumber" autocomplete="off" class="layui-input">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label">款号</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" name="orderName" id="orderName" autocomplete="off" class="layui-input">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label"></label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <div class="layui-input-inline">
                                    <button class="layui-btn" lay-submit lay-filter="searchBeat">搜索</button>
                                </div>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>

            <table id="dataTable" lay-filter="dataTable"></table>

            <script type="text/html" id="toolbarTop">
                <div class="layui-btn-container">
                    <button class="layui-btn layui-btn-sm" lay-event="clearFilter">清除所有筛选</button>
                    <button class="layui-btn layui-btn-sm" lay-event="exportExcel">导出</button>
                </div>
            </script>

            <script type="text/html" id="barTop">
                <a class="layui-btn layui-btn-xs" lay-event="return">退货</a>
                <a class="layui-btn layui-btn-xs layui-btn-warm" lay-event="thisOrder">本款补料</a>
                <a class="layui-btn layui-btn-xs layui-btn-normal" lay-event="otherOrder">换款调用</a>
            </script>

            <div id="operateDiv" hidden>
                <div class="layui-row">
                    <span style="font-weight: bolder; font-size: large; padding-top: 20px; padding-bottom: 20px; color: #00c7f7">要换款的面料信息</span>
                    <table>
                        <tr>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label">单号</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" name="clothesVersionNumber" autocomplete="off" class="layui-input">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label">款号</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" name="orderName" autocomplete="off" class="layui-input">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label">面料名称</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" name="fabricName" autocomplete="off" class="layui-input">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label">面料号</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" name="fabricNumber" autocomplete="off" class="layui-input">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label">供应商</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" name="supplier" autocomplete="off" class="layui-input">
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label">面料颜色</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" name="fabricColor" autocomplete="off" class="layui-input">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label">面料色号</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" name="fabricColorNumber" autocomplete="off" class="layui-input">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label">缸号</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" name="jarName" autocomplete="off" class="layui-input">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label">单位</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" name="unit" autocomplete="off" class="layui-input">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label">卷数</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" name="batchNumber" autocomplete="off" class="layui-input">
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label">重量</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" name="weight" autocomplete="off" class="layui-input">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                            </td>
                            <td><span style="font-weight: bolder; font-size: large; color: #00c7f7">数量确认--></span></td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label">调用卷数</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" name="changeBatch" id="changeBatch" autocomplete="off" class="layui-input">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label">调用重量</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" name="changeWeight" id="changeWeight" autocomplete="off" class="layui-input">
                            </td>
                        </tr>
                    </table>
                </div>
                <hr class="layui-border-green">
                <span style="font-weight: bolder; font-size: large; padding-top: 20px; padding-bottom: 20px; color: #00c7f7">要调用到哪个款</span>
                <form class="layui-form" style="margin: 0 auto;padding-top: 3px;" lay-filter="changeInfo">
                    <table>
                        <tr>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label">单号</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" id="clothesVersionNumberTwo" autocomplete="off" class="layui-input">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label">款号</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" id="orderNameTwo" autocomplete="off" class="layui-input">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label"></label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <div class="layui-input-inline">
                                    <button class="layui-btn" lay-submit lay-filter="searchChangeBeat">搜索</button>
                                </div>
                            </td>
                        </tr>
                    </table>
                </form>
                <hr class="layui-border-green">
                <span style="font-weight: bolder; font-size: large; padding-top: 20px; padding-bottom: 20px; color: #00c7f7">请选择面料信息(调用到的面料名称和颜色必须与上面的一致才行)</span>
                <table id='fabricChangeTable' lay-filter='fabricChangeTable' style="margin-top: 20px"></table>
            </div>


        </section>
    </section>
</section>

    <link rel="stylesheet" href="/css/layui.css" type="text/css">
    <link rel="stylesheet" href="/css/step.css" type="text/css">
    <link rel="stylesheet" href="/css/soulTable.css" type="text/css">
    <link rel="stylesheet" href="/css/cropper.css" type="text/css">

    <script src="/js/common/layer.js"></script>
    <script src="/js/common/laydate.js" type="text/javascript" ></script>
    <script src="/js/common/xm-select.js?t=${currentDate?c}" type="text/javascript"></script>
    <script src="/js/common/selectize.js" type="text/javascript"></script>
    <script src="/js/tmpFabric/tmpFabric.js?t=${currentDate?c}"></script>
    <script src="/js/common/moment.min.js" type="text/javascript"></script>
</#macro>

<style>
    ::-webkit-scrollbar-track-piece {

        background-color:#f8f8f8;

    }

    ::-webkit-scrollbar {

        width:15px;

        height:15px;

    }

    ::-webkit-scrollbar-thumb {

        background-color:#dddddd;

        background-clip:padding-box;

        min-height:28px;

    }

    ::-webkit-scrollbar-thumb:hover {

        background-color:#bbb;

    }
</style>
