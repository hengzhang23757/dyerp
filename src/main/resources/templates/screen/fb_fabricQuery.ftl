<#macro search>
    <section id="content">
        <section class="vbox">
            <section class="scrollable">
                <div class="layui-row layui-col-sm12">
                    <div class="layui-col-sm12" align="center"><span style="font-size: x-large; font-weight: bolder; color: black;">松布裁剪计划</span></div>
                    <#--<div class="layui-col-sm6" align="center"><span style="font-size: x-large; font-weight: bolder;color: black;">计划面料汇总</span></div>-->
                </div>
                <div class="layui-row layui-col-sm12">
                    <div class="layui-col-sm12"><table id="cutFabricTable" lay-filter="cutFabricTable" style="height: 450px"></table></div>
                    <#--<div class="layui-col-sm12"><div id="container" style="height: 500px"></div></div>-->
                </div>
                <div class="layui-row layui-col-sm12">
                    <div class="layui-col-sm12" align="center"><span style="font-size: x-large; font-weight: bolder; color: black;">面料库存详情</span></div>
                </div>
                <div class="layui-row layui-col-sm12">
                    <div class="layui-col-sm12"><table id="fabricQueryTable" lay-filter="fabricQueryTable" style="height: 500px; font-weight: bolder; font-size: x-large"></table></div>
                </div>
            </section>
        </section>
    </section>
    <link rel="stylesheet" href="/css/layui.css" type="text/css">
    <link rel="stylesheet" href="/css/soulTable.css" type="text/css">
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/echarts@5/dist/echarts.min.js"></script>
    <script src="/js/screen/fabricQuery.js?t=${currentDate?c}"></script>

</#macro>

<style>
    button {
        background:rgb(45, 202, 147);
        opacity:0.86;
        color: white;
        font-family: PingFangSC-Semibold, sans-serif;
    }
    .layui-table th{
        font-size: 18px;
    }
    .layui-progress-text{
        color: black !important;
        font-weight: 700 !important;
    }
    ::-webkit-scrollbar-track-piece {

        background-color:#f8f8f8;

    }

    ::-webkit-scrollbar {

        width:15px;

        height:15px;

    }

    ::-webkit-scrollbar-thumb {

        background-color:#dddddd;

        background-clip:padding-box;

        min-height:28px;

    }

    ::-webkit-scrollbar-thumb:hover {

        background-color:#bbb;

    }
</style>
