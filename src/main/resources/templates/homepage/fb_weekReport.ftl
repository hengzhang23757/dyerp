<#macro search>
<section id="content">
    <section class="vbox">
        <section class="scrollable padder water-mark-visible-ff">

            <div class="row">
                <div class="col-md-6">
                    <section class="panel panel-default">
                        <div class="panel-body" style="text-align: center;">
                            <div id="container" style="height: 380px;"></div>
                        </div>
                    </section>
                </div>
                <div class="col-md-6">
                    <section class="panel panel-default">
                        <div class="panel-body" style="text-align: center;">
                            <div id="yesterdayFinish" style="height: 380px;"></div>
                        </div>
                    </section>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <section class="panel panel-default">
                        <div class="panel-body" style="text-align: center;">
                            <div id="yesterdayTailor" style="height: 330px;"></div>
                        </div>
                    </section>
                 </div>
                <div class="col-md-6">
                    <section class="panel panel-default">
                        <div class="panel-body" style="text-align: center;">
                            <div id="yesterdayWorkShop" style="height: 330px;"></div>
                        </div>
                    </section>
                </div>
                <#--<div class="col-md-4">-->
                    <#--<section class="panel panel-default">-->
                        <#--<div class="panel-body" style="text-align: center;">-->
                            <#--<div class="carousel slide" data-ride="carousel">-->
                                <#--<!-- 轮播图片 &ndash;&gt;-->
                                <#--<div class="carousel-inner">-->
                                    <#--<#if orderNameList??>-->
                                        <#--<#list orderNameList as vo>-->
                                            <#--<#if vo_index == 0>-->
                                                <#--<div class="carousel-item active">-->
                                                    <#--<div id="yesterdayReWork${vo_index}" style="width: 364px;height: 330px;"></div>-->
                                                <#--</div>-->
                                            <#--<#else>-->
                                                <#--<div class="carousel-item">-->
                                                    <#--<div id="yesterdayReWork${vo_index}" style="width: 364px;height: 330px;"></div>-->
                                                <#--</div>-->
                                            <#--</#if>-->
                                        <#--</#list>-->
                                    <#--</#if>-->

                                <#--</div>-->
                            <#--</div>-->
                        <#--</div>-->
                    <#--</section>-->
                <#--</div>-->
            </div>

        </section>
    </section>
    <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen"
       data-target="#nav"></a>
</section>
<aside class="bg-light lter b-l aside-md hide" id="notes">
    <div class="wrapper">Notification</div>
</aside>
</section>

<link rel="stylesheet" href="/css/bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="/css/b.tabs.css" type="text/css">
<script src="/js/common/laydate.js" type="text/javascript" ></script>
<script src="/js/common/jquery.blockUI.js" type="text/javascript" ></script>
<script src="/js/common/b.tabs.js?t=${currentDate?c}"></script>
<link rel="stylesheet" href="/css/selectize.bootstrap3.css" type="text/css">
<script src="/js/common/selectize.js" type="text/javascript"></script>
<#--<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" type="text/css">-->
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css" type="text/css">
<script type="text/javascript" src="https://cdn.bootcss.com/echarts/4.2.1-rc1/echarts.min.js"></script>
<script src="/js/common/moment.min.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/homepage/echarts-auto-tooltip.js"></script>
<script type="text/javascript" src="/js/homepage/weekReport.js?t=${currentDate?c}"></script>

</#macro>
