<#macro search>
<section id="content">
    <section class="vbox">
        <section class="scrollable padder water-mark-visible-ff">

            <table id="homepageTable" lay-filter="homepageTable"></table>

            <script type="text/html" id="toolbarTop">
                <div class="layui-btn-container">
                    <button class="layui-btn layui-btn-sm" lay-event="refresh">刷新</button>
                </div>
            </script>

        </section>
    </section>
    <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen"
       data-target="#nav"></a>
</section>
<aside class="bg-light lter b-l aside-md hide" id="notes">
    <div class="wrapper">Notification</div>
</aside>
<style>

    #first i {
        display: none
    }

    ::-webkit-scrollbar-track-piece {

        background-color:#f8f8f8;

    }

    ::-webkit-scrollbar {

        width:15px;

        height:15px;

    }

    ::-webkit-scrollbar-thumb {

        background-color:#dddddd;

        background-clip:padding-box;

        min-height:28px;

    }

    ::-webkit-scrollbar-thumb:hover {

        background-color:#bbb;

    }

</style>

<link rel="stylesheet" href="/css/bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="/css/b.tabs.css" type="text/css">
<link rel="stylesheet" href="/css/layui.css" type="text/css">
<link rel="stylesheet" href="/css/soulTable.css" type="text/css">
<script src="/js/common/laydate.js" type="text/javascript" ></script>
<script src="/js/common/jquery.blockUI.js" type="text/javascript" ></script>
<script src="/js/common/b.tabs.js?t=${currentDate?c}"></script>
<link rel="stylesheet" href="/css/selectize.bootstrap3.css" type="text/css">
<script src="/js/common/selectize.js" type="text/javascript"></script>
<#--<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" type="text/css">-->
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css" type="text/css">
<script type="text/javascript" src="https://cdn.bootcss.com/echarts/4.2.1-rc1/echarts.min.js"></script>
<script src="/js/common/moment.min.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/homepage/echarts-auto-tooltip.js"></script>
<script type="text/javascript" src="/js/homepage/homepage.js?t=${currentDate?c}"></script>

</#macro>
