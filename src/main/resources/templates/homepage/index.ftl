<!DOCTYPE html>
<html lang="en" class="app">
<#include "../feedback/fb_script.ftl">
    <@script> </@script>
<head>
    <script src="/js/common/layui.js" type="text/javascript"></script>
<#--<script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>-->
<#--<script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>-->
</head>
<body class="layui-layout-body">
<div class="layui-layout layui-layout-admin">
    <div class="layui-header ">
        <div class="layui-logo">
            <i class="layui-icon" style="font-size: 26px; color: #1E9FFF;">&#xe857;</i>
            恒达精益管理
        </div>
        <!-- 头部区域（可配合layui已有的水平导航） -->
        <ul class="layui-nav layui-layout-left">
            <li class="layui-nav-item layadmin-flexible" lay-unselect>
                <a href="javascript:;" layadmin-event="flexible" title="侧边伸缩" id="animation-left-nav">
                    <i class="layui-icon layui-icon-shrink-right" id="LAY_app_flexible"></i>
                </a>
            </li>
        </ul>
        <ul class="layui-nav layui-layout-right">
            <li class="layui-nav-item">
                <a href="javascript:;">
                    <input hidden id="loginUserName" value="${userName!""}">
                ${userName!""}
                </a>
                <dl class="layui-nav-child">
                    <dd><a href="javascript:;" onclick="updatePwd()">修改密码</a></dd>
                    <dd><a href="javascript:;" onclick="loginOut()">退出</a></dd>
                </dl>
            </li>
        </ul>
    </div>

    <div class="layui-side layui-bg-black">
        <div class="layui-side-scroll">
            <!-- 左侧导航区域（可配合layui已有的垂直导航） -->
            <ul class="layui-nav layui-nav-tree" lay-filter="left-menu" lay-shrink="all">
                <#if role! == 'root'><#--根-->
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-form"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;</span><span>订单模块</span></a>
                        <dl class="layui-nav-child">
                            <#--<dd><a id="m1" class="menuTag" href="javascript:;" url="orderStart"><span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><i class="layui-icon layui-icon-file"></i><span>&nbsp;&nbsp;订单信息</span></a></dd>-->
                            <#--<dd><a id="m92" class="menuTag" href="javascript:;" url="clothesVersionProcessStart"><span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><i class="layui-icon layui-icon-app"></i><span>&nbsp;&nbsp;版单进度</span></a></dd>-->
                            <dd><a id="m114" class="menuTag" href="javascript:;" url="processOrderStart"><span>制单信息</span></a></dd>
                            <dd><a id="m125" class="menuTag" href="javascript:;" url="weekReportStart"><span>一周看板</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-rate-solid"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;票菲管理</span></a>
                        <dl class="layui-nav-child">
                        <#--<dd><a id="m2" class="menuTag" href="javascript:;" url="singleTailorStart">票菲打印</a></dd>-->
                            <dd><a id="m3" class="menuTag" href="javascript:;" url="multiTailorStart"><span>主身生成</span></a></dd>
                            <dd><a id="m12" class="menuTag" href="javascript:;" url="otherMultiTailorStart"><span>配料生成</span></a></dd>
                            <dd><a id="m4" class="menuTag" href="javascript:;" url="tailorReprintStart"><span>票菲打印</span></a></dd>
                            <dd><a id="m50" class="menuTag" href="javascript:;" url="tailorChangeCodeStart"><span>票菲改码</span></a></dd>
                            <dd><a id="m5" class="menuTag" href="javascript:;" url="bedTailorInfoStart"><span>票菲删除</span></a></dd>
                            <dd><a id="m127" class="menuTag" href="javascript:;" url="recoveryStart"><span>误删恢复</span></a></dd>
                            <#--<dd><a id="m6" class="menuTag" href="javascript:;" url="oneTailorInfoStart"><span>按扎删除</span></a></dd>-->
                        </dl>
                    </li>
                    <#--<li class="layui-nav-item ">-->
                        <#--<a class="bigMenuTag"><i class="layui-icon layui-icon-rate-half"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;裁床配料管理</span></a>-->
                        <#--<dl class="layui-nav-child">-->
                            <#--<dd><a id="m12" class="menuTag" href="javascript:;" url="otherMultiTailorStart"><span>配料生成</span></a></dd>-->
                            <#--<dd><a id="m11" class="menuTag" href="javascript:;" url="otherTailorReprintStart"><span>配料打印</span></a></dd>-->
                            <#--<dd><a id="m106" class="menuTag" href="javascript:;" url="otherTailorChangeCodeStart"><span>配料改码</span></a></dd>-->
                            <#--&lt;#&ndash;<dd><a id="m9" class="menuTag" href="javascript:;" url="bedOtherTailorInfoStart"><span>按床删除</span></a></dd>&ndash;&gt;-->
                            <#--&lt;#&ndash;<dd><a id="m10" class="menuTag" href="javascript:;" url="oneOtherTailorInfoStart"><span>按扎删除</span></a></dd>&ndash;&gt;-->
                        <#--</dl>-->
                    <#--</li>-->
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-read"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;裁床报表</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m13" class="menuTag" href="javascript:;" url="cutReportStart"><span>裁床单</span></a></dd>
                            <dd><a id="m14" class="menuTag" href="javascript:;" url="cutQueryLeakStart"><span>单款报表查询</span></a></dd>
                            <dd><a id="m15" class="menuTag" href="javascript:;" url="cutMonthReportStart"><span>月度报表查询</span></a></dd>
                            <dd><a id="m110" class="menuTag" href="javascript:;" url="unitConsumptionStart"><span>单耗查询</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-export"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;花片收发管理</span></a>
                        <dl class="layui-nav-child">
                            <#--<dd><a id="m19" class="menuTag" href="javascript:;" url="opaStart"><span>花片出厂</span></a></dd>-->
                            <#--<dd><a id="m20" class="menuTag" href="javascript:;" url="otherOpaStart"><span>士啤出厂</span></a></dd>-->
                            <#--<dd><a id="m21" class="menuTag" href="javascript:;" url="opaBackInputStart"><span>花片回厂</span></a></dd>-->
                            <dd><a id="m22" class="menuTag" href="javascript:;" url="opaReportStart"><span>单款花片查询</span></a></dd>
                            <#--<dd><a id="m91" class="menuTag" href="javascript:;" url="monthOpaInfoStart"><span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><i class="layui-icon layui-icon-search"></i><span>&nbsp;&nbsp;月度花片查询</span></a></dd>-->
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-upload-circle"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;裁片&衣胚查询</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m107" class="menuTag" href="javascript:;" url="allStorageStart"><span>裁片库存</span></a></dd>
                            <dd><a id="m108" class="menuTag" href="javascript:;" url="allEmbStorageStart"><span>衣胚库存</span></a></dd>
                            <#--<dd><a id="m33" class="menuTag" href="javascript:;" url="cutStorageQueryStart"><span>裁片单款库存</span></a></dd>-->
                            <#--<dd><a id="m34" class="menuTag" href="javascript:;" url="embStorageQueryStart"><span>衣胚单款库存</span></a></dd>-->
                            <dd><a id="m35" class="menuTag" href="javascript:;" url="embOutDetailStart"><span>衣胚阶段出库</span></a></dd>
                            <dd><a id="m36" class="menuTag" href="javascript:;" url="embOutPackageDetailStart"><span>衣胚出库详情</span></a></dd>
                            <dd><a id="m37" class="menuTag" href="javascript:;" url="queryEmbLeakStart"><span>衣胚出库汇总</span></a></dd>
                            <dd><a id="m38" class="menuTag" href="javascript:;" url="cutLocationStart"><span>裁片位置详情</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-tree"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;面辅料管理</span></a>
                        <dl class="layui-nav-child">
                            <#--<dd><a id="m115" class="menuTag" href="javascript:;" url="fabricAccessoryStart"><span>面辅料采购</span></a></dd>-->
                            <dd><a id="m136" class="menuTag" href="javascript:;" url="fabricAccessoryCheckStart"><span>一站式下单</span></a></dd>
                            <dd><a id="m117" class="menuTag" href="javascript:;" url="fabricReturnStart"><span>面料管理</span></a></dd>
                            <dd><a id="m118" class="menuTag" href="javascript:;" url="accessoryInStoreStart"><span>辅料管理</span></a></dd>
                            <#--<dd><a id="m120" class="menuTag" href="javascript:;" url="placeOrderStart"><span>面辅料审核</span></a></dd>-->
                            <#--<dd><a id="m133" class="menuTag" href="javascript:;" url="orderCheckStart"><span>面辅料合同</span></a></dd>-->
                            <dd><a id="m42" class="menuTag" href="javascript:;" url="looseFabricStart"><span>松布打印</span></a></dd>
                            <dd><a id="m43" class="menuTag" href="javascript:;" url="looseFabricPrintStart"><span>松布补打</span></a></dd>
                            <dd><a id="m111" class="menuTag" href="javascript:;" url="fabricLoChangeStart"><span>LO色修改</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-camera"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;面辅料报表</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m46" class="menuTag" href="javascript:;" url="fabricQueryStart"><span>面料库存</span></a></dd>
                            <dd><a id="m119" class="menuTag" href="javascript:;" url="accessoryStorageStart"><span>辅料库存</span></a></dd>
                            <dd><a id="m47" class="menuTag" href="javascript:;" url="fabricOutRecordStart"><span>面料出库详情</span></a></dd>
                            <dd><a id="m134" class="menuTag" href="javascript:;" url="fabricAccessoryPrintStart"><span>出入库打印</span></a></dd>
                            <dd><a id="m48" class="menuTag" href="javascript:;" url="fabricLeakStart"><span>单款面辅料详情</span></a></dd>
                            <dd><a id="m49" class="menuTag" href="javascript:;" url="looseFabricSearchStart"><span>松布查询</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-website"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;IE基础资料</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m51" class="menuTag" href="javascript:;" url="procedureTemplateStart"><span>工序数据库</span></a></dd>
                            <dd><a id="m52" class="menuTag" href="javascript:;" url="orderProcedureStart"><span>订单工序</span></a></dd>
                            <dd><a id="m53" class="menuTag" href="javascript:;" url="procedureProgressStart"><span>工序进度</span></a></dd>
                            <dd><a id="m54" class="menuTag" href="javascript:;" url="procedureLevelStart"><span>工序等级</span></a></dd>
                            <dd><a id="m200" class="menuTag" href="javascript:;" url="beatStart"><span>节拍查询</span></a></dd>
                            <dd><a id="m58" class="menuTag" href="javascript:;" url="completionRateStart"><span>达成率</span></a></dd>
                            <dd><a id="m56" class="menuTag" href="javascript:;" url="printPartStart"><span>票菲部位管理</span></a></dd>
                            <dd><a id="m90" class="menuTag" href="javascript:;" url="specialProcedureStart"><span>特殊工序</span></a>
                            </dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-console"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;计划排产</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m112" class="menuTag" href="javascript:;" url="embPlanStart"><span>衣胚计划</span></a></dd>
                            <dd><a id="m113" class="menuTag" href="javascript:;" url="departmentPlanStart"><span>松布裁剪计划</span></a></dd>
                            <#--<dd><a id="m57" class="menuTag" href="javascript:;" url="groupPlanStart"><span>车缝计划</span></a></dd>-->
                            <dd><a id="m126" class="menuTag" href="javascript:;" url="sewingPlanStart"><span>计划总览</span></a></dd>
                            <dd><a id="m124" class="menuTag" href="javascript:;" url="empSkillsStart"><span>技能库</span></a></dd>
                            <dd><a id="m123" class="menuTag" href="javascript:;" url="orderScheduleStart"><span>自动排产</span></a></dd>
                            <dd><a id="m132" class="menuTag" href="javascript:;" url="prenatalProgressSummaryStart"><span>产前进度</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="fa fa-film"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;计件管理</span></a>
                        <dl class="layui-nav-child">
                            <#--<dd><a id="m60" class="menuTag" href="javascript:;" url="dispatchStart"><span>工序分配</span></a></dd>-->
                            <#--<dd><a id="m61" class="menuTag" href="javascript:;" url="pieceWorkStatisticStart"><span>产能统计</span></a></dd>-->
                            <dd><a id="m116" class="menuTag" href="javascript:;" url="pieceWorkLeakStart"><span>计件扎号详情</span></a></dd>
                            <dd><a id="m62" class="menuTag" href="javascript:;" url="pieceWorkManageStart"><span>计件管理</span></a></dd>
                            <dd><a id="m63" class="menuTag" href="javascript:;" url="manualInputStart"><span>手工入数</span></a></dd>
                            <dd><a id="m64" class="menuTag" href="javascript:;" url="hourEmpStart"><span>时工入数</span></a></dd>
                            <#--<dd><a id="m65" class="menuTag" href="javascript:;" url="wrongStart"><span>疵点代码</span></a></dd>-->
                            <#--<dd><a id="m66" class="menuTag" href="javascript:;" url="inspectionStart"><span>抽查疵点统计</span></a></dd>-->
                            <#--<dd><a id="m67" class="menuTag" href="javascript:;" url="sampleInspectionStart"><span>QC疵点统计</span></a>-->
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-slider"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;财务数据</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m68" class="menuTag" href="javascript:;" url="empSalaryStart"><span>员工个人产能</span></a></dd>
                            <dd><a id="m69" class="menuTag" href="javascript:;" url="groupSalaryStart"><span>员工分组产能</span></a></dd>
                            <#--<dd><a id="m70" class="menuTag" href="javascript:;" url="cutDailyDetailStart"><span>裁床每日明细</span></a></dd>-->
                            <dd><a id="m109" class="menuTag" href="javascript:;" url="groupProgressStart"><span>区间产能汇总</span></a></dd>
                            <dd><a id="m104" class="menuTag" href="javascript:;" url="pieceWorkDetailStart"><span>产能详情</span></a></dd>
                            <dd><a id="m131" class="menuTag" href="javascript:;" url="costAccountStart"><span>成本核算</span></a></dd>
                            <dd><a id="m105" class="menuTag" href="javascript:;" url="fixedSalaryStart"><span>锁定工资</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-engine"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;行政管理</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m99" class="menuTag" href="javascript:;" url="staffStart"><span>员工管理</span></a></dd>
                            <dd><a id="m100" class="menuTag" href="javascript:;" url="roomStart"><span>宿舍管理</span></a></dd>
                            <dd><a id="m101" class="menuTag" href="javascript:;" url="empRoomStart"><span>入住详情</span></a></dd>
                            <dd><a id="m102" class="menuTag" href="javascript:;" url="accommodationStart"><span>住宿管理</span></a></dd>
                            <dd><a id="m121" class="menuTag" href="javascript:;" url="checkDetailStart"><span>考勤管理</span></a></dd>
                            <dd><a id="m78" class="menuTag" href="javascript:;" url="userStart"><span>用户信息</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-carousel"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;生产报表数据</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m72" class="menuTag" href="javascript:;" url="procedureBalanceStart"><span>订单工序平衡</span></a>
                            </dd>
                            <#--<dd><a id="m73" class="menuTag" href="javascript:;" url="completionStart"><span>完工结算</span></a></dd>-->
                            <dd><a id="m74" class="menuTag" href="javascript:;" url="reWorkStart"><span>返工率</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-app"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;工厂基础信息</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m75" class="menuTag" href="javascript:;" url="storeHouseStart"><span>仓库信息</span></a></dd>
                            <dd><a id="m76" class="menuTag" href="javascript:;" url="embStoreStart"><span>衣胚仓库</span></a></dd>
                            <dd><a id="m77" class="menuTag" href="javascript:;" url="sizeManageStart"><span>尺码管理</span></a></dd>
                            <#--<dd><a id="m78" class="menuTag" href="javascript:;" url="userStart"><span>用户信息</span></a></dd>-->
                            <#--<dd><a id="m79" class="menuTag" href="javascript:;" url="staffStart"><span>员工信息</span></a></dd>-->
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-reply-fill"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;其他计件</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m81" class="menuTag" href="javascript:;" url="otherPieceWorkStart"><span>其他计件</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-rate"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;后整打菲</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m82" class="menuTag" href="javascript:;" url="finishTailorStart"><span>票菲生成</span></a></dd>
                            <dd><a id="m85" class="menuTag" href="javascript:;" url="finishTailorPrintStart"><span>票菲打印</span></a></dd>
                            <dd><a id="m83" class="menuTag" href="javascript:;" url="finishTailorDeleteStart"><span>菲票管理</span></a></dd>
                            <dd><a id="m84" class="menuTag" href="javascript:;" url="finishTailorLeakStart"><span>后整打菲报表</span></a></dd>
                        </dl>
                    </li>
                <#elseif role! == 'role1'> <#--裁床-->
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-form"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;订单模块</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m114" class="menuTag" href="javascript:;" url="processOrderStart"><span>制单信息</span></a></dd>
                            <dd><a id="m125" class="menuTag" href="javascript:;" url="weekReportStart"><span>一周看板</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-rate-solid"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;票菲管理</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m3" class="menuTag" href="javascript:;" url="multiTailorStart"><span>票菲生成</span></a></dd>
                            <dd><a id="m12" class="menuTag" href="javascript:;" url="otherMultiTailorStart"><span>配料生成</span></a></dd>
                            <dd><a id="m4" class="menuTag" href="javascript:;" url="tailorReprintStart"><span>票菲打印</span></a></dd>
                            <dd><a id="m50" class="menuTag" href="javascript:;" url="tailorChangeCodeStart"><span>票菲改码</span></a></dd>
                            <dd><a id="m5" class="menuTag" href="javascript:;" url="bedTailorInfoStart"><span>票菲删除</span></a></dd>
                            <dd><a id="m127" class="menuTag" href="javascript:;" url="recoveryStart"><span>误删恢复</span></a></dd>
                            <#--<dd><a id="m6" class="menuTag" href="javascript:;" url="oneTailorInfoStart"><span>按扎删除</span></a></dd>-->
                        </dl>
                    </li>
                    <#--<li class="layui-nav-item ">-->
                        <#--<a class="bigMenuTag"><i class="layui-icon layui-icon-rate-half"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;裁床配料管理</span></a>-->
                        <#--<dl class="layui-nav-child">-->
                            <#--<dd><a id="m12" class="menuTag" href="javascript:;" url="otherMultiTailorStart"><span>配料生成</span></a></dd>-->
                            <#--<dd><a id="m11" class="menuTag" href="javascript:;" url="otherTailorReprintStart"><span>配料打印</span></a></dd>-->
                            <#--<dd><a id="m106" class="menuTag" href="javascript:;" url="otherTailorChangeCodeStart"><span>配料改码</span></a></dd>-->
                            <#--&lt;#&ndash;<dd><a id="m9" class="menuTag" href="javascript:;" url="bedOtherTailorInfoStart"><span>按床删除</span></a></dd>&ndash;&gt;-->
                            <#--&lt;#&ndash;<dd><a id="m10" class="menuTag" href="javascript:;" url="oneOtherTailorInfoStart"><span>按扎删除</span></a></dd>&ndash;&gt;-->
                        <#--</dl>-->
                    <#--</li>-->
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-read"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;裁床报表</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m13" class="menuTag" href="javascript:;" url="cutReportStart"><span>裁床单</span></a></dd>
                            <dd><a id="m14" class="menuTag" href="javascript:;" url="cutQueryLeakStart"><span>单款报表查询</span></a></dd>
                            <dd><a id="m15" class="menuTag" href="javascript:;" url="cutMonthReportStart"><span>月度报表查询</span></a></dd>
                            <dd><a id="m110" class="menuTag" href="javascript:;" url="unitConsumptionStart"><span>单耗查询</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-export"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;花片收发管理</span></a>
                        <dl class="layui-nav-child">
                            <#--<dd><a id="m19" class="menuTag" href="javascript:;" url="opaStart"><span>花片出厂</span></a></dd>-->
                            <#--<dd><a id="m20" class="menuTag" href="javascript:;" url="otherOpaStart"><span>士啤出厂</span></a></dd>-->
                            <#--<dd><a id="m21" class="menuTag" href="javascript:;" url="opaBackInputStart"><span>花片回厂</span></a></dd>-->
                            <dd><a id="m22" class="menuTag" href="javascript:;" url="opaReportStart"><span>单款花片查询</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-upload-circle"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;裁片&衣胚查询</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m107" class="menuTag" href="javascript:;" url="allStorageStart"><span>裁片库存</span></a></dd>
                            <dd><a id="m108" class="menuTag" href="javascript:;" url="allEmbStorageStart"><span>衣胚库存</span></a></dd>
                            <dd><a id="m35" class="menuTag" href="javascript:;" url="embOutDetailStart"><span>衣胚阶段出库</span></a></dd>
                            <dd><a id="m36" class="menuTag" href="javascript:;" url="embOutPackageDetailStart"><span>衣胚出库详情</span></a></dd>
                            <dd><a id="m37" class="menuTag" href="javascript:;" url="queryEmbLeakStart"><span>衣胚出库汇总</span></a></dd>
                            <dd><a id="m38" class="menuTag" href="javascript:;" url="cutLocationStart"><span>裁片位置详情</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-camera"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;面辅料报表</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m46" class="menuTag" href="javascript:;" url="fabricQueryStart"><span>面料库存</span></a></dd>
                            <dd><a id="m47" class="menuTag" href="javascript:;" url="fabricOutRecordStart"><span>面料出库详情</span></a></dd>
                            <dd><a id="m48" class="menuTag" href="javascript:;" url="fabricLeakStart"><span>单款面辅料详情</span></a></dd>
                            <dd><a id="m49" class="menuTag" href="javascript:;" url="looseFabricSearchStart"><span>松布查询</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-website"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;IE基础资料</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m56" class="menuTag" href="javascript:;" url="printPartStart"><span>票菲部位管理</span></a></dd>
                            </dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="fa fa-film"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;计件管理</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m116" class="menuTag" href="javascript:;" url="pieceWorkLeakStart"><span>计件扎号详情</span></a></dd>
                            <dd><a id="m62" class="menuTag" href="javascript:;" url="pieceWorkManageStart"><span>计件管理</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-console"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;计划排产</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m112" class="menuTag" href="javascript:;" url="embPlanStart"><span>衣胚计划</span></a></dd>
                            <dd><a id="m113" class="menuTag" href="javascript:;" url="departmentPlanStart"><span>松布裁剪计划</span></a></dd>
                            <dd><a id="m126" class="menuTag" href="javascript:;" url="sewingPlanStart"><span>计划总览</span></a></dd>
                            <dd><a id="m132" class="menuTag" href="javascript:;" url="prenatalProgressSummaryStart"><span>产前进度</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-slider"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;财务数据</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m68" class="menuTag" href="javascript:;" url="empSalaryStart"><span>员工个人产能</span></a></dd>
                            <dd><a id="m69" class="menuTag" href="javascript:;" url="groupSalaryStart"><span>员工分组产能</span></a></dd>
                            <#--<dd><a id="m70" class="menuTag" href="javascript:;" url="cutDailyDetailStart"><span>裁床每日明细</span></a></dd>-->
                            <dd><a id="m109" class="menuTag" href="javascript:;" url="groupProgressStart"><span>区间产能汇总</span></a></dd>
                            <dd><a id="m104" class="menuTag" href="javascript:;" url="pieceWorkDetailStart"><span>产能详情</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-carousel"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;生产报表数据</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m72" class="menuTag" href="javascript:;" url="procedureBalanceStart"><span>订单工序平衡</span></a>
                            </dd>
                            <#--<dd><a id="m73" class="menuTag" href="javascript:;" url="completionStart"><span>完工结算</span></a></dd>-->
                            <dd><a id="m74" class="menuTag" href="javascript:;" url="reWorkStart"><span>返工率</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-rate"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;后整打菲</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m82" class="menuTag" href="javascript:;" url="finishTailorStart"><span>票菲生成</span></a></dd>
                            <dd><a id="m85" class="menuTag" href="javascript:;" url="finishTailorPrintStart"><span>票菲打印</span></a></dd>
                            <dd><a id="m83" class="menuTag" href="javascript:;" url="finishTailorDeleteStart"><span>菲票管理</span></a></dd>
                            <dd><a id="m84" class="menuTag" href="javascript:;" url="finishTailorLeakStart"><span>后整打菲报表</span></a></dd>
                        </dl>
                    </li>
                <#elseif role! == 'role2'> <#--业务-->
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-form"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;订单模块</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m114" class="menuTag" href="javascript:;" url="processOrderStart"><span>制单信息</span></a></dd>
                            <dd><a id="m125" class="menuTag" href="javascript:;" url="weekReportStart"><span>一周看板</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-read"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;裁床报表</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m13" class="menuTag" href="javascript:;" url="cutReportStart"><span>裁床单</span></a></dd>
                            <dd><a id="m14" class="menuTag" href="javascript:;" url="cutQueryLeakStart"><span>单款报表查询</span></a></dd>
                            <dd><a id="m15" class="menuTag" href="javascript:;" url="cutMonthReportStart"><span>月度报表查询</span></a>
                            </dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-export"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;花片收发管理</span></a>
                        <dl class="layui-nav-child">
                            <#--<dd><a id="m19" class="menuTag" href="javascript:;" url="opaStart"><span>花片出厂</span></a></dd>-->
                            <#--<dd><a id="m20" class="menuTag" href="javascript:;" url="otherOpaStart"><span>士啤出厂</span></a></dd>-->
                            <#--<dd><a id="m21" class="menuTag" href="javascript:;" url="opaBackInputStart"><span>花片回厂</span></a></dd>-->
                            <dd><a id="m22" class="menuTag" href="javascript:;" url="opaReportStart"><span>单款花片查询</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-upload-circle"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;裁片&衣胚查询</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m107" class="menuTag" href="javascript:;" url="allStorageStart"><span>裁片库存</span></a></dd>
                            <dd><a id="m108" class="menuTag" href="javascript:;" url="allEmbStorageStart"><span>衣胚库存</span></a></dd>
                            <dd><a id="m35" class="menuTag" href="javascript:;" url="embOutDetailStart"><span>衣胚阶段出库</span></a></dd>
                            <dd><a id="m36" class="menuTag" href="javascript:;" url="embOutPackageDetailStart"><span>衣胚出库详情</span></a></dd>
                            <dd><a id="m37" class="menuTag" href="javascript:;" url="queryEmbLeakStart"><span>衣胚出库汇总</span></a></dd>
                            <dd><a id="m38" class="menuTag" href="javascript:;" url="cutLocationStart"><span>裁片位置详情</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-tree"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;面辅料管理</span></a>
                        <dl class="layui-nav-child">
                            <#--<dd><a id="m115" class="menuTag" href="javascript:;" url="fabricAccessoryStart"><span>面辅料采购</span></a></dd>-->
                            <dd><a id="m136" class="menuTag" href="javascript:;" url="fabricAccessoryCheckStart"><span>一站式下单</span></a></dd>
                            <dd><a id="m117" class="menuTag" href="javascript:;" url="fabricReturnStart"><span>面料管理</span></a></dd>
                            <dd><a id="m118" class="menuTag" href="javascript:;" url="accessoryInStoreStart"><span>辅料管理</span></a></dd>
                            <#--<dd><a id="m120" class="menuTag" href="javascript:;" url="placeOrderStart"><span>面辅料审核</span></a></dd>-->
                            <#--<dd><a id="m133" class="menuTag" href="javascript:;" url="orderCheckStart"><span>面辅料合同</span></a></dd>-->
                            <dd><a id="m42" class="menuTag" href="javascript:;" url="looseFabricStart"><span>松布打印</span></a></dd>
                            <dd><a id="m43" class="menuTag" href="javascript:;" url="looseFabricPrintStart"><span>松布补打</span></a></dd>
                            <dd><a id="m111" class="menuTag" href="javascript:;" url="fabricLoChangeStart"><span>LO色修改</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-camera"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;面辅料报表</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m46" class="menuTag" href="javascript:;" url="fabricQueryStart"><span>面料库存</span></a></dd>
                            <dd><a id="m47" class="menuTag" href="javascript:;" url="fabricOutRecordStart"><span>面料出库详情</span></a></dd>
                            <dd><a id="m48" class="menuTag" href="javascript:;" url="fabricLeakStart"><span>单款面辅料详情</span></a></dd>
                            <dd><a id="m49" class="menuTag" href="javascript:;" url="looseFabricSearchStart"><span>松布查询</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-console"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;计划排产</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m112" class="menuTag" href="javascript:;" url="embPlanStart"><span>衣胚计划</span></a></dd>
                            <dd><a id="m113" class="menuTag" href="javascript:;" url="departmentPlanStart"><span>松布裁剪计划</span></a></dd>
                            <dd><a id="m126" class="menuTag" href="javascript:;" url="sewingPlanStart"><span>计划总览</span></a></dd>
                            <dd><a id="m132" class="menuTag" href="javascript:;" url="prenatalProgressSummaryStart"><span>产前进度</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="fa fa-film"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;计件管理</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m116" class="menuTag" href="javascript:;" url="pieceWorkLeakStart"><span>计件扎号详情</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-carousel"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;生产报表数据</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m72" class="menuTag" href="javascript:;" url="procedureBalanceStart"><span>订单工序平衡</span></a></dd>
                            <#--<dd><a id="m73" class="menuTag" href="javascript:;" url="completionStart"><span>完工结算</span></a></dd>-->
                            <dd><a id="m74" class="menuTag" href="javascript:;" url="reWorkStart"><span>返工率</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-app"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;工厂基础信息</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m77" class="menuTag" href="javascript:;" url="sizeManageStart"><span>尺码管理</span></a></dd>
                        </dl>
                    </li>
                <#elseif role! == 'role3'>
                <#elseif role! == 'role4'><#--前台-->
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-form"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;</span><span>订单模块</span></a>
                        <dl class="layui-nav-child">
                            <#--<dd><a id="m1" class="menuTag" href="javascript:;" url="orderStart"><span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><i class="layui-icon layui-icon-file"></i><span>&nbsp;&nbsp;订单信息</span></a></dd>-->
                            <#--<dd><a id="m92" class="menuTag" href="javascript:;" url="clothesVersionProcessStart"><span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><i class="layui-icon layui-icon-app"></i><span>&nbsp;&nbsp;版单进度</span></a></dd>-->
                            <dd><a id="m114" class="menuTag" href="javascript:;" url="processOrderStart"><span>制单信息</span></a></dd>
                            <dd><a id="m125" class="menuTag" href="javascript:;" url="weekReportStart"><span>一周看板</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-read"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;裁床报表</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m13" class="menuTag" href="javascript:;" url="cutReportStart"><span>裁床单</span></a></dd>
                            <dd><a id="m14" class="menuTag" href="javascript:;" url="cutQueryLeakStart"><span>单款报表查询</span></a></dd>
                            <dd><a id="m15" class="menuTag" href="javascript:;" url="cutMonthReportStart"><span>月度报表查询</span></a></dd>
                            <dd><a id="m110" class="menuTag" href="javascript:;" url="unitConsumptionStart"><span>单耗查询</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-export"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;花片收发管理</span></a>
                        <dl class="layui-nav-child">
                            <#--<dd><a id="m19" class="menuTag" href="javascript:;" url="opaStart"><span>花片出厂</span></a></dd>-->
                            <#--<dd><a id="m20" class="menuTag" href="javascript:;" url="otherOpaStart"><span>士啤出厂</span></a></dd>-->
                            <#--<dd><a id="m21" class="menuTag" href="javascript:;" url="opaBackInputStart"><span>花片回厂</span></a></dd>-->
                            <dd><a id="m22" class="menuTag" href="javascript:;" url="opaReportStart"><span>单款花片查询</span></a></dd>
                            <#--<dd><a id="m91" class="menuTag" href="javascript:;" url="monthOpaInfoStart"><span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><i class="layui-icon layui-icon-search"></i><span>&nbsp;&nbsp;月度花片查询</span></a></dd>-->
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-upload-circle"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;裁片&衣胚查询</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m107" class="menuTag" href="javascript:;" url="allStorageStart"><span>裁片库存</span></a></dd>
                            <dd><a id="m108" class="menuTag" href="javascript:;" url="allEmbStorageStart"><span>衣胚库存</span></a></dd>
                            <dd><a id="m35" class="menuTag" href="javascript:;" url="embOutDetailStart"><span>衣胚阶段出库</span></a></dd>
                            <dd><a id="m36" class="menuTag" href="javascript:;" url="embOutPackageDetailStart"><span>衣胚出库详情</span></a></dd>
                            <dd><a id="m37" class="menuTag" href="javascript:;" url="queryEmbLeakStart"><span>衣胚出库汇总</span></a></dd>
                            <dd><a id="m38" class="menuTag" href="javascript:;" url="cutLocationStart"><span>裁片位置详情</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-camera"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;面辅料报表</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m46" class="menuTag" href="javascript:;" url="fabricQueryStart"><span>面料库存</span></a></dd>
                            <dd><a id="m119" class="menuTag" href="javascript:;" url="accessoryStorageStart"><span>辅料库存</span></a></dd>
                            <dd><a id="m47" class="menuTag" href="javascript:;" url="fabricOutRecordStart"><span>面料出库详情</span></a></dd>
                            <dd><a id="m48" class="menuTag" href="javascript:;" url="fabricLeakStart"><span>单款面辅料详情</span></a></dd>
                            <dd><a id="m49" class="menuTag" href="javascript:;" url="looseFabricSearchStart"><span>松布查询</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-slider"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;财务数据</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m68" class="menuTag" href="javascript:;" url="empSalaryStart"><span>员工个人产能</span></a></dd>
                            <dd><a id="m69" class="menuTag" href="javascript:;" url="groupSalaryStart"><span>员工分组产能</span></a></dd>
                            <#--<dd><a id="m70" class="menuTag" href="javascript:;" url="cutDailyDetailStart"><span>裁床每日明细</span></a></dd>-->
                            <dd><a id="m109" class="menuTag" href="javascript:;" url="groupProgressStart"><span>区间产能汇总</span></a></dd>
                            <dd><a id="m104" class="menuTag" href="javascript:;" url="pieceWorkDetailStart"><span>产能详情</span></a></dd>
                            <dd><a id="m105" class="menuTag" href="javascript:;" url="fixedSalaryStart"><span>锁定工资</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-engine"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;行政管理</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m99" class="menuTag" href="javascript:;" url="staffStart"><span>员工管理</span></a></dd>
                            <dd><a id="m100" class="menuTag" href="javascript:;" url="roomStart"><span>宿舍管理</span></a></dd>
                            <dd><a id="m101" class="menuTag" href="javascript:;" url="empRoomStart"><span>入住详情</span></a></dd>
                            <dd><a id="m102" class="menuTag" href="javascript:;" url="accommodationStart"><span>住宿管理</span></a></dd>
                            <dd><a id="m121" class="menuTag" href="javascript:;" url="checkDetailStart"><span>考勤管理</span></a></dd>
                            <dd><a id="m78" class="menuTag" href="javascript:;" url="userStart"><span>用户信息</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-carousel"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;生产报表数据</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m72" class="menuTag" href="javascript:;" url="procedureBalanceStart"><span>订单工序平衡</span></a>
                            </dd>
                            <#--<dd><a id="m73" class="menuTag" href="javascript:;" url="completionStart"><span>完工结算</span></a></dd>-->
                            <dd><a id="m74" class="menuTag" href="javascript:;" url="reWorkStart"><span>返工率</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-app"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;工厂基础信息</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m75" class="menuTag" href="javascript:;" url="storeHouseStart"><span>仓库信息</span></a></dd>
                            <dd><a id="m76" class="menuTag" href="javascript:;" url="embStoreStart"><span>衣胚仓库</span></a></dd>
                            <dd><a id="m77" class="menuTag" href="javascript:;" url="customerStart"><span>顾客信息</span></a></dd>
                            <#--<dd><a id="m78" class="menuTag" href="javascript:;" url="userStart"><span>用户信息</span></a></dd>-->
                            <#--<dd><a id="m79" class="menuTag" href="javascript:;" url="staffStart"><span>员工信息</span></a></dd>-->
                        </dl>
                    </li>
                <#elseif role! == 'role5'> <#--IE-->
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-form"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;订单模块</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m114" class="menuTag" href="javascript:;" url="processOrderStart"><span>制单信息</span></a></dd>
                            <dd><a id="m125" class="menuTag" href="javascript:;" url="weekReportStart"><span>一周看板</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-read"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;裁床报表</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m13" class="menuTag" href="javascript:;" url="cutReportStart"><span>裁床单</span></a></dd>
                            <dd><a id="m14" class="menuTag" href="javascript:;" url="cutQueryLeakStart"><span>单款报表查询</span></a></dd>
                            <dd><a id="m15" class="menuTag" href="javascript:;" url="cutMonthReportStart"><span>月度报表查询</span></a></dd>
                            <dd><a id="m110" class="menuTag" href="javascript:;" url="unitConsumptionStart"><span>单耗查询</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-upload-circle"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;裁片&衣胚查询</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m107" class="menuTag" href="javascript:;" url="allStorageStart"><span>裁片库存</span></a></dd>
                            <dd><a id="m108" class="menuTag" href="javascript:;" url="allEmbStorageStart"><span>衣胚库存</span></a></dd>
                            <dd><a id="m35" class="menuTag" href="javascript:;" url="embOutDetailStart"><span>衣胚阶段出库</span></a></dd>
                            <dd><a id="m36" class="menuTag" href="javascript:;" url="embOutPackageDetailStart"><span>衣胚出库详情</span></a></dd>
                            <dd><a id="m37" class="menuTag" href="javascript:;" url="queryEmbLeakStart"><span>衣胚出库汇总</span></a></dd>
                            <dd><a id="m38" class="menuTag" href="javascript:;" url="cutLocationStart"><span>裁片位置详情</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-export"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;花片收发管理</span></a>
                        <dl class="layui-nav-child">
                            <#--<dd><a id="m19" class="menuTag" href="javascript:;" url="opaStart"><span>花片出厂</span></a></dd>-->
                            <#--<dd><a id="m20" class="menuTag" href="javascript:;" url="otherOpaStart"><span>士啤出厂</span></a></dd>-->
                            <#--<dd><a id="m21" class="menuTag" href="javascript:;" url="opaBackInputStart"><span>花片回厂</span></a></dd>-->
                            <dd><a id="m22" class="menuTag" href="javascript:;" url="opaReportStart"><span>单款花片查询</span></a></dd>
                            <#--<dd><a id="m91" class="menuTag" href="javascript:;" url="monthOpaInfoStart"><span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><i class="layui-icon layui-icon-search"></i><span>&nbsp;&nbsp;月度花片查询</span></a></dd>-->
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-camera"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;面辅料报表</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m46" class="menuTag" href="javascript:;" url="fabricQueryStart"><span>面料库存</span></a></dd>
                            <dd><a id="m119" class="menuTag" href="javascript:;" url="accessoryStorageStart"><span>辅料库存</span></a></dd>
                            <dd><a id="m47" class="menuTag" href="javascript:;" url="fabricOutRecordStart"><span>面料出库详情</span></a></dd>
                            <dd><a id="m134" class="menuTag" href="javascript:;" url="fabricAccessoryPrintStart"><span>出入库打印</span></a></dd>
                            <dd><a id="m48" class="menuTag" href="javascript:;" url="fabricLeakStart"><span>单款面辅料详情</span></a></dd>
                            <dd><a id="m49" class="menuTag" href="javascript:;" url="looseFabricSearchStart"><span>松布查询</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-website"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;IE基础资料</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m51" class="menuTag" href="javascript:;" url="procedureTemplateStart"><span>工序数据库</span></a>
                            </dd>
                            <dd><a id="m52" class="menuTag" href="javascript:;" url="orderProcedureStart"><span>订单工序</span></a></dd>
                            <dd><a id="m53" class="menuTag" href="javascript:;" url="procedureProgressStart"><span>工序进度</span></a>
                            </dd>
                            <dd><a id="m54" class="menuTag" href="javascript:;" url="procedureLevelStart"><span>工序等级</span></a></dd>
                            <dd><a id="m200" class="menuTag" href="javascript:;" url="beatStart"><span>节拍查询</span></a></dd>
                            <dd><a id="m56" class="menuTag" href="javascript:;" url="printPartStart"><span>票菲部位管理</span></a></dd>
                            <dd><a id="m57" class="menuTag" href="javascript:;" url="groupPlanStart"><span>计划产能</span></a></dd>
                            <dd><a id="m58" class="menuTag" href="javascript:;" url="completionRateStart"><span>达成率</span></a></dd>
                            <dd><a id="m90" class="menuTag" href="javascript:;" url="specialProcedureStart"><span>特殊工序</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-console"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;计划排产</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m112" class="menuTag" href="javascript:;" url="embPlanStart"><span>衣胚计划</span></a></dd>
                            <#--<dd><a id="m113" class="menuTag" href="javascript:;" url="departmentPlanStart"><span>部门计划</span></a></dd>-->
                            <#--<dd><a id="m57" class="menuTag" href="javascript:;" url="groupPlanStart"><span>车缝计划</span></a></dd>-->
                            <dd><a id="m126" class="menuTag" href="javascript:;" url="sewingPlanStart"><span>计划总览</span></a></dd>
                            <dd><a id="m124" class="menuTag" href="javascript:;" url="empSkillsStart"><span>技能库</span></a></dd>
                            <dd><a id="m123" class="menuTag" href="javascript:;" url="orderScheduleStart"><span>自动排产</span></a></dd>
                            <dd><a id="m132" class="menuTag" href="javascript:;" url="prenatalProgressSummaryStart"><span>产前进度</span></a></dd>
                        </dl>
                    </li>

                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="fa fa-film"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;计件管理</span></a>
                        <dl class="layui-nav-child">
                            <#--<dd><a id="m60" class="menuTag" href="javascript:;" url="dispatchStart"><span>工序分配</span></a></dd>-->
                            <#--<dd><a id="m61" class="menuTag" href="javascript:;" url="pieceWorkStatisticStart"><span>产能统计</span></a></dd>-->
                            <dd><a id="m62" class="menuTag" href="javascript:;" url="pieceWorkManageStart"><span>计件管理</span></a></dd>
                            <dd><a id="m63" class="menuTag" href="javascript:;" url="manualInputStart"><span>手工入数</span></a></dd>
                            <dd><a id="m64" class="menuTag" href="javascript:;" url="hourEmpStart"><span>时工入数</span></a></dd>
                            <dd><a id="m116" class="menuTag" href="javascript:;" url="pieceWorkLeakStart"><span>计件扎号详情</span></a></dd>
                            <#--<dd><a id="m65" class="menuTag" href="javascript:;" url="wrongStart"><span>疵点代码</span></a></dd>-->
                            <#--<dd><a id="m66" class="menuTag" href="javascript:;" url="inspectionStart"><span>抽查疵点统计</span></a></dd>-->
                            <#--<dd><a id="m67" class="menuTag" href="javascript:;" url="sampleInspectionStart"><span>QC疵点统计</span></a>-->
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-slider"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;财务数据</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m68" class="menuTag" href="javascript:;" url="empSalaryStart"><span>员工个人产能</span></a></dd>
                            <dd><a id="m69" class="menuTag" href="javascript:;" url="groupSalaryStart"><span>员工分组产能</span></a></dd>
                            <#--<dd><a id="m70" class="menuTag" href="javascript:;" url="cutDailyDetailStart"><span>裁床每日明细</span></a></dd>-->
                            <dd><a id="m109" class="menuTag" href="javascript:;" url="groupProgressStart"><span>区间产能汇总</span></a></dd>
                            <dd><a id="m104" class="menuTag" href="javascript:;" url="pieceWorkDetailStart"><span>产能详情</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-carousel"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;生产报表数据</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m72" class="menuTag" href="javascript:;" url="procedureBalanceStart"><span>订单工序平衡</span></a>
                            </dd>
                            <#--<dd><a id="m73" class="menuTag" href="javascript:;" url="completionStart"><span>完工结算</span></a></dd>-->
                            <dd><a id="m74" class="menuTag" href="javascript:;" url="reWorkStart"><span>返工率</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-engine"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;行政管理</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m99" class="menuTag" href="javascript:;" url="staffStart"><span>员工管理</span></a></dd>
                            <dd><a id="m100" class="menuTag" href="javascript:;" url="roomStart"><span>宿舍管理</span></a></dd>
                            <dd><a id="m101" class="menuTag" href="javascript:;" url="empRoomStart"><span>入住详情</span></a></dd>
                            <dd><a id="m102" class="menuTag" href="javascript:;" url="accommodationStart"><span>住宿管理</span></a></dd>
                            <dd><a id="m78" class="menuTag" href="javascript:;" url="userStart"><span>用户信息</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-rate"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;后整打菲</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m82" class="menuTag" href="javascript:;" url="finishTailorStart"><span>票菲生成</span></a></dd>
                            <dd><a id="m85" class="menuTag" href="javascript:;" url="finishTailorPrintStart"><span>票菲打印</span></a></dd>
                            <dd><a id="m83" class="menuTag" href="javascript:;" url="finishTailorDeleteStart"><span>菲票管理</span></a></dd>
                            <dd><a id="m84" class="menuTag" href="javascript:;" url="finishTailorLeakStart"><span>后整打菲报表</span></a></dd>
                        </dl>
                    </li>
                <#elseif role! == 'role6'> <#--裁片超市-->
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-read"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;裁床报表</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m13" class="menuTag" href="javascript:;" url="cutReportStart"><span>裁床单</span></a></dd>
                            <dd><a id="m14" class="menuTag" href="javascript:;" url="cutQueryLeakStart"><span>单款报表查询</span></a></dd>
                            <dd><a id="m15" class="menuTag" href="javascript:;" url="cutMonthReportStart"><span>月度报表查询</span></a>
                            </dd>
                            <dd><a id="m110" class="menuTag" href="javascript:;" url="unitConsumptionStart"><span>单耗查询</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-export"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;花片收发管理</span></a>
                        <dl class="layui-nav-child">
                            <#--<dd><a id="m19" class="menuTag" href="javascript:;" url="opaStart"><span>花片出厂</span></a></dd>-->
                            <#--<dd><a id="m20" class="menuTag" href="javascript:;" url="otherOpaStart"><span>士啤出厂</span></a></dd>-->
                            <#--<dd><a id="m21" class="menuTag" href="javascript:;" url="opaBackInputStart"><span>花片回厂</span></a></dd>-->
                            <dd><a id="m22" class="menuTag" href="javascript:;" url="opaReportStart"><span>单款花片查询</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-upload-circle"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;裁片&衣胚查询</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m107" class="menuTag" href="javascript:;" url="allStorageStart"><span>裁片库存</span></a></dd>
                            <dd><a id="m108" class="menuTag" href="javascript:;" url="allEmbStorageStart"><span>衣胚库存</span></a></dd>
                            <dd><a id="m35" class="menuTag" href="javascript:;" url="embOutDetailStart"><span>衣胚阶段出库</span></a></dd>
                            <dd><a id="m36" class="menuTag" href="javascript:;" url="embOutPackageDetailStart"><span>衣胚出库详情</span></a></dd>
                            <dd><a id="m37" class="menuTag" href="javascript:;" url="queryEmbLeakStart"><span>衣胚出库汇总</span></a></dd>
                            <dd><a id="m38" class="menuTag" href="javascript:;" url="cutLocationStart"><span>裁片位置详情</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-website"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span>&nbsp;&nbsp;&nbsp;&nbsp;IE基础资料</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m52" class="menuTag" href="javascript:;" url="orderProcedureStart"><span>订单工序</span></a></dd>
                            </dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-console"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;计划排产</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m112" class="menuTag" href="javascript:;" url="embPlanStart"><span>衣胚计划</span></a></dd>
                            <dd><a id="m132" class="menuTag" href="javascript:;" url="prenatalProgressSummaryStart"><span>产前进度</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="fa fa-film"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;计件管理</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m116" class="menuTag" href="javascript:;" url="pieceWorkLeakStart"><span>计件扎号详情</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-slider"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;财务数据</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m68" class="menuTag" href="javascript:;" url="empSalaryStart"><span>员工个人产能</span></a></dd>
                            <dd><a id="m69" class="menuTag" href="javascript:;" url="groupSalaryStart"><span>员工分组产能</span></a></dd>
                            <#--<dd><a id="m70" class="menuTag" href="javascript:;" url="cutDailyDetailStart"><span>裁床每日明细</span></a></dd>-->
                            <dd><a id="m109" class="menuTag" href="javascript:;" url="groupProgressStart"><span>区间产能汇总</span></a></dd>
                            <dd><a id="m104" class="menuTag" href="javascript:;" url="pieceWorkDetailStart"><span>产能详情</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-carousel"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;生产报表数据</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m72" class="menuTag" href="javascript:;" url="procedureBalanceStart"><span>订单工序平衡</span></a>
                            </dd>
                            <#--<dd><a id="m73" class="menuTag" href="javascript:;" url="completionStart"><span>完工结算</span></a></dd>-->
                            <dd><a id="m74" class="menuTag" href="javascript:;" url="reWorkStart"><span>返工率</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-reply-fill"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;其他计件</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m81" class="menuTag" href="javascript:;" url="otherPieceWorkStart"><span>其他计件</span></a></dd>
                        </dl>
                    </li>
                <#elseif role! == 'role7'> <#--吊挂查数-->
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-form"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;订单模块</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m114" class="menuTag" href="javascript:;" url="processOrderStart"><span>制单信息</span></a></dd>
                            <dd><a id="m125" class="menuTag" href="javascript:;" url="weekReportStart"><span>一周看板</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-read"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;裁床报表</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m13" class="menuTag" href="javascript:;" url="cutReportStart"><span>裁床单</span></a></dd>
                            <dd><a id="m14" class="menuTag" href="javascript:;" url="cutQueryLeakStart"><span>单款报表查询</span></a></dd>
                            <dd><a id="m15" class="menuTag" href="javascript:;" url="cutMonthReportStart"><span>月度报表查询</span></a>
                            </dd>
                            <dd><a id="m110" class="menuTag" href="javascript:;" url="unitConsumptionStart"><span>单耗查询</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-upload-circle"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;裁片&衣胚查询</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m107" class="menuTag" href="javascript:;" url="allStorageStart"><span>裁片库存</span></a></dd>
                            <dd><a id="m108" class="menuTag" href="javascript:;" url="allEmbStorageStart"><span>衣胚库存</span></a></dd>
                            <dd><a id="m35" class="menuTag" href="javascript:;" url="embOutDetailStart"><span>衣胚阶段出库</span></a></dd>
                            <dd><a id="m36" class="menuTag" href="javascript:;" url="embOutPackageDetailStart"><span>衣胚出库详情</span></a></dd>
                            <dd><a id="m37" class="menuTag" href="javascript:;" url="queryEmbLeakStart"><span>衣胚出库汇总</span></a></dd>
                            <dd><a id="m38" class="menuTag" href="javascript:;" url="cutLocationStart"><span>裁片位置详情</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-export"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;花片收发管理</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m22" class="menuTag" href="javascript:;" url="opaReportStart"><span>单款花片查询</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="fa fa-film"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;计件管理</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m116" class="menuTag" href="javascript:;" url="pieceWorkLeakStart"><span>计件扎号详情</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-carousel"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;生产报表数据</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m72" class="menuTag" href="javascript:;" url="procedureBalanceStart"><span>订单工序平衡</span></a></dd>
                            <dd><a id="m74" class="menuTag" href="javascript:;" url="reWorkStart"><span>返工率</span></a></dd>
                        </dl>
                    </li>

                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-console"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;计划排产</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m132" class="menuTag" href="javascript:;" url="prenatalProgressSummaryStart"><span>产前进度</span></a></dd>
                        </dl>
                    </li>

                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-slider"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;财务数据</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m68" class="menuTag" href="javascript:;" url="empSalaryStart"><span>员工个人产能</span></a></dd>
                        </dl>
                    </li>
                <#elseif role! == 'role8'> <#--车间文员入数-->
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-form"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;订单模块</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m114" class="menuTag" href="javascript:;" url="processOrderStart"><span>制单信息</span></a></dd>
                            <dd><a id="m125" class="menuTag" href="javascript:;" url="weekReportStart"><span>一周看板</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-read"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;裁床报表</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m13" class="menuTag" href="javascript:;" url="cutReportStart"><span>裁床单</span></a></dd>
                            <dd><a id="m14" class="menuTag" href="javascript:;" url="cutQueryLeakStart"><span>单款报表查询</span></a></dd>
                            <dd><a id="m15" class="menuTag" href="javascript:;" url="cutMonthReportStart"><span>月度报表查询</span></a>
                            </dd>
                            <dd><a id="m110" class="menuTag" href="javascript:;" url="unitConsumptionStart"><span>单耗查询</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-upload-circle"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;裁片&衣胚查询</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m107" class="menuTag" href="javascript:;" url="allStorageStart"><span>裁片库存</span></a></dd>
                            <dd><a id="m108" class="menuTag" href="javascript:;" url="allEmbStorageStart"><span>衣胚库存</span></a></dd>
                            <dd><a id="m35" class="menuTag" href="javascript:;" url="embOutDetailStart"><span>衣胚阶段出库</span></a></dd>
                            <dd><a id="m36" class="menuTag" href="javascript:;" url="embOutPackageDetailStart"><span>衣胚出库详情</span></a></dd>
                            <dd><a id="m37" class="menuTag" href="javascript:;" url="queryEmbLeakStart"><span>衣胚出库汇总</span></a></dd>
                            <dd><a id="m38" class="menuTag" href="javascript:;" url="cutLocationStart"><span>裁片位置详情</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-website"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;IE基础资料</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m52" class="menuTag" href="javascript:;" url="orderProcedureStart"><span>订单工序</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="fa fa-film"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;计件管理</span></a>
                        <dl class="layui-nav-child">
                            <#--<dd><a id="m61" class="menuTag" href="javascript:;" url="pieceWorkStatisticStart"><span>产能统计</span></a></dd>-->
                            <dd><a id="m63" class="menuTag" href="javascript:;" url="manualInputStart"><span>手工入数</span></a></dd>
                            <dd><a id="m64" class="menuTag" href="javascript:;" url="hourEmpStart"><span>时工入数</span></a></dd>
                            </dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-slider"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;财务数据</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m68" class="menuTag" href="javascript:;" url="empSalaryStart"><span>员工个人产能</span></a></dd>
                            <dd><a id="m69" class="menuTag" href="javascript:;" url="groupSalaryStart"><span>员工分组产能</span></a></dd>
                            <#--<dd><a id="m70" class="menuTag" href="javascript:;" url="cutDailyDetailStart"><span>裁床每日明细</span></a></dd>-->
                            <dd><a id="m109" class="menuTag" href="javascript:;" url="groupProgressStart"><span>区间产能汇总</span></a></dd>
                            <dd><a id="m104" class="menuTag" href="javascript:;" url="pieceWorkDetailStart"><span>产能详情</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-carousel"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;生产报表数据</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m72" class="menuTag" href="javascript:;" url="procedureBalanceStart"><span>订单工序平衡</span></a>
                            </dd>
                            <#--<dd><a id="m73" class="menuTag" href="javascript:;" url="completionStart"><span>完工结算</span></a></dd>-->
                            <dd><a id="m74" class="menuTag" href="javascript:;" url="reWorkStart"><span>返工率</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="fa fa-film"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;计件管理</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m116" class="menuTag" href="javascript:;" url="pieceWorkLeakStart"><span>计件扎号详情</span></a></dd>
                        </dl>
                    </li>
                <#elseif role! == 'role9'> <#--分厂-->
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-form"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;订单模块</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m114" class="menuTag" href="javascript:;" url="processOrderStart"><span>制单信息</span></a></dd>
                            <dd><a id="m125" class="menuTag" href="javascript:;" url="weekReportStart"><span>一周看板</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-read"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;裁床报表</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m13" class="menuTag" href="javascript:;" url="cutReportStart"><span>裁床单</span></a></dd>
                            <dd><a id="m14" class="menuTag" href="javascript:;" url="cutQueryLeakStart"><span>单款报表查询</span></a></dd>
                            <dd><a id="m15" class="menuTag" href="javascript:;" url="cutMonthReportStart"><span>月度报表查询</span></a>
                            </dd>
                            <dd><a id="m110" class="menuTag" href="javascript:;" url="unitConsumptionStart"><span>单耗查询</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-export"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;花片收发管理</span></a>
                        <dl class="layui-nav-child">
                            <#--<dd><a id="m19" class="menuTag" href="javascript:;" url="opaStart"><span>花片出厂</span></a></dd>-->
                            <#--<dd><a id="m20" class="menuTag" href="javascript:;" url="otherOpaStart"><span>士啤出厂</span></a></dd>-->
                            <#--<dd><a id="m21" class="menuTag" href="javascript:;" url="opaBackInputStart"><span>花片回厂</span></a></dd>-->
                            <dd><a id="m22" class="menuTag" href="javascript:;" url="opaReportStart"><span>单款花片查询</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-upload-circle"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;裁片&衣胚查询</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m107" class="menuTag" href="javascript:;" url="allStorageStart"><span>裁片库存</span></a></dd>
                            <dd><a id="m108" class="menuTag" href="javascript:;" url="allEmbStorageStart"><span>衣胚库存</span></a></dd>
                            <dd><a id="m35" class="menuTag" href="javascript:;" url="embOutDetailStart"><span>衣胚阶段出库</span></a></dd>
                            <dd><a id="m36" class="menuTag" href="javascript:;" url="embOutPackageDetailStart"><span>衣胚出库详情</span></a></dd>
                            <dd><a id="m37" class="menuTag" href="javascript:;" url="queryEmbLeakStart"><span>衣胚出库汇总</span></a></dd>
                            <dd><a id="m38" class="menuTag" href="javascript:;" url="cutLocationStart"><span>裁片位置详情</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-website"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;IE基础资料</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m52" class="menuTag" href="javascript:;" url="orderProcedureStart"><span>订单工序</span></a></dd>
                            </dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="fa fa-film"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;计件管理</span></a>
                        <dl class="layui-nav-child">
                            <#--<dd><a id="m60" class="menuTag" href="javascript:;" url="dispatchStart"><span>工序分配</span></a></dd>-->
                            <#--<dd><a id="m61" class="menuTag" href="javascript:;" url="pieceWorkStatisticStart"><span>产能统计</span></a></dd>-->
                            <dd><a id="m62" class="menuTag" href="javascript:;" url="pieceWorkManageStart"><span>计件管理</span></a></dd>
                            <dd><a id="m63" class="menuTag" href="javascript:;" url="manualInputStart"><span>手工入数</span></a></dd>
                            <dd><a id="m64" class="menuTag" href="javascript:;" url="hourEmpStart"><span>时工入数</span></a></dd>
                            <#--<dd><a id="m65" class="menuTag" href="javascript:;" url="wrongStart"><span>疵点代码</span></a></dd>-->
                            <#--<dd><a id="m66" class="menuTag" href="javascript:;" url="inspectionStart"><span>抽查疵点统计</span></a></dd>-->
                            <#--<dd><a id="m67" class="menuTag" href="javascript:;" url="sampleInspectionStart"><span>QC疵点统计</span></a>-->
                            </dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-slider"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;财务数据</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m68" class="menuTag" href="javascript:;" url="empSalaryStart"><span>员工个人产能</span></a></dd>
                            <dd><a id="m69" class="menuTag" href="javascript:;" url="groupSalaryStart"><span>员工分组产能</span></a></dd>
                            <#--<dd><a id="m70" class="menuTag" href="javascript:;" url="cutDailyDetailStart"><span>裁床每日明细</span></a></dd>-->
                            <dd><a id="m109" class="menuTag" href="javascript:;" url="groupProgressStart"><span>区间产能汇总</span></a></dd>
                            <dd><a id="m104" class="menuTag" href="javascript:;" url="pieceWorkDetailStart"><span>产能详情</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-carousel"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;生产报表数据</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m72" class="menuTag" href="javascript:;" url="procedureBalanceStart"><span>订单工序平衡</span></a>
                            </dd>
                            <#--<dd><a id="m73" class="menuTag" href="javascript:;" url="completionStart"><span>完工结算</span></a></dd>-->
                            <dd><a id="m74" class="menuTag" href="javascript:;" url="reWorkStart"><span>返工率</span></a></dd>
                        </dl>
                    </li>
                <#elseif role! == 'role10'> <#--财务-->
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-form"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;订单模块</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m114" class="menuTag" href="javascript:;" url="processOrderStart"><span>制单信息</span></a></dd>
                            <dd><a id="m125" class="menuTag" href="javascript:;" url="weekReportStart"><span>一周看板</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-read"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;裁床报表</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m13" class="menuTag" href="javascript:;" url="cutReportStart"><span>裁床单</span></a></dd>
                            <dd><a id="m14" class="menuTag" href="javascript:;" url="cutQueryLeakStart"><span>单款报表查询</span></a></dd>
                            <dd><a id="m15" class="menuTag" href="javascript:;" url="cutMonthReportStart"><span>月度报表查询</span></a>
                            </dd>
                            <dd><a id="m110" class="menuTag" href="javascript:;" url="unitConsumptionStart"><span>单耗查询</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-export"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;花片收发管理</span></a>
                        <dl class="layui-nav-child">
                            <#--<dd><a id="m19" class="menuTag" href="javascript:;" url="opaStart"><span>花片出厂</span></a></dd>-->
                            <#--<dd><a id="m20" class="menuTag" href="javascript:;" url="otherOpaStart"><span>士啤出厂</span></a></dd>-->
                            <#--<dd><a id="m21" class="menuTag" href="javascript:;" url="opaBackInputStart"><span>花片回厂</span></a></dd>-->
                            <dd><a id="m22" class="menuTag" href="javascript:;" url="opaReportStart"><span>单款花片查询</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-upload-circle"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;裁片&衣胚查询</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m107" class="menuTag" href="javascript:;" url="allStorageStart"><span>裁片库存</span></a></dd>
                            <dd><a id="m108" class="menuTag" href="javascript:;" url="allEmbStorageStart"><span>衣胚库存</span></a></dd>
                            <dd><a id="m35" class="menuTag" href="javascript:;" url="embOutDetailStart"><span>衣胚阶段出库</span></a></dd>
                            <dd><a id="m36" class="menuTag" href="javascript:;" url="embOutPackageDetailStart"><span>衣胚出库详情</span></a></dd>
                            <dd><a id="m37" class="menuTag" href="javascript:;" url="queryEmbLeakStart"><span>衣胚出库汇总</span></a></dd>
                            <dd><a id="m38" class="menuTag" href="javascript:;" url="cutLocationStart"><span>裁片位置详情</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-tree"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;面辅料管理</span></a>
                        <dl class="layui-nav-child">
                            <#--<dd><a id="m115" class="menuTag" href="javascript:;" url="fabricAccessoryStart"><span>面辅料采购</span></a></dd>-->
                            <dd><a id="m136" class="menuTag" href="javascript:;" url="fabricAccessoryCheckStart"><span>一站式下单</span></a></dd>
                            <dd><a id="m117" class="menuTag" href="javascript:;" url="fabricReturnStart"><span>面料管理</span></a></dd>
                            <dd><a id="m118" class="menuTag" href="javascript:;" url="accessoryInStoreStart"><span>辅料管理</span></a></dd>
                            <#--<dd><a id="m120" class="menuTag" href="javascript:;" url="placeOrderStart"><span>面辅料审核</span></a></dd>-->
                            <#--<dd><a id="m133" class="menuTag" href="javascript:;" url="orderCheckStart"><span>面辅料合同</span></a></dd>-->
                            <dd><a id="m42" class="menuTag" href="javascript:;" url="looseFabricStart"><span>松布打印</span></a></dd>
                            <dd><a id="m43" class="menuTag" href="javascript:;" url="looseFabricPrintStart"><span>松布补打</span></a></dd>
                            <dd><a id="m111" class="menuTag" href="javascript:;" url="fabricLoChangeStart"><span>LO色修改</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-camera"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;面辅料报表</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m46" class="menuTag" href="javascript:;" url="fabricQueryStart"><span>面料库存</span></a></dd>
                            <dd><a id="m47" class="menuTag" href="javascript:;" url="fabricOutRecordStart"><span>面料出库详情</span></a></dd>
                            <dd><a id="m48" class="menuTag" href="javascript:;" url="fabricLeakStart"><span>单款面辅料详情</span></a></dd>
                            <dd><a id="m49" class="menuTag" href="javascript:;" url="looseFabricSearchStart"><span>松布查询</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-website"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;IE基础资料</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m51" class="menuTag" href="javascript:;" url="procedureTemplateStart"><span>工序数据库</span></a>
                            </dd>
                            <dd><a id="m52" class="menuTag" href="javascript:;" url="orderProcedureStart"><span>订单工序</span></a></dd>
                            <dd><a id="m53" class="menuTag" href="javascript:;" url="procedureProgressStart"><span>工序进度</span></a>
                            </dd>
                            <dd><a id="m54" class="menuTag" href="javascript:;" url="procedureLevelStart"><span>工序等级</span></a></dd>
                            <dd><a id="m200" class="menuTag" href="javascript:;" url="beatStart"><span>节拍查询</span></a></dd>
                            <dd><a id="m56" class="menuTag" href="javascript:;" url="printPartStart"><span>票菲部位管理</span></a></dd>
                            <dd><a id="m57" class="menuTag" href="javascript:;" url="groupPlanStart"><span>计划产能</span></a></dd>
                            <dd><a id="m58" class="menuTag" href="javascript:;" url="completionRateStart"><span>达成率</span></a></dd>
                            <dd><a id="m90" class="menuTag" href="javascript:;" url="specialProcedureStart"><span>特殊工序</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-console"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;计划排产</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m112" class="menuTag" href="javascript:;" url="embPlanStart"><span>衣胚计划</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="fa fa-film"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;计件管理</span></a>
                        <dl class="layui-nav-child">
                            <#--<dd><a id="m60" class="menuTag" href="javascript:;" url="dispatchStart"><span>工序分配</span></a></dd>-->
                            <#--<dd><a id="m61" class="menuTag" href="javascript:;" url="pieceWorkStatisticStart"><span>产能统计</span></a></dd>-->
                            <dd><a id="m62" class="menuTag" href="javascript:;" url="pieceWorkManageStart"><span>计件管理</span></a></dd>
                            <dd><a id="m63" class="menuTag" href="javascript:;" url="manualInputStart"><span>手工入数</span></a></dd>
                            <dd><a id="m64" class="menuTag" href="javascript:;" url="hourEmpStart"><span>时工入数</span></a></dd>
                            <#--<dd><a id="m65" class="menuTag" href="javascript:;" url="wrongStart"><span>疵点代码</span></a></dd>-->
                            <#--<dd><a id="m66" class="menuTag" href="javascript:;" url="inspectionStart"><span>抽查疵点统计</span></a></dd>-->
                            <#--<dd><a id="m67" class="menuTag" href="javascript:;" url="sampleInspectionStart"><span>QC疵点统计</span></a>-->
                            </dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-slider"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;财务数据</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m68" class="menuTag" href="javascript:;" url="empSalaryStart"><span>员工个人产能</span></a></dd>
                            <dd><a id="m69" class="menuTag" href="javascript:;" url="groupSalaryStart"><span>员工分组产能</span></a></dd>
                            <#--<dd><a id="m70" class="menuTag" href="javascript:;" url="cutDailyDetailStart"><span>裁床每日明细</span></a></dd>-->
                            <dd><a id="m109" class="menuTag" href="javascript:;" url="groupProgressStart"><span>区间产能汇总</span></a></dd>
                            <dd><a id="m104" class="menuTag" href="javascript:;" url="pieceWorkDetailStart"><span>产能详情</span></a></dd>
                            <dd><a id="m131" class="menuTag" href="javascript:;" url="costAccountStart"><span>成本核算</span></a></dd>
                            <dd><a id="m105" class="menuTag" href="javascript:;" url="fixedSalaryStart"><span>锁定工资</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-carousel"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;生产报表数据</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m72" class="menuTag" href="javascript:;" url="procedureBalanceStart"><span>订单工序平衡</span></a>
                            </dd>
                            <#--<dd><a id="m73" class="menuTag" href="javascript:;" url="completionStart"><span>完工结算</span></a></dd>-->
                            <dd><a id="m74" class="menuTag" href="javascript:;" url="reWorkStart"><span>返工率</span></a></dd>
                        </dl>
                    </li>
                <#elseif role! == 'role11'> <#--面料管理-->
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-form"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;订单模块</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m114" class="menuTag" href="javascript:;" url="processOrderStart"><span>制单信息</span></a></dd>
                            <dd><a id="m125" class="menuTag" href="javascript:;" url="weekReportStart"><span>一周看板</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-read"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;裁床报表</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m13" class="menuTag" href="javascript:;" url="cutReportStart"><span>裁床单</span></a></dd>
                            <dd><a id="m14" class="menuTag" href="javascript:;" url="cutQueryLeakStart"><span>单款报表查询</span></a></dd>
                            <dd><a id="m15" class="menuTag" href="javascript:;" url="cutMonthReportStart"><span>月度报表查询</span></a>
                            </dd>
                            <dd><a id="m110" class="menuTag" href="javascript:;" url="unitConsumptionStart"><span>单耗查询</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-upload-circle"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;裁片&衣胚查询</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m107" class="menuTag" href="javascript:;" url="allStorageStart"><span>裁片库存</span></a></dd>
                            <dd><a id="m108" class="menuTag" href="javascript:;" url="allEmbStorageStart"><span>衣胚库存</span></a></dd>
                            <dd><a id="m35" class="menuTag" href="javascript:;" url="embOutDetailStart"><span>衣胚阶段出库</span></a></dd>
                            <dd><a id="m36" class="menuTag" href="javascript:;" url="embOutPackageDetailStart"><span>衣胚出库详情</span></a></dd>
                            <dd><a id="m37" class="menuTag" href="javascript:;" url="queryEmbLeakStart"><span>衣胚出库汇总</span></a></dd>
                            <dd><a id="m38" class="menuTag" href="javascript:;" url="cutLocationStart"><span>裁片位置详情</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-tree"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;面辅料管理</span></a>
                        <dl class="layui-nav-child">
                            <#--<dd><a id="m115" class="menuTag" href="javascript:;" url="fabricAccessoryStart"><span>面辅料采购</span></a></dd>-->
                            <dd><a id="m136" class="menuTag" href="javascript:;" url="fabricAccessoryCheckStart"><span>一站式下单</span></a></dd>
                            <dd><a id="m117" class="menuTag" href="javascript:;" url="fabricReturnStart"><span>面料管理</span></a></dd>
                            <dd><a id="m118" class="menuTag" href="javascript:;" url="accessoryInStoreStart"><span>辅料管理</span></a></dd>
                            <#--<dd><a id="m120" class="menuTag" href="javascript:;" url="placeOrderStart"><span>面辅料审核</span></a></dd>-->
                            <#--<dd><a id="m133" class="menuTag" href="javascript:;" url="orderCheckStart"><span>面辅料合同</span></a></dd>-->
                            <dd><a id="m42" class="menuTag" href="javascript:;" url="looseFabricStart"><span>松布打印</span></a></dd>
                            <dd><a id="m43" class="menuTag" href="javascript:;" url="looseFabricPrintStart"><span>松布补打</span></a></dd>
                            <dd><a id="m111" class="menuTag" href="javascript:;" url="fabricLoChangeStart"><span>LO色修改</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-camera"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;面辅料报表</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m46" class="menuTag" href="javascript:;" url="fabricQueryStart"><span>面料库存</span></a></dd>
                            <dd><a id="m119" class="menuTag" href="javascript:;" url="accessoryStorageStart"><span>辅料库存</span></a></dd>
                            <dd><a id="m47" class="menuTag" href="javascript:;" url="fabricOutRecordStart"><span>面料出库详情</span></a></dd>
                            <dd><a id="m48" class="menuTag" href="javascript:;" url="fabricLeakStart"><span>单款面辅料详情</span></a></dd>
                            <dd><a id="m49" class="menuTag" href="javascript:;" url="looseFabricSearchStart"><span>松布查询</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-console"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;计划排产</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m112" class="menuTag" href="javascript:;" url="embPlanStart"><span>衣胚计划</span></a></dd>
                        </dl>
                    </li>
                <#elseif role! == 'role12'> <#--管理层-->
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-form"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;订单模块</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m114" class="menuTag" href="javascript:;" url="processOrderStart"><span>制单信息</span></a></dd>
                            <dd><a id="m125" class="menuTag" href="javascript:;" url="weekReportStart"><span>一周看板</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-read"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;裁床报表</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m13" class="menuTag" href="javascript:;" url="cutReportStart"><span>裁床单</span></a></dd>
                            <dd><a id="m14" class="menuTag" href="javascript:;" url="cutQueryLeakStart"><span>单款报表查询</span></a></dd>
                            <dd><a id="m15" class="menuTag" href="javascript:;" url="cutMonthReportStart"><span>月度报表查询</span></a>
                            </dd>
                            <dd><a id="m110" class="menuTag" href="javascript:;" url="unitConsumptionStart"><span>单耗查询</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-export"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;花片收发管理</span></a>
                        <dl class="layui-nav-child">
                            <#--<dd><a id="m19" class="menuTag" href="javascript:;" url="opaStart"><span>花片出厂</span></a></dd>-->
                            <#--<dd><a id="m20" class="menuTag" href="javascript:;" url="otherOpaStart"><span>士啤出厂</span></a></dd>-->
                            <#--<dd><a id="m21" class="menuTag" href="javascript:;" url="opaBackInputStart"><span>花片回厂</span></a></dd>-->
                            <dd><a id="m22" class="menuTag" href="javascript:;" url="opaReportStart"><span>单款花片查询</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-upload-circle"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;裁片&衣胚查询</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m107" class="menuTag" href="javascript:;" url="allStorageStart"><span>裁片库存</span></a></dd>
                            <dd><a id="m108" class="menuTag" href="javascript:;" url="allEmbStorageStart"><span>衣胚库存</span></a></dd>
                            <dd><a id="m35" class="menuTag" href="javascript:;" url="embOutDetailStart"><span>衣胚阶段出库</span></a></dd>
                            <dd><a id="m36" class="menuTag" href="javascript:;" url="embOutPackageDetailStart"><span>衣胚出库详情</span></a></dd>
                            <dd><a id="m37" class="menuTag" href="javascript:;" url="queryEmbLeakStart"><span>衣胚出库汇总</span></a></dd>
                            <dd><a id="m38" class="menuTag" href="javascript:;" url="cutLocationStart"><span>裁片位置详情</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-tree"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;面辅料管理</span></a>
                        <dl class="layui-nav-child">
                            <#--<dd><a id="m115" class="menuTag" href="javascript:;" url="fabricAccessoryStart"><span>面辅料采购</span></a></dd>-->
                            <dd><a id="m136" class="menuTag" href="javascript:;" url="fabricAccessoryCheckStart"><span>一站式下单</span></a></dd>
                            <dd><a id="m117" class="menuTag" href="javascript:;" url="fabricReturnStart"><span>面料管理</span></a></dd>
                            <dd><a id="m118" class="menuTag" href="javascript:;" url="accessoryInStoreStart"><span>辅料管理</span></a></dd>
                            <#--<dd><a id="m120" class="menuTag" href="javascript:;" url="placeOrderStart"><span>面辅料审核</span></a></dd>-->
                            <#--<dd><a id="m133" class="menuTag" href="javascript:;" url="orderCheckStart"><span>面辅料合同</span></a></dd>-->
                            <dd><a id="m42" class="menuTag" href="javascript:;" url="looseFabricStart"><span>松布打印</span></a></dd>
                            <dd><a id="m43" class="menuTag" href="javascript:;" url="looseFabricPrintStart"><span>松布补打</span></a></dd>
                            <dd><a id="m111" class="menuTag" href="javascript:;" url="fabricLoChangeStart"><span>LO色修改</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-camera"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;面辅料报表</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m46" class="menuTag" href="javascript:;" url="fabricQueryStart"><span>面料库存</span></a></dd>
                            <dd><a id="m47" class="menuTag" href="javascript:;" url="fabricOutRecordStart"><span>面料出库详情</span></a></dd>
                            <dd><a id="m48" class="menuTag" href="javascript:;" url="fabricLeakStart"><span>单款面辅料详情</span></a></dd>
                            <dd><a id="m49" class="menuTag" href="javascript:;" url="looseFabricSearchStart"><span>松布查询</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-website"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;IE基础资料</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m52" class="menuTag" href="javascript:;" url="orderProcedureStart"><span>订单工序</span></a></dd>
                            <dd><a id="m53" class="menuTag" href="javascript:;" url="procedureProgressStart"><span>工序进度</span></a>
                            </dd>
                            <dd><a id="m54" class="menuTag" href="javascript:;" url="procedureLevelStart"><span>工序等级</span></a></dd>
                            <dd><a id="m200" class="menuTag" href="javascript:;" url="beatStart"><span>节拍查询</span></a></dd>
                            <dd><a id="m56" class="menuTag" href="javascript:;" url="printPartStart"><span>票菲部位管理</span></a></dd>
                            <dd><a id="m57" class="menuTag" href="javascript:;" url="groupPlanStart"><span>计划产能</span></a></dd>
                            <dd><a id="m58" class="menuTag" href="javascript:;" url="completionRateStart"><span>达成率</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-console"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;计划排产</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m112" class="menuTag" href="javascript:;" url="embPlanStart"><span>衣胚计划</span></a></dd>
                            <dd><a id="m132" class="menuTag" href="javascript:;" url="prenatalProgressSummaryStart"><span>产前进度</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-slider"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;财务数据</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m68" class="menuTag" href="javascript:;" url="empSalaryStart"><span>员工个人产能</span></a></dd>
                            <dd><a id="m69" class="menuTag" href="javascript:;" url="groupSalaryStart"><span>员工分组产能</span></a></dd>
                            <#--<dd><a id="m70" class="menuTag" href="javascript:;" url="cutDailyDetailStart"><span>裁床每日明细</span></a></dd>-->
                            <dd><a id="m109" class="menuTag" href="javascript:;" url="groupProgressStart"><span>区间产能汇总</span></a></dd>
                            <dd><a id="m104" class="menuTag" href="javascript:;" url="pieceWorkDetailStart"><span>产能详情</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-carousel"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;生产报表数据</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m72" class="menuTag" href="javascript:;" url="procedureBalanceStart"><span>订单工序平衡</span></a>
                            </dd>
                            <#--<dd><a id="m73" class="menuTag" href="javascript:;" url="completionStart"><span>完工结算</span></a></dd>-->
                            <dd><a id="m74" class="menuTag" href="javascript:;" url="reWorkStart"><span>返工率</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-engine"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;行政管理</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m99" class="menuTag" href="javascript:;" url="staffStart"><span>员工管理</span></a></dd>
                            <dd><a id="m100" class="menuTag" href="javascript:;" url="roomStart"><span>宿舍管理</span></a></dd>
                            <dd><a id="m101" class="menuTag" href="javascript:;" url="empRoomStart"><span>入住详情</span></a></dd>
                            <dd><a id="m102" class="menuTag" href="javascript:;" url="accommodationStart"><span>住宿管理</span></a></dd>
                            <dd><a id="m78" class="menuTag" href="javascript:;" url="userStart"><span>用户信息</span></a></dd>
                        </dl>
                    </li>
                <#elseif role! == 'role13'>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-reply-fill"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;其他计件</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m81" class="menuTag" href="javascript:;" url="otherPieceWorkStart"><span>其他计件</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-slider"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;财务数据</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m68" class="menuTag" href="javascript:;" url="empSalaryStart"><span>员工个人产能</span></a></dd>
                        </dl>
                    </li>
                <#elseif role! == 'role14'>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-form"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;</span><span>订单模块</span></a>
                        <dl class="layui-nav-child">
                            <#--<dd><a id="m1" class="menuTag" href="javascript:;" url="orderStart"><span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><i class="layui-icon layui-icon-file"></i><span>&nbsp;&nbsp;订单信息</span></a></dd>-->
                            <#--<dd><a id="m92" class="menuTag" href="javascript:;" url="clothesVersionProcessStart"><span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><i class="layui-icon layui-icon-app"></i><span>&nbsp;&nbsp;版单进度</span></a></dd>-->
                            <dd><a id="m114" class="menuTag" href="javascript:;" url="processOrderStart"><span>制单信息</span></a></dd>
                            <dd><a id="m125" class="menuTag" href="javascript:;" url="weekReportStart"><span>一周看板</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-read"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;裁床报表</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m13" class="menuTag" href="javascript:;" url="cutReportStart"><span>裁床单</span></a></dd>
                            <dd><a id="m14" class="menuTag" href="javascript:;" url="cutQueryLeakStart"><span>单款报表查询</span></a></dd>
                            <dd><a id="m15" class="menuTag" href="javascript:;" url="cutMonthReportStart"><span>月度报表查询</span></a></dd>
                            <dd><a id="m110" class="menuTag" href="javascript:;" url="unitConsumptionStart"><span>单耗查询</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-upload-circle"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;裁片&衣胚查询</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m107" class="menuTag" href="javascript:;" url="allStorageStart"><span>裁片库存</span></a></dd>
                            <dd><a id="m108" class="menuTag" href="javascript:;" url="allEmbStorageStart"><span>衣胚库存</span></a></dd>
                            <dd><a id="m35" class="menuTag" href="javascript:;" url="embOutDetailStart"><span>衣胚阶段出库</span></a></dd>
                            <dd><a id="m36" class="menuTag" href="javascript:;" url="embOutPackageDetailStart"><span>衣胚出库详情</span></a></dd>
                            <dd><a id="m37" class="menuTag" href="javascript:;" url="queryEmbLeakStart"><span>衣胚出库汇总</span></a></dd>
                            <dd><a id="m38" class="menuTag" href="javascript:;" url="cutLocationStart"><span>裁片位置详情</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-tree"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;面辅料管理</span></a>
                        <dl class="layui-nav-child">
                            <#--<dd><a id="m115" class="menuTag" href="javascript:;" url="fabricAccessoryStart"><span>面辅料采购</span></a></dd>-->
                            <dd><a id="m136" class="menuTag" href="javascript:;" url="fabricAccessoryCheckStart"><span>一站式下单</span></a></dd>
                            <dd><a id="m117" class="menuTag" href="javascript:;" url="fabricReturnStart"><span>面料管理</span></a></dd>
                            <dd><a id="m118" class="menuTag" href="javascript:;" url="accessoryInStoreStart"><span>辅料管理</span></a></dd>
                            <#--<dd><a id="m120" class="menuTag" href="javascript:;" url="placeOrderStart"><span>面辅料审核</span></a></dd>-->
                            <#--<dd><a id="m42" class="menuTag" href="javascript:;" url="looseFabricStart"><span>松布打印</span></a></dd>-->
                            <dd><a id="m43" class="menuTag" href="javascript:;" url="looseFabricPrintStart"><span>松布补打</span></a></dd>
                            <dd><a id="m111" class="menuTag" href="javascript:;" url="fabricLoChangeStart"><span>LO色修改</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-camera"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;面辅料报表</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m46" class="menuTag" href="javascript:;" url="fabricQueryStart"><span>面料库存</span></a></dd>
                            <dd><a id="m119" class="menuTag" href="javascript:;" url="accessoryStorageStart"><span>辅料库存</span></a></dd>
                            <dd><a id="m47" class="menuTag" href="javascript:;" url="fabricOutRecordStart"><span>面料出库详情</span></a></dd>
                            <dd><a id="m48" class="menuTag" href="javascript:;" url="fabricLeakStart"><span>单款面辅料详情</span></a></dd>
                            <dd><a id="m49" class="menuTag" href="javascript:;" url="looseFabricSearchStart"><span>松布查询</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-carousel"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;生产报表数据</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m72" class="menuTag" href="javascript:;" url="procedureBalanceStart"><span>订单工序平衡</span></a>
                            </dd>
                            <#--<dd><a id="m73" class="menuTag" href="javascript:;" url="completionStart"><span>完工结算</span></a></dd>-->
                            <dd><a id="m74" class="menuTag" href="javascript:;" url="reWorkStart"><span>返工率</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-rate"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;后整打菲</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m82" class="menuTag" href="javascript:;" url="finishTailorStart"><span>票菲生成</span></a></dd>
                            <dd><a id="m85" class="menuTag" href="javascript:;" url="finishTailorPrintStart"><span>票菲打印</span></a></dd>
                            <dd><a id="m83" class="menuTag" href="javascript:;" url="finishTailorDeleteStart"><span>菲票管理</span></a></dd>
                            <dd><a id="m84" class="menuTag" href="javascript:;" url="finishTailorLeakStart"><span>后整打菲报表</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="fa fa-film"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;计件管理</span></a>
                        <dl class="layui-nav-child">
                            <#--<dd><a id="m60" class="menuTag" href="javascript:;" url="dispatchStart"><span>工序分配</span></a></dd>-->
                            <#--<dd><a id="m61" class="menuTag" href="javascript:;" url="pieceWorkStatisticStart"><span>产能统计</span></a></dd>-->
                            <dd><a id="m116" class="menuTag" href="javascript:;" url="pieceWorkLeakStart"><span>计件扎号详情</span></a></dd>
                            <dd><a id="m62" class="menuTag" href="javascript:;" url="pieceWorkManageStart"><span>计件管理</span></a></dd>
                            <dd><a id="m63" class="menuTag" href="javascript:;" url="manualInputStart"><span>手工入数</span></a></dd>
                            <dd><a id="m64" class="menuTag" href="javascript:;" url="hourEmpStart"><span>时工入数</span></a></dd>
                            <#--<dd><a id="m65" class="menuTag" href="javascript:;" url="wrongStart"><span>疵点代码</span></a></dd>-->
                            <#--<dd><a id="m66" class="menuTag" href="javascript:;" url="inspectionStart"><span>抽查疵点统计</span></a></dd>-->
                            <#--<dd><a id="m67" class="menuTag" href="javascript:;" url="sampleInspectionStart"><span>QC疵点统计</span></a>-->
                        </dl>
                    </li>

                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-console"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;计划排产</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m112" class="menuTag" href="javascript:;" url="embPlanStart"><span>衣胚计划</span></a></dd>
                            <dd><a id="m132" class="menuTag" href="javascript:;" url="prenatalProgressSummaryStart"><span>产前进度</span></a></dd>
                        </dl>
                    </li>

                <#elseif role! == 'role15'> <#--IQC-->
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-form"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;订单模块</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m114" class="menuTag" href="javascript:;" url="processOrderStart"><span>制单信息</span></a></dd>
                            <dd><a id="m125" class="menuTag" href="javascript:;" url="weekReportStart"><span>一周看板</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-read"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;裁床报表</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m13" class="menuTag" href="javascript:;" url="cutReportStart"><span>裁床单</span></a></dd>
                            <dd><a id="m14" class="menuTag" href="javascript:;" url="cutQueryLeakStart"><span>单款报表查询</span></a></dd>
                            <dd><a id="m15" class="menuTag" href="javascript:;" url="cutMonthReportStart"><span>月度报表查询</span></a>
                            </dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-camera"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;面辅料报表</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m46" class="menuTag" href="javascript:;" url="fabricQueryStart"><span>面料库存</span></a></dd>
                            <dd><a id="m47" class="menuTag" href="javascript:;" url="fabricOutRecordStart"><span>面料出库详情</span></a></dd>
                            <dd><a id="m48" class="menuTag" href="javascript:;" url="fabricLeakStart"><span>单款面辅料详情</span></a></dd>
                            <dd><a id="m49" class="menuTag" href="javascript:;" url="looseFabricSearchStart"><span>松布查询</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-carousel"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;生产报表数据</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m72" class="menuTag" href="javascript:;" url="procedureBalanceStart"><span>订单工序平衡</span></a></dd>
                            <#--<dd><a id="m73" class="menuTag" href="javascript:;" url="completionStart"><span>完工结算</span></a></dd>-->
                            <dd><a id="m74" class="menuTag" href="javascript:;" url="reWorkStart"><span>返工率</span></a></dd>
                        </dl>
                    </li>

                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-console"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;计划排产</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m112" class="menuTag" href="javascript:;" url="embPlanStart"><span>衣胚计划</span></a></dd>
                            <dd><a id="m132" class="menuTag" href="javascript:;" url="prenatalProgressSummaryStart"><span>产前进度</span></a></dd>
                        </dl>
                    </li>


                <#elseif role! == 'role16'> <#--板房-->
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-form"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;订单模块</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m114" class="menuTag" href="javascript:;" url="processOrderStart"><span>制单信息</span></a></dd>
                            <dd><a id="m125" class="menuTag" href="javascript:;" url="weekReportStart"><span>一周看板</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-read"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;裁床报表</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m13" class="menuTag" href="javascript:;" url="cutReportStart"><span>裁床单</span></a></dd>
                            <dd><a id="m14" class="menuTag" href="javascript:;" url="cutQueryLeakStart"><span>单款报表查询</span></a></dd>
                            <dd><a id="m15" class="menuTag" href="javascript:;" url="cutMonthReportStart"><span>月度报表查询</span></a>
                            </dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-camera"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;面辅料报表</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m46" class="menuTag" href="javascript:;" url="fabricQueryStart"><span>面料库存</span></a></dd>
                            <dd><a id="m47" class="menuTag" href="javascript:;" url="fabricOutRecordStart"><span>面料出库详情</span></a></dd>
                            <dd><a id="m48" class="menuTag" href="javascript:;" url="fabricLeakStart"><span>单款面辅料详情</span></a></dd>
                            <dd><a id="m49" class="menuTag" href="javascript:;" url="looseFabricSearchStart"><span>松布查询</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-website"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;IE基础资料</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m51" class="menuTag" href="javascript:;" url="procedureTemplateStart"><span>工序数据库</span></a></dd>
                            <dd><a id="m52" class="menuTag" href="javascript:;" url="orderProcedureStart"><span>订单工序</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-carousel"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;生产报表数据</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m72" class="menuTag" href="javascript:;" url="procedureBalanceStart"><span>订单工序平衡</span></a></dd>
                            <#--<dd><a id="m73" class="menuTag" href="javascript:;" url="completionStart"><span>完工结算</span></a></dd>-->
                            <dd><a id="m74" class="menuTag" href="javascript:;" url="reWorkStart"><span>返工率</span></a></dd>
                        </dl>
                    </li>

                <#elseif role! == 'role17'> <#--辅料管理-->
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-form"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;订单模块</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m114" class="menuTag" href="javascript:;" url="processOrderStart"><span>制单信息</span></a></dd>
                            <dd><a id="m125" class="menuTag" href="javascript:;" url="weekReportStart"><span>一周看板</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-read"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;裁床报表</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m13" class="menuTag" href="javascript:;" url="cutReportStart"><span>裁床单</span></a></dd>
                            <dd><a id="m14" class="menuTag" href="javascript:;" url="cutQueryLeakStart"><span>单款报表查询</span></a></dd>
                            <dd><a id="m15" class="menuTag" href="javascript:;" url="cutMonthReportStart"><span>月度报表查询</span></a>
                            </dd>
                            <dd><a id="m110" class="menuTag" href="javascript:;" url="unitConsumptionStart"><span>单耗查询</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-upload-circle"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;裁片&衣胚查询</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m107" class="menuTag" href="javascript:;" url="allStorageStart"><span>裁片库存</span></a></dd>
                            <dd><a id="m108" class="menuTag" href="javascript:;" url="allEmbStorageStart"><span>衣胚库存</span></a></dd>
                            <dd><a id="m35" class="menuTag" href="javascript:;" url="embOutDetailStart"><span>衣胚阶段出库</span></a></dd>
                            <dd><a id="m36" class="menuTag" href="javascript:;" url="embOutPackageDetailStart"><span>衣胚出库详情</span></a></dd>
                            <dd><a id="m37" class="menuTag" href="javascript:;" url="queryEmbLeakStart"><span>衣胚出库汇总</span></a></dd>
                            <dd><a id="m38" class="menuTag" href="javascript:;" url="cutLocationStart"><span>裁片位置详情</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-tree"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;面辅料管理</span></a>
                        <dl class="layui-nav-child">
                            <#--<dd><a id="m115" class="menuTag" href="javascript:;" url="fabricAccessoryStart"><span>面辅料采购</span></a></dd>-->
                            <dd><a id="m136" class="menuTag" href="javascript:;" url="fabricAccessoryCheckStart"><span>一站式下单</span></a></dd>
                            <dd><a id="m117" class="menuTag" href="javascript:;" url="fabricReturnStart"><span>面料管理</span></a></dd>
                            <dd><a id="m118" class="menuTag" href="javascript:;" url="accessoryInStoreStart"><span>辅料管理</span></a></dd>
                            <#--<dd><a id="m120" class="menuTag" href="javascript:;" url="placeOrderStart"><span>面辅料审核</span></a></dd>-->
                            <#--<dd><a id="m133" class="menuTag" href="javascript:;" url="orderCheckStart"><span>面辅料合同</span></a></dd>-->
                            <dd><a id="m42" class="menuTag" href="javascript:;" url="looseFabricStart"><span>松布打印</span></a></dd>
                            <dd><a id="m43" class="menuTag" href="javascript:;" url="looseFabricPrintStart"><span>松布补打</span></a></dd>
                            <dd><a id="m111" class="menuTag" href="javascript:;" url="fabricLoChangeStart"><span>LO色修改</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-camera"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;面辅料报表</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m46" class="menuTag" href="javascript:;" url="fabricQueryStart"><span>面料库存</span></a></dd>
                            <dd><a id="m119" class="menuTag" href="javascript:;" url="accessoryStorageStart"><span>辅料库存</span></a></dd>
                            <dd><a id="m47" class="menuTag" href="javascript:;" url="fabricOutRecordStart"><span>面料出库详情</span></a></dd>
                            <dd><a id="m48" class="menuTag" href="javascript:;" url="fabricLeakStart"><span>单款面辅料详情</span></a></dd>
                            <dd><a id="m49" class="menuTag" href="javascript:;" url="looseFabricSearchStart"><span>松布查询</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item ">
                        <a class="bigMenuTag"><i class="layui-icon layui-icon-console"></i><span>&nbsp;&nbsp;&nbsp;&nbsp;计划排产</span></a>
                        <dl class="layui-nav-child">
                            <dd><a id="m112" class="menuTag" href="javascript:;" url="embPlanStart"><span>衣胚计划</span></a></dd>
                        </dl>
                    </li>
                </#if>
            </ul>
        </div>
    </div>

    <div class="layui-body" style="bottom: 0px;">
        <div style="padding-left: 15px">
            <!-- 内容主体区域 -->
            <div class="layui-tab layui-tab-brief" lay-allowClose="true" lay-filter="tab-switch">
                <ul class="layui-tab-title" lay-allowClose="true">
                    <li class="layui-this " id="first">首页</li>
                </ul>
                <div class="layui-tab-content">
                    <div class="layui-tab-item layui-show">
                        <table id="homepageTable" lay-filter="homepageTable"></table>

                        <script type="text/html" id="toolbarTop">
                            <div class="layui-btn-container">
                                <button class="layui-btn layui-btn-sm" lay-event="refresh">刷新</button>
                            </div>
                        </script>

                        <#--<div class="row">-->
                            <#--<div class="col-md-6">-->
                                <#--<section class="panel panel-default">-->
                                    <#--<div class="panel-body" style="text-align: center;">-->
                                        <#--<div id="container" style="height: 380px;"></div>-->
                                    <#--</div>-->
                                <#--</section>-->
                            <#--</div>-->
                            <#--<div class="col-md-6">-->
                                <#--<section class="panel panel-default">-->
                                    <#--<div class="panel-body" style="text-align: center;">-->
                                        <#--<div id="yesterdayFinish" style="height: 380px;"></div>-->
                                    <#--</div>-->
                                <#--</section>-->
                            <#--</div>-->
                        <#--</div>-->

                        <#--<div class="row">-->
                            <#--<div class="col-md-4">-->
                                <#--<section class="panel panel-default">-->
                                    <#--<div class="panel-body" style="text-align: center;">-->
                                        <#--<div id="yesterdayTailor" style="height: 330px;"></div>-->
                                    <#--</div>-->
                                <#--</section>-->
                            <#--</div>-->
                            <#--<div class="col-md-4">-->
                                <#--<section class="panel panel-default">-->
                                    <#--<div class="panel-body" style="text-align: center;">-->
                                        <#--<div id="yesterdayWorkShop" style="height: 330px;"></div>-->
                                    <#--</div>-->
                                <#--</section>-->
                            <#--</div>-->
                            <#--<div class="col-md-4">-->
                                <#--<section class="panel panel-default">-->
                                    <#--<div class="panel-body" style="text-align: center;">-->
                                        <#--<div class="carousel slide" data-ride="carousel">-->
                                            <#--<!-- 轮播图片 &ndash;&gt;-->
                                            <#--<div class="carousel-inner">-->
                                                <#--<#if orderNameList??>-->
                                                <#--<#list orderNameList as vo>-->
                                                    <#--<#if vo_index == 0>-->
                                                    <#--<div class="carousel-item active">-->
                                                        <#--<div id="yesterdayReWork${vo_index}"-->
                                                             <#--style="width: 364px;height: 330px;"></div>-->
                                                    <#--</div>-->
                                                    <#--<#else>-->
                                                        <#--<div class="carousel-item">-->
                                                            <#--<div id="yesterdayReWork${vo_index}"-->
                                                                 <#--style="width: 364px;height: 330px;"></div>-->
                                                        <#--</div>-->
                                                    <#--</#if>-->
                                                <#--</#list>-->
                                                <#--</#if>-->

                                            <#--</div>-->
                                        <#--</div>-->
                                    <#--</div>-->
                                <#--</section>-->
                            <#--</div>-->
                        <#--</div>-->

                    </div>
                </div>

            </div>
        </div>
    </div>

    <div id="pwdEdit" style="display: none;cursor:default">
        <div style="overflow-x: auto;overflow-y:auto;">
            <div style="text-align: right;margin-bottom:20px">
                <a id="pwdEditNo" href="#" style="font-size:20px"><i class="fa fa-times icon"
                                                                     style="color: rgb(182,182,182)"></i></a>
            </div>
            <table style="border-collapse:separate;border-spacing: 10px;width: 100%;">
                <tr>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">原密码</label>
                    </td>
                    <td>
                        <input id="originPassWord" class="form-control"
                               style="width: 100%;margin-bottom: 15px;border-top: none;border-right: none;border-left: none;"
                               autocomplete="off" type="password" placeholder="请输入原密码">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">新密码</label>
                    </td>
                    <td>
                        <input id="newPassWord" class="form-control"
                               style="margin-bottom: 15px;width: 100%;border-top: none;border-right: none;border-left: none;"
                               autocomplete="off" type="password" placeholder="请输入新密码">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">新密码</label>
                    </td>
                    <td>
                        <input id="secondNewPassWord" class="form-control"
                               style="margin-bottom: 15px;width: 100%;border-top: none;border-right: none;border-left: none;"
                               autocomplete="off" type="password" placeholder='请再次输入新密码'>
                    </td>
                </tr>
            </table>
        </div>
        <div class="col-sm-12" style="padding-top: 10px">
            <button id="pwdEditYes" class="btn btn-s-lg" style="border-radius: 5px;" style="text-align: center;">保存
            </button>
        </div>
    </div>
</div>
<style>

    #first i {
        display: none
    }

    ::-webkit-scrollbar-track-piece {

        background-color:#f8f8f8;

    }

    ::-webkit-scrollbar {

        width:15px;

        height:15px;

    }

    ::-webkit-scrollbar-thumb {

        background-color:#dddddd;

        background-clip:padding-box;

        min-height:28px;

    }

    ::-webkit-scrollbar-thumb:hover {

        background-color:#bbb;

    }

</style>
</body>
<link rel="stylesheet" href="https://cdn.staticfile.org/twitter-bootstrap/4.3.1/css/bootstrap.min.css">
<script src="https://cdn.staticfile.org/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdn.staticfile.org/popper.js/1.15.0/umd/popper.min.js"></script>
<script src="https://cdn.staticfile.org/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="/js/common/jquery.blockUI.js" type="text/javascript"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css" type="text/css">
<script type="text/javascript" src="https://cdn.bootcss.com/echarts/4.2.1-rc1/echarts.min.js"></script>
<script src="/js/common/moment.min.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/homepage/echarts-auto-tooltip.js"></script>
<link rel="stylesheet" href="/css/layui.css" type="text/css">
<link rel="stylesheet" href="/css/soulTable.css" type="text/css">
<script type="text/javascript" src="/js/homepage/homepage.js?t=${currentDate?c}"></script>
</html>