<#macro search>
<section id="content">
    <section class="vbox">
        <section class="scrollable">
            <div class="layui-row">
                <form class="layui-form" style="margin: 0 auto;padding-top: 3px;" lay-filter="formBaseInfo">
                    <table>
                        <tr>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label" style="width: 100px">开发号</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" id="searchClothesDevNumber" autocomplete="off" class="layui-input">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label" style="width: 100px">款号</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" id="searchOrderName" autocomplete="off" class="layui-input">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label"></label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <div class="layui-input-inline">
                                    <button class="layui-btn" lay-submit lay-filter="searchBeat">搜索</button>
                                </div>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
            <table id="clothesDevTable" lay-filter="clothesDevTable"></table>

            <script type="text/html" id="toolbarTop">
                <div class="layui-btn-container">
                    <button class="layui-btn layui-btn-sm" lay-event="refresh">刷新</button>
                    <button class="layui-btn layui-btn-sm" lay-event="clearFilter">清除所有筛选</button>
                    <button class="layui-btn layui-btn-sm" lay-event="addNew">新增</button>
                    <button class="layui-btn layui-btn-sm" lay-event="exportExcel">导出</button>
                </div>
            </script>

            <script type="text/html" id="barTop">
                <a class="layui-btn layui-btn-xs" lay-event="detail">详情</a>
                <a class="layui-btn layui-btn-xs layui-btn-warm" lay-event="update">修改</a>
                <a class="layui-btn layui-btn-xs" lay-event="change">转生产</a>
                <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
            </script>

            <div class="layui-row" id="clothesDevAdd" style="display: none">
                <div class="layui-tab" lay-filter="docDemoTabBrief">
                    <ul class="layui-tab-title">
                        <li class="layui-this" lay-id="1">基本信息</li>
                        <li lay-id="2">款式图</li>
                        <li lay-id="3">面料信息</li>
                        <li lay-id="4">辅料信息</li>
                        <li lay-id="5">尺寸</li>
                        <li lay-id="6">工艺</li>
                    </ul>
                    <div class="layui-tab-content">
                        <div class="layui-tab-item layui-show">
                            <form class="layui-form" style="margin: 0 auto;padding-top: 3px;" lay-filter="formClothsDevInfo">
                                <table>
                                    <tr>
                                        <td style="text-align: right;margin-bottom: 15px;">
                                            <label class="layui-form-label" style="width: 150px">开发号</label>
                                        </td>
                                        <td style="margin-bottom: 15px;">
                                            <input type="text" name="clothesDevNumber" id="clothesDevNumber" autocomplete="off" class="layui-input" readonly>
                                        </td>
                                        <td style="text-align: right;margin-bottom: 15px;">
                                            <label class="layui-form-label" style="width: 150px">客户编号</label>
                                        </td>
                                        <td style="margin-bottom: 15px;">
                                            <input type="text" name="customerCode" id="customerCode" autocomplete="off" class="layui-input">
                                        </td>
                                        <td style="text-align: right;margin-bottom: 15px;">
                                            <label class="layui-form-label" style="width: 150px">客户名称</label>
                                        </td>
                                        <td style="margin-bottom: 15px;">
                                            <input type="text" name="customerName" id="customerName" autocomplete="off" class="layui-input">
                                        </td>
                                        <td style="text-align: right;margin-bottom: 15px;">
                                            <label class="layui-form-label" style="width: 150px">版类</label>
                                        </td>
                                        <td style="margin-bottom: 15px;">
                                            <select id="stylePattern" name="stylePattern" autocomplete="off" class="layui-input">
                                                <option value="开发版">开发版</option>
                                                <option value="大货版">大货版</option>
                                                <option value="复样">复样</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right;margin-bottom: 15px;">
                                            <label class="layui-form-label" style="width: 150px">年份</label>
                                        </td>
                                        <td style="margin-bottom: 15px;">
                                            <input type="text" id="clothesYear" name="clothesYear" autocomplete="off" class="layui-input">
                                        </td>
                                        <td style="text-align: right;margin-bottom: 15px;">
                                            <label class="layui-form-label" style="width: 150px">季度</label>
                                        </td>
                                        <td style="margin-bottom: 15px;">
                                            <select type="text" id="devSeason" name="devSeason" autocomplete="off" class="layui-input">
                                                <option value="春夏">春夏</option>
                                                <option value="秋冬">秋冬</option>
                                            </select>
                                        </td>
                                        <td style="text-align: right;margin-bottom: 15px;">
                                            <label class="layui-form-label" style="width: 150px">要求版期</label>
                                        </td>
                                        <td style="margin-bottom: 15px;">
                                            <input type="text" id="requireDate" name="requireDate" autocomplete="off" class="layui-input">
                                        </td>
                                        <td style="text-align: right;margin-bottom: 15px;">
                                            <label class="layui-form-label" style="width: 150px">预计版期</label>
                                        </td>
                                        <td style="margin-bottom: 15px;">
                                            <input type="text" id="expectDate" name="expectDate" autocomplete="off" class="layui-input">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right;margin-bottom: 15px;">
                                            <label class="layui-form-label" style="width: 150px">款式类别</label>
                                        </td>
                                        <td style="margin-bottom: 15px;">
                                            <input type="text" id="styleCategory" name="styleCategory" autocomplete="off" class="layui-input">
                                        </td>
                                        <td style="text-align: right;margin-bottom: 15px;">
                                            <label class="layui-form-label" style="width: 150px">类别代码</label>
                                        </td>
                                        <td style="margin-bottom: 15px;">
                                            <input type="text" id="categoryCode" name="categoryCode" autocomplete="off" class="layui-input">
                                        </td>
                                        <td style="text-align: right;margin-bottom: 15px;">
                                            <label class="layui-form-label" style="width: 150px">款式名称</label>
                                        </td>
                                        <td style="margin-bottom: 15px;">
                                            <input type="text" id="styleName" name="styleName" autocomplete="off" class="layui-input">
                                        </td>
                                        <td style="text-align: right;margin-bottom: 15px;">
                                            <label class="layui-form-label" style="width: 150px">款式代码</label>
                                        </td>
                                        <td style="margin-bottom: 15px;">
                                            <input type="text" id="styleNumber" name="styleNumber" autocomplete="off" class="layui-input">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right;margin-bottom: 15px;">
                                            <label class="layui-form-label" style="width: 150px">设计部</label>
                                        </td>
                                        <td style="margin-bottom: 15px;">
                                            <select type="text" id="designDepart" name="designDepart" autocomplete="off" class="layui-input"></select>
                                        </td>
                                        <td style="text-align: right;margin-bottom: 15px;">
                                            <label class="layui-form-label" style="width: 150px">设计师</label>
                                        </td>
                                        <td style="margin-bottom: 15px;">
                                            <input type="text" id="designer" name="designer" autocomplete="off" class="layui-input">
                                        </td>
                                        <td style="text-align: right;margin-bottom: 15px;">
                                            <label class="layui-form-label" style="width: 150px">款号</label>
                                        </td>
                                        <td style="margin-bottom: 15px;">
                                            <input type="text" id="devOrderName" name="devOrderName" autocomplete="off" placeholder="点击右下角生成" class="layui-input">
                                        </td>
                                        <td style="text-align: right;margin-bottom: 15px;">
                                            <label class="layui-form-label" style="width: 150px">数量</label>
                                        </td>
                                        <td style="margin-bottom: 15px;">
                                            <input type="text" id="clothesCount" name="clothesCount" autocomplete="off" class="layui-input">
                                        </td>
                                    </tr>
                                </table>
                                <div class="layui-row layui-col-space10" style="padding-top: 50px">
                                    <div class="layui-col-md12">
                                        <label class="layui-form-label" style="font-weight: bolder; width: 150px; font-size: medium">尺码选择</label>
                                        <div class="layui-input-inline" style="width: 76%">
                                            <div id="sizeInfo" class="xm-select-demo"></div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <form>
                                <div class="layui-col-md12" style="padding-top: 20px">
                                    <label class="layui-form-label" style="font-weight: bolder; width: 150px; font-size: medium;">颜色选择</label>
                                    <div class="layui-input-inline" style="width: 76%">
                                        <div id="colorInfo" class="xm-select-demo"></div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="layui-tab-item">
                            <div class="layui-row" id="changeClothesImage">
                                <form class="layui-form" style="margin: 0 auto;padding-top: 10px;">
                                    <div class="layui-row">
                                        <div style="width: 90%; padding-left: 30px; height: 800px">
                                            <textarea id="clothesImageTextarea"></textarea>
                                        </div>
                                      </div>
                                </form>
                            </div s>
                        </div>
                        <div class="layui-tab-item">
                            <table id="fabricTable" lay-filter="fabricTable"></table>
                        </div>
                        <div class="layui-tab-item">
                            <table id="accessoryTable" lay-filter="accessoryTable"></table>
                        </div>
                        <div class="layui-tab-item">
                            <div class="layui-row" id="sizeImage">
                                <table>
                                    <tr>
                                        <td style="text-align: right;margin-bottom: 15px;">
                                            <label class="layui-form-label" style="width: 100px">类别</label>
                                        </td>
                                        <td style="margin-bottom: 15px;">
                                            <select type="text" id="sizeSizeManage" autocomplete="off" class="layui-input" style="width: 200px;"></select>
                                        </td>
                                        <td style="text-align: right;margin-bottom: 15px;">
                                            <label class="layui-form-label"></label>
                                        </td>
                                        <td style="margin-bottom: 15px;">
                                            <div class="layui-input-inline">
                                                <button class="layui-btn" lay-submit lay-filter="searchBeatSize">搜索</button>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <form class="layui-form" style="margin: 0 auto;padding-top: 10px;">
                                    <div class="layui-row">
                                        <div style="width: 90%; padding-left: 30px">
                                            <textarea id="sizeImageTextarea"></textarea>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="layui-tab-item">
                            <div class="layui-row" id="processImage">
                                <table>
                                    <tr>
                                        <td style="text-align: right;margin-bottom: 15px;">
                                            <label class="layui-form-label" style="width: 100px">类别</label>
                                        </td>
                                        <td style="margin-bottom: 15px;">
                                            <select type="text" id="processSizeManage" autocomplete="off" class="layui-input" style="width: 200px;"></select>
                                        </td>
                                        <td style="text-align: right;margin-bottom: 15px;">
                                            <label class="layui-form-label"></label>
                                        </td>
                                        <td style="margin-bottom: 15px;">
                                            <div class="layui-input-inline">
                                                <button class="layui-btn" lay-submit lay-filter="searchBeatProcess">搜索</button>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <form class="layui-form" style="margin: 0 auto;padding-top: 10px;">
                                    <div class="layui-row">
                                        <div style="width: 90%; padding-left: 30px">
                                            <textarea id="processImageTextarea"></textarea>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="layui-row" id="clothesDevChange" style="display: none">
                <div class="layui-row">
                    <form class="layui-form" style="margin: 0 auto;padding-top: 3px;" lay-filter="formClothsDevInfo">
                        <div class="layui-row layui-col-space10" style="padding-top: 50px">
                            <div class="layui-col-md12">
                                <label class="layui-form-label" style="font-weight: bolder; width: 150px; font-size: medium">尺码选择</label>
                                <div class="layui-input-inline" style="width: 76%">
                                    <div id="changeSizeInfo" class="xm-select-demo"></div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="layui-row">
                    <form>
                        <div class="layui-col-md12" style="padding-top: 20px">
                            <label class="layui-form-label" style="font-weight: bolder; width: 150px; font-size: medium;">颜色选择</label>
                            <div class="layui-input-inline" style="width: 76%">
                                <div id="changeColorInfo" class="xm-select-demo"></div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="layui-row" style="padding-top: 30px">
                    <table>
                        <tr>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label" style="width: 100px">开发款号</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" id="thisDevOrderName" autocomplete="off" class="layui-input" style="width: 200px;">
                            </td>
                            <#--<td style="text-align: right;margin-bottom: 15px;">-->
                                <#--<label class="layui-form-label" style="width: 100px">序列号</label>-->
                            <#--</td>-->
                            <#--<td style="margin-bottom: 15px;">-->
                                <#--<input type="text" id="serialNumber" autocomplete="off" readonly class="layui-input" style="width: 200px;" value="001">-->
                            <#--</td>-->
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label" style="width: 100px">客户编号</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" id="changeCustomerCode" autocomplete="off" class="layui-input" style="width: 200px;">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label" style="width: 100px">客户名称</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" id="changeCustomerName" autocomplete="off" class="layui-input" style="width: 200px;">
                            </td>
                        </tr><tr>
                            <#--<td style="text-align: right;margin-bottom: 15px;">-->
                                <#--<label class="layui-form-label" style="width: 100px">商标</label>-->
                            <#--</td>-->
                            <#--<td style="margin-bottom: 15px;">-->
                                <#--<input type="text" id="brandName" autocomplete="off" class="layui-input" style="width: 200px;">-->
                            <#--</td>-->
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label" style="width: 100px">客户款号</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" id="changeOrderName" autocomplete="off" class="layui-input" style="width: 200px;">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label" style="width: 100px">交期</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" id="changeDelivery" autocomplete="off" class="layui-input" style="width: 200px;">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label"></label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <div class="layui-input-inline">
                                    <button class="layui-btn" lay-submit lay-filter="searchBeatOrder">加载</button>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="layui-row">
                    <table id="colorSizeTable" lay-filter="colorSizeTable" style="width: 98%; padding-top: 20px"></table>
                </div>
            </div>

        </section>
    </section>
</section>

    <link rel="stylesheet" href="/css/layui.css" type="text/css">
    <link rel="stylesheet" href="/css/step.css" type="text/css">
    <link rel="stylesheet" href="/css/soulTable.css" type="text/css">
    <link rel="stylesheet" href="/css/publish.css" type="text/css">
    <link rel="stylesheet" href="/css/selectize.bootstrap3.css" type="text/css">

    <script src="/js/common/layer.js"></script>
    <script src="/js/step/step.js?t=${currentDate?c}" type="text/javascript"></script>
    <script src="/js/common/xm-select.js?t=${currentDate?c}" type="text/javascript"></script>
    <script src="/js/common/selectize.js" type="text/javascript"></script>
    <script src="/js/clothesDev/clothesDev.js?t=${currentDate?c}"></script>
    <script src="/js/common/moment.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/common/tinymce/tinymce.min.js"></script>
    <script type="text/javascript" src="/js/common/tinymce/zh_CN.js"></script>
</#macro>

<style>

    ::-webkit-scrollbar-track-piece {

        background-color:#f8f8f8;

    }

    ::-webkit-scrollbar {

        width:15px;

        height:15px;

    }

    ::-webkit-scrollbar-thumb {

        background-color:#dddddd;

        background-clip:padding-box;

        min-height:28px;

    }

    ::-webkit-scrollbar-thumb:hover {

        background-color:#bbb;

    }
</style>
