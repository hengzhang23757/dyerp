<#macro search>
<section id="content">
    <section class="vbox">
        <section class="scrollable">
            <div class="layui-row">
                <form class="layui-form" style="margin: 0 auto;padding-top: 3px;" lay-filter="formBaseInfo">
                    <table>
                        <tr>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label" style="width: 100px">面料名</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" id="fabricName" autocomplete="off" class="layui-input">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label" style="width: 100px">面料号</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" id="fabricNumber" autocomplete="off" class="layui-input">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label"></label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <div class="layui-input-inline">
                                    <button class="layui-btn" lay-submit lay-filter="searchBeat">搜索</button>
                                </div>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
            <table id="dataTable" lay-filter="dataTable"></table>

            <script type="text/html" id="toolbarTop">
                <div class="layui-btn-container">
                    <button class="layui-btn layui-btn-sm" lay-event="refresh">刷新</button>
                    <button class="layui-btn layui-btn-sm" lay-event="clearFilter">清除所有筛选</button>
                    <button class="layui-btn layui-btn-sm" lay-event="addNew">添加</button>
                    <button class="layui-btn layui-btn-sm" lay-event="exportExcel">导出</button>
                    <button class="layui-btn layui-btn-sm" lay-event="storage">库存</button>
                </div>
            </script>

            <script type="text/html" id="barTop">
                <a class="layui-btn layui-btn-xs layui-btn-normal" lay-event="add">新增</a>
                <a class="layui-btn layui-btn-xs" lay-event="inStore">入库</a>
                <#--<a class="layui-btn layui-btn-xs layui-btn-normal" lay-event="outStore">出库</a>-->
                <a class="layui-btn layui-btn-xs" lay-event="update">修改</a>
                <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
            </script>

            <div class="layui-row" id="fabricOperate" style="display: none">
                <form class="layui-form" style="margin: 0 auto;padding-top: 3px;" lay-filter="fabricAdd">
                    <table>
                        <tr>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label" style="width: 100px">面料名</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" id="fabricNameAdd" name="fabricNameAdd" autocomplete="off" class="layui-input">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label" style="width: 100px">代码</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <select type="text" id="fabricNumberAdd" name="fabricNumberAdd" autocomplete="off" class="layui-input" lay-filter="demo"></select>
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label" style="width: 100px">序号</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" id="fabricNumberOrderAdd" name="fabricNumberOrderAdd" autocomplete="off" class="layui-input">
                            </td>
                        </tr><tr>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label" style="width: 100px">颜色</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <div id="colorInfo" class="xm-select-demo"></div>
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label" style="width: 100px">布封</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" id="fabricWidth" name="fabricWidth" autocomplete="off" class="layui-input">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label" style="width: 100px">克重</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" id="fabricWeight" name="fabricWeight" autocomplete="off" class="layui-input">
                            </td>
                        </tr><tr>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label" style="width: 100px">成分</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" id="fabricContent" name="fabricContent" autocomplete="off" class="layui-input">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label" style="width: 100px">单位</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <select type="text" id="unit" name="unit" autocomplete="off" class="layui-input">
                                    <option value="米">米</option>
                                    <option value="公斤">公斤</option>
                                    <option value="磅">磅</option>
                                    <option value="码">码</option>
                                </select>
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label" style="width: 100px">单位2</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <select type="text" id="unitTwo" name="unitTwo" autocomplete="off" class="layui-input">
                                    <option value="">选择单位</option>
                                    <option value="公斤">公斤</option>
                                    <option value="磅">磅</option>
                                    <option value="米">米</option>
                                    <option value="码">码</option>
                                </select>
                            </td>
                        </tr><tr>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label" style="width: 100px">转换系数</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" id="ratio" name="ratio" autocomplete="off" class="layui-input">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label" style="width: 100px">单价2</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" id="priceTwo" name="priceTwo" autocomplete="off" class="layui-input">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label" style="width: 100px">供应商</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <div id="supplier" class="xm-select-demo"></div>
                            </td>
                        </tr><tr>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label" style="width: 100px">备注</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" id="remark" name="remark" autocomplete="off" class="layui-input">
                            </td>
                        </tr>
                    </table>
                </form>
            </div>

            <div class="layui-row" id="inStoreOperate" style="display: none">
                <form class="layui-form" style="margin: 0 auto;padding-top: 3px;" lay-filter="inStoreAdd">
                    <table>
                        <tr>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label" style="width: 100px">数量</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" id="fabricInCount" autocomplete="off" class="layui-input">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label" style="width: 100px">卷数</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" id="batchNumberIn" autocomplete="off" class="layui-input">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label" style="width: 100px">单位</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <select type="text" id="unitTwoIn" autocomplete="off" class="layui-input"></select>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label" style="width: 100px">单价</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" id="priceTwoIn" autocomplete="off" class="layui-input">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label" style="width: 100px">日期</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" id="operateDate" autocomplete="off" class="layui-input">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label" style="width: 100px">位置</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" id="location" autocomplete="off" class="layui-input">
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label" style="width: 100px">状态</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <select type="text" id="payStateIn" autocomplete="off" class="layui-input">
                                    <option value="发票">发票</option>
                                    <option value="收据">收据</option>
                                    <option value="已付">已付</option>
                                    <option value="未付">未付</option>
                                    <option value="现金">现金</option>
                                </select>
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label" style="width: 100px">备注</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" id="remarkIn" autocomplete="off" class="layui-input">
                            </td>
                        </tr>
                    </table>
                </form>
            </div>

            <div class="layui-row" id="outStoreOperate" style="display: none">
                <form class="layui-form" style="margin: 0 auto;padding-top: 3px;" lay-filter="outStoreAdd">
                    <table>
                        <tr>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label" style="width: 100px">单位</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <select type="text" id="unitTwoOut" autocomplete="off" class="layui-input"></select>
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label" style="width: 100px">数量</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" id="fabricOutCount" autocomplete="off" class="layui-input">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label" style="width: 100px">卷数</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" id="batchNumberOut" autocomplete="off" class="layui-input">
                            </td>
                        </tr>
                    </table>
                </form>
            </div>

        </section>
    </section>
</section>

    <link rel="stylesheet" href="/css/layui.css" type="text/css">
    <link rel="stylesheet" href="/css/step.css" type="text/css">
    <link rel="stylesheet" href="/css/soulTable.css" type="text/css">
    <link rel="stylesheet" href="/css/publish.css" type="text/css">
    <link rel="stylesheet" href="/css/selectize.bootstrap3.css" type="text/css">

    <script src="/js/common/layer.js"></script>
    <script src="/js/step/step.js?t=${currentDate?c}" type="text/javascript"></script>
    <script src="/js/common/xm-select.js?t=${currentDate?c}" type="text/javascript"></script>
    <script src="/js/common/selectize.js" type="text/javascript"></script>
    <script src="/js/clothesDev/clothesDevFabric.js?t=${currentDate?c}"></script>
    <script src="/js/common/moment.min.js" type="text/javascript"></script>
</#macro>

<style>

    ::-webkit-scrollbar-track-piece {

        background-color:#f8f8f8;

    }

    ::-webkit-scrollbar {

        width:15px;

        height:15px;

    }

    ::-webkit-scrollbar-thumb {

        background-color:#dddddd;

        background-clip:padding-box;

        min-height:28px;

    }

    ::-webkit-scrollbar-thumb:hover {

        background-color:#bbb;

    }
</style>
