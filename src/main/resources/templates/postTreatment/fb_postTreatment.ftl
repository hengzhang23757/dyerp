<#macro search>
    <section id="content">
        <section class="vbox">
            <section class="scrollable">
                <div class="layui-row">
                    <form class="layui-form" style="margin: 0 auto;padding-top: 3px;" lay-filter="baseInfo">
                        <table>
                            <tr>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label">季度</label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <select type="text" name="season" id="season" autocomplete="off" class="layui-input"></select>
                                </td>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label">品牌</label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <select type="text" name="customerName" id="customerName" autocomplete="off" class="layui-input"></select>
                                </td>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label">单号</label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <input type="text" name="clothesVersionNumber" id="clothesVersionNumber" autocomplete="off" class="layui-input">
                                </td>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label">款号</label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <input type="text" name="orderName" id="orderName" autocomplete="off" class="layui-input">
                                </td>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label">开始</label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <input type="text" name="from" id="from" autocomplete="off" class="layui-input">
                                </td>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label">结束</label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <input type="text" name="to" id="to" autocomplete="off" class="layui-input">
                                </td>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label"></label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <div class="layui-input-inline">
                                        <button class="layui-btn" lay-submit lay-filter="searchBeat">搜索</button>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
                <table id="reportTable" lay-filter="reportTable"></table>
                <script type="text/html" id="toolbarTop">
                    <div class="layui-btn-container">
                        <button class="layui-btn layui-btn-sm" lay-event="clearFilter">清除所有筛选</button>
                        <button class="layui-btn layui-btn-sm" lay-event="addWash">添加洗水记录</button>
                        <button class="layui-btn layui-btn-sm" lay-event="addShip">添加出货记录</button>
                        <button class="layui-btn layui-btn-sm" lay-event="addSurplus">添加正品余数</button>
                        <button class="layui-btn layui-btn-sm" lay-event="addDefective">添加次品数量</button>
                        <button class="layui-btn layui-btn-sm" lay-event="addChicken">添加次片数量</button>
                        <button class="layui-btn layui-btn-sm" lay-event="addSample">添加抽办</button>
                    </div>
                </script>
                <script type="text/html" id="washCountChildBar">
                    <a class="layui-btn layui-btn-xs layui-btn-normal" lay-event="updateWashCount">修改</a>
                    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="deleteWashCount">删除</a>
                </script>
                <script type="text/html" id="shipCountChildBar">
                    <a class="layui-btn layui-btn-xs layui-btn-normal" lay-event="updateShipCount">修改</a>
                    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="deleteShipCount">删除</a>
                </script>
                <script type="text/html" id="surplusCountChildBar">
                    <a class="layui-btn layui-btn-xs layui-btn-normal" lay-event="updateSurplusCount">修改</a>
                    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="deleteSurplusCount">删除</a>
                </script>
                <script type="text/html" id="defectiveCountChildBar">
                    <a class="layui-btn layui-btn-xs layui-btn-normal" lay-event="updateDefectiveCount">修改</a>
                    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="deleteDefectiveCount">删除</a>
                </script>
                <script type="text/html" id="chickenCountChildBar">
                    <a class="layui-btn layui-btn-xs layui-btn-normal" lay-event="updateChickenCount">修改</a>
                    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="deleteChickenCount">删除</a>
                </script>
                <script type="text/html" id="sampleCountChildBar">
                    <a class="layui-btn layui-btn-xs layui-btn-normal" lay-event="updateSampleCount">修改</a>
                    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="deleteSampleCount">删除</a>
                </script>
            </section>
        </section>
    </section>
    <link rel="stylesheet" href="/css/layui.css" type="text/css">
    <link rel="stylesheet" href="/css/soulTable.css" type="text/css">
    <script src="/js/postTreatment/postTreatment.js?t=${currentDate?c}"></script>

</#macro>

<style>
    button {
        background:rgb(45, 202, 147);
        opacity:0.86;
        color: white;
        font-family: PingFangSC-Semibold, sans-serif;
    }
    ::-webkit-scrollbar-track-piece {

        background-color:#f8f8f8;

    }

    ::-webkit-scrollbar {

        width:15px;

        height:15px;

    }

    ::-webkit-scrollbar-thumb {

        background-color:#dddddd;

        background-clip:padding-box;

        min-height:28px;

    }

    ::-webkit-scrollbar-thumb:hover {

        background-color:#bbb;

    }
</style>