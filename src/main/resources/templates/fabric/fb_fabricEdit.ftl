<#macro entities>
    <div id="editFabric" style="display: none;cursor:default">
        <div style="overflow-x: auto;overflow-y:auto;">
            <div style="text-align: right;margin-bottom:20px">
                <a id="editFabricNo" href="#" style="font-size:20px"><i class="fa fa-times icon" style="color: rgb(182,182,182)"></i></a>
            </div>
            <table style="border-collapse:separate;border-spacing: 10px;width: 100%;">
                <tr>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">成衣客户</label>
                    </td>
                    <td>
                        <input id="customerName" class="form-control" autocomplete="off" style="width: 100%;margin-bottom: 15px;">
                    </td>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">季度</label>
                    </td>
                    <td>
                        <input id="season" class="form-control" autocomplete="off" style="margin-bottom: 15px;width: 100%;">
                    </td>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">版单号</label>
                    </td>
                    <td>
                        <input id="versionNumber" class="form-control" autocomplete="off" style="margin-bottom: 15px;width: 100%;">
                    </td>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">大货款号</label>
                    </td>
                    <td>
                        <input id="styleNumber" class="form-control" autocomplete="off" style="margin-bottom: 15px;width: 100%;">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">供应商</label>
                    </td>
                    <td>
                        <input id="supplier" class="form-control" autocomplete="off" style="margin-bottom: 15px;width: 100%;">
                    </td>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">下单日期</label>
                    </td>
                    <td>
                        <input id="orderDate" class="form-control" autocomplete="off" style="margin-bottom: 15px;width: 100%;">
                    </td>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">面料交期</label>
                    </td>
                    <td>
                        <input id="deliveryDate" class="form-control" autocomplete="off" style="margin-bottom: 15px;width: 100%;">
                    </td>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">面料名称</label>
                    </td>
                    <td>
                        <input id="fabricName" class="form-control" style="margin-bottom: 15px;width: 100%;" autocomplete="off">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">面料编号</label>
                    </td>
                    <td>
                        <input id="fabricNumber" class="form-control" style="margin-bottom: 15px;width: 100%;" autocomplete="off">
                    </td>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">面料颜色</label>
                    </td>
                    <td>
                        <input id="fabricColor" class="form-control" style="margin-bottom: 15px;width: 100%;" autocomplete="off">
                    </td>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">面料色号</label>
                    </td>
                    <td>
                        <input id="fabricColorNumber" class="form-control" style="margin-bottom: 15px;width: 100%;" autocomplete="off">
                    </td>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">面料用途</label>
                    </td>
                    <td>
                        <input id="fabricUse" class="form-control" style="margin-bottom: 15px;width: 100%;" autocomplete="off">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">单位</label>
                    </td>
                    <td>
                        <input id="unit" class="form-control" style="margin-bottom: 15px;width: 100%;">
                    </td>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">单位用量</label>
                    </td>
                    <td>
                        <input id="pieceUsage" class="form-control" style="margin-bottom: 15px;width: 100%;" autocomplete="off">
                    </td>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">成衣数量</label>
                    </td>
                    <td>
                        <input id="clothesCount" class="form-control" style="margin-bottom: 15px;width: 100%;">
                    </td>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">订布数量</label>
                    </td>
                    <td>
                        <input id="fabricCount" class="form-control" style="margin-bottom: 15px;width: 100%;" autocomplete="off">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">单价</label>
                    </td>
                    <td>
                        <input id="price" class="form-control" style="margin-bottom: 15px;width: 100%;">
                    </td>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">有效门幅</label>
                    </td>
                    <td>
                        <input id="fabricWidth" class="form-control" style="margin-bottom: 15px;width: 100%;" autocomplete="off">
                    </td>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">克重</label>
                    </td>
                    <td>
                        <input id="fabricWeight" class="form-control" style="margin-bottom: 15px;width: 100%;">
                    </td>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">实际交货日期</label>
                    </td>
                    <td>
                        <input id="actualDeliveryDate" class="form-control" style="margin-bottom: 15px;width: 100%;" autocomplete="off">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">交货数量</label>
                    </td>
                    <td>
                        <input id="deliveryQuantity" class="form-control" style="margin-bottom: 15px;width: 100%;" autocomplete="off">
                    </td>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">卷数</label>
                    </td>
                    <td>
                        <input id="batchNumber" class="form-control" style="margin-bottom: 15px;width: 100%;" autocomplete="off">
                    </td>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">缸号</label>
                    </td>
                    <td>
                        <input id="jarNumber" class="form-control" style="margin-bottom: 15px;width: 100%;">
                    </td>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">备注</label>
                    </td>
                    <td>
                        <input id="remark" class="form-control" style="margin-bottom: 15px;width: 100%;" autocomplete="off">
                    </td>
                </tr>

            </table>
        </div>
        <div class="col-sm-12" style="padding-top: 10px">
            <button id="editFabricYes" class="btn btn-s-lg" style="border-radius: 5px;text-align: center;color:white;font-family: PingFangSC-Semibold;">保存</button>
        </div>
    </div>
</#macro>