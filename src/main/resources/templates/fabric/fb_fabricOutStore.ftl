<#macro search>
<section id="content">
    <section class="vbox">
        <section class="scrollable padder water-mark-visible-ff">
            <div class="row">
                <div class="col-md-6" style="margin-left: 35px;height: 100px;text-align: center;vertical-align: middle;background:rgb(74, 144, 226);border-radius: 15px;">
                    <label style="color:white;font-size:35px;font-family: PingFangSC-Semibold,sans-serif;width:100%;height: 100%;vertical-align: middle;margin-top: 4%;">面料出仓</label>
                </div>
            </div>
            <div class="col-md-12" style="padding-top: 30px;">

                <section class="panel panel-default">
                    <header class="panel-heading font-bold">
                        <span style="font-family: PingFangSC-Semibold,sans-serif;color:black;font-size:22px">面料信息</span>
                    </header>
                    <table style="border-collapse:separate;border-spacing: 10px;width: 100%;" id="baseInfo">
                        <tr>
                            <td style="text-align: right;margin-bottom: 15px;width: 100px">
                                <label class="control-label" style="">版单号</label>
                            </td>
                            <td style="margin-bottom: 15px;width: 300px">
                                <input id="clothesVersionNumber" class="form-control" autocomplete="off" style="width: 100%">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;width: 100px">
                                <label class="control-label" style="">订单</label>
                            </td>
                            <td style="margin-bottom: 15px;width: 300px">
                                <select id="orderName" class="form-control" style="width: 100%"></select>
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;width: 100px">
                                <label class="control-label" style="">色组</label>
                            </td>
                            <td style="margin-bottom: 15px;width: 300px">
                                <select id="colorName" class="form-control" style="width: 100%"></select>
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;width: 100px">
                                <label class="control-label" style="">面料</label>
                            </td>
                            <td style="margin-bottom: 15px;width: 300px">
                                <select id="fabricName" class="form-control" style="width: 100%"></select>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right;margin-bottom: 15px;width: 100px">
                                <label class="control-label" style="">面料颜色</label>
                            </td>
                            <td style="margin-bottom: 15px;width: 300px">
                                <select id="fabricColor" class="form-control" style="width: 100%"></select>
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;width: 100px">
                                <label class="control-label">缸号</label>
                            </td>
                            <td style="margin-bottom: 15px;width: 300px">
                                <select id="jarName" class="form-control" autocomplete="off" style="width: 100%"></select>
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;width: 100px">
                                <label class="control-label">货架</label>
                            </td>
                            <td style="margin-bottom: 15px;width: 300px">
                                <select id="location" class="form-control" style="width: 100%"></select>
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;width: 100px">
                                <label class="control-label">数量</label>
                            </td>
                            <td style="margin-bottom: 15px;width: 300px">
                                <input id="weight" class="form-control" style="width: 100%">
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right;margin-bottom: 15px;width: 100px">
                                <label class="control-label">卷数</label>
                            </td>
                            <td style="margin-bottom: 15px;width: 300px">
                                <input id="batchNumber" class="form-control" style="width: 100%">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;width: 100px">
                                <label class="control-label">类型</label>
                            </td>
                            <td style="margin-bottom: 15px;width: 300px">
                                <select id="operateType" class="form-control" autocomplete="off" style="width: 100%">
                                    <option value="">出库类型</option>
                                    <option value="外发加工">外发加工</option>
                                    <option value="退面料厂">退面料厂</option>
                                    <option value="余布出售">余布出售</option>
                                    <option value="面料异常">面料异常</option>
                                    <option value="换款">换款</option>
                                </select>
                            </td>
                        </tr>
                    </table>
                </section>
                <section class="panel panel-default" id ="transOrder" hidden>
                    <header class="panel-heading font-bold">
                        <span style="font-family: PingFangSC-Semibold,sans-serif;color:black;font-size:22px">换款信息</span>
                    </header>
                    <div class="panel-body" style="text-align: left">
                        <div style="text-align: right;font-size:20px">
                            <div class="col-md-12" style="margin-left: 10px;margin-top: 10px;">
                                <div class="col-md-3">
                                    <span>版单：</span>
                                    <input id="toClothesVersionNumber" class="form-control" style="background: rgb(248,248,248);width: 70%;">
                                </div>
                                <div class="col-md-3">
                                    <span>订单：</span>
                                    <select id="toOrderName" class="form-control" style="background: rgb(248,248,248);width: 70%;"></select>
                                </div>
                                <div class="col-md-3">
                                    <span>色组：</span>
                                    <select id="toColorName" class="form-control" style="background: rgb(248,248,248);width: 70%;"></select>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <div class="col-md-12" style="text-align: center;margin-top: 20px">
                <button  class="btn btn-s-lg" style="border-radius: 5px;font-size:20px"  onclick="outStore()">提交</button>
            </div>
        </section>
    </section>
    <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen"
       data-target="#nav"></a>
</section>
<aside class="bg-light lter b-l aside-md hide" id="notes">
    <div class="wrapper">Notification</div>
</aside>
</section>
<script src="/js/common/jquery.blockUI.js" type="text/javascript" ></script>
<script src="/js/fabric/fabricOutStore.js?t=${currentDate?c}"></script>

</#macro>

<style>
    button {
        background:rgb(74, 144, 226);
        opacity:0.86;
        color: white;

        font-family: PingFangSC-Semibold, sans-serif;
    }

    #orderNameTips{
        margin-left: 105px
    }
</style>

