<#macro search>
<section id="content">
    <section class="vbox">
        <section class="scrollable">
            <div class="layui-row">
                <form class="layui-form" style="margin: 0 auto;padding-top: 3px;" lay-filter="baseInfo">
                    <table>
                        <tr>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label">类型</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <select type="text" name="searchType" id="searchType" lay-filter="test">
                                    <option value="面料">面料</option>
                                    <option value="辅料">辅料</option>
                                </select>
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label">供应商</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <select type="text" name="supplier" id="supplier">
                                </select>
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label">状态</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <select type="text" name="checkState" id="checkState">
                                    <option value="全部">全部</option>
                                    <option value="未提交">未提交</option>
                                    <option value="已提交">已提交</option>
                                    <option value="通过">通过</option>
                                    <option value="未通过">未通过</option>
                                </select>
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label">单号</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" name="clothesVersionNumber" class="layui-input" id="clothesVersionNumber">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label">款号</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" name="orderName" class="layui-input" id="orderName">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label"></label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <div class="layui-input-inline">
                                    <button class="layui-btn" lay-submit lay-filter="searchPlaceOrder">搜索</button>
                                </div>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>

            <table id="placeOrderTable" lay-filter="placeOrderTable"></table>

            <script type="text/html" id="toolbarTop">
                <div class="layui-btn-container">
                    <button class="layui-btn layui-btn-sm" lay-event="exportExcel">导出</button>
                    <button class="layui-btn layui-btn-sm" lay-event="pass">通过</button>
                    <button class="layui-btn layui-btn-sm" lay-event="unPass">不通过</button>
                    <button class="layui-btn layui-btn-sm" lay-event="returnReview">反审</button>
                </div>
            </script>

        </section>
    </section>
</section>

    <link rel="stylesheet" href="/css/layui.css" type="text/css">
    <link rel="stylesheet" href="/css/soulTable.css" type="text/css">
    <link rel="stylesheet" href="/css/selectize.bootstrap3.css" type="text/css">
    <#--<script src="/js/common/layui.js" type="text/javascript"></script>-->
    <script src="/js/common/layer.js"></script>
    <script src="/js/common/xm-select.js?t=${currentDate?c}" type="text/javascript"></script>
    <script src="/js/common/selectize.js" type="text/javascript"></script>
    <script src="/js/common/moment.min.js" type="text/javascript"></script>
    <script src="/js/fabric/placeOrder.js?t=${currentDate?c}"></script>
</#macro>

<style>

    ::-webkit-scrollbar-track-piece {

        background-color:#f8f8f8;

    }

    ::-webkit-scrollbar {

        width:15px;

        height:15px;

    }

    ::-webkit-scrollbar-thumb {

        background-color:#dddddd;

        background-clip:padding-box;

        min-height:28px;

    }

    ::-webkit-scrollbar-thumb:hover {

        background-color:#bbb;

    }
</style>

