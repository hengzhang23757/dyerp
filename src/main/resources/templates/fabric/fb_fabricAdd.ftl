<!DOCTYPE html>
<html>
<head>
    <input  type="hidden" value="${userName!?string}"  id="userName"/>
    <#include "../feedback/fb_script.ftl">
    <@script> </@script>
    <link rel="stylesheet" href="/css/handsontable.full.min.css" type="text/css">
    <link rel="stylesheet" href="/css/selectize.bootstrap3.css" type="text/css">
    <link rel="stylesheet" href="/css/bootstrap.min.css" type="text/css">
    <script src="/js/common/handsontable.full.min.js" type="text/javascript" ></script>
    <script src="/js/common/zh-CN.js" type="text/javascript" ></script>
    <script src="/js/fabric/addFabric.js?t=${currentDate?c}"></script>
</head>

<body>
    <input type="hidden" value="${basePath}" id="basePath"/>
    <div>
        <div id="addFabricExcel" style="margin-left: 10px;overflow-x: auto;height: 500px"></div>
    </div>
    <#if type == "add">
        <div class="col-md-12" style="text-align: center;margin-top: 20px">
            <button  class="btn btn-s-lg" style="border-radius: 5px;"  onclick="add()">提交</button>
        </div>
    </#if>

</body>
</html>
<style>
    button {
        background:rgb(45, 202, 147);
        opacity:0.86;
        color: white;
        font-family: PingFangSC-Semibold;
    }
</style>
