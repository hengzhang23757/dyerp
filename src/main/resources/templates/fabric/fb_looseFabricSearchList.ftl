<section class="panel panel-default">

    <div id="tableDiv">
        <table id="looseFabricTable" class="table table-striped m-b-none ">
            <#if looseFabricList??>
            <thead>
            <tr bgcolor="#ffcb99" style="color: black; text-align: center">
                <th style="width: 45px; text-align: center">序号</th>
                <th style="width: 120px; text-align: center">版单</th>
                <th style="width: 120px; text-align: center">订单</th>
                <th style="width: 60px; text-align: center">订单颜色</th>
                <th style="width: 200px; text-align: center">面料名称</th>
                <th style="width: 60px; text-align: center">面料颜色</th>
                <th style="width: 120px; text-align: center">缸号</th>
                <th style="width: 80px; text-align: center">时间</th>
                <th style="width: 60px; text-align: center">数量</th>
                <th style="width: 60px; text-align: center">卷数</th>
                <th style="width: 60px; text-align: center">总卷数</th>
                <th style="width: 80px; text-align: center">类型</th>
                <th style="width: 60px; text-align: center">出库仓</th>
            </tr>
            </thead>
            <tbody>
                <#list looseFabricList as vo>
                <tr>
                    <td style="text-align: center">${vo_index+1}</td>
                    <td style="text-align: center"><#if vo.clothesVersionNumber??>${vo.clothesVersionNumber}</#if></td>
                    <td style="text-align: center"><#if vo.orderName??>${vo.orderName}</#if></td>
                    <td style="text-align: center"><#if vo.colorName??>${vo.colorName}</#if></td>
                    <td style="text-align: center"><#if vo.fabricName??>${vo.fabricName}</#if></td>
                    <td style="text-align: center"><#if vo.fabricColor??>${vo.fabricColor}</#if></td>
                    <td style="text-align: center"><#if vo.jarNumber??>${vo.jarNumber}</#if></td>
                    <td style="text-align: center"><#if vo.looseTime??>${vo.looseTime?date}</#if></td>
                    <td style="text-align: center"><#if vo.weight??>${vo.weight?string("#.##")}</#if></td>
                    <td style="text-align: center"><#if vo.batchOrder??>${vo.batchOrder}</#if></td>
                    <td style="text-align: center"><#if vo.totalBatch??>${vo.totalBatch}</#if></td>
                    <td style="text-align: center"><#if vo.operateType??>${vo.operateType}</#if></td>
                    <td style="text-align: center"><#if vo.fromLocation??>${vo.fromLocation}</#if></td>
                </tr>
                </#list>
            </tbody>
            <tfoot>
            <tr>
                <th colspan="6" style="text-align:right"></th>
            </tr>
            </tfoot>
            <#else>
            <h4 style="text-align: center">无该状态的数据</h4>
            </#if>
        </table>
    </div>

</section>

<style>
    td {
        word-break:break-all;
    }

</style>