<#macro search>
    <section id="content">
        <section class="vbox">
            <section class="scrollable padder water-mark-visible-ff"  style="background-color: #f7f7f7;">
                <div class="col-md-12" style="padding-top: 10px;">
                    <section class="panel panel-default">
                        <div class="panel-body">
                            <div class="row" style="margin-bottom: 10px">
                                <table style="border-collapse:separate;border-spacing: 10px;width: 100%;" id="baseInfo">
                                    <tr>
                                        <td style="text-align: right;margin-bottom: 15px;">
                                            <label class="control-label" style="">类型</label>
                                        </td>
                                        <td style="margin-bottom: 15px;">
                                            <select id="type" class="form-control" style="width: 150px">
                                                <option value="面料">面料</option>
                                                <option value="辅料">辅料</option>
                                            </select>
                                        </td>
                                        <td style="text-align: right;margin-bottom: 15px;">
                                            <label class="control-label" style="">品牌</label>
                                        </td>
                                        <td style="margin-bottom: 15px;">
                                            <select id="customerName" class="form-control" style="width: 150px"></select>
                                        </td>
                                        <td style="text-align: right;margin-bottom: 15px;">
                                            <label class="control-label" style="">季度</label>
                                        </td>
                                        <td style="margin-bottom: 15px;">
                                            <input id="season" class="form-control" style="width: 150px">
                                        </td>
                                        <td style="text-align: right;margin-bottom: 15px;">
                                            <label class="control-label" style="">单号</label>
                                        </td>
                                        <td style="margin-bottom: 15px;">
                                            <input id="clothesVersionNumber" class="form-control" placeholder="输入单号" autocomplete="off" style="width: 150px">
                                        </td>
                                        <td style="text-align: right;margin-bottom: 15px;">
                                            <label class="control-label" style="">款号</label>
                                        </td>
                                        <td style="margin-bottom: 15px;">
                                            <input id="orderName" class="form-control" autocomplete="off" placeholder="输入款号" style="width: 150px">
                                        </td>
                                        <td style="text-align: right;margin-bottom: 15px;">
                                            <label class="control-label" style="">物料</label>
                                        </td>
                                        <td style="margin-bottom: 15px;">
                                            <input id="materialName" class="form-control" autocomplete="off" style="width: 150px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right;margin-bottom: 15px;">
                                            <label class="control-label" style="">供应商</label>
                                        </td>
                                        <td style="margin-bottom: 15px;">
                                            <input id="supplierName" class="form-control" autocomplete="off" style="width: 150px">
                                        </td>
                                        <td style="text-align: right;margin-bottom: 15px;">
                                            <label class="control-label" style="">乙方公司</label>
                                        </td>
                                        <td style="margin-bottom: 15px;">
                                            <input id="supplierFullName" class="form-control" autocomplete="off" placeholder="乙方公司全称" style="width: 150px">
                                        </td>
                                        <td style="text-align: right;margin-bottom: 15px;">
                                            <label class="control-label" style="">乙方地址</label>
                                        </td>
                                        <td style="margin-bottom: 15px;">
                                            <input id="supplierAddress" class="form-control" autocomplete="off" placeholder="乙方地址" style="width: 150px">
                                        </td>
                                        <td style="text-align: right;margin-bottom: 15px;">
                                            <label class="control-label" style="">签约日期</label>
                                        </td>
                                        <td style="margin-bottom: 15px;">
                                            <input id="orderDate" class="form-control" autocomplete="off" style="width: 150px">
                                        </td>
                                        <td style="text-align: right;margin-bottom: 15px;">
                                            <label class="control-label" style="">交货日期</label>
                                        </td>
                                        <td style="margin-bottom: 15px;">
                                            <input id="deliveryDate" class="form-control" autocomplete="off" style="width: 150px">
                                        </td>
                                        <td style="text-align: right;margin-bottom: 15px;">
                                            <label class="control-label" style=""></label>
                                        </td>
                                        <td style="margin-bottom: 15px;">
                                            <button  class="btn" style="width: 100px; border-radius: 5px;color:white;font-family: PingFangSC-Semibold, sans-serif;"  onclick="search()">查找</button>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-12" style="padding-top: 20px;padding-bottom:10px" id="exportDiv" hidden>
                                <#--<button  class="btn btn-s-lg" style="border-radius: 5px;color:white;"  onclick="exportData()">导出</button>-->
                                <button  class="btn btn-s-lg" style="border-radius: 5px;color:white;"  onclick="printDeal()">打印</button>
                            </div>
                            <div id="addOrderExcel" hidden style="margin-left: 10px;"></div>
                        </div>
                    </section>
                </div>
                <div class="row">
                    <div id="entities">
                    </div>
                </div>
                <iframe id="printf" src="" width="0" height="0" frameborder="0"></iframe>
            </section>
        </section>
        <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen"
           data-target="#nav"></a>
    </section>
    <aside class="bg-light lter b-l aside-md hide" id="notes">
        <div class="wrapper">Notification</div>
    </aside>
    </section>
<#--<link rel="stylesheet" href="/css/bootstrap.min.css" type="text/css">-->
    <link rel="stylesheet" href="/css/handsontable.full.min.css" type="text/css">
    <script src="/js/common/handsontable.full.min.js" type="text/javascript" ></script>
    <script src="/js/common/export2Excel2.js" type="text/javascript" ></script>
    <script src="/js/common/zh-CN.js" type="text/javascript" ></script>
    <script src="/js/common/jquery.blockUI.js" type="text/javascript" ></script>
    <script src="/js/common/moment.min.js" type="text/javascript"></script>
    <script src="/js/common/export.js"></script>
    <script src="/js/common/xlsx.full.min.js"></script>
    <script src="/js/fabric/orderCheck.js?t=${currentDate?c}"></script>

</#macro>

<style>
    button {
        background:rgb(45, 202, 147);
        opacity:0.86;
        color: white;
        font-family: PingFangSC-Semibold, sans-serif;
    }
    ::-webkit-scrollbar-track-piece {

        background-color:#f8f8f8;

    }

    ::-webkit-scrollbar {

        width:15px;

        height:15px;

    }

    ::-webkit-scrollbar-thumb {

        background-color:#dddddd;

        background-clip:padding-box;

        min-height:28px;

    }

    ::-webkit-scrollbar-thumb:hover {

        background-color:#bbb;

    }
</style>
