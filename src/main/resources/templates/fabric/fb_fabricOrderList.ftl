<section class="panel panel-default">

    <div id="tableDiv">
        <table id="fabricOrderTable" class="table table-striped m-b-none ">
            <#if fabricOrderList??>
            <thead>
            <tr bgcolor="#ffcb99" style="color: black;">
                <th style="width: 30px">序号</th>
                <th style="width: 100px">订单编号</th>
                <th style="width: 100px">版单号</th>
                <th style="width: 100px">大货款号</th>
                <th style="width: 100px">面料名称</th>
                <th style="width: 100px">布种</th>
                <th style="width: 100px">面料编号</th>
                <th style="width: 100px">面料颜色</th>
                <th style="width: 100px">面料色号</th>
                <th style="width: 100px">SKC颜色</th>
                <th style="width: 100px">面料用途</th>
                <th style="width: 60px">单件用量</th>
                <th style="width: 60px">供应商</th>
                <th style="width: 60px">成衣件数</th>
                <th style="width: 60px">面料总量</th>
                <th style="width: 50px">单价</th>
                <th style="width: 50px">总价</th>
                <th style="width: 60px">交货日期</th>
                <th style="width: 60px">有效门幅</th>
                <th style="width: 50px">克重</th>
                <th style="width: 50px">纬斜</th>
                <th style="width: 50px">扭度</th>
                <th style="width: 90px">执行标准</th>
                <th style="width: 200px">备注</th>
                <th style="width: 120px">操作</th>
            </tr>
            </thead>
            <tbody>
                <#list fabricOrderList as vo>
                <tr>
                    <td>${vo_index+1}</td>
                    <td><#if vo.fabricOrderName??>${vo.fabricOrderName}</#if></td>
                    <td><#if vo.clothesVersionNumber??>${vo.clothesVersionNumber}</#if></td>
                    <td><#if vo.orderName??>${vo.orderName}</#if></td>
                    <td><#if vo.fabricName??>${vo.fabricName}</#if></td>
                    <td><#if vo.fabricTypeNumber??>${vo.fabricTypeNumber}</#if></td>
                    <td><#if vo.fabricNumber??>${vo.fabricNumber}</#if></td>
                    <td><#if vo.fabricColor??>${vo.fabricColor}</#if></td>
                    <td><#if vo.fabricColorNumber??>${vo.fabricColorNumber}</#if></td>
                    <td><#if vo.skcColor??>${vo.skcColor}</#if></td>
                    <td><#if vo.fabricUse??>${vo.fabricUse}</#if></td>
                    <td><#if vo.singleCount??>${vo.singleCount?string("#.##")}</#if></td>
                    <td><#if vo.fabricSupplier??>${vo.fabricSupplier}</#if></td>
                    <td><#if vo.orderCount??>${vo.orderCount?c}</#if></td>
                    <td><#if vo.fabricSumCount??>${vo.fabricSumCount?string("#.##")}</#if></td>
                    <td><#if vo.price??>${vo.price?string("#.##")}</#if></td>
                    <td><#if vo.total??>${vo.total?string("#.##")}</#if></td>
                    <td><#if vo.deliveryDate??>${vo.deliveryDate}</#if></td>
                    <td><#if vo.width??>${vo.width?string("#.##")}</#if></td>
                    <td><#if vo.weight??>${vo.weight?string("#.##")}</#if></td>
                    <td><#if vo.filling??>${vo.filling}</#if></td>
                    <td><#if vo.twist??>${vo.twist}</#if></td>
                    <td><#if vo.standard??>${vo.standard}</#if></td>
                    <td><#if vo.remark??>${vo.remark}</#if></td>
                    <#if vo.stateType == 1>
                        <td><a href="#" style="color:#3e8eea" onclick="deleteFabricOrder(${vo.fabricOrderID?c})">删除</a>&nbsp;&nbsp;<a href="#" style="color:red" onclick="updateFabricOrder(this,${vo.fabricOrderID?c})">修改</a>&nbsp;&nbsp;<font color='green'>已提交</font></td>
                    <#else>
                        <td><a href="#" style="color:#3e8eea" onclick="deleteFabricOrder(${vo.fabricOrderID?c})">删除</a>&nbsp;&nbsp;<a href="#" style="color:red" onclick="updateFabricOrder(this,${vo.fabricOrderID?c})">修改</a>&nbsp;&nbsp;<a href="#" style="color:red" onclick="commitFabricOrder(this,${vo.fabricOrderID?c})">提交</a></td>
                    </#if>
                </tr>
                </#list>
            </tbody>
            <tfoot>
            <tr>
                <th colspan="6" style="text-align:left"></th>
            </tr>
            </tfoot>
            <#else>
            <h4 style="text-align: center">无该状态的数据</h4>
            </#if>
        </table>
    </div>

</section>

<style>
    td {
        word-break:break-all;
    }

</style>