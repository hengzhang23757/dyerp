<#macro search>
<section id="content">
    <section class="vbox">
        <section class="scrollable padder water-mark-visible-ff">
            <div class="row">
                <div class="col-md-6" style="margin-left: 35px;height: 100px;text-align: center;vertical-align: middle;background:rgb(45, 202, 147);border-radius: 15px;">
                    <label style="color:white;font-size:35px;font-family: PingFangSC-Semibold,sans-serif;width:100%;height: 100%;vertical-align: middle;margin-top: 4%;">面料入仓</label>
                </div>
            </div>
            <div class="col-md-12" style="padding-top: 30px;">
                <section class="panel panel-default">
                    <header class="panel-heading font-bold">
                        <span style="font-family: PingFangSC-Semibold,sans-serif;color:black;font-size:22px">面料信息</span>
                    </header>
                    <table style="border-collapse:separate;border-spacing: 10px;width: 100%;" id="baseInfo">
                        <tr>
                            <td style="text-align: right;margin-bottom: 15px;width: 100px">
                                <label class="control-label" style="">版单号</label>
                            </td>
                            <td style="margin-bottom: 15px;width: 300px">
                                <input id="clothesVersionNumber" class="form-control" autocomplete="off" style="width: 100%">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;width: 100px">
                                <label class="control-label" style="">款号</label>
                            </td>
                            <td style="margin-bottom: 15px;width: 300px">
                                <select id="orderName" class="form-control" style="width: 100%"></select>
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;width: 100px">
                                <label class="control-label" style="">色组</label>
                            </td>
                            <td style="margin-bottom: 15px;width: 300px">
                                <select id="colorName" class="form-control" style="width: 100%"></select>
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;width: 100px">
                                <label class="control-label" style="">面料名</label>
                            </td>
                            <td style="margin-bottom: 15px;width: 300px">
                                <select id="fabricName" class="form-control" style="width: 100%"></select>
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;width: 100px">
                                <label class="control-label" style="">面料颜色</label>
                            </td>
                            <td style="margin-bottom: 15px;width: 300px">
                                <select id="fabricColor" class="form-control" style="width: 100%"></select>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right;margin-bottom: 15px;width: 100px">
                                <label class="control-label">缸号</label>
                            </td>
                            <td style="margin-bottom: 15px;width: 300px">
                                <select id="jarName" class="form-control" autocomplete="off" style="width: 100%"></select>
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;width: 100px">
                                <label class="control-label">类型</label>
                            </td>
                            <td style="margin-bottom: 15px;width: 300px">
                                <select id="operateType" class="form-control" style="width: 100%">
                                    <option value="余布入仓">余布入仓</option>
                                    <option value="余布整合">余布整合</option>
                                </select>
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;width: 100px">
                                <label class="control-label">重量</label>
                            </td>
                            <td style="margin-bottom: 15px;width: 300px">
                                <input id="weight" class="form-control" style="width: 100%">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;width: 100px">
                                <label class="control-label">卷数</label>
                            </td>
                            <td style="margin-bottom: 15px;width: 300px">
                                <input id="batch" class="form-control" style="width: 100%">
                            </td>
                        </tr>
                    </table>
                </section>

                <section class="panel panel-default" id="fromLocation" hidden>
                    <header class="panel-heading font-bold">
                        <span style="font-family: PingFangSC-Semibold,sans-serif;color:black;font-size:22px">当前仓位</span>
                    </header>
                    <div class="panel-body" style="text-align: left">
                        <div style="text-align: right;font-size:20px">
                            <div class="row" style="margin-left: 10px;margin-top: 10px;">
                                <div class="col-md-4">
                                    <span>仓位:</span>
                                    <select id="storageRow" class="form-control" style="background: rgb(248,248,248);width: 70%;">
                                        <option value="A">A</option>
                                        <option value="B">B</option>
                                        <option value="C">C</option>
                                        <option value="0">0</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section class="panel panel-default">
                    <header class="panel-heading font-bold">
                        <span style="font-family: PingFangSC-Semibold,sans-serif;color:black;font-size:22px">目的仓位</span>
                    </header>
                    <div class="panel-body" style="text-align: left">
                        <div style="text-align: right;font-size:20px">
                            <div class="row" style="margin-left: 10px;margin-top: 10px;">
                                <div class="col-md-4">
                                    <span>排次:</span>
                                    <select id="storageRow" class="form-control" style="background: rgb(248,248,248);width: 70%;">
                                        <option value="A">A</option>
                                        <option value="B">B</option>
                                        <option value="C">C</option>
                                        <option value="0">0</option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <span>卡位:</span>
                                    <select id="storageSite" class="form-control" style="background: rgb(248,248,248);width: 70%;">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12">12</option>
                                        <option value="13">13</option>
                                        <option value="14">14</option>
                                        <option value="15">15</option>
                                        <option value="16">16</option>
                                        <option value="17">17</option>
                                        <option value="18">18</option>
                                        <option value="19">19</option>
                                        <option value="20">20</option>
                                        <option value="21">21</option>
                                        <option value="22">22</option>
                                        <option value="23">23</option>
                                        <option value="24">24</option>
                                        <option value="25">25</option>
                                        <option value="26">26</option>
                                        <option value="27">27</option>
                                        <option value="28">28</option>
                                        <option value="0">0</option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <span>层数：</span>
                                    <select id="storageStand" class="form-control" style="background: rgb(248,248,248);width: 70%;">
                                        <option value="上">上</option>
                                        <option value="下">下</option>
                                        <option value="0">0</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <div class="col-md-12" style="text-align: center;margin-top: 50px">
                <button  class="btn btn-s-lg" style="border-radius: 5px;font-size:20px"  onclick="inStore()">提交</button>
            </div>
        </section>
    </section>
    <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen"
       data-target="#nav"></a>
</section>
<aside class="bg-light lter b-l aside-md hide" id="notes">
    <div class="wrapper">Notification</div>
</aside>
</section>
<script src="/js/common/jquery.blockUI.js" type="text/javascript" ></script>
<script src="/js/fabric/fabricInStore.js?t=${currentDate?c}"></script>

</#macro>

<style>
    button {
        background:rgb(45, 202, 147);
        opacity:0.86;
        color: white;

        font-family: PingFangSC-Semibold, sans-serif;
    }

    #orderNameTips{
        margin-left: 105px
    }
</style>

