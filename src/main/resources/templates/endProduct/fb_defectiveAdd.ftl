<#macro defectiveAdd>
    <div id="defectiveAdd" style="cursor:default;">
        <div class="layui-row">
            <table>
                <tr>
                    <td style="text-align: right;margin-bottom: 15px;">
                        <label class="layui-form-label" style="width: 100px; font-weight: bolder; font-size: large">类别</label>
                    </td>
                    <td style="margin-bottom: 15px;">
                        <select type="text" name="defectiveType" id="defectiveType" style="width: 200px" autocomplete="off" class="layui-input">
                            <option value="面料">次品-面料问题</option>
                            <option value="印花">次品-印花问题</option>
                            <option value="绣花">次品-绣花问题</option>
                            <option value="脏污">次品-脏污问题</option>
                            <option value="车缝">次品-车缝问题</option>
                            <option value="市场退残">次品-市场退残</option>
                            <option value="其他">次品-其他问题</option>
                            <option value="正品余数">正品余数</option>
                        </select>
                    </td>
                </tr>
            </table>
        </div>
        <table id="defectiveAddTable" name = "defectiveAddTable" style="background-color: white; color: black">
        </table>
    </div>
</#macro>

<style>
    .layui-col-md3 .layui-form-label {
        width:90px
    }
    .layui-col-md6 .layui-form-label {
        width:90px
    }
    .layui-col-md12 .layui-form-label {
        width:90px
    }

    #staffAdd .text {
        width: 8%;
        text-align: center;
    }

    #staffAdd .content {
        width: 17%;
    }

    #staffAdd input {
        border-radius: 0;
        border-color: white;
    }

    #staffAdd select {
        border-color: white;
    }

    #staffAdd td {
        border: solid black;
        border-width: 0px 1px 1px 0px;
    }

    #staffAdd table {
        border: solid black;
        border-width: 1px 0px 0px 1px;
    }
</style>