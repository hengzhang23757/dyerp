<#macro search>
    <section id="content">
        <section class="vbox">
            <section class="scrollable">
                <div class="layui-row">
                    <form class="layui-form" style="margin: 0 auto;padding-top: 3px;" lay-filter="baseInfo">
                        <table>
                            <tr>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label" style="width: 100px">月份</label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <input type="text" name="monthStr" id="monthStr" autocomplete="off" class="layui-input">
                                </td>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label" style="width: 100px">客户</label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <select type="text" name="customerName" id="customerName" autocomplete="off" class="layui-input"></select>
                                </td>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label" style="width: 100px">单号</label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <input type="text" name="clothesVersionNumber" id="clothesVersionNumber" autocomplete="off" placeholder="单号" class="layui-input">
                                </td>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label" style="width: 100px">款号</label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <input type="text" name="orderName" id="orderName" autocomplete="off" placeholder="款号" class="layui-input">
                                </td>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label" style="width: 100px">类型</label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <select type="text" name="type" id="type" autocomplete="off" class="layui-input">
                                        <option value="分款" selected>分款汇总</option>
                                        <option value="明细">出库明细</option>
                                        <option value="客户">客户对账</option>
                                    </select>
                                </td>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label"></label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <div class="layui-input-inline">
                                        <button class="layui-btn" lay-submit lay-filter="searchBeat">搜索</button>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
                <table id="reportTable" lay-filter="reportTable"></table>
                <script type="text/html" id="toolbarTop">
                    <div class="layui-btn-container">
                        <button class="layui-btn layui-btn-sm" lay-event="clearFilter">清除所有筛选</button>
                        <button class="layui-btn layui-btn-sm" lay-event="refresh">刷新</button>
                        <button class="layui-btn layui-btn-sm" lay-event="exportExcel">导出</button>
                        <button class="layui-btn layui-btn-sm" lay-event="increase">收款</button>
                        <button class="layui-btn layui-btn-sm" lay-event="decrease">扣款</button>
                    </div>
                </script>
                <script type="text/html" id="barTop">
                    <a class="layui-btn layui-btn-xs" lay-event="updatePrice">改价</a>
                </script>
                <script type="text/html" id="customerBarTop">
                    <a class="layui-btn layui-btn-xs layui-btn-danger" lay-event="delete">删除</a>
                </script>
            </section>
        </section>



    </section>
    <link rel="stylesheet" href="/css/layui.css" type="text/css">
    <link rel="stylesheet" href="/css/soulTable.css" type="text/css">
    <script src="/js/common/moment.min.js" type="text/javascript"></script>
    <script src="/js/endProduct/endProduct.js?t=${currentDate?c}"></script>

</#macro>

<style>

    ::-webkit-scrollbar-track-piece {

        background-color:#f8f8f8;

    }

    ::-webkit-scrollbar {

        width:15px;

        height:15px;

    }

    ::-webkit-scrollbar-thumb {

        background-color:#dddddd;

        background-clip:padding-box;

        min-height:28px;

    }

    ::-webkit-scrollbar-thumb:hover {

        background-color:#bbb;

    }
</style>