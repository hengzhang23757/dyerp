<#macro endProductDetail>
    <div id="endProductDetail" style="cursor:default;">
        <table id="endProductDetailTable" lay-filter="endProductDetailTable">
        </table>
    </div>
</#macro>

<style>
    .layui-col-md3 .layui-form-label {
        width:90px
    }
    .layui-col-md6 .layui-form-label {
        width:90px
    }
    .layui-col-md12 .layui-form-label {
        width:90px
    }

    #staffAdd .text {
        width: 8%;
        text-align: center;
    }

    #staffAdd .content {
        width: 17%;
    }

    #staffAdd input {
        border-radius: 0;
        border-color: white;
    }

    #staffAdd select {
        border-color: white;
    }

    #staffAdd td {
        border: solid black;
        border-width: 0px 1px 1px 0px;
    }

    #staffAdd table {
        border: solid black;
        border-width: 1px 0px 0px 1px;
    }
</style>