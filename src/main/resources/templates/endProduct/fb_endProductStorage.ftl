<#macro search>
    <section id="content">
        <section class="vbox">
            <section class="scrollable">
                <div class="layui-row">
                    <table>
                        <tr>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label" style="width: 100px">单号</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" name="clothesVersionNumber" id="clothesVersionNumber" autocomplete="off" class="layui-input">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label" style="width: 100px">款号</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" name="orderName" id="orderName" autocomplete="off" class="layui-input">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label"></label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <div class="layui-input-inline">
                                    <button class="layui-btn" lay-submit lay-filter="searchBeat">搜索</button>
                                </div>
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label"></label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <button class="layui-btn" onclick="addInStore()">正品入库</button>
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label"></label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <button class="layui-btn layui-btn-warm" onclick="addDefectiveInStore()">次品入库</button>
                            </td>
                        </tr>
                    </table>
                </div>
                <table id="reportTable" lay-filter="reportTable"></table>
                <script type="text/html" id="toolbarTop">
                    <div class="layui-btn-container">
                        <button class="layui-btn layui-btn-sm" lay-event="clearFilter">清除所有筛选</button>
                        <button class="layui-btn layui-btn-sm" lay-event="refresh">刷新</button>
                        <button class="layui-btn layui-btn-sm" lay-event="exportExcel">导出</button>
                        <button class="layui-btn layui-btn-sm" lay-event="init">初始</button>
                        <button class="layui-btn layui-btn-sm layui-btn-warm" lay-event="defectiveSummary">次品汇总</button>
                    </div>
                </script>
                <script type="text/html" id="barTop">
                    <a class="layui-btn layui-btn-xs" lay-event="outDetail">详情</a>
                    <a class="layui-btn layui-btn-xs" lay-event="addInStore">入库</a>
                    <a class="layui-btn layui-btn-xs" lay-event="outStore">出库</a>
                    <a class="layui-btn layui-btn-xs layui-btn-warm" lay-event="defectiveDetail">次详</a>
                    <a class="layui-btn layui-btn-xs layui-btn-warm" lay-event="defectiveIn">次入</a>
                    <a class="layui-btn layui-btn-xs layui-btn-warm" lay-event="defectiveOut">次出</a>
                </script>
            </section>
        </section>
        <#--<#include "fb_defectiveAdd.ftl">-->
        <#--<@defectiveAdd></@defectiveAdd>-->
    </section>
    <link rel="stylesheet" href="/css/layui.css" type="text/css">
    <link rel="stylesheet" href="/css/soulTable.css" type="text/css">
    <script src="/js/common/moment.min.js" type="text/javascript"></script>
    <script src="/js/endProduct/endProductStorage.js?t=${currentDate?c}"></script>

</#macro>

<style>
    button {
        background:rgb(45, 202, 147);
        opacity:0.86;
        color: white;
        font-family: PingFangSC-Semibold, sans-serif;
    }
    ::-webkit-scrollbar-track-piece {

        background-color:#f8f8f8;

    }

    ::-webkit-scrollbar {

        width:15px;

        height:15px;

    }

    ::-webkit-scrollbar-thumb {

        background-color:#dddddd;

        background-clip:padding-box;

        min-height:28px;

    }

    ::-webkit-scrollbar-thumb:hover {

        background-color:#bbb;

    }
</style>