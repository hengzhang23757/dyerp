<#macro search>
    <section id="content">
        <section class="vbox">
            <section class="scrollable">
                <div class="layui-row">
                    <form class="layui-form" style="margin: 0 auto;padding-top: 3px;" lay-filter="baseInfo">
                        <table>
                            <tr>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label">开始</label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <input type="text" name="from" id="from" autocomplete="off" class="layui-input">
                                </td>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label">结束</label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <input type="text" name="to" id="to" autocomplete="off" class="layui-input">
                                </td>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label">组别</label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <div id="xmGroup" class="xm-select-demo" style="width: 150px"></div>
                                </td>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label">单号</label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <input type="text" name="clothesVersionNumber" id="clothesVersionNumber" placeholder="请输入单号" class="layui-input">
                                </td>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label">款号</label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <input type="text" name="orderName" id="orderName" placeholder="请输入款号" class="layui-input">
                                </td>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label">类型</label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <select type="text" name="type" id="type">
                                        <option value="1">每天汇总</option>
                                        <option value="0">区间汇总</option>
                                    </select>
                                </td>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label">节点</label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <select type="text" name="procedureName" id="procedureName">
                                        <option value="中查">中查</option>
                                        <option value="查片">查片</option>
                                        <option value="挂片">挂片</option>
                                    </select>
                                </td>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label"></label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <div class="layui-input-inline">
                                        <button class="layui-btn" lay-submit lay-filter="searchBeat">搜索</button>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>

                <table id="reportTable" lay-filter="reportTable"></table>

                <script type="text/html" id="toolbarTop">
                    <div class="layui-btn-container">
                        <button class="layui-btn layui-btn-sm" lay-event="exportExcel">导出</button>
                    </div>
                </script>

                <script type="text/html" id="barTopOne">
                    <a class="layui-btn layui-btn-sm" lay-event="detail">详情</a>
                </script>

                <script type="text/html" id="barTopTwo">
                    <a class="layui-btn layui-btn-normal layui-btn-sm" lay-event="leakDetail">未齐查看</a>
                </script>

            </section>
        </section>
    </section>

    <link rel="stylesheet" href="/css/layui.css" type="text/css">
    <link rel="stylesheet" href="/css/soulTable.css" type="text/css">
    <script src="/js/common/xm-select.js?t=${currentDate?c}" type="text/javascript"></script>
    <script src="/js/common/moment.min.js" type="text/javascript"></script>
    <script src="/js/finance/groupWorkProgress.js?t=${currentDate?c}"></script>
</#macro>

<style>

    ::-webkit-scrollbar-track-piece {

        background-color:#f8f8f8;

    }

    ::-webkit-scrollbar {

        width:15px;

        height:15px;

    }

    ::-webkit-scrollbar-thumb {

        background-color:#dddddd;

        background-clip:padding-box;

        min-height:28px;

    }

    ::-webkit-scrollbar-thumb:hover {

        background-color:#bbb;

    }
</style>
