<#macro search>
    <section id="content">
        <section class="vbox">
            <section class="scrollable padder water-mark-visible-ff"  style="background-color: #f7f7f7;">
                <div class="col-md-12" style="padding-top: 5px;">
                    <section class="panel panel-default">
                        <div class="panel-body">
                            <table style="border-collapse:separate;border-spacing: 10px;width: 100%;" id="baseInfo">
                                <tr>
                                    <td style="text-align: right;margin-bottom: 15px;">
                                        <label class="control-label" style="">单号</label>
                                    </td>
                                    <td style="margin-bottom: 15px;">
                                        <input type="text" id="clothesVersionNumber" class="form-control" autocomplete="off" placeholder="请输入单号" style="width:150px">
                                    </td>
                                    <td style="text-align: right;margin-bottom: 15px;">
                                        <label class="control-label">款号</label>
                                    </td>
                                    <td style="margin-bottom: 15px;">
                                        <input type="text" id="orderName" class="form-control" autocomplete="off" placeholder="请输入款号" style="width:150px">
                                    </td>

                                    <td style="text-align: right;margin-bottom: 15px;">
                                        <label class="control-label" style="">客供单价</label>
                                    </td>
                                    <td style="margin-bottom: 15px;">
                                        <input id="pieceCost" class="form-control" autocomplete="off" style="width:150px" onkeyup="onlyNumber(this);">
                                    </td>
                                    <td style="text-align: right;margin-bottom: 15px;">
                                    </td>
                                    <td style="margin-bottom: 15px;">
                                        <button  class="btn" style="border-radius: 5px;color:white;font-family: PingFangSC-Semibold, sans-serif;width:100px;"  onclick="search()">查找</button>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </section>
                </div>
                <div class="row" id="detailDiv" hidden>
                    <div class="panel panel-success col-md-12">
                        <div class="panel-heading">成本明细</div>
                        <table class="table" id="costCountTable">
                        </table>
                        <div class="panel-footer panel-primary">裁床、车缝、后整均含有产值奖</div>
                    </div>
                </div>
                <div class="row" id="figureDiv" hidden>
                    <div class="panel panel-success col-md-12">
                        <div class="panel-heading">成本比例图</div>
                        <div id="container" style="height: 500px"></div>
                    </div>
                </div>
            </section>
        </section>
        <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen"
           data-target="#nav"></a>
    </section>
    <aside class="bg-light lter b-l aside-md hide" id="notes">
        <div class="wrapper">Notification</div>
    </aside>
    </section>
    <link rel="stylesheet" href="/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="/css/b.tabs.css" type="text/css">
    <link rel="stylesheet" href="/css/bootstrap-select.min.css" type="text/css">
    <script src="/js/common/laydate.js" type="text/javascript" ></script>
    <script src="/js/common/jquery.blockUI.js" type="text/javascript" ></script>
    <script src="/js/common/b.tabs.js?t=${currentDate?c}"></script>
    <script src="/js/common/moment.min.js" type="text/javascript"></script>
    <script src="/js/common/table2excel.js" type="text/javascript"></script>
    <script src="/js/common/bootstrap-select.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/echarts/dist/echarts.min.js"></script>
    <script src="/js/finance/costAccount.js?t=${currentDate?c}"></script>
</#macro>

<style>
    button {
        background:rgb(45, 202, 147);
        opacity:0.86;
        color: white;
        font-family: PingFangSC-Semibold, sans-serif;
    }
</style>
