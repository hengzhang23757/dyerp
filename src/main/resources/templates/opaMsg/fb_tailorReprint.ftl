<#macro search>
    <section id="content">
        <section class="vbox">
            <section class="scrollable">

                <div class="layui-row">
                    <form class="layui-form" style="margin: 0 auto;padding-top: 3px;" lay-filter="baseInfo">
                        <table>
                            <tr>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label">单号</label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <input type="text" autocomplete="off" id="clothesVersionNumber" class="layui-input">
                                </td>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label">款号</label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <input type="text" autocomplete="off" id="orderName" class="layui-input">
                                </td>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label">床号</label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <select type="text" id="bedNumber" autocomplete="off" class="layui-input"></select>
                                </td>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label">扎号</label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <input type="text" autocomplete="off" id="packageNumber" class="layui-input">
                                </td>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label"></label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <div class="layui-input-inline">
                                        <button class="layui-btn" lay-submit lay-filter="searchBeat">搜索</button>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>

                <table id="printTailorTable" lay-filter="printTailorTable"></table>


                <script type="text/html" id="toolbarTop">
                    <div class="layui-btn-container">
                        <button class="layui-btn layui-btn-sm" lay-event="clearFilter">清除所有筛选</button>
                        <button class="layui-btn layui-btn-sm" lay-event="printTailor">打印</button>
                        <button class="layui-btn layui-btn-sm" lay-event="send">发分厂</button>
                    </div>
                </script>

                <script type="text/html" id="barTop">
                    <a class="layui-btn layui-btn-xs" lay-event="layerCount">改数量</a>
                    <a class="layui-btn layui-btn-xs" lay-event="weight">改重量</a>
                    <a class="layui-btn layui-btn-xs" lay-event="group">改组名</a>
                </script>


            </section>
        </section>
    </section>
    <link rel="stylesheet" href="/css/soulTable.css" type="text/css">
    <link rel="stylesheet" href="/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="/css/b.tabs.css" type="text/css">
    <script src="/js/common/jquery.blockUI.js" type="text/javascript" ></script>
    <script src="/js/common/b.tabs.js?t=${currentDate?c}"></script>
    <script src="/js/common/LodopFuncs.js" type="text/javascript" ></script>
    <script src="/js/opaMsg/tailorReprint.js?t=${currentDate?c}"></script>

</#macro>

<style>

    ::-webkit-scrollbar-track-piece {

        background-color:#f8f8f8;

    }

    ::-webkit-scrollbar {

        width:15px;

        height:15px;

    }

    ::-webkit-scrollbar-thumb {

        background-color:#dddddd;

        background-clip:padding-box;

        min-height:28px;

    }

    ::-webkit-scrollbar-thumb:hover {

        background-color:#bbb;

    }

</style>
