<#macro entities>
    <div id="updateGroup" style="display: none;cursor:default">
        <div style="overflow-x: auto;overflow-y:auto;">
            <div style="text-align: right;margin-bottom:20px">
                <a id="updateGroupNo" href="#" style="font-size:20px"><i class="fa fa-times icon" style="color: rgb(182,182,182)"></i></a>
            </div>
            <table style="border-collapse:separate;border-spacing: 10px;width: 100%;">
                <tr>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">原始组名</label>
                    </td>
                    <td>
                        <input id="groupName1" class="form-control" autocomplete="off" style="width: 100%;margin-bottom: 15px;">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">修改成</label>
                    </td>
                    <td>
                        <select id="updateToGroup" class="form-control" autocomplete="off" style="margin-bottom: 15px;width: 100%;">
                            <option value="">选择组名</option>
                            <option value="A组">A组</option>
                            <option value="B组">B组</option>
                            <option value="C组">C组</option>
                            <option value="D组">D组</option>
                            <option value="E组">E组</option>
                            <option value="F组">F组</option>
                            <option value="G组">G组</option>
                            <option value="绅维纪">绅维纪</option>
                            <option value="抄更">抄更</option>
                        </select>
                    </td>
                </tr>

            </table>
        </div>
        <div class="col-sm-12" style="padding-top: 10px">
            <button id="updateGroupYes" class="btn btn-s-lg" style="border-radius: 5px;text-align: center;color:white;font-family: PingFangSC-Semibold;">保存</button>
        </div>
    </div>
</#macro>