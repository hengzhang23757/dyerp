<#macro search>
    <section id="content">
        <section class="vbox">
            <section class="scrollable padder water-mark-visible-ff"  style="background-color: #f7f7f7;">
                <div class="col-md-12" style="padding-top: 10px;">
                    <section class="panel panel-default">
                        <div class="panel-body">

                            <table style="border-collapse:separate;border-spacing: 10px;width: 100%;" id="baseInfo">
                                <tr>
                                    <td style="text-align: right;margin-bottom: 15px;">
                                        <label class="control-label" style="">单号</label>
                                    </td>
                                    <td style="margin-bottom: 15px;">
                                        <input type="text" id="clothesVersionNumber" class="form-control" placeholder="请输入单号" autocomplete="off" style="width:200px">
                                    </td>
                                    <td style="text-align: right;margin-bottom: 15px;">
                                        <label class="control-label" style="">款号</label>
                                    </td>
                                    <td style="margin-bottom: 15px;">
                                        <input type="text" id="orderName" class="form-control" placeholder="请输入款号" autocomplete="off" style="width:200px">
                                    </td>
                                    <td style="text-align: right;margin-bottom: 15px;">
                                        <label class="control-label" style="">床号</label>
                                    </td>
                                    <td style="margin-bottom: 15px;">
                                        <select id="bedNumber" class="form-control" style="width:200px;"></select>
                                    </td>
                                    <td style="text-align: right;margin-bottom: 15px;">
                                    </td>
                                    <td style="margin-bottom: 15px;">
                                        <button  class="btn" style="border-radius: 5px;color:white;font-family: PingFangSC-Semibold, sans-serif;width:100px;"  onclick="searchTailorInfo()">查找</button>
                                    </td>
                                </tr>
                            </table>
                            <div>
                                <button  class="btn" style="border-radius: 5px;color:white;font-family: PingFangSC-Semibold, sans-serif;background:#ec6c62;display:none" id="delBtn"  onclick="batchDelete()">删除</button>
                            </div>
                            <div style="text-align: center;font-family: PingFangSC-Semibold;">
                                <table id="tailorInfoTable" class="table table-striped table-hover">
                                </table>
                            </div>

                        </div>
                    </section>
                </div>
            </section>
        </section>
        <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen"
           data-target="#nav"></a>
    </section>
    <aside class="bg-light lter b-l aside-md hide" id="notes">
        <div class="wrapper">Notification</div>
    </aside>
    </section>
    <link rel="stylesheet" href="/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="/css/b.tabs.css" type="text/css">
    <script src="/js/common/b.tabs.js?t=${currentDate?c}"></script>
    <script src="/js/common/handsontable.full.min.js" type="text/javascript" ></script>
    <script src="/js/common/zh-CN.js" type="text/javascript" ></script>
    <script src="/js/common/jquery.blockUI.js" type="text/javascript" ></script>
    <script src="/js/opaMsg/oneTailorInfo.js?t=${currentDate?c}"></script>

</#macro>

<style>
    button {
        background:rgb(45, 202, 147);
        opacity:0.86;
        color: white;
        font-family: PingFangSC-Semibold, sans-serif;
    }

    #orderNameTips{
        margin-left: 60px
    };
</style>
