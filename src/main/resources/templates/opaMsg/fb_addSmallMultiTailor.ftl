<!DOCTYPE html>
<html>
<head>
    <#include "../feedback/fb_script.ftl">
    <@script> </@script>
    <link rel="stylesheet" href="/css/selectize.bootstrap3.css" type="text/css">
    <script src="/js/common/selectize.js" type="text/javascript"></script>
    <script src="/js/opaMsg/addSmallMultiTailor.js?t=${currentDate?c}"></script>
</head>

<body>
<input  type="hidden" value="${basePath}"  id="basePath"/>
<input  type="hidden" value="${type}"  id="type"/>
<div class="row">
    <div class="col-md-12">
        <section class="panel panel-default">
            <header class="panel-heading font-bold">
                <span class="label bg-success pull-right"></span>基本信息
            </header>
            <table style="border-collapse:separate;border-spacing: 10px;width: 100%;" id="baseInfo">
                <tr>

                    <td style="text-align: right;margin-bottom: 15px;">
                        <label class="control-label" style="">单号</label>
                    </td>
                    <td style="margin-bottom: 15px;">
                        <input id="clothesVersionNumber" class="form-control" autocomplete="off">
                    </td>
                    <td style="text-align: right;margin-bottom: 15px;">
                        <label class="control-label" style="">款号</label>
                    </td>
                    <td style="margin-bottom: 15px;">
                        <input id="orderName" class="form-control" autocomplete="off">
                    </td>
                    <td style="text-align: right;margin-bottom: 15px;">
                        <label class="control-label" style="">客户</label>
                    </td>
                    <td style="margin-bottom: 15px;">
                        <input id="customerName" class="form-control">
                    </td>

                    <td style="text-align: right;margin-bottom: 15px;">
                        <label class="control-label" style="">床号</label>
                    </td>
                    <td style="margin-bottom: 15px;">
                        <input id="bedNumber" class="form-control" oninput="value=value.replace(/[^\d]/g,'')">
                    </td>
                </tr>
            </table>
        </section>

        <section class="panel panel-default">
            <header class="panel-heading font-bold">
                <span class="label bg-success pull-right"></span>部位选择<button class="btn btn-s-xs" style="border-radius: 5px;margin-left: 10px" onclick="loadPartName()">刷新</button>
            </header>
            <div style="margin-top: 10px;margin-left: 20px;height: 40px;">
                <select id="partName" multiple style="width:90%" >
                </select>
            </div>
        </section>
        <section class="panel panel-default" id="numberAddDiv">
            <header class="panel-heading font-bold">
                <span class="label bg-success pull-right"></span>数量录入
            </header>
            <div style="overflow-x:auto;">
                <table id="numberTable" style="overflow-x:auto;margin-top: 10px;margin-bottom: 10px;margin-left: 20px;white-space:nowrap;border-collapse:separate; border-spacing:0px 10px;">
                    <tr>
                        <td style="width: 20px"></td>
                        <td class="tdWidth"></td>
                        <td class="tdWidth">
                            <select id="sizeName0" name="sizeName" class="form-control">
                                <option>选择尺码</option>
                            </select>
                            <button name="addSize" class="btn btn-s-xs" style="min-width: 12px;line-height: 0.5;padding: 7px;"  onclick="addSize(this)"><i class="layui-icon layui-icon-addition"></i></button>
                            <button name="delSize" class="btn btn-s-xs" style="min-width: 12px;line-height: 0.5;padding: 7px;background-color: rgb(236, 108, 98);display: none"  onclick="delSize(this)"><i class="layui-icon layui-icon-subtraction"></i></button>
                        </td>
                        <td class="tdWidth">
                            <select id="sizeName1" name="sizeName" class="form-control">
                                <option>选择尺码</option>
                            </select>
                            <button name="addSize" class="btn btn-s-xs" style="min-width: 12px;line-height: 0.5;padding: 7px;display: none"  onclick="addSize(this)"><i class="layui-icon layui-icon-addition"></i></button>
                            <button name="delSize" class="btn btn-s-xs" style="min-width: 12px;line-height: 0.5;padding: 7px;background-color: rgb(236, 108, 98);"  onclick="delSize(this)"><i class="layui-icon layui-icon-subtraction"></i></button>
                        </td>
                        <td class="tdWidth">
                            <select id="sizeName2" name="sizeName" class="form-control">
                                <option>选择尺码</option>
                            </select>
                            <button name="addSize" class="btn btn-s-xs" style="min-width: 12px;line-height: 0.5;padding: 7px;display: none"  onclick="addSize(this)"><i class="layui-icon layui-icon-addition"></i></button>
                            <button name="delSize" class="btn btn-s-xs" style="min-width: 12px;line-height: 0.5;padding: 7px;background-color: rgb(236, 108, 98);"  onclick="delSize(this)"><i class="layui-icon layui-icon-subtraction"></i></button>
                        </td>
                    </tr>
                    <tr name="numberTr">
                        <td>
                            <span style="font-weight: 700;color:black" name="orderOfLayer">1</span>
                        </td>
                        <td>
                            <select name="colorName" class="form-control">
                                <option>选择颜色</option>
                            </select>
                        </td>
                        <td>
                            <input name="number" class="form-control" autocomplete="off" oninput="value=value.replace(/[^\d]/g,'')" onkeydown="moveCursor(event,this)">
                        </td>
                        <td>
                            <input name="number" class="form-control" autocomplete="off" oninput="value=value.replace(/[^\d]/g,'')" onkeydown="moveCursor(event,this)">
                        </td>
                        <td>
                            <input name="number" class="form-control" autocomplete="off" oninput="value=value.replace(/[^\d]/g,'')" onkeydown="moveCursor(event,this)">
                        </td>
                        <td>
                            <button name="addNumber" class="btn btn-s-xs" style="min-width: 12px;line-height: 0.5;padding: 7px;"  onclick="addNumber(this)">
                                <i class="layui-icon layui-icon-addition"></i>
                            </button>
                            <button name="delNumber" class="btn btn-s-xs" style="min-width: 12px;line-height: 0.5;padding: 7px;background-color: rgb(236, 108, 98);display: none"  onclick="delNumber(this)">
                                <i class="layui-icon layui-icon-subtraction"></i>
                            </button>
                        </td>
                    </tr>
                </table>
            </div>
        </section>
    </div>
</div>
<div class="col-md-12" style="text-align: center;margin-top: auto">
    <button  class="btn btn-s-lg" style="border-radius: 5px;"  onclick="addOrder()">生成</button>
</div>
</body>
</html>
<style>

    .tdWidth {
        min-width: 150px;
        max-width: 150px;
    }

    label {
        font-family: PingFangSC-Semibold;
    }

    ::-webkit-scrollbar-track-piece {

        background-color:#f8f8f8;

    }

    ::-webkit-scrollbar {

        width:15px;

        height:15px;

    }

    ::-webkit-scrollbar-thumb {

        background-color:#dddddd;

        background-clip:padding-box;

        min-height:28px;

    }

    ::-webkit-scrollbar-thumb:hover {

        background-color:#bbb;

    }

</style>
