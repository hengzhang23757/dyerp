<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>恒达精益管理</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <link rel="stylesheet" href="/layuiadmin/layui/css/layui.css" media="all">
  <link rel="stylesheet" href="/layuiadmin/style/admin.css" media="all">
</head>
<body>

<div class="layui-fluid">
  <div class="layui-row layui-col-space15">
      <div class="layui-col-md8">
        <div class="layui-row layui-col-space15">
          <div class="layui-col-md12">
            <div class="layui-card">
              <div class="layui-card-header">创建消息</div>
              <div class="layui-card-body">
                <form class="layui-form" action="">
                  <div class="layui-form-item">
                    <label class="layui-form-label">标题</label>
                    <div class="layui-input-block">
                      <input type="text" name="title" required  lay-verify="required" placeholder="请输入标题" autocomplete="off" class="layui-input">
                    </div>
                  </div>
                  <div class="layui-form-item">
                    <label class="layui-form-label">选择类型</label>
                    <div class="layui-input-block">
                      <select name="messageType" lay-verify="required">
                        <option value=""></option>
                        <option value="通知">通知</option>
                        <option value="私信">私信</option>
                        <option value="跟单">跟单</option>
                        <option value="面料">面料</option>
                        <option value="裁床">裁床</option>
                        <option value="车缝">车缝</option>
                        <option value="后整">后整</option>
                      </select>
                    </div>
                  </div>
                  <div class="layui-form-item layui-form-text">
                    <label class="layui-form-label">文本域</label>
                    <div class="layui-input-block">
                      <textarea name="desc" placeholder="请输入内容" class="layui-textarea"></textarea>
                    </div>
                  </div>
                  <fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px; margin-left: 50px">
                    <legend>拖拽上传图片(如果需要)</legend>
                  </fieldset>

                  <div class="layui-upload-drag" id="test10" style="margin-bottom: 20px; margin-left: 50px">
                    <i class="layui-icon"></i>
                    <p>点击上传，或将文件拖拽到此处</p>
                    <div class="layui-hide" id="uploadDemoView">
                      <hr>
                      <img src="" alt="上传成功后渲染" style="max-width: 196px">
                    </div>
                  </div>
                  <div class="layui-form-item">
                    <div class="layui-input-block">
                      <button class="layui-btn" lay-submit lay-filter="formDemo">立即提交</button>
                      <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="layui-col-md4">
        <div class="layui-card" id="selectUser">
          <div class="layui-card-header">要推送给谁</div>
          <div class="layui-card-body">
            <div id="departmentUser" class="demo-tree-more"></div>
          </div>
        </div>
      </div>
  </div>
</div>
<style>

  #first i {
    display: none
  }

  ::-webkit-scrollbar-track-piece {

    background-color:#f8f8f8;

  }

  ::-webkit-scrollbar {

    width:15px;

    height:15px;

  }

  ::-webkit-scrollbar-thumb {

    background-color:#dddddd;

    background-clip:padding-box;

    min-height:28px;

  }

  ::-webkit-scrollbar-thumb:hover {

    background-color:#bbb;

  }

</style>
<script src="/layuiadmin/layui/layui.js?t=1"></script>
<script src="/js/about/about.js?t=${currentDate?c}"></script>
<script src="/js/common/moment.min.js" type="text/javascript"></script>
</body>
</html>
