<#macro search>
    <div style="overflow: scroll; height: 2000px; width: 100%">
        <div class="layui-fluid">
            <div class="layui-row layui-col-space15" id="contentDiv">

                <div class="layui-col-md8">
                    <div class="layui-row layui-col-space15">
                        <div class="layui-col-md12">
                            <div class="layui-card">
                                <div class="layui-card-header" style="font-size: large">创建消息</div>
                                <div class="layui-card-body">
                                    <form class="layui-form" action="" id="messageForm">
                                        <div class="layui-form-item">
                                            <label class="layui-form-label">标题</label>
                                            <div class="layui-input-block">
                                                <input type="text" name="title" id="title" required  lay-verify="required" placeholder="请输入标题" autocomplete="off" class="layui-input">
                                            </div>
                                        </div>
                                        <table>
                                            <tr>
                                                <td>
                                                    <div class="layui-form-item">
                                                        <label class="layui-form-label">类型</label>
                                                        <div class="layui-input-block">
                                                            <select name="messageType" lay-verify="required" id="messageType">
                                                                <option value=""></option>
                                                                <option value="通知">通知</option>
                                                                <option value="私信">私信</option>
                                                                <option value="跟单">跟单</option>
                                                                <option value="面料">面料</option>
                                                                <option value="裁床">裁床</option>
                                                                <option value="车缝">车缝</option>
                                                                <option value="后整">后整</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="layui-form-item">
                                                        <label class="layui-form-label">单号</label>
                                                        <div class="layui-input-block">
                                                            <input autocomplete="off" class="layui-input" id="clothesVersionNumber">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="layui-form-item">
                                                        <label class="layui-form-label">款号</label>
                                                        <div class="layui-input-block">
                                                            <input autocomplete="off" class="layui-input" id="orderName">
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                        <div id="div1" style="margin-bottom: 10px"></div>
                                        <div class="layui-form-item">
                                            <div class="layui-input-block" align="center">
                                                <button class="layui-btn" lay-submit lay-filter="commitData">立即提交</button>
                                                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="layui-col-md4">
                    <div class="layui-card" id="selectUser">
                        <div class="layui-card-header" style="font-size: large">选择收件人
                            <div class="demoTable" style="margin:0 30px 0 20px;float: right">
                                <div class="layui-inline">
                                    <input type="text" class="layui-input" id="tree_name" autocomplete="on" />
                                </div>
                                <button class="layui-btn" id="btn_query">搜索</button>
                            </div>
                        </div>
                        <div class="layui-card-body">
                            <div id="departmentUser" class="demo-tree-more"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <style>
        #first i {
            display: none
        }

        .demo-tree-more *{
            -webkit-box-sizing:content-box !important;
            -moz-box-sizing:content-box !important;
            box-sizing:content-box !important;
        }
        ::-webkit-scrollbar-track-piece {

            background-color:#f8f8f8;

        }

        ::-webkit-scrollbar {

            width:15px;

            height:15px;

        }

        ::-webkit-scrollbar-thumb {

            background-color:#dddddd;

            background-clip:padding-box;

            min-height:28px;

        }

        ::-webkit-scrollbar-thumb:hover {

            background-color:#bbb;

        }
    </style>
    <link rel="stylesheet" href="/layuiadmin/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/layuiadmin/style/admin.css" media="all">
    <script src="/js/about/about.js?t=${currentDate?c}" type="text/javascript"></script>
    <script src="/js/common/moment.min.js" type="text/javascript"></script>
    <script src="/js/about/crypto.js" type="text/javascript"></script>
    <script src="/js/about/hmac.js" type="text/javascript"></script>
    <script src="/js/about/sha1.js" type="text/javascript"></script>
    <script src="/js/about/base64.js" type="text/javascript"></script>
    <script src="/js/about/plupload.full.min.js" type="text/javascript"></script>
    <script src="/js/about/wangUpload.js?t=${currentDate?c}" type="text/javascript"></script>
    <script type="text/javascript" src="//unpkg.com/wangeditor/dist/wangEditor.min.js"></script>
    <script src="/js/about/aliyun-oss-sdk-4.4.4.min.js"></script>
</#macro>