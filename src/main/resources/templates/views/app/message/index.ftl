<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>消息中心</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <link rel="stylesheet" href="/layuiadmin/layui/css/layui.css" media="all">
  <link rel="stylesheet" href="/layuiadmin/style/admin.css" media="all">
</head>
<body>

  <input hidden id="loginUserName" value="${userName!""}">
  <div class="layui-fluid" id="LAY-app-message">
    <div class="layui-card">
      <div class="layui-tab layui-tab-brief">
        <ul class="layui-tab-title">
          <li><a href="javascript:;" lay-href="/erp/messageWrite">写信</a></li>
          <li>已发<span class="layui-badge" id="messageSended"></span></li>
          <li class="layui-this">全部消息<span class="layui-badge" id="messageAll"></span></li>
          <li>通知<span class="layui-badge" id="messageNotice"></span></li>
          <li>私信<span class="layui-badge" id="messageDirect"></span></li>
          <li>跟单<span class="layui-badge" id="messageOrder"></span></li>
          <li>面料<span class="layui-badge" id="messageFabric"></span></li>
          <li>辅料<span class="layui-badge" id="messageAccessory"></span></li>
          <li>IE<span class="layui-badge" id="messageIe"></span></li>
          <li>裁床<span class="layui-badge" id="messageCut"></span></li>
          <li>车缝<span class="layui-badge" id="messageSew"></span></li>
          <li>后整<span class="layui-badge" id="messageFinish"></span></li>
          <li>财务<span class="layui-badge" id="messageFinance"></span></li>
        </ul>
        <div class="layui-tab-content">

          <div class="layui-tab-item"></div>
          <div class="layui-tab-item">
            <table id="LAY-app-message-send" lay-filter="LAY-app-message-send"></table>
          </div>

          <div class="layui-tab-item layui-show">
            <div class="LAY-app-message-btns" style="margin-bottom: 10px;">
              <#--<button class="layui-btn layui-btn-primary layui-btn-sm" data-type="all" data-events="del">删除</button>-->
              <button class="layui-btn layui-btn-primary layui-btn-sm" data-type="all" data-events="ready">标记已读</button>
              <#--<button class="layui-btn layui-btn-primary layui-btn-sm" data-type="all" data-events="readyAll">全部已读</button>-->
            </div>
            
            <table id="LAY-app-message-all" lay-filter="LAY-app-message-all"></table>
          </div>

          <div class="layui-tab-item">
          
            <div class="LAY-app-message-btns" style="margin-bottom: 10px;">
              <#--<button class="layui-btn layui-btn-primary layui-btn-sm" data-type="notice" data-events="del">删除</button>-->
              <button class="layui-btn layui-btn-primary layui-btn-sm" data-type="notice" data-events="ready">标记已读</button>
              <#--<button class="layui-btn layui-btn-primary layui-btn-sm" data-type="notice" data-events="readyAll">全部已读</button>-->
            </div>
            
            <table id="LAY-app-message-notice" lay-filter="LAY-app-message-notice"></table>
          </div>
          <div class="layui-tab-item">
          
            <div class="LAY-app-message-btns" style="margin-bottom: 10px;">
              <#--<button class="layui-btn layui-btn-primary layui-btn-sm" data-type="direct" data-events="del">删除</button>-->
              <button class="layui-btn layui-btn-primary layui-btn-sm" data-type="direct" data-events="ready">标记已读</button>
              <#--<button class="layui-btn layui-btn-primary layui-btn-sm" data-type="direct" data-events="readyAll">全部已读</button>-->
            </div>
            
            <table id="LAY-app-message-direct" lay-filter="LAY-app-message-direct"></table>
          </div>
          <div class="layui-tab-item">

            <div class="LAY-app-message-btns" style="margin-bottom: 10px;">
              <#--<button class="layui-btn layui-btn-primary layui-btn-sm" data-type="direct" data-events="del">删除</button>-->
              <button class="layui-btn layui-btn-primary layui-btn-sm" data-type="order" data-events="ready">标记已读</button>
              <#--<button class="layui-btn layui-btn-primary layui-btn-sm" data-type="direct" data-events="readyAll">全部已读</button>-->
            </div>

            <table id="LAY-app-message-order" lay-filter="LAY-app-message-order"></table>
          </div>
          <div class="layui-tab-item">

            <div class="LAY-app-message-btns" style="margin-bottom: 10px;">
              <#--<button class="layui-btn layui-btn-primary layui-btn-sm" data-type="direct" data-events="del">删除</button>-->
              <button class="layui-btn layui-btn-primary layui-btn-sm" data-type="fabric" data-events="ready">标记已读</button>
              <#--<button class="layui-btn layui-btn-primary layui-btn-sm" data-type="direct" data-events="readyAll">全部已读</button>-->
            </div>

            <table id="LAY-app-message-fabric" lay-filter="LAY-app-message-fabric"></table>
          </div>

          <div class="layui-tab-item">

            <div class="LAY-app-message-btns" style="margin-bottom: 10px;">
              <#--<button class="layui-btn layui-btn-primary layui-btn-sm" data-type="direct" data-events="del">删除</button>-->
              <button class="layui-btn layui-btn-primary layui-btn-sm" data-type="accessory" data-events="ready">标记已读</button>
              <#--<button class="layui-btn layui-btn-primary layui-btn-sm" data-type="direct" data-events="readyAll">全部已读</button>-->
            </div>

            <table id="LAY-app-message-accessory" lay-filter="LAY-app-message-accessory"></table>
          </div>

          <div class="layui-tab-item">

            <div class="LAY-app-message-btns" style="margin-bottom: 10px;">
              <#--<button class="layui-btn layui-btn-primary layui-btn-sm" data-type="direct" data-events="del">删除</button>-->
              <button class="layui-btn layui-btn-primary layui-btn-sm" data-type="IE" data-events="ready">标记已读</button>
              <#--<button class="layui-btn layui-btn-primary layui-btn-sm" data-type="direct" data-events="readyAll">全部已读</button>-->
            </div>

            <table id="LAY-app-message-ie" lay-filter="LAY-app-message-ie"></table>
          </div>

          <div class="layui-tab-item">

            <div class="LAY-app-message-btns" style="margin-bottom: 10px;">
              <#--<button class="layui-btn layui-btn-primary layui-btn-sm" data-type="direct" data-events="del">删除</button>-->
              <button class="layui-btn layui-btn-primary layui-btn-sm" data-type="cut" data-events="ready">标记已读</button>
              <#--<button class="layui-btn layui-btn-primary layui-btn-sm" data-type="direct" data-events="readyAll">全部已读</button>-->
            </div>

            <table id="LAY-app-message-cut" lay-filter="LAY-app-message-cut"></table>
          </div>
          <div class="layui-tab-item">

            <div class="LAY-app-message-btns" style="margin-bottom: 10px;">
              <#--<button class="layui-btn layui-btn-primary layui-btn-sm" data-type="direct" data-events="del">删除</button>-->
              <button class="layui-btn layui-btn-primary layui-btn-sm" data-type="sew" data-events="ready">标记已读</button>
              <#--<button class="layui-btn layui-btn-primary layui-btn-sm" data-type="direct" data-events="readyAll">全部已读</button>-->
            </div>

            <table id="LAY-app-message-sew" lay-filter="LAY-app-message-sew"></table>
          </div>
          <div class="layui-tab-item">

            <div class="LAY-app-message-btns" style="margin-bottom: 10px;">
              <#--<button class="layui-btn layui-btn-primary layui-btn-sm" data-type="direct" data-events="del">删除</button>-->
              <button class="layui-btn layui-btn-primary layui-btn-sm" data-type="finish" data-events="ready">标记已读</button>
              <#--<button class="layui-btn layui-btn-primary layui-btn-sm" data-type="direct" data-events="readyAll">全部已读</button>-->
            </div>

            <table id="LAY-app-message-finish" lay-filter="LAY-app-message-finish"></table>
          </div>

          <div class="layui-tab-item">

            <div class="LAY-app-message-btns" style="margin-bottom: 10px;">
              <#--<button class="layui-btn layui-btn-primary layui-btn-sm" data-type="direct" data-events="del">删除</button>-->
              <button class="layui-btn layui-btn-primary layui-btn-sm" data-type="finance" data-events="ready">标记已读</button>
              <#--<button class="layui-btn layui-btn-primary layui-btn-sm" data-type="direct" data-events="readyAll">全部已读</button>-->
            </div>

            <table id="LAY-app-message-finance" lay-filter="LAY-app-message-finance"></table>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script type="text/html" id="barDemo">
    <a class="layui-btn layui-btn-xs" lay-event="detail">详情</a>
    <#--<a class="layui-btn layui-btn-xs" lay-event="direct">录入</a>-->
  </script>

  <script src="/layuiadmin/layui/layui.js"></script>
  <script src="/js/common/jquery.js"></script>
  <script src="/js/message/index.js?t=${currentDate?c}"></script>
</body>
</html>