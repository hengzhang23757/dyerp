<!DOCTYPE html>
<html lang="en" class="app">
<head>
  <meta charset="utf-8">
  <title>恒达精益管理</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <link rel="stylesheet" href="/layuiadmin/layui/css/layui.css" media="all">
  <link rel="stylesheet" href="/layuiadmin/style/admin.css" media="all">
</head>
<body class="layui-layout-body">
  
  <div id="LAY_app">
    <div class="layui-layout layui-layout-admin">
      <div class="layui-header">
        <!-- 头部区域 -->
        <ul class="layui-nav layui-layout-left">
          <li class="layui-nav-item layadmin-flexible" lay-unselect>
            <a href="javascript:;" layadmin-event="flexible" title="侧边伸缩">
              <i class="layui-icon layui-icon-shrink-right" id="LAY_app_flexible"></i>
            </a>
          </li>
          <!--<li class="layui-nav-item layui-hide-xs" lay-unselect>-->
            <!--<a href="http://www.layui.com/admin/" target="_blank" title="前台">-->
              <!--<i class="layui-icon layui-icon-website"></i>-->
            <!--</a>-->
          <!--</li>-->
          <li class="layui-nav-item" lay-unselect>
            <a href="javascript:;" layadmin-event="refresh" title="刷新">
              <i class="layui-icon layui-icon-refresh-3"></i>
            </a>
          </li>
          <li class="layui-nav-item layui-hide-xs" lay-unselect>
            <input type="text" placeholder="搜索..." autocomplete="off" class="layui-input layui-input-search" layadmin-event="serach" lay-action="template/search.html?keywords=">
          </li>
        </ul>
        <ul class="layui-nav layui-layout-right" lay-filter="layadmin-layout-right">
          
          <li class="layui-nav-item" lay-unselect>
            <a lay-href="/erp/message" layadmin-event="message" lay-text="消息中心">
              <i class="layui-icon layui-icon-notice"></i>  
              
              <!-- 如果有新消息，则显示小圆点 -->
              <span class="layui-badge" style="margin: -16px -6px 0;height: 12px;line-height: 12px;padding: 0 4px;" id="noticeDot"></span>
            </a>
          </li>
          <li class="layui-nav-item layui-hide-xs" lay-unselect>
            <a href="javascript:;" layadmin-event="theme">
              <i class="layui-icon layui-icon-theme"></i>
            </a>
          </li>
          <li class="layui-nav-item layui-hide-xs" lay-unselect>
            <a href="javascript:;" layadmin-event="note">
              <i class="layui-icon layui-icon-note"></i>
            </a>
          </li>
          <li class="layui-nav-item layui-hide-xs" lay-unselect>
            <a href="javascript:;" layadmin-event="fullscreen">
              <i class="layui-icon layui-icon-screen-full"></i>
            </a>
          </li>
          <li class="layui-nav-item" lay-unselect>
            <a href="javascript:;">
              <input hidden id="loginUserName" value="${userName!""}">
              <input hidden id="role" value="${role!""}">
              ${userName!""}
            </a>
            <dl class="layui-nav-child">
              <dd><a lay-href="/erp/userInfoStart">基本资料</a></dd>
              <dd><a lay-href="/erp/passwordStart">修改密码</a></dd>
              <hr>
              <dd><a href="javascript:;" onclick="loginOut()">退出</a></dd>
            </dl>
          </li>
          
          <li class="layui-nav-item layui-hide-xs" lay-unselect>
<#--            <a href="javascript:;" lay-href="/erp/aboutStart"><i class="layui-icon layui-icon-more-vertical"></i></a>-->
            <a href="javascript:;" layadmin-event="about"><i class="layui-icon layui-icon-more-vertical"></i></a>
          </li>
          <li class="layui-nav-item layui-show-xs-inline-block layui-hide-sm" lay-unselect>
<#--            <a href="javascript:;" lay-href="/erp/aboutStart"><i class="layui-icon layui-icon-more-vertical"></i></a>-->
            <a href="javascript:;" layadmin-event="about"><i class="layui-icon layui-icon-more-vertical"></i></a>
          </li>
        </ul>
      </div>
      
      <!-- 侧边菜单 -->
      <div class="layui-side layui-side-menu">
        <div class="layui-side-scroll">
          <div class="layui-logo" lay-href="/erp/consoleStart">
            <img src="/images/favicon.ico"  style="height: 30px; width: 30px; margin-bottom: 7px; border-radius:50%;"><span style="margin-left: 5px; font-size: x-large;color: #009688; font-weight: bolder; font-family: 黑体">恒达精益管理</span>
          </div>
          
          <ul class="layui-nav layui-nav-tree" lay-shrink="all" id="LAY-system-side-menu" lay-filter="layadmin-system-side-menu">
            <#if role! == 'root'><#--根-->
              <#--工作台-->
              <li data-name="home" class="layui-nav-item layui-nav-itemed">
                <a href="javascript:;" lay-tips="工作台" lay-direction="2">
                  <i class="layui-icon layui-icon-home"></i>
                  <cite>工作台</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console" class="layui-this">
                    <a lay-href="/erp/consoleStart">开始</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/homepageNew">订单进度</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/weekReportStart">一周看板</a>
                  </dd>
                </dl>
              </li>
              <#--订单模块-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="订单模块" lay-direction="2">
                  <i class="layui-icon layui-icon-list"></i>
                  <cite>订单模块</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/clothesDevStart">版单开发</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/clothesDevFabricManageStart">开发面料</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/clothesDevAccessoryManageStart">开发辅料</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/processOrderStart">制单信息</a>
                  </dd>
                </dl>
              </li>
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="IQC" lay-direction="2">
                  <i class="layui-icon layui-icon-list"></i>
                  <cite>品质管理</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/detectionTemplateStart">质检模板</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/orderDetectionStart">质检管理</a>
                  </dd>
                </dl>
              </li>
              <#--裁床打菲-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="票菲管理" lay-direction="1">
                  <i class="layui-icon layui-icon-note"></i>
                  <cite>票菲管理</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/multiTailorStart">主身生成</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/otherMultiTailorStart">配料生成</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/printPartStart">部位管理</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/tailorReprintStart">票菲打印</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/tailorChangeCodeStart">票菲改码</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/bedTailorInfoStart">票菲删除</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/recoveryStart">误删恢复</a>
                  </dd>
                </dl>
              </li>
              <#--裁床报表-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="裁床报表" lay-direction="1">
                  <i class="layui-icon layui-icon-table"></i>
                  <cite>裁床报表</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/cutReportStart">裁床单</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/cutQueryLeakStart">单款报表</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/cutMonthReportStart">月度报表</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/unitConsumptionStart">单耗报表</a>
                  </dd>
                </dl>
              </li>
              <#--花片管理-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="花片管理" lay-direction="1">
                  <i class="layui-icon layui-icon-light"></i>
                  <cite>花片管理</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/opaReportStart">花片查询</a>
                  </dd>
                </dl>
              </li>
              <#--裁片超市-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="裁片超市" lay-direction="1">
                  <i class="layui-icon layui-icon-cart"></i>
                  <cite>裁片超市</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/allStorageStart">裁片库存</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/allEmbStorageStart">衣胚库存</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/embOutAmongStart">衣胚出库查询</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/embOutPrintStart">衣胚出库打印</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/queryEmbLeakStart">衣胚出库汇总</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/cutLocationStart">裁片位置总览</a>
                  </dd>
                </dl>
              </li>
              <#--面辅料管理-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="面辅料管理" lay-direction="1">
                  <i class="layui-icon layui-icon-template-1"></i>
                  <cite>面辅料管理</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/fabricAccessoryCheckStart">一站式下单</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/fabricReturnStart">面料管理</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/tmpFabricStart">暂存面料</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/accessoryInStoreStart">辅料管理</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/accessoryPreStoreStart">共用辅料</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/looseFabricStart">松布打印</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/looseFabricPrintStart">松布补打</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/fabricLoChangeStart">LO色修改</a>
                  </dd>
                </dl>
              </li>
              <#--面辅料报表-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="面辅料报表" lay-direction="1">
                  <i class="layui-icon layui-icon-align-center"></i>
                  <cite>面辅料报表</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/fabricQueryStart">面料库存</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/accessoryStorageStart">辅料库存</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/invoicingStart">面辅料进销存</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/fabricAccessoryReportLeakStart">单款面辅料</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/fabricAccessoryPrintStart">出入库打印</a>
                  </dd>
                  <#--<dd data-name="console">-->
                    <#--<a lay-href="/erp/fabricLeakStart">单款面辅料详情</a>-->
                  <#--</dd>-->
                  <dd data-name="console">
                    <a lay-href="/erp/fabricAccessoryLeakStart">面辅料汇总查询</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/looseFabricSearchStart">松布查询</a>
                  </dd>
                </dl>
              </li>
              <#--IE基础资料-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="IE基础资料" lay-direction="1">
                  <i class="layui-icon layui-icon-set-fill"></i>
                  <cite>IE基础资料</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/procedureTemplateStart">工序数据库</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/orderProcedureNewStart">订单工序</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/procedureReviewStart">审核查询</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/procedureProgressStart">工序进度</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/procedureLevelStart">工序等级</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/beatStart">节拍查询</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/fixedProcedureStart">平板工序</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/orderMeasureStart">尺寸表</a>
                  </dd>
                </dl>
              </li>
              <#--计划排产-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="计划排产" lay-direction="1">
                  <i class="layui-icon layui-icon-log"></i>
                  <cite>计划排产</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/embPlanStart">衣胚计划</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/cutLoosePlanStart">松布裁剪计划</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/sewingPlanStart">计划总览</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/empSkillsStart">技能库</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/orderScheduleStart">自动排产</a>
                  </dd>
<#--                  <dd data-name="console">-->
<#--                    <a lay-href="/erp/autoPlanStart">自动排产</a>-->
<#--                  </dd>-->
                  <dd data-name="console">
                    <a lay-href="/erp/prenatalProgressSummaryStart">产前进度</a>
                  </dd>
                </dl>
              </li>
              <#--计件管理-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="计件管理" lay-direction="1">
                  <i class="layui-icon layui-icon-edit"></i>
                  <cite>计件管理</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/pieceWorkLeakStart">计件扎号详情</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/pieceWorkManageStart">计件管理</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/keyPieceWorkDataDeleteStart">删除记录</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/manualInputStart">手工入数</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/hourEmpStart">时工入数</a>
                  </dd>
                </dl>
              </li>
              <#--财务数据-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="财务数据" lay-direction="1">
                  <i class="layui-icon layui-icon-rmb"></i>
                  <cite>财务数据</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/empSalaryStart">个人产能</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/groupSalaryStart">分组产能</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/excessReportStart">爆数查询</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/gatherPieceWorkReportStart">产能详情</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/costBalanceStart">单款核算</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/orderSalaryStart">单款工资</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/fixedSalaryStart">锁定工资</a>
                  </dd>
                </dl>
              </li>
            <#--财务数据-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="财务对账" lay-direction="1">
                  <i class="layui-icon layui-icon-form"></i>
                  <cite>财务对账</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/fabricAccessoryPreCheckStart">面辅料预审核</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/fabricAccessoryMonthCheckStart">面辅料对账</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/checkMonthStart">月份锁定</a>
                  </dd>
                </dl>
              </li>
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="成品管理" lay-direction="1">
                  <i class="layui-icon layui-icon-tree"></i>
                  <cite>成品管理</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/endProductStorageStart">成品进销存</a>
                  </dd>
                </dl>
              </li>
              <#--行政管理-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="行政管理" lay-direction="1">
                  <i class="layui-icon layui-icon-group"></i>
                  <cite>行政管理</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/staffStart">员工管理</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/userStart">用户信息</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/departmentGroupStart">部门组别</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/checkDetailStart">考勤管理</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/checkRuleStart">上下班管理</a>
                  </dd>
                </dl>
              </li>
              <#--生产报表-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="生产报表" lay-direction="1">
                  <i class="layui-icon layui-icon-chart-screen"></i>
                  <cite>生产报表</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/procedureBalanceNewStart">订单工序平衡</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/groupProgressStart">区间产能汇总</a>
                  </dd>
                  <#--<dd data-name="console">-->
                    <#--<a lay-href="/erp/postTreatmentStart">完工结算</a>-->
                  <#--</dd>-->
                </dl>
              </li>
              <#--工厂基础信息-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="工厂基础信息" lay-direction="1">
                  <i class="layui-icon layui-icon-menu-fill"></i>
                  <cite>工厂基础信息</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/customerStart">客户</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/supplierInfoStart">供应商</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/styleManageStart">款式管理</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/storeHouseStart">仓库信息</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/embStoreStart">衣胚仓库</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/sizeManageStart">尺码管理</a>
                  </dd>
                </dl>
              </li>
              <#--后整打菲-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="后整打菲" lay-direction="1">
                  <i class="layui-icon layui-icon-flag"></i>
                  <cite>后整打菲</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/finishTailorStart">票菲生成</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/finishTailorPrintStart">票菲打印</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/finishTailorDeleteStart">菲票管理</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/finishTailorLeakStart">后整打菲报表</a>
                  </dd>
                </dl>
              </li>
              <#--平板计件-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="平板计件" lay-direction="1">
                  <i class="layui-icon layui-icon-windows"></i>
                  <cite>平板计件</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/otherPieceWorkStart">其他计件</a>
                  </dd>
                </dl>
              </li>
            <#elseif role! == 'role1'> <#--裁床-->
            <#--工作台-->
              <li data-name="home" class="layui-nav-item layui-nav-itemed">
                <a href="javascript:;" lay-tips="工作台" lay-direction="2">
                  <i class="layui-icon layui-icon-home"></i>
                  <cite>工作台</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console" class="layui-this">
                    <a lay-href="/erp/consoleStart">开始</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/homepageNew">订单进度</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/weekReportStart">一周看板</a>
                  </dd>
                </dl>
              </li>
            <#--订单模块-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="订单模块" lay-direction="2">
                  <i class="layui-icon layui-icon-list"></i>
                  <cite>订单模块</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/processOrderStart">制单信息</a>
                  </dd>
                </dl>
              </li>
            <#--裁床打菲-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="票菲管理" lay-direction="1">
                  <i class="layui-icon layui-icon-note"></i>
                  <cite>票菲管理</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/multiTailorStart">主身生成</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/otherMultiTailorStart">配料生成</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/printPartStart">部位管理</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/tailorReprintStart">票菲打印</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/tailorChangeCodeStart">票菲改码</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/bedTailorInfoStart">票菲删除</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/recoveryStart">误删恢复</a>
                  </dd>
                </dl>
              </li>
            <#--裁床报表-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="裁床报表" lay-direction="1">
                  <i class="layui-icon layui-icon-table"></i>
                  <cite>裁床报表</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/cutReportStart">裁床单</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/cutQueryLeakStart">单款报表</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/cutMonthReportStart">月度报表</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/unitConsumptionStart">单耗报表</a>
                  </dd>
                </dl>
              </li>
            <#--花片管理-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="花片管理" lay-direction="1">
                  <i class="layui-icon layui-icon-light"></i>
                  <cite>花片管理</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/opaReportStart">花片查询</a>
                  </dd>
                </dl>
              </li>
            <#--裁片超市-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="裁片超市" lay-direction="1">
                  <i class="layui-icon layui-icon-cart"></i>
                  <cite>裁片超市</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/allStorageStart">裁片库存</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/allEmbStorageStart">衣胚库存</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/embOutAmongStart">衣胚出库查询</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/embOutPrintStart">衣胚出库打印</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/queryEmbLeakStart">衣胚出库汇总</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/cutLocationStart">裁片位置总览</a>
                  </dd>
                </dl>
              </li>
            <#--面辅料报表-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="面辅料报表" lay-direction="1">
                  <i class="layui-icon layui-icon-align-center"></i>
                  <cite>面辅料报表</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/fabricQueryStart">面料库存</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/accessoryStorageStart">辅料库存</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/invoicingStart">面辅料进销存</a>
                  </dd>
                  <#--<dd data-name="console">-->
                    <#--<a lay-href="/erp/fabricOutRecordStart">面料出库详情</a>-->
                  <#--</dd>-->
                  <dd data-name="console">
                    <a lay-href="/erp/fabricAccessoryPrintStart">出入库打印</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/fabricAccessoryReportLeakStart">单款面辅料</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/fabricAccessoryLeakStart">面辅料汇总查询</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/looseFabricSearchStart">松布查询</a>
                  </dd>
                </dl>
              </li>
            <#--计件管理-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="计件管理" lay-direction="1">
                  <i class="layui-icon layui-icon-login-wechat"></i>
                  <cite>计件管理</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/pieceWorkLeakStart">计件扎号详情</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/pieceWorkManageStart">计件管理</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/manualInputStart">手工入数</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/hourEmpStart">时工入数</a>
                  </dd>
                </dl>
              </li>
            <#--IE基础资料-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="IE基础资料" lay-direction="1">
                  <i class="layui-icon layui-icon-set-fill"></i>
                  <cite>IE基础资料</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/procedureProgressStart">工序进度</a>
                  </dd>
                </dl>
              </li>
            <#--财务数据-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="财务数据" lay-direction="1">
                  <i class="layui-icon layui-icon-rmb"></i>
                  <cite>财务数据</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/empSalaryStart">个人产能</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/groupSalaryStart">分组产能</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/gatherPieceWorkReportStart">产能详情</a>
                  </dd>
                </dl>
              </li>
            <#--生产报表-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="生产报表" lay-direction="1">
                  <i class="layui-icon layui-icon-chart-screen"></i>
                  <cite>生产报表</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/procedureBalanceNewStart">订单工序平衡</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/groupProgressStart">区间产能汇总</a>
                  </dd>
                  <#--<dd data-name="console">-->
                    <#--<a lay-href="/erp/postTreatmentStart">完工结算</a>-->
                  <#--</dd>-->
                </dl>
              </li>
            <#--工厂基础信息-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="工厂基础信息" lay-direction="1">
                  <i class="layui-icon layui-icon-menu-fill"></i>
                  <cite>工厂基础信息</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/storeHouseStart">仓库信息</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/embStoreStart">衣胚仓库</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/sizeManageStart">尺码管理</a>
                  </dd>
                </dl>
              </li>
            <#elseif role! == 'role2'> <#--跟单-->
            <#--工作台-->
              <li data-name="home" class="layui-nav-item layui-nav-itemed">
                <a href="javascript:;" lay-tips="工作台" lay-direction="2">
                  <i class="layui-icon layui-icon-home"></i>
                  <cite>工作台</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console" class="layui-this">
                    <a lay-href="/erp/consoleStart">开始</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/homepageNew">订单进度</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/weekReportStart">一周看板</a>
                  </dd>
                </dl>
              </li>
            <#--订单模块-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="订单模块" lay-direction="2">
                  <i class="layui-icon layui-icon-list"></i>
                  <cite>订单模块</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/processOrderStart">制单信息</a>
                  </dd>
                </dl>
              </li>
            <#--裁床报表-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="裁床报表" lay-direction="1">
                  <i class="layui-icon layui-icon-table"></i>
                  <cite>裁床报表</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/cutReportStart">裁床单</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/cutQueryLeakStart">单款报表</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/cutMonthReportStart">月度报表</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/unitConsumptionStart">单耗报表</a>
                  </dd>
                </dl>
              </li>
            <#--花片管理-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="花片管理" lay-direction="1">
                  <i class="layui-icon layui-icon-light"></i>
                  <cite>花片管理</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/opaReportStart">花片查询</a>
                  </dd>
                </dl>
              </li>
            <#--裁片超市-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="裁片超市" lay-direction="1">
                  <i class="layui-icon layui-icon-cart"></i>
                  <cite>裁片超市</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/allStorageStart">裁片库存</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/allEmbStorageStart">衣胚库存</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/embOutAmongStart">衣胚出库查询</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/embOutPrintStart">衣胚出库打印</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/queryEmbLeakStart">衣胚出库汇总</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/cutLocationStart">裁片位置总览</a>
                  </dd>
                </dl>
              </li>
            <#--面辅料管理-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="面辅料管理" lay-direction="1">
                  <i class="layui-icon layui-icon-template-1"></i>
                  <cite>面辅料管理</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/fabricAccessoryCheckStart">一站式下单</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/fabricReturnStart">面料管理</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/tmpFabricStart">暂存面料</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/accessoryInStoreStart">辅料管理</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/accessoryPreStoreStart">共用辅料</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/looseFabricStart">松布打印</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/looseFabricPrintStart">松布补打</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/fabricLoChangeStart">LO色修改</a>
                  </dd>
                </dl>
              </li>
            <#--面辅料报表-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="面辅料报表" lay-direction="1">
                  <i class="layui-icon layui-icon-align-center"></i>
                  <cite>面辅料报表</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/fabricQueryStart">面料库存</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/accessoryStorageStart">辅料库存</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/invoicingStart">面辅料进销存</a>
                  </dd>
                  <#--<dd data-name="console">-->
                    <#--<a lay-href="/erp/fabricOutRecordStart">面料出库详情</a>-->
                  <#--</dd>-->
                  <dd data-name="console">
                    <a lay-href="/erp/fabricAccessoryPrintStart">出入库打印</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/fabricAccessoryReportLeakStart">单款面辅料</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/fabricAccessoryLeakStart">面辅料汇总查询</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/looseFabricSearchStart">松布查询</a>
                  </dd>
                </dl>
              </li>
            <#--财务数据-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="财务数据" lay-direction="1">
                  <i class="layui-icon layui-icon-rmb"></i>
                  <cite>财务数据</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/empSalaryStart">个人产能</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/groupSalaryStart">分组产能</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/costBalanceStart">单款核算</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/gatherPieceWorkReportStart">产能详情</a>
                  </dd>
                </dl>
              </li>
            <#--生产报表-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="生产报表" lay-direction="1">
                  <i class="layui-icon layui-icon-chart-screen"></i>
                  <cite>生产报表</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/procedureBalanceNewStart">订单工序平衡</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/groupProgressStart">区间产能汇总</a>
                  </dd>
                  <#--<dd data-name="console">-->
                    <#--<a lay-href="/erp/postTreatmentStart">完工结算</a>-->
                  <#--</dd>-->
                </dl>
              </li>
            <#--工厂基础信息-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="工厂基础信息" lay-direction="1">
                  <i class="layui-icon layui-icon-menu-fill"></i>
                  <cite>工厂基础信息</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/customerStart">客户</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/supplierInfoStart">供应商</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/storeHouseStart">仓库信息</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/embStoreStart">衣胚仓库</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/sizeManageStart">尺码管理</a>
                  </dd>
                </dl>
              </li>
            <#elseif role! == 'role4'> <#--行政-->
            <#--工作台-->
              <li data-name="home" class="layui-nav-item layui-nav-itemed">
                <a href="javascript:;" lay-tips="工作台" lay-direction="2">
                  <i class="layui-icon layui-icon-home"></i>
                  <cite>工作台</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console" class="layui-this">
                    <a lay-href="/erp/consoleStart">开始</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/homepageNew">订单进度</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/weekReportStart">一周看板</a>
                  </dd>
                </dl>
              </li>
            <#--财务数据-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="财务数据" lay-direction="1">
                  <i class="layui-icon layui-icon-rmb"></i>
                  <cite>财务数据</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/empSalaryStart">个人产能</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/groupSalaryStart">分组产能</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/gatherPieceWorkReportStart">产能详情</a>
                  </dd>
                </dl>
              </li>
            <#--行政管理-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="行政管理" lay-direction="1">
                  <i class="layui-icon layui-icon-group"></i>
                  <cite>行政管理</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/departmentGroupStart">部门组别</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/staffStart">员工管理</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/userStart">用户信息</a>
                  </dd>
                </dl>
              </li>
            <#--工厂基础信息-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="工厂基础信息" lay-direction="1">
                  <i class="layui-icon layui-icon-menu-fill"></i>
                  <cite>工厂基础信息</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/storeHouseStart">仓库信息</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/embStoreStart">衣胚仓库</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/sizeManageStart">尺码管理</a>
                  </dd>
                </dl>
              </li>
            <#elseif role! == 'role5'> <#--IE-->
            <#--工作台-->
              <li data-name="home" class="layui-nav-item layui-nav-itemed">
                <a href="javascript:;" lay-tips="工作台" lay-direction="2">
                  <i class="layui-icon layui-icon-home"></i>
                  <cite>工作台</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console" class="layui-this">
                    <a lay-href="/erp/consoleStart">开始</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/homepageNew">订单进度</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/weekReportStart">一周看板</a>
                  </dd>
                </dl>
              </li>
            <#--订单模块-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="订单模块" lay-direction="2">
                  <i class="layui-icon layui-icon-list"></i>
                  <cite>订单模块</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/processOrderStart">制单信息</a>
                  </dd>
                </dl>
              </li>
            <#--裁床报表-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="裁床报表" lay-direction="1">
                  <i class="layui-icon layui-icon-table"></i>
                  <cite>裁床报表</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/cutReportStart">裁床单</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/cutQueryLeakStart">单款报表</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/cutMonthReportStart">月度报表</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/unitConsumptionStart">单耗报表</a>
                  </dd>
                </dl>
              </li>
            <#--花片管理-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="花片管理" lay-direction="1">
                  <i class="layui-icon layui-icon-light"></i>
                  <cite>花片管理</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/opaReportStart">花片查询</a>
                  </dd>
                </dl>
              </li>
            <#--裁片超市-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="裁片超市" lay-direction="1">
                  <i class="layui-icon layui-icon-cart"></i>
                  <cite>裁片超市</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/allStorageStart">裁片库存</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/allEmbStorageStart">衣胚库存</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/embOutAmongStart">衣胚出库查询</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/embOutPrintStart">衣胚出库打印</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/queryEmbLeakStart">衣胚出库汇总</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/cutLocationStart">裁片位置总览</a>
                  </dd>
                </dl>
              </li>
            <#--面辅料管理-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="面辅料管理" lay-direction="1">
                  <i class="layui-icon layui-icon-template-1"></i>
                  <cite>面辅料管理</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/fabricAccessoryCheckStart">一站式下单</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/fabricReturnStart">面料管理</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/tmpFabricStart">暂存面料</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/accessoryInStoreStart">辅料管理</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/accessoryPreStoreStart">共用辅料</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/looseFabricStart">松布打印</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/looseFabricPrintStart">松布补打</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/fabricLoChangeStart">LO色修改</a>
                  </dd>
                </dl>
              </li>
            <#--面辅料报表-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="面辅料报表" lay-direction="1">
                  <i class="layui-icon layui-icon-align-center"></i>
                  <cite>面辅料报表</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/fabricQueryStart">面料库存</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/accessoryStorageStart">辅料库存</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/invoicingStart">面辅料进销存</a>
                  </dd>
                  <#--<dd data-name="console">-->
                    <#--<a lay-href="/erp/fabricOutRecordStart">面料出库详情</a>-->
                  <#--</dd>-->
                  <dd data-name="console">
                    <a lay-href="/erp/fabricAccessoryPrintStart">出入库打印</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/fabricAccessoryReportLeakStart">单款面辅料</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/fabricAccessoryLeakStart">面辅料汇总查询</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/looseFabricSearchStart">松布查询</a>
                  </dd>
                </dl>
              </li>
            <#--IE基础资料-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="IE基础资料" lay-direction="1">
                  <i class="layui-icon layui-icon-set-fill"></i>
                  <cite>IE基础资料</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/procedureTemplateStart">工序数据库</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/orderProcedureNewStart">订单工序</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/procedureReviewStart">审核查询</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/procedureProgressStart">工序进度</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/procedureLevelStart">工序等级</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/beatStart">节拍查询</a>
                  </dd>
                </dl>
              </li>
            <#--计划排产-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="计划排产" lay-direction="1">
                  <i class="layui-icon layui-icon-log"></i>
                  <cite>计划排产</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/embPlanStart">衣胚计划</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/cutLoosePlanStart">松布裁剪计划</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/sewingPlanStart">计划总览</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/empSkillsStart">技能库</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/orderScheduleStart">自动排产</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/prenatalProgressSummaryStart">产前进度</a>
                  </dd>
                </dl>
              </li>
            <#--计件管理-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="计件管理" lay-direction="1">
                  <i class="layui-icon layui-icon-login-wechat"></i>
                  <cite>计件管理</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/excessReportStart">爆数查询</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/pieceWorkLeakStart">计件扎号详情</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/pieceWorkManageStart">计件管理</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/manualInputStart">手工入数</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/hourEmpStart">时工入数</a>
                  </dd>
                </dl>
              </li>
            <#--财务数据-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="财务数据" lay-direction="1">
                  <i class="layui-icon layui-icon-rmb"></i>
                  <cite>财务数据</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/empSalaryStart">个人产能</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/groupSalaryStart">分组产能</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/gatherPieceWorkReportStart">产能详情</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/costBalanceStart">单款核算</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/orderSalaryStart">单款工资</a>
                  </dd>
                </dl>
              </li>
            <#--生产报表-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="生产报表" lay-direction="1">
                  <i class="layui-icon layui-icon-chart-screen"></i>
                  <cite>生产报表</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/procedureBalanceNewStart">订单工序平衡</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/groupProgressStart">区间产能汇总</a>
                  </dd>
                  <#--<dd data-name="console">-->
                    <#--<a lay-href="/erp/postTreatmentStart">完工结算</a>-->
                  <#--</dd>-->
                </dl>
              </li>
            <#--行政管理-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="行政管理" lay-direction="1">
                  <i class="layui-icon layui-icon-group"></i>
                  <cite>行政管理</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/staffStart">员工管理</a>
                  </dd>
                </dl>
              </li>
            <#--工厂基础信息-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="工厂基础信息" lay-direction="1">
                  <i class="layui-icon layui-icon-menu-fill"></i>
                  <cite>工厂基础信息</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/storeHouseStart">仓库信息</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/embStoreStart">衣胚仓库</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/sizeManageStart">尺码管理</a>
                  </dd>
                </dl>
              </li>
            <#--后整打菲-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="后整打菲" lay-direction="1">
                  <i class="layui-icon layui-icon-flag"></i>
                  <cite>后整打菲</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/finishTailorStart">票菲生成</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/finishTailorPrintStart">票菲打印</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/finishTailorDeleteStart">菲票管理</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/finishTailorLeakStart">后整打菲报表</a>
                  </dd>
                </dl>
              </li>
            <#--平板计件-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="平板计件" lay-direction="1">
                  <i class="layui-icon layui-icon-windows"></i>
                  <cite>平板计件</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/otherPieceWorkStart">其他计件</a>
                  </dd>
                </dl>
              </li>
            <#elseif role! == 'role6'> <#--裁片超市-->
            <#--工作台-->
              <li data-name="home" class="layui-nav-item layui-nav-itemed">
                <a href="javascript:;" lay-tips="工作台" lay-direction="2">
                  <i class="layui-icon layui-icon-home"></i>
                  <cite>工作台</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console" class="layui-this">
                    <a lay-href="/erp/consoleStart">开始</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/homepageNew">订单进度</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/weekReportStart">一周看板</a>
                  </dd>
                </dl>
              </li>
            <#--订单模块-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="订单模块" lay-direction="2">
                  <i class="layui-icon layui-icon-list"></i>
                  <cite>订单模块</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/processOrderStart">制单信息</a>
                  </dd>
                </dl>
              </li>
            <#--裁床报表-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="裁床报表" lay-direction="1">
                  <i class="layui-icon layui-icon-table"></i>
                  <cite>裁床报表</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/cutReportStart">裁床单</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/cutQueryLeakStart">单款报表</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/cutMonthReportStart">月度报表</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/unitConsumptionStart">单耗报表</a>
                  </dd>
                </dl>
              </li>
            <#--花片管理-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="花片管理" lay-direction="1">
                  <i class="layui-icon layui-icon-light"></i>
                  <cite>花片管理</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/opaReportStart">花片查询</a>
                  </dd>
                </dl>
              </li>
            <#--裁片超市-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="裁片超市" lay-direction="1">
                  <i class="layui-icon layui-icon-cart"></i>
                  <cite>裁片超市</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/allStorageStart">裁片库存</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/allEmbStorageStart">衣胚库存</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/embOutAmongStart">衣胚出库查询</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/embOutPrintStart">衣胚出库打印</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/queryEmbLeakStart">衣胚出库汇总</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/cutLocationStart">裁片位置总览</a>
                  </dd>
                </dl>
              </li>
            <#--计件管理-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="计件管理" lay-direction="1">
                  <i class="layui-icon layui-icon-login-wechat"></i>
                  <cite>计件管理</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/pieceWorkLeakStart">计件扎号详情</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/pieceWorkManageStart">计件管理</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/manualInputStart">手工入数</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/hourEmpStart">时工入数</a>
                  </dd>
                </dl>
              </li>
            <#--生产报表-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="生产报表" lay-direction="1">
                  <i class="layui-icon layui-icon-chart-screen"></i>
                  <cite>生产报表</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/procedureBalanceNewStart">订单工序平衡</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/groupProgressStart">区间产能汇总</a>
                  </dd>
                  <#--<dd data-name="console">-->
                    <#--<a lay-href="/erp/postTreatmentStart">完工结算</a>-->
                  <#--</dd>-->
                </dl>
              </li>
            <#elseif role! == 'role7'> <#--吊挂查数-->
            <#--工作台-->
              <li data-name="home" class="layui-nav-item layui-nav-itemed">
                <a href="javascript:;" lay-tips="工作台" lay-direction="2">
                  <i class="layui-icon layui-icon-home"></i>
                  <cite>工作台</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console" class="layui-this">
                    <a lay-href="/erp/consoleStart">开始</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/homepageNew">订单进度</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/weekReportStart">一周看板</a>
                  </dd>
                </dl>
              </li>
            <#--订单模块-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="订单模块" lay-direction="2">
                  <i class="layui-icon layui-icon-list"></i>
                  <cite>订单模块</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/processOrderStart">制单信息</a>
                  </dd>
                </dl>
              </li>
            <#--裁床报表-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="裁床报表" lay-direction="1">
                  <i class="layui-icon layui-icon-table"></i>
                  <cite>裁床报表</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/cutReportStart">裁床单</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/cutQueryLeakStart">单款报表</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/cutMonthReportStart">月度报表</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/unitConsumptionStart">单耗报表</a>
                  </dd>
                </dl>
              </li>
            <#--花片管理-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="花片管理" lay-direction="1">
                  <i class="layui-icon layui-icon-light"></i>
                  <cite>花片管理</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/opaReportStart">花片查询</a>
                  </dd>
                </dl>
              </li>
            <#--裁片超市-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="裁片超市" lay-direction="1">
                  <i class="layui-icon layui-icon-cart"></i>
                  <cite>裁片超市</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/allStorageStart">裁片库存</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/allEmbStorageStart">衣胚库存</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/embOutAmongStart">衣胚出库查询</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/embOutPrintStart">衣胚出库打印</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/queryEmbLeakStart">衣胚出库汇总</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/cutLocationStart">裁片位置总览</a>
                  </dd>
                </dl>
              </li>
            <#--面辅料报表-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="面辅料报表" lay-direction="1">
                  <i class="layui-icon layui-icon-align-center"></i>
                  <cite>面辅料报表</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/fabricAccessoryReportLeakStart">单款面辅料</a>
                  </dd>
                </dl>
              </li>
            <#--生产报表-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="生产报表" lay-direction="1">
                  <i class="layui-icon layui-icon-chart-screen"></i>
                  <cite>生产报表</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/procedureBalanceNewStart">订单工序平衡</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/groupProgressStart">区间产能汇总</a>
                  </dd>
                  <#--<dd data-name="console">-->
                    <#--<a lay-href="/erp/postTreatmentStart">完工结算</a>-->
                  <#--</dd>-->
                </dl>
              </li>
            <#elseif role! == 'role8'> <#--车间文员入数-->
            <#--工作台-->
              <li data-name="home" class="layui-nav-item layui-nav-itemed">
                <a href="javascript:;" lay-tips="工作台" lay-direction="2">
                  <i class="layui-icon layui-icon-home"></i>
                  <cite>工作台</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console" class="layui-this">
                    <a lay-href="/erp/consoleStart">开始</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/homepageNew">订单进度</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/weekReportStart">一周看板</a>
                  </dd>
                </dl>
              </li>
            <#--订单模块-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="订单模块" lay-direction="2">
                  <i class="layui-icon layui-icon-list"></i>
                  <cite>订单模块</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/processOrderStart">制单信息</a>
                  </dd>
                </dl>
              </li>
            <#--裁床报表-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="裁床报表" lay-direction="1">
                  <i class="layui-icon layui-icon-table"></i>
                  <cite>裁床报表</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/cutReportStart">裁床单</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/cutQueryLeakStart">单款报表</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/cutMonthReportStart">月度报表</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/unitConsumptionStart">单耗报表</a>
                  </dd>
                </dl>
              </li>
            <#--IE基础资料-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="IE基础资料" lay-direction="1">
                  <i class="layui-icon layui-icon-set-fill"></i>
                  <cite>IE基础资料</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/orderProcedureNewStart">订单工序</a>
                  </dd>
                </dl>
              </li>
            <#--计件管理-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="计件管理" lay-direction="1">
                  <i class="layui-icon layui-icon-login-wechat"></i>
                  <cite>计件管理</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/pieceWorkLeakStart">计件扎号详情</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/pieceWorkManageStart">计件管理</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/manualInputStart">手工入数</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/hourEmpStart">时工入数</a>
                  </dd>
                </dl>
              </li>
            <#--财务数据-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="财务数据" lay-direction="1">
                  <i class="layui-icon layui-icon-rmb"></i>
                  <cite>财务数据</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/empSalaryStart">个人产能</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/groupSalaryStart">分组产能</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/gatherPieceWorkReportStart">产能详情</a>
                  </dd>
                </dl>
              </li>
            <#--生产报表-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="生产报表" lay-direction="1">
                  <i class="layui-icon layui-icon-chart-screen"></i>
                  <cite>生产报表</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/procedureBalanceNewStart">订单工序平衡</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/groupProgressStart">区间产能汇总</a>
                  </dd>
                  <#--<dd data-name="console">-->
                    <#--<a lay-href="/erp/postTreatmentStart">完工结算</a>-->
                  <#--</dd>-->
                </dl>
              </li>
            <#--后整打菲-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="后整打菲" lay-direction="1">
                  <i class="layui-icon layui-icon-flag"></i>
                  <cite>后整打菲</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/finishTailorStart">票菲生成</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/finishTailorPrintStart">票菲打印</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/finishTailorDeleteStart">菲票管理</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/finishTailorLeakStart">后整打菲报表</a>
                  </dd>
                </dl>
              </li>
            <#--平板计件-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="平板计件" lay-direction="1">
                  <i class="layui-icon layui-icon-windows"></i>
                  <cite>平板计件</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/otherPieceWorkStart">其他计件</a>
                  </dd>
                </dl>
              </li>
            <#elseif role! == 'role10'> <#--财务-->
            <#--工作台-->
              <li data-name="home" class="layui-nav-item layui-nav-itemed">
                <a href="javascript:;" lay-tips="工作台" lay-direction="2">
                  <i class="layui-icon layui-icon-home"></i>
                  <cite>工作台</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console" class="layui-this">
                    <a lay-href="/erp/consoleStart">开始</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/homepageNew">订单进度</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/weekReportStart">一周看板</a>
                  </dd>
                </dl>
              </li>
            <#--订单模块-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="订单模块" lay-direction="2">
                  <i class="layui-icon layui-icon-list"></i>
                  <cite>订单模块</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/processOrderStart">制单信息</a>
                  </dd>
                </dl>
              </li>
            <#--裁床报表-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="裁床报表" lay-direction="1">
                  <i class="layui-icon layui-icon-table"></i>
                  <cite>裁床报表</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/cutReportStart">裁床单</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/cutQueryLeakStart">单款报表</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/cutMonthReportStart">月度报表</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/unitConsumptionStart">单耗报表</a>
                  </dd>
                </dl>
              </li>
            <#--花片管理-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="花片管理" lay-direction="1">
                  <i class="layui-icon layui-icon-light"></i>
                  <cite>花片管理</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/opaReportStart">花片查询</a>
                  </dd>
                </dl>
              </li>
            <#--裁片超市-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="裁片超市" lay-direction="1">
                  <i class="layui-icon layui-icon-cart"></i>
                  <cite>裁片超市</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/allStorageStart">裁片库存</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/allEmbStorageStart">衣胚库存</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/embOutAmongStart">衣胚出库查询</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/embOutPrintStart">衣胚出库打印</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/queryEmbLeakStart">衣胚出库汇总</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/cutLocationStart">裁片位置总览</a>
                  </dd>
                </dl>
              </li>
            <#--面辅料管理-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="面辅料管理" lay-direction="1">
                  <i class="layui-icon layui-icon-template-1"></i>
                  <cite>面辅料管理</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/fabricAccessoryCheckStart">一站式下单</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/fabricReturnStart">面料管理</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/tmpFabricStart">暂存面料</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/accessoryInStoreStart">辅料管理</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/accessoryPreStoreStart">共用辅料</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/looseFabricStart">松布打印</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/looseFabricPrintStart">松布补打</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/fabricLoChangeStart">LO色修改</a>
                  </dd>
                </dl>
              </li>
            <#--面辅料报表-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="面辅料报表" lay-direction="1">
                  <i class="layui-icon layui-icon-align-center"></i>
                  <cite>面辅料报表</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/fabricQueryStart">面料库存</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/accessoryStorageStart">辅料库存</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/invoicingStart">面辅料进销存</a>
                  </dd>
                  <#--<dd data-name="console">-->
                    <#--<a lay-href="/erp/fabricOutRecordStart">面料出库详情</a>-->
                  <#--</dd>-->
                  <dd data-name="console">
                    <a lay-href="/erp/fabricAccessoryPrintStart">出入库打印</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/fabricAccessoryReportLeakStart">单款面辅料</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/fabricAccessoryLeakStart">面辅料汇总查询</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/looseFabricSearchStart">松布查询</a>
                  </dd>
                </dl>
              </li>
            <#--IE基础资料-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="IE基础资料" lay-direction="1">
                  <i class="layui-icon layui-icon-set-fill"></i>
                  <cite>IE基础资料</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/procedureTemplateStart">工序数据库</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/orderProcedureNewStart">订单工序</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/procedureProgressStart">工序进度</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/procedureLevelStart">工序等级</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/beatStart">节拍查询</a>
                  </dd>
                </dl>
              </li>
            <#--计划排产-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="计划排产" lay-direction="1">
                  <i class="layui-icon layui-icon-log"></i>
                  <cite>计划排产</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/embPlanStart">衣胚计划</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/cutLoosePlanStart">松布裁剪计划</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/sewingPlanStart">计划总览</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/empSkillsStart">技能库</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/orderScheduleStart">自动排产</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/prenatalProgressSummaryStart">产前进度</a>
                  </dd>
                </dl>
              </li>
            <#--计件管理-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="计件管理" lay-direction="1">
                  <i class="layui-icon layui-icon-login-wechat"></i>
                  <cite>计件管理</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/pieceWorkLeakStart">计件扎号详情</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/pieceWorkManageStart">计件管理</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/keyPieceWorkDataDeleteStart">删除记录</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/manualInputStart">手工入数</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/hourEmpStart">时工入数</a>
                  </dd>
                </dl>
              </li>
            <#--财务数据-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="财务数据" lay-direction="1">
                  <i class="layui-icon layui-icon-rmb"></i>
                  <cite>财务数据</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/empSalaryStart">个人产能</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/groupSalaryStart">分组产能</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/excessReportStart">爆数查询</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/gatherPieceWorkReportStart">产能详情</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/costBalanceStart">单款核算</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/orderSalaryStart">单款工资</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/fixedSalaryStart">锁定工资</a>
                  </dd>
                </dl>
              </li>
            <#--财务数据-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="财务对账" lay-direction="1">
                  <i class="layui-icon layui-icon-form"></i>
                  <cite>财务对账</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/fabricAccessoryPreCheckStart">面辅料预审核</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/fabricAccessoryMonthCheckStart">面辅料对账</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/checkMonthStart">月份锁定</a>
                  </dd>
                </dl>
              </li>
            <#--生产报表-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="生产报表" lay-direction="1">
                  <i class="layui-icon layui-icon-chart-screen"></i>
                  <cite>生产报表</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/procedureBalanceNewStart">订单工序平衡</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/groupProgressStart">区间产能汇总</a>
                  </dd>
                  <#--<dd data-name="console">-->
                    <#--<a lay-href="/erp/postTreatmentStart">完工结算</a>-->
                  <#--</dd>-->
                </dl>
              </li>
            <#--行政管理-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="行政管理" lay-direction="1">
                  <i class="layui-icon layui-icon-group"></i>
                  <cite>行政管理</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/staffStart">员工管理</a>
                  </dd>
                </dl>
              </li>
            <#--工厂基础信息-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="工厂基础信息" lay-direction="1">
                  <i class="layui-icon layui-icon-menu-fill"></i>
                  <cite>工厂基础信息</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/customerStart">客户</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/supplierInfoStart">供应商</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/storeHouseStart">仓库信息</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/embStoreStart">衣胚仓库</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/sizeManageStart">尺码管理</a>
                  </dd>
                </dl>
              </li>
            <#elseif role! == 'role11'> <#--面料管理-->
            <#--工作台-->
              <li data-name="home" class="layui-nav-item layui-nav-itemed">
                <a href="javascript:;" lay-tips="工作台" lay-direction="2">
                  <i class="layui-icon layui-icon-home"></i>
                  <cite>工作台</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console" class="layui-this">
                    <a lay-href="/erp/consoleStart">开始</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/homepageNew">订单进度</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/weekReportStart">一周看板</a>
                  </dd>
                </dl>
              </li>
            <#--订单模块-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="订单模块" lay-direction="2">
                  <i class="layui-icon layui-icon-list"></i>
                  <cite>订单模块</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/processOrderStart">制单信息</a>
                  </dd>
                </dl>
              </li>
            <#--裁床报表-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="裁床报表" lay-direction="1">
                  <i class="layui-icon layui-icon-table"></i>
                  <cite>裁床报表</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/cutReportStart">裁床单</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/cutQueryLeakStart">单款报表</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/cutMonthReportStart">月度报表</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/unitConsumptionStart">单耗报表</a>
                  </dd>
                </dl>
              </li>
            <#--花片管理-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="花片管理" lay-direction="1">
                  <i class="layui-icon layui-icon-light"></i>
                  <cite>花片管理</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/opaReportStart">花片查询</a>
                  </dd>
                </dl>
              </li>
            <#--裁片超市-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="裁片超市" lay-direction="1">
                  <i class="layui-icon layui-icon-cart"></i>
                  <cite>裁片超市</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/allStorageStart">裁片库存</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/allEmbStorageStart">衣胚库存</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/embOutAmongStart">衣胚出库查询</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/embOutPrintStart">衣胚出库打印</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/queryEmbLeakStart">衣胚出库汇总</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/cutLocationStart">裁片位置总览</a>
                  </dd>
                </dl>
              </li>
            <#--面辅料管理-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="面辅料管理" lay-direction="1">
                  <i class="layui-icon layui-icon-template-1"></i>
                  <cite>面辅料管理</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/fabricAccessoryCheckStart">一站式下单</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/fabricReturnStart">面料管理</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/tmpFabricStart">暂存面料</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/accessoryInStoreStart">辅料管理</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/accessoryPreStoreStart">共用辅料</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/looseFabricStart">松布打印</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/looseFabricPrintStart">松布补打</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/fabricLoChangeStart">LO色修改</a>
                  </dd>
                </dl>
              </li>
            <#--面辅料报表-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="面辅料报表" lay-direction="1">
                  <i class="layui-icon layui-icon-align-center"></i>
                  <cite>面辅料报表</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/fabricQueryStart">面料库存</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/accessoryStorageStart">辅料库存</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/invoicingStart">面辅料进销存</a>
                  </dd>
                  <#--<dd data-name="console">-->
                    <#--<a lay-href="/erp/fabricOutRecordStart">面料出库详情</a>-->
                  <#--</dd>-->
                  <dd data-name="console">
                    <a lay-href="/erp/fabricAccessoryPrintStart">出入库打印</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/fabricAccessoryReportLeakStart">单款面辅料</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/fabricAccessoryLeakStart">面辅料汇总查询</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/looseFabricSearchStart">松布查询</a>
                  </dd>
                </dl>
              </li>
            <#--计件管理-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="计件管理" lay-direction="1">
                  <i class="layui-icon layui-icon-login-wechat"></i>
                  <cite>计件管理</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/pieceWorkLeakStart">计件扎号详情</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/pieceWorkManageStart">计件管理</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/manualInputStart">手工入数</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/hourEmpStart">时工入数</a>
                  </dd>
                </dl>
              </li>
            <#--财务数据-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="财务数据" lay-direction="1">
                  <i class="layui-icon layui-icon-rmb"></i>
                  <cite>财务数据</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/empSalaryStart">个人产能</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/groupSalaryStart">分组产能</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/gatherPieceWorkReportStart">产能详情</a>
                  </dd>
                </dl>
              </li>
            <#--生产报表-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="生产报表" lay-direction="1">
                  <i class="layui-icon layui-icon-chart-screen"></i>
                  <cite>生产报表</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/procedureBalanceNewStart">订单工序平衡</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/groupProgressStart">区间产能汇总</a>
                  </dd>
                  <#--<dd data-name="console">-->
                    <#--<a lay-href="/erp/postTreatmentStart">完工结算</a>-->
                  <#--</dd>-->
                </dl>
              </li>
            <#elseif role! == 'role12'> <#--管理层-->
            <#--工作台-->
              <li data-name="home" class="layui-nav-item layui-nav-itemed">
                <a href="javascript:;" lay-tips="工作台" lay-direction="2">
                  <i class="layui-icon layui-icon-home"></i>
                  <cite>工作台</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console" class="layui-this">
                    <a lay-href="/erp/consoleStart">开始</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/homepageNew">订单进度</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/weekReportStart">一周看板</a>
                  </dd>
                </dl>
              </li>
            <#--订单模块-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="订单模块" lay-direction="2">
                  <i class="layui-icon layui-icon-list"></i>
                  <cite>订单模块</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/processOrderStart">制单信息</a>
                  </dd>
                </dl>
              </li>
            <#--裁床报表-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="裁床报表" lay-direction="1">
                  <i class="layui-icon layui-icon-table"></i>
                  <cite>裁床报表</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/cutReportStart">裁床单</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/cutQueryLeakStart">单款报表</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/cutMonthReportStart">月度报表</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/unitConsumptionStart">单耗报表</a>
                  </dd>
                </dl>
              </li>
            <#--花片管理-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="花片管理" lay-direction="1">
                  <i class="layui-icon layui-icon-light"></i>
                  <cite>花片管理</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/opaReportStart">花片查询</a>
                  </dd>
                </dl>
              </li>
            <#--裁片超市-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="裁片超市" lay-direction="1">
                  <i class="layui-icon layui-icon-cart"></i>
                  <cite>裁片超市</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/allStorageStart">裁片库存</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/allEmbStorageStart">衣胚库存</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/embOutAmongStart">衣胚出库查询</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/embOutPrintStart">衣胚出库打印</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/queryEmbLeakStart">衣胚出库汇总</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/cutLocationStart">裁片位置总览</a>
                  </dd>
                </dl>
              </li>
            <#--IE基础资料-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="IE基础资料" lay-direction="1">
                  <i class="layui-icon layui-icon-set-fill"></i>
                  <cite>IE基础资料</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/orderProcedureNewStart">订单工序</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/procedureProgressStart">工序进度</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/procedureLevelStart">工序等级</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/beatStart">节拍查询</a>
                  </dd>
                </dl>
              </li>

            <#--计划排产-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="计划排产" lay-direction="1">
                  <i class="layui-icon layui-icon-log"></i>
                  <cite>计划排产</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/embPlanStart">衣胚计划</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/cutLoosePlanStart">松布裁剪计划</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/sewingPlanStart">计划总览</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/empSkillsStart">技能库</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/orderScheduleStart">自动排产</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/prenatalProgressSummaryStart">产前进度</a>
                  </dd>
                </dl>
              </li>
            <#--面辅料报表-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="面辅料报表" lay-direction="1">
                  <i class="layui-icon layui-icon-align-center"></i>
                  <cite>面辅料报表</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/fabricQueryStart">面料库存</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/accessoryStorageStart">辅料库存</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/invoicingStart">面辅料进销存</a>
                  </dd>
                  <#--<dd data-name="console">-->
                    <#--<a lay-href="/erp/fabricOutRecordStart">面料出库详情</a>-->
                  <#--</dd>-->
                  <dd data-name="console">
                    <a lay-href="/erp/fabricAccessoryPrintStart">出入库打印</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/fabricAccessoryReportLeakStart">单款面辅料</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/fabricAccessoryLeakStart">面辅料汇总查询</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/looseFabricSearchStart">松布查询</a>
                  </dd>
                </dl>
              </li>
            <#--计件管理-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="计件管理" lay-direction="1">
                  <i class="layui-icon layui-icon-login-wechat"></i>
                  <cite>计件管理</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/pieceWorkLeakStart">计件扎号详情</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/pieceWorkManageStart">计件管理</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/manualInputStart">手工入数</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/hourEmpStart">时工入数</a>
                  </dd>
                </dl>
              </li>
            <#--财务数据-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="财务数据" lay-direction="1">
                  <i class="layui-icon layui-icon-rmb"></i>
                  <cite>财务数据</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/empSalaryStart">个人产能</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/groupSalaryStart">分组产能</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/gatherPieceWorkReportStart">产能详情</a>
                  </dd>
                </dl>
              </li>
            <#--生产报表-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="生产报表" lay-direction="1">
                  <i class="layui-icon layui-icon-chart-screen"></i>
                  <cite>生产报表</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/procedureBalanceNewStart">订单工序平衡</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/groupProgressStart">区间产能汇总</a>
                  </dd>
                  <#--<dd data-name="console">-->
                    <#--<a lay-href="/erp/postTreatmentStart">完工结算</a>-->
                  <#--</dd>-->
                </dl>
              </li>
            <#--行政管理-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="行政管理" lay-direction="1">
                  <i class="layui-icon layui-icon-group"></i>
                  <cite>行政管理</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/staffStart">员工管理</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/departmentGroupStart">部门组别</a>
                  </dd>
                </dl>
              </li>
            <#--工厂基础信息-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="工厂基础信息" lay-direction="1">
                  <i class="layui-icon layui-icon-menu-fill"></i>
                  <cite>工厂基础信息</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/customerStart">客户</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/supplierInfoStart">供应商</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/storeHouseStart">仓库信息</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/embStoreStart">衣胚仓库</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/sizeManageStart">尺码管理</a>
                  </dd>
                </dl>
              </li>
            <#--后整打菲-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="后整打菲" lay-direction="1">
                  <i class="layui-icon layui-icon-flag"></i>
                  <cite>后整打菲</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/finishTailorStart">票菲生成</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/finishTailorPrintStart">票菲打印</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/finishTailorDeleteStart">菲票管理</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/finishTailorLeakStart">后整打菲报表</a>
                  </dd>
                </dl>
              </li>
            <#--平板计件-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="平板计件" lay-direction="1">
                  <i class="layui-icon layui-icon-windows"></i>
                  <cite>平板计件</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/otherPieceWorkStart">其他计件</a>
                  </dd>
                </dl>
              </li>
            <#elseif role! == 'role14'> <#--后整打菲-->
            <#--工作台-->
              <li data-name="home" class="layui-nav-item layui-nav-itemed">
                <a href="javascript:;" lay-tips="工作台" lay-direction="2">
                  <i class="layui-icon layui-icon-home"></i>
                  <cite>工作台</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console" class="layui-this">
                    <a lay-href="/erp/consoleStart">开始</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/homepageNew">订单进度</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/weekReportStart">一周看板</a>
                  </dd>
                </dl>
              </li>
            <#--订单模块-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="订单模块" lay-direction="2">
                  <i class="layui-icon layui-icon-list"></i>
                  <cite>订单模块</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/processOrderStart">制单信息</a>
                  </dd>
                </dl>
              </li>
            <#--裁床报表-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="裁床报表" lay-direction="1">
                  <i class="layui-icon layui-icon-table"></i>
                  <cite>裁床报表</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/cutReportStart">裁床单</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/cutQueryLeakStart">单款报表</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/cutMonthReportStart">月度报表</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/unitConsumptionStart">单耗报表</a>
                  </dd>
                </dl>
              </li>
            <#--花片管理-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="花片管理" lay-direction="1">
                  <i class="layui-icon layui-icon-light"></i>
                  <cite>花片管理</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/opaReportStart">花片查询</a>
                  </dd>
                </dl>
              </li>
            <#--裁片超市-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="裁片超市" lay-direction="1">
                  <i class="layui-icon layui-icon-cart"></i>
                  <cite>裁片超市</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/allStorageStart">裁片库存</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/allEmbStorageStart">衣胚库存</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/embOutAmongStart">衣胚出库查询</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/embOutPrintStart">衣胚出库打印</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/queryEmbLeakStart">衣胚出库汇总</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/cutLocationStart">裁片位置总览</a>
                  </dd>
                </dl>
              </li>
            <#--生产报表-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="生产报表" lay-direction="1">
                  <i class="layui-icon layui-icon-chart-screen"></i>
                  <cite>生产报表</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/procedureBalanceNewStart">订单工序平衡</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/groupProgressStart">区间产能汇总</a>
                  </dd>
                  <#--<dd data-name="console">-->
                    <#--<a lay-href="/erp/postTreatmentStart">完工结算</a>-->
                  <#--</dd>-->
                </dl>
              </li>
            <#--后整打菲-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="后整打菲" lay-direction="1">
                  <i class="layui-icon layui-icon-flag"></i>
                  <cite>后整打菲</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/finishTailorStart">票菲生成</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/finishTailorPrintStart">票菲打印</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/finishTailorDeleteStart">菲票管理</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/finishTailorLeakStart">后整打菲报表</a>
                  </dd>
                </dl>
              </li>
            <#--平板计件-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="平板计件" lay-direction="1">
                  <i class="layui-icon layui-icon-windows"></i>
                  <cite>平板计件</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/otherPieceWorkStart">其他计件</a>
                  </dd>
                </dl>
              </li>
            <#elseif role! == 'role15'> <#--IQC-->
            <#--工作台-->
              <li data-name="home" class="layui-nav-item layui-nav-itemed">
                <a href="javascript:;" lay-tips="工作台" lay-direction="2">
                  <i class="layui-icon layui-icon-home"></i>
                  <cite>工作台</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console" class="layui-this">
                    <a lay-href="/erp/consoleStart">开始</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/homepageNew">订单进度</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/weekReportStart">一周看板</a>
                  </dd>
                </dl>
              </li>
            <#--订单模块-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="订单模块" lay-direction="2">
                  <i class="layui-icon layui-icon-list"></i>
                  <cite>订单模块</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/processOrderStart">制单信息</a>
                  </dd>
                </dl>
              </li>
            <#--裁床报表-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="裁床报表" lay-direction="1">
                  <i class="layui-icon layui-icon-table"></i>
                  <cite>裁床报表</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/cutReportStart">裁床单</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/cutQueryLeakStart">单款报表</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/cutMonthReportStart">月度报表</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/unitConsumptionStart">单耗报表</a>
                  </dd>
                </dl>
              </li>
            <#--花片管理-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="花片管理" lay-direction="1">
                  <i class="layui-icon layui-icon-light"></i>
                  <cite>花片管理</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/opaReportStart">花片查询</a>
                  </dd>
                </dl>
              </li>
            <#--裁片超市-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="裁片超市" lay-direction="1">
                  <i class="layui-icon layui-icon-cart"></i>
                  <cite>裁片超市</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/allStorageStart">裁片库存</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/allEmbStorageStart">衣胚库存</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/embOutAmongStart">衣胚出库查询</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/embOutPrintStart">衣胚出库打印</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/queryEmbLeakStart">衣胚出库汇总</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/cutLocationStart">裁片位置总览</a>
                  </dd>
                </dl>
              </li>
            <#--面辅料管理-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="面辅料管理" lay-direction="1">
                  <i class="layui-icon layui-icon-template-1"></i>
                  <cite>面辅料管理</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/fabricAccessoryCheckStart">一站式下单</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/fabricReturnStart">面料管理</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/tmpFabricStart">暂存面料</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/accessoryInStoreStart">辅料管理</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/accessoryPreStoreStart">共用辅料</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/looseFabricStart">松布打印</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/looseFabricPrintStart">松布补打</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/fabricLoChangeStart">LO色修改</a>
                  </dd>
                </dl>
              </li>
            <#--面辅料报表-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="面辅料报表" lay-direction="1">
                  <i class="layui-icon layui-icon-align-center"></i>
                  <cite>面辅料报表</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/fabricQueryStart">面料库存</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/accessoryStorageStart">辅料库存</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/invoicingStart">面辅料进销存</a>
                  </dd>
                  <#--<dd data-name="console">-->
                    <#--<a lay-href="/erp/fabricOutRecordStart">面料出库详情</a>-->
                  <#--</dd>-->
                  <dd data-name="console">
                    <a lay-href="/erp/fabricAccessoryPrintStart">出入库打印</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/fabricAccessoryReportLeakStart">单款面辅料</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/fabricAccessoryLeakStart">面辅料汇总查询</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/looseFabricSearchStart">松布查询</a>
                  </dd>
                </dl>
              </li>
            <#--财务数据-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="财务数据" lay-direction="1">
                  <i class="layui-icon layui-icon-rmb"></i>
                  <cite>财务数据</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/empSalaryStart">个人产能</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/groupSalaryStart">分组产能</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/gatherPieceWorkReportStart">产能详情</a>
                  </dd>
                </dl>
              </li>
            <#--生产报表-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="生产报表" lay-direction="1">
                  <i class="layui-icon layui-icon-chart-screen"></i>
                  <cite>生产报表</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/procedureBalanceNewStart">订单工序平衡</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/groupProgressStart">区间产能汇总</a>
                  </dd>
                  <#--<dd data-name="console">-->
                    <#--<a lay-href="/erp/postTreatmentStart">完工结算</a>-->
                  <#--</dd>-->
                </dl>
              </li>
            <#--工厂基础信息-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="工厂基础信息" lay-direction="1">
                  <i class="layui-icon layui-icon-menu-fill"></i>
                  <cite>工厂基础信息</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/storeHouseStart">仓库信息</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/embStoreStart">衣胚仓库</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/sizeManageStart">尺码管理</a>
                  </dd>
                </dl>
              </li>
            <#elseif role! == 'role16'> <#--板房-->
            <#--工作台-->
              <li data-name="home" class="layui-nav-item layui-nav-itemed">
                <a href="javascript:;" lay-tips="工作台" lay-direction="2">
                  <i class="layui-icon layui-icon-home"></i>
                  <cite>工作台</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console" class="layui-this">
                    <a lay-href="/erp/consoleStart">开始</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/homepageNew">订单进度</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/weekReportStart">一周看板</a>
                  </dd>
                </dl>
              </li>
            <#--订单模块-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="订单模块" lay-direction="2">
                  <i class="layui-icon layui-icon-list"></i>
                  <cite>订单模块</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/processOrderStart">制单信息</a>
                  </dd>
                </dl>
              </li>
            <#--裁床报表-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="裁床报表" lay-direction="1">
                  <i class="layui-icon layui-icon-table"></i>
                  <cite>裁床报表</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/cutReportStart">裁床单</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/cutQueryLeakStart">单款报表</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/cutMonthReportStart">月度报表</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/unitConsumptionStart">单耗报表</a>
                  </dd>
                </dl>
              </li>
            <#--裁片超市-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="裁片超市" lay-direction="1">
                  <i class="layui-icon layui-icon-cart"></i>
                  <cite>裁片超市</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/allStorageStart">裁片库存</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/allEmbStorageStart">衣胚库存</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/embOutAmongStart">衣胚出库查询</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/embOutPrintStart">衣胚出库打印</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/queryEmbLeakStart">衣胚出库汇总</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/cutLocationStart">裁片位置总览</a>
                  </dd>
                </dl>
              </li>
            <#--面辅料报表-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="面辅料报表" lay-direction="1">
                  <i class="layui-icon layui-icon-align-center"></i>
                  <cite>面辅料报表</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/fabricQueryStart">面料库存</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/accessoryStorageStart">辅料库存</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/invoicingStart">面辅料进销存</a>
                  </dd>
                  <#--<dd data-name="console">-->
                    <#--<a lay-href="/erp/fabricOutRecordStart">面料出库详情</a>-->
                  <#--</dd>-->
                  <dd data-name="console">
                    <a lay-href="/erp/fabricAccessoryPrintStart">出入库打印</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/fabricAccessoryReportLeakStart">单款面辅料</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/fabricAccessoryLeakStart">面辅料汇总查询</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/looseFabricSearchStart">松布查询</a>
                  </dd>
                </dl>
              </li>
            <#--IE基础资料-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="IE基础资料" lay-direction="1">
                  <i class="layui-icon layui-icon-set-fill"></i>
                  <cite>IE基础资料</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/orderProcedureNewStart">订单工序</a>
                  </dd>
                </dl>
              </li>
            <#--生产报表-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="生产报表" lay-direction="1">
                  <i class="layui-icon layui-icon-chart-screen"></i>
                  <cite>生产报表</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/procedureBalanceNewStart">订单工序平衡</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/groupProgressStart">区间产能汇总</a>
                  </dd>
                  <#--<dd data-name="console">-->
                    <#--<a lay-href="/erp/postTreatmentStart">完工结算</a>-->
                  <#--</dd>-->
                </dl>
              </li>
            <#elseif role! == 'role17'> <#--辅料-->
            <#--工作台-->
              <li data-name="home" class="layui-nav-item layui-nav-itemed">
                <a href="javascript:;" lay-tips="工作台" lay-direction="2">
                  <i class="layui-icon layui-icon-home"></i>
                  <cite>工作台</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console" class="layui-this">
                    <a lay-href="/erp/consoleStart">开始</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/homepageNew">订单进度</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/weekReportStart">一周看板</a>
                  </dd>
                </dl>
              </li>
            <#--订单模块-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="订单模块" lay-direction="2">
                  <i class="layui-icon layui-icon-list"></i>
                  <cite>订单模块</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/processOrderStart">制单信息</a>
                  </dd>
                </dl>
              </li>
            <#--裁床报表-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="裁床报表" lay-direction="1">
                  <i class="layui-icon layui-icon-table"></i>
                  <cite>裁床报表</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/cutReportStart">裁床单</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/cutQueryLeakStart">单款报表</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/cutMonthReportStart">月度报表</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/unitConsumptionStart">单耗报表</a>
                  </dd>
                </dl>
              </li>
            <#--花片管理-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="花片管理" lay-direction="1">
                  <i class="layui-icon layui-icon-light"></i>
                  <cite>花片管理</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/opaReportStart">花片查询</a>
                  </dd>
                </dl>
              </li>
            <#--裁片超市-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="裁片超市" lay-direction="1">
                  <i class="layui-icon layui-icon-cart"></i>
                  <cite>裁片超市</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/allStorageStart">裁片库存</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/allEmbStorageStart">衣胚库存</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/embOutAmongStart">衣胚出库查询</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/embOutPrintStart">衣胚出库打印</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/queryEmbLeakStart">衣胚出库汇总</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/cutLocationStart">裁片位置总览</a>
                  </dd>
                </dl>
              </li>
            <#--面辅料管理-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="面辅料管理" lay-direction="1">
                  <i class="layui-icon layui-icon-template-1"></i>
                  <cite>面辅料管理</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/fabricAccessoryCheckStart">一站式下单</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/fabricReturnStart">面料管理</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/tmpFabricStart">暂存面料</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/accessoryInStoreStart">辅料管理</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/accessoryPreStoreStart">共用辅料</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/looseFabricStart">松布打印</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/looseFabricPrintStart">松布补打</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/fabricLoChangeStart">LO色修改</a>
                  </dd>
                </dl>
              </li>
            <#--面辅料报表-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="面辅料报表" lay-direction="1">
                  <i class="layui-icon layui-icon-align-center"></i>
                  <cite>面辅料报表</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/fabricQueryStart">面料库存</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/accessoryStorageStart">辅料库存</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/invoicingStart">面辅料进销存</a>
                  </dd>
                  <#--<dd data-name="console">-->
                    <#--<a lay-href="/erp/fabricOutRecordStart">面料出库详情</a>-->
                  <#--</dd>-->
                  <dd data-name="console">
                    <a lay-href="/erp/fabricAccessoryPrintStart">出入库打印</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/fabricAccessoryReportLeakStart">单款面辅料</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/fabricAccessoryLeakStart">面辅料汇总查询</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/looseFabricSearchStart">松布查询</a>
                  </dd>
                </dl>
              </li>
            <#--财务数据-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="财务数据" lay-direction="1">
                  <i class="layui-icon layui-icon-rmb"></i>
                  <cite>财务数据</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/empSalaryStart">个人产能</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/groupSalaryStart">分组产能</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/costBalanceStart">单款核算</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/gatherPieceWorkReportStart">产能详情</a>
                  </dd>
                </dl>
              </li>
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="财务对账" lay-direction="1">
                  <i class="layui-icon layui-icon-form"></i>
                  <cite>财务对账</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/fabricAccessoryMonthCheckStart">面辅料对账</a>
                  </dd>
                </dl>
              </li>
            <#--生产报表-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="生产报表" lay-direction="1">
                  <i class="layui-icon layui-icon-chart-screen"></i>
                  <cite>生产报表</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/procedureBalanceNewStart">订单工序平衡</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/groupProgressStart">区间产能汇总</a>
                  </dd>
                </dl>
              </li>
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="工厂基础信息" lay-direction="1">
                  <i class="layui-icon layui-icon-menu-fill"></i>
                  <cite>工厂基础信息</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/customerStart">客户</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/supplierInfoStart">供应商</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/storeHouseStart">仓库信息</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/embStoreStart">衣胚仓库</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/sizeManageStart">尺码管理</a>
                  </dd>
                </dl>
              </li>
            <#elseif role! == 'role18'><#--根-->
            <#--裁床打菲-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="票菲管理" lay-direction="1">
                  <i class="layui-icon layui-icon-note"></i>
                  <cite>票菲管理</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/multiTailorStart">主身生成</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/otherMultiTailorStart">配料生成</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/printPartStart">部位管理</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/tailorReprintStart">票菲打印</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/tailorChangeCodeStart">票菲改码</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/bedTailorInfoStart">票菲删除</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/recoveryStart">误删恢复</a>
                  </dd>
                </dl>
              </li>
            <#--裁床报表-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="裁床报表" lay-direction="1">
                  <i class="layui-icon layui-icon-table"></i>
                  <cite>裁床报表</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/cutReportStart">裁床单</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/cutQueryLeakStart">单款报表</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/cutMonthReportStart">月度报表</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/unitConsumptionStart">单耗报表</a>
                  </dd>
                </dl>
              </li>
            <#elseif role! == 'role19'> <#--外发-->
            <#--工作台-->
              <li data-name="home" class="layui-nav-item layui-nav-itemed">
                <a href="javascript:;" lay-tips="工作台" lay-direction="2">
                  <i class="layui-icon layui-icon-home"></i>
                  <cite>工作台</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console" class="layui-this">
                    <a lay-href="/erp/consoleStart">开始</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/homepageNew">订单进度</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/weekReportStart">一周看板</a>
                  </dd>
                </dl>
              </li>
            <#--订单模块-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="订单模块" lay-direction="2">
                  <i class="layui-icon layui-icon-list"></i>
                  <cite>订单模块</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/processOrderStart">制单信息</a>
                  </dd>
                </dl>
              </li>
            <#--裁床报表-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="裁床报表" lay-direction="1">
                  <i class="layui-icon layui-icon-table"></i>
                  <cite>裁床报表</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/cutReportStart">裁床单</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/cutQueryLeakStart">单款报表</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/cutMonthReportStart">月度报表</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/unitConsumptionStart">单耗报表</a>
                  </dd>
                </dl>
              </li>
            <#--花片管理-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="花片管理" lay-direction="1">
                  <i class="layui-icon layui-icon-light"></i>
                  <cite>花片管理</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/opaReportStart">花片查询</a>
                  </dd>
                </dl>
              </li>
            <#--裁片超市-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="裁片超市" lay-direction="1">
                  <i class="layui-icon layui-icon-cart"></i>
                  <cite>裁片超市</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/allStorageStart">裁片库存</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/allEmbStorageStart">衣胚库存</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/embOutAmongStart">衣胚出库查询</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/embOutPrintStart">衣胚出库打印</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/queryEmbLeakStart">衣胚出库汇总</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/cutLocationStart">裁片位置总览</a>
                  </dd>
                </dl>
              </li>
            <#--面辅料报表-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="面辅料报表" lay-direction="1">
                  <i class="layui-icon layui-icon-align-center"></i>
                  <cite>面辅料报表</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/fabricAccessoryReportLeakStart">单款面辅料</a>
                  </dd>
                </dl>
              </li>
            <#--生产报表-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="生产报表" lay-direction="1">
                  <i class="layui-icon layui-icon-chart-screen"></i>
                  <cite>生产报表</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/procedureBalanceNewStart">订单工序平衡</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/groupProgressStart">区间产能汇总</a>
                  </dd>
                  <#--<dd data-name="console">-->
                  <#--<a lay-href="/erp/postTreatmentStart">完工结算</a>-->
                  <#--</dd>-->
                </dl>
              </li>
            <#--后整打菲-->
              <li data-name="component" class="layui-nav-item">
                <a href="javascript:;" lay-tips="后整打菲" lay-direction="1">
                  <i class="layui-icon layui-icon-flag"></i>
                  <cite>后整打菲</cite>
                </a>
                <dl class="layui-nav-child">
                  <dd data-name="console">
                    <a lay-href="/erp/finishTailorStart">票菲生成</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/finishTailorPrintStart">票菲打印</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/finishTailorDeleteStart">菲票管理</a>
                  </dd>
                  <dd data-name="console">
                    <a lay-href="/erp/finishTailorLeakStart">后整打菲报表</a>
                  </dd>
                </dl>
              </li>
            </#if>
          </ul>
        </div>
      </div>

      <!-- 页面标签 -->
      <div class="layadmin-pagetabs" id="LAY_app_tabs">
        <div class="layui-icon layadmin-tabs-control layui-icon-prev" layadmin-event="leftPage"></div>
        <div class="layui-icon layadmin-tabs-control layui-icon-next" layadmin-event="rightPage"></div>
        <div class="layui-icon layadmin-tabs-control layui-icon-down">
          <ul class="layui-nav layadmin-tabs-select" lay-filter="layadmin-pagetabs-nav">
            <li class="layui-nav-item" lay-unselect>
              <a href="javascript:;"></a>
              <dl class="layui-nav-child layui-anim-fadein">
                <dd layadmin-event="closeThisTabs"><a href="javascript:;">关闭当前标签页</a></dd>
                <dd layadmin-event="closeOtherTabs"><a href="javascript:;">关闭其它标签页</a></dd>
                <dd layadmin-event="closeAllTabs"><a href="javascript:;">关闭全部标签页</a></dd>
              </dl>
            </li>
          </ul>
        </div>
        <div class="layui-tab" lay-unauto lay-allowClose="true" lay-filter="layadmin-layout-tabs">
          <ul class="layui-tab-title" id="LAY_app_tabsheader">
            <li lay-id="/erp/consoleStart" lay-attr="/erp/consoleStart" class="layui-this"><i class="layui-icon layui-icon-home"></i></li>
          </ul>
        </div>
      </div>
      
      
      <!-- 主体内容 -->
      <div class="layui-body" id="LAY_app_body">
        <div class="layadmin-tabsbody-item layui-show">
          <iframe src="/erp/consoleStart" frameborder="0" class="layadmin-iframe"></iframe>
        </div>
      </div>
      
      <!-- 辅助元素，一般用于移动设备下遮罩 -->
      <div class="layadmin-body-shade" layadmin-event="shade"></div>
    </div>
  </div>

  <script src="/layuiadmin/layui/layui.js"></script>
  <script src="/js/common/jquery.js"></script>
  <script src="/js/userInfo/passWord.js?t=${currentDate?c}"></script>
</body>
</html>


