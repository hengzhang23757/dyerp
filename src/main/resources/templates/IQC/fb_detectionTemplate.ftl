<#macro search>
<section id="content">
    <section class="vbox">
        <section class="scrollable">
            <#--<div class="layui-row">-->
                <#--<form class="layui-form" style="margin: 0 auto;padding-top: 3px;" lay-filter="formBaseInfo">-->
                    <#--<table>-->
                        <#--<tr>-->
                            <#--<td style="text-align: right;margin-bottom: 15px;">-->
                                <#--<label class="layui-form-label" style="width: 100px">面料名</label>-->
                            <#--</td>-->
                            <#--<td style="margin-bottom: 15px;">-->
                                <#--<input type="text" id="fabricName" autocomplete="off" class="layui-input">-->
                            <#--</td>-->
                            <#--<td style="text-align: right;margin-bottom: 15px;">-->
                                <#--<label class="layui-form-label" style="width: 100px">面料号</label>-->
                            <#--</td>-->
                            <#--<td style="margin-bottom: 15px;">-->
                                <#--<input type="text" id="fabricNumber" autocomplete="off" class="layui-input">-->
                            <#--</td>-->
                            <#--<td style="text-align: right;margin-bottom: 15px;">-->
                                <#--<label class="layui-form-label"></label>-->
                            <#--</td>-->
                            <#--<td style="margin-bottom: 15px;">-->
                                <#--<div class="layui-input-inline">-->
                                    <#--<button class="layui-btn" lay-submit lay-filter="searchBeat">搜索</button>-->
                                <#--</div>-->
                            <#--</td>-->
                        <#--</tr>-->
                    <#--</table>-->
                <#--</form>-->
            <#--</div>-->
            <table id="dataTable" lay-filter="dataTable"></table>

            <script type="text/html" id="toolbarTop">
                <div class="layui-btn-container">
                    <button class="layui-btn layui-btn-sm" lay-event="refresh">刷新</button>
                    <button class="layui-btn layui-btn-sm" lay-event="clearFilter">清除所有筛选</button>
                    <button class="layui-btn layui-btn-sm" lay-event="addNew">添加</button>
                    <button class="layui-btn layui-btn-sm" lay-event="exportExcel">导出</button>
                </div>
            </script>

            <script type="text/html" id="barTop">
                <a class="layui-btn layui-btn-xs" lay-event="update">修改</a>
                <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
            </script>

            <div class="layui-row" id="templateOperate" style="display: none">
                <form class="layui-form" style="margin: 0 auto;padding-top: 3px;" lay-filter="templateAdd">
                    <table>
                        <tr>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label" style="width: 100px">类别</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <select type="text" id="detectionType" name="detectionType" autocomplete="off" class="layui-input">
                                    <option value="工艺测试">工艺测试</option>
                                    <option value="面料测试">面料测试</option>
                                    <option value="洗水测试">洗水测试</option>
                                </select>
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label" style="width: 100px">名称</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" id="detectionName" name="detectionName" autocomplete="off" class="layui-input">
                            </td>
                        </tr>
                    </table>
                    <form class="layui-form">
                        <div class="layui-row" >
                            <div style="width: 90%; padding-left: 30px; margin-top: 30px;">
                                <textarea id="detectionTemplateTextarea"></textarea>
                            </div>
                        </div>
                    </form>
                </form>
            </div>
        </section>
    </section>
</section>

    <link rel="stylesheet" href="/css/layui.css" type="text/css">
    <link rel="stylesheet" href="/css/step.css" type="text/css">
    <link rel="stylesheet" href="/css/soulTable.css" type="text/css">
    <link rel="stylesheet" href="/css/publish.css" type="text/css">
    <link rel="stylesheet" href="/css/selectize.bootstrap3.css" type="text/css">

    <script src="/js/common/layer.js"></script>
    <script src="/js/step/step.js?t=${currentDate?c}" type="text/javascript"></script>
    <script src="/js/common/xm-select.js?t=${currentDate?c}" type="text/javascript"></script>
    <script src="/js/common/selectize.js" type="text/javascript"></script>
    <script src="/js/IQC/detectionTemplate.js?t=${currentDate?c}"></script>
    <script type="text/javascript" src="/js/common/tinymce/tinymce.min.js"></script>
    <script type="text/javascript" src="/js/common/tinymce/zh_CN.js"></script>
    <script src="/js/common/moment.min.js" type="text/javascript"></script>
</#macro>

<style>

    ::-webkit-scrollbar-track-piece {

        background-color:#f8f8f8;

    }

    ::-webkit-scrollbar {

        width:15px;

        height:15px;

    }

    ::-webkit-scrollbar-thumb {

        background-color:#dddddd;

        background-clip:padding-box;

        min-height:28px;

    }

    ::-webkit-scrollbar-thumb:hover {

        background-color:#bbb;

    }
</style>
