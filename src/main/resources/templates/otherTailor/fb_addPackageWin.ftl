<#macro entities>
    <div id="addPackageWin" style="display: none;cursor:default">
        <div style="text-align: right;">
            <a id="addPackageWinNo" href="#" style="font-size:20px"><i class="fa fa-times icon" style="color: rgb(182,182,182)"></i></a>
        </div>
        <div style="overflow-x: auto;overflow-y:auto;height: 350px">
            <table class="col-md-12"  id="packageTableDiv">
            </table>
        </div>
        <div class="col-sm-12" style="padding-top: 10px">
            <button id="addPackageWinYes" class="btn btn-s-lg" style="border-radius: 5px;color: white;font-family: PingFangSC-Semibold, sans-serif;"  style="text-align: center;">保存</button>
        </div>
    </div>
</#macro>

<style>
    button {
        background:rgb(45, 202, 147);
        opacity:0.86;
        color: white;
        font-family: PingFangSC-Semibold, sans-serif;
    }
</style>