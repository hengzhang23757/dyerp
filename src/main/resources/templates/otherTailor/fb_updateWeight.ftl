<#macro entities>
    <div id="updateWeight" style="display: none;cursor:default">
        <div style="overflow-x: auto;overflow-y:auto;">
            <div style="text-align: right;margin-bottom:20px">
                <a id="updateWeightNo" href="#" style="font-size:20px"><i class="fa fa-times icon" style="color: rgb(182,182,182)"></i></a>
            </div>
            <table style="border-collapse:separate;border-spacing: 10px;width: 100%;">
                <tr>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">原始重量</label>
                    </td>
                    <td>
                        <input id="weight1" class="form-control" autocomplete="off" style="width: 100%;margin-bottom: 15px;">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">修改成</label>
                    </td>
                    <td>
                        <input id="updateToWeight" class="form-control" autocomplete="off" style="margin-bottom: 15px;width: 100%;">
                    </td>
                </tr>

            </table>
        </div>
        <div class="col-sm-12" style="padding-top: 10px">
            <button id="updateWeightYes" class="btn btn-s-lg" style="border-radius: 5px;text-align: center;color:white;font-family: PingFangSC-Semibold;">保存</button>
        </div>
    </div>
</#macro>