<#macro search>
    <section id="content">
        <section class="vbox">
            <section class="scrollable">
                <table id="dataTable" lay-filter="dataTable"></table>
                <script type="text/html" id="toolbarTop">
                    <div class="layui-btn-container">
                        <button class="layui-btn layui-btn-sm" lay-event="refresh">刷新</button>
                        <button class="layui-btn layui-btn-sm" lay-event="add">添加</button>
                    </div>
                </script>
                <script type="text/html" id="barTop">
                    <a class="layui-btn layui-btn-xs" lay-event="update">修改</a>
                    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
                </script>

                <div id="operateDiv" hidden>
                    <form class="layui-form" style="margin: 0 auto;padding-top: 3px;" lay-filter="formBaseInfo">
                        <table>
                            <tr>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label" style="width: 100px">客户名</label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <input type="text" id="customerName" name="customerName" autocomplete="off" class="layui-input">
                                </td>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label" style="width: 100px">客户代码</label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <input type="text" id="customerCode" name="customerCode" autocomplete="off" class="layui-input">
                                </td>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label" style="width: 100px">公司名</label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <input type="text" id="companyName" name="companyName" autocomplete="off" class="layui-input">
                                </td>
                            </tr><tr>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label" style="width: 100px">联系人</label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <input type="text" id="linkmanName" name="linkmanName" autocomplete="off" class="layui-input">
                                </td>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label" style="width: 100px">电话</label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <input type="text" id="linkmanPhone" name="linkmanPhone" autocomplete="off" class="layui-input">
                                </td>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label" style="width: 100px">公司地址</label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <input type="text" id="companyAddress" name="companyAddress" autocomplete="off" class="layui-input">
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
            </section>
        </section>
    </section>
    <link rel="stylesheet" href="/css/layui.css" type="text/css">
    <link rel="stylesheet" href="/css/soulTable.css" type="text/css">
    <script src="/js/basicData/customer.js?t=${currentDate?c}"></script>

</#macro>

<style>

    ::-webkit-scrollbar-track-piece {

        background-color:#f8f8f8;

    }

    ::-webkit-scrollbar {

        width:15px;

        height:15px;

    }

    ::-webkit-scrollbar-thumb {

        background-color:#dddddd;

        background-clip:padding-box;

        min-height:28px;

    }

    ::-webkit-scrollbar-thumb:hover {

        background-color:#bbb;

    }
</style>