<#macro search>
<section id="content">
    <section class="vbox">
        <section class="scrollable">

            <div class="layui-row">
                <form class="layui-form" style="margin: 0 auto;padding-top: 3px;" lay-filter="baseInfo">
                    <table>
                        <tr>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label" style="width: 100px">单号</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" id="clothesVersionNumber" placeholder="输入单号" autocomplete="off" class="layui-input">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label" style="width: 100px">款号</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" id="orderName" autocomplete="off" placeholder="输入款号" class="layui-input">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label" style="width: 100px">类别</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <select type="text" id="type" autocomplete="off" class="layui-input">
                                    <option value="面料">面料</option>
                                    <option value="辅料">辅料</option>
                                    <option value="扁机">扁机</option>
                                    <option value="共用辅料">共用辅料</option>
                                </select>
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label" style="width: 100px">状态</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <select type="text" id="preCheck" autocomplete="off" class="layui-input">
                                    <option value="">全部</option>
                                    <option value="已提交" selected>已提交</option>
                                    <option value="未提交">未提交</option>
                                    <option value="通过">通过</option>
                                    <option value="未通过">未通过</option>
                                </select>
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label"></label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <div class="layui-input-inline">
                                    <button class="layui-btn" lay-submit lay-filter="searchBeat">搜索</button>
                                </div>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>


            <table id="fabricAccessoryTable" lay-filter="fabricAccessoryTable"></table>
<#--            <div style="display: none"><table id="fabricAccessoryExcelTable" lay-filter="fabricAccessoryExcelTable"></table></div>-->

            <script type="text/html" id="toolbarTop">
                <div class="layui-btn-container">
                    <button class="layui-btn layui-btn-sm" lay-event="clearFilter">清除所有筛选</button>
                </div>
            </script>

            <script type="text/html" id="barTopFabric">
                <a class="layui-btn layui-btn-xs layui-btn-normal" lay-event="reviewFabric">审核</a>
            </script>
            <script type="text/html" id="barTopAccessory">
                <a class="layui-btn layui-btn-xs layui-btn-normal" lay-event="reviewAccessory">审核</a>
            </script>
            <script type="text/html" id="barTopBj">
                <a class="layui-btn layui-btn-xs layui-btn-normal" lay-event="reviewBj">审核</a>
            </script>
            <script type="text/html" id="barTopShareAccessory">
                <a class="layui-btn layui-btn-xs layui-btn-normal" lay-event="reviewShareAccessory">审核</a>
            </script>

        </section>
    </section>
</section>

    <link rel="stylesheet" href="/css/layui.css" type="text/css">
    <script src="/js/common/layer.js"></script>
    <script src="/js/common/moment.min.js" type="text/javascript"></script>
    <script src="/js/basicInfo/fabricAccessoryPreCheck.js?t=${currentDate?c}"></script>
</#macro>

<style>
    ::-webkit-scrollbar-track-piece {

        background-color:#f8f8f8;

    }

    ::-webkit-scrollbar {

        width:15px;

        height:15px;

    }

    ::-webkit-scrollbar-thumb {

        background-color:#dddddd;

        background-clip:padding-box;

        min-height:28px;

    }

    ::-webkit-scrollbar-thumb:hover {

        background-color:#bbb;

    }
</style>
