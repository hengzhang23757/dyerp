<#macro search>
<section id="content">
    <section class="vbox">
        <section class="scrollable">
            <table id="fabricAccessoryTable" lay-filter="fabricAccessoryTable"></table>

            <script type="text/html" id="toolbarTop">
                <div class="layui-btn-container">
                    <button class="layui-btn layui-btn-sm" lay-event="clearFilter">清除所有筛选</button>
                    <button class="layui-btn layui-btn-sm" lay-event="addFabricPlaceOrder">面料下单/修改</button>
                    <button class="layui-btn layui-btn-sm" lay-event="addAccessoryPlaceOrder">辅料下单/修改</button>
                    <button class="layui-btn layui-btn-sm" lay-event="exportExcel">导出</button>
                </div>
            </script>

            <script type="text/html" id="barTop">
                <a class="layui-btn layui-btn-xs" lay-event="detail">详情</a>
            </script>

            <script type="text/html" id="fabricChildBar">
                <a class="layui-btn layui-btn-xs layui-btn-normal" onclick="updateFabricDetailData(this)">修改</a>
            </script>

            <script type="text/html" id="accessoryChildBar">
                <a class="layui-btn layui-btn-xs layui-btn-normal" onclick="updateAccessoryDetailData(this)">修改</a>
            </script>
        </section>
    </section>
    <#include "fb_fabricPlaceOrderAdd.ftl">
    <@fabricOrderAdd></@fabricOrderAdd>
    <#include "fb_accessoryPlaceOrderAdd.ftl">
    <@accessoryOrderAdd></@accessoryOrderAdd>
    <#include "fb_fabricPlaceOrderUpdate.ftl">
    <@fabricOrderUpdate></@fabricOrderUpdate>
    <#include "fb_accessoryPlaceOrderUpdate.ftl">
    <@accessoryOrderUpdate></@accessoryOrderUpdate>
</section>

    <link rel="stylesheet" href="/css/layui.css" type="text/css">
    <link rel="stylesheet" href="/css/step.css" type="text/css">
    <link rel="stylesheet" href="/css/soulTable.css" type="text/css">
    <link rel="stylesheet" href="/css/publish.css" type="text/css">
    <link rel="stylesheet" href="/css/cropper.css" type="text/css">
    <link rel="stylesheet" href="/css/selectize.bootstrap3.css" type="text/css">
    <link rel="stylesheet" href="/css/zoom.css" type="text/css">

    <#--<script src="/js/common/layui.js" type="text/javascript"></script>-->
    <script src="/js/common/layer.js"></script>
    <script src="/js/step/step.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/common/move.js"></script>
    <script type="text/javascript" src="/js/common/publishImg.js"></script>
    <script src="/js/common/xm-select.js?t=${currentDate?c}" type="text/javascript"></script>
    <script src="/js/common/selectize.js" type="text/javascript"></script>
    <script src="/js/common/zoom.js" type="text/javascript"></script>
    <script src="/js/common/moment.min.js" type="text/javascript"></script>
    <script src="/js/basicInfo/fabricAccessory.js?t=${currentDate?c}"></script>
</#macro>

<style>
    button {
        background:rgb(45, 202, 147);
        opacity:0.86;
        color: white;
        font-family: PingFangSC-Semibold, sans-serif;
    }
    ::-webkit-scrollbar-track-piece {

        background-color:#f8f8f8;

    }

    ::-webkit-scrollbar {

        width:15px;

        height:15px;

    }

    ::-webkit-scrollbar-thumb {

        background-color:#dddddd;

        background-clip:padding-box;

        min-height:28px;

    }

    ::-webkit-scrollbar-thumb:hover {

        background-color:#bbb;

    }
</style>
