<#macro search>
<section id="content">
    <section class="vbox">
        <section class="scrollable">
            <div class="layui-row">
                <form class="layui-form" style="margin: 0 auto;padding-top: 3px;" lay-filter="baseInfo">
                    <table>
                        <tr>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label" style="width: 100px">开始日期</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" name="from" class="layui-input" id="from">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label" style="width: 100px">结束日期</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" name="to" id="to" class="layui-input">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label" style="width: 100px">类型</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <select type="text" name="searchType" id="searchType">
                                    <option value="面料入库">面料入库</option>
                                    <option value="面料出库">面料出库</option>
                                    <option value="辅料入库">辅料入库</option>
                                    <option value="辅料出库">辅料出库</option>
                                </select>
                            </td>
                            <td>
                                <label class="layui-form-label"></label>
                            </td>
                            <td>
                                <button class="layui-btn" lay-submit lay-filter="search">搜索</button>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>

            <table id="reportTable" lay-filter="reportTable"></table>

            <script type="text/html" id="toolbarTop">
                <div class="layui-btn-container">
                    <button class="layui-btn layui-btn-sm" lay-event="exportExcel">导出</button>
                    <button class="layui-btn layui-btn-sm" lay-event="print">打印</button>
                </div>
            </script>

        </section>
    </section>
</section>
    <link rel="stylesheet" href="/css/layui.css" type="text/css">
    <link rel="stylesheet" href="/css/soulTable.css" type="text/css">
    <script src="/js/common/moment.min.js" type="text/javascript"></script>
    <script src="/js/common/LodopFuncs.js" type="text/javascript" ></script>
    <script src="/js/basicInfo/fabricAccessoryPrint.js?t=${currentDate?c}"></script>
</#macro>

<style>

    ::-webkit-scrollbar-track-piece {

        background-color:#f8f8f8;

    }

    ::-webkit-scrollbar {

        width:15px;

        height:15px;

    }

    ::-webkit-scrollbar-thumb {

        background-color:#dddddd;

        background-clip:padding-box;

        min-height:28px;

    }

    ::-webkit-scrollbar-thumb:hover {

        background-color:#bbb;

    }
</style>

