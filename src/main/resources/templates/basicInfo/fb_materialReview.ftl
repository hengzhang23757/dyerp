<#macro materialReview>
    <div id="materialReview">
        <form class="layui-form" style="margin: 0 auto;max-width:100%;padding-top: 20px;">
            <table class="layui-hide" id="materialReviewTable" lay-filter="materialReviewTable"></table>
        </form>
    </div>
</#macro>