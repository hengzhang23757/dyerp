<#macro accessoryOrderUpdate>
    <div id="accessoryOrderUpdate" style="display: none;cursor:default;">
        <form class="layui-form" style="margin: 5px;padding-top: 40px;" lay-filter="accessoryOrderUpdate">
            <div class="layui-row layui-col-space10">
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">辅料名称</label>
                    <div class="layui-input-inline">
                        <input type="text" name="accessoryName" autocomplete="off" class="layui-input" readonly>
                    </div>
                </div>
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">规格</label>
                    <div class="layui-input-inline">
                        <input type="text" name="specification" autocomplete="off" class="layui-input" readonly>
                    </div>
                </div>
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">单位</label>
                    <div class="layui-input-inline">
                        <input type="text" name="accessoryUnit" autocomplete="off" class="layui-input" readonly>
                    </div>
                </div>
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">辅料颜色</label>
                    <div class="layui-input-inline">
                        <input type="text" name="accessoryColor" autocomplete="off" class="layui-input" readonly>
                    </div>
                </div>
            </div>
            <div class="layui-row layui-col-space10">
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">接单用量</label>
                    <div class="layui-input-inline">
                        <input type="text" name="orderPieceUsage" autocomplete="off" class="layui-input" readonly>
                    </div>
                </div>
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">单件用量</label>
                    <div class="layui-input-inline">
                        <input type="text" name="pieceUsage" autocomplete="off" class="layui-input" readonly>
                    </div>
                </div>
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">成衣尺码</label>
                    <div class="layui-input-inline">
                        <input type="text" name="sizeName" autocomplete="off" class="layui-input" readonly>
                    </div>
                </div>
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">成衣颜色</label>
                    <div class="layui-input-inline">
                        <input type="text" name="colorName" autocomplete="off" class="layui-input" readonly>
                    </div>
                </div>
            </div>
            <div class="layui-row layui-col-space10">
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">供应商</label>
                    <div class="layui-input-inline">
                        <input type="text" name="supplier" autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">订单量</label>
                    <div class="layui-input-inline">
                        <input type="text" name="orderCount" autocomplete="off" class="layui-input" readonly>
                    </div>
                </div>
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">上浮(%)</label>
                    <div class="layui-input-inline">
                        <input type="text" name="accessoryAdd" autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">订购量</label>
                    <div class="layui-input-inline">
                        <input type="text" name="accessoryCount" autocomplete="off" class="layui-input">
                    </div>
                </div>
            </div>

            <div class="layui-row layui-col-space10">
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">单价</label>
                    <div class="layui-input-inline">
                        <input type="text" name="price" autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">总金额</label>
                    <div class="layui-input-inline">
                        <input type="text" name="sumMoney" autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">是否通用</label>
                    <div class="layui-input-inline">
                        <input type="text" name="accessoryType" autocomplete="off" class="layui-input">
                    </div>
                </div>
            </div>
        </form>
    </div>
</#macro>

<style>
    .layui-col-md3 .layui-form-label {
        width:90px
    }
    .layui-col-md6 .layui-form-label {
        width:90px
    }
    .layui-col-md12 .layui-form-label {
        width:90px
    }

    #staffAdd .text {
        width: 8%;
        text-align: center;
    }

    #staffAdd .content {
        width: 17%;
    }

    #staffAdd input {
        border-radius: 0;
        border-color: white;
    }

    #staffAdd select {
        border-color: white;
    }

    #staffAdd td {
        border: solid black;
        border-width: 0px 1px 1px 0px;
    }

    #staffAdd table {
        border: solid black;
        border-width: 1px 0px 0px 1px;
    }
</style>