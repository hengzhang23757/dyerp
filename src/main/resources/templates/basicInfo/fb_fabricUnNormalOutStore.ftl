<#macro fabricUnNormalOutStore>
    <div id="fabricUnNormalOutStore" style="cursor:default;" xmlns="http://www.w3.org/1999/html">
        <form class="layui-form" style="margin: 5px;padding-top: 40px;" lay-filter="unNormalOutStore">
            <div class="layui-row layui-col-space10">
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">面料名称</label>
                    <div class="layui-input-inline">
                        <input type="text" name="fabricName" autocomplete="off" class="layui-input" readonly="true">
                    </div>
                </div>
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">面料号</label>
                    <div class="layui-input-inline">
                        <input type="text" name="fabricNumber" autocomplete="off" class="layui-input" readonly="true">
                    </div>
                </div>
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">色组</label>
                    <div class="layui-input-inline">
                        <input type="text" name="colorName" autocomplete="off" class="layui-input" readonly="true">
                    </div>
                </div>
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">是否撞色</label>
                    <div class="layui-input-inline">
                        <input type="text" name="isHit" autocomplete="off" class="layui-input" readonly="true">
                    </div>
                </div>
            </div>
            <div class="layui-row layui-col-space10">
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">面料颜色</label>
                    <div class="layui-input-inline">
                        <input type="text" name="fabricColor" autocomplete="off" class="layui-input" readonly="true">
                    </div>
                </div>
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">面料色号</label>
                    <div class="layui-input-inline">
                        <input type="text" name="fabricColorNumber" autocomplete="off" class="layui-input" readonly="true">
                    </div>
                </div>
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">供应商</label>
                    <div class="layui-input-inline">
                        <input type="text" name="supplier" autocomplete="off" class="layui-input" readonly="true">
                    </div>
                </div>
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">单位</label>
                    <div class="layui-input-inline">
                        <input type="text" name="unit" autocomplete="off" class="layui-input" readonly="true">
                    </div>
                </div>

            </div>
            <div class="layui-row layui-col-space10">
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">缸号</label>
                    <div class="layui-input-inline">
                        <input type="text" name="jarName" autocomplete="off" class="layui-input" readonly="true">
                    </div>
                </div>
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">卷数</label>
                    <div class="layui-input-inline">
                        <input type="text" name="batchNumber" autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">重量</label>
                    <div class="layui-input-inline">
                        <input type="text" name="weight" autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">入库时间</label>
                    <div class="layui-input-inline">
                        <input type="text" name="returnTime" id="updateStorageTime" autocomplete="off" class="layui-input" readonly="true">
                    </div>
                </div>

            </div>
            <div class="layui-row layui-col-space10">
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">位置</label>
                    <div class="layui-input-inline">
                        <input type="text" name="location" autocomplete="off" class="layui-input" readonly="true">
                    </div>
                </div>
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">类型</label>
                    <div class="layui-input-inline">
                        <select type="text" name="operateType" autocomplete="off" class="layui-input">
                            <option value="">出库类型</option>
                            <option value="外发加工">外发加工</option>
                            <option value="退面料厂">退面料厂</option>
                            <option value="余布出售">余布出售</option>
                            <option value="面料异常">面料异常</option>
                        </select>
                    </div>
                </div>
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">接收</label>
                    <div class="layui-input-inline">
                        <input type="text" name="receiver" autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-col-md3"></div>
            </div>
        </form>
    </div>
</#macro>

<style>
    .layui-col-md3 .layui-form-label {
        width:90px
    }
    .layui-col-md6 .layui-form-label {
        width:90px
    }
    .layui-col-md12 .layui-form-label {
        width:90px
    }

    #staffAdd .text {
        width: 8%;
        text-align: center;
    }

    #staffAdd .content {
        width: 17%;
    }

    #staffAdd input {
        border-radius: 0;
        border-color: white;
    }

    #staffAdd select {
        border-color: white;
    }

    #staffAdd td {
        border: solid black;
        border-width: 0px 1px 1px 0px;
    }

    #staffAdd table {
        border: solid black;
        border-width: 1px 0px 0px 1px;
    }
</style>