<#macro materialProgress>
    <div id="materialProgress">
        <form class="layui-form" style="margin: 0 auto;max-width:100%;padding-top: 20px;">
            <table class="layui-hide" id="materialProgressTable" lay-filter="materialProgressTable"></table>
        </form>
    </div>
</#macro>