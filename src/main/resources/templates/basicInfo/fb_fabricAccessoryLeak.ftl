<#macro search>
    <section id="content">
        <section class="vbox">
            <section class="scrollable">
                <div class="layui-row">
                    <form class="layui-form" style="margin: 0 auto;padding-top: 3px;" lay-filter="baseInfo">
                        <table>
                            <tr>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label">品牌</label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <div id="xmCustomer" class="xm-select-demo"></div>
                                </td>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label">季度</label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <div id="xmSeason" class="xm-select-demo"></div>
                                </td>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label">组别系列</label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <select type="text" name="modelType" id="modelType">
                                        <option value="">请选择</option>
                                        <option value="期货">期货</option>
                                        <option value="快返">快返</option>
                                        <option value="直播">直播</option>
                                        <option value="电商">电商</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label">单号</label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <input type="text" name="clothesVersionNumber" id="clothesVersionNumber" placeholder="请输入单号" class="layui-input">
                                </td>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label">款号</label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <input type="text" name="orderName" id="orderName" placeholder="请输入款号" class="layui-input">
                                </td>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label">类型</label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <select type="text" name="type" id="type">
                                        <option value="面料">面料</option>
                                        <option value="辅料">辅料</option>
                                        <option value="面料金额">面料金额</option>
                                        <option value="辅料金额">辅料金额</option>
                                    </select>
                                </td>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label"></label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <div class="layui-input-inline">
                                        <button class="layui-btn" lay-submit lay-filter="searchBeat">搜索</button>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>

                <table id="reportTable" lay-filter="reportTable"></table>

                <script type="text/html" id="toolbarTop">
                    <div class="layui-btn-container">
                        <button class="layui-btn layui-btn-sm" lay-event="exportExcel">导出</button>
                    </div>
                </script>

                <script type="text/html" id="barTopOne">
                    <a class="layui-btn layui-btn-sm" lay-event="detail">详情</a>
                </script>

                <script type="text/html" id="barTopTwo">
                    <a class="layui-btn layui-btn-normal layui-btn-sm" lay-event="leakDetail">未齐查看</a>
                </script>

                <div class="layui-row" id="fabricAccessoryTab" style="display: none">
                    <div class="layui-tab" lay-filter="docDemoTabBrief">
                        <ul class="layui-tab-title">
                            <li class="layui-this" lay-id="1">汇总</li>
                            <li lay-id="2">入库</li>
                            <li lay-id="3">库存</li>
                            <li lay-id="4">暂存</li>
                            <li lay-id="5">出库</li>
                        </ul>
                        <div class="layui-tab-content">
                            <div class="layui-tab-item layui-show">
                                <table id="summaryTable" lay-filter="summaryTable"></table>
                            </div>
                            <div class="layui-tab-item">
                                <table id="inStoreTable" lay-filter="inStoreTable"></table>
                            </div>
                            <div class="layui-tab-item">
                                <table id="storageTable" lay-filter="storageTable"></table>
                            </div>
                            <div class="layui-tab-item">
                                <table id="tmpStorageTable" lay-filter="tmpStorageTable"></table>
                            </div>
                            <div class="layui-tab-item">
                                <table id="outStoreTable" lay-filter="outStoreTable"></table>
                            </div>
                        </div>
                    </div>
                </div>

            </section>
        </section>
    </section>

    <link rel="stylesheet" href="/css/layui.css" type="text/css">
    <link rel="stylesheet" href="/css/soulTable.css" type="text/css">
    <script src="/js/common/xm-select.js?t=${currentDate?c}" type="text/javascript"></script>
    <script src="/js/common/moment.min.js" type="text/javascript"></script>
    <script src="/js/basicInfo/fabricAccessoryLeak.js?t=${currentDate?c}"></script>
</#macro>

<style>
    .layui-form-label {
        width:120px !important;
    }

    /*.layui-table-cell{*/
        /*height:36px !important;*/
        /*line-height: 36px !important;*/
    /*}*/

    ::-webkit-scrollbar-track-piece {

        background-color:#f8f8f8;

    }

    ::-webkit-scrollbar {

        width:15px;

        height:15px;

    }

    ::-webkit-scrollbar-thumb {

        background-color:#dddddd;

        background-clip:padding-box;

        min-height:28px;

    }

    ::-webkit-scrollbar-thumb:hover {

        background-color:#bbb;

    }
</style>
