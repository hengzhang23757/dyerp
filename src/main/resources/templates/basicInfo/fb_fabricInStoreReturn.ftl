<#macro fabricInStoreReturn>
    <div id="fabricInStoreReturn" style="cursor:default;">
        <form class="layui-form" style="margin: 5px;padding-top: 40px;" lay-filter="inStoreReturn">
            <div class="layui-row layui-col-space10">
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">面料名称</label>
                    <div class="layui-input-inline">
                        <input type="text" name="fabricName" autocomplete="off" class="layui-input" readonly="true">
                    </div>
                </div>
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">色组</label>
                    <div class="layui-input-inline">
                        <input type="text" name="colorName" autocomplete="off" class="layui-input" readonly="true">
                    </div>
                </div>
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">面料颜色</label>
                    <div class="layui-input-inline">
                        <input type="text" name="fabricColor" autocomplete="off" class="layui-input" readonly="true">
                    </div>
                </div>
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">单位</label>
                    <div class="layui-input-inline">
                        <input type="text" name="unit" autocomplete="off" class="layui-input" readonly="true">
                    </div>
                </div>
            </div>

            <div class="layui-row layui-col-space10">
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">缸号</label>
                    <div class="layui-input-inline">
                        <input type="text" name="jarName" autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">卷数</label>
                    <div class="layui-input-inline">
                        <input type="text" name="batchNumber" autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">重量</label>
                    <div class="layui-input-inline">
                        <input type="text" name="weight" autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">收货</label>
                    <div class="layui-input-inline">
                        <input type="text" name="receiver" id="receiver" autocomplete="off" class="layui-input">
                    </div>
                </div>

            </div>
        </form>
    </div>
</#macro>

<style>
    .layui-col-md3 .layui-form-label {
        width:90px
    }
    .layui-col-md6 .layui-form-label {
        width:90px
    }
    .layui-col-md12 .layui-form-label {
        width:90px
    }

    #staffAdd .text {
        width: 8%;
        text-align: center;
    }

    #staffAdd .content {
        width: 17%;
    }

    #staffAdd input {
        border-radius: 0;
        border-color: white;
    }

    #staffAdd select {
        border-color: white;
    }

    #staffAdd td {
        border: solid black;
        border-width: 0px 1px 1px 0px;
    }

    #staffAdd table {
        border: solid black;
        border-width: 1px 0px 0px 1px;
    }
</style>