<#macro search>
<section id="content">
    <section class="vbox">
        <section class="scrollable">
            <#--<table id="fabricAccessoryTable" lay-filter="fabricAccessoryTable"></table>-->
            <#--<script type="text/html" id="toolbarTop">-->
                <#--<div class="layui-btn-container">-->
                    <#--<button class="layui-btn layui-btn-sm" lay-event="clearFilter">清除所有筛选</button>-->
                <#--</div>-->
            <#--</script>-->
            <script type="text/html" id="barTopReview">
                <a class="layui-btn layui-btn-xs layui-btn-normal" lay-event="reviewCommit">审核</a>
            </script>

            <div id="materialReview" style="display:none;">
                <form class="layui-form" style="margin: 0 auto;max-width:100%;padding-top: 20px;">
                    <table class="layui-hide" id="materialReviewTable" lay-filter="materialReviewTable"></table>
                </form>
            </div>

            <div class="layui-row" id="fabricAccessoryTab">
                <div class="layui-tab" lay-filter="docDemoTabBrief">
                    <ul class="layui-tab-title">
                        <li class="layui-this" lay-id="1">面料</li>
                        <li lay-id="2">辅料</li>
                        <li lay-id="3">扁机</li>
                        <li lay-id="4">共用辅料</li>
                    </ul>
                    <div class="layui-tab-content">
                        <div class="layui-tab-item layui-show">
                            <table id="fabricTable" lay-filter="fabricTable"></table>
                        </div>
                        <div class="layui-tab-item">
                            <table id="accessoryTable" lay-filter="accessoryTable"></table>
                        </div>
                        <div class="layui-tab-item">
                            <table id="bjTable" lay-filter="bjTable"></table>
                        </div>
                        <div class="layui-tab-item">
                            <table id="shareAccessoryTable" lay-filter="shareAccessoryTable"></table>
                        </div>
                    </div>
                </div>
            </div>


        </section>
    </section>
    <#--<#include "fb_materialReview.ftl">-->
    <#--<@materialReview></@materialReview>-->
    <#--<#include "fb_materialCheck.ftl">-->
    <#--<@materialCheck></@materialCheck>-->
</section>

    <link rel="stylesheet" href="/css/layui.css" type="text/css">
    <link rel="stylesheet" href="/css/soulTable.css" type="text/css">
    <script src="/js/common/layer.js"></script>
    <script src="/js/common/moment.min.js" type="text/javascript"></script>
    <script src="/js/basicInfo/fabricAccessoryOnlyCheck.js?t=${currentDate?c}"></script>
</#macro>

<style>
    button {
        background:rgb(45, 202, 147);
        opacity:0.86;
        color: white;
        font-family: PingFangSC-Semibold, sans-serif;
    }
    ::-webkit-scrollbar-track-piece {

        background-color:#f8f8f8;

    }

    ::-webkit-scrollbar {

        width:15px;

        height:15px;

    }

    ::-webkit-scrollbar-thumb {

        background-color:#dddddd;

        background-clip:padding-box;

        min-height:28px;

    }

    ::-webkit-scrollbar-thumb:hover {

        background-color:#bbb;

    }
</style>
