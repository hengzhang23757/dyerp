<section class="panel panel-default">

    <div id="tableDiv">

        <table class="table table-striped table-bordered " id="otherOPATable">
            <#if otherOpaList??>
            <thead>
            <tr bgcolor="#ffcb99" style="color: black;">
                <th style="width: 7%;text-align:left;font-size:14px">序号</th>
                <th style="width: 12%;text-align:left;font-size:14px">订单号</th>
                <th style="width: 12%;text-align:left;font-size:14px">版单号</th>
                <th style="width: 10%;text-align:left;font-size:14px">客户</th>
                <th style="width: 10%;text-align:left;font-size:14px">印花厂</th>
                <th style="width: 10%;text-align:left;font-size:14px">颜色</th>
                <th style="width: 8%;text-align:left;font-size:14px">尺码</th>
                <th style="width: 10%;text-align:left;font-size:14px">部位</th>
                <th style="width: 7%;text-align:left;font-size:14px">件数</th>
                <th style="width: 8%;text-align:left;font-size:14px">日期</th>
                <th style="width: 10%;text-align:left;font-size:14px">操作</th>
            </tr>
            </thead>
            <tbody>
                <#list otherOpaList as opa>
                <tr>
                    <td>${opa_index+1}</td>
                    <td>${opa.orderName!}</td>
                    <td>${opa.clothesVersionNumber!}</td>
                    <td>${opa.customerName!}</td>
                    <td>${opa.destination!}</td>
                    <td>${opa.colorName!}</td>
                    <td>${opa.sizeName!}</td>
                    <td>${opa.partName!}</td>
                    <td>${opa.opaCount!}</td>
                    <td><#if opa.opaDate??>${opa.opaDate?date}</#if></td>
                    <td><a href="#" style="color:#3e8eea;" onclick="deleteOtherOPA(${opa.otherOpaID?c})">删除</a></td>
                </tr>
                </#list>
            </tbody>
            <tfoot>
                <tr>
                    <th colspan="11" style="text-align:right"></th>
                </tr>
            </tfoot>
            <#else>
                <h4 style="text-align: center">暂无数据</h4>
            </#if>
        </table>
    </div>

</section>

<style>
    td {
        word-break:break-all;
    }

</style>