<section class="panel panel-default">

    <div id="tableDiv">

        <table class="table table-striped table-bordered " id="opaBackInputTable">
            <#if opaBackInputList??>
            <thead>
                <tr bgcolor="#ffcb99" style="color: black;">
                    <th style="width: 8%;text-align:left;font-size:14px">序号</th>
                    <th style="width: 12%;text-align:left;font-size:14px">订单号</th>
                    <th style="width: 12%;text-align:left;font-size:14px">版单号</th>
                    <th style="width: 8%;text-align:left;font-size:14px">床号</th>
                    <th style="width: 10%;text-align:left;font-size:14px">颜色</th>
                    <th style="width: 10%;text-align:left;font-size:14px">尺码</th>
                    <th style="width: 10%;text-align:left;font-size:14px">部位</th>
                    <th style="width: 8%;text-align:left;font-size:14px">件数</th>
                    <th style="width: 10%;text-align:left;font-size:14px">日期</th>
                    <th style="width: 10%;text-align:left;font-size:14px">操作</th>
                </tr>
            </thead>
            <tbody>
                <#list opaBackInputList as opa>
                <tr>
                    <td>${opa_index+1}</td>
                    <td>${opa.orderName!}</td>
                    <td>${opa.clothesVersionNumber!}</td>
                    <td>${opa.bedNumber!}</td>
                    <td>${opa.colorName!}</td>
                    <td>${opa.sizeName!}</td>
                    <td>${opa.partName!}</td>
                    <td>${opa.opaBackCount!}</td>
                    <td><#if opa.opaBackDate??>${opa.opaBackDate?date}</#if></td>
                    <td><a href="#" style="color:#3e8eea;" onclick="deleteOpaBackInput(${opa.opaBackInputID?c},this)">删除</a>&nbsp;&nbsp;<a href="#" style="color:red" onclick="updateOpaBackInput(this,${opa.opaBackInputID?c})">修改</a></td>
                </tr>
                </#list>
            </tbody>
            <tfoot>
                <tr>
                    <th colspan="10" style="text-align:right"></th>
                </tr>
            </tfoot>
            <#else>
            <h4 style="text-align: center">暂无数据</h4>
            </#if>
        </table>


        <#--<table id="opaTable" class="table table-striped m-b-none ">-->
            <#--<#if opaList??>-->
            <#--<thead>-->
            <#--<tr bgcolor="#ffcb99" style="color: black;">-->
                <#--<th style="width: 10%">序号</th>-->
                <#--<th style="width: 20%">订单号</th>-->
                <#--<th style="width: 20%">版单号</th>-->
                <#--<th style="width: 10%">客户</th>-->
                <#--<th style="width: 10%">订单量</th>-->
                <#--<th style="width: 10%">季度</th>-->
                <#--<th style="width: 10%">交货日期</th>-->
                <#--<th style="width: 10%">操作</th>-->
            <#--</tr>-->
            <#--</thead>-->
            <#--<tbody>-->
                <#--<#list orderClothesList as vo>-->
                <#--<tr>-->
                    <#--<td>${vo_index+1}</td>-->
                    <#--<td><#if vo.orderName??>${vo.orderName}</#if></td>-->
                    <#--<td><#if vo.clothesVersionNumber??>${vo.clothesVersionNumber}</#if></td>-->
                    <#--<td><#if vo.customerName??>${vo.customerName}</#if></td>-->
                    <#--<td><#if vo.orderSum??>${vo.orderSum?c}</#if></td>-->
                    <#--<td><#if vo.season??>${vo.season}</#if></td>-->
                    <#--<td><#if vo.deadLine??>${vo.deadLine?date}</#if></td>-->
                    <#--<td><a href="#" style="color:#3e8eea" onclick="addOrder('${vo.orderName}')">详情</a>&nbsp;&nbsp;<a href="#" style="color:red" onclick="deleteOrder('${vo.orderName}')">删除</a></td>-->
                <#--</tr>-->
                <#--</#list>-->
            <#--</tbody>-->
            <#--<tfoot>-->
            <#--<tr>-->
                <#--<th colspan="6" style="text-align:right"></th>-->
            <#--</tr>-->
            <#--</tfoot>-->
            <#--<#else>-->
            <#--<h4 style="text-align: center">无该状态的数据</h4>-->
            <#--</#if>-->
        <#--</table>-->
    </div>

</section>

<style>
    td {
        word-break:break-all;
    }

</style>