<section class="panel panel-default">

    <div id="tableDiv">

        <table class="table table-striped table-bordered " id="opaTable">
            <#if opaList??>
            <thead>
            <tr bgcolor="#ffcb99" style="color: black;">
                <th style="width: 6%;text-align:left;font-size:14px">序号</th>
                <th style="width: 12%;text-align:left;font-size:14px">订单号</th>
                <th style="width: 12%;text-align:left;font-size:14px">版单号</th>
                <th style="width: 10%;text-align:left;font-size:14px">客户名</th>
                <th style="width: 10%;text-align:left;font-size:14px">印花厂</th>
                <th style="width: 8%;text-align:left;font-size:14px">床号</th>
                <th style="width: 12%;text-align:left;font-size:14px">部位</th>
                <th style="width: 8%;text-align:left;font-size:14px">数量</th>
                <th style="width: 8%;text-align:left;font-size:14px">日期</th>
                <th style="width: 10%;text-align:left;font-size:14px">操作</th>
            </tr>
            </thead>
        <tbody>
            <#list opaList as opa>
            <tr>
                <td>${opa_index+1}</td>
                <td>${opa.orderName!}</td>
                <td>${opa.clothesVersionNumber!}</td>
                <td>${opa.customerName!}</td>
                <td>${opa.destination!}</td>
                <td>${opa.bedNumber!}</td>
                <td>${opa.partName!}</td>
                <td>${opa.opaCount!}</td>
                <td><#if opa.opaDate??>${opa.opaDate?date}</#if></td>
                <td><a href="#" style="color:#3e8eea" onclick="detail('${opa.orderName!}','${opa.bedNumber!}','${opa.partName!}')">详情</a>&nbsp;&nbsp;<a href="#" style="color:#3e8eea;" onclick="deleteOPA(${opa.opaID?c})">删除</a></td>
            </tr>
            </#list>
        </tbody>
        <tfoot>
            <tr>
                <th colspan="10" style="text-align:right"></th>
            </tr>
        </tfoot>
        <#else>
        <h4 style="text-align: center">暂无数据</h4>
        </#if>
        </table>


        <#--<table id="opaTable" class="table table-striped m-b-none ">-->
            <#--<#if opaList??>-->
            <#--<thead>-->
            <#--<tr bgcolor="#ffcb99" style="color: black;">-->
                <#--<th style="width: 10%">序号</th>-->
                <#--<th style="width: 20%">订单号</th>-->
                <#--<th style="width: 20%">版单号</th>-->
                <#--<th style="width: 10%">客户</th>-->
                <#--<th style="width: 10%">订单量</th>-->
                <#--<th style="width: 10%">季度</th>-->
                <#--<th style="width: 10%">交货日期</th>-->
                <#--<th style="width: 10%">操作</th>-->
            <#--</tr>-->
            <#--</thead>-->
            <#--<tbody>-->
                <#--<#list orderClothesList as vo>-->
                <#--<tr>-->
                    <#--<td>${vo_index+1}</td>-->
                    <#--<td><#if vo.orderName??>${vo.orderName}</#if></td>-->
                    <#--<td><#if vo.clothesVersionNumber??>${vo.clothesVersionNumber}</#if></td>-->
                    <#--<td><#if vo.customerName??>${vo.customerName}</#if></td>-->
                    <#--<td><#if vo.orderSum??>${vo.orderSum?c}</#if></td>-->
                    <#--<td><#if vo.season??>${vo.season}</#if></td>-->
                    <#--<td><#if vo.deadLine??>${vo.deadLine?date}</#if></td>-->
                    <#--<td><a href="#" style="color:#3e8eea" onclick="addOrder('${vo.orderName}')">详情</a>&nbsp;&nbsp;<a href="#" style="color:red" onclick="deleteOrder('${vo.orderName}')">删除</a></td>-->
                <#--</tr>-->
                <#--</#list>-->
            <#--</tbody>-->
            <#--<tfoot>-->
            <#--<tr>-->
                <#--<th colspan="6" style="text-align:right"></th>-->
            <#--</tr>-->
            <#--</tfoot>-->
            <#--<#else>-->
            <#--<h4 style="text-align: center">无该状态的数据</h4>-->
            <#--</#if>-->
        <#--</table>-->
    </div>

</section>

<style>
    td {
        word-break:break-all;
    }

</style>