<#macro search>
    <section id="content">
        <section class="vbox">
            <section class="scrollable padder water-mark-visible-ff">
                <div id="mainFrameTabs" style="height:80%">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active noclose" id="orderListTab"><a href="#orderListDiv" data-toggle="tab">花片外发</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="orderListDiv" style="background-color: #f7f7f7;overflow-y:auto">
                            <div class="col-md-12" style="padding-top: 20px;">
                                <section class="panel panel-default">
                                    <div class="panel-body" style="text-align: left">
                                        <div class="row" style="margin-bottom: 10px">
                                            <div class="col-md-2" style="padding:0;text-align: left; margin-left: 10px;">
                                                <button  class="btn" style="border-radius: 5px;color:white;font-family: PingFangSC-Semibold, sans-serif;width:60%;"  onclick="addOPA()">出厂录入</button>
                                            </div>
                                            <div class="col-md-2" style="padding:0;margin-left: 50px;">
                                            </div>
                                            <div class="col-md-2" style="padding:0;margin-left: 50px;">
                                            </div>
                                            <div class="col-md-2" style="padding:0;margin-left: 50px;">
                                            </div>
                                            <div class="col-md-2" style="padding:0;text-align: right; margin-left: 0px;">
                                                <select class="btn btn-default" style="border-radius: 5px;color:black;font-family: PingFangSC-Semibold, sans-serif;width:60%;" onchange="changeTable(this)">
                                                    <option value="today">今日</option>
                                                    <option value="oneMonth">近一个月</option>
                                                    <option value="threeMonths">近三个月</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div id="entities" class="col-sm-12"></div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <#--<div id="entities" class="col-sm-12">-->
                    <#--</div>-->
                    <#include "fb_opaAdd.ftl">
                    <@entities></@entities>
                </div>
            </section>
        </section>
        <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen"
           data-target="#nav"></a>
    </section>
    <aside class="bg-light lter b-l aside-md hide" id="notes">
        <div class="wrapper">Notification</div>
    </aside>
    </section>
    <link rel="stylesheet" href="/css/b.tabs.css" type="text/css">

    <link rel="stylesheet" href="/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.bootstrap.min.css">

    <script src="/js/common/moment.min.js" type="text/javascript"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.bootstrap.min.js"></script>


    <script src="/js/common/jquery.blockUI.js" type="text/javascript" ></script>
    <script src="/js/common/laydate.js" type="text/javascript" ></script>
    <script src="/js/common/b.tabs.js?t=${currentDate?c}"></script>
    <link rel="stylesheet" href="/css/selectize.bootstrap3.css" type="text/css">
    <script src="/js/common/selectize.js" type="text/javascript"></script>
    <script src="/js/opa/opa.js?t=${currentDate?c}"></script>

</#macro>

<style>
    button {
        background:rgb(45, 202, 147);
        opacity:0.86;
        color: white;
        font-family: PingFangSC-Semibold, sans-serif;
    }
</style>





<#--<#macro search>-->
<#--<section id="content">-->
    <#--<section class="vbox">-->
        <#--<section class="scrollable padder water-mark-visible-ff">-->
            <#--<ul class="breadcrumb no-border no-radius b-b b-light pull-in">-->
                <#--<li><a href="homepage"><i class="fa fa-home"></i> Home</a></li>-->
                <#--<li class="active">花片出厂</li>-->
            <#--</ul>-->
            <#--<div id="mainFrameTabs" style="height:80%">-->
                <#--<ul class="nav nav-tabs" role="tablist">-->
                    <#--<li role="presentation" class="active noclose" id="orderListTab"><a href="#orderListDiv" data-toggle="tab">花片出厂</a></li>-->
                <#--</ul>-->
                <#--<div class="tab-content">-->
                    <#--<div class="tab-pane fade in active" id="orderListDiv" style="background-color: #f7f7f7;overflow-y:auto">-->
                        <#--<div class="col-md-12" style="padding-top: 20px;">-->
                            <#--<section class="panel panel-default">-->
                                <#--<div class="panel-body" style="text-align: left">-->
                                    <#--<div class="row" style="margin-left: 10px;margin-bottom: 20px">-->
                                        <#--<button  class="btn btn-s-lg" style="border-radius: 5px;color: white;font-family: PingFangSC-Semibold, sans-serif;"  onclick="addOPA()">出厂录入</button>-->
                                    <#--</div>-->
                                    <#--<div style="text-align: center;font-family: PingFangSC-Semibold,sans-serif;">-->
                                        <#--<table class="table table-striped table-bordered " id="opaTable">-->
                                        <#--<#if opaList?? && opaList?size gt 0>-->
                                            <#--<thead>-->
                                            <#--<tr bgcolor="#ffcb99" style="color: black;">-->
                                                <#--<th style="width: 8%;text-align:center;font-size:14px">序号</th>-->
                                                <#--<th style="width: 10%;text-align:center;font-size:14px">订单号</th>-->
                                                <#--<th style="width: 10%;text-align:center;font-size:14px">版单号</th>-->
                                                <#--<th style="width: 10%;text-align:center;font-size:14px">客户名</th>-->
                                                <#--<th style="width: 14%;text-align:center;font-size:14px">发送目的地</th>-->
                                                <#--<th style="width: 8%;text-align:center;font-size:14px">床号</th>-->
                                                <#--<th style="width: 10%;text-align:center;font-size:14px">部位</th>-->
                                                <#--<th style="width: 10%;text-align:center;font-size:14px">出厂数量</th>-->
                                                <#--<th style="width: 10%;text-align:center;font-size:14px">出厂日期</th>-->
                                                <#--<th style="width: 10%;text-align:center;font-size:14px">操作</th>-->
                                            <#--</tr>-->
                                            <#--</thead>-->
                                            <#--<tbody>-->
                                                <#--<#list opaList as opa>-->
                                                <#--<tr>-->
                                                    <#--<td>${opa_index+1}</td>-->
                                                    <#--<td>${opa.orderName!}</td>-->
                                                    <#--<td>${opa.clothesVersionNumber!}</td>-->
                                                    <#--<td>${opa.customerName!}</td>-->
                                                    <#--<td>${opa.destination!}</td>-->
                                                    <#--<td>${opa.bedNumber!}</td>-->
                                                    <#--<td>${opa.partName!}</td>-->
                                                    <#--<td>${opa.opaCount!}</td>-->
                                                    <#--<td>${opa.opaDate!}</td>-->
                                                    <#--<td><a href="#" style="color:#3e8eea" onclick="detail('${opa.orderName!}','${opa.bedNumber!}','${opa.partName!}')">详情</a>&nbsp;&nbsp;<a href="#" style="color:#3e8eea;" onclick="deleteOPA(${opa.opaID?c})">删除</a></td>-->
                                                <#--</tr>-->
                                                <#--</#list>-->
                                            <#--</tbody>-->
                                            <#--<tfoot>-->
                                                <#--<tr>-->
                                                    <#--<th colspan="10" style="text-align:right"></th>-->
                                                <#--</tr>-->
                                            <#--</tfoot>-->
                                        <#--<#else>-->
                                        <#--<h4 style="text-align: center">暂无数据</h4>-->
                                        <#--</#if>-->
                                        <#--</table>-->
                                    <#--</div>-->
                                <#--</div>-->
                            <#--</section>-->
                        <#--</div>-->
                    <#--</div>-->

                <#--</div>-->
            <#--</div>-->
            <#--<div class="row">-->
                <#--<div id="entities" class="col-sm-12">-->
                <#--</div>-->
                <#--<#include "fb_opaAdd.ftl">-->
                <#--<@entities></@entities>-->
            <#--</div>-->
        <#--</section>-->
    <#--</section>-->
    <#--<a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen"-->
       <#--data-target="#nav"></a>-->
<#--</section>-->
<#--<aside class="bg-light lter b-l aside-md hide" id="notes">-->
    <#--<div class="wrapper">Notification</div>-->
<#--</aside>-->
<#--</section>-->
<#--&lt;#&ndash;<link rel="stylesheet" href="/css/laydate.css" type="text/css">&ndash;&gt;-->
<#--<link rel="stylesheet" href="/css/b.tabs.css" type="text/css">-->

<#--<link rel="stylesheet" href="/css/bootstrap.min.css" type="text/css">-->
<#--<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css">-->
<#--<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.bootstrap.min.css">-->

<#--<script src="/js/common/moment.min.js" type="text/javascript"></script>-->
<#--<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>-->
<#--<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>-->
<#--<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>-->
<#--<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.bootstrap.min.js"></script>-->


<#--<script src="/js/common/jquery.blockUI.js" type="text/javascript" ></script>-->
<#--<script src="/js/common/laydate.js" type="text/javascript" ></script>-->
<#--<script src="/js/common/b.tabs.js?t=${currentDate?c}"></script>-->
<#--<link rel="stylesheet" href="/css/selectize.bootstrap3.css" type="text/css">-->
<#--<script src="/js/common/selectize.js" type="text/javascript"></script>-->
<#--<script src="/js/opa/opa.js?t=${currentDate?c}"></script>-->

<#--</#macro>-->

<#--<style>-->
    <#--button {-->
        <#--background:rgb(45, 202, 147);-->
        <#--opacity:0.86;-->
        <#--color: white;-->
        <#--font-family: PingFangSC-Semibold, sans-serif;-->
    <#--}-->
<#--</style>-->
