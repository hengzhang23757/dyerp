<!DOCTYPE html>
<html lang="en" class="app">
<#include "../feedback/fb_script.ftl">
<@script> </@script>
<body>
<section class="vbox">
    <input  type="hidden" value="${basePath}"  id="basePath"/>
    <section>
        <section class="hbox stretch">
            <#include "fb_opaReport.ftl">
            <@search> </@search>
        </section>
    </section>
</section>
</body>
</html>