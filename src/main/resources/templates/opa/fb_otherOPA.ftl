<#macro search>
    <section id="content">
        <section class="vbox">
            <section class="scrollable padder water-mark-visible-ff">

                <div class="tab-content">
                    <div class="tab-pane fade in active" id="orderListDiv" style="background-color: #f7f7f7;overflow-y:auto">
                        <div class="col-md-12" style="padding-top: 0px;">
                            <section class="panel panel-default">
                                <div class="panel-body" style="text-align: left">
                                    <div class="row" style="margin-bottom: 10px">
                                        <div class="col-md-2" style="padding:0;text-align: left; margin-left: 10px;">
                                            <button  class="btn" style="border-radius: 5px;color:white;font-family: PingFangSC-Semibold, sans-serif;width:60%;"  onclick="addOtherOPA()">出厂录入</button>
                                        </div>
                                        <div class="col-md-2" style="padding:0;margin-left: 50px;">
                                        </div>
                                        <div class="col-md-2" style="padding:0;margin-left: 50px;">
                                        </div>
                                        <div class="col-md-2" style="padding:0;margin-left: 50px;">
                                        </div>
                                        <div class="col-md-2" style="padding:0;text-align: right; margin-left: 0px;">
                                            <select class="btn btn-default" style="border-radius: 5px;color:black;font-family: PingFangSC-Semibold, sans-serif;width:60%;" onchange="changeTable(this)">
                                                <option value="today">今日</option>
                                                <option value="oneMonth">近一个月</option>
                                                <option value="threeMonths">近三个月</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div id="entities" class="col-sm-12"></div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>

                </div>

                <div class="row">
                    <#--<div id="entities" class="col-sm-12">-->
                    <#--</div>-->
                    <#include "fb_otherOPAAdd.ftl">
                    <@entities></@entities>
                </div>
            </section>
        </section>
        <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen"
           data-target="#nav"></a>
    </section>
    <aside class="bg-light lter b-l aside-md hide" id="notes">
        <div class="wrapper">Notification</div>
    </aside>
    </section>
    <link rel="stylesheet" href="/css/b.tabs.css" type="text/css">

    <link rel="stylesheet" href="/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.bootstrap.min.css">

    <script src="/js/common/moment.min.js" type="text/javascript"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.bootstrap.min.js"></script>


    <script src="/js/common/jquery.blockUI.js" type="text/javascript" ></script>
    <script src="/js/common/laydate.js" type="text/javascript" ></script>
    <script src="/js/common/b.tabs.js?t=${currentDate?c}"></script>
    <link rel="stylesheet" href="/css/selectize.bootstrap3.css" type="text/css">
    <script src="/js/common/selectize.js" type="text/javascript"></script>
    <script src="/js/opa/otherOPA.js?t=${currentDate?c}"></script>

</#macro>

<style>
    button {
        background:rgb(45, 202, 147);
        opacity:0.86;
        color: white;
        font-family: PingFangSC-Semibold, sans-serif;
    }
</style>


<#--<#macro search>-->
    <#--<section id="content">-->
        <#--<section class="vbox">-->
            <#--<section class="scrollable padder water-mark-visible-ff">-->
                <#--<ul class="breadcrumb no-border no-radius b-b b-light pull-in">-->
                    <#--<li><a href="homepage"><i class="fa fa-home"></i> Home</a></li>-->
                    <#--<li class="active">士啤出厂</li>-->
                <#--</ul>-->
                <#--<div class="col-md-12" style="padding-top: 20px;">-->
                    <#--<section class="panel panel-default">-->
                        <#--<div class="panel-body" style="text-align: left">-->
                            <#--<div class="row" style="margin-left: 10px;margin-bottom: 20px">-->
                                <#--<button  class="btn btn-s-lg" style="border-radius: 5px;color:white;font-family: PingFangSC-Semibold, sans-serif;"  onclick="addOtherOPA()">出厂录入</button>-->
                            <#--</div>-->
                            <#--<div style="overflow-x: auto;overflow-y:auto;max-height: 600px;text-align: center;font-family: PingFangSC-Semibold,sans-serif;">-->
                                <#--<table class="table table-striped table-bordered " id="otherOPATable">-->
                                    <#--<#if otherOpaList?? && otherOpaList?size gt 0>-->
                                        <#--<thead>-->
                                        <#--<tr bgcolor="#ffcb99" style="color: black;">-->
                                            <#--<th style="width: 8%;text-align:center;font-size:14px">序号</th>-->
                                            <#--<th style="width: 12%;text-align:center;font-size:14px">订单号</th>-->
                                            <#--<th style="width: 12%;text-align:center;font-size:14px">版单号</th>-->
                                            <#--<th style="width: 10%;text-align:center;font-size:14px">客户</th>-->
                                            <#--<th style="width: 10%;text-align:center;font-size:14px">目的地</th>-->
                                            <#--<th style="width: 10%;text-align:center;font-size:14px">颜色</th>-->
                                            <#--<th style="width: 8%;text-align:center;font-size:14px">尺码</th>-->
                                            <#--<th style="width: 10%;text-align:center;font-size:14px">部位</th>-->
                                            <#--<th style="width: 8%;text-align:center;font-size:14px">件数</th>-->
                                            <#--<th style="width: 12%;text-align:center;font-size:14px">日期</th>-->
                                            <#--<th style="width: 8%;text-align:center;font-size:14px">操作</th>-->
                                        <#--</tr>-->
                                        <#--</thead>-->
                                        <#--<tbody>-->
                                        <#--<#list otherOpaList as opa>-->
                                            <#--<tr>-->
                                                <#--<td>${opa_index+1}</td>-->
                                                <#--<td>${opa.orderName!}</td>-->
                                                <#--<td>${opa.clothesVersionNumber!}</td>-->
                                                <#--<td>${opa.customerName!}</td>-->
                                                <#--<td>${opa.destination!}</td>-->
                                                <#--<td>${opa.colorName!}</td>-->
                                                <#--<td>${opa.sizeName!}</td>-->
                                                <#--<td>${opa.partName!}</td>-->
                                                <#--<td>${opa.opaCount!}</td>-->
                                                <#--<td><#if opa.opaDate??>${opa.opaDate?date}</#if></td>-->
                                                <#--<td><a href="#" style="color:#3e8eea;" onclick="deleteOtherOPA(${opa.otherOpaID?c})">删除</a></td>-->
                                            <#--</tr>-->
                                        <#--</#list>-->
                                        <#--</tbody>-->
                                        <#--<tfoot>-->
                                        <#--<tr>-->
                                            <#--<th colspan="10" style="text-align:right"></th>-->
                                        <#--</tr>-->
                                        <#--</tfoot>-->
                                    <#--<#else>-->
                                        <#--<h4 style="text-align: center">暂无数据</h4>-->
                                    <#--</#if>-->
                                <#--</table>-->
                            <#--</div>-->
                        <#--</div>-->
                    <#--</section>-->
                <#--</div>-->
                <#--<div class="row">-->
                    <#--<div id="entities" class="col-sm-12">-->
                    <#--</div>-->
                    <#--<#include "fb_otherOPAAdd.ftl">-->
                    <#--<@entities></@entities>-->
                <#--</div>-->
            <#--</section>-->
        <#--</section>-->
        <#--<a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen"-->
           <#--data-target="#nav"></a>-->
    <#--</section>-->
    <#--<aside class="bg-light lter b-l aside-md hide" id="notes">-->
        <#--<div class="wrapper">Notification</div>-->
    <#--</aside>-->
    <#--</section>-->
<#--&lt;#&ndash;<link rel="stylesheet" href="/css/laydate.css" type="text/css">&ndash;&gt;-->
    <#--<link rel="stylesheet" href="/css/b.tabs.css" type="text/css">-->

    <#--<link rel="stylesheet" href="/css/bootstrap.min.css" type="text/css">-->
    <#--<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css">-->
    <#--<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.bootstrap.min.css">-->

    <#--<script src="/js/common/moment.min.js" type="text/javascript"></script>-->
    <#--<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>-->
    <#--<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>-->
    <#--<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>-->
    <#--<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.bootstrap.min.js"></script>-->
    <#--<script src="/js/common/jquery.blockUI.js" type="text/javascript" ></script>-->
    <#--<script src="/js/common/laydate.js" type="text/javascript" ></script>-->
    <#--<script src="/js/common/b.tabs.js?t=${currentDate?c}"></script>-->
    <#--<link rel="stylesheet" href="/css/selectize.bootstrap3.css" type="text/css">-->
    <#--<script src="/js/common/selectize.js" type="text/javascript"></script>-->
    <#--<script src="/js/opa/otherOPA.js?t=${currentDate?c}"></script>-->

<#--</#macro>-->

<#--<style>-->
    <#--button {-->
        <#--background:rgb(45, 202, 147);-->
        <#--opacity:0.86;-->
        <#--color: white;-->
        <#--font-family: PingFangSC-Semibold, sans-serif;-->
    <#--}-->
<#--</style>-->
