<#macro entities>
<#--<section class="panel panel-default">-->
    <div id="editOtherPro" style="display: none;cursor:default">
        <div style="overflow-x: auto;overflow-y:auto;">
            <div style="text-align: right;margin-bottom:20px">
                <a id="editOtherNo" href="#" style="font-size:20px"><i class="fa fa-times icon" style="color: rgb(182,182,182)"></i></a>
            </div>
            <div style="overflow-x: auto;overflow-y:auto;height: 300px">
                <table style="border-collapse:separate;border-spacing: 10px;width: 100%;" id="addSizeOtherDiv">
                    <tr>
                        <td>
                            <label class="control-label" style="margin-bottom: 15px;text-align: right;">版单号</label>
                        </td>
                        <td>
                            <input id="clothesVersionNumberOther" class="form-control" autocomplete="off" style="width: 150px;margin-bottom: 15px;border-top: none;border-right: none;border-left: none;">
                            <div id="versionNumberTipsOther"></div>
                        </td>
                        <td>
                            <label class="control-label" style="margin-bottom: 15px;text-align: right;">订单号</label>
                        </td>
                        <td>
                            <select id="orderNameOther" class="form-control" autocomplete="off" style="width: 150px;margin-bottom: 15px;border-top: none;border-right: none;border-left: none;"></select>
                        </td>
                        <td>
                            <label class="control-label" style="margin-bottom: 15px;text-align: right;">日期</label>
                        </td>
                        <td>
                            <input id="opaBackDateOther" class="form-control" style="margin-bottom: 15px;width: 150px;border-top: none;border-right: none;border-left: none;">
                        </td>
                        <td>
                            <label class="control-label" style="margin-bottom: 15px;text-align: right;">床号</label>
                        </td>
                        <td>
                            <input id="bedNumberOther" class="form-control" style="margin-bottom: 15px;width: 150px;border-top: none;border-right: none;border-left: none;">
                        </td>
                    </tr>

                    <tr name="sizeDivOther">
                        <td>
                            <label class="control-label" style="margin-bottom: 15px;text-align: right;">部位</label>
                        </td>
                        <td>
                            <select name="partNameOther" class="form-control" style="margin-bottom: 15px;width: 150px;border-top: none;border-right: none;border-left: none;">
                            </select>
                        </td>
                        <td>
                            <label class="control-label" style="margin-bottom: 15px;text-align: right;">颜色</label>
                        </td>
                        <td>
                            <select name="colorNameOther" class="form-control" style="margin-bottom: 15px;width: 150px;border-top: none;border-right: none;border-left: none;">
                            </select>
                        </td>
                        <td>
                            <label class="control-label" style="margin-bottom: 15px;text-align: right;">尺码</label>
                        </td>
                        <td>
                            <select name="sizeNameOther" class="form-control" style="margin-bottom: 15px;width: 150px;border-top: none;border-right: none;border-left: none;">
                            </select>
                        </td>
                        <td>
                            <label class="control-label" style="margin-bottom: 15px;text-align: right;">数量</label>
                        </td>
                        <td style="text-align: left;">
                            <form class="form-inline">
                                <input name="opaBackCountOther" class="form-control" style="margin-bottom: 15px;width: 120px;border-top: none;border-right: none;border-left: none;">
                                <button name="addSizeOtherBtn" class="btn" style="margin-bottom: 15px;outline:none;border-radius: 5px;color: white;" onclick="addSizeOther(this)"><i class="fa fa-plus"></i></button>
                                <button name="delSizeOtherBtn" class="btn" style="margin-bottom: 15px;outline:none;border-radius: 5px;color: white;;display: none;background-color: rgb(236, 108, 98);" onclick="delSizeOther(this)"><i class="fa fa-minus"></i></button>
                            </form>
                        </td>
                    </tr>

                </table>
            </div>

        </div>
        <div class="col-sm-12" style="padding-top: 10px">
            <button id="editOtherYes" class="btn btn-s-lg" style="border-radius: 5px;"  style="text-align: center;">保存</button>
        </div>
    </div>
<#--</section>-->
</#macro>