<#macro entities>
    <div id="editOpaBackInput" style="display: none;cursor:default">
        <div style="overflow-x: auto;overflow-y:auto;">
            <div style="text-align: right;margin-bottom:20px">
                <a id="editOpaBackInputNo" href="#" style="font-size:20px"><i class="fa fa-times icon" style="color: rgb(182,182,182)"></i></a>
            </div>
            <table style="border-collapse:separate;border-spacing: 10px;width: 100%;">
                <tr>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">版单号</label>
                    </td>
                    <td>
                        <input id="clothesVersionNumber1" readonly="readonly" class="form-control" autocomplete="off" style="width: 100%;margin-bottom: 15px;">
                    </td>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">订单号</label>
                    </td>
                    <td>
                        <input id="orderName1" readonly="readonly" class="form-control" autocomplete="off" style="margin-bottom: 15px;width: 100%;">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">床号</label>
                    </td>
                    <td>
                        <input id="bedNumber1" class="form-control" autocomplete="off" style="margin-bottom: 15px;width: 100%;">
                    </td>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">颜色</label>
                    </td>
                    <td>
                        <input id="colorName1" class="form-control" autocomplete="off" style="margin-bottom: 15px;width: 100%;">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">尺码</label>
                    </td>
                    <td>
                        <input id="sizeName1" class="form-control" style="margin-bottom: 15px;width: 100%;" autocomplete="off">
                    </td>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">部位</label>
                    </td>
                    <td>
                        <input id="partName1" class="form-control" style="margin-bottom: 15px;width: 100%;">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">件数</label>
                    </td>
                    <td>
                        <input id="opaBackCount1" class="form-control" style="margin-bottom: 15px;width: 100%;">
                    </td>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">日期</label>
                    </td>
                    <td>
                        <input id="opaBackDate1" class="form-control" style="margin-bottom: 15px;width: 100%;">
                    </td>
                </tr>
            </table>
        </div>
        <div class="col-sm-12" style="padding-top: 10px">
            <button id="editOpaBackInputYes" class="btn btn-s-lg" style="border-radius: 5px;text-align: center;color:white;font-family: PingFangSC-Semibold;">保存</button>
        </div>
    </div>
</#macro>