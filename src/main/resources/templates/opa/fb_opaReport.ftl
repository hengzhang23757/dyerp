<#macro search>
    <section id="content">
        <section class="vbox">
            <section class="scrollable padder water-mark-visible-ff"  style="background-color: #f7f7f7;">

                <div class="col-md-12" style="padding-top: 0px;">
                    <section class="panel panel-default">
                        <div class="panel-body">
                            <div class="row" style="margin-bottom: 10px; margin-left: 10px">
                                <form class="layui-form" style="margin: 0 auto;padding-top: 3px;" lay-filter="baseInfo">
                                    <table>
                                        <tr>
                                            <td style="text-align: right;margin-bottom: 15px;">
                                                <label class="layui-form-label" style="">单号</label>
                                            </td>
                                            <td style="margin-bottom: 15px;">
                                                <input id="clothesVersionNumber" class="layui-input" placeholder="请输入单号" autocomplete="off" style="width: 220px;">
                                            </td>
                                            <td style="text-align: right;margin-bottom: 15px;">
                                                <label class="layui-form-label" style="">款号</label>
                                            </td>
                                            <td style="margin-bottom: 15px;">
                                                <input id="orderName" class="layui-input" placeholder="请输入款号" style="width: 220px">
                                            </td>
                                            <td style="text-align: right;margin-bottom: 15px;">
                                                <label class="layui-form-label" style="">部位</label>
                                            </td>
                                            <td style="margin-bottom: 15px;">
                                                <select id="partName" class="layui-input" style="width: 220px"></select>
                                            </td>
                                            <td></td><td></td>
                                        </tr><tr>
                                            <td style="text-align: right;margin-bottom: 15px;">
                                                <label class="layui-form-label" style="">开始</label>
                                            </td>
                                            <td style="margin-bottom: 15px;">
                                                <input id="from" class="layui-input" placeholder="开始日期" style="width: 220px">
                                            </td>
                                            <td style="text-align: right;margin-bottom: 15px;">
                                                <label class="layui-form-label" style="">结束</label>
                                            </td>
                                            <td style="margin-bottom: 15px;">
                                                <input id="to" class="layui-input" placeholder="结束日期" style="width: 220px">
                                            </td>
                                            <td style="text-align: right;margin-bottom: 15px;">
                                                <label class="layui-form-label" style="">类型</label>
                                            </td>
                                            <td style="margin-bottom: 15px;">
                                                <select id="type" class="layui-input" style="width: 220px">
                                                    <option value="汇总">汇总</option>
                                                    <option value="按天明细">按天明细</option>
                                                </select>
                                            </td>
                                            <td style="text-align: right;margin-bottom: 15px;">
                                                <label class="layui-form-label"></label>
                                            </td>
                                            <td style="margin-bottom: 15px;">
                                                <div class="layui-input-inline">
                                                    <button class="layui-btn" lay-submit lay-filter="searchBeat">搜索</button>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </form>
                            </div>

                            <table id="opaTable" lay-filter="opaTable"></table>

                            <script type="text/html" id="toolbarTop">
                                <div class="layui-btn-container">
                                    <button class="layui-btn layui-btn-sm" lay-event="exportExcel">导出</button>
                                </div>
                            </script>

                            <script type="text/html" id="barTop">
                                <a class="layui-btn layui-btn-xs" lay-event="detail">详情</a>
                            </script>
                        </div>
                    </section>
                </div>
            </section>
        </section>
        <#include "fb_opaReportDetail.ftl">
        <@opaReportDetail></@opaReportDetail>
    </section>

    <link rel="stylesheet" href="/css/layui.css" type="text/css">
    <link rel="stylesheet" href="/css/soulTable.css" type="text/css">
    <link rel="stylesheet" href="/css/selectize.bootstrap3.css" type="text/css">
    <script src="/js/common/layer.js"></script>
    <script src="/js/common/xm-select.js?t=${currentDate?c}" type="text/javascript"></script>
    <script src="/js/common/selectize.js" type="text/javascript"></script>
    <script src="/js/common/moment.min.js" type="text/javascript"></script>
    <script src="/js/opa/opaReport.js?t=${currentDate?c}"></script>

</#macro>

<style>
    .layui-col-md3 .layui-form-label {
        width:90px
    }
    .layui-col-md6 .layui-form-label {
        width:90px
    }
    .layui-col-md12 .layui-form-label {
        width:90px
    }

    ::-webkit-scrollbar-track-piece {

        background-color:#f8f8f8;

    }

    ::-webkit-scrollbar {

        width:15px;

        height:15px;

    }

    ::-webkit-scrollbar-thumb {

        background-color:#dddddd;

        background-clip:padding-box;

        min-height:28px;

    }

    ::-webkit-scrollbar-thumb:hover {

        background-color:#bbb;

    }
</style>
