<#macro sewPlanDetail>
    <div id="sewPlanDetail">
        <div class="layui-col-space10 layui-row" style="padding-top: 30px">
            <table>
                <tr>
                    <td style="text-align: right;padding-bottom: 15px;margin-top: 15px;width: 13%">
                        <label class="layui-form-label" style="font-weight: bolder; font-size: larger">分配颜色</label>
                    </td>
                    <td style="padding-bottom: 15px;width: 7%">
                        <span id="colorName1" style="font-weight: bolder; font-size: large; color: #0f9d58"></span>
                    </td>
                    <td style="text-align: right;padding-bottom: 15px;margin-top: 15px;width: 13%">
                        <label class="layui-form-label" style="font-weight: bolder; font-size: larger">分配尺码</label>
                    </td>
                    <td style="padding-bottom: 15px;width: 7%">
                        <span id="sizeName1" style="font-weight: bolder; font-size: large; color: #0f9d58"></span>
                    </td>
                    <td style="text-align: right;padding-bottom: 15px;margin-top: 15px;width: 13%">
                        <label class="layui-form-label" style="font-weight: bolder; font-size: larger">数量</label>
                    </td>
                    <td style="padding-bottom: 15px;margin-top: 15px;width: 7%">
                        <span id="planCount1" style="font-weight: bolder; font-size: large; color: #0f9d58"></span>
                    </td>
                    <td style="text-align: right;padding-bottom: 15px;margin-top: 15px;width: 13%">
                        <label class="layui-form-label" style="font-weight: bolder; font-size: larger">已完成</label>
                    </td>
                    <td style="padding-bottom: 15px;width: 7%">
                        <span id="finishCount1" style="font-weight: bolder; font-size: large; color: #0f9d58"></span>
                    </td>
                    <td style="text-align: right;padding-bottom: 15px;margin-top: 15px;width: 13%">
                        <label class="layui-form-label" style="font-weight: bolder; font-size: larger">进度</label>
                    </td>
                    <td style="padding-bottom: 15px;width: 7%">
                        <span id="progress1" style="font-weight: bolder; font-size: large; color: #0f9d58"></span>
                    </td>
                </tr>

                <tr>
                    <td style="text-align: right;padding-bottom: 15px;margin-top: 15px;width: 13%">
                        <label class="layui-form-label" style="font-weight: bolder; font-size: larger">当前产能</label>
                    </td>
                    <td style="padding-bottom: 15px;width: 7%">
                        <span id="dailyCount1" style="font-weight: bolder; font-size: large; color: #0f9d58"></span>
                    </td>
                    <td style="text-align: right;padding-bottom: 15px;margin-top: 15px;;width: 13%">
                        <label class="layui-form-label" style="font-weight: bolder; font-size: larger">上线日期</label>
                    </td>
                    <td style="padding-bottom: 15px;width: 7%">
                        <span id="startDate1" style="font-weight: bolder; font-size: large; color: #0f9d58"></span>
                    </td>
                    <td style="text-align: right;padding-bottom: 15px;margin-top: 15px;width: 13%">
                        <label class="layui-form-label" style="font-weight: bolder; font-size: larger">预计下线</label>
                    </td>
                    <td style="padding-bottom: 15px;width: 7%">
                        <span id="endDate1" style="font-weight: bolder; font-size: large; color: #0f9d58"></span>
                    </td>
                    <td style="text-align: right;padding-bottom: 15px;margin-top: 15px;width: 13%">
                        <label class="layui-form-label" style="font-weight: bolder; font-size: larger">离厂日期</label>
                    </td>
                    <td style="padding-bottom: 15px;width: 7%">
                        <span id="deadLine1" style="font-weight: bolder; font-size: large; color: #0f9d58"></span>
                    </td>
                    <td style="text-align: right;padding-bottom: 15px;margin-top: 15px;width: 13%">
                        <label class="layui-form-label" style="font-weight: bolder; font-size: larger">备注</label>
                    </td>
                    <td style="padding-bottom: 15px;width: 7%">
                        <span id="remark1" style="font-weight: bolder; font-size: large; color: #0f9d58"></span>
                    </td>
                </tr>
            </table>
        </div>
        <div style="border:2px solid #CCC"></div>
        <div class="layui-row"><div class="layui-col-xs3" style="color:#FF5722; font-weight: bolder; font-size: x-large;" align="center">衣胚库存</div><div class="layui-col-xs6" style="color:#FF5722;font-weight: bolder; font-size: x-large;" align="center">辅料详情</div><div class="layui-col-xs3" style="color:#FF5722;font-weight: bolder; font-size: x-large;" align="center">每日详情</div></div>
        <div class="layui-row">
            <div class="layui-col-xs3">
                <table id="embStorageTable" name = "embStorageTable" style="background-color: white; color: black">
                </table>
            </div>
            <div class="layui-col-xs6">
                <table id="accessoryStorageTable" name = "accessoryStorageTable" style="background-color: white; color: black">
                </table>
            </div>
            <div class="layui-col-xs3">
                <table id="dailyActionTable" name = "dailyActionTable" style="background-color: white; color: black">
                </table>
            </div>
        </div>

    </div>

</#macro>

<style>

    .layui-form-label {
        font-weight: bold !important;
        width: 120px !important;
    }

    #staffAdd .text {
        width: 8%;
        text-align: center;
    }

    #staffAdd .content {
        width: 17%;
    }

    #staffAdd input {
        border-radius: 0;
        border-color: white;
    }

    #staffAdd select {
        border-color: white;
    }

    #staffAdd td {
        border: solid black;
        border-width: 0px 1px 1px 0px;
    }

    #staffAdd table {
        border: solid black;
        border-width: 1px 0px 0px 1px;
    }
</style>