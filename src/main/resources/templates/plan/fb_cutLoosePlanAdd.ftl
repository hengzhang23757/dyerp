<#macro cutLoosePlanAdd>
    <div id="cutLoosePlanAdd">
    <form class="layui-form" style="margin: 0 auto;padding-top: 20px;" lay-filter="baseInfo">
        <div class="layui-col-space10">
            <table>
                <tr>
                    <td style="text-align: right;margin-bottom: 15px;margin-top: 30px;">
                        <label class="layui-form-label">单号</label>
                    </td>
                    <td style="margin-bottom: 15px;">
                        <input type="text" name="clothesVersionNumber" id="clothesVersionNumber" class="layui-input">
                    </td>
                    <td style="text-align: right;margin-bottom: 15px;margin-top: 30;">
                        <label class="layui-form-label">款号</label>
                    </td>
                    <td style="margin-bottom: 15px;">
                        <input type="text" name="orderName" id="orderName" class="layui-input">
                    </td>
                    <td style="text-align: right;margin-bottom: 15px;margin-top: 30;">
                        <label class="layui-form-label">颜色</label>
                    </td>
                    <td style="margin-bottom: 15px;">
                        <div id="xmColor" class="xm-select-demo"></div>
                    </td>
                    <td style="text-align: right;margin-bottom: 15px;margin-top: 30px;">
                        <label class="layui-form-label">订单数</label>
                    </td>
                    <td style="margin-bottom: 15px;">
                        <input type="text" name="orderCount" id="orderCount" class="layui-input">
                    </td>
                    <td style="text-align: right;margin-bottom: 15px;margin-top: 30;">
                        <label class="layui-form-label">已裁数</label>
                    </td>
                    <td style="margin-bottom: 15px;">
                        <input type="text" name="cutCount" id="cutCount" class="layui-input">
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right;margin-bottom: 15px;margin-top: 30px;">
                        <label class="layui-form-label">入库(卷)</label>
                    </td>
                    <td style="margin-bottom: 15px;margin-top: 15px">
                        <input type="text" name="inStoreBatch" id="inStoreBatch" class="layui-input">
                    </td>
                    <td style="text-align: right;margin-bottom: 15px;margin-top: 30px;">
                        <label class="layui-form-label">已松待裁(卷)</label>
                    </td>
                    <td style="margin-bottom: 15px;">
                        <input type="text" name="preCutBatch" id="preCutBatch" class="layui-input">
                    </td>
                    <td style="text-align: right;margin-bottom: 15px;margin-top: 30px;">
                        <label class="layui-form-label">库存量(卷)</label>
                    </td>
                    <td style="margin-bottom: 15px;">
                        <input type="text" name="storageBatch" id="storageBatch" class="layui-input">
                    </td>
                    <td style="text-align: right;margin-bottom: 15px;margin-top: 30px;">
                        <label class="layui-form-label">计划开始</label>
                    </td>
                    <td style="margin-bottom: 15px;">
                        <input type="text" name="beginDate" id="beginDate" class="layui-input">
                    </td>
                    <td style="text-align: right;margin-bottom: 15px;margin-top: 30;">
                        <label class="layui-form-label">预计完成</label>
                    </td>
                    <td style="margin-bottom: 15px;">
                        <input type="text" name="endDate" id="endDate" class="layui-input">
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right;margin-bottom: 15px;margin-top: 30px;">
                        <label class="layui-form-label">印绣花厂</label>
                    </td>
                    <td style="margin-bottom: 15px;">
                        <select type="text" name="printingFactory" id="printingFactory" class="layui-input">
                            <option value="新华隆">新华隆</option>
                            <option value="其他">其他</option>
                        </select>
                    </td>
                    <td style="text-align: right;margin-bottom: 15px;margin-top: 30px;">
                        <label class="layui-form-label">印绣花部位</label>
                    </td>
                    <td style="margin-bottom: 15px;">
                        <input type="text" name="printingPart" id="printingPart" class="layui-input">
                    </td>
                    <td style="text-align: right;margin-bottom: 15px;margin-top: 30px;">
                        <label class="layui-form-label">回厂日期</label>
                    </td>
                    <td style="margin-bottom: 15px;">
                        <input type="text" name="returnDate" id="returnDate" class="layui-input">
                    </td>
                    <td style="text-align: right;margin-bottom: 15px;margin-top: 30px;">
                        <label class="layui-form-label">备注</label>
                    </td>
                    <td style="margin-bottom: 15px;">
                        <input type="text" name="remark" id="remark" class="layui-input">
                    </td>
                </tr>
            </table>
        </div>
    </form>
    </div>

</#macro>

<style>

    .layui-form-label {
        font-weight: bold !important;
        width: 120px !important;
    }

    #staffAdd .text {
        width: 8%;
        text-align: center;
    }

    #staffAdd .content {
        width: 17%;
    }

    #staffAdd input {
        border-radius: 0;
        border-color: white;
    }

    #staffAdd select {
        border-color: white;
    }

    #staffAdd td {
        border: solid black;
        border-width: 0px 1px 1px 0px;
    }

    #staffAdd table {
        border: solid black;
        border-width: 1px 0px 0px 1px;
    }
</style>