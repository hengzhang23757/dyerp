<#macro search>
    <section id="content">
        <section class="vbox">
            <section class="scrollable padder water-mark-visible-ff">
                <div class="tab-content">
                    <div class="col-md-12" style="padding-top: 0px;">
                        <section class="panel panel-default">
                            <div class="panel-body" style="text-align: left">
                                <div class="row" style="margin-left: 0;margin-bottom: 0px">
                                    <button  class="btn btn-s-lg" style="border-radius: 5px;color:white;font-family: PingFangSC-Semibold, sans-serif;"  onclick="addEmbPlan()">计划录入</button>
                                </div>
                                <div style="text-align: center;font-family: PingFangSC-Semibold;">
                                    <table id="embPlanTable" class="table table-striped table-hover">
                                    </table>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
                <div class="row">
                    <div id="entities" class="col-sm-12">
                    </div>
                    <#include "fb_embPlanAdd.ftl">
                    <@entities></@entities>
                    <#include "fb_embPlanEdit.ftl">
                    <@entities></@entities>
                </div>
            </section>
        </section>
        <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen"
           data-target="#nav"></a>
    </section>
    <aside class="bg-light lter b-l aside-md hide" id="notes">
        <div class="wrapper">Notification</div>
    </aside>
    <link rel="stylesheet" href="/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="/css/b.tabs.css" type="text/css">
    <link rel="stylesheet" href="/css/selectize.bootstrap3.css" type="text/css">
    <script src="/js/common/jquery.blockUI.js" type="text/javascript" ></script>
    <script src="/js/common/laydate.js" type="text/javascript" ></script>
    <script src="/js/common/b.tabs.js?t=${currentDate?c}"></script>
    <script src="/js/common/selectize.js" type="text/javascript"></script>
    <script src="/js/common/moment.min.js" type="text/javascript"></script>
    <script src="/js/plan/embPlan.js?t=${currentDate?c}"></script>
</#macro>

<style>
    button {
        background:rgb(45, 202, 147);
        opacity:0.86;
        color: white;
        font-family: PingFangSC-Semibold, sans-serif;
    }
</style>
