<#macro search>
    <section id="content">
        <section class="vbox">
            <section class="scrollable">
                <div class="layui-row">
                    <form class="layui-form" style="margin: 0 auto;padding-top: 3px;" lay-filter="baseInfo">
                        <table>
                            <tr>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label">开始</label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <input type="text" name="from" id="from" placeholder="请输入单号" autocomplete="off" class="layui-input">
                                </td>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label">结束</label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <input type="text" name="to" id="to" placeholder="请输入款号" autocomplete="off" class="layui-input">
                                </td>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label">单号</label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <input type="text" name="clothesVersionNumber" id="clothesVersionNumber" autocomplete="off" placeholder="请输入单号" class="layui-input">
                                </td>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label">款号</label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <input type="text" name="orderName" id="orderName" placeholder="请输入款号" autocomplete="off" class="layui-input">
                                </td>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label"></label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <div class="layui-input-inline">
                                        <button class="layui-btn" lay-submit lay-filter="searchData">搜索</button>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>

                <table id="dataTable" lay-filter="dataTable"></table>

                <script type="text/html" id="toolbarTop">
                    <div class="layui-btn-container">
                        <button class="layui-btn layui-btn-sm" lay-event="exportExcel">导出</button>
                    </div>
                </script>

                <script type="text/html" id="barTop">
                    <a class="layui-btn layui-btn-xs" lay-event="fabricDetail">面料</a>
                    <a class="layui-btn layui-btn-xs layui-btn-warm" lay-event="accessoryDetail">辅料</a>
                </script>
            </section>
        </section>
    </section>

    <link rel="stylesheet" href="/css/layui.css" type="text/css">
    <link rel="stylesheet" href="/css/soulTable.css" type="text/css">
    <link rel="stylesheet" href="/css/selectize.bootstrap3.css" type="text/css">
<#--<script src="/js/common/layui.js" type="text/javascript"></script>-->
    <script src="/js/common/layer.js"></script>
    <script src="/js/common/xm-select.js?t=${currentDate?c}" type="text/javascript"></script>
    <script src="/js/common/selectize.js" type="text/javascript"></script>
    <script src="/js/common/moment.min.js" type="text/javascript"></script>
    <script src="/js/plan/prenatalProgress.js?t=${currentDate?c}"></script>
</#macro>

<style>
    .layui-col-md3 .layui-form-label {
        width:90px
    }
    .layui-col-md6 .layui-form-label {
        width:90px
    }
    .layui-col-md12 .layui-form-label {
        width:90px
    }

    ::-webkit-scrollbar-track-piece {

        background-color:#f8f8f8;

    }

    ::-webkit-scrollbar {

        width:15px;

        height:15px;

    }

    ::-webkit-scrollbar-thumb {

        background-color:#dddddd;

        background-clip:padding-box;

        min-height:28px;

    }

    ::-webkit-scrollbar-thumb:hover {

        background-color:#bbb;

    }
</style>

