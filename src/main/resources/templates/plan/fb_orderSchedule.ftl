<#macro search>
    <section id="content">
        <section class="vbox">
            <section class="scrollable padder water-mark-visible-ff">
                <div class="layui-row">
                    <form class="layui-form" style="margin: 0 auto;padding-top: 3px;" lay-filter="baseInfo">
                        <table>
                            <tr>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label">单号</label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <input type="text" class="layui-input" name="clothesVersionNumber" placeholder="请输入单号" id="clothesVersionNumber">
                                </td>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label">款号</label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <input type="text" class="layui-input" name="orderName" id="orderName" placeholder="请输入款号">
                                </td>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label">组名</label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <select type="text" name="groupName" id="groupName">
                                        <option value="车缝A组">车缝A组</option>
                                        <option value="车缝B组">车缝B组</option>
                                        <option value="车缝C组">车缝C组</option>
                                        <option value="车缝D组">车缝D组</option>
                                        <option value="车缝E组">车缝E组</option>
                                        <option value="车缝F组">车缝F组</option>
                                        <option value="车缝G组">车缝G组</option>
                                        <option value="车缝H组">车缝H组</option>
                                    </select>
                                </td>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label">工序</label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <div id="procedureNumber" class="xm-select-demo" style="width: 200px;"></div>
                                </td>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label">工时</label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <select type="text" name="workHour" class="layui-input" id="workHour">
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        <option value="11" selected>11</option>
                                        <option value="12">12</option>
                                    </select>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <div class="layui-input-inline" style="margin-left: 10px">
                                        <button class="layui-btn layui-btn-normal" lay-submit lay-filter="loadBasicData">加载</button>
                                    </div>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <div class="layui-input-inline" style="margin-left: 10px">
                                        <button class="layui-btn" lay-submit lay-filter="orderSchedule">排产</button>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>

                <table id="scheduleTable" lay-filter="scheduleTable"></table>
                <script type="text/html" id="toolbarTop">
                    <div class="layui-btn-container">
                        <button class="layui-btn layui-btn-sm" lay-event="exportExcel">导出</button>
                        <button class="layui-btn layui-btn-sm" lay-event="saveScheduleData">保存</button>
                        <button class="layui-btn layui-btn-sm" lay-event="fillHour">填充</button>
                        <button class="layui-btn layui-btn-sm" lay-event="transRecord">切换</button>
                    </div>
                </script>

                <script type="text/html" id="barTop">
                    <a class="layui-btn layui-btn-xs" lay-event="detail">详情</a>
                    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
                </script>
            </section>
        </section>
        <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen"
           data-target="#nav"></a>

        <#include "fb_scheduleDetail.ftl">
        <@scheduleDetail></@scheduleDetail>

    </section>
    <aside class="bg-light lter b-l aside-md hide" id="notes">
        <div class="wrapper">Notification</div>
    </aside>
    <link rel="stylesheet" href="/css/layui.css" type="text/css">
    <link rel="stylesheet" href="/css/soulTable.css" type="text/css">
    <link rel="stylesheet" href="/css/selectize.bootstrap3.css" type="text/css">
    <script src="/js/common/layer.js"></script>
    <script src="/js/common/xm-select.js?t=${currentDate?c}" type="text/javascript"></script>
    <script src="/js/common/selectize.js" type="text/javascript"></script>
    <script src="/js/common/moment.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="https://cdn.bootcss.com/echarts/4.2.1-rc1/echarts.min.js"></script>
    <script src="/js/plan/orderSchedule.js?t=${currentDate?c}"></script>

</#macro>

<style>
    button {
        background:rgb(45, 202, 147);
        opacity:0.86;
        color: white;
        font-family: PingFangSC-Semibold, sans-serif;
    }
    ::-webkit-scrollbar-track-piece {

        background-color:#f8f8f8;

    }

    ::-webkit-scrollbar {

        width:15px;

        height:15px;

    }

    ::-webkit-scrollbar-thumb {

        background-color:#dddddd;

        background-clip:padding-box;

        min-height:28px;

    }

    ::-webkit-scrollbar-thumb:hover {

        background-color:#bbb;

    }
</style>
