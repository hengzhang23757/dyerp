<#macro search>
    <section id="content">
        <section class="vbox">
            <section class="scrollable">
                <table id="reportTable" lay-filter="reportTable"></table>
                <script type="text/html" id="toolbarTop">
                    <div class="layui-btn-container">
                        <button class="layui-btn layui-btn-sm" lay-event="refresh">刷新</button>
                        <button class="layui-btn layui-btn-sm" lay-event="clearFilter">清除所有筛选</button>
                        <button class="layui-btn layui-btn-sm" lay-event="exportExcel">导出</button>
                        <button class="layui-btn layui-btn-sm" lay-event="finishData">已完成计划</button>
                        <button class="layui-btn layui-btn-sm" lay-event="unFinishData">未完成计划</button>
                        <button class="layui-btn layui-btn-sm" lay-event="addCutLoosePlan">添加</button>
                    </div>
                </script>
                <script type="text/html" id="barTop">
                    <a class="layui-btn layui-btn-xs" lay-event="finish">完成</a>
                    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
                </script>
            </section>
        </section>
        <#include "fb_cutLoosePlanAdd.ftl">
        <@cutLoosePlanAdd></@cutLoosePlanAdd>
    </section>
    <link rel="stylesheet" href="/css/layui.css" type="text/css">
    <link rel="stylesheet" href="/css/soulTable.css" type="text/css">
    <script src="/js/common/moment.min.js" type="text/javascript"></script>
    <script src="/js/common/xm-select.js?t=${currentDate?c}" type="text/javascript"></script>
    <script src="/js/plan/cutLoosePlan.js?t=${currentDate?c}"></script>

</#macro>

<style>
    button {
        background:rgb(45, 202, 147);
        opacity:0.86;
        color: white;
        font-family: PingFangSC-Semibold, sans-serif;
    }
    ::-webkit-scrollbar-track-piece {

        background-color:#f8f8f8;

    }

    ::-webkit-scrollbar {

        width:15px;

        height:15px;

    }

    ::-webkit-scrollbar-thumb {

        background-color:#dddddd;

        background-clip:padding-box;

        min-height:28px;

    }

    ::-webkit-scrollbar-thumb:hover {

        background-color:#bbb;

    }
</style>