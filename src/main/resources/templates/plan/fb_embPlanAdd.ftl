<#macro entities>
    <div id="editPro" style="display: none;cursor:default">
        <div style="text-align: right;">
            <a id="editNo" href="#" style="font-size:20px"><i class="fa fa-times icon" style="color: rgb(182,182,182)"></i></a>
        </div>
        <div style="overflow-x: auto;overflow-y:auto;height: 400px" id="sizeTableDiv">
            <table style="border-collapse:separate;border-spacing: 10px;" name="sizeDiv" class="col-md-12">
                <tr>
                    <td>
                        <span style="font-weight: 700;color:black" name="orderOfLayer">1</span>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">组名</label>
                    </td>
                    <td>
                        <select name="groupSelect0" class="form-control" autocomplete="off" style="width: 130px;margin-bottom: 10px;border-top: none;border-right: none;border-left: none;">
                            <option value="">请选择组名</option>
                            <option value="车缝A组">车缝A组</option>
                            <option value="车缝B组">车缝B组</option>
                            <option value="车缝C组">车缝C组</option>
                            <option value="车缝D组">车缝D组</option>
                            <option value="车缝E组">车缝E组</option>
                            <option value="车缝F组">车缝F组</option>
                            <option value="车缝G组">车缝G组</option>
                            <option value="车缝H组">车缝H组</option>
                        </select>
                    </td>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">时间</label>
                    </td>
                    <td>
                        <input name="endDate0" class="form-control" autocomplete="off" style="width: 130px;margin-bottom: 10px;border-top: none;border-right: none;border-left: none;">
                    </td>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">版单号</label>
                    </td>
                    <td>
                        <input name="clothesVersionNumber0" class="form-control" autocomplete="off" style="width: 130px;margin-bottom: 10px;border-top: none;border-right: none;border-left: none;">
                    </td>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">款号</label>
                    </td>
                    <td>
                        <select name="orderName0" class="form-control" autocomplete="off" style="width: 130px;margin-bottom: 10px;border-top: none;border-right: none;border-left: none;"></select>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">颜色</label>
                    </td>
                    <td>
                        <select name="colorName0" class="form-control" autocomplete="off" style="width: 130px;margin-bottom: 10px;border-top: none;border-right: none;border-left: none;"></select>
                    </td>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">尺码</label>
                    </td>
                    <td>
                        <select name="sizeName0" class="form-control" autocomplete="off" style="width: 130px;margin-bottom: 10px;border-top: none;border-right: none;border-left: none;"></select>
                    </td>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">数量</label>
                    </td>
                    <td style="border-spacing:0">
                        <input name="planCount0" class="form-control" autocomplete="off" style="width: 130px;margin-bottom: 10px;border-top: none;border-right: none;border-left: none;" onkeyup="value=value.replace(/^(0+)|[^\d]+/g,'')">
                    </td>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">上浮(%)</label>
                    </td>
                    <td>
                        <input name="driftPercent0" class="form-control" autocomplete="off" style="width: 130px;margin-bottom: 10px;border-top: none;border-right: none;border-left: none;" oninput="value=value.replace(/[^\d]/g,'')">
                    </td>
                    <td>
                        <button name="addSizeBtn" class="btn" style="margin-bottom: 5px;outline:none;border-radius: 10px;color: white;" onclick="addSize(this)"><i class="fa fa-plus"></i></button>
                        <button name="delSizeBtn" class="btn" style="margin-bottom: 5px;outline:none;border-radius: 10px;color: white;display: none;background-color: rgb(236, 108, 98);" onclick="delSize(this)"><i class="fa fa-minus"></i></button>
                    </td>
                </tr>
            </table>
        </div>
        <div class="col-sm-12" style="padding-top: 10px">
            <button id="editYes" class="btn btn-s-lg" style="border-radius: 5px;color: white;font-family: PingFangSC-Semibold, sans-serif;text-align: center;">保存</button>
        </div>
    </div>
</#macro>

<style>
    button {
        background:rgb(45, 202, 147);
        opacity:0.86;
        color: white;
        font-family: PingFangSC-Semibold, sans-serif;
    }
</style>