<#macro scheduleDetail>
    <table id="scheduleDetailTable" name = "scheduleDetailTable" style="background-color: white; color: black">
    </table>
    <script type="text/html" id="toolbarDetailTop">
        <div class="layui-btn-container">
            <button class="layui-btn layui-btn-sm" lay-event="exportExcel">导出</button>
        </div>
    </script>
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12">
            <div class="layui-card">
                <div class="layui-card-header">工序平衡图</div>
                <div class="layui-card-body">
                    <div id="balanceFigure" class="chart" style="width: 100%;height:400px;margin: 0 auto"></div>
                </div>
            </div>
        </div>
    </div>
</#macro>