<#macro sewPlanAdd>
    <div id="sewPlanAdd">
    <#--<form class="layui-form" style="margin: 0 auto;padding-top: 20px;" lay-filter="baseInfo">-->
        <div class="layui-col-space10" style="padding-top: 30px">
            <table>
                <tr>
                    <td style="text-align: right;padding-bottom: 15px;margin-top: 30px;">
                        <label class="layui-form-label">单号</label>
                    </td>
                    <td style="padding-bottom: 15px;">
                        <input type="text" name="clothesVersionNumber" id="clothesVersionNumber" autocomplete="off" class="layui-input">
                    </td>
                    <td style="text-align: right;padding-bottom: 15px;margin-top: 30px;">
                        <label class="layui-form-label">款号</label>
                    </td>
                    <td style="padding-bottom: 15px;">
                        <input type="text" name="orderName" id="orderName" autocomplete="off" class="layui-input">
                    </td>
                    <td style="text-align: right;padding-bottom: 15px;margin-top: 30px;">
                        <label class="layui-form-label">颜色</label>
                    </td>
                    <td style="padding-bottom: 15px;">
                        <div id="xmColor" class="xm-select-demo"></div>
                    </td>
                    <td style="text-align: right;padding-bottom: 15px;margin-top: 30px;">
                        <label class="layui-form-label">尺码</label>
                    </td>
                    <td style="padding-bottom: 15px;">
                        <div id="xmSize" class="xm-select-demo"></div>
                    </td>
                    <td style="text-align: right;padding-bottom: 15px;margin-top: 30px;">
                        <label class="layui-form-label">数量</label>
                    </td>
                    <td style="padding-bottom: 15px;margin-top: 15px">
                        <input type="text" name="planCount" id="planCount" autocomplete="off" class="layui-input">
                    </td>
                    <td style="text-align: right;padding-bottom: 15px;margin-top: 30px;padding-left: 20px">
                        <input type="text" name="taskDataID" id="taskDataID" hidden>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right;padding-bottom: 15px;margin-top: 30px;">
                        <label class="layui-form-label">组名</label>
                    </td>
                    <td style="padding-bottom: 15px;">
                        <input type="text" name="groupName" autocomplete="off" id="groupName" class="layui-input">
                    </td>
                    <td style="text-align: right;padding-bottom: 15px;margin-top: 30px;">
                        <label class="layui-form-label">人数</label>
                    </td>
                    <td style="padding-bottom: 15px;">
                        <input type="text" name="employeeCount" autocomplete="off" id="employeeCount" class="layui-input" readonly>
                    </td>
                    <td style="text-align: right;padding-bottom: 15px;margin-top: 30px;">
                        <label class="layui-form-label">CT</label>
                    </td>
                    <td style="padding-bottom: 15px;">
                        <input type="text" name="samValue" autocomplete="off" id="samValue" class="layui-input">
                    </td>
                    <td style="text-align: right;padding-bottom: 15px;margin-top: 30px;">
                        <label class="layui-form-label">日产能</label>
                    </td>
                    <td style="padding-bottom: 15px;">
                        <input type="text" name="dailyCount" autocomplete="off" id="dailyCount" class="layui-input">
                    </td>
                    <td style="text-align: right;padding-bottom: 15px;margin-top: 30px;">
                        <label class="layui-form-label">客户交期</label>
                    </td>
                    <td style="padding-bottom: 15px;">
                        <input type="text" name="deadLine" autocomplete="off" id="deadLine" class="layui-input">
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right;padding-bottom: 15px;margin-top: 30px;">
                        <label class="layui-form-label">启动日期</label>
                    </td>
                    <td style="padding-bottom: 15px;">
                        <input type="text" name="initDate" autocomplete="off" id="initDate" class="layui-input">
                    </td>
                    <td style="text-align: right;padding-bottom: 15px;margin-top: 30px;">
                        <label class="layui-form-label">备注</label>
                    </td>
                    <td style="padding-bottom: 15px;">
                        <input type="text" name="remark" autocomplete="off" id="remark" value="无" class="layui-input">
                    </td>
                    <td style="text-align: right;padding-bottom: 15px;margin-top: 30px;">
                        <label class="layui-form-label"></label>
                    </td>

                    <td style="text-align: right;padding-bottom: 15px;margin-top: 30px;padding-left: 20px">
                        <button  class="layui-btn layui-btn-normal" onclick="confirmTaskData()">确认</button>
                    </td>

                    <td style="text-align: center;padding-bottom: 15px;margin-top: 30px;" colspan="3">
                        <span style="text-align: center; font-size: x-large;font-weight: bolder;color: #1E9FFF">预排结果如下表</span>
                    </td>
                </tr>
                <#--<tr>-->
                    <#--&lt;#&ndash;<td style="text-align: right;padding-bottom: 15px;margin-top: 30px;">&ndash;&gt;-->
                        <#--&lt;#&ndash;<label class="layui-form-label">预计天数</label>&ndash;&gt;-->
                    <#--&lt;#&ndash;</td>&ndash;&gt;-->
                    <#--&lt;#&ndash;<td style="padding-bottom: 15px;">&ndash;&gt;-->
                        <#--&lt;#&ndash;<input type="text" name="planDay" autocomplete="off" id="planDay" class="layui-input" readonly>&ndash;&gt;-->
                    <#--&lt;#&ndash;</td>&ndash;&gt;-->
                    <#--&lt;#&ndash;<td style="text-align: right;padding-bottom: 15px;margin-top: 30px;">&ndash;&gt;-->
                        <#--&lt;#&ndash;<label class="layui-form-label">开始日期</label>&ndash;&gt;-->
                    <#--&lt;#&ndash;</td>&ndash;&gt;-->
                    <#--&lt;#&ndash;<td style="padding-bottom: 15px;">&ndash;&gt;-->
                        <#--&lt;#&ndash;<input type="text" name="beginDate" autocomplete="off" id="beginDate" class="layui-input" readonly>&ndash;&gt;-->
                    <#--&lt;#&ndash;</td>&ndash;&gt;-->
                    <#--&lt;#&ndash;<td style="text-align: right;padding-bottom: 15px;margin-top: 30px;">&ndash;&gt;-->
                        <#--&lt;#&ndash;<label class="layui-form-label">预计完成</label>&ndash;&gt;-->
                    <#--&lt;#&ndash;</td>&ndash;&gt;-->
                    <#--&lt;#&ndash;<td style="padding-bottom: 15px;">&ndash;&gt;-->
                        <#--&lt;#&ndash;<input type="text" name="endDate" autocomplete="off" id="endDate" class="layui-input" readonly>&ndash;&gt;-->
                    <#--&lt;#&ndash;</td>&ndash;&gt;-->
                    <#--<td style="text-align: right;padding-bottom: 15px;margin-top: 30px;">-->
                        <#--<label class="layui-form-label">整体开始日期</label>-->
                    <#--</td>-->
                    <#--<td style="padding-bottom: 15px;">-->
                        <#--<input type="text" name="deadLine" autocomplete="off" id="deadLine" class="layui-input">-->
                    <#--</td>-->
                    <#--<td style="padding-bottom: 15px;"></td>-->
                <#--</tr>-->
            </table>
        </div>
        <table id="sewPlanAddUpdateTable" name = "sewPlanAddUpdateTable" style="background-color: white; color: black">
        </table>
    <#--</form>-->
    </div>

</#macro>

<style>

    .layui-form-label {
        font-weight: bold !important;
        width: 120px !important;
    }

    #staffAdd .text {
        width: 8%;
        text-align: center;
    }

    #staffAdd .content {
        width: 17%;
    }

    #staffAdd input {
        border-radius: 0;
        border-color: white;
    }

    #staffAdd select {
        border-color: white;
    }

    #staffAdd td {
        border: solid black;
        border-width: 0px 1px 1px 0px;
    }

    #staffAdd table {
        border: solid black;
        border-width: 1px 0px 0px 1px;
    }
</style>