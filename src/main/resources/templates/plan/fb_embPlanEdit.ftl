<#macro entities>
<#--<section class="panel panel-default">-->
    <div id="updatePro" style="display: none;cursor:default">
        <div style="overflow-x: auto;overflow-y:auto;">
            <div style="text-align: right;margin-bottom:5px">
                <a id="updateNo" href="#" style="font-size:20px"><i class="fa fa-times icon" style="color: rgb(182,182,182)"></i></a>
            </div>
            <table style="border-collapse:separate;border-spacing: 10px;width: 100%;">
                <tr>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">组名</label>
                    </td>
                    <td>
                        <input id="groupSelectUpdate" class="form-control" autocomplete="off" style="margin-bottom: 15px;width: 150px;" readonly>
                    </td>

                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">时间</label>
                    </td>
                    <td>
                        <input id="endDateUpdate" class="form-control" autocomplete="off" style="width: 150px;margin-bottom: 15px;" readonly>
                    </td>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">版单号</label>
                    </td>
                    <td>
                        <input id="clothesVersionNumberUpdate" class="form-control" autocomplete="off" style="margin-bottom: 15px;width: 150px;" readonly>
                    </td>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">款号</label>
                    </td>
                    <td>
                        <input id="orderNameUpdate" class="form-control" autocomplete="off" style="width: 150px;margin-bottom: 15px;" readonly>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">颜色</label>
                    </td>
                    <td>
                        <input id="colorNameUpdate" class="form-control" autocomplete="off" style="width: 150px;margin-bottom: 15px;" readonly>
                    </td>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">尺码</label>
                    </td>
                    <td>
                        <input id="sizeNameUpdate" class="form-control" autocomplete="off" style="width: 150px;margin-bottom: 15px;" readonly>
                    </td>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">数量</label>
                    </td>
                    <td>
                        <input id="planCountUpdate" class="form-control" autocomplete="off" style="margin-bottom: 15px;width: 150px;" onkeyup="value=value.replace(/^(0+)|[^\d]+/g,'')">
                    </td>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">上浮(%)</label>
                    </td>
                    <td>
                        <input id="driftPercentUpdate" class="form-control" autocomplete="off" style="width: 150px;margin-bottom: 15px;" onkeyup="value=value.replace(/^(0+)|[^\d]+/g,'')">
                    </td>
                </tr>
            </table>
        </div>
        <div class="col-sm-12" style="padding-top: 10px">
            <button id="updateYes" class="btn btn-s-lg" style="border-radius: 5px; text-align: center;">保存</button>
        </div>
    </div>
<#--</section>-->
</#macro>