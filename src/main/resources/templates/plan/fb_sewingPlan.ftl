<#macro search>
    <section id="content">
        <section class="vbox">
            <section class="scrollable">
                <div class="tab-content">
                    <div class="col-md-12" style="padding-top: 0px;">
                        <section class="panel panel-default">
                            <div class="panel-body" style="text-align: left">
                                <div style="text-align: center;font-family: PingFangSC-Semibold;">
                                    <div id="gantt_here"></div>
                                    <div id="ganttTableDiv" hidden>
                                        <table class="table table-striped table-hover" id="ganttTable"></table>
                                    </div>
                                    <div id="fabricTableDiv" hidden>
                                        <table class="table table-striped table-hover" id="fabricTable"></table>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>

                <script type="text/html" id="barTop">
                    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="remove">移除</a>
                </script>
            </section>
        </section>
        <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen"
           data-target="#nav"></a>

        <#include "fb_sewPlanAdd.ftl">
        <@sewPlanAdd></@sewPlanAdd>
        <#include "fb_sewPlanDetail.ftl">
        <@sewPlanDetail></@sewPlanDetail>
    </section>
    <aside class="bg-light lter b-l aside-md hide" id="notes">
        <div class="wrapper">Notification</div>
    </aside>
    <link rel="stylesheet" href="/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="/css/b.tabs.css" type="text/css">
    <link rel="stylesheet" href="/css/selectize.bootstrap3.css" type="text/css">
    <link rel="stylesheet" href="/css/dhtmlxgantt_broadway.css?v=7.0.3" type="text/css">
    <link rel="stylesheet" href="/css/layui.css" type="text/css">
    <link rel="stylesheet" href="/css/soulTable.css" type="text/css">
    <script src="/js/common/jquery.blockUI.js" type="text/javascript" ></script>
    <script src="/js/common/laydate.js" type="text/javascript" ></script>
    <script src="/js/common/b.tabs.js?t=${currentDate?c}"></script>
    <script src="/js/common/selectize.js" type="text/javascript"></script>
    <script src="/js/common/moment.min.js" type="text/javascript"></script>
    <script src="/js/common/xm-select.js?t=${currentDate?c}" type="text/javascript"></script>
    <script src="/js/common/dhtmlxgantt.js?v=7.0.3" type="text/javascript"></script>
    <script src="https://cdn.bootcdn.net/ajax/libs/dayjs/1.8.29/dayjs.min.js"></script>
    <script src="/js/plan/sewingPlan.js?t=${currentDate?c}"></script>
</#macro>

<style>
    button {
        background:rgb(45, 202, 147);
        opacity:0.86;
        color: white;
        font-family: PingFangSC-Semibold, sans-serif;
    }

    ::-webkit-scrollbar-track-piece {

        background-color:#f8f8f8;

    }

    ::-webkit-scrollbar {

        width:15px;

        height:15px;

    }

    ::-webkit-scrollbar-thumb {

        background-color:#dddddd;

        background-clip:padding-box;

        min-height:28px;

    }

    ::-webkit-scrollbar-thumb:hover {

        background-color:#bbb;

    }

    html, body {
        height: 100%;
        padding: 0px;
        margin: 0px;
        overflow: hidden;
    }

    .red .gantt_cell, .odd.red .gantt_cell,
    .red .gantt_task_cell, .odd.red .gantt_task_cell {
        background-color: #FDE0E0;
    }

    .green .gantt_cell, .odd.green .gantt_cell,
    .green .gantt_task_cell, .odd.green .gantt_task_cell {
        background-color: #BEE4BE;
    }
    .gantt_task_progress {
        text-align: left;
        padding-left: 10px;
        box-sizing: border-box;
        color: white;
        font-weight: bold;
    }
    .weekend{
        background: #F0DFE5 !important;
    }
    .plan {
        color: green;
    }
    .acture {
        color: orange;
    }
    .high {
        border: 0px solid #d96c49;
        color: #d96c49;
        background: #d96c49;
    }

    .high .gantt_task_progress {
        background: #db2536;
    }

    .medium {
        border: 2px solid #34c461;
        color: #34c461;
        background: #34c461;
    }

    .medium .gantt_task_progress {
        background: #23964d;
    }

    .low {
        border: 0px !important;
        color: #009688;
        background: #009688;
    }

    .low .gantt_task_progress {
        background: #009688;
    }

    .gantt-fullscreen {
        position: absolute;
        bottom: 20px;
        right: 20px;
        width: 30px;
        height: 30px;
        padding: 2px;
        font-size: 32px;
        background: transparent;
        cursor: pointer;
        opacity: 0.5;
        text-align: center;
        -webkit-transition: background-color 0.5s, opacity 0.5s;
        transition: background-color 0.5s, opacity 0.5s;
    }

    .gantt-fullscreen:hover {
        background: rgba(150, 150, 150, 0.5);
        opacity: 1;
    }
</style>
