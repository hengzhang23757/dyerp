<#macro search>
    <section id="content">
        <section class="vbox">
            <section class="scrollable padder water-mark-visible-ff">
                <div class="gantt_ot" style="width:auto; margin:100px; auto">
                    <div class="gantt"></div>
                </div>
            </section>
        </section>
        <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen"
           data-target="#nav"></a>
    </section>
    <aside class="bg-light lter b-l aside-md hide" id="notes">
        <div class="wrapper">Notification</div>
    </aside>
    <link rel="stylesheet" href="/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="/css/prettify.min.css">
    <script src="/js/common/jquery.fn.gantt.js?t=${currentDate?c}" charset ="GB2312"></script>
    <script src="/js/common/prettify.min.js"></script>
    <script src="/js/common/jquery.blockUI.js" type="text/javascript" ></script>
    <script src="/js/common/laydate.js" type="text/javascript" ></script>
    <script src="/js/common/b.tabs.js?t=${currentDate?c}"></script>
    <script src="/js/common/selectize.js" type="text/javascript"></script>
    <script src="/js/common/moment.min.js" type="text/javascript"></script>
    <script src="/js/plan/orderPlan.js?t=${currentDate?c}"></script>

</#macro>

<style>
    button {
        background:rgb(45, 202, 147);
        opacity:0.86;
        color: white;
        font-family: PingFangSC-Semibold, sans-serif;
    }
</style>
<style type="text/css">
    body {
        font-family: Helvetica, Arial, sans-serif;
        font-size: 13px;
        padding: 0 0 50px 0;
    }
</style>



<#--<#macro search>-->
    <#--<section id="content">-->
        <#--<section class="vbox">-->
            <#--<section class="scrollable padder water-mark-visible-ff">-->
                <#--<div class="gantt_ot" style="width:auto; margin:100px; auto">-->
                    <#--<div class="gantt"></div>-->
                <#--</div>-->
            <#--</section>-->
        <#--</section>-->
        <#--<a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen"-->
           <#--data-target="#nav"></a>-->
    <#--</section>-->
    <#--<aside class="bg-light lter b-l aside-md hide" id="notes">-->
        <#--<div class="wrapper">Notification</div>-->
    <#--</aside>-->
    <#--<link rel="stylesheet" href="/css/bootstrap.min.css" type="text/css">-->
    <#--<link rel="stylesheet" href="/css/style.css">-->
    <#--<link rel="stylesheet" href="/css/prettify.min.css">-->
    <#--<script src="/js/common/jquery.fn.gantt.js?t=${currentDate?c}" charset ="GB2312"></script>-->
    <#--<script src="/js/common/prettify.min.js"></script>-->
    <#--<script src="/js/common/jquery.blockUI.js" type="text/javascript" ></script>-->
    <#--<script src="/js/common/laydate.js" type="text/javascript" ></script>-->
    <#--<script src="/js/common/b.tabs.js?t=${currentDate?c}"></script>-->
    <#--<script src="/js/common/selectize.js" type="text/javascript"></script>-->
    <#--<script src="/js/common/moment.min.js" type="text/javascript"></script>-->
    <#--<script src="/js/plan/orderPlan.js?t=${currentDate?c}"></script>-->

<#--</#macro>-->

<#--<style>-->
    <#--button {-->
        <#--background:rgb(45, 202, 147);-->
        <#--opacity:0.86;-->
        <#--color: white;-->
        <#--font-family: PingFangSC-Semibold, sans-serif;-->
    <#--}-->
<#--</style>-->
<#--<style type="text/css">-->
    <#--body {-->
        <#--font-family: Helvetica, Arial, sans-serif;-->
        <#--font-size: 13px;-->
        <#--padding: 0 0 50px 0;-->
    <#--}-->
<#--</style>-->

