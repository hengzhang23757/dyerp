<#macro search>
<section id="content">
    <section class="vbox">
        <section class="scrollable padder water-mark-visible-ff">
            <div id="mainFrameTabs">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active noclose" id="orderListTab"><a href="#orderListDiv" data-toggle="tab">版单信息</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade in active" id="orderListDiv" style="background-color: #f7f7f7;overflow-y:auto">
                        <div class="col-md-12" style="padding-top: 20px;">
                            <section class="panel panel-default">
                                <div class="panel-body" style="text-align: left">
                                    <div class="row" style="margin-left: 0;margin-bottom: 10px;">
                                        <div style="text-align: center;font-family: PingFangSC-Semibold;">
                                            <table id="clothesVersionProcessTable" class="table table-striped table-hover" style="width: 100%">
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>

                </div>
            </div>
            <div class="row">
                <#include "fb_clothesVersionProcessAdd.ftl">
                <@entities></@entities>
            </div>
        </section>
    </section>
    <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen"
       data-target="#nav"></a>
</section>
<aside class="bg-light lter b-l aside-md hide" id="notes">
    <div class="wrapper">Notification</div>
</aside>
</section>
<link rel="stylesheet" href="/css/bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="/css/b.tabs.css" type="text/css">
<script src="/js/common/laydate.js" type="text/javascript" ></script>
<link rel="stylesheet" href="/css/selectize.bootstrap3.css" type="text/css">
<script src="/js/common/selectize.js" type="text/javascript"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.bootstrap.min.css">
<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="/js/common/moment.min.js" type="text/javascript"></script>
<script src="/js/common/b.tabs.js?t=${currentDate?c}"></script>
<script src="/js/common/jquery.blockUI.js" type="text/javascript" ></script>
<script src="/js/versionProcess/clothesVersionProcess.js?t=${currentDate?c}"></script>

</#macro>

<style>
    button {
        background:rgb(45, 202, 147);
        opacity:0.86;
        color: white;
        font-family: PingFangSC-Semibold, sans-serif;
    }
</style>
