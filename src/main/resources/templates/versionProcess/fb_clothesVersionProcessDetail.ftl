<!DOCTYPE html>
<html>
<head>
    <#include "../feedback/fb_script.ftl">
    <@script> </@script>
    <link rel="stylesheet" href="/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="/css/b.tabs.css" type="text/css">
    <link rel="stylesheet" href="/css/layui.css" type="text/css">
    <script src="/js/common/layui.all.js" type="text/javascript" ></script>
    <script src="/js/versionProcess/clothesVersionProcessDetail.js?t=${currentDate?c}"></script>
</head>

<body>
<input  type="hidden" value="${basePath}"  id="basePath"/>
<input  type="hidden" value="${orderName}"  id="orderName"/>
<div class="col-md-12" style="padding-top: 20px;padding-bottom:10px;margin-left: 10px">
    <section class="layui-col-md12" style="margin: 0 auto; float: none;">
        <div class="layui-card">
            <div class="layui-card-body layui-text">
                <div id="toolbar">
                    <div>
                        <button type="button" class="layui-btn layui-btn-sm" data-type="addRow" title="添加进度">
                            <i class="layui-icon layui-icon-add-1"></i> 添加进度
                        </button>
                    </div>
                </div>
                <div id="tableRes" class="table-overlay">
                    <table id="dataTable" lay-filter="dataTable" class="layui-hide"></table>
                </div>
                <div id="action" class="text-center">
                    <button type="button" name="btnSave" class="layui-btn" data-type="save"><i class="layui-icon layui-icon-ok-circle"></i>保存</button>
                    <button type="reset" name="btnReset" class="layui-btn layui-btn-primary">取消</button>
                </div>
            </div>
        </div>

    </section>
</div>
</body>
</html>
<style>
    button {
        background:rgb(45, 202, 147);
        opacity:0.86;
        color: white;
        font-family: PingFangSC-Semibold;
    }

    /*您可以将下列样式写入自己的样式表中*/
    .childBody{padding: 15px;}

    /*layui 元素样式改写*/
    .layui-btn-sm{line-height: normal; font-size: 12.5px;}
    .layui-table-view .layui-table-body{min-height: 256px;}
    .layui-table-cell .layui-input.layui-unselect{height: 30px; line-height: 30px;}

    /*设置 layui 表格中单元格内容溢出可见样式*/
    .table-overlay .layui-table-view,
    .table-overlay .layui-table-box,
    .table-overlay .layui-table-body{overflow: visible;}
    .table-overlay .layui-table-cell{height: auto; overflow: visible;}

    /*文本对齐方式*/
    .text-center{text-align: center;}
</style>
