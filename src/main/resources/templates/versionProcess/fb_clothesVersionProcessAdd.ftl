<#macro entities>
<#--<section class="panel panel-default">-->
    <div id="editPro" style="display: none;cursor:default">
        <div style="text-align: right;">
            <a id="editNo" href="#" style="font-size:20px"><i class="fa fa-times icon" style="color: rgb(182,182,182)"></i></a>
        </div>
        <div style="overflow-x: auto;overflow-y:auto;">
            <table style="border-collapse:separate;border-spacing: 10px;width: 100%;">
                <tr>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">版单号</label>
                    </td>
                    <td>
                        <input id="clothesVersionNumber" class="form-control" autocomplete="off" style="width: 250px;margin-bottom: 15px;border-top: none;border-right: none;border-left: none;">
                    </td>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">订单号</label>
                    </td>
                    <td>
                        <select id="orderName" class="form-control" autocomplete="off" style="width: 250px;margin-bottom: 15px;border-top: none;border-right: none;border-left: none;"></select>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">款式</label>
                    </td>
                    <td>
                        <input id="styleDescription" class="form-control" style="margin-bottom: 15px;width: 250px;border-top: none;border-right: none;border-left: none;">
                    </td>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">日期</label>
                    </td>
                    <td>
                        <input id="beginDate" class="form-control" style="margin-bottom: 15px;width: 250px;border-top: none;border-right: none;border-left: none;">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">类型</label>
                    </td>
                    <td>
                        <select id="processType" class="form-control" style="margin-bottom: 15px;width: 250px;border-top: none;border-right: none;border-left: none;">
                            <option value="">请选择操作类型</option>
                            <option value="初次打版">初次打版</option>
                            <option value="第二次打版">第二次打版</option>
                            <option value="第三次打版">第三次打版</option>
                            <option value="第四次打版">第四次打版</option>
                        </select>
                    </td>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">备注</label>
                    </td>
                    <td>
                        <input name="processDescription" class="form-control" style="margin-bottom: 15px;width: 250px;border-top: none;border-right: none;border-left: none;">
                    </td>
                </tr>
            </table>
        </div>
        <div class="col-sm-12" style="padding-top: 10px">
            <button id="editYes" class="btn btn-s-lg" style="border-radius: 5px;color: white;font-family: PingFangSC-Semibold, sans-serif;"  style="text-align: center;">保存</button>
        </div>
    </div>
<#--</section>-->
</#macro>

<style>
    button {
        background:rgb(45, 202, 147);
        opacity:0.86;
        color: white;
        font-family: PingFangSC-Semibold, sans-serif;
    }
</style>