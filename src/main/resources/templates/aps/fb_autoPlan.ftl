<#macro search>
    <section id="content">
        <section class="vbox">
            <section class="scrollable padder water-mark-visible-ff">
                <div class="layui-row">
                    <form class="layui-form" style="margin: 0 auto;padding-top: 3px;" lay-filter="baseInfo">
                        <table>
                            <tr>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label">单号</label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <input type="text" class="layui-input" name="clothesVersionNumber" placeholder="请输入单号" id="clothesVersionNumber">
                                </td>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label">款号</label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <input type="text" class="layui-input" name="orderName" id="orderName" placeholder="请输入款号">
                                </td>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label">组名</label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <select type="text" name="groupName" id="groupName">
                                        <option value="车缝A组">车缝A组</option>
                                        <option value="车缝B组">车缝B组</option>
                                        <option value="车缝C组">车缝C组</option>
                                        <option value="车缝D组">车缝D组</option>
                                        <option value="车缝E组">车缝E组</option>
                                        <option value="车缝F组">车缝F组</option>
                                        <option value="车缝G组">车缝G组</option>
                                        <option value="车缝H组">车缝H组</option>
                                    </select>
                                </td>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label">工时</label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <select type="text" name="workHour" class="layui-input" id="workHour">
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10" selected>10</option>
                                        <option value="11">11</option>
                                        <option value="12">12</option>
                                    </select>
                                </td>
<#--                                <td style="margin-bottom: 15px;">-->
<#--                                    <div class="layui-input-inline" style="margin-left: 10px">-->
<#--                                        <button class="layui-btn layui-btn-normal" lay-submit lay-filter="loadBasicData">加载</button>-->
<#--                                    </div>-->
<#--                                </td>-->
                                <td style="margin-bottom: 15px;">
                                    <div class="layui-input-inline" style="margin-left: 10px">
                                        <button class="layui-btn" lay-submit lay-filter="profile">配置</button>
                                    </div>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <div class="layui-input-inline" style="margin-left: 10px">
                                        <button class="layui-btn" lay-submit lay-filter="orderSchedule">排产</button>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
                <div id="profilePage" hidden>
                    <form class="layui-form" action="">
                        <div class="layui-form-item" style="margin-top: 20px;">
                            <label class="layui-form-label" style="width:110px !important;">车缝人员</label>
                            <div class="layui-input-block" id="sewEmp">
                            </div>
                        </div>
                        <div class="layui-form-item" style="margin-top: 20px;">
                            <label class="layui-form-label" style="width:110px !important;">工序列表</label>
                            <div class="layui-input-block" id="procedure">
                            </div>
                        </div>
                    </form>
                </div>
                <table id="scheduleTable" lay-filter="scheduleTable"></table>
                <div id="scheduleResult" style="padding-left: 20px;" hidden>
                    <table id="scheduleDetailTable" name = "scheduleDetailTable" style="background-color: white; color: black">
                    </table>
                    <script type="text/html" id="toolbarDetailTop">
                        <div class="layui-btn-container">
                            <button class="layui-btn layui-btn-sm" lay-event="exportExcel">导出</button>
                            <button class="layui-btn layui-btn-sm" lay-event="balance">平衡图</button>
                        </div>
                    </script>
                </div>

                <div id="echartFigure" style="padding-left: 20px;" hidden>
                    <div class="layui-row layui-col-space15">
                        <div class="layui-col-md12">
                            <div class="layui-card">
                                <div class="layui-card-header">工序平衡图</div>
                                <div class="layui-card-body">
                                    <div id="balanceFigure" class="chart" style="width: 100%;height:400px;margin: 0 auto"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <script type="text/html" id="toolbarTop">
                    <div class="layui-btn-container">
                        <button class="layui-btn layui-btn-sm" lay-event="exportExcel">导出</button>
                        <button class="layui-btn layui-btn-sm" lay-event="saveScheduleData">保存</button>
                        <button class="layui-btn layui-btn-sm" lay-event="fillHour">填充</button>
                        <button class="layui-btn layui-btn-sm" lay-event="transRecord">切换</button>
                    </div>
                </script>

                <script type="text/html" id="barTop">
                    <a class="layui-btn layui-btn-xs" lay-event="detail">详情</a>
                    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
                </script>

            </section>
        </section>

    </section>
    <aside class="bg-light lter b-l aside-md hide" id="notes">
        <div class="wrapper">Notification</div>
    </aside>
    <link rel="stylesheet" href="/css/layui.css" type="text/css">
    <link rel="stylesheet" href="/css/soulTable.css" type="text/css">
    <link rel="stylesheet" href="/css/selectize.bootstrap3.css" type="text/css">
    <script src="/js/common/layer.js"></script>
    <script src="/js/common/xm-select.js?t=${currentDate?c}" type="text/javascript"></script>
    <script src="/js/common/selectize.js" type="text/javascript"></script>
    <script src="/js/common/moment.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="https://cdn.bootcss.com/echarts/4.2.1-rc1/echarts.min.js"></script>
    <script src="/js/autoPlan/autoPlan.js?t=${currentDate?c}"></script>

</#macro>

<style>
    button {
        background:rgb(45, 202, 147);
        opacity:0.86;
        color: white;
        font-family: PingFangSC-Semibold, sans-serif;
    }
    ::-webkit-scrollbar-track-piece {

        background-color:#f8f8f8;

    }

    ::-webkit-scrollbar {

        width:15px;

        height:15px;

    }

    ::-webkit-scrollbar-thumb {

        background-color:#dddddd;

        background-clip:padding-box;

        min-height:28px;

    }

    ::-webkit-scrollbar-thumb:hover {

        background-color:#bbb;
    }

    div[lay-id="scheduleDetailTable"] .layui-table-cell {
        height: auto !important;
    }
</style>
