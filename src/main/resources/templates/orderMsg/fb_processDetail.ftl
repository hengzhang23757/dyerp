<#macro processDetail>
<div id="processDetail" name="processDetail" style="background-color: white; color: black;font-size: 20px;">
    <div id="contentPage" style="background-color: white">
        <h1 style="text-align: center;font-size: 20px; font-weight: 800; margin-top: 0px">中山德悦服饰生产制单</h1>
        <div class="row" style="margin:0">
            <div class="col-md-12">
                <section class="panel panel-default">
                    <header class="panel-heading font-bold" style="font-size: 16px;background-color: #dcd9d9;border-color: black;">
                        <span class="label bg-success pull-right"></span>基本信息
                    </header>
                    <div id="basicDetailInfo"></div>
                </section>
            </div>
            <div class="row" style="margin:0">
                <div class="col-md-12">
                    <section class="panel panel-default">
                        <header class="panel-heading font-bold" style="font-size: 16px;background-color: #dcd9d9;border-color: black;">
                            <span class="label bg-success pull-right"></span>款式图
                        </header>
                        <div id="styleImgDiv" style="text-align: center; height: 300px"></div>
                    </section>
                </div>
            </div>
        </div>
        <div class="row" style="margin:0">
            <div class="col-md-12">
                <section class="panel panel-default">
                    <header class="panel-heading font-bold table-head" style="font-size: 16px;background-color: #dcd9d9;border-color: black;">
                        <span class="label bg-success pull-right"></span>下单细数
                    </header>
                    <div id="orderClothesTable"></div>
                </section>
            </div>
        </div>

        <div class="row" style="margin:0">
            <div class="col-md-12">
                <section class="panel panel-default">
                    <header class="panel-heading font-bold" style="font-size: 16px;background-color:#dcd9d9;border-color: black;">
                        <span class="label bg-success pull-right"></span>面料信息
                    </header>
                    <div id="manufactureFabricTable"></div>
                </section>
            </div>
        </div>

        <div class="row" style="margin:0">
            <div class="col-md-12">
                <section class="panel panel-default">
                    <header class="panel-heading font-bold" style="font-size: 16px;background-color: #dcd9d9;border-color: black;">
                        <span class="label bg-success pull-right"></span>辅料信息
                    </header>
                    <div id="manufactureAccessoryTable"></div>
                </section>
            </div>
        </div>
    </div>
</div>
    <style>
        section {
            border-color: #000000 !important;
        }
    </style>
</#macro>

