<#macro addSizeDetail>
    <div>
        <div id="addSizeDetail">
            <form class="layui-form" style="margin: 0 auto;max-width:100%;padding-top: 10px;">
                <div class="layui-col-md12" style="width: 100%;padding-top: 5px">
                    <div class="layui-tab layui-tab-card">
                        <textarea id="sizeDetailTextarea"></textarea>
                    </div>
                </div>
            </form>
        </div>
        <style>
            .tox-tinymce-aux {
                z-index:199910170
            }
        </style>
    </div>
</#macro>
