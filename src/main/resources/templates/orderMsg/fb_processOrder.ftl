<#macro search>
<section id="content">
    <section class="vbox">
        <section class="scrollable">
            <div class="layui-row">
                <form class="layui-form" style="margin: 0 auto;padding-top: 3px;" lay-filter="formBaseInfo">
                    <table>
                    <tr>
                        <td style="text-align: right;margin-bottom: 15px;">
                            <label class="layui-form-label" style="width: 100px">单号</label>
                        </td>
                        <td style="margin-bottom: 15px;">
                            <input type="text" id="clothesVersionNumber" autocomplete="off" class="layui-input">
                        </td>
                        <td style="text-align: right;margin-bottom: 15px;">
                            <label class="layui-form-label" style="width: 100px">款号</label>
                        </td>
                        <td style="margin-bottom: 15px;">
                            <input type="text" id="orderName" autocomplete="off" class="layui-input">
                        </td>
                        <td style="text-align: right;margin-bottom: 15px;">
                            <label class="layui-form-label" style="width: 100px">品牌</label>
                        </td>
                        <td style="margin-bottom: 15px;">
<#--                            <select id="customerName" autocomplete="off"></select>-->
                            <div id="customerName" style="width: 200px;" class="xm-select-demo"></div>
                        </td>
                        <td style="text-align: right;margin-bottom: 15px;">
                            <label class="layui-form-label" style="width: 100px">季度</label>
                        </td>
                        <td style="margin-bottom: 15px;">
                            <select id="seasonName" autocomplete="off"></select>
                        </td>
                        <td style="text-align: right;margin-bottom: 15px;">
                            <label class="layui-form-label"></label>
                        </td>
                        <td style="margin-bottom: 15px;">
                            <div class="layui-input-inline">
                                <button class="layui-btn" lay-submit lay-filter="searchBeat">搜索</button>
                            </div>
                        </td>
                    </tr>
                </table>
                </form>
            </div>
            <table id="processOrderTable" lay-filter="processOrderTable"></table>

            <script type="text/html" id="toolbarTop">
                <div class="layui-btn-container">
                    <button class="layui-btn layui-btn-sm" lay-event="refresh">刷新</button>
                    <button class="layui-btn layui-btn-sm" lay-event="clearFilter">清除所有筛选</button>
                    <button class="layui-btn layui-btn-sm" lay-event="addBasicOrderData">录入基础信息</button>
                    <button class="layui-btn layui-btn-sm" lay-event="addFabricData">录入面料信息</button>
                    <button class="layui-btn layui-btn-sm" lay-event="addAccessoryData">录入辅料信息</button>
                    <button class="layui-btn layui-btn-sm" lay-event="addPrice">录入价格</button>
                    <button class="layui-btn layui-btn-sm" lay-event="copyOrder">复制制单</button>
                    <button class="layui-btn layui-btn-sm" lay-event="addColorData">添加颜色</button>
                    <button class="layui-btn layui-btn-sm" lay-event="addSizeData">添加尺码</button>
                    <button class="layui-btn layui-btn-sm" lay-event="normalOrder">未关订单</button>
                    <button class="layui-btn layui-btn-sm" lay-event="closedOrder">已关订单</button>
                    <button class="layui-btn layui-btn-sm" lay-event="exportExcel">导出</button>
                </div>
            </script>

            <script type="text/html" id="barTop">
                <a class="layui-btn layui-btn-xs" lay-event="detail">详情</a>
                <a class="layui-btn layui-btn-xs layui-btn-warm" lay-event="cutFabric">裁剪单</a>
                <a class="layui-btn layui-btn-xs" lay-event="size">工艺</a>
                <#--<a class="layui-btn layui-btn-xs" lay-event="materialCard">物料卡</a>-->
                <a class="layui-btn layui-btn-xs layui-btn-normal" lay-event="changeImage">公仔图</a>
                <a class="layui-btn layui-btn-xs" lay-event="send">发送</a>
<#--                <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>-->
                <a class="layui-btn layui-btn-xs" lay-event="closeOrder">关单</a>
            </script>

            <script type="text/html" id="baseInfoChildBar">
                <a class="layui-btn layui-btn-xs layui-btn-normal" onclick="updateBasicOrderData(this)">修改</a>
            </script>

            <script type="text/html" id="accessoryDataChildBar">
                <a class="layui-btn layui-btn-xs layui-btn-normal" onclick="updateAccessoryData(this)">修改</a>
                <a class="layui-btn layui-btn-danger layui-btn-xs" onclick="deleteAccessoryData(this)">删除</a>
            </script>

            <script type="text/html" id="requirementChildBar">
                <a class="layui-btn layui-btn-xs layui-btn-normal" onclick="updateRequirementData(this)">修改</a>
                <a class="layui-btn layui-btn-danger layui-btn-xs" onclick="deleteRequirementData(this)">删除</a>
            </script>

            <script type="text/html" id="fabricDetailChildBar">
                <a class="layui-btn layui-btn-xs layui-btn-normal" onclick="updateFabricDetail(this)">修改</a>
                <a class="layui-btn layui-btn-danger layui-btn-xs" onclick="deleteFabricDetail(this)">删除</a>
            </script>

            <script type="text/html" id="clothesChildBar">
                <a class="layui-btn layui-btn-xs layui-btn-normal" onclick="updateClothesDetail(this)">修改</a>
                <a class="layui-btn layui-btn-danger layui-btn-xs" onclick="deleteClothesDetail(this)">删除</a>
            </script>

            <script type="text/html" id="sizeDetailChildBar">
                <a class="layui-btn layui-btn-xs layui-btn-normal" onclick="updateSizeDetailData(this)">修改</a>
            </script>

            <script type="text/html" id="toolbarTopFabric">
                <a class="layui-btn layui-btn-xs" lay-event="">扁机录入(如果有必要)</a>
                <a class="layui-btn layui-btn-xs" lay-event="addGroup">加一组</a>
                <a class="layui-btn layui-btn-xs" lay-event="minusGroup">减一组</a>
<#--                <a class="layui-btn layui-btn-xs" lay-event="fillup">填充</a>-->
            </script>



        </section>
    </section>
    <#include "fb_basicOrderInfoAdd.ftl">
    <@entities></@entities>
    <#include "fb_basicOrderInfoUpdate.ftl">
    <@basicInfoUpdate></@basicInfoUpdate>
</section>

    <link rel="stylesheet" href="/css/layui.css" type="text/css">
    <link rel="stylesheet" href="/css/step.css" type="text/css">
    <link rel="stylesheet" href="/css/soulTable.css" type="text/css">
    <link rel="stylesheet" href="/css/publish.css" type="text/css">
    <link rel="stylesheet" href="/css/cropper.css" type="text/css">
    <link rel="stylesheet" href="/css/selectize.bootstrap3.css" type="text/css">

    <#--<script src="/js/common/layui.js" type="text/javascript"></script>-->
    <script src="/js/common/layer.js"></script>
    <script src="/js/step/step.js?t=${currentDate?c}" type="text/javascript"></script>
    <script type="text/javascript" src="/js/common/move.js"></script>
    <script type="text/javascript" src="/js/common/publishImg.js"></script>
    <script src="/js/common/xm-select.js?t=${currentDate?c}" type="text/javascript"></script>
    <script src="/js/common/selectize.js" type="text/javascript"></script>
    <script src="/js/common/html2canvas.js" type="text/javascript"></script>
    <script src="/js/common/jsPdf.debug.js" type="text/javascript"></script>
    <script src="/js/orderMsg/processOrder.js?t=${currentDate?c}"></script>
    <script src="/js/common/moment.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/common/tinymce/tinymce.min.js"></script>
    <script type="text/javascript" src="/js/common/tinymce/zh_CN.js"></script>
</#macro>

<style>
    button {
        background:rgb(45, 202, 147);
        opacity:0.86;
        color: white;
        font-family: PingFangSC-Semibold, sans-serif;
    }
    ::-webkit-scrollbar-track-piece {

        background-color:#f8f8f8;

    }

    ::-webkit-scrollbar {

        width:15px;

        height:15px;

    }

    ::-webkit-scrollbar-thumb {

        background-color:#dddddd;

        background-clip:padding-box;

        min-height:28px;

    }

    ::-webkit-scrollbar-thumb:hover {

        background-color:#bbb;

    }
</style>
