<#macro addAccessoryData>
    <div id="addAccessoryData">
        <form class="layui-form" style="margin: 0 auto;width:100%; min-height:400px; padding-top: 5px;">
            <table class="layui-hide" id="accessoryDataTable" lay-filter="accessoryDataTable"></table>
        </form>
    </div>
</#macro>