<#macro basicInfoUpdate>
    <div id="basicOrderInfoUpdate" style="display: none;cursor:default;">
        <form class="layui-form" style="margin: 5px;padding-top: 40px;" lay-filter="basicInfoUpdate">
            <div class="layui-row layui-col-space10">
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">品牌</label>
                    <div class="layui-input-inline">
                        <input type="text" name="customerName" autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">订购方式</label>
                    <div class="layui-input-inline">
                        <input type="text" name="purchaseMethod" autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">款名</label>
                    <div class="layui-input-inline">
                        <input type="text" name="styleName" autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">单号</label>
                    <div class="layui-input-inline">
                        <input type="text" name="clothesVersionNumber" autocomplete="off" class="layui-input" readonly="true">
                    </div>
                </div>
            </div>
            <div class="layui-row layui-col-space10">
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">款号</label>
                    <div class="layui-input-inline">
                        <input type="text" name="orderName" autocomplete="off" class="layui-input" readonly="true">
                    </div>
                </div>
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">季度</label>
                    <div class="layui-input-inline">
                        <input type="text" name="season" autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">跟单</label>
                    <div class="layui-input-inline">
                        <input type="text" name="buyer" autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">样板类型</label>
                    <div class="layui-input-inline">
                        <input type="text" name="modelType" autocomplete="off" class="layui-input">
                    </div>
                </div>
            </div>
            <div class="layui-row layui-col-space10">
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">收单日期</label>
                    <div class="layui-input-inline">
                        <input type="text" name="deadLine" autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">交期</label>
                    <div class="layui-input-inline">
                        <input type="text" name="deliveryDate" autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">允许加裁%</label>
                    <div class="layui-input-inline">
                        <input type="text" name="additional" autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">单价</label>
                    <div class="layui-input-inline">
                        <input type="text" name="price" autocomplete="off" class="layui-input">
                    </div>
                </div>
            </div>
            <div class="layui-row layui-col-space10">
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">印绣花价</label>
                    <div class="layui-input-inline">
                        <input type="text" name="printPrice" autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">洗水价</label>
                    <div class="layui-input-inline">
                        <input type="text" name="washPrice" autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">颜色</label>
                    <div class="layui-input-inline">
                        <input type="text" name="colorInfo" autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">尺码</label>
                    <div class="layui-input-inline">
                        <input type="text" name="sizeInfo" autocomplete="off" class="layui-input">
                    </div>
                </div>
            </div>
            <div class="layui-row layui-col-space10">
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">备注</label>
                    <div class="layui-input-inline">
                        <input type="text" name="remark" autocomplete="off" class="layui-input">
                    </div>
                </div>
            </div>
        </form>
    </div>
</#macro>

<style>
    .layui-col-md3 .layui-form-label {
        width:90px
    }
    .layui-col-md6 .layui-form-label {
        width:90px
    }
    .layui-col-md12 .layui-form-label {
        width:90px
    }

    #staffAdd .text {
        width: 8%;
        text-align: center;
    }

    #staffAdd .content {
        width: 17%;
    }

    #staffAdd input {
        border-radius: 0;
        border-color: white;
    }

    #staffAdd select {
        border-color: white;
    }

    #staffAdd td {
        border: solid black;
        border-width: 0px 1px 1px 0px;
    }

    #staffAdd table {
        border: solid black;
        border-width: 1px 0px 0px 1px;
    }
</style>