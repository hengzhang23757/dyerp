<#macro entities>
    <div class="layui-carousel" id="stepForm" lay-filter="stepForm" style="display: none; margin: 10px auto;">
        <div carousel-item>
            <div>
                <input  type="hidden" value="${userName!?string}"  id="userName"/>
                <form class="layui-form" style="margin: 0 auto;padding-top: 40px;" lay-filter="baseInfo">
                    <div class="layui-row">
                        <table>
                            <tr>
                                <td style="text-align: right;padding-bottom: 30px;">
                                    <label class="layui-form-label layui-required" style="font-weight: bolder; font-size: medium; width: 150px;">品牌</label>
                                </td>
                                <td style="padding-bottom: 30px;">
<#--                                    <select type="text" name="customerNameAdd" id="customerNameAdd" autocomplete="off" class="layui-input"></select>-->
                                    <div id="customerNameAdd" style="width: 200px;" class="xm-select-demo"></div>
                                </td>
                                <td style="padding-bottom: 30px;">
                                    <label class="layui-form-label layui-required" style="font-weight: bolder; width: 150px; font-size: medium">订购方式</label>
                                </td>
                                <td style="padding-bottom: 30px;">
                                    <select type="text" name="purchaseMethod" autocomplete="off" class="layui-input">
                                        <option value="FOB">FOB</option>
                                        <option value="CFOB">CFOB</option>
                                        <option value="纯加工">纯加工</option>
                                        <option value="来料加工">来料加工</option>
                                        <option value="ODM">ODM</option>
                                    </select>
                                </td>
                                <td style="padding-bottom: 30px;">
                                    <label class="layui-form-label layui-required" style="font-weight: bolder; width: 150px; font-size: medium">款式描述</label>
                                </td>
                                <td style="padding-bottom: 30px;">
                                    <input type="text" name="styleName" autocomplete="off" class="layui-input">
                                </td>
                                <td style="padding-bottom: 30px;">
                                    <label class="layui-form-label layui-required" style="font-weight: bolder; width: 150px; font-size: medium">季度</label>
                                </td>
                                <td style="padding-bottom: 30px;">
                                    <input type="text" name = "season" id="season" autocomplete="off" class="layui-input">
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-bottom: 30px;">
                                    <label class="layui-form-label layui-required" style="font-weight: bolder; width: 150px; font-size: medium">单号</label>
                                </td>
                                <td style="padding-bottom: 30px;">
                                    <input type="text" name="clothesVersionNumber" autocomplete="off" class="layui-input">
                                </td>
                                <td style="padding-bottom: 30px;">
                                    <label class="layui-form-label layui-required" style="font-weight: bolder; width: 150px; font-size: medium">款号</label>
                                </td>
                                <td style="padding-bottom: 30px;">
                                    <input type="text" name="orderName" autocomplete="off" class="layui-input">
                                </td>
                                <td style="padding-bottom: 30px;">
                                    <label class="layui-form-label layui-required" style="font-weight: bolder; width: 150px; font-size: medium">分组系列</label>
                                </td>
                                <td style="padding-bottom: 30px;">
                                    <select type="text" name="modelType" autocomplete="off" class="layui-input">
                                        <option value="期货">期货</option>
                                        <option value="电商">电商</option>
                                        <option value="快返">快返</option>
                                        <option value="直播">直播</option>
                                    </select>
                                </td>
                                <td style="padding-bottom: 30px;">
                                    <label class="layui-form-label layui-required" style="font-weight: bolder; width: 150px; font-size: medium">接单日期</label>
                                </td>
                                <td style="padding-bottom: 30px;">
                                    <input type="text" name="deadLine" id="deadLine" autocomplete="off" class="layui-input">
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-bottom: 30px;">
                                    <label class="layui-form-label layui-required" style="font-weight: bolder; width: 150px; font-size: medium">交期</label>
                                </td>
                                <td style="padding-bottom: 30px;">
                                    <input type="text" name="deliveryDate" id="deliveryDate" autocomplete="off" class="layui-input">
                                </td>
                                <td style="padding-bottom: 30px;">
                                    <label class="layui-form-label layui-required" style="font-weight: bolder; width: 150px; font-size: medium">允许加裁%</label>
                                </td>
                                <td style="padding-bottom: 30px;">
                                    <input type="text" name="additional" id="additional" autocomplete="off" class="layui-input">
                                </td>
                                <td style="padding-bottom: 30px;">
                                    <label class="layui-form-label layui-required" style="font-weight: bolder; width: 150px; font-size: medium">备注</label>
                                </td>
                                <td style="padding-bottom: 30px;">
                                    <input type="text" name="remark" id="remark" autocomplete="off" value="无" class="layui-input">
                                </td>
                                <td></td>
                                <td></td>
                            </tr>
                        </table>
                    </div>
                    <div class="layui-row layui-col-space10">
                        <div class="layui-col-md12">
                            <label class="layui-form-label" style="font-weight: bolder; width: 150px; font-size: medium">尺码选择</label>
                            <div class="layui-input-inline" style="width: 76%">
                                <div id="sizeInfo"></div>
                            </div>
                        </div>
                    </div>
                </form>
                <form>
                    <div class="layui-col-md12">
                        <label class="layui-form-label" style="font-weight: bolder; width: 150px; font-size: medium;">颜色输入</label>
                        <div class="layui-input-inline" style="width: 76%">
                            <select id="colorSelect" multiple style="width:100%;" lay-ignore>
                            </select>
                        </div>
                    </div>
                </form>

                <form class="layui-form">
                    <div class="layui-form-item" style="text-align: center">
                        <button class="layui-btn" lay-submit lay-filter="formStep">
                            &emsp;下一步&emsp;
                        </button>
                    </div>
                </form>
            </div>
            <div>
                <form class="layui-form" style="margin: 0 auto;max-width:80%;padding-top: 40px;">
                    <div id="colorSizeTableDiv">
                        <table id="colorSizeTable" lay-filter="colorSizeTable"></table>
                    </div>

                    <div class="layui-form-item" style="text-align: center;" id="controlStep">
                        <button type="button" class="layui-btn layui-btn-primary pre">上一步</button>
                        <button class="layui-btn" lay-submit lay-filter="formStep2">
                            下一步
                        </button>
                    </div>
                </form>
            </div>
            <div>
                <div style="text-align: center;margin-top: 90px;">
                    <i class="layui-icon layui-circle"
                       style="color: white;font-size:30px;font-weight:bold;background: #52C41A;padding: 20px;line-height: 80px;">&#xe605;</i>
                    <div style="font-size: 24px;color: #333;font-weight: 500;margin-top: 30px;">
                        录入成功
                    </div>
                </div>
                <div style="text-align: center;margin-top: 50px;">
                    <button class="layui-btn next">再入一单</button>
                    <button class="layui-btn layui-btn-primary detail">查看详情</button>
                </div>
            </div>
        </div>
    </div>
</#macro>

<style>

    .tox-tinymce-aux {
        z-index:199910172
    }

    .layui-col-md3 .layui-form-label {
        width:90px
    }
    .layui-col-md6 .layui-form-label {
        width:90px
    }
    .layui-col-md12 .layui-form-label {
        width:90px
    }

    .layui-form-label.layui-required:after{
        content:"*";
        color:red;
        position: absolute;
        top:5px;
        left:15px;
    }
</style>