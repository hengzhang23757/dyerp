<#macro changeClothesImage>
    <div id="changeClothesImage">
        <form class="layui-form" style="margin: 0 auto;max-width:90%;padding-top: 10px;">
            <div class="layui-row layui-col-space10">
                <div class="layui-col-md12" id="clothesImageOperate">
                    <div class="layui-tab layui-tab-card" style="width: 90%; height:500px">
                        <textarea id="clothesImageTextarea"></textarea>
                    </div>
                </div>
            </div>
        </form>
    </div>
</#macro>