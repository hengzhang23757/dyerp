<#macro search>
    <section id="content">
        <section class="vbox">
            <section class="scrollable padder water-mark-visible-ff"  style="background-color: #f7f7f7;">
                <div class="col-md-12" style="padding-top: 10px;">
                    <section class="panel panel-default">
                        <div class="panel-body" style="margin-bottom: 10px">
                            <div class="row" style="margin-bottom: 10px">
                                <table style="border-collapse:separate;border-spacing: 10px;width: 100%;" id="baseInfo">
                                    <tr>
                                        <td style="text-align: right;margin-bottom: 15px;">
                                            <label class="control-label" style="">单号</label>
                                        </td>
                                        <td style="margin-bottom: 15px;">
                                            <input id="clothesVersionNumber" class="form-control" placeholder="请输入单号" autocomplete="off" style="width: 200px;">
                                        </td>
                                        <td style="text-align: right;margin-bottom: 15px;">
                                            <label class="control-label" style="">款号</label>
                                        </td>
                                        <td style="margin-bottom: 15px;">
                                            <input id="orderName" class="form-control" placeholder="请输入款号" style="width: 200px;">
                                        </td>
                                        <td style="text-align: right;margin-bottom: 15px;">
                                            <label class="control-label" style="">部位</label>
                                        </td>
                                        <td style="margin-bottom: 15px;">
                                            <select id="partName" class="form-control" autocomplete="off" style="width: 200px;"></select>
                                        </td>
                                        <td style="text-align: right;margin-bottom: 15px;">
                                            <label class="control-label" style="">床号</label>
                                        </td>
                                        <td style="margin-bottom: 15px;">
                                            <select id="bedNumber" class="form-control" style="width: 200px;"></select>
                                        </td>
                                        <td style="margin-bottom: 15px;">
                                            <button  class="btn" style="border-radius: 5px;color:white;font-family: PingFangSC-Semibold, sans-serif;width:70px;"  onclick="reportCut()">查找</button>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-12" style="padding-top: 20px;padding-bottom:10px" id="exportDiv" hidden>
                                <button  class="btn btn-s-lg" style="border-radius: 5px;color:white;"  onclick="exportData()">导出</button>
                                <button  class="btn btn-s-lg" style="border-radius: 5px;color:white;"  onclick="printDeal()">打印</button>
                            </div>
                            <div id="reportExcel" hidden style="overflow-x: auto;height: auto"></div>
                        </div>
                    </section>
                </div>
                <div class="row">
                    <div id="entities">
                    </div>
                </div>
                <iframe id="printf" src="" width="0" height="0" frameborder="0"></iframe>
            </section>
        </section>
        <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen"
           data-target="#nav"></a>
    </section>
    <aside class="bg-light lter b-l aside-md hide" id="notes">
        <div class="wrapper">Notification</div>
    </aside>
    </section>
    <link rel="stylesheet" href="/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="/css/b.tabs.css" type="text/css">
    <link rel="stylesheet" href="/css/handsontable.full.min.css" type="text/css">
    <script src="/js/common/handsontable.full.min.js" type="text/javascript" ></script>
    <script src="/js/common/zh-CN.js" type="text/javascript" ></script>
    <script src="/js/common/laydate.js" type="text/javascript" ></script>
    <script src="/js/common/jquery.blockUI.js" type="text/javascript" ></script>
    <script src="/js/common/b.tabs.js?t=${currentDate?c}"></script>
    <link rel="stylesheet" href="/css/selectize.bootstrap3.css" type="text/css">
    <script src="/js/common/selectize.js" type="text/javascript"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css" type="text/css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
    <script src="/js/common/export2Excel2.js" type="text/javascript" ></script>
    <script src="/js/orderMsg/cutReport.js?t=${currentDate?c}"></script>

</#macro>

<style>
    button {
        background:rgb(45, 202, 147);
        opacity:0.86;
        color: white;
        font-family: PingFangSC-Semibold, sans-serif;
    }

    #orderNameTips{
        margin-left: 60px
    };

    ::-webkit-scrollbar-track-piece {

        background-color:#f8f8f8;

    }

    ::-webkit-scrollbar {

        width:15px;

        height:15px;

    }

    ::-webkit-scrollbar-thumb {

        background-color:#dddddd;

        background-clip:padding-box;

        min-height:28px;

    }

    ::-webkit-scrollbar-thumb:hover {

        background-color:#bbb;

    }
</style>
