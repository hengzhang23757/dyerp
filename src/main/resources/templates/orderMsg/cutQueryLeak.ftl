<!DOCTYPE html>
<html lang="en" class="app">
<#include "../feedback/fb_script.ftl">
<@script> </@script>
<body>
<section class="vbox">
    <input  type="hidden" value="${basePath}"  id="basePath"/>
    <#if type == "detail">
        <input  type="hidden" value="${detailOrderName}"  id="detailOrderName"/>
        <input  type="hidden" value="${detailClothesVersionNumber}"  id="detailClothesVersionNumber"/>
    </#if>
    <section>
        <section class="hbox stretch">
            <#include "fb_cutQueryLeak.ftl">
            <@search> </@search>
        </section>
    </section>
</section>
</body>
</html>