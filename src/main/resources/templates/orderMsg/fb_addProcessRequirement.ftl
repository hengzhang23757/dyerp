<#macro addProcessRequirement>
    <div id="addProcessRequirement">
        <form class="layui-form" style="margin: 0 auto;max-width:90%;padding-top: 10px;">
            <div class="layui-row layui-col-space10">
                <div class="layui-col-md12" style="width: 90%; height:800px">
                    <div class="layui-tab layui-tab-card">
                        <div class="layui-form-item">
                            <label class="layui-form-label">工艺信息图片上传</label>
                            <div class="layui-input-inline" style="padding-top: 80px">
                                <div class="layui-upload-list layui-icon" id="addZmImgProcessRequirement">
                                    &#xe64a;
                                </div>
                            </div>
                        </div>
                        <div class="" style="padding-top: 50px">
                            <ul id="imgZmListProcessRequirement"></ul>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</#macro>