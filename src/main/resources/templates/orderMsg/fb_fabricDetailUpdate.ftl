<#macro fabricDetailUpdate>
    <div id="fabricDetailUpdate" style="display: none;cursor:default;">
        <table id="fabricDetailUpdateTable" name = "fabricDetailUpdateTable" style="background-color: white; color: black">
        </table>
    </div>
</#macro>

<style>
    .layui-col-md3 .layui-form-label {
        width:90px
    }
    .layui-col-md6 .layui-form-label {
        width:90px
    }
    .layui-col-md12 .layui-form-label {
        width:90px
    }

    #staffAdd .text {
        width: 8%;
        text-align: center;
    }

    #staffAdd .content {
        width: 17%;
    }

    #staffAdd input {
        border-radius: 0;
        border-color: white;
    }

    #staffAdd select {
        border-color: white;
    }

    #staffAdd td {
        border: solid black;
        border-width: 0px 1px 1px 0px;
    }

    #staffAdd table {
        border: solid black;
        border-width: 1px 0px 0px 1px;
    }
</style>



<#--<#macro fabricDetailUpdate>-->
    <#--<div id="fabricDetailUpdate" style="display: none;cursor:default;">-->
        <#--<form class="layui-form" style="margin: 5px;padding-top: 40px;" lay-filter="fabricDetailUpdate">-->
            <#--<div class="layui-row layui-col-space10">-->
                <#--<div class="layui-col-md3">-->
                    <#--<!-- 填充内容 &ndash;&gt;-->
                    <#--<label class="layui-form-label">面料名称</label>-->
                    <#--<div class="layui-input-inline">-->
                        <#--<input type="text" name="fabricName" autocomplete="off" class="layui-input">-->
                    <#--</div>-->
                <#--</div>-->
                <#--<div class="layui-col-md3">-->
                    <#--<!-- 填充内容 &ndash;&gt;-->
                    <#--<label class="layui-form-label">面料号</label>-->
                    <#--<div class="layui-input-inline">-->
                        <#--<input type="text" name="fabricNumber" autocomplete="off" class="layui-input">-->
                    <#--</div>-->
                <#--</div>-->
                <#--<div class="layui-col-md3">-->
                    <#--<!-- 填充内容 &ndash;&gt;-->
                    <#--<label class="layui-form-label">布封</label>-->
                    <#--<div class="layui-input-inline">-->
                        <#--<input type="text" name="fabricWidth" autocomplete="off" class="layui-input">-->
                    <#--</div>-->
                <#--</div>-->
                <#--<div class="layui-col-md3">-->
                    <#--<!-- 填充内容 &ndash;&gt;-->
                    <#--<label class="layui-form-label">克重</label>-->
                    <#--<div class="layui-input-inline">-->
                        <#--<input type="text" name="fabricWeight" autocomplete="off" class="layui-input">-->
                    <#--</div>-->
                <#--</div>-->
            <#--</div>-->
            <#--<div class="layui-row layui-col-space10">-->
                <#--<div class="layui-col-md3">-->
                    <#--<!-- 填充内容 &ndash;&gt;-->
                    <#--<label class="layui-form-label">面料直缩</label>-->
                    <#--<div class="layui-input-inline">-->
                        <#--<input type="text" name="fabricDeflation" autocomplete="off" class="layui-input">-->
                    <#--</div>-->
                <#--</div>-->
                <#--<div class="layui-col-md3">-->
                    <#--<!-- 填充内容 &ndash;&gt;-->
                    <#--<label class="layui-form-label">面料横缩</label>-->
                    <#--<div class="layui-input-inline">-->
                        <#--<input type="text" name="fabricShrink" autocomplete="off" class="layui-input">-->
                    <#--</div>-->
                <#--</div>-->
                <#--<div class="layui-col-md3">-->
                    <#--<!-- 填充内容 &ndash;&gt;-->
                    <#--<label class="layui-form-label">成衣直缩</label>-->
                    <#--<div class="layui-input-inline">-->
                        <#--<input type="text" name="clothesDeflation" autocomplete="off" class="layui-input">-->
                    <#--</div>-->
                <#--</div>-->
                <#--<div class="layui-col-md3">-->
                    <#--<!-- 填充内容 &ndash;&gt;-->
                    <#--<label class="layui-form-label">成衣横缩</label>-->
                    <#--<div class="layui-input-inline">-->
                        <#--<input type="text" name="clothesShrink" autocomplete="off" class="layui-input">-->
                    <#--</div>-->
                <#--</div>-->
            <#--</div>-->
            <#--<div class="layui-row layui-col-space10">-->
                <#--<div class="layui-col-md3">-->
                    <#--<!-- 填充内容 &ndash;&gt;-->
                    <#--<label class="layui-form-label">单位</label>-->
                    <#--<div class="layui-input-inline">-->
                        <#--<input type="text" name="unit" autocomplete="off" class="layui-input">-->
                    <#--</div>-->
                <#--</div>-->
                <#--<div class="layui-col-md3">-->
                    <#--<!-- 填充内容 &ndash;&gt;-->
                    <#--<label class="layui-form-label">部位</label>-->
                    <#--<div class="layui-input-inline">-->
                        <#--<input type="text" name="partName" autocomplete="off" class="layui-input">-->
                    <#--</div>-->
                <#--</div>-->
                <#--<div class="layui-col-md3">-->
                    <#--<!-- 填充内容 &ndash;&gt;-->
                    <#--<label class="layui-form-label">损耗</label>-->
                    <#--<div class="layui-input-inline">-->
                        <#--<input type="text" name="fabricLoss" autocomplete="off" class="layui-input">-->
                    <#--</div>-->
                <#--</div>-->
                <#--<div class="layui-col-md3">-->
                    <#--<!-- 填充内容 &ndash;&gt;-->
                    <#--<label class="layui-form-label">接单用量</label>-->
                    <#--<div class="layui-input-inline">-->
                        <#--<input type="text" name="orderPieceUsage" autocomplete="off" class="layui-input">-->
                    <#--</div>-->
                <#--</div>-->
            <#--</div>-->
            <#--<div class="layui-row layui-col-space10">-->
                <#--<div class="layui-col-md3">-->
                    <#--<!-- 填充内容 &ndash;&gt;-->
                    <#--<label class="layui-form-label">单件用量</label>-->
                    <#--<div class="layui-input-inline">-->
                        <#--<input type="text" name="pieceUsage" autocomplete="off" class="layui-input">-->
                    <#--</div>-->
                <#--</div>-->
                <#--<div class="layui-col-md3">-->
                    <#--<!-- 填充内容 &ndash;&gt;-->
                    <#--<label class="layui-form-label">订单颜色</label>-->
                    <#--<div class="layui-input-inline">-->
                        <#--<input type="text" name="colorName" autocomplete="off" class="layui-input" readonly="true">-->
                    <#--</div>-->
                <#--</div>-->
                <#--<div class="layui-col-md3">-->
                    <#--<!-- 填充内容 &ndash;&gt;-->
                    <#--<label class="layui-form-label">是否撞色</label>-->
                    <#--<div class="layui-input-inline">-->
                        <#--<select type="text" name="isHit" autocomplete="off" class="layui-input">-->
                            <#--<option value="是">是</option>-->
                            <#--<option value="否">否</option>-->
                        <#--</select>-->
                    <#--</div>-->
                <#--</div>-->
                <#--<div class="layui-col-md3">-->
                    <#--<!-- 填充内容 &ndash;&gt;-->
                    <#--<label class="layui-form-label">面料颜色</label>-->
                    <#--<div class="layui-input-inline">-->
                        <#--<input type="text" name="fabricColor" autocomplete="off" class="layui-input">-->
                    <#--</div>-->
                <#--</div>-->
            <#--</div>-->
            <#--<div class="layui-row layui-col-space10">-->
                <#--<div class="layui-col-md3">-->
                    <#--<!-- 填充内容 &ndash;&gt;-->
                    <#--<label class="layui-form-label">面料色号</label>-->
                    <#--<div class="layui-input-inline">-->
                        <#--<input type="text" name="fabricColorNumber" autocomplete="off" class="layui-input">-->
                    <#--</div>-->
                <#--</div>-->
            <#--</div>-->
        <#--</form>-->
    <#--</div>-->
<#--</#macro>-->

<#--<style>-->
    <#--.layui-col-md3 .layui-form-label {-->
        <#--width:90px-->
    <#--}-->
    <#--.layui-col-md6 .layui-form-label {-->
        <#--width:90px-->
    <#--}-->
    <#--.layui-col-md12 .layui-form-label {-->
        <#--width:90px-->
    <#--}-->

    <#--#staffAdd .text {-->
        <#--width: 8%;-->
        <#--text-align: center;-->
    <#--}-->

    <#--#staffAdd .content {-->
        <#--width: 17%;-->
    <#--}-->

    <#--#staffAdd input {-->
        <#--border-radius: 0;-->
        <#--border-color: white;-->
    <#--}-->

    <#--#staffAdd select {-->
        <#--border-color: white;-->
    <#--}-->

    <#--#staffAdd td {-->
        <#--border: solid black;-->
        <#--border-width: 0px 1px 1px 0px;-->
    <#--}-->

    <#--#staffAdd table {-->
        <#--border: solid black;-->
        <#--border-width: 1px 0px 0px 1px;-->
    <#--}-->
<#--</style>-->