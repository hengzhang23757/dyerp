<#macro search>
    <section id="content">
        <section class="vbox">
            <section class="scrollable">
                <div class="layui-row">
                    <form class="layui-form" style="margin: 0 auto;padding-top: 3px;" lay-filter="baseInfo">
                        <table>
                            <tr>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label">开始</label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <input type="text" name="from" id="from" class="layui-input">
                                </td>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label">结束</label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <input type="text" name="to" id="to" class="layui-input">
                                </td>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label">单号</label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <input type="text" name="clothesVersionNumber" id="clothesVersionNumber" placeholder="请输入单号" class="layui-input">
                                </td>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label">款号</label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <input type="text" name="orderName" id="orderName" placeholder="请输入款号" class="layui-input">
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label">类型</label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <select type="text" name="type" id="type">
                                        <option value="分床汇总">分床汇总</option>
                                        <option value="主身布">主身布</option>
                                        <option value="汇总">汇总</option>
                                        <option value="按天主身">按天主身</option>
                                        <option value="按天汇总">按天汇总</option>
                                    </select>
                                </td>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label">组名</label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <select type="text" name="groupName" id="groupName">
                                        <option value="">请选择组名</option>
                                        <option value="A组">A组</option>
                                        <option value="B组">B组</option>
                                        <option value="C组">C组</option>
                                        <option value="D组">D组</option>
                                        <option value="E组">E组</option>
                                        <option value="F组">F组</option>
                                        <option value="G组">G组</option>
                                        <option value="绅维纪">绅维纪</option>
                                        <option value="抄更">抄更</option>
                                    </select>
                                </td>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label"></label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <div class="layui-input-inline">
                                        <button class="layui-btn" lay-submit lay-filter="searchBeat">搜索</button>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>

                <table id="cutMonthTable" lay-filter="cutMonthTable"></table>

                <script type="text/html" id="toolbarTop">
                    <div class="layui-btn-container">
                        <button class="layui-btn layui-btn-sm" lay-event="exportExcel">导出</button>
                        <#--<button class="layui-btn layui-btn-sm" lay-event="init">初始</button>-->
                    </div>
                </script>

                <#--<script type="text/html" id="barTop">-->
                    <#--&lt;#&ndash;<a class="layui-btn layui-btn-xs" lay-event="detail">详情</a>&ndash;&gt;-->
                    <#--<li class="layui-col-xs3 layui-btn layui-btn-xs">-->
                        <#--<a lay-href="/erp/cutQueryLeakStart?a=1">-->
                            <#--<cite>详情</cite>-->
                        <#--</a>-->
                    <#--</li>-->
                <#--</script>-->

                <script type="text/html" id="barTop">
                    <a class="layui-btn layui-btn-xs" lay-event="detail">详情</a>
                </script>

            </section>
        </section>
    </section>

    <link rel="stylesheet" href="/css/layui.css" type="text/css">
    <link rel="stylesheet" href="/css/soulTable.css" type="text/css">
    <link rel="stylesheet" href="/css/selectize.bootstrap3.css" type="text/css">
    <script src="/layuiadmin/layui/layui.js?t=1"></script>
    <script src="/js/common/layer.js"></script>
    <script src="/js/common/xm-select.js?t=${currentDate?c}" type="text/javascript"></script>
    <script src="/js/common/selectize.js" type="text/javascript"></script>
    <script src="/js/common/moment.min.js" type="text/javascript"></script>
    <script src="/js/orderMsg/cutMonthReport.js?t=${currentDate?c}"></script>
</#macro>

<style>
    .layui-col-md3 .layui-form-label {
        width:90px
    }
    .layui-col-md6 .layui-form-label {
        width:90px
    }
    .layui-col-md12 .layui-form-label {
        width:90px
    }

    ::-webkit-scrollbar-track-piece {

        background-color:#f8f8f8;

    }

    ::-webkit-scrollbar {

        width:15px;

        height:15px;

    }

    ::-webkit-scrollbar-thumb {

        background-color:#dddddd;

        background-clip:padding-box;

        min-height:28px;

    }

    ::-webkit-scrollbar-thumb:hover {

        background-color:#bbb;

    }
</style>




<#--<#macro search>-->
    <#--<section id="content">-->
        <#--<section class="vbox">-->
            <#--<section class="scrollable padder water-mark-visible-ff"  style="background-color: #f7f7f7;">-->
                <#--<div class="col-md-12" style="padding-top: 20px;">-->
                    <#--<section class="panel panel-default">-->
                        <#--<div class="panel-body">-->
                            <#--<div class="row">-->
                                <#--<table style="border-collapse:separate;border-spacing: 10px;width: 100%;" id="baseInfo">-->
                                    <#--<tr>-->
                                        <#--<td style="text-align: right;margin-bottom: 15px;">-->
                                            <#--<label class="control-label" style="">开始日期</label>-->
                                        <#--</td>-->
                                        <#--<td style="margin-bottom: 15px;">-->
                                            <#--<input type="text" id="from" class="form-control" autocomplete="off" placeholder="开始日期" style="width:150px;">-->
                                        <#--</td>-->
                                        <#--<td style="text-align: right;margin-bottom: 15px;">-->
                                            <#--<label class="control-label">结束日期</label>-->
                                        <#--</td>-->
                                        <#--<td style="margin-bottom: 15px;">-->
                                            <#--<input type="text" id="to" class="form-control" autocomplete="off" placeholder="结束日期" style="width:150px;">-->
                                        <#--</td>-->
                                        <#--<td style="text-align: right;margin-bottom: 15px;">-->
                                            <#--<label class="control-label" style="">版单号</label>-->
                                        <#--</td>-->
                                        <#--<td style="margin-bottom: 15px;">-->
                                            <#--<input id="clothesVersionNumber" class="form-control" autocomplete="off" style="width: 150px;">-->
                                        <#--</td>-->
                                        <#--<td style="text-align: right;margin-bottom: 15px;">-->
                                            <#--<label class="control-label" style="">订单号</label>-->
                                        <#--</td>-->
                                        <#--<td style="margin-bottom: 15px;">-->
                                            <#--<select id="orderName" class="form-control" style="width: 150px;"></select>-->
                                        <#--</td>-->
                                    <#--</tr>-->
                                    <#--<tr>-->
                                        <#--<td style="text-align: right;margin-bottom: 15px;">-->
                                            <#--<label class="control-label" style="">类型</label>-->
                                        <#--</td>-->
                                        <#--<td style="margin-bottom: 15px;">-->
                                            <#--<select id="type" class="form-control" autocomplete="off" style="width:150px;">-->
                                                <#--<option value="汇总">汇总</option>-->
                                                <#--<option value="主身布">主身布</option>-->
                                            <#--</select>-->
                                        <#--</td>-->
                                        <#--<td style="text-align: right;margin-bottom: 15px;">-->
                                            <#--<label class="control-label" style="">组名</label>-->
                                        <#--</td>-->
                                        <#--<td style="margin-bottom: 15px;">-->
                                            <#--<select id="groupName" class="form-control" autocomplete="off" style="width:150px;">-->
                                                <#--<option value="">请选择组名</option>-->
                                                <#--<option value="A组">A组</option>-->
                                                <#--<option value="B组">B组</option>-->
                                                <#--<option value="C组">C组</option>-->
                                                <#--<option value="D组">D组</option>-->
                                                <#--<option value="E组">E组</option>-->
                                                <#--<option value="F组">F组</option>-->
                                                <#--<option value="G组">G组</option>-->
                                            <#--</select>-->
                                        <#--</td>-->
                                        <#--<td style="text-align: right;margin-bottom: 15px;">-->

                                        <#--</td>-->
                                        <#--<td style="margin-bottom: 15px;">-->
                                            <#--<button  class="btn" style="border-radius: 5px;color:white;font-family: PingFangSC-Semibold, sans-serif;width:50%;"  onclick="search()">查找</button>-->
                                        <#--</td>-->
                                    <#--</tr>-->
                                <#--</table>-->
                            <#--</div>-->

                            <#--<div class="col-md-12" style="padding-top: 10px;padding-bottom:10px" id="exportDiv" hidden>-->
                                <#--<button  class="btn btn-s-lg" style="border-radius: 5px;color:white;"  onclick="exportData()">导出</button>-->
                            <#--</div>-->
                            <#--<div class="col-md-12" style="padding-top: 10px;padding-bottom:10px" id="exportOtherDiv" hidden>-->
                                <#--<button  class="btn btn-s-lg" style="border-radius: 5px;color:white;"  onclick="exportOtherData()">导出</button>-->
                            <#--</div>-->
                            <#--<div class="col-md-12">-->
                                <#--<div id="reportExcel" style="overflow-x: auto;height: auto"></div>-->
                            <#--</div>-->
                            <#--<div class="col-md-12">-->
                                <#--<div id="reportOtherExcel" style="overflow-x: auto;height: auto"></div>-->
                            <#--</div>-->
                        <#--</div>-->
                    <#--</section>-->
                <#--</div>-->
            <#--</section>-->
        <#--</section>-->
        <#--<a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen"-->
           <#--data-target="#nav"></a>-->
    <#--</section>-->
    <#--<aside class="bg-light lter b-l aside-md hide" id="notes">-->
        <#--<div class="wrapper">Notification</div>-->
    <#--</aside>-->
    <#--</section>-->
    <#--<script src="/js/common/laydate.js" type="text/javascript" ></script>-->
    <#--<script src="/js/common/selectize.js" type="text/javascript"></script>-->
    <#--<link rel="stylesheet" href="/css/handsontable.full.min.css" type="text/css">-->
    <#--<script src="/js/common/handsontable.full.min.js" type="text/javascript" ></script>-->
    <#--<script src="/js/common/export2Excel2.js" type="text/javascript" ></script>-->
    <#--<script src="/js/common/jquery.blockUI.js" type="text/javascript" ></script>-->
    <#--<script src="/js/orderMsg/cutMonthReport.js?t=${currentDate?c}"></script>-->

<#--</#macro>-->

<#--<style>-->
    <#--button {-->
        <#--background:rgb(45, 202, 147);-->
        <#--opacity:0.86;-->
        <#--color: white;-->
        <#--font-family: PingFangSC-Semibold, sans-serif;-->
    <#--}-->
<#--</style>-->
