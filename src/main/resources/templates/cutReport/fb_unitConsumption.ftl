<#macro search>
    <section id="content">
        <section class="vbox">
            <section class="scrollable padder water-mark-visible-ff"  style="background-color: #f7f7f7;">
                <div class="col-md-12" style="padding-top: 5px;">
                    <section class="panel panel-default">
                        <div class="panel-body">
                            <table style="border-collapse:separate;border-spacing: 10px;width: 100%;" id="baseInfo">
                                <tr>
                                    <td style="text-align: right;margin-bottom: 15px;">
                                        <label class="control-label" style="">单号</label>
                                    </td>
                                    <td style="margin-bottom: 15px;">
                                        <input type="text" id="clothesVersionNumber" class="form-control" autocomplete="off" placeholder="请输入单号" style="width:200px">
                                    </td>
                                    <td style="text-align: right;margin-bottom: 15px;">
                                        <label class="control-label">款号</label>
                                    </td>
                                    <td style="margin-bottom: 15px;">
                                        <input id="orderName" class="form-control" placeholder="请输入单号" style="width: 200px">
                                    </td>
                                    <td style="text-align: right;margin-bottom: 15px;">
                                        <label class="control-label" style="">颜色</label>
                                    </td>
                                    <td style="margin-bottom: 15px;">
                                        <select id="colorName" class="form-control" autocomplete="off" style="width: 200px">
                                    </td>
                                    <td style="text-align: right;margin-bottom: 15px;">
                                        <label class="control-label" style="">部位</label>
                                    </td>
                                    <td style="margin-bottom: 15px;">
                                        <select id="partName" class="form-control" autocomplete="off" style="width: 200px">
                                    </td>
                                    <td style="margin-bottom: 15px;">
                                        <button  class="btn" style="border-radius: 5px;color:white;font-family: PingFangSC-Semibold, sans-serif;width:100px"  onclick="search()">查找</button>
                                    </td>
                                </tr>
                            </table>

                            <div class="col-md-12" style="padding-top: 20px;padding-bottom:10px" id="exportDiv" hidden>
                                <button  class="btn btn-s-lg" style="border-radius: 5px;color:white;"  onclick="exportData()">导出</button>
                            </div>
                            <div class="col-md-12">
                                <div id="reportExcel" style="overflow-x: auto;height: auto"></div>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="row">
                    <div id="entities">
                    </div>
                </div>
                <iframe id="printf" src="" width="0" height="0" frameborder="0"></iframe>
            </section>
        </section>
        <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen"
           data-target="#nav"></a>
    </section>
    <aside class="bg-light lter b-l aside-md hide" id="notes">
        <div class="wrapper">Notification</div>
    </aside>
    <link rel="stylesheet" href="/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="/css/b.tabs.css" type="text/css">
    <link rel="stylesheet" href="/css/handsontable.full.min.css" type="text/css">
    <script src="/js/common/handsontable.full.min.js" type="text/javascript" ></script>
    <script src="/js/common/export2Excel2.js" type="text/javascript" ></script>
    <script src="/js/common/zh-CN.js" type="text/javascript" ></script>
    <script src="/js/common/laydate.js" type="text/javascript" ></script>
    <script src="/js/common/jquery.blockUI.js" type="text/javascript" ></script>
    <script src="/js/common/b.tabs.js?t=${currentDate?c}"></script>
    <script src="/js/common/moment.min.js" type="text/javascript"></script>
    <script src="/js/cutReport/unitConsumption.js?t=${currentDate?c}"></script>
</#macro>

<style>
    button {
        background:rgb(45, 202, 147);
        opacity:0.86;
        color: white;
        font-family: PingFangSC-Semibold, sans-serif;
    }
    ::-webkit-scrollbar-track-piece {

        background-color:#f8f8f8;

    }

    ::-webkit-scrollbar {

        width:15px;

        height:15px;

    }

    ::-webkit-scrollbar-thumb {

        background-color:#dddddd;

        background-clip:padding-box;

        min-height:28px;

    }

    ::-webkit-scrollbar-thumb:hover {

        background-color:#bbb;

    }
</style>
