<#macro search>
<section id="content">
    <section class="vbox">
        <section class="scrollable">
            <div class="layui-row">
                <form class="layui-form" style="margin: 0 auto;padding-top: 3px;" lay-filter="baseInfo">
                    <table>
                        <tr>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label" style="width: 100px">开始</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" id="from" placeholder="开始日期" autocomplete="off" class="layui-input">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label" style="width: 100px">结束</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" id="to" autocomplete="off" placeholder="结束日期" class="layui-input">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label" style="width: 100px">品牌</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <select id="customerName" autocomplete="off"></select>
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label" style="width: 100px">季度</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <select id="seasonName" autocomplete="off"></select>
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label" style="width: 100px">状态</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <select id="checkState" autocomplete="off">
                                    <option value="">全部</option>
                                    <option value="0">未入账</option>
                                    <option value="1">已入账</option>
                                </select>
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                            </td>
                            <td style="margin-bottom: 15px;">
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label" style="width: 100px">单号</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" id="clothesVersionNumber" placeholder="输入单号" autocomplete="off" class="layui-input">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label" style="width: 100px">款号</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" id="orderName" autocomplete="off" placeholder="输入款号" class="layui-input">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label" style="width: 100px">类型</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <select id="type" autocomplete="off" lay-filter="demo">
                                    <option value="面料">面料</option>
                                    <option value="辅料">辅料</option>
                                    <option value="共用辅料">共用辅料</option>
                                </select>
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label" style="width: 100px">供应商</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <div id="supplier" class="xm-select-demo"></div>
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label" style="width: 100px">类别</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <select id="showType" autocomplete="off">
                                    <option value="流水">流水</option>
                                    <option value="汇总">汇总</option>
                                </select>
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label"></label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <div class="layui-input-inline">
                                    <button class="layui-btn" lay-submit lay-filter="searchBeat">搜索</button>
                                </div>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>


            <table id="reportTable" lay-filter="reportTable"></table>

            <script type="text/html" id="toolbarTop">
                <div class="layui-btn-container">
                    <button class="layui-btn layui-btn-sm" lay-event="exportExcel">导出</button>
                    <button class="layui-btn layui-btn-sm" lay-event="clearFilter">清除所有筛选</button>
                    <button class="layui-btn layui-btn-sm" lay-event="check">入账</button>
                    <button class="layui-btn layui-btn-sm" lay-event="unCheck">取消入账</button>
                </div>
            </script>

            <script type="text/html" id="barTop">
                <a class="layui-btn layui-btn-xs" lay-event="detail">详情</a>
                <a class="layui-btn layui-btn-xs layui-btn-warm" lay-event="check">入账</a>
            </script>

            <script type="text/html" id="barTop2">
                <a class="layui-btn layui-btn-xs" lay-event="detail2">详情</a>
                <a class="layui-btn layui-btn-xs layui-btn-warm" lay-event="check2">入账</a>
            </script>

            <script type="text/html" id="barTopReview">
                <a class="layui-btn layui-btn-xs layui-btn-normal" lay-event="reviewCommit">审核</a>
                <a class="layui-btn layui-btn-xs layui-btn-warm" lay-event="checkDetailUpdate">修改</a>
                <a class="layui-btn layui-btn-xs layui-btn-danger" lay-event="destroyCheck">废除合同</a>
                <a class="layui-btn layui-btn-xs" lay-event="exportCheck">打印/导出</a>
                <a class="layui-btn layui-btn-xs layui-btn-warm" lay-event="changeOrderState">下单</a>
                <a class="layui-btn layui-btn-xs" lay-event="progress">进度</a>
            </script>

            <script type="text/html" id="fabricChildBar">
                <a class="layui-btn layui-btn-xs layui-btn-normal" onclick="updateFabricDetailData(this)">修改</a>
            </script>

            <script type="text/html" id="accessoryChildBar">
                <a class="layui-btn layui-btn-xs layui-btn-normal" onclick="updateAccessoryDetailData(this)">修改</a>
            </script>


            <div id="operateDiv" hidden>
                <form class="layui-form" style="margin: 0 auto;padding-top: 3px;" lay-filter="formBaseInfo">
                    <table>
                        <tr>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label" style="width: 100px">月份</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" id="checkMonth" name="checkMonth" autocomplete="off" class="layui-input">
                            </td>
                        </tr>
                    </table>
                </form>
                <table id="checkTable" lay-filter="checkTable"></table>
            </div>

            <div id="detailDiv" hidden>
                <table id="detailTable" lay-filter="detailTable"></table>
            </div>

        </section>
    </section>
    <#include "fb_checkAdd.ftl">
    <@checkAdd></@checkAdd>
</section>

    <link rel="stylesheet" href="/css/layui.css" type="text/css">
    <link rel="stylesheet" href="/css/step.css" type="text/css">
    <link rel="stylesheet" href="/css/soulTable.css" type="text/css">
    <link rel="stylesheet" href="/css/publish.css" type="text/css">
    <link rel="stylesheet" href="/css/cropper.css" type="text/css">
    <link rel="stylesheet" href="/css/selectize.bootstrap3.css" type="text/css">
    <link rel="stylesheet" href="/css/zoom.css" type="text/css">


    <script src="/js/common/layer.js"></script>
    <script src="/js/step/step.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/common/move.js"></script>
    <script type="text/javascript" src="/js/common/publishImg.js"></script>
    <script src="/js/common/xm-select.js?t=${currentDate?c}" type="text/javascript"></script>
    <script src="/js/common/selectize.js" type="text/javascript"></script>
    <script src="/js/common/zoom.js" type="text/javascript"></script>
    <script src="/js/common/moment.min.js" type="text/javascript"></script>
    <script src="/js/checkModule/fabricAccessoryMonthCheck.js?t=${currentDate?c}"></script>
</#macro>

<style>
    button {
        background:rgb(45, 202, 147);
        opacity:0.86;
        color: white;
        font-family: PingFangSC-Semibold, sans-serif;
    }
    ::-webkit-scrollbar-track-piece {

        background-color:#f8f8f8;

    }

    ::-webkit-scrollbar {

        width:15px;

        height:15px;

    }

    ::-webkit-scrollbar-thumb {

        background-color:#dddddd;

        background-clip:padding-box;

        min-height:28px;

    }

    ::-webkit-scrollbar-thumb:hover {

        background-color:#bbb;

    }
</style>
