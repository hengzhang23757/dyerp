<#macro search>
    <section id="content">
        <section class="vbox">
            <section class="scrollable padder water-mark-visible-ff"  style="background-color: #f7f7f7;">
                <div class="col-md-12" style="padding-top: 20px;">
                    <section class="panel panel-default">
                        <div class="panel-body">
                            <table style="border-collapse:separate;border-spacing: 10px;width: 100%;" id="baseInfo">
                                <tr>
                                    <td style="text-align: right;margin-bottom: 15px;">
                                        <label class="control-label" style="">单号</label>
                                    </td>
                                    <td style="margin-bottom: 15px;">
                                        <input id="clothesVersionNumber" class="form-control" placeholder="请输入单号" autocomplete="off" style="width: 200px;">
                                    </td>
                                    <td style="text-align: right;margin-bottom: 15px;">
                                        <label class="control-label" style="">款号</label>
                                    </td>
                                    <td style="margin-bottom: 15px;">
                                        <input id="orderName" class="form-control" placeholder="请输入款号" autocomplete="off" style="width: 200px;">
                                    </td>
                                    <td style="text-align: right;margin-bottom: 15px;">
                                        <label class="control-label" style="">组名</label>
                                    </td>
                                    <td style="margin-bottom: 15px;">
                                        <select id="groupName" class="form-control" autocomplete="off" style="width: 200px;"></select>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: right;margin-bottom: 15px;">
                                        <label class="control-label" style="">开始时间</label>
                                    </td>
                                    <td style="margin-bottom: 15px;">
                                        <input id="from" class="form-control" placeholder="开始时间" autocomplete="off" style="width: 200px;">
                                    </td>
                                    <td style="text-align: right;margin-bottom: 15px;">
                                        <label class="control-label" style="">结束时间</label>
                                    </td>
                                    <td style="margin-bottom: 15px;">
                                        <input id="to" class="form-control" placeholder="结束时间" autocomplete="off" style="width: 200px;">
                                    </td>
                                    <td style="margin-bottom: 15px;">
                                    </td>
                                    <td style="margin-bottom: 15px;">
                                        <button  class="btn" style="width:100px; border-radius: 5px;color:white;font-family: PingFangSC-Semibold, sans-serif;"  onclick="search()">查找</button>
                                    </td>
                                </tr>
                            </table>

                            <div class="col-md-12" style="padding-top: 20px;padding-bottom:10px" id="exportDiv" hidden>
                                <button  class="btn btn-s-lg" style="border-radius: 5px;color:white;"  onclick="exportData()">导出</button>
                            </div>
                            <div class="col-md-12" style="padding-top: 20px;padding-bottom:10px;margin-left: 10px">
                                <div id="reportExcel" style="overflow-x: auto;height: auto"></div>
                            </div>
                        </div>
                    </section>
                </div>
            </section>
        </section>
        <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen"
           data-target="#nav"></a>
    </section>
    <aside class="bg-light lter b-l aside-md hide" id="notes">
        <div class="wrapper">Notification</div>
    </aside>
    </section>

    <link rel="stylesheet" href="/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="/css/b.tabs.css" type="text/css">
    <link rel="stylesheet" href="/css/handsontable.full.min.css" type="text/css">
    <script src="/js/common/handsontable.full.min.js" type="text/javascript" ></script>
    <script src="/js/common/export2Excel2.js?t=${currentDate?c}" type="text/javascript" ></script>
    <script src="/js/common/zh-CN.js" type="text/javascript" ></script>
    <script src="/js/common/laydate.js" type="text/javascript" ></script>
    <script src="/js/common/jquery.blockUI.js" type="text/javascript" ></script>
    <script src="/js/common/b.tabs.js?t=${currentDate?c}"></script>
    <link rel="stylesheet" href="/css/selectize.bootstrap3.css" type="text/css">
    <script src="/js/common/selectize.js" type="text/javascript"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css" type="text/css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
    <script src="/js/common/export2Excel2.js" type="text/javascript" ></script>
    <script src="/js/embMarket/embOutPackageDetail.js?t=${currentDate?c}"></script>

</#macro>

<style>
    button {
        background:rgb(45, 202, 147);
        opacity:0.86;
        color: white;
        font-family: PingFangSC-Semibold, sans-serif;
    }
    ::-webkit-scrollbar-track-piece {

        background-color:#f8f8f8;

    }

    ::-webkit-scrollbar {

        width:15px;

        height:15px;

    }

    ::-webkit-scrollbar-thumb {

        background-color:#dddddd;

        background-clip:padding-box;

        min-height:28px;

    }

    ::-webkit-scrollbar-thumb:hover {

        background-color:#bbb;

    }
</style>
