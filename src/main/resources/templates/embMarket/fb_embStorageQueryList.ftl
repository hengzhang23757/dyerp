<section class="panel panel-default">
    <div id="tableDiv">
        <table class="table table-striped table-bordered " id="embStorageTable">
            <#if embStorageQueryList??>
                <thead>
                    <tr bgcolor="#ffcb99" style="color: black;">
                        <th style="width: 10%;text-align:left;font-size:14px">序号</th>
                        <th style="width: 15%;text-align:left;font-size:14px">款号</th>
                        <th style="width: 15%;text-align:left;font-size:14px">颜色</th>
                        <th style="width: 15%;text-align:left;font-size:14px">尺码</th>
                        <th style="width: 15%;text-align:left;font-size:14px">数量</th>
                        <th style="width: 15%;text-align:left;font-size:14px">位置</th>
                        <th style="width: 15%;text-align:left;font-size:14px">操作</th>
                    </tr>
                </thead>
                <tbody>
                    <#list embStorageQueryList as opa>
                        <tr>
                            <td>${opa_index+1}</td>
                            <td>${opa.orderName!}</td>
                            <td>${opa.colorName!}</td>
                            <td>${opa.sizeName!}</td>
                            <td>${opa.embStorageCount!}</td>
                            <td>${opa.embStoreLocation!}</td>
                            <td><a href="#" style="color:red;" onclick="deleteStorage('${opa.orderName?string}','${opa.colorName?string}','${opa.sizeName?string}','${opa.embStoreLocation?string}',this)">删除</a></td>
                        </tr>
                    </#list>
                </tbody>
                <tfoot>
                    <tr>
                        <th colspan="7" style="text-align:right"></th>
                    </tr>
                </tfoot>
            <#else>
                <h4 style="text-align: center">暂无数据</h4>
            </#if>
        </table>
    </div>

</section>

<style>
    td {
        word-break:break-all;
    }

</style>