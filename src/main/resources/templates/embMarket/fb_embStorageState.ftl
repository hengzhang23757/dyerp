<#macro search>
<section id="content">
    <section class="vbox">
        <section class="scrollable padder water-mark-visible-ff" id="storageSection">
            <div class="col-md-12" style="padding-top: 20px;padding-bottom: 20px">
                <span style="font-size: 20px;font-family: PingFangSC-Semibold">仓库实时概览</span>
                <label style="margin-left: 20px;border-radius: 6px;background: rgb(206,39,60)">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                <span style="font-size: 16px;font-family: PingFangSC-Semibold">满仓</span>
                <label style="margin-left: 20px;border-radius: 6px;background: rgb(217,202,23)">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                <span style="font-size: 16px;font-family: PingFangSC-Semibold">半仓</span>
                <label style="margin-left: 20px;border-radius: 6px;background: rgb(45,202,147)">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                <span style="font-size: 16px;font-family: PingFangSC-Semibold">空仓</span>
            </div>
        </section>
    </section>
</section>
</section>

<script src="/js/embMarket/embStorageState.js?t=${currentDate?c}"></script>


</#macro>

<style>
    button {
        background:rgb(45, 202, 147);
        opacity:0.86;
        color: white;
        font-family: PingFangSC-Semibold, sans-serif;
    }
</style>
