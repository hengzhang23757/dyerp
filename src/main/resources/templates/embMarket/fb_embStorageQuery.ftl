<#macro search>
<section id="content">
    <section class="vbox">
        <section class="scrollable padder water-mark-visible-ff"  style="background-color: #f7f7f7;">
            <div class="col-md-12" style="padding-top: 20px;">
                <section class="panel panel-default">
                    <div class="panel-body">
                        <table style="border-collapse:separate;border-spacing: 10px;width: 100%;" id="baseInfo">
                            <tr>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="control-label" style="">单号</label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <input id="clothesVersionNumber" class="form-control" placeholder="请输入单号" autocomplete="off" style="width: 200px;">
                                </td>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="control-label" style="">款号</label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <input id="orderName" class="form-control" placeholder="请输入款号" autocomplete="off" style="width: 200px;">
                                </td>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="control-label" style="">颜色</label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <select id="colorName" class="form-control" style="width: 200px;"></select>
                                </td>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="control-label" style="">尺码</label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <select id="sizeName" class="form-control" style="width: 200px;"></select>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <button  class="btn" style="width:100px; border-radius: 5px;color:white;font-family: PingFangSC-Semibold, sans-serif;"  onclick="search()">查找</button>
                                </td>
                            </tr>
                        </table>
                    </div>
                </section>
            </div>
            <div class="row">
                <div id="entities" class="col-sm-12"></div>
            </div>
        </section>
    </section>
    <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen"
       data-target="#nav"></a>
</section>
<aside class="bg-light lter b-l aside-md hide" id="notes">
    <div class="wrapper">Notification</div>
</aside>
    <link rel="stylesheet" href="/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="/css/b.tabs.css" type="text/css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.bootstrap.min.css">
    <script src="/js/common/moment.min.js" type="text/javascript"></script>
    <script src="/js/common/laydate.js" type="text/javascript" ></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.bootstrap.min.js"></script>

    <script src="/js/common/jquery.blockUI.js" type="text/javascript" ></script>
    <script src="/js/common/b.tabs.js?t=${currentDate?c}"></script>
    <script src="/js/embMarket/embStorageQuery.js?t=${currentDate?c}"></script>

</#macro>

<style>
    button {
        background:rgb(45, 202, 147);
        opacity:0.86;
        color: white;
        font-family: PingFangSC-Semibold, sans-serif;
    }
</style>
