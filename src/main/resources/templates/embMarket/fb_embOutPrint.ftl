<#macro search>
    <section id="content">
        <section class="vbox">
            <section class="scrollable" style="background-color: #f7f7f7;">
                <div class="col-md-12" style="padding-top: 10px;">
                    <section class="panel panel-default">
                        <div class="panel-body" style="margin-bottom: 10px">
                            <div class="row">
                                <table style="border-collapse:separate;border-spacing: 10px;width: 100%;" id="baseInfo">
                                    <tr>
                                        <td style="text-align: right;margin-bottom: 15px;">
                                            <label class="control-label" style="">单号</label>
                                        </td>
                                        <td style="margin-bottom: 15px;">
                                            <input id="clothesVersionNumber" class="form-control" placeholder="请输入单号" autocomplete="off" style="width: 200px;">
                                        </td>
                                        <td style="text-align: right;margin-bottom: 15px;">
                                            <label class="control-label" style="">款号</label>
                                        </td>
                                        <td style="margin-bottom: 15px;">
                                            <input id="orderName" class="form-control" placeholder="请输入款号" style="width: 200px;">
                                        </td>
                                        <td style="text-align: right;margin-bottom: 15px;">
                                            <label class="control-label" style="">组名</label>
                                        </td>
                                        <td style="margin-bottom: 15px;">
                                            <select id="groupName" class="form-control" autocomplete="off" style="width: 200px;"></select>
                                        </td>
                                        <td style="text-align: right;margin-bottom: 15px;">
                                            <label class="control-label" style="">开始</label>
                                        </td>
                                        <td style="margin-bottom: 15px;">
                                            <input id="from" class="form-control" autocomplete="off" style="width: 200px;">
                                        </td>
                                        <td style="text-align: right;margin-bottom: 15px;">
                                            <label class="control-label" style="">结束</label>
                                        </td>
                                        <td style="margin-bottom: 15px;">
                                            <input id="to" class="form-control" autocomplete="off" style="width: 200px;">
                                        </td>
                                        <td style="margin-bottom: 15px;">
                                            <button  class="btn" style="border-radius: 5px;color:white;font-family: PingFangSC-Semibold, sans-serif;width:70px;"  onclick="search()">查找</button>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-12" style="padding-top: 20px;padding-bottom:10px" id="exportDiv" hidden>
                                <button  class="btn btn-s-lg" style="border-radius: 5px;color:white;"  onclick="exportData()">导出</button>
                                <button  class="btn btn-s-lg" style="border-radius: 5px;color:white;"  onclick="printDeal()">打印</button>
                            </div>
                            <div id="reportExcel" hidden style="overflow-x: auto;height: auto"></div>
                        </div>
                    </section>
                </div>
                <div class="row">
                    <div id="entities">
                    </div>
                </div>
                <iframe id="printf" src="" width="0" height="0" frameborder="0"></iframe>
            </section>
        </section>
        <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen"
           data-target="#nav"></a>
    </section>
    <link rel="stylesheet" href="/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="/css/b.tabs.css" type="text/css">
    <script src="/js/embMarket/embOutPrint.js?t=${currentDate?c}"></script>

</#macro>

<style>
    button {
        background:rgb(45, 202, 147);
        opacity:0.86;
        color: white;
        font-family: PingFangSC-Semibold, sans-serif;
    }

    #orderNameTips{
        margin-left: 60px
    };

    ::-webkit-scrollbar-track-piece {

        background-color:#f8f8f8;

    }

    ::-webkit-scrollbar {

        width:15px;

        height:15px;

    }

    ::-webkit-scrollbar-thumb {

        background-color:#dddddd;

        background-clip:padding-box;

        min-height:28px;

    }

    ::-webkit-scrollbar-thumb:hover {

        background-color:#bbb;

    }
</style>
