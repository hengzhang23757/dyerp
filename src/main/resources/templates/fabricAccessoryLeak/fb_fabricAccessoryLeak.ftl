<#macro search>
<section id="content">
    <section class="vbox">
        <section class="scrollable">

            <div class="layui-row">
                <form class="layui-form" style="margin: 0 auto;padding-top: 3px;" lay-filter="baseInfo">
                    <table>
                        <tr>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label">单号</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" id="clothesVersionNumber" autocomplete="off" class="layui-input">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label">款号</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" id="orderName" autocomplete="off" class="layui-input">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label">色组</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <select type="text" id="colorName" autocomplete="off" class="layui-input"></select>
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label">类别</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <select type="text" id="type" autocomplete="off" class="layui-input">
                                    <option value="面料">面料</option>
                                    <option value="辅料">辅料</option>
                                </select>
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label"></label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <div class="layui-input-inline">
                                    <button class="layui-btn" lay-submit lay-filter="searchBeat">搜索</button>
                                </div>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>

            <div class="layui-row" id="fabricAccessoryTab">
                <div class="layui-tab" lay-filter="docDemoTabBrief">
                    <ul class="layui-tab-title">
                        <li class="layui-this" lay-id="1">汇总</li>
                        <li lay-id="2">入库</li>
                        <li lay-id="3">库存</li>
                        <li lay-id="4">暂存</li>
                        <li lay-id="5">出库</li>
                    </ul>
                    <div class="layui-tab-content">
                        <div class="layui-tab-item layui-show">
                            <table id="summaryTable" lay-filter="summaryTable"></table>
                        </div>
                        <div class="layui-tab-item">
                            <table id="inStoreTable" lay-filter="inStoreTable"></table>
                        </div>
                        <div class="layui-tab-item">
                            <table id="storageTable" lay-filter="storageTable"></table>
                        </div>
                        <div class="layui-tab-item">
                            <table id="tmpStorageTable" lay-filter="tmpStorageTable"></table>
                        </div>
                        <div class="layui-tab-item">
                            <table id="outStoreTable" lay-filter="outStoreTable"></table>
                        </div>
                    </div>
                </div>
            </div>

            <script type="text/html" id="toolbarTop">
                <div class="layui-btn-container">
                    <button class="layui-btn layui-btn-sm" lay-event="clearFilter">清除所有筛选</button>
                    <button class="layui-btn layui-btn-sm" lay-event="exportExcel">导出</button>
                </div>
            </script>

        </section>
    </section>
</section>

    <link rel="stylesheet" href="/css/layui.css" type="text/css">
    <link rel="stylesheet" href="/css/step.css" type="text/css">
    <link rel="stylesheet" href="/css/soulTable.css" type="text/css">
    <link rel="stylesheet" href="/css/publish.css" type="text/css">
    <link rel="stylesheet" href="/css/cropper.css" type="text/css">
    <link rel="stylesheet" href="/css/selectize.bootstrap3.css" type="text/css">
    <link rel="stylesheet" href="/css/zoom.css" type="text/css">

    <script src="/js/common/layer.js"></script>
    <script src="/js/common/laydate.js" type="text/javascript" ></script>
    <script src="/js/step/step.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/common/move.js"></script>
    <script type="text/javascript" src="/js/common/publishImg.js"></script>
    <script src="/js/common/xm-select.js?t=${currentDate?c}" type="text/javascript"></script>
    <script src="/js/common/selectize.js" type="text/javascript"></script>
    <script src="/js/common/zoom.js" type="text/javascript"></script>
    <script src="/js/fabricAccessoryLeak/fabricAccessoryLeak.js?t=${currentDate?c}"></script>
    <script src="/js/common/moment.min.js" type="text/javascript"></script>
</#macro>

<style>

    ::-webkit-scrollbar-track-piece {

        background-color:#f8f8f8;

    }

    ::-webkit-scrollbar {

        width:15px;

        height:15px;

    }

    ::-webkit-scrollbar-thumb {

        background-color:#dddddd;

        background-clip:padding-box;

        min-height:28px;

    }

    ::-webkit-scrollbar-thumb:hover {

        background-color:#bbb;

    }
</style>
