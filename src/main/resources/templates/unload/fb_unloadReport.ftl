<#macro search>
    <section id="content">
        <section class="vbox">
            <section class="scrollable padder water-mark-visible-ff"  style="background-color: #f7f7f7;">

                <div class="col-md-12" style="padding-top: 10px;">
                    <section class="panel panel-default">
                        <div class="panel-body">
                            <div class="row" style="margin-bottom: 10px; margin-left: 10px">
                                <div class="col-md-3" style="padding:0;">
                                    <span class="font-bold">版单号：</span>
                                    <input type="text" id="clothesVersionNumber" class="form-control" autocomplete="off" placeholder="请输入版单号" style="width:60%;">
                                </div>
                                <div class="col-md-3" style="padding:0;">
                                    <span class="font-bold">订单号：</span>
                                    <select id="orderName" class="form-control" autocomplete="off" style="width:60%;"></select>
                                </div>
                                <div class="col-md-2" style="padding:0;">
                                    <span class="font-bold">从：</span>
                                    <input type="text" id="from" class="form-control" autocomplete="off" placeholder="开始日期" style="width:70%;">
                                </div>
                                <div class="col-md-2" style="padding:0;">
                                    <span class="font-bold">到：</span>
                                    <input type="text" id="to" class="form-control" autocomplete="off" placeholder="结束日期" style="width:70%;">
                                </div>
                                <div class="col-md-1" style="padding:0">
                                    <button  class="btn" style="border-radius: 5px;color:white;font-family: PingFangSC-Semibold, sans-serif;width:50%;"  onclick="search()">查询</button>
                                </div>

                            </div>

                            <div class="col-md-12" style="padding-top: 20px;padding-bottom:10px" id="exportDiv" hidden>
                                <button  class="btn btn-s-lg" style="border-radius: 5px;color:white;"  onclick="exportData()">导出</button>
                            </div>
                            <div class="col-md-12" style="overflow-x: auto;height: 500px">
                                <div id="reportExcel"></div>
                            </div>
                        </div>
                    </section>
                </div>
            </section>
        </section>
        <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen"
           data-target="#nav"></a>
    </section>
    <aside class="bg-light lter b-l aside-md hide" id="notes">
        <div class="wrapper">Notification</div>
    </aside>
    </section>
    <link rel="stylesheet" href="/css/b.tabs.css" type="text/css">
    <link rel="stylesheet" href="/css/handsontable.full.min.css" type="text/css">
    <script src="/js/common/handsontable.full.min.js" type="text/javascript" ></script>
    <script src="/js/common/zh-CN.js" type="text/javascript" ></script>
    <script src="/js/common/laydate.js" type="text/javascript" ></script>
    <script src="/js/common/jquery.blockUI.js" type="text/javascript" ></script>
    <script src="/js/common/b.tabs.js?t=${currentDate?c}"></script>
    <link rel="stylesheet" href="/css/selectize.bootstrap3.css" type="text/css">
    <script src="/js/common/selectize.js" type="text/javascript"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css" type="text/css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
    <script src="/js/common/export2Excel2.js" type="text/javascript" ></script>
    <script src="/js/unload/unloadReport.js?t=${currentDate?c}"></script>

</#macro>

<style>
    button {
        background:rgb(45, 202, 147);
        opacity:0.86;
        color: white;
        font-family: PingFangSC-Semibold, sans-serif;
    }
</style>
