<#macro search>
    <section id="content">
        <section class="vbox">
            <section class="scrollable padder water-mark-visible-ff"  style="background-color: #f7f7f7;">
                <div class="col-md-12" style="padding-top: 5px;">
                    <section class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <table style="border-collapse:separate;border-spacing: 10px;width: 100%;" id="baseInfo">
                                    <tr>
                                        <td style="text-align: right;margin-bottom: 15px;">
                                            <label class="control-label" style="">从</label>
                                        </td>
                                        <td style="margin-bottom: 15px;">
                                            <input type="text" id="from" class="form-control" autocomplete="off" placeholder="开始日期" style="width:150px">
                                        </td>
                                        <td style="text-align: right;margin-bottom: 15px;">
                                            <label class="control-label">到</label>
                                        </td>
                                        <td style="margin-bottom: 15px;">
                                            <input type="text" id="to" class="form-control" autocomplete="off" placeholder="结束日期" style="width:150px">
                                        </td>
                                        <td style="text-align: right;margin-bottom: 15px;">
                                            <label class="control-label" style="">单号</label>
                                        </td>
                                        <td style="margin-bottom: 15px;">
                                            <input id="clothesVersionNumber" class="form-control" autocomplete="off" placeholder="请输入单号" style="width: 150px">
                                        </td>
                                        <td style="text-align: right;margin-bottom: 15px;">
                                            <label class="control-label" style="">款号</label>
                                        </td>
                                        <td style="margin-bottom: 15px;">
                                            <input id="orderName" class="form-control" autocomplete="off" placeholder="请输入款号" style="width: 150px">
                                        </td>
                                        <td style="text-align: right;margin-bottom: 15px;">
                                            <label class="control-label" style="">组名</label>
                                        </td>
                                        <td style="margin-bottom: 15px;">
                                        <td style="margin-bottom: 15px;">
                                            <div id="groupName" class="xm-select-demo" style="width:200px"></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right;margin-bottom: 15px;">
                                            <label class="control-label" style="">工序从</label>
                                        </td>
                                        <td style="margin-bottom: 15px;">
                                            <input id="procedureFrom" class="form-control" autocomplete="off" style="width:150px" onkeyup="value=value.replace(/^(0+)|[^\d]+/g,'')">
                                        </td>
                                        <td style="text-align: right;margin-bottom: 15px;">
                                            <label class="control-label" style="">工序到</label>
                                        </td>
                                        <td style="margin-bottom: 15px;">
                                            <input id="procedureTo" class="form-control" autocomplete="off" style="width:150px" onkeyup="value=value.replace(/^(0+)|[^\d]+/g,'')">
                                        </td>
                                        <td style="text-align: right;margin-bottom: 15px;">
                                            <label class="control-label" style="">工号</label>
                                        </td>
                                        <td style="margin-bottom: 15px;">
                                            <input id="employeeNumber" class="form-control" autocomplete="off" style="width:150px" onkeyup="value=value.replace(/^(0+)|[^\d]+/g,'')">
                                        </td>
                                        <td style="text-align: right;margin-bottom: 15px;">
                                        </td>
                                        <td style="margin-bottom: 15px;">
                                            <button  class="btn" style="border-radius: 5px;color:white;font-family: PingFangSC-Semibold, sans-serif;width:50%;"  onclick="search()">查找</button>
                                        </td>
                                    </tr>
                                </table>
                            </div>

                            <div class="col-md-12" style="padding-top: 20px;padding-bottom:10px" id="exportDiv" hidden>
                                <button  class="btn btn-s-lg" style="border-radius: 5px;color:white;" id="exportButton">导出</button>
                                <button  class="btn btn-s-lg" style="border-radius: 5px;color:white;"  onclick="printDeal()">打印</button>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="row">
                    <div id="entities">
                    </div>
                </div>
                <iframe id="printf" src="" width="0" height="0" frameborder="0"></iframe>
            </section>
        </section>
        <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen"
           data-target="#nav"></a>
    </section>
    <aside class="bg-light lter b-l aside-md hide" id="notes">
        <div class="wrapper">Notification</div>
    </aside>
    </section>

    <link rel="stylesheet" href="/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="/css/b.tabs.css" type="text/css">
    <link rel="stylesheet" href="/css/bootstrap-select.min.css" type="text/css">
    <script src="/js/common/laydate.js" type="text/javascript" ></script>
    <script src="/js/common/jquery.blockUI.js" type="text/javascript" ></script>
    <script src="/js/common/b.tabs.js?t=${currentDate?c}"></script>
    <script src="/js/common/moment.min.js" type="text/javascript"></script>
    <script src="/js/common/table2excel.js" type="text/javascript"></script>
    <script src="/js/common/bootstrap-select.min.js" type="text/javascript"></script>
    <script src="/js/common/xm-select.js" type="text/javascript"></script>
    <script src="/js/report/procedureProgress.js?t=${currentDate?c}"></script>

</#macro>

<style>
    button {
        background:rgb(45, 202, 147);
        opacity:0.86;
        color: white;
        font-family: PingFangSC-Semibold, sans-serif;
    }
</style>
