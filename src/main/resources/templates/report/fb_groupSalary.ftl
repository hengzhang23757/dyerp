<#macro search>
    <section id="content">
        <section class="vbox">
            <section class="scrollable">
                <div class="layui-row">
                    <form class="layui-form" style="margin: 0 auto;padding-top: 3px;" lay-filter="baseInfo">
                        <table>
                            <tr>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label">开始</label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <input type="text" name="from" id="from" autocomplete="off" class="layui-input">
                                </td>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label">结束</label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <input type="text" name="to" id="to" autocomplete="off" class="layui-input">
                                </td>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label">组名</label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <select type="text" name="groupName" autocomplete="off" id="groupName">
                                    </select>
                                </td>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label">版单号</label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <select name="salaryType" id="salaryType" autocomplete="off" class="layui-form-select" style="width:150px">
                                        <option value="计件">计件</option>
                                        <option value="计时">计时</option>
                                        <option value="汇总">汇总</option>
                                    </select>
                                </td>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label"></label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <div class="layui-input-inline">
                                        <button class="layui-btn" lay-submit lay-filter="searchBeat">搜索</button>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>

                <table id="groupSalaryTable" lay-filter="groupSalaryTable"></table>

                <script type="text/html" id="toolbarTop">
                    <div class="layui-btn-container">
                        <button class="layui-btn layui-btn-sm" lay-event="exportExcel">导出</button>
                    </div>
                </script>

            </section>
        </section>
    </section>

    <link rel="stylesheet" href="/css/layui.css" type="text/css">
    <link rel="stylesheet" href="/css/soulTable.css" type="text/css">
    <link rel="stylesheet" href="/css/selectize.bootstrap3.css" type="text/css">
<#--<script src="/js/common/layui.js" type="text/javascript"></script>-->
    <script src="/js/common/layer.js"></script>
    <script src="/js/common/xm-select.js?t=${currentDate?c}" type="text/javascript"></script>
    <script src="/js/common/selectize.js" type="text/javascript"></script>
    <script src="/js/common/moment.min.js" type="text/javascript"></script>
    <script src="/js/report/groupSalary.js?t=${currentDate?c}"></script>
</#macro>

<style>
    .layui-col-md3 .layui-form-label {
        width:90px
    }
    .layui-col-md6 .layui-form-label {
        width:90px
    }
    .layui-col-md12 .layui-form-label {
        width:90px
    }

</style>


<#--<#macro search>-->
<#--<section id="content">-->
    <#--<section class="vbox">-->
        <#--<section class="scrollable padder water-mark-visible-ff">-->
            <#--<div class="col-md-12" style="padding-top: 20px;padding-left: 0;padding-right: 0;">-->
                <#--<section class="panel panel-default">-->
                    <#--<div class="panel-body">-->
                        <#--<div class="row" style="margin-bottom: 10px">-->
                            <#--<div class="col-md-2" style="padding:0;text-align: right; margin-left: 10px;">-->
                                <#--<span class="font-bold">从：</span>-->
                                <#--<input id="from" type="text" autocomplete="off" class="form-control" placeholder="开始日期" style="width:80%;">-->
                            <#--</div>-->
                            <#--<div class="col-md-2" style="padding:0;margin-left: 50px;">-->
                                <#--<span class="font-bold">到：</span>-->
                                <#--<input id="to" type="text" autocomplete="off" class="form-control" placeholder="结束日期" style="width:80%;">-->
                                <#--</select>-->
                            <#--</div>-->
                            <#--<div class="col-md-2" style="padding:0;margin-left: 50px;">-->
                                <#--<span class="font-bold">组名：</span>-->
                                <#--<select type="text" id="groupName" class="form-control" autocomplete="off" placeholder="选择组名" style="width:60%;">-->
                                    <#--<option value="">请选择组名</option>-->
                                    <#--<option value="车缝A组">车缝A组</option>-->
                                    <#--<option value="车缝B组">车缝B组</option>-->
                                    <#--<option value="车缝C组">车缝C组</option>-->
                                    <#--<option value="车缝D组">车缝D组</option>-->
                                    <#--<option value="车缝E组">车缝E组</option>-->
                                    <#--<option value="车缝F组">车缝F组</option>-->
                                    <#--<option value="车缝G组">车缝G组</option>-->
                                    <#--<option value="车缝H组">车缝H组</option>-->
                                    <#--<option value="后整I组">后整I组</option>-->
                                    <#--<option value="后整J组">后整J组</option>-->
                                    <#--<option value="品控部">品控部</option>-->
                                    <#--<option value="车间">车间</option>-->
                                    <#--<option value="裁床">裁床</option>-->
                                <#--</select>-->
                            <#--</div>-->
                            <#--<div class="col-md-2" style="padding:0;margin-left: 50px;">-->
                                <#--<span class="font-bold">类型：</span>-->
                                <#--<select type="text" id="salaryType" class="form-control" autocomplete="off" style="width:60%;">-->
                                    <#--<option value="计件">计件</option>-->
                                    <#--<option value="计时">计时</option>-->
                                    <#--<option value="汇总">汇总</option>-->
                                <#--</select>-->
                            <#--</div>-->
                            <#--<div class="col-md-2" style="padding:0;width:12% margin-left: 100px;">-->
                                <#--<button  class="btn" style="border-radius: 5px;color:white;font-family: PingFangSC-Semibold, sans-serif;width:60%;"  onclick="search()">查找</button>-->
                            <#--</div>-->
                        <#--</div>-->
                        <#--<div class="col-md-12" style="padding-top: 20px;padding-bottom:10px" id="exportDiv" hidden>-->
                            <#--<button  class="btn btn-s-lg" style="border-radius: 5px;color:white;"  onclick="exportData()">导出</button>-->
                            <#--<button  class="btn btn-s-lg" style="border-radius: 5px;color:white;"  onclick="printDeal()">打印</button>-->
                        <#--</div>-->
                        <#--<div id="addOrderExcel" hidden style="margin-left: 10px;"></div>-->
                    <#--</div>-->
                <#--</section>-->
            <#--</div>-->

            <#--<div class="row">-->
                <#--<div id="entities">-->
                <#--</div>-->
            <#--</div>-->
            <#--<iframe id="printf" src="" width="0" height="0" frameborder="0"></iframe>-->
        <#--</section>-->
    <#--</section>-->
    <#--<a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen"-->
       <#--data-target="#nav"></a>-->
<#--</section>-->
<#--<aside class="bg-light lter b-l aside-md hide" id="notes">-->
    <#--<div class="wrapper">Notification</div>-->
<#--</aside>-->
<#--</section>-->
<#--<link rel="stylesheet" href="/css/handsontable.full.min.css" type="text/css">-->
<#--<script src="/js/common/handsontable.full.min.js" type="text/javascript" ></script>-->
<#--<script src="/js/common/laydate.js" type="text/javascript" ></script>-->
<#--<script src="/js/common/selectize.js" type="text/javascript"></script>-->
<#--<script src="/js/common/export2Excel2.js" type="text/javascript" ></script>-->
<#--<script src="/js/common/zh-CN.js" type="text/javascript" ></script>-->
<#--<script src="/js/common/jquery.blockUI.js" type="text/javascript" ></script>-->
<#--<script src="/js/common/export.js"></script>-->
<#--<script src="/js/common/xlsx.full.min.js"></script>-->
<#--<script src="/js/report/groupSalary.js?t=${currentDate?c}"></script>-->


<#--</#macro>-->

<#--<style>-->
    <#--button {-->
        <#--background:rgb(45, 202, 147);-->
        <#--opacity:0.86;-->
        <#--color: white;-->
        <#--font-family: PingFangSC-Semibold, sans-serif;-->
    <#--}-->

    <#--#entities td {-->
        <#--border: 1px solid #000000;-->
    <#--}-->
<#--</style>-->
