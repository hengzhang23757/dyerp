<#macro search>
    <section id="content">
        <section class="vbox">
            <section class="scrollable">
                <div class="layui-row">
                    <form class="layui-form" style="margin: 0 auto;padding-top: 3px;" lay-filter="baseInfo">
                        <table>
                            <tr>
                                <td><label class="layui-form-label">开始</label></td><td><input type="text" name="from" id="from" autocomplete="off" class="layui-input" style="width:150px"></td>
                                <td><label class="layui-form-label">结束</label></td><td><input name="to" id="to" autocomplete="off" class="layui-input" style="width:150px"></td>
                                <td><label class="layui-form-label">工号</label></td><td><input type="text" name="employeeNumber" id="employeeNumber" autocomplete="off" class="layui-input" style="width:150px"></td>
                                <td><label class="layui-form-label">姓名</label></td><td><input type="text" name="employeeName" id="employeeName" readonly autocomplete="off" class="layui-input" style="width:150px"></td>
                                <td><label class="layui-form-label">组名</label></td><td><input type="text" name="groupName" id="groupName" autocomplete="off" readonly class="layui-input" style="width:150px"></td>
                                <td><label class="layui-form-label"></label></td><td><button class="layui-btn" lay-submit lay-filter="searchBeat">搜索</button></td>
                                <td><label class="layui-form-label"></label></td><td><button class="layui-btn" lay-submit lay-filter="uniformGroup">统一组名</button></td>
                            </tr>
                        </table>
                    </form>
                </div>

                <table id="empSalaryTable" lay-filter="empSalaryTable"></table>

                <script type="text/html" id="toolbarTop">
                    <div class="layui-btn-container">
                        <button class="layui-btn layui-btn-sm" lay-event="exportExcel">导出</button>
                    </div>
                </script>

            </section>
        </section>
    </section>

    <link rel="stylesheet" href="/css/layui.css" type="text/css">
    <link rel="stylesheet" href="/css/soulTable.css" type="text/css">
    <link rel="stylesheet" href="/css/selectize.bootstrap3.css" type="text/css">
<#--<script src="/js/common/layui.js" type="text/javascript"></script>-->
    <script src="/js/common/layer.js"></script>
    <script src="/js/common/xm-select.js?t=${currentDate?c}" type="text/javascript"></script>
    <script src="/js/common/selectize.js" type="text/javascript"></script>
    <script src="/js/common/moment.min.js" type="text/javascript"></script>
    <script src="/js/report/empSalary.js?t=${currentDate?c}"></script>
</#macro>

<style>
    .layui-col-md3 .layui-form-label {
        width:90px
    }
    .layui-col-md6 .layui-form-label {
        width:90px
    }
    .layui-col-md12 .layui-form-label {
        width:90px
    }

    ::-webkit-scrollbar-track-piece {

        background-color:#f8f8f8;

    }

    ::-webkit-scrollbar {

        width:15px;

        height:15px;

    }

    ::-webkit-scrollbar-thumb {

        background-color:#dddddd;

        background-clip:padding-box;

        min-height:28px;

    }

    ::-webkit-scrollbar-thumb:hover {

        background-color:#bbb;

    }

</style>