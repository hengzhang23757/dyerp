<!DOCTYPE html>
<html>
<head>
    <#include "../feedback/fb_script.ftl">
    <@script> </@script>
    <link rel="stylesheet" href="/css/handsontable.full.min.css" type="text/css">
    <script src="/js/common/handsontable.full.min.js" type="text/javascript" ></script>
    <script src="/js/common/export2Excel2.js" type="text/javascript" ></script>
    <script src="/js/common/zh-CN.js" type="text/javascript" ></script>
    <script src="/js/report/procedureBalanceDetail.js?t=${currentDate?c}"></script>
</head>

<body>
    <input  type="hidden" value="${basePath}"  id="basePath"/>
    <input  type="hidden" value="${orderName}"  id="orderName"/>
    <input  type="hidden" value="${from}"  id="from"/>
    <input  type="hidden" value="${to}"  id="to"/>
    <input  type="hidden" value="${groupName}"  id="groupName"/>
    <input  type="hidden" value="${procedureNumber}"  id="procedureNumber"/>
    <div>
        <div class="col-md-12" style="padding-top: 20px;padding-bottom:10px" id="exportDiv" hidden>
            <button  class="btn btn-s-lg" style="border-radius: 5px;color:white;"  onclick="exportData()">导出</button>
        </div>
        <div id="detailExcel" style="margin-left: 10px;"></div>
    </div>
</body>
</html>
<style>
    button {
        background:rgb(45, 202, 147);
        opacity:0.86;
        color: white;
        font-family: PingFangSC-Semibold;
    }
</style>
