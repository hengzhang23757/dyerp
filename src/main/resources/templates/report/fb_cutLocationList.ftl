<section class="panel panel-default">

    <div id="tableDiv">
        <table id="cutLocationTable" class="table table-striped m-b-none ">
            <#if cutLocationList??>
            <thead>
            <tr bgcolor="#ffcb99" style="color: black;">
                <th style="width: 40px">序号</th>
                <th style="width: 90px">订单</th>
                <th style="width: 90px">版单</th>
                <th style="width: 45px">床号</th>
                <th style="width: 45px">颜色</th>
                <th style="width: 45px">尺码</th>
                <th style="width: 45px">数量</th>
                <th style="width: 70px">扎号</th>
                <th style="width: 70px">裁片超市</th>
                <th style="width: 50px">配扎</th>
                <th style="width: 70px">衣胚超市</th>
                <th style="width: 70px">下车间</th>
                <th style="width: 70px">计件</th>
            </tr>
            </thead>
            <tbody>
                <#list cutLocationList as vo>
                <tr>
                    <td>${vo_index+1}</td>
                    <td><#if vo.orderName??>${vo.orderName}</#if></td>
                    <td><#if vo.clothesVersionNumber??>${vo.clothesVersionNumber}</#if></td>
                    <td><#if vo.bedNumber??>${vo.bedNumber?c}</#if></td>
                    <td><#if vo.colorName??>${vo.colorName}</#if></td>
                    <td><#if vo.sizeName??>${vo.sizeName}</#if></td>
                    <td><#if vo.layerCount??>${vo.layerCount?c}</#if></td>
                    <td><#if vo.packageNumber??>${vo.packageNumber?c}</#if></td>
                    <td><#if vo.cutLocation??>${vo.cutLocation}</#if></td>
                    <td><#if vo.matchWork??>${vo.matchWork}</#if></td>
                    <td><#if vo.embLocation??>${vo.embLocation}</#if></td>
                    <td><#if vo.workshop??>${vo.workshop}</#if></td>
                    <td><#if vo.pieceWork??>${vo.pieceWork}</#if></td>
                </tr>
                </#list>
            </tbody>
            <tfoot>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>好片<#if cutLayerCount??>${cutLayerCount?c}</#if>件</td>
                    <td>好片<#if cutCount??>${cutCount?c}</#if>扎</td>
                    <td>入裁片<#if cutLocationCount??>${cutLocationCount?c}</#if>扎</td>
                    <td>配扎<#if matchCount??>${matchCount?c}</#if>扎</td>
                    <td>入衣胚<#if embLocationCount??>${embLocationCount?c}</#if>扎</td>
                    <td>下车间<#if workshopCount??>${workshopCount?c}</#if>扎</td>
                    <td>计件<#if pieceWorkCount??>${pieceWorkCount?c}</#if>扎</td>
                </tr>
            </tfoot>
            <#else>
            <h4 style="text-align: center">无该状态的数据</h4>
            </#if>
        </table>
    </div>

</section>

<style>
    td {
        word-break:break-all;
    }

</style>