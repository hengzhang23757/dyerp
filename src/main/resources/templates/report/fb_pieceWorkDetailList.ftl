<section class="panel panel-default">

    <div id="tableDiv">
        <table id="pieceWorkDetailTable" class="table table-striped m-b-none ">
            <#if pieceWorkDetailList??>
            <thead>
            <tr bgcolor="#ffcb99" style="color: black; text-align: center">
                <th style="width: 45px; text-align: center">序号</th>
                <th style="width: 60px; text-align: center">组名</th>
                <th style="width: 60px; text-align: center">工号</th>
                <th style="width: 60px; text-align: center">姓名</th>
                <th style="width: 60px; text-align: center">日期</th>
                <th style="width: 80px; text-align: center">订单</th>
                <th style="width: 80px; text-align: center">颜色</th>
                <th style="width: 60px; text-align: center">尺码</th>
                <th style="width: 60px; text-align: center">工序号</th>
                <th style="width: 80px; text-align: center">工序名</th>
                <th style="width: 60px; text-align: center">数量</th>
                <th style="width: 60px; text-align: center">单价</th>
                <th style="width: 60px; text-align: center">金额</th>
                <th style="width: 90px; text-align: center">补贴单价</th>
                <th style="width: 90px; text-align: center">补贴金额</th>
            </tr>
            </thead>
            <tbody>
                <#list pieceWorkDetailList as vo>
                <tr>
                    <td style="text-align: center">${vo_index+1}</td>
                    <td style="text-align: center"><#if vo.groupName??>${vo.groupName}</#if></td>
                    <td style="text-align: center"><#if vo.employeeNumber??>${vo.employeeNumber}</#if></td>
                    <td style="text-align: center"><#if vo.employeeName??>${vo.employeeName}</#if></td>
                    <td style="text-align: center"><#if vo.generalDate??>${vo.generalDate?date}</#if></td>
                    <td style="text-align: center"><#if vo.orderName??>${vo.orderName}</#if></td>
                    <td style="text-align: center"><#if vo.colorName??>${vo.colorName}</#if></td>
                    <td style="text-align: center"><#if vo.sizeName??>${vo.sizeName}</#if></td>
                    <td style="text-align: center"><#if vo.procedureNumber??>${vo.procedureNumber}</#if></td>
                    <td style="text-align: center"><#if vo.procedureName??>${vo.procedureName}</#if></td>
                    <td style="text-align: center"><#if vo.pieceCount??>${vo.pieceCount}</#if></td>
                    <td style="text-align: center"><#if vo.price??>${vo.price?string("#.##")}</#if></td>
                    <td style="text-align: center"><#if vo.salary??>${vo.salary?string("#.##")}</#if></td>
                    <td style="text-align: center"><#if vo.priceTwo??>${vo.priceTwo?string("#.##")}</#if></td>
                    <td style="text-align: center"><#if vo.salaryTwo??>${vo.salaryTwo?string("#.##")}</#if></td>
                </tr>
                </#list>
            </tbody>
            <tfoot>
            <tr>
                <th colspan="6" style="text-align:right"></th>
            </tr>
            </tfoot>
            <#else>
            <h4 style="text-align: center">无该状态的数据</h4>
            </#if>
        </table>
    </div>

</section>

<style>
    td {
        word-break:break-all;
    }

</style>