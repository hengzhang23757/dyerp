<#macro search>
    <section id="content">
        <section class="vbox">
            <section class="scrollable padder water-mark-visible-ff"  style="background-color: #f7f7f7;">
                <div class="col-md-12" style="padding-top: 0px;">
                    <section class="panel panel-default">
                        <div class="panel-body">
                            <div class="row" style="margin-bottom: 0px">
                                <table style="border-collapse:separate;border-spacing: 10px;width: 100%;" id="baseInfo">
                                    <tr>
                                        <td style="text-align: right;margin-bottom: 5px;">
                                            <label class="control-label" style="">单号</label>
                                        </td>
                                        <td style="margin-bottom: 5px;">
                                            <input id="clothesVersionNumber" class="form-control" autocomplete="off" placeholder="请输入单号" style="width: 200px">
                                        </td>
                                        <td style="text-align: right;margin-bottom: 5px;">
                                            <label class="control-label">款号</label>
                                        </td>
                                        <td style="margin-bottom: 5px;">
                                            <input id="orderName"  autocomplete="off" placeholder="请输入款号" class="form-control" style="width: 200px">
                                        </td>
                                        <td style="text-align: right;margin-bottom: 5px;">
                                            <label class="control-label">类型</label>
                                        </td>
                                        <td style="margin-bottom: 5px;">
                                            <select id="type" class="form-control" style="width: 200px">
                                                <option value="">选择类型</option>
                                                <option value="detail">详情</option>
                                                <option value="summary">汇总</option>
                                                <option value="figure">图表</option>
                                            </select>
                                        </td>
                                        <td style="margin-bottom: 5px;">
                                            <button  class="btn" style="border-radius: 5px;color:white;font-family: PingFangSC-Semibold, sans-serif;width:50px"  onclick="search()">查找</button>
                                        </td>
                                    </tr>

                                </table>

                            </div>
                            <div id="resultTable" style="text-align: center;font-family: PingFangSC-Semibold;">
                                <table id="reWorkTable" class="table table-striped table-hover">
                                </table>
                            </div>
                            <div id="resultSummaryTable" style="text-align: center;font-family: PingFangSC-Semibold;">
                                <table id="reWorkSummaryTable" class="table table-striped table-hover">
                                </table>
                            </div>
                            <div id="reWorkFigure" style="text-align: center;font-family: PingFangSC-Semibold;margin-top: 20px">
                                <div id="container" style="width: 100%; height: 550px;"></div>
                            </div>
                        </div>
                    </section>
                </div>

                <div class="row">
                    <div id="entities" class="col-sm-12"></div>
                </div>
            </section>
        </section>
        <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen"
           data-target="#nav"></a>
    </section>
    <aside class="bg-light lter b-l aside-md hide" id="notes">
        <div class="wrapper">Notification</div>
    </aside>
    </section>
    <link rel="stylesheet" href="/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="/css/b.tabs.css" type="text/css">
    <script src="/js/common/laydate.js" type="text/javascript" ></script>
    <script src="/js/common/jquery.blockUI.js" type="text/javascript" ></script>
    <script src="/js/common/b.tabs.js?t=${currentDate?c}"></script>
    <link rel="stylesheet" href="/css/selectize.bootstrap3.css" type="text/css">
    <script src="/js/common/selectize.js" type="text/javascript"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css" type="text/css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.bootstrap.min.css">
    <script src="/js/common/moment.min.js" type="text/javascript"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.bootstrap.min.js"></script>
    <script type="text/javascript" src="https://cdn.bootcss.com/echarts/4.2.1-rc1/echarts.min.js"></script>
    <script type="text/javascript" src="/js/homepage/echarts-auto-tooltip.js"></script>
    <script src="/js/report/reWork.js?t=${currentDate?c}"></script>

</#macro>

<style>
    button {
        background:rgb(45, 202, 147);
        opacity:0.86;
        color: white;
        font-family: PingFangSC-Semibold, sans-serif;
    }
</style>
