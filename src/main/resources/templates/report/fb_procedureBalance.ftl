<#macro search>
    <section id="content">
        <section class="vbox">
            <section class="scrollable padder water-mark-visible-ff"  style="background-color: #f7f7f7;">

                <div class="col-md-12" style="padding-top: 10px;height：98%;overflow-x: auto;">
                    <section class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <table style="border-collapse:separate;border-spacing: 10px;width: 100%;" id="baseInfo">
                                    <tr>
                                        <td style="text-align: right;margin-bottom: 15px;">
                                            <label class="control-label" style="">单号</label>
                                        </td>
                                        <td style="margin-bottom: 15px;">
                                            <input id="clothesVersionNumber" class="form-control" autocomplete="off" placeholder="请输入单号" style="width: 200px">
                                        </td>
                                        <td style="text-align: right;margin-bottom: 15px;">
                                            <label class="control-label" style="">款号</label>
                                        </td>
                                        <td style="margin-bottom: 15px;">
                                            <input id="orderName" class="form-control" autocomplete="off" placeholder="请输入款号" style="width: 200px">
                                        </td>
                                        <td style="text-align: right;margin-bottom: 15px;">
                                            <label class="control-label" style="">组名</label>
                                        </td>
                                        <td style="margin-bottom: 15px;">
                                            <select id="groupName" class="form-control" autocomplete="off" style="width:200px">
                                                <option value="">请选择组名</option>
                                                <option value="车缝A组">车缝A组</option>
                                                <option value="车缝B组">车缝B组</option>
                                                <option value="车缝C组">车缝C组</option>
                                                <option value="车缝D组">车缝D组</option>
                                                <option value="车缝E组">车缝E组</option>
                                                <option value="车缝F组">车缝F组</option>
                                                <option value="车缝G组">车缝G组</option>
                                                <option value="车缝H组">车缝H组</option>
                                                <option value="后整I组">后整I组</option>
                                                <option value="后整J组">后整J组</option>
                                                <option value="品控部">品控部</option>
                                                <option value="车间">车间</option>
                                                <option value="裁床">裁床</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right;margin-bottom: 15px;">
                                            <label class="control-label" style="">开始日期</label>
                                        </td>
                                        <td style="margin-bottom: 15px;">
                                            <input type="text" id="from" class="form-control" autocomplete="off" placeholder="开始日期" style="width:200px">
                                        </td>
                                        <td style="text-align: right;margin-bottom: 15px;">
                                            <label class="control-label">结束日期</label>
                                        </td>
                                        <td style="margin-bottom: 15px;">
                                            <input type="text" id="to" class="form-control" autocomplete="off" placeholder="结束日期" style="width:200px">
                                        </td>
                                        <td style="text-align: right;margin-bottom: 15px;">

                                        </td>
                                        <td style="margin-bottom: 15px;">
                                            <button  class="btn" style="border-radius: 5px;color:white;font-family: PingFangSC-Semibold, sans-serif;width:50%;"  onclick="search()">查找</button>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="form-inline">

                                <div style="text-align: center;font-family: PingFangSC-Semibold; width: 45%;float:left">
                                    <table id="procedureBalanceTable" class="table table-striped table-hover">
                                    </table>
                                </div>
                                <div style="width: 54%;float: right">
                                    <div id="procedureInformation" style="text-align: center;font-family: PingFangSC-Semibold; width: 100%;" hidden>
                                        <h3 style="color: #2BC08B">点击左侧查看工序详情</h3>
                                    </div>
                                    <div id="procedureDetail" style="width: 100%;height: 600px;float:left;overflow-x: auto;overflow-y: auto;margin-top: 10px" hidden></div>
                                    <div id="personProcedureDetail" style="width: 100%;height: 500px;float:left;overflow-x: auto;overflow-y: auto;margin-top: 10px" hidden></div>
                                </div>
                                <div id="procedureBalanceFigure" style="text-align: center;font-family: PingFangSC-Semibold;margin-top: 10px; width: 100%;float:left">
                                    <div id="container" style="width: 100%; height: 600px;"></div>
                                </div>

                            </div>
                        </div>
                    </section>
                </div>
            </section>
        </section>
        <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen"
           data-target="#nav"></a>
    </section>
    <aside class="bg-light lter b-l aside-md hide" id="notes">
        <div class="wrapper">Notification</div>
    </aside>
    </section>

    <link rel="stylesheet" href="/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="/css/b.tabs.css" type="text/css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.bootstrap.min.css">

    <script type="text/javascript" src="https://cdn.bootcss.com/echarts/4.2.1-rc1/echarts.min.js"></script>
    <script type="text/javascript" src="/js/homepage/echarts-auto-tooltip.js"></script>
    <script src="/js/common/jquery.blockUI.js" type="text/javascript" ></script>
    <script src="/js/common/moment.min.js" type="text/javascript"></script>
    <script src="/js/common/b.tabs.js?t=${currentDate?c}"></script>
    <script src="/js/common/laydate.js" type="text/javascript" ></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.bootstrap.min.js"></script>
    <script src="/js/report/procedureBalance.js?t=${currentDate?c}"></script>


</#macro>

<style>
    button {
        background:rgb(45, 202, 147);
        opacity:0.86;
        color: white;
        font-family: PingFangSC-Semibold, sans-serif;
    }
    .selected td {
        background-color: #2BC08B !important; /* Add !important to make sure override datables base styles */
    }
</style>
