<#macro search>
    <section id="content">
        <div class="col-md-12" style="padding-top: 20px;padding-bottom:10px;margin-left: 10px">
            <span style="font-size: 20px;font-family: PingFangSC-Semibold;color:rgb(55,56,57)">输入裁片信息</span>
        </div>
        <div class="row">
            <div class="col-md-12">
                <section class="panel panel-default">
                    <header class="panel-heading font-bold">
                        <span class="label bg-success pull-right"></span>基本信息
                    </header>
                    <table style="border-collapse:separate;border-spacing: 10px;width: 100%;" id="baseInfo">
                        <tr>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="control-label" style="">单号</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input id="clothesVersionNumber" class="form-control" autocomplete="off">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="control-label" style="">款号</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input id="orderName" class="form-control" autocomplete="off">
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="control-label" style="">客户</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input id="customerName" class="form-control">
                            </td>

                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="control-label" style="">床号</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input id="bedNumber" class="form-control" oninput="value=value.replace(/[^\d]/g,'')">
                            </td>

                        </tr>

                    </table>
                </section>

                <section class="panel panel-default" id="numberAddDiv">
                    <header class="panel-heading font-bold">
                        <span class="label bg-success pull-right"></span>数量录入
                    </header>
                    <div name="numberDiv" class="row" style="margin-top: 10px;margin-bottom: 10px;margin-left: 20px">
                        <div  class="col-md-3">
                            <span style="font-weight: 700;color:black" name="orderOfLayer">1</span>
                            <label class="control-label" style="">颜色</label>
                            <select name="colorName" class="form-control">
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label class="control-label" style="">缸号</label>
                            <input name="jarNumber" class="form-control" autocomplete="off" onkeydown="moveCursor(event,this)">
                        </div>
                        <div class="col-md-2">
                            <label class="control-label" style="">层数</label>
                            <input name="layer" class="form-control" autocomplete="off" oninput="value=value.replace(/[^\d]/g,'')" onkeyup="totalLayer(this)" onkeydown="moveCursor(event,this)">
                            <div style="font-size: 10px;font-weight: 700;color:black">总计：<span name="totalLayer">0</span>层</div>
                        </div>
                        <div class="col-md-2">
                            <label class="control-label" style="">重量</label>
                            <input name="weight" class="form-control" autocomplete="off" onkeyup="totalWeight(this)"  onkeydown="moveCursor(event,this)">
                            <div style="font-size: 10px;font-weight: 700;color:black">总计：<span name="totalWeight">0</span></div>
                        </div>
                        <div class="col-md-2">
                            <button name="addNumber" class="btn btn-s-xs" style="border-radius: 5px;margin-left: 10px"  onclick="addNumber(this)">增加</button>
                            <button name="delNumber" class="btn btn-s-xs" style="border-radius: 5px;margin-left: 10px;background-color: rgb(236, 108, 98);display: none"  onclick="delNumber(this)">删除</button>
                        </div>
                    </div>
                </section>

                <section class="panel panel-default" id="addSizeDiv">
                    <header class="panel-heading font-bold">
                        <span class="label bg-success pull-right"></span>唛架配比
                    </header>
                    <div name="sizeRadioDiv" class="row" style="margin-top: 10px;margin-bottom: 10px;margin-left: 20px">
                        <div  class="col-md-3">
                            <span style="font-weight: 700;color:black" name="orderOfSizeRadio">1</span>
                            <label class="control-label" style="">尺码</label>
                            <select name="size" class="form-control">
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label class="control-label" style="">配比</label>
                            <input name="radio" class="form-control" autocomplete="off" oninput="value=value.replace(/[^\d]/g,'')" onkeyup="totalSizeRadio(this)" onkeydown="moveCursor(event,this)">
                            <div style="font-size: 10px;font-weight: 700;color:black">总计：<span name="totalSizeRadio">0</span></div>
                        </div>
                        <div class="col-md-3">
                            <button name="addSize" class="btn btn-s-xs" style="border-radius: 5px;margin-left: 10px"  onclick="addSize(this)">增加</button>
                            <button name="delSize" class="btn btn-s-xs" style="border-radius: 5px;margin-left: 10px;background-color: rgb(236, 108, 98);display: none"  onclick="delSize(this)">删除</button>
                        </div>
                    </div>
                </section>

                <section class="panel panel-default">
                    <header class="panel-heading font-bold">
                        <span class="label bg-success pull-right"></span>部位选择<button class="btn btn-s-xs" style="border-radius: 5px;margin-left: 10px" onclick="loadPartName()">刷新</button>
                    </header>
                    <div style="margin-top: 10px;margin-left: 20px;height: 120px;">
                        <select id="partName" multiple style="width:90%" >
                            <#--<option value="大身">大身</option>-->
                        </select>
                    </div>
                </section>
                <div class="col-md-12" id="tailorDataTable" style="height: auto;overflow-x: auto;overflow-y: auto;margin-top: 10px" hidden></div>
            </div>
        </div>
        <div class="col-md-12" style="text-align: center;margin-top: auto">
            <button  class="btn btn-s-lg" style="border-radius: 5px;"  onclick="previewData()">预览</button>
            <button  class="btn btn-s-lg" style="border-radius: 5px;"  onclick="addOrder()">生成</button>
        </div>
    </section>
    <link rel="stylesheet" href="/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="/css/b.tabs.css" type="text/css">
    <link rel="stylesheet" href="/css/selectize.bootstrap3.css" type="text/css">
    <script src="/js/common/selectize.js" type="text/javascript"></script>
    <script src="/js/common/jquery.blockUI.js" type="text/javascript" ></script>
    <script src="/js/common/b.tabs.js?t=${currentDate?c}"></script>
    <script src="/js/tailor/tailor.js?t=${currentDate?c}"></script>
</#macro>

<style>
    button {
        background:rgb(45, 202, 147);
        opacity:0.86;
        color: white;
        font-family: PingFangSC-Semibold;
    }
    ::-webkit-scrollbar-track-piece {

        background-color:#f8f8f8;

    }

    ::-webkit-scrollbar {

        width:15px;

        height:15px;

    }

    ::-webkit-scrollbar-thumb {

        background-color:#dddddd;

        background-clip:padding-box;

        min-height:28px;

    }

    ::-webkit-scrollbar-thumb:hover {

        background-color:#bbb;

    }
</style>
