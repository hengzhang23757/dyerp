<#macro search>
<section id="content">
    <section class="vbox">
        <section class="scrollable padder water-mark-visible-ff"  style="background-color: #f7f7f7;">
            <div class="col-md-12" style="padding-top: 20px;">
                <section class="panel panel-default">
                    <div class="panel-body">
                        <div class="row" style="margin-bottom: 10px">
                            <div class="col-md-3" style="padding:0;text-align: right;">
                                <div class="form-inline">
                                    <div class="form-group" style="text-align:left;">
                                <span class="font-bold">版单号：</span>
                                <input type="text" id="clothesVersionNumber" class="form-control" autocomplete="off" placeholder="请输入版单号" style="width:70%;">
                            </div>
                            </div>
                            </div>
                            <div class="col-md-3" style="padding:0;text-align: right;">
                                <span class="font-bold">订单号：</span>
                                <select type="text" id="orderName" class="form-control" autocomplete="off" style="width:70%;"></select>
                            </div>
                            <div class="col-md-2" style="padding:0;margin-left: 30px;">
                                <span class="font-bold">从：</span>
                                <input type="text" id="from" class="form-control" autocomplete="off" placeholder="开始日期" style="width:70%;">
                            </div>
                            <div class="col-md-2" style="padding:0;margin-left: 20px;">
                                <span class="font-bold">到：</span>
                                <input type="text" id="to" class="form-control" autocomplete="off" placeholder="开始日期" style="width:70%;">
                            </div>
                            <div class="col-md-2" style="padding:0;width:12%">
                            <button  class="btn" style="border-radius: 5px;color:white;font-family: PingFangSC-Semibold, sans-serif;width:50%;"  onclick="search()">查找</button>
                            </div>
                        </div>

                        <div class="col-md-12" style="padding-top: 20px;padding-bottom:10px" id="exportDiv" hidden>
                            <button  class="btn btn-s-lg" style="border-radius: 5px;color:white;"  onclick="exportData()">导出</button>
                        </div>
                        <div id="reportExcel" style="margin-left: 10px;"></div>
                    </div>
                </section>
            </div>
        </section>
    </section>
    <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen"
       data-target="#nav"></a>
</section>
<aside class="bg-light lter b-l aside-md hide" id="notes">
    <div class="wrapper">Notification</div>
</aside>
</section>
<#--<link rel="stylesheet" href="/css/bootstrap.min.css" type="text/css">-->
<script src="/js/common/laydate.js" type="text/javascript" ></script>
<script src="/js/common/selectize.js" type="text/javascript"></script>
<script src="/js/common/jquery.blockUI.js" type="text/javascript" ></script>
<link rel="stylesheet" href="/css/handsontable.full.min.css" type="text/css">
<script src="/js/common/handsontable.full.min.js" type="text/javascript" ></script>
<script src="/js/common/export.js"></script>
<script src="/js/common/xlsx.full.min.js"></script>
<script src="/js/miniProgram/orderHourEmpDetail.js?t=${currentDate?c}"></script>

</#macro>

<style>
    button {
        background:rgb(45, 202, 147);
        opacity:0.86;
        color: white;
        font-family: PingFangSC-Semibold, sans-serif;
    }
</style>
