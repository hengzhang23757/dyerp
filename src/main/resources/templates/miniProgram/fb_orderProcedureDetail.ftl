<!DOCTYPE html>
<html>
<head>
    <#include "../feedback/fb_script.ftl">
    <@script> </@script>
    <link rel="stylesheet" href="/css/handsontable.full.min.css" type="text/css">
    <script src="/js/common/handsontable.full.min.js" type="text/javascript" ></script>
    <script src="/js/common/export2Excel2.js" type="text/javascript" ></script>
    <script src="/js/common/zh-CN.js" type="text/javascript" ></script>
    <script src="/js/miniProgram/orderProcedureDetail.js?t=${currentDate?c}"></script>
</head>

<body>
<input  type="hidden" value="${basePath}"  id="basePath"/>
<input  type="hidden" value="${orderName}"  id="orderName"/>
<input  type="hidden" value="${type}"  id="type"/>
<div class="col-md-12" style="padding-top: 20px;padding-bottom:10px;margin-left: 10px">
    <button  class="btn btn-s-lg" style="border-radius: 5px;"  onclick="exportData()">导出</button>
</div>
<div class="col-md-12" style="padding-top: 20px;padding-bottom:10px;margin-left: 10px">
    <div id="addOrderExcel" style="overflow-x: auto;height: auto"></div>
</div>
<#if type == "review">
    <div class="col-md-12" id="reviewButton" style="text-align: center;margin-top: 20px; margin-bottom: 20px">
        <button  class="btn btn-s-lg" style="border-radius: 5px; background:#ec6c62"  onclick="unPass()">不通过</button>
        <button  class="btn btn-s-lg" style="border-radius: 5px; margin-left: 20px"  onclick="getPass()">通过</button>
    </div>
</#if>
<input hidden id="type" value="${type}">
</body>
</html>
<style>
    button {
        background:rgb(45, 202, 147);
        opacity:0.86;
        color: white;
        font-family: PingFangSC-Semibold;
    }
</style>
