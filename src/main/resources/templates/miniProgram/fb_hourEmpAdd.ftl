<#macro entities>
<#--<section class="panel panel-default">-->
    <div id="editPro" style="display: none;cursor:default">
        <div style="overflow-x: auto;overflow-y:auto;">
            <div style="text-align: right;margin-bottom:20px">
                <a id="editNo" href="#" style="font-size:20px"><i class="fa fa-times icon" style="color: rgb(182,182,182)"></i></a>
            </div>
            <input type="text" hidden id="pieceWorkID">
            <table style="border-collapse:separate;border-spacing: 10px;width: 100%;">
                <tr>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">姓名</label>
                    </td>
                    <td>
                        <input id="employeeName" class="form-control" autocomplete="off" style="margin-bottom: 15px;width: 150px;">
                    </td>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">工号</label>
                    </td>
                    <td>
                        <select id="employeeNumber" class="form-control" autocomplete="off" style="margin-bottom: 15px;width: 150px;"></select>
                    </td>

                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">组名</label>
                    </td>
                    <td>
                        <input id="groupName" class="form-control" autocomplete="off" style="margin-bottom: 15px;width: 150px;">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">单号</label>
                    </td>
                    <td>
                        <input id="clothesVersionNumber" class="form-control" autocomplete="off" placeholder="请输入单号" style="width: 150px;margin-bottom: 15px;">
                    </td>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">款号</label>
                    </td>
                    <td>
                        <input id="orderName" class="form-control" autocomplete="off" placeholder="请输入款号" style="width: 150px;margin-bottom: 15px;">
                    </td>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">数量</label>
                    </td>
                    <td>
                        <input id="layerCount" class="form-control" autocomplete="off" style="margin-bottom: 15px;width: 150px;">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">日期</label>
                    </td>
                    <td>
                        <input id="from" class="form-control" autocomplete="off" style="width: 150px;margin-bottom: 15px;">
                    </td>
                </tr>
            </table>
        </div>
        <div class="col-sm-12" style="padding-top: 10px">
            <button id="editYes" class="btn btn-s-lg" style="border-radius: 5px;"  style="text-align: center;">保存</button>
        </div>
    </div>
<#--</section>-->
</#macro>