<#macro search>
<section id="content">
    <section class="vbox">
        <section class="scrollable padder water-mark-visible-ff">
            <div class="col-md-12" style="padding-top: 20px;">
                <section class="panel panel-default">
                    <div class="panel-body">
                        <div class="row" style="margin-bottom: 10px">
                            <div class="col-md-2" style="padding:0;text-align: left; margin-left: 10px;">
                                <button  class="btn" style="border-radius: 5px;color:white;font-family: PingFangSC-Semibold, sans-serif;width:60%;"  onclick="addPieceWork()">手动输入</button>
                            </div>
                            <div class="col-md-2" style="padding:0;margin-left: 50px;">
                            </div>
                            <div class="col-md-2" style="padding:0;margin-left: 50px;">
                            </div>
                            <div class="col-md-2" style="padding:0;margin-left: 50px;">
                            </div>
                            <div class="col-md-2" style="padding:0;text-align: right; margin-left: 0px;">
                                <select class="btn btn-default" style="border-radius: 5px;color:black;font-family: PingFangSC-Semibold, sans-serif;width:60%;" onchange="changeTable(this)">
                                    <option value="today">今日</option>
                                    <option value="oneMonth">近一个月</option>
                                    <option value="threeMonths">近三个月</option>
                                </select>
                            </div>
                        </div>
                        <div style="text-align: center;font-family: PingFangSC-Semibold;">
                            <table id="pieceWorkDetailTable" class="table table-striped table-hover">
                            </table>
                        </div>
                    </div>
                </section>
            </div>
            <div class="row">
                <div id="entities" class="col-sm-12">
                </div>
                <#include "fb_hourEmpAdd.ftl">
                <@entities></@entities>
                <#include "fb_hourEmpEdit.ftl">
                <@entities></@entities>
            </div>
        </section>
    </section>
    <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen"
       data-target="#nav"></a>
</section>
<aside class="bg-light lter b-l aside-md hide" id="notes">
    <div class="wrapper">Notification</div>
</aside>
</section>
<link rel="stylesheet" href="/css/bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="/css/b.tabs.css" type="text/css">
<script src="/js/common/laydate.js" type="text/javascript" ></script>
<script src="/js/common/jquery.blockUI.js" type="text/javascript" ></script>
<script src="/js/common/b.tabs.js?t=${currentDate?c}"></script>
<link rel="stylesheet" href="/css/selectize.bootstrap3.css" type="text/css">
<script src="/js/common/selectize.js" type="text/javascript"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.bootstrap.min.css">
<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="/js/common/moment.min.js" type="text/javascript"></script>
<script src="/js/miniProgram/hourEmp.js?t=${currentDate?c}"></script>


</#macro>

<style>
    button {
        background:rgb(45, 202, 147);
        opacity:0.86;
        color: white;
        font-family: PingFangSC-Semibold, sans-serif;
    }
</style>
