<#macro entities>
<#--<section class="panel panel-default">-->
    <div id="editManualInputPro" style="display: none;cursor:default">
        <div style="overflow-x: auto;overflow-y:auto;">
            <div style="text-align: right;margin-bottom:20px">
                <a id="editManualInputNo" href="#" style="font-size:20px"><i class="fa fa-times icon" style="color: rgb(182,182,182)"></i></a>
            </div>
            <input type="text" hidden id="pieceWorkID1">
            <table style="border-collapse:separate;border-spacing: 10px;width: 100%;">
                <tr>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">姓名</label>
                    </td>
                    <td>
                        <input id="employeeName1" class="form-control" autocomplete="off" style="margin-bottom: 15px;width: 150px;" readonly>
                    </td>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">工号</label>
                    </td>
                    <td>
                        <input id="employeeNumber1" class="form-control" autocomplete="off" style="margin-bottom: 15px;width: 150px;" readonly>
                    </td>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">组名</label>
                    </td>
                    <td>
                        <input id="groupName1" class="form-control" autocomplete="off" style="margin-bottom: 15px;width: 150px;" readonly>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">版单号</label>
                    </td>
                    <td>
                        <input id="clothesVersionNumber1" class="form-control" autocomplete="off" style="width: 150px;margin-bottom: 15px;" readonly>
                    </td>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">订单号</label>
                    </td>
                    <td>
                        <input id="orderName1" class="form-control" autocomplete="off" style="width: 150px;margin-bottom: 15px;" readonly>
                    </td>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">数量</label>
                    </td>
                    <td>
                        <input id="layerCount1" class="form-control" autocomplete="off" style="width: 150px;margin-bottom: 15px;">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">颜色</label>
                    </td>
                    <td>
                        <input id="colorName1" class="form-control" autocomplete="off" style="margin-bottom: 15px;width: 150px;" readonly>
                    </td>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">尺码</label>
                    </td>
                    <td>
                        <input id="sizeName1" class="form-control" autocomplete="off" style="width: 150px;margin-bottom: 15px;" readonly>
                    </td>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">工序号</label>
                    </td>
                    <td>
                        <input id="procedureNumber1" class="form-control" autocomplete="off" style="width: 150px;margin-bottom: 15px;" readonly>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">工序</label>
                    </td>
                    <td>
                        <input id="procedureName1" class="form-control" autocomplete="off" style="margin-bottom: 15px;width: 150px;" readonly>
                    </td>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">日期</label>
                    </td>
                    <td>
                        <input id="from1" class="form-control" autocomplete="off" style="margin-bottom: 15px;width: 150px;">
                    </td>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">备注</label>
                    </td>
                    <td>
                        <input id="remark1" class="form-control" autocomplete="off" style="margin-bottom: 15px;width: 150px;">
                    </td>
                </tr>
            </table>
        </div>
        <div class="col-sm-12" style="padding-top: 10px">
            <button id="editManualInputYes" class="btn btn-s-lg" style="border-radius: 5px;"  style="text-align: center;">保存</button>
        </div>
    </div>
<#--</section>-->
</#macro>