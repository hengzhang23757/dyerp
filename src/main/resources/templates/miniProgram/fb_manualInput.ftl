<#macro search>
<section id="content">
    <section class="vbox">
        <section class="scrollable">
            <div class="col-md-12">
                <form class="layui-form" style="margin: 0 auto;padding-top: 3px;" lay-filter="baseInfo">
                    <table>
                        <tr>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label">开始</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" name="fromDate" id="fromDate" autocomplete="off" placeholder="开始日期" class="layui-input">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label">结束</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" name="toDate" id="toDate" placeholder="结束日期" autocomplete="off" class="layui-input">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label"></label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <div class="layui-input-inline">
                                    <button class="layui-btn" lay-submit lay-filter="searchBeat">搜索</button>
                                </div>
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label"></label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <div class="layui-input-inline">
                                    <button class="layui-btn" lay-submit lay-filter="create">添加</button>
                                </div>
                            </td>
                        </tr>
                    </table>
                </form>
                <table id="reportTable" lay-filter="reportTable"></table>
            </div>

            <script type="text/html" id="toolbarTop">
                <div class="layui-btn-container">
                    <button class="layui-btn layui-btn-sm" lay-event="clearFilter">清除所有筛选</button>
                    <button class="layui-btn layui-btn-sm" lay-event="exportExcel">导出</button>
                </div>
            </script>

            <div class="row">
                <div id="entities" class="col-sm-12">
                </div>
                <#include "fb_manualInputAdd.ftl">
                <@entities></@entities>
                <#include "fb_manualInputEdit.ftl">
                <@entities></@entities>
            </div>
        </section>
    </section>
</section>
<link rel="stylesheet" href="/css/bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="/css/b.tabs.css" type="text/css">
<link rel="stylesheet" href="/css/layui.css" type="text/css">
<link rel="stylesheet" href="/css/soulTable.css" type="text/css">
<script src="/js/common/laydate.js" type="text/javascript" ></script>
<script src="/js/common/jquery.blockUI.js" type="text/javascript" ></script>
<script src="/js/common/b.tabs.js?t=${currentDate?c}"></script>
<link rel="stylesheet" href="/css/selectize.bootstrap3.css" type="text/css">
<script src="/js/common/selectize.js" type="text/javascript"></script>
<script src="/js/common/moment.min.js" type="text/javascript"></script>
<script src="/js/miniProgram/manualInput.js?t=${currentDate?c}"></script>


</#macro>

<style>
    ::-webkit-scrollbar-track-piece {

        background-color:#f8f8f8;

    }

    ::-webkit-scrollbar {

        width:15px;

        height:15px;

    }

    ::-webkit-scrollbar-thumb {

        background-color:#dddddd;

        background-clip:padding-box;

        min-height:28px;

    }

    ::-webkit-scrollbar-thumb:hover {

        background-color:#bbb;

    }
</style>
