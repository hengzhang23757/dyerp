<!DOCTYPE html>
<html>
<head>
    <#include "../feedback/fb_script.ftl">
    <@script> </@script>
    <link rel="stylesheet" href="/css/handsontable.full.min.css" type="text/css">
    <link rel="stylesheet" href="/css/selectize.bootstrap3.css" type="text/css">
    <link rel="stylesheet" href="/css/tableFilter.css" type="text/css">
    <script src="/js/common/laydate.js" type="text/javascript" ></script>
    <link rel="stylesheet" href="/css/bootstrap.min.css" type="text/css">
    <script src="/js/common/jquery-form.js"></script>
    <script src="/js/common/handsontable.full.min.js" type="text/javascript" ></script>
    <script src="/js/common/zh-CN.js" type="text/javascript" ></script>
    <script src="/js/common/tableFilter.js" type="text/javascript" ></script>
    <script src="/js/common/selectize.js" type="text/javascript"></script>
    <script src="/js/common/layer.js"></script>
    <script src="/js/miniProgram/orderProcedureAdd.js?t=${currentDate?c}"></script>
</head>

<body>
<input type="hidden" value="${basePath}" id="basePath"/>
<input type="hidden" value="${type!?string}" id="hideType"/>
<input type="hidden" value="${orderName!?string}" id="hideOrderName"/>
<input type="hidden" value="${userName}" id="userName"/>

<div class="col-md-12 row" style="padding-top: 10px;">
    <section class="panel panel-default">
        <header class="panel-heading font-bold">
            <span class="label bg-success pull-right"></span>订单信息
        </header>
        <table style="border-collapse:separate;border-spacing: 10px;width: 100%;" id="baseInfo">
            <tr>
                <td style="text-align: right;margin-bottom: 15px;">
                    <label class="control-label" style="">单号</label>
                </td>
                <td style="margin-bottom: 15px;">
                    <input id="clothesVersionNumber" class="form-control" placeholder="请输入单号" autocomplete="off">
                </td>
                <td style="text-align: right;margin-bottom: 15px;">
                    <label class="control-label" style="">款号</label>
                </td>
                <td style="margin-bottom: 15px;">
                    <input id="orderName" class="form-control" placeholder="请输入款号" autocomplete="off">
                </td>
                <td style="text-align: right;margin-bottom: 15px;">
                    <label class="control-label" style="">客户</label>
                </td>
                <td style="margin-bottom: 15px;">
                    <input id="customerName" class="form-control">
                </td>
                <td style="text-align: right;margin-bottom: 15px;">
                    <label class="control-label" style="">款式描述</label>
                </td>
                <td style="margin-bottom: 15px;">
                    <input id="styleDescription" class="form-control">
                </td>
            </tr>
            <tr>
                <td style="text-align: right;margin-bottom: 15px;">
                    <label class="control-label">开单日期</label>
                </td>
                <td style="margin-bottom: 15px;">
                    <input id="beginDate" class="form-control" autocomplete="off">
                </td>
                <td style="text-align: right;margin-bottom: 15px;">
                    <label class="control-label">数量</label>
                </td>
                <td style="margin-bottom: 15px;">
                    <input id="orderCount" class="form-control">
                </td>
                <td style="text-align: right;margin-bottom: 15px;">
                    <label class="control-label">补贴比例</label>
                </td>
                <td style="margin-bottom: 15px;">
                    <input id="subsidyPer" class="form-control">
                </td>

                <#--<td style="text-align: right;margin-bottom: 15px;">-->
                <#--<label class="control-label">选择图片</label>-->
                <#--</td>-->
                <#--<td style="margin-bottom: 15px;">-->
                <#--<form id="fileForm" name="fileForm" method="POST" enctype="multipart/form-data">-->
                <#--<input name="file" class="btn btn-s-lg" style="border-radius: 5px;" id="fileToUpload" type="file"/>-->
                <#--</form>-->
                <#--</td>-->
            </tr>
        </table>
    </section>

    <div class="row">
        <div class="col-md-6" style="padding: 10px 0 0 0;">
            <section  class="panel panel-default">
                <header class="panel-heading font-bold">
                    <span class="label bg-success pull-right"></span>已选工序
                </header>
                <div style="text-align: center;font-family: PingFangSC-Semibold,sans-serif;">
                    <table class="table table-striped" id="orderProcedureSelTable" style="width:100%" lay-filter="selTableFilter">
                    </table>
                </div>
            </section>
        </div>
        <div class="col-md-6" style="padding: 10px 0 0 0;">
            <section  class="panel panel-default">
                <header class="panel-heading font-bold">
                    <span class="label bg-success pull-right"></span>全部工序
                </header>
                <div style="text-align: center;font-family: PingFangSC-Semibold,sans-serif;">
                    <table class="table table-striped table-hover" id="orderProcedureAddTable" lay-filter="addTableFilter">
                    </table>
                </div>
            </section>
        </div>
    </div>
</div>

<div class="col-md-12" style="text-align: center;">
    <button  class="btn btn-s-lg" style="border-radius: 5px;color: white;background:rgb(45, 202, 147);opacity:0.86;"  onclick="add()">提交</button>
</div>

</body>
<script type="text/html" id="switchTpl">
    <input type="checkbox" name="special" value="{{d.specialProcedure}}" lay-skin="switch" lay-text="是|否" lay-filter="specialFilter" {{ d.specialProcedure == "是" ? 'checked' : '' }} <#if type=="update">{{ d.procedureState=='1' || d.procedureState=='2' ? 'disabled' : '' }}</#if>
    <#--<input type="checkbox" name="special" value="{{d.specialProcedure}}" lay-skin="switch" lay-text="是|否" lay-filter="specialFilter" {{ d.specialProcedure == "是" ? 'checked' : '' }} {{ d.procedureState=='1' || d.procedureState=='2' ? 'disabled' : '' }}>-->
</script>

<style>
    button {
        background:rgb(45, 202, 147);
        opacity:0.86;
        color: white;
        font-family: PingFangSC-Semibold, sans-serif;
    }
    ::-webkit-scrollbar-track-piece {

        background-color:#f8f8f8;

    }

    ::-webkit-scrollbar {

        width:15px;

        height:15px;

    }

    ::-webkit-scrollbar-thumb {

        background-color:#dddddd;

        background-clip:padding-box;

        min-height:28px;

    }

    ::-webkit-scrollbar-thumb:hover {

        background-color:#bbb;

    }
</style>


</html>
