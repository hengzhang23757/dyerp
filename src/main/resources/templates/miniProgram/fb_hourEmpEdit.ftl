<#macro entities>
<#--<section class="panel panel-default">-->
    <div id="editHourEmpPro" style="display: none;cursor:default">
        <div style="overflow-x: auto;overflow-y:auto;">
            <div style="text-align: right;margin-bottom:20px">
                <a id="editHourEmpNo" href="#" style="font-size:20px"><i class="fa fa-times icon" style="color: rgb(182,182,182)"></i></a>
            </div>
            <input type="text" hidden id="hourEmpID1">
            <table style="border-collapse:separate;border-spacing: 10px;width: 100%;">
                <tr>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">工号</label>
                    </td>
                    <td>
                        <input id="employeeNumber1" class="form-control" autocomplete="off" style="margin-bottom: 15px;width: 150px;">
                    </td>

                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">订单号</label>
                    </td>
                    <td>
                        <input id="orderName1" class="form-control" autocomplete="off" style="width: 150px;margin-bottom: 15px;">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">数量</label>
                    </td>
                    <td>
                        <input id="layerCount1" class="form-control" autocomplete="off" style="margin-bottom: 15px;width: 150px;">
                    </td>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">日期</label>
                    </td>
                    <td>
                        <input id="from1" class="form-control" autocomplete="off" style="width: 150px;margin-bottom: 15px;">
                    </td>
                </tr>
            </table>
        </div>
        <div class="col-sm-12" style="padding-top: 10px">
            <button id="editHourEmpYes" class="btn btn-s-lg" style="border-radius: 5px;"  style="text-align: center;">保存</button>
        </div>
    </div>
<#--</section>-->
</#macro>