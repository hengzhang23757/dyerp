<#macro entities>
<#--<section class="panel panel-default">-->
    <div id="editProOther" style="display: none;cursor:default">
        <div style="overflow-x: auto;overflow-y:auto;">
            <div style="text-align: right;margin-bottom:20px">
                <a id="editNoOther" href="#" style="font-size:20px"><i class="fa fa-times icon" style="color: rgb(182,182,182)"></i></a>
            </div>
            <input type="text" hidden id="pieceWorkIDOther">
            <table style="border-collapse:separate;border-spacing: 10px;width: 100%;">
                <tr>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">工号</label>
                    </td>
                    <td>
                        <input id="employeeNumberOther" class="form-control" autocomplete="off" style="margin-bottom: 15px;width: 100%;">
                        <div id="employeeNumberTipsOther"></div>
                    </td>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">姓名</label>
                    </td>
                    <td>
                        <input id="employeeNameOther" class="form-control" autocomplete="off" style="width: 100%;margin-bottom: 15px;">
                    </td>

                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">组名</label>
                    </td>
                    <td>
                        <input id="groupNameOther" class="form-control" autocomplete="off" style="margin-bottom: 15px;width: 100%;">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">订单号</label>
                    </td>
                    <td>
                        <input id="orderNameOther" class="form-control" autocomplete="off" style="width: 100%;margin-bottom: 15px;">
                        <div id="orderNameTipsOther"></div>
                    </td>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">床号</label>
                    </td>
                    <td>
                        <select id="bedNumberOther" class="form-control" autocomplete="off" style="margin-bottom: 15px;width: 100%;"></select>
                    </td>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">颜色</label>
                    </td>
                    <td>
                        <select id="colorNameOther" class="form-control" autocomplete="off" style="margin-bottom: 15px;width: 100%;"></select>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">扎号</label>
                    </td>
                    <td>
                        <input id="packageNumberOther" class="form-control" autocomplete="off" style="width: 100%;margin-bottom: 15px;">
                    </td>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">数量</label>
                    </td>
                    <td>
                        <input id="layerCountOther" class="form-control" autocomplete="off" style="margin-bottom: 15px;width: 100%;">
                    </td>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">工序号</label>
                    </td>
                    <td>
                        <select id="procedureNumberOther" class="form-control" autocomplete="off" style="margin-bottom: 15px;width: 100%;"></select>
                    </td>
                </tr>
            </table>
        </div>
        <div class="col-sm-12" style="padding-top: 10px">
            <button id="editYesOther" class="btn btn-s-lg" style="border-radius: 5px;"  style="text-align: center;">保存</button>
            <#--<button id="editNo" class="btn btn-danger btn-rounded btn-small" style="text-align: center;">取消</button>-->
        </div>
    </div>
<#--</section>-->
</#macro>