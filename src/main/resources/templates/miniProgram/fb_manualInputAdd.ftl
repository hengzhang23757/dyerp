<#macro entities>
<#--<section class="panel panel-default">-->
    <div id="editPro" style="display: none;cursor:default">
        <div style="overflow-x: auto;overflow-y:auto;">
            <div style="text-align: right;margin-bottom:20px">
                <a id="editNo" href="#" style="font-size:20px"><i class="fa fa-times icon" style="color: rgb(182,182,182)"></i></a>
            </div>
            <input type="text" id="pieceWorkID" autofocus hidden>
            <table style="border-collapse:separate;border-spacing: 10px;width: 100%;">
                <tr>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">工号</label>
                    </td>
                    <td>
                        <input id="employeeNumber" class="form-control" autocomplete="off" style="margin-bottom: 15px;width: 70%;">
                    </td>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">姓名</label>
                    </td>
                    <td>
                        <input id="employeeName" class="form-control" autocomplete="off" style="margin-bottom: 15px;width: 70%;">
                    </td>

                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">组名</label>
                    </td>
                    <td>
                        <input id="groupName" class="form-control" autocomplete="off" style="margin-bottom: 15px;width: 70%;">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">单号</label>
                    </td>
                    <td>
                        <input id="clothesVersionNumber" class="form-control" placeholder="请输入单号" autocomplete="off" style="width: 70%;margin-bottom: 15px;">
                    </td>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">款号</label>
                    </td>
                    <td>
                        <input id="orderName" class="form-control" autocomplete="off" placeholder="请输入款号" style="width: 70%;margin-bottom: 15px;">
                    </td>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">工序</label>
                    </td>
                    <td>
                        <select id="procedureNumber" class="form-control" autocomplete="off" style="width: 70%;margin-bottom: 15px;"></select>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">颜色可选</label>
                    </td>
                    <td>
                        <input id="colorNameOption" class="form-control" style="width: 70%;margin-bottom: 15px;" readonly>
                    </td>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">尺码可选</label>
                    </td>
                    <td>
                        <input id="sizeNameOption" class="form-control" style="margin-bottom: 15px;width: 70%;" readonly>
                    </td>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">好/已/可</label>
                    </td>
                    <td>
                        <input id="finishCount" class="form-control" style="margin-bottom: 15px;width: 70%;" readonly>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">颜色</label>
                    </td>
                    <td>
                        <select id="colorName" class="form-control" autocomplete="off" style="width: 70%;margin-bottom: 15px;"></select>
                    </td>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">尺码</label>
                    </td>
                    <td>
                        <select id="sizeName" class="form-control" autocomplete="off" style="margin-bottom: 15px;width: 70%;"></select>
                    </td>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">数量</label>
                    </td>
                    <td>
                        <input id="layerCount" class="form-control" autocomplete="off" style="margin-bottom: 15px;width: 70%;">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">日期</label>
                    </td>
                    <td>
                        <input id="from" class="form-control" autocomplete="off" style="width: 70%;margin-bottom: 15px;">
                    </td>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">备注</label>
                    </td>
                    <td>
                        <input id="remark" class="form-control" value="无" autocomplete="off" style="width: 70%;margin-bottom: 15px;">
                    </td>
                </tr>
            </table>
        </div>
        <div class="col-sm-12" style="padding-top: 10px">
            <button id="editYes" class="btn btn-s-lg" style="border-radius: 5px;"  style="text-align: center;">保存</button>
            <#--<button id="editNo" class="btn btn-danger btn-rounded btn-small" style="text-align: center;">取消</button>-->
        </div>
    </div>
<#--</section>-->
</#macro>