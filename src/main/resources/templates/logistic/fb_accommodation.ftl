<#macro search>
<section id="content">
    <section class="vbox">
        <section class="scrollable padder water-mark-visible-ff" id="roomList"
                 style="padding-top:20px;text-align: center">
            <#if data?? && (data?size > 0)>
            <#list data?keys as floor>
            <div class="col-sm-12">
                <section class="panel panel-default">
                    <div class="panel-body">
                        <#list data[floor]?keys as room>
                            <div class="col-md-3">
                                    <#if data[floor][room]?? && (data[floor][room]?size > 0) && (data[floor][room]?size < data[floor][room][0].capacity)>
                                    <label style="border-radius: 6px;color:white;height:200px;background: rgb(45,202,147);padding:5px;width:100%;line-height: 22px;">
                                    <#list data[floor][room] as emproom>
                                        ${emproom.bedNumber!}&nbsp;&nbsp;${emproom.employeeName!}
                                        &nbsp;&nbsp;${emproom.groupName!}&nbsp;&nbsp;<a style="color:dodgerblue" href="#" onclick="deleteEmpRoom(${emproom.empRoomID?c},'${emproom.employeeName!}')">退房</a><br>
                                    </#list>
                                    </label>
                                    <div style="padding-bottom: 20px;font-weight: 600;">
                                        ${room}&nbsp;<a style="color:dodgerblue" href="#" onclick="addEmpRoom('${floor}','${room}')">入住</a>
                                    </div>
                                    <#elseif data[floor][room]?? && (data[floor][room]?size > 0) && (data[floor][room]?size == data[floor][room][0].capacity)>
                                    <label style="border-radius: 6px;color:white;height:200px;background: orange;padding:5px;width:100%;line-height: 22px;">
                                        <#list data[floor][room] as emproom>
                                            ${emproom.bedNumber!}&nbsp;&nbsp;${emproom.employeeName!}
                                        &nbsp;&nbsp;${emproom.groupName!}&nbsp;&nbsp;<a style="color:dodgerblue" href="#" onclick="deleteEmpRoom(${emproom.empRoomID?c},'${emproom.employeeName!}')">退房</a><br>
                                        </#list>
                                    </label>
                                    <div style="padding-bottom: 20px;font-weight: 600;">
                                        ${room}&nbsp;<span style="color:red">已满</span>
                                    </div>
                                    <#else>
                                    <label style="border-radius: 6px;color:white;height:200px;background: rgb(45,202,147);padding:5px;width:100%;line-height: 22px;">
                                        &nbsp;
                                    </label>
                                    <div style="padding-bottom: 20px;font-weight: 600;">
                                        ${room}&nbsp;<a style="color:dodgerblue" href="#" onclick="addEmpRoom('${floor}','${room}')">入住</a>
                                    </div>
                                    </#if>

                            </div>
                        </#list>
                    </div>
                </section>
            </div>
            </#list>
            </#if>
        </section>
    </section>
    <#include "fb_empRoomAdd.ftl">
    <@entities></@entities>
</section>
</section>
<script src="/js/common/laydate.js" type="text/javascript"></script>
<script src="/js/common/moment.min.js" type="text/javascript"></script>
<script src="/js/common/layer.js"></script>
<script src="/js/logistic/accommodation.js?t=${currentDate?c}"></script>
</#macro>

<style>
    button {
        background: rgb(45, 202, 147);
        opacity: 0.86;
        color: white;
        font-family: PingFangSC-Semibold, sans-serif;
    }
</style>
