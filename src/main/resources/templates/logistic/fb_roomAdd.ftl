<#macro entities>
    <div id="editPro" style="display: none;cursor:default">
        <input type="text" hidden id="roomID">
        <table style="border-collapse:separate;border-spacing: 10px;width: 100%;" id="roomTableDiv">
            <tr name="roomDiv">
                <td>
                    <label class="control-label" style="margin-bottom: 15px;text-align: right;">楼层</label>
                </td>
                <td>
                    <input name="floor" class="form-control" style="margin-bottom: 15px;width: 150px;border-top: none;border-right: none;border-left: none;">
                </td>
                <td>
                    <label class="control-label" style="margin-bottom: 15px;text-align: right;">房间号</label>
                </td>
                <td>
                    <input name="roomNumber" class="form-control" style="margin-bottom: 15px;width: 150px;border-top: none;border-right: none;border-left: none;">
                </td>
                <td>
                    <label class="control-label" style="margin-bottom: 15px;text-align: right;">容量</label>
                </td>
                <td>
                    <input name="capacity" class="form-control" style="margin-bottom: 15px;width: 150px;border-top: none;border-right: none;border-left: none;">
                </td>
                <td>
                    <label class="control-label" style="margin-bottom: 15px;text-align: right;">备注</label>
                </td>
                <td style="text-align: left;">
                    <form class="form-inline">
                        <input name="remark" class="form-control" style="margin-bottom: 15px;width: 120px;border-top: none;border-right: none;border-left: none;">
                        <button name="addSizeBtn" class="btn" style="margin-bottom: 15px;outline:none;border-radius: 5px;color: white;" onclick="addSize(this)"><i class="fa fa-plus"></i></button>
                        <button name="delSizeBtn" class="btn" style="margin-bottom: 15px;outline:none;border-radius: 5px;color: white;;display: none;background-color: rgb(236, 108, 98);" onclick="delSize(this)"><i class="fa fa-minus"></i></button>
                    </form>
                </td>
            </tr>
        </table>
    </div>
</#macro>