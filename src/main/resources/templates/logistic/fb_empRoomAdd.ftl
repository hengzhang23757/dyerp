<#macro entities>
    <div id="editPro" style="display: none;cursor:default">
        <table style="border-collapse:separate;border-spacing: 10px;width: 100%;">
            <tr>
                <td>
                    <label class="control-label" style="margin-bottom: 15px;text-align: right;">工号</label>
                </td>
                <td>
                    <input id="employeeNumber" class="form-control" style="margin-bottom: 15px;width: 150px;border-top: none;border-right: none;border-left: none;">
                </td>
                <td>
                    <label class="control-label" style="margin-bottom: 15px;text-align: right;">姓名</label>
                </td>
                <td>
                    <input id="employeeName" class="form-control" style="margin-bottom: 15px;width: 150px;border-top: none;border-right: none;border-left: none;">
                </td>
                <td>
                    <label class="control-label" style="margin-bottom: 15px;text-align: right;">楼层</label>
                </td>
                <td>
                    <select id="floor" class="form-control" style="margin-bottom: 15px;width: 150px;border-top: none;border-right: none;border-left: none;"></select>
                </td>
            </tr>
            <tr>
                <td>
                    <label class="control-label" style="margin-bottom: 15px;text-align: right;">房间号</label>
                </td>
                <td>
                    <select id="roomNumber" class="form-control" style="margin-bottom: 15px;width: 150px;border-top: none;border-right: none;border-left: none;"></select>
                </td>
                <td>
                    <label class="control-label" style="margin-bottom: 15px;text-align: right;">床号</label>
                </td>
                <td>
                    <select id="bedNumber" class="form-control" style="margin-bottom: 15px;width: 150px;border-top: none;border-right: none;border-left: none;"></select>
                </td>
                <td>
                    <label class="control-label" style="margin-bottom: 15px;text-align: right;">入住日期</label>
                </td>
                <td>
                    <input id="checkInDate" class="form-control" style="margin-bottom: 15px;width: 150px;border-top: none;border-right: none;border-left: none;">
                </td>
            </tr>
        </table>
    </div>
</#macro>

<style>
    .layui-layer-page .layui-layer-content {overflow: inherit !important;}
</style>