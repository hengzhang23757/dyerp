<#macro entities>
    <div id="editRoomPro" style="display: none;cursor:default">
            <input type="text" hidden id="roomID1">
            <table style="border-collapse:separate;border-spacing: 10px;width: 100%;">
                <tr>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">楼层</label>
                    </td>
                    <td>
                        <input id="floor1" class="form-control" autocomplete="off" style="margin-bottom: 15px;width: 250px;">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">房间号</label>
                    </td>
                    <td>
                        <input id="roomNumber1" class="form-control" autocomplete="off" style="width: 250px;margin-bottom: 15px;">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">容量</label>
                    </td>
                    <td>
                        <input id="capacity1" class="form-control" autocomplete="off" style="width: 250px;margin-bottom: 15px;">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">备注</label>
                    </td>
                    <td>
                        <input id="remark1" class="form-control" autocomplete="off" style="width: 250px;margin-bottom: 15px;">
                    </td>
                </tr>
            </table>
    </div>
</#macro>