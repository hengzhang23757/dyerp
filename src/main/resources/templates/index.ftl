<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>恒达精益管理</title>
</head>
<body>
    <input  type="hidden" value="${basePath}"  id="basePath"/>
    <div class="row" style="text-align: center">
        <label style="color:rgb(45, 202, 147);font-size: 50px;margin-top: 8%;">恒达精益管理</label>
    </div>
    <div class="row">
        <div class="col-md-6 col-md-offset-3" style="margin-top: 50px;">
            <div class="center-block" style="width: 43%;text-align: left;">
                <label style="color:white;font-size: 18px;">管理员账号</label>
                <input  id="userName" type="text" autocomplete="off" class="form-control center-block" placeholder="请输入账号">
            </div>
        </div>
        <div class="col-md-6 col-md-offset-3" style="margin-top: 50px">
            <div class="center-block" style="width: 43%;text-align: left;">
                <label style="color:white;font-size: 18px;">密码</label>
                <input id="passWord" autocomplete="off" type="password" class="form-control center-block" placeholder="请输入密码">
            </div>
        </div>
        <div class="col-md-6 col-md-offset-3" style="margin-top: 84px;">
            <button id="loginBtn" class="btn btn-s-lg" onclick="login()">登录</button>
        </div>
    </div>

    <div class="footer">
        <div class="col-md-12">
            <a href="https://beian.miit.gov.cn" target="_blank" style="font-size: medium">粤ICP备19055494号-1</a>
        </div>
    </div>

</body>


</html>
<link rel="stylesheet" href="/css/app.v2.css" type="text/css"/>
<link rel="stylesheet" href="/css/sweetalert.css" type="text/css">
<script src="/js/common/jquery.js"></script>
<script src="/js/common/jquery-ui.js"></script>
<script src="/js/common/sweetalert.min.js"></script>
<script src="/js/login/login.js?t=${currentDate?c}"></script>

<style>
    html,body { height: 100% }

    body{
        background:url("/images/index.png") no-repeat;
        background-size: cover;
        text-align:center;
        filter: brightness(0.9);
    }

    #loginBtn {
        outline:none;
        background:rgb(45, 202, 147);
        opacity:0.86;
        color: white;
        font-size: 16px;
        width:43%;
        /*font-family:PingFangSC-Semibold,sans-serif;*/
    }

    .form-control {
        width:100%;
        background: rgb(225,225,225);
        border: rgba(255,255,255);
        opacity:0.86;
        /*font-family:PingFangSC-Medium,sans-serif;*/
    }

    .footer{
        position:absolute;
        bottom:0;
        width:100%;
        background-color: white;
        min-height:30px
    }
</style>