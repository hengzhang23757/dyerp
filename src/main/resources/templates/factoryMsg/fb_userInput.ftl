<#macro userInput>
    <div id="userInput" style="display: none;cursor:default;">
        <form class="layui-form" style="margin: 5px;padding-top: 0px;" lay-filter="userAdd">
            <input hidden id="userID">
            <table>
                <tr>
                    <td style="text-align: right;margin-bottom: 15px;margin-top: 15px;width: 100px">
                        <label class="layui-form-label">部门</label>
                    </td>
                    <td style="margin-bottom: 15px;margin-top: 15px">
                        <select type="text" name="department" id="department" class="layui-input">
                            <option value="总经办">总经办</option>
                            <option value="业务部">业务</option>
                            <option value="面料">面料</option>
                            <option value="辅料">辅料</option>
                            <option value="裁床">裁床</option>
                            <option value="车缝">车缝</option>
                            <option value="后整">后整</option>
                            <option value="财务部">财务</option>
                            <option value="IQC">IQC</option>
                            <option value="技术部">技术部</option>
                            <option value="行政部">行政部</option>
                        </select>
                    </td>
                    <td style="text-align: right;margin-bottom: 15px;margin-top: 15px;width: 100px">
                        <label class="layui-form-label">职位</label>
                    </td>
                    <td style="margin-bottom: 15px;margin-top: 15px">
                        <select type="text" name="positionName" id="positionName" class="layui-input">
                            <option value=""></option>
                            <option value="root">管理员</option>
                            <option value="role1">裁床打菲</option>
                            <option value="role2">跟单</option>
                            <option value="role3">面料跟单</option>
                            <option value="role4">行政助理</option>
                            <option value="role5">IE主管</option>
                            <option value="role6">裁片衣胚</option>
                            <option value="role7">吊挂查数</option>
                            <option value="role8">车间文员</option>
                            <option value="role9">分厂</option>
                            <option value="role10">财务管理</option>
                            <option value="role11">面料仓</option>
                            <option value="role12">部门主管</option>
                            <option value="role13">计件工</option>
                            <option value="role14">包装打菲</option>
                            <option value="role15">IQC</option>
                            <option value="role16">板房</option>
                            <option value="role17">辅料</option>
                            <option value="role18">辅料仓</option>
                            <option value="role19">外发负责</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right;margin-bottom: 15px;width: 100px">
                        <label class="layui-form-label">用户名</label>
                    </td>
                    <td style="margin-bottom: 15px;">
                        <input type="text" name="userName" id="userName" class="layui-input">
                    </td>
                    <td style="text-align: right;margin-bottom: 15px;width: 100px">
                        <label class="layui-form-label">密码</label>
                    </td>
                    <td style="margin-bottom: 15px;">
                        <input type="text" name="passWord" id="passWord" class="layui-input">
                    </td>
                </tr>
            </table>
        </form>
    </div>
</#macro>

<style>
    .layui-col-md3 .layui-form-label {
        width:90px
    }
    .layui-col-md6 .layui-form-label {
        width:90px
    }
    .layui-col-md12 .layui-form-label {
        width:90px
    }
</style>