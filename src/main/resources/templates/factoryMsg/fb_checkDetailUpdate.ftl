<#macro checkDetailUpdate>
    <div id="checkDetailUpdate" style="display: none;cursor:default;">
        <form class="layui-form" style="margin: 5px;padding-top: 40px;" lay-filter="checkDetailUpdate">
            <div class="layui-row layui-col-space10">
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">工号</label>
                    <div class="layui-input-inline">
                        <input type="text" name="employeeNumber" autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">姓名</label>
                    <div class="layui-input-inline">
                        <input type="text" name="employeeName" autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">日期</label>
                    <div class="layui-input-inline">
                        <input type="text" name="signDate" autocomplete="off" class="layui-input">
                    </div>
                </div>
            </div>
            <div class="layui-row layui-col-space10">
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">第一次</label>
                    <div class="layui-input-inline">
                        <input type="text" name="in1" id="in1" autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">第二次</label>
                    <div class="layui-input-inline">
                        <input type="text" name="out1" id="out1" autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">第三次</label>
                    <div class="layui-input-inline">
                        <input type="text" name="in2" id="in2" autocomplete="off" class="layui-input">
                    </div>
                </div>
            </div>
            <div class="layui-row layui-col-space10">
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">第四次</label>
                    <div class="layui-input-inline">
                        <input type="text" name="out2" id="out2" autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">第五次</label>
                    <div class="layui-input-inline">
                        <input type="text" name="in3" id="in3" autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">第六次</label>
                    <div class="layui-input-inline">
                        <input type="text" name="out3" id="out3" autocomplete="off" class="layui-input">
                    </div>
                </div>
            </div>
            <div class="layui-row layui-col-space10">
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">出勤</label>
                    <div class="layui-input-inline">
                        <input type="text" name="workDays" autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">缺勤</label>
                    <div class="layui-input-inline">
                        <input type="text" name="absentDays" autocomplete="off" class="layui-input" readonly="true">
                    </div>
                </div>
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">实际(时)</label>
                    <div class="layui-input-inline">
                        <input type="text" name="factHours" autocomplete="off" class="layui-input">
                    </div>
                </div>
            </div>
            <div class="layui-row layui-col-space10">
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">加班(时)</label>
                    <div class="layui-input-inline">
                        <input type="text" name="otHours" autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">迟到(分)</label>
                    <div class="layui-input-inline">
                        <input type="text" name="lateMinutes1" autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">早退(分)</label>
                    <div class="layui-input-inline">
                        <input type="text" name="leaveMinutes1" autocomplete="off" class="layui-input">
                    </div>
                </div>

            </div>
            <div class="layui-row layui-col-space10">
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">旷工(时)</label>
                    <div class="layui-input-inline">
                        <input type="text" name="absentHours1" autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">备注</label>
                    <div class="layui-input-inline">
                        <select type="text" name="notes" autocomplete="off" class="layui-input">
                            <option value="正常">正常</option>
                        </select>
                    </div>
                </div>
            </div>
        </form>
    </div>
</#macro>

<style>
    .layui-col-md3 .layui-form-label {
        width:90px
    }
    .layui-col-md6 .layui-form-label {
        width:90px
    }
    .layui-col-md12 .layui-form-label {
        width:90px
    }

    #staffAdd .text {
        width: 8%;
        text-align: center;
    }

    #staffAdd .content {
        width: 17%;
    }

    #staffAdd input {
        border-radius: 0;
        border-color: white;
    }

    #staffAdd select {
        border-color: white;
    }

    #staffAdd td {
        border: solid black;
        border-width: 0px 1px 1px 0px;
    }

    #staffAdd table {
        border: solid black;
        border-width: 1px 0px 0px 1px;
    }
</style>