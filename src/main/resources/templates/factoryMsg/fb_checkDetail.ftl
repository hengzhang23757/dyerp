<#macro search>
<section id="content">
    <section class="vbox">
        <section class="scrollable">
            <div class="layui-row">
                <form class="layui-form" style="margin: 0 auto;padding-top: 3px;" lay-filter="baseInfo">
                    <div class="layui-row layui-col-space10">
                        <div class="layui-col-md3">
                            <!-- 填充内容 -->
                            <label class="layui-form-label">月份</label>
                            <div class="layui-input-inline">
                                <input type="text" name="month" id="month" autocomplete="off" class="layui-input" style="width: 150px">
                            </div>
                        </div>
                        <div class="layui-col-md3">
                            <!-- 填充内容 -->
                            <label class="layui-form-label">组名</label>
                            <div class="layui-input-inline">
                                <select type="text" name="groupName" id="groupName" autocomplete="off" class="layui-input" style="width: 150px"></select>
                            </div>
                        </div>
                        <div class="layui-col-md2">
                            <!-- 填充内容 -->
                            <label class="layui-form-label"></label>
                            <div class="layui-input-inline">
                                <button class="layui-btn" lay-submit lay-filter="searchBeat">搜索</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <table id="checkTable" lay-filter="checkTable"></table>

            <script type="text/html" id="toolbarTop">
                <div class="layui-btn-container">
                    <button class="layui-btn layui-btn-sm" lay-event="exportExcel">导出</button>
                </div>
            </script>
            <script type="text/html" id="barTop">
                <a class="layui-btn layui-btn-xs" lay-event="detail">详情</a>
            </script>

        </section>
    </section>
    <#include "fb_checkDetailUpdate.ftl">
    <@checkDetailUpdate></@checkDetailUpdate>
</section>

    <link rel="stylesheet" href="/css/layui.css" type="text/css">
    <link rel="stylesheet" href="/css/soulTable.css" type="text/css">
    <link rel="stylesheet" href="/css/selectize.bootstrap3.css" type="text/css">
    <#--<script src="/js/common/layui.js" type="text/javascript"></script>-->
    <script src="/js/common/layer.js"></script>
    <script src="/js/common/xm-select.js?t=${currentDate?c}" type="text/javascript"></script>
    <script src="/js/common/selectize.js" type="text/javascript"></script>
    <script src="/js/common/moment.min.js" type="text/javascript"></script>
    <script src="/js/factoryMsg/checkDetail.js?t=${currentDate?c}"></script>
</#macro>

<style>
    .layui-col-md3 .layui-form-label {
        width:90px
    }
    .layui-col-md6 .layui-form-label {
        width:90px
    }
    .layui-col-md12 .layui-form-label {
        width:90px
    }

    <style>
     button {
         background:rgb(45, 202, 147);
         opacity:0.86;
         color: white;
         font-family: PingFangSC-Semibold, sans-serif;
     }
    ::-webkit-scrollbar-track-piece {

        background-color:#f8f8f8;

    }

    ::-webkit-scrollbar {

        width:15px;

        height:15px;

    }

    ::-webkit-scrollbar-thumb {

        background-color:#dddddd;

        background-clip:padding-box;

        min-height:28px;

    }

    ::-webkit-scrollbar-thumb:hover {

        background-color:#bbb;

    }
</style>
</style>
