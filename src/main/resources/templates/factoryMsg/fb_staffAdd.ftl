<#macro entities>
    <div id="staffAdd" style="display: none;cursor:default;">
        <div>
            <form class="layui-form" action="">

            <table style="border-collapse:separate;width: 100%;margin-top:30px">
                <tr>
                    <td class="text">
                        <label class="control-label">姓名</label>
                    </td>
                    <td class="content">
                        <input id="employeeName" class="form-control" autocomplete="off">
                        <input id="employeeID" class="form-control" autocomplete="off" type="hidden">
                    </td>
                    <td class="text">
                        <label class="control-label">性别</label>
                    </td>
                    <td class="content">
                        <select id="gender" class="form-control" autocomplete="off">
                            <option value="">请选择性别</option>
                            <option value="男">男</option>
                            <option value="女">女</option>
                        </select>
                    </td>

                    <td class="text">
                        <label class="control-label">身份证号</label>
                    </td>
                    <td class="content">
                        <input id="IDCard" class="form-control" autocomplete="off">
                    </td>

                    <td class="text">
                        <label class="control-label">民族</label>
                    </td>
                    <td class="content">
                        <select id="nation" class="form-control" lay-search="" autocomplete="off">
                            <option value="">请选择民族</option>
                            <option value="汉族">汉族</option>
                            <option value="蒙古族">蒙古族</option>
                            <option value="回族">回族</option>
                            <option value="藏族">藏族</option>
                            <option value="维吾尔族">维吾尔族</option>
                            <option value="苗族">苗族</option>
                            <option value="彝族">彝族</option>
                            <option value="壮族">壮族</option>
                            <option value="布依族">布依族</option>
                            <option value="朝鲜族">朝鲜族</option>
                            <option value="满族">满族</option>
                            <option value="侗族">侗族</option>
                            <option value="瑶族">瑶族</option>
                            <option value="白族">白族</option>
                            <option value="土家族">土家族</option>
                            <option value="哈尼族">哈尼族</option>
                            <option value="哈萨克族">哈萨克族</option>
                            <option value="傣族">傣族</option>
                            <option value="黎族">黎族</option>
                            <option value="僳僳族">僳僳族</option>
                            <option value="佤族">佤族</option>
                            <option value="畲族">畲族</option>
                            <option value="高山族">高山族</option>
                            <option value="拉祜族">拉祜族</option>
                            <option value="水族">水族</option>
                            <option value="东乡族">东乡族</option>
                            <option value="纳西族">纳西族</option>
                            <option value="景颇族">景颇族</option>
                            <option value="柯尔克孜族">柯尔克孜族</option>
                            <option value="土族">土族</option>
                            <option value="达斡尔族">达斡尔族</option>
                            <option value="仫佬族">仫佬族</option>
                            <option value="羌族">羌族</option>
                            <option value="布朗族">布朗族</option>
                            <option value="撒拉族">撒拉族</option>
                            <option value="毛南族">毛南族</option>
                            <option value="仡佬族">仡佬族</option>
                            <option value="锡伯族">锡伯族</option>
                            <option value="阿昌族">阿昌族</option>
                            <option value="普米族">普米族</option>
                            <option value="塔吉克族">塔吉克族</option>
                            <option value="怒族">怒族</option>
                            <option value="乌孜别克族">乌孜别克族</option>
                            <option value="俄罗斯族">俄罗斯族</option>
                            <option value="鄂温克族">鄂温克族</option>
                            <option value="德昂族">德昂族</option>
                            <option value="保安族">保安族</option>
                            <option value="裕固族">裕固族</option>
                            <option value="京族">京族</option>
                            <option value="塔塔尔族">塔塔尔族</option>
                            <option value="独龙族">独龙族</option>
                            <option value="鄂伦春族">鄂伦春族</option>
                            <option value="赫哲族">赫哲族</option>
                            <option value="门巴族">门巴族</option>
                            <option value="珞巴族">珞巴族</option>
                            <option value="基诺族">基诺族</option>
                        </select>
                    </td>
                    <td class="text">
                        <label class="control-label">籍贯</label>
                    </td>
                    <td class="content">
                        <select id="province" class="form-control" autocomplete="off">
                            <option value="">请选择籍贯</option>
                            <option value="广东">广东</option>
                            <option value="湖南">湖南</option>
                            <option value="湖北">湖北</option>
                            <option value="四川">四川</option>
                            <option value="重庆">重庆</option>
                            <option value="江西">江西</option>
                            <option value="广西">广西</option>
                            <option value="河南">河南</option>
                            <option value="贵州">贵州</option>
                            <option value="云南">云南</option>
                            <option value="安徽">安徽</option>
                            <option value="河北">河北</option>
                            <option value="山西">山西</option>
                            <option value="山东">山东</option>
                            <option value="江苏">江苏</option>
                            <option value="浙江">浙江</option>
                            <option value="福建">福建</option>
                            <option value="陕西">陕西</option>
                            <option value="甘肃">甘肃</option>
                            <option value="青海">青海</option>
                            <option value="内蒙古">内蒙古</option>
                            <option value="海南">海南</option>
                            <option value="黑龙江">黑龙江</option>
                            <option value="吉林">吉林</option>
                            <option value="辽宁">辽宁</option>
                            <option value="宁夏">宁夏</option>
                            <option value="新疆">新疆</option>
                            <option value="西藏">西藏</option>
                            <option value="上海">上海</option>
                            <option value="北京">北京</option>
                            <option value="天津">天津</option>
                            <option value="香港">香港</option>
                            <option value="澳门">澳门</option>
                            <option value="台湾">台湾</option>

                        </select>
                    </td>

                </tr>
                <tr>
                    <td class="text">
                        <label class="control-label">住址</label>
                    </td>
                    <td class="content" colspan="3">
                        <input id="birthPlace" class="form-control" autocomplete="off">
                    </td>

                    <td class="text">
                        <label class="control-label">出生年月</label>
                    </td>
                    <td class="content">
                        <input id="birthday" class="form-control" autocomplete="off">
                    </td>
                    <td class="text">
                        <label class="control-label">最高学历</label>
                    </td>
                    <td class="content">
                        <select id="education" class="form-control" autocomplete="off">
                            <option value="">请选择学历</option>
                            <option value="小学">小学</option>
                            <option value="初中">初中</option>
                            <option value="中专">中专</option>
                            <option value="高中">高中</option>
                            <option value="专科">专科</option>
                            <option value="本科">本科</option>
                            <option value="硕士">硕士</option>
                            <option value="博士">博士</option>
                            <option value="其他">其他</option>
                        </select>
                    </td>
                    <td class="text">
                        <label class="control-label">工卡号</label>
                    </td>
                    <td class="content">
                        <input id="identifyCard" class="form-control" autocomplete="off" onkeyup="value=value.replace(/^(0+)|[^\d]+/g,'')">
                    </td>

                </tr>
                <tr>
                    <td class="text">
                        <label class="control-label">部门</label>
                    </td>
                    <td class="content">
                        <select id="department" class="form-control" autocomplete="off"></select>
                    </td>
                    <td class="text">
                        <label class="control-label">组别</label>
                    </td>
                    <td class="content">
                        <select id="groupName" class="form-control" autocomplete="off"></select>
                    </td>
                    <td class="text">
                        <label class="control-label">职位</label>
                    </td>
                    <td class="content">
                        <input id="position" class="form-control" autocomplete="off">
                    </td>
                    <td class="text">
                        <label class="control-label">工号</label>
                    </td>
                    <td class="content">
                        <input id="employeeNumber" class="form-control" autocomplete="off" onkeyup="value=value.replace(/^(0+)|[^\d]+/g,'')">
                    </td>
                    <td class="text" rowspan="4">
                        <div class="layui-upload">
                            <button type="button" class="layui-btn" id="uploadPic">上传照片</button>
                        </div>
                    </td>
                    <td class="content" rowspan="4">
                        <img class="layui-upload-img" id="imgUrl"
                             style="height: 150px;width:120px;border: 1px solid #a7a4a4;">
                    </td>
                </tr>
                <tr>
                    <td class="text">
                        <label class="control-label">管理岗</label>
                    </td>
                    <td class="content">
                        <select id="isManagement" class="form-control">
                            <option value="非管理岗">非管理岗</option>
                            <option value="管理岗">管理岗</option>
                        </select>
                    </td>
                    <td class="text">
                        <label class="control-label">车位</label>
                    </td>
                    <td class="content">
                        <select id="turner" class="form-control">
                            <option value="">请选择</option>
                            <option value="车位">车位</option>
                            <option value="非车位">非车位</option>
                        </select>
                    </td>
                    <td class="text">
                        <label class="control-label">电话</label>
                    </td>
                    <td class="content">
                        <input id="telephone" class="form-control" autocomplete="off">
                    </td>
                    <td class="text">
                        <label class="control-label">紧急联系人</label>
                    </td>
                    <td class="content">
                        <input id="emergencyContact" class="form-control" autocomplete="off">
                    </td>
                </tr>
                <tr>
                    <td class="text">
                        <label class="control-label">与本人关系</label>
                    </td>
                    <td class="content">
                        <input id="relation" class="form-control" autocomplete="off">
                    </td>
                    <td class="text">
                        <label class="control-label">联系人电话</label>
                    </td>
                    <td class="content">
                        <input id="emergencyPhone" class="form-control" autocomplete="off">
                    </td>
                    <td class="text">
                        <label class="control-label">银行卡号</label>
                    </td>
                    <td class="content">
                        <input id="bankCardNumber" class="form-control" autocomplete="off">
                    </td>
                    <td class="text">
                        <label class="control-label">开户行</label>
                    </td>
                    <td class="content">
                        <input id="cardOfBank" class="form-control" autocomplete="off">
                    </td>
                </tr>
                <tr>
                    <td class="text">
                        <label class="control-label">工资类别</label>
                    </td>
                    <td class="content">
                        <select id="salaryType" class="form-control" autocomplete="off">
                            <option value="">请选择类别</option>
                            <option value="时工">时工</option>
                            <option value="时薪">时薪</option>
                            <option value="计件">计件</option>
                        </select>
                    </td>
                    <td class="text">
                        <label class="control-label">入职时间</label>
                    </td>
                    <td class="content">
                        <input id="beginDate" class="form-control" autocomplete="off">
                    </td>
                    <td class="text">
                        <label id="positionStateLabel" class="control-label">在职状态</label>
                    </td>
                    <td class="content">
                        <div id="positionStateDiv">
                            <select id="positionState" class="form-control" autocomplete="off">
                                <option value="">请选择在职状态</option>
                                <option value="在职">在职</option>
                                <option value="离职">离职</option>
                                <option value="自离">自离</option>
                                <option value="辞退">辞退</option>
                            </select>
                        </div>
                    </td>
                    <td class="text">
                        <label id="leaveDateLabel" class="control-label">离职时间</label>
                    </td>
                    <td class="content">
                        <input id="leaveDate" class="form-control" autocomplete="off">
                    </td>
                </tr>
                <tr>
                    <td class="text">
                        <label class="control-label">合同开始</label>
                    </td>
                    <td class="content">
                        <input id="contractBegin" class="form-control" autocomplete="off">
                    </td>
                    <td class="text">
                        <label class="control-label">合同结束</label>
                    </td>
                    <td class="content">
                        <input id="contractEnd" class="form-control" autocomplete="off">
                    </td>
                    <td class="text">
                        <label class="control-label">婚姻状态</label>
                    </td>
                    <td class="content">
                        <select id="maritalState" class="form-control" autocomplete="off">
                            <option value="">请选择婚姻状态</option>
                            <option value="未婚">未婚</option>
                            <option value=已婚>已婚</option>
                        </select>
                    </td>
                    <td class="text">
                        <label class="control-label">角色</label>
                    </td>
                    <td class="content">
                        <select id="role" class="form-control" autocomplete="off">
                            <option value="role1">计件</option>
                            <option value="role2">派工</option>
                        </select>
                    </td>
                    <td></td><td></td>
                </tr>
            </table>
            </form>
        </div>
        <#--<div>-->
            <div class="col-md-12" style="margin-top: 10px" id="identifyIdcardDiv">
                <div class="col-md-5">
                    <section class="panel panel-default">
                        <div class="panel-body" style="text-align: center;">
                            <video id="video" width="100%" height="320px" autoplay="autoplay" controls></video>
                        </div>
                    </section>
                </div>
                <div class="col-md-5">
                    <section class="panel panel-default">
                        <div class="panel-body" style="text-align: center;">
                            <canvas id="canvas" width="480px" height="320px"></canvas>
                        </div>
                    </section>
                </div>
                <div class="col-md-2">
                    <button class="btn btn-s-lg" style="background-color:#009688;border-radius: 5px;color:white;font-family: PingFangSC-Semibold; margin-top: 10px" onclick="openMedia()">开启摄像头</button>
                    <button class="btn btn-s-lg" style="background-color:#009688;border-radius: 5px;color:white;font-family: PingFangSC-Semibold; margin-top: 10px" onclick="takePhoto()">识别身份证</button>
                    <button class="btn btn-s-lg" style="background-color:#009688;border-radius: 5px;color:white;font-family: PingFangSC-Semibold; margin-top: 10px" onclick="takeBankCardPhoto()">识别银行卡</button>
                    <button class="btn btn-s-lg" style="background-color:#009688;border-radius: 5px;color:white;font-family: PingFangSC-Semibold; margin-top: 10px" onclick="closeMedia()">关闭摄像头</button>
                </div>
            </div>

        <#--</div>-->
    </div>
</#macro>

<style>
    #staffAdd .text {
        width: 8%;
        text-align: center;
    }

    #staffAdd .content {
        width: 17%;
    }

    #staffAdd input {
        border-radius: 0;
        border-color: white;
    }

    #staffAdd select {
        border-color: white;
    }

    #staffAdd td {
        border: solid black;
        border-width: 0px 1px 1px 0px;
    }

    #staffAdd table {
        border: solid black;
        border-width: 1px 0px 0px 1px;
    }
</style>