<section class="panel panel-default">

    <div id="tableDiv">
        <table id="orderTable" class="table table-striped m-b-none ">
            <#if employeeList??>
            <thead>
                <tr bgcolor="#ffcb99" style="color: black;">
                    <th style="width: 45px;text-align: center">序号</th>
                    <th style="width: 60px;text-align: center">姓名</th>
                    <th style="width: 60px;text-align: center">工号</th>
                    <th style="width: 45px;text-align: center">性别</th>
                    <th style="width: 60px;text-align: center">部门</th>
                    <th style="width: 60px;text-align: center">职位</th>
                    <th style="width: 60px;text-align: center">车位</th>
                    <th style="width: 60px;text-align: center">组别</th>
                    <th style="width: 60px;text-align: center">工资类别</th>
                    <th style="width: 60px;text-align: center">工卡号</th>
                    <th style="width: 180px;text-align: center">身份证号</th>
                    <th style="width: 60px;text-align: center">民族</th>
                    <th style="width: 60px;text-align: center">籍贯</th>
                    <th style="width: 90px;text-align: center">出生年月</th>
                    <th style="width: 220px;text-align: center">家庭住址</th>
                    <th style="width: 60px;text-align: center">最高学历</th>
                    <th style="width: 90px;text-align: center">电话</th>
                    <th style="width: 90px;text-align: center">紧急联系人</th>
                    <th style="width: 90px;text-align: center">与本人关系</th>
                    <th style="width: 90px;text-align: center">紧急电话</th>
                    <th style="width: 90px;text-align: center">婚姻状态</th>
                    <th style="width: 90px;text-align: center">开户行</th>
                    <th style="width: 180px;text-align: center">银行卡号</th>
                    <th style="width: 60px;text-align: center">入职日期</th>
                    <th style="width: 45px;text-align: center">状态</th>
                    <th style="width: 60px;text-align: center">合同开始</th>
                    <th style="width: 60px;text-align: center">合同结束</th>
                    <th style="width: 60px;text-align: center">离职日期</th>
                    <th style="width: 120px;text-align: center">操作</th>
                </tr>
            </thead>
            <tbody>
                <#list employeeList as vo>
                <tr>
                    <td style="text-align: center">${vo_index+1}</td>
                    <td style="text-align: center"><#if vo.employeeName??>${vo.employeeName}</#if></td>
                    <td style="text-align: center"><#if vo.employeeNumber??>${vo.employeeNumber}</#if></td>
                    <td style="text-align: center"><#if vo.gender??>${vo.gender}</#if></td>
                    <td style="text-align: center"><#if vo.department??>${vo.department}</#if></td>
                    <td style="text-align: center"><#if vo.position??>${vo.position}</#if></td>
                    <td style="text-align: center"><#if vo.turner??>${vo.turner}</#if></td>
                    <td style="text-align: center"><#if vo.groupName??>${vo.groupName}</#if></td>
                    <td style="text-align: center"><#if vo.salaryType??>${vo.salaryType}</#if></td>
                    <td style="text-align: center"><#if vo.identifyCard??>${vo.identifyCard}</#if></td>
                    <td style="text-align: center"><#if vo.IDCard??>${vo.IDCard}</#if></td>
                    <td style="text-align: center"><#if vo.nation??>${vo.nation}</#if></td>
                    <td style="text-align: center"><#if vo.province??>${vo.province}</#if></td>
                    <td style="text-align: center"><#if vo.birthday??>${vo.birthday?date}</#if></td>
                    <td style="text-align: center"><#if vo.birthPlace??>${vo.birthPlace}</#if></td>
                    <td style="text-align: center"><#if vo.education??>${vo.education}</#if></td>
                    <td style="text-align: center"><#if vo.telephone??>${vo.telephone}</#if></td>
                    <td style="text-align: center"><#if vo.emergencyContact??>${vo.emergencyContact}</#if></td>
                    <td style="text-align: center"><#if vo.relation??>${vo.relation}</#if></td>
                    <td style="text-align: center"><#if vo.emergencyPhone??>${vo.emergencyPhone}</#if></td>
                    <td style="text-align: center"><#if vo.maritalState??>${vo.maritalState}</#if></td>
                    <td style="text-align: center"><#if vo.cardOfBank??>${vo.cardOfBank}</#if></td>
                    <td style="text-align: center"><#if vo.bankCardNumber??>${vo.bankCardNumber}</#if></td>
                    <td style="text-align: center"><#if vo.beginDate??>${vo.beginDate?date}</#if></td>
                    <td style="text-align: center"><#if vo.positionState??>${vo.positionState}</#if></td>
                    <td style="text-align: center"><#if vo.contractBegin??>${vo.contractBegin?date}</#if></td>
                    <td style="text-align: center"><#if vo.contractEnd??>${vo.contractEnd?date}</#if></td>
                    <td style="text-align: center"><#if vo.leaveDate??>${vo.leaveDate?date}</#if></td>
                    <td style="text-align: center"><a href="#" style="color:#3e8eea" onclick="detail('${vo.employeeID?c}')">详情</a>&nbsp;&nbsp;&nbsp;<a href="#" style="color:#3e8eea" onclick="update('${vo.employeeID?c}')">修改</a>&nbsp;&nbsp;&nbsp;<a href="#" style="color:red" onclick="del('${vo.employeeID?c}')">删除</a></td>
                </tr>
                </#list>
            </tbody>
            <tfoot>
            <tr>
                <th colspan="6" style="text-align:right"></th>
            </tr>
            </tfoot>
            <#else>
            <h4 style="text-align: center">无该状态的数据</h4>
            </#if>
        </table>
    </div>

</section>

<style>
    td {
        word-break:break-all;
    }

</style>