<#macro entities>
<#--<section class="panel panel-default">-->
    <div id="editPro" style="display: none;cursor:default">
        <div style="overflow-x: auto;overflow-y:auto;">
            <div style="text-align: right;margin-bottom:20px">
                <a id="editNo" href="#" style="font-size:20px"><i class="fa fa-times icon" style="color: rgb(182,182,182)"></i></a>
            </div>
            <input type="text" hidden id="customerID">
            <table style="border-collapse:separate;border-spacing: 10px;width: 100%;">
                <tr>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">用户名</label>
                    </td>
                    <td>
                        <input id="userName" class="form-control" style="width: 100%;margin-bottom: 15px;border-top: none;border-right: none;border-left: none;">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">登陆密码</label>
                    </td>
                    <td>
                        <input id="passWord" class="form-control" style="margin-bottom: 15px;width: 100%;border-top: none;border-right: none;border-left: none;">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">职务</label>
                    </td>
                    <td>
                        <select id="role" class="form-control" style="margin-bottom: 15px;width: 100%;border-top: none;border-right: none;border-left: none;">
                            <option value=""></option>
                            <option value="root">管理员</option>
                            <option value="role1">裁床打菲</option>
                            <option value="role2">跟单业务</option>
                            <option value="role3">面料业务</option>
                            <option value="role4">人事行政助理</option>
                            <option value="role5">IE主管</option>
                            <option value="role6">裁片衣胚</option>
                            <option value="role7">吊挂查数</option>
                            <option value="role8">车间文员</option>
                            <option value="role9">分厂</option>
                            <option value="role10">财务管理</option>
                            <option value="role11">面料</option>
                            <option value="role12">主管</option>
                            <option value="role13">计件工</option>
                            <option value="role14">包装打菲</option>
                            <option value="role15">IQC</option>
                            <option value="role16">板房</option>
                            <option value="role17">辅料</option>
                            <option value="role18">辅料仓</option>
                            <option value="role19">外发负责</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">电话</label>
                    </td>
                    <td>
                        <input id="phoneNumber" class="form-control" style="margin-bottom: 15px;width: 100%;border-top: none;border-right: none;border-left: none;">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">身份证号</label>
                    </td>
                    <td>
                        <input id="userCardID" class="form-control" style="margin-bottom: 15px;width: 100%;border-top: none;border-right: none;border-left: none;">
                    </td>
                </tr>
            </table>
        </div>
        <div class="col-sm-12" style="padding-top: 10px">
            <button id="editYes" class="btn btn-s-lg" style="border-radius: 5px;"  style="text-align: center;">保存</button>
            <#--<button id="editNo" class="btn btn-danger btn-rounded btn-small" style="text-align: center;">取消</button>-->
        </div>
    </div>
<#--</section>-->
</#macro>