<#macro sizeInput>
    <div id="sizeInput" style="display: none;cursor:default;">
        <form class="layui-form" style="margin: 5px;padding-top: 0px;" lay-filter="sizeNameAdd">
            <table>
                <tr>
                    <td style="text-align: right;margin-bottom: 15px;margin-top: 15px;width: 100px">
                        <label class="layui-form-label">尺码组</label>
                    </td>
                    <td style="margin-bottom: 15px;margin-top: 15px">
                        <input type="text" name="sizeGroup" id="sizeGroup" class="layui-input">
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right;margin-bottom: 15px;width: 100px">
                        <label class="layui-form-label">尺码</label>
                    </td>
                    <td style="margin-bottom: 15px;">
                        <input type="text" name="sizeName" id="sizeName" class="layui-input">
                    </td>
                </tr>
            </table>
        </form>
    </div>
</#macro>

<style>
    .layui-col-md3 .layui-form-label {
        width:90px
    }
    .layui-col-md6 .layui-form-label {
        width:90px
    }
    .layui-col-md12 .layui-form-label {
        width:90px
    }
</style>