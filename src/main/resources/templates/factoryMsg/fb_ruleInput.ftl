<#macro ruleInput>
    <div id="ruleInput" style="display: none;cursor:default;">
        <form class="layui-form" style="margin: 5px;padding-top: 0px;" lay-filter="ruleAdd">
            <table>
                <tr>
                    <td style="text-align: right;margin-bottom: 15px;margin-top: 15px;width: 100px">
                        <label class="layui-form-label">名称</label>
                    </td>
                    <td style="margin-bottom: 15px;margin-top: 15px">
                        <select type="text" name="workName" id="workName" class="layui-input">
                            <option value="in1">上午上班</option>
                            <option value="out1">上午下班</option>
                            <option value="in2">下午上班</option>
                            <option value="out2">下午下班</option>
                            <option value="in3">晚上上班</option>
                            <option value="out3">晚上下班</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right;margin-bottom: 15px;width: 100px">
                        <label class="layui-form-label">时间</label>
                    </td>
                    <td style="margin-bottom: 15px;">
                        <input type="text" name="checkTime" id="checkTime" class="layui-input" placeholder="HH:mm:ss">
                    </td>
                </tr>
            </table>
        </form>
    </div>
</#macro>

<style>
    .layui-col-md3 .layui-form-label {
        width:90px
    }
    .layui-col-md6 .layui-form-label {
        width:90px
    }
    .layui-col-md12 .layui-form-label {
        width:90px
    }
</style>