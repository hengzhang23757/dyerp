<#macro search>
<section id="content">
    <section class="vbox">
        <section class="scrollable">
            <table id="staffTable" lay-filter="staffTable"></table>
            <script type="text/html" id="toolbarTop">
                <div class="layui-btn-container">
                    <button class="layui-btn layui-btn-sm" lay-event="clearFilter">清除所有筛选</button>
                    <button class="layui-btn layui-btn-sm" lay-event="add">添加员工</button>
                    <button class="layui-btn layui-btn-sm" lay-event="onState">在职</button>
                    <button class="layui-btn layui-btn-sm" lay-event="offState">离职</button>
                    <button class="layui-btn layui-btn-sm" lay-event="exportExcel">导出</button>
                </div>
            </script>

            <script type="text/html" id="barTop">
                <a class="layui-btn layui-btn-xs" lay-event="update">修改</a>
                <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
            </script>
        </section>
    </section>
    <#include "fb_staffAdd.ftl">
    <@entities></@entities>

</section>

<link rel="stylesheet" href="/css/b.tabs.css" type="text/css">
<link rel="stylesheet" href="/css/bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="/css/layui.css" type="text/css">
<link rel="stylesheet" href="/css/soulTable.css" type="text/css">
<link rel="stylesheet" href="/css/viewer.min.css" type="text/css">
<script src="/js/common/layui.js" type="text/javascript" ></script>
<script src="/js/common/layer.js?t=${currentDate?c}"></script>
<script src="/js/common/laydate.js" type="text/javascript" ></script>
<script src="/js/common/viewer.min.js" type="text/javascript" ></script>
<script src="/js/common/jquery.blockUI.js" type="text/javascript" ></script>
<script src="/js/common/moment.min.js" type="text/javascript"></script>
<script src="/js/factoryMsg/staff.js?t=${currentDate?c}"></script>

</#macro>

<style>
    button {
        background:rgb(45, 202, 147);
        opacity:0.86;
        color: white;
        font-family: PingFangSC-Semibold, sans-serif;
    }
    <style>
     button {
         background:rgb(45, 202, 147);
         opacity:0.86;
         color: white;
         font-family: PingFangSC-Semibold, sans-serif;
     }
    ::-webkit-scrollbar-track-piece {

        background-color:#f8f8f8;

    }

    ::-webkit-scrollbar {

        width:15px;

        height:15px;

    }

    ::-webkit-scrollbar-thumb {

        background-color:#dddddd;

        background-clip:padding-box;

        min-height:28px;

    }

    ::-webkit-scrollbar-thumb:hover {

        background-color:#bbb;

    }
</style>
</style>
