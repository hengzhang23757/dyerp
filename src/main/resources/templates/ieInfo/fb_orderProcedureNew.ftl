<#macro search>
    <section id="content">
        <section class="vbox">
            <section class="scrollable padder water-mark-visible-ff">

                <div id="mainFrameTabs" style="height:90%">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active noclose" id="orderListTab"><a href="#orderListDiv" data-toggle="tab">订单工序</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="orderListDiv" style="background-color: #f7f7f7;overflow-y:auto">
                            <div class="col-md-12">
                                <section class="scrollable">
                                    <div class="layui-row layui-form" style="margin-top: 10px">
                                        <table>
                                            <tr>
                                                <td><label class="layui-form-label">款号</label></td>
                                                <td><input type="text" id="orderName" class="layui-input" autocomplete="off"></td>
                                                <td><label class="layui-form-label">单号</label></td>
                                                <td><input type="text" id="clothesVersionNumber" class="layui-input" autocomplete="off"></td>
                                                <td><label class="layui-form-label">客户名</label></td>
                                                <td><input type="text" id="customerName" class="layui-input" autocomplete="off"></td>
                                                <td><label class="layui-form-label">款式描述</label></td>
                                                <td><input type="text" id="styleDescription" class="layui-input" autocomplete="off"></td>
                                                <td><label class="layui-form-label">开单日期</label></td>
                                                <td><input type="text" id="beginDate" class="layui-input" autocomplete="off"></td>
                                                <td><label class="layui-form-label">状态</label></td>
                                                <td><select type="text" id="procedureState" class="layui-input" autocomplete="off">
                                                        <option value="">全部</option>
                                                        <option value="0">未提交</option>
                                                        <option value="1">已提交</option>
                                                        <option value="2">通过</option>
                                                        <option value="3">未通过</option>
                                                    </select></td>
                                                <td><label class="layui-form-label"></label></td>
                                                <td><button class="layui-btn mgl-20" lay-submit="" lay-filter="search" ><i class="layui-icon">&#xe615;</i>查询</button></td>
                                            </tr>
                                        </table>
                                    </div>
                                    <table id="orderProcedureTable" lay-filter="orderProcedureTable"></table>
                                    <script type="text/html" id="toolbarTop">
                                        <div class="layui-btn-container">
                                            <button class="layui-btn layui-btn-sm" lay-event="addOrderProcedure">添加订单工序</button>
                                        </div>
                                    </script>
                                </section>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <#include "fb_orderProcedureAuditNew.ftl">
                    <@entities></@entities>
                </div>
            </section>
        </section>
        <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen"
           data-target="#nav"></a>
    </section>
    <aside class="bg-light lter b-l aside-md hide" id="notes">
        <div class="wrapper">Notification</div>
    </aside>
    </section>
    <link rel="stylesheet" href="/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="/css/b.tabs.css" type="text/css">
    <link rel="stylesheet" href="/css/layui.css" type="text/css">
    <link rel="stylesheet" href="/css/soulTable.css" type="text/css">

    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.bootstrap.min.css">
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="/js/common/jquery.blockUI.js" type="text/javascript" ></script>
    <script src="/js/common/b.tabs.js?t=${currentDate?c}"></script>
    <script src="/js/common/layui.all.js" type="text/javascript" ></script>
    <script src="/js/common/layer.js"></script>
    <script src="/js/common/xm-select.js?t=${currentDate?c}" type="text/javascript"></script>
<#--<script src="/js/common/tableFilter.js" type="text/javascript" ></script>-->
    <script src="/js/ieInfo/orderProcedureNew.js?t=${currentDate?c}"></script>

</#macro>

<style>
    button {
        background:rgb(45, 202, 147);
        opacity:0.86;
        color: white;
        font-family: PingFangSC-Semibold, sans-serif;
    }
    .layui-form-label {
        width:100px !important;
    }
    ::-webkit-scrollbar-track-piece {

        background-color:#f8f8f8;

    }

    ::-webkit-scrollbar {

        width:15px;

        height:15px;

    }

    ::-webkit-scrollbar-thumb {

        background-color:#dddddd;

        background-clip:padding-box;

        min-height:28px;

    }

    ::-webkit-scrollbar-thumb:hover {

        background-color:#bbb;

    }

</style>
