<#macro search>
<section id="content">
    <section class="vbox">
        <section class="scrollable">
            <div class="layui-row">
                <form class="layui-form" style="margin: 0 auto;padding-top: 10px;" lay-filter="baseInfo">
                    <table>
                        <tr>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label" style="width: 120px;">开始日期</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" id="from" placeholder="开始日期" autocomplete="off" class="layui-input">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label" style="width: 120px;">结束日期</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" id="to" placeholder="结束日期" autocomplete="off" class="layui-input">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label" style="width: 120px;">单号</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" name="clothesVersionNumber" autocomplete="off" id="clothesVersionNumber" placeholder="请输入单号" class="layui-input">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label" style="width: 120px;">款号</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" autocomplete="off" id="orderName" placeholder="请输入款号" class="layui-input">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label" style="width: 120px;">状态</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <select type="text" id="procedureState" class="layui-input" autocomplete="off">
                                    <option value="">全部</option>
                                    <option value="0">未提交</option>
                                    <option value="1">已提交</option>
                                    <option value="2">通过</option>
                                    <option value="3">未通过</option>
                                </select>
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label"></label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <div class="layui-input-inline">
                                    <button class="layui-btn" lay-submit lay-filter="searchBeat">搜索</button>
                                </div>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>

            <table id="reportTable" lay-filter="reportTable"></table>

            <script type="text/html" id="toolbarTop">
                <div class="layui-btn-container">
                    <button class="layui-btn layui-btn-sm" lay-event="change">批量提交审核</button>
                    <button class="layui-btn layui-btn-sm" lay-event="exportExcel">导出</button>
                </div>
            </script>

        </section>
    </section>
</section>

    <link rel="stylesheet" href="/css/layui.css" type="text/css">
    <link rel="stylesheet" href="/css/soulTable.css" type="text/css">
    <link rel="stylesheet" href="/css/selectize.bootstrap3.css" type="text/css">
    <#--<script src="/js/common/layui.js" type="text/javascript"></script>-->
    <script src="/js/common/layer.js"></script>
    <script src="/js/common/xm-select.js?t=${currentDate?c}" type="text/javascript"></script>
    <script src="/js/common/selectize.js" type="text/javascript"></script>
    <script src="/js/common/moment.min.js" type="text/javascript"></script>
    <script src="/js/ieInfo/procedureReview.js?t=${currentDate?c}"></script>
</#macro>

<style>
    .layui-col-md3 .layui-form-label {
        width:90px
    }
    .layui-col-md6 .layui-form-label {
        width:90px
    }
    .layui-col-md12 .layui-form-label {
        width:90px
    }

    ::-webkit-scrollbar-track-piece {

        background-color:#f8f8f8;

    }

    ::-webkit-scrollbar {

        width:15px;

        height:15px;

    }

    ::-webkit-scrollbar-thumb {

        background-color:#dddddd;

        background-clip:padding-box;

        min-height:28px;

    }

    ::-webkit-scrollbar-thumb:hover {

        background-color:#bbb;

    }

</style>
