<#macro entities>
<#--<section class="panel panel-default">-->
    <div id="editPro" style="display: none;cursor:default">
        <div>
            <div style="text-align: right;margin-bottom:20px">
                <a id="editNo" href="#" style="font-size:20px"><i class="fa fa-times icon" style="color: rgb(182,182,182)"></i></a>
            </div>
            <div style="overflow-x: auto;overflow-y:auto;height: 300px">
                <table style="border-collapse:separate;border-spacing: 10px;width: 100%;">
                    <tr>
                        <td style="width:5%">
                            <label class="control-label" style="margin-bottom: 15px;text-align: right;">单号</label>
                        </td>
                        <td style="width:15%">
                            <input id="clothesVersionNumber" autocomplete="off" class="form-control" placeholder="请输入单号" style="width: 200px;margin-bottom: 15px;border-top: none;border-right: none;border-left: none;">
                        </td>

                        <td style="width:5%">
                            <label class="control-label" style="margin-bottom: 15px;text-align: right;">款号</label>
                        </td>
                        <td colspan="4">
                            <input id="orderName" class="form-control" placeholder="请输入单号" style="width: 200px;margin-bottom: 15px;border-top: none;border-right: none;border-left: none;">
                        </td>
                    </tr>
                    <tr name="partDiv">
                        <td>
                            <label class="control-label" style="margin-bottom: 15px;text-align: right;">工序</label>
                        </td>
                        <td>
                            <select name="procedureInfo" class="form-control" style="margin-bottom: 15px;width: 200px;border-top: none;border-right: none;border-left: none;"></select>
                        </td>
                        <td>
                            <label class="control-label" style="margin-bottom: 15px;text-align: right;">颜色</label>
                        </td>
                        <td style="width:15%">
                            <div id="colorNameSel0"></div>
                        </td>
                        <td style="width:5%">
                            <label class="control-label" style="margin-bottom: 15px;text-align: right;">尺码</label>
                        </td>
                        <td style="width:15%">
                            <div id="sizeNameSel0"></div>
                        </td>
                        <td style="width:10%">
                            <form class="form-inline">
                                <button class="btn" style="margin-bottom: 15px;outline:none;border-radius: 5px;color: white;" onclick="addPart(this)"><i class="fa fa-plus"></i></button>
                                <button class="btn" style="margin-bottom: 15px;outline:none;border-radius: 5px;color: white;;display: none;background-color: rgb(236, 108, 98);" onclick="delPart(this)"><i class="fa fa-minus"></i></button>
                            </form>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="col-sm-12" style="padding-top: 10px">
            <button id="editYes" class="btn btn-s-lg" style="border-radius: 5px;"  style="text-align: center;">保存</button>
            <#--<button id="editNo" class="btn btn-danger btn-rounded btn-small" style="text-align: center;">取消</button>-->
        </div>
    </div>
<#--</section>-->
</#macro>