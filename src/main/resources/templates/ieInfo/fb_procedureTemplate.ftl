<#macro search>
<section id="content">
    <section class="vbox">
        <section class="scrollable">
            <div class="layui-row layui-form" style="margin-top: 10px">
                <table>
                    <tr>
                        <td><label class="layui-form-label">工序号</label></td><td><input type="text" name="procedureNumber" id="searchProcedureNumber" class="layui-input" placeholder="请输入工序号"></td>
                        <td><label class="layui-form-label">工序代码</label></td><td><input type="text" name="procedureCode" id="searchProcedureCode" class="layui-input" placeholder="输入工序代码"></td>
                        <td><label class="layui-form-label">工序名</label></td><td><input type="text" name="procedureName" id="searchProcedureName" class="layui-input" placeholder="请输入关键字"></td>
                        <td><label class="layui-form-label">工序描述</label></td><td><input type="text" name="procedureDescription" id="searchProcedureDescription" class="layui-input" placeholder="请输入关键字"></td>
                        <td><label class="layui-form-label">码段</label></td><td><input type="text" name="segment" id="searchSegment" class="layui-input" placeholder="请输入关键字"></td>
                        <td><label class="layui-form-label">备注</label></td><td><input type="text" name="remark" id="searchRemark" class="layui-input" placeholder="请输入关键字"></td>
                        <td><label class="layui-form-label"></label></td><td><button class="layui-btn mgl-20" lay-submit="" lay-filter="search"><i class="layui-icon">&#xe615;</i>查询</button></td>
                    </tr>
                </table>
            </div>
            <table id="procedureTemplateTable" lay-filter="procedureTemplateTable"></table>
            <script type="text/html" id="toolbarTop">
                <div class="layui-btn-container">
                    <button class="layui-btn layui-btn-sm" lay-event="refresh">刷新</button>
                    <button class="layui-btn layui-btn-sm" lay-event="clearFilter">清除所有筛选</button>
                    <button class="layui-btn layui-btn-sm" lay-event="addBatch">添加</button>
                    <button class="layui-btn layui-btn-sm" lay-event="batchDelete">删除</button>
                    <button class="layui-btn layui-btn-sm" lay-event="exportExcel">导出</button>
                </div>
            </script>
            <script type="text/html" id="barTop">
                <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
                <a class="layui-btn layui-btn-xs" lay-event="update">修改</a>
                <a class="layui-btn layui-btn-normal layui-btn-xs" lay-event="copy">新增</a>
            </script>
        </section>
    </section>
    <#include "fb_procedureTemplateUpdate.ftl">
    <@procedureUpdate></@procedureUpdate>
    <#include "fb_procedureTemplateAdd.ftl">
    <@procedureAdd></@procedureAdd>
</section>

<link rel="stylesheet" href="/css/layui.css" type="text/css">
<link rel="stylesheet" href="/css/soulTable.css" type="text/css">
<link rel="stylesheet" href="/css/handsontable.full.min.css" type="text/css">
<script src="/js/common/moment.min.js" type="text/javascript"></script>
<script src="/js/common/laydate.js" type="text/javascript" ></script>
<script src="/js/common/jquery.blockUI.js" type="text/javascript" ></script>
<script src="/js/ieInfo/procedureTemplate.js?t=${currentDate?c}"></script>
<script src="/js/common/handsontable.full.min.js" type="text/javascript" ></script>
<script src="/js/common/zh-CN.js" type="text/javascript" ></script>


</#macro>

<style>
    button {
        background:rgb(45, 202, 147);
        opacity:0.86;
        color: white;
        font-family: PingFangSC-Semibold, sans-serif;
    }
    .layui-form-label {
        width:90px !important;
    }
    ::-webkit-scrollbar-track-piece {

        background-color:#f8f8f8;

    }

    ::-webkit-scrollbar {

        width:15px;

        height:15px;

    }

    ::-webkit-scrollbar-thumb {

        background-color:#dddddd;

        background-clip:padding-box;

        min-height:28px;

    }

    ::-webkit-scrollbar-thumb:hover {

        background-color:#bbb;

    }
</style>
