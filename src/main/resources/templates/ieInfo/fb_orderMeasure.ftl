<#macro search>
<section id="content">
    <section class="vbox">
        <section class="scrollable">

            <form class="layui-form" style="margin: 0 auto;padding-top: 3px;" lay-filter="formBaseInfo">
                <table>
                    <tr>
                        <td style="text-align: right;margin-bottom: 15px;">
                            <label class="layui-form-label" style="width: 100px">款号</label>
                        </td>
                        <td style="margin-bottom: 15px;">
                            <input type="text" id="clothesVersionNumber" autocomplete="off" class="layui-input">
                        </td>
                        <td style="text-align: right;margin-bottom: 15px;">
                            <label class="layui-form-label" style="width: 100px">单号</label>
                        </td>
                        <td style="margin-bottom: 15px;">
                            <input type="text" id="orderName" autocomplete="off" class="layui-input">
                        </td>
                        <td style="text-align: right;margin-bottom: 15px;">
                            <label class="layui-form-label"></label>
                        </td>
                        <td style="margin-bottom: 15px;">
                            <div class="layui-input-inline">
                                <button class="layui-btn" lay-submit lay-filter="searchBeat">搜索</button>
                            </div>
                        </td>
                    </tr>
                </table>
            </form>

            <table id="dataTable" lay-filter="dataTable"></table>

            <script type="text/html" id="toolbarTop">
                <div class="layui-btn-container">
                    <button class="layui-btn layui-btn-sm" lay-event="add">添加</button>
                </div>
            </script>

            <script type="text/html" id="toolbar">
                <div class="layui-btn-container">
                    <button class="layui-btn layui-btn-sm" lay-event="copy">复制</button>
                    <button class="layui-btn layui-btn-sm" lay-event="print">打印</button>
                </div>
            </script>

            <div id="sizeDiv" style="width: 100%; height: 85vh;background-color: rgb(223 219 219 / 24%);" class="layui-row layui-col-space10" hidden>
                <div class="layui-fluid">
                    <form class="layui-form" style="margin: 0 auto;padding-top: 3px;" lay-filter="formBaseInfo">
                        <table>
                            <tr>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label" style="width: 100px">款号</label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <input type="text" id="clothesVersionNumberTwo" autocomplete="off" class="layui-input">
                                </td>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label" style="width: 100px">单号</label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <input type="text" id="orderNameTwo" autocomplete="off" class="layui-input">
                                </td>
                                <td style="text-align: right;margin-bottom: 15px;">
                                    <label class="layui-form-label"></label>
                                </td>
                                <td style="margin-bottom: 15px;">
                                    <div class="layui-input-inline">
                                        <button class="layui-btn" lay-submit lay-filter="searchBeatSize">加载</button>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </form>
                    <div class="layui-row layui-col-space8">
                        <div class="layui-col-md8" style="height: 500px;">
                            <div class="layui-card">
                                <div class="layui-card-header" style="font-weight: bolder; font-size: large; color: #1E9FFF">尺寸</div>
                                <div class="layui-card-body layui-row layui-col-space10">
                                    <div class="layui-col-md12">
                                        <table id="orderMeasureItemTable" lay-filter="orderMeasureItemTable"></table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="layui-col-md4" style="height: 500px;">
                            <div class="layui-card">
                                <div class="layui-card-header" style="font-weight: bolder; font-size: large; color: #1E9FFF">图片</div>
                                <div class="layui-card-body layui-row layui-col-space10">
                                    <form class="layui-form" style="margin: 0 auto;max-width:100%;padding-top: 10px;">
                                        <div class="layui-col-md12" style="width: 100%;padding-top: 5px">
                                            <div class="layui-tab layui-tab-card">
                                                <textarea id="sizeDetailTextarea"></textarea>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
    </section>
</section>

    <link rel="stylesheet" href="/css/layui.css" type="text/css">
    <link rel="stylesheet" href="/css/soulTable.css" type="text/css">
    <link rel="stylesheet" href="/css/selectize.bootstrap3.css" type="text/css">
    <script src="/js/common/layer.js"></script>
    <script src="/js/common/xm-select.js?t=${currentDate?c}" type="text/javascript"></script>
    <script src="/js/common/selectize.js" type="text/javascript"></script>
    <script src="/js/common/moment.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/common/tinymce/tinymce.min.js"></script>
    <script type="text/javascript" src="/js/common/tinymce/zh_CN.js"></script>
    <script src="/js/ieInfo/orderMeasure.js?t=${currentDate?c}"></script>
</#macro>

<style>

   .tox-tinymce-aux {
        z-index: 0
    }

    .layui-col-md3 .layui-form-label {
        width:90px
    }
    .layui-col-md6 .layui-form-label {
        width:90px
    }
    .layui-col-md12 .layui-form-label {
        width:90px
    }

    ::-webkit-scrollbar-track-piece {

        background-color:#f8f8f8;

    }

    ::-webkit-scrollbar {

        width:15px;

        height:15px;

    }

    ::-webkit-scrollbar-thumb {

        background-color:#dddddd;

        background-clip:padding-box;

        min-height:28px;

    }

    ::-webkit-scrollbar-thumb:hover {

        background-color:#bbb;

    }

</style>
