<!DOCTYPE html>
<html>
<head>
    <#include "../feedback/fb_script.ftl">
    <@script> </@script>
    <link rel="stylesheet" href="/css/b.tabs.css" type="text/css">
    <link rel="stylesheet" href="/css/layui.css" type="text/css">
    <link rel="stylesheet" href="/css/soulTable.css" type="text/css">

    <script src="/js/common/b.tabs.js?t=${currentDate?c}"></script>
    <script src="/js/common/layui.all.js" type="text/javascript" ></script>
    <script src="/js/common/layer.js"></script>
    <script src="/js/common/xm-select.js?t=${currentDate?c}" type="text/javascript"></script>
    <script src="/js/ieInfo/orderProcedureDetailNew.js?t=${currentDate?c}"></script>
</head>

<body>
<input  type="hidden" value="${basePath}"  id="basePath"/>
<input  type="hidden" value="${orderName}"  id="orderName"/>
<input  type="hidden" value="${type}"  id="type"/>
<div class="col-md-12" style="padding-top: 20px;padding-bottom:10px;margin-left: 10px">
</div>
</body>
</html>
<style>
    ::-webkit-scrollbar-track-piece {

        background-color:#f8f8f8;

    }

    ::-webkit-scrollbar {

        width:15px;

        height:15px;

    }

    ::-webkit-scrollbar-thumb {

        background-color:#dddddd;

        background-clip:padding-box;

        min-height:28px;

    }

    ::-webkit-scrollbar-thumb:hover {

        background-color:#bbb;

    }
</style>
