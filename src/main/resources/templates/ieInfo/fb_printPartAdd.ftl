<#macro entities>
    <div id="editPro" style="display: none;cursor:default">
        <div>
            <div style="text-align: right;margin-bottom:20px">
                <a id="editNo" href="#" style="font-size:20px"><i class="fa fa-times icon" style="color: rgb(182,182,182)"></i></a>
            </div>
            <table style="border-collapse:separate;border-spacing: 10px;width: 100%;" id="printPartDiv">
                <tr>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">单号</label>
                    </td>
                    <td>
                        <input id="clothesVersionNumber" autocomplete="off" class="form-control" placeholder="请输入单号" style="width: 100%;margin-bottom: 15px;border-top: none;border-right: none;border-left: none;">
                    </td>

                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">订单号</label>
                    </td>
                    <td>
                        <input id="orderName" class="form-control" autocomplete="off" placeholder="请输入款号" style="width: 100%;margin-bottom: 15px;border-top: none;border-right: none;border-left: none;">
                    </td>
                </tr>
                <tr name="partDiv">
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">部位</label>
                    </td>
                    <td>
                        <select name="printPart" class="form-control" style="margin-bottom: 15px;width: 100%;border-top: none;border-right: none;border-left: none;">
                            <option value="">请选择部位</option>
                            <option value="主身">主身</option>
                            <option value="罗纹">罗纹</option>
                            <option value="里布">里布</option>
                            <option value="袋布">袋布</option>
                            <option value="扁机">扁机</option>
                            <option value="帽子">帽子</option>
                            <option value="士啤">士啤</option>
                            <option value="罗纹1">罗纹1</option>
                            <option value="罗纹2">罗纹2</option>
                            <option value="罗纹3">罗纹3</option>
                            <option value="里布1">里布1</option>
                            <option value="里布2">里布2</option>
                            <option value="里布3">里布3</option>
                            <option value="袋布1">袋布1</option>
                            <option value="袋布2">袋布2</option>
                            <option value="袋布3">袋布3</option>
                            <option value="扁机1">扁机1</option>
                            <option value="扁机2">扁机2</option>
                            <option value="扁机3">扁机3</option>
                            <option value="撞色">撞色</option>
                            <option value="其他">其他</option>
                            <option value="其他1">其他1</option>
                            <option value="撞色">撞色</option>
                            <option value="撞色1">撞色1</option>
                            <option value="撞色2">撞色2</option>
                            <option value="帽子1">帽子1</option>
                            <option value="帽子2">帽子2</option>
                        </select>
                    </td>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">名称</label>
                    </td>
                    <td style="text-align: left;">
                        <form class="form-inline">
                            <input name="printName" class="form-control" style="margin-bottom: 15px;width: 85%;border-top: none;border-right: none;border-left: none;">
                            <button name="addPartBtn" class="btn" style="margin-bottom: 15px;outline:none;border-radius: 5px;color: white;" onclick="addPart(this)"><i class="fa fa-plus"></i></button>
                            <button name="delPartBtn" class="btn" style="margin-bottom: 15px;outline:none;border-radius: 5px;color: white;;display: none;background-color: rgb(236, 108, 98);" onclick="delPart(this)"><i class="fa fa-minus"></i></button>
                        </form>
                    </td>
                </tr>
            </table>
        </div>
        <div class="col-sm-12" style="padding-top: 10px">
            <button id="editYes" class="btn btn-s-lg" style="border-radius: 5px;"  style="text-align: center;">保存</button>
        </div>
    </div>
</#macro>