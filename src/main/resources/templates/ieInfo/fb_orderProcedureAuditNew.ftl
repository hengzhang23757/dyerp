<#macro entities>
    <div id="proceduteAuditDiv" style="display: none;cursor:default">
        <div style="overflow-x: hidden;overflow-y:auto;">
            <table id="orderProcedureAuditTable" class="table table-striped table-hover" style="width:100%">
            </table>
        </div>
    </div>
</#macro>