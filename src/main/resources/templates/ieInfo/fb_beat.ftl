<#macro search>
<section id="content">
    <section class="vbox">
        <section class="scrollable">
            <div class="layui-row">
                <form class="layui-form" style="margin: 0 auto;padding-top: 3px;" lay-filter="baseInfo">
                    <div class="layui-row layui-col-space10">
                        <div class="layui-col-md3">
                            <!-- 填充内容 -->
                            <label class="layui-form-label">单号</label>
                            <div class="layui-input-inline">
                                <input type="text" name="clothesVersionNumber" id="clothesVersionNumber" autocomplete="off" placeholder="请输入单号" class="layui-input" style="width:200px">
                            </div>
                        </div>
                        <div class="layui-col-md3">
                            <!-- 填充内容 -->
                            <label class="layui-form-label">款号</label>
                            <div class="layui-input-inline">
                                <input name="orderName" id="orderName" class="layui-input" placeholder="请输入款号" autocomplete="off" style="width:200px">
                            </div>
                        </div>
                        <div class="layui-col-md3">
                            <!-- 填充内容 -->
                            <label class="layui-form-label">工序</label>
                            <div class="layui-input-inline">
                                <div id="procedureNumber" class="xm-select-demo" style="width:200px"></div>
                            </div>
                        </div>
                        <div class="layui-col-md3">
                            <!-- 填充内容 -->
                            <label class="layui-form-label">组名</label>
                            <div class="layui-input-inline">
                                <div id="groupName" class="xm-select-demo" style="width:200px"></div>
                            </div>
                        </div>
                    </div>
                    <div class="layui-row layui-col-space10">
                        <div class="layui-col-md3">
                            <!-- 填充内容 -->
                            <label class="layui-form-label">员工</label>
                            <div class="layui-input-inline">
                                <div id="employeeName" class="xm-select-demo" style="width:200px"></div>
                            </div>
                        </div>
                        <div class="layui-col-md3">
                            <!-- 填充内容 -->
                            <label class="layui-form-label">开始</label>
                            <div class="layui-input-inline">
                                <input type="text" name="beginDate" id="beginDate" autocomplete="off" class="layui-input" style="width:200px">
                            </div>
                        </div>
                        <div class="layui-col-md3">
                            <!-- 填充内容 -->
                            <label class="layui-form-label">结束</label>
                            <div class="layui-input-inline">
                                <input type="text" name="endDate" id="endDate" autocomplete="off" class="layui-input" style="width:200px">
                            </div>
                        </div>
                        <div class="layui-col-md3">
                            <!-- 填充内容 -->
                            <label class="layui-form-label"></label>
                            <div class="layui-input-inline">
                                <button class="layui-btn" lay-submit lay-filter="searchBeat">搜索</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <table id="beatTable" lay-filter="beatTable"></table>

            <script type="text/html" id="toolbarTop">
                <div class="layui-btn-container">
                    <button class="layui-btn layui-btn-sm" lay-event="exportExcel">导出</button>
                </div>
            </script>

        </section>
    </section>
</section>

    <link rel="stylesheet" href="/css/layui.css" type="text/css">
    <link rel="stylesheet" href="/css/soulTable.css" type="text/css">
    <link rel="stylesheet" href="/css/selectize.bootstrap3.css" type="text/css">
    <#--<script src="/js/common/layui.js" type="text/javascript"></script>-->
    <script src="/js/common/layer.js"></script>
    <script src="/js/common/xm-select.js?t=${currentDate?c}" type="text/javascript"></script>
    <script src="/js/common/selectize.js" type="text/javascript"></script>
    <script src="/js/common/moment.min.js" type="text/javascript"></script>
    <script src="/js/ieInfo/beat.js?t=${currentDate?c}"></script>
</#macro>

<style>
    .layui-col-md3 .layui-form-label {
        width:90px
    }
    .layui-col-md6 .layui-form-label {
        width:90px
    }
    .layui-col-md12 .layui-form-label {
        width:90px
    }

    ::-webkit-scrollbar-track-piece {

        background-color:#f8f8f8;

    }

    ::-webkit-scrollbar {

        width:15px;

        height:15px;

    }

    ::-webkit-scrollbar-thumb {

        background-color:#dddddd;

        background-clip:padding-box;

        min-height:28px;

    }

    ::-webkit-scrollbar-thumb:hover {

        background-color:#bbb;

    }

</style>
