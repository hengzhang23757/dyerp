<#macro procedureUpdate>
    <div id="procedureUpdate" style="display: none;cursor:default;">
        <form class="layui-form" style="margin: 5px;padding-top: 40px;" lay-filter="procedureUpdate">
            <div class="layui-row layui-col-space10">
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">款式类型</label>
                    <div class="layui-input-inline">
                        <input type="text" name="styleType" autocomplete="off" class="layui-input" lay-verify="required">
                    </div>
                </div>
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">部位</label>
                    <div class="layui-input-inline">
                        <input type="text" name="partName" autocomplete="off" class="layui-input" lay-verify="required">
                    </div>
                </div>
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">工序代码</label>
                    <div class="layui-input-inline">
                        <input type="text" name="procedureCode" autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">工序号</label>
                    <div class="layui-input-inline">
                        <input type="text" name="procedureNumber" autocomplete="off" class="layui-input">
                    </div>
                </div>
            </div>
            <div class="layui-row layui-col-space10">
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">工序名</label>
                    <div class="layui-input-inline">
                        <input type="text" name="procedureName" autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">工段</label>
                    <div class="layui-input-inline">
                        <input type="text" name="procedureSection" autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">工序描述</label>
                    <div class="layui-input-inline">
                        <input type="text" name="procedureDescription" autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">备注</label>
                    <div class="layui-input-inline">
                        <input type="text" name="remark" autocomplete="off" class="layui-input">
                    </div>
                </div>
            </div>
            <div class="layui-row layui-col-space10">
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">码段</label>
                    <div class="layui-input-inline">
                        <input type="text" name="segment" autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">设备类型</label>
                    <div class="layui-input-inline">
                        <input type="text" name="equType" autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">级别</label>
                    <div class="layui-input-inline">
                        <select type="text" name="procedureLevel" id="procedureLevel" autocomplete="off" class="layui-input" lay-filter="levelTest"></select>
                    </div>
                </div>
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">SAM值</label>
                    <div class="layui-input-inline">
                        <input type="text" name="sam" id="sam" autocomplete="off" class="layui-input" oninput="changeFunction()">
                    </div>
                </div>
            </div>
            <div class="layui-row layui-col-space10">
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">工价/打</label>
                    <div class="layui-input-inline">
                        <input type="text" name="packagePrice" id="packagePrice" autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-col-md3">
                    <!-- 填充内容 -->
                    <label class="layui-form-label">工价/件</label>
                    <div class="layui-input-inline">
                        <input type="text" name="piecePrice" id="piecePrice" autocomplete="off" class="layui-input">
                    </div>
                </div>
            </div>
        </form>
    </div>
</#macro>

<style>
    .layui-col-md3 .layui-form-label {
        width:90px
    }
    .layui-col-md6 .layui-form-label {
        width:90px
    }
    .layui-col-md12 .layui-form-label {
        width:90px
    }

    #staffAdd .text {
        width: 8%;
        text-align: center;
    }

    #staffAdd .content {
        width: 17%;
    }

    #staffAdd input {
        border-radius: 0;
        border-color: white;
    }

    #staffAdd select {
        border-color: white;
    }

    #staffAdd td {
        border: solid black;
        border-width: 0px 1px 1px 0px;
    }

    #staffAdd table {
        border: solid black;
        border-width: 1px 0px 0px 1px;
    }
</style>