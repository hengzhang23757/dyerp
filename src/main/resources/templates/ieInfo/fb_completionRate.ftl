<#macro search>
<section id="content">
    <section class="vbox">
        <section class="scrollable padder water-mark-visible-ff">

            <div class="col-md-12" style="padding-top: 20px;">
                <section class="panel panel-default">
                    <div class="panel-body">
                        <div class="row" style="margin-bottom: 10px">
                            <table style="border-collapse:separate;border-spacing: 10px;width: 100%;" id="baseInfo">
                                <tr>
                                    <td style="text-align: right;margin-bottom: 15px;">
                                        <label class="control-label" style="">开始日期</label>
                                    </td>
                                    <td style="margin-bottom: 15px;">
                                        <input type="text" id="from" class="form-control" autocomplete="off" placeholder="开始日期" style="width:200px">
                                    </td>
                                    <td style="text-align: right;margin-bottom: 15px;">
                                        <label class="control-label">结束日期</label>
                                    </td>
                                    <td style="margin-bottom: 15px;">
                                        <input type="text" id="to" class="form-control" autocomplete="off" placeholder="结束日期" style="width:200px">
                                    </td>
                                    <td style="text-align: right;margin-bottom: 15px;">
                                        <label class="control-label" style="">组名</label>
                                    </td>
                                    <td style="margin-bottom: 15px;">
                                        <select id="groupName" class="form-control" autocomplete="off" style="width:200px">
                                            <option value="">请选择组名</option>
                                            <option value="车缝A组">车缝A组</option>
                                            <option value="车缝B组">车缝B组</option>
                                            <option value="车缝C组">车缝C组</option>
                                            <option value="车缝D组">车缝D组</option>
                                            <option value="车缝E组">车缝E组</option>
                                            <option value="车缝F组">车缝F组</option>
                                            <option value="车缝G组">车缝G组</option>
                                            <option value="车缝H组">车缝H组</option>
                                            <option value="后整I组">后整I组</option>
                                            <option value="后整J组">后整I组</option>
                                        </select>
                                    </td>
                                    <td style="text-align: right;margin-bottom: 15px;">
                                        <label class="control-label" style="">小时</label>
                                    </td>
                                    <td style="margin-bottom: 15px;">
                                        <select id="workHour" class="form-control" autocomplete="off" style="width:200px">
                                            <option value="8">8</option>
                                            <option value="9">9</option>
                                            <option value="10">10</option>
                                            <option value="11" selected>11</option>
                                            <option value="12">12</option>
                                            <option value="13">13</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: right;margin-bottom: 15px;">
                                        <label class="control-label" style="">单号</label>
                                    </td>
                                    <td style="margin-bottom: 15px;">
                                        <input id="clothesVersionNumber" class="form-control" autocomplete="off" style="width: 200px">
                                    </td>
                                    <td style="text-align: right;margin-bottom: 15px;">
                                        <label class="control-label" style="">款号</label>
                                    </td>
                                    <td style="margin-bottom: 15px;">
                                        <input id="orderName" class="form-control" style="width: 200px">
                                    </td>
                                    <td style="text-align: right;margin-bottom: 15px;">
                                    </td>
                                    <td style="margin-bottom: 15px;">
                                        <button  class="btn" style="border-radius: 5px;color:white;font-family: PingFangSC-Semibold, sans-serif;width:100px;"  onclick="search()">查找</button>
                                    </td>
                                    <td style="text-align: right;margin-bottom: 15px;">
                                    </td>
                                    <td style="margin-bottom: 15px;">
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="col-md-12" style="padding-top: 20px;padding-bottom:10px" id="exportDiv" hidden>
                            <button  class="btn btn-s-lg" style="border-radius: 5px;color:white;"  onclick="exportData()">导出</button>
                        </div>
                        <div class="col-md-12" style="overflow-x: auto;height: 500px">
                            <div id="reportExcel"></div>
                        </div>
                    </div>
                </section>
            </div>
        </section>
    </section>
    <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen"
       data-target="#nav"></a>
</section>
<aside class="bg-light lter b-l aside-md hide" id="notes">
    <div class="wrapper">Notification</div>
</aside>
</section>
<link rel="stylesheet" href="/css/bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="/css/b.tabs.css" type="text/css">
<link rel="stylesheet" href="/css/handsontable.full.min.css" type="text/css">
<script src="/js/common/handsontable.full.min.js" type="text/javascript" ></script>
<script src="/js/common/zh-CN.js" type="text/javascript" ></script>
<script src="/js/common/laydate.js" type="text/javascript" ></script>
<script src="/js/common/jquery.blockUI.js" type="text/javascript" ></script>
<script src="/js/common/b.tabs.js?t=${currentDate?c}"></script>
<link rel="stylesheet" href="/css/selectize.bootstrap3.css" type="text/css">
<script src="/js/common/selectize.js" type="text/javascript"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css" type="text/css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script src="/js/common/export2Excel2.js" type="text/javascript" ></script>
<script src="/js/common/moment.min.js" type="text/javascript"></script>

<script src="/js/ieInfo/completionRate.js?t=${currentDate?c}"></script>


</#macro>

<style>
    button {
        background:rgb(45, 202, 147);
        opacity:0.86;
        color: white;
        font-family: PingFangSC-Semibold, sans-serif;
    }
</style>
