<!DOCTYPE html>
<html>
<head>
    <#include "../feedback/fb_script.ftl">
    <@script> </@script>
    <link rel="stylesheet" href="/css/handsontable.full.min.css" type="text/css">
    <link rel="stylesheet" href="/css/selectize.bootstrap3.css" type="text/css">
    <link rel="stylesheet" href="/css/tableFilter.css" type="text/css">
    <script src="/js/common/laydate.js" type="text/javascript" ></script>
    <link rel="stylesheet" href="/css/bootstrap.min.css" type="text/css">
    <script src="/js/common/jquery-form.js"></script>
    <script src="/js/common/handsontable.full.min.js" type="text/javascript" ></script>
    <script src="/js/common/zh-CN.js" type="text/javascript" ></script>
    <script src="/js/common/tableFilter.js" type="text/javascript" ></script>
    <script src="/js/common/selectize.js" type="text/javascript"></script>
    <script src="/js/common/layer.js"></script>
    <script src="/js/common/xm-select.js?t=${currentDate?c}" type="text/javascript"></script>
    <script src="/js/ieInfo/orderProcedureAddNew.js?t=${currentDate?c}"></script>
</head>

<body>
<input type="hidden" value="${basePath}" id="basePath"/>
<input type="hidden" value="${type!?string}" id="hideType"/>
<input type="hidden" value="${orderName!?string}" id="hideOrderName"/>
<input type="hidden" value="${userName}" id="userName"/>

<div class="col-md-12 row">
    <section class="panel panel-default">
        <header class="panel-heading font-bold">
            <span class="label bg-success pull-right"></span>订单信息
        </header>
        <table style="border-collapse:separate;border-spacing: 10px;width: 100%;" id="baseInfo">
            <tr>
                <td style="text-align: right;margin-bottom: 15px;">
                    <label class="control-label" style="">单号</label>
                </td>
                <td style="margin-bottom: 15px;">
                    <input id="clothesVersionNumber" class="form-control" placeholder="请输入单号" autocomplete="off">
                </td>
                <td style="text-align: right;margin-bottom: 15px;">
                    <label class="control-label" style="">款号</label>
                </td>
                <td style="margin-bottom: 15px;">
                    <input id="orderName" class="form-control" placeholder="请输入款号" autocomplete="off">
                </td>
                <td style="text-align: right;margin-bottom: 15px;">
                    <label class="control-label" style="">客户</label>
                </td>
                <td style="margin-bottom: 15px;">
                    <input id="customerName" class="form-control" autocomplete="off">
                </td>
                <td style="text-align: right;margin-bottom: 15px;">
                    <label class="control-label" style="">款式描述</label>
                </td>
                <td style="margin-bottom: 15px;">
                    <input id="styleDescription" class="form-control" autocomplete="off">
                </td>
            </tr>
            <tr>
                <td style="text-align: right;margin-bottom: 15px;">
                    <label class="control-label">开单日期</label>
                </td>
                <td style="margin-bottom: 15px;">
                    <input id="beginDate" class="form-control" autocomplete="off">
                </td>
                <td style="text-align: right;margin-bottom: 15px;">
                    <label class="control-label">数量</label>
                </td>
                <td style="margin-bottom: 15px;">
                    <input id="orderCount" class="form-control" autocomplete="off">
                </td>
                <td style="text-align: right;margin-bottom: 15px;">
                    <label class="control-label">补贴比例</label>
                </td>
                <td style="margin-bottom: 15px;">
                    <input id="subsidyPer" class="form-control" value="0" autocomplete="off">
                </td>
                <td style="text-align: right;margin-bottom: 15px;" id="isScanLabel">
                    <label class="control-label">开放扫描</label>
                </td>
                <td style="margin-bottom: 15px;" id="isScanSelect">
                    <select id="scanAvailable" class="form-control" autocomplete="off">
                        <option value="1">不开放</option>
                        <option value="0">开放</option>
                    </select>
                </td>
            </tr>
        </table>
    </section>

    <div class="row" style="margin-top: -20px;">
        <table class="layui-hide" id="procedureTable" lay-filter="procedureTable"></table>
        <script type="text/html" id="toolbarTop">
            <div class="layui-btn-container">
                <button class="layui-btn layui-btn-sm" lay-event="addRow">添加</button>
                <button class="layui-btn layui-btn-sm" lay-event="submitProcedure">提交</button>
                <button class="layui-btn layui-btn-sm" lay-event="procedureNumberAsc">工序号正序</button>
                <button class="layui-btn layui-btn-sm" lay-event="procedureNumberDesc">工序号倒序</button>
                <button class="layui-btn layui-btn-sm" lay-event="calPrice">计算</button>
            </div>
        </script>
    </div>
</div>


</body>
<script type="text/html" id="switchTpl">
    <input type="checkbox" name="special" value="{{d.specialProcedure}}" lay-skin="switch" lay-text="是|否" lay-filter="specialFilter" {{ d.specialProcedure == "是" ? 'checked' : '' }} <#if type=="update">{{ d.procedureState=='1' || d.procedureState=='2' ? 'disabled' : '' }}</#if>
    <#--<input type="checkbox" name="special" value="{{d.specialProcedure}}" lay-skin="switch" lay-text="是|否" lay-filter="specialFilter" {{ d.specialProcedure == "是" ? 'checked' : '' }} {{ d.procedureState=='1' || d.procedureState=='2' ? 'disabled' : '' }}>-->
</script>

<style>
    button {
        background:rgb(45, 202, 147);
        opacity:0.86;
        color: white;
        font-family: PingFangSC-Semibold, sans-serif;
    }
    .layui-table-cell{
        height:36px;
        line-height: 36px;
    }
    ::-webkit-scrollbar-track-piece {

        background-color:#f8f8f8;

    }

    ::-webkit-scrollbar {

        width:15px;

        height:15px;

    }

    ::-webkit-scrollbar-thumb {

        background-color:#dddddd;

        background-clip:padding-box;

        min-height:28px;

    }

    ::-webkit-scrollbar-thumb:hover {

        background-color:#bbb;

    }
</style>


</html>
