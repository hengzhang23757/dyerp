<#macro entities>
<#--<section class="panel panel-default">-->
    <div id="editUpdatePro" style="display: none;cursor:default">
        <div>
            <div style="text-align: right;">
                <a id="editUpdateNo" href="#" style="font-size:20px"><i class="fa fa-times icon" style="color: rgb(182,182,182)"></i></a>
            </div>
            <table style="border-collapse:separate;border-spacing: 10px;width: 100%;">
                <tr>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">版单号</label>
                    </td>
                    <td>
                        <input id="clothesVersionNumber1" readonly="readonly" class="form-control" style="width: 100%;margin-bottom: 15px;border-top: none;border-right: none;border-left: none;">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">订单号</label>
                    </td>
                    <td>
                        <input id="orderName1" readonly="readonly" class="form-control" style="width: 100%;margin-bottom: 15px;border-top: none;border-right: none;border-left: none;">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">组名</label>
                    </td>
                    <td>
                        <select id="groupName1" class="form-control" style="width: 100%;margin-bottom: 15px;border-top: none;border-right: none;border-left: none;">
                            <option value="">请选择组名</option>
                            <option value="车缝A组">车缝A组</option>
                            <option value="车缝B组">车缝B组</option>
                            <option value="车缝C组">车缝C组</option>
                            <option value="车缝D组">车缝D组</option>
                            <option value="车缝E组">车缝E组</option>
                            <option value="车缝F组">车缝F组</option>
                            <option value="车缝G组">车缝G组</option>
                            <option value="车缝H组">车缝H组</option>
                            <option value="后整I组">后整I组</option>
                            <option value="后整J组">后整J组</option>
                        </select>
                    </td>
                </tr>

                <tr>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">下线日期</label>
                    </td>
                    <td>
                        <input id="to1" class="form-control" style="margin-bottom: 15px;width: 100%;border-top: none;border-right: none;border-left: none;">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">每日工作(小时)</label>
                    </td>
                    <td>
                        <input id="workHour1" class="form-control" style="margin-bottom: 15px;width: 100%;border-top: none;border-right: none;border-left: none;">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">计划产量</label>
                    </td>
                    <td>
                        <input id="planCount1" class="form-control" style="margin-bottom: 15px;width: 100%;border-top: none;border-right: none;border-left: none;">
                    </td>
                </tr>
            </table>
        </div>
        <div class="col-sm-12" style="padding-top: 10px">
            <button id="editUpdateYes" class="btn btn-s-lg" style="border-radius: 5px;"  style="text-align: center;">保存</button>
            <#--<button id="editNo" class="btn btn-danger btn-rounded btn-small" style="text-align: center;">取消</button>-->
        </div>
    </div>
<#--</section>-->
</#macro>