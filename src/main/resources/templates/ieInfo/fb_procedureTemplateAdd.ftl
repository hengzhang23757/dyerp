<#macro procedureAdd>
    <div id="procedureAdd" style="display: none;cursor:default;">
        <form class="layui-form" style="margin: 5px;padding-top: 20px;" lay-filter="procedureAdd">
            <div class="col-md-12">
                <div id="addProcedureTemplateExcel" style="margin-left: 30px;overflow-x: auto;height: 700px"></div>
            </div>
        </form>
    </div>
</#macro>