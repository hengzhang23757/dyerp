<#macro search>
<section id="content">
    <section class="vbox">
        <section class="scrollable">
            <div class="layui-fluid">
                <div class="layui-row layui-col-space15" style="overflow-y: visible">
                    <div class="layui-col-md12">
                        <div class="layui-card" style="height: 90px;">
                            <div class="layui-card-body layui-text">
                                <form class="layui-form" style="margin: 0 auto;padding-top: 10px;" lay-filter="baseInfo">
                                    <table>
                                        <tr>
                                            <td style="text-align: right;margin-bottom: 15px;">
                                                <label class="layui-form-label">单号</label>
                                            </td>
                                            <td style="margin-bottom: 15px;">
                                                <input type="text" name="clothesVersionNumber" id="clothesVersionNumber" placeholder="请输入单号" class="layui-input">
                                            </td>
                                            <td style="text-align: right;margin-bottom: 15px;">
                                                <label class="layui-form-label">款号</label>
                                            </td>
                                            <td style="margin-bottom: 15px;">
                                                <input type="text" name="orderName" id="orderName" placeholder="请输入款号" class="layui-input">
                                            </td>
                                            <td style="text-align: right;margin-bottom: 15px;">
                                                <label class="layui-form-label">类别</label>
                                            </td>
                                            <td style="margin-bottom: 15px;">
                                                <select type="text" name="opType" id="opType" class="layui-input">
                                                    <option value="汇总">汇总</option>
                                                    <option value="下单">下单</option>
                                                    <option value="入库">入库</option>
                                                </select>
                                            </td>
                                            <td style="text-align: right;margin-bottom: 15px;">
                                                <label class="layui-form-label"></label>
                                            </td>
                                            <td style="margin-bottom: 15px;">
                                                <div class="layui-input-inline">
                                                    <button class="layui-btn" lay-submit lay-filter="searchBeat">搜索</button>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="layui-col-md5">
                        <div class="layui-card">
                            <div class="layui-card-header" style="font-weight: bolder; font-size: large; color: #1E9FFF">汇总表</div>
                            <div class="layui-card-body layui-text">
                                <table id="reportTable" lay-filter="reportTable"></table>
                            </div>
                            <script type="text/html" id="toolbarTop">
                                <div class="layui-btn-container">
                                    <button class="layui-btn layui-btn-sm" lay-event="refresh">刷新</button>
                                </div>
                            </script>
                        </div>
                    </div>

                    <div class="layui-col-md7">
                        <div class="layui-row layui-col-space15">
                            <div class="layui-col-md12">
                                <div class="layui-card">
                                    <div class="layui-card-header" id="progressDetailDiv"></div>
                                    <div class="layui-card-body layui-text">
                                        <table id="detailTable" lay-filter="detailTable"></table>
                                    </div>
                                    <script type="text/html" id="toolbarTopDetail">
                                        <div class="layui-btn-container">
                                            <button class="layui-btn layui-btn-sm" lay-event="export">导出</button>
                                        </div>
                                    </script>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>
</section>
    <link rel="stylesheet" href="/css/layui.css" type="text/css">
    <link rel="stylesheet" href="/css/soulTable.css" type="text/css">
    <script src="/js/common/moment.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/homepage/echarts-auto-tooltip.js"></script>
    <script src="/js/cost/costBalance.js?t=${currentDate?c}"></script>
</#macro>

<style>

    .layui-table-click {
        background-color:#5FB878; !important;
    }

    ::-webkit-scrollbar-track-piece {

        background-color:#f8f8f8;

    }

    ::-webkit-scrollbar {

        width:15px;

        height:15px;

    }

    ::-webkit-scrollbar-thumb {

        background-color:#dddddd;

        background-clip:padding-box;

        min-height:28px;

    }

    ::-webkit-scrollbar-thumb:hover {

        background-color:#bbb;

    }
</style>

