<#macro aside>
<aside class="bg-dark lter aside-md hidden-print" id="nav">
    <section class="vbox">
        <section class="w-f scrollable">
            <div class="slim-scroll" data-height="auto" data-disable-fade-out="true" data-distance="0"
                 data-size="5px" data-color="#333333"> <!-- nav -->
                <nav class="nav-primary hidden-xs">
                    <ul class="nav">
                        <#--<li><a href="/homepage"> <i-->
                                <#--class="fa fa-dashboard icon"> <b class="bg-danger"></b> </i> <span>主页</span>-->
                        <#--</a></li>-->
                        <#if role! == 'root'>
                            <li <#if bigMenuTag==1> class="active"</#if>> <a href="#" <#if bigMenuTag==1> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                <span>订单模块</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==11> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/orderStart"><span class="liActive">订单信息</span> </a></li>
                                </ul>
                            </li>

                            <li <#if bigMenuTag==2> class="active"</#if>> <a href="#" <#if bigMenuTag==2> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>裁床主身管理</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==20> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/singleTailorStart"><span class="liActive">票菲打印</span> </a></li>
                                    <li<#if menuTag==27> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/multiTailorStart"><span class="liActive">混色打菲</span> </a></li>
                                    <li<#if menuTag==21> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/tailorReprintStart"><span class="liActive">票菲补打</span> </a></li>
                                    <#--<li<#if menuTag==22> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/finishReprintStart"><span class="liActive">后整补打</span> </a></li>-->
                                    <li<#if menuTag==23> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/bedTailorInfoStart"><span class="liActive">按床删除</span> </a></li>
                                    <li<#if menuTag==24> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/oneTailorInfoStart"><span class="liActive">按扎删除</span> </a></li>
                                </ul>
                            </li>


                            <li <#if bigMenuTag==3> class="active"</#if>> <a href="#" <#if bigMenuTag==3> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>裁床配料管理</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==31> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/singleOtherTailorStart"><span class="liActive">配料打菲(合并)</span> </a></li>
                                    <li<#if menuTag==32> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/singleOtherTailorCombineStart"><span class="liActive">配料打菲(分开)</span> </a></li>
                                    <li<#if menuTag==33> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/bedOtherTailorInfoStart"><span class="liActive">按床删除</span> </a></li>
                                    <li<#if menuTag==34> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/oneOtherTailorInfoStart"><span class="liActive">按扎删除</span> </a></li>
                                    <li<#if menuTag==35> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/otherTailorReprintStart"><span class="liActive">配料重打</span> </a></li>
                                    <li<#if menuTag==36> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/otherMultiTailorStart"><span class="liActive">配料混色打菲</span> </a></li>
                                </ul>
                            </li>

                            <li <#if bigMenuTag==4> class="active"</#if>> <a href="#" <#if bigMenuTag==4> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>裁床报表</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==41> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/cutReportStart"><span class="liActive">裁床单报表</span> </a></li>
                                    <li<#if menuTag==42> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/cutQueryLeakStart"><span class="liActive">单款报表查询</span> </a></li>
                                    <li<#if menuTag==43> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/cutMonthReportStart"><span class="liActive">月度报表查询</span> </a></li>
                                    <li<#if menuTag==44> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/otherCutReportStart"><span class="liActive">配料裁床单</span> </a></li>
                                    <li<#if menuTag==45> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/otherTailorMonthReportStart"><span class="liActive">配料月度报表</span> </a></li>
                                    <li<#if menuTag==46> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/getCutBedColorStart"><span class="liActive">裁床分组明细</span> </a></li>
                                </ul>
                            </li>

                            <li <#if bigMenuTag==17> class="active"</#if>> <a href="#" <#if bigMenuTag==4> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>花片收发管理</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==171> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/opaStart"><span class="liActive">花片出厂</span> </a></li>
                                    <li<#if menuTag==172> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/otherOpaStart"><span class="liActive">士啤出厂</span> </a></li>
                                    <li<#if menuTag==173> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/opaBackInputStart"><span class="liActive">花片回厂</span> </a></li>
                                    <li<#if menuTag==174> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/opaReportStart"><span class="liActive">单款花片查询</span> </a></li>
                                </ul>
                            </li>

                            <li <#if bigMenuTag==5> class="active"</#if>> <a href="#" <#if bigMenuTag==5> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>裁片超市</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==51> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/instoreStart"><span class="liActive">裁片入库</span> </a></li>
                                    <li<#if menuTag==52> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/changeStoreStart"><span class="liActive">裁片调库</span> </a></li>
                                    <li<#if menuTag==53> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/outstoreStart"><span class="liActive">裁片出库</span> </a></li>
                                    <li<#if menuTag==54> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/storageStateStart"><span class="liActive">裁片库存状态</span> </a></li>
                                    <li<#if menuTag==55> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/opaMatchStart"><span class="liActive">花片配对</span> </a></li>
                                </ul>
                            </li>

                            <li <#if bigMenuTag==6> class="active"</#if>> <a href="#" <#if bigMenuTag==6> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>衣胚超市</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==61> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/embPlanStart"><span class="liActive">衣胚计划</span> </a></li>
                                    <li<#if menuTag==62> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/embInStoreStart"><span class="liActive">衣胚入库</span> </a></li>
                                    <li<#if menuTag==63> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/embOutStoreStart"><span class="liActive">衣胚出库</span> </a></li>
                                    <li<#if menuTag==64> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/embStorageStateStart"><span class="liActive">衣胚库存状态</span> </a></li>
                                    <li<#if menuTag==65> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/embOutStoreSingleStart"><span class="liActive">衣胚单扎出库</span> </a></li>
                                </ul>
                            </li>

                            <li <#if bigMenuTag==7> class="active"</#if>> <a href="#" <#if bigMenuTag==7> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>裁片&衣胚查询</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==71> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/cutStorageQueryStart"><span class="liActive">裁片单款库存</span> </a></li>
                                    <li<#if menuTag==72> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/embStorageQueryStart"><span class="liActive">衣胚单款库存</span> </a></li>
                                    <li<#if menuTag==73> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/embOutDetailStart"><span class="liActive">衣胚阶段出库</span> </a></li>
                                    <li<#if menuTag==74> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/embOutPackageDetailStart"><span class="liActive">衣胚出库详情</span> </a></li>
                                    <li<#if menuTag==75> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/queryEmbLeakStart"><span class="liActive">衣胚出库汇总</span> </a></li>
                                    <li<#if menuTag==76> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/cutLocationStart"><span class="liActive">裁片位置详情</span> </a></li>
                                </ul>
                            </li>

                            <li <#if bigMenuTag==8> class="active"</#if>> <a href="#" <#if bigMenuTag==8> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>面料管理</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==81> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/fabricStart"><span class="liActive">面料录入</span> </a></li>
                                    <li<#if menuTag==83> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/fabricInStoreStart"><span class="liActive">面料入仓</span> </a></li>
                                    <li<#if menuTag==84> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/fabricOutStoreStart"><span class="liActive">面料出仓</span> </a></li>
                                    <li<#if menuTag==85> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/fabricQueryStart"><span class="liActive">面料库存</span> </a></li>
                                    <li<#if menuTag==86> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/fabricOutRecordStart"><span class="liActive">面料出库详情</span> </a></li>
                                    <li<#if menuTag==811> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/fabricLeakQueryStart"><span class="liActive">季度面料查询</span> </a></li>
                                    <li<#if menuTag==812> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/fabricDetailStart"><span class="liActive">单款面料详情</span> </a></li>
                                    <li<#if menuTag==82> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/innerBoardStart"><span class="liActive">内版录入</span> </a></li>
                                    <li<#if menuTag==87> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/innerBoardInStart"><span class="liActive">内板入库</span> </a></li>
                                    <li<#if menuTag==88> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/innerBoardOutStart"><span class="liActive">内板出库</span> </a></li>
                                    <li<#if menuTag==89> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/innerBoardQueryStart"><span class="liActive">内板库存</span> </a></li>
                                    <li<#if menuTag==810> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/innerBoardOutRecordStart"><span class="liActive">内板出库详情</span> </a></li>

                                </ul>
                            </li>

                            <li <#if bigMenuTag==9> class="active"</#if>> <a href="#" <#if bigMenuTag==9> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>IE基础资料</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==91> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/procedureTemplateStart"><span class="liActive">工序数据库</span> </a></li>
                                    <li<#if menuTag==92> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/orderProcedureStart"><span class="liActive">订单工序</span> </a></li>
                                    <li<#if menuTag==93> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/procedureProgressStart"><span class="liActive">工序进度</span> </a></li>
                                    <li<#if menuTag==94> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/procedureLevelStart"><span class="liActive">工序等级</span> </a></li>
                                    <li<#if menuTag==95> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/metreStart"><span class="liActive">节拍查询</span> </a></li>
                                    <li<#if menuTag==96> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/printPartStart"><span class="liActive">票菲部位管理</span> </a></li>
                                    <li<#if menuTag==97> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/groupPlanStart"><span class="liActive">计划产能</span> </a></li>
                                    <li<#if menuTag==98> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/completionRateStart"><span class="liActive">达成率</span> </a></li>
                                </ul>
                            </li>

                            <li <#if bigMenuTag==10> class="active"</#if>> <a href="#" <#if bigMenuTag==10> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>手机计件</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==101> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/employeeStart"><span class="liActive">员工管理</span> </a></li>
                                    <li<#if menuTag==102> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/dispatchStart"><span class="liActive">工序分配</span> </a></li>
                                    <li<#if menuTag==103> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/pieceWorkStatisticStart"><span class="liActive">产能统计</span> </a></li>
                                    <li<#if menuTag==104> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/pieceWorkManageStart"><span class="liActive">计件管理</span> </a></li>
                                    <li<#if menuTag==105> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/manualInputStart"><span class="liActive">手工入数</span> </a></li>
                                    <li<#if menuTag==109> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/hourEmpStart"><span class="liActive">时工入数</span> </a></li>
                                    <#--<li<#if menuTag==59> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/pieceWorkDetailQueryStart"><span class="liActive">计件明细查询</span> </a></li>-->
                                    <li<#if menuTag==106> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/wrongStart"><span class="liActive">疵点代码</span> </a></li>
                                    <li<#if menuTag==107> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/inspectionStart"><span class="liActive">抽查疵点统计</span> </a></li>
                                    <li<#if menuTag==108> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/sampleInspectionStart"><span class="liActive">QC疵点统计</span> </a></li>
                                </ul>
                            </li>

                            <li <#if bigMenuTag==11> class="active"</#if>> <a href="#" <#if bigMenuTag==11> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>财务数据</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==111> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/empSalaryStart"><span class="liActive">员工个人产能</span> </a></li>
                                    <li<#if menuTag==112> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/groupSalaryStart"><span class="liActive">员工分组产能</span> </a></li>
                                    <li<#if menuTag==113> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/cutDailyDetailStart"><span class="liActive">裁床每日明细</span> </a></li>
                                    <li<#if menuTag==115> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/orderSalarySummaryStart"><span class="liActive">订单总工资</span> </a></li>
                                </ul>
                            </li>

                            <li <#if bigMenuTag==12> class="active"</#if>> <a href="#" <#if bigMenuTag==12> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>生产报表数据</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==121> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/procedureBalanceStart"><span class="liActive">订单工序平衡</span> </a></li>
                                    <li<#if menuTag==122> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/completionStart"><span class="liActive">完工结算</span> </a></li>
                                    <li<#if menuTag==123> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/reWorkStart"><span class="liActive">返工率</span> </a></li>
                                </ul>
                            </li>

                            <li <#if bigMenuTag==13> class="active"</#if>> <a href="#" <#if bigMenuTag==13> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                <span>工厂基础信息</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==131> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/storeHouseStart"> <span class="liActive">仓库信息</span> </a></li>
                                    <li<#if menuTag==132> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/embStoreStart"> <span class="liActive">衣胚仓库</span> </a></li>
                                    <li<#if menuTag==133> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/customerStart"> <span class="liActive">顾客信息</span> </a></li>
                                    <li<#if menuTag==134> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/userStart"> <span class="liActive">用户信息</span> </a></li>
                                </ul>
                            </li>

                            <li <#if bigMenuTag==14> class="active"</#if>> <a href="#" <#if bigMenuTag==14> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>下成品</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==141> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/unloadStart"><span class="liActive">下成品扫描</span> </a></li>
                                    <li<#if menuTag==142> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/unloadReportStart"><span class="liActive">下成品记录</span> </a></li>
                                </ul>
                            </li>

                            <li <#if bigMenuTag==15> class="active"</#if>> <a href="#" <#if bigMenuTag==14> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>其他计件</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==151> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/otherPieceWorkStart"><span class="liActive">其他计件</span> </a></li>
                                </ul>
                            </li>

                            <li <#if bigMenuTag==16> class="active"</#if>> <a href="#" <#if bigMenuTag==16> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>后整打菲</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==161> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/finishTailorStart"><span class="liActive">后整打菲</span> </a></li>
                                    <li<#if menuTag==162> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/finishTailorDeleteStart"><span class="liActive">菲票管理</span> </a></li>
                                    <li<#if menuTag==163> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/finishTailorLeakStart"><span class="liActive">后整打菲报表</span> </a></li>
                                </ul>
                            </li>

                        <#elseif role! == 'role1'>

                            <li <#if bigMenuTag==1> class="active"</#if>> <a href="#" <#if bigMenuTag==1> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>订单模块</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==11> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/orderStart"><span class="liActive">订单信息</span> </a></li>
                                </ul>
                            </li>

                            <li <#if bigMenuTag==2> class="active"</#if>> <a href="#" <#if bigMenuTag==2> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>裁床主身管理</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==20> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/singleTailorStart"><span class="liActive">票菲打印</span> </a></li>
                                    <li<#if menuTag==27> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/multiTailorStart"><span class="liActive">混色打菲</span> </a></li>
                                    <li<#if menuTag==21> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/tailorReprintStart"><span class="liActive">票菲补打</span> </a></li>
                                    <li<#if menuTag==23> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/bedTailorInfoStart"><span class="liActive">按床删除</span> </a></li>
                                    <li<#if menuTag==24> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/oneTailorInfoStart"><span class="liActive">按扎删除</span> </a></li>
                                </ul>
                            </li>


                            <li <#if bigMenuTag==3> class="active"</#if>> <a href="#" <#if bigMenuTag==3> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>裁床配料管理</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==31> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/singleOtherTailorStart"><span class="liActive">配料打菲(合并)</span> </a></li>
                                    <li<#if menuTag==32> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/singleOtherTailorCombineStart"><span class="liActive">配料打菲(分开)</span> </a></li>
                                    <li<#if menuTag==33> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/bedOtherTailorInfoStart"><span class="liActive">按床删除</span> </a></li>
                                    <li<#if menuTag==34> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/oneOtherTailorInfoStart"><span class="liActive">按扎删除</span> </a></li>
                                    <li<#if menuTag==35> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/otherTailorReprintStart"><span class="liActive">配料重打</span> </a></li>
                                    <li<#if menuTag==36> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/otherMultiTailorStart"><span class="liActive">配料混色打菲</span> </a></li>
                                </ul>
                            </li>

                            <li <#if bigMenuTag==4> class="active"</#if>> <a href="#" <#if bigMenuTag==4> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>裁床报表</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==41> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/cutReportStart"><span class="liActive">裁床单报表</span> </a></li>
                                    <li<#if menuTag==42> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/cutQueryLeakStart"><span class="liActive">单款报表查询</span> </a></li>
                                    <li<#if menuTag==43> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/cutMonthReportStart"><span class="liActive">月度报表查询</span> </a></li>
                                    <li<#if menuTag==44> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/otherCutReportStart"><span class="liActive">配料裁床单</span> </a></li>
                                    <li<#if menuTag==45> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/otherTailorMonthReportStart"><span class="liActive">配料月度报表</span> </a></li>
                                    <li<#if menuTag==46> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/getCutBedColorStart"><span class="liActive">裁床分组明细</span> </a></li>
                                </ul>
                            </li>

                            <li <#if bigMenuTag==17> class="active"</#if>> <a href="#" <#if bigMenuTag==4> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>花片收发管理</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==171> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/opaStart"><span class="liActive">花片出厂</span> </a></li>
                                    <li<#if menuTag==172> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/otherOpaStart"><span class="liActive">士啤出厂</span> </a></li>
                                    <li<#if menuTag==173> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/opaBackInputStart"><span class="liActive">花片回厂</span> </a></li>
                                    <li<#if menuTag==174> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/opaReportStart"><span class="liActive">单款花片查询</span> </a></li>
                                </ul>
                            </li>

                            <li <#if bigMenuTag==7> class="active"</#if>> <a href="#" <#if bigMenuTag==7> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>裁片&衣胚查询</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==71> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/cutStorageQueryStart"><span class="liActive">裁片单款库存</span> </a></li>
                                    <li<#if menuTag==72> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/embStorageQueryStart"><span class="liActive">衣胚单款库存</span> </a></li>
                                    <li<#if menuTag==73> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/embOutDetailStart"><span class="liActive">衣胚阶段出库</span> </a></li>
                                    <li<#if menuTag==74> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/embOutPackageDetailStart"><span class="liActive">衣胚出库详情</span> </a></li>
                                    <li<#if menuTag==75> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/queryEmbLeakStart"><span class="liActive">衣胚出库汇总</span> </a></li>
                                    <li<#if menuTag==76> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/cutLocationStart"><span class="liActive">裁片位置详情</span> </a></li>
                                </ul>
                            </li>

                            <li <#if bigMenuTag==8> class="active"</#if>> <a href="#" <#if bigMenuTag==8> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>面料管理</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==85> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/fabricQueryStart"><span class="liActive">面料库存</span> </a></li>
                                    <li<#if menuTag==86> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/fabricOutRecordStart"><span class="liActive">面料出库详情</span> </a></li>
                                    <li<#if menuTag==812> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/fabricDetailStart"><span class="liActive">单款面料详情</span> </a></li>
                                    <li<#if menuTag==89> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/innerBoardQueryStart"><span class="liActive">内板库存</span> </a></li>
                                    <li<#if menuTag==810> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/innerBoardOutRecordStart"><span class="liActive">内板出库详情</span> </a></li>
                                    <li<#if menuTag==811> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/fabricLeakQueryStart"><span class="liActive">季度面料查询</span> </a></li>
                                </ul>
                            </li>

                            <li <#if bigMenuTag==11> class="active"</#if>> <a href="#" <#if bigMenuTag==11> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>财务数据</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==111> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/empSalaryStart"><span class="liActive">员工个人产能</span> </a></li>
                                    <li<#if menuTag==112> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/groupSalaryStart"><span class="liActive">员工分组产能</span> </a></li>
                                    <li<#if menuTag==113> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/cutDailyDetailStart"><span class="liActive">裁床每日明细</span> </a></li>
                                    <li<#if menuTag==115> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/orderSalarySummaryStart"><span class="liActive">订单总工资</span> </a></li>
                                </ul>
                            </li>

                            <li <#if bigMenuTag==12> class="active"</#if>> <a href="#" <#if bigMenuTag==12> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>生产报表数据</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==121> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/procedureBalanceStart"><span class="liActive">订单工序平衡</span> </a></li>
                                    <li<#if menuTag==122> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/completionStart"><span class="liActive">完工结算</span> </a></li>
                                    <li<#if menuTag==123> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/reWorkStart"><span class="liActive">返工率</span> </a></li>
                                </ul>
                            </li>

                        <#elseif role! == 'role2'>
                            <li <#if bigMenuTag==1> class="active"</#if>> <a href="#" <#if bigMenuTag==1> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>订单模块</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==11> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/orderStart"><span class="liActive">订单信息</span> </a></li>
                                </ul>
                            </li>

                            <li <#if bigMenuTag==17> class="active"</#if>> <a href="#" <#if bigMenuTag==4> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>花片收发管理</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==171> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/opaStart"><span class="liActive">花片出厂</span> </a></li>
                                    <li<#if menuTag==172> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/otherOpaStart"><span class="liActive">士啤出厂</span> </a></li>
                                    <li<#if menuTag==173> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/opaBackInputStart"><span class="liActive">花片回厂</span> </a></li>
                                    <li<#if menuTag==174> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/opaReportStart"><span class="liActive">单款花片查询</span> </a></li>
                                </ul>
                            </li>

                            <li <#if bigMenuTag==4> class="active"</#if>> <a href="#" <#if bigMenuTag==4> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>裁床报表</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==41> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/cutReportStart"><span class="liActive">裁床单报表</span> </a></li>
                                    <li<#if menuTag==42> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/cutQueryLeakStart"><span class="liActive">单款报表查询</span> </a></li>
                                    <li<#if menuTag==43> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/cutMonthReportStart"><span class="liActive">月度报表查询</span> </a></li>
                                    <li<#if menuTag==44> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/otherCutReportStart"><span class="liActive">配料裁床单</span> </a></li>
                                    <li<#if menuTag==45> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/otherTailorMonthReportStart"><span class="liActive">配料月度报表</span> </a></li>
                                    <li<#if menuTag==46> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/getCutBedColorStart"><span class="liActive">裁床分组明细</span> </a></li>
                                </ul>
                            </li>

                            <li <#if bigMenuTag==7> class="active"</#if>> <a href="#" <#if bigMenuTag==7> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>裁片&衣胚查询</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==71> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/cutStorageQueryStart"><span class="liActive">裁片单款库存</span> </a></li>
                                    <li<#if menuTag==72> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/embStorageQueryStart"><span class="liActive">衣胚单款库存</span> </a></li>
                                    <li<#if menuTag==73> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/embOutDetailStart"><span class="liActive">衣胚阶段出库</span> </a></li>
                                    <li<#if menuTag==74> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/embOutPackageDetailStart"><span class="liActive">衣胚出库详情</span> </a></li>
                                    <li<#if menuTag==75> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/queryEmbLeakStart"><span class="liActive">衣胚出库汇总</span> </a></li>
                                    <li<#if menuTag==76> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/cutLocationStart"><span class="liActive">裁片位置详情</span> </a></li>
                                </ul>
                            </li>



                            <li <#if bigMenuTag==8> class="active"</#if>> <a href="#" <#if bigMenuTag==8> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>面料管理</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==85> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/fabricQueryStart"><span class="liActive">面料库存</span> </a></li>
                                    <li<#if menuTag==86> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/fabricOutRecordStart"><span class="liActive">面料出库详情</span> </a></li>
                                    <li<#if menuTag==812> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/fabricDetailStart"><span class="liActive">单款面料详情</span> </a></li>
                                    <#--<li<#if menuTag==89> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/innerBoardQueryStart"><span class="liActive">内板库存</span> </a></li>-->
                                    <#--<li<#if menuTag==810> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/innerBoardOutRecordStart"><span class="liActive">内板出库详情</span> </a></li>-->
                                    <li<#if menuTag==811> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/fabricLeakQueryStart"><span class="liActive">季度面料查询</span> </a></li>
                                </ul>
                            </li>

                            <li <#if bigMenuTag==11> class="active"</#if>> <a href="#" <#if bigMenuTag==11> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>财务数据</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==111> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/empSalaryStart"><span class="liActive">员工个人产能</span> </a></li>
                                    <li<#if menuTag==112> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/groupSalaryStart"><span class="liActive">员工分组产能</span> </a></li>
                                    <li<#if menuTag==113> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/cutDailyDetailStart"><span class="liActive">裁床每日明细</span> </a></li>
                                    <li<#if menuTag==115> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/orderSalarySummaryStart"><span class="liActive">订单总工资</span> </a></li>
                                </ul>
                            </li>

                            <li <#if bigMenuTag==12> class="active"</#if>> <a href="#" <#if bigMenuTag==12> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>生产报表数据</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==121> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/procedureBalanceStart"><span class="liActive">订单工序平衡</span> </a></li>
                                    <li<#if menuTag==122> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/completionStart"><span class="liActive">完工结算</span> </a></li>
                                    <li<#if menuTag==123> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/reWorkStart"><span class="liActive">返工率</span> </a></li>
                                </ul>
                            </li>

                        <#elseif role! == 'role3'>
                            <li <#if bigMenuTag==1> class="active"</#if>> <a href="#" <#if bigMenuTag==1> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>订单模块</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==11> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/orderStart"><span class="liActive">订单信息</span> </a></li>
                                </ul>
                            </li>

                            <li <#if bigMenuTag==4> class="active"</#if>> <a href="#" <#if bigMenuTag==4> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>裁床报表</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==41> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/cutReportStart"><span class="liActive">裁床单报表</span> </a></li>
                                    <li<#if menuTag==42> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/cutQueryLeakStart"><span class="liActive">单款报表查询</span> </a></li>
                                    <li<#if menuTag==43> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/cutMonthReportStart"><span class="liActive">月度报表查询</span> </a></li>
                                    <li<#if menuTag==44> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/otherCutReportStart"><span class="liActive">配料裁床单</span> </a></li>
                                    <li<#if menuTag==45> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/otherTailorMonthReportStart"><span class="liActive">配料月度报表</span> </a></li>
                                    <li<#if menuTag==46> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/getCutBedColorStart"><span class="liActive">裁床分组明细</span> </a></li>
                                </ul>
                            </li>

                            <li <#if bigMenuTag==8> class="active"</#if>> <a href="#" <#if bigMenuTag==8> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>面料管理</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==81> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/fabricStart"><span class="liActive">面料录入</span> </a></li>
                                    <li<#if menuTag==83> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/fabricInStoreStart"><span class="liActive">面料入仓</span> </a></li>
                                    <li<#if menuTag==84> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/fabricOutStoreStart"><span class="liActive">面料出仓</span> </a></li>
                                    <li<#if menuTag==85> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/fabricQueryStart"><span class="liActive">面料库存</span> </a></li>
                                    <li<#if menuTag==86> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/fabricOutRecordStart"><span class="liActive">面料出库详情</span> </a></li>
                                    <li<#if menuTag==811> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/fabricLeakQueryStart"><span class="liActive">季度面料查询</span> </a></li>
                                    <li<#if menuTag==812> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/fabricDetailStart"><span class="liActive">单款面料详情</span> </a></li>
                                    <#--<li<#if menuTag==82> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/innerBoardStart"><span class="liActive">内版录入</span> </a></li>-->
                                    <#--<li<#if menuTag==87> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/innerBoardInStart"><span class="liActive">内板入库</span> </a></li>-->
                                    <#--<li<#if menuTag==88> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/innerBoardOutStart"><span class="liActive">内板出库</span> </a></li>-->
                                    <#--<li<#if menuTag==89> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/innerBoardQueryStart"><span class="liActive">内板库存</span> </a></li>-->
                                    <#--<li<#if menuTag==810> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/innerBoardOutRecordStart"><span class="liActive">内板出库详情</span> </a></li>-->
                                </ul>
                            </li>

                            <li <#if bigMenuTag==11> class="active"</#if>> <a href="#" <#if bigMenuTag==11> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>财务数据</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==111> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/empSalaryStart"><span class="liActive">员工个人产能</span> </a></li>
                                    <li<#if menuTag==112> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/groupSalaryStart"><span class="liActive">员工分组产能</span> </a></li>
                                    <li<#if menuTag==113> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/cutDailyDetailStart"><span class="liActive">裁床每日明细</span> </a></li>
                                    <li<#if menuTag==115> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/orderSalarySummaryStart"><span class="liActive">订单总工资</span> </a></li>
                                </ul>
                            </li>

                            <li <#if bigMenuTag==12> class="active"</#if>> <a href="#" <#if bigMenuTag==12> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>生产报表数据</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==121> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/procedureBalanceStart"><span class="liActive">订单工序平衡</span> </a></li>
                                    <li<#if menuTag==122> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/completionStart"><span class="liActive">完工结算</span> </a></li>
                                    <li<#if menuTag==123> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/reWorkStart"><span class="liActive">返工率</span> </a></li>
                                </ul>
                            </li>


                        <#elseif role! == 'role4'>
                            <li <#if bigMenuTag==10> class="active"</#if>> <a href="#" <#if bigMenuTag==10> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>手机计件</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==101> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/employeeStart"><span class="liActive">员工管理</span> </a></li>
                                </ul>
                            </li>

                        <#elseif role! == 'role5'>
                            <li <#if bigMenuTag==1> class="active"</#if>> <a href="#" <#if bigMenuTag==1> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>订单模块</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==11> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/orderStart"><span class="liActive">订单信息</span> </a></li>
                                </ul>
                            </li>

                            <li <#if bigMenuTag==4> class="active"</#if>> <a href="#" <#if bigMenuTag==4> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>裁床报表</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==41> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/cutReportStart"><span class="liActive">裁床单报表</span> </a></li>
                                    <li<#if menuTag==42> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/cutQueryLeakStart"><span class="liActive">单款报表查询</span> </a></li>
                                    <li<#if menuTag==43> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/cutMonthReportStart"><span class="liActive">月度报表查询</span> </a></li>
                                    <li<#if menuTag==44> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/otherCutReportStart"><span class="liActive">配料裁床单</span> </a></li>
                                    <li<#if menuTag==45> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/otherTailorMonthReportStart"><span class="liActive">配料月度报表</span> </a></li>
                                    <li<#if menuTag==46> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/getCutBedColorStart"><span class="liActive">裁床分组明细</span> </a></li>
                                </ul>
                            </li>

                            <li <#if bigMenuTag==7> class="active"</#if>> <a href="#" <#if bigMenuTag==7> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>裁片&衣胚查询</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==71> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/cutStorageQueryStart"><span class="liActive">裁片单款库存</span> </a></li>
                                    <li<#if menuTag==72> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/embStorageQueryStart"><span class="liActive">衣胚单款库存</span> </a></li>
                                    <li<#if menuTag==73> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/embOutDetailStart"><span class="liActive">衣胚阶段出库</span> </a></li>
                                    <li<#if menuTag==74> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/embOutPackageDetailStart"><span class="liActive">衣胚出库详情</span> </a></li>
                                    <li<#if menuTag==75> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/queryEmbLeakStart"><span class="liActive">衣胚出库汇总</span> </a></li>
                                    <li<#if menuTag==76> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/cutLocationStart"><span class="liActive">裁片位置详情</span> </a></li>
                                </ul>
                            </li>

                            <li <#if bigMenuTag==9> class="active"</#if>> <a href="#" <#if bigMenuTag==9> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>IE基础资料</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==91> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/procedureTemplateStart"><span class="liActive">工序数据库</span> </a></li>
                                    <li<#if menuTag==92> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/orderProcedureStart"><span class="liActive">订单工序</span> </a></li>
                                    <li<#if menuTag==93> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/procedureProgressStart"><span class="liActive">工序进度</span> </a></li>
                                    <li<#if menuTag==94> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/procedureLevelStart"><span class="liActive">工序等级</span> </a></li>
                                    <li<#if menuTag==95> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/metreStart"><span class="liActive">节拍查询</span> </a></li>
                                    <li<#if menuTag==96> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/printPartStart"><span class="liActive">票菲部位管理</span> </a></li>
                                    <li<#if menuTag==97> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/groupPlanStart"><span class="liActive">计划产能</span> </a></li>
                                    <li<#if menuTag==98> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/completionRateStart"><span class="liActive">达成率</span> </a></li>
                                </ul>
                            </li>


                            <li <#if bigMenuTag==10> class="active"</#if>> <a href="#" <#if bigMenuTag==10> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>手机计件</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==101> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/employeeStart"><span class="liActive">员工管理</span> </a></li>
                                    <li<#if menuTag==102> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/dispatchStart"><span class="liActive">工序分配</span> </a></li>
                                    <li<#if menuTag==103> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/pieceWorkStatisticStart"><span class="liActive">产能统计</span> </a></li>
                                    <li<#if menuTag==104> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/pieceWorkManageStart"><span class="liActive">计件管理</span> </a></li>
                                    <li<#if menuTag==105> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/manualInputStart"><span class="liActive">手工入数</span> </a></li>
                                    <#--<li<#if menuTag==59> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/pieceWorkDetailQueryStart"><span class="liActive">计件明细查询</span> </a></li>-->
                                    <li<#if menuTag==106> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/wrongStart"><span class="liActive">疵点代码</span> </a></li>
                                    <li<#if menuTag==107> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/inspectionStart"><span class="liActive">抽查疵点统计</span> </a></li>
                                    <li<#if menuTag==108> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/sampleInspectionStart"><span class="liActive">QC疵点统计</span> </a></li>
                                </ul>
                            </li>

                            <li <#if bigMenuTag==12> class="active"</#if>> <a href="#" <#if bigMenuTag==12> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>生产报表数据</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==121> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/procedureBalanceStart"><span class="liActive">订单工序平衡</span> </a></li>
                                    <li<#if menuTag==122> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/completionStart"><span class="liActive">完工结算</span> </a></li>
                                    <li<#if menuTag==123> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/reWorkStart"><span class="liActive">返工率</span> </a></li>
                                </ul>
                            </li>

                            <li <#if bigMenuTag==11> class="active"</#if>> <a href="#" <#if bigMenuTag==11> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>财务数据</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==111> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/empSalaryStart"><span class="liActive">员工个人产能</span> </a></li>
                                    <li<#if menuTag==112> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/groupSalaryStart"><span class="liActive">员工分组产能</span> </a></li>
                                    <li<#if menuTag==113> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/cutDailyDetailStart"><span class="liActive">裁床每日明细</span> </a></li>
                                    <li<#if menuTag==115> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/orderSalarySummaryStart"><span class="liActive">订单总工资</span> </a></li>
                                </ul>
                            </li>
                        <#elseif role! == 'role6'>
                            <li <#if bigMenuTag==4> class="active"</#if>> <a href="#" <#if bigMenuTag==4> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>裁床报表</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==41> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/cutReportStart"><span class="liActive">裁床单报表</span> </a></li>
                                    <li<#if menuTag==42> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/cutQueryLeakStart"><span class="liActive">单款报表查询</span> </a></li>
                                    <li<#if menuTag==43> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/cutMonthReportStart"><span class="liActive">月度报表查询</span> </a></li>
                                    <li<#if menuTag==44> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/otherCutReportStart"><span class="liActive">配料裁床单</span> </a></li>
                                    <li<#if menuTag==45> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/otherTailorMonthReportStart"><span class="liActive">配料月度报表</span> </a></li>
                                    <li<#if menuTag==46> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/getCutBedColorStart"><span class="liActive">裁床分组明细</span> </a></li>
                                </ul>
                            </li>

                            <li <#if bigMenuTag==17> class="active"</#if>> <a href="#" <#if bigMenuTag==4> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>花片收发管理</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==171> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/opaStart"><span class="liActive">花片出厂</span> </a></li>
                                    <li<#if menuTag==172> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/otherOpaStart"><span class="liActive">士啤出厂</span> </a></li>
                                    <li<#if menuTag==173> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/opaBackInputStart"><span class="liActive">花片回厂</span> </a></li>
                                    <li<#if menuTag==174> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/opaReportStart"><span class="liActive">单款花片查询</span> </a></li>
                                </ul>
                            </li>

                            <li <#if bigMenuTag==5> class="active"</#if>> <a href="#" <#if bigMenuTag==5> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>裁片超市</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==51> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/instoreStart"><span class="liActive">裁片入库</span> </a></li>
                                    <li<#if menuTag==52> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/changeStoreStart"><span class="liActive">裁片调库</span> </a></li>
                                    <li<#if menuTag==53> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/outstoreStart"><span class="liActive">裁片出库</span> </a></li>
                                    <li<#if menuTag==54> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/storageStateStart"><span class="liActive">裁片库存状态</span> </a></li>
                                    <li<#if menuTag==55> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/opaMatchStart"><span class="liActive">花片配对</span> </a></li>
                                </ul>
                            </li>

                            <li <#if bigMenuTag==6> class="active"</#if>> <a href="#" <#if bigMenuTag==6> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>衣胚超市</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==61> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/embPlanStart"><span class="liActive">衣胚计划</span> </a></li>
                                    <li<#if menuTag==62> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/embInStoreStart"><span class="liActive">衣胚入库</span> </a></li>
                                    <li<#if menuTag==63> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/embOutStoreStart"><span class="liActive">衣胚出库</span> </a></li>
                                    <li<#if menuTag==64> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/embStorageStateStart"><span class="liActive">衣胚库存状态</span> </a></li>
                                </ul>
                            </li>

                            <li <#if bigMenuTag==7> class="active"</#if>> <a href="#" <#if bigMenuTag==7> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>裁片&衣胚查询</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==71> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/cutStorageQueryStart"><span class="liActive">裁片单款库存</span> </a></li>
                                    <li<#if menuTag==72> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/embStorageQueryStart"><span class="liActive">衣胚单款库存</span> </a></li>
                                    <li<#if menuTag==73> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/embOutDetailStart"><span class="liActive">衣胚阶段出库</span> </a></li>
                                    <li<#if menuTag==74> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/embOutPackageDetailStart"><span class="liActive">衣胚出库详情</span> </a></li>
                                    <li<#if menuTag==75> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/queryEmbLeakStart"><span class="liActive">衣胚出库汇总</span> </a></li>
                                    <li<#if menuTag==76> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/cutLocationStart"><span class="liActive">裁片位置详情</span> </a></li>
                                </ul>
                            </li>

                            <li <#if bigMenuTag==12> class="active"</#if>> <a href="#" <#if bigMenuTag==12> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>生产报表数据</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==121> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/procedureBalanceStart"><span class="liActive">订单工序平衡</span> </a></li>
                                    <li<#if menuTag==122> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/completionStart"><span class="liActive">完工结算</span> </a></li>
                                    <li<#if menuTag==123> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/reWorkStart"><span class="liActive">返工率</span> </a></li>
                                </ul>
                            </li>

                            <li <#if bigMenuTag==15> class="active"</#if>> <a href="#" <#if bigMenuTag==14> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>其他计件</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==151> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/otherPieceWorkStart"><span class="liActive">其他计件</span> </a></li>
                                </ul>
                            </li>
                        <#elseif role! == 'role7'>
                            <li <#if bigMenuTag==4> class="active"</#if>> <a href="#" <#if bigMenuTag==4> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>裁床报表</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==42> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/cutQueryLeakStart"><span class="liActive">单款报表查询</span> </a></li>
                                </ul>
                            </li>

                            <li <#if bigMenuTag==7> class="active"</#if>> <a href="#" <#if bigMenuTag==7> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>裁片&衣胚查询</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==71> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/cutStorageQueryStart"><span class="liActive">裁片单款库存</span> </a></li>
                                    <li<#if menuTag==72> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/embStorageQueryStart"><span class="liActive">衣胚单款库存</span> </a></li>
                                    <li<#if menuTag==73> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/embOutDetailStart"><span class="liActive">衣胚阶段出库</span> </a></li>
                                    <li<#if menuTag==74> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/embOutPackageDetailStart"><span class="liActive">衣胚出库详情</span> </a></li>
                                    <li<#if menuTag==75> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/queryEmbLeakStart"><span class="liActive">衣胚出库汇总</span> </a></li>
                                    <li<#if menuTag==76> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/cutLocationStart"><span class="liActive">裁片位置详情</span> </a></li>
                                </ul>
                            </li>

                            <li <#if bigMenuTag==9> class="active"</#if>> <a href="#" <#if bigMenuTag==9> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>IE基础资料</span> </a>
                                <ul class="nav lt">

                                    <li<#if menuTag==92> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/orderProcedureStart"><span class="liActive">订单工序</span> </a></li>

                                </ul>
                            </li>

                            <li <#if bigMenuTag==11> class="active"</#if>> <a href="#" <#if bigMenuTag==11> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>财务数据</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==111> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/empSalaryStart"><span class="liActive">员工个人产能</span> </a></li>
                                    <li<#if menuTag==112> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/groupSalaryStart"><span class="liActive">员工分组产能</span> </a></li>
                                    <li<#if menuTag==113> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/cutDailyDetailStart"><span class="liActive">裁床每日明细</span> </a></li>
                                    <li<#if menuTag==115> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/orderSalarySummaryStart"><span class="liActive">订单总工资</span> </a></li>
                                </ul>
                            </li>

                            <li <#if bigMenuTag==12> class="active"</#if>> <a href="#" <#if bigMenuTag==12> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>生产报表数据</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==121> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/procedureBalanceStart"><span class="liActive">订单工序平衡</span> </a></li>
                                    <li<#if menuTag==122> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/completionStart"><span class="liActive">完工结算</span> </a></li>
                                    <li<#if menuTag==123> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/reWorkStart"><span class="liActive">返工率</span> </a></li>
                                </ul>
                            </li>

                        <#elseif role! == 'role8'>

                            <li <#if bigMenuTag==1> class="active"</#if>> <a href="#" <#if bigMenuTag==1> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>订单模块</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==11> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/orderStart"><span class="liActive">订单信息</span> </a></li>
                                </ul>
                            </li>

                            <li <#if bigMenuTag==9> class="active"</#if>> <a href="#" <#if bigMenuTag==9> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>IE基础资料</span> </a>
                                <ul class="nav lt">

                                    <li<#if menuTag==92> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/orderProcedureStart"><span class="liActive">订单工序</span> </a></li>

                                </ul>
                            </li>

                            <li <#if bigMenuTag==10> class="active"</#if>> <a href="#" <#if bigMenuTag==10> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>手机计件</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==101> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/employeeStart"><span class="liActive">员工管理</span> </a></li>
                                    <li<#if menuTag==103> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/pieceWorkStatisticStart"><span class="liActive">产能统计</span> </a></li>
                                    <li<#if menuTag==104> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/pieceWorkManageStart"><span class="liActive">计件管理</span> </a></li>
                                    <li<#if menuTag==105> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/manualInputStart"><span class="liActive">手工入数</span> </a></li>
                                    <li<#if menuTag==109> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/hourEmpStart"><span class="liActive">时工入数</span> </a></li>
                                </ul>
                            </li>

                            <li <#if bigMenuTag==11> class="active"</#if>> <a href="#" <#if bigMenuTag==11> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>财务数据</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==111> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/empSalaryStart"><span class="liActive">员工个人产能</span> </a></li>
                                    <li<#if menuTag==112> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/groupSalaryStart"><span class="liActive">员工分组产能</span> </a></li>
                                    <li<#if menuTag==113> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/cutDailyDetailStart"><span class="liActive">裁床每日明细</span> </a></li>
                                    <li<#if menuTag==115> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/orderSalarySummaryStart"><span class="liActive">订单总工资</span> </a></li>
                                </ul>
                            </li>

                            <li <#if bigMenuTag==12> class="active"</#if>> <a href="#" <#if bigMenuTag==12> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>生产报表数据</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==121> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/procedureBalanceStart"><span class="liActive">订单工序平衡</span> </a></li>
                                    <li<#if menuTag==122> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/completionStart"><span class="liActive">完工结算</span> </a></li>
                                    <li<#if menuTag==123> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/reWorkStart"><span class="liActive">返工率</span> </a></li>
                                </ul>
                            </li>

                            <li <#if bigMenuTag==16> class="active"</#if>> <a href="#" <#if bigMenuTag==16> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>后整打菲</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==161> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/finishTailorStart"><span class="liActive">后整打菲</span> </a></li>
                                    <li<#if menuTag==162> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/finishTailorDeleteStart"><span class="liActive">菲票管理</span> </a></li>
                                    <li<#if menuTag==163> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/finishTailorLeakStart"><span class="liActive">后整打菲报表</span> </a></li>
                                </ul>
                            </li>

                        <#elseif role! == 'role9'>
                            <li <#if bigMenuTag==1> class="active"</#if>> <a href="#" <#if bigMenuTag==1> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>订单模块</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==11> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/orderStart"><span class="liActive">订单信息</span> </a></li>
                                </ul>
                            </li>
                            <li <#if bigMenuTag==4> class="active"</#if>> <a href="#" <#if bigMenuTag==4> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>裁床报表</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==41> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/cutReportStart"><span class="liActive">裁床单报表</span> </a></li>
                                    <li<#if menuTag==42> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/cutQueryLeakStart"><span class="liActive">单款报表查询</span> </a></li>
                                    <li<#if menuTag==43> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/cutMonthReportStart"><span class="liActive">月度报表查询</span> </a></li>
                                    <li<#if menuTag==44> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/otherCutReportStart"><span class="liActive">配料裁床单</span> </a></li>
                                    <li<#if menuTag==45> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/otherTailorMonthReportStart"><span class="liActive">配料月度报表</span> </a></li>
                                    <li<#if menuTag==46> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/getCutBedColorStart"><span class="liActive">裁床分组明细</span> </a></li>
                                </ul>
                            </li>

                            <li <#if bigMenuTag==7> class="active"</#if>> <a href="#" <#if bigMenuTag==7> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>裁片&衣胚查询</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==71> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/cutStorageQueryStart"><span class="liActive">裁片单款库存</span> </a></li>
                                    <li<#if menuTag==72> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/embStorageQueryStart"><span class="liActive">衣胚单款库存</span> </a></li>
                                    <li<#if menuTag==73> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/embOutDetailStart"><span class="liActive">衣胚阶段出库</span> </a></li>
                                    <li<#if menuTag==74> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/embOutPackageDetailStart"><span class="liActive">衣胚出库详情</span> </a></li>
                                    <li<#if menuTag==75> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/queryEmbLeakStart"><span class="liActive">衣胚出库汇总</span> </a></li>
                                    <li<#if menuTag==76> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/cutLocationStart"><span class="liActive">裁片位置详情</span> </a></li>
                                </ul>
                            </li>
                            <li <#if bigMenuTag==9> class="active"</#if>> <a href="#" <#if bigMenuTag==9> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>IE基础资料</span> </a>
                                <ul class="nav lt">

                                    <li<#if menuTag==92> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/orderProcedureStart"><span class="liActive">订单工序</span> </a></li>

                                </ul>
                            </li>
                            <li <#if bigMenuTag==10> class="active"</#if>> <a href="#" <#if bigMenuTag==10> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>手机计件</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==103> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/pieceWorkStatisticStart"><span class="liActive">产能统计</span> </a></li>
                                    <li<#if menuTag==104> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/pieceWorkManageStart"><span class="liActive">计件管理</span> </a></li>
                                    <li<#if menuTag==106> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/wrongStart"><span class="liActive">疵点代码</span> </a></li>
                                    <li<#if menuTag==105> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/manualInputStart"><span class="liActive">手工入数</span> </a></li>
                                    <li<#if menuTag==109> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/hourEmpStart"><span class="liActive">时工入数</span> </a></li>
                                    <li<#if menuTag==107> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/inspectionStart"><span class="liActive">抽查疵点统计</span> </a></li>
                                    <li<#if menuTag==108> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/sampleInspectionStart"><span class="liActive">QC疵点统计</span> </a></li>
                                </ul>
                            </li>

                            <li <#if bigMenuTag==11> class="active"</#if>> <a href="#" <#if bigMenuTag==11> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>财务数据</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==111> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/empSalaryStart"><span class="liActive">员工个人产能</span> </a></li>
                                    <li<#if menuTag==112> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/groupSalaryStart"><span class="liActive">员工分组产能</span> </a></li>
                                    <li<#if menuTag==115> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/orderSalarySummaryStart"><span class="liActive">订单总工资</span> </a></li>
                                </ul>
                            </li>

                            <li <#if bigMenuTag==12> class="active"</#if>> <a href="#" <#if bigMenuTag==12> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>生产报表数据</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==121> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/procedureBalanceStart"><span class="liActive">订单工序平衡</span> </a></li>
                                    <li<#if menuTag==122> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/completionStart"><span class="liActive">完工结算</span> </a></li>
                                    <li<#if menuTag==123> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/reWorkStart"><span class="liActive">返工率</span> </a></li>
                                </ul>
                            </li>

                            <li <#if bigMenuTag==14> class="active"</#if>> <a href="#" <#if bigMenuTag==14> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>下成品</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==141> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/unloadStart"><span class="liActive">下成品扫描</span> </a></li>
                                    <li<#if menuTag==142> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/unloadReportStart"><span class="liActive">下成品记录</span> </a></li>
                                </ul>
                            </li>

                        <#elseif role! == 'role10'>
                            <li <#if bigMenuTag==1> class="active"</#if>> <a href="#" <#if bigMenuTag==1> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>订单模块</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==11> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/orderStart"><span class="liActive">订单信息</span> </a></li>
                                </ul>
                            </li>

                            <li <#if bigMenuTag==4> class="active"</#if>> <a href="#" <#if bigMenuTag==4> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>裁床报表</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==41> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/cutReportStart"><span class="liActive">裁床单报表</span> </a></li>
                                    <li<#if menuTag==42> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/cutQueryLeakStart"><span class="liActive">单款报表查询</span> </a></li>
                                    <li<#if menuTag==43> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/cutMonthReportStart"><span class="liActive">月度报表查询</span> </a></li>
                                    <li<#if menuTag==44> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/otherCutReportStart"><span class="liActive">配料裁床单</span> </a></li>
                                    <li<#if menuTag==45> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/otherTailorMonthReportStart"><span class="liActive">配料月度报表</span> </a></li>
                                    <li<#if menuTag==46> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/getCutBedColorStart"><span class="liActive">裁床分组明细</span> </a></li>
                                </ul>
                            </li>

                            <li <#if bigMenuTag==7> class="active"</#if>> <a href="#" <#if bigMenuTag==7> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>裁片&衣胚查询</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==71> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/cutStorageQueryStart"><span class="liActive">裁片单款库存</span> </a></li>
                                    <li<#if menuTag==72> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/embStorageQueryStart"><span class="liActive">衣胚单款库存</span> </a></li>
                                    <li<#if menuTag==73> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/embOutDetailStart"><span class="liActive">衣胚阶段出库</span> </a></li>
                                    <li<#if menuTag==74> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/embOutPackageDetailStart"><span class="liActive">衣胚出库详情</span> </a></li>
                                    <li<#if menuTag==75> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/queryEmbLeakStart"><span class="liActive">衣胚出库汇总</span> </a></li>
                                    <li<#if menuTag==76> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/cutLocationStart"><span class="liActive">裁片位置详情</span> </a></li>
                                </ul>
                            </li>


                            <li <#if bigMenuTag==8> class="active"</#if>> <a href="#" <#if bigMenuTag==8> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>面料管理</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==81> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/fabricStart"><span class="liActive">面料录入</span> </a></li>
                                    <li<#if menuTag==85> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/fabricQueryStart"><span class="liActive">面料库存</span> </a></li>
                                    <li<#if menuTag==86> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/fabricOutRecordStart"><span class="liActive">面料出库详情</span> </a></li>
                                    <li<#if menuTag==811> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/fabricLeakQueryStart"><span class="liActive">季度面料查询</span> </a></li>
                                    <li<#if menuTag==812> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/fabricDetailStart"><span class="liActive">单款面料详情</span> </a></li>
                                </ul>
                            </li>


                            <li <#if bigMenuTag==9> class="active"</#if>> <a href="#" <#if bigMenuTag==9> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>IE基础资料</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==91> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/procedureTemplateStart"><span class="liActive">工序数据库</span> </a></li>
                                    <li<#if menuTag==92> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/orderProcedureStart"><span class="liActive">订单工序</span> </a></li>
                                </ul>
                            </li>

                            <li <#if bigMenuTag==10> class="active"</#if>> <a href="#" <#if bigMenuTag==10> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>手机计件</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==101> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/employeeStart"><span class="liActive">员工管理</span> </a></li>
                                    <li<#if menuTag==102> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/dispatchStart"><span class="liActive">工序分配</span> </a></li>
                                    <li<#if menuTag==103> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/pieceWorkStatisticStart"><span class="liActive">产能统计</span> </a></li>
                                    <li<#if menuTag==104> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/pieceWorkManageStart"><span class="liActive">计件管理</span> </a></li>
                                    <li<#if menuTag==105> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/manualInputStart"><span class="liActive">手工入数</span> </a></li>
                                    <#--<li<#if menuTag==59> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/pieceWorkDetailQueryStart"><span class="liActive">计件明细查询</span> </a></li>-->
                                    <li<#if menuTag==106> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/wrongStart"><span class="liActive">疵点代码</span> </a></li>
                                    <li<#if menuTag==107> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/inspectionStart"><span class="liActive">抽查疵点统计</span> </a></li>
                                    <li<#if menuTag==108> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/sampleInspectionStart"><span class="liActive">QC疵点统计</span> </a></li>
                                </ul>
                            </li>

                            <li <#if bigMenuTag==11> class="active"</#if>> <a href="#" <#if bigMenuTag==11> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>财务数据</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==111> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/empSalaryStart"><span class="liActive">员工个人产能</span> </a></li>
                                    <li<#if menuTag==112> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/groupSalaryStart"><span class="liActive">员工分组产能</span> </a></li>
                                    <li<#if menuTag==113> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/cutDailyDetailStart"><span class="liActive">裁床每日明细</span> </a></li>
                                    <li<#if menuTag==115> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/orderSalarySummaryStart"><span class="liActive">订单总工资</span> </a></li>
                                </ul>
                            </li>

                            <li <#if bigMenuTag==12> class="active"</#if>> <a href="#" <#if bigMenuTag==12> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>生产报表数据</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==121> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/procedureBalanceStart"><span class="liActive">订单工序平衡</span> </a></li>
                                    <li<#if menuTag==122> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/completionStart"><span class="liActive">完工结算</span> </a></li>
                                    <li<#if menuTag==123> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/reWorkStart"><span class="liActive">返工率</span> </a></li>
                                </ul>
                            </li>

                        <#elseif role! == 'role11'>
                            <li <#if bigMenuTag==1> class="active"</#if>> <a href="#" <#if bigMenuTag==1> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>订单模块</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==11> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/orderStart"><span class="liActive">订单信息</span> </a></li>
                                </ul>
                            </li>

                            <li <#if bigMenuTag==4> class="active"</#if>> <a href="#" <#if bigMenuTag==4> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>裁床报表</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==41> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/cutReportStart"><span class="liActive">裁床单报表</span> </a></li>
                                    <li<#if menuTag==42> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/cutQueryLeakStart"><span class="liActive">单款报表查询</span> </a></li>
                                    <li<#if menuTag==43> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/cutMonthReportStart"><span class="liActive">月度报表查询</span> </a></li>
                                    <li<#if menuTag==44> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/otherCutReportStart"><span class="liActive">配料裁床单</span> </a></li>
                                    <li<#if menuTag==45> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/otherTailorMonthReportStart"><span class="liActive">配料月度报表</span> </a></li>
                                    <li<#if menuTag==46> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/getCutBedColorStart"><span class="liActive">裁床分组明细</span> </a></li>
                                </ul>
                            </li>

                            <li <#if bigMenuTag==8> class="active"</#if>> <a href="#" <#if bigMenuTag==8> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>面料管理</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==81> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/fabricStart"><span class="liActive">面料录入</span> </a></li>
                                    <li<#if menuTag==83> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/fabricInStoreStart"><span class="liActive">面料入仓</span> </a></li>
                                    <li<#if menuTag==84> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/fabricOutStoreStart"><span class="liActive">面料出仓</span> </a></li>
                                    <li<#if menuTag==85> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/fabricQueryStart"><span class="liActive">面料库存</span> </a></li>
                                    <li<#if menuTag==86> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/fabricOutRecordStart"><span class="liActive">面料出库详情</span> </a></li>
                                    <li<#if menuTag==811> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/fabricLeakQueryStart"><span class="liActive">季度面料查询</span> </a></li>
                                    <li<#if menuTag==812> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/fabricDetailStart"><span class="liActive">单款面料详情</span> </a></li>
                                    <#--<li<#if menuTag==82> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/innerBoardStart"><span class="liActive">内版录入</span> </a></li>-->
                                    <#--<li<#if menuTag==87> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/innerBoardInStart"><span class="liActive">内板入库</span> </a></li>-->
                                    <#--<li<#if menuTag==88> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/innerBoardOutStart"><span class="liActive">内板出库</span> </a></li>-->
                                    <#--<li<#if menuTag==89> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/innerBoardQueryStart"><span class="liActive">内板库存</span> </a></li>-->
                                    <#--<li<#if menuTag==810> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/innerBoardOutRecordStart"><span class="liActive">内板出库详情</span> </a></li>-->
                                </ul>
                            </li>

                        <#elseif role! == 'role12'>
                            <li <#if bigMenuTag==1> class="active"</#if>> <a href="#" <#if bigMenuTag==1> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>订单模块</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==11> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/orderStart"><span class="liActive">订单信息</span> </a></li>
                                </ul>
                            </li>

                            <li <#if bigMenuTag==17> class="active"</#if>> <a href="#" <#if bigMenuTag==4> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>花片收发管理</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==171> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/opaStart"><span class="liActive">花片出厂</span> </a></li>
                                    <li<#if menuTag==172> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/otherOpaStart"><span class="liActive">士杯出厂</span> </a></li>
                                    <li<#if menuTag==173> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/opaBackInputStart"><span class="liActive">花片回厂</span> </a></li>
                                </ul>
                            </li>

                            <li <#if bigMenuTag==4> class="active"</#if>> <a href="#" <#if bigMenuTag==4> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>裁床报表</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==41> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/cutReportStart"><span class="liActive">裁床单报表</span> </a></li>
                                    <li<#if menuTag==42> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/cutQueryLeakStart"><span class="liActive">单款报表查询</span> </a></li>
                                    <li<#if menuTag==43> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/cutMonthReportStart"><span class="liActive">月度报表查询</span> </a></li>
                                    <li<#if menuTag==44> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/otherCutReportStart"><span class="liActive">配料裁床单</span> </a></li>
                                    <li<#if menuTag==45> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/otherTailorMonthReportStart"><span class="liActive">配料月度报表</span> </a></li>
                                    <li<#if menuTag==46> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/getCutBedColorStart"><span class="liActive">裁床分组明细</span> </a></li>
                                </ul>
                            </li>

                            <li <#if bigMenuTag==7> class="active"</#if>> <a href="#" <#if bigMenuTag==7> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>裁片&衣胚查询</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==71> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/cutStorageQueryStart"><span class="liActive">裁片单款库存</span> </a></li>
                                    <li<#if menuTag==72> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/embStorageQueryStart"><span class="liActive">衣胚单款库存</span> </a></li>
                                    <li<#if menuTag==73> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/embOutDetailStart"><span class="liActive">衣胚阶段出库</span> </a></li>
                                    <li<#if menuTag==74> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/embOutPackageDetailStart"><span class="liActive">衣胚出库详情</span> </a></li>
                                    <li<#if menuTag==75> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/queryEmbLeakStart"><span class="liActive">衣胚出库汇总</span> </a></li>
                                    <li<#if menuTag==76> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/cutLocationStart"><span class="liActive">裁片位置详情</span> </a></li>
                                </ul>
                            </li>

                            <li <#if bigMenuTag==8> class="active"</#if>> <a href="#" <#if bigMenuTag==8> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>面料管理</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==81> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/fabricStart"><span class="liActive">面料录入</span> </a></li>
                                    <li<#if menuTag==83> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/fabricInStoreStart"><span class="liActive">面料入仓</span> </a></li>
                                    <li<#if menuTag==84> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/fabricOutStoreStart"><span class="liActive">面料出仓</span> </a></li>
                                    <li<#if menuTag==85> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/fabricQueryStart"><span class="liActive">面料库存</span> </a></li>
                                    <li<#if menuTag==86> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/fabricOutRecordStart"><span class="liActive">面料出库详情</span> </a></li>
                                    <li<#if menuTag==811> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/fabricLeakQueryStart"><span class="liActive">季度面料查询</span> </a></li>
                                    <li<#if menuTag==812> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/fabricDetailStart"><span class="liActive">单款面料详情</span> </a></li>
                                </ul>
                            </li>

                            <li <#if bigMenuTag==9> class="active"</#if>> <a href="#" <#if bigMenuTag==9> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>IE基础资料</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==93> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/procedureProgressStart"><span class="liActive">工序进度</span> </a></li>
                                    <li<#if menuTag==95> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/metreStart"><span class="liActive">节拍查询</span> </a></li>
                                    <li<#if menuTag==97> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/groupPlanStart"><span class="liActive">计划产能</span> </a></li>
                                    <li<#if menuTag==98> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/completionRateStart"><span class="liActive">达成率</span> </a></li>
                                </ul>
                            </li>

                            <li <#if bigMenuTag==11> class="active"</#if>> <a href="#" <#if bigMenuTag==11> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>财务数据</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==111> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/empSalaryStart"><span class="liActive">员工个人产能</span> </a></li>
                                    <li<#if menuTag==112> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/groupSalaryStart"><span class="liActive">员工分组产能</span> </a></li>
                                    <li<#if menuTag==113> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/cutDailyDetailStart"><span class="liActive">裁床每日明细</span> </a></li>
                                    <li<#if menuTag==115> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/orderSalarySummaryStart"><span class="liActive">订单总工资</span> </a></li>
                                </ul>
                            </li>

                            <li <#if bigMenuTag==12> class="active"</#if>> <a href="#" <#if bigMenuTag==12> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>生产报表数据</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==121> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/procedureBalanceStart"><span class="liActive">订单工序平衡</span> </a></li>
                                    <li<#if menuTag==122> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/completionStart"><span class="liActive">完工结算</span> </a></li>
                                    <li<#if menuTag==123> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/reWorkStart"><span class="liActive">返工率</span> </a></li>
                                </ul>
                            </li>
                        <#elseif role! == 'role13'>
                            <li <#if bigMenuTag==13> class="active"</#if>> <a href="#" <#if bigMenuTag==13> class="active"</#if>> <i class="fa fa-clock-o fa-flip-horizontal icon"></i>
                                    <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span>
                                    <span>工厂基础信息</span> </a>
                                <ul class="nav lt">
                                    <li<#if menuTag==131> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/storeHouseStart"> <span class="liActive">仓库信息</span> </a></li>
                                    <li<#if menuTag==132> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/embStoreStart"> <span class="liActive">衣胚仓库</span> </a></li>
                                    <li<#if menuTag==133> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/customerStart"> <span class="liActive">顾客信息</span> </a></li>
                                    <li<#if menuTag==134> class="active" style="background:rgb(45, 202, 147)" </#if>><a href="/erp/userStart"> <span class="liActive">用户信息</span> </a></li>
                                </ul>
                            </li>

                        </#if>

                    </ul>
                </nav>
            </div>
        </section>

        <footer class="footer lt hidden-xs b-t b-dark">
            <a href="#nav" data-toggle="class:nav-xs" class="pull-right btn btn-sm btn-dark btn-icon"> <i
                    class="fa fa-angle-left text"></i> <i class="fa fa-angle-right text-active"></i> </a>
        </footer>
    </section>
</aside>

</#macro>

<style>
    .liActive {
        margin-left:40px;
        font-family: PingFangSC-Medium,sans-serif;
    }
</style>