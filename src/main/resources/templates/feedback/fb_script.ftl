<#macro script>
<head>
    <meta charset="utf-8"/>
    <title>恒达精益管理</title>
    <meta name="description"
          content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>

    <link rel="stylesheet" href="/css/app.v2.css" type="text/css"/>
    <link href="/css/select2.css" rel="stylesheet"/>
    <link rel="stylesheet" href="/css/jquery.autocompleter.css" type="text/css">
    <link rel="stylesheet" href="/css/jquery.searchableSelect.css" type="text/css">
    <link rel="stylesheet" href="/css/flatpickr.min.css" type="text/css">
    <link rel="stylesheet" href="/css/darktooltip.min.css">
    <link rel="stylesheet" href="/css/sweetalert.css" type="text/css">
    <link rel="stylesheet" href="/css/simplePagination.css" type="text/css">
    <link rel="stylesheet" href="/css/popModal.css">
    <link rel="stylesheet" href="/css/jquery.webui-popover.css" type="text/css">
    <link rel="stylesheet" href="/css/flatpickr.min.css" type="text/css">
    <link rel="stylesheet" href="/css/jquery.dropdown.css" type="text/css">
    <link rel="stylesheet" href="/css/chosen.css?t=${currentDate?c}" type="text/css">
    <link rel="stylesheet" href="/css/autoSearch.css?t=${currentDate?c}" type="text/css">
    <link rel="stylesheet" href="/css/dataTables.bootstrap.css" type="text/css">
    <link rel="stylesheet" href="/css/layui.css" type="text/css">
    <link rel="stylesheet" href="/css/autocomplete.css" type="text/css">
<#--<link rel="stylesheet" href="http://cdn.datatables.net/plug-ins/28e7751dbec/integration/bootstrap/3/dataTables.bootstrap.css" type="text/css">-->

    <style type="text/css">

        /*@font-face {*/
            /*font-family: 'PingFangSC-Semibold';*/
            /*src: url('/fonts/苹方黑体-中粗-简.ttf');*/
            /*font-family: 'PingFangSC-Regular';*/
            /*src: url('/fonts/苹方黑体-准-简.ttf');*/
            /*font-family: 'PingFangSC-Thin';*/
            /*src: url('/fonts/苹方黑体-纤细-简.ttf');*/
            /*font-family: 'PingFangSC-Light';*/
            /*src: url('/fonts/苹方黑体-细-简.ttf');*/
            /*font-family: 'PingFangSC-Ultralight';*/
            /*src: url('/fonts/苹方黑体-极细-简.ttf');*/
            /*font-family: 'PingFangSC-Medium';*/
            /*src: url('/fonts/苹方黑体-中黑-简.ttf');*/

        /*}*/

        body {
            font-family: "微软雅黑";
        }

        button {
            outline:none;
            background:rgb(45, 202, 147);
            opacity:0.86;
            color: white;
            font-family: PingFangSC-Semibold, sans-serif;
        }

        button:focus {
            outline:none !important
        }

        .sweet-overlay{z-index: 999999999 !important;}

        .sweet-alert{z-index: 1000000000}


        body a:hover {
            text-decoration: none;
        }

    </style>
    <script src="/js/common/jquery.js"></script>
    <script src="/js/common/jquery-1.11.2.min.js"></script>

    <script src="/js/common/jquery-migrate-1.2.1.js"></script>
    <script src="/js/common/jquery-ui.js"></script>

    <script src="/js/common/jquery.blockUI.js"></script>
    <script src="/js/common/select2.min.js"></script>
    <script src="/js/common/jquery.cookie.js"></script>
    <script src="/js/common/jquery-form.js"></script>
    <script src="/js/common/app.v2.js"></script>
    <script src="/js/common/jquery.autocompleter.js"></script>
    <script src="/js/common/sweetalert.min.js"></script>
    <script src="/js/common/jquery.flot.min.js"></script>
    <script src="/js/common/jquery.flot.pie.min.js"></script>
    <script src="/js/common/jquery.simplePagination.js"></script>
    <script src="/js/common/flatpickr.min.js"></script>
    <script src="/js/common/jquery.searchableSelect.js"></script>
    <script src="/js/common/jquery.dataTables.min.js"></script>
    <script src="/js/common/dataTables.bootstrap.min.js"></script>
    <script src="/js/common/globalSizeSort.js" type="text/javascript"></script>
    <script src="/js/common/moment.min.js" type="text/javascript"></script>
    <script src="/js/common/transDate.js" type="text/javascript"></script>
    <script src="/js/common/layui.all.js?t=${currentDate?c}" type="text/javascript"></script>
    <script src="/js/ext/autocomplete.js" type="text/javascript"></script>
    <script src="/js/common/redirect.js" type="text/javascript"></script>

    <script>
        layui.config({
            base: '/resources/static/js/common/',   // 模块目录
        });
    </script>

</head>
</#macro>