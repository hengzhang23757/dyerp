<#macro head>
<script src="/js/common/jquery.blockUI.js" type="text/javascript" ></script>

<header class="bg-dark dk header navbar navbar-fixed-top-xs">
    <div class="navbar-header aside-md">
        <#--<a class="btn btn-link visible-xs" data-toggle="class:nav-off-screen" data-target="#nav">-->
            <#--<i class="fa fa-bars"></i>-->
        <#--</a>-->
        <a href="/erp/homepage" class="navbar-brand" style="font-size: 24px;color:rgb(45, 202, 147);margin-left: 40px;" data-toggle="fullscreen">恒达精益管理</a>
        <a class="btn btn-link visible-xs" data-toggle="dropdown" data-target=".nav-user">
            <i class="fa fa-cog"></i>
        </a>
    </div>
    <ul class="nav navbar-nav navbar-right hidden-xs nav-user">
        <input hidden id="loginUserName" value="${userName!""}">
        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"> ${userName!""} <b
                class="caret"></b> </a>
            <ul class="dropdown-menu animated fadeInRight">
                <span class="arrow top"></span>
                <li><a onclick="updatePwd()" href="#">修改密码</a></li>
                <li><a onclick="loginOut()" href="#">退出</a></li>
            </ul>
        </li>
    </ul>
    <div id="pwdEdit" style="display: none;cursor:default">
        <div style="overflow-x: auto;overflow-y:auto;">
            <div style="text-align: right;margin-bottom:20px">
                <a id="pwdEditNo" href="#" style="font-size:20px"><i class="fa fa-times icon" style="color: rgb(182,182,182)"></i></a>
            </div>
            <table style="border-collapse:separate;border-spacing: 10px;width: 100%;">
                <tr>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">原密码</label>
                    </td>
                    <td>
                        <input id="originPassWord" class="form-control" style="width: 100%;margin-bottom: 15px;border-top: none;border-right: none;border-left: none;" autocomplete="off" type="password" placeholder="请输入原密码">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">新密码</label>
                    </td>
                    <td>
                        <input id="newPassWord" class="form-control" style="margin-bottom: 15px;width: 100%;border-top: none;border-right: none;border-left: none;" autocomplete="off" type="password" placeholder="请输入新密码">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">新密码</label>
                    </td>
                    <td>
                        <input id="secondNewPassWord" class="form-control" style="margin-bottom: 15px;width: 100%;border-top: none;border-right: none;border-left: none;" autocomplete="off" type="password" placeholder='请再次输入新密码'>
                    </td>
                </tr>
            </table>
        </div>
        <div class="col-sm-12" style="padding-top: 10px">
            <button id="pwdEditYes" class="btn btn-s-lg" style="border-radius: 5px;"  style="text-align: center;">保存</button>
        </div>
    </div>
</header>
<script>
    function loginOut() {
        swal({
            title: "",
            text: "<span style=\"font-weight:bolder;font-size: 20px\">你确定退出么？</span>",
            type: "warning",
            html:true,
            showCancelButton: true,
            closeOnConfirm: false,
            confirmButtonText: "确定",
            cancelButtonText:"我再想想",
            confirmButtonColor: "#ec6c62",
            showLoaderOnConfirm: false
        }, function() {
            window.location.href = "/";
        });
    }

    function updatePwd() {
        $.blockUI({
            css: {
                width: '30%',
                top: '15%',
                border: 'none',
                padding: '15px',
                backgroundColor: '#fff',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: 1,
                color: '#000'
            },
            message: $('#pwdEdit')
        });

        $("#pwdEditYes").unbind("click").bind("click", function () {
            if($("#originPassWord").val().trim()=="") {
                swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入原密码！</span>",html: true});
                return false;
            }
            if($("#newPassWord").val().trim()=="") {
                swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请输入新密码！</span>",html: true});
                return false;
            }
            if($("#secondNewPassWord").val().trim()=="") {
                swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">请再次输入新密码！</span>",html: true});
                return false;
            }
            if($("#newPassWord").val().trim()!=$("#secondNewPassWord").val().trim()) {
                swal({type:"warning",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">两次输入新密码不一致！</span>",html: true});
                return false;
            }
            $.ajax({
                url: "updateUser",
                type: 'GET',
                data: {
                    originPassWord:$("#originPassWord").val(),
                    newPassWord:$("#newPassWord").val(),
                    userName:$("#loginUserName").val()
                },
                success: function (data) {
                    if(data == 1) {
                        $("#originPassWord").val("");
                        $("#secondNewPassWord").val("");
                        $("#newPassWord").val("");
                        $.unblockUI();
                        swal({
                            type:"success",
                            title:"",
                            text: "<span style=\"font-weight:bolder;font-size: 20px\">恭喜你，保存成功！</span>",
                            html: true
                        });
                    }else if(data == -1){
                        swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，原密码错误！</span>",html: true});
                    }else {
                        swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">对不起，保存失败！</span>",html: true});
                    }
                },
                error: function () {
                    swal({type:"error",title:"",text: "<span style=\"font-weight:bolder;font-size: 20px\">服务发生未知错误～</span>",html: true});
                }
            });
        });

        $("#pwdEditNo").unbind("click").bind("click", function () {
            $("#originPassWord").val("");
            $("#secondNewPassWord").val("");
            $("#newPassWord").val("");
            $.unblockUI();
        });
    }
</script>
</#macro>


