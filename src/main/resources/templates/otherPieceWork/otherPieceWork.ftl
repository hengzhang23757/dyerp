<!DOCTYPE html>
<html lang = "en" class = "app">
<#include "../feedback/fb_script.ftl">
<@script> </@script>
<body>
<section class = "vbox">
    <input type = "hidden" value = "${basePath}" id = "basePath"/>
    <input  type="hidden" value="${userName!?string}"  id="userName"/>
    <input  type="hidden" value="${employeeName!?string}"  id="employeeName"/>
    <input  type="hidden" value="${groupName!?string}"  id="groupName"/>
    <section>
            <section class = "hbox stretch">
                <#include "fb_otherPieceWork.ftl">
                <@search></@search>
            </section>
    </section>
</section>
</body>
</html>