<#macro search>
    <section id="content">
        <section class="vbox">
            <section class="scrollable" style="padding-left: 0px; padding-right: 0px">
                <div class="form-inline">
                    <div class="col-md-12">
                        <div style="margin-left: 10px;height: 120px;width: 95%;text-align: center;vertical-align: middle;background:rgb(0, 150, 136);border-radius: 15px;margin-top: 20px">
                            <label style="color:white;font-size:35px;font-family: PingFangSC-Semibold,sans-serif;width:100%;height: 100%;vertical-align: middle;margin-top: 25px" id="selectProcedureNumber" onclick="procedureSelect(this)">点击扫描工序号</label>
                        </div>
                        <div style="padding-top: 5px; width: 100%;">
                            <section class="panel panel-default" style="margin-top: 10px;">
                                <div class="panel-body">
                                    <table style="border-collapse:separate;border-spacing: 10px;" id="baseInfo">
                                        <tr>
                                            <td style="text-align: right;margin-bottom: 15px;">
                                                <label class="control-label" style="font-family: PingFangSC-Semibold,sans-serif;color:black;font-size:15px">配扎并下货</label>
                                            </td>
                                            <td>
                                                <form class="layui-form">
                                                    <div class="layui-form-item" style="margin-left: -100px; margin-bottom: 0px">
                                                        <div class="layui-input-block">
                                                            <input type="checkbox" style="width: 60px;" name="isOutBound" lay-skin="switch" lay-filter="outBound" lay-text="开|关">
                                                        </div>
                                                    </div>
                                                </form>
                                            </td>
                                            <td style="text-align: right;margin-bottom: 15px;" id="groupLable" hidden>
                                                <label class="control-label" style="font-family: PingFangSC-Semibold,sans-serif;color:black;font-size:15px">下货组名</label>
                                            </td>
                                            <td style="margin-bottom: 15px;" id="groupNameSelectTd" hidden>
                                                <select id="groupNameSelect" class="form-control" autocomplete="off" style="width: 150px">
                                                    <option value="车缝A组">车缝A组</option>
                                                    <option value="车缝B组">车缝B组</option>
                                                    <option value="车缝C组">车缝C组</option>
                                                    <option value="车缝D组">车缝D组</option>
                                                    <option value="车缝E组">车缝E组</option>
                                                    <option value="车缝F组">车缝F组</option>
                                                    <option value="车缝G组">车缝G组</option>
                                                    <option value="车缝H组">车缝H组</option>
                                                    <option value="外厂1">外厂1</option>
                                                    <option value="外厂2">外厂2</option>
                                                    <option value="外厂3">外厂3</option>
                                                    <option value="外厂4">外厂4</option>
                                                    <option value="外厂5">外厂5</option>
                                                </select>
                                            </td>
                                            <td style="text-align: right;margin-bottom: 15px;">
                                                <label class="control-label" style="font-family: PingFangSC-Semibold,sans-serif;color:black;font-size:15px">保留历史计件信息</label>
                                            </td>
                                            <td>
                                                <form class="layui-form">
                                                    <div class="layui-form-item" style="margin-left: -100px; margin-bottom: 0px">
                                                        <div class="layui-input-block">
                                                            <input type="checkbox" style="width: 60px;" name="isDisappear" lay-skin="switch" lay-filter="isDisappear" lay-text="开|关">
                                                        </div>
                                                    </div>
                                                </form>
                                            </td>
                                        </tr>
                                    </table>
                                </div>

                            </section>
                        </div>
                        <div style="padding-top: 5px; width: 100%;">
                            <section class="panel panel-default" style="margin-top: 10px;">
                                <div class="panel-body">
                                    <table style="border-collapse:separate;border-spacing: 10px;" id="baseInfo">
                                        <tr>
                                            <td style="text-align: right;margin-bottom: 15px;">
                                                <label class="control-label" style="font-family: PingFangSC-Semibold,sans-serif;color:black;font-size:15px">客户</label>
                                            </td>
                                            <td style="margin-bottom: 15px;">
                                                <input id="customerName" class="form-control" autocomplete="off" style="width: 100%">
                                            </td>
                                            <td style="text-align: right;margin-bottom: 15px;">
                                                <label class="control-label" style="font-family: PingFangSC-Semibold,sans-serif;color:black;font-size:15px">款号</label>
                                            </td>
                                            <td style="margin-bottom: 15px;">
                                                <input id="orderName" class="form-control" autocomplete="off" style="width: 100%">
                                            </td>
                                            <td style="text-align: right;margin-bottom: 15px;">
                                                <label class="control-label" style="font-family: PingFangSC-Semibold,sans-serif;color:black;font-size:15px">扎号</label>
                                            </td>
                                            <td style="margin-bottom: 15px;">
                                                <input id="packageNumber" class="form-control" autocomplete="off" style="width: 100%">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: right;margin-bottom: 15px;">
                                                <label class="control-label" style="font-family: PingFangSC-Semibold,sans-serif;color:black;font-size:15px">颜色</label>
                                            </td>
                                            <td style="margin-bottom: 15px;">
                                                <input id="colorName" class="form-control" autocomplete="off" style="width: 100%">
                                            </td>
                                            <td style="text-align: right;margin-bottom: 15px;">
                                                <label class="control-label" style="font-family: PingFangSC-Semibold,sans-serif;color:black;font-size:15px">尺码</label>
                                            </td>
                                            <td style="margin-bottom: 15px;">
                                                <input id="sizeName" class="form-control" autocomplete="off" style="width: 100%">
                                            </td>
                                            <td style="text-align: right;margin-bottom: 15px;">
                                                <label class="control-label" style="font-family: PingFangSC-Semibold,sans-serif;color:black;font-size:15px">数量</label>
                                            </td>
                                            <td style="margin-bottom: 15px;">
                                                <input id="thisLayerCount" class="form-control" autocomplete="off" style="width: 100%">
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </section>
                        </div>
                        <div style="padding-top: 10px; width: 100%;">
                            <section class="panel panel-default">
                                <div class="panel-body" style="text-align: left">
                                    <div class="row" style="margin-left: 10px;margin-top: 10px;">
                                        <span style="font-family: PingFangSC-Semibold,sans-serif;color:black;font-size:20px">请扫描裁片二维码</span>
                                    </div>
                                    <div class="row" style="margin-left: 10px;margin-top: 10px;">
                                        <textarea id="tailorQcode" rows="4" class="form-control" style="background: rgb(248,248,248);width: 90%;"></textarea>
                                    </div>
                                    <div class="row" style="margin-top: 10px;text-align: right">
                                        <span id="scanNum" style="font-family: PingFangSC-Semibold;font-size:22px;margin-right: 20px;">已扫描0件</span>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                    <div class="col-md-12" style="float: right" id="pieceWorkDetailDiv"></div>
                </div>
                <#include "fb_procedureSelect.ftl">
                <@entities></@entities>
            </section>
        </section>
        <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen"
           data-target="#nav"></a>
    </section>
    <aside class="bg-light lter b-l aside-md hide" id="notes">
        <div class="wrapper">Notification</div>
    </aside>
    <link rel="stylesheet" href="/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="/css/b.tabs.css" type="text/css">
    <link rel="stylesheet" href="/css/layui.css" type="text/css">
    <script src="/js/common/jquery.blockUI.js" type="text/javascript" ></script>
    <script src="/js/common/sweetalert2.js" type="text/javascript" ></script>
    <script src="/js/otherPieceWork/otherPieceWork.js?t=${currentDate?c}"></script>
</#macro>

<style>
    button {
        background:rgb(255, 117, 0);
        opacity:0.86;
        color: white;

        font-family: PingFangSC-Semibold, sans-serif;
    }
    .layui-form-switch{
        width: 50px;
        height: 40px;
    }
</style>

