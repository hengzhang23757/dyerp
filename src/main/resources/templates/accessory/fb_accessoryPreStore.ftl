<#macro search>
<section id="content">
    <section class="vbox">
        <section class="scrollable">

            <div class="layui-row">
                <form class="layui-form" style="margin: 0 auto;padding-top: 3px;" lay-filter="baseInfo">
                    <table>
                        <tr>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label">名称</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" name="accessoryName" id="accessoryName" autocomplete="off" class="layui-input">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label">编号</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" name="accessoryNumber" id="accessoryNumber" autocomplete="off" class="layui-input">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label"></label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <div class="layui-input-inline">
                                    <button class="layui-btn" lay-submit lay-filter="searchBeat">搜索</button>
                                </div>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>

            <table id="dataTable" lay-filter="dataTable"></table>
            <div id="combineDiv" class="layui-row" style="display: none">
                <table id="combineTable" lay-filter="combineTable"></table>
            </div>

            <script type="text/html" id="toolbarTop">
                <div class="layui-btn-container">
                    <button class="layui-btn layui-btn-sm" lay-event="clearFilter">清除所有筛选</button>
                    <button class="layui-btn layui-btn-sm" lay-event="exportExcel">导出</button>
                    <button class="layui-btn layui-btn-sm" lay-event="addNew">建档</button>
                    <button class="layui-btn layui-btn-sm" lay-event="combine">组合下单</button>
                </div>
            </script>

            <script type="text/html" id="barTop">
                <a class="layui-btn layui-btn-xs" lay-event="add">新增</a>
                <a class="layui-btn layui-btn-xs layui-btn-warm" lay-event="buy">采购</a>
                <a class="layui-btn layui-btn-xs layui-btn-danger" lay-event="delete">删除</a>
            </script>

            <div id="preStoreAdd" class="layui-row" style="display: none">
                <form class="layui-form" style="margin: 0 auto;padding-top: 3px;" lay-filter="preAccessoryAdd">
                    <table>
                        <tr>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label" style="width: 100px">名称</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" id="accessoryNameAdd" autocomplete="off" class="layui-input">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label" style="width: 100px">类别</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <select type="text" id="accessoryTypeAdd" autocomplete="off" class="layui-input" lay-filter="demo">
                                    <option value="">请选择</option>
                                    <option value="FX">FX/线</option>
                                    <option value="FN">FN/纽扣</option>
                                    <option value="FZ">FZ/织带</option>
                                    <option value="FL">FL/拉链</option>
                                    <option value="FP">FP/饰品</option>
                                    <option value="FM">FM/唛</option>
                                    <option value="FS">FS/商标</option>
                                    <option value="FT">FT/包装</option>
                                    <option value="FR">FR/打包</option>
                                    <option value="FR">FR/打包</option>
                                </select>
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label" style="width: 100px">编号</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" id="accessoryNumberAdd" autocomplete="off" class="layui-input">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label" style="width: 100px">规格</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" id="specificationAdd" autocomplete="off" class="layui-input">
                            </td>
                        </tr><tr>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label" style="width: 100px">颜色</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" id="accessoryColorAdd" autocomplete="off" class="layui-input">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label" style="width: 100px">单位</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" id="accessoryUnitAdd" autocomplete="off" class="layui-input">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label" style="width: 100px">供应商</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <div id="supplierAdd" class="xm-select-demo"></div>
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label" style="width: 100px">单价</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" id="priceAdd" autocomplete="off" class="layui-input">
                            </td>
                        </tr>
                    </table>
                </form>
            </div>

        </section>
    </section>
</section>

    <link rel="stylesheet" href="/css/layui.css" type="text/css">
    <link rel="stylesheet" href="/css/step.css" type="text/css">
    <link rel="stylesheet" href="/css/soulTable.css" type="text/css">
    <link rel="stylesheet" href="/css/publish.css" type="text/css">
    <link rel="stylesheet" href="/css/cropper.css" type="text/css">
    <link rel="stylesheet" href="/css/selectize.bootstrap3.css" type="text/css">
    <link rel="stylesheet" href="/css/zoom.css" type="text/css">

    <#--<script src="/js/common/layui.js" type="text/javascript"></script>-->
    <script src="/js/common/layer.js"></script>
    <script src="/js/common/laydate.js" type="text/javascript" ></script>
    <script src="/js/step/step.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/common/move.js"></script>
    <script type="text/javascript" src="/js/common/publishImg.js"></script>
    <script src="/js/common/xm-select.js?t=${currentDate?c}" type="text/javascript"></script>
    <script src="/js/common/selectize.js" type="text/javascript"></script>
    <script src="/js/common/zoom.js" type="text/javascript"></script>
    <script src="/js/accessory/accessoryPreStore.js?t=${currentDate?c}"></script>
    <script src="/js/common/moment.min.js" type="text/javascript"></script>
</#macro>

<style>

    ::-webkit-scrollbar-track-piece {

        background-color:#f8f8f8;

    }

    ::-webkit-scrollbar {

        width:15px;

        height:15px;

    }

    ::-webkit-scrollbar-thumb {

        background-color:#dddddd;

        background-clip:padding-box;

        min-height:28px;

    }

    ::-webkit-scrollbar-thumb:hover {

        background-color:#bbb;

    }
</style>
