<#macro search>
<section id="content">
    <section class="vbox">
        <section class="scrollable">

            <div class="layui-row">
                <form class="layui-form" style="margin: 0 auto;padding-top: 3px;" lay-filter="baseInfo">
                    <table>
                        <tr>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label">单号</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" name="clothesVersionNumber" id="clothesVersionNumber" autocomplete="off" class="layui-input">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label">款号</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" name="orderName" id="orderName" autocomplete="off" class="layui-input">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label"></label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <div class="layui-input-inline">
                                    <button class="layui-btn" lay-submit lay-filter="searchBeat">搜索</button>
                                </div>
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label">合同号</label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <input type="text" name="checkNumber" id="checkNumber" autocomplete="off" class="layui-input">
                            </td>
                            <td style="text-align: right;margin-bottom: 15px;">
                                <label class="layui-form-label"></label>
                            </td>
                            <td style="margin-bottom: 15px;">
                                <div class="layui-input-inline">
                                    <button class="layui-btn layui-btn-normal" lay-submit lay-filter="searchNumberBeat">搜索</button>
                                </div>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>



            <table id="accessoryReturnTable" lay-filter="accessoryReturnTable"></table>

            <script type="text/html" id="toolbarTop">
                <div class="layui-btn-container">
                    <button class="layui-btn layui-btn-sm" lay-event="clearFilter">清除所有筛选</button>
                    <button class="layui-btn layui-btn-sm" lay-event="addAccessoryReturn">辅料入库/修改</button>
                    <button class="layui-btn layui-btn-sm" lay-event="accessoryOutStore">辅料出库</button>
                    <button class="layui-btn layui-btn-sm" lay-event="exportExcel">导出</button>
                </div>
            </script>

            <script type="text/html" id="accessoryChildBar">
                <a class="layui-btn layui-btn-xs layui-btn-normal" lay-event="change">转补单</a>
                <a class="layui-btn layui-btn-xs layui-btn-danger" lay-event="deleteAccessoryInStore">删除</a>
            </script>

            <script type="text/html" id="accessoryStorageChildBar">
                <a class="layui-btn layui-btn-xs" lay-event="outStoreSingle">出库</a>
            </script>

        </section>
    </section>
    <#--<#include "fb_accessoryReturnAdd.ftl">-->
    <#--<@accessoryReturnAdd></@accessoryReturnAdd>-->
    <#--<#include "fb_accessoryOutStore.ftl">-->
    <#--<@accessoryOutStore></@accessoryOutStore>-->
</section>

    <link rel="stylesheet" href="/css/layui.css" type="text/css">
    <link rel="stylesheet" href="/css/step.css" type="text/css">
    <link rel="stylesheet" href="/css/soulTable.css" type="text/css">
    <link rel="stylesheet" href="/css/publish.css" type="text/css">
    <link rel="stylesheet" href="/css/cropper.css" type="text/css">
    <link rel="stylesheet" href="/css/selectize.bootstrap3.css" type="text/css">
    <link rel="stylesheet" href="/css/zoom.css" type="text/css">

    <#--<script src="/js/common/layui.js" type="text/javascript"></script>-->
    <script src="/js/common/layer.js"></script>
    <script src="/js/common/laydate.js" type="text/javascript" ></script>
    <script src="/js/step/step.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/common/move.js"></script>
    <script type="text/javascript" src="/js/common/publishImg.js"></script>
    <script src="/js/common/xm-select.js?t=${currentDate?c}" type="text/javascript"></script>
    <script src="/js/common/selectize.js" type="text/javascript"></script>
    <script src="/js/common/zoom.js" type="text/javascript"></script>
    <script src="/js/accessory/accessoryReturn.js?t=${currentDate?c}"></script>
    <script src="/js/common/moment.min.js" type="text/javascript"></script>
</#macro>

<style>
    button {
        background:rgb(45, 202, 147);
        opacity:0.86;
        color: white;
        font-family: PingFangSC-Semibold, sans-serif;
    }
    ::-webkit-scrollbar-track-piece {

        background-color:#f8f8f8;

    }

    ::-webkit-scrollbar {

        width:15px;

        height:15px;

    }

    ::-webkit-scrollbar-thumb {

        background-color:#dddddd;

        background-clip:padding-box;

        min-height:28px;

    }

    ::-webkit-scrollbar-thumb:hover {

        background-color:#bbb;

    }
</style>
