<#macro accessoryOutStore>
    <div id="accessoryOutStore">
        <form class="layui-form" style="margin: 0 auto;max-width:100%; min-height:400px; padding-top: 40px;">
            <table class="layui-hide" id="accessoryOutStoreTable" lay-filter="accessoryOutStoreTable"></table>
        </form>
    </div>
</#macro>