<#macro entities>
    <div id="qrCodeWin" style="display: none;cursor:default">
        <div style="overflow-x: auto;overflow-y:auto;height: 480px;">
            <div style="text-align: right;margin-bottom:10px;">
                <a id="closeQrCodeWin" href="#" style="font-size:20px"><i class="fa fa-times icon" style="color: rgb(182,182,182)"></i></a>
            </div>
            <div style="text-align: center">
                    <div style="width:50%;float:left;text-align: right;padding:0">版单号：</div><div class="col-md-6" style="width:50%;float:left;text-align: left;padding:0"><span id="printClothesVersionNumber">版单号</span></div><br>
                    <div style="width:50%;float:left;text-align: right;padding:0">订单号：</div><div class="col-md-6" style="width:50%;float:left;text-align: left;padding:0"><span id="printOrderName">订单号</span></div><br>
                    <div style="width:50%;float:left;text-align: right;padding:0">客户：</div><div class="col-md-6" style="width:50%;float:left;text-align: left;padding:0"><span id="printCustomerName">客户</span></div><br>
                    <div style="width:50%;float:left;text-align: right;padding:0">颜色：</div><div class="col-md-6" style="width:50%;float:left;text-align: left;padding:0"><span id="printColorName">白色</span></div><br>
                    <div style="width:50%;float:left;text-align: right;padding:0">缸号：</div><div class="col-md-6" style="width:50%;float:left;text-align: left;padding:0"><span id="printJarName">缸号</span></div><br>
                    <div style="width:50%;float:left;text-align: right;padding:0">床号：</div><div class="col-md-6" style="width:50%;float:left;text-align: left;padding:0"><span id="printBedNumber">2</span></div><br>
                    <div style="width:50%;float:left;text-align: right;padding:0">数量：</div><div class="col-md-6" style="width:50%;float:left;text-align: left;padding:0"><span id="printLayerCount">2</span></div><br>
                    <div style="width:50%;float:left;text-align: right;padding:0">扎号：</div><div class="col-md-6" style="width:50%;float:left;text-align: left;padding:0"><span id="printPackageNumber">2</span></div><br>
                    <div style="width:50%;float:left;text-align: right;padding:0">部位：</div><div class="col-md-6" style="width:50%;float:left;text-align: left;padding:0"><span id="printPartName">2</span></div><br>
                    <div style="width:50%;float:left;text-align: right;padding:0">尺码：</div><div class="col-md-6" style="width:50%;float:left;text-align: left;padding:0"><span id="printSizeName"></span></div><br>

                <div id="qrCode" style="width:100px; height:100px;margin: 0 auto;"></div>
            </div>
        </div>
    </div>

    <style>
        tr {
            height: 30px;
        }
    </style>
</#macro>
