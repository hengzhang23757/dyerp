<!DOCTYPE html>
<html>
<head>
    <#include "../feedback/fb_script.ftl">
    <@script> </@script>
    <link rel="stylesheet" href="/css/selectize.bootstrap3.css" type="text/css">
    <script src="/js/common/selectize.js" type="text/javascript"></script>
    <script src="/js/finishTailor/addFinishTailor.js?t=${currentDate?c}"></script>
</head>

<body>
<input  type="hidden" value="${basePath}"  id="basePath"/>
<input  type="hidden" value="${type}"  id="type"/>
<div class="col-md-12" style="padding-top: 20px;padding-bottom:10px;margin-left: 10px">
    <span style="font-size: 20px;font-family: PingFangSC-Semibold;color:rgb(55,56,57)">输入裁片信息</span>
</div>
<div class="row">
    <div class="col-md-12">
        <section class="panel panel-default">
            <header class="panel-heading font-bold">
                <span class="label bg-success pull-right"></span>基本信息
            </header>
            <table style="border-collapse:separate;border-spacing: 10px;width: 100%;" id="baseInfo">
                <tr class="col-md-12">
                    <td style="text-align: right;margin-bottom: 15px;">
                        <label class="control-label" style="">单号</label>
                    </td>
                    <td style="margin-bottom: 15px;" class="col-md-3">
                        <input id="clothesVersionNumber" class="form-control" placeholder="请输入单号" autocomplete="off">
                    </td>
                    <td style="text-align: right;margin-bottom: 15px;">
                        <label class="control-label" style="">款号</label>
                    </td>
                    <td style="margin-bottom: 15px;" class="col-md-3">
                        <input id="orderName" class="form-control" placeholder="请输入款号" autocomplete="off">
                    </td>
                    <td style="text-align: right;margin-bottom: 15px;">
                        <label class="control-label" style="">客户</label>
                    </td>
                    <td style="margin-bottom: 15px;" class="col-md-3">
                        <input id="customerName" class="form-control">
                    </td>
                </tr>
                <tr class="col-md-12">
                    <td style="text-align: right;margin-bottom: 15px;">
                        <label class="control-label" style="">类型</label>
                    </td>
                    <td style="margin-bottom: 15px;" class="col-md-3">
                        <select id="partName" class="form-control">
                            <option value="">请选择</option>
                            <option value="成品">成品</option>
                            <option value="返工">返工</option>
                            <option value="包装">包装</option>
                        </select>
                    </td>
                    <td style="text-align: right;margin-bottom: 15px;">
                        <label class="control-label" style="">件数</label>
                    </td>
                    <td style="margin-bottom: 15px;" class="col-md-3">
                        <input id="layerCount" class="form-control" oninput="value=value.replace(/[^\d]/g,'')">
                    </td>
                </tr>
            </table>
        </section>
        <div class="col-md-12" id="finishTailorTable" style="height: auto;overflow-x: auto;overflow-y: auto;margin-top: 10px" hidden></div>
        <section class="panel panel-default"  id="numberAddDiv">
            <header class="panel-heading font-bold">
                <span class="label bg-success pull-right"></span>数量录入
            </header>
            <div name="numberDiv" class="row" style="margin-top: 10px;margin-bottom: 10px;margin-left: 20px">
                <div  class="col-md-3">
                    <label class="control-label" style="">颜色</label>
                    <select name="colorName" class="form-control">
                    </select>
                </div>
                <div class="col-md-3">
                    <label class="control-label" style="">LOT色</label>
                    <select name="LOColor" class="form-control" autocomplete="off">
                        <option value="A">A</option>
                        <option value="B">B</option>
                        <option value="C">C</option>
                        <option value="D">D</option>
                        <option value="E">E</option>
                        <option value="F">F</option>
                        <option value="G">G</option>
                    </select>
                </div>
                <div class="col-md-3">
                    <button name="addNumber" class="btn btn-s-xs" style="border-radius: 5px;margin-left: 10px"  onclick="addNumber(this)">增加</button>
                    <button name="delNumber" class="btn btn-s-xs" style="border-radius: 5px;margin-left: 10px;background-color: rgb(236, 108, 98);display: none"  onclick="delNumber(this)">删除</button>
                </div>
            </div>
        </section>

        <section class="panel panel-default" id="addSizeDiv">
            <header class="panel-heading font-bold">
                <span class="label bg-success pull-right"></span>唛架配比
            </header>
            <div name="sizeRadioDiv" class="row" style="margin-top: 10px;margin-bottom: 10px;margin-left: 20px">
                <div  class="col-md-3">
                    <span style="font-weight: 700;color:black" name="orderOfSizeRadio">1</span>
                    <label class="control-label" style="">尺码</label>
                    <select name="size" class="form-control">
                    </select>
                </div>
                <div class="col-md-3">
                    <label class="control-label" style="">配比</label>
                    <input name="radio" class="form-control" autocomplete="off" oninput="value=value.replace(/[^\d]/g,'')" onkeyup="totalSizeRadio(this)">
                    <div style="font-size: 10px;font-weight: 700;color:black">总计：<span name="totalSizeRadio">0</span></div>
                </div>
                <div class="col-md-3">
                    <button name="addSize" class="btn btn-s-xs" style="border-radius: 5px;margin-left: 10px"  onclick="addSize(this)">增加</button>
                    <button name="delSize" class="btn btn-s-xs" style="border-radius: 5px;margin-left: 10px;background-color: rgb(236, 108, 98);display: none"  onclick="delSize(this)">删除</button>
                    <div style="font-size: 10px;font-weight: 700;color:black">总件数：<span name="totalClothesCount">0</span></div>
                </div>
            </div>
        </section>
    </div>
</div>
<div class="col-md-12" style="text-align: center;margin-top: auto">
    <button  class="btn btn-s-lg" style="border-radius: 5px;"  onclick="addOrder()">生成</button>
</div>
</body>
</html>
<style>
    button {
        background:rgb(45, 202, 147);
        opacity:0.86;
        color: white;
        font-family: PingFangSC-Semibold, sans-serif;
    }

    label {
        font-family: PingFangSC-Semibold;
    }

</style>
