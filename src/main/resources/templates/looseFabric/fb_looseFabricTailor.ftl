<#macro search>
    <section id="content">
        <section class="vbox">
            <section class="scrollable padder water-mark-visible-ff">
                <div id="mainFrameTabs" style="height: 90%;">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active noclose" id="orderListTab"><a id="tailorA" href="#orderListDiv" data-toggle="tab">扎号信息</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="orderListDiv" style="background-color: #f7f7f7;overflow-y:auto">
                            <div class="col-md-12" style="padding-top: 20px">
                                <section class="panel panel-default">
                                    <div class="panel-body" style="text-align: left;">
                                        <div class="row" style="margin-left: 0;margin-bottom: 20px">
                                            <button  class="btn btn-s-lg" style="border-radius: 5px;color:white;font-family: PingFangSC-Semibold, sans-serif;"  onclick="addLooseFabric()">输入松布信息</button>
                                        </div>
                                        <div id="tailorTableDiv" style="text-align: center;font-family: PingFangSC-Semibold,sans-serif;">
                                            <table class="table table-striped" id="tailorTable" >
                                                <thead>
                                                <tr bgcolor="#ffcb99" style="color: black;">
                                                    <input type="text" hidden id="numberID" value="1">
                                                    <th style="width: 30px;text-align:center;font-size:14px"><input type="checkbox" onclick="checkAll(this)"></th>
                                                    <th style="width: 60px;text-align:center;font-size:14px">序号</th>
                                                    <th style="width: 90px;text-align:center;font-size:14px">订单号</th>
                                                    <th style="width: 90px;text-align:center;font-size:14px">版单号</th>
                                                    <th style="width: 220px;text-align:center;font-size:14px">面料名称</th>
                                                    <th style="width: 90px;text-align:center;font-size:14px">面料颜色</th>
                                                    <th style="width: 90px;text-align:center;font-size:14px">缸号</th>
                                                    <th style="width: 60px;text-align:center;font-size:14px">卷次</th>
                                                    <th style="width: 60px;text-align:center;font-size:14px">卷号</th>
                                                    <th style="width: 60px;text-align:center;font-size:14px">总卷数</th>
                                                    <th style="width: 60px;text-align:center;font-size:14px">重量</th>
                                                    <th style="width: 60px;text-align:center;font-size:14px">单位</th>
                                                    <th style="width: 90px;text-align:center;font-size:14px">订单颜色</th>
                                                    <th style="width: 120px;text-align:center;font-size:14px">时间</th>
                                                    <th style="width: 90px;text-align:center;font-size:14px">松布时长</th>
                                                    <th style="width: 120px;text-align:center;font-size:14px">出库类型</th>
                                                    <th style="width: 120px;text-align:center;font-size:14px">位置</th>
                                                    <th style="width: 90px;text-align:center;font-size:14px">二维码</th>
                                                    <th style="width: 120px;text-align:center;font-size:14px">操作</th>
                                                </tr>
                                                </thead>
                                                <tbody id="tailorBody">
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="col-md-12" style="text-align: center;margin-top: 20px" id="saveButton">
                                            <button class="btn btn-s-lg" style="border-radius: 5px;color:white;font-family: PingFangSC-Semibold;"  onclick="printer()">打印</button>
                                        </div>
                                    </div>
                                </section>
                            </div>

                        </div>

                    </div>
                </div>
                <#include "fb_printWin.ftl">
                <@entities></@entities>
            </section>
        </section>
        <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen"
           data-target="#nav"></a>
    </section>
    <aside class="bg-light lter b-l aside-md hide" id="notes">
        <div class="wrapper">Notification</div>
    </aside>
    </section>
    <link rel="stylesheet" href="/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="/css/b.tabs.css" type="text/css">

    <script src="/js/common/jquery.blockUI.js" type="text/javascript" ></script>
    <script src="/js/common/b.tabs.js?t=${currentDate?c}"></script>
    <script src="/js/common/LodopFuncs.js" type="text/javascript" ></script>
    <script src="/js/common/moment.min.js" type="text/javascript"></script>
    <script src="/js/looseFabric/looseFabricTailor.js?t=${currentDate?c}"></script>

</#macro>

<style>
    button {
        background:rgb(45, 202, 147);
        opacity:0.86;
        color: white;
        font-family: PingFangSC-Semibold;
    }

    ::-webkit-scrollbar-track-piece {

        background-color:#f8f8f8;

    }

    ::-webkit-scrollbar {

        width:15px;

        height:15px;

    }

    ::-webkit-scrollbar-thumb {

        background-color:#dddddd;

        background-clip:padding-box;

        min-height:28px;

    }

    ::-webkit-scrollbar-thumb:hover {

        background-color:#bbb;

    }
</style>
