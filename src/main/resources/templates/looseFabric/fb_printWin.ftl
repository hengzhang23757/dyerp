<#macro entities>
    <div id="qrCodeWin" style="display: none;cursor:default">
        <div style="overflow-x: auto;overflow-y:auto;height: 480px;">
            <div style="text-align: right;margin-bottom:10px;">
                <a id="closeQrCodeWin" href="#" style="font-size:20px"><i class="fa fa-times icon" style="color: rgb(182,182,182)"></i></a>
            </div>
            <div style="text-align: center">
                    <div style="width:50%;float:left;text-align: right;padding:0">版单号：</div><div class="col-md-6" style="width:50%;float:left;text-align: left;padding:0"><span id="printClothesVersionNumber">版单号</span></div><br>
                    <div style="width:50%;float:left;text-align: right;padding:0">订单号：</div><div class="col-md-6" style="width:50%;float:left;text-align: left;padding:0"><span id="printOrderName">订单号</span></div><br>
                    <div style="width:50%;float:left;text-align: right;padding:0">面料：</div><div class="col-md-6" style="width:50%;float:left;text-align: left;padding:0"><span id="printFabricName">面料</span></div><br>
                    <div style="width:50%;float:left;text-align: right;padding:0">面料颜色：</div><div class="col-md-6" style="width:50%;float:left;text-align: left;padding:0"><span id="printFabricColor">面料颜色</span></div><br>
                    <div style="width:50%;float:left;text-align: right;padding:0">缸号：</div><div class="col-md-6" style="width:50%;float:left;text-align: left;padding:0"><span id="printJarNumber">缸号</span></div><br>
                    <div style="width:50%;float:left;text-align: right;padding:0">卷次：</div><div class="col-md-6" style="width:50%;float:left;text-align: left;padding:0"><span id="printBatchOrder">卷次</span></div><br>
                    <div style="width:50%;float:left;text-align: right;padding:0">重量：</div><div class="col-md-6" style="width:50%;float:left;text-align: left;padding:0"><span id="printWeight"></span></div><br>
                    <div style="width:50%;float:left;text-align: right;padding:0">订单颜色：</div><div class="col-md-6" style="width:50%;float:left;text-align: left;padding:0"><span id="printClorName"></span></div><br>
                    <div style="width:50%;float:left;text-align: right;padding:0">时间：</div><div class="col-md-6" style="width:50%;float:left;text-align: left;padding:0"><span id="printLooseTime"></span></div><br>
                    <div style="width:50%;float:left;text-align: right;padding:0">松布时长：</div><div class="col-md-6" style="width:50%;float:left;text-align: left;padding:0"><span id="printLooseHour"></span></div><br>
                    <div style="width:50%;float:left;text-align: right;padding:0">出库类型：</div><div class="col-md-6" style="width:50%;float:left;text-align: left;padding:0"><span id="printOperateType"></span></div><br>
                <div id="qrCode" style="width:100px; height:100px;margin: 0 auto;"></div>
            </div>
        </div>
    </div>

    <style>
        tr {
            height: 30px;
        }
    </style>
</#macro>
