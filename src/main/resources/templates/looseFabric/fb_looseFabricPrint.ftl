<#macro search>
<section id="content">
    <section class="vbox">
        <section class="scrollable padder water-mark-visible-ff">
            <div class="col-md-12" style="padding-top: 10px;">
                <section class="panel panel-default">
                    <div class="panel-body">
                        <div class="row" style="margin-bottom: 10px">
                            <table style="border-collapse:separate;border-spacing: 10px;width: 100%;" id="baseInfo">
                                <tr>
                                    <td style="text-align: right;margin-bottom: 15px;">
                                        <label class="control-label" style="">单号</label>
                                    </td>
                                    <td style="margin-bottom: 15px;">
                                        <input id="clothesVersionNumber" class="form-control" autocomplete="off" style="width: 150px;">
                                    </td>
                                    <td style="text-align: right;margin-bottom: 15px;">
                                        <label class="control-label" style="">款号</label>
                                    </td>
                                    <td style="margin-bottom: 15px;">
                                        <input id="orderName" class="form-control" autocomplete="off" style="width: 150px;">
                                    </td>
                                    <td style="text-align: right;margin-bottom: 15px;">
                                        <label class="control-label" style="">色组</label>
                                    </td>
                                    <td style="margin-bottom: 15px;">
                                        <select id="colorName" class="form-control" autocomplete="off" style="width: 150px;"></select>
                                    </td>
                                    <td style="text-align: right;margin-bottom: 15px;">
                                        <label class="control-label" style="">面料</label>
                                    </td>
                                    <td style="margin-bottom: 15px;">
                                        <select id="fabricName" class="form-control" autocomplete="off" style="width: 150px;"></select>
                                    </td>
                                    <td style="text-align: right;margin-bottom: 15px;">
                                        <label class="control-label" style="">颜色</label>
                                    </td>
                                    <td style="margin-bottom: 15px;">
                                        <select id="fabricColor" class="form-control" style="width: 150px;"></select>
                                    </td>
                                    <td style="margin-bottom: 15px;">
                                        <button  class="btn" style="border-radius: 5px;color:white;font-family: PingFangSC-Semibold, sans-serif;width:70px;"  onclick="search()">查找</button>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div id="tailorTableDiv" style="text-align: center;font-family: PingFangSC-Semibold,sans-serif;">
                            <table class="table table-striped" id="tailorInfoTable" >
                            </table>
                        </div>
                        <div class="col-md-12" style="text-align: center;margin-top: 20px">
                            <button  class="btn btn-s-lg" style="border-radius: 5px;color:white;font-family: PingFangSC-Semibold;"  onclick="printer()">打印</button>
                        </div>
                    </div>
                </section>
            </div>
            <#include "fb_printWin.ftl">
                <@entities></@entities>
            <#include "fb_updateCount.ftl">
                <@entities></@entities>
        </section>
    </section>
    <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen"
       data-target="#nav"></a>
</section>
<aside class="bg-light lter b-l aside-md hide" id="notes">
    <div class="wrapper">Notification</div>
</aside>
</section>
<link rel="stylesheet" href="/css/bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="/css/b.tabs.css" type="text/css">

<script src="/js/common/jquery.blockUI.js" type="text/javascript" ></script>
<script src="/js/common/b.tabs.js?t=${currentDate?c}"></script>
<script src="/js/common/LodopFuncs.js" type="text/javascript" ></script>
<script src="/js/common/moment.min.js" type="text/javascript"></script>
<script src="/js/looseFabric/looseFabricPrint.js?t=${currentDate?c}"></script>

</#macro>

<style>
    button {
        background:rgb(45, 202, 147);
        opacity:0.86;
        color: white;
        font-family: PingFangSC-Semibold;
    }

    ::-webkit-scrollbar-track-piece {

        background-color:#f8f8f8;

    }

    ::-webkit-scrollbar {

        width:15px;

        height:15px;

    }

    ::-webkit-scrollbar-thumb {

        background-color:#dddddd;

        background-clip:padding-box;

        min-height:28px;

    }

    ::-webkit-scrollbar-thumb:hover {

        background-color:#bbb;

    }

</style>
