<#macro entities>
    <div id="updateLoColor" style="display: none;cursor:default">
        <div style="overflow-x: auto;overflow-y:auto;">
            <div style="text-align: right;margin-bottom:20px">
                <a id="updateLoNo" href="#" style="font-size:20px"><i class="fa fa-times icon" style="color: rgb(182,182,182)"></i></a>
            </div>
            <table style="border-collapse:separate;border-spacing: 10px;width: 100%;">
                <tr>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">订单</label>
                    </td>
                    <td>
                        <input id="orderName1" class="form-control" autocomplete="off" style="width: 100%;margin-bottom: 15px;" readonly>
                    </td>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">版单</label>
                    </td>
                    <td>
                        <input id="clothesVersionNumber1" class="form-control" autocomplete="off" style="width: 100%;margin-bottom: 15px;" readonly>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">订单颜色</label>
                    </td>
                    <td>
                        <input id="colorName1" class="form-control" autocomplete="off" style="width: 100%;margin-bottom: 15px;" readonly>
                    </td>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">面料颜色</label>
                    </td>
                    <td>
                        <input id="fabricColor1" class="form-control" autocomplete="off" style="width: 100%;margin-bottom: 15px;">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">面料</label>
                    </td>
                    <td colspan="3">
                        <input id="fabricName1" class="form-control" autocomplete="off" style="width: 100%;margin-bottom: 15px;" readonly>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">原始缸号</label>
                    </td>
                    <td>
                        <input id="jarNumber1" class="form-control" autocomplete="off" style="margin-bottom: 15px;width: 100%;" readonly>
                    </td>
                    <td>
                        <label class="control-label" style="margin-bottom: 15px;text-align: right;">修改成</label>
                    </td>
                    <td>
                        <input id="updateJar1" class="form-control" autocomplete="off" style="width: 100%;margin-bottom: 15px;">
                    </td>
                </tr>
            </table>
        </div>
        <div class="col-sm-12" style="padding-top: 10px">
            <button id="updateLoYes" class="btn btn-s-lg" style="border-radius: 5px;text-align: center;color:white;font-family: PingFangSC-Semibold;">保存</button>
        </div>
    </div>
</#macro>