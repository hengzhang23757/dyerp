<!DOCTYPE html>
<html>
<head>
    <#include "../feedback/fb_script.ftl">
    <@script> </@script>
    <link rel="stylesheet" href="/css/selectize.bootstrap3.css" type="text/css">
    <script src="/js/common/selectize.js" type="text/javascript"></script>
    <script src="/js/common/moment.min.js" type="text/javascript"></script>
    <script src="/js/looseFabric/addLooseFabricTailor.js?t=${currentDate?c}"></script>
</head>

<body>
<input  type="hidden" value="${basePath}"  id="basePath"/>
    <div class="col-md-12" style="padding-top: 20px;padding-bottom:10px;">
        <span style="font-size: 20px;font-weight: 900;font-family: PingFangSC-Semibold;color:rgb(55,56,57)">输入松布信息</span>
    </div>
    <div class="row" id="generateFabricInfo">
        <div class="col-md-12">
            <section class="panel panel-default">
                <header class="panel-heading font-bold">
                    <span class="label bg-success pull-right"></span>基本信息
                </header>
                <table style="border-collapse:separate;border-spacing: 10px;width: 100%;" id="baseInfo">
                    <tr>
                        <td style="text-align: right;margin-bottom: 15px;">
                            <label class="control-label" style="">单号</label>
                        </td>
                        <td style="margin-bottom: 15px;">
                            <input id="clothesVersionNumber" style="width: 200px" class="form-control" autocomplete="off">
                        </td>
                        <td style="text-align: right;margin-bottom: 15px;">
                            <label class="control-label" style="">款号</label>
                        </td>
                        <td style="margin-bottom: 15px;">
                            <input id="orderName" style="width: 200px" class="form-control" autocomplete="off">
                        </td>
                        <td style="text-align: right;margin-bottom: 15px;">
                            <label class="control-label" style="">色组</label>
                        </td>
                        <td style="margin-bottom: 15px;">
                            <select id="colorName" style="width: 200px" class="form-control"></select>
                        </td>
                        <td style="text-align: right;margin-bottom: 15px;">
                            <label class="control-label" style="">面料</label>
                        </td>
                        <td style="margin-bottom: 15px;">
                            <select id="fabricName" style="width: 200px" class="form-control"></select>
                        </td>
                        <td style="text-align: right;margin-bottom: 15px;">
                            <label class="control-label" style="">面料颜色</label>
                        </td>
                        <td style="margin-bottom: 15px;">
                            <select id="fabricColor" style="width: 200px" class="form-control"></select>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right;margin-bottom: 15px;">
                            <label class="control-label" style="">缸号</label>
                        </td>
                        <td style="margin-bottom: 15px;border-spacing:0">
                            <select id="jarNumber" style="width: 200px" class="form-control"></select>
                        </td>
                        <td style="text-align: right;margin-bottom: 15px;">
                            <label class="control-label" style="">LO色</label>
                        </td>
                        <td style="margin-bottom: 15px;border-spacing:0">
                            <select id="loColor" style="width: 200px" class="form-control">
                                <option value="无">无</option>
                                <option value="A">A</option>
                                <option value="B">B</option>
                                <option value="C">C</option>
                                <option value="D">D</option>
                                <option value="E">E</option>
                                <option value="F">F</option>
                            </select>
                        </td>
                        <td style="text-align: right;margin-bottom: 15px;">
                            <label class="control-label" style="">总卷数</label>
                        </td>
                        <td style="margin-bottom: 15px;">
                            <input id="batchNumber" readonly="readonly" style="width: 200px" class="form-control">
                        </td>
                        <td style="text-align: right;margin-bottom: 15px;">
                            <label class="control-label" style="">时长</label>
                        </td>
                        <td style="margin-bottom: 15px;">
                            <select id="looseHour" style="width: 200px" class="form-control">
                                <option value="24">24小时</option>
                                <option value="36">36小时</option>
                                <option value="48">48小时</option>
                                <option value="1">1小时</option>
                            </select>
                        </td>
                        <td style="text-align: right;margin-bottom: 15px;">
                            <label class="control-label" style="">位置</label>
                        </td>
                        <td style="margin-bottom: 15px;">
                            <select id="location" style="width: 200px" class="form-control"></select>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right;margin-bottom: 15px;">
                            <label class="control-label" style="">单位</label>
                        </td>
                        <td style="margin-bottom: 15px;">
                            <input id="unit" readonly="readonly" style="width: 200px" class="form-control">
                        </td>
                        <td style="text-align: right;margin-bottom: 15px;">
                            <label class="control-label" style="">用作部位</label>
                        </td>
                        <td style="margin-bottom: 15px;">
                            <select id="partName" style="width: 200px" class="form-control"></select>
                        </td>
                        <td style="text-align: right;margin-bottom: 15px;">
                            <label class="control-label" style="">裁数</label>
                        </td>
                        <td style="margin-bottom: 15px;">
                            <input id="cutInfo" readonly="readonly" style="width: 200px" class="form-control">
                        </td>
                        <td style="text-align: right;margin-bottom: 15px;">
                            <label class="control-label" style="">平均单耗</label>
                        </td>
                        <td style="margin-bottom: 15px;">
                            <input id="historyUnit" readonly="readonly" style="width: 200px" class="form-control">
                        </td>
                        <td style="text-align: right;margin-bottom: 15px;">
                            <label class="control-label" style="">还需面料</label>
                        </td>
                        <td style="margin-bottom: 15px;">
                            <input id="needFabric" readonly="readonly" style="width: 200px" class="form-control">
                        </td>
                    </tr>

                </table>
            </section>

            <section class="panel panel-default" id="addNumberDiv">
                <header class="panel-heading font-bold">
                    <span class="label bg-success pull-right"></span>缸号信息
                </header>
                <div class="row" style="margin-top: 10px;margin-bottom: 10px;margin-left: 20px">
                    <div class="col-md-4">
                        <div style="font-size: 20px;font-weight: 700;color:black">已生成：<span id="finishCount" style="font-size: 25px;font-weight: 700;color:red">0</span>卷</div>
                    </div>
                    <div  class="col-md-4">
                        <div style="font-size: 20px;font-weight: 700;color:black">还可生成：<span id="remainCount" style="font-size: 25px;font-weight: 700;color:red">0</span>卷</div>
                    </div>
                    <div  class="col-md-4">
                        <div style="font-size: 20px;font-weight: 700;color:black">本仓位可生成：<span id="locationCount" style="font-size: 25px;font-weight: 700;color:red">0</span>卷</div>
                    </div>
                </div>
                <div name="fabricInfoDiv" class="row" style="margin-top: 20px;margin-bottom: 10px;margin-left: 20px">
                    <div  class="col-md-3">
                        <span style="font-weight: 700;color:black" name="orderOfLayer">1</span>
                        <label class="control-label" style="">第</label>
                        <input name="batchOrder" class="form-control" autocomplete="off" readonly>
                        <label class="control-label" style="">卷</label>
                    </div>
                    <div  class="col-md-2">
                        <label class="control-label" style="">卷号</label>
                        <input name="batchNumber" class="form-control" autocomplete="off" onkeydown="moveCursor(event,this)">
                    </div>
                    <div class="col-md-2">
                        <label class="control-label" style="">重量</label>
                        <input name="weight" class="form-control" autocomplete="off" onkeyup="totalWeight(this)" onkeydown="moveCursor(event,this)">
                        <div style="font-size: 10px;font-weight: 700;color:black">总计：<span name="totalWeight">0</span></div>
                    </div>
                    <div class="col-md-3">
                        <label class="control-label" style="">出库类型</label>
                        <select name="operateType" class="form-control" autocomplete="off">
                            <option value="发裁床A组">发裁床A组</option>
                            <option value="发裁床B组">发裁床B组</option>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <button name="addNumber" class="btn btn-s-xs" style="border-radius: 5px;margin-left: 10px"  onclick="addNumber(this)">增加</button>
                        <button name="delNumber" class="btn btn-s-xs" style="border-radius: 5px;margin-left: 10px;background-color: rgb(236, 108, 98);display: none"  onclick="delNumber(this)">删除</button>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <div class="col-md-12" style="text-align: center;margin-top: auto">
        <button  class="btn btn-s-lg" style="border-radius: 5px;"  onclick="kgToP()">公斤转磅</button>
        <button  class="btn btn-s-lg" style="border-radius: 5px;"  onclick="pToKg()">磅转公斤</button>
        <button  class="btn btn-s-lg" style="border-radius: 5px;"  onclick="addFabric()">生成</button>
    </div>
</body>
</html>
<style>
    button {
        background:rgb(45, 202, 147);
        opacity:0.86;
        color: white;
        font-family: PingFangSC-Semibold, sans-serif;
    }

    label {
        font-family: PingFangSC-Semibold;
    }

    ::-webkit-scrollbar-track-piece {

        background-color:#f8f8f8;

    }

    ::-webkit-scrollbar {

        width:15px;

        height:15px;

    }

    ::-webkit-scrollbar-thumb {

        background-color:#dddddd;

        background-clip:padding-box;

        min-height:28px;

    }

    ::-webkit-scrollbar-thumb:hover {

        background-color:#bbb;

    }
</style>
